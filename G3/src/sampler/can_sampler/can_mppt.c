/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : NGMC-HF(Next Generation Controller - High Feature)
*
*  FILENAME : can_MPPTifier.c
*  CREATOR  : Marco Yang                DATE: 2008-07-25 09:38
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <time.h>

#include "can_sampler_main.h"
#include "can_mppt.h"
#include "run_timer.h"


extern CAN_SAMPLER_DATA	g_CanData;


#define	INVALID_MPPT_TYPE	(0xffffffff)

static DWORD GetTestReduceTimes(void);

//------------added by Jimmy Wu 2013.11.08
//static int GetPhaseNo(int iSeqNo)
//{
//	return iSeqNo % RT_AC_PHASE_NUM;
//}
static BOOL g_bNeedClearMpptIDs = FALSE;
static void ClrMpptFalshInfo(void);//���λ�ú���Ϣ
static BOOL	MpptAnalyseSerialNo(int iAddr);
static SIG_ENUM MpptSampleInputPowerCmd(int iAddr);
static BOOL MpptUnpackInputPowerCmd(int iAddr,
				    int iReadLen,
				    BYTE* pbyRcvBuf);
static void ClrMpptFalshInfo()
{
	int i = 0;
	for(i = 0; i < MAX_NUM_MPPT; i++)
	{
		g_CanData.CanFlashData.aMpptInfo[i].dwSerialNo = 0;
		g_CanData.CanFlashData.aMpptInfo[i].iPositionNo = -1;
	}
	CAN_WriteFlashData(&(g_CanData.CanFlashData));
	g_bNeedClearMpptIDs = TRUE;
}

static int gs_iMpptFailedTimer = MT_3_DAYS ; // 3 days is the default value

/*==========================================================================*
* FUNCTION : PackAndSendMpptCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: UINT   uiMsgType    : 
*            UINT   uiDestAddr   : 
*            UINT   uiCmdType    : 
*            UINT   uiValueTypeH : 
*            UINT   uiValueTypeL : 
*            float  fParam       : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-08 13:57
*==========================================================================*/
static void PackAndSendMpptCmd(UINT uiMsgType,
			       UINT uiDestAddr,
			       UINT uiCmdType,
			       UINT uiValueTypeH,
			       UINT uiValueTypeL,
			       BYTE* pbyValue)
{
	BYTE*	pbySendBuf = g_CanData.CanCommInfo.abySendBuf;
	int		i;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_MPPT_CONTROLLER,
		uiDestAddr, 
		CAN_SELF_ADDR,
		CAN_ERR_CODE,
		CAN_FRAME_DATA_LEN,
		uiCmdType,
		pbySendBuf);

	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	//CAN_FloatToString(fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);	

	for(i = 0; i < 4; i++)
	{
		pbySendBuf[CAN_FRAME_OFFSET_VALUE + i] = *(pbyValue + i);
	}

	write(g_CanData.CanCommInfo.iCanHandle, 
		(void *)pbySendBuf,
		CAN_FRAME_LEN);

	return;
}

/*static BOOL BarCodeDataIsValid(BYTE byData)
{
	if ((0x20 != byData) && (0xff != byData))
	{
		return TRUE;
	}

	return FALSE;
}*/
/*=============================================================================*
* FUNCTION: MtSetComm()
* PURPOSE  : In Slave mode report data
* RETURN   : void
* ARGUMENTS:
* CALLS    : 
* CALLED BY: 
*							DLLExport BOOL Query()
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-05 13:59
*==========================================================================*/
static int MtSetComm(HANDLE hself, int idTimer, int iAddr)
{
	if(g_CanData.aRoughDataGroup[GROUP_MT_ALL_NO_RESPONSE].iValue)
	{
		g_CanData.aRoughDataMppt[iAddr][MPPT_INTERRUPT_ST].iValue 
			= MT_COMM_ALL_INTERRUPT_ST;
	}
	else
	{
		g_CanData.aRoughDataMppt[iAddr][MPPT_INTERRUPT_ST].iValue 
			= MT_COMM_INTERRUPT_ST;
	}	
	return 1;
}

static int MtSetCalcAlarm(IN HANDLE hself, IN int idTimer,IN int iAddr)
{
	g_CanData.bCalcAlarm[iAddr][0] = TRUE;

	return 0;
}
#define MPPT_NOUSED				-1
/*==========================================================================*
* FUNCTION : UnpackRtAlarm
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr      : 
*            BYTE*  pbyAlmBuff : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:29
*==========================================================================*/
static void UnpackMpptAlarm(int iAddr,
			    BYTE* pbyAlmBuff)
{
	BYTE	byMask ;
	BYTE	byRough;
	//The order is based on the protocol, do not change it.
	static int		s_aiSigIdx[BYTES_OF_ALM_INFO * BITS_PER_BYTE] = 
	{
		MPPT_DC_OVER_VOLTAGE,
		MPPT_OVER_TEMP_STATUS,
		MPPT_RECTFIER_FAULT,
		MPPT_PROTECTION,
		MPPT_FAN_FAULT,
		MPPT_EEPROM_FAULT,
		MPPT_NOUSED,		//MPPT_POWER_LIMITED_FOR_AC,
		MPPT_POWER_LIMITED,		//MPPT_POWER_LIMITED_FOR_TEMP,

		MPPT_NOUSED,
		MPPT_DC_ON_OFF_STATUS,
		MPPT_FAN_FULL_SPEED,
		MPPT_WALKIN_STATUS,
		MPPT_AC_ON_OFF_STATUS,
		MPPT_NOUSED,
		MPPT_SHARE_CURR_STATUS,
		MPPT_NOUSED,

		MPPT_SEQ_START_STATUS,
		MPPT_AC_UNDER_VOLT_STATUS,
		MPPT_NOUSED,
		MPPT_NOUSED,
		MPPT_SEVERE_CURR_IMBALANCE,
		MPPT_NOUSED,
		MPPT_NOUSED,
		MPPT_NOUSED,

		MPPT_CURR_IMBALANCE,
		MPPT_NOUSED,
		MPPT_NOUSED,
		MPPT_NOUSED,
		MPPT_NOUSED,
		MPPT_OUTPUT_FUSE_STATUS,	//MPPT_ESTOP_STATUS,
		MPPT_INPUT_NOT_DC,	
		MPPT_NOUSED,
	};

	int		i, j;

	for(i = 0; i < BYTES_OF_ALM_INFO; i++)
	{
		byMask = 0x80;
		byRough = *(pbyAlmBuff + i);

		for(j = 0; j < BITS_PER_BYTE; j++)
		{
			if(s_aiSigIdx[i * BITS_PER_BYTE + j] != MPPT_NOUSED)
			{
				int		iIdx = s_aiSigIdx[i * BITS_PER_BYTE + j];
				g_CanData.aRoughDataMppt[iAddr][iIdx].iValue
					= (byRough & byMask) ? 1 : 0;

				//printf("g_CanData.aRoughDataMppt[%d][%d].iValue = %d\n",iAddr, iIdx, g_CanData.aRoughDataMppt[iAddr][iIdx].iValue);

			}
			byMask >>= 1;
		}
	}


	if(g_CanData.aRoughDataMppt[iAddr][MPPT_DC_ON_OFF_STATUS].iValue == MT_DC_OFF && !g_CanData.bCalcAlarm[iAddr][1] && !g_CanData.bCalcAlarm[iAddr][0])
	{
		Timer_Set(RunThread_GetId(NULL), ID_TIMER_MPPT_CALC_ALARM_20MIN + iAddr, MT_20_MINUTES/GetTestReduceTimes(),(ON_TIMER_PROC)MtSetCalcAlarm, (DWORD)iAddr);
		g_CanData.bCalcAlarm[iAddr][1] = TRUE;
	}
	if(g_CanData.aRoughDataMppt[iAddr][MPPT_DC_ON_OFF_STATUS].iValue == MT_DC_ON)
	{
		Timer_Reset(RunThread_GetId(NULL), ID_TIMER_MPPT_CALC_ALARM_20MIN + iAddr);
		g_CanData.bCalcAlarm[iAddr][0] = TRUE;
	}

	//printf("1----g_CanData.bCalcAlarm[iAddr][1]= %d,g_CanData.bCalcAlarm[iAddr][0] = %d\n", g_CanData.bCalcAlarm[iAddr][1],g_CanData.bCalcAlarm[iAddr][0]);

	if(!g_CanData.bCalcAlarm[iAddr][0])
	{
		for(i = 0; i < BYTES_OF_ALM_INFO; i++)
		{
			byMask = 0x80;
			byRough = *(pbyAlmBuff + i);

			for(j = 0; j < BITS_PER_BYTE; j++)
			{
				if(s_aiSigIdx[i * BITS_PER_BYTE + j] != MPPT_NOUSED)
				{
					int		iIdx = s_aiSigIdx[i * BITS_PER_BYTE + j];
					g_CanData.aRoughDataMppt[iAddr][iIdx].iValue
						=  0;

					//printf("g_CanData.aRoughDataMppt[%d][%d].iValue = %d\n",iAddr, iIdx, g_CanData.aRoughDataMppt[iAddr][iIdx].iValue);

				}
				byMask >>= 1;
			}
		}
		g_CanData.aRoughDataMppt[iAddr][MPPT_DC_ON_OFF_STATUS].iValue = MT_DC_OFF; 
	}

	return;
}


//#define DEBUG_MPPT			1

enum	MT_CMD00_FRAME_ORDER
{
	MT_CMD00_ORDER_DC_VOLT = 0,
	MT_CMD00_ORDER_REAL_CURR,
	MT_CMD00_ORDER_CURR_LMT,
#ifdef DEBUG_MPPT
	MT_CMD00_ORDER_INPUT_VOLT,
	MT_CMD00_ORDER_OUTPUT_POWER,
#else
	MT_CMD00_ORDER_OUTPUT_POWER,
	MT_CMD00_ORDER_INPUT_VOLT,
#endif
	MT_CMD00_ORDER_INPUT_CURR,
	MT_CMD00_ORDER_ALARM_BITS,
	MT_CMD00_ORDER_SN_LOW,
	MT_CMD00_ORDER_RUN_TIME,

	MT_CMD00_ORDER_FRAME_NUM,
};
/*==========================================================================*
* FUNCTION : RtUnpackCmd00
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
*            int    iReadLen  : 
*            BYTE*  pbyRcvBuf : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:29
*==========================================================================*/
static BOOL MtUnpackCmd00(int iAddr,
			  int iReadLen,
			  BYTE* pbyRcvBuf)
{
	int			i;
	//UINT		uiSerialNo;
	int			iFrameOrder = MT_CMD00_ORDER_DC_VOLT;

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;
		float		fRunTime;

		BYTE*		pbyFrame = pbyRcvBuf + i * CAN_FRAME_LEN;

		/*TRACE("**********i = %d  VAL_TYPE_H = %02X  VAL_TYPE_L = %02X  ",
		i,
		pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H],
		pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L]);*/

		if((CAN_GetProtNo(pbyFrame) != PROTNO_MPPT_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyFrame) != (UINT)iAddr)
			|| (pbyFrame[CAN_FRAME_OFFSET_VAL_TYPE_H] != 0))
		{
			/*TRACE(" It is NOT frame of mine!!!!\n");*/
			continue;
		}

		switch (pbyFrame[CAN_FRAME_OFFSET_VAL_TYPE_L])
		{
		case MT_VAL_TYPE_R_DC_VOLT:
			g_CanData.aRoughDataMppt[iAddr][MPPT_DC_VOLTAGE].fValue 
				= CAN_StringToFloat(pValueBuf);
			if(MT_CMD00_ORDER_DC_VOLT == iFrameOrder)
			{
				iFrameOrder++;
			}
			//printf(" iAddr = %d, SigIdx = %d, MPPT_DC_VOLTAGE = %.02f ok!!!\n", 
			//	iAddr,
			//	MPPT_DC_VOLTAGE,
			//	g_CanData.aRoughDataMppt[iAddr][MPPT_DC_VOLTAGE].fValue); 
			break;

		case MT_VAL_TYPE_R_REAL_CURR:
			g_CanData.aRoughDataMppt[iAddr][MPPT_ACTUAL_CURRENT].fValue 
				= CAN_StringToFloat(pValueBuf);
			if(MT_CMD00_ORDER_REAL_CURR == iFrameOrder)
			{
				iFrameOrder++;
			}
			/*printf(" iAddr = %d, SigIdx = %d, MPPT_ACTUAL_CURRENT = %.02f ok!!!\n", 
			iAddr,
			MPPT_DC_VOLTAGE,
			g_CanData.aRoughDataMppt[iAddr][MPPT_ACTUAL_CURRENT].fValue);*/
			break;

		case MT_VAL_TYPE_R_CURR_LMT:
			g_CanData.aRoughDataMppt[iAddr][MPPT_CURRENT_LIMIT].fValue 
				= 100.0 * CAN_StringToFloat(pValueBuf);
			if(MT_CMD00_ORDER_CURR_LMT == iFrameOrder)
			{
				iFrameOrder++;
			}
			/*printf(" iAddr = %d, SigIdx = %d, MPPT_CURRENT_LIMIT = %.02f ok!!!\n", 
			iAddr,
			MPPT_DC_VOLTAGE,
			g_CanData.aRoughDataMppt[iAddr][MPPT_CURRENT_LIMIT].fValue);*/

			break;
#ifdef DEBUG_MPPT
		case MT_VAL_TYPE_R_AC_VOLT:
			g_CanData.aRoughDataMppt[iAddr][MPPT_AC_VOLTAGE].fValue 
				= CAN_StringToFloat(pValueBuf);


			if(MT_CMD00_ORDER_INPUT_VOLT == iFrameOrder)
			{
				iFrameOrder++;
			}

			/*printf(" iAddr = %d, SigIdx = %d, MPPT_AC_VOLTAGE = %.02f ok!!!\n", 
			iAddr,
			MPPT_DC_VOLTAGE,
			g_CanData.aRoughDataMppt[iAddr][MPPT_AC_VOLTAGE].fValue);*/

			break;
		case MT_VAL_TYPE_R_OUTPUT_POWER:
			g_CanData.aRoughDataMppt[iAddr][MPPT_OUTPUT_POWER].fValue
				= CAN_StringToFloat(pValueBuf);

			if(MT_CMD00_ORDER_OUTPUT_POWER == iFrameOrder)
			{
				iFrameOrder++;
			}

			/*printf(" iAddr = %d, SigIdx = %d, MPPT_AC_VOLTAGE = %.02f ok!!!\n",
			iAddr,
			MPPT_DC_VOLTAGE,
			g_CanData.aRoughDataMppt[iAddr][MPPT_AC_VOLTAGE].fValue);*/
			break;
		
#else
		case MT_VAL_TYPE_R_OUTPUT_POWER:
			g_CanData.aRoughDataMppt[iAddr][MPPT_OUTPUT_POWER].fValue
				= CAN_StringToFloat(pValueBuf);



			if(MT_CMD00_ORDER_OUTPUT_POWER == iFrameOrder)
			{
				iFrameOrder++;
			}

			/*printf(" iAddr = %d, SigIdx = %d, MPPT_AC_VOLTAGE = %.02f ok!!!\n",
			iAddr,
			MPPT_DC_VOLTAGE,
			g_CanData.aRoughDataMppt[iAddr][MPPT_AC_VOLTAGE].fValue);*/
			break;
		case MT_VAL_TYPE_R_AC_VOLT:
			g_CanData.aRoughDataMppt[iAddr][MPPT_AC_VOLTAGE].fValue 
				= CAN_StringToFloat(pValueBuf);


			if(MT_CMD00_ORDER_INPUT_VOLT == iFrameOrder)
			{
				iFrameOrder++;
			}

			/*printf(" iAddr = %d, SigIdx = %d, MPPT_AC_VOLTAGE = %.02f ok!!!\n", 
			iAddr,
			MPPT_DC_VOLTAGE,
			g_CanData.aRoughDataMppt[iAddr][MPPT_AC_VOLTAGE].fValue);*/

			break;

#endif		
		case MT_VAL_TYPE_R_PFC_TEMP:
			g_CanData.aRoughDataMppt[iAddr][MPPT_CURRENT_FOR_INPUT].fValue 
				= CAN_StringToFloat(pValueBuf);
			if(MT_CMD00_ORDER_INPUT_CURR == iFrameOrder)
			{
				iFrameOrder++;
			}

			/*printf(" iAddr = %d, SigIdx = %d, MPPT_CURRENT_FOR_DISP = %.02f ok!!!\n", 
			iAddr,
			MPPT_DC_VOLTAGE,
			g_CanData.aRoughDataMppt[iAddr][MPPT_CURRENT_FOR_DISP].fValue);*/

			break;

		case MT_VAL_TYPE_R_ALARM_BITS:
			UnpackMpptAlarm(iAddr, pValueBuf);
			if(MT_CMD00_ORDER_ALARM_BITS == iFrameOrder)
			{
				iFrameOrder++;
			}
			/*	printf(" iFrameOrder = %d, MT_VAL_TYPE_R_ALARM_BITS ok!!!\n", iFrameOrder);*/


			break;

		case MT_VAL_TYPE_R_SN_LOW:		
			g_CanData.aRoughDataMppt[iAddr][MPPT_SERIEL_NO_LOW].uiValue 
				= CAN_StringToUint(pValueBuf);

			if(MT_CMD00_ORDER_SN_LOW == iFrameOrder)
			{
				iFrameOrder++;
			}
			/*printf(" iAddr = %d, SigIdx = %ld, MPPT_SERIEL_NO_LOW = %d--%d--%d--%d--%d ok!!!\n", 
			iAddr,
			MPPT_SERIEL_NO_LOW,
			g_CanData.aRoughDataMppt[iAddr][MPPT_SERIEL_NO_LOW].uiValue,
			(UINT)(pValueBuf[0]),
			(UINT)(pValueBuf[1]),
			(UINT)(pValueBuf[2]),
			(UINT)(pValueBuf[3]));*/

			break;


		case MT_VAL_TYPE_R_RUN_TIME:
			fRunTime = CAN_StringToFloat(pValueBuf);
			if(g_CanData.aRoughDataMppt[iAddr][MPPT_TOTAL_RUN_TIME].iValue
				== CAN_SAMP_INVALID_VALUE)
			{
				g_CanData.aRoughDataMppt[iAddr][MPPT_TOTAL_RUN_TIME].uiValue
					= (uint)fRunTime;
			}
			if(MT_CMD00_ORDER_RUN_TIME == iFrameOrder)
			{
				iFrameOrder++;
			}
			/*TRACE(" iAddr = %d, SigIdx = %d, MPPT_TOTAL_RUN_TIME = %d ok!!!\n", 
			iAddr,
			MT_VAL_TYPE_R_RUN_TIME,
			g_CanData.aRoughDataMppt[iAddr][MPPT_TOTAL_RUN_TIME].uiValue);*/

			break;
		default:
			break;
		}
	}

	if(MT_CMD00_ORDER_FRAME_NUM == iFrameOrder)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}



enum	CMD20_FRAME_ORDER
{
	CMD20_ORDER_FEATURE = 0,
	CMD20_ORDER_SN_LOW,
	CMD20_ORDER_SN_HIGH,
	CMD20_ORDER_VER_NO,
	CMD20_ORDER_BARCODE1,
	CMD20_ORDER_BARCODE2,
	CMD20_ORDER_BARCODE3,
	CMD20_ORDER_BARCODE4,

	CMD20_ORDER_FRAME_NUM,
};
/*==========================================================================*
* FUNCTION : MpptUnpackCmd20
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
*            int    iReadLen  : 
*            BYTE*  pbyRcvBuf : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:29
*==========================================================================*/
static BOOL MpptUnpackCmd20(int iAddr,
			    int iReadLen,
			    BYTE* pbyRcvBuf)
{
	int			i;
	UINT		uiSerialNoHi, uiSerialNoLow, uiVerNo, uiBarcode;
	int			iFrameOrder = MT_VAL_TYPE_R_FEATURE;

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;

		/*TRACE("**********i = %d  VAL_TYPE_H = %02X  VAL_TYPE_L = %02X  ",
		i,
		pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H],
		pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L]);*/

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
			!= PROTNO_MPPT_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
			!= (UINT)iAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H]
		!= 0))
		{
			continue;
		}

		switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
		{
		case MT_VAL_TYPE_R_FEATURE:
			/*if(iAddr == 0)
			{
			MtAnalyseFeature(pValueBuf);
			}*/	

			g_CanData.aRoughDataMppt[iAddr][MPPT_FEATURE].uiValue 
				= CAN_StringToUint(pValueBuf);
			/*printf(" iFrameOrder = %d, MT_VAL_TYPE_R_FEATURE ok!!!\n", iFrameOrder);*/

			if(CMD20_ORDER_FEATURE == iFrameOrder)
			{
				iFrameOrder++;
			}

			break;

		case MT_VAL_TYPE_R_SN_HIGH:
			uiSerialNoHi = CAN_StringToUint(pValueBuf);
			uiSerialNoLow = g_CanData.aRoughDataMppt[iAddr][MPPT_SERIEL_NO_LOW].uiValue;
			g_CanData.aRoughDataMppt[iAddr][MPPT_SERIEL_NO_HIGH].uiValue 
				= uiSerialNoHi;

			g_CanData.aRoughDataMppt[iAddr][MPPT_HIGH_SN_REPORT].uiValue
				= ((uiSerialNoHi & 0x0000000f) << 2) + (uiSerialNoLow >> 30);

			//printf("Mppt%d iFrameOrder = %d, MT_VAL_TYPE_R_SN_HIGH ok!!!\n",iAddr, g_CanData.aRoughDataMppt[iAddr][MPPT_SERIEL_NO_HIGH].uiValue);

			if(CMD20_ORDER_SN_HIGH == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case MT_VAL_TYPE_R_VER_NO:
			uiVerNo =  CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataMppt[iAddr][MPPT_VERSION_NO].uiValue 
				= uiVerNo;
			//printf(" iFrameOrder = %d, MT_VAL_TYPE_R_VER_NO ok!!!\n", g_CanData.aRoughDataMppt[iAddr][MPPT_VERSION_NO].uiValue);


			if(CMD20_ORDER_VER_NO == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case MT_VAL_TYPE_R_BARCODE1:		
			uiBarcode =  CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataMppt[iAddr][MPPT_BARCODE1].uiValue 
				= uiBarcode;

			//printf(" iFrameOrder = %d, MT_VAL_TYPE_R_BARCODE1 ok!!!\n", g_CanData.aRoughDataMppt[iAddr][MPPT_BARCODE1].uiValue );

			if(CMD20_ORDER_BARCODE1 == iFrameOrder)
			{
				iFrameOrder++;
			}

			break;


		case MT_VAL_TYPE_R_BARCODE2:		
			uiBarcode = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataMppt[iAddr][MPPT_BARCODE2].uiValue 
				= uiBarcode;

			//printf(" iFrameOrder = %d, MT_VAL_TYPE_R_BARCODE2 ok!!!\n", g_CanData.aRoughDataMppt[iAddr][MPPT_BARCODE2].uiValue);

			if(CMD20_ORDER_BARCODE2 == iFrameOrder)
			{
				iFrameOrder++;
			}

			break;

		case MT_VAL_TYPE_R_BARCODE3:		
			uiBarcode = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataMppt[iAddr][MPPT_BARCODE3].uiValue
				= uiBarcode;
			//printf(" iFrameOrder = %d, MT_VAL_TYPE_R_BARCODE3 ok!!!\n", g_CanData.aRoughDataMppt[iAddr][MPPT_BARCODE3].uiValue);

			if(CMD20_ORDER_BARCODE3 == iFrameOrder)
			{
				iFrameOrder++;
			}

			break;

		case MT_VAL_TYPE_R_BARCODE4:		
			uiBarcode = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataMppt[iAddr][MPPT_BARCODE4].uiValue 
				= uiBarcode;
			//printf(" iFrameOrder = %d, MT_VAL_TYPE_R_BARCODE4 ok!!!\n", g_CanData.aRoughDataMppt[iAddr][MPPT_BARCODE4].uiValue );

			if(CMD20_ORDER_BARCODE4 == iFrameOrder)
			{
				iFrameOrder++;
			}

			break;

		default:
			break;
		}
	}


	if(CMD20_ORDER_FRAME_NUM == iFrameOrder)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}




#define MT_DC_ON_OFF_MASK				(0x80)
#define	MT_OVER_VOLT_RESET_MASK			(0x40)
#define	MT_WALK_IN_MASK					(0x20)
#define	MT_FAN_FULL_SPEED_MASK			(0x10)
#define	MT_LED_BLINK_MASK				(0x08)
#define	MT_OVERVOLT_RELAY_MASK			(0x04)
#define	MT_RESET_CAN_POMT_MASK			(0x02)
#define	MT_AC_OVERVOLT_PROMPPT_MASK		(0x01)
/*==========================================================================*
* FUNCTION : GetMtSetByteCmd00
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static BYTE : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:28
*==========================================================================*/
static BYTE GetMtSetByteCmd00(int iAddr)
{
	BYTE			byOutput = 0;

	MPPT_SET_SIG*	pSetSig 
		= &(g_CanData.CanCommInfo.MpptCommInfo.aMpptSetSig[iAddr]);

	if(pSetSig->dwDcOnOff != MT_DC_ON)
	{		
		byOutput |= MT_DC_ON_OFF_MASK;	
	}
	else
	{
		byOutput &= ~MT_DC_ON_OFF_MASK;	
	}


	if(pSetSig->dwResetOnDcOV != MT_RESET_NORMAL)  //Rectifier Over Voltage Reset
	{
		//2010/12/27  
		//������ĵ�Ŀ����������ĳ��ģ�����RESET����ִֻ��һ�飡��
		pSetSig->dwResetOnDcOV = MT_RESET_NORMAL;
		byOutput |= MT_OVER_VOLT_RESET_MASK;

	}	
	else
	{
		byOutput &= ~MT_OVER_VOLT_RESET_MASK;
	}


	if(pSetSig->dwWalkinEnabled != MT_WALKIN_NORMAL)    //Mpptifier Walkin Enable
	{
		byOutput |= MT_WALK_IN_MASK;
	}
	else
	{
		byOutput &= ~MT_WALK_IN_MASK;
	}

	if(pSetSig->dwFanFullSpeed != MT_FAN_AUTO) //Fan is full speed or not
	{
		byOutput |= MT_FAN_FULL_SPEED_MASK;
	}
	else
	{
		byOutput &= ~MT_FAN_FULL_SPEED_MASK;
	}

	if(pSetSig->dwLedBlink != MT_LED_NORMAL)
	{
		byOutput |= MT_LED_BLINK_MASK;
	}
	else
	{
		byOutput &= ~MT_LED_BLINK_MASK;
	}

	byOutput &= ~MT_OVERVOLT_RELAY_MASK;
	byOutput &= ~MT_RESET_CAN_POMT_MASK;
	byOutput &= ~MT_AC_OVERVOLT_PROMPPT_MASK;


	return byOutput;	
}



/*==========================================================================*
* FUNCTION : ClrRtIntrruptTimes
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:29
*==========================================================================*/
static void ClrMtIntrruptTimes(int iAddr)
{
	g_CanData.aRoughDataMppt[iAddr][MPPT_INTERRUPT_TIMES].iValue = 0;
	g_CanData.aRoughDataMppt[iAddr][MPPT_INTERRUPT_ST].iValue 
		= MT_COMM_NORMAL_ST;
	Timer_Reset(RunThread_GetId(NULL), ID_TIMER_MPPT_INTERRUPT + iAddr);
	return;
}

static int Mppt_GetSignalValue(int iEquipID,
						int iSignalType,
						int iSignalID,
						SIG_BASIC_VALUE **pSignalInfo,
						const char *pszTaskName)
{
	int iVarSubID, iError, iBufLen;
	iVarSubID = DXI_MERGE_SIG_ID(iSignalType, iSignalID);
	iError = DxiGetData(VAR_A_SIGNAL_VALUE,
			iEquipID,
			iVarSubID,
			&iBufLen,
			(void *)pSignalInfo,
			0);
	if(iError != ERR_DXI_OK)
	{
		AppLogOut(pszTaskName, APP_LOG_ERROR, 
			"Get signal[%d %d %d] point error!\n",iEquipID,iSignalType,iSignalID);
	}
	return iError;
}

static int Mppt_GetMpptFailedTimer(int iMpptFailedTimer)
{
#define MPPT_FAILED_TIMER_SIGID		52

	int iError = 0;
	int iMpptFailedTimerTemp, iAddrTemp;
	static BOOL bInited = FALSE;

	static SIG_BASIC_VALUE *s_pMpptFailedTimer = NULL;
	if(s_pMpptFailedTimer == NULL && bInited == FALSE)
	{
		bInited = TRUE;//in case the log write too much if the SIGNAL lost.
		iError = Mppt_GetSignalValue(MT_GROUP_EQUIPID,
						SIG_TYPE_SETTING,
						MPPT_FAILED_TIMER_SIGID,
						&s_pMpptFailedTimer,
						"IncMtIntrruptTimes");
		if(iError != ERR_DXI_OK)
		{
			return iMpptFailedTimer;	
		}
	}

	if(s_pMpptFailedTimer != NULL)
	{
		iMpptFailedTimer = s_pMpptFailedTimer->varValue.ulValue * MT_1_DAY;
		//iMpptFailedTimer = iMpptFailedTimerTemp;
		//if(iMpptFailedTimer != iMpptFailedTimerTemp)
		//{
		//	//for(iAddrTemp = 0; iAddrTemp < g_CanData.CanCommInfo.MpptCommInfo.iCommMpptNum; iAddrTemp++)
		//	for(iAddrTemp = 0; iAddrTemp < MAX_NUM_MPPT; iAddrTemp++)
		//	{
		//		Timer_Reset(hself, ID_TIMER_MPPT_INTERRUPT + iAddrTemp);
		//	}
		//	iMpptFailedTimer = iMpptFailedTimerTemp;
		//}
	}
	return iMpptFailedTimer;
}
/*==========================================================================*
* FUNCTION : IncRtIntrruptTimes
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:29
*==========================================================================*/
static void IncMtIntrruptTimes(int iAddr)
{
	HANDLE hself = RunThread_GetId(NULL);


	//������һ��ͨѶ�ɹ����ϱ���ѹ>60V or DC is ON,  ��ֱ���ж�ͨѶʧ��
	//����������ͨѶʧ�ܣ���ʱ

	//if(g_CanData.aRoughDataMppt[iAddr][MPPT_DC_ON_OFF_STATUS].iValue == MT_DC_ON )
	//	//120V converter won't < 60V before communicate failed.
	//	//|| g_CanData.aRoughDataMppt[iAddr][MPPT_AC_VOLTAGE].fValue > GC_MIN_VOLTAGE )
	//{
	//	if(g_CanData.aRoughDataMppt[iAddr][MPPT_INTERRUPT_TIMES].iValue 
	//		> MT_MAX_INTERRUPT_TIMES + 1)
	//	{	
	//		if(g_CanData.aRoughDataGroup[GROUP_MT_ALL_NO_RESPONSE].iValue)
	//		{
	//			g_CanData.aRoughDataMppt[iAddr][MPPT_INTERRUPT_ST].iValue 
	//				= MT_COMM_ALL_INTERRUPT_ST;
	//		}
	//		else
	//		{
	//			g_CanData.aRoughDataMppt[iAddr][MPPT_INTERRUPT_ST].iValue 
	//				= MT_COMM_INTERRUPT_ST;
	//		}
	//	}
	//	g_CanData.aRoughDataMppt[iAddr][MPPT_INTERRUPT_TIMES].iValue++;
	//	return;
	//}
	//else
	{
		if(g_CanData.aRoughDataMppt[iAddr][MPPT_INTERRUPT_TIMES].iValue 
			< MT_MAX_INTERRUPT_TIMES + 2)
		{
			g_CanData.aRoughDataMppt[iAddr][MPPT_INTERRUPT_TIMES].iValue++;
		}
	}
	
	
	
	if(g_CanData.aRoughDataMppt[iAddr][MPPT_INTERRUPT_TIMES].iValue
		>= (MT_MAX_INTERRUPT_TIMES + 1))
	{
		/*if(g_CanData.aRoughDataGroup[GROUP_MT_ALL_NO_RESPONSE].iValue)
		{
			g_CanData.aRoughDataMppt[iAddr][MPPT_INTERRUPT_ST].iValue 
				= MT_COMM_ALL_INTERRUPT_ST;
		}
		else
		{
			g_CanData.aRoughDataMppt[iAddr][MPPT_INTERRUPT_ST].iValue 
				= MT_COMM_INTERRUPT_ST;
		}*/
		if(g_CanData.aRoughDataMppt[iAddr][MPPT_INTERRUPT_TIMES].iValue == MT_MAX_INTERRUPT_TIMES + 2)
		{
			if(gs_iMpptFailedTimer == 0)
			{
				MtSetComm(NULL,NULL,iAddr);
				Timer_Reset(hself, ID_TIMER_MPPT_INTERRUPT + iAddr);
			}
			else
			{
				Timer_Set(hself, ID_TIMER_MPPT_INTERRUPT + iAddr, gs_iMpptFailedTimer/GetTestReduceTimes(), (ON_TIMER_PROC)MtSetComm, (DWORD)iAddr);
			}
			g_CanData.aRoughDataMppt[iAddr][MPPT_INTERRUPT_TIMES].iValue++;
		}
	}
	else
	{
		g_CanData.aRoughDataMppt[iAddr][MPPT_INTERRUPT_ST].iValue 
			= MT_COMM_NORMAL_ST;
		Timer_Reset(hself, ID_TIMER_MPPT_INTERRUPT + iAddr);
	}
	return;
}



/*==========================================================================*
* FUNCTION : RtSampleCmd00
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static SIG_ENUM : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:30
*==========================================================================*/
static SIG_ENUM MpptSampleCmd00(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;

	FLOAT_STRING	unVal;

	unVal.ulValue = 0;
	PackAndSendMpptCmd(MSG_TYPE_RQST_DATA00,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		(UINT)GetMtSetByteCmd00(iAddr),
		MT_VAL_TYPE_NO_TRIM_VOLT,
		unVal.abyValue);
	//In most situations, rectifer responds in 15s
	Sleep(MT_CMD00_RCV_FRAMES_NUM * CAN_SAMPLE_1ST_WAIT_FACTOR);
	if(CAN_MPPT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_CanData.CanCommInfo.abyRcvBuf, 
		&iReadLen1st))
	{
		return CAN_MPPT_REALLOCATE;
	}
	else
	{
		if(iReadLen1st < (MT_CMD00_RCV_FRAMES_NUM * CAN_FRAME_LEN))
		{
			//After 15s+85s, if no respond, interrup times increase.
			Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
			if(CAN_MPPT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
				g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
				&iReadLen2nd))

			{
				return CAN_MPPT_REALLOCATE;
			}
		}
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= MT_CMD00_RCV_FRAMES_NUM)
	{
		//TRACE("****iReadLen1st = %d, iReadLen2nd= %d\n", iReadLen1st, iReadLen2nd);
		if(MtUnpackCmd00(iAddr,
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf))
		{
			ClrMtIntrruptTimes(iAddr);
		}
		else
		{
			IncMtIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;		
		}
	}
	else
	{
		IncMtIntrruptTimes(iAddr);
		return CAN_SAMPLE_FAIL;		
	}

	return CAN_SAMPLE_OK;
}




/*==========================================================================*
* FUNCTION : MpptSampleCmd20
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static SIG_ENUM : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:30
*==========================================================================*/
static SIG_ENUM MpptSampleCmd20(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;
	FLOAT_STRING	unVal;

	unVal.ulValue = 0;
	PackAndSendMpptCmd(MSG_TYPE_RQST_DATA20,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		0,
		MT_VAL_TYPE_NO_TRIM_VOLT,
		unVal.abyValue);
	Sleep(MT_CMD20_RCV_FRAMES_NUM * CAN_SAMPLE_1ST_WAIT_FACTOR);
	if(CAN_MPPT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_CanData.CanCommInfo.abyRcvBuf, 
		&iReadLen1st))
	{
		return CAN_MPPT_REALLOCATE;
	}
	else
	{
		if(iReadLen1st < (MT_CMD20_RCV_FRAMES_NUM * CAN_FRAME_LEN))
		{
			Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
			if(CAN_MPPT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
				g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
				&iReadLen2nd))

			{
				return CAN_MPPT_REALLOCATE;
			}
		}
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= MT_CMD20_RCV_FRAMES_NUM)
	{
		MpptUnpackCmd20(iAddr,
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf);
	}
	else
	{
		return CAN_SAMPLE_FAIL;	
	}

	//TRACE("iAddr = %d, OK!!!\n", iAddr);

	return CAN_SAMPLE_OK;
}



#define MT_MAX_POS_NO		999
/*==========================================================================*
* FUNCTION : GetOneRestMpptPosNo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:28
*==========================================================================*/
static int GetOneRestMpptPosNo(void)
{
	int		i , j;
	int		iMpptNum = g_CanData.CanCommInfo.MpptCommInfo.iCommMpptNum;


	for(i = 1; i < MT_MAX_POS_NO; i++)
	{
		BOOL	bUsed = FALSE;
		for(j = 0; j < iMpptNum; j++)
		{
			if(i == g_CanData.aRoughDataMppt[j][MPPT_POSITION_NO].iValue)
			{
				bUsed = TRUE;
				break;
			}
		}

		if(!bUsed)
		{
			return i;
		}
	}
	return i;
}






/*==========================================================================*
* FUNCTION : MT_RefreshFlashInfo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:28
*==========================================================================*/
//static void MT_RefreshFlashInfo(void)
//{
//	int			i, j;
//	int			iMpptNum = g_CanData.CanCommInfo.MpptCommInfo.iCommMpptNum;
//	DWORD		dwSnHigh;
//	DWORD		dwSnLow;
//	int			iPosNo;
//	int			iAcPhase;
//	//HANDLE		hCanFlashData;
//
//	MPPT_POS_ADDR	MpptPosAddr[MAX_NUM_MPPT];
//
//	//Assign position No. and phase No. to rectifers by flash data
//	for(i = 0; i < iMpptNum; i++)
//	{
//		dwSnHigh = g_CanData.aRoughDataMppt[i][MPPT_HIGH_SN_ROUGH].dwValue;
//		dwSnLow = g_CanData.aRoughDataMppt[i][MPPT_SERIEL_NO_LOW].dwValue;
//
//		for(j = 0; j < MAX_NUM_MPPT; j ++)
//		{
//			if((dwSnHigh == g_CanData.CanFlashData.aMpptInfo[j].dwHighSn)
//				&& (dwSnLow == g_CanData.CanFlashData.aMpptInfo[j].dwSerialNo))
//			{
//
//				g_CanData.aRoughDataMppt[i][MPPT_POSITION_NO].iValue
//					= g_CanData.CanFlashData.aMpptInfo[j].iPositionNo;
//				break;
//			}
//		}
//	}
//
//	//Assign position No. and phase No. to rest rectifers
//	for(i = 0; i < iMpptNum; i++)
//	{
//		iPosNo = g_CanData.aRoughDataMppt[i][MPPT_POSITION_NO].iValue;
//
//		if(iPosNo == CAN_SAMP_INVALID_VALUE)
//		{
//			g_CanData.aRoughDataMppt[i][MPPT_POSITION_NO].iValue
//				= GetOneRestMpptPosNo();
//
//		}
//
//		MpptPosAddr[i].iAddr = i;
//		MpptPosAddr[i].iPositionNo = g_CanData.aRoughDataMppt[i][MPPT_POSITION_NO].iValue;
//		MpptPosAddr[i].iPhaseNo = 0;
//
//		g_CanData.aRoughDataMppt[i][MPPT_SEQ_NO].iValue
//			= CAN_SAMP_INVALID_VALUE;		//be evaluated later
//
//		g_CanData.CanFlashData.aMpptInfo[i].bExistence = TRUE;
//		g_CanData.CanFlashData.aMpptInfo[i].dwAddress = i;
//		g_CanData.CanFlashData.aMpptInfo[i].dwHighSn 
//			= g_CanData.aRoughDataMppt[i][MPPT_HIGH_SN_ROUGH].dwValue;
//		g_CanData.CanFlashData.aMpptInfo[i].dwSerialNo 
//			= g_CanData.aRoughDataMppt[i][MPPT_SERIEL_NO_LOW].dwValue;
//		g_CanData.CanFlashData.aMpptInfo[i].iPositionNo
//			= g_CanData.aRoughDataMppt[i][MPPT_POSITION_NO].iValue;
//		g_CanData.CanFlashData.aMpptInfo[i].iSeqNo 
//			= CAN_SAMP_INVALID_VALUE; //be evaluated later
//	}
//
//	//effervescing sort
//	if(iMpptNum)
//	{
//		for (i = 0; i < (iMpptNum - 1); i++)
//		{
//			for (j = 0; j < (iMpptNum - 1 - i); j++)
//			{
//				if(MpptPosAddr[j].iPositionNo > MpptPosAddr[j + 1].iPositionNo)
//				{
//					MPPT_POS_ADDR		PosAddr;
//
//					PosAddr.iAddr = MpptPosAddr[j + 1].iAddr;
//					PosAddr.iPositionNo = MpptPosAddr[j + 1].iPositionNo;
//
//
//					MpptPosAddr[j + 1].iPositionNo = MpptPosAddr[j].iPositionNo;
//					MpptPosAddr[j + 1].iAddr = MpptPosAddr[j].iAddr;
//
//					MpptPosAddr[j].iPositionNo = PosAddr.iPositionNo;
//					MpptPosAddr[j].iAddr = PosAddr.iAddr;
//				}
//			}
//		}
//	}
//
//	//Assign sequence No. to every rectifier
//	for(i = 0; i < iMpptNum; i++)
//	{
//		int iAddr = MpptPosAddr[i].iAddr;
//
//		g_CanData.aRoughDataMppt[iAddr][MPPT_SEQ_NO].iValue = i;
//		g_CanData.CanFlashData.aMpptInfo[iAddr].iSeqNo = i;
//		SetDwordSigValue(MT_START_EQUIP_ID + i,
//			SIG_TYPE_SETTING,
//			MT_POS_SET_SIG_ID,
//			g_CanData.aRoughDataMppt[iAddr][MPPT_POSITION_NO].iValue,
//			"CAN_SAMP");
//
//	}
//
//
//	CAN_WriteFlashData(&(g_CanData.CanFlashData));
//
//	return;
//}
//
//

/*==========================================================================*
* FUNCTION : MpptUnifyParam
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2009-02-02 15:08
*==========================================================================*/
static void MpptUnifyParam(void)
{
	int				i;
	DWORD			dwValue;
	SIG_ENUM		enumValue;
	BYTE			abyVal[4];


	//Interval of restart on over voltage
	dwValue = GetDwordSigValue(MT_GROUP_EQUIPID, 
		SIG_TYPE_SETTING,
		MT_OV_RESTART_INTERVAL_SIGID,
		"CAN_SAMP");

	CAN_FloatToString((float)dwValue, abyVal);

	PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
		CAN_ADDR_FOR_BROADCAST,
		CAN_CMD_TYPE_BROADCAST,
		0,
		MT_VAL_TYPE_W_RESTAMT_TIME,
		abyVal);
	Sleep(100);


	//Restart on over voltage Enabled	
	enumValue = GetEnumSigValue(MT_GROUP_EQUIPID, 
		SIG_TYPE_SETTING,
		MT_OV_RESTART_ENB_SIGID,
		"CAN_SAMP");

	abyVal[0] = 0;
	abyVal[1] = (BYTE)enumValue;
	abyVal[2] = 0;
	abyVal[3] = 0;

	//2010/12/27
	//���Ķ�Э�飬��ѹ����������ʹ��	��VALUETYPEL 0X39 txy ����Ҫ�úò���һ��
	PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
		CAN_ADDR_FOR_BROADCAST,
		CAN_CMD_TYPE_BROADCAST,
		0,
		0x39,
		abyVal);
	Sleep(100);

	//Interval of sequential start
	dwValue = GetDwordSigValue(MT_GROUP_EQUIPID, 
		SIG_TYPE_SETTING,
		MT_SEQ_START_INTERVAL_SIGID,
		"CAN_SAMP");

	CAN_FloatToString((float)dwValue, abyVal);

	PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
		CAN_ADDR_FOR_BROADCAST,
		CAN_CMD_TYPE_BROADCAST,
		0,
		MT_VAL_TYPE_W_SEQ_STAMT_TIME,
		abyVal);

	Sleep(100);

	//Walk-in time	
	dwValue = GetDwordSigValue(MT_GROUP_EQUIPID, 
		SIG_TYPE_SETTING,
		MT_WALKIN_TIME_SIGID,
		"CAN_SAMP");

	CAN_FloatToString((float)dwValue * 100/*The unit is 10ms*/, abyVal);

	PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
		CAN_ADDR_FOR_BROADCAST,
		CAN_CMD_TYPE_BROADCAST,
		0,
		MT_VAL_TYPE_W_WALKIN_TIME,
		abyVal);

	Sleep(100);

	//Walk-in Enabled	
	enumValue = GetEnumSigValue(MT_GROUP_EQUIPID, 
		SIG_TYPE_SETTING,
		MT_WALKIN_ENB_SIGID,
		"CAN_SAMP");

	abyVal[0] = 0;
	abyVal[1] = (BYTE)enumValue;
	abyVal[2] = 0;
	abyVal[3] = 0;

	PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
		CAN_ADDR_FOR_BROADCAST,
		CAN_CMD_TYPE_BROADCAST,
		0,
		MT_VAL_TYPE_W_WALKIN_ENB,
		abyVal);

	for(i = 0; i < MAX_NUM_MPPT; i++)
	{
		g_CanData.CanCommInfo.MpptCommInfo.aMpptSetSig[i].dwWalkinEnabled 
			= (0 == enumValue) ? MT_WALKIN_NORMAL : MT_WALKIN_ENB;
	}
	Sleep(100);

	//PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
	//					CAN_ADDR_FOR_BROADCAST,
	//					CAN_CMD_TYPE_BROADCAST,
	//					0,
	//					MT_VAL_TYPE_W_RESTAMT_ON_OV,
	//					abyVal);	
	//

	/*	
	for(i = 0; i < MAX_NUM_MPPT; i++)
	{
	g_CanData.CanCommInfo.MpptCommInfo.aMpptSetSig[i].dwResetOnDcOV
	= (0 == enumValue) ? MT_OV_RESET_DISB : MT_OV_RESET_ENB;
	}	
	*/	

	Sleep(100);

	//Restart on AC over voltage Enabled	
	//There is no AC Over voltage Enabled for MPPT, so just delete it. by Yang Guoxin,2014/7/14
	//enumValue = GetEnumSigValue(MT_GROUP_EQUIPID, 
	//	SIG_TYPE_SETTING,
	//	MT_AC_OV_RESTART_ENB_SIGID,
	//	"CAN_SAMP");


	//if(enumValue == 0)
	//{
	//	for(i = 0; i < MAX_NUM_MPPT; i++)
	//	{
	//		g_CanData.CanCommInfo.MpptCommInfo.aMpptSetSig[i].dwResetOnAcOV 
	//			=  MT_AC_OV_PROTECT_DISB;
	//	}
	//	//printf("\n	This is if(enumValue == 0) for all DISB		\n");
	//}
	//else
	//{
	//	BOOL	bSet = FALSE;
	//	for(i = 0; i < MAX_NUM_MPPT; i++)
	//	{
	//		//Only one rectifier needs to be controlled to enabled
	//		if((!bSet) 
	//			&& (g_CanData.aRoughDataMppt[i][MPPT_INTERRUPT_TIMES].iValue
	//			< MT_MAX_INTERRUPT_TIMES))
	//		{
	//			g_CanData.CanCommInfo.MpptCommInfo.aMpptSetSig[i].dwResetOnAcOV 
	//				=  MT_AC_OV_PROTECT_ENB;
	//			bSet = TRUE;
	//		}
	//		else
	//		{
	//			g_CanData.CanCommInfo.MpptCommInfo.aMpptSetSig[i].dwResetOnAcOV 
	//				=  MT_AC_OV_PROTECT_DISB;

	//		}
	//	}
	//	//printf("\n	This is ELSE 	if(enumValue == 0)	\n");
	//}		

	return;
}


/*==========================================================================*
* FUNCTION : RtDefaultVoltParam
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2009-01-08 14:41
*==========================================================================*/
static void MpptDefaultVoltParam(BOOL bDirectSendCmd)
{
	float			fFloatVolt;
	float			fOverVolt;
	BYTE			abyVal[4];
	static	float	s_fLastDefaultVolt = 0.0;
	static	float	s_fLastOverVolt = 0.0;


	fFloatVolt = GetFloatSigValue(BT_GROUP_EQUIPID, 
		SIG_TYPE_SETTING,
		MT_FLOAT_VOLT_SIGID,
		"CAN_SAMP");

	fOverVolt = GetFloatSigValue(MT_GROUP_EQUIPID, 
		SIG_TYPE_SETTING,
		MT_OVER_VOLTAGE_SIGID,
		"CAN_SAMP");



	if(bDirectSendCmd 
		|| (FLOAT_NOT_EQUAL(s_fLastDefaultVolt, fFloatVolt)))
	{
		s_fLastDefaultVolt = fFloatVolt;

		CAN_FloatToString(fFloatVolt, abyVal);
		PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
			CAN_ADDR_FOR_BROADCAST,
			CAN_CMD_TYPE_BROADCAST,
			0,
			MT_VAL_TYPE_W_DEFAULT_VOLT,
			abyVal);
		Sleep(100);
	}

	if(bDirectSendCmd
		|| (FLOAT_NOT_EQUAL(s_fLastOverVolt, fOverVolt)))
	{
		s_fLastOverVolt = fOverVolt;

		CAN_FloatToString(fOverVolt, abyVal);
		PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
			CAN_ADDR_FOR_BROADCAST,
			CAN_CMD_TYPE_BROADCAST,
			0,
			MT_VAL_TYPE_W_HIGH_VOLT,
			abyVal);
		Sleep(100);
	}

	return;
}




/*==========================================================================*
* FUNCTION : MT_TriggerAllocation
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:31
*==========================================================================*/
void MT_TriggerAllocation(void)
{
	int		i;
	//Mpptifier re-allocation cmd require below data, 
	//please refer to the protocol
	BYTE	abyDataOfReallcate[CAN_FRAME_DATA_LEN] 
	= {0x04, 0xf0, 0x01, 0x5a, 0x00, 0x00, 0x00, 0x00};

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_MPPT_MPPT,
		CAN_ADDR_FOR_BROADCAST, 
		CAN_SELF_ADDR,
		CAN_ERR_CODE,
		CAN_FRAME_DATA_LEN,
		CAN_CMD_TYPE_BROADCAST,
		g_CanData.CanCommInfo.abySendBuf);

	//Stuff the rest bits
	for(i = 0; i < CAN_FRAME_DATA_LEN; i++)
	{
		g_CanData.CanCommInfo.abySendBuf[CAN_FRAME_OFFSET_MSG + i] 
		= abyDataOfReallcate[i];
	}

	write(g_CanData.CanCommInfo.iCanHandle, 
		(void *)g_CanData.CanCommInfo.abySendBuf,
		CAN_FRAME_LEN);

	//TRACE("************CAN DEBUG: Writed a reallocting command!\n");

	//����HVDC��ʱ���ֵ�
	g_CanData.CanCommInfo.MpptCommInfo.bNeedReconfig = TRUE;

	//Sleep(12000);			//Waiting 12s
	//CAN_ClearReadBuf();

	return;

}



/*==========================================================================*
* FUNCTION : MT_InitPosAndPhase
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: OUT CAN_FLASH_DATA*  pFlashData : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:31
*==========================================================================*/
void MT_InitPosAndPhase(void)
{
	int				i;

	for(i = 0; i < MAX_NUM_MPPT; i++)
	{
		g_CanData.CanFlashData.aMpptInfo[i].iAcPhase = AC_PHASE_INVAILID;
		g_CanData.CanFlashData.aMpptInfo[i].iPositionNo
			= MPPT_POSITION_INVAILID;
	}
	return;
}


/*==========================================================================*
* FUNCTION : MT_InitRoughValue
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:31
*==========================================================================*/
void MT_InitRoughValue(void)
{
	int			i, j;
	SIG_ENUM		enumValue = GetEnumSigValue(MT_GROUP_EQUIPID, 
		SIG_TYPE_SETTING,
		MT_WALKIN_ENB_SIGID,
		"CAN_SAMP");

	g_CanData.CanCommInfo.MpptCommInfo.bNeedRefreshRuntime = TRUE;

	g_CanData.CanCommInfo.MpptCommInfo.iCommMpptNum = 0;
	//if(g_CanData.aRoughDataGroup[GROUP_MT_ACTUAL_NUM].iValue 
	//	== CAN_SAMP_INVALID_VALUE)
	//{
	g_CanData.aRoughDataGroup[GROUP_MT_ACTUAL_NUM].iValue = 0;	
	//}
	g_CanData.aRoughDataGroup[GROUP_MT_EXIST_STAT].iValue = MT_EQUIP_NOT_EXISTENT;//inexistence
	//Ranged by rectifier address order
	for(i = 0; i < MAX_NUM_MPPT; i++)
	{
		//Assign invalid value to sampling signals first
		for(j = 0; j < MPPT_MAX_SIGNALS_NUM; j++)
		{
			g_CanData.aRoughDataMppt[i][j].iValue = CAN_SAMP_INVALID_VALUE;
		}
		g_CanData.aRoughDataMppt[i][MPPT_EXIST_ST].iValue = MT_EQUIP_NOT_EXISTENT;
		g_CanData.CanFlashData.aMpptInfo[i].bExistence = FALSE;

		MPPT_SET_SIG* pMpptSetSig 
			= g_CanData.CanCommInfo.MpptCommInfo.aMpptSetSig + i;

		//2011/2/21�գ����ģ���������ú�ģ��HVSD����δ���ֵĸ��ġ�
		//pMpptSetSig->dwResetOnDcOV = MT_OV_RESET_ENB;
		pMpptSetSig->dwWalkinEnabled = (0 == enumValue) ? MT_WALKIN_NORMAL : MT_WALKIN_ENB;//MT_WALKIN_NORMAL;
		pMpptSetSig->dwFanFullSpeed = MT_FAN_AUTO;
		pMpptSetSig->dwLedBlink = MT_LED_NORMAL;
		pMpptSetSig->dwDcOnOff = MT_DC_ON;
		pMpptSetSig->dwCanInit = MT_CAN_NORMAL;
		pMpptSetSig->dwResetOnAcOV = MT_AC_OV_PROTECT_ENB;
		
		g_CanData.bCalcAlarm[i][0] = FALSE;
		g_CanData.bCalcAlarm[i][1] = FALSE;

		

	}

	return;
}


/*==========================================================================*
* FUNCTION : GetMpptAddrByChnNo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   iChannelNo : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:31
*==========================================================================*/
static int GetMpptAddrByChnNo(int iChannelNo)
{
	int i;

	int iSeqNo = (iChannelNo - CNMG_MAX_MT_BROADCAST_CMD_CHN - 1) 
		/ MAX_CHN_NUM_PER_MPPT;
	//ASSERT(iSeqNo >= 0 && iSeqNo < MAX_NUM_MPPT);

	//printf("iSeqNo = %d\n", iSeqNo);
	for(i = 0; i < g_CanData.CanCommInfo.MpptCommInfo.iCommMpptNum; i++)
	{
		//aRoughDataGroup is ranged by address order, so "i" is address 
		//printf("g_CanData.aRoughDataMppt[i][MPPT_SEQ_NO].iValue = %d\n", g_CanData.aRoughDataMppt[i][MPPT_SEQ_NO].iValue);
		if(iSeqNo == g_CanData.aRoughDataMppt[i][MPPT_SEQ_NO].iValue)
		{
			return i;
		}
	}

	return 0;
}



/*==========================================================================*
* FUNCTION : MT_SendCtlCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iChannelNo : 
*            float  fParam     : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:31
*==========================================================================*/
void MT_SendCtlCmd(int iChannelNo, float fParam)
{
	int				i;
	BYTE			abyVal[4];

	if(iChannelNo < CNMG_MAX_MT_BROADCAST_CMD_CHN)
	{
		switch(iChannelNo)
		{
		case CNMG_CURR_LIMT:
			CAN_FloatToString(fParam / 100, abyVal);
			PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
				CAN_ADDR_FOR_BROADCAST,
				CAN_CMD_TYPE_BROADCAST,
				0,
				MT_VAL_TYPE_W_CURR_LMT,
				abyVal);
			break;

		case CNMG_DC_VOLT:
			CAN_FloatToString(fParam , abyVal);
			PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
				CAN_ADDR_FOR_BROADCAST,
				CAN_CMD_TYPE_BROADCAST,
				0,
				MT_VAL_TYPE_W_DC_VOLT,
				abyVal);
			break;

		case CNMG_DC_ON_OFF:

			abyVal[0] = 0;
			abyVal[1] = FLOAT_EQUAL0(fParam)? MT_DC_ON : MT_DC_OFF;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
				CAN_ADDR_FOR_BROADCAST,
				CAN_CMD_TYPE_BROADCAST,
				0,
				MT_VAL_TYPE_W_DC_ON_OFF,
				abyVal);

			for(i = 0; i < MAX_NUM_MPPT; i++)
			{
				g_CanData.CanCommInfo.MpptCommInfo.aMpptSetSig[i].dwDcOnOff 
					= FLOAT_EQUAL0(fParam)? MT_DC_ON : MT_DC_OFF;
			}
			break;

		case CNMG_HI_VOLT_LMT:
			CAN_FloatToString(fParam , abyVal);
			PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
				CAN_ADDR_FOR_BROADCAST,
				CAN_CMD_TYPE_BROADCAST,
				0,
				MT_VAL_TYPE_W_HIGH_VOLT,
				abyVal);
			break;

		case CNMG_FAN_FULL_SPEED:
			abyVal[0] = 0;
			abyVal[1] = FLOAT_EQUAL0(fParam) ? MT_FAN_AUTO : MT_FAN_FULL_SPEED;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
				CAN_ADDR_FOR_BROADCAST,
				CAN_CMD_TYPE_BROADCAST,
				0,
				MT_VAL_TYPE_W_FAN_FULL_SPEED,
				abyVal);
			for(i = 0; i < MAX_NUM_MPPT; i++)
			{
				g_CanData.CanCommInfo.MpptCommInfo.aMpptSetSig[i].dwFanFullSpeed 
					= FLOAT_EQUAL0(fParam) ? MT_FAN_AUTO : MT_FAN_FULL_SPEED;
			}
			break;


		case CNMG_ESTOP_FUNCTION:

			if(FLOAT_EQUAL0(fParam))
			{
				abyVal[0] = 0xff;
				abyVal[1] = 0xff;
				abyVal[2] = 0;
				abyVal[3] = 0;
			}
			else
			{
				abyVal[0] = 0;
				abyVal[1] = 0;
				abyVal[2] = 0x5a;
				abyVal[3] = 0x5a;
			}

			PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
				CAN_ADDR_FOR_BROADCAST,
				CAN_CMD_TYPE_BROADCAST,
				0,
				MT_VAL_TYPE_W_ESTOP_ENB,
				abyVal);
			break;

		case CNMG_ALL_LEDS_BLINK:
			abyVal[0] = 0;
			abyVal[1] = FLOAT_EQUAL0(fParam) ? MT_LED_NOT_BLINK : MT_LED_BLINK;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
				CAN_ADDR_FOR_BROADCAST,
				CAN_CMD_TYPE_BROADCAST,
				0,
				MT_VAL_TYPE_W_LED_BLINK,
				abyVal);
			for(i = 0; i < MAX_NUM_MPPT; i++)
			{
				g_CanData.CanCommInfo.MpptCommInfo.aMpptSetSig[i].dwLedBlink
					= FLOAT_EQUAL0(fParam)? MT_LED_NOT_BLINK : MT_LED_BLINK;
			}
			break;

		case CNMG_OVER_V_RESTART_TIME:
			CAN_FloatToString(fParam , abyVal);
			PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
				CAN_ADDR_FOR_BROADCAST,
				CAN_CMD_TYPE_BROADCAST,
				0,
				MT_VAL_TYPE_W_RESTAMT_TIME,
				abyVal);
			break;

		case CNMG_MPPT_DELTA_VOLTAGE:
			CAN_FloatToString(fParam , abyVal);
			PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
				CAN_ADDR_FOR_BROADCAST,
				CAN_CMD_TYPE_BROADCAST,
				0,
				MT_VAL_TYPE_W_MPPT_DELTA_VOLTAGE,
				abyVal);
			break;

		case CNMG_WALKIN_TIME:
			CAN_FloatToString(fParam * 100/*The unit is 10ms*/ , abyVal);
			PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
				CAN_ADDR_FOR_BROADCAST,
				CAN_CMD_TYPE_BROADCAST,
				0,
				MT_VAL_TYPE_W_WALKIN_TIME,
				abyVal);
			break;

		case CNMG_WALKIN_ENB:
			abyVal[0] = 0;
			abyVal[1] = FLOAT_EQUAL0(fParam) ? MT_WALKIN_NORMAL : MT_WALKIN_ENB;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
				CAN_ADDR_FOR_BROADCAST,
				CAN_CMD_TYPE_BROADCAST,
				0,
				MT_VAL_TYPE_W_WALKIN_ENB,
				abyVal);
			for(i = 0; i < MAX_NUM_MPPT; i++)
			{
				g_CanData.CanCommInfo.MpptCommInfo.aMpptSetSig[i].dwWalkinEnabled 
					= FLOAT_EQUAL0(fParam)? MT_WALKIN_NORMAL : MT_WALKIN_ENB;
			}
			break;

		case CNMG_RESTART_FOR_DC_OV:	
			abyVal[0] = 0;
			abyVal[1] = FLOAT_EQUAL0(fParam) ? MT_OV_RESET_DISB : MT_OV_RESET_ENB;
			abyVal[2] = 0;
			abyVal[3] = 0;

			//2010/12/27
			//��Э��VALUETYPEL  0x39,�������������ʹ�ܵ�����
			PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
				CAN_ADDR_FOR_BROADCAST,
				CAN_CMD_TYPE_BROADCAST,
				0,
				0x39,
				abyVal);
			break;
		case CNMG_MPPT_CLR_IDS:
			ClrMpptFalshInfo();
			break;
		case CNMG_MPPT_CLR_COMM_FAIL:
			MT_TriggerAllocation();			
			break;
		default:
			TRACE("Invalid RT Group control channel %d!\n",
				iChannelNo);
			AppLogOut("SAMP", 
				APP_LOG_WARNING, 
				"Invalid RT Group control channel %d!\n",
				iChannelNo);
			break;
		}
		return;
	}
	else
	{
		int		iMpptAddr = GetMpptAddrByChnNo(iChannelNo);
		int		iSubChnNo = ((iChannelNo - CNMG_MAX_MT_BROADCAST_CMD_CHN - 1) 
			% MAX_CHN_NUM_PER_MPPT) 
			+ CNMG_MAX_MT_BROADCAST_CMD_CHN 
			+ 1;
		//printf("iMpptAddr=%d, iSubChnNo = %d\n", iMpptAddr, iSubChnNo);
		switch(iSubChnNo)
		{
		case CNMS_DC_ON_OFF:
			g_CanData.CanCommInfo.MpptCommInfo.aMpptSetSig[iMpptAddr].dwDcOnOff
				= FLOAT_EQUAL0(fParam)? MT_DC_ON : MT_DC_OFF;
			break;

		case CNMS_LED:
			g_CanData.CanCommInfo.MpptCommInfo.aMpptSetSig[iMpptAddr].dwLedBlink 
				= FLOAT_EQUAL0(fParam)? MT_LED_NORMAL : MT_LED_BLINK;
			break;		
		case CNMS_MPPT_RESET_FOR_DC_OV:
			g_CanData.CanCommInfo.MpptCommInfo.aMpptSetSig[iMpptAddr].dwResetOnDcOV 
				= FLOAT_EQUAL0(fParam)? MT_RESET_NORMAL : MT_RESET_ENB;
			break;
		default:
			TRACE("Invalid RT Unit control channel %d, SubChn = %d!\n",
				iChannelNo,
				iSubChnNo);
			AppLogOut("SAMP", 
				APP_LOG_WARNING, 
				"Invalid RT Unit control channel %d, SubChn = %d!\n",
				iChannelNo,
				iSubChnNo);
			break;
		}
	}
	return;

}
#define	MT_NO_DISCHAGE_ST		0
#define	MT_DISCHAGE_ST			1
/*==========================================================================*
* FUNCTION : MtJudgeAcFail
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-10-07 17:21
*==========================================================================*/
static BOOL MtJudgeNightStatus(void)
{
	static	BOOL	s_bLastAcFail = FALSE;
	static INT32	s_iDelayRCTAcFailTimes = 0;
	int				i;
	BOOL			bAllNightStatus = TRUE;
	BOOL			bAllCommInterrupt = TRUE;

#define  RCT_AC_FAIL_DELAY  4

	if (s_iDelayRCTAcFailTimes < RCT_AC_FAIL_DELAY)
	{
		s_iDelayRCTAcFailTimes++;// Reconfig Query  Query  Query. Enough!
	}


	for(i = 0; i < g_CanData.CanCommInfo.MpptCommInfo.iCommMpptNum; i++)
	{

		//printf("\n	This is g_CanData.aRoughDataMppt[%d][MPPT_AC_VOLTAGE].fValue =%f	\n",i,
		//		g_CanData.aRoughDataMppt[i][MPPT_AC_VOLTAGE].fValue);

		//printf("	g_CanData.aRoughDataMppt[%d][MPPT_INTERRUPT_TIMES].iValue=%d	\n",i,
		//	g_CanData.aRoughDataMppt[i][MPPT_INTERRUPT_TIMES].iValue);

		if(g_CanData.aRoughDataMppt[i][MPPT_INTERRUPT_TIMES].iValue)
		{
			continue;
		}

		bAllCommInterrupt = FALSE;


		if(g_CanData.aRoughDataMppt[i][MPPT_DC_ON_OFF_STATUS].iValue == MT_DC_ON)
		{
			g_CanData.aRoughDataMppt[i][MPPT_DAYNIGHT_STATUS].iValue = MT_STATUS_DAY;

			if (s_iDelayRCTAcFailTimes < RCT_AC_FAIL_DELAY)
			{
				g_CanData.aRoughDataMppt[i][MPPT_DAYNIGHT_STATUS].iValue = MT_STATUS_DAY;
			}
			//printf("g_CanData.aRoughDataMppt[%d][MPPT_DAYNIGHT_STATUS].iValue = %d\n",i, g_CanData.aRoughDataMppt[i][MPPT_DAYNIGHT_STATUS].iValue);
		}
		else
		{
			g_CanData.aRoughDataMppt[i][MPPT_DAYNIGHT_STATUS].iValue = MT_STATUS_NIGHT;
			//printf("g_CanData.aRoughDataMppt[%d][MPPT_DAYNIGHT_STATUS].iValue = %d\n",i, g_CanData.aRoughDataMppt[i][MPPT_DAYNIGHT_STATUS].iValue);
		}

	}



	for(i = 0; i < g_CanData.CanCommInfo.MpptCommInfo.iCommMpptNum; i++)
	{
		if(g_CanData.aRoughDataMppt[i][MPPT_INTERRUPT_TIMES].iValue)
		{
			continue;
		}

		if(g_CanData.aRoughDataMppt[i][MPPT_DAYNIGHT_STATUS].iValue != MT_STATUS_NIGHT)
		{
			bAllNightStatus = FALSE;
			break;
		}
	}

	if(bAllCommInterrupt)
	{
		/*Need change it to Day or Night*/		
	}

	s_bLastAcFail = bAllNightStatus;

	return bAllNightStatus;
}

/*==========================================================================*
* FUNCTION : MtCalcAcVolt
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-10-08 17:49
*==========================================================================*/
static void MtCalcInputVolt(void)
{
	int			i;
	SAMPLING_VALUE		MaxVolt;
	BOOL			bCalculated = FALSE;

	//Need redefine.	
	MaxVolt.iValue = CAN_SAMP_INVALID_VALUE;


	for(i = 0; i < g_CanData.CanCommInfo.MpptCommInfo.iCommMpptNum; i++)
	{

		if((MaxVolt.iValue == CAN_SAMP_INVALID_VALUE)
			|| (MaxVolt.fValue < g_CanData.aRoughDataMppt[i][MPPT_AC_VOLTAGE].fValue))
		{
			MaxVolt.fValue 
				= g_CanData.aRoughDataMppt[i][MPPT_AC_VOLTAGE].fValue;
			bCalculated = TRUE;
		}
	}

	if(bCalculated)
	{			
		g_CanData.aRoughDataGroup[GROUP_MT_INPUT_VOLT].iValue = MaxVolt.iValue;		
	}

	return;
}



//����HVDC��ʱ���ֵ�
//1.���ֶ����ģ���ʱ�򣬻ᵼ��CAN���ϣ�
//���Ǵ�ʱ�����ϻ�������ģ�飬������Ҫ����ɨ��
//2.����LVD����ģ�鶼ͣ���ˣ����ʱ��ɨ��һ��0-9��ģ��
//������ɹ����������Ϊ����Ϊͣ���ˣ�����Ҫ����ԭ����״̬
//�ȴ������ˣ������ģ��ָ������ˡ�
BOOL MpptIsNeedReConfig()
{
	int			iRepeat00CmdTimes;
	SIG_ENUM	emRst;
	int			iAddr;

	for(iAddr = 0; iAddr < 10; iAddr++)
	{
		iRepeat00CmdTimes = 0;

		while(iRepeat00CmdTimes < 2)
		{
			iRepeat00CmdTimes++;

			//emRst = RtSampleCmd00(iAddr);
			emRst = MpptSampleCmd00(iAddr);		//G3 ���� Catherine Wang

			if(CAN_SAMPLE_OK == emRst
				|| CAN_MPPT_REALLOCATE == emRst)
			{
				return TRUE;
			}
		}
	}

	return FALSE;
}

/*==========================================================================*
* FUNCTION : RtCalcRatedCurr
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-10-07 17:31
*==========================================================================*/
static int RtCalcRatedCurr(void)
{
	int				i;
	SAMPLING_VALUE	RatedCurr;

	RatedCurr.iValue = CAN_SAMP_INVALID_VALUE;

	for(i = 0; i < g_CanData.CanCommInfo.MpptCommInfo.iCommMpptNum; i++)
	{
		if(g_CanData.aRoughDataMppt[i][MPPT_INTERRUPT_TIMES].iValue)
		{
			continue;
		}

		if(CAN_SAMP_INVALID_VALUE != g_CanData.aRoughDataMppt[i][MPPT_RATED_CURR].iValue)
		{
			RatedCurr.fValue = g_CanData.aRoughDataMppt[i][MPPT_RATED_CURR].fValue;
			break;
		}
	}

	return RatedCurr.iValue;
}


/*==========================================================================*
* FUNCTION : RtIsAllNoResponse
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-25 10:55
*==========================================================================*/
static BOOL MpptIsAllNoResponse(void)
{
	int		i;
	int		iMpptNum = g_CanData.CanCommInfo.MpptCommInfo.iCommMpptNum;
	BOOL	bRst = TRUE;
	static time_t	s_tNow = 0, s_tLastTime = 0;
	if(s_tLastTime == 0)
	{
		s_tLastTime = time((time_t *)0);
	}

	for(i = 0; i < iMpptNum; i++)
	{
		/*if(g_CanData.aRoughDataMppt[i][MPPT_INTERRUPT_TIMES].iValue 
			< MT_MAX_INTERRUPT_TIMES)
		{
			bRst = FALSE;
			break;
		}*/
		if(!g_CanData.aRoughDataMppt[i][MPPT_INTERRUPT_ST].iValue)
		{
			bRst = FALSE;
			break;
		}
	}

	if(bRst)
	{
		for(i = 0; i < iMpptNum; i++)
		{
			g_CanData.aRoughDataMppt[i][MPPT_INTERRUPT_ST].iValue 
				= MT_COMM_ALL_INTERRUPT_ST;
		}	
	}

	if(bRst && (iMpptNum== 0))
	{
		s_tNow = time((time_t *)0);
//		printf("%d -%d= %d  > %d\n",s_tNow , s_tLastTime,s_tNow - s_tLastTime,gs_iMpptFailedTimer/1000);
		if((s_tNow - s_tLastTime) > gs_iMpptFailedTimer/1000)
		{
			bRst = TRUE;
		}
		else 
		{
			bRst = FALSE;
		}
	}
	else
	{
		s_tLastTime = time((time_t *)0);
	}

	return bRst;
}

#define		MT_TYPE_NO_G1_55_1		3
#define		MT_TYPE_NO_G1_55_2		4
#define		MT_TYPE_NO_G1_55_3		5
#define		MT_TYPE_NO_G2_55		35
#define		MT_TYPE_NO_G2_60		39



/*==========================================================================*
* FUNCTION : MpptSetGoupFeatureSig
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : by IlockTeng                DATE: 2009-10-21 16:25
*==========================================================================*/
static void MpptSetGoupFeatureSig()
{
	if(g_CanData.aRoughDataMppt[0][MPPT_RATED_VOLT].iValue 
		== CAN_SAMP_INVALID_VALUE)
	{
		g_CanData.aRoughDataGroup[GROUP_MT_INPUT_RATED_VOLT].fValue = 80.0;
		g_CanData.aRoughDataGroup[GROUP_MT_RATED_VOLT].fValue = 48.0;
		g_CanData.aRoughDataGroup[GROUP_MT_RATED_CURR].fValue = 50.0;		
	}
	else
	{
		g_CanData.aRoughDataGroup[GROUP_MT_INPUT_RATED_VOLT].fValue 
			= g_CanData.aRoughDataMppt[0][MPPT_AC_RATED_VOLT].fValue;
		g_CanData.aRoughDataGroup[GROUP_MT_RATED_VOLT].fValue 
			= g_CanData.aRoughDataMppt[0][MPPT_RATED_VOLT].fValue;
		g_CanData.aRoughDataGroup[GROUP_MT_RATED_CURR].fValue 
			= g_CanData.aRoughDataMppt[0][MPPT_RATED_CURR].fValue;

	}
}

#define	PHASE_AB_NO		0
#define	PHASE_BC_NO		1
#define	PHASE_CA_NO		2

#define	AC_RESTORE_HYSTERESIS	4
/*==========================================================================*
* FUNCTION : RtNonSampSigHandle
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-10-07 17:23
*==========================================================================*/
static void MpptNonSampSigHandle(void)
{
	static	int		s_iAcRestoreCounter = 0;
	static  int		s_iDelayAcFailTimes = 0;//Need a little hysteresis first running

#define  MT_AC_FAIL_DELAY  4

	if (s_iDelayAcFailTimes < MT_AC_FAIL_DELAY)
	{
		s_iDelayAcFailTimes++;// Reconfig Query  Query  Query. Enough!
	}

	g_CanData.aRoughDataGroup[GROUP_MT_ALL_NO_RESPONSE].iValue 
		= MpptIsAllNoResponse();

	MtCalcInputVolt();

	//need a little hysteresis when AC restored
	if(MtJudgeNightStatus())
	{
		//
		g_CanData.aRoughDataGroup[GROUP_MT_DAYNIGHT_STATUS].iValue = MT_STATUS_DAY;
		s_iAcRestoreCounter = 0;

		if (s_iDelayAcFailTimes >= MT_AC_FAIL_DELAY)
		{
			//ԭ���룬���ڼӸ��������ϵ��Ҫ��120��QUERY��ʱ��Ż�澯
			g_CanData.aRoughDataGroup[GROUP_MT_DAYNIGHT_STATUS].iValue = MT_STATUS_NIGHT;
		}
		/*printf("MtJudgeNightStatus g_CanData.aRoughDataGroup[GROUP_MT_DAYNIGHT_STATUS].iValue= %d\n", g_CanData.aRoughDataGroup[GROUP_MT_DAYNIGHT_STATUS].iValue);*/
	}
	else
	{

		g_CanData.aRoughDataGroup[GROUP_MT_DAYNIGHT_STATUS].iValue = MT_STATUS_DAY;

		TRACE("\n  if(RtJudgeAcFail()  else (s_iAcRestoreCounter < AC_RESTORE_HYSTERESIS) \n");

		if (s_iDelayAcFailTimes < MT_AC_FAIL_DELAY)
		{
			//���ﲻ����������ǼӸ��ж��ݴ�
			g_CanData.aRoughDataGroup[GROUP_MT_DAYNIGHT_STATUS].iValue = MT_STATUS_NIGHT;
		}
		/*printf("g_CanData.aRoughDataGroup[GROUP_MT_DAYNIGHT_STATUS].iValue= %d\n", g_CanData.aRoughDataGroup[GROUP_MT_DAYNIGHT_STATUS].iValue);*/

	}

	MpptDefaultVoltParam(FALSE);
	MpptSetGoupFeatureSig();
	return;
}


/*==========================================================================*
* FUNCTION : RtAnalyseFeature
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE*  pbyBuf : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:28
*==========================================================================*/
static BOOL MtAnalyseFeature(int iAddr)

{
	UINT		uiFeature;
	int			iDcTypeSamp;
	int			iDcTypeReturn;
	int			iAcPhaseNum;
	int			iAcRatedVoltNo;
	int			iRatedCurrNo;
	int			iRatedVoltNo;
	int			iEfficiencyNo;

	float		fMinRatedCurr = 1000.0;


	if(g_CanData.aRoughDataMppt[iAddr][MPPT_FEATURE].iValue 
		== CAN_SAMP_INVALID_VALUE)
	{
		g_CanData.aRoughDataMppt[iAddr][MPPT_RATED_VOLT].fValue
			= 48.0;
		g_CanData.aRoughDataMppt[iAddr][MPPT_RATED_CURR].fValue
			= 50.0;
		g_CanData.aRoughDataMppt[iAddr][MPPT_AC_RATED_VOLT].fValue
			= 220.0;


		return;
	}

	uiFeature = g_CanData.aRoughDataMppt[iAddr][MPPT_FEATURE].uiValue;

	iRatedVoltNo = (uiFeature & 0x00003ff8) >> 3;
	g_CanData.aRoughDataMppt[iAddr][MPPT_RATED_VOLT].fValue = 0.5 * iRatedVoltNo;

	iRatedCurrNo = (uiFeature & 0x00ffc000) >> 14;
	g_CanData.aRoughDataMppt[iAddr][MPPT_RATED_CURR].fValue = iRatedCurrNo *0.5;

	iEfficiencyNo = (uiFeature & 0x00000007);
	g_CanData.aRoughDataMppt[iAddr][MPPT_EFFICIENCY_NO].iValue = iEfficiencyNo;
	return;
}




#define	MAX_SAMPLE_TIMES	5
/*==========================================================================*
* FUNCTION : MT_Reconfig
* PURPOSE  : scan all rectifiers, get rectifiers connecting information and 
*            save the information into the flash
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-06-24 15:59
*==========================================================================*/
void MT_Reconfig(void)
{
	int			iAddr,iAddrAgain;
	SIG_ENUM	emRst = 10;//To avoid the C++ test issue.
	static	int	siMaxReallocTimes = 0;
	int			i;
	//TRACE("************CAN DEBUG: Mpptifiers is reconfiging!\n");

	MT_InitRoughValue();

	for(iAddr = 0; iAddr < MAX_NUM_MPPT; iAddr++)
	{
		int		iRepeat00CmdTimes = 0;
		int		iRepeat20CmdTimes = 0;

		while(iRepeat00CmdTimes < MAX_SAMPLE_TIMES)
		{
			emRst = MpptSampleCmd00(iAddr);

			//printf("************CAN DEBUG: Sampled %d rectifier, result is %d!\n",
			//iAddr,
			//emRst);

			if(CAN_SAMPLE_OK == emRst)
			{
				break;
			}

			if(CAN_MPPT_REALLOCATE == emRst)
			{
				return;
			}

			if(CAN_SAMPLE_FAIL == emRst)
			{
				iRepeat00CmdTimes++;
				Sleep(150);
			}
		}

		if(iRepeat00CmdTimes >= MAX_SAMPLE_TIMES)
		{
			break;
		}

		g_CanData.CanFlashData.aMpptInfo[iAddr].bExistence = TRUE;

		g_CanData.aRoughDataMppt[iAddr][MPPT_EXIST_ST].iValue = MT_EQUIP_EXISTENT;

		Timer_Set(RunThread_GetId(NULL), ID_TIMER_MPPT_CALC_ALARM_1HOUR + iAddr, MT_ONE_HOUR/GetTestReduceTimes(), (ON_TIMER_PROC)MtSetCalcAlarm, (DWORD)iAddr);
		while(iRepeat20CmdTimes < MAX_SAMPLE_TIMES)
		{
			emRst = MpptSampleCmd20(iAddr);

			if(CAN_SAMPLE_OK == emRst)
			{
				if(!MpptAnalyseSerialNo(iAddr))
				{
					MpptAnalyseFeature(iAddr);

				}
				
				break;
			}

			if(CAN_MPPT_REALLOCATE == emRst)
			{
				return;
			}

			if(CAN_SAMPLE_FAIL == emRst)
			{
				iRepeat20CmdTimes++;
				Sleep(150);
			}
		}
	}

	//������Ź����У��м�ģ�鱻�ε������
	for (iAddrAgain = iAddr; iAddrAgain < iAddr + 3; iAddrAgain++)
	{
		emRst = MpptSampleCmd00(iAddrAgain);

		if(CAN_SAMPLE_OK == emRst)
		{
			siMaxReallocTimes++;

			if (siMaxReallocTimes < 3)
			{			
				MT_TriggerAllocation();
				return;
			}
			else
			{
				break;
			}

		}

		if(CAN_MPPT_REALLOCATE == emRst)
		{
			//��������������ǲ���Ҫ�ģ���������˵�������⡣
			//Delete it , will go into the dead cycle.by YangGuoxin 2014/3/14
			return;
		}

		if(CAN_SAMPLE_FAIL == emRst)
		{
			Sleep(20);
		}
	}

	siMaxReallocTimes = 0;
	g_CanData.CanCommInfo.MpptCommInfo.iCommMpptNum = iAddr;
	//printf("iAddr = %d\n", iAddr);
	if(iAddr)
	{
		g_CanData.aRoughDataGroup[GROUP_MT_ACTUAL_NUM].iValue = iAddr;

		g_CanData.aRoughDataGroup[GROUP_MT_EXIST_STAT].iValue = MT_EQUIP_EXISTENT;//exist
		//MT_RefreshFlashInfo();
	}
	//If the MPPT number is same with last running, the controller need calculate the alarm
	if(g_CanData.aRoughDataGroup[GROUP_MT_ACTUAL_NUM].iValue >= GetEnumSigValue(MT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							MT_LAST_RUN_ON_NUMBER_SIGID,
							"CAN_SAMP"))
	{
		for(i = 0; i < iAddr; i++)
		{
			g_CanData.bCalcAlarm[i][0] = TRUE;
		}
	}

	for(; iAddr < MAX_NUM_MPPT; iAddr++)
	{
		g_CanData.aRoughDataMppt[iAddr][MPPT_EXIST_ST].iValue = MT_EQUIP_NOT_EXISTENT;
		//	printf("%d status is ",g_CanData.aRoughDataMppt[iAddr][MPPT_EXIST_ST].iValue);

	}
	//printf("g_CanData.aRoughDataGroup[GROUP_MT_ACTUAL_NUM].iValue = %d\n", g_CanData.aRoughDataGroup[GROUP_MT_ACTUAL_NUM].iValue);
	if (0 == g_CanData.aRoughDataGroup[GROUP_MT_ACTUAL_NUM].iValue)
	{
		g_CanData.aRoughDataGroup[GROUP_MT_EXIST_STAT].iValue = MT_EQUIP_NOT_EXISTENT;//inexistence
	}
	
	if(GetEnumSigValue(MT_SYSTEM_EQUIPID, 
		SIG_TYPE_SETTING,
		MT_MPPT_SETTING_MODE,
		"CAN_SAMP"))// Enable
	{
		g_CanData.aRoughDataGroup[GROUP_MT_EXIST_STAT].iValue = MT_EQUIP_EXISTENT;//exist
	}

	MpptNonSampSigHandle();

	MpptUnifyParam();
	Sleep(100);

	MpptDefaultVoltParam(TRUE);
	Sleep(100);

	return;
}

/*==========================================================================*
* FUNCTION : SetMpptRunTime
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr  : 
*            float  fParam : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:30
*==========================================================================*/
static void SetMpptRunTime(int iAddr, float fParam)
{
	BYTE			abyVal[4];

	CAN_FloatToString(fParam , abyVal);
	PackAndSendMpptCmd(MSG_TYPE_RQST_SETTINGS,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		0,
		MT_VAL_TYPE_W_RUN_TIME,
		abyVal);
	return;
}



/*==========================================================================*
* FUNCTION : RefreshMpptRunTime
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:30
*==========================================================================*/
static void RefreshMpptRunTime(void)
{
	int					i;
	time_t				tNow;   
	struct tm			tmNow; 


	tNow = time(NULL);

	gmtime_r(&tNow, &tmNow);

	if(g_CanData.CanCommInfo.MpptCommInfo.bNeedRefreshRuntime)
	{
		if(tmNow.tm_min == 59)
		{
			for(i = 0; i < MAX_NUM_MPPT; i++)
			{
				if(g_CanData.aRoughDataMppt[i][MPPT_TOTAL_RUN_TIME].iValue
					== CAN_SAMP_INVALID_VALUE)
				{
					//Do nothing until a sampling
					continue;
				}

				if(g_CanData.aRoughDataMppt[i][MPPT_INTERRUPT_TIMES].iValue
					< MT_MAX_INTERRUPT_TIMES)
				{
					if(MT_DC_ON 
						== g_CanData.aRoughDataMppt[i][MPPT_DC_ON_OFF_STATUS].uiValue)
					{
						g_CanData.aRoughDataMppt[i][MPPT_TOTAL_RUN_TIME].uiValue++;
						SetMpptRunTime(i, 
							(float)(g_CanData.aRoughDataMppt[i][MPPT_TOTAL_RUN_TIME].uiValue));
						Sleep(10);
					}
				}
			}
			g_CanData.CanCommInfo.MpptCommInfo.bNeedRefreshRuntime = FALSE;
		}
	}
	else
	{
		if(tmNow.tm_min == 1)
		{
			g_CanData.CanCommInfo.MpptCommInfo.bNeedRefreshRuntime = TRUE;
		}
	}

	return;
}


/*==========================================================================*
* FUNCTION : NeedHandleRtPosPhase
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:28
*==========================================================================*/
static BOOL NeedHandleMpptPosPhase(void)
{
	if(GetDwordSigValue(MT_GROUP_EQUIPID, 
		SIG_TYPE_SETTING,
		MT_CONFIRM_POS_PHASE_SIGID,
		"CAN_SAMP") > 0)
	{
		SetDwordSigValue(MT_GROUP_EQUIPID,
			SIG_TYPE_SETTING,
			MT_CONFIRM_POS_PHASE_SIGID,
			0,
			"CAN_SAMP");
		return TRUE;
	}

	return FALSE;
}



/*==========================================================================*
* FUNCTION : RtPosUniquenessHandle
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-10-07 14:42
*==========================================================================*/
static void RtPosUniquenessHandle(void)
{
	int	i, j;
	int	iMpptNum = g_CanData.CanCommInfo.MpptCommInfo.iCommMpptNum;

	if(!iMpptNum)
	{
		return;
	}

	for(i = 1; i < iMpptNum; i++)
	{
		int iPos = g_CanData.aRoughDataMppt[i][MPPT_POSITION_NO].iValue;
		for(j = 0; j < i; j++)
		{	
			if(iPos == g_CanData.aRoughDataMppt[j][MPPT_POSITION_NO].iValue)
			{
				g_CanData.aRoughDataMppt[i][MPPT_POSITION_NO].iValue
					= GetOneRestMpptPosNo();
				g_CanData.CanFlashData.aMpptInfo[i].iPositionNo 
					= g_CanData.aRoughDataMppt[i][MPPT_POSITION_NO].iValue;
			}
		}
	}

	return;
}




/*==========================================================================*
* FUNCTION : HandleRtPosAndPhase
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:28
*==========================================================================*/
static void HandleMtPosAndPhase(BOOL bReadConfirm)
{
	int			i, j;
	int			iMpptNum = g_CanData.CanCommInfo.MpptCommInfo.iCommMpptNum;
	if(iMpptNum <= 0)
	{
		return;
	}
	//Ranged by sequence No.
	MPPT_POS_ADDR	Flash_Map[MAX_NUM_MPPT]; //�洢��Flash��Map
	MPPT_POS_ADDR	sFinal_Map[MAX_NUM_MPPT]; //������õ�Map
	//��ʼ��
	static BOOL bFirstTime = TRUE;
	int iFlashMapedNo = 0;
	int iSeqNo = 0;
	//printf("\n_____________BEGIN__________");
	for(i = 0; i < MAX_NUM_MPPT; i++)
	{
		if((g_CanData.CanFlashData.aMpptInfo[i].dwSerialNo > 0) && 
			(g_CanData.CanFlashData.aMpptInfo[i].dwSerialNo != -1) &&
			(g_CanData.CanFlashData.aMpptInfo[i].iPositionNo > 0) &&
			(g_CanData.CanFlashData.aMpptInfo[i].iPositionNo <= 999))
		{
			Flash_Map[i].dwSerialNo = g_CanData.CanFlashData.aMpptInfo[i].dwSerialNo;
			Flash_Map[i].dwHiSN = g_CanData.CanFlashData.aMpptInfo[i].dwHighSn;
			Flash_Map[i].iPhaseNo = g_CanData.CanFlashData.aMpptInfo[i].iAcPhase;
			Flash_Map[i].iPositionNo = g_CanData.CanFlashData.aMpptInfo[i].iPositionNo;
			iFlashMapedNo++;
			//printf("\n i=%d; dwSerialNo=%d; iPos=%d",i,
			//	Flash_Map[i].dwSerialNo,Flash_Map[i].iPositionNo);
		}
		sFinal_Map[i].iPositionNo = -1;
		sFinal_Map[i].bIsNewInserted = TRUE;
		sFinal_Map[i].iSeqNo = -1;
		sFinal_Map[i].dwSerialNo = 0;
		//sFinal_Map[i].iPhaseNo = GetPhaseNo(i);//��ABCABC�ķ�ʽ����
		//if(bFirstTime)
		//{
		//	iFlashMapedNo = 0;
		//	Flash_Map[i].dwSerialNo = -1;
		//	sFinal_Map[i].iSeqNo = i;
		//}
		//else
		//{
		//	sFinal_Map[i].iPositionNo = -1;
		//	sFinal_Map[i].dwSerialNo = -1;
		//}
		
	}
	//printf("_____________END,and the FlashMappedNo=%d__________\n",iFlashMapedNo);
	//bFirstTime = FALSE;
	//���´�Flash��λ�úŲ�ƥ����֪��ģ��λ�ú�
	BOOL bIsNeedWriteFlash = TRUE; //�Ƿ���Ҫд��Flash
	if(iFlashMapedNo > iMpptNum && (!bReadConfirm)) //˵����ģ�鶪ʧ��
	{
		bIsNeedWriteFlash = FALSE;
	}
	BOOL bHasInvalidRect = FALSE;
	BOOL bHasRepeastedRect = FALSE;//�Ƿ������кų�ͻ
	//������кų�ͻ,ʵ��Ӧ�û����������ظ��������Ե�ʱ����Բ��Ե�
	for(i = 0; i < iMpptNum; i++)
	{
		for(j = i + 1; j < iMpptNum; j++)
		{
			if(g_CanData.aRoughDataMppt[i][MPPT_SERIEL_NO_LOW].dwValue == g_CanData.aRoughDataMppt[j][MPPT_SERIEL_NO_LOW].dwValue)
			{
				bHasRepeastedRect = TRUE;
				break;
			}
		}
	}
	if(!bReadConfirm)
	{
		for(i = 0; i < iMpptNum; i++)
		{
			sFinal_Map[i].dwSerialNo = g_CanData.aRoughDataMppt[i][MPPT_SERIEL_NO_LOW].dwValue;
			sFinal_Map[i].iSeqNo = i;
			//��ͨ���жϣ��򲻱�д��Flash���ڶ��ν����»ָ�ԭ��Flash���ֵ
			//bIsNeedWriteFlash = (sFinal_Map[i].dwSerialNo == CAN_SAMP_INVALID_VALUE) ? FALSE : TRUE;
			if(sFinal_Map[i].dwSerialNo == CAN_SAMP_INVALID_VALUE)
			{
				bHasInvalidRect = TRUE;
			}
			for(j = 0; j < iFlashMapedNo; j++)
			{
				if(sFinal_Map[i].dwSerialNo == Flash_Map[j].dwSerialNo)
				{
					sFinal_Map[i].bIsNewInserted = FALSE;
					sFinal_Map[i].iPositionNo = Flash_Map[j].iPositionNo;
					sFinal_Map[i].iPhaseNo = Flash_Map[j].iPhaseNo;
					break;
				}
			}
		}
	}
	else //��ȡ����ֵ
	{
		for(i = 0; i < iMpptNum; i++)
		{

			sFinal_Map[i].dwSerialNo = GetDwordSigValue(MT_START_EQUIP_ID + i,
								SIG_TYPE_SAMPLING,
								43,
								"CAN_SAMP");
			//sFinal_Map[i].iPhaseNo = GetDwordSigValue(MT_START_EQUIP_ID + i,
			//					SIG_TYPE_SETTING,
			//					MT_PHASE_SET_SIG_ID,
			//					"CAN_SAMP");
			sFinal_Map[i].iPositionNo = GetDwordSigValue(MT_START_EQUIP_ID + i,
											SIG_TYPE_SETTING,
											MT_POS_SET_SIG_ID,
											"CAN_SAMP");

			//��ͨ���жϣ��򲻱�д��Flash���ڶ��ν����»ָ�ԭ��Flash���ֵ
			//bIsNeedWriteFlash = (sFinal_Map[i].dwSerialNo == CAN_SAMP_INVALID_VALUE) ? FALSE : TRUE;

			if(sFinal_Map[i].dwSerialNo == 0)
			{
				bHasInvalidRect = TRUE;
			}
			sFinal_Map[i].bIsNewInserted = FALSE;

			for(j = 0; j < iMpptNum; j++)
			{
				if(g_CanData.aRoughDataMppt[j][MPPT_SERIEL_NO_LOW].dwValue == sFinal_Map[i].dwSerialNo)
				{
					sFinal_Map[i].iSeqNo = j;
					break;
				}
			}
		}
	}
	//������Ҫ����λ�úų�ͻ�����
	//���������
	int k,s,iFoundPos;
	if(bReadConfirm || bFirstTime || bHasRepeastedRect) //�û������ˣ���Ҫ��������ͻ
	{
		for(i = 0; i < iMpptNum; i++)
		{
			if(sFinal_Map[i].iPositionNo < 1 || sFinal_Map[i].iPositionNo > 999)
			{
				iFoundPos = -1;
				for(k = 1; k <= MAX_NUM_MPPT; k++)
				{
					iFoundPos = k;
					for(s = 0; s < iMpptNum; s++)
					{
						if(sFinal_Map[s].iPositionNo == k)
						{
							iFoundPos = -1;
							break;
						}
					}
					if(iFoundPos != -1)
					{
						break;
					}
				}
				sFinal_Map[i].iPositionNo = (iFoundPos > 0 ? iFoundPos : 998);
				
			}
			else if(sFinal_Map[i].iSeqNo < 0 || sFinal_Map[i].iSeqNo > iMpptNum)
			{
				iFoundPos = -1;
				for(k = 0; k < iMpptNum; k++)
				{
					iFoundPos = k;
					for(s = 0; s < iMpptNum; s++)
					{
						if(sFinal_Map[s].iSeqNo == k)
						{
							iFoundPos = -1;
							break;
						}
					}
					if(iFoundPos != -1)
					{
						break;
					}
				}
				sFinal_Map[i].iSeqNo = (iFoundPos > 0 ? iFoundPos : 0);
			}
			else
			{
				for(j = i+1;j < iMpptNum; j++)
				{
					if(sFinal_Map[i].iPositionNo == sFinal_Map[j].iPositionNo)
					{
						iFoundPos = -1;
						for(k = 1; k <= MAX_NUM_MPPT; k++)
						{
							iFoundPos = k;
							for(s = 0; s < iMpptNum; s++)
							{
								if(sFinal_Map[s].iPositionNo == k)
								{
									iFoundPos = -1;
									break;
								}
							}
							if(iFoundPos != -1)
							{
								break;
							}
						}
						sFinal_Map[i].iPositionNo = (iFoundPos > 0 ? iFoundPos : 998);
						break;
					}

				}
				for(j = i+1;j < iMpptNum; j++)
				{
					
					if(sFinal_Map[i].iSeqNo == sFinal_Map[j].iSeqNo)
					{
						iFoundPos = -1;
						for(k = 0; k < iMpptNum; k++)
						{
							iFoundPos = k;
							for(s = 0; s < iMpptNum; s++)
							{
								if(sFinal_Map[s].iSeqNo == k)
								{
									iFoundPos = -1;
									break;
								}
							}
							if(iFoundPos != -1)
							{
								break;
							}
						}
						sFinal_Map[i].iSeqNo = (iFoundPos > 0 ? iFoundPos : 0);
						break;
					}
				}
			}
		}
	}
	else //���ŵ��µ�
	{
		for(j = 0;j < iMpptNum; j++)
		{
			if(sFinal_Map[j].iPositionNo < 1 ||
				sFinal_Map[j].bIsNewInserted)//����������һ���ط��ţ�ѡ����С�Ŀ���λ�ú�
			{
				if(!bHasInvalidRect)//���-9999��д������ͨ���жϺ��ģ���ǲ�ס��
				{
					bIsNeedWriteFlash = TRUE; //����ģ����£�˵����Ҫ����дFlash
				}
				iFoundPos = -1;
				for(k = 1; k <= MAX_NUM_MPPT; k++)
				{
					iFoundPos = k;
					for(s = 0; s < iMpptNum; s++)
					{
						if(sFinal_Map[s].iPositionNo == k)
						{
							iFoundPos = -1;
							break;
						}
					}
					if(iFoundPos != -1)
					{
						break;
					}
				}
				sFinal_Map[j].iPositionNo = (iFoundPos > 0 ? iFoundPos : 998);	
			}
		}
	}

	//���¿�ʼ����Postion���ã��Լ�Flash
	//printf("\n__________Begin Test Data____________");
	for(i = 0; i < MAX_NUM_MPPT; i++)
	{
		if(i < iMpptNum)
		{
			j = sFinal_Map[i].iSeqNo;
			//������ע�� �� i ���� j��Ҫ����,����Ĵ���Ƚϻ�ɬ�Ѷ�����Ҫ���׸Ķ�
			g_CanData.aRoughDataMppt[i][MPPT_SEQ_NO].iValue = j;

			g_CanData.aRoughDataMppt[j][MPPT_POSITION_NO].iValue = sFinal_Map[i].iPositionNo;			
			//g_CanData.aRoughDataMppt[j][MPPT_PHASE_NO].iValue = sFinal_Map[i].iPhaseNo;
			
			if((bIsNeedWriteFlash && (!bHasInvalidRect)) || bFirstTime)
			{
				g_CanData.CanFlashData.aMpptInfo[i].iPositionNo = sFinal_Map[i].iPositionNo;
				g_CanData.CanFlashData.aMpptInfo[i].iAcPhase = sFinal_Map[i].iPhaseNo;
				g_CanData.CanFlashData.aMpptInfo[i].dwSerialNo = sFinal_Map[i].dwSerialNo;
				g_CanData.CanFlashData.aMpptInfo[i].dwHighSn = sFinal_Map[i].dwHiSN;
				g_CanData.CanFlashData.aMpptInfo[i].iSeqNo = sFinal_Map[i].iSeqNo;
			}
			//printf("\ni=%d,HiSN=%d,BarC1=%d;   Pos=%d;  SeqNo=%d",i,sFinal_Map[i].dwHiSN,
			//	sFinal_Map[i].dwSerialNo,sFinal_Map[i].iPositionNo,sFinal_Map[i].iSeqNo);
		}
		else
		{
			if((bIsNeedWriteFlash && (!bHasInvalidRect)) || bFirstTime)
			{
				g_CanData.CanFlashData.aMpptInfo[i].dwSerialNo = 0;
				g_CanData.CanFlashData.aMpptInfo[i].iPositionNo = -1;
			}
			
		}
	}
	//printf("\n__________End Test Data____________\n");
	
	//-----------���»�Ҫ���򣬰�#1��#2��#3.��������
	MPPT_POS_ADDR	Sorted_Map[MAX_NUM_MPPT];//��������
	for (i = 0; i < iMpptNum; i++)
	{
		Sorted_Map[i].iPositionNo = g_CanData.aRoughDataMppt[i][MPPT_POSITION_NO].iValue;
		Sorted_Map[i].iSeqNo = i;
	}
	int iPos;
	for (i = 0; i < iMpptNum - 1; i++)
	{
		for (j = 0; j < (iMpptNum - 1 - i); j++)
		{
			if(Sorted_Map[j].iPositionNo > Sorted_Map[j + 1].iPositionNo)
			{
				iPos = Sorted_Map[j].iPositionNo;
				Sorted_Map[j].iPositionNo = Sorted_Map[j + 1].iPositionNo;
				Sorted_Map[j + 1].iPositionNo = iPos;
			}
		}
	}
	for (i = 0; i < iMpptNum; i++)
	{
		for (j = 0; j < iMpptNum; j++)
		{
			if(Sorted_Map[i].iPositionNo == g_CanData.aRoughDataMppt[j][MPPT_POSITION_NO].iValue)
			{
				g_CanData.aRoughDataMppt[j][MPPT_SEQ_NO].iValue = Sorted_Map[i].iSeqNo;
				break;
			}
		}
	}
	//-----------�������
	//�������ֵ
	for (i = 0; i < iMpptNum; i++)
	{
		j = (g_CanData.aRoughDataMppt[i][MPPT_SEQ_NO].iValue);

		SetDwordSigValue(MT_START_EQUIP_ID + j,
					SIG_TYPE_SETTING,
					MT_POS_SET_SIG_ID,
					sFinal_Map[i].iPositionNo,
					"CAN_SAMP");
		//SetDwordSigValue(MT_START_EQUIP_ID + j,
		//			SIG_TYPE_SETTING,
		//			MT_PHASE_SET_SIG_ID,
		//			sFinal_Map[i].iPhaseNo,
		//			"CAN_SAMP");


	}

	if((bIsNeedWriteFlash && (!bHasInvalidRect)) || bFirstTime)//�д���rectʱ��дFalsh
	{
		CAN_WriteFlashData(&(g_CanData.CanFlashData));
	}

	bFirstTime = FALSE;
	return;
}

/*==========================================================================*
* FUNCTION : WaitAllocation
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  :                 DATE: 2010-02-05 08:31
*==========================================================================*/
INT32 MT_WaitAllocation()
{
	INT32 i;
	INT32 iResetingAddr = 0;
	INT32 iTimeOut = 0;
	INT32 iRst;

	//�󾭹��������Ī��ƽȷ�ϣ����ŵ�ʱ��	ֻ��	��ַ1	
	//�Ϳ���ȷ���������������Ź����еĽ���ַ��0��100����Ϊ1
	//MAX_NUM_MPPT
	//for (i = 0; i <= 1; i++)
	//{
	//	iRst = MpptSampleCmd20(i);
	//	//TRACE(" Cmd20 Address %d  Rst %d \n",i,iRst);

	//	if (CAN_MPPT_REALLOCATE == iRst)
	//	{
	//		iResetingAddr = i;
	//		break;
	//	}
	//}

	while (CAN_MPPT_REALLOCATE == MpptSampleCmd20(iResetingAddr))
	{
		iTimeOut++;
		Sleep(500);

		TRACE("\n The Mpptifer Reseting \n");

		if (iTimeOut > 40)//500MS * 40 = 20S
		{
			TRACE("\n The Mpptifer have Bug that Allocation timeout \n");
			return -1;
		}
	}

	return 0;
}

static void SetMpptInfoChangedFlag()
{
#define		MODULE_INFO_CHANGE_SIG_TYPE		2
#define		MODULE_INFO_CHANGE_SIG_ID		37

	SetDwordSigValue(MT_GROUP_EQUIPID, 
		MODULE_INFO_CHANGE_SIG_TYPE, 
		MODULE_INFO_CHANGE_SIG_ID,
		TRUE,
		"FOR_RS485");
	//TRACE("\n @@@@@@@    SLAVE MODULES WAS CHANGE NOTIFY TO MASTER \n");

}

static DWORD GetTestReduceTimes()
{
	SIG_ENUM enumTestState = GetEnumSigValue(1, 2, 38, "CAN_SAMP");
	DWORD dwReduceTimes = GetDwordSigValue(1, 2, 40, "CAN_SAMP");

	return enumTestState ? dwReduceTimes : 1;	

}


#define MAX_TIMES_MT_CMD00	3
/*==========================================================================*
* FUNCTION : MT_Sample
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:31
*==========================================================================*/
void MT_Sample(void)
{
	int		iAddr;
	static		int iTwice = 0;
	static		BOOL bReport = FALSE;
	//if(!GetEnumSigValue(MT_SYSTEM_EQUIPID, 
	//			SIG_TYPE_SETTING,
	//			MT_MPPT_SETTING_MODE,
	//			"CAN_SAMP"))// Disable
	//{
	//	return;
	//}

	RefreshMpptRunTime();
	gs_iMpptFailedTimer = Mppt_GetMpptFailedTimer(gs_iMpptFailedTimer);

	//Modified by Samson Fan, 2009-11-17
	//���⣺������ͣ���ҵ���µ������ģ��ͨѶ�жϣ���ʱCAN�ֻ������ã�����ģ����ϢȫΪδ���ã�
	//�޷��鿴ͨѶ�ж�ǰ��ģ����Ϣ��Ӧ���Ǳ���֮ǰ����Ϣ��������ʾ
	//�����Ϊֻ���ڴ�û�ҵ���ģ�飬���߲���ģ�����ŵ�ַʱ�������á�
	if((g_CanData.CanCommInfo.MpptCommInfo.bNeedReconfig)
		|| (g_CanData.aRoughDataGroup[GROUP_MT_ACTUAL_NUM].iValue == 0))

	{
		RT_UrgencySendCurrLimit();
		MT_WaitAllocation();

		//This sentence must be in front of MT_Reconfig();
		g_CanData.CanCommInfo.MpptCommInfo.bNeedReconfig = FALSE;
		MT_Reconfig();
		SetMpptInfoChangedFlag();
		iTwice = 0;

		bReport = TRUE;
		RT_UrgencySendCurrLimit();
		return;
	}

	if(bReport)
	{
		NotificationToAll(MSG_RECT_ALLOCATION);
		bReport = FALSE;
	}
	//printf("NeedHandleRtPosPhase() = %d\n", NeedHandleRtPosPhase());
	BOOL bNeedConfirmPos = NeedHandleMpptPosPhase();
	if(bNeedConfirmPos || iTwice < 3 || g_bNeedClearMpptIDs)
	{
		if(g_bNeedClearMpptIDs)
		{
			g_bNeedClearMpptIDs = FALSE;
		}
		HandleMtPosAndPhase(bNeedConfirmPos);
		//Don't delete:Here is for fixing the reposition problem ,to handle twice!
		//HandleCtPos(bNeedConfirmPos);
		//printf("1----RunThread_PostMessage(-1, &msgFindEquip, FALSE);\n");
		if(iTwice == 3)
		{
			iTwice = 0;
		}
		else if(iTwice == 2)
		{
			NotificationToAll(MSG_CONIFIRM_ID);
			//printf("RunThread_PostMessage(-1, &msgFindEquip, FALSE);nResult = %d\n", nResult);
		}
		iTwice++;
		//Don't delete
	}

	for(iAddr = 0; iAddr < g_CanData.CanCommInfo.MpptCommInfo.iCommMpptNum; iAddr++)
	{
		if(g_CanData.CanFlashData.aMpptInfo[iAddr].bExistence)
		{
			if(CAN_MPPT_REALLOCATE == MpptSampleCmd00(iAddr))
			{
				break;
			}
			if( g_CanData.aRoughDataMppt[iAddr][MPPT_TYPE_NO].iValue == MPPT48V35A)
			{
				if(CAN_MPPT_REALLOCATE == MpptSampleInputPowerCmd(iAddr))
				{
					break;
				}

			}

			/*g_CanData.CanCommInfo.MpptCommInfo.iCmd00Counter[iAddr]++;

			if((g_CanData.CanCommInfo.MpptCommInfo.iCmd00Counter[iAddr] 
			>= MAX_TIMES_MT_CMD00))
			{

				g_CanData.CanCommInfo.MpptCommInfo.iCmd00Counter[iAddr] = 0; 
			}*/
		}
	}

	MpptNonSampSigHandle();

	return;
}



/*==========================================================================*
* FUNCTION : GetMpptAddrBySeqNo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iSeqNo : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:28
*==========================================================================*/
static int GetMpptAddrBySeqNo(int iSeqNo)
{
	int		i;
	int		iMpptNum = g_CanData.CanCommInfo.MpptCommInfo.iCommMpptNum;

	for(i = 0; i < iMpptNum; i++)
	{
		if(g_CanData.aRoughDataMppt[i][MPPT_SEQ_NO].iValue == iSeqNo)
		{
			return i;
		}
	}

	return 0;
}



#define MT_CH_END_FLAG			-1
/*==========================================================================*
* FUNCTION : MT_StuffChannel
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: ENUMSIGNALPROC EnumProc : //Callback function for stuffing channels 
*            LPVOID  lpvoid   : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:27
*==========================================================================*/
void MT_StuffChannel(ENUMSIGNALPROC EnumProc,	//Callback function for stuffing channels
		     LPVOID lpvoid)				//Parameter of the callback function
{
	int		i, j;
	int		iMpptNum = g_CanData.aRoughDataGroup[GROUP_MT_ACTUAL_NUM].iValue;

	CHANNEL_TO_ROUGH_DATA		MtGroupSig[] =
	{

		{MT_CH_MPPT_NUM,		GROUP_MT_ACTUAL_NUM,	},
		{MT_CH_RATED_VOLT,		GROUP_MT_RATED_VOLT,	},	
		{MT_CH_RATED_CURR,		GROUP_MT_RATED_CURR,	},

		{MT_CH_DAY_NIGHT_STATUS,		GROUP_MT_DAYNIGHT_STATUS,	},
		{MT_CH_ALL_NO_RESPONSE,		GROUP_MT_ALL_NO_RESPONSE,	},
		{MT_CH_INPUT_RATED_VOLT,		GROUP_MT_INPUT_RATED_VOLT,		},

		{MT_CH_GROUP_EXIST_STATUS,	GROUP_MT_EXIST_STAT,		},

		{MT_CH_END_FLAG,		MT_CH_END_FLAG,		},
	};

	CHANNEL_TO_ROUGH_DATA		MtSig[] =
	{
		{MT_CH_DC_VOLT,				MPPT_DC_VOLTAGE,			},
		{MT_CH_ACTUAL_CURR,			MPPT_ACTUAL_CURRENT,		},	
		{MT_CH_CURR_LMT,			MPPT_CURRENT_LIMIT,			},
		{MT_CH_TEMPERATURE,			MPPT_TEMPERATURE,			},
		{MT_CH_AC_VOLT,				MPPT_AC_VOLTAGE,			},
		{MT_CH_INPUTCURRENT,			MPPT_CURRENT_FOR_INPUT,		},
		{MT_CH_SERIAL_NO_LOW,			MPPT_SERIEL_NO_LOW,				},
		{MT_CH_HIGH_SERIAL_NO,			MPPT_HIGH_SN_REPORT/*MPPT_SERIEL_NO_HIGH*/,		},
		{MT_CH_INTERRUPT_ST,			MPPT_INTERRUPT_ST,			},
		{MT_CH_DAY_NIGHT_ST,			MPPT_DAYNIGHT_STATUS,			},
		{MT_CH_OVER_VOLT_ST,			MPPT_DC_OVER_VOLTAGE,		},
		{MT_CH_FAULT_ST,			MPPT_RECTFIER_FAULT,		},
		{MT_CH_PROTECTION_ST,			MPPT_PROTECTION,			},
		{MT_CH_FAN_FAULT_ST,			MPPT_FAN_FAULT,				},
		{MT_CH_EEPROM_FAULT_ST,			MPPT_EEPROM_FAULT,			},
		{MT_CH_VALID_RATED_CURR,		MPPT_RATED_CURR,			},
		{MT_CH_EFFICIENCY_NO_ST,		MPPT_EFFICIENCY_NO,			},
		{MT_CH_POWER_LMT_ST,			MPPT_POWER_LIMITED,			},
		{MT_CH_DC_ON_OFF_ST,			MPPT_DC_ON_OFF_STATUS,		},
		{MT_CH_FAN_FULL_SPEED_ST,		MPPT_FAN_FULL_SPEED,		},
		{MT_CH_WALKIN_ST,			MPPT_WALKIN_STATUS,			},
		{MT_CH_AC_ON_OFF_ST,			MPPT_AC_ON_OFF_STATUS,		},
		{MT_CH_SHARE_CURR_ST,			MPPT_SHARE_CURR_STATUS,		},
		{MT_CH_OVER_TEMP_ST,			MPPT_OVER_TEMP_STATUS,		},
		{MT_CH_VER_NO,				MPPT_VERSION_NO,			},
		{MT_CH_RUN_TIME,			MPPT_TOTAL_RUN_TIME,		},

		{MT_CH_POS_NO,				MPPT_POSITION_NO,			},
		{MT_CH_BARCODE1,			MPPT_BARCODE1,				},
		{MT_CH_BARCODE2,			MPPT_BARCODE2,				},
		{MT_CH_BARCODE3,			MPPT_BARCODE3,				},
		{MT_CH_BARCODE4,			MPPT_BARCODE4,				},
		{MT_CH_EXIST_STATE,			MPPT_EXIST_ST,				},
		{MT_CH_SEVERE_CURR_IMB,			MPPT_SEVERE_CURR_IMBALANCE,	},
		{MT_CH_CURR_IMB,			MPPT_CURR_IMBALANCE,		},
		
		{MT_CH_INPUT_POWER,			MPPT_INPUT_POWER,		},
		{MT_CH_OUTPUT_POWER,			MPPT_OUTPUT_POWER,		},
		{MT_CH_INPUT_NOT_DC,			MPPT_INPUT_NOT_DC,},

		{MT_CH_END_FLAG,			MT_CH_END_FLAG,				},
	};

	i = 0;

	
	while(MtGroupSig[i].iChannel != MT_CH_END_FLAG)
	{
		//printf("Stuff Group iChannel = %d, Real Channel = %d, lValue = %d, fValue = %.02f...\n",
		//	MtGroupSig[i].iChannel,
		//	MPPT_G_SIG_START_CHANNEL + MtGroupSig[i].iRoughData,
		//	g_CanData.aRoughDataGroup[MtGroupSig[i].iRoughData].iValue,
		//	g_CanData.aRoughDataGroup[MtGroupSig[i].iRoughData].fValue);
		
		EnumProc(MPPT_G_SIG_START_CHANNEL + MtGroupSig[i].iChannel, 
			g_CanData.aRoughDataGroup[MtGroupSig[i].iRoughData].fValue, 
			lpvoid );
		i++;
	}

	
	//Stuff the channels by the order of position NO.
	//printf("iMpptNum = %d\n", iMpptNum);
	for(i = 0; i < iMpptNum; i++)
	{
		int		iAddr = GetMpptAddrBySeqNo(i);

		//g_CanData.aRoughDataMppt[iAddr][MPPT_SERIEL_NO_LOW].uiValue = g_CanData.aRoughDataMppt[iAddr][MPPT_SERIEL_NO_LOW].uiValue & 0x3FFFFFFF;
		j = 0;
		
		//Special for MPPT, if the input voltage  < 35V ,just clear to zero.
		if(g_CanData.aRoughDataMppt[iAddr][MPPT_AC_VOLTAGE].fValue < 35.0)	
		{
			g_CanData.aRoughDataMppt[iAddr][MPPT_AC_VOLTAGE].fValue = 0.0;
		}

		while(MtSig[j].iChannel != MT_CH_END_FLAG)
		{
			/*printf("Stuff unit: iAddr = %d, SigIdx = %d, iChannel = %d, lValue = %d, fValue = %.02f...\n",
			iAddr,
			MtSig[j].iRoughData,
			MPPT_UNIT_SIG_START_CHANNEL + i * MAX_CHN_NUM_PER_MPPT + MtSig[j].iChannel,
			g_CanData.aRoughDataMppt[iAddr][MtSig[j].iRoughData].iValue,
			g_CanData.aRoughDataMppt[iAddr][MtSig[j].iRoughData].fValue);*/

			EnumProc(MPPT_UNIT_SIG_START_CHANNEL + i * MAX_CHN_NUM_PER_MPPT + MtSig[j].iChannel, 
				g_CanData.aRoughDataMppt[iAddr][MtSig[j].iRoughData].fValue, 
				lpvoid );


			j++;
		}	
	}

	for(i = iMpptNum; i < MAX_NUM_MPPT; i++)
	{
		/*printf("Stuff unit: Mppt %d,  MPPT_EXIST_ST = %d...\n",
		i,
		g_CanData.aRoughDataMppt[iMpptNum][MPPT_EXIST_ST].iValue);*/

		EnumProc(MPPT_UNIT_SIG_START_CHANNEL + i * MAX_CHN_NUM_PER_MPPT + MT_CH_EXIST_STATE, 
			g_CanData.aRoughDataMppt[iMpptNum][MPPT_EXIST_ST].fValue, 
			lpvoid );
	}


	return;
}

BOOL	MtCheckConvertExist()
{
	int			iAddr = 0;
	SIG_ENUM	emRst = 10;//To avoid C++ test warning.

	emRst = MpptSampleCmd00(iAddr);

	if(CAN_MPPT_REALLOCATE == emRst)
	{
		//printf("\n	CtCheckConvertExist	if(CAN_CONVERT_REALLOCATE == emRst)	\n");
		return FALSE;
	}
	else if(CAN_SAMPLE_FAIL == emRst)
	{
		//printf("\n	CtCheckConvertExist if(CAN_SAMPLE_FAIL == emRst)	\n");
		return FALSE;
	}
	else if (CAN_SAMPLE_OK == emRst)
	{
		//printf("\n	CtCheckConvertExist	if (CAN_SAMPLE_OK == emRst)	\n");
		return TRUE;
	}
	else
	{
		//printf("\n	CtCheckConvertExist	else	\n");
		return FALSE;
	}
}

VOID Mppt_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI)
{
	UNUSED(hComm);
	INT32	iTemp;
	INT32	i,iAddr;
	BYTE	byTempBuf[4];
	UINT	uiTemp;
	UINT	uiTemp11;
	UINT	uiTemp12;

	PRODUCT_INFO *				pInfo;
	pInfo						= (PRODUCT_INFO*)pPI;
#define		BARCODE_FF_OFST			 0
#define		BARCODE_YY_OFST			 2
#define		BARCODE_MM_OFST			 4	
#define		BARCODE_WELLWELL_OFST	 6//##
#define		BARCODE_VV_OFST			 1//##

	//printf("\n	########		IN	Convert_	GetProdctInfo	########	\n");
	SIG_ENUM sigState = GetEnumSigValue(MT_SYSTEM_EQUIPID, 
		SIG_TYPE_SETTING,
		MT_MPPT_SETTING_MODE,
		"CAN_SAMP");


	//printf("\n	########		IN	Convert_	GetProdctInfo	########	\n");
	int kk;
	for(iAddr = 0; iAddr < MAX_NUM_MPPT; iAddr++)
	{
		if ((CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataMppt[iAddr][MPPT_SERIEL_NO_LOW].iValue)
			&&(CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataMppt[iAddr][MPPT_VERSION_NO].iValue)
			&&(CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataMppt[iAddr][MPPT_BARCODE1].iValue)
			|| !sigState)
		{
			pInfo->bSigModelUsed = FALSE;
			pInfo++;
			continue;
		}
		else
		{
			pInfo->bSigModelUsed = TRUE;
		}
		kk = GetMpptAddrBySeqNo(iAddr);
		//printf("\n	######## IN Convert addr=%d  VERSION=%2x CODE1=%2x CODE2=%2x CODE3=%2x CODE4=%2x \n",
		//	iAddr,
		//	
		//	g_CanData.aRoughDataConvert[kk][MPPT_VERSION_NO].uiValue,
		//	g_CanData.aRoughDataConvert[kk][MPPT_BARCODE1].uiValue,
		//	g_CanData.aRoughDataConvert[kk][MPPT_BARCODE2].uiValue,
		//	g_CanData.aRoughDataConvert[kk][MPPT_BARCODE3].uiValue,
		//	g_CanData.aRoughDataConvert[kk][MPPT_BARCODE4].uiValue);

		//1.������ ��ʹ��
		//g_CanData.aRoughDataConvert[kk][CONVERT_FEATURE].uiValue

		//2.���ŵ�λ	SERIAL_NO_LOW=580005dc
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_SERIEL_NO_LOW].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_SERIEL_NO_LOW].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_SERIEL_NO_LOW].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_SERIEL_NO_LOW].uiValue);

		//��
		iTemp = byTempBuf[0] & 0x1f;//year
		snprintf(pInfo->szSerialNumber + BARCODE_YY_OFST, 3, "%d%d",iTemp/10,iTemp%10);
		//��
		iTemp = byTempBuf[1] & 0x0f;
		snprintf(pInfo->szSerialNumber + BARCODE_MM_OFST, 3, "%d%d",iTemp/10,iTemp%10);	
		//���ŵ�λ�ĵ�16�ֽ� ��Ӧ	##### ��������������
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		snprintf(pInfo->szSerialNumber + BARCODE_WELLWELL_OFST,
			6,"%05ld",uiTemp);
		pInfo->szSerialNumber[11] = '\0';//end		

		//3.�汾��VERSION=ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_VERSION_NO].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_VERSION_NO].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_VERSION_NO].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_VERSION_NO].uiValue);

		//ģ��汾��
		//H		A00��ʽ�����ֽڰ�A=0 B=1���ƣ�����A=65
		byTempBuf[0] = 0x3f & byTempBuf[0];
		uiTemp = byTempBuf[0] + 65;
		pInfo->szHWVersion[0] = uiTemp;

		//VV 00����ʵ����ֵ������ֽ�
		snprintf(pInfo->szHWVersion + BARCODE_VV_OFST, 3,
			"%d%d",byTempBuf[1]/10, byTempBuf[1]%10);
		pInfo->szHWVersion[4] = '\0';//end

		//����汾		��������汾Ϊ1.30,��������Ϊ�����ֽ�������130
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		uiTemp11 = (uiTemp - (uiTemp/100)*100)/10;
		uiTemp12 = (uiTemp - (uiTemp/100)*100)%10;
		snprintf(pInfo->szSWVersion, 5,
			"%d.%d%d",uiTemp/100, uiTemp11,uiTemp12);
		pInfo->szSWVersion[5] = '\0';//end

		//3.CODE1	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_BARCODE1].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_BARCODE1].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_BARCODE1].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_BARCODE1].uiValue);

		//FF ��ASCII���ʾ,ռ��CODE1 �� CODE2
		pInfo->szSerialNumber[0] = byTempBuf[0];
		pInfo->szSerialNumber[1] = byTempBuf[1];
		//T ��ʾ��Ʒ����,ASCIIռ��CODE12
		pInfo->szPartNumber[0] = byTempBuf[2];//1
		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[1] = byTempBuf[3];//S CODE13

		//3.CODE2	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_BARCODE2].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_BARCODE2].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_BARCODE2].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_BARCODE2].uiValue);

		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[2] = byTempBuf[0];//M CODE14
		pInfo->szPartNumber[3] = byTempBuf[1];//D CODE15
		pInfo->szPartNumber[4] = byTempBuf[2];//U CODE16
		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[5] = byTempBuf[3];//+ CODE17
		}
		else
		{
			pInfo->szPartNumber[5] = '\0';
		}		

		//3.CODE3	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_BARCODE3].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_BARCODE3].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_BARCODE3].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_BARCODE3].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[6] = byTempBuf[0];//CODE18
		}
		else
		{
			pInfo->szPartNumber[6] = '\0';//CODE18
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[7] = byTempBuf[1];//CODE19
		}
		else
		{
			pInfo->szPartNumber[7] = '\0';//CODE19
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[8] = byTempBuf[2];//CODE20
		}
		else
		{
			pInfo->szPartNumber[8] = '\0';//CODE20
		}

		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[9] = byTempBuf[3];//CODE21
		}
		else
		{
			pInfo->szPartNumber[9] = '\0';//CODE21
		}


		//3.CODE4		ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_BARCODE4].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_BARCODE4].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_BARCODE4].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataMppt[kk][MPPT_BARCODE4].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[10] = byTempBuf[0];//CODE22
		}
		else
		{
			pInfo->szPartNumber[10] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[11] = byTempBuf[1];//CODE23
		}
		else
		{
			pInfo->szPartNumber[11] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[12] = byTempBuf[2];//CODE24
		}
		else
		{
			pInfo->szPartNumber[12] = '\0';
		}

		pInfo->szPartNumber[13] = '\0';//CODE25

		//printf("\nCovert bSigModelUsed:%d\n", pInfo->bSigModelUsed);
		//printf("\nCovert PartNumber:%s\n", pInfo->szPartNumber);//1smdu+
		//printf("\nCnCovert	version:%s\n", pInfo->szSWVersion); //1.20
		//printf("\nnCovert version:%s\n", pInfo->szHWVersion); //A00
		//printf("\nnCovert Serial Number:%s\n", pInfo->szSerialNumber);//03100500006

		pInfo++;
	}
	//printf("\n	########		END		Convert_	GetProdctInfo	########	\n");

}
static BOOL	MpptAnalyseSerialNo(int iAddr)
{
	int					i;
	UINT				uiType;

	int iHiSn = g_CanData.aRoughDataMppt[iAddr][MPPT_SERIEL_NO_HIGH].iValue;
	int iLowSn = g_CanData.aRoughDataMppt[iAddr][MPPT_SERIEL_NO_LOW].iValue;
	UINT uiHiSn = g_CanData.aRoughDataMppt[iAddr][MPPT_SERIEL_NO_HIGH].uiValue;
	UINT uiLowSn = g_CanData.aRoughDataMppt[iAddr][MPPT_SERIEL_NO_LOW].uiValue;

	if((CAN_SAMP_INVALID_VALUE != iHiSn)
		&& (CAN_SAMP_INVALID_VALUE != iLowSn))
	{
		uiType = ((uiHiSn >> 4) & 0x000f) * 32 
			+ ((uiLowSn >> 29) & 0x0001) * 16 
			+ ((uiLowSn >> 20) & 0x000f);

		g_CanData.aRoughDataMppt[iAddr][MPPT_TYPE_NO].iValue = uiType;

		g_CanData.aRoughDataMppt[iAddr][MPPT_EFFICIENCY_NO].iValue = 0;//��ģ��һ����Ч�ʶ��ǳ�ʼ��
		if(uiType == MPPT48V50A) //48V50A
		{
			g_CanData.aRoughDataMppt[iAddr][MPPT_RATED_VOLT].fValue = 48.0;
			g_CanData.aRoughDataMppt[iAddr][MPPT_RATED_CURR].fValue = 50.0;

		}
		else if(uiType == MPPT336V)//336V35A
		{
			g_CanData.aRoughDataMppt[iAddr][MPPT_RATED_VOLT].fValue = 336.0;
			g_CanData.aRoughDataMppt[iAddr][MPPT_RATED_CURR].fValue = 35.0;
		}
		else if(uiType == MPPT48V35A)//48V35A
		{
			g_CanData.aRoughDataMppt[iAddr][MPPT_RATED_VOLT].fValue = 48.0;
			g_CanData.aRoughDataMppt[iAddr][MPPT_RATED_CURR].fValue = 35.0;
		}
		else if(uiType == MPPT400V)//400V37.5
		{
			g_CanData.aRoughDataMppt[iAddr][MPPT_RATED_VOLT].fValue = 400.0;
			g_CanData.aRoughDataMppt[iAddr][MPPT_RATED_CURR].fValue = 37.5;
		}
		else
		{
			return FALSE;
		}
		return TRUE;

	}
	return FALSE;
}
/*==========================================================================*
* FUNCTION : MpptUnpackInputPowerCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
*            int    iReadLen  : 
*            BYTE*  pbyRcvBuf : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:29
*==========================================================================*/
static BOOL MpptUnpackInputPowerCmd(int iAddr,
			    int iReadLen,
			    BYTE* pbyRcvBuf)
{
	int			i;
	UINT		uiSerialNoHi, uiSerialNoLow, uiVerNo, uiBarcode;

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;

		/*TRACE("**********i = %d  VAL_TYPE_H = %02X  VAL_TYPE_L = %02X  ",
		i,
		pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H],
		pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L]);*/

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
			!= PROTNO_MPPT_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
			!= (UINT)iAddr)
			|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H] != 0))
		{
			continue;
		}

		switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
		{
		case MT_VAL_TYPE_INPUT_POWER:
			/*if(iAddr == 0)
			{
			MtAnalyseFeature(pValueBuf);
			}*/	

			g_CanData.aRoughDataMppt[iAddr][MPPT_INPUT_POWER].fValue 
				= CAN_StringToFloat(pValueBuf);
			/*printf(" iFrameOrder = %d, MT_VAL_TYPE_R_FEATURE ok!!!\n", iFrameOrder);*/


			break;

		default:
			break;
		}
	}


	return TRUE;


}
/*==========================================================================*
* FUNCTION : MpptSampleInputPowerCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static SIG_ENUM : 
* COMMENTS : 
* CREATOR  : Marco Yang                DATE: 2008-07-19 17:30
*==========================================================================*/
static SIG_ENUM MpptSampleInputPowerCmd(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;
	FLOAT_STRING	unVal;

	unVal.ulValue = 0;
	PackAndSendMpptCmd(MSG_TYPE_RQST_BYTE,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		0,
		MT_VAL_TYPE_INPUT_POWER,
		unVal.abyValue);
	Sleep(CAN_SAMPLE_1ST_WAIT_FACTOR);
	if(CAN_MPPT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_CanData.CanCommInfo.abyRcvBuf, 
		&iReadLen1st))
	{
		return CAN_MPPT_REALLOCATE;
	}

	if((iReadLen1st / CAN_FRAME_LEN) 
		>= 1)
	{
		MpptUnpackInputPowerCmd(iAddr,
			iReadLen1st,
			g_CanData.CanCommInfo.abyRcvBuf);
	}
	else
	{
		return CAN_SAMPLE_FAIL;	
	}

	//TRACE("iAddr = %d, OK!!!\n", iAddr);

	return CAN_SAMPLE_OK;
}

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : Can_smdue.c
 *  CREATOR  : Fengel hu              DATE: 2016-08-30 
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/


#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <time.h>

#include "can_sampler_main.h"
#include "can_smdue.h"

extern CAN_SAMPLER_DATA	g_CanData;


/*==========================================================================*
* FUNCTION : IncSmdueIntrruptTimes
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Fengel                DATE: 2018-12-7 
*==========================================================================*/
static void IncSmdueIntrruptTimes(int iAddr)
{
	if(g_CanData.aRoughDataSmdue[iAddr][SMDUE_INTERRUPT_TIMES].iValue 
		< SMDUE_MAX_INTERRUPT_TIMES + 2)
	{
		g_CanData.aRoughDataSmdue[iAddr][SMDUE_INTERRUPT_TIMES].iValue++;
	}

	if(g_CanData.aRoughDataSmdue[iAddr][SMDUE_INTERRUPT_TIMES].iValue
		>= (SMDUE_MAX_INTERRUPT_TIMES + 1))
	{
		if(g_CanData.aRoughDataGroup[GROUP_SMDUE_ALL_NORESPONSE].iValue)
		{
			if( 1 == g_CanData.aRoughDataGroup[GROUP_SYS_CAN_INTERRUPT].iValue)//CAN_ST_INTERRUPT
			{
				g_CanData.aRoughDataSmdue[iAddr][SMDUE_INTERRUPT_ST].iValue 
					= SMDUE_COMM_ALL_INTERRUPT_ST;
			}
			else
			{
				g_CanData.aRoughDataSmdue[iAddr][SMDUE_INTERRUPT_ST].iValue 
					= SMDUE_COMM_INTERRUPT_ST;
			}
		}
		else
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_INTERRUPT_ST].iValue 
				= SMDUE_COMM_INTERRUPT_ST;
		}
	}
	else
	{
		g_CanData.aRoughDataSmdue[iAddr][SMDUE_INTERRUPT_ST].iValue 
			= SMDUE_COMM_NORMAL_ST;
	}

	return;
}

/*==========================================================================*
* FUNCTION : ClrSmdueIntrruptTimes
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iAddr : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Fengel                DATE: 2018-12-7 
*==========================================================================*/
static void ClrSmdueIntrruptTimes(int iAddr)
{
	g_CanData.aRoughDataSmdue[iAddr][SMDUE_INTERRUPT_TIMES].iValue = 0;
	g_CanData.aRoughDataSmdue[iAddr][SMDUE_INTERRUPT_ST].iValue 
		= SMDUE_COMM_NORMAL_ST;

	return;
}

/*==========================================================================*
* FUNCTION : CAN_AlmModeToString
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: float  fInput    : 
*            char*  strOutput : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  :Fengel                DATE: 2018-12-7 
*==========================================================================*/
void CAN_AlmModeToString(int iInput, BYTE* strOutput)
{
	int iTemp = iInput;

	strOutput[0] = (iTemp >> 24) & 0xff;
	strOutput[1] = (iTemp >> 16) & 0xff;
	strOutput[2] = (iTemp >> 8) & 0xff;
	strOutput[3] = iTemp & 0xff;

	return;
}


/*==========================================================================*
* FUNCTION : PackAndSendSmdueCmd_AlarmMode
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: float  fInput    : 
*            char*  strOutput : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  :Fengel                DATE: 2018-12-7 
*==========================================================================*/
static void PackAndSendSmdueCmd_AlarmMode(UINT uiMsgType,
							   UINT uiDestAddr,
							   UINT uiCmdType,
							   UINT uiValueTypeH,
							   UINT uiValueTypeL,
							   int  iParam,
							   UINT uiCANPort)
{
	BYTE*	pbySendBuf = g_CanData.CanCommInfo.abySendBuf;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_SMDUE_CONTROLLER,
		(uiDestAddr + SMDUE_ADDR_OFFSET), 
		CAN_SELF_ADDR,
		CAN_ERR_CODE,
		CAN_FRAME_DATA_LEN,
		uiCmdType,
		pbySendBuf);

	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	if (  (MSG_TYPE_RQST_SETTINGS == uiMsgType)  &&  (  0x01 == uiValueTypeH ) && ( SMDUE_FUSE_ALARM_MODE_VALUE_TYPE == uiValueTypeL) )
	{
		CAN_AlmModeToString(iParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);
	}
	else if(  (MSG_TYPE_RQST_SETTINGS == uiMsgType)  &&  (  0x0 == uiValueTypeH ) && ( SMDUE_CHANNEL_DEFINE == uiValueTypeL) )
	{
		CAN_AlmModeToString(iParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);
	}
	else
	{
		return;	
	}

	if(uiCANPort == SMDUEonCAN1)
	{
		write(g_CanData.CanCommInfo.iCanHandle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);
	}
	else if(uiCANPort == SMDUEonCAN2)
	{
		write(g_CanData.CanCommInfo.iCan2Handle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);
	}
	/*else
	{
		write(g_CanData.CanCommInfo.iCanHandle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);
		write(g_CanData.CanCommInfo.iCan2Handle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);
	}*/

	return;
}


/*==========================================================================*
* FUNCTION : PackAndSendSmdueCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: float  fInput    : 
*            char*  strOutput : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  :Fengel                DATE: 2018-12-7 
*==========================================================================*/
static void PackAndSendSmdueCmd(UINT uiMsgType,
							   UINT uiDestAddr,
							   UINT uiCmdType,
							   UINT uiValueTypeH,
							   UINT uiValueTypeL,
							   float fParam,
							   UINT uiCANPort)
{
	BYTE*	pbySendBuf = g_CanData.CanCommInfo.abySendBuf;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_SMDUE_CONTROLLER,
		(uiDestAddr + SMDUE_ADDR_OFFSET), 
		CAN_SELF_ADDR,
		CAN_ERR_CODE,
		CAN_FRAME_DATA_LEN,
		uiCmdType,
		pbySendBuf);

	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	CAN_FloatToString(fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);	
	

	if(uiCANPort == SMDUEonCAN1)
	{
		write(g_CanData.CanCommInfo.iCanHandle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);
	}
	else if(uiCANPort == SMDUEonCAN2)
	{
		write(g_CanData.CanCommInfo.iCan2Handle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);
	}
	/*else
	{
		write(g_CanData.CanCommInfo.iCanHandle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);
		write(g_CanData.CanCommInfo.iCan2Handle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);
	}*/

	return;
}



/*==========================================================================*
* FUNCTION : SmdueUnpackShuntCoeff
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
*            int    iReadLen  : 
*            BYTE*  pbyRcvBuf : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : fengel hu                DATE: 2018-12-7 
*==========================================================================*/
static BOOL SmdueUnpackShuntCoeff(int iAddr,
						  int iReadLen,
						  BYTE* pbyRcvBuf)
{
	int	i, iFrameNum = 0;
	BYTE	bValueType;
	int	iTempAddr = 0;

	iTempAddr = iAddr;
	

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
				!= PROTNO_SMDUE_CONTROLLER)
				|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN) != ((UINT)iTempAddr + SMDUE_ADDR_OFFSET))
				|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H] != 1))
		{
			printf("SMDUE unpack ShuntCoeff:  There some err  in unpack package~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n ");			
			continue;
		}

		bValueType = *(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L);
		
		if( (bValueType == SMDUE_VOLT_SHUNT_COEFF1) ||(bValueType == SMDUE_CURRENT_SHUNT_COEFF1) )
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_VOLT_SHUNT_COEFF_1 + bValueType - SMDUE_VOLT_SHUNT_COEFF1].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++; 
			//if(bValueType == 1)
			//	printf("g_CanData.aRoughDataSmdue[iAddr][SMDUE_CURRENT_SHUNT_COEFF_1].fValue = %f \n",g_CanData.aRoughDataSmdue[iAddr][SMDUE_CURRENT_SHUNT_COEFF_1].fValue);

		}   
		else if((bValueType == SMDUE_VOLT_SHUNT_COEFF2)  ||  (bValueType == SMDUE_CURRENT_SHUNT_COEFF2 ))
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_VOLT_SHUNT_COEFF_2 + bValueType - SMDUE_VOLT_SHUNT_COEFF2].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		else if((bValueType == SMDUE_VOLT_SHUNT_COEFF3) ||  (bValueType == SMDUE_CURRENT_SHUNT_COEFF3 ))
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_VOLT_SHUNT_COEFF_3 + bValueType - SMDUE_VOLT_SHUNT_COEFF3].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		else if((bValueType == SMDUE_VOLT_SHUNT_COEFF4) ||  (bValueType == SMDUE_CURRENT_SHUNT_COEFF4))
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_VOLT_SHUNT_COEFF_4 + bValueType - SMDUE_VOLT_SHUNT_COEFF4].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		else if((bValueType == SMDUE_VOLT_SHUNT_COEFF5) ||  (bValueType == SMDUE_CURRENT_SHUNT_COEFF5 ))
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_VOLT_SHUNT_COEFF_5 + bValueType - SMDUE_VOLT_SHUNT_COEFF5].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		else if((bValueType == SMDUE_VOLT_SHUNT_COEFF6) ||  (bValueType == SMDUE_CURRENT_SHUNT_COEFF6 ))
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_VOLT_SHUNT_COEFF_6 + bValueType - SMDUE_VOLT_SHUNT_COEFF6].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		else if((bValueType == SMDUE_VOLT_SHUNT_COEFF7) ||  (bValueType == SMDUE_CURRENT_SHUNT_COEFF7 ))
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_VOLT_SHUNT_COEFF_7 + bValueType - SMDUE_VOLT_SHUNT_COEFF7].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		else if((bValueType == SMDUE_VOLT_SHUNT_COEFF8) ||  (bValueType == SMDUE_CURRENT_SHUNT_COEFF8 ))
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_VOLT_SHUNT_COEFF_8 + bValueType - SMDUE_VOLT_SHUNT_COEFF8].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		else if((bValueType == SMDUE_VOLT_SHUNT_COEFF9) ||  (bValueType == SMDUE_CURRENT_SHUNT_COEFF9 ))
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_VOLT_SHUNT_COEFF_9 + bValueType - SMDUE_VOLT_SHUNT_COEFF9].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		else if((bValueType == SMDUE_VOLT_SHUNT_COEFF10) ||  (bValueType == SMDUE_CURRENT_SHUNT_COEFF10 ))
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_VOLT_SHUNT_COEFF_10 + bValueType - SMDUE_VOLT_SHUNT_COEFF10].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		else
		{
			iFrameNum++;
		}
	}

/*
	if(iFrameNum == SMDUE_SHUNTCOEFF_FRAMES_NUM)
		return TRUE;
	else
		return FALSE;
*/

	if(iFrameNum == SMDUE_SHUNTCOEFF_FRAMES_NUM)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*==========================================================================*
* FUNCTION : SmdueSampleShuntCoeff
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
*            int    iReadLen  : 
*            BYTE*  pbyRcvBuf : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : fengel hu                DATE: 2018-12-7 
*==========================================================================*/
static SIG_ENUM SmdueSampleShuntCoeff(int iAddr, UINT uiMsgType, UINT uiCANPort)
{
	int		iReadLen1st = 0;
	int		iTempAddr = 0;
	int           i = 0;
	
	iTempAddr = iAddr;
	

	for(i = 0;  i < MAX_CHANEL_NUM_SMDUE_SHUNT_COEFF ; i++ )
	{

		PackAndSendSmdueCmd(uiMsgType,
			(UINT)iTempAddr,
			CAN_CMD_TYPE_P2P,
			//(UINT)GetSmSetByteCmd00(iAddr),
			0,
			i+1,//fengel   modify
			0.0,
			uiCANPort);
		
		Sleep(SMDUE_SHUNTCOEFF_FRAMES_NUM * CAN_SAMPLE_1ST_WAIT_FACTOR * 2*2);
		

		CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
				g_CanData.CanCommInfo.abyRcvBuf, 
				&iReadLen1st);
		
		if(iReadLen1st >= (SMDUE_CMDGETSHUNTCOEFF_FRAMES_NUM * CAN_FRAME_LEN))
		{
			if(SmdueUnpackShuntCoeff(iAddr,
				iReadLen1st,
				g_CanData.CanCommInfo.abyRcvBuf))
			{
				ClrSmdueIntrruptTimes(iAddr);
			}
			else
			{			
				//IncSmdueIntrruptTimes(iAddr);
				return CAN_SAMPLE_FAIL;		
			}
		}
		else
		{		
			//IncSmdueIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;		
		}

	}	
	


	return CAN_SAMPLE_OK;
}

/*==========================================================================*
* FUNCTION : ShuntCoeffBackwardRead
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
*            int    iReadLen  : 
*            BYTE*  pbyRcvBuf : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : fengel hu                DATE: 2018-12-7 
*==========================================================================*/
static void ShuntCoeffBackwardRead(int iAddr, UINT uiMsgType, UINT uiCANPort)
{
	if( g_CanData.iSmdueParamUnifyConter[iAddr] < SMDUE_PARAM_UNIFY_AT_MOST_TIMES )
	{
		SmdueSampleShuntCoeff(iAddr, uiMsgType, uiCANPort);
	}

	return;
}

/*==========================================================================*
* FUNCTION : SmdueUnpackFuseAlarmMode
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
*            int    iReadLen  : 
*            BYTE*  pbyRcvBuf : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : fengel hu                DATE: 2018-12-7 
*==========================================================================*/
static BOOL SmdueUnpackFuseAlarmMode(int iAddr, int iReadLen,BYTE* pbyRcvBuf)
{
	int	i, iFrameNum = 0;
	BYTE	bValueType;
	int	iTempAddr = 0;
	UINT	uiValue;


	iTempAddr = iAddr;
	
	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
				!= PROTNO_SMDUE_CONTROLLER)
				|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN) != ((UINT)iTempAddr + SMDUE_ADDR_OFFSET))
				|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H] != 1))
		{
			printf("SmdueUnpackFuseAlarmMode:  There some err  in unpack package~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n ");			
			continue;
		}

		bValueType = *(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L);
		
		if(  SMDUE_FUSE_ALARM_MODE_VALUE_TYPE == bValueType  )
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_FUSE_ALARM_MODE].iValue 
					=  CAN_StringToUint(pValueBuf);
			iFrameNum++;
		}
		else
		{
			iFrameNum++;
		}
	}

	if(iFrameNum == SMDUE_FUSE_ALARM_MODE_FRAMES_NUM)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
	
	return;
}


/*==========================================================================*
* FUNCTION : SmdueSampleFuseAlarmMode
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2018-12-7 
*==========================================================================*/
static SIG_ENUM SmdueSampleFuseAlarmMode(int iAddr, UINT uiMsgType, UINT uiCANPort)
{
	int		iReadLen1st = 0;
	int		iTempAddr = 0;

	iTempAddr = iAddr;
	
	PackAndSendSmdueCmd(uiMsgType,
		(UINT)iTempAddr,
		CAN_CMD_TYPE_P2P,
		//(UINT)GetSmSetByteCmd00(iAddr),
		0x01,
		SMDUE_FUSE_ALARM_MODE_VALUE_TYPE,
		0.0,
		uiCANPort);


	Sleep(CAN_SAMPLE_2ND_WAIT_TIME);

	
	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
			g_CanData.CanCommInfo.abyRcvBuf, 
			&iReadLen1st);


	if(iReadLen1st >= (SMDUE_FUSE_ALARM_MODE_FRAMES_NUM * CAN_FRAME_LEN))
	{
		if(SmdueUnpackFuseAlarmMode(iAddr,
			iReadLen1st,
			g_CanData.CanCommInfo.abyRcvBuf))
		{
			ClrSmdueIntrruptTimes(iAddr);
		}
		else
		{
			//IncSmdueIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;		
		}
	}
	else
	{	
		//IncSmdueIntrruptTimes(iAddr);
		return CAN_SAMPLE_FAIL;		
	}

	return CAN_SAMPLE_OK;
}


/*==========================================================================*
* FUNCTION : SmdueFuseAlarmModeBackwardRead
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2018-12-7 
*==========================================================================*/
static void SmdueFuseAlarmModeBackwardRead(int iAddr, UINT uiMsgType, UINT uiCANPort)
{
	if( g_CanData.iSmdueParamUnifyConter[iAddr] < SMDUE_PARAM_UNIFY_AT_MOST_TIMES )
	{
		SmdueSampleFuseAlarmMode(iAddr,uiMsgType,uiCANPort);
	}

	return;
}


/*==========================================================================*
* FUNCTION : SmdueUnpackGetData
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
*            int    iReadLen  : 
*            BYTE*  pbyRcvBuf : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : fengel hu                DATE: 2018-12-7 
*==========================================================================*/
static BOOL SmdueUnpackGetData(int iAddr,
						  int iReadLen,
						  BYTE* pbyRcvBuf)
{
	int	i, iFrameNum = 0;
	BYTE	bValueType;
	int	iTempAddr = 0;
	UINT	uiValue;

	iTempAddr = iAddr;
	

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
				!= PROTNO_SMDUE_CONTROLLER)
				|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN) != ((UINT)iTempAddr + SMDUE_ADDR_OFFSET))
				|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H] != 0))
		{
			printf("SMDUE unpack GetData:  There some err  in unpack package~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n ");			
			continue;
		}

		bValueType = *(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L);
		
		if(  SMDUE_CHANNEL_DEFINE == bValueType  )
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_AI_CHANNEL_TYPE].iValue 
					=  CAN_StringToUint(pValueBuf);
			iFrameNum++;
		}
		else if((bValueType >= SMDUE_SHUNT_CURRENT1) && (bValueType <= SMDUE_SHUNT_CURRENT10))
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_SHUNT_CURRENT_1 + bValueType - SMDUE_SHUNT_CURRENT1].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		else if((bValueType >= SMDUE_HALL_CURRENT1) && (bValueType <= SMDUE_HALL_CURRENT10))
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_HALL_CURRENT_1 + bValueType - SMDUE_HALL_CURRENT1].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		else if((bValueType >= SMDUE_VOLT1) && (bValueType <= SMDUE_VOLT10))
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_VOLT_1 + bValueType - SMDUE_VOLT1].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		else if((bValueType >= SMDUE_TEMP1) && (bValueType <= SMDUE_TEMP10))
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_TEMP_1 + bValueType - SMDUE_TEMP1].fValue 
					= CAN_StringToFloat(pValueBuf);
			//printf("g_CanData.aRoughDataSmdue[%d][SMDUE_TEMP_1].fValue=%f~~\n",iAddr,g_CanData.aRoughDataSmdue[iAddr][SMDUE_TEMP_1].fValue);
			iFrameNum++;
		}
		else
		{
			iFrameNum++;
		}
	}

	if(iFrameNum == SMDUE_GETDATA_FRAMES_NUM)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*==========================================================================*
* FUNCTION : SmdueSampleGetData
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2018-12-7
*==========================================================================*/
static SIG_ENUM SmdueSampleGetData(int iAddr, UINT uiMsgType, UINT uiCANPort)
{
	int		iReadLen1st = 0;
	int		iTempAddr = 0;
	static BOOL s_bFirstRunGetData = TRUE;


	iTempAddr = iAddr;
	
	PackAndSendSmdueCmd(uiMsgType,
		(UINT)iTempAddr,
		CAN_CMD_TYPE_P2P,
		//(UINT)GetSmSetByteCmd00(iAddr),
		0,
		0,
		0.0,
		uiCANPort);


	Sleep(SMDUE_GETDATA_FRAMES_NUM * CAN_SAMPLE_1ST_WAIT_FACTOR * 2);
	

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
			g_CanData.CanCommInfo.abyRcvBuf, 
			&iReadLen1st);

	if( s_bFirstRunGetData )
	{
		if ( iReadLen1st < (SMDUE_GETDATA_FRAMES_NUM * CAN_FRAME_LEN)  )
		{
			Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
			CAN_ReadFrames(MAX_FRAMES_IN_BUFF, g_CanData.CanCommInfo.abyRcvBuf, &iReadLen1st);


			if ( iReadLen1st < (SMDUE_GETDATA_FRAMES_NUM * CAN_FRAME_LEN)  )
			{
				PackAndSendSmdueCmd(uiMsgType,(UINT)iTempAddr,CAN_CMD_TYPE_P2P,0,0,0.0,uiCANPort);
				Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
				CAN_ReadFrames(MAX_FRAMES_IN_BUFF, g_CanData.CanCommInfo.abyRcvBuf, &iReadLen1st);


				if ( iReadLen1st < (SMDUE_GETDATA_FRAMES_NUM * CAN_FRAME_LEN) )
				{
					PackAndSendSmdueCmd(uiMsgType,(UINT)iTempAddr,CAN_CMD_TYPE_P2P,0,0,0.0,uiCANPort);
					Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
					CAN_ReadFrames(MAX_FRAMES_IN_BUFF, g_CanData.CanCommInfo.abyRcvBuf, &iReadLen1st);
				}
			}
		}
	}
	s_bFirstRunGetData = FALSE;

	if(iReadLen1st >= (SMDUE_GETDATA_FRAMES_NUM * CAN_FRAME_LEN))
	{
		if(SmdueUnpackGetData(iAddr,
			iReadLen1st,
			g_CanData.CanCommInfo.abyRcvBuf))
		{
			ClrSmdueIntrruptTimes(iAddr);
		}
		else
		{	
			IncSmdueIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;		
		}
	}
	else
	{
		IncSmdueIntrruptTimes(iAddr);
		return CAN_SAMPLE_FAIL;		
	}

	return CAN_SAMPLE_OK;
}


/*==========================================================================*
* FUNCTION : SmdueUnpackVoltAlm
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
*            int    iReadLen  : 
*            BYTE*  pbyRcvBuf : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : fengel hu                DATE: 2018-12-7 
*==========================================================================*/
static BOOL SmdueUnpackVoltAlm(int iAddr,
						  int iReadLen,
						  BYTE* pbyRcvBuf)
{
	int	i, iFrameNum = 0;
	BYTE	bValueType;
	int	iTempAddr = 0;

	iTempAddr = iAddr;
	

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
				!= PROTNO_SMDUE_CONTROLLER)
				|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN) != ((UINT)iTempAddr + SMDUE_ADDR_OFFSET))
				|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H] != 0))
		{
			printf("SMDUE unpack Alm:  There some err  in unpack package~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n ");	
			/*
			if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) != PROTNO_SMDUE_CONTROLLER))
			{
				printf("SMDUE unpack Alm:  here--1~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n ");	
			}
			if((CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN) != ((UINT)iTempAddr + SMDUE_ADDR_OFFSET)))
			{
				printf("SMDUE unpack Alm:  here--2~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n ");	
			}
			if((pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H] != 0))
			{
				printf("SMDUE unpack Alm:  here--3~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n ");	
			}*/
			
			continue;
		}

		bValueType = *(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L);
		
		if( bValueType == SMDUE_BUSBAR_VOLT )
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_BUS_BAR_VOLT].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		else if((bValueType >= PANEL_VOLT1) && (bValueType <= PANEL_VOLT10))
		{
			g_CanData.aRoughDataSmdue[iAddr][PANEL_VOLT_1 + bValueType - PANEL_VOLT1].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		else if(bValueType == SMDUE_ALARM)
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_ALL_ALRM].iValue 
					=  CAN_StringToUint(pValueBuf);
			iFrameNum++;
		}
		else  
		{
			iFrameNum++;
		}
	}

	if(iFrameNum == SMDUE_VOLTANDALARM_FRAMES_NUM)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*==========================================================================*
* FUNCTION : SmdueSampleVoltAlm
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2018-12-7
*==========================================================================*/
static SIG_ENUM SmdueSampleVoltAlm(int iAddr, UINT uiMsgType, UINT uiCANPort)
{
	int		iReadLen1st = 0;
	int		iTempAddr = 0;
	static BOOL s_bFirstRunAlarm = TRUE;



	iTempAddr = iAddr;
		
	PackAndSendSmdueCmd(uiMsgType,
		(UINT)iTempAddr,
		CAN_CMD_TYPE_P2P,
		//(UINT)GetSmSetByteCmd00(iAddr),
		0,
		0,//�Ѻ�������ȷ�ϣ�SMDUE���յ�00����󣬲������յ�֡��7-12�ֽ�
		0.0,
		uiCANPort);

	Sleep(SMDUE_VOLTANDALARM_FRAMES_NUM * CAN_SAMPLE_1ST_WAIT_FACTOR * 2);
	

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, g_CanData.CanCommInfo.abyRcvBuf, &iReadLen1st);


	if( s_bFirstRunAlarm )
	{
		if ( iReadLen1st < (SMDUE_VOLTANDALARM_FRAMES_NUM * CAN_FRAME_LEN)  )
		{
			Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
			CAN_ReadFrames(MAX_FRAMES_IN_BUFF, g_CanData.CanCommInfo.abyRcvBuf, &iReadLen1st);


			if ( iReadLen1st < (SMDUE_VOLTANDALARM_FRAMES_NUM * CAN_FRAME_LEN)  )
			{
				PackAndSendSmdueCmd(uiMsgType,(UINT)iTempAddr,CAN_CMD_TYPE_P2P,0,0,0.0,uiCANPort);
				Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
				CAN_ReadFrames(MAX_FRAMES_IN_BUFF, g_CanData.CanCommInfo.abyRcvBuf, &iReadLen1st);


				if ( iReadLen1st < (SMDUE_VOLTANDALARM_FRAMES_NUM * CAN_FRAME_LEN)  )
				{
					PackAndSendSmdueCmd(uiMsgType,(UINT)iTempAddr,CAN_CMD_TYPE_P2P,0,0,0.0,uiCANPort);
					Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
					CAN_ReadFrames(MAX_FRAMES_IN_BUFF, g_CanData.CanCommInfo.abyRcvBuf, &iReadLen1st);

				}
			}
		}
	}
	s_bFirstRunAlarm = FALSE;


	if(iReadLen1st >= (SMDUE_VOLTANDALARM_FRAMES_NUM * CAN_FRAME_LEN))
	{
		if(SmdueUnpackVoltAlm(iAddr,
			iReadLen1st,
			g_CanData.CanCommInfo.abyRcvBuf))
		{
			ClrSmdueIntrruptTimes(iAddr);
		}
		else
		{
			//IncSmdueIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;		
		}
	}
	else
	{
		//IncSmdueIntrruptTimes(iAddr);
		return CAN_SAMPLE_FAIL;		
	}

	return CAN_SAMPLE_OK;
}

/*==========================================================================*
* FUNCTION : SmdueCalFuseAlm
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
* COMMENTS : 
* CREATOR  : fengel hu                DATE: 2018-12-7 
*==========================================================================*/
static void SmdueCalFuseAlm(int iAddr)
{

#define	SMDUE_CHANEL_NUM	10
#define SMDUE_ALARM_FUSE_STATE_OFFSET	22

	int i =0;

	//printf("g_CanData.aRoughDataSmdue[7][SMDUE_ALL_ALRM].iValue = %#x~~~~~~~\n",g_CanData.aRoughDataSmdue[7][SMDUE_ALL_ALRM].iValue);
	 
	if( g_CanData.aRoughDataSmdue[iAddr][SMDUE_EXISTENCE].iValue == SMDUE_EQUIP_EXISTENT )	
	{
		for(i=0; i< SMDUE_CHANEL_NUM; i++)
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_FUSESTAT1+i].iValue = (g_CanData.aRoughDataSmdue[iAddr][SMDUE_ALL_ALRM].iValue >> (SMDUE_ALARM_FUSE_STATE_OFFSET+i)) & 0x01;
		}
	}
	else
	{
		for(i=0; i< SMDUE_CHANEL_NUM; i++)
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_FUSESTAT1+i].iValue = 0;
		}
	}

	return;
}


/*==========================================================================*
* FUNCTION : SmdueCalVoltAlm
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
* COMMENTS : 
* CREATOR  : fengel hu                DATE: 2018-12-7 
*==========================================================================*/
static void SmdueCalVoltAlm(int iAddr)
{

#define	SMDUE_CHANEL_NUM	10
#define SMDUE_ALARM_VOLTAGE_STATE_OFFSET	2


	int i =0;

	//printf("g_CanData.aRoughDataSmdue[7][SMDUE_ALL_ALRM].iValue = %#x~~~~~~~\n",g_CanData.aRoughDataSmdue[7][SMDUE_ALL_ALRM].iValue);
	 
	if( g_CanData.aRoughDataSmdue[iAddr][SMDUE_EXISTENCE].iValue == SMDUE_EQUIP_EXISTENT )	
	{
		g_CanData.aRoughDataSmdue[iAddr][SMDUE_BUS_OVER_VOLT_STA].iValue = (g_CanData.aRoughDataSmdue[iAddr][SMDUE_ALL_ALRM].iValue) & 0x01;
		g_CanData.aRoughDataSmdue[iAddr][SMDUE_BUS_UNDR_VOLT_STA].iValue = (g_CanData.aRoughDataSmdue[iAddr][SMDUE_ALL_ALRM].iValue >> 1) & 0x01;

		for(i=0; i< SMDUE_CHANEL_NUM*2; i++)
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_OVER_VOLT1_STA+i].iValue = (g_CanData.aRoughDataSmdue[iAddr][SMDUE_ALL_ALRM].iValue >> (SMDUE_ALARM_VOLTAGE_STATE_OFFSET+i)) & 0x01;
		}
	}
	else
	{
		g_CanData.aRoughDataSmdue[iAddr][SMDUE_BUS_OVER_VOLT_STA].iValue = 0;
		g_CanData.aRoughDataSmdue[iAddr][SMDUE_BUS_UNDR_VOLT_STA].iValue = 0;

		for(i=0; i< SMDUE_CHANEL_NUM*2; i++)
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_OVER_VOLT1_STA+i].iValue = 0;
		}
	}

	return;
}


/*==========================================================================*
* FUNCTION : SmdueUnpackBarcodeVer
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iAddr     : 
*            int    iReadLen  : 
*            BYTE*  pbyRcvBuf : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : fengel hu                DATE: 2018-12-7 
*==========================================================================*/
static BOOL SmdueUnpackBarcodeVer(int iAddr,
						  int iReadLen,
						  BYTE* pbyRcvBuf)
{
	int	i, iFrameNum = 0;
	BYTE	bValueType;
	int	iTempAddr = 0;
	UINT	uiValue;
	UINT	uiSerialNo;


	iTempAddr = iAddr;
	

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
			+ i * CAN_FRAME_LEN
			+ CAN_FRAME_OFFSET_VALUE;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
				!= PROTNO_SMDUE_CONTROLLER)
				|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN) != ((UINT)iTempAddr + SMDUE_ADDR_OFFSET))
				|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H] != 0))
		{
			printf("SMDUE unpack barcode:  There some err  in unpack package~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n ");
			continue;
		}

		bValueType = *(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L);


		if((bValueType >= SMDUE_BARCODE_1) && (bValueType <= SMDUE_BARCODE_4))
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_BARCODE1 + bValueType - SMDUE_BARCODE_1].iValue 
					= CAN_StringToUint(pValueBuf);
			iFrameNum++;
		}
		else if(bValueType == SMDUE_SERIALNO)
		{
			uiSerialNo = CAN_StringToUint(pValueBuf);
			//printf("SMDUE_SERIALNO ---------uiSerialNo = %d\n",uiSerialNo);
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_SERIAL_NO_LOW].iValue 
				= uiSerialNo;
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_SERIAL_NO_HIGH].iValue 
				= uiSerialNo >> 30;
			iFrameNum++;
		}
		else if(bValueType == SMDUE_VERSIOM)
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_VERSION_NO].iValue 
				= CAN_StringToUint(pValueBuf);
				//printf("SmdueUnpackBarcodeVer()-SMDUE_VERSION_NO:pValueBuf:%s,g_CanData.aRoughDataSmdue[iAddr][SMDUE_VERSION_NO].iValue=%d~~\n",pValueBuf,g_CanData.aRoughDataSmdue[iAddr][SMDUE_VERSION_NO].iValue);
			
			iFrameNum++;
		}
		else 
		{
			iFrameNum++;
		}
		
		
	}

	if(iFrameNum == SMDUE_BARCODEVERSION_FRAMES_NUM)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


/*==========================================================================*
* FUNCTION : SmdueSampleBarcodeVer
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2018-12-7 
*==========================================================================*/
static SIG_ENUM SmdueSampleBarcodeVer(int iAddr, UINT uiMsgType, UINT uiCANPort)
{
	int		iReadLen1st = 0;
	int		iTempAddr = 0;
	static BOOL   s_bFirstRunBarcode = TRUE;

	
	iTempAddr = iAddr;
	
	PackAndSendSmdueCmd(uiMsgType,
		(UINT)iTempAddr,
		CAN_CMD_TYPE_P2P,
		//(UINT)GetSmSetByteCmd00(iAddr),
		0,
		0,//�Ѻ�������ȷ�ϣ�SMDUE���յ�00����󣬲������յ�֡��7-12�ֽ�
		0.0,
		uiCANPort);

	Sleep(SMDUE_BARCODEVERSION_FRAMES_NUM * CAN_SAMPLE_1ST_WAIT_FACTOR * 2);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, g_CanData.CanCommInfo.abyRcvBuf, &iReadLen1st);


	if( s_bFirstRunBarcode )
	{
		if ( iReadLen1st < (SMDUE_BARCODEVERSION_FRAMES_NUM * CAN_FRAME_LEN)  )
		{
			printf("SmdueSampleBarcodeVer():iAddr=%d,--------------------------------1\n",iAddr);
			Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
			CAN_ReadFrames(MAX_FRAMES_IN_BUFF, g_CanData.CanCommInfo.abyRcvBuf, &iReadLen1st);
			if ( iReadLen1st < (SMDUE_BARCODEVERSION_FRAMES_NUM * CAN_FRAME_LEN)  )
			{
				printf("SmdueSampleBarcodeVer():iAddr=%d,--------------------------------2\n",iAddr);
				PackAndSendSmdueCmd(uiMsgType,(UINT)iTempAddr,CAN_CMD_TYPE_P2P,0,0,0.0,uiCANPort);
				Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
				CAN_ReadFrames(MAX_FRAMES_IN_BUFF, g_CanData.CanCommInfo.abyRcvBuf, &iReadLen1st);
				
				if ( iReadLen1st < (SMDUE_BARCODEVERSION_FRAMES_NUM * CAN_FRAME_LEN)  )
				{
					printf("SmdueSampleBarcodeVer():iAddr=%d,--------------------------------3\n",iAddr);
					PackAndSendSmdueCmd(uiMsgType,(UINT)iTempAddr,CAN_CMD_TYPE_P2P,0,0,0.0,uiCANPort);
					Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
					CAN_ReadFrames(MAX_FRAMES_IN_BUFF, g_CanData.CanCommInfo.abyRcvBuf, &iReadLen1st);

				}
			}
		}
	}
	s_bFirstRunBarcode = FALSE;

	if(iReadLen1st >= (SMDUE_BARCODEVERSION_FRAMES_NUM * CAN_FRAME_LEN))
	{
		if(SmdueUnpackBarcodeVer(iAddr,
			iReadLen1st,
			g_CanData.CanCommInfo.abyRcvBuf))
		{
			ClrSmdueIntrruptTimes(iAddr);
		}
		else
		{
			//IncSmdueIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;		
		}
	}
	else
	{		
		//IncSmdueIntrruptTimes(iAddr);
		return CAN_SAMPLE_FAIL;		
	}

	return CAN_SAMPLE_OK;
}



void SMDUE_InitRoughValue(void)
{
	int i,j;
	if(g_CanData.aRoughDataGroup[GROUP_SMDUE_ACTUAL_NUM].iValue == CAN_SAMP_INVALID_VALUE)
	{
		g_CanData.aRoughDataGroup[GROUP_SMDUE_ACTUAL_NUM].iValue = 0;	
	}

	for(i = 0; i < MAX_NUM_SMDUE; i++)
	{
		for(j = 0; j < SMDUE_MAX_SIGNALS_NUM; j++)
		{
			g_CanData.aRoughDataSmdue[i][j].iValue = CAN_SAMP_INVALID_VALUE;
		}
	}

	//g_CanData.CanCommInfo.SmdueCommInfo.bNeedFreshKWH = TRUE;
}

/*==========================================================================*
* FUNCTION : SmdueAllNoResp
* PURPOSE  : Judge if SM-DUs need to re-config, if need, scan SM-DUs and 
*            save SM-DU connecting information.
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Fengel  hu                DATE: 2018-12-7 
*==========================================================================*/
static BOOL SmdueAllNoResp(void)
{
	int i;
	 
	for(i = 0; i < MAX_NUM_SMDUE; i++)
	{
		if((g_CanData.aRoughDataSmdue[i][SMDUE_EXISTENCE].iValue == SMDUE_EQUIP_EXISTENT)
			 && (g_CanData.aRoughDataSmdue[i][SMDUE_INTERRUPT_TIMES].iValue < SMDUE_MAX_INTERRUPT_TIMES))
		{
			return FALSE;
		}
	}
	return TRUE;
}



/*==========================================================================*
* FUNCTION : SMDUE_Reconfig
* PURPOSE  : Judge if SM-DUs need to re-config, if need, scan SM-DUs and 
*            save SM-DU connecting information.
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Fengel  hu                 DATE:2018-12-7 
*==========================================================================*/
void SMDUE_Reconfig(void)
{
	INT32		i;
	INT32		iAddr;
	BOOL		iRst;
	INT32		iSeqNum = 0;
	UINT		iCanBus = SMDUEonCAN1;


	for (iAddr = 0; iAddr < CAN_SMDUE_MAX_NUM; iAddr++)
	{
		iRst		= CAN_SAMPLE_FAIL;

		for (i = 0; i < SMDUE_SCAN_TRY_TIME; i++)
		{
			iRst = SmdueSampleBarcodeVer(iAddr, MSG_TYPE_RQST_DATA20,iCanBus);


/******************************For DEBUG Start**********************************/
//			if( (iAddr >=0) || (iAddr <8))
//			{
//				iRst=CAN_SAMPLE_OK;
//			}
/******************************For DEBUG End**********************************/


			if (CAN_SAMPLE_OK == iRst)
			{
				break;
			}
			else
			{
				Sleep(30);
				//CAN_ClearReadBuf();
			}
		}

		if (CAN_SAMPLE_OK == iRst)
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_EXISTENCE].iValue = SMDUE_EQUIP_EXISTENT;
			iSeqNum++;
		}
		else
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_EXISTENCE].iValue = SMDUE_EQUIP_NOT_EXISTENT;
		}

		Sleep(60);

		printf("SMDUE_Reconfig(): g_CanData.aRoughDataSmdue[%d][SMDUE_EXISTENCE].iValue=%d~~~~~\n",iAddr,g_CanData.aRoughDataSmdue[iAddr][SMDUE_EXISTENCE].iValue);
	}

	if(iSeqNum)
	{
		g_CanData.aRoughDataGroup[GROUP_SMDUE_ACTUAL_NUM].iValue = iSeqNum;
		g_CanData.aRoughDataGroup[GROUP_SMDUE_GROUP_STATE].iValue = CAN_SMDUE_EXIT;
	}
	else
	{
		g_CanData.aRoughDataGroup[GROUP_SMDUE_GROUP_STATE].iValue = CAN_SMDUE_NOT_EXIT;
		g_CanData.aRoughDataGroup[GROUP_SMDUE_ACTUAL_NUM].iValue =0;
	}

	return;
}


/*==========================================================================*
* FUNCTION : SMDUE_Sample 
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel hu                DATE: 2018-12-7 
*==========================================================================*/
void SMDUE_Sample(void)
{
	int			iAddr = 0;
	UINT		iCanBus = SMDUEonCAN1;

	if(g_CanData.CanCommInfo.SmdueCommInfo.bNeedReconfig)
	{
		g_CanData.CanCommInfo.SmdueCommInfo.bNeedReconfig = FALSE;
		SMDUE_Reconfig();
		g_CanData.aRoughDataGroup[GROUP_SMDUE_ALL_NORESPONSE].iValue = SmdueAllNoResp();
		return;
	}

	for(iAddr = 0; iAddr < MAX_NUM_SMDUE; iAddr++)
	{
		if( g_CanData.aRoughDataSmdue[iAddr][SMDUE_EXISTENCE].iValue == SMDUE_EQUIP_EXISTENT )	
		{
			SmdueSampleVoltAlm(iAddr, MSG_TYPE_RQST_DATA00,iCanBus);
			SmdueSampleGetData(iAddr, MSG_TYPE_RQST_DATA30, iCanBus);
			ShuntCoeffBackwardRead(iAddr, MSG_TYPE_RQST_DATA31,iCanBus);
			SmdueFuseAlarmModeBackwardRead(iAddr, MSG_TYPE_RQST_DATA32, iCanBus);
			
			//for test
			//SmdueSampleShuntCoeff(iAddr, MSG_TYPE_RQST_DATA31, iCanBus);//For test
		}
		else
		{
			g_CanData.aRoughDataSmdue[iAddr][SMDUE_INTERRUPT_ST].iValue = SMDUE_COMM_NORMAL_ST;
		}
		SmdueCalFuseAlm(iAddr);
	}


	//for test-----start
	//g_CanData.aRoughDataSmdue[0][SMDUE_SHUNT_CURRENT_1].fValue = 10;
	//g_CanData.aRoughDataSmdue[0][SMDUE_SHUNT_CURRENT_2].fValue = 20;
	
	SMDUE_Custom_Analogs_is_Display();
	SMDUE_ReDispatchShuntReadings();
	SMDUE_Transducer_Calculation();

	g_CanData.aRoughDataGroup[GROUP_SMDUE_ALL_NORESPONSE].iValue = SmdueAllNoResp();

	return;
}

/*==========================================================================*
* FUNCTION : SMDUE_Transducer_Calculation 
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel hu                DATE: 2018-12-7 
*==========================================================================*/
static void SMDUE_Get_Point_AI_Type(MATH_POINT_XY	*pstMath_Point,int iAddr,int iCh)
{
		int i = iAddr;
		int j = iCh;

	
		pstMath_Point->enumAiType = GetEnumSigValue( EQUIP_ID_SMDUE_UNIT1 + i, 
										SIG_TYPE_SETTING,
										SIG_ID_SMDUE_AI_TYPE + j,
										"CAN_SAMP" );
										
		pstMath_Point->fTsrVoltageX1 = GetFloatSigValue( EQUIP_ID_SMDUE_UNIT1 + i, 
											  SIG_TYPE_SETTING,
											  SIG_ID_SMDUE_TSR_VOL_X1 + j,
											  "CAN_SAMP" );	

		pstMath_Point->fTsrVoltageY1 = GetFloatSigValue( EQUIP_ID_SMDUE_UNIT1 + i, 
											  SIG_TYPE_SETTING,
											  SIG_ID_SMDUE_TSR_VOL_Y1 + j,
											  "CAN_SAMP" );	
											  
		pstMath_Point->fTsrVoltageX2 = GetFloatSigValue( EQUIP_ID_SMDUE_UNIT1 + i, 
											  SIG_TYPE_SETTING,
											  SIG_ID_SMDUE_TSR_VOL_X2 + j,
											  "CAN_SAMP" );	

		pstMath_Point->fTsrVoltageY2 = GetFloatSigValue( EQUIP_ID_SMDUE_UNIT1 + i, 
											  SIG_TYPE_SETTING,
											  SIG_ID_SMDUE_TSR_VOL_Y2 + j,
											  "CAN_SAMP" );			

		pstMath_Point->fTsrCurrentX1 = GetFloatSigValue( EQUIP_ID_SMDUE_UNIT1 + i, 
											  SIG_TYPE_SETTING,
											  SIG_ID_SMDUE_TSR_CUR_X1 + j,
											  "CAN_SAMP" );	

		pstMath_Point->fTsrCurrentY1 = GetFloatSigValue( EQUIP_ID_SMDUE_UNIT1 + i, 
											  SIG_TYPE_SETTING,
											  SIG_ID_SMDUE_TSR_CUR_Y1 + j,
											  "CAN_SAMP" );	
											  
		pstMath_Point->fTsrCurrentX2 = GetFloatSigValue( EQUIP_ID_SMDUE_UNIT1 + i, 
											  SIG_TYPE_SETTING,
											  SIG_ID_SMDUE_TSR_CUR_X2 + j,
											  "CAN_SAMP" );	

		pstMath_Point->fTsrCurrentY2 = GetFloatSigValue( EQUIP_ID_SMDUE_UNIT1 + i, 
													  SIG_TYPE_SETTING,
												  SIG_ID_SMDUE_TSR_CUR_Y2 + j,
													  "CAN_SAMP" );	
													  
	return;
}

/*==========================================================================*
* FUNCTION : SMDUE_Custom_Analogs_is_Display 
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel hu                DATE: 2018-12-7 
*==========================================================================*/
static void SMDUE_Custom_Analogs_is_Display()
{
#define DISPLAY 0
#define NOT_DISPLAY 1

	int i=0;
	int j=0;

	SIG_ENUM	enumAiType;

	
	for (i = 0; i < MAX_NUM_SMDUE; i++)
	{
		if( g_CanData.aRoughDataSmdue[i][SMDUE_EXISTENCE].iValue == SMDUE_EQUIP_EXISTENT )		
		{
			for(j = 0; j < MAX_CHANEL_NUM_SMDUE; j++)
			{
				enumAiType = GetEnumSigValue( EQUIP_ID_SMDUE_UNIT1 + i, 
										SIG_TYPE_SETTING,
										SIG_ID_SMDUE_AI_TYPE + j,
										"CAN_SAMP" );
				if( (2 == enumAiType) || (3 == enumAiType) )
				{
					SetEnumSigValue(1,
					  				SIG_TYPE_SETTING,
					  				710,
					  				DISPLAY,
					 	 			"CAN_SAMP");
					
				
					return;
				}

				enumAiType = GetEnumSigValue( EQUIP_ID_SMDUE_UNIT1 + i, 
										SIG_TYPE_SETTING,
										121 + j,
										"CAN_SAMP" );
				if( 1 == enumAiType )
				{
					SetEnumSigValue(1,
							SIG_TYPE_SETTING,
							710,
							DISPLAY,
							"CAN_SAMP");
				
					return;
				}			
			}
		}
	}

	SetEnumSigValue(1,
					  SIG_TYPE_SETTING,
					  710,
					  NOT_DISPLAY,
					 "CAN_SAMP");

	return;
}

/*==========================================================================*
* FUNCTION : SMDUE_Transducer_Calculation 
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel hu                DATE: 2018-12-7 
*==========================================================================*/
static void SMDUE_Transducer_Calculation()
{
#define 	AI_TYPE_TRANSDUCER_V		3
#define 	AI_TYPE_TRANSDUCER_A		2

	int i=0;
	int j=0;
	float fSlope = 0.0;
	float fintercept=0.0;
	
	MATH_POINT_XY	stMath_Point;
	MATH_POINT_XY	*pMath_Point;
	pMath_Point = &stMath_Point;
	

	for (i = 0; i < MAX_NUM_SMDUE; i++)
	{
		if( g_CanData.aRoughDataSmdue[i][SMDUE_EXISTENCE].iValue == SMDUE_EQUIP_EXISTENT )		
		{
			for(j = 0; j < MAX_CHANEL_NUM_SMDUE; j++)
			{

				SMDUE_Get_Point_AI_Type(pMath_Point,i,j);													  
				
												
				if( AI_TYPE_TRANSDUCER_V == pMath_Point->enumAiType )
				{
					if( pMath_Point->fTsrVoltageX2 == pMath_Point->fTsrVoltageX1 )
					{
						continue;
					}
					fSlope = (pMath_Point->fTsrVoltageY2-pMath_Point->fTsrVoltageY1)/(pMath_Point->fTsrVoltageX2-pMath_Point->fTsrVoltageX1);
					fintercept = pMath_Point->fTsrVoltageY1 - fSlope * (pMath_Point->fTsrVoltageX1);

					if ( g_CanData.aRoughDataSmdue[i][SMDUE_VOLT_1+j].iValue == CAN_SAMP_INVALID_VALUE )
					{
						g_CanData.aRoughDataSmdue[i][SMDUE_VOLT_1+j].fValue = 0.0;
					}

					g_CanData.aRoughDataSmdue[i][SMDUE_TRANSDUCER_VOL_1+j].fValue = 
											fSlope * (g_CanData.aRoughDataSmdue[i][SMDUE_VOLT_1+j].fValue) + fintercept;
				}
				else if( AI_TYPE_TRANSDUCER_A == pMath_Point->enumAiType )
				{
					if( pMath_Point->fTsrCurrentX2 == pMath_Point->fTsrCurrentX1 )
					{
						continue;
					}
					fSlope = (pMath_Point->fTsrCurrentY2-pMath_Point->fTsrCurrentY1)/(pMath_Point->fTsrCurrentX2-pMath_Point->fTsrCurrentX1);
					fintercept = pMath_Point->fTsrCurrentY1 - fSlope * (pMath_Point->fTsrCurrentX1);

					if ( g_CanData.aRoughDataSmdue[i][SMDUE_HALL_CURRENT_1+j].iValue == CAN_SAMP_INVALID_VALUE )
					{
						g_CanData.aRoughDataSmdue[i][SMDUE_HALL_CURRENT_1+j].fValue = 0.0;
					}

					g_CanData.aRoughDataSmdue[i][SMDUE_TRANSDUCER_CURR_1+j].fValue = 
											fSlope * (g_CanData.aRoughDataSmdue[i][SMDUE_HALL_CURRENT_1+j].fValue) + fintercept;
				}
				else
				{
				}
			}
		}
	}

	return;
}

/*==========================================================================*
* FUNCTION : SMDUE_StuffChannel 
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel hu                DATE: 2018-12-7 
*==========================================================================*/
static void SMDUE_ReDispatchShuntReadings()
{
	int i=0;
	int j=0;
	SIG_ENUM	enumValue = 0;
	SIG_ENUM	enumValue1 = 0;
	float 		fTotalSMDUEShuntCurr = 0.0;
	BOOL 		bNeedRefreshPage = FALSE;
	static 		SIG_ENUM iLastShuntSetting[MAX_NUM_SMDUE][MAX_CHANEL_NUM_SMDUE]= {0};

	for (i = 0; i < MAX_NUM_SMDUE; i++)
	{
		if( g_CanData.aRoughDataSmdue[i][SMDUE_EXISTENCE].iValue == SMDUE_EQUIP_EXISTENT )		
		{
			for(j = 0; j < MAX_CHANEL_NUM_SMDUE; j++)
			{
				enumValue = GetEnumSigValue(EQUIP_ID_SMDUE_UNIT1 + i, 
								SIG_TYPE_SETTING,
								SIG_ID_SMDUE_SHUNT1_SET_AS + j,
								"CAN_SAMP");

				if((enumValue !=iLastShuntSetting[i][j]) && 
					(enumValue==3||iLastShuntSetting[i][j]==3))//˵���е�ط����䶯����Ҫˢ��ҳ��
				{
					bNeedRefreshPage = TRUE;
				}
				iLastShuntSetting[i][j] = enumValue;

				if(enumValue == 0)//NotUsed
				{
					g_CanData.aRoughDataSmdue[i][SMDUE_BATT_CURRENT_1+j].iValue
						= CAN_SAMP_INVALID_VALUE;
					g_CanData.aRoughDataSmdue[i][SMDUE_LOAD_CURRENT_1+j].iValue
						= CAN_SAMP_INVALID_VALUE;
					g_CanData.aRoughDataSmdue[i][SMDUE_BATT1_EXIST_STAT+j].iValue 
						= SMDUE_EQUIP_NOT_EXISTENT;	
				}
				else if(enumValue == 1)//General
				{
					g_CanData.aRoughDataSmdue[i][SMDUE_BATT_CURRENT_1+j].iValue
						= CAN_SAMP_INVALID_VALUE;
					if( CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataSmdue[i][SMDUE_SHUNT_CURRENT_1+j].iValue )
					{
						g_CanData.aRoughDataSmdue[i][SMDUE_LOAD_CURRENT_1+j].fValue= 0.0;
					}
					else
					{
						g_CanData.aRoughDataSmdue[i][SMDUE_LOAD_CURRENT_1+j].fValue
							= g_CanData.aRoughDataSmdue[i][SMDUE_SHUNT_CURRENT_1+j].fValue;
					}
					g_CanData.aRoughDataSmdue[i][SMDUE_BATT1_EXIST_STAT+j].iValue 
						= SMDUE_EQUIP_NOT_EXISTENT;	
				}
				else if(enumValue == 2)//Load
				{
					g_CanData.aRoughDataSmdue[i][SMDUE_BATT_CURRENT_1+j].iValue
						= CAN_SAMP_INVALID_VALUE;
					if( CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataSmdue[i][SMDUE_SHUNT_CURRENT_1+j].iValue )
					{
						g_CanData.aRoughDataSmdue[i][SMDUE_LOAD_CURRENT_1+j].fValue = 0.0;
					}
					else
					{
						g_CanData.aRoughDataSmdue[i][SMDUE_LOAD_CURRENT_1+j].fValue
							= g_CanData.aRoughDataSmdue[i][SMDUE_SHUNT_CURRENT_1+j].fValue;
						fTotalSMDUEShuntCurr += g_CanData.aRoughDataSmdue[i][SMDUE_SHUNT_CURRENT_1+j].fValue;	
					}
					g_CanData.aRoughDataSmdue[i][SMDUE_BATT1_EXIST_STAT+j].iValue 
						= SMDUE_EQUIP_NOT_EXISTENT;	
				}
				else if(enumValue == 3)//Battery  SMDUE3-8 will not going to here. Because they have not battery setting
				{
					if( CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataSmdue[i][SMDUE_SHUNT_CURRENT_1+j].iValue )
					{
						g_CanData.aRoughDataSmdue[i][SMDUE_BATT_CURRENT_1+j].fValue = 0.0;
					}
					else
					{
						g_CanData.aRoughDataSmdue[i][SMDUE_BATT_CURRENT_1+j].fValue
							= g_CanData.aRoughDataSmdue[i][SMDUE_SHUNT_CURRENT_1+j].fValue;				
					}
					g_CanData.aRoughDataSmdue[i][SMDUE_LOAD_CURRENT_1+j].iValue
						= CAN_SAMP_INVALID_VALUE;

					enumValue1 = GetEnumSigValue(EQUIP_ID_SMDUE_UNIT1 + i, 
								SIG_TYPE_SETTING,
								141 + j,
								"CAN_SAMP");

					if( 1 == enumValue1 )
					{
						g_CanData.aRoughDataSmdue[i][SMDUE_BATT1_EXIST_STAT+j].iValue 
								= SMDUE_EQUIP_EXISTENT;	
					}
					else
					{
						g_CanData.aRoughDataSmdue[i][SMDUE_BATT1_EXIST_STAT+j].iValue 
								= SMDUE_EQUIP_NOT_EXISTENT;	
					}
				}
			}
		}
		else
		{
			for(j = 0; j < MAX_CHANEL_NUM_SMDUE; j++)
			{
				g_CanData.aRoughDataSmdue[i][SMDUE_BATT1_EXIST_STAT+j].iValue = SMDUE_EQUIP_NOT_EXISTENT;
			}
		}
	}

	if(GetEnumSigValue(SYSTEM_EQUIPID, 
							SIG_TYPE_SETTING,
							452,
							"CAN_SAMP") > 0)
	{
		SetFloatSigValue(SMDUE_GROUP_EQUIPID, 
							SIG_TYPE_SAMPLING,
							4,
							fTotalSMDUEShuntCurr,
							"CAN_SAMP");

		static int s_dwChangeForWeb = 0;
		if(bNeedRefreshPage)
		{
		
			//for LCD
			SetDwordSigValue(SMDUE_GROUP_EQUIPID, 
					SIG_TYPE_SAMPLING,
					CFG_CHANGED_SIG_ID,
					1,
					"CAN_SAMP");

			//for Web
			s_dwChangeForWeb++;
			if(s_dwChangeForWeb >= 255)
			{
				s_dwChangeForWeb = 0;
			}
			else
			{
				//
			}

			SetDwordSigValue(SMDUE_GROUP_EQUIPID, 
				SIG_TYPE_SAMPLING,
				CFG_CHANGED_SIG_ID + 1,
				s_dwChangeForWeb,
				"CAN_SAMP");

		}

	}

	return;
}




/*==========================================================================*
* FUNCTION : SMDUE_StuffChannel 
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel hu                DATE: 2018-12-7 
*==========================================================================*/
void SMDUE_StuffChannel(ENUMSIGNALPROC EnumProc, LPVOID lpvoid)	
{
	int		i,j,k;
	int		iSmdueNum = g_CanData.aRoughDataGroup[GROUP_SMDUE_ACTUAL_NUM].iValue;

	CHANNEL_TO_ROUGH_DATA		SmdueGroupSig[] =
	{
		{SM_CH_SMDUE_GROUP_STATE,		GROUP_SMDUE_GROUP_STATE,	},
		{SM_CH_SMDUE_NUM,				GROUP_SMDUE_ACTUAL_NUM,	},

		{SMDUE_CH_END_FLAG,				SMDUE_CH_END_FLAG,	},
	};

	CHANNEL_TO_ROUGH_DATA		SmdueSig[] =
	{
		{SMDUE_CH_SHUNT_CURRENT_1,		SMDUE_SHUNT_CURRENT_1,},
		{SMDUE_CH_SHUNT_CURRENT_2,		SMDUE_SHUNT_CURRENT_2,},
		{SMDUE_CH_SHUNT_CURRENT_3,		SMDUE_SHUNT_CURRENT_3,},
		{SMDUE_CH_SHUNT_CURRENT_4,		SMDUE_SHUNT_CURRENT_4,},
		{SMDUE_CH_SHUNT_CURRENT_5,		SMDUE_SHUNT_CURRENT_5,},
		{SMDUE_CH_SHUNT_CURRENT_6,		SMDUE_SHUNT_CURRENT_6,},
		{SMDUE_CH_SHUNT_CURRENT_7,		SMDUE_SHUNT_CURRENT_7,},
		{SMDUE_CH_SHUNT_CURRENT_8,		SMDUE_SHUNT_CURRENT_8,},
		{SMDUE_CH_SHUNT_CURRENT_9,		SMDUE_SHUNT_CURRENT_9,},
		{SMDUE_CH_SHUNT_CURRENT_10,		SMDUE_SHUNT_CURRENT_10,},
		{SMDUE_CH_HALL_CURRENT_1,		SMDUE_HALL_CURRENT_1,},
		{SMDUE_CH_HALL_CURRENT_2,		SMDUE_HALL_CURRENT_2,},
		{SMDUE_CH_HALL_CURRENT_3,		SMDUE_HALL_CURRENT_3,},
		{SMDUE_CH_HALL_CURRENT_4,		SMDUE_HALL_CURRENT_4,},
		{SMDUE_CH_HALL_CURRENT_5,		SMDUE_HALL_CURRENT_5,},
		{SMDUE_CH_HALL_CURRENT_6,		SMDUE_HALL_CURRENT_6,},
		{SMDUE_CH_HALL_CURRENT_7,		SMDUE_HALL_CURRENT_7,},
		{SMDUE_CH_HALL_CURRENT_8,		SMDUE_HALL_CURRENT_8,},
		{SMDUE_CH_HALL_CURRENT_9,		SMDUE_HALL_CURRENT_9,},
		{SMDUE_CH_HALL_CURRENT_10,		SMDUE_HALL_CURRENT_10,},
		{SMDUE_CH_VOLT_1,				SMDUE_VOLT_1,},
		{SMDUE_CH_VOLT_2,				SMDUE_VOLT_2,},
		{SMDUE_CH_VOLT_3,				SMDUE_VOLT_3,},
		{SMDUE_CH_VOLT_4,				SMDUE_VOLT_4,},
		{SMDUE_CH_VOLT_5,				SMDUE_VOLT_5,},
		{SMDUE_CH_VOLT_6,				SMDUE_VOLT_6,},
		{SMDUE_CH_VOLT_7,				SMDUE_VOLT_7,},
		{SMDUE_CH_VOLT_8,				SMDUE_VOLT_8,},
		{SMDUE_CH_VOLT_9,				SMDUE_VOLT_9,},
		{SMDUE_CH_VOLT_10,				SMDUE_VOLT_10,},
		{SMDUE_CH_TEMP_1,				SMDUE_TEMP_1,},
		{SMDUE_CH_TEMP_2,				SMDUE_TEMP_2,},
		{SMDUE_CH_TEMP_3,				SMDUE_TEMP_3,},
		{SMDUE_CH_TEMP_4,				SMDUE_TEMP_4,},
		{SMDUE_CH_TEMP_5,				SMDUE_TEMP_5,},
		{SMDUE_CH_TEMP_6,				SMDUE_TEMP_6,},
		{SMDUE_CH_TEMP_7,				SMDUE_TEMP_7,},
		{SMDUE_CH_TEMP_8,				SMDUE_TEMP_8,},
		{SMDUE_CH_TEMP_9,				SMDUE_TEMP_9,},
		{SMDUE_CH_TEMP_10,				SMDUE_TEMP_10,},
		{PANEL_CH_VOLT_1,				PANEL_VOLT_1,},
		{PANEL_CH_VOLT_2,				PANEL_VOLT_2,},
		{PANEL_CH_VOLT_3,				PANEL_VOLT_3,},
		{PANEL_CH_VOLT_4,				PANEL_VOLT_4,},
		{PANEL_CH_VOLT_5,				PANEL_VOLT_5,},
		{PANEL_CH_VOLT_6,				PANEL_VOLT_6,},
		{PANEL_CH_VOLT_7,				PANEL_VOLT_7,},
		{PANEL_CH_VOLT_8,				PANEL_VOLT_8,},
		{PANEL_CH_VOLT_9,				PANEL_VOLT_9,},
		{PANEL_CH_VOLT_10,				PANEL_VOLT_10,},
		{SMDUE_CH_BUS_BAR_VOLT,			SMDUE_BUS_BAR_VOLT,},
		{SMDUE_CH_VERSION_NO,			SMDUE_VERSION_NO,},
		{SMDUE_CH_SERIAL_NO_HIGH,		SMDUE_SERIAL_NO_HIGH,},
		{SMDUE_CH_SERIAL_NO_LOW,		SMDUE_SERIAL_NO_LOW,},
		{SMDUE_CH_BARCODE1,				SMDUE_BARCODE1,},
		{SMDUE_CH_BARCODE2,				SMDUE_BARCODE2,},
		{SMDUE_CH_BARCODE3,				SMDUE_BARCODE3,},
		{SMDUE_CH_BARCODE4,				SMDUE_BARCODE4,},
		{SMDUE_CH_CH_INTTERUPT_ST,		SMDUE_INTERRUPT_ST,},
		{SMDUE_CH_CH_EXIST_ST,			SMDUE_EXISTENCE,},
		{SM_CH_SMDUE_FUSESTAT1	,		SMDUE_FUSESTAT1,},
		{SM_CH_SMDUE_FUSESTAT2,			SMDUE_FUSESTAT2,},
		{SM_CH_SMDUE_FUSESTAT3,			SMDUE_FUSESTAT3,},
		{SM_CH_SMDUE_FUSESTAT4,			SMDUE_FUSESTAT4,},
		{SM_CH_SMDUE_FUSESTAT5,			SMDUE_FUSESTAT5,},
		{SM_CH_SMDUE_FUSESTAT6,			SMDUE_FUSESTAT6,},
		{SM_CH_SMDUE_FUSESTAT7,			SMDUE_FUSESTAT7,},
		{SM_CH_SMDUE_FUSESTAT8,			SMDUE_FUSESTAT8,},
		{SM_CH_SMDUE_FUSESTAT9,			SMDUE_FUSESTAT9,},
		{SM_CH_SMDUE_FUSESTAT10,		SMDUE_FUSESTAT10,},
		{SM_CH_SMDUE_FUSE_COMM_STAT,	SMDUE_INTERRUPT_ST,},
		{SM_CH_SMDUE_FUSE_EXIST_STAT,	SMDUE_EXISTENCE,},
		
		{SMDUE_CH_LOAD_CURRENT_1,		SMDUE_LOAD_CURRENT_1,},
		{SMDUE_CH_LOAD_CURRENT_2,		SMDUE_LOAD_CURRENT_2,},
		{SMDUE_CH_LOAD_CURRENT_3,		SMDUE_LOAD_CURRENT_3,},
		{SMDUE_CH_LOAD_CURRENT_4,		SMDUE_LOAD_CURRENT_4,},
		{SMDUE_CH_LOAD_CURRENT_5,		SMDUE_LOAD_CURRENT_5,},
		{SMDUE_CH_LOAD_CURRENT_6,		SMDUE_LOAD_CURRENT_6,},
		{SMDUE_CH_LOAD_CURRENT_7,		SMDUE_LOAD_CURRENT_7,},
		{SMDUE_CH_LOAD_CURRENT_8,		SMDUE_LOAD_CURRENT_8,},
		{SMDUE_CH_LOAD_CURRENT_9,		SMDUE_LOAD_CURRENT_9,},
		{SMDUE_CH_LOAD_CURRENT_10,		SMDUE_LOAD_CURRENT_10,},

		{SMDUE_CH_TRANSDUCER_CURR_1,	SMDUE_TRANSDUCER_CURR_1,},
		{SMDUE_CH_TRANSDUCER_CURR_2,	SMDUE_TRANSDUCER_CURR_2,},
		{SMDUE_CH_TRANSDUCER_CURR_3,	SMDUE_TRANSDUCER_CURR_3,},
		{SMDUE_CH_TRANSDUCER_CURR_4,	SMDUE_TRANSDUCER_CURR_4,},
		{SMDUE_CH_TRANSDUCER_CURR_5,	SMDUE_TRANSDUCER_CURR_5,},
		{SMDUE_CH_TRANSDUCER_CURR_6,	SMDUE_TRANSDUCER_CURR_6,},
		{SMDUE_CH_TRANSDUCER_CURR_7,	SMDUE_TRANSDUCER_CURR_7,},
		{SMDUE_CH_TRANSDUCER_CURR_8,	SMDUE_TRANSDUCER_CURR_8,},
		{SMDUE_CH_TRANSDUCER_CURR_9,	SMDUE_TRANSDUCER_CURR_9,},
		{SMDUE_CH_TRANSDUCER_CURR_10,	SMDUE_TRANSDUCER_CURR_10,},

		{SMDUE_CH_TRANSDUCER_VOL_1,		SMDUE_TRANSDUCER_VOL_1,},
		{SMDUE_CH_TRANSDUCER_VOL_2,		SMDUE_TRANSDUCER_VOL_2,},
		{SMDUE_CH_TRANSDUCER_VOL_3,		SMDUE_TRANSDUCER_VOL_3,},
		{SMDUE_CH_TRANSDUCER_VOL_4,		SMDUE_TRANSDUCER_VOL_4,},
		{SMDUE_CH_TRANSDUCER_VOL_5,		SMDUE_TRANSDUCER_VOL_5,},
		{SMDUE_CH_TRANSDUCER_VOL_6,		SMDUE_TRANSDUCER_VOL_6,},
		{SMDUE_CH_TRANSDUCER_VOL_7,		SMDUE_TRANSDUCER_VOL_7,},
		{SMDUE_CH_TRANSDUCER_VOL_8,		SMDUE_TRANSDUCER_VOL_8,},
		{SMDUE_CH_TRANSDUCER_VOL_9,		SMDUE_TRANSDUCER_VOL_9,},
		{SMDUE_CH_TRANSDUCER_VOL_10,	SMDUE_TRANSDUCER_VOL_10,},


		{SMDUE_CH_END_FLAG,				SMDUE_CH_END_FLAG,},
	};	

	CHANNEL_TO_ROUGH_DATA		SmdueBattSig[] =
	{
		{SMDUE_CH_BATT_CURRENT_1,		SMDUE_BATT_CURRENT_1,},
		{SMDUE_CH_BATT_CURRENT_2,		SMDUE_BATT_CURRENT_2,},
		{SMDUE_CH_BATT_CURRENT_3,		SMDUE_BATT_CURRENT_3,},
		{SMDUE_CH_BATT_CURRENT_4,		SMDUE_BATT_CURRENT_4,},
		{SMDUE_CH_BATT_CURRENT_5,		SMDUE_BATT_CURRENT_5,},
		{SMDUE_CH_BATT_CURRENT_6,		SMDUE_BATT_CURRENT_6,},
		{SMDUE_CH_BATT_CURRENT_7,		SMDUE_BATT_CURRENT_7,},
		{SMDUE_CH_BATT_CURRENT_8,		SMDUE_BATT_CURRENT_8,},
		{SMDUE_CH_BATT_CURRENT_9,		SMDUE_BATT_CURRENT_9,},
		{SMDUE_CH_BATT_CURRENT_10,		SMDUE_BATT_CURRENT_10,},
		
		{SM_CH_SMDUE_BATT1_EXIST_STAT,	SMDUE_BATT1_EXIST_STAT,},
		{SM_CH_SMDUE_BATT2_EXIST_STAT,	SMDUE_BATT2_EXIST_STAT,},
		{SM_CH_SMDUE_BATT3_EXIST_STAT,	SMDUE_BATT3_EXIST_STAT,},
		{SM_CH_SMDUE_BATT4_EXIST_STAT,	SMDUE_BATT4_EXIST_STAT,},
		{SM_CH_SMDUE_BATT5_EXIST_STAT,	SMDUE_BATT5_EXIST_STAT,},
		{SM_CH_SMDUE_BATT6_EXIST_STAT,	SMDUE_BATT6_EXIST_STAT,},
		{SM_CH_SMDUE_BATT7_EXIST_STAT,	SMDUE_BATT7_EXIST_STAT,},
		{SM_CH_SMDUE_BATT8_EXIST_STAT,	SMDUE_BATT8_EXIST_STAT,},
		{SM_CH_SMDUE_BATT9_EXIST_STAT,	SMDUE_BATT9_EXIST_STAT,},
		{SM_CH_SMDUE_BATT10_EXIST_STAT,	SMDUE_BATT10_EXIST_STAT,},
		{SMDUE_CH_END_FLAG,				SMDUE_CH_END_FLAG,},
	};
	
	i = 0;
	while(SmdueGroupSig[i].iChannel != SMDUE_CH_END_FLAG)
	{
		EnumProc(SmdueGroupSig[i].iChannel, 
			g_CanData.aRoughDataGroup[SmdueGroupSig[i].iRoughData].fValue, 
			lpvoid );
		i++;
	}
			
	for(i = 0; i < MAX_NUM_SMDUE; i++)
	{
		SMDUE_CommLoss_To_ParamUnify(i);
		
		j = 0;
		while(SmdueSig[j].iChannel != SMDUE_CH_END_FLAG)
		{
			EnumProc(i * MAX_CHN_NUM_PER_SMDUE + SmdueSig[j].iChannel, 
				g_CanData.aRoughDataSmdue[i][SmdueSig[j].iRoughData].fValue, 
				lpvoid );

			j++;
		}
		
		if( (0==i) || (1==i))
		{
			k=0;
			while(SmdueBattSig[k].iChannel != SMDUE_CH_END_FLAG)
			{
				EnumProc(i * MAX_CHN_NUM_PER_SMDUE + SmdueBattSig[k].iChannel, 
					g_CanData.aRoughDataSmdue[i][SmdueBattSig[k].iRoughData].fValue, 
					lpvoid );
			
				k++;
			}
		}
	}

/*
	for(i=0;i<8;i++)
	{
		if(0==i)
		{
			for(k=0;k<10;k++)
			printf("g_CanData.aRoughDataSmdue[%d][SMDUE_BATT%d_EXIST_STAT].iValue=%d\n ",i,k+1,g_CanData.aRoughDataSmdue[i][SMDUE_BATT1_EXIST_STAT+k].iValue);
		}
		if(1==i)
		{
			for(k=0;k<10;k++)
			printf("g_CanData.aRoughDataSmdue[%d][SMDUE_BATT%d_EXIST_STAT].iValue=%d\n ",i,k+1+10,g_CanData.aRoughDataSmdue[i][SMDUE_BATT1_EXIST_STAT+k].iValue);
		}
	}	
*/
	return;
}


/*==========================================================================*
* FUNCTION : SMDUHE_GetProdctInfo 
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel hu                DATE: 2018-12-7 
*==========================================================================*/
void SMDUHE_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI)
{

#define		BARCODE_FF_OFST			 0
#define		BARCODE_YY_OFST			 2
#define		BARCODE_MM_OFST			 4	
#define		BARCODE_WELLWELL_OFST	 6//##
#define		BARCODE_VV_OFST			 1//##

	UNUSED(hComm);
	UNUSED(nUnitNo); 
	int iAddr =0;
	BYTE	byTempBuf[4];
	UINT	uiTemp;
	INT32	iTemp;
	UINT	uiTemp11;
	UINT	uiTemp12;
	PRODUCT_INFO *				pInfo;
	pInfo					= (PRODUCT_INFO*)pPI;
	for(iAddr = 0; iAddr < MAX_NUM_SMDUE; iAddr++)
	{
		if( (SMDUE_EQUIP_EXISTENT == g_CanData.aRoughDataSmdue[iAddr][SMDUE_EXISTENCE].iValue) 
		     && ( g_CanData.aRoughDataSmdue[iAddr][SMDUE_BARCODE1].iValue != CAN_SAMP_INVALID_VALUE ) )	
		{
			pInfo->bSigModelUsed = TRUE;
		}
		else
		{
			pInfo->bSigModelUsed = FALSE;			
			pInfo++;
			continue;
		}
	

		//1.������ ��ʹ��
		//g_CanData.aRoughDataConvert[iAddr][CONVERT_FEATURE].uiValue

		//2.���ŵ�λ	SERIAL_NO_LOW=580005dc
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_SERIAL_NO_LOW].iValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_SERIAL_NO_LOW].iValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_SERIAL_NO_LOW].iValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_SERIAL_NO_LOW].iValue);

		//printf("SMDUE SERIAL_LOW:%d;%d;%d;%d;\n", byTempBuf[0], byTempBuf[1],byTempBuf[2],byTempBuf[3]);
		//��
		iTemp = byTempBuf[0] & 0x1f;//year
		snprintf(pInfo->szSerialNumber + BARCODE_YY_OFST, 3, "%d%d",iTemp/10,iTemp%10);
		//��
		iTemp = byTempBuf[1] & 0x0f;
		snprintf(pInfo->szSerialNumber + BARCODE_MM_OFST, 3, "%d%d",iTemp/10,iTemp%10);	
		//���ŵ�λ�ĵ�16�ֽ� ��Ӧ	##### ��������������
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		snprintf(pInfo->szSerialNumber + BARCODE_WELLWELL_OFST,
			6,"%05ld",uiTemp);
		pInfo->szSerialNumber[11] = '\0';//end	

		//3.�汾��VERSION=ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_VERSION_NO].iValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_VERSION_NO].iValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_VERSION_NO].iValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_VERSION_NO].iValue);

		//printf("SMDUE VERSION:%#x;%#x;%#x;%#x;\n", byTempBuf[0], byTempBuf[1],byTempBuf[2],byTempBuf[3]);

		//ģ��汾��
		//H		A00��ʽ�����ֽڰ�A=0 B=1���ƣ�����A=65
		byTempBuf[0] = 0x3f & byTempBuf[0];
		uiTemp = byTempBuf[0] + 65;
		pInfo->szHWVersion[0] = uiTemp;

		//VV 00����ʵ����ֵ������ֽ�
		snprintf(pInfo->szHWVersion + BARCODE_VV_OFST, 3,
			"%d%d",byTempBuf[1]/10, byTempBuf[1]%10);
		pInfo->szHWVersion[4] = '\0';//end

		//����汾		��������汾Ϊ1.30,��������Ϊ�����ֽ�������130
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		uiTemp11 = (uiTemp - (uiTemp/100)*100)/10;
		uiTemp12 = (uiTemp - (uiTemp/100)*100)%10;
		snprintf(pInfo->szSWVersion, 5,
			"%d.%d%d",uiTemp/100, uiTemp11,uiTemp12);
		pInfo->szSWVersion[5] = '\0';//end

		//printf("SMDUE: pInfo->bSigModelUsed = %d,  pInfo->szSerialNumber :%s , pInfo->szSWVersion:%s, pInfo->szHWVersion:%s \n ", pInfo->bSigModelUsed, pInfo->szSerialNumber,pInfo->szSWVersion,pInfo->szHWVersion);


		//3.CODE1	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_BARCODE1].iValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_BARCODE1].iValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_BARCODE1].iValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_BARCODE1].iValue);

		//FF ��ASCII���ʾ,ռ��CODE1 �� CODE2
		pInfo->szSerialNumber[0] = byTempBuf[0];
		pInfo->szSerialNumber[1] = byTempBuf[1];
		//printf("SMDUH Serial:%d/ %s\n", byTempBuf[0],pInfo->szSerialNumber);
		//T ��ʾ��Ʒ����,ASCIIռ��CODE12
		pInfo->szPartNumber[0] = byTempBuf[2];//1
		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[1] = byTempBuf[3];//S CODE13

		//3.CODE2	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_BARCODE2].iValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_BARCODE2].iValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_BARCODE2].iValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_BARCODE2].iValue);

		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[2] = byTempBuf[0];//M CODE14
		pInfo->szPartNumber[3] = byTempBuf[1];//D CODE15
		pInfo->szPartNumber[4] = byTempBuf[2];//U CODE16
		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[5] = byTempBuf[3];//+ CODE17
		}
		else
		{
			pInfo->szPartNumber[5] = '\0';
		}		

		//3.CODE3	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_BARCODE3].iValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_BARCODE3].iValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_BARCODE3].iValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_BARCODE3].iValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[6] = byTempBuf[0];//CODE18
		}
		else
		{
			pInfo->szPartNumber[6] = '\0';//CODE18
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[7] = byTempBuf[1];//CODE19
		}
		else
		{
			pInfo->szPartNumber[7] = '\0';//CODE19
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[8] = byTempBuf[2];//CODE20
		}
		else
		{
			pInfo->szPartNumber[8] = '\0';//CODE20
		}

		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[9] = byTempBuf[3];//CODE21
		}
		else
		{
			pInfo->szPartNumber[9] = '\0';//CODE21
		}


		//3.CODE4		ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_BARCODE4].iValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_BARCODE4].iValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_BARCODE4].iValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmdue[iAddr][SMDUE_BARCODE4].iValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[10] = byTempBuf[0];//CODE22
		}
		else
		{
			pInfo->szPartNumber[10] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[11] = byTempBuf[1];//CODE23
		}
		else
		{
			pInfo->szPartNumber[11] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[12] = byTempBuf[2];//CODE24
		}
		else
		{
			pInfo->szPartNumber[12] = '\0';
		}

		pInfo->szPartNumber[13] = '\0';//CODE25 

		//printf("\nSMTEMP bSigModelUsed:%d\n", pInfo->bSigModelUsed);
		/*
		pInfo->szPartNumber[0] = 'S';
		pInfo->szPartNumber[1] = 'M';
		pInfo->szPartNumber[2] = 'D';
		pInfo->szPartNumber[3] = 'U';
		pInfo->szPartNumber[4] = 'E';
		pInfo->szPartNumber[5] = '\0';
		pInfo->szPartNumber[6] = 'S';
		*/
		pInfo++; 

	}

	return;
}



/*==========================================================================*
* FUNCTION : SMDUE_CommLoss_To_ParamUnify 
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel hu                DATE: 2018-12-7 
*==========================================================================*/
static void SMDUE_CommLoss_To_ParamUnify(int iAddr)
{ 
	if( g_CanData.aRoughDataSmdue[iAddr][SMDUE_INTERRUPT_ST].iValue > 0 )
	{
		g_CanData.iSmdueCommLossToParamUnify[iAddr] = COMM_LOST_HOLD;
	}
	else
	{
		if ( COMM_LOST_HOLD == g_CanData.iSmdueCommLossToParamUnify[iAddr] )
		{
			g_CanData.iSmdueCommLossToParamUnify[iAddr] = COM_LOST_TO_PARAM_UNIFY;
			//printf("SMDUE_CommLoss_To_ParamUnify( ) : g_CanData.iSmdueCommLossToParamUnify[%d] = %d ~~~\n",iAddr,g_CanData.iSmdueCommLossToParamUnify[iAddr]);
		}
		else
		{
			g_CanData.iSmdueCommLossToParamUnify[iAddr] = COMM_NORMAL;
		}	
	}
	
	return;
}

/*==========================================================================*
* FUNCTION : Smdue_AI_Ch_Type_Unify
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 	   void  Smdue_Param_Unify(void)
* ARGUMENTS: 	
* RETURN   : void: 
* COMMENTS : 
* CREATOR  : Fengel  Hu               DATE: 2018-12-7 
*==========================================================================*/
void Smdue_AI_Ch_Type_Unify(int iAddr)
{
	int i = 0;
	int  iSigId = 0;
	int iTemp=0;
	int iSetValue = 0,iAiType;
	int  iSampleValue = 0;
	UINT uiCANPort = SMDUEonCAN1;
	UINT uiValueTypeH = 0x0;
	UINT uiValueTypeL = SMDUE_CHANNEL_DEFINE;
	
	iSampleValue = g_CanData.aRoughDataSmdue[iAddr][SMDUE_AI_CHANNEL_TYPE].iValue & 0x3FFFFFFF;	
	
	for(i=0; i<MAX_CHANEL_NUM_SMDUE; i++)
	{
		iSigId = SIG_ID_SMDUE_AI_TYPE+i;
		iTemp = GetEnumSigValue(EQUIP_ID_SMDUE_UNIT1+iAddr, SIG_TYPE_SETTING,iSigId,"CAN_SAMP");

		if( (iTemp > 0) && (iTemp < 5) )
		{
			iTemp = iTemp -1;
		}
		else
		{
			iTemp = 0;
		}
		
		iAiType	= iTemp;
		iSetValue |= (iAiType & 0x07) << (i*3);
	}
	
	if ( iSampleValue != iSetValue )
	{	
		PackAndSendSmdueCmd_AlarmMode(MSG_TYPE_RQST_SETTINGS,
								 	iAddr,
									CAN_CMD_TYPE_P2P,
									uiValueTypeH,
									uiValueTypeL,
									iSetValue,
									uiCANPort);
		Sleep(10);
		printf("Smdue_AI_Ch_Type_Unify: Not equal ,iSampleValue= %d, iSetValue=%d\n",iSampleValue,iSetValue);
	}
	else
	{
		printf("Smdue_AI_Ch_Type_Unify: Equal ,iSampleValue= %d, iSetValue=%d\n",iSampleValue,iSetValue);
	}
	return;
}
/*==========================================================================*
* FUNCTION : Smdue_Fuse_Alarm_Mode_Unify
* PURPOSE  :  
* CALLS    : 
* CALLED BY: 	   void  Smdue_Param_Unify(void)
* ARGUMENTS: 	
* RETURN   : void: 
* COMMENTS : 
* CREATOR  : Fengel  Hu               DATE: 2018-12-7 
*==========================================================================*/
void Smdue_Fuse_Alarm_Mode_Unify(int iAddr)
{
	int i = 0;
	int  iSigId = 0;
	
	int iSetValue = 0,iModeValue;
	int  iSampleValue = 0;
	
	UINT uiCANPort = SMDUEonCAN1;
	UINT uiValueTypeH = 0x01;
	UINT uiValueTypeL = SMDUE_FUSE_ALARM_MODE_VALUE_TYPE;
	

	iSampleValue = g_CanData.aRoughDataSmdue[iAddr][SMDUE_FUSE_ALARM_MODE].iValue & 0x3FFFFFFF;	

	for(i=0; i<MAX_CHANEL_NUM_SMDUE; i++)
	{
		iSigId = SIGID_FUSE1_ALARM_MODE+i;

		if(GetEnumSigValue(EQUIP_ID_SMDUE_UNIT1+iAddr, SIG_TYPE_SETTING,iSigId,"CAN_SAMP") == 0)
		{
			iModeValue = FUSE_ALARM_MODE_NORMAL;
		}
		else
		{
			iModeValue = FUSE_ALARM_MODE_ESNA;
		}
		
		iSetValue |= (iModeValue & 0x07) << (i*3);
	}
	
	if ( iSampleValue != iSetValue )
	{	
		PackAndSendSmdueCmd_AlarmMode(MSG_TYPE_RQST_SETTINGS,
								 	iAddr,
									CAN_CMD_TYPE_P2P,
									uiValueTypeH,
									uiValueTypeL,
									iSetValue,
									uiCANPort);
		Sleep(10);

		printf("Smdue_Fuse_Alarm_Mode_Unify: Not equal ,iSampleValue= %d, iSetValue=%d\n",iSampleValue,iSetValue);
	}
	else
	{
		printf("Smdue_Fuse_Alarm_Mode_Unify: Equal ,iSampleValue= %d, iSetValue=%d\n",iSampleValue,iSetValue);
	}
	
	return;
}


/*==========================================================================*
* FUNCTION : Smdue_Shunt_Coeff_Unify 
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel hu                DATE: 2018-12-7 
*==========================================================================*/
void Smdue_Shunt_Coeff_Unify(int iAddr)
{

	int	i = 0;

	int		iSigId = 0;
	float		fParam = 0.0;
	UINT 	uiValueTypeH = 0x01;
	UINT 	uiCANPort = SMDUEonCAN1;
	float		fSampleValue=0.0;

	PARAM_UNIFY_IDX		SmdueParamIdx[] =
	{
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT1, 	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT1, 		SMDUE_VOLT_SHUNT_COEFF_1,			SMDUE_VAL_TYPE_W_SHUNT1_MV,},
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR1, 	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR1, 		SMDUE_CURRENT_SHUNT_COEFF_1,		SMDUE_VAL_TYPE_W_SHUNT1_A	,},
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT2, 	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT2, 		SMDUE_VOLT_SHUNT_COEFF_2,			SMDUE_VAL_TYPE_W_SHUNT2_MV,},
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR2, 	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR2, 		SMDUE_CURRENT_SHUNT_COEFF_2,		SMDUE_VAL_TYPE_W_SHUNT2_A	,},
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT3, 	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT3, 		SMDUE_VOLT_SHUNT_COEFF_3,			SMDUE_VAL_TYPE_W_SHUNT3_MV,},
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR3,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR3,		SMDUE_CURRENT_SHUNT_COEFF_3,		SMDUE_VAL_TYPE_W_SHUNT3_A,},
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT4, 	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT4, 		SMDUE_VOLT_SHUNT_COEFF_4,			SMDUE_VAL_TYPE_W_SHUNT4_MV,},
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR4, 	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR4, 		SMDUE_CURRENT_SHUNT_COEFF_4,		SMDUE_VAL_TYPE_W_SHUNT4_A,},
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT5, 	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT5, 		SMDUE_VOLT_SHUNT_COEFF_5,			SMDUE_VAL_TYPE_W_SHUNT5_MV,},
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR5, 	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR5, 		SMDUE_CURRENT_SHUNT_COEFF_5,		SMDUE_VAL_TYPE_W_SHUNT5_A,},
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT6, 	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT6, 		SMDUE_VOLT_SHUNT_COEFF_6,			SMDUE_VAL_TYPE_W_SHUNT6_MV,},
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR6, 	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR6, 		SMDUE_CURRENT_SHUNT_COEFF_6,		SMDUE_VAL_TYPE_W_SHUNT6_A,},
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT7, 	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT7, 		SMDUE_VOLT_SHUNT_COEFF_7,			SMDUE_VAL_TYPE_W_SHUNT7_MV,},
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR7, 	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR7, 		SMDUE_CURRENT_SHUNT_COEFF_7,		SMDUE_VAL_TYPE_W_SHUNT7_A,},
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT8, 	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT8, 		SMDUE_VOLT_SHUNT_COEFF_8,			SMDUE_VAL_TYPE_W_SHUNT8_MV,},
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR8,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR8,		SMDUE_CURRENT_SHUNT_COEFF_8,		SMDUE_VAL_TYPE_W_SHUNT8_A,},
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT9, 	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT9, 		SMDUE_VOLT_SHUNT_COEFF_9,			SMDUE_VAL_TYPE_W_SHUNT9_MV,},
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR9, 	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR9, 		SMDUE_CURRENT_SHUNT_COEFF_9,		SMDUE_VAL_TYPE_W_SHUNT9_A,},
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT10, SMDUE_PARAM_SIG_ID_UNIT_SHUNT_VOLT10, 	SMDUE_VOLT_SHUNT_COEFF_10,			SMDUE_VAL_TYPE_W_SHUNT10_MV,},
 		{EQUIP_ID_SMDUE_UNIT1,1,	SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR10, SMDUE_PARAM_SIG_ID_UNIT_SHUNT_CURR10,		SMDUE_CURRENT_SHUNT_COEFF_10,		SMDUE_VAL_TYPE_W_SHUNT10_A,},

		{0, 0, SMDUE_PARAM_SIG_ID_END_FLAG, SMDUE_PARAM_SIG_ID_END_FLAG, 0, 0,},
	};

		i = 0;
		while(SmdueParamIdx[i].iSigId  != SMDUE_PARAM_SIG_ID_END_FLAG)
		{
			iSigId = SmdueParamIdx[i].iSigId;
			
			fParam = GetFloatSigValue((SmdueParamIdx[i].iEquipId + iAddr) , 
										SIG_TYPE_SETTING,
										iSigId,
										"CAN_SAMP");

			fSampleValue = g_CanData.aRoughDataSmdue[iAddr][SmdueParamIdx[i].iRoughData].fValue;


			if(FLOAT_NOT_EQUAL(fParam, fSampleValue))
			{
					printf("Smdue_Shunt_Coeff_Unify():Not equal ,iAddr = %d, i =%d,fParam=%f,fSampleValue=%f~~\n",iAddr,i,fParam,fSampleValue);

					PackAndSendSmdueCmd(MSG_TYPE_RQST_SETTINGS,
										       iAddr,
											CAN_CMD_TYPE_P2P,
											uiValueTypeH,
											SmdueParamIdx[i].uiValueTypeL,
											fParam,
											uiCANPort);
					Sleep(100);
			}
			else
			{
					printf("Smdue_Shunt_Coeff_Unify():Equal ,iAddr = %d, i =%d,fParam=%f,fSampleValue=%f~~\n",iAddr,i,fParam,fSampleValue);
			}
			
			i++;
		}
		
	printf("\n\n");
	
	return;
}


/*==========================================================================*
* FUNCTION : Smdue_PowerOn_CommLoss_Unify 
* PURPOSE  : SMDUE unify the shunt and fuse alarm mode when Power on and communication fail 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel hu                DATE: 2018-12-7 
*==========================================================================*/
void Smdue_PowerOn_CommLoss_Unify(void)
{
	int i=0;

	if( TRUE == g_CanData.bSmdueStartUnify)
	{
		for(i=0; i<MAX_NUM_SMDUE; i++)
		{
			if ( SMDUE_EQUIP_EXISTENT == g_CanData.aRoughDataSmdue[i][SMDUE_EXISTENCE ].iValue)
			{
				//printf("g_CanData.iSmdueParamUnifyConter[%d]=%d,g_CanData.iSmdueCommLossToParamUnify[%d]=%d~~\n",i,g_CanData.iSmdueParamUnifyConter[i],i,g_CanData.iSmdueCommLossToParamUnify[i]);
				
				if( g_CanData.iSmdueParamUnifyConter[i] < SMDUE_PARAM_UNIFY_AT_MOST_TIMES  )
				{
					g_CanData.iSmdueParamUnifyConter[i]++;  
				}	
			
				if( COM_LOST_TO_PARAM_UNIFY == g_CanData.iSmdueCommLossToParamUnify[i] ) 
				{
					g_CanData.iSmdueParamUnifyConter[i] = 1;
				}

				if( ( g_CanData.iSmdueParamUnifyConter[i] >1 ) && (g_CanData.iSmdueParamUnifyConter[i] < SMDUE_PARAM_UNIFY_AT_MOST_TIMES) )
				{
					Smdue_Shunt_Coeff_Unify(i);
					Smdue_Fuse_Alarm_Mode_Unify(i);
					Smdue_AI_Ch_Type_Unify(i);
				}
			}
		}
	}
	
	return;
}

/*==========================================================================*
* FUNCTION : GetSmdueAddrByChnNo 
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel hu                DATE: 2018-12-7 
*==========================================================================*/
int GetSmdueAddrByChnNo(IN int iChannelNo)
{
	int i;

	int iSeqNo = (iChannelNo - CNMR_MAX_SMDUE_BROADCAST_CMD_CHN - 1) 
		/ MAX_CHN_NUM_PER_SMDUE;
	

	for(i = 0; i < CAN_SMDUE_MAX_NUM; i++)
	{
		if(iSeqNo == i)
		{
			return i;
		}
	}

	return 0;
}

/*==========================================================================*
* FUNCTION : SetChannel_Ai_Type_Smdue 
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel hu                DATE: 2018-12-7 
*==========================================================================*/
void SetChannel_Ai_Type_Smdue(IN int SubChnNo,IN float fParam,IN int iAddr,IN UINT uiCANPort)
{
	UINT ValueTypeH  = 0;
	UINT ValueTypeL  = 0 ; 
	UINT iResendCount = 0;
	UINT uiDestAddr = iAddr;
	UINT j = 0;
	int iSetValue;
	 
	ValueTypeH  = 0x00;
	ValueTypeL = SMDUE_CHANNEL_DEFINE;

	iSetValue=(int)fParam;  //why not use floor(x) ?

	/*
	for(i=0; i<MAX_CHANEL_NUM_SMDUE; i++)
	{
		iSigId = SIG_ID_SMDUE_AI_TYPE+i;
		iTemp = GetEnumSigValue(EQUIP_ID_SMDUE_UNIT1+iAddr, SIG_TYPE_SETTING,iSigId,"CAN_SAMP");

		if( (iTemp > 0) && (iTemp < 5) )
		{
			iTemp = iTemp -1;
		}
		else
		{
			iTemp = 0;
		}
		
		iAiType	= iTemp;
		iSetValue |= (iAiType & 0x07) << (i*3);
	}


	iResendCount =1;
	for(j = 0; j < iResendCount; j++)
	{
		PackAndSendSmdueCmd_AlarmMode(MSG_TYPE_RQST_SETTINGS,
								 	iAddr,
									CAN_CMD_TYPE_P2P,
									ValueTypeH,
									ValueTypeL,
									iSetValue,
									uiCANPort);
		Sleep(50);
	}
	*/
		

	g_CanData.iSmdueParamUnifyConter[uiDestAddr] = 1;
	
	printf("SetChannel_Ai_Type_Smdue():uiDestAddr=%d,ValueTypeH=%d,ValueTypeL=%d,fParam=%f~~~\n",uiDestAddr,ValueTypeH,ValueTypeL,fParam);
	
	return;
}


/*==========================================================================*
* FUNCTION : SetShunt_Coeff_Smdue 
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel hu                DATE: 2018-12-7 
*==========================================================================*/
void SetShunt_Coeff_Smdue(IN int SubChnNo,IN float fParam,IN int iAddr,IN UINT uiCANPort)
{
	UINT ValueTypeH  = 0;
	UINT ValueTypeL  = 0 ; 
	UINT iResendCount = 0;
	UINT uiDestAddr = iAddr;
	UINT j = 0;

	 
	ValueTypeH  = 0x01;
	ValueTypeL = SubChnNo -CNMR_MAX_SMDUE_BROADCAST_CMD_CHN-1;

	iResendCount =1;
	for(j = 0; j < iResendCount; j++)
	{
		PackAndSendSmdueCmd(MSG_TYPE_RQST_SETTINGS,
			uiDestAddr,
			CAN_CMD_TYPE_P2P,
			ValueTypeH,
			ValueTypeL,
			fParam,
			uiCANPort);
		Sleep(50);
	}

	g_CanData.iSmdueParamUnifyConter[uiDestAddr] = 1;
	
	printf("SetShunt_Coeff_Smdue()-1:uiDestAddr=%d,ValueTypeH=%d,ValueTypeL=%d,fParam=%f~~~\n",uiDestAddr,ValueTypeH,ValueTypeL,fParam);
	
	return;
}

/*==========================================================================*
* FUNCTION : SMDUE_SendCtlCmd 
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel hu                DATE: 2018-12-7 
*==========================================================================*/
void SMDUE_SendCtlCmd(IN int iChannelNo, IN float fParam)
{
	int	iSmdueAddr,iSubChnNo,iSeqNo;
	
	UINT uiCANPort = SMDUEonCAN1;

	if(iChannelNo < CNMR_MAX_SMDUE_BROADCAST_CMD_CHN)
	{
		switch(iChannelNo)
		{
			default:

				CAN_LOG_OUT(APP_LOG_WARNING, "SMDUE_SendCtlCmd():Invalid control channel!\n");
				break;
		}
		return;
	}
	else
	{
		iSmdueAddr = GetSmdueAddrByChnNo(iChannelNo);//0~7

		iSubChnNo = ((iChannelNo - CNMR_MAX_SMDUE_BROADCAST_CMD_CHN - 1)
			% MAX_CHN_NUM_PER_SMDUE) 
			+ CNMR_MAX_SMDUE_BROADCAST_CMD_CHN 
			+ 1;

		if (( iSubChnNo >=  CNMR_SMDUE_VOLT_SHUNT_COEFF1 ) && ( iSubChnNo <=  CNMR_SMDUE_VOLT_SHUNT_COEFF10 ) )
		{
			 SetShunt_Coeff_Smdue(iSubChnNo,fParam,iSmdueAddr,uiCANPort);
		}
		else if( ( iSubChnNo >=  CNMR_SMDUE_CH1_AI_TYPE ) && ( iSubChnNo <=  CNMR_SMDUE_CH10_AI_TYPE ) )
		{
			SetChannel_Ai_Type_Smdue(iSubChnNo,fParam,iSmdueAddr,uiCANPort);
		}
		else
		{
			
		}	
	}

	return;
}



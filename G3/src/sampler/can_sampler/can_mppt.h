/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : NGMC-HF(Next Generation Controller - High Feature)
 *
 *  FILENAME : can_rectifier.h
 *  CREATOR  : Frank Cao                DATE: 2008-06-24 16:47
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/


#ifndef __CAN_SAMPLER_MPPT_H
#define __CAN_SAMPLER_MPPT_H

#include <stdio.h>

#include "basetypes.h"
#include "err_code.h"
#include "pubfunc.h"
#include "string.h"
#include "can_sampler_main.h"

#define MT_DC_TYPE_SAMP_SINGLE				0
#define MT_DC_TYPE_SAMP_SMALL				1
#define MT_DC_TYPE_SAMP_DOUBLE				2
									
#define MT_DC_TYPE_RETURN_SINGLE			1
#define MT_DC_TYPE_RETURN_SMALL			2
#define MT_DC_TYPE_RETURN_DOUBLE			0
									

									
									
#define	MT_COMM_NORMAL_ST				0
#define	MT_COMM_ALL_INTERRUPT_ST		1
#define	MT_COMM_INTERRUPT_ST			2
									
									
#define MT_EQUIP_EXISTENT				0
#define MT_EQUIP_NOT_EXISTENT			1
									
#define MT_MAX_INTERRUPT_TIMES			2
									
#define MT_CMD00_RCV_FRAMES_NUM			9
#define MT_CMD20_RCV_FRAMES_NUM			7


#define MT_DC_TYPE_SAMP_SINGLE				0
#define MT_DC_TYPE_SAMP_SMALL				1
#define MT_DC_TYPE_SAMP_DOUBLE				2

#define MT_DC_TYPE_RETURN_SINGLE			1
#define MT_DC_TYPE_RETURN_SMALL			2
#define MT_DC_TYPE_RETURN_DOUBLE			0


#define	BT_GROUP_EQUIPID				115
#define	BT_BATT_DISCH_SIGID				32

#define	MT_GROUP_EQUIPID				1350
#define MT_START_EQUIP_ID				1351
#define MT_POS_SET_SIG_ID				1
#define MT_PHASE_SET_SIG_ID				2
#define MT_CONFIRM_POS_PHASE_SIGID		21
#define MT_FLOAT_VOLT_SIGID				16
#define MT_OVER_VOLTAGE_SIGID			3
#define MT_LAST_RUN_ON_NUMBER_SIGID			18

#define MT_SEQ_START_INTERVAL_SIGID		31
#define MT_OV_RESTART_ENB_SIGID			30
#define MT_OV_RESTART_INTERVAL_SIGID	6
#define MT_WALKIN_TIME_SIGID			7
#define MT_WALKIN_ENB_SIGID				8
#define MT_AC_OV_RESTART_ENB_SIGID		19
#define MT_FULL_POWER_ENB_SIGID			38


#define	MT_DC_ON						0
#define	MT_DC_OFF						1
#define	MT_RESET_NORMAL					0
#define	MT_RESET_ENB					1
#define	MT_WALKIN_NORMAL				0
#define	MT_WALKIN_ENB					1
#define	MT_FAN_AUTO						0
#define	MT_FAN_FULL_SPEED				1
#define	MT_LED_NORMAL					0
#define	MT_LED_BLINK					1
#define	MT_AC_ON						0
#define	MT_AC_OFF						1
#define	MT_CAN_NORMAL					0
#define	MT_CAN_INIT						1
#define	MT_OV_RESET_DISB				0
#define	MT_OV_RESET_ENB					1
#define	MT_LED_NOT_BLINK				0
#define	MT_LED_BLINK					1
#define	MT_AC_OV_PROTECT_ENB			0
#define	MT_AC_OV_PROTECT_DISB			1
#define	MT_DEGRADATIVE					0
#define	MT_FULL_POWER_CAP_ENB			1


#define	MPPT_POSITION_INVAILID	(-1)
#define	AC_PHASE_INVAILID		(-1)

#define	MT_AC_NORMAL		0
#define	MT_AC_LOW_VOLT		1
#define	MT_AC_FAILURE		2
#define	MT_AC_FAIL_POINT	(80.0)

//#define MAX_NUM_MPPT		16

#define MT_STATUS_DAY			0
#define MT_STATUS_NIGHT			1

#define MT_SIG_EXISTANCE				0
#define MT_SIG_INEXISTANCE				1

enum MPPT_SAMP_CHANNEL
{
	//SYS_CH_CAN_INTERRUPT = 20,	//CAN all no response

	MT_CH_MPPT_NUM = 50,		//Total Rectifier Number
	MT_CH_RATED_VOLT,			//Rated Voltage
	MT_CH_RATED_CURR,			//Rated Current
	
	MT_CH_DC_DC_TYPE = 59,			//Monitoring Board Power Supply Type
	MT_CH_DAY_NIGHT_STATUS,			//All rectifers AC failure
	MT_CH_INPUT_RATED_VOLT,
	MT_CH_ALL_NO_RESPONSE = 62,		//All rectifers no response
	MT_CH_GROUP_EXIST_STATUS = 63,			//No used
	
	

	MT_CH_DC_VOLT = 101,		//Output Voltage
	MT_CH_ACTUAL_CURR,		//Actual Current
	MT_CH_CURR_LMT,			//Current Limitation
	MT_CH_TEMPERATURE,		//Temperature
	MT_CH_AC_VOLT,			//AC Input Voltage
	MT_CH_HIGH_VOLT,		//Over Voltage Limit
	MT_CH_INPUTCURRENT,		//Current for display
	MT_CH_SERIAL_NO_LOW,		//Serial No.
	MT_CH_HIGH_SERIAL_NO,		//High Serial No.
	MT_CH_INTERRUPT_ST,		//Interrupt State
	MT_CH_DAY_NIGHT_ST,		//AC Failure 
	MT_CH_OVER_VOLT_ST,		//DC Over Voltage Status
	MT_CH_FAULT_ST,			//Fault Status
	MT_CH_PROTECTION_ST,		//Protection Status
	MT_CH_FAN_FAULT_ST,		//Fan Fault Status
	MT_CH_EEPROM_FAULT_ST,		//EEprom Fault Status
	MT_CH_VALID_RATED_CURR,		//Valid rated current, before it is "Power limited for AC"
	MT_CH_EFFICIENCY_NO_ST,		//Rectifer Efficiency, before it is "Power limited for temperature"
	MT_CH_POWER_LMT_ST,		//Power limited
	MT_CH_DC_ON_OFF_ST,		//DC On/off status
	MT_CH_FAN_FULL_SPEED_ST,	//Fan full speed status
	MT_CH_WALKIN_ST,		//Walk-in status
	MT_CH_AC_ON_OFF_ST,		//AC on/off status
	MT_CH_SHARE_CURR_ST,		//Share current status
	MT_CH_OVER_TEMP_ST,		//Over temperature Status
	MT_CH_VER_NO,			//Version
	MT_CH_RUN_TIME,			//Total Run Time
	MT_CH_SEVERE_CURR_IMB,		//Severe current imbalance
	MT_CH_INPUT_POWER,		//Input Power
	MT_CH_INPUT_NOT_DC,		//
	MT_CH_OUTPUT_POWER,		//Output Power
	MT_CH_POS_NO,			//Position No.
	MT_CH_RESERVE4,			//Phase No.
	MT_CH_BARCODE1,			//Barcode1
	MT_CH_BARCODE2,			//Barcode2
	MT_CH_BARCODE3,			//Barcode3
	MT_CH_BARCODE4,			//Barcode4
	MT_CH_RESERVE5,			//Before it was E-Stop State
	MT_CH_EXIST_STATE,
	MT_CH_CURR_IMB,

};

enum MPPT_TYPE
{
	MPPT48V50A,
	MPPT336V,
	MPPT48V35A,
	MPPT400V,
};

//Please refer to the document of CAN protocol
//Following is the data to be read from rectifier
/////////////////////////////////////////////
#define MT_VAL_TYPE_R_DC_VOLT			0x01
#define MT_VAL_TYPE_R_REAL_CURR			0x02
#define MT_VAL_TYPE_R_CURR_LMT			0x03
#define MT_VAL_TYPE_R_TEMPERATURE		0x04
#define MT_VAL_TYPE_R_AC_VOLT			0x05
#define MT_VAL_TYPE_R_HIGH_VOLT			0x06
#define MT_VAL_TYPE_R_DISP_CURR			0x07
#define MT_VAL_TYPE_R_PFC0_VOLT			0x08
#define MT_VAL_TYPE_R_HIGH_TEMP			0x09
#define MT_VAL_TYPE_R_OUTPUT_POWER		0x0A
#define MT_VAL_TYPE_R_U1BOARD_TEMP		0x0B
#define MT_VAL_TYPE_R_P_A_VOLT			0x0C
#define MT_VAL_TYPE_R_P_B_VOLT			0x0D
#define MT_VAL_TYPE_R_P_C_VOLT			0x0E
#define MT_VAL_TYPE_R_RATED_VOLT		0x0F
#define MT_VAL_TYPE_R_PFC_TEMP			0x10
#define MT_VAL_TYPE_R_RATED_POWER		0x11
#define MT_VAL_TYPE_R_RATED_CURR		0x12
#define MT_VAL_TYPE_R_RATED_INPUT_VOLT	0x13
#define MT_VAL_TYPE_R_POWER_LMT_POINT	0x14

#define MT_VAL_TYPE_R_ALARM_BITS		0x40
#define MT_VAL_TYPE_R_ANALOG_ALARM		0x41
#define MT_VAL_TYPE_R_OTHER_ALARM		0x42

#define MT_VAL_TYPE_R_FEATURE			0x51

#define MT_VAL_TYPE_R_SN_LOW			0x54
#define MT_VAL_TYPE_R_SN_HIGH			0x55
#define MT_VAL_TYPE_R_VER_NO			0x56

#define MT_VAL_TYPE_R_RUN_TIME			0x58

#define MT_VAL_TYPE_R_BARCODE1			0x5A
#define MT_VAL_TYPE_R_BARCODE2			0x5B
#define MT_VAL_TYPE_R_BARCODE3			0x5C
#define MT_VAL_TYPE_R_BARCODE4			0x5D
////////////////////////////////////////////

//Following is the data to be wrote to rectifier
#define MT_VAL_TYPE_W_ESTOP_ENB			0x16
#define MT_VAL_TYPE_W_CURR_LMT			0x22
#define MT_VAL_TYPE_W_HIGH_VOLT			0x23
#define MT_VAL_TYPE_W_DC_VOLT			0x21
#define MT_VAL_TYPE_W_DEFAULT_VOLT		0x24
#define	MT_VAL_TYPE_W_MPPT_DELTA_VOLTAGE	0x27		//Error!! It should be 0x2F
#define MT_VAL_TYPE_W_RESTAMT_TIME		0x28	//restart time on over voltage
#define MT_VAL_TYPE_W_WALKIN_TIME		0x29	//the time of soft time with load
#define MT_VAL_TYPE_W_SEQ_STAMT_TIME	0x2A	//Sequence start interval
#define	MT_VAL_TYPE_W_FULL_POWER_ENB	0x2F
#define MT_VAL_TYPE_W_DC_ON_OFF			0x30
#define MT_VAL_TYPE_W_RESTAMT_ON_OV		0x31
#define MT_VAL_TYPE_W_WALKIN_ENB		0x32
#define MT_VAL_TYPE_W_FAN_FULL_SPEED	0x33
#define MT_VAL_TYPE_W_LED_BLINK			0x34
#define MT_VAL_TYPE_W_AC_ON_OFF			0x35
#define MT_VAL_TYPE_W_RUN_TIME			0xA4
#define MT_VAL_TYPE_W_AC_CURR_LIMIT		0x1a	//Input current limit

#define MT_VAL_TYPE_NO_TRIM_VOLT		0x80	//used while MsgType = 0x00, 0x10, 0x20
#define MT_VAL_TYPE_INPUT_POWER			0xd3

#define ID_TIMER_MPPT_INTERRUPT			0
#define ID_TIMER_MPPT_CALC_ALARM_1HOUR		(ID_TIMER_MPPT_INTERRUPT + MAX_NUM_MPPT)
#define ID_TIMER_MPPT_CALC_ALARM_20MIN		(ID_TIMER_MPPT_INTERRUPT + 2 * MAX_NUM_MPPT)
#define MT_ONE_HOUR				(60*60*1000)
#define MT_20_MINUTES				(20*60*1000)
#define MT_3_DAYS				(3*24*60*60*1000)
#define MT_1_DAY				(24*60*60*1000)
#define GC_MIN_VOLTAGE				(60.0)

struct	tagMPPT_TYPE_INFO
{
	UINT		uiTypeNo;
	float		fRatedVolt;
	float		fRatedCurr;
	int			iDcDcType;
	int			iAcInputType;
	float		fAcRatedVolt;
	float		fPower;
	int			iEfficiencyNo;
};
typedef struct tagMPPT_TYPE_INFO MPPT_TYPE_INFO;

struct	tagMPPT_POS_ADDR
{
	DWORD			dwSerialNo;//��Barcode1����
	DWORD			dwHiSN; 
	int			iPositionNo;
	int			iPhaseNo;
	BOOL			bIsNewInserted; //�Ƿ��²����
	int			iSeqNo; //���ź��λ��
};
typedef struct tagMPPT_POS_ADDR MPPT_POS_ADDR;


void MT_Reconfig(void);
void MT_TriggerAllocation(void);
void MT_InitPosAndPhase(void);
void MT_InitRoughValue(void);
void MT_SendCtlCmd(int iChannelNo, float fParam);
void MT_Sample(void);
void MT_StuffChannel(ENUMSIGNALPROC EnumProc,	//Callback function for stuffing channels
					 LPVOID lpvoid);			//Parameter of the callback function
BOOL MpptIsNeedReConfig();
INT32 MT_WaitAllocation();
BOOL	MtCheckConvertExist();
VOID Mppt_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI);

#endif

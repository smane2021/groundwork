#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <time.h>


#include "can_sampler_main.h"
#include "can_smdup.h"

#define	SMDUP_GROUP_EQUIPID		642
extern	CAN_SAMPLER_DATA		g_CanData;
G_CAN_SMDUP_SAMP			g_can_smduplus;
#define SMDUP_SHUNT_MAX_NUM	25
static float g_fSMDUPShuntReadings[CAN_SMDUP_MAX_NUM][SMDUP_SHUNT_MAX_NUM];
static BOOL g_bIsSMDUEIBMode = FALSE;
//Please refer to the protocol document
const ANALYSE_DATA_STRUCT st_SmpMSG20Data[]=
{
	//Pst/iValueType/iSignalType/iRoughDataIdx
	{0,  0x0051, SMDUP_SIG_TYPE_ULONG, SM_SMDUP_FEATURE,		SMDUP_CMD20_RCV_FRAMES_NUM},
	{13, 0x0054, SMDUP_SIG_TYPE_ULONG, SM_SMDUP_SERIAL_NO_LOW,	SMDUP_CMD20_RCV_FRAMES_NUM},	
	{26, 0x0056, SMDUP_SIG_TYPE_ULONG, SM_SMDUP_VERSION_NO,		SMDUP_CMD20_RCV_FRAMES_NUM},
	{39, 0x005A, SMDUP_SIG_TYPE_ULONG, SM_SMDUP_BARCODE1,		SMDUP_CMD20_RCV_FRAMES_NUM},	
	{52, 0x005B, SMDUP_SIG_TYPE_ULONG, SM_SMDUP_BARCODE2,		SMDUP_CMD20_RCV_FRAMES_NUM},		
	{65, 0x005C, SMDUP_SIG_TYPE_ULONG, SM_SMDUP_BARCODE3,		SMDUP_CMD20_RCV_FRAMES_NUM},		
	{78, 0x005D, SMDUP_SIG_TYPE_ULONG, SM_SMDUP_BARCODE4,		SMDUP_CMD20_RCV_FRAMES_NUM},
	{-1,-1,		-1,		-1,										SMDUP_CMD20_RCV_FRAMES_NUM},
};

//Please refer to the protocol document
const ANALYSE_DATA_STRUCT st_SmpMSG24Data[]=
{
	//Pst/iValueType/iSignalType/iRoughDataIdx
	{0,  0x0100, SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT1, SMDUP_CMD24_RCV_FRAMES_NUM},
	{13, 0x0101, SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT2, SMDUP_CMD24_RCV_FRAMES_NUM},	
	{26, 0x0102, SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT3, SMDUP_CMD24_RCV_FRAMES_NUM},
	{39, 0x0103, SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT4, SMDUP_CMD24_RCV_FRAMES_NUM},	
	{52, 0x0104, SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT5, SMDUP_CMD24_RCV_FRAMES_NUM},		
	{65, 0x0105, SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT6, SMDUP_CMD24_RCV_FRAMES_NUM},		
	{78, 0x0106, SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT7, SMDUP_CMD24_RCV_FRAMES_NUM},
	{91, 0x0107, SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT8, SMDUP_CMD24_RCV_FRAMES_NUM},		
	{104,0x0108, SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT9, SMDUP_CMD24_RCV_FRAMES_NUM},		
	{117,0x0109, SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT10,SMDUP_CMD24_RCV_FRAMES_NUM},
	{-1,	-1,			-1,				-1				 ,SMDUP_CMD24_RCV_FRAMES_NUM},
};

//Please refer to the protocol document
const ANALYSE_DATA_STRUCT st_SmpMSG25Data[]=
{
	//Pst/iValueType/iSignalType/iRoughDataIdx
	{0,	0x010A,	SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT11, SMDUP_CMD25_RCV_FRAMES_NUM},
	{13,	0x010B,	SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT12, SMDUP_CMD25_RCV_FRAMES_NUM},	
	{26,	0x010C,	SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT13, SMDUP_CMD25_RCV_FRAMES_NUM},
	{39,	0x010D,	SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT14, SMDUP_CMD25_RCV_FRAMES_NUM},	
	{52,	0x010E,	SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT15, SMDUP_CMD25_RCV_FRAMES_NUM},		
	{65,	0x010F,	SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT16, SMDUP_CMD25_RCV_FRAMES_NUM},		
	{78,	0x0110,	SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT17, SMDUP_CMD25_RCV_FRAMES_NUM},
	{91,	0x0111,	SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT18, SMDUP_CMD25_RCV_FRAMES_NUM},		
	{104,	0x0112,	SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT19, SMDUP_CMD25_RCV_FRAMES_NUM},		
	{117,	0x0113,	SMDUP_SIG_TYPE_FLOAT, SM_SMDUP_CURRENT20, SMDUP_CMD25_RCV_FRAMES_NUM},
	{-1,	-1,			-1,					-1				, SMDUP_CMD25_RCV_FRAMES_NUM},
};

//Please refer to the protocol document
const ANALYSE_DATA_STRUCT st_SmpMSG26Data[]=
{
	//Pst/iValueType/iSignalType/iRoughDataIdx
	{0,	0x0114,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_CURRENT21, SMDUP_CMD26_RCV_FRAMES_NUM},
	{13,	0x0115,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_CURRENT22, SMDUP_CMD26_RCV_FRAMES_NUM},	
	{26,	0x0116,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_CURRENT23, SMDUP_CMD26_RCV_FRAMES_NUM},
	{39,	0x0117,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_CURRENT24, SMDUP_CMD26_RCV_FRAMES_NUM},	
	{52,	0x0118,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_CURRENT25, SMDUP_CMD26_RCV_FRAMES_NUM},		
	{-1,	-1,			-1,						-1,					SMDUP_CMD26_RCV_FRAMES_NUM},
};

//Please refer to the protocol document
const ANALYSE_DATA_STRUCT st_SmpMSG27Data[]=
{
	//Pst/iValueType/iSignalType/iRoughDataIdx
	{0,	0x0119,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT1, SMDUP_CMD27_RCV_FRAMES_NUM},
	{13,	0x011A,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT2, SMDUP_CMD27_RCV_FRAMES_NUM},	
	{26,	0x011B,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT3, SMDUP_CMD27_RCV_FRAMES_NUM},
	{39,	0x011C,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT4, SMDUP_CMD27_RCV_FRAMES_NUM},	
	{52,	0x011D,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT5, SMDUP_CMD27_RCV_FRAMES_NUM},		
	{65,	0x011E,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT6, SMDUP_CMD27_RCV_FRAMES_NUM},		
	{78,	0x011F,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT7, SMDUP_CMD27_RCV_FRAMES_NUM},
	{91,	0x0120,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT8, SMDUP_CMD27_RCV_FRAMES_NUM},		
	{104,	0x0121,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT9, SMDUP_CMD27_RCV_FRAMES_NUM},		
	{117,	0x0122,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT10,SMDUP_CMD27_RCV_FRAMES_NUM},
	{-1,	-1,			-1,						-1,				SMDUP_CMD27_RCV_FRAMES_NUM},
};

//Please refer to the protocol document
const ANALYSE_DATA_STRUCT st_SmpMSG28Data[]=
{
	//Pst/iValueType/iSignalType/iRoughDataIdx
	{0,	0x0123,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT11, SMDUP_CMD28_RCV_FRAMES_NUM},
	{13,	0x0124,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT12, SMDUP_CMD28_RCV_FRAMES_NUM},	
	{26,	0x0125,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT13, SMDUP_CMD28_RCV_FRAMES_NUM},
	{39,	0x0126,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT14, SMDUP_CMD28_RCV_FRAMES_NUM},	
	{52,	0x0127,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT15, SMDUP_CMD28_RCV_FRAMES_NUM},		
	{65,	0x0128,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT16, SMDUP_CMD28_RCV_FRAMES_NUM},		
	{78,	0x0129,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT17, SMDUP_CMD28_RCV_FRAMES_NUM},
	{91,	0x012A,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT18, SMDUP_CMD28_RCV_FRAMES_NUM},		
	{104,	0x012B,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT19, SMDUP_CMD28_RCV_FRAMES_NUM},		
	{117,	0x012C,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT20, SMDUP_CMD28_RCV_FRAMES_NUM},
	{-1,	-1,			-1,						-1,				 SMDUP_CMD28_RCV_FRAMES_NUM},
};

//Please refer to the protocol document
const ANALYSE_DATA_STRUCT st_SmpMSG29Data[]=
{
	//Pst/iValueType/iSignalType/iRoughDataIdx
	{0,	0x012D,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT21, SMDUP_CMD29_RCV_FRAMES_NUM},
	{13,	0x012E,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT22, SMDUP_CMD29_RCV_FRAMES_NUM},	
	{26,	0x012F,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT23, SMDUP_CMD29_RCV_FRAMES_NUM},
	{39,	0x0130,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT24, SMDUP_CMD29_RCV_FRAMES_NUM},	
	{52,	0x0131,		SMDUP_SIG_TYPE_FLOAT,	SM_SMDUP_VOLT25, SMDUP_CMD29_RCV_FRAMES_NUM},		
	{-1,	-1,			-1,						-1,				 SMDUP_CMD29_RCV_FRAMES_NUM},
};
//added by Jimmy
//Please refer to the protocol document
const ANALYSE_DATA_STRUCT st_SmpMSG2AData[]=
{
	//Pst/iValueType/iSignalType/iRoughDataIdx
	{0,	0x0133,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE1, SMDUP_CMD2A_RCV_FRAMES_NUM},
	{13,	0x0134,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE2, SMDUP_CMD2A_RCV_FRAMES_NUM},	
	{26,	0x0135,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE3, SMDUP_CMD2A_RCV_FRAMES_NUM},
	{39,	0x0136,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE4, SMDUP_CMD2A_RCV_FRAMES_NUM},	
	{52,	0x0137,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE5, SMDUP_CMD2A_RCV_FRAMES_NUM},		
	{65,	0x0138,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE6, SMDUP_CMD2A_RCV_FRAMES_NUM},		
	{78,	0x0139,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE7, SMDUP_CMD2A_RCV_FRAMES_NUM},
	{91,	0x013A,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE8, SMDUP_CMD2A_RCV_FRAMES_NUM},		
	{104,	0x013B,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE9, SMDUP_CMD2A_RCV_FRAMES_NUM},		
	{117,	0x013C,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE10, SMDUP_CMD2A_RCV_FRAMES_NUM},
	{-1,	-1,			-1,						-1,				 SMDUP_CMD28_RCV_FRAMES_NUM},
};
const ANALYSE_DATA_STRUCT st_SmpMSG2BData[]=
{
	//Pst/iValueType/iSignalType/iRoughDataIdx
	{0,	0x013D,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE11, SMDUP_CMD2B_RCV_FRAMES_NUM},
	{13,	0x013E,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE12, SMDUP_CMD2B_RCV_FRAMES_NUM},	
	{26,	0x013F,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE13, SMDUP_CMD2B_RCV_FRAMES_NUM},
	{39,	0x0140,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE14, SMDUP_CMD2B_RCV_FRAMES_NUM},	
	{52,	0x0141,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE15, SMDUP_CMD2B_RCV_FRAMES_NUM},		
	{65,	0x0142,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE16, SMDUP_CMD2B_RCV_FRAMES_NUM},		
	{78,	0x0143,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE17, SMDUP_CMD2B_RCV_FRAMES_NUM},
	{91,	0x0144,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE18, SMDUP_CMD2B_RCV_FRAMES_NUM},		
	{104,	0x0145,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE19, SMDUP_CMD2B_RCV_FRAMES_NUM},		
	{117,	0x0146,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE20, SMDUP_CMD2B_RCV_FRAMES_NUM},
	{-1,	-1,			-1,						-1,				 SMDUP_CMD28_RCV_FRAMES_NUM},
};
const ANALYSE_DATA_STRUCT st_SmpMSG2CData[]=
{
	//Pst/iValueType/iSignalType/iRoughDataIdx
	{0,	0x0147,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE21, SMDUP_CMD2C_RCV_FRAMES_NUM},
	{13,	0x0148,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE22, SMDUP_CMD2C_RCV_FRAMES_NUM},	
	{26,	0x0149,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE23, SMDUP_CMD2C_RCV_FRAMES_NUM},
	{39,	0x014A,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE24, SMDUP_CMD2C_RCV_FRAMES_NUM},	
	{52,	0x014B,		SMDUP_SIG_TYPE_ULONG,	ROUGH_SMDUP_SHUNT_SIZE25, SMDUP_CMD2C_RCV_FRAMES_NUM},		
	{-1,	-1,			-1,						-1,				 SMDUP_CMD28_RCV_FRAMES_NUM},
};
static void SMDUP_ReadAllShuntSize(INT32 iAddr);
/*=============================================================================*
* FUNCTION: SMDUP_InitRoughValue
* PURPOSE : Initialization smdu+ RoughData
* INPUT:	 	
*   
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void SMDUP_InitRoughValue(void)
{
	INT32 i,j,k;

	for (i = 0; i < CAN_SMDUP_MAX_NUM;i++)
	{
		for (j = 0; j < SM_SMDUP_MAX_SIG_NUM;j++)
		{
			g_can_smduplus.aRoughData[i][j].iValue = CAN_SMDUP_INVALID_VALUE;
		}

		g_can_smduplus.aRoughData[i][SM_SMDUP_EXIST_STAT].iValue
			= CAN_SMDUP_NOT_EXIT;
		g_can_smduplus.aRoughData[i][SM_SMDUP_COMM_STAT].iValue
			= CAN_SMDUP_COMM_OK;
		//初始化
		g_can_smduplus.bOldSMDUP[i] = TRUE;
	}
	g_can_smduplus.aRoughData[0][SM_SMDUP_GROUP_STATE].iValue
		= CAN_SMDUP_NOT_EXIT;
	g_can_smduplus.aRoughData[0][SM_SMDUP_NUM].iValue
		= 0;
}

/*==========================================================================*
* FUNCTION : PackAndSendSmduCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: UINT   uiMsgType    : 
*            UINT   uiDestAddr   : 
*            UINT   uiCmdType    : 
*            UINT   uiValueTypeH : 
*            UINT   uiValueTypeL : 
*            float  fParam       : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Copy Frank Cao                DATE: 2010-02-25 16:27
*==========================================================================*/
static void PackAndSendSmduCmd(UINT uiMsgType,
							   UINT uiDestAddr,
							   UINT uiCmdType,
							   UINT uiValueTypeH,
							   UINT uiValueTypeL,
							   float fParam)
{
	BYTE*	pbySendBuf = g_can_smduplus.cBySendBuf;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_SMDU_CONTROLLER,
		uiDestAddr, 
		CAN_SELF_ADDR,
		CAN_ERR_CODE,
		CAN_FRAME_DATA_LEN,
		uiCmdType,
		pbySendBuf);

	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	CAN_FloatToString(fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);	

	write(g_CanData.CanCommInfo.iCanHandle, 
		(void *)pbySendBuf,
		CAN_FRAME_LEN);

	return;
}


/*=============================================================================*
* FUNCTION: SmdupGetValueType(BYTE *pFrame)
* PURPOSE : Get uValue from One CAN Frame
*			
* INPUT:	BYTE *pFrame:	point to One Frame
*     
* RETURN:
*     INT32   iValueType
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 SmdupGetValueType(BYTE *pFrame)
{
	BYTE * pValue;
	INT32 iValueType	= 0;
	INT32 iValueTypeH	= 0;
	INT32 iValueTypeL	= 0;

	pValue = pFrame + SMDUP_FRAME_OFFSET_VALUETYPE_H;
	iValueTypeH = *(pValue);
	pValue = pFrame + SMDUP_FRAME_OFFSET_VALUETYPE_L;
	iValueTypeL = *(pValue);
	iValueType = (iValueTypeH << 8) + iValueTypeL;
	
	return iValueType;
}

/*=============================================================================*
* FUNCTION: SmdupGetuValue(BYTE *pFrame)
* PURPOSE : Get uValue from One CAN Frame
*			
* INPUT:	BYTE *pFrame:	point to One Frame
*     
* RETURN:
*     UINT   uiValue
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL UINT SmdupGetuValue(BYTE *pFrame)
{
	BYTE * pValue;
	pValue = pFrame + SMDUP_FRAME_OFFSET_VALUE;

	UINT uiValue = (((UINT)(pValue[0])) << 24)
						+ (((UINT)(pValue[1])) << 16)
						+ (((UINT)(pValue[2])) << 8)
						+ ((UINT)(pValue[3]));
	return uiValue;
}
/*=============================================================================*
* FUNCTION: SmdupGetfValue(BYTE *pFrame)
* PURPOSE : Get fValue from One CAN Frame
*			
* INPUT:	BYTE *pFrame:	point to One Frame
*     
* RETURN:
*     UINT   uiValue
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL float SmdupGetfValue(BYTE *pFrame)
{
	BYTE*				pValue;
	int					i;	
	FLOAT_STRING		unValue;
	pValue = pFrame + SMDUP_FRAME_OFFSET_VALUE;
	
	for(i = 0; i < 4; i++)
	{
		//printf(" %02X", pValue[i]);
		unValue.abyValue[4 - i - 1] = pValue[i];
	}
	//printf("float value = %f \n", unValue.fValue);

	return unValue.fValue;
}
/*=============================================================================*
* FUNCTION:	ClrSmdupIntrruptTimes
* PURPOSE : 
*					
* INPUT:	INT32 iAddr
*     
*
* RETURN:
*     void	g_can_smduplus.aRoughData
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void ClrSmdupIntrruptTimes(INT32 iAddr)
{
	INT32		iTempAddr;
	iTempAddr	= iAddr - SMDUP_ADDR_OFFSET;
	g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_COMM_STAT].iValue = CAN_SMDUP_COMM_OK;
	g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_COMM_FAIL_TIMES].iValue = 0;
}
/*=============================================================================*
* FUNCTION:	IncSmdupIntrruptTimes
* PURPOSE : 
*					
* INPUT:	INT32 iAddr
*     
*
* RETURN:
*     void	g_can_smduplus.aRoughData
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void IncSmdupIntrruptTimes(INT32 iAddr)
{
	INT32		iTempAddr;
	iTempAddr	= iAddr - SMDUP_ADDR_OFFSET;
	if (g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_COMM_FAIL_TIMES].iValue < SMDUP_B_COMMFAIL_TIME)
	{
		g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_COMM_FAIL_TIMES].iValue++;
		//printf("\n  ^^^  COMM_FAIL_TIMES = %d \n",g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_COMM_FAIL_TIMES].iValue);
	}

	if (g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_COMM_FAIL_TIMES].iValue >= SMDUP_B_COMMFAIL_TIME)
	{
		g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_COMM_STAT].iValue = CAN_SMDUP_COMM_FAIL;
	}
}


LOCAL BOOL SmdupUpackCmdXX(int VirtualiAddr,
						   int iReadLen,
						   BYTE* pbyRcvBuf,
						   ANALYSE_DATA_STRUCT stAnalyseData[])
{
	INT32		i,j,k;
	INT32		iTempAddr;
	BYTE*		pFrameofPackOffset;
	BOOL		bAllFrameSucceedFlag;
	float		fTempData;
	iTempAddr			 = VirtualiAddr;
	bAllFrameSucceedFlag = TRUE;


	INT32 iValueType ;
	//The MSG20 response package  is too short
	//if ((iReadLen/CAN_ONE_FRAME_LEN) < (sizeof(stAnalyseData)/sizeof(stAnalyseData[0]) - 1))
	if ((iReadLen/CAN_ONE_FRAME_LEN) < stAnalyseData[0].iFrameNum)
	{
		//printf("	 The MSGXX response package  is too short \n");
		return -1;
	}

	
	for (i = 0; i < stAnalyseData[0].iFrameNum; i++)
	{
		pFrameofPackOffset = pbyRcvBuf + stAnalyseData[i].iCanFrameStartPst;
		iValueType		 = SmdupGetValueType(pFrameofPackOffset);

		if (stAnalyseData[i].iValueType != iValueType)
		{
			bAllFrameSucceedFlag = FALSE;
			//printf(" The Frame receiving is fail ValueType = %d  \n",iValueType);
			continue;
		}
		else
		{
			if (stAnalyseData[i].iSignalType == SMDUP_SIG_TYPE_ULONG)
			{
				g_can_smduplus.aRoughData[iTempAddr][stAnalyseData[i].iRoughDataIdx].uiValue
					= SmdupGetuValue(pFrameofPackOffset);
			}
			else if (stAnalyseData[i].iSignalType == SMDUP_SIG_TYPE_FLOAT)
			{
				fTempData = SmdupGetfValue(pFrameofPackOffset);

				// -1 A < fTempData < 1 A,voltage no problem!!
				if ((ABS(((fTempData) - (0.0))) < 1.0))
				{
					fTempData = 0.00;
				}
				g_can_smduplus.aRoughData[iTempAddr][stAnalyseData[i].iRoughDataIdx].fValue
					= fTempData;	
				//printf("  !!!! %f  \n",g_can_smduplus.aRoughData[iTempAddr][stAnalyseData[i].iRoughDataIdx].fValue);
			}
			else if (stAnalyseData[i].iSignalType == SMDUP_SIG_TYPE_BYTE)
			{
			}
			else
			{
			}
		}
	}

	//printf(" ############ Smdup UpackCmdXX   FLAG =%d  \n",bAllFrameSucceedFlag);
	return bAllFrameSucceedFlag;

	//if (!bAllFrameSucceedFlag)
	//{
	//	return FALSE;
	//}
	//else
	//{
	//	printf(" ############ Smdup UpackCmdXX 20   okokok\n");
	//	return TRUE;
	//}
}

/*=============================================================================*
* FUNCTION: Smdup UnpackCmd20
* PURPOSE : Analyse the MSG 20 command
*			According to Value Type 0x51 0x54 0x56 0x5A 0x5B 0x5C 0x5D
*			
* INPUT:	INT32 iAddr
*     
*
* RETURN:
*     INT32   TRUE or FALSE		g_can_smduplus.aRoughData
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
static BOOL  SmdupUnpackCmd20(int iAddr,
							int iReadLen,
							BYTE* pbyRcvBuf)
{
	INT32		iTempAddr;
	BOOL		bRst;
	iTempAddr = iAddr - SMDUP_ADDR_OFFSET;
	bRst	  = SmdupUpackCmdXX(iTempAddr, iReadLen,pbyRcvBuf,st_SmpMSG20Data);
	//Special processing
	g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_SERIAL_NO_HIGH].uiValue
			= g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_SERIAL_NO_LOW].uiValue >> 30;


	//CMD 20 FEATURE d=0 x=   0               SERIAL_L d=-754647040 x= d3050000
	//	VERSION d=1050148984 x=3e980078                Code1 d=808661331 x=30333153 
	//	Code2 d=1296323883 x=4d44552b  Code3 d=1093677311 x=413030ff Code4 d=-1 x=ffffffff

	//printf("\nCMD 20 FEATURE d=%d x=%4x\
	//	SERIAL_L d=%d x= %4x\n VERSION d=%d x=%4x\
	//	Code1 d=%d x=%4x \n\
	//	Code2 d=%d x=%4x  Code3 d=%d x=%4x Code4 d=%d x=%4x\n",
	//g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_FEATURE].uiValue,
	//g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_FEATURE].uiValue,
	//g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_SERIAL_NO_LOW].uiValue,
	//g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_SERIAL_NO_LOW].uiValue,
	//g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_VERSION_NO].uiValue,
	//g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_VERSION_NO].uiValue,
	//g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_BARCODE1].uiValue,
	//g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_BARCODE1].uiValue,
	//g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_BARCODE2].uiValue,
	//g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_BARCODE2].uiValue,
	//g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_BARCODE3].uiValue,
	//g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_BARCODE3].uiValue,
	//g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_BARCODE4].uiValue,
	//g_can_smduplus.aRoughData[iTempAddr][SM_SMDUP_BARCODE4].uiValue);


	return	bRst;
}

void TestCMD20(INT32 iReadLengh, BYTE* cByRecvBuf)
{
	BYTE* byRecvData;
	BYTE* analyseRcvData;
	byRecvData = cByRecvBuf;
	//SMDUP_FRAME_OFFSET_VALUE
	UINT uiTemp;

	//1.特征字 不使用
	byRecvData =cByRecvBuf + st_SmpMSG20Data[0].iCanFrameStartPst;
	analyseRcvData = byRecvData + SMDUP_FRAME_OFFSET_VALUE;
	
	//2.串号低位
	byRecvData = cByRecvBuf + st_SmpMSG20Data[1].iCanFrameStartPst;
	analyseRcvData = byRecvData + SMDUP_FRAME_OFFSET_VALUE;

	//UINT uiValue = (((UINT)(pValue[0])) << 24)
					//	+ (((UINT)(pValue[1])) << 16)
					//	+ (((UINT)(pValue[2])) << 8)
					//	+ ((UINT)(pValue[3]));

	uiTemp = (((UINT)(analyseRcvData[0])) << 24)
			+ (((UINT)(analyseRcvData[1])) << 16)
			+ (((UINT)(analyseRcvData[2])) << 8)
			+ ((UINT)(analyseRcvData[3]));

	//printf("\n\n Series No D=%d,0=%2x 1=%2x 2=%2x 3=%2x\n",
	//							uiTemp,
	//							analyseRcvData[0],
	//							analyseRcvData[1],
	//							analyseRcvData[2],
	//							analyseRcvData[3]);

	//3.版本号
	byRecvData =cByRecvBuf + st_SmpMSG20Data[2].iCanFrameStartPst;
	analyseRcvData = byRecvData + SMDUP_FRAME_OFFSET_VALUE;

	uiTemp = (((UINT)(analyseRcvData[0])) << 24)
					+ (((UINT)(analyseRcvData[1])) << 16)
					+ (((UINT)(analyseRcvData[2])) << 8)
					+ ((UINT)(analyseRcvData[3]));

	//printf("\n Version No D=%d,0=%2x 1=%2x 2=%2x 3=%2x\n",
	//						uiTemp,
	//						analyseRcvData[0],
	//						analyseRcvData[1],
	//						analyseRcvData[2],
	//						analyseRcvData[3]);
	
	//3.CODE1
	byRecvData =cByRecvBuf + st_SmpMSG20Data[3].iCanFrameStartPst;
	analyseRcvData = byRecvData + SMDUP_FRAME_OFFSET_VALUE;
	uiTemp = (((UINT)(analyseRcvData[0])) << 24)
					+ (((UINT)(analyseRcvData[1])) << 16)
					+ (((UINT)(analyseRcvData[2])) << 8)
					+ ((UINT)(analyseRcvData[3]));

	//printf("\n CODE111111  D=%d,0=%2x 1=%2x 2=%2x 3=%2x\n",
	//				uiTemp,
	//				analyseRcvData[0],
	//				analyseRcvData[1],
	//				analyseRcvData[2],
	//				analyseRcvData[3]);	
	
	//3.CODE2
	byRecvData =cByRecvBuf + st_SmpMSG20Data[4].iCanFrameStartPst;
	analyseRcvData = byRecvData + SMDUP_FRAME_OFFSET_VALUE;
	uiTemp = (((UINT)(analyseRcvData[0])) << 24)
						+ (((UINT)(analyseRcvData[1])) << 16)
						+ (((UINT)(analyseRcvData[2])) << 8)
						+ ((UINT)(analyseRcvData[3]));

	//printf("\n CODE22222  D=%d,0=%2x 1=%2x 2=%2x 3=%2x\n",
	//						uiTemp,
	//						analyseRcvData[0],
	//						analyseRcvData[1],
	//						analyseRcvData[2],
	//						analyseRcvData[3]);	
	
	//3.CODE3
	byRecvData =cByRecvBuf + st_SmpMSG20Data[5].iCanFrameStartPst;
	analyseRcvData = byRecvData + SMDUP_FRAME_OFFSET_VALUE;
	uiTemp = (((UINT)(analyseRcvData[0])) << 24)
						+ (((UINT)(analyseRcvData[1])) << 16)
						+ (((UINT)(analyseRcvData[2])) << 8)
						+ ((UINT)(analyseRcvData[3]));

	//printf("\n CODE33333  D=%d,0=%2x 1=%2x 2=%2x 3=%2x\n",
	//					uiTemp,
	//					analyseRcvData[0],
	//					analyseRcvData[1],
	//					analyseRcvData[2],
	//					analyseRcvData[3]);
	
	//3.CODE4
	byRecvData =cByRecvBuf + st_SmpMSG20Data[6].iCanFrameStartPst;
	analyseRcvData = byRecvData + SMDUP_FRAME_OFFSET_VALUE;
	uiTemp = (((UINT)(analyseRcvData[0])) << 24)
				+ (((UINT)(analyseRcvData[1])) << 16)
				+ (((UINT)(analyseRcvData[2])) << 8)
				+ ((UINT)(analyseRcvData[3]));

	//printf("\n CODE4444  D=%d,0=%2x 1=%2x 2=%2x 3=%2x\n\n",
	//				uiTemp,
	//				analyseRcvData[0],
	//				analyseRcvData[1],
	//				analyseRcvData[2],
	//				analyseRcvData[3]);
		
		
		
		
		
		
		
		
		
}

/*=============================================================================*
* FUNCTION: Smdup SampCmd20
* PURPOSE : Sampling  data by MSG 0x20,
*			It return 0x51 0x54 0x56 0x5A 0x5B 0x5C 0x5D Frame
* INPUT:	INT32 iAddr
*     
*
* RETURN:
*     INT32   1 or 0
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32  SmdupSampCmd20(INT32 iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;

	PackAndSendSmduCmd(SMDUP_MSG_TYPE_RQST_DATA20,
					(UINT)iAddr,
					CAN_CMD_TYPE_P2P,
					SMDUP_COM_LED_BLINK,
					SMDUP_VAL_TYPE_NO_TRIM_VOLT,
					0.0);

	Sleep((SMDUP_CMD20_RCV_FRAMES_NUM * SMDUP_FRAME_FRAME_INTTERVAL) 
			+ SMDUP_FRAME_ENOUGH_WAIT_TIME);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
					g_can_smduplus.cByRecvBuf, 
					&iReadLen1st);

	if(iReadLen1st < (SMDUP_CMD20_RCV_FRAMES_NUM * CAN_ONE_FRAME_LEN))
	{
		Sleep(SMDUP_FRAME_ENOUGH_LONG_WAIT_TIME);
		//printf(" MSG 20 Sampling again \n");
		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_ONE_FRAME_LEN, 
						g_can_smduplus.cByRecvBuf + iReadLen1st,
						&iReadLen2nd);
	}
	else
	{
		//Received the Frame number is full
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN) 
		>= SMDUP_CMD20_RCV_FRAMES_NUM)
	{
		//printf(" $$$$$$$$$$$$$$$$$$$ Smdup SampCmd20   \n");
		if (SmdupUnpackCmd20(iAddr,
							iReadLen1st + iReadLen2nd,
							g_can_smduplus.cByRecvBuf))
		{
			//TestCMD20(iReadLen1st + iReadLen2nd, g_can_smduplus.cByRecvBuf);

			//printf(" #  $    $		# Smdup SampCmd20  ok \n");
			return CAN_SAMPLE_OK;
		}
		else
		{
			//printf(" #  $    $		# Smdup SampCmd20  FAIL 1 \n");
			//IncSmdupIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;	
		}
	}
	else
	{
		//printf(" #  $    $		# Smdup SampCmd20  FAIL 2 \n");
		return CAN_SAMPLE_FAIL;		
	}

	return CAN_SAMPLE_OK;
}

/*=============================================================================*
* FUNCTION: Smdup UnpackCmd24
* PURPOSE : Analyse the MSG 24 command
*			According to Value Type 0x0100 0x0101 ...... 0x0109
*			
* INPUT:	INT32 iAddr
*     
*
* RETURN:
*     INT32   FALSE or TRUE		g_can_smduplus.aRoughData
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL BOOL SmdupUnpackCmd24(int iAddr,
							 int iReadLen,
							 BYTE* pbyRcvBuf)
{
	INT32		iRst;
	INT32		iTempAddr;
	iTempAddr			 = iAddr - SMDUP_ADDR_OFFSET;

	
	
	iRst				 = SmdupUpackCmdXX(iTempAddr,iReadLen,pbyRcvBuf,st_SmpMSG24Data);
	return iRst;
}

/*=============================================================================*
* FUNCTION: Smdup SampCmd24
* PURPOSE : Sampling  data by MSG 0x24 to get current signals
*			It return 0x0100 0x0101 0x0102 0x0103 0x0104 0x0105 0x0106
*					  0x0107 0x0108 0x0109	Frame
* INPUT:	INT32 iAddr
*     
* RETURN:
*     
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 SmdupSampCmd24(INT32 iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;

	PackAndSendSmduCmd(SMDUP_MSG_TYPE_RQST_DATA24,
						(UINT)iAddr,
						CAN_CMD_TYPE_P2P,
						SMDUP_COM_LED_BLINK,
						SMDUP_VAL_TYPE_NO_TRIM_VOLT,
						0.0);
	Sleep((SMDUP_CMD24_RCV_FRAMES_NUM * SMDUP_FRAME_FRAME_INTTERVAL) 
			+ SMDUP_FRAME_ENOUGH_WAIT_TIME);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
					g_can_smduplus.cByRecvBuf, 
					&iReadLen1st);

	if(iReadLen1st < (SMDUP_CMD24_RCV_FRAMES_NUM * CAN_ONE_FRAME_LEN))
	{
		Sleep(SMDUP_FRAME_ENOUGH_LONG_WAIT_TIME);

		//printf(" MSG 24 Sampling again \n");

		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_ONE_FRAME_LEN, 
						g_can_smduplus.cByRecvBuf + iReadLen1st,
						&iReadLen2nd);
	}
	else
	{
		//Received the Frame number is full
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN) 
		>= SMDUP_CMD24_RCV_FRAMES_NUM)
	{
		if (SmdupUnpackCmd24(iAddr,
							iReadLen1st + iReadLen2nd,
							g_can_smduplus.cByRecvBuf))
		{
			ClrSmdupIntrruptTimes(iAddr);
		}
		else
		{
			IncSmdupIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;	
		}

	}
	else
	{
		IncSmdupIntrruptTimes(iAddr);
		int iRST = ((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN);
		//printf("   24 CMD LENGH = %d \n",iRST);
		return CAN_SAMPLE_FAIL;		
	}

	//printf("#######  Smdup SampCmd24  ok \n");

	return CAN_SAMPLE_OK;
}
/*=============================================================================*
* FUNCTION: Smdup UnpackCmd25
* PURPOSE : Analyse the MSG 25 command
*			According to Value Type 0x010A 0x0101 ...... 0x0113
*			
* INPUT:	INT32 iAddr
*     
*
* RETURN:
*     INT32   FALSE or TRUE		g_can_smduplus.aRoughData
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL BOOL SmdupUnpackCmd25(int iAddr,
							int iReadLen,
							BYTE* pbyRcvBuf)
{
	INT32		iRst;
	INT32		iTempAddr;
	iTempAddr			 = iAddr - SMDUP_ADDR_OFFSET;

	//printf("\n\n receive cmd 25 \n");

	iRst				 = SmdupUpackCmdXX(iTempAddr,iReadLen,pbyRcvBuf,st_SmpMSG25Data);
	return iRst;


}
/*=============================================================================*
* FUNCTION: Smdup SampCmd25
* PURPOSE : Sampling  data by MSG 0x25 to get current signals
*			It return 0x010A 0x010B 0x010C 0x010D 0x010E 0x010F 0x0110
*					  0x0111 0x0112 0x0113	Frame
* INPUT:	INT32 iAddr
*     
* RETURN:
*     
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 SmdupSampCmd25(INT32 iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;
	//INT32	iRST;
	PackAndSendSmduCmd(SMDUP_MSG_TYPE_RQST_DATA25,
					  (UINT)iAddr,
					  CAN_CMD_TYPE_P2P,
					  SMDUP_COM_LED_BLINK,
					  SMDUP_VAL_TYPE_NO_TRIM_VOLT,
					  0.0);

	Sleep((SMDUP_CMD25_RCV_FRAMES_NUM * SMDUP_FRAME_FRAME_INTTERVAL) 
									 + SMDUP_FRAME_ENOUGH_WAIT_TIME);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
					g_can_smduplus.cByRecvBuf, 
					&iReadLen1st);

	if(iReadLen1st < (SMDUP_CMD25_RCV_FRAMES_NUM * CAN_ONE_FRAME_LEN))
	{
		Sleep(SMDUP_FRAME_ENOUGH_LONG_WAIT_TIME);

		//printf(" MSG 25 Sampling again \n");

		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_ONE_FRAME_LEN, 
						g_can_smduplus.cByRecvBuf + iReadLen1st,
						&iReadLen2nd);
	}
	else
	{
		//Received the Frame number is full
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN) 
		>= SMDUP_CMD25_RCV_FRAMES_NUM)
	{
		if (SmdupUnpackCmd25(iAddr,
							iReadLen1st + iReadLen2nd,
							g_can_smduplus.cByRecvBuf))
		{
			//ClrSmdupIntrruptTimes(iAddr);
		}
		else
		{
			//IncSmdupIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;	
		}

	}
	else
	{
		//printf("  25 CMD  bytes num  too short \n");
		int iRST = ((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN);
		//printf("   25 CMD LENGH = %d \n",iRST);
		return CAN_SAMPLE_FAIL;		
	}

	//printf("#######  Smdup SampCmd25  ok \n");
	return CAN_SAMPLE_OK;
}
/*=============================================================================*
* FUNCTION: Smdup UnpackCmd26
* PURPOSE : Analyse the MSG 26 command
*			According to Value Type 0x0114 0x0115 ...... 0x0118
*			
* INPUT:	INT32 iAddr
*     
*
* RETURN:
*     INT32   FALSE or TRUE		g_can_smduplus.aRoughData
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL BOOL SmdupUnpackCmd26(int iAddr,
							int iReadLen,
							BYTE* pbyRcvBuf)
{
	INT32		iRst;
	INT32		iTempAddr;
	iTempAddr			 = iAddr - SMDUP_ADDR_OFFSET;
	//printf("\n\n receive cmd 26 \n");
	iRst				 = SmdupUpackCmdXX(iTempAddr, iReadLen, pbyRcvBuf, st_SmpMSG26Data);
	return iRst;
}
/*=============================================================================*
* FUNCTION: Smdup SampCmd26
* PURPOSE : Sampling  data by MSG 0x26 to get current signals
*			It return 0x0114 0x0115 0x0116 0x0117 0x0118 Frame
* INPUT:	INT32 iAddr
*     
* RETURN:
*     
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 SmdupSampCmd26(INT32 iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;
	//int		iRST;

	PackAndSendSmduCmd(SMDUP_MSG_TYPE_RQST_DATA26,
						(UINT)iAddr,
						CAN_CMD_TYPE_P2P,
						SMDUP_COM_LED_BLINK,
						SMDUP_VAL_TYPE_NO_TRIM_VOLT,
						0.0);

	Sleep((SMDUP_CMD26_RCV_FRAMES_NUM * SMDUP_FRAME_FRAME_INTTERVAL) 
				+ SMDUP_FRAME_ENOUGH_WAIT_TIME 
				+ SMDUP_FRAME_ENOUGH_WAIT_TIME);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
					g_can_smduplus.cByRecvBuf, 
					&iReadLen1st);

	if(iReadLen1st < (SMDUP_CMD26_RCV_FRAMES_NUM * CAN_ONE_FRAME_LEN))
	{
		Sleep(SMDUP_FRAME_ENOUGH_LONG_WAIT_TIME);

		//printf(" MSG 26 Sampling again \n");

		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_ONE_FRAME_LEN, 
						g_can_smduplus.cByRecvBuf + iReadLen1st,
						&iReadLen2nd);
	}
	else
	{
		//Received the Frame number is full
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN) 
		>= SMDUP_CMD26_RCV_FRAMES_NUM)
	{
		if (SmdupUnpackCmd26(iAddr,
							iReadLen1st + iReadLen2nd,
							g_can_smduplus.cByRecvBuf))
		{
			//ClrSmdupIntrruptTimes(iAddr);
		}
		else
		{
			//IncSmdupIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;	
		}

	}
	else
	{
		//printf("  26 CMD  bytes num  too short \n");

		int iRST = ((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN);
		//printf("   26 CMD LENGH = %d \n",iRST);
		return CAN_SAMPLE_FAIL;		
	}

	//printf("#######  Smdup SampCmd26  ok \n");

	return CAN_SAMPLE_OK;
}
/*=============================================================================*
* FUNCTION: Smdup UnpackCmd27
* PURPOSE : Analyse the MSG 27 command
*			According to Value Type 0x0119 0x011A ...... 0x0122
*			
* INPUT:	INT32 iAddr
*     
*
* RETURN:
*     INT32   FALSE or TRUE		g_can_smduplus.aRoughData
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL BOOL SmdupUnpackCmd27(int iAddr,
							int iReadLen,
							BYTE* pbyRcvBuf)
{
	INT32		iRst;
	INT32		iTempAddr;
	iTempAddr			 = iAddr - SMDUP_ADDR_OFFSET;
	//printf("\n\n receive cmd 27 \n");
	iRst				 = SmdupUpackCmdXX(iTempAddr, iReadLen, pbyRcvBuf, st_SmpMSG27Data);
	return iRst;
}
/*=============================================================================*
* FUNCTION: Smdup SampCmd27
* PURPOSE : Sampling  data by MSG 0x27 to get Volt signals
*			It return 0x0119 0x011A ... 0x0122 Frame
* INPUT:	INT32 iAddr
*     
* RETURN:
*     
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 SmdupSampCmd27(INT32 iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;

	PackAndSendSmduCmd(SMDUP_MSG_TYPE_RQST_DATA27,
						(UINT)iAddr,
						CAN_CMD_TYPE_P2P,
						SMDUP_COM_LED_BLINK,
						SMDUP_VAL_TYPE_NO_TRIM_VOLT,
						0.0);

	Sleep((SMDUP_CMD27_RCV_FRAMES_NUM * SMDUP_FRAME_FRAME_INTTERVAL) 
									 + SMDUP_FRAME_ENOUGH_WAIT_TIME);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
					g_can_smduplus.cByRecvBuf, 
					&iReadLen1st);

	if(iReadLen1st < (SMDUP_CMD27_RCV_FRAMES_NUM * CAN_ONE_FRAME_LEN))
	{
		Sleep(SMDUP_FRAME_ENOUGH_LONG_WAIT_TIME);

		//printf(" MSG 27 Sampling again \n");

		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_ONE_FRAME_LEN, 
						g_can_smduplus.cByRecvBuf + iReadLen1st,
						&iReadLen2nd);
	}
	else
	{
		//Received the Frame number is full
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN) 
		>= SMDUP_CMD27_RCV_FRAMES_NUM)
	{
		if (SmdupUnpackCmd27(iAddr,
							iReadLen1st + iReadLen2nd,
							g_can_smduplus.cByRecvBuf))
		{
			//ClrSmdupIntrruptTimes(iAddr);
		}
		else
		{
			//IncSmdupIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;	
		}

	}
	else
	{
		//printf("  27 CMD  bytes num  too short \n");
		int iRST = ((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN);

		//printf("   27 CMD LENGH = %d \n",iRST);
		return CAN_SAMPLE_FAIL;		
	}

	//printf("#######  Smdup SampCmd27  ok \n");
	return CAN_SAMPLE_OK;
}
/*=============================================================================*
* FUNCTION: Smdup UnpackCmd28
* PURPOSE : Analyse the MSG 28 command
*			According to Value Type 0x0123 0x0124 ...... 0x012C
*			
* INPUT:	INT32 iAddr
*     
*
* RETURN:
*     INT32   FALSE or TRUE		g_can_smduplus.aRoughData
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL BOOL SmdupUnpackCmd28(int iAddr,
							int iReadLen,
							BYTE* pbyRcvBuf)
{
	INT32		iRst;
	INT32		iTempAddr;
	iTempAddr			 = iAddr - SMDUP_ADDR_OFFSET;
	//printf("\n\n receive cmd 28 \n");
	iRst				 = SmdupUpackCmdXX(iTempAddr, iReadLen, pbyRcvBuf, st_SmpMSG28Data);
	return iRst;
}
/*=============================================================================*
* FUNCTION: Smdup SampCmd28
* PURPOSE : Sampling  data by MSG 0x28 to get Volt signals
*			It return 0x0123 0x0124 ... 0x012C Frame
* INPUT:	INT32 iAddr
*     
* RETURN:
*     
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 SmdupSampCmd28(INT32 iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;

	PackAndSendSmduCmd(SMDUP_MSG_TYPE_RQST_DATA28,
						(UINT)iAddr,
						CAN_CMD_TYPE_P2P,
						SMDUP_COM_LED_BLINK,
						SMDUP_VAL_TYPE_NO_TRIM_VOLT,
						0.0);

	Sleep((SMDUP_CMD28_RCV_FRAMES_NUM * SMDUP_FRAME_FRAME_INTTERVAL) 
									+ SMDUP_FRAME_ENOUGH_WAIT_TIME);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
					g_can_smduplus.cByRecvBuf, 
					&iReadLen1st);

	if(iReadLen1st < (SMDUP_CMD28_RCV_FRAMES_NUM * CAN_ONE_FRAME_LEN))
	{
		Sleep(SMDUP_FRAME_ENOUGH_LONG_WAIT_TIME);

		//printf(" MSG 28 Sampling again \n");

		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_ONE_FRAME_LEN, 
						g_can_smduplus.cByRecvBuf + iReadLen1st,
						&iReadLen2nd);
	}
	else
	{
		//Received the Frame number is full
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN) 
		>= SMDUP_CMD28_RCV_FRAMES_NUM)
	{
		if (SmdupUnpackCmd28(iAddr,
							iReadLen1st + iReadLen2nd,
							g_can_smduplus.cByRecvBuf))
		{
			//ClrSmdupIntrruptTimes(iAddr);
		}
		else
		{
			//IncSmdupIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;	
		}

	}
	else
	{
		//printf("  28  CMD  bytes num  too short \n");
		int iRST = ((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN);

		//printf("   28 CMD LENGH = %d \n",iRST);
		return CAN_SAMPLE_FAIL;		
	}
//printf("#######  Smdup SampCmd28  ok \n");
	return CAN_SAMPLE_OK;
}
/*=============================================================================*
* FUNCTION: Smdup UnpackCmd29
* PURPOSE : Analyse the MSG 29 command
*			According to Value Type 0x012D 0x012E ...... 0x0131
*			
* INPUT:	INT32 iAddr
*     
*
* RETURN:
*     INT32   FALSE or TRUE		g_can_smduplus.aRoughData
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL BOOL SmdupUnpackCmd29(int iAddr,
							int iReadLen,
							BYTE* pbyRcvBuf)
{
	INT32		iRst;
	INT32		iTempAddr;
	iTempAddr			 = iAddr - SMDUP_ADDR_OFFSET;
	//printf("\n\n receive cmd 29 \n");
	iRst				 = SmdupUpackCmdXX(iTempAddr, iReadLen, pbyRcvBuf, st_SmpMSG29Data);
	return iRst;
}
/*=============================================================================*
* FUNCTION: Smdup SampCmd29
* PURPOSE : Sampling  data by MSG 0x29 to get Volt signals
*			It return 0x012D 0x012E ... 0x0131 Frame
* INPUT:	INT32 iAddr
*     
* RETURN:
*     
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 SmdupSampCmd29(INT32 iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;

	PackAndSendSmduCmd(SMDUP_MSG_TYPE_RQST_DATA29,
						(UINT)iAddr,
						CAN_CMD_TYPE_P2P,
						SMDUP_COM_LED_BLINK,
						SMDUP_VAL_TYPE_NO_TRIM_VOLT,
						0.0);

	Sleep((SMDUP_CMD29_RCV_FRAMES_NUM * SMDUP_FRAME_FRAME_INTTERVAL) 
										+ SMDUP_FRAME_ENOUGH_WAIT_TIME
										+ SMDUP_FRAME_ENOUGH_WAIT_TIME);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
					g_can_smduplus.cByRecvBuf, 
					&iReadLen1st);

	if(iReadLen1st < (SMDUP_CMD29_RCV_FRAMES_NUM * CAN_ONE_FRAME_LEN))
	{
		Sleep(SMDUP_FRAME_ENOUGH_LONG_WAIT_TIME);

		//printf(" MSG 29 Sampling again \n");

		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_ONE_FRAME_LEN, 
			g_can_smduplus.cByRecvBuf + iReadLen1st,
			&iReadLen2nd);
	}
	else
	{
		//Received the Frame number is full
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN) 
							>= SMDUP_CMD29_RCV_FRAMES_NUM)
	{
		if (SmdupUnpackCmd29(iAddr,
								iReadLen1st + iReadLen2nd,
								g_can_smduplus.cByRecvBuf))
		{
			//ClrSmdupIntrruptTimes(iAddr);
		}
		else
		{
			//IncSmdupIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;	
		}

	}
	else
	{
		//printf("	  29 CMD  bytes num  too short \n");
		int iRST = ((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN);
		//printf("   29 CMD LENGH = %d \n",iRST);

		return CAN_SAMPLE_FAIL;		
	}

	//printf("#######  Smdup SampCmd29  ok \n");
	return CAN_SAMPLE_OK;
}

/*=============================================================================*
* FUNCTION: Smdup GetFuseStat
* PURPOSE : Get Fuse status from 4 bytes Value
*						
* INPUT:	INT32 iAddr
*     
*
* RETURN:
*     INT32   FALSE or TRUE		g_can_smduplus.aRoughData
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void SmdupGetFuseStat(INT32 VirtualiAddr, BYTE* pValue)
{
	INT32		i,j,k;
	BYTE*		pRecvValue0;
	BYTE*		pRecvValue1;
	BYTE*		pRecvValue2;
	BYTE*		pRecvValue3;

	pRecvValue0	= pValue + 3;
	pRecvValue1 = pValue + 2;
	pRecvValue2 = pValue + 1;
	pRecvValue3 = pValue + 0;

	for (i = 0; i < 8; i++)
	{
		g_can_smduplus.aRoughData[VirtualiAddr][SM_SMDUP_FUSESTAT1 + i].iValue 
				= ((*pRecvValue0 >> i) & 0x01);
		g_can_smduplus.aRoughData[VirtualiAddr][SM_SMDUP_FUSESTAT9 + i].iValue
				= ((*pRecvValue1 >> i) & 0x01);				
		g_can_smduplus.aRoughData[VirtualiAddr][SM_SMDUP_FUSESTAT17 + i].iValue
				= ((*pRecvValue2 >> i) & 0x01);	

	}

	g_can_smduplus.aRoughData[VirtualiAddr][SM_SMDUP_FUSESTAT25].iValue
		= ((*pRecvValue3) & 0x01);	
	//注意：第26位用作传分流器软件设置拨码开关状态，与张天柱商议使用这条协议来实现。2012/05/18
	//OFF = 1(拨码设置),ON = 0(软件设置)
	//printf("\n Byte1: %d, Byte2: %d, Byte3: %d, Byte4: %d",*pRecvValue0,*pRecvValue1,*pRecvValue2,*pRecvValue3);
	if(g_can_smduplus.bOldSMDUP[VirtualiAddr])
	{
		g_can_smduplus.aRoughData[VirtualiAddr][ROUGH_SMDUP_SHUNT_SIZE_SETTABLE].iValue
			= SIG_VALUE_SHUNTSIZE_NOSUPPORT;
	}
	else
	{
		g_can_smduplus.aRoughData[VirtualiAddr][ROUGH_SMDUP_SHUNT_SIZE_SETTABLE].iValue
			= (((*pRecvValue3 >> 1) & 0x01) > 0) ? SIG_VALUE_SHUNTSIZE_DIPSWITCH : SIG_VALUE_SHUNTSIZE_SOFTWARE;
	}
}

/*=============================================================================*
* FUNCTION: Smdup UnpackCmd01
* PURPOSE : Analyse the MSG 03 command
*			According to  VALUE_TYPE 0x0132
*			
* INPUT:	INT32 iAddr
*     
*
* RETURN:
*     INT32   FALSE or TRUE		g_can_smduplus.aRoughData
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL BOOL SmdupUnpackCmd01(int iAddr,
							int iReadLen,
							BYTE* pbyRcvBuf)
{
	INT32		VirtualiAddr;
	BYTE*		pValueType;
	VirtualiAddr = iAddr - SMDUP_ADDR_OFFSET;

	if (SMDUP_FUSE_STAT_VAL_TYPE == SmdupGetValueType(pbyRcvBuf))
	{
		SmdupGetFuseStat(VirtualiAddr, pbyRcvBuf + SMDUP_FRAME_OFFSET_VALUE);
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*=============================================================================*
* FUNCTION: Smdup SampCmd01
* PURPOSE : Sampling  data by MSG 0x03 to get Volt signals
*			It return 0x0132 Frame,But bit setting
* INPUT:	INT32 iAddr
*     
* RETURN:
*     
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 SmdupSampCmd01(INT32 iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;

	PackAndSendSmduCmd(SMDUP_MSG_TYPE_RQST_DATA01,
						(UINT)iAddr,
						CAN_CMD_TYPE_P2P,
						SMDUP_FUSE_STAT_VALTYPE_H,
						SMDUP_FUSE_STAT_VALTYPE_L,
						0.0);

	Sleep((SMDUP_CMD01_RCV_FRAMES_NUM * SMDUP_FRAME_FRAME_INTTERVAL) 
										+ SMDUP_FRAME_ENOUGH_WAIT_TIME 
										+ SMDUP_FRAME_ENOUGH_WAIT_TIME
										+ SMDUP_FRAME_ENOUGH_WAIT_TIME);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
					g_can_smduplus.cByRecvBuf, 
					&iReadLen1st);

	if(iReadLen1st < (SMDUP_CMD01_RCV_FRAMES_NUM * CAN_ONE_FRAME_LEN))
	{
		Sleep(SMDUP_FRAME_ENOUGH_LONG_WAIT_TIME);

		//printf(" MSG 01 Sampling again \n");

		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_ONE_FRAME_LEN, 
									g_can_smduplus.cByRecvBuf + iReadLen1st,
									&iReadLen2nd);
	}
	else
	{
		//Received the Frame number is full
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN) 
							>= SMDUP_CMD01_RCV_FRAMES_NUM)
	{
		if (SmdupUnpackCmd01(iAddr,
							iReadLen1st + iReadLen2nd,
							g_can_smduplus.cByRecvBuf))
		{
			//ClrSmdupIntrruptTimes(iAddr);
		}
		else
		{
			//IncSmdupIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;	
		}

	}
	else
	{
		int iRST = ((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN);
		//printf("   01 CMD LENGH = %d \n",iRST);
		return CAN_SAMPLE_FAIL;		
	}

	//printf("#######  Smdup SampCmd01  ok \n");

	return CAN_SAMPLE_OK;
}

/*********************************************************************************
*  
*  FUNCTION NAME : 
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-5-17 14:45:30
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
LOCAL BOOL SmdupUnpack_ShuntSize_For2ABC(int iAddr,
					 int iReadLen,
					 BYTE* pbyRcvBuf,
					 ANALYSE_DATA_STRUCT stAnalyseData[])
{
	INT32		iRst;
	INT32		iTempAddr;
	iTempAddr			 = iAddr - SMDUP_ADDR_OFFSET;
	//printf("\n\n receive cmd 27 \n");
	iRst				 = SmdupUpackCmdXX(iTempAddr, iReadLen, pbyRcvBuf, stAnalyseData);
	return iRst;	
}
LOCAL INT32 SmdupSampCmd2A(INT32 iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;

	PackAndSendSmduCmd(SMDUP_MSG_TYPE_RQST_DATA2A,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		0x00,
		0x00,
		0.0);

	Sleep((SMDUP_CMD2A_RCV_FRAMES_NUM * SMDUP_FRAME_FRAME_INTTERVAL) 
		+ SMDUP_FRAME_ENOUGH_WAIT_TIME 
		+ SMDUP_FRAME_ENOUGH_WAIT_TIME
		+ SMDUP_FRAME_ENOUGH_WAIT_TIME);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_can_smduplus.cByRecvBuf, 
		&iReadLen1st);

	if(iReadLen1st < (SMDUP_CMD2A_RCV_FRAMES_NUM * CAN_ONE_FRAME_LEN))
	{
		Sleep(SMDUP_FRAME_ENOUGH_LONG_WAIT_TIME);

		//printf(" MSG 01 Sampling again \n");

		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_ONE_FRAME_LEN, 
			g_can_smduplus.cByRecvBuf + iReadLen1st,
			&iReadLen2nd);
	}
	else
	{
		//Received the Frame number is full
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN) 
		>= SMDUP_CMD2A_RCV_FRAMES_NUM)
	{
		//printf("\nRun here proves read 2A OK!!!!");
		if (SmdupUnpack_ShuntSize_For2ABC(iAddr,
			iReadLen1st + iReadLen2nd,
			g_can_smduplus.cByRecvBuf,st_SmpMSG2AData))
		{
			//printf("\nRun here proves unpack 2A OK!!!!");
			//ClrSmdupIntrruptTimes(iAddr);
		}
		else
		{
			//printf("\nRun here proves unpack 2A ERROR!!!!");
			//IncSmdupIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;	
		}

	}
	else
	{
		int iRST = ((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN);
		//printf("   01 CMD LENGH = %d \n",iRST);
		return CAN_SAMPLE_FAIL;		
	}

	//printf("#######  Smdup SampCmd01  ok \n");

	return CAN_SAMPLE_OK;
}
LOCAL INT32 SmdupSampCmd2B(INT32 iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;

	PackAndSendSmduCmd(SMDUP_MSG_TYPE_RQST_DATA2B,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		0x00,
		0x00,
		0.0);

	Sleep((SMDUP_CMD2B_RCV_FRAMES_NUM * SMDUP_FRAME_FRAME_INTTERVAL) 
		+ SMDUP_FRAME_ENOUGH_WAIT_TIME 
		+ SMDUP_FRAME_ENOUGH_WAIT_TIME
		+ SMDUP_FRAME_ENOUGH_WAIT_TIME);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_can_smduplus.cByRecvBuf, 
		&iReadLen1st);

	if(iReadLen1st < (SMDUP_CMD2B_RCV_FRAMES_NUM * CAN_ONE_FRAME_LEN))
	{
		Sleep(SMDUP_FRAME_ENOUGH_LONG_WAIT_TIME);

		//printf(" MSG 01 Sampling again \n");

		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_ONE_FRAME_LEN, 
			g_can_smduplus.cByRecvBuf + iReadLen1st,
			&iReadLen2nd);
	}
	else
	{
		//Received the Frame number is full
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN) 
		>= SMDUP_CMD2B_RCV_FRAMES_NUM)
	{
		//printf("\nRun here proves read 2B OK!!!!");
		if (SmdupUnpack_ShuntSize_For2ABC(iAddr,
			iReadLen1st + iReadLen2nd,
			g_can_smduplus.cByRecvBuf,st_SmpMSG2BData))
		{
			//printf("\nRun here proves unpack 2B OK!!!!");
			//ClrSmdupIntrruptTimes(iAddr);
		}
		else
		{
			//printf("\nRun here proves unpack 2B ERROR!!!!");
			//IncSmdupIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;	
		}

	}
	else
	{
		int iRST = ((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN);
		//printf("   01 CMD LENGH = %d \n",iRST);
		return CAN_SAMPLE_FAIL;		
	}

	//printf("#######  Smdup SampCmd01  ok \n");

	return CAN_SAMPLE_OK;
}
LOCAL INT32 SmdupSampCmd2C(INT32 iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;

	PackAndSendSmduCmd(SMDUP_MSG_TYPE_RQST_DATA2C,
		(UINT)iAddr,
		CAN_CMD_TYPE_P2P,
		0x00,
		0x00,
		0.0);

	Sleep((SMDUP_CMD2C_RCV_FRAMES_NUM * SMDUP_FRAME_FRAME_INTTERVAL) 
		+ SMDUP_FRAME_ENOUGH_WAIT_TIME 
		+ SMDUP_FRAME_ENOUGH_WAIT_TIME
		+ SMDUP_FRAME_ENOUGH_WAIT_TIME);

	CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
		g_can_smduplus.cByRecvBuf, 
		&iReadLen1st);

	if(iReadLen1st < (SMDUP_CMD2C_RCV_FRAMES_NUM * CAN_ONE_FRAME_LEN))
	{
		Sleep(SMDUP_FRAME_ENOUGH_LONG_WAIT_TIME);

		//printf(" MSG 01 Sampling again \n");

		CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_ONE_FRAME_LEN, 
			g_can_smduplus.cByRecvBuf + iReadLen1st,
			&iReadLen2nd);
	}
	else
	{
		//Received the Frame number is full
	}


	if(((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN) 
		>= SMDUP_CMD2C_RCV_FRAMES_NUM)
	{
		//printf("\nRun here proves read 2C OK!!!!");
		if (SmdupUnpack_ShuntSize_For2ABC(iAddr,
			iReadLen1st + iReadLen2nd,
			g_can_smduplus.cByRecvBuf,st_SmpMSG2CData))
		{
			//printf("\nRun here proves unpack 2C OK!!!!");
			//ClrSmdupIntrruptTimes(iAddr);
		}
		else
		{
			//printf("\nRun here proves unpack 2C ERR!!!!");
			//IncSmdupIntrruptTimes(iAddr);
			return CAN_SAMPLE_FAIL;	
		}

	}
	else
	{
		int iRST = ((iReadLen1st + iReadLen2nd) / CAN_ONE_FRAME_LEN);
		//printf("   01 CMD LENGH = %d \n",iRST);
		return CAN_SAMPLE_FAIL;		
	}

	//printf("#######  Smdup SampCmd01  ok \n");

	return CAN_SAMPLE_OK;
}
/*********************************************************************************
*  
*  FUNCTION NAME : Judge_IsOldSMDUP
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-5-17 11:18:29
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
LOCAL void Judge_IsOldSMDUP(INT32 iAddr)
{
	g_can_smduplus.bOldSMDUP[iAddr] = TRUE;
	int i,iRst;
	INT32 iActualAddr = iAddr + SMDUP_ADDR_OFFSET;
	for (i = 0; i < SMDUP_SCAN_TRY_TIME; i++)
	{
		iRst = SmdupSampCmd2C(iActualAddr); //命令2C只有5回包，所以采用它来判断是否是oldSMDUP

		if (CAN_SAMPLE_OK == iRst)
		{
			g_can_smduplus.bOldSMDUP[iAddr] = FALSE;
			SMDUP_ReadAllShuntSize(iActualAddr); //这里初始读一遍所有的
			break;
		}
		else
		{
			Sleep(30);
			//CAN_ClearReadBuf();
		}
	}
}
/*=============================================================================*
* FUNCTION: SMDUP _Reconfig(void)
* PURPOSE : Scan the smduplus equipment
*			
* INPUT:	void
*     
*
* RETURN:
*			void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void SMDUP_Reconfig(void)
{
	INT32		i;
	INT32		iAddr;
	BOOL		iRst;
	INT32		iSeqNum;
	INT32		iActualAddr;
	iSeqNum		= 0;
	//printf("\n********************SMDUP Doing Reconfig********************\n");
	//Max address CAN_SMDUP_MAX_NUM
	for (iAddr = 0; iAddr < CAN_SMDUP_MAX_NUM; iAddr++)
	{
		iActualAddr = iAddr + SMDUP_ADDR_OFFSET;
		iRst		= CAN_SAMPLE_FAIL;

		for (i = 0; i < SMDUP_SCAN_TRY_TIME; i++)
		{
			iRst = SmdupSampCmd20(iActualAddr);



/******************************For DEBUG Start**********************************/
//		if( (iAddr >=0) || (iAddr <20))
//			{
//				iRst=CAN_SAMPLE_OK;
//			}
/******************************For DEBUG End**********************************/


			if (CAN_SAMPLE_OK == iRst)
			{
				break;
			}
			else
			{
				Sleep(30);
				//CAN_ClearReadBuf();
			}
		}

		//iRst		= SmdupSampCmd20(iActualAddr);
		//printf("  reconfig 20   iActualAddr = %d \n",iActualAddr);
		if (CAN_SAMPLE_OK == iRst)
		{
			//printf("  SCAN 20 ok  EXIST ActualADDR =%d  iAddr =%d\n",iActualAddr,iAddr);

			//SmdupSampCmd24(iActualAddr);//Current 1-10
			//SmdupSampCmd25(iActualAddr);//Current 11-20
			//SmdupSampCmd26(iActualAddr);//Current 21-25
			//SmdupSampCmd27(iActualAddr);//Voltage 1-10
			//SmdupSampCmd28(iActualAddr);//Voltage 11-20
			//SmdupSampCmd29(iActualAddr);//Voltage 21-25
			//SmdupSampCmd01(iActualAddr);//Get Fuse status 1-25
			//注意地址转换不要搞错
			Judge_IsOldSMDUP(iAddr);

			g_can_smduplus.aRoughData[iAddr][SM_SMDUP_EXIST_STAT].iValue =CAN_SMDUP_EXIT;
			g_can_smduplus.aRoughData[iAddr][SM_SMDUP_SEQ_NUM].iValue = iSeqNum;
			g_can_smduplus.aRoughData[iAddr][SM_SMDUP_ADDR].iValue	  = iActualAddr;
			iSeqNum++;
		}
		else
		{
			//Continue!! Next address!!
		}

		Sleep(60);
	}

	if(iSeqNum)
	{
		g_can_smduplus.aRoughData[0][SM_SMDUP_NUM].iValue		  = iSeqNum;
		g_can_smduplus.aRoughData[0][SM_SMDUP_GROUP_STATE].iValue = CAN_SMDUP_EXIT;
	}
	else
	{
		g_can_smduplus.aRoughData[0][SM_SMDUP_GROUP_STATE].iValue = CAN_SMDUP_NOT_EXIT;
		g_can_smduplus.aRoughData[0][SM_SMDUP_NUM].iValue		  =0;
	}
}

//长整数转化为字符
static void Long2CharBlank(char* buf,long num,char len)
{
	char i;
	for(i=len;i>0;i--)
	{
		buf[i-1] = num %10 + 0x30;
		num = num / 10;
	}

	for(i=0;i<len-1;i++)
	{
		if(buf[i] == (char)0x30)
		{
			buf[i] = ' ';
		}
		else
		{
			break;
		}
	}
}
/*BOOL BarCodeDataIsValid(BYTE byData)
{
	if ((0x20 != byData) && (0xff != byData))
	{
		return TRUE;
	}
	
	return FALSE;
}*/
//void SMDUPUnpackProdctInfo(void *pPI)
//{
//	PRODUCT_INFO *				pInfo;
//	pInfo						= (PRODUCT_INFO*)pPI;
//	UINT	uiTemp11;
//	UINT	uiTemp12;
//
//#define		BARCODE_FF_OFST			 0
//#define		BARCODE_YY_OFST			 2
//#define		BARCODE_MM_OFST			 4	
//#define		BARCODE_WELLWELL_OFST	 6//##
//#define		BARCODE_VV_OFST			 1//##
//
//	INT32	iTemp;
//	BYTE* byRecvData;
//	BYTE* analyseRcvData;
//	byRecvData = g_can_smduplus.cByRecvBuf;
//	UINT uiTemp;
//
//	pInfo->bSigModelUsed = TRUE;
//
//	//1.特征字 不使用
//	byRecvData =g_can_smduplus.cByRecvBuf + st_SmpMSG20Data[0].iCanFrameStartPst;
//	analyseRcvData = byRecvData + SMDUP_FRAME_OFFSET_VALUE;
//	
//	/***********************************************
//	
//	FF YYMM ##### T NNNNNNNNNNNN HVV
//	
//	具体阅读条码特征字说明
//
//	***********************************************/
//
//	//2.串号低位
//	byRecvData = g_can_smduplus.cByRecvBuf + st_SmpMSG20Data[1].iCanFrameStartPst;
//	analyseRcvData = byRecvData + SMDUP_FRAME_OFFSET_VALUE;
//	//年
//	iTemp = analyseRcvData[0] & 0x1f;//year
//	snprintf(pInfo->szSerialNumber + BARCODE_YY_OFST, 3, "%d%d",iTemp/10,iTemp%10);
//	//月
//	iTemp = analyseRcvData[1] & 0x0f;
//	snprintf(pInfo->szSerialNumber + BARCODE_MM_OFST, 3, "%d%d",iTemp/12,iTemp%12);
//	
//	//串号低位的低16字节 对应	##### 代表当月生产数量
//	uiTemp = (((UINT)(analyseRcvData[2])) << 8) + ((UINT)(analyseRcvData[3]));
//	snprintf(pInfo->szSerialNumber + BARCODE_WELLWELL_OFST,
//				6,"%05ld",uiTemp);
//	pInfo->szSerialNumber[11] = '\0';//end
//
//
//
//	//3.版本号
//	byRecvData =g_can_smduplus.cByRecvBuf + st_SmpMSG20Data[2].iCanFrameStartPst;
//	analyseRcvData = byRecvData + SMDUP_FRAME_OFFSET_VALUE;
//	//模块版本号
//	//H		A00形式，高字节按A=0 B=1类推，所以A=65
//	analyseRcvData[0] = 0x3f & analyseRcvData[0];
//	uiTemp = analyseRcvData[0] + 65;
//	pInfo->szHWVersion[0] = uiTemp;
//
//	//VV 00按照实际数值存入低字节
//	snprintf(pInfo->szHWVersion + BARCODE_VV_OFST, 3,
//		"%d%d",analyseRcvData[1]/10, analyseRcvData[1]%10);
//	pInfo->szHWVersion[4] = '\0';//end
//
//	//软件版本		如若软件版本为1.30,保存内容为两两字节整型数130
//	uiTemp = (((UINT)(analyseRcvData[2])) << 8) + ((UINT)(analyseRcvData[3]));
//	//snprintf(pInfo->szSWVersion, 5,
//	//	"%d.%2d",uiTemp/100, uiTemp % 100);
//	uiTemp11 = (uiTemp - (uiTemp/100)*100)/10;
//	uiTemp12 = (uiTemp - (uiTemp/100)*100)%10;
//	snprintf(pInfo->szSWVersion, 5,
//		"%d.%d%d",uiTemp/100, uiTemp11,uiTemp12);
//	pInfo->szSWVersion[5] = '\0';//end
//
//
//
//	//3.CODE1
//	byRecvData =g_can_smduplus.cByRecvBuf + st_SmpMSG20Data[3].iCanFrameStartPst;
//	analyseRcvData = byRecvData + SMDUP_FRAME_OFFSET_VALUE;
//	//FF 用ASCII码表示,占用CODE1 和 CODE2
//	pInfo->szSerialNumber[0] = analyseRcvData[0];
//	pInfo->szSerialNumber[1] = analyseRcvData[1];
//	//T 表示产品类型,ASCII占用CODE12
//	pInfo->szPartNumber[0] = analyseRcvData[2];//1
//	//NN.....NN			CODE13--CODE24
//	pInfo->szPartNumber[1] = analyseRcvData[3];//S CODE13
//
//
//
//	//3.CODE2
//	byRecvData =g_can_smduplus.cByRecvBuf + st_SmpMSG20Data[4].iCanFrameStartPst;
//	analyseRcvData = byRecvData + SMDUP_FRAME_OFFSET_VALUE;
//	//NN.....NN			CODE13--CODE24
//	pInfo->szPartNumber[2] = analyseRcvData[0];//M CODE14
//	pInfo->szPartNumber[3] = analyseRcvData[1];//D CODE15
//	pInfo->szPartNumber[4] = analyseRcvData[2];//U CODE16
//	if (BarCodeDataIsValid(analyseRcvData[3]))
//	{
//		pInfo->szPartNumber[5] = analyseRcvData[3];//+ CODE17
//	}
//	else
//	{
//		pInfo->szPartNumber[5] = '\0';
//	}
//	
//
//
//
//	//3.CODE3
//	byRecvData =g_can_smduplus.cByRecvBuf + st_SmpMSG20Data[5].iCanFrameStartPst;
//	analyseRcvData = byRecvData + SMDUP_FRAME_OFFSET_VALUE;
//	//NN.....NN			CODE13--CODE24
//	if (BarCodeDataIsValid(analyseRcvData[0]))
//	{
//		pInfo->szPartNumber[6] = analyseRcvData[0];//CODE18
//	}
//	else
//	{
//		pInfo->szPartNumber[6] = '\0';//CODE18
//	}
//
//	if (BarCodeDataIsValid(analyseRcvData[1]))
//	{
//		pInfo->szPartNumber[7] = analyseRcvData[1];//CODE19
//	}
//	else
//	{
//		pInfo->szPartNumber[7] = '\0';//CODE19
//	}
//
//	if (BarCodeDataIsValid(analyseRcvData[2]))
//	{
//		pInfo->szPartNumber[8] = analyseRcvData[2];//CODE20
//	}
//	else
//	{
//		pInfo->szPartNumber[8] = '\0';//CODE20
//	}
//	
//	if (BarCodeDataIsValid(analyseRcvData[3]))
//	{
//		pInfo->szPartNumber[9] = analyseRcvData[3];//CODE21
//	}
//	else
//	{
//		pInfo->szPartNumber[9] = '\0';//CODE21
//	}
//
//
//	//3.CODE4
//	byRecvData =g_can_smduplus.cByRecvBuf + st_SmpMSG20Data[6].iCanFrameStartPst;
//	analyseRcvData = byRecvData + SMDUP_FRAME_OFFSET_VALUE;
//	//NN.....NN			CODE13--CODE24
//	if (BarCodeDataIsValid(analyseRcvData[0]))
//	{
//		pInfo->szPartNumber[10] = analyseRcvData[0];//CODE22
//	}
//	else
//	{
//		pInfo->szPartNumber[10] = '\0';
//	}
//
//	if (BarCodeDataIsValid(analyseRcvData[1]))
//	{
//		pInfo->szPartNumber[11] = analyseRcvData[1];//CODE23
//	}
//	else
//	{
//		pInfo->szPartNumber[11] = '\0';
//	}
//
//	if (BarCodeDataIsValid(analyseRcvData[2]))
//	{
//		pInfo->szPartNumber[12] = analyseRcvData[2];//CODE24
//	}
//	else
//	{
//		pInfo->szPartNumber[12] = '\0';
//	}
//
//	pInfo->szPartNumber[13] = '\0';//CODE25
//
//	//printf("\nSMDU + PartNumber:%s\n", pInfo->szPartNumber);//1smdu+
//	//printf("\nSMDU + SW version:%s\n", pInfo->szSWVersion); //1.20
//	//printf("\nSMDU + HW version:%s\n", pInfo->szHWVersion); //A00
//	//printf("\nSMDU + Serial Number:%s\n", pInfo->szSerialNumber);//03100500006
//
//}
//



//老外提供的函数，为了802项目
void SMDUP_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI)
{
	//INT32		i;
	//INT32		iAddr;
	//BOOL		iRst;
	//INT32		iSeqNum;
	//INT32		iActualAddr;
	//iSeqNum		= 0;

	////Max address CAN_SMDUP_MAX_NUM
	//for (iAddr = 0; iAddr < CAN_SMDUP_MAX_NUM; iAddr++)
	//{
	//	iActualAddr = iAddr + SMDUP_ADDR_OFFSET;
	//	iRst		= CAN_SAMPLE_FAIL;

	//	for (i = 0; i < SMDUP_SCAN_TRY_TIME; i++)
	//	{
	//		iRst = SmdupSampCmd20(iActualAddr);

	//		if (CAN_SAMPLE_OK == iRst)
	//		{
	//			SMDUPUnpackProdctInfo(pPI);
	//			pPI++;//向前堆放！！
	//			break;
	//		}
	//		else
	//		{
	//			pPI->bSigModelUsed = FALSE;
	//			Sleep(30);
	//			//CAN_ClearReadBuf();
	//		}
	//	}

	//	Sleep(60);
	//}



	//以下为不往前堆时的算法：吴政武修改201205
	UNUSED(hComm);
	UNUSED(nUnitNo);
	int iAddr;
	PRODUCT_INFO *				pInfo;
	BOOL	legacy_NS802;
	UINT	uiTemp13;

	pInfo					= (PRODUCT_INFO*)pPI;
	for(iAddr = 0; iAddr < CAN_SMDUP_MAX_NUM; iAddr++)
	{
		if(CAN_SAMP_INVALID_VALUE == g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE1].iValue 
			|| CAN_SMDUP_NOT_EXIT == g_can_smduplus.aRoughData[iAddr][SM_SMDUP_EXIST_STAT].iValue)
		{
			pInfo->bSigModelUsed = FALSE;			
			//printf("\n	This is ADDR=%d continue	\n",iAddr);
			pInfo++;
			continue;
		}
		else
		{
			//printf("\n	Can SM Temp Samp: This is ADDR=%d  and bSigModeUsed=TRUE	\n",iAddr);
			pInfo->bSigModelUsed = TRUE;
			//算法太土了，不用了，下面重新解析
			//SMDUPUnpackProdctInfo(pPI);
		}

		BYTE	byTempBuf[4];
		UINT	uiTemp;
		INT32	iTemp;
		UINT	uiTemp11;
		UINT	uiTemp12;

#define		BARCODE_FF_OFST			 0
#define		BARCODE_YY_OFST			 2
#define		BARCODE_MM_OFST			 4	
#define		BARCODE_WELLWELL_OFST	 6//##
#define		BARCODE_VV_OFST			 1//##

		pInfo->bSigModelUsed = TRUE;
		legacy_NS802 = FALSE;

		//1.特征字 不使用
		//g_CanData.aRoughDataConvert[iAddr][CONVERT_FEATURE].uiValue

		//2.串号低位	SERIAL_NO_LOW=580005dc
		byTempBuf[0] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_SERIAL_NO_LOW].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_SERIAL_NO_LOW].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_SERIAL_NO_LOW].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_SERIAL_NO_LOW].uiValue);

		// added by George Plaza
		if ( byTempBuf[1] == 0xFF )
		{
				// year and month encoded in CODE23 and CODE24 of barcode 4 for legacy_NS802
			legacy_NS802 = TRUE;
			byTempBuf[0] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE4].uiValue >> 16);
			byTempBuf[1] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE4].uiValue >> 8);
		}
		
		else
		{
			//年
			byTempBuf[0] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_SERIAL_NO_LOW].uiValue >> 24) & 0x1f;	// year
			byTempBuf[1] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_SERIAL_NO_LOW].uiValue >> 16) & 0x0f;	// month
		}
		snprintf(pInfo->szSerialNumber + BARCODE_YY_OFST, 3, "%d%d",byTempBuf[0]/10, byTempBuf[0]%10);
		//月
		snprintf(pInfo->szSerialNumber + BARCODE_MM_OFST, 3, "%d%d", byTempBuf[1]/10, byTempBuf[1]%10);	
		
		//串号低位的低16字节 对应	##### 代表当月生产数量
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		snprintf(pInfo->szSerialNumber + BARCODE_WELLWELL_OFST,
			6,"%05ld",uiTemp);
		pInfo->szSerialNumber[11] = '\0';//end		

		//3.版本号VERSION=ffffd8f1
		byTempBuf[0] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_VERSION_NO].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_VERSION_NO].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_VERSION_NO].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_VERSION_NO].uiValue);

		// added by George Plaza
		if ( (0xFF == byTempBuf[0]) && (0xFF == byTempBuf[1]) )
		{
			strcpy(pInfo->szHWVersion, "---");
		}
		else
		{
			//模块版本号
			//H		A00形式，高字节按A=0 B=1类推，所以A=65
			byTempBuf[0] = 0x3f & byTempBuf[0];
			uiTemp = byTempBuf[0] + 65;
			pInfo->szHWVersion[0] = uiTemp;
			
			//VV 00按照实际数值存入低字节
			snprintf(pInfo->szHWVersion + BARCODE_VV_OFST, 3,
				"%d%d",byTempBuf[1]/10, byTempBuf[1]%10);
			pInfo->szHWVersion[4] = '\0';//end
		}	

		// added by George Plaza
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		if ( legacy_NS802 )
		{
			uiTemp11 = (uiTemp % 1000);
			uiTemp12 = (uiTemp11 % 100);
			uiTemp13 = (uiTemp12 % 10);
			
			snprintf(pInfo->szSWVersion, 8,
				"%d.%d.%d.%d",uiTemp/1000, uiTemp11/100,uiTemp12/10, uiTemp13);
			pInfo->szSWVersion[8] = '\0';//end
		}
		else
		{
			//软件版本		如若软件版本为1.30,保存内容为两两字节整型数130
			uiTemp11 = (uiTemp - (uiTemp/100)*100)/10;
			uiTemp12 = (uiTemp - (uiTemp/100)*100)%10;
			snprintf(pInfo->szSWVersion, 5,
				"%d.%d%d",uiTemp/100, uiTemp11,uiTemp12);
			pInfo->szSWVersion[5] = '\0';//end
		}	

		//3.CODE1	ffffd8f1
		byTempBuf[0] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE1].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE1].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE1].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE1].uiValue);

		//FF 用ASCII码表示,占用CODE1 和 CODE2
		pInfo->szSerialNumber[0] = byTempBuf[0];
		pInfo->szSerialNumber[1] = byTempBuf[1];
		//T 表示产品类型,ASCII占用CODE12
		pInfo->szPartNumber[0] = byTempBuf[2];//1
		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[1] = byTempBuf[3];//S CODE13

		//3.CODE2	ffffd8f1
		byTempBuf[0] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE2].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE2].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE2].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE2].uiValue);

		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[2] = byTempBuf[0];//M CODE14
		pInfo->szPartNumber[3] = byTempBuf[1];//D CODE15
		pInfo->szPartNumber[4] = byTempBuf[2];//U CODE16
		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[5] = byTempBuf[3];//+ CODE17
		}
		else
		{
			pInfo->szPartNumber[5] = '\0';
		}		

		//3.CODE3	ffffd8f1
		byTempBuf[0] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE3].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE3].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE3].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE3].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[6] = byTempBuf[0];//CODE18
		}
		else
		{
			pInfo->szPartNumber[6] = '\0';//CODE18
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[7] = byTempBuf[1];//CODE19
		}
		else
		{
			pInfo->szPartNumber[7] = '\0';//CODE19
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[8] = byTempBuf[2];//CODE20
		}
		else
		{
			pInfo->szPartNumber[8] = '\0';//CODE20
		}

		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[9] = byTempBuf[3];//CODE21
		}
		else
		{
			pInfo->szPartNumber[9] = '\0';//CODE21
		}


		//3.CODE4		ffffd8f1
		byTempBuf[0] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE4].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE4].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE4].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE4].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[10] = byTempBuf[0];//CODE22
		}
		else
		{
			pInfo->szPartNumber[10] = '\0';
		}

		// added by George Plaza
		if ( legacy_NS802 )
		{
			pInfo->szPartNumber[11] = 0x20;
			pInfo->szPartNumber[12] = 0x20;
		}
		else
		{
			if (BarCodeDataIsValid(byTempBuf[1]))
			{
				pInfo->szPartNumber[11] = byTempBuf[1];//CODE23
			}
			else
			{
				pInfo->szPartNumber[11] = '\0';
			}

			if (BarCodeDataIsValid(byTempBuf[2]))
			{
				pInfo->szPartNumber[12] = byTempBuf[2];//CODE24
			}
			else
			{
				pInfo->szPartNumber[12] = '\0';
			}
		}	

		pInfo->szPartNumber[13] = '\0';//CODE25

		//printf("\nSMTEMP bSigModelUsed:%d\n", pInfo->bSigModelUsed);

		pInfo++;

	}
}





/*
void SMDUP_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI)
{
	//INT32		i;
	//INT32		iAddr;
	//BOOL		iRst;
	//INT32		iSeqNum;
	//INT32		iActualAddr;
	//iSeqNum		= 0;

	////Max address CAN_SMDUP_MAX_NUM
	//for (iAddr = 0; iAddr < CAN_SMDUP_MAX_NUM; iAddr++)
	//{
	//	iActualAddr = iAddr + SMDUP_ADDR_OFFSET;
	//	iRst		= CAN_SAMPLE_FAIL;

	//	for (i = 0; i < SMDUP_SCAN_TRY_TIME; i++)
	//	{
	//		iRst = SmdupSampCmd20(iActualAddr);

	//		if (CAN_SAMPLE_OK == iRst)
	//		{
	//			SMDUPUnpackProdctInfo(pPI);
	//			pPI++;//向前堆放！！
	//			break;
	//		}
	//		else
	//		{
	//			pPI->bSigModelUsed = FALSE;
	//			Sleep(30);
	//			//CAN_ClearReadBuf();
	//		}
	//	}

	//	Sleep(60);
	//}



	//以下为不往前堆时的算法：吴政武修改201205
	UNUSED(hComm);
	UNUSED(nUnitNo);
	int iAddr;
	PRODUCT_INFO *				pInfo;
	pInfo					= (PRODUCT_INFO*)pPI;
	for(iAddr = 0; iAddr < CAN_SMDUP_MAX_NUM; iAddr++)
	{
		if(CAN_SAMP_INVALID_VALUE == g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE1].iValue 
			|| CAN_SMDUP_NOT_EXIT == g_can_smduplus.aRoughData[iAddr][SM_SMDUP_EXIST_STAT].iValue)
		{
			pInfo->bSigModelUsed = FALSE;			
			//printf("\n	This is ADDR=%d continue	\n",iAddr);
			pInfo++;
			continue;
		}
		else
		{
			//printf("\n	Can SM Temp Samp: This is ADDR=%d  and bSigModeUsed=TRUE	\n",iAddr);
			pInfo->bSigModelUsed = TRUE;
			//算法太土了，不用了，下面重新解析
			//SMDUPUnpackProdctInfo(pPI);
		}

		BYTE	byTempBuf[4];
		UINT	uiTemp;
		INT32	iTemp;
		UINT	uiTemp11;
		UINT	uiTemp12;

#define		BARCODE_FF_OFST			 0
#define		BARCODE_YY_OFST			 2
#define		BARCODE_MM_OFST			 4	
#define		BARCODE_WELLWELL_OFST	 6//##
#define		BARCODE_VV_OFST			 1//##

		pInfo->bSigModelUsed = TRUE;

		//1.特征字 不使用
		//g_CanData.aRoughDataConvert[iAddr][CONVERT_FEATURE].uiValue

		//2.串号低位	SERIAL_NO_LOW=580005dc
		byTempBuf[0] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_SERIAL_NO_LOW].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_SERIAL_NO_LOW].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_SERIAL_NO_LOW].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_SERIAL_NO_LOW].uiValue);

		//年
		iTemp = byTempBuf[0] & 0x1f;//year
		snprintf(pInfo->szSerialNumber + BARCODE_YY_OFST, 3, "%d%d",iTemp/10,iTemp%10);
		//月
		iTemp = byTempBuf[1] & 0x0f;
		snprintf(pInfo->szSerialNumber + BARCODE_MM_OFST, 3, "%d%d",iTemp/10,iTemp%10);	
		//串号低位的低16字节 对应	##### 代表当月生产数量
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		snprintf(pInfo->szSerialNumber + BARCODE_WELLWELL_OFST,
			6,"%05ld",uiTemp);
		pInfo->szSerialNumber[11] = '\0';//end		

		//3.版本号VERSION=ffffd8f1
		byTempBuf[0] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_VERSION_NO].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_VERSION_NO].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_VERSION_NO].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_VERSION_NO].uiValue);

		//模块版本号
		//H		A00形式，高字节按A=0 B=1类推，所以A=65
		byTempBuf[0] = 0x3f & byTempBuf[0];
		uiTemp = byTempBuf[0] + 65;
		pInfo->szHWVersion[0] = uiTemp;

		//VV 00按照实际数值存入低字节
		snprintf(pInfo->szHWVersion + BARCODE_VV_OFST, 3,
			"%d%d",byTempBuf[1]/10, byTempBuf[1]%10);
		pInfo->szHWVersion[4] = '\0';//end

		//软件版本		如若软件版本为1.30,保存内容为两两字节整型数130
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		uiTemp11 = (uiTemp - (uiTemp/100)*100)/10;
		uiTemp12 = (uiTemp - (uiTemp/100)*100)%10;
		snprintf(pInfo->szSWVersion, 5,
			"%d.%d%d",uiTemp/100, uiTemp11,uiTemp12);
		pInfo->szSWVersion[5] = '\0';//end

		//3.CODE1	ffffd8f1
		byTempBuf[0] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE1].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE1].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE1].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE1].uiValue);

		//FF 用ASCII码表示,占用CODE1 和 CODE2
		pInfo->szSerialNumber[0] = byTempBuf[0];
		pInfo->szSerialNumber[1] = byTempBuf[1];
		//T 表示产品类型,ASCII占用CODE12
		pInfo->szPartNumber[0] = byTempBuf[2];//1
		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[1] = byTempBuf[3];//S CODE13

		//3.CODE2	ffffd8f1
		byTempBuf[0] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE2].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE2].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE2].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE2].uiValue);

		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[2] = byTempBuf[0];//M CODE14
		pInfo->szPartNumber[3] = byTempBuf[1];//D CODE15
		pInfo->szPartNumber[4] = byTempBuf[2];//U CODE16
		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[5] = byTempBuf[3];//+ CODE17
		}
		else
		{
			pInfo->szPartNumber[5] = '\0';
		}		

		//3.CODE3	ffffd8f1
		byTempBuf[0] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE3].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE3].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE3].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE3].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[6] = byTempBuf[0];//CODE18
		}
		else
		{
			pInfo->szPartNumber[6] = '\0';//CODE18
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[7] = byTempBuf[1];//CODE19
		}
		else
		{
			pInfo->szPartNumber[7] = '\0';//CODE19
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[8] = byTempBuf[2];//CODE20
		}
		else
		{
			pInfo->szPartNumber[8] = '\0';//CODE20
		}

		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[9] = byTempBuf[3];//CODE21
		}
		else
		{
			pInfo->szPartNumber[9] = '\0';//CODE21
		}


		//3.CODE4		ffffd8f1
		byTempBuf[0] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE4].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE4].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE4].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_can_smduplus.aRoughData[iAddr][SM_SMDUP_BARCODE4].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[10] = byTempBuf[0];//CODE22
		}
		else
		{
			pInfo->szPartNumber[10] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[11] = byTempBuf[1];//CODE23
		}
		else
		{
			pInfo->szPartNumber[11] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[12] = byTempBuf[2];//CODE24
		}
		else
		{
			pInfo->szPartNumber[12] = '\0';
		}

		pInfo->szPartNumber[13] = '\0';//CODE25

		//printf("\nSMTEMP bSigModelUsed:%d\n", pInfo->bSigModelUsed);

		pInfo++;

	}
}
*/

/*==========================================================================*
* FUNCTION : ReceiveReconfigCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-10-10 16:55
*==========================================================================*/
static BOOL ReceiveReconfigCmd(void)
{
	if(GetDwordSigValue(SMDUP_SYS_GROUP_EQUIPID, 
		SIG_TYPE_SETTING,
		SMDUP_SYS_RECONFIG_SIG_ID,
		"CAN_SAMP") > 0)
	{
		SetDwordSigValue(SMDUP_SYS_GROUP_EQUIPID, 
			SIG_TYPE_SETTING,
			SMDUP_SYS_RECONFIG_SIG_ID,
			0,
			"CAN_SAMP");
		return TRUE;
	}

	return FALSE;
}
static BOOL SMDUP_AllIsNoRespone()
{
	INT32	i;
	INT32	iAddr;

	for (iAddr = 0; iAddr < CAN_SMDUP_MAX_NUM; iAddr++)
	{
		if(CAN_SMDUP_EXIT == g_can_smduplus.aRoughData[iAddr][SM_SMDUP_EXIST_STAT].iValue)
		{
			//只要有一个通信正常，就将返回FALSE
			if (CAN_SMDUP_COMM_OK == g_can_smduplus.aRoughData[iAddr][SM_SMDUP_COMM_STAT].iValue)
			{
				return FALSE;
			}
		}
	}
	
	return TRUE;
}
/*********************************************************************************
*  
*  FUNCTION NAME : SMDUP_ReadAllShuntSize
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-5-16 11:22:07
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static void SMDUP_ReadAllShuntSize(INT32 iAddr)
{
	//printf("\n Run Here proves beginning sample all shunt!!!");
	SmdupSampCmd2A(iAddr);
	SmdupSampCmd2B(iAddr);
	SmdupSampCmd2C(iAddr);
	return;
}

/*********************************************************************************
*  
*  FUNCTION NAME : SMDUP_Basic_Sampling
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-5-17 16:32:00
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static void SMDUP_Basic_Sampling(INT32 iActualAddr, BOOL s_SampFrequencyCtrl)
{
	SmdupSampCmd01(iActualAddr);

	SmdupSampCmd24(iActualAddr);//Current 1-10  其中判断通信中断

	SmdupSampCmd25(iActualAddr);//Current 11-20

	SmdupSampCmd26(iActualAddr);//Current 21-25


	if (s_SampFrequencyCtrl)
	{
		SmdupSampCmd27(iActualAddr);//Voltage 1-10

		SmdupSampCmd28(iActualAddr);//Voltage 11-20

		SmdupSampCmd29(iActualAddr);//Voltage 21-25

	}
	else
	{
		//Jump to Next Query,Now Don't Sampling Voltage
	}
}

/*=============================================================================*
* FUNCTION: SMDUP_ReDispatchShuntReadings(void)
* PURPOSE : Scan the smduplus equipment
*			
* INPUT:	void
*     
*
* RETURN:
*			void
*
* CALLS: 
*     void
*
* CALLED BY: 
*  
*
*============================================================================*/
static void SMDUP_ReDispatchShuntReadings()
{
	static SIG_ENUM iLastShuntSetting[CAN_SMDUP_MAX_NUM][SMDUP_SHUNT_MAX_NUM]= {0};
	int i = 0;
	int j = 0;
	SIG_ENUM		enumValue = 0;
	BOOL bNeedRefreshPage = FALSE;
	//计算总负载电流
	float fTotalSMDUPShuntCurr = 0.0;
#define SIG_ID_SHUNT1_SET_AS		126
#define EQUP_ID_SMDUP1					643
#define EQUP_ID_SMDUP9					1870
	

	int iEquipID_Start=EQUP_ID_SMDUP1;
	//not used  ,    采集的值都为  invalid	    不显示
	// general  采集的值 为	    正常的值，
	//load采集的值为显示的值 

	for (i = 0; i < CAN_SMDUP_MAX_NUM; i++)
	{
		if(CAN_SMDUP_EXIT == g_can_smduplus.aRoughData[i][SM_SMDUP_EXIST_STAT].iValue)
		{
			for(j = 0; j < SMDUP_SHUNT_MAX_NUM; j++)
			{
				if(i>=8)
				{
					iEquipID_Start=EQUP_ID_SMDUP9-8;
				}
				enumValue = GetEnumSigValue(iEquipID_Start + i, 
								SIG_TYPE_SETTING,
								SIG_ID_SHUNT1_SET_AS + j,
								"CAN_SAMP");
				//if((enumValue !=iLastShuntSetting[i][j]) && 
				//	(enumValue==3||iLastShuntSetting[i][j]==3))  //说明有电池发生变动，需要刷新页面
				//{
				//	bNeedRefreshPage = TRUE;
				//}
				//iLastShuntSetting[i][j] = enumValue;
				/*g_CanData.aRoughDataSmdu[i][iBattExChann[j]].iValue 
						= SMDU_EQUIP_NOT_EXISTENT;*/
				//以下对ShuntReadings进行赋值
				if(enumValue == 0)//NotUsed
				{
					g_can_smduplus.aRoughData[i][SM_SMDUP_READING1+j].iValue
						= g_can_smduplus.aRoughData[i][SM_SMDUP_CURRENT1+j].iValue;
					g_can_smduplus.aRoughData[i][SM_SMDUP_CURRENT1+j].iValue
						= CAN_SAMP_INVALID_VALUE;
					g_can_smduplus.aRoughData[i][SM_SMDUP_LOADCURR1+j].iValue
						= CAN_SAMP_INVALID_VALUE;
				}
				else if(enumValue == 1)//General
				{
					g_can_smduplus.aRoughData[i][SM_SMDUP_READING1+j].iValue
						= g_can_smduplus.aRoughData[i][SM_SMDUP_CURRENT1+j].iValue;
					g_can_smduplus.aRoughData[i][SM_SMDUP_LOADCURR1+j].iValue
						= CAN_SAMP_INVALID_VALUE;//CAN_SAMP_INVALID_VALUE;
					//printf("SMDUP[%d] Set as General  Value [%f]\n",i,g_can_smduplus.aRoughData[i][SM_SMDUP_CURRENT1+j].fValue);
				}
				else if(enumValue == 2)//Load
				{
					g_can_smduplus.aRoughData[i][SM_SMDUP_READING1+j].iValue = g_can_smduplus.aRoughData[i][SM_SMDUP_CURRENT1+j].iValue;

					if( CAN_SAMP_INVALID_VALUE == g_can_smduplus.aRoughData[i][SM_SMDUP_READING1+j].iValue )
					{
						g_can_smduplus.aRoughData[i][SM_SMDUP_LOADCURR1+j].fValue = 0.0;
					}
					else
					{
						g_can_smduplus.aRoughData[i][SM_SMDUP_LOADCURR1+j].fValue = g_can_smduplus.aRoughData[i][SM_SMDUP_READING1+j].fValue;
					}

					//g_can_smduplus.aRoughData[i][SM_SMDUP_CURRENT1+j].iValue
					//	= CAN_SAMP_INVALID_VALUE;//CAN_SAMP_INVALID_VALUE;
					if( g_can_smduplus.aRoughData[i][SM_SMDUP_LOADCURR1+j].fValue > 0 )
					{
						fTotalSMDUPShuntCurr+=g_can_smduplus.aRoughData[i][SM_SMDUP_LOADCURR1+j].fValue;
					}
					//printf("SMDUP[%d] Set as Load  Value [%f]  [%f]\n",i,g_can_smduplus.aRoughData[i][SM_SMDUP_LOADCURR1+j].fValue,g_can_smduplus.aRoughData[i][SM_SMDUP_CURRENT1+j].fValue);
				}


			}
		}
	}

	if(g_bIsSMDUEIBMode)
	{
		//总负载电流计算
		SetFloatSigValue(SMDUP_GROUP_EQUIPID, 
				SIG_TYPE_SAMPLING,
				2,
				fTotalSMDUPShuntCurr,
				"CAN_SAMP");
	}

}
static BOOL Is_SMDU_EIBMode()
{
#define	SYS_GROUP_EQUIPID		1
	if(GetEnumSigValue(SYS_GROUP_EQUIPID, 
		SIG_TYPE_SETTING,
		452,
		"CAN_SAMP") > 0)
	{
		return TRUE;
	}
	return FALSE;
}
/*=============================================================================*
* FUNCTION: SMDUP _Reconfig(void)
* PURPOSE : Scan the smduplus equipment
*			
* INPUT:	void
*     
*
* RETURN:
*			void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void SMDUP_Sample(void)
{
	INT32			i;
	INT32			iAddr;
	INT32			iActualAddr;
	static	BOOL	s_SampFrequencyCtrl = FALSE;

	g_bIsSMDUEIBMode = Is_SMDU_EIBMode();
	//if(g_can_smduplus.bNeedReconfig || ReceiveReconfigCmd())
	if(g_can_smduplus.bNeedReconfig)
	{
		RT_UrgencySendCurrLimit();
		SMDUP_InitRoughValue();

		//First running set TRUE in the function CAN_QueryInit()
		g_can_smduplus.bNeedReconfig = FALSE;
		SMDUP_Reconfig();
		return;
	}
	
	for (iAddr = 0; iAddr < CAN_SMDUP_MAX_NUM; iAddr++)
	{
		iActualAddr = iAddr + SMDUP_ADDR_OFFSET;

		if(CAN_SMDUP_EXIT == g_can_smduplus.aRoughData[iAddr][SM_SMDUP_EXIST_STAT].iValue)
		{

			////////////added by Jimmy for shunt size reading
			if(!g_can_smduplus.bOldSMDUP[iAddr])
			{
				//读取所有分流器系数，这里每5圈采集一次，减少采集频率

				//将基本采集与分流器采集分开，以减少总线压力
				//changed by Frank Wu,20140211,1/1, for  SMDUP blanch current count except---start---
#if 0
				static int nPeriod = 0;
				if(nPeriod <= 5 )
				{
					nPeriod++;
					SMDUP_Basic_Sampling(iActualAddr,s_SampFrequencyCtrl);
					//return;
				}
				else
				{
					SMDUP_ReadAllShuntSize(iActualAddr);
					nPeriod = 0;
				}
#else
				static int nPeriod[CAN_SMDUP_MAX_NUM] = {0,};
				if(nPeriod[iAddr] <= 5 )
				{
					nPeriod[iAddr]++;
					SMDUP_Basic_Sampling(iActualAddr,s_SampFrequencyCtrl);
					//return;
				}
				else
				{
					SMDUP_ReadAllShuntSize(iActualAddr);
					nPeriod[iAddr] = 0;
				}
#endif
				//changed by Frank Wu,20140211,1/1, for  SMDUP blanch current count except---end---
			}
			else
			{
				SMDUP_Basic_Sampling(iActualAddr,s_SampFrequencyCtrl);
				//do nothing
			}
			/////////////////////			

		}
		else
		{
			//Jump to Next Query,Now Don't Sampling Voltage
		}
	}
	
	//TRACE(" !!!!   IN  THE	SMDUP _Sample    END \n");

	s_SampFrequencyCtrl = (!s_SampFrequencyCtrl);


	//不上填充，内部使用
	if (SMDUP_AllIsNoRespone())
	{
		
		g_can_smduplus.aRoughData[0][SM_SMDUP_G_COMM_STAT].iValue = CAN_SMDUP_COMM_FAIL;
	}
	else
	{
		g_can_smduplus.aRoughData[0][SM_SMDUP_G_COMM_STAT].iValue = CAN_SMDUP_COMM_OK;
	}
	
	SMDUP_ReDispatchShuntReadings();
	
}

/*=============================================================================*
* FUNCTION: GetSmdup VirAddrBySeqNo(INT32 iSeqNo)	
* PURPOSE : 			
* INPUT:	void
*     
* RETURN:
*			void
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
static int GetSmdupVirAddrBySeqNo(INT32 iSeqNo)
{
	INT32		i;
	INT32		iVirtualAddr;


	//printf("\n GetSmdupVirAddrBySeqNo IN iSeqNo=%d \n",iSeqNo);

	for(i = 0; i < CAN_SMDUP_MAX_NUM; i++)
	{
		//printf("\n $$$$$$$$$$ i=%d SEQ=%d\n",i,g_can_smduplus.aRoughData[i][SM_SMDUP_SEQ_NUM].iValue);
		if(g_can_smduplus.aRoughData[i][SM_SMDUP_SEQ_NUM].iValue == iSeqNo)
		{
			iVirtualAddr = i;
			
			return iVirtualAddr;
		}
	}

	return -1;
}


/*=============================================================================*
* FUNCTION: SMDUP _StuffChannel(ENUMSIGNALPROC EnumProc,
*											LPVOID lpvoid)	
* PURPOSE : 
*			
* INPUT:	void
*     
* RETURN:
*			void
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void SMDUP_StuffChannel(ENUMSIGNALPROC EnumProc,
									LPVOID lpvoid)	
{

	/*#################For Debug--Start##################*/
	/*int iA=0;
	g_can_smduplus.aRoughData[0][SM_SMDUP_NUM].iValue = 16;

	for(iA=8; iA<16; iA++)
	{
		g_can_smduplus.aRoughData[iA][SM_SMDUP_EXIST_STAT].iValue = CAN_SMDUP_EXIT;
		g_can_smduplus.aRoughData[iA][SM_SMDUP_COMM_STAT].iValue = CAN_SMDUP_COMM_OK;

		g_can_smduplus.aRoughData[iA][SM_SMDUP_FEATURE].iValue = g_can_smduplus.aRoughData[0][SM_SMDUP_FEATURE].iValue;
		g_can_smduplus.aRoughData[iA][SM_SMDUP_SERIAL_NO].iValue = g_can_smduplus.aRoughData[0][SM_SMDUP_SERIAL_NO].iValue;
		g_can_smduplus.aRoughData[iA][SM_SMDUP_SERIAL_NO_HIGH].iValue = g_can_smduplus.aRoughData[0][SM_SMDUP_SERIAL_NO_HIGH].iValue;
		g_can_smduplus.aRoughData[iA][SM_SMDUP_SERIAL_NO_LOW].iValue = g_can_smduplus.aRoughData[0][SM_SMDUP_SERIAL_NO_LOW].iValue;

		g_can_smduplus.aRoughData[iA][SM_SMDUP_BARCODE1].iValue = g_can_smduplus.aRoughData[0][SM_SMDUP_BARCODE1].iValue;
		g_can_smduplus.aRoughData[iA][SM_SMDUP_BARCODE2].iValue = g_can_smduplus.aRoughData[0][SM_SMDUP_BARCODE2].iValue;
		g_can_smduplus.aRoughData[iA][SM_SMDUP_BARCODE3].iValue = g_can_smduplus.aRoughData[0][SM_SMDUP_BARCODE3].iValue;
		g_can_smduplus.aRoughData[iA][SM_SMDUP_BARCODE4].iValue = g_can_smduplus.aRoughData[0][SM_SMDUP_BARCODE4].iValue;
		g_can_smduplus.aRoughData[iA][SM_SMDUP_VERSION_NO].iValue = g_can_smduplus.aRoughData[0][SM_SMDUP_VERSION_NO].iValue;
	}*/
	/*#################For Debug--End##################*/


	INT32				i,j,k;
	SAMPLING_VALUE		stTempEquipExist;

	CHANNEL_TO_ROUGH_DATA		Smdup_G_Sig[] =
	{
		{SM_CH_SMDUP_GROUP_STATE,		SM_SMDUP_GROUP_STATE,	},
		{SM_CH_SMDUP_NUM,				SM_SMDUP_NUM,			},
		//{SM_CH_SMDUP_GROUP_COMM_STATE,	SM_SMDUP_G_COMM_STAT	},//3月22日增加,其实是不需要的	保留

		{SMDUP_CH_END_FLAG,				SMDUP_CH_END_FLAG,		},//end
	};

	CHANNEL_TO_ROUGH_DATA		SmdupSig[] =
	{
		//added by Jimmy 
		{SM_CH_SMDUP_SHUNT_SIZE_CONFILICT,	ROUGH_SMDUP_SHUNT_SIZE_CONFILICT,},
		{SM_CH_SMDUP_SHUNT_SIZE_SETTABLE,	ROUGH_SMDUP_SHUNT_SIZE_SETTABLE,},
		//end add
		{SM_CH_SMDUP_FEATURE,			SM_SMDUP_FEATURE,		},
		{SM_CH_SMDUP_SERIAL_NO,			SM_SMDUP_SERIAL_NO,		},
		{SM_CH_SMDUP_SERIAL_NO_HIGH,	SM_SMDUP_SERIAL_NO_HIGH,},
		{SM_CH_SMDUP_SERIAL_NO_LOW,		SM_SMDUP_SERIAL_NO_LOW,	},
		{SM_CH_SMDUP_BARCODE1,			SM_SMDUP_BARCODE1,		},
		{SM_CH_SMDUP_BARCODE2,			SM_SMDUP_BARCODE2,		},
		{SM_CH_SMDUP_BARCODE3,			SM_SMDUP_BARCODE3,		},
		{SM_CH_SMDUP_BARCODE4,			SM_SMDUP_BARCODE4,		},
		{SM_CH_SMDUP_VERSION_NO,		SM_SMDUP_VERSION_NO,	},
		{SM_CH_SMDUP_CURRENT1,			SM_SMDUP_CURRENT1,		},
		{SM_CH_SMDUP_CURRENT2,			SM_SMDUP_CURRENT2,		},
		{SM_CH_SMDUP_CURRENT3,			SM_SMDUP_CURRENT3,		},
		{SM_CH_SMDUP_CURRENT4,			SM_SMDUP_CURRENT4,		},
		{SM_CH_SMDUP_CURRENT5,			SM_SMDUP_CURRENT5,		},
		{SM_CH_SMDUP_CURRENT6,			SM_SMDUP_CURRENT6,		},
		{SM_CH_SMDUP_CURRENT7,			SM_SMDUP_CURRENT7,		},
		{SM_CH_SMDUP_CURRENT8,			SM_SMDUP_CURRENT8,		},
		{SM_CH_SMDUP_CURRENT9,			SM_SMDUP_CURRENT9,		},
		{SM_CH_SMDUP_CURRENT10,			SM_SMDUP_CURRENT10,		},
		{SM_CH_SMDUP_CURRENT11,			SM_SMDUP_CURRENT11,		},
		{SM_CH_SMDUP_CURRENT12,			SM_SMDUP_CURRENT12,		},
		{SM_CH_SMDUP_CURRENT13,			SM_SMDUP_CURRENT13,		},
		{SM_CH_SMDUP_CURRENT14,			SM_SMDUP_CURRENT14,		},
		{SM_CH_SMDUP_CURRENT15,			SM_SMDUP_CURRENT15,		},
		{SM_CH_SMDUP_CURRENT16,			SM_SMDUP_CURRENT16,		},
		{SM_CH_SMDUP_CURRENT17,			SM_SMDUP_CURRENT17,		},
		{SM_CH_SMDUP_CURRENT18,			SM_SMDUP_CURRENT18,		},
		{SM_CH_SMDUP_CURRENT19,			SM_SMDUP_CURRENT19,		},
		{SM_CH_SMDUP_CURRENT20,			SM_SMDUP_CURRENT20,		},
		{SM_CH_SMDUP_CURRENT21,			SM_SMDUP_CURRENT21,		},
		{SM_CH_SMDUP_CURRENT22,			SM_SMDUP_CURRENT22,		},
		{SM_CH_SMDUP_CURRENT23,			SM_SMDUP_CURRENT23,		},
		{SM_CH_SMDUP_CURRENT24,			SM_SMDUP_CURRENT24,		},
		{SM_CH_SMDUP_CURRENT25,			SM_SMDUP_CURRENT25,		},
		{SM_CH_SMDUP_VOLT1,				SM_SMDUP_VOLT1,			},
		{SM_CH_SMDUP_VOLT2,				SM_SMDUP_VOLT2,			},
		{SM_CH_SMDUP_VOLT3,				SM_SMDUP_VOLT3,			},
		{SM_CH_SMDUP_VOLT4,				SM_SMDUP_VOLT4,			},
		{SM_CH_SMDUP_VOLT5,				SM_SMDUP_VOLT5,			},
		{SM_CH_SMDUP_VOLT6,				SM_SMDUP_VOLT6,			},
		{SM_CH_SMDUP_VOLT7,				SM_SMDUP_VOLT7,			},
		{SM_CH_SMDUP_VOLT8,				SM_SMDUP_VOLT8,			},
		{SM_CH_SMDUP_VOLT9,				SM_SMDUP_VOLT9,			},
		{SM_CH_SMDUP_VOLT10,			SM_SMDUP_VOLT10,		},
		{SM_CH_SMDUP_VOLT11,			SM_SMDUP_VOLT11,		},
		{SM_CH_SMDUP_VOLT12,			SM_SMDUP_VOLT12,		},
		{SM_CH_SMDUP_VOLT13,			SM_SMDUP_VOLT13,		},
		{SM_CH_SMDUP_VOLT14,			SM_SMDUP_VOLT14,		},
		{SM_CH_SMDUP_VOLT15,			SM_SMDUP_VOLT15,		},
		{SM_CH_SMDUP_VOLT16,			SM_SMDUP_VOLT16,		},
		{SM_CH_SMDUP_VOLT17,			SM_SMDUP_VOLT17,		},
		{SM_CH_SMDUP_VOLT18,			SM_SMDUP_VOLT18,		},
		{SM_CH_SMDUP_VOLT19,			SM_SMDUP_VOLT19,		},
		{SM_CH_SMDUP_VOLT20,			SM_SMDUP_VOLT20,		},
		{SM_CH_SMDUP_VOLT21,			SM_SMDUP_VOLT21,		},
		{SM_CH_SMDUP_VOLT22,			SM_SMDUP_VOLT22,		},
		{SM_CH_SMDUP_VOLT23,			SM_SMDUP_VOLT23,		},
		{SM_CH_SMDUP_VOLT24,			SM_SMDUP_VOLT24,		},
		{SM_CH_SMDUP_VOLT25,			SM_SMDUP_VOLT25,		},
		//fuse unit 
		{SM_CH_SMDUP_FUSESTAT1,			SM_SMDUP_FUSESTAT1,		},
		{SM_CH_SMDUP_FUSESTAT2,			SM_SMDUP_FUSESTAT2,		},
		{SM_CH_SMDUP_FUSESTAT3,			SM_SMDUP_FUSESTAT3,		},
		{SM_CH_SMDUP_FUSESTAT4,			SM_SMDUP_FUSESTAT4,		},
		{SM_CH_SMDUP_FUSESTAT5,			SM_SMDUP_FUSESTAT5,		},
		{SM_CH_SMDUP_FUSESTAT6,			SM_SMDUP_FUSESTAT6,		},
		{SM_CH_SMDUP_FUSESTAT7,			SM_SMDUP_FUSESTAT7,		},
		{SM_CH_SMDUP_FUSESTAT8,			SM_SMDUP_FUSESTAT8,		},
		{SM_CH_SMDUP_FUSESTAT9,			SM_SMDUP_FUSESTAT9,		},
		{SM_CH_SMDUP_FUSESTAT10,		SM_SMDUP_FUSESTAT10,	},
		{SM_CH_SMDUP_FUSESTAT11,		SM_SMDUP_FUSESTAT11,	},
		{SM_CH_SMDUP_FUSESTAT12,		SM_SMDUP_FUSESTAT12,	},
		{SM_CH_SMDUP_FUSESTAT13,		SM_SMDUP_FUSESTAT13,	},
		{SM_CH_SMDUP_FUSESTAT14,		SM_SMDUP_FUSESTAT14,	},
		{SM_CH_SMDUP_FUSESTAT15,		SM_SMDUP_FUSESTAT15,	},
		{SM_CH_SMDUP_FUSESTAT16,		SM_SMDUP_FUSESTAT16,	},
		{SM_CH_SMDUP_FUSESTAT17,		SM_SMDUP_FUSESTAT17,	},
		{SM_CH_SMDUP_FUSESTAT18,		SM_SMDUP_FUSESTAT18,	},
		{SM_CH_SMDUP_FUSESTAT19,		SM_SMDUP_FUSESTAT19,	},
		{SM_CH_SMDUP_FUSESTAT20,		SM_SMDUP_FUSESTAT20,	},
		{SM_CH_SMDUP_FUSESTAT21,		SM_SMDUP_FUSESTAT21,	},
		{SM_CH_SMDUP_FUSESTAT22,		SM_SMDUP_FUSESTAT22,	},
		{SM_CH_SMDUP_FUSESTAT23,		SM_SMDUP_FUSESTAT23,	},
		{SM_CH_SMDUP_FUSESTAT24,		SM_SMDUP_FUSESTAT24,	},
		{SM_CH_SMDUP_FUSESTAT25,		SM_SMDUP_FUSESTAT25,	},

		{SM_CH_SMDUP_LOADCURR1,			SM_SMDUP_LOADCURR1,		},
		{SM_CH_SMDUP_LOADCURR2,			SM_SMDUP_LOADCURR2,		},
		{SM_CH_SMDUP_LOADCURR3,			SM_SMDUP_LOADCURR3,		},
		{SM_CH_SMDUP_LOADCURR4,			SM_SMDUP_LOADCURR4,		},
		{SM_CH_SMDUP_LOADCURR5,			SM_SMDUP_LOADCURR5,		},
		{SM_CH_SMDUP_LOADCURR6,			SM_SMDUP_LOADCURR6,		},
		{SM_CH_SMDUP_LOADCURR7,			SM_SMDUP_LOADCURR7,		},
		{SM_CH_SMDUP_LOADCURR8,			SM_SMDUP_LOADCURR8,		},
		{SM_CH_SMDUP_LOADCURR9,			SM_SMDUP_LOADCURR9,		},
		{SM_CH_SMDUP_LOADCURR10,			SM_SMDUP_LOADCURR10,		},
		{SM_CH_SMDUP_LOADCURR11,			SM_SMDUP_LOADCURR11,		},
		{SM_CH_SMDUP_LOADCURR12,			SM_SMDUP_LOADCURR12,		},
		{SM_CH_SMDUP_LOADCURR13,			SM_SMDUP_LOADCURR13,		},
		{SM_CH_SMDUP_LOADCURR14,			SM_SMDUP_LOADCURR14,		},
		{SM_CH_SMDUP_LOADCURR15,			SM_SMDUP_LOADCURR15,		},
		{SM_CH_SMDUP_LOADCURR16,			SM_SMDUP_LOADCURR16,		},
		{SM_CH_SMDUP_LOADCURR17,			SM_SMDUP_LOADCURR17,		},
		{SM_CH_SMDUP_LOADCURR18,			SM_SMDUP_LOADCURR18,		},
		{SM_CH_SMDUP_LOADCURR19,			SM_SMDUP_LOADCURR19,		},
		{SM_CH_SMDUP_LOADCURR20,			SM_SMDUP_LOADCURR20,		},
		{SM_CH_SMDUP_LOADCURR21,			SM_SMDUP_LOADCURR21,		},
		{SM_CH_SMDUP_LOADCURR22,			SM_SMDUP_LOADCURR22,		},
		{SM_CH_SMDUP_LOADCURR23,			SM_SMDUP_LOADCURR23,		},
		{SM_CH_SMDUP_LOADCURR24,			SM_SMDUP_LOADCURR24,		},
		{SM_CH_SMDUP_LOADCURR25,			SM_SMDUP_LOADCURR25,		},

		{SM_CH_SMDUP_READING1,			SM_SMDUP_READING1,		},
		{SM_CH_SMDUP_READING2,			SM_SMDUP_READING2,		},
		{SM_CH_SMDUP_READING3,			SM_SMDUP_READING3,		},
		{SM_CH_SMDUP_READING4,			SM_SMDUP_READING4,		},
		{SM_CH_SMDUP_READING5,			SM_SMDUP_READING5,		},
		{SM_CH_SMDUP_READING6,			SM_SMDUP_READING6,		},
		{SM_CH_SMDUP_READING7,			SM_SMDUP_READING7,		},
		{SM_CH_SMDUP_READING8,			SM_SMDUP_READING8,		},
		{SM_CH_SMDUP_READING9,			SM_SMDUP_READING9,		},
		{SM_CH_SMDUP_READING10,			SM_SMDUP_READING10,		},
		{SM_CH_SMDUP_READING11,			SM_SMDUP_READING11,		},
		{SM_CH_SMDUP_READING12,			SM_SMDUP_READING12,		},
		{SM_CH_SMDUP_READING13,			SM_SMDUP_READING13,		},
		{SM_CH_SMDUP_READING14,			SM_SMDUP_READING14,		},
		{SM_CH_SMDUP_READING15,			SM_SMDUP_READING15,		},
		{SM_CH_SMDUP_READING16,			SM_SMDUP_READING16,		},
		{SM_CH_SMDUP_READING17,			SM_SMDUP_READING17,		},
		{SM_CH_SMDUP_READING18,			SM_SMDUP_READING18,		},
		{SM_CH_SMDUP_READING19,			SM_SMDUP_READING19,		},
		{SM_CH_SMDUP_READING20,			SM_SMDUP_READING20,		},
		{SM_CH_SMDUP_READING21,			SM_SMDUP_READING21,		},
		{SM_CH_SMDUP_READING22,			SM_SMDUP_READING22,		},
		{SM_CH_SMDUP_READING23,			SM_SMDUP_READING23,		},
		{SM_CH_SMDUP_READING24,			SM_SMDUP_READING24,		},
		{SM_CH_SMDUP_READING25,			SM_SMDUP_READING25,		},

		{SM_CH_SMDUP_FUSE_COMM_STAT,	SM_SMDUP_COMM_STAT,		},
		{SM_CH_SMDUP_COMM_STAT,			SM_SMDUP_COMM_STAT,		},
		{SM_CH_SMDUP_FUSE_EXIST_STAT,	SM_SMDUP_EXIST_STAT,	},
		{SM_CH_SMDUP_EXIST_STAT,		SM_SMDUP_EXIST_STAT,	},


		{SMDUP_CH_END_FLAG,				SMDUP_CH_END_FLAG}		//end
	};


	//Stuff Group signals
	i = 0;
	while(Smdup_G_Sig[i].iChannel != SMDUP_CH_END_FLAG)
	{
		EnumProc(Smdup_G_Sig[i].iChannel, 
				g_can_smduplus.aRoughData[0][Smdup_G_Sig[i].iRoughData].fValue, 
				lpvoid );
		i++;
	}

	for (i = 0; i < CAN_SMDUP_MAX_NUM; i++)
	{
		if(CAN_SMDUP_EXIT == g_can_smduplus.aRoughData[i][SM_SMDUP_EXIST_STAT].iValue)
		{
			j = 0;
			while(SmdupSig[j].iChannel != SMDUP_CH_END_FLAG)
			{
				if( i < CAN_SMDUP_NUM1 )
				{
					EnumProc(i * SMDUP_CH_PER_CH + SmdupSig[j].iChannel, 
						g_can_smduplus.aRoughData[i][SmdupSig[j].iRoughData].fValue, 
						lpvoid );
				}
				else
				{
					if(SmdupSig[j].iChannel >=	SM_CH_SMDUP_FUSESTAT1)
					{
						EnumProc((i-CAN_SMDUP_NUM1) * SMDUP_CH_PER_CH + SmdupSig[j].iChannel+SMDUPFUSE_OFFSET, 
							g_can_smduplus.aRoughData[i][SmdupSig[j].iRoughData].fValue, 
							lpvoid );
					}
					else
					{
						EnumProc((i-CAN_SMDUP_NUM1) * SMDUP_CH_PER_CH + SmdupSig[j].iChannel+SMDUP_OFFSET, 
							g_can_smduplus.aRoughData[i][SmdupSig[j].iRoughData].fValue, 
							lpvoid );
					}
				}
				j++;
			}
		}
		else
		{
			j = 0;
			stTempEquipExist.iValue = CAN_SMDUP_INVALID_VALUE;
			while(SmdupSig[j].iChannel != SMDUP_CH_END_FLAG)
			{
				if( i < CAN_SMDUP_NUM1 )
				{
					EnumProc(i * SMDUP_CH_PER_CH + SmdupSig[j].iChannel, 
						stTempEquipExist.fValue, 
						lpvoid );
				}
				else
				{
					if(SmdupSig[j].iChannel >=	SM_CH_SMDUP_FUSESTAT1
						&& SmdupSig[j].iChannel <=SM_CH_SMDUP_FUSE_EXIST_STAT)
					{
						EnumProc((i-CAN_SMDUP_NUM1) * SMDUP_CH_PER_CH + SmdupSig[j].iChannel+SMDUPFUSE_OFFSET, 
							g_can_smduplus.aRoughData[i][SmdupSig[j].iRoughData].fValue, 
							lpvoid );
					}
					else
					{
						EnumProc((i-CAN_SMDUP_NUM1) * SMDUP_CH_PER_CH + SmdupSig[j].iChannel+SMDUP_OFFSET, 
							g_can_smduplus.aRoughData[i][SmdupSig[j].iRoughData].fValue, 
							lpvoid );
					}
				}
				j++;
			}
			stTempEquipExist.iValue = CAN_SMDUP_NOT_EXIT;
			
			if( i < CAN_SMDUP_NUM1 )
			{
				EnumProc(i * SMDUP_CH_PER_CH + SM_CH_SMDUP_EXIST_STAT, 
					stTempEquipExist.fValue, 
					lpvoid );
				EnumProc(i * SMDUP_CH_PER_CH + SM_CH_SMDUP_FUSE_EXIST_STAT, 
					stTempEquipExist.fValue, 
					lpvoid );
			}
			else
			{
				EnumProc((i-CAN_SMDUP_NUM1) * SMDUP_CH_PER_CH + SM_CH_SMDUP_EXIST_STAT+SMDUP_OFFSET, 
					stTempEquipExist.fValue, 
					lpvoid );
				EnumProc((i-CAN_SMDUP_NUM1) * SMDUP_CH_PER_CH + SM_CH_SMDUP_FUSE_EXIST_STAT+SMDUPFUSE_OFFSET, 
					stTempEquipExist.fValue, 
					lpvoid );
			}
		}

	}

}

/*********************************************************************************
*  
*  FUNCTION NAME : SMDUP_Get_mVA_FromUint
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-5-16 16:52:17
*  DESCRIPTION   : Get mV and A value form raw data
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
LOCAL UINT SMDUP_Get_mVA_FromUint(int imV,UINT uiSize)
{
	UINT uiResult = 0;
	if(imV == 0) //取mV值
	{
		uiResult = uiSize & 0x0000FFFF;	
		//uiResult = (uiSize & 0xFFFF0000) >> 16 ;
	}
	else//取电流A值
	{
		//uiResult = uiSize & 0x0000FFFF;	
		uiResult = (uiSize & 0xFFFF0000) >> 16 ;
	}
	return uiResult;
}
/*********************************************************************************
*  
*  FUNCTION NAME : 
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-3-6 16:18:38
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
LOCAL BYTE* Combine2Uint(ULONG uiVal1,ULONG uiVal2)
{
	BYTE byTemp[4];
	byTemp[0] = (BYTE)((uiVal1 & 0xFF00)>>8);
	byTemp[1] = (BYTE)(uiVal1 & 0x00FF);
	byTemp[2] = (BYTE)((uiVal2 & 0xFF00)>>8);
	byTemp[3] = (BYTE)(uiVal2 & 0x00FF);
	return byTemp;
}
/*********************************************************************************
*  
*  FUNCTION NAME : 
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-3-6 16:18:38
*  DESCRIPTION   : 
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
LOCAL void PackAndSendCmdForShuntSize(UINT uiMsgType,
				      UINT uiDestAddr,
				      UINT uiCmdType,
				      UINT uiValueTypeH,
				      UINT uiValueTypeL,
				      ULONG ulVar1,ULONG ulVar2)
{
	BYTE*	pbySendBuf = g_can_smduplus.cBySendBuf;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_SMDU_CONTROLLER,
		uiDestAddr, 
		CAN_SELF_ADDR,
		CAN_ERR_CODE,
		CAN_FRAME_DATA_LEN,
		uiCmdType,
		pbySendBuf);
	/////////////////////for debug only
	//int i;
	//printf("\n*****The sent package is: ");
	//for(i=0;i<13;i++)
	//{
	//	printf("%2X",g_can_DCEM.cBySendBuf[i]);
	//
	//}
	//printf(" *****end\n ");
	////////////end 

	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	//CAN_FloatToString(ulParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);
	int	i;	
	BYTE* strOutput = pbySendBuf + CAN_FRAME_OFFSET_VALUE;
	BYTE* pbyTempVar = Combine2Uint(ulVar1,ulVar2);

	for(i = 0; i < 4; i++)
	{		
		strOutput[i] = pbyTempVar[i];
	}
	//printf("**********Original values are %d:%d:%d:%d;\n", strOutput[0],strOutput[1],strOutput[2],strOutput[3]);
	write(g_CanData.CanCommInfo.iCanHandle, 
		(void *)pbySendBuf,
		CAN_FRAME_LEN);

	return;
}
/*********************************************************************************
*  
*  FUNCTION NAME : SMDUP_Param_Unify
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-5-16 11:07:54
*  DESCRIPTION   : For CR - shunt setting page
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
void SMDUP_Param_Unify(void)
{
#define SMDUP_SHUNT_NUM		25
#define SMDUP_CONFILIT_TIMES	3
	PARAM_UNIFY_IDX		ParamIdx[] =
	{
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT1_MV,	SMDUP_SETTING_SHUNT1_MV, ROUGH_SMDUP_SHUNT_SIZE1, SM_VALUE_TYPE_SHUNTSIZE1,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT2_MV,	SMDUP_SETTING_SHUNT2_MV, ROUGH_SMDUP_SHUNT_SIZE2, SM_VALUE_TYPE_SHUNTSIZE2,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT3_MV,	SMDUP_SETTING_SHUNT3_MV, ROUGH_SMDUP_SHUNT_SIZE3, SM_VALUE_TYPE_SHUNTSIZE3,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT4_MV,	SMDUP_SETTING_SHUNT4_MV, ROUGH_SMDUP_SHUNT_SIZE4, SM_VALUE_TYPE_SHUNTSIZE4,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT5_MV,	SMDUP_SETTING_SHUNT5_MV, ROUGH_SMDUP_SHUNT_SIZE5, SM_VALUE_TYPE_SHUNTSIZE5,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT6_MV,	SMDUP_SETTING_SHUNT6_MV, ROUGH_SMDUP_SHUNT_SIZE6, SM_VALUE_TYPE_SHUNTSIZE6,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT7_MV,	SMDUP_SETTING_SHUNT7_MV, ROUGH_SMDUP_SHUNT_SIZE7, SM_VALUE_TYPE_SHUNTSIZE7,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT8_MV,	SMDUP_SETTING_SHUNT8_MV, ROUGH_SMDUP_SHUNT_SIZE8, SM_VALUE_TYPE_SHUNTSIZE8,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT9_MV,	SMDUP_SETTING_SHUNT9_MV, ROUGH_SMDUP_SHUNT_SIZE9, SM_VALUE_TYPE_SHUNTSIZE9,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT10_MV,SMDUP_SETTING_SHUNT10_MV,ROUGH_SMDUP_SHUNT_SIZE10,SM_VALUE_TYPE_SHUNTSIZE10,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT11_MV,SMDUP_SETTING_SHUNT11_MV,ROUGH_SMDUP_SHUNT_SIZE11,SM_VALUE_TYPE_SHUNTSIZE11,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT12_MV,SMDUP_SETTING_SHUNT12_MV,ROUGH_SMDUP_SHUNT_SIZE12,SM_VALUE_TYPE_SHUNTSIZE12,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT13_MV,SMDUP_SETTING_SHUNT13_MV,ROUGH_SMDUP_SHUNT_SIZE13,SM_VALUE_TYPE_SHUNTSIZE13,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT14_MV,SMDUP_SETTING_SHUNT14_MV,ROUGH_SMDUP_SHUNT_SIZE14,SM_VALUE_TYPE_SHUNTSIZE14,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT15_MV,SMDUP_SETTING_SHUNT15_MV,ROUGH_SMDUP_SHUNT_SIZE15,SM_VALUE_TYPE_SHUNTSIZE15,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT16_MV,SMDUP_SETTING_SHUNT16_MV,ROUGH_SMDUP_SHUNT_SIZE16,SM_VALUE_TYPE_SHUNTSIZE16,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT17_MV,SMDUP_SETTING_SHUNT17_MV,ROUGH_SMDUP_SHUNT_SIZE17,SM_VALUE_TYPE_SHUNTSIZE17,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT18_MV,SMDUP_SETTING_SHUNT18_MV,ROUGH_SMDUP_SHUNT_SIZE18,SM_VALUE_TYPE_SHUNTSIZE18,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT19_MV,SMDUP_SETTING_SHUNT19_MV,ROUGH_SMDUP_SHUNT_SIZE19,SM_VALUE_TYPE_SHUNTSIZE19,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT20_MV,SMDUP_SETTING_SHUNT20_MV,ROUGH_SMDUP_SHUNT_SIZE20,SM_VALUE_TYPE_SHUNTSIZE20,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT21_MV,SMDUP_SETTING_SHUNT21_MV,ROUGH_SMDUP_SHUNT_SIZE21,SM_VALUE_TYPE_SHUNTSIZE21,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT22_MV,SMDUP_SETTING_SHUNT22_MV,ROUGH_SMDUP_SHUNT_SIZE22,SM_VALUE_TYPE_SHUNTSIZE22,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT23_MV,SMDUP_SETTING_SHUNT23_MV,ROUGH_SMDUP_SHUNT_SIZE23,SM_VALUE_TYPE_SHUNTSIZE23,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT24_MV,SMDUP_SETTING_SHUNT24_MV,ROUGH_SMDUP_SHUNT_SIZE24,SM_VALUE_TYPE_SHUNTSIZE24,},
		{EQUIP_ID_SMDUP_UNIT,1,SMDUP_SETTING_SHUNT25_MV,SMDUP_SETTING_SHUNT25_MV,ROUGH_SMDUP_SHUNT_SIZE25,SM_VALUE_TYPE_SHUNTSIZE25,},
	
		{0, 0, SMDUP_CH_END_FLAG, SMDUP_CH_END_FLAG, 0, 0,},
	};

	int 	iEqpID=0;
	INT32	iAddr;
	INT32	iActualAddr;
	int	j = 0;
	DWORD	dwWeb_mV,dwWeb_A; //web页面的用户设置值
	DWORD	uiSampleValue_mV,uiSampleValue_A;//通过CAN采集上来的值	
	int	iSigId = 0;
	BOOL	bShuntSizeConflict = FALSE; //Flag
	SIG_ENUM stVoltLevelState = GetEnumSigValue(1,2,224,"SMDUP Get SysVolt Level");

	static int s_iCountConflictNum[CAN_SMDUP_MAX_NUM][SMDUP_SHUNT_NUM]={0};

	for(iAddr = 0; iAddr < CAN_SMDUP_MAX_NUM; iAddr++)
	{
		if(CAN_SMDUP_EXIT == g_can_smduplus.aRoughData[iAddr][SM_SMDUP_EXIST_STAT].iValue
			&& (SIG_VALUE_SHUNTSIZE_SOFTWARE == g_can_smduplus.aRoughData[iAddr][ROUGH_SMDUP_SHUNT_SIZE_SETTABLE].iValue))
			//&& (!g_can_smduplus.bOldSMDUP[iAddr])) //在软件设置状态下才下发
		{
			j = 0;
			bShuntSizeConflict = FALSE;
			while(ParamIdx[j].iSigId != SMDUP_CH_END_FLAG)
			{
				//if(g_CanData.aRoughDataGroup[GROUP_RT_RATED_VOLT].fValue > 35.0)
				if(0 == stVoltLevelState)//48 V
				{
					iSigId = ParamIdx[j].iSigId;
				}
				else
				{
					iSigId = ParamIdx[j].iSigId24V;
				}

				uiSampleValue_mV = SMDUP_Get_mVA_FromUint(0,g_can_smduplus.aRoughData[iAddr][ParamIdx[j].iRoughData].dwValue); //mV
				uiSampleValue_A = SMDUP_Get_mVA_FromUint(1,g_can_smduplus.aRoughData[iAddr][ParamIdx[j].iRoughData].dwValue); //A
				//printf("\nSMDUP %d*******Now the Channel%d shunt size is: %d,%d\n",iAddr+1,j+1,uiSampleValue_mV,uiSampleValue_A);

				if( iAddr < CAN_SMDUP_NUM1)
				{
					iEqpID =ParamIdx[j].iEquipId + iAddr * ParamIdx[j].iEquipIdDifference;
				}
				else 
				{
					iEqpID = 1870 + (iAddr - CAN_SMDUP_NUM1);
				}
				
				dwWeb_mV = GetDwordSigValue(iEqpID, 
					SIG_TYPE_SETTING,
					iSigId,
					"CAN_SAMP");
				dwWeb_A = GetDwordSigValue(iEqpID, 
					SIG_TYPE_SETTING,
					iSigId + 1,
					"CAN_SAMP");

				if((dwWeb_A != uiSampleValue_A) || (dwWeb_mV != uiSampleValue_mV))
				{
					//g_can_DCEM.aRoughData[i][ParamIdx[j].iRoughData].dwValue
					//	= uiParam_mV; //mV
					//g_can_DCEM.aRoughData[i][ParamIdx[j].iRoughData + 1].dwValue
					//	= uiParam_A; //A
					//printf("\n*******Now the Channel%d shunt size is: %d,%d\n",j+1,uiSampleValue_mV,uiSampleValue_A);
					
					if(s_iCountConflictNum[iAddr][j] > SMDUP_CONFILIT_TIMES)
					{
						bShuntSizeConflict = TRUE;
					}
					else
					{
						s_iCountConflictNum[iAddr][j]++;
					}
					iActualAddr = iAddr + SMDUP_ADDR_OFFSET;
					PackAndSendCmdForShuntSize(MSG_TYPE_RQST_SETTINGS,
						(UINT)iActualAddr,
						CAN_CMD_TYPE_P2P,
						SM_VALUE_TYPE_SHUNTSIZE_VH,
						ParamIdx[j].uiValueTypeL,
						dwWeb_A,dwWeb_mV); //电流在前，电压在后
					Sleep(100);
				}
				else
				{
					//clear the size conflict count number
					s_iCountConflictNum[iAddr][j] = 0;
				}

				j++;
			}
			//判断有分流器系数正在设置过程中
			g_can_smduplus.aRoughData[iAddr][ROUGH_SMDUP_SHUNT_SIZE_CONFILICT].uiValue = (UINT)bShuntSizeConflict;
		}
		else
		{
			//printf("\nNo Support setting shunt!!");
		}
	}

}

static void PackAndSendSmdupCmd_New(UINT uiMsgType,
							   UINT uiDestAddr,
							   UINT uiCmdType,
							   UINT uiValueTypeH,
							   UINT uiValueTypeL,
							   int	iParamType,
							   float fParam,
							   int iParam)
{
	BYTE*	pbySendBuf = g_can_smduplus.cBySendBuf;
	BYTE* strOutput1 = NULL;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_SMDU_CONTROLLER,
		uiDestAddr, 
		CAN_SELF_ADDR,
		CAN_ERR_CODE,
		CAN_FRAME_DATA_LEN,
		uiCmdType,
		pbySendBuf);

	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	if( 0 == iParamType )
	{
		CAN_UintToString(iParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);
	}
	else
	{		
		CAN_FloatToString(fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);	
	}

	write(g_CanData.CanCommInfo.iCanHandle, 
		(void *)pbySendBuf,
		CAN_FRAME_LEN);

	return;
}


int GetSmdupAddrByChnNo(IN int iChannelNo)
{
	int i;

	int iSeqNo = (iChannelNo - CNMR_MAX_SMDUP_BROADCAST_CMD_CHN - 1) 
		/ SMDUP_CH_PER_CH;
	

	for(i = 0; i < CAN_SMDUP_MAX_NUM; i++)
	{
		if(iSeqNo == i)
		{
			return i;
		}
	}

	return 0;
}


void Virtual_SMDUP_Control()
{
	UINT ValueTypeH  = 0;
	UINT ValueTypeL  = 0 ; 
	UINT iResendCount = 0;
	UINT iAddr=0xff;//broadcast
	UINT j = 0;
	int iParam = 0x5A5A;
	 
	ValueTypeH  = 0x00;
	ValueTypeL = 0x30;

	iResendCount =1;
	for(j = 0; j < iResendCount; j++)
	{
		//for(iAddr = 0; iAddr < CAN_SMDUP_MAX_NUM; iAddr++)
		//{
			//if(CAN_SMDUP_EXIT == g_can_smduplus.aRoughData[iAddr][SM_SMDUP_EXIST_STAT].iValue)
			//{

				PackAndSendSmdupCmd_New(	MSG_TYPE_RQST_SETTINGS,
							   				CAN_ADDR_FOR_BROADCAST,
							   				CAN_CMD_TYPE_BROADCAST,
							  				ValueTypeH,
							   				ValueTypeL,
							   				0,
							   				0.0,
							   				iParam );
				
			//}
		//}
	}
		
	return;
}


void SMDUP_SendCtlCmd(IN int iChannelNo, IN float fParam)
{
#define SMDUPonCAN1 0

	int	iSmdupAddr,iSubChnNo,iSeqNo;
	
	UINT uiCANPort = SMDUPonCAN1;

	if(iChannelNo < CNMR_MAX_SMDUP_BROADCAST_CMD_CHN)
	{
		switch(iChannelNo)
		{
			default:

				CAN_LOG_OUT(APP_LOG_WARNING, "SMDUP_SendCtlCmd():Invalid control channel!\n");
				break;
		}
		return;
	}
	else
	{
		iSmdupAddr = GetSmdupAddrByChnNo(iChannelNo);

		iSubChnNo = ((iChannelNo - CNMR_MAX_SMDUP_BROADCAST_CMD_CHN - 1)
			% SMDUP_CH_PER_CH) 
			+ CNMR_MAX_SMDUP_BROADCAST_CMD_CHN 
			+ 1;

		if (( iSubChnNo >=  CNSS_SMDUP_CR_0789_19_BROADCAST_CMD ) && ( iSubChnNo <=  CNSS_SMDUP_CR_0789_19_BROADCAST_CMD ) )
		{
			 Virtual_SMDUP_Control();
		}
		else
		{
			
		}	
	}
	
	return;
}



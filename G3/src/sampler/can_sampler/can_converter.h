/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : NGMC-HF(Next Generation Controller - High Feature)
 *
 *  FILENAME : can_converter.h
 *  CREATOR  : Frank Cao                DATE: 2008-07-01 11:34
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/


#ifndef __CAN_CONVERTER_H
#define __CAN_CONVERTER_H

#include <stdio.h>
#include "basetypes.h"
#include "err_code.h"
#include "pubfunc.h"
#include "string.h"

#define CT_MAX_INTERRUPT_TIMES			2

#define CT_CMD00_RCV_FRAMES_NUM			7


#define	CT_GROUP_EQUIPID				210
#define CT_START_EQUIP_ID				211
#define CT_START_EQUIP_ID_EXTEND			1400
#define CT_POS_SET_SIG_ID				1

#define CT_SIG_EXISTANCE				0
#define CT_SIG_INEXISTANCE				1

#define CT_COMM_NORMAL_ST				0
#define CT_COMM_ALL_INTERRUPT_ST		1
#define CT_COMM_INTERRUPT_ST			2

//Please refer to the document of CAN protocol
//Reading data
#define CT_VAL_TYPE_R_DC_VOLT			0x01
#define CT_VAL_TYPE_R_REAL_CURR			0x02
#define CT_VAL_TYPE_R_TEMPERATURE		0x03
#define CT_VAL_TYPE_R_ALARM_BITS		0x04
#define CT_VAL_TYPE_R_HIGH_SN			0x05
#define CT_VAL_TYPE_R_RUN_TIME			0x06
#define CT_VAL_TYPE_R_LOW_SN			0x21
#define CT_VAL_TYPE_R_BARCODE1			0x22
#define CT_VAL_TYPE_R_BARCODE2			0x23
#define CT_VAL_TYPE_R_BARCODE3			0x24
#define CT_VAL_TYPE_R_BARCODE4			0x25

#define CG_VAL_TYPE_R_NEWFEATURE		0x1F

#define CT_VAL_TYPE_W_ESTOP_ENB			0x16
#define CT_VAL_TYPE_W_CURR_LMT			0x08
#define CT_VAL_TYPE_W_HIGH_VOLT			0x09
#define CT_VAL_TYPE_W_DC_VOLT			0x07
#define CT_VAL_TYPE_W_DEFAULT_LIMT		0x0b
#define CT_VAL_TYPE_W_DEFAULT_VOLT		0x0a
#define CT_VAL_TYPE_W_DEFAULT_HVSD		0x0c
#define CT_VAL_TYPE_W_RUN_TIME			0xA4


#define CT_VAL_TYPE_NO_TRIM_VOLT		0x80	//used while MsgType = 0x00

#define CT_G_SIG_DEFAULT_LIMIT_ID		22
#define CT_G_SIG_DEFAULT_VOLT_ID		23
#define CT_G_SIG_DEFAULT_VOLT_ID_24V		29

#define CT_G_SIG_AVERAGE_VOLT_ID		4
#define CT_G_SIG_AVERAGE_CURR_ID		3
#define CT_G_SIG_DEFAULT_HVSD_ID		27
#define CT_G_SIG_DEFAULT_HVSD_ID_24V		32

#define CT_G_SIG_CTRL_HVSD_ID_24V		16
#define CT_G_SIG_CTRL_HVSD_ID			12
#define CT_G_SIG_CTRL_LIMIT_ID			1
#define CT_G_SIG_CTRL_OUTPUT_VLT_ID		2
#define CG_G_SIG_SET_WALKIN_ENB			8

#define	CT_DC_ON				0
#define	CT_DC_OFF				1
#define	CT_RESET_NORMAL			0
#define	CT_RESET_ENB			1
#define	CT_WALKIN_NORMAL		0
#define	CT_WALKIN_ENB			1
#define	CT_FAN_AUTO				0
#define	CT_FAN_FULL_SPEED		1
#define	CT_LED_NORMAL			0
#define	CT_LED_BLINK			1

#define CT_RATED_CURR			27.0			//Conv's rated current is fixed 27A.

#define	CONVERT_POSITION_INVAILID	(-1)


#define DC_TYPE_RETURN_SINGLE			1
#define DC_TYPE_RETURN_SMALL			2
#define DC_TYPE_RETURN_DOUBLE			0


enum CONVERT_SAMP_CHANNEL
{
	CT_CH_CONVERT_NUM = 4101,	//Total Converter Number
	CT_CH_CONVERT_EXIST = 4102,
	CT_CH_CONVERT_COMM_STATUS = 4103,
	CT_CH_G_INPUT_RATED_VOLTAGE,
	CT_CH_G_OUTPUT_RATED_VOLTAGE,
	CT_CH_G_OUTPUT_RATED_CURRENT,
	CT_CH_G_CONVERTER_TYPE,

	

	CT_CH_DC_VOLT = 4151,		//Output Voltage
	CT_CH_ACTUAL_CURR,			//Actual Current
	CT_CH_TEMPERATURE,			//Temperature
	CT_CH_HIGH_SERIAL_NO,		//High Serial No.
	CT_CH_LOW_SERIAL_NO,		//Low Serial No. 
	CT_CH_RUN_TIME,				//Total Run Time in hour
	CT_CH_ID_OVERLAP_ST,		//Converter ID overlap
	CT_CH_IDENTIFYING_ST,		//Converter Identifying Status
	CT_CH_FAN_FULL_SPEED_ST,	//Fan Full Speed status
	CT_CH_EEPROM_FAULT_ST,		//EEprom Fault Status
	CT_CH_THERMAL_SHUTDOWN_ST,	//Thermal Shutdown Status
	CT_CH_INPUT_LOW_VOLT_ST,	//Input Low Voltage Status
	CT_CH_HI_AMB_TEMP_ST,		//High Ambient Temperature Status
	CT_CH_WALKIN_ENB_ST,		//WALK-In function Enabled Status
	CT_CH_ON_OFF_ST,			//On/off status
	CT_CH_STOP_ST,				//Stop Status
	CT_CH_POWER_LMT_TEMP_ST,	//Power limitation by temperature Status
	CT_CH_OVER_VOLT_ST,			//Over Voltage Status
	CT_CH_FAN_FAIL_ST,			//Fan failure Status
	CT_CH_CONVERT_FAIL_ST,		//Converter Failure Status
	CT_CH_BARCODE1,				//Bar Code 1
	CT_CH_BARCODE2,				//Bar Code 2
	CT_CH_BARCODE3,				//Bar Code 3
	CT_CH_BARCODE4,				//Bar Code 4
	CT_CH_INTERRUPT_STATE,		//Interrupt State
	CT_CH_EXISTENCE,
	CT_CH_POSITION_NO,		//Position number
	CT_CH_INPUT_RATED_VOLTAGE,
	CT_CH_OUTPUT_RATED_VOLTAGE,
	CT_CH_OUTPUT_RATED_CURRENT,
	CT_CH_INPUT_TYPE,
	CT_CH_EFFECIEY,
	CT_CH_TYPE_NO,

};


enum CONV_EFFICIENCY_NO_DEF
{
	CONV_EFFICIENCY_LT93 = 0,
	CONV_EFFICIENCY_GT93,
	CONV_EFFICIENCY_GT94,
	CONV_EFFICIENCY_GT95,
	CONV_EFFICIENCY_GT96,
	CONV_EFFICIENCY_GT97,
	CONV_EFFICIENCY_GT98,
	CONV_EFFICIENCY_GT99,

	CONV_EFFICIENCY_MAX_NUM,
};


struct	tagCONVERT_POS_ADDR
{
	DWORD			dwSerialNo;//��Barcode1����
	//DWORD			dwHiSN; 
	int			iPositionNo;	
	BOOL			bIsNewInserted; //�Ƿ��²����
	int			iSeqNo; //���ź��λ��
};
typedef struct tagCONVERT_POS_ADDR CONVERT_POS_ADDR;

struct	tagCONV_TYPE_INFO
{
	UINT		uiTypeNo;
	float		fRatedVolt;
	float		fRatedCurr;
	int		iDcDcType;
	float		fAcRatedVolt;
	float		fPower;
	int		iEfficiencyNo;
};
typedef struct tagCONV_TYPE_INFO CONV_TYPE_INFO;

void CT_Reconfig(void);
void CT_TriggerAllocation(void);
void CT_InitPosition(void);
void CT_InitRoughValue(void);
void CT_SendCtlCmd(int iChannelNo, float fParam);
void CT_Sample(void);
void CT_StuffChannel(ENUMSIGNALPROC EnumProc,	//Callback function for stuffing channels
					 LPVOID lpvoid);			//Parameter of the callback function

BOOL	CtCheckConvertExist();
VOID Convert_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI);
void Ct_BroadcastUnify(BOOL bForceBroadCast);

#endif

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : sim_win32api.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-09 16:18
 *  VERSION  : V1.00
 *  PURPOSE  : forge the apis used by equipment samplers whose codes are 
 *             developed under Windows. To simulate win32 calls.
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include <unistd.h>
#include <sys/types.h>
#include <stdarg.h>

#include "local.h"

#define MAX_DEBUG_FILE_SIZE		(64*1024)	// 64K


static FILE *OpenDebugFile(char *pszFileName, long nMaxAllowedSize)
{
	FILE	*fp;
	char	szFullName[MAX_FILE_PATH];
    long    nLogSize;

    fp = fopen( 
		MakeFullFileName(szFullName,"/var/", NULL, pszFileName),
		"a+b");

    if( fp == NULL )
    {
        fprintf( stderr, "Open log file %s fail.\n", szFullName );
	    return NULL;
    }

	fseek(fp, 0l, SEEK_END);
    nLogSize = ftell(fp); // at the end of file, we use a+ open
    
	if(nLogSize >= nMaxAllowedSize )
    {
		ftruncate(fileno(fp), 0L);	// cut to 0
		rewind(fp);
	}

	return fp;
}

//дASCII���¼�ļ�
BOOL DebugWriteAsc(char *szFileName, LPCTSTR pszFormat, ...)
{
	va_list     arglist;
    char        szBuf[512];
    FILE        *fp;
	int			nLen;

    //ClearWDT(); /* clear watch dog */
	fp = OpenDebugFile(szFileName, MAX_DEBUG_FILE_SIZE);
    if( fp == NULL )
    {
        return FALSE;
    }

	va_start(arglist, pszFormat);
    nLen = vsnprintf(szBuf, sizeof(szBuf), pszFormat, arglist);
	va_end(arglist);

	if(szBuf[nLen-1] == '\n')	// added by maofuhua, 2004-11-04
	{
		szBuf[nLen-1] = 0; 
	}

    fprintf(fp, "%s\n", szBuf);

    fflush( fp );
    fclose( fp );

	return TRUE;
}



//д�����Ƽ�¼�ļ�
BOOL DebugWriteHex(char *szFileName, char *pBuf, int nLen)
{
    FILE	*fp;
	int		k;

	fp = OpenDebugFile(szFileName, MAX_DEBUG_FILE_SIZE);
    if( fp == NULL )
    {
        return FALSE;
    }

    for( k=0; k<nLen; k++ )
    {
        fprintf( fp, "%02X ", (unsigned char)pBuf[k] );
    }

	fprintf(fp, "\n");

	fflush( fp );
    fclose( fp );

	return TRUE;
}

void CheckSum( BYTE *Frame );

//ȡ��ַ��Ϣ
BOOL GetAddr(HANDLE hComm, int CID1, int *ADDR)
{
    BYTE Frame[512]={0};    // �������ݻ�����
    BYTE szSendStr[20]={0};
    
     sprintf( (char *)szSendStr, 
		 "~%02X%02X%02X%02X%04X",
		 0x20,	// version always is 2.0=0x20
		 0x00,	// ignore addr
		 CID1, 0x50, 0 );

    CheckSum( szSendStr );

	if( !SendString(hComm, szSendStr, 18) )     // �������
	{
		return FALSE;
	}

    if (ReceiveString( hComm, Frame, 18 ) < 10)
	{
        return FALSE;
	}

    sscanf( (char*)Frame+3, "%2X", ADDR);
    
    return TRUE;
}



#ifdef _SAMPLER_REPLACE_SSCANF

/*=====================================================================*
 * Function name: HexToLong
 * Description  : 
 * Arguments    : unsigned char *hex	: 
 *                int nHex	: 
 * Return type  : unsigned long 
 *
 * Create       : Mao Fuhua    2000-11-16 15:40:09
 * Comment(s)   : 
 *--------------------------------------------------------------------*/
unsigned long HexToLong( unsigned char *hex, int nHex )
{
    unsigned long  w       = 0;
    unsigned char *pEndHex = hex + nHex;

#ifdef _DEBUG
    if( (nHex < 1) || (nHex > 8) )
    {
        printf( "\n\r[HexToLong] -- invalid nHex value %d, must be in 1-8.",
            nHex );
        return (unsigned long)-1;
    }
#endif 	/*_DEBUG	*/

    while( hex < pEndHex )
    {
        w <<= 4;

        if( (*hex >= '0') && (*hex <= '9') )
            w += *hex - '0';
        else if( (*hex >= 'A') && (*hex <= 'F') )
            w += *hex - 55; // *hex - 'A' + 10;
        else if( (*hex >= 'a') && (*hex <= 'f') )
            w += *hex - 87; // *hex - 'a' + 10;
        else
            return 0;

        hex ++;
    }

    return w;
}

#endif //#ifdef _SAMPLER_REPLACE_SSCANF

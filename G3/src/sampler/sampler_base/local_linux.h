/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : sim_win32api.c
 *  CREATOR  : Mao Fuhua(Frank)         DATE: 2004-11-09 16:18
 *  VERSION  : V1.00
 *  PURPOSE  : forge the apis used by equipment samplers whose codes are 
 *             developed under Windows. To simulate win32 calls.
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef __LOCAL_LINUX_H
#define __LOCAL_LINUX_H

#include <stdio.h>

#include "basetypes.h"
#include "err_code.h"
#include "halcomm.h"
#include "pubfunc.h"
#include "string.h"

//#ifdef ASSERT
//#undef ASSERT
//#define ASSERT		SAMPLER_ASSERT
//#endif

#ifdef __cplusplus
extern "C" {
#endif

//types for simulating win32
typedef const char	*LPCTSTR;
typedef void		*LPOVERLAPPED;

#define DLLExport 
typedef BOOL ( CALLBACK *ENUMSIGNALPROC )( int, float, LPVOID );


DLLExport char* DLLInfo(void);
DLLExport BOOL Query(HANDLE hComm, int nUnitNo, ENUMSIGNALPROC EnumProc,
                     LPVOID lpvoid);
DLLExport BOOL Read(
                    HANDLE hComm,    // ͨѶ�ھ��
                    int nUnitNo,     // �ɼ�����Ԫ��ַ
                    void* pData);    // �ϱ����ݻ�����ָ��
DLLExport BOOL Control(
                       HANDLE hComm,    // ͨѶ�ھ��
                       int nUnitNo,     // �ɼ�����Ԫ��ַ
                       char *pCmdStr);  // �������
DLLExport BOOL Write(
                       HANDLE hComm,    // ͨѶ�ھ��
                       int nUnitNo,     // �ɼ�����Ԫ��ַ
                       char *pCmdStr);  // �������

// the functions are defined in win32_stub.c
#define PURGE_TXCLEAR		(0x01)
#define PURGE_RXCLEAR		(0x02)

#define PurgeComm(hFile, dwFlags)									\
	((((((dwFlags) & PURGE_TXCLEAR) == PURGE_TXCLEAR)				\
		? CommControl( (hFile), COMM_PURGE_TXCLEAR, (void*)1, 0) : (void)0),\
	((((dwFlags) & PURGE_RXCLEAR) == PURGE_RXCLEAR)					\
		? CommControl( (hFile), COMM_PURGE_RXCLEAR, (void*)1, 0) : (void)0)))

#define ReadFile(hFile, lpBuffer, nNumberOfBytesToRead,			\
			  lpNumberOfBytesRead, lpOverlapped)				\
	(((*(lpNumberOfBytesRead) = CommRead((hFile), (lpBuffer), (nNumberOfBytesToRead))), \
	 ((CommGetLastError(hFile) == ERR_COMM_OK) || (CommGetLastError(hFile) == ERR_COMM_TIMEOUT))))

#define WriteFile(hFile, lpBuffer, nNumberOfBytesToWrite,		\
			   lpNumberOfBytesWritten, lpOverlapped)			\
	(((*(lpNumberOfBytesWritten) = CommWrite((hFile), (lpBuffer), (nNumberOfBytesToWrite))), \
	 ((CommGetLastError(hFile) == ERR_COMM_OK) || (CommGetLastError(hFile) == ERR_COMM_TIMEOUT))))


#define MessageBox(hwnd, msg, cap, btn)	TRACEX("%s: %s", (cap), (msg))


//дASCII���¼�ļ� 
BOOL DebugWriteAsc(char *szFileName, LPCTSTR pszFormat, ...);
#define WriteAsc	(!bTestFlag) ? 0 : DebugWriteAsc

//д�����Ƽ�¼�ļ�
BOOL DebugWriteHex(char *szFilename, char *pBuf, int nLen);
#define WriteHex	(!bTestFlag) ? 0 : DebugWriteHex



//*****************************************************************
// �������ƣ�ReceiveString
// �������������豸 hComm �����ַ��� sRecStr �����ַ�
// ���������hComm - ͨѶ�ھ��,sRecStr - �����ַ���ָ��,
//           nStrLen - ��Ҫ���յ��ַ�����
// ���������ʵ�ʽ��յ��ַ�����
// ��    �أ��ɹ��ɼ����ַ�����
// ������
//*****************************************************************
int ReceiveString(
                  HANDLE hComm,     // ͨѶ�ھ��
                  BYTE* sRecStr,    // �����ַ���ָ��
                  int nStrLen       // ��Ҫ���յ��ַ�����
                  );

//*****************************************************************
// �������ƣ�SendString
// ������������ͨѶ�ھ��hComm �����ַ���sSendStr��ǰ nStrLen���ַ�
// ���������hComm - ͨѶ�ھ��,sSendStr - ��Ҫ���͵��ַ���,
//           nStrLen - ��Ҫ���͵��ַ�����
// ���������
// ��    �أ�TRUE���ɹ���FALSE��ʧ�ܡ�    
// ������
//*****************************************************************
BOOL SendString(
                HANDLE hComm,       // ͨѶ�ھ��
                BYTE* sSendStr,     // Ҫ���͵��ַ���ָ��
                int nStrLen );       // Ҫ�����ַ�����



#define _SAMPLER_REPLACE_SSCANF	1

// replace the sscanf to improve the converting speed. maofuhua, 2004-11-28
#ifdef _SAMPLER_REPLACE_SSCANF

// the nHex must be 1~4
unsigned long HexToLong( unsigned char *hex, int nHex );

#ifdef _DEBUG
    //#define _DEBUG_SSCANF
#endif

#ifdef _DEBUG_SSCANF
    #define sscanf( sBuf, fmt, pResult )    \
        do {    \
            if( (strlen(fmt) == 4) && ((fmt)[2]>='1') && ((fmt)[2]<='8'))     \
            {   \
                (*(pResult)) = HexToLong((unsigned char *)(sBuf), (fmt)[2]-'0');  \
            }   \
            else    \
            {   \
                printf( "[hex_sscanf] -- given format error,"   \
                    "only support %%02x and %%04x" );   \
                *(pResult) = 0; \
            }   \
        } while( 0 )
#else
    #define sscanf( sBuf, fmt, pResult)     \
        ( (*(pResult)) = HexToLong((unsigned char *)(sBuf), (fmt)[2]-'0') )
#endif 	/*_DEBUG	*/

#endif // _SAMPLER_REPLACE_SSCANF


#ifdef __cplusplus
}
#endif

#endif

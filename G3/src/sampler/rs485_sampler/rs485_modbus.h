/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : NGMC-HF(Next Generation Controller - High Feature)
 *
 *  FILENAME : rs485_modbus.h
 *  CREATOR  : Frank Cao                DATE: 2009-06-16 13:24
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#ifndef __RS485_SAMPLER_MODBUS_H
#define __RS485_SAMPLER_MODBUS_H

#include <stdio.h>

#include "basetypes.h"
#include "err_code.h"
#include "pubfunc.h"
#include "string.h"

#include "rs485_main.h"


enum	tagMDB_ERROR_CODE
{
	MDB_ERROR_OK = ERR_OK,
	MDB_ERROR_ADDR,
	MDB_ERROR_FUN_CODE,
	MDB_ERROR_RESPOND_REG_ADDR,
	MDB_ERROR_RESPOND_DATA_LEN,
	MDB_ERROR_RESPOND_DATA_VALUE,
	MDB_ERROR_CRC,

	MDB_ERROR_FAIL,
	MDB_ERROR_NO_MEMORY,
	MDB_ERROR_OTHER,

	MDB_ERROR_MAX,
};


#define	MDB_EQUIP_EXIST			0
#define	MDB_EQUIP_NOT_EXIST		1

#define MDB_COMM_OK			0
#define MDB_COMM_FAILURE		1

#define MDB_NORMAL			0
#define MDB_ALARM			1

#define MDB_COMM_FAIL_TIMES		3

#define	RS485_MODBUS_STARTADDR		35000
#define RS485_MODBUS_GROUP_SPACE	50	    //���ź�ռ50���ź�
#define RS485_MODBUS_ACMETER_SPACE	100	    //AC METER �ź�ռ100���ź�

#define MDB_SAM_INVALID_VALUE		(-9999)

//���ź�
enum	tagROUGH_DATA_IDX_MDB_GROUP
{
	MDB_GROUP_EXIST = 0,
	MDB_GROUP_ACMETER_NUM,
	
	MDB_GROUP_ROUGH_DATA_MAX,
};
extern RS485_VALUE g_MdbGroupRoughData[MDB_GROUP_ROUGH_DATA_MAX];

//���źŶ�Ӧ �ɼ��� ͨ��
enum	tagSAMPLER_CH_MDB_GROUP
{
	S_CH_MDB_GROUP_EXIST = RS485_MODBUS_STARTADDR+2,
	S_CH_MDB_GROUP_ACMETER_NUM,
};


enum	tagMDB_SIGNAL_TYPE
{
	MDB_SIGNAL_TYPE_UINT16 = 0,  //�źų���Ϊ 2bytes
	MDB_SIGNAL_TYPE_INT16,	    //�źų���Ϊ 2bytes
	MDB_SIGNAL_TYPE_UINT32,	    //�źų���Ϊ 4bytes
	MDB_SIGNAL_TYPE_INT32,	    //�źų���Ϊ 4bytes
};


//�ӼĴ�����RoughData��ӳ���
typedef struct tagRegToRoughMap
{
	BYTE byAddr;          //�豸��ַ
	int nStartRegAddr;    //�Ĵ�����ʼ��ַ
	BYTE bySignalType;    //�ź����Ͷ���,�μ�enum tagMDB_SIGNAL_TYPE
	int nSuccessiveNum;   //������ȡ�źŸ���	
	RS485_VALUE *pRoughDataBuf; //�ɼ������źŴ��Ŀ���ַָ�� RoughData
}REG_TO_ROUGH_MAP;


// ��ͨ��
enum	tagMDB_SCALE_TYPE
{
	MDB_SCALE_TYPE_NORMAL = 0,			// 1:1
	MDB_SCALE_TYPE_EN10,					  // 1:10
	MDB_SCALE_TYPE_EN100,					  // 1:100
	MDB_SCALE_TYPE_EN1000,				  // 1:1000	
	MDB_SCALE_TYPE_PHASE_SEQ,			  // �μ�����enum tagACMETER_PHASE_SEQ

	MDB_SCALE_TYPE_MAX,
};
typedef struct tagMdbRegValueToRough
{
	int		iRegValueIdx;
	int		iRoughDataIdx;

	int		nScaleType;			//refer to tagMDB_SCALE_TYPE
	int		nSigValueType;
}MDB_REG_VALUE_TO_ROUGH;

// �ϱ��ɼ���
typedef	struct tagMdbChannelRoughIndex 
{
	int		iChnNo;
	int		iRoughIdx;

	int		nScaleType;			//refer to tagMDB_SCALE_TYPE

}MDB_CHN_ROUGH_INDEX;

//ACMETER�豸����
#define ACMETER_NUM_MAX				5

//ACMETER��ַ��Χ
#define ACMETER_ADDR_START			1
#define ACMETER_ADDR_END			(ACMETER_ADDR_START + ACMETER_NUM_MAX)

//ACMETER�Ĵ������� (��ΪAC meterһ�����ֻ�ܻظ�11words�����ݣ�������Ҫ�ֶ�ζ�ȡ)
enum	tagACMETER_REGISTER
{
	ACMETER_REG_VOLT_L1_N = 0,
	ACMETER_REG_VOLT_L3_L1 = 0x0A,
	
	ACMETER_REG_W_L2 = 0x14,
	ACMETER_REG_VAR_L1 = 0x1E,
	
	ACMETER_REG_WATT_ACC = 0x28,

	ACMETER_REG_PF_L1 = 0x32,	

	ACMETER_REG_DMD_W_ACC_MAX = 0x38,
	ACMETER_REG_KWH_P_PAR = 0x42,
	ACMETER_REG_KWH_P_T1 = 0x4C,
	ACMETER_REG_KVARH_P_T2 = 0x56,
	ACMETER_REG_HOUR = 0x60,


	ACMETER_REG_VERSION_CODE = 0x0302,	
	ACMETER_REG_REVISION_CODE = 0x0303,

	ACMETER_REG_MAX,
};

//AC METER �ڲ��ź�����
enum	tagROUGH_DATA_IDX_ACMETER
{
	ACMETER_EXISTENCE = 0,
	ACMETER_ADDRESS,
	ACMETER_COMM_FAIL_STATUS,
	ACMETER_COMM_BREAK_TIMES,

	VOLT_L1_N_ACMETER,
	VOLT_L2_N_ACMETER,
	VOLT_L3_N_ACMETER,
	
	VOLT_L1_L2_ACMETER,
	VOLT_L2_L3_ACMETER,
	VOLT_L3_L1_ACMETER,
	
	AMP_L1_ACMETER,
	AMP_L2_ACMETER,
	AMP_L3_ACMETER,
	
	WATT_L1_ACMETER,
	WATT_L2_ACMETER,
	WATT_L3_ACMETER,
	
	VA_L1_ACMETER,
	VA_L2_ACMETER,
	VA_L3_ACMETER,
	
	VAR_L1_ACMETER,
	VAR_L2_ACMETER,
	VAR_L3_ACMETER,
	
	VOLT_L_N_ACC_ACMETER,
	VOLT_L_L_ACC_ACMETER,
	WATT_ACC_ACMETER,
	VA_ACC_ACMETER,
	VAR_ACC_ACMETER,
	
	DMD_W_ACC_ACMETER,
	DMD_VA_ACC_ACMETER,
	
	PF_L1_ACMETER,
	PF_L2_ACMETER,
	PF_L3_ACMETER,	
	PF_ACC_ACMETER,
	PHASE_SEQ_ACMETER,
	HZ_ACMETER,

	DMD_WATT_ACC_MAX_ACMETER,
	DMD_VA_ACC_MAX_ACMETER,
	DMD_AMP_MAX_ACMETER,
	
	KWH_P_TOT_ACMETER,
	KVARH_P_TOT_ACMETER,
	KWH_P_PAR_ACMETER,
	KVARH_P_PAR_ACMETER,
	KWH_L1_ACMETER,
	KWH_L2_ACMETER,
	KWH_L3_ACMETER,
	KWH_T1_ACMETER,
	KWH_T2_ACMETER,
	KWH_T3_ACMETER,
	KWH_T4_ACMETER,
	KVARH_T1_ACMETER,
	KVARH_T2_ACMETER,
	KVARH_T3_ACMETER,
	KVARH_T4_ACMETER,
	
	KWH_N_TOT_ACMETER,
	KVARH_N_TOT_ACMETER,

	HOUR_ACMETER,
	COUNTER1_ACMETER,
	COUNTER2_ACMETER,
	COUNTER3_ACMETER,
  
	//Digital input
	DigitalInput_ACMETER,   //bit0~2 : Ch1~3
  
	//Current tariff
	Current_tariff_ACMETER,   //0~3 : tariff 1~4
	
	Version_code_ACMETER,	/*  Value=0: EM24DINAV93XO2X
				    Value=1: EM24DINAV93XISX
				    Value=2: EM24DINAV53DO2X
				    Value=3: EM24DINAV53DISX
				    Value=4: EM24DINAV93XR2X
				    Value=5: EM24DINAV53DR2X
				*/
	Revision_code_ACMETER,
	
	Front_selector_status_ACMETER,  /*Value=3: keypad locked
					  Value=2: keypad unlocked
					  Value=1: keypad unlocked
					  Value=0: keypad unlocked
					*/
	
	Controls_identification_code_ACMETER, /*Value=45: EM24-DIN AV9 input product code
						Value=46: EM24-DIN AV0 input product code
						Value=47: EM24-DIN AV5 input product code
						Value=48: EM24-DIN AV6 input product code
					     */
	
	ACMETER_ROUGH_DATA_MAX,
};

//AC METER �ɼ���ͨ�� ����
enum	tagSAMPLER_CH_ACMETER
{
	S_CH_VOLT_L1_N_ACMETER = RS485_MODBUS_STARTADDR + RS485_MODBUS_GROUP_SPACE,  //0
	S_CH_VOLT_L2_N_ACMETER,
	S_CH_VOLT_L3_N_ACMETER,	
	S_CH_VOLT_L1_L2_ACMETER,
	S_CH_VOLT_L2_L3_ACMETER,

	S_CH_VOLT_L3_L1_ACMETER,	
	S_CH_AMP_L1_ACMETER,
	S_CH_AMP_L2_ACMETER,
	S_CH_AMP_L3_ACMETER,	
	S_CH_WATT_L1_ACMETER,

	S_CH_WATT_L2_ACMETER,  //10
	S_CH_WATT_L3_ACMETER,	
	S_CH_VA_L1_ACMETER,
	S_CH_VA_L2_ACMETER,
	S_CH_VA_L3_ACMETER,
	
	S_CH_VAR_L1_ACMETER,  //15
	S_CH_VAR_L2_ACMETER,
	S_CH_VAR_L3_ACMETER,	
	S_CH_VOLT_L_N_ACC_ACMETER,
	S_CH_VOLT_L_L_ACC_ACMETER,

	S_CH_WATT_ACC_ACMETER,  //20
	S_CH_VA_ACC_ACMETER,
	S_CH_VAR_ACC_ACMETER,
	S_CH_DMD_W_ACC_ACMETER,
	S_CH_DMD_VA_ACC_ACMETER,
	
	S_CH_PF_L1_ACMETER,  //25
	S_CH_PF_L2_ACMETER,
	S_CH_PF_L3_ACMETER,
	S_CH_PF_ACC_ACMETER,
	S_CH_PHASE_SEQ_ACMETER,
	S_CH_HZ_ACMETER,  //30


	S_CH_DMD_W_ACC_MAX_ACMETER,  //31
	S_CH_DMD_VA_ACC_MAX_ACMETER,
	S_CH_DMD_A_MAX_ACMETER,
	S_CH_KWH_P_TOT_ACMETER,
	S_CH_KVARH_P_TOT_ACMETER,  //35

	S_CH_KWH_P_PAR_ACMETER,  //36
	S_CH_KVARH_P_PAR_ACMETER,
	S_CH_KWH_P_L1_ACMETER,
	S_CH_KWH_P_L2_ACMETER,
	S_CH_KWH_P_L3_ACMETER,  //40

	S_CH_KWH_P_T1_ACMETER,  //41
	S_CH_KWH_P_T2_ACMETER,
	S_CH_KWH_P_T3_ACMETER,
	S_CH_KWH_P_T4_ACMETER,
	S_CH_KVARH_P_T1_ACMETER,  //45

	S_CH_KVARH_P_T2_ACMETER,  //46
	S_CH_KVARH_P_T3_ACMETER,
	S_CH_KVARH_P_T4_ACMETER,
	S_CH_KWH_N_TOT_ACMETER,  
	S_CH_KVARH_N_TOT_ACMETER,  //50

	S_CH_HOUR_ACMETER,	  //51  
	S_CH_COUNTER1_ACMETER,    
	S_CH_COUNTER2_ACMETER,  
	S_CH_COUNTER3_ACMETER,  //54

	
	
	S_CH_ACMETER_COMM_FAIL_STATUS = S_CH_VOLT_L1_N_ACMETER + RS485_MODBUS_ACMETER_SPACE - 2,  //98
	S_CH_ACMETER_EXISTENCE,	 //99

};

#define MDB_START_CHN_ACMETER	    S_CH_VOLT_L1_N_ACMETER   //ACMETER ��ʼͨ���� 


//Phase sequence(AC Meter�ϴ�ԭʼֵ����): �C1 correspond to L1-L3-L2 sequence, value 0 correspond to L1-L2-L3 sequence
enum	tagACMETER_PHASE_SEQ
{
    PH_SEQ_L1_L3_L2 = -1,
    PH_SEQ_L1_L2_L3,
};
//Phase sequence:  ��䵽�ɼ�����ʱ��Ӧ����ֵ
enum	tagACMETER_PHASE_SEQ_STUFF_VALUE
{
    PH_SEQ_L1_L2_L3_STUFFVALUE = 0,
    PH_SEQ_L1_L3_L2_STUFFVALUE,    
};

//ACMETER ����ͨ��
//enum	tagACMETER_CONTROL_CHANNEL
//{	
//};

typedef	struct tagMdbCtrlChnToReg 
{
	int		iCtrlChnNo;
	int		iRegNo;
	int		nScaleType;			//refer to tagMDB_SCALE_TYPE
}MDB_CTRL_CHN_TO_REG;


// ����ͬ��
typedef	struct tagMdbParamUnifyItem 
{
	int		nEquipId;
	int		nSigType;
	int		nSigId;

	int		nSigValueType;

	int		nRoughIdx;
	int		nCtrlChn;

}MDB_PARAM_UNIFY_ITEM;

#define	ACMETER_CHANNEL_SPACE	RS485_MODBUS_ACMETER_SPACE    //ÿ��ACMETER����Ĳɼ�ͨ���ռ� 100

//����
#define MDB_CH_END_FLAG		-1
#define	EQUIP_ID_ACMETER1	0
#define MDB_SAMPLER_TASK	"Modbus Sample"
#define TRACE_FILE_LINE_FUN(info)			TRACE("[%s](L%d)%s:%s", __FILE__, __LINE__, __FUNCTION__, (info));
#define TRACE_FILE_LINE_FUN_ENTER(info)		TRACE("[%s](L%d)%s:%s\n", __FILE__, __LINE__, __FUNCTION__, (info));
#define TRACE_ERROR_INT(info, value)		TRACE("[%s](L%d)%s:%s %d\n", __FILE__, __LINE__, __FUNCTION__, (info), (value));
#define TRACE_ERROR_FLOAT(info, value)		TRACE("[%s](L%d)%s:%s %f\n", __FILE__, __LINE__, __FUNCTION__, (info), (value));

#define LOGOUT_NO_MEMORY(p) \
    if(!(p)) {AppLogOut(MDB_SAMPLER_TASK, APP_LOG_ERROR, \
    "[%s]-[%s]-[%d]: There is no enough memory!\n", \
    __FILE__, __FUNCTION__, __LINE__); \
    return MDB_ERROR_NO_MEMORY;}

#define DELETE_ITEM(p) if((p)) { DELETE(p);  (p) = NULL; }

#define MDB_EPSILON		0.001
#define MDB_FLOAT_NOT_EQUAL(f1, f2)	(ABS(((f1)-(f2)))>MDB_EPSILON)

//MODBUS������
#define MDB_FUN_CODE_GET_MANY_DATA		0x03
#define MDB_FUN_CODE_SET_ONE_DATA		0x06

void MDB_Init(void* pDevice);
void MDB_Exit(void* pDevice);
void MDB_Reconfig(void* pDevice);
void MDB_Sample(void* pDevice);
void MDB_StuffChn(void* pDevice, ENUMSIGNALPROC EnumProc, LPVOID lpvoid);
void MDB_ParamUnify(void* pDevice);
void MDB_SendCtlCmd(void* pDevice, int iChn, float fParam, char* strParam);

int ACMETER_Init(void* pDevice);
void ACMETER_Reconfig(void* pDevice);
void ACMETER_SendCtlCmd(void* pDevice, int iChn, float fParam, char* strParam);
void ACMETER_ParamUnify(void *pDevice);
void ACMETER_Sample(void* pDevice);
void ACMETER_StuffChn(void* pDevice, ENUMSIGNALPROC EnumProc, LPVOID lpvoid);
void ACMETER_GetProdctInfo(HANDLE hComm, int nUnitNo, void *pPI);

/////////////  define Modbus transfer mode ///////////
#define MDBEXT_TRANS_MODE_ASCII				"ASCII"
#define MDBEXT_TRANS_MODE_RTU				"RTU"

/////////////  define Modbus function ID ///////////
#define MDBEXT_FUN_CODE_01								0x01//Read Coils Status(Digital, bit).
#define MDBEXT_FUN_CODE_02								0x02//Read Input Status(Digital, bit)
#define MDBEXT_FUN_CODE_03								0x03//Read Holding Registers(Integer,Status,Char,Float)
#define MDBEXT_FUN_CODE_04								0x04//Read Input Registers(Integer,Status,Char,Float).
#define MDBEXT_FUN_CODE_05								0x05//Force Single Coil (Digital). Forces a single coil to either ON or OFF.
#define MDBEXT_FUN_CODE_06								0x06//Preset Single Register(Integer,Status,Char,Float).
//Force Multiple Coils(Digital). Forces each coil in a sequence of coils to either ON or OFF
#define MDBEXT_FUN_CODE_15								0x0F
//Preset Multiple Registers(Integer,Status,Char,Float). Presets values into a sequence of holding registers.
#define MDBEXT_FUN_CODE_16								0x10
#define MDBEXT_FUN_CODE_17								0x11//Report Slave ID

/////////////  define Modbus data type ///////////
#define MDBEXT_DATATYPE_ID_10							10// one byte
#define MDBEXT_DATATYPE_ID_20							20//two bytes, signed int, big-endian format with the byte order 2,1
#define MDBEXT_DATATYPE_ID_21							21//two bytes, signed int, little-endian format with the byte order 1,2
#define MDBEXT_DATATYPE_ID_22							22//two bytes, unsigned int, big-endian format with the byte order 2,1
#define MDBEXT_DATATYPE_ID_23							23//two bytes, unsigned int, little-endian format with the byte order 1,2

/////////////  define Modbus special chars in "ASCII" mode  ///////////
#define MDBEXT_ASCII_MODE_START_CHR						0x3A//:
#define MDBEXT_ASCII_MODE_STOP_CHR_HI					0x0D//CR
#define MDBEXT_ASCII_MODE_STOP_CHR_LO					0x0A//LF

/////////////  define sample method in sample table  ///////////
#define MDBEXT_SAMPLE_METHOD_ONLY_SAMPLE				0
#define MDBEXT_SAMPLE_METHOD_ONLY_RECONFIG				1

#define MDBEXT_RECONFIG_PARSE_STR						0
#define MDBEXT_RECONFIG_PARSE_INT						1
#define MDBEXT_RECONFIG_PARSE_INT_BYTE					2

#define MDBEXT_RECONFIG_SNNUM_STR						100					
#define MDBEXT_RECONFIG_SWVER_STR						101
#define MDBEXT_RECONFIG_HWVER_STR						102
#define MDBEXT_RECONFIG_PROD_ID_STR						103
#define MDBEXT_RECONFIG_PARSE_SNNUM_STR					200
#define MDBEXT_RECONFIG_PARSE_SWVER_STR					201
#define MDBEXT_RECONFIG_PARSE_HWVER_STR					202
#define MDBEXT_RECONFIG_PARSE_PROD_ID_STR				203
#define MDBEXT_RECONFIG_SNNUM							300
#define MDBEXT_RECONFIG_HWVER							301


#define MDBEXT_SAMPLE_METHOD_IS_SAMPLE(iArgMethod)		(iArgMethod == MDBEXT_SAMPLE_METHOD_ONLY_SAMPLE)
#define MDBEXT_SAMPLE_METHOD_IS_RECONFIG(iArgMethod)	(iArgMethod == MDBEXT_SAMPLE_METHOD_ONLY_RECONFIG)

/////////////  define Channel Value Type  ///////////
#define MDBEXT_CHAN_DATA_TYPE_LONG						VAR_LONG
#define MDBEXT_CHAN_DATA_TYPE_FLOAT						VAR_FLOAT
#define MDBEXT_CHAN_DATA_TYPE_ULONG						VAR_UNSIGNED_LONG
#define MDBEXT_CHAN_DATA_TYPE_TIME						VAR_DATE_TIME
#define MDBEXT_CHAN_DATA_TYPE_ENUM						VAR_ENUM

/////////////  define stuff method in stuff table  ///////////
#define MDBEXT_STUFF_METHOD_DATA						0
#define MDBEXT_STUFF_METHOD_BIT							1
#define MDBEXT_GROUP_STUFF_METHOD_START					100
#define MDBEXT_GROUP_STUFF_METHOD_AVERAGE				MDBEXT_GROUP_STUFF_METHOD_START
#define MDBEXT_GROUP_STUFF_METHOD_SUMMER				(MDBEXT_GROUP_STUFF_METHOD_START + 1)
#define MDBEXT_GROUP_STUFF_METHOD_NUMBER				(MDBEXT_GROUP_STUFF_METHOD_START + 2)
#define MDBEXT_GROUP_STUFF_COMM_FAIL					(MDBEXT_GROUP_STUFF_METHOD_START + 3)
#define MDBEXT_GROUP_STUFF_EXISTENCE					(MDBEXT_GROUP_STUFF_METHOD_START + 4)

/////////////  define special rough data offset in rough data table ///////////
#define MDBEXT_RAUGH_DATA_ID_COMMFAIL					99
#define MDBEXT_RAUGH_DATA_ID_EXISTENCE					100
#define MDBEXT_RAUGH_DATA_ID_ADDRESS					200
#define MDBEXT_RAUGH_DATA_ID_COMMBREAKTIMES				201


/////////////  define config file parameters ///////////
#define MDBEXT_PATH_STR_MAX								256
#define MDBEXT_CFG_FILE									"private/modbus/ModbusRS485Sample.cfg"

#define MDBEXT_CFG_TABLE_NAME_MAX						48
#define MDBEXT_CFG_TABLE_COMMUNICATION					"[COMMUNICATION_TABLE_%d]"
#define MDBEXT_CFG_TABLE_ROUGH_DATA						"[ROUGH_DATA_TABLE_%d]"
#define MDBEXT_CFG_TABLE_SAMPLE							"[SAMPLE_TABLE_%d]"
#define MDBEXT_CFG_TABLE_STUFF_CHANNEL					"[STUFF_CHANNEL_TABLE_%d]"
#define MDBEXT_CFG_TABLE_CONTROL						"[CONTROL_TABLE_%d]"
#define MDBEXT_CFG_TABLE_UNIFY_PARAM					"[UNIFY_PARAM_TABLE_%d]"
#define MDBEXT_CFG_TABLE_EQUIPMENT						"[EQUIPMENT_TABLE_%d]"
#define MDBEXT_CFG_MODBUS_GROUP							"[MODBUS_GROUP]"
#define MDBEXT_CFG_TABLE_PRODUCT_INFO					"[PRODUCT_INFO_TABLE_%d]"

/////////////  define product info scan method in [MODBUS_GROUP] ///////////
#define MDBEXT_SCAN_METHOD_SONICK_BATTERY				0	
#define MDBEXT_SCAN_METHOD_SEPARATOR					1	// Use item separator method
#define MDBEXT_SCAN_METHOD_LENGTH						2	// Use item length method
#define MDBEXT_SCAN_METHOD_NARADA_BMS					MDBEXT_SCAN_METHOD_SEPARATOR

/////////////  define flag status in data struct MDB_INFO ///////////
#define MDBEXT_FLAG_FAIL								(-1)
#define MDBEXT_FLAG_NONE								0
#define MDBEXT_FLAG_SUCCESS								1

#define MDBEXT_MOD_NAME									"MDBEXT_MOD"


typedef struct tagMDBCfgCommTableItem
{
	int								iID;//unused
	//char							*pszDescription;
	int								iPortNum;//unused
	char							*pszPortAttr;
	int								iOpenTimeout;//Ms
	int								iOpenRetryMaxCount;
	int								iOpenRetryDelay;//Ms
	char							*pszModbusTransMode;//RTU/ASCII
	unsigned int					uiModbusValueOn;
	unsigned int					uiModbusValueOff;

}MDB_CFG_COMM_TABLE_ITEM;

typedef	struct tagMDBCfgCommTable
{
	int								iNum;
	MDB_CFG_COMM_TABLE_ITEM			*pstItem;

}MDB_CFG_COMM_TABLE;


typedef struct tagMDBCfgRoughDataTableItem
{
	int								iID;
	//char							*pszDescription;
	double							dfOffset;
	double							dfScale;
	double							dfDefaultVal;
	int								iRegAddrStart;
	int								iRegQuantity;

}MDB_CFG_ROUGH_DATA_TABLE_ITEM;

typedef	struct tagMDBCfgRoughDataTable
{
	int								iNum;
	MDB_CFG_ROUGH_DATA_TABLE_ITEM	*pstItem;

}MDB_CFG_ROUGH_DATA_TABLE;

typedef struct tagMDBCfgSampleTableItem
{
	int								iID;
	//char							*pszDescription;
	int								iSampleMethod;
	int								iModbusFunID;
	int								iModbusData;
	int								iSplitCount;
	int								iRegAddrStart;
	int								iRegQuantity;
	int								iDataType;
	int								iRoughDataIDStart;//NA is -1
	int								iResponseTimeout;
	int								iReadDelay;
	char							*pszResponseKeyStr;//NA is ""

}MDB_CFG_SAMPLE_TABLE_ITEM;

typedef	struct tagMDBCfgSampleTable
{
	int								iNum;
	MDB_CFG_SAMPLE_TABLE_ITEM		*pstItem;

}MDB_CFG_SAMPLE_TABLE;


typedef struct tagMDBCfgStuffChanTableItem
{
	int								iID;
	//char							*pszDescription;
	int								iSampleChanStart;
	int								iSampleChanQuantity;
	int								iChanDataType;
	int								iRoughDataIDStart;//NA is -1
	int								iStuffMethod;
	float							fStuffArg1;
	float							fStuffArg2;

}MDB_CFG_STUFF_CHAN_TABLE_ITEM;

typedef	struct tagMDBCfgStuffChanTable
{
	int								iNum;
	MDB_CFG_STUFF_CHAN_TABLE_ITEM	*pstItem;

}MDB_CFG_STUFF_CHAN_TABLE;


typedef struct tagMDBCfgControlTableItem
{
	int								iID;
	//char							*pszDescription;
	int								iControlChanIndex;
	int								iControlMethod;
	float							fControlArg1;
	float							fControlArg2;
	int								iModbusFunID;
	int								iRoughDataID;
	int								iDataType;

}MDB_CFG_CONTROL_TABLE_ITEM;

typedef	struct tagMDBCfgControlTable
{
	int								iNum;
	MDB_CFG_CONTROL_TABLE_ITEM		*pstItem;

}MDB_CFG_CONTROL_TABLE;


typedef struct tagMDBCfgUnifyParamTableItem
{
	int								iID;
	//char							*pszDescription;
	int								iEquipID;
	int								iSigTypeID;
	int								iSigIDStart;
	int								iSigIDQuantity;
	int								iControlTableIDStart;

}MDB_CFG_UNIFY_PARAM_TABLE_ITEM;

typedef	struct tagMDBCfgUnifyParamTable
{
	int								iNum;
	MDB_CFG_UNIFY_PARAM_TABLE_ITEM	*pstItem;

}MDB_CFG_UNIFY_PARAM_TABLE;


typedef struct tagMDBCfgEquipmentTableItem
{
	int								iID;
	//char							*pszDescription;
	int								iEquipIDStart;
	int								iEquipQuantity;
	int								iChannelStart;
	int								iChannelQuantity;//Channel Quantity of Each Equip

}MDB_CFG_EQUIPMENT_TABLE_ITEM;

typedef	struct tagMDBCfgEquipmentTable
{
	int								iNum;
	MDB_CFG_EQUIPMENT_TABLE_ITEM	*pstItem;

}MDB_CFG_EQUIPMENT_TABLE;

#define MDBEXT_MAX_PRODUCT_INFO_ITEMS		10
#define MDBEXT_PRODUCT_VERSION_MAX_LEN		16	// Maximum length of version string in PRODUCT_INFO struct
#define MDBEXT_PRODUCT_SN_MAX_LEN			32	// Maximum length of SN string in PRODUCT_INFO struct

typedef struct tagMDBCfgProdInfoTableItem
{
	int								iID;
	//char							*pszDescription;
	int								iParseMethod;
	int								iReadIdentifier;
	int								iItemSeparator;
	int								iItemNumber;
	int								iItemOffset;

}MDB_CFG_PRODUCT_INFO_TABLE_ITEM;

#define MDBEXT_PRODUCT_ID_INDEX			0
#define MDBEXT_PRODUCT_SN_INDEX			1
#define MDBEXT_PRODUCT_SWVERSION_INDEX	2
#define MDBEXT_PRODUCT_HWVERSION_INDEX	3

typedef	struct tagMDBCfgProductInfoTable
{
	int								iNum;
	MDB_CFG_PRODUCT_INFO_TABLE_ITEM	*pstItem;

}MDB_CFG_PRODUCT_INFO_TABLE;


typedef	struct tagMDBCfgTable
{
	MDB_CFG_COMM_TABLE				stCfgCommTable;
	MDB_CFG_ROUGH_DATA_TABLE		stCfgRoughDataTable;
	MDB_CFG_SAMPLE_TABLE			stCfgSampleTable;
	MDB_CFG_STUFF_CHAN_TABLE		stCfgStuffChanTable;
	MDB_CFG_CONTROL_TABLE			stCfgControlTable;//unused
	MDB_CFG_UNIFY_PARAM_TABLE		stCfgUnifyParamTable;//unused
	MDB_CFG_EQUIPMENT_TABLE			stCfgEquipTable;
	MDB_CFG_PRODUCT_INFO_TABLE		stCfgProductInfoTable;

}MDB_CFG_TABLE;


typedef struct tagMDBCfgGroupItem
{
	int								iID;
	//char							*pszDescription;
	int								iConfigTableID;
	int								iGroupEquipmentID;
	int								iGroupChannelStart;
	int								iProductInfoScanMethod;
	int								iModbusAddrStart;
	BOOL							bEnable;

}MDB_CFG_GROUP_ITEM;


typedef	struct tagMDBCfgCommon
{
	//1. the first loading items
	int								iGroupNum;
	MDB_CFG_GROUP_ITEM				*pstGroupList;

	//2. the second loading items, the data is all tables of the first loaded equipments
	MDB_CFG_TABLE					*pstTableList;

}MDB_CFG_COMMON;


typedef	struct tagMDBRunInfoEquipment
{
	//Rough Data Table relevant
	int								iRoughDataNum;
	RS485_VALUE						*punRoughData;
	//special rough data index, for operating the special data with more faster speed
	int								iOffsetCommFail;
	int								iOffsetExistence;
	int								iOffsetAddress;
	int								iOffsetCommBreakTimes;

	//Equipment Table relevant
	int								iEquipID;
	int								iChannelStart;
	int								iChannelEnd;

	//product info
	PRODUCT_INFO					stPI;

}MDB_RUNINFO_EQUIPMENT;


typedef	struct tagMDBRunInfoGroup
{
	int								iEquipNum;
	MDB_RUNINFO_EQUIPMENT			*pstEquipList;

	int								iActiveEquipNum;
	BOOL							bOnlyStuffActiveEquip;

}MDB_RUNINFO_GROUP;

typedef	struct tagMDBInfo
{
	MDB_CFG_COMMON					stCommonCfg;//be loaded from config file rs485_modbus.cfg

	//run info 
	int								iLoadCfgFlag;	//-1, failed; 0, do none; 1, sucessful;
	//loading cfg is the first step, if successful, the second step is init running info data
	int								iInitFlag;		//-1, failed; 0, do none; 1, sucessful;
	int								iGroupNum;
	MDB_RUNINFO_GROUP				*pstGroupList;//be generated when running
	BYTE							*pbySendBuf;
	BYTE							*pbyRecvBuf;

}MDB_INFO;



extern int MDBEXT_LoadCfg(void);
extern int MDBEXT_GetProdctInfo(IN HANDLE hComm, IN int iUnitNo, OUT void *pPI);









#endif //__RS485_SAMPLER_MODBUS_H

/*============================================================================*
 *         Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : Unified Data Type Definition
 *
 *  FILENAME : rs485_smio.c
 *  PURPOSE  : sampling  smac data and control smac equip
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2007-07-12      V1.0           IlockTeng         Created.    
 *    2009-04-14     
 *                                                     
 *-----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *          
 *      
 *-----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *      
 *    
 *============================================================================*/

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "rs485_smio.h"
#include "rs485_comm.h"


#define	   _RS485_SMIO_DEBUG	0


extern 	RS485_SAMPLER_DATA		g_RS485Data;
SMIO_SAMPLER_DATA				g_SmioSamplerData;
BYTE							SMIOszVer[5] = {0}; 

static BYTE SMIOAscToHex(char c);
BOOL SMIOSendControlCmd45(
			  HANDLE hComm,		  // ͨѶ�ھ��
			  INT32 nUnitNo,        // �ɼ�����Ԫ��ַ
			  INT32 nChannelNo,     // �����       //-->ͨ����(0->14ͨ��,1->15ͨ��,2->16ͨ��,) Lintao���¶����ʽ  BPY 2005.01.06
			  INT32 nInvalid,       // ����ͨ����   //������Ч��Ч(1-��Ч��0����Ч)
			  INT32 nControlType,   // ��������     //ֻ��4�����0-��͵�ƽ��1-��ߵ�ƽ��2-�����壻3-������
			  INT32 nPulseTime	  // ������     //ע�����nInvalidΪ1, nControlType������ֵ��
			  // ���nInvalidΪ0, nControlType��ֵ0,1������2��3������
			  );
INT32 FixIntDat( char* sStr, int nLen );
static void FuelEquipStatusChangeProc(void);
/*=============================================================================*
 * FUNCTION: MakeDataLen
 * PURPOSE : account the length with DataInfo check 
 * INPUT: 
 *			 int nLen
 *
 * RETURN:
 *     WORD
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     
 *			copy from smac
 *============================================================================*/
WORD MakeDataLen( int nLen )
{
	WORD wSum = (-(nLen + (nLen>>4) + (nLen>>8) )) << 12;

	return (WORD)(( nLen & 0x0FFF) | wSum );
}


/*=============================================================================*
 * FUNCTION: SmioGetEquipAddr
 * PURPOSE : get smio address by the sig channel map
 * INPUT: 
 *     
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SmioGetEquipAddr(INT32 iChannelNo, RS485_DEVICE_CLASS*  SmioDeviceClass)	
{
	//addr: 96 to 103		SMIO (8)
	//channel:
	//4000 to 4039		SMIO1
	//4040 to 4079		SMIO2
	//4080 to 4119		SMIO3
	//4120 to 4159		SMIO4
	//4160 to 4199		SMIO5
	//4200 to 4239		SMIO6
	//4240 to 4279		SMIO7
	//4280 to 4319		SMIO8
	UNUSED(SmioDeviceClass);
	INT32				iTemp = 0;
	//INT32				iSequenceNum = -1;
	//INT32				iAddr;
	//INT32				iRoughDataPosition;

	//Get SMIO Sequence Number
	for(iTemp = 0; iTemp < MAX_NUM_SMIO; iTemp++)
	{
		if((iChannelNo >= (SMIO_CH_START + iTemp * MAX_CHN_DISTANCE_SMIO)) 
			&& (iChannelNo < (SMIO_CH_START + (iTemp + 1) * MAX_CHN_DISTANCE_SMIO) - 1))
		{
			return SMIO_ADDR_NUM_96 + iTemp;				//����ǰ�ѷž;͵�������������Ļ���ַ����ȷ��
			//iSequenceNum  = iTemp;
			//break;
		}
	}

	//if (iSequenceNum == -1)
	//{
	//	return -1;//There isn't smio!
	//}

	////Get addr by Sequence Number
	//for(iAddr = SMIO_ADDR_START; iAddr <= SMIO_ADDR_END; iAddr++)
	//{
	//	iRoughDataPosition = (iAddr - SMIO_ADDR_START) * SMIO_MAX_SIGNALS_NUM;

	//	if (iSequenceNum
	//		== SmioDeviceClass->pRoughData[iRoughDataPosition + SMIO_SEQ_NUM].iValue)
	//	{
	//		return iAddr;
	//	}	 
	//}
	
	return -1;
}

static BYTE SMIOAscToHex(char c)
{
	if (c >= '0' && c <= '9')
	{
		return c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		return c - 'A' + 10;
	}

	return 0xFF;
}

/*=============================================================================*
 * FUNCTION: SMIOMergeAsc()
 * PURPOSE : assistant function, used to merge two ASCII chars
 * INPUT: 
 *			CHAR*	p
 *
 * RETURN:
 *		static BYTE : merge result
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     
 *		 copy from SMAC        DATE: 2009-05-08		14:39
 *============================================================================*/
static BYTE SMIOMergeAsc(char *p)
{
	BYTE byHi, byLo;
	byHi = SMIOAscToHex((char)(p[0]));
	byLo = SMIOAscToHex((char)(p[1]));

	//error data
	if (byHi == 0xFF || byLo == 0xFF)
	{
		return 0;
	}

	return (byHi*16 + byLo);
}


/*=============================================================================*
 * FUNCTION: SMIO_InitRoughValue
 * PURPOSE : initialization  smio sampler RoughData[][]
 * INPUT: 
 *     
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMIO_InitRoughValue(RS485_DEVICE_CLASS*  SmioDeviceClass)
{
	INT32	i;
	INT32	j;
	INT32	iRoughIndexPosition;
	SmioDeviceClass->pfnSample		= (DEVICE_CLASS_SAMPLE)SMIOSample;	
	SmioDeviceClass->pfnSendCmd		= (DEVICE_CLASS_SEND_CMD)SMIO_SendCtlCmd;
	SmioDeviceClass->pfnStuffChn		= (DEVICE_CLASS_STUFF_CHN)SMIO_StuffChannel;
	SmioDeviceClass->pRoughData		= (RS485_VALUE*)(&(g_SmioSamplerData.aRoughDataSmio));

	//MAX_NUM_SLAVE
	for(i = SMIO_ADDR_START; i <= SMIO_ADDR_END; i++)
	{
		iRoughIndexPosition = (i - SMIO_ADDR_START) * SMIO_MAX_SIGNALS_NUM;

		for(j = 0; j < SMIO_MAX_SIGNALS_NUM; j++)
		{		
			SmioDeviceClass->pRoughData[iRoughIndexPosition  + j].iValue 
				= SMIO_SAMP_INVALID_VALUE;//-9999
		}	
		SmioDeviceClass->pRoughData[iRoughIndexPosition + SMIO_INTERRUPT_TIMES].iValue
			= 0;
		SmioDeviceClass->pRoughData[iRoughIndexPosition + SMIO_WORKING_STATUS].iValue 
			= SMIO_EQUIP_NOT_EXISTENT;		
	}

	SmioDeviceClass->pRoughData[SMIO_WORKING_GROUP_STATUS].iValue 
			= SMIO_EQUIP_NOT_EXISTENT;
	SmioDeviceClass->pRoughData[SMIO_G_EXIST].iValue = SMIO_EQUIP_NOT_EXISTENT;
	SmioDeviceClass->bNeedReconfig = TRUE;
	return 0 ;
}


/*=============================================================================*
 * FUNCTION: CheckSum()
 * PURPOSE : Cumulative  result
 * INPUT:	 BYTE *Frame
 *     
 *
 * RETURN:
 *     static void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *    
 *			 copy from SMAC        DATE: 2009-05-08		14:39
 *============================================================================*/
static void CheckSum( BYTE *Frame )
{
    WORD R = 0;

    int i, nLen = (int)strlen((char*)Frame);  

    for( i=1; i < nLen; i++ )
	{
        R += *(Frame + i);
	}
    sprintf( (char *)Frame + nLen, "%04X\r", (WORD)-R );
}

/*=============================================================================*
 * FUNCTION: RecvDataFromSMIO()
 * PURPOSE  :receive data of SM from 485 com
 * RETURN   : int : byte number ,but if return -1  means  error
 * ARGUMENTS:
 *						CHAR*	sRecStr :		
 *								hComm	 :	485 com handle 
 *								iStrLen	 :
 * CALLS    : 
 * CALLED BY: 
 *								DLLExport BOOL Query()
 * COMMENTS : 
 * CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
 *==========================================================================*/
LOCAL INT32	RecvDataFromSMIO(HANDLE hComm, CHAR* sRecStr, INT32* piStrLen,INT32 iWantToReadnByte)
{
		INT32	iReadLen;		
		INT32	i		= 0;
		INT32	j		= 0;
		INT32	iHead	= -1;
		INT32	iTail	= -1;
	    ASSERT( hComm != 0 );
		ASSERT( sRecStr != NULL);
		
		//Read the data from Smac
		iReadLen = RS485_Read(hComm, (BYTE *)sRecStr, iWantToReadnByte);

		for (i = 0; i < iReadLen; i++)
		{
			if(bySOI == *(sRecStr + i))
			{
				iHead = i;
				break;
			}
		}

		if (iHead < 0)	//   no head
		{
			TRACE(" IO  IO  IO  RRR (iHead < 0)no head %d %s \n",(int)iReadLen,sRecStr);
			return FALSE;
		}

		for (i = iHead + 1; i < iReadLen; i++)
		{
			if(byEOI == *(sRecStr + i))
			{
				iTail = i;
				break;
			}
		}	

		if (iTail < 0)	//   no tail
		{
			TRACE(" IO  IO  IO  RRR (iTail < 0)no tail %d %s \n",(int)iReadLen,sRecStr);
			return FALSE;
		}	

		*piStrLen = iTail - iHead + 1;
		
		if(iHead > 0)
		{
			for (j = iHead; j < *piStrLen; j++)
			{
				*(sRecStr + (j - iHead)) = *(sRecStr + j);
			}
		}

		*(sRecStr + (iTail - iHead + 2)) = '\0';

		//printf("\n SMIO		RRRRR		=	%s  \n",sRecStr);

		return TRUE;
}

/*=============================================================================*
 * FUNCTION: RS485WaitReadable
 * PURPOSE : wait RS485 data ready
 * INPUT: 
 *     
 *
 * RETURN:
 *     int : 1: data ready, 0: timeout,Allow get mode or auto config
 *		
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32
 *			Ilockteng		2009-05-19
 *============================================================================*/
LOCAL INT32 RS485WaitReadable(INT32 fd, INT32 nmsTimeOut)
{
    fd_set readfd;
    struct timeval timeout;

	while (nmsTimeOut > 0)
	{
		// need add ClearWDT() here.
		if (nmsTimeOut < (5*1000))
		{
			timeout.tv_usec = nmsTimeOut%1000*1000;			/* usec     */
			timeout.tv_sec  = nmsTimeOut/1000;				/* seconds  */

			nmsTimeOut = 0;
		}
		else
		{
			timeout.tv_usec = 0;							/* usec     */
			timeout.tv_sec  = 5;							/* seconds  */

			RUN_THREAD_HEARTBEAT();

			nmsTimeOut -= (5*1000);
		}

	}

	FD_ZERO(&readfd);										/*  Initializes the set to the NULL set. */
	FD_SET(fd, &readfd);									/*  Adds descriptor s to set. */

	select(fd+1, &readfd, NULL, NULL, &timeout);
	
	if(FD_ISSET(fd, &readfd))	
	{
		return TRUE;										//1: data ready, can be read
	}
	else
	{
		return FALSE;										//0: Data not ready, can't read!!but,Allow get mode or auto config
	}
}
/*=============================================================================*
* FUNCTION: CheckStrLength
* PURPOSE : Check receive data length 
* INPUT: 
*     
*
* RETURN:
*     INT32   1:  succeed	0:  error  
*		
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*
*============================================================================*/
LOCAL  INT32 CheckStrLength(BYTE *abyRcvBuf, INT32 RecBufOffsetNum, INT32 RecDataLength)
{
	WORD		wLength;
	CHAR		c_CheckLength[5]	= {0};
	INT32		OffsetToLength		= RecBufOffsetNum;

	//repeat  length check
	wLength							= MakeDataLen(RecDataLength);
	sprintf((char *)c_CheckLength,"%04X\0",wLength);

	if((abyRcvBuf[OffsetToLength + 3]		!= c_CheckLength[3]) //fourth byte of  length check
		&& (abyRcvBuf[OffsetToLength + 2]	!= c_CheckLength[2]) //third  byte of  length check
		&&(abyRcvBuf[OffsetToLength + 1]	!= c_CheckLength[1]) //second byte of  length check
		&& (abyRcvBuf[OffsetToLength]		!= c_CheckLength[0]))//first  byte of  length check
	{
		return FALSE;
	}

	return TRUE;
}

/*=============================================================================*
* FUNCTION: CheckStrSum
* PURPOSE : Check receive data sum 
* INPUT: 
*     
*
* RETURN:
*     INT32   1:  succeed	0:  error  
*		
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*
*============================================================================*/
LOCAL INT32  CheckStrSum(CHAR *abyRcvBuf, INT32 RecTotalLength)
{
	CHAR	 szCheckCode[5]	= {0};
	//copy receive checkSum to szCheckCode 
	strncpy(szCheckCode, (CHAR*)abyRcvBuf + RecTotalLength - SMIO_SHECKSUM_EOI,4); 
	abyRcvBuf[RecTotalLength - SMIO_SHECKSUM_EOI] = 0;

	//repeat check sum
	CheckSum((BYTE*)abyRcvBuf);

	if((abyRcvBuf[RecTotalLength - 2]		== szCheckCode[3])  //fourth byte of check sum
		&& (abyRcvBuf[RecTotalLength - 3]	== szCheckCode[2])	//third	 byte of check sum
		&&(abyRcvBuf[RecTotalLength - 4]	== szCheckCode[1])	//second byte of check sum
		&& (abyRcvBuf[RecTotalLength - 5]	== szCheckCode[0]))	//first  byte of check sum
	{ 
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*=============================================================================*
 * FUNCTION: SMIOGetResponeData
 * PURPOSE : 
 * INPUT: 
 *     
 *
 * RETURN:
 *     INT32   0:  succeed	1:no response working  2:fail to communication 
 *			  -1:  function error
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32
 *			Copy from smio
 *============================================================================*/
LOCAL INT32 SMIOSendAndRead(
                   HANDLE	hComm,			 
                   BYTE		*sSendStr,		
                   BYTE		*abyRcvBuf,     
                   INT32	nSend,
				   int     cWaitTime,
				   INT32	iWantToReadnByte)
{
		UNUSED(cWaitTime);
		INT32		iReceiveNum;
		//INT32		iReceiveNumByClr;
		INT32		iDataInfoLength;
		//CHAR		c_CheckLength[5]	= {0};
		RS485_DRV*	pPort				= (RS485_DRV *)hComm;
		INT32		fd					= pPort->fdSerial;
		//BYTE		cClrBuffer[MAX_RECVBUFFER_NUM_SMIO];
		ASSERT( hComm !=0 && sSendStr != NULL && abyRcvBuf != NULL);

		RS485_ClrBuffer(hComm);

		
		if(RS485_Write(hComm, sSendStr, nSend) != nSend)
		{
			//note	app log	!!
			AppLogOut("SMIO SendAndRead", APP_LOG_UNUSED,
				  "[%s]-[%d] ERROR: failed to  RS485_Write \n\r", 
				  __FILE__, __LINE__);

			TRACE("\n*****   !!!!! SMIO  RS485_Write   ******\n");
			return NO_RESPONSE_BEXISTENCE;
		}

		if (RS485WaitReadable(fd, 1000) > 0 )
		{
			if (RecvDataFromSMIO(hComm, (CHAR*)abyRcvBuf, &iReceiveNum,iWantToReadnByte))
			{
				//check cid1
				if( abyRcvBuf[5] != sSendStr[5] || abyRcvBuf[6] != sSendStr[6] )
				{
					TRACE("\n*****SMIO  check cid1  error ******\n");
					return NO_RESPONSE_BEXISTENCE;					
				}
				//check equip addr 
				else if( abyRcvBuf[3] != sSendStr[3] || abyRcvBuf[4] != sSendStr[4] )
				{
					TRACE("\n*****SMIO  check equip addr  error ******\n");
					return NO_RESPONSE_BEXISTENCE;						
				}
				//check RTN
				else  if( abyRcvBuf[7] != '0' || abyRcvBuf[8] != '0' )
				{
					TRACE("\n*****SMIO check RTN  error ******\n");
					return NO_RESPONSE_BEXISTENCE;
				}
				else
				{
					//check  strlen
					iDataInfoLength = iReceiveNum - SMIO_SHECKSUM_EOI - DATA_YDN23_SMIO;
					//	check  strlen 
					if (!CheckStrLength(abyRcvBuf, LENGTH_YDN23_SMIO, iDataInfoLength))
					{
						TRACE("\n*****SMIO  check StrLength error ******\n");
						return NO_RESPONSE_BEXISTENCE;
					}

					// check sum
					if(CheckStrSum((CHAR*)abyRcvBuf, iReceiveNum))
					{
						return RESPONSE_OK;

					}
					else
					{
						TRACE("\n*****SMIO  check sum error ******\n");
						return NO_RESPONSE_BEXISTENCE;
					}
				}
			}
			else
			{
				TRACE("\n*****SMIO  else  read 0 ******\n");
				return NO_RESPONSE_BEXISTENCE;
			}
		}
		else
		{
			TRACE("\n*****SMIO  RS485WaitReadable read 0 ******\n");
			return NO_RESPONSE_BEXISTENCE;
		}
}
/*=============================================================================*
 * FUNCTION: SMIO_SendCtlCmd
 * PURPOSE : control smio by iChannelNo
 * INPUT: 
 *    
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
VOID SMIO_SendCtlCmd(RS485_DEVICE_CLASS*  SmioDeviceClass,
						INT32 iChannelNo, 
						float fParam,
						char *pString)	
{
	UNUSED(fParam);
	CHAR	 sTarget[128]={0};
	INT32	 nPoint1 = 0;
	INT32	 param1,param2,nChanel;
	INT32	 nPoint, nCmdNo;
	INT32	 iUnitNo;

	if (RS485_RUN_MODE_SLAVE == g_RS485Data.enumRunningMode)
	{
		return;
	}

	if(iChannelNo < SMIO_CH_START ||  SMIO_CH_END < iChannelNo)
	{
		return ;//no SMIO
	}	

	
	 iUnitNo = SmioGetEquipAddr(iChannelNo, SmioDeviceClass);

	 if(iUnitNo == -1)
	 {
		 return;//Get addr fail
	 }

	//Get nCmdNo
	nPoint = RS485_StrExtract(pString, sTarget, ',' );
	
	if( sTarget[0] == 0 )           //BPY 2005.01.06
	{
		return;
	}
	nCmdNo = (int)atoi(pString);
	nPoint1 += nPoint;

	//ͨ����
	if( nPoint>0 )
	{
		nPoint = RS485_StrExtract( pString + nPoint1, sTarget, ',' );
		if( sTarget[0] == 0 )       //BPY 2005.01.06
		{
			return;
		}
		nChanel = atoi( sTarget );
	}

	nPoint1 += nPoint;

	//����1
	if( nPoint>0 )
	{
		nPoint = RS485_StrExtract( pString+nPoint1, sTarget, ',' );
		if( sTarget[0] == 0 )    //BPY 2005.01.06
		{
			return;
		}
		param1 = atoi( sTarget );
	}

	nPoint1 += nPoint;
	//����2
	if( nPoint>0 )
	{
		nPoint = RS485_StrExtract( pString+nPoint1, sTarget, ',' );
		if( sTarget[0] == 0 )    //BPY 2005.01.06
		{
			return;
		}
		param2 = atoi( sTarget );
	}
	else
	{
		param2 = 0;
	}

	if(SMIONeedCommProc())
	{
		SMIOReopenPort();
	}

	SMIOSendControlCmd45(SmioDeviceClass->pCommPort->hCommPort,
						iUnitNo,
						nCmdNo,
						nChanel,
						param1,
						param2);

	Sleep(20);

//return SetDev( hComm, nUnitNo, nCmdNo, nChanel, param1, param2 );
//	INT32		iUnitNo;
//	INT32		iCtrlInvalidChannelNo;
//	INT32		nPointStr;
//	INT32		CtrlType;
//	INT32		iPulseTime;	
//	BYTE		sTarget[128] = {0};
//	nPointStr				 = 0;
//
//#if TST_RS485_DEBUG_SMIO
//	//return;
//#endif
//
//	if(iChannelNo < SMIO_CH_START || SMIO_CH_END < iChannelNo)
//	{
//		return ;//no SMIO
//	}
//#if  TST_RS485_DEBUG_SMIO
//	printf("  ####  This SMIO  iChannelNo = %d \n",iChannelNo);
//#endif
//	//get addr
//	iUnitNo = SmioGetEquipAddr(iChannelNo);
//
//#if  TST_RS485_DEBUG_SMIO
//	printf("  ####  This SMIO  iUnitNo = %d \n",iUnitNo);
//#endif
//	//param4
//	iCtrlInvalidChannelNo = (INT32)fParam;
//
//#if  TST_RS485_DEBUG_SMIO
//	printf("  ####  This SMIO  iCtrlInvalidChannelNo = %d \n",iCtrlInvalidChannelNo);
//#endif
//
//	//param5	
//	nPointStr = RS485_StrExtract(pString, sTarget, ',');
//
//	if(sTarget[0] == 0)
//	{
//		return FALSE;
//	}
//
//	CtrlType = atoi(sTarget);
//	//param6
//	RS485_StrExtract(pString + nPointStr, sTarget, ',');
//
//#if  TST_RS485_DEBUG_SMIO
//	printf("  ####  This SMIO  CtrlType = %d \n",CtrlType);
//#endif
//
//	if(sTarget[0] == 0)
//	{
//		return FALSE;
//	}
//
//	iPulseTime = atoi(sTarget);
//#if  TST_RS485_DEBUG_SMIO
//	printf("  ####  This SMIO  iPulseTime = %d \n",iPulseTime);
//#endif
//
//	SMIOSendControlCmd45(SmioDeviceClass->pCommPort->hCommPort,
//						iUnitNo,
//						iChannelNo,
//						iCtrlInvalidChannelNo,
//						CtrlType,
//						iPulseTime);
}

/*=============================================================================*
 * FUNCTION: SMIOGetVer
 * PURPOSE :get vertion  for scan smio equip 
 * INPUT: 
 *    
 *
 * RETURN:
 *		szVer   OUT
 *		INT32   0:  succeed	 1:no response working 
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32
 *copy from smio
 *============================================================================*/
LOCAL INT32 SMIOGetVer( HANDLE hComm, 
					   INT32 nUnitNo,
					   BYTE cCID1,
					   BYTE cCID2, 
					   BYTE*szVer)
{
	INT32		iSmioStatus;   
	BYTE		szSendStr[20] = {0};
	BYTE		abyRcvBuf[MAX_RECVBUFFER_NUM_SMIO] = {0};

    sprintf((CHAR *)szSendStr,
			"~%02X%02X%02X%02X%04X\0",
			0x20, 
			(unsigned int)nUnitNo, 
			cCID1,
			cCID2, 
			0 );

	CheckSum( szSendStr );
	
	//1	  1	  1	  1	   1        2	   X	2   1
	//1	  2	  2	  2	   2	    4	   2X	4   1
	//SOI VER ADR CID1 CID2/RTN	LENGTH INFO	SUM	EOI
	*(szSendStr + 17) =  byEOI;
   
	iSmioStatus = SMIOSendAndRead( hComm, szSendStr, abyRcvBuf, 18, RS485_WAIT_LONG_TIME,512);
	
	if(iSmioStatus == 0)
	{
		strncpy( (char*)szVer, (CHAR*)abyRcvBuf + 1, 2 );
	}
	return  iSmioStatus;
}


/*=============================================================================*
 * FUNCTION: SMIOSendControlCmd45
 * PURPOSE : sen control command to smio by 0x45
 * INPUT: 
 *    
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
BOOL SMIOSendControlCmd45(
						HANDLE hComm,		  // ͨѶ�ھ��
						INT32 nUnitNo,        // �ɼ�����Ԫ��ַ
						INT32 nChannelNo,     // �����       //-->ͨ����(0->14ͨ��,1->15ͨ��,2->16ͨ��,) Lintao���¶����ʽ  BPY 2005.01.06
						INT32 nInvalid,       // ����ͨ����   //������Ч��Ч(1-��Ч��0����Ч)
						INT32 nControlType,   // ��������     //ֻ��4�����0-��͵�ƽ��1-��ߵ�ƽ��2-�����壻3-������
						INT32 nPulseTime	  // ������     //ע�����nInvalidΪ1, nControlType������ֵ��
											  // ���nInvalidΪ0, nControlType��ֵ0,1������2��3������
						)	
{

	 WORD		wLength;
	 //INT32		i;
	 INT32		SmioStatus;
	 //INT32		nLen			= 0;
	 BYTE		szSendStr[100]	= {0};
	 INT32		iTemp;
	 INT32		iSequenceNum;
	 INT32		iRelayID = 0;

	 //Get Sequence Number
	 for(iTemp = 0; iTemp < MAX_NUM_SMIO; iTemp++)
	 {
		 if((nChannelNo >= (SMIO_CH_START + iTemp * MAX_CHN_DISTANCE_SMIO)) 
			 && (nChannelNo < (SMIO_CH_START + (iTemp + 1) * MAX_CHN_DISTANCE_SMIO) - 1))
		 {
			 iSequenceNum  = iTemp;
			 break;
		 }
	 }

	nChannelNo = nChannelNo - (SMIO_CH_A_DINPUT1 + ((iSequenceNum) * 40));

	if(nChannelNo < 0 || nChannelNo > 2)
	{
		return FALSE;
	}

    if(nPulseTime > 2600)
	{
        nPulseTime = 2600;
	}
    if(nPulseTime < 100)
	{
        nPulseTime = 100;
	}
//#if TST_RS485_DEBUG_SMIO
//	printf("\n BBBBBBB   MakeDataLen BBBB\n");
//#endif
	wLength = MakeDataLen(6);    

	iRelayID = nChannelNo;
    nChannelNo = nChannelNo + 14;

    if(nInvalid == 0x01)
    {
        nControlType = nControlType + 0x10;				
    }
    else if(nInvalid == 0x00)
    {
        if(nControlType==0x00 || nControlType==0x02)
        {
            nControlType = nControlType + 0x11;				
        }
        else if(nControlType==0x01 || nControlType==0x03)
        {
            nControlType = nControlType + 0x0F;				
        }
        else
        {
            return FALSE;
        }
    }
    else
    {
		return FALSE;
    }
    
	//sprintf((char *)szSendStr,
	//			"~%s%02X%02X%02X%04X%02X%02X%02X\0",
	//			SMIOszVer,
	//			nUnitNo, 
	//			SMIO_CID1_0XD1, 
	//			SMIO_CID2_0X45,
	//			wLength,
	//			nChannelNo,
	//			nControlType,
	//			nPulseTime/100 );

	sprintf((char *)szSendStr,
				"~%s%02X%02X%02X%04X\0",
				SMIOszVer,
				(unsigned int)nUnitNo, 
				SMIO_CID1_0XD1, 
				SMIO_CID2_0X45,
				wLength);
				//nChannelNo,
				//nControlType,
				//nPulseTime/100 );
//02X%02X%02X
	sprintf((char*)(szSendStr + 13 ),
		"%02X\0",
		(unsigned int)nChannelNo);
	sprintf((char*)(szSendStr + 15),
		"%02X\0",
		(unsigned int)nControlType);
	sprintf((char*)(szSendStr + 17),
		"%02X\0",
		(unsigned int)(nPulseTime/100));

	//printf("\n	SMIO nChannelNo=%d		nControlType=%d		nPulseTime=%d	\n",nChannelNo,nControlType,nPulseTime);

	CheckSum(szSendStr);

	SmioStatus = SMIOSendAndRead(hComm, 
								szSendStr, 
								g_SmioSamplerData.abyRcvBuf, 
								24,
								RS485_WAIT_VERY_SHORT_TIME,
								18);

	/*****************************************************************************
		�����TEST RELAY��������Ҫ���ٵĽ�RELAY״̬�ϱ����˴����
		RS485_SpecificProc ���������ٽ�״̬���ͣ���
	******************************************************************************/
	//int iRoughDataPosition = (nUnitNo - SMIO_ADDR_START) * SMIO_MAX_SIGNALS_NUM 
	//	+ SMIO_RELAY1_STATUS + (iRelayID);

	if (RESPONSE_OK == SmioStatus)
	{
		//�����ļ� 0��OFF 1 ��ON ,����SMЭ��ͽ�ϴ�ӡ0x10��OFF�� 0x11��ON
		if (0x10 == nControlType)
		{
			g_SmioSamplerData.aRoughDataSmio[nUnitNo - SMIO_ADDR_START][SMIO_RELAY1_STATUS + (iRelayID)].uiValue = 0;	//off
		}
		else if (0x11 == nControlType)
		{
			g_SmioSamplerData.aRoughDataSmio[nUnitNo - SMIO_ADDR_START][SMIO_RELAY1_STATUS + (iRelayID)].uiValue = 1;	//on
		}
		//SmioDeviceClass->pRoughData[iRoughDataPosition + i].uiValue
	}

	return 0;
}



/*=============================================================================*
 * FUNCTION: FixFloatDat
 * PURPOSE : eight byte convert to float
 * INPUT: 
 *     
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *		kenn.wan                 DATE: 2008-08-15 14:55
 *============================================================================*/
static float FixFloatDat( char* sStr )
{
	INT32					i;
	STRTOFLOAT				floatvalue;
	CHAR					cTemp;
	CHAR					cTempHi;
	CHAR					cTempLo;

	for( i=0; i<4; i++ )
	{
#ifdef _CPU_BIG_ENDIAN		// ppc
#ifdef _SAMPLER_REPLACE_SSCANF
		//TRACE("------------------------1----------------------------\n");
		sscanf((const char *)sStr+i, "%x", 
			&floatvalue.by_value[3-i]);
#else
		int	c;
		//TRACE("------------------------2----------------------------\n");
		sscanf((const char *)sStr+i, "%x", 	&c);
		floatvalue.by_value[3-i] = (BYTE)c;
#endif
#else	// for x86
		//hi bit
		if((*(sStr + i * 2) > 0x40))
		{
			cTempHi = (*(sStr + i * 2) - 0x40) + 9;
		}
		else
		{
			cTempHi = (*(sStr + i * 2) - 0x30);
		}

		//low bit
		if((*(sStr + i * 2 + 1)) > 0x40)
		{
			cTempLo = (*(sStr + i * 2 + 1) - 0x40) + 9;
		}
		else
		{
			cTempLo = (*(sStr + i * 2 + 1) - 0x30);
		}

		cTemp = cTempHi * 16 + cTempLo;
		floatvalue.by_value[i] = cTemp;
#endif
	}

	return floatvalue.f_value;
}



/*=============================================================================*
 * FUNCTION: FixIntDat
 * PURPOSE : convect from chat string to int 
 * INPUT: 
 *    
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])

 *
 *============================================================================*/
INT32 FixIntDat( char* sStr, int nLen )
{
    INT32		nData	= 0;
    CHAR		S[10]	= {0};
    CHAR		temp[3]	= {0};
	
    if(nLen == 2)
    {
        strncpy( S, (char*)sStr, 2 );   
        S[2] = 0;
        
        if(S[0] == 0x20)
        {
            nData = 0x20;
        }
        else
        {
            sscanf( S, "%02x",(unsigned int *)(&nData) );
        }
    }
    else if(nLen == 4)
    {
        strncpy( S, (char*)sStr, 4 );   

		temp[0] = S[2];	
		temp[1] = S[3];
		S[2]	= S[0];
		S[3]	= S[1];
		S[0]	= temp[0];
		S[1]	= temp[1];
        S[4]	= 0;
		temp[2] = 0;
        
        sscanf( S, "%04x", (unsigned int *)(&nData) );
    }
    else
    {
    }
    return nData;
}

/*=============================================================================*
 * FUNCTION: SMIOUnpackCmd43
 * PURPOSE : arrange sampling data to  RoughData
 * INPUT: 
 * 
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMIOUnpackCmd43(INT32 iAddr,RS485_DEVICE_CLASS*  SmioDeviceClass,BYTE* pReceiveData)
{
	INT32			i;
	INT32			iChanelNum;
	INT32			iRoughDataPosition;
	BYTE*			pRecvDataInfo;

	//notes : there haven't data flag
	pRecvDataInfo	= pReceiveData + DATA_YDN23_SMIO;
	iChanelNum		= FixIntDat((char*)pRecvDataInfo, 2);
	iChanelNum		= SMIOMergeAsc((char*)pRecvDataInfo);
	pRecvDataInfo	= pRecvDataInfo + 2;
	
	if(iChanelNum <= 3)
	{
		iRoughDataPosition = (iAddr - SMIO_ADDR_START) * SMIO_MAX_SIGNALS_NUM 
							 + SMIO_RELAY1_STATUS;

		for(i = 0; i < iChanelNum; i++)   //00H--low��01H--high
		{
			SmioDeviceClass->pRoughData[iRoughDataPosition + i].uiValue 
				= SMIOMergeAsc((char*)(pRecvDataInfo + i * 2));
		}
	}
	return FALSE;
}

/*=============================================================================*
 * FUNCTION: SMIOUnpackCmd41
 * PURPOSE : arrange sampling data by 0x41 to  RoughData
 * INPUT: 
 *    
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMIOUnpackCmd41(INT32 iAddr, RS485_DEVICE_CLASS* SmioDeviceClass, BYTE* pReceiveData)
{
	INT32			i;
	INT32			iChanelNum;
	INT32			iRoughDataPosition;
	BYTE*			pRecvDataInfo;
	float			f_Temp;
	static float	sfLastTimeSampling[SMIO_ADDR_END - SMIO_ADDR_START + 1][SMIO_CID2_41SIG_NUM];
	long			lLastTempData;

	pRecvDataInfo		= pReceiveData + DATA_YDN23_SMIO;
	pRecvDataInfo		= pRecvDataInfo + 2;
	iChanelNum			= FixIntDat((char*)pRecvDataInfo, 2);
	pRecvDataInfo		= pRecvDataInfo + 2;
	iRoughDataPosition	= (iAddr - SMIO_ADDR_START) * SMIO_MAX_SIGNALS_NUM;
	f_Temp				= 0;

	if(iChanelNum <= SMIO_CID2_41SIG_NUM)
	{
		for(i = 0; i < iChanelNum; i++)
		{	
			//refer to smio protocol cmmd 0x41
			if (i < SMIO_CID2_AD_INPUT)
			{
				f_Temp	= FixFloatDat((char*)(pRecvDataInfo + i * 8));
				lLastTempData = sfLastTimeSampling[iAddr - SMIO_ADDR_START][i];

				//����澯��Ҫȥ�ˣ�Ӧ���Ǵ�810G�������ģ�����������Ҫ���׼������ˡ�
				if( ((long)f_Temp > 4) || (lLastTempData) && ((long)f_Temp < 4)
					&& ((long)f_Temp) >= 1 )
				{
					SmioDeviceClass->pRoughData[iRoughDataPosition + i].dwValue 
							= 0x01;
				}
				else
				{
					SmioDeviceClass->pRoughData[iRoughDataPosition + i].dwValue 
							= 0x00;				
				}

				sfLastTimeSampling[iAddr - SMIO_ADDR_START][i] = f_Temp;
			}
			else
			{
				SmioDeviceClass->pRoughData[iRoughDataPosition + i].fValue 
						= FixFloatDat((char*)(pRecvDataInfo + i * 8));
			}
		}
	}

	return 0;
}


/*=============================================================================*
 * FUNCTION: SMIOSampleCmd43
 * PURPOSE : sampling smio by 0x43
 * INPUT: 
 *   
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMIOSampleCmd43(INT32 iAddr,RS485_DEVICE_CLASS*  SmioDeviceClass)
{
	INT32			i;
	INT32			nUnitNo;
	//INT32			SmioStatus;
	INT32			iSmioStatus;
	//INT32			iSamplingTimes;  
	INT32			iRoughPosition;
	BYTE			szSendStr[21]	= {0};
	BYTE			abyRcvBuf[MAX_RECVBUFFER_NUM_SMIO] = {0};
	nUnitNo							= iAddr;

	sprintf((char *)szSendStr,
				"~%s%02X%02X%02X%04X\0",
				SMIOszVer,
				(unsigned int)nUnitNo, 
				(unsigned int)SMIO_CID1_0XD1, 
				(unsigned int)SMIO_CID2_0X43,
				0);

	CheckSum( szSendStr );
	//1	  1	  1	  1	   1        2	   X	2   1
	//1	  2	  2	  2	   2	    4	   2X	4   1
	//SOI VER ADR CID1 CID2/RTN	LENGTH INFO	SUM	EOI
	*(szSendStr + 17) =  byEOI;

	for (i = 0;i < RS485_RECONNECT_TIMES;i++)
	{
		iSmioStatus = SMIOSendAndRead( SmioDeviceClass->pCommPort->hCommPort,
									szSendStr,
									abyRcvBuf,
									18,
									RS485_WAIT_SHORT_TIME,
									0x08 + 18);

		if(iSmioStatus == RESPONSE_OK)
		{
			break;
		}
	}

	if (RESPONSE_OK != iSmioStatus)
	{
		return FALSE;
		//TRACE("\n  ***SMIO  CID2 43 ERROR i = %d \n",i);
	}
	else if(iSmioStatus == RESPONSE_OK)
	{	
		iRoughPosition = (nUnitNo - SMIO_ADDR_START) * SMIO_MAX_SIGNALS_NUM;
		SmioDeviceClass->pRoughData[iRoughPosition + SMIO_COMM_STATUS].iValue 
			= SMIO_COMM_OK;
		SMIOUnpackCmd43(nUnitNo, SmioDeviceClass, abyRcvBuf);
		return TRUE;
	}

	return FALSE;
}

/*=============================================================================*
 * FUNCTION: SMIOSampleCmd41
 * PURPOSE : sampling smio by 0x41
 * INPUT: 
 *   
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMIOSampleCmd41(INT32 iAddr,RS485_DEVICE_CLASS*  SmioDeviceClass)
{	
	INT32	i;
	INT32	nUnitNo;
	INT32	iCommStat;
	INT32	iSmioStatus;
	//INT32	iSamplingTimes;
	INT32	iRoughPosition;	
	BYTE	szSendStr[21]	= {0};
	BYTE	abyRcvBuf[MAX_RECVBUFFER_NUM_SMIO] = {0};

	nUnitNo					= iAddr;

	sprintf((char *)szSendStr,
					"~%s%02X%02X%02X%04X\0", 
					SMIOszVer,
					(unsigned int)nUnitNo, 
					SMIO_CID1_0XD1, 
					SMIO_CID2_0X41,
					0);

	CheckSum( szSendStr );
	//1	  1	  1	  1	   1        2	   X	2   1
	//1	  2	  2	  2	   2	    4	   2X	4   1
	//SOI VER ADR CID1 CID2/RTN	LENGTH INFO	SUM	EOI
	*(szSendStr + 17) =  byEOI;
	iRoughPosition = (nUnitNo - SMIO_ADDR_START) * SMIO_MAX_SIGNALS_NUM;

	for (i = 0;i < RS485_RECONNECT_TIMES; i++)
	{
		iSmioStatus = SMIOSendAndRead(SmioDeviceClass->pCommPort->hCommPort,
									szSendStr,
									abyRcvBuf,
									18,
									RS485_WAIT_LONG_TIME,
									1024);

		iCommStat
			= SmioDeviceClass->pRoughData[iRoughPosition + SMIO_COMM_STATUS].iValue;

		if((iSmioStatus == RESPONSE_OK) || (iCommStat == SMIO_COMM_FAIL))
		{
			break;
		}
	}

	if(iSmioStatus == RESPONSE_OK)
	{
		SMIOUnpackCmd41(nUnitNo, SmioDeviceClass, abyRcvBuf);
		return TRUE;

	}
	else//(RESPONSE_OK != iSmioStatus)
	{
		TRACE("\n  ***SMIO CID2 41 ERROR i = %d  nUnitNo = %d \n",(int)i,(int)nUnitNo);
		return FALSE;	
	}
}


/*=============================================================================*
 * FUNCTION: SMIO_Reconfig
 * PURPOSE : scan smio equip info ,record info to RoughData
 * INPUT: 
 *    
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMIO_Reconfig(RS485_DEVICE_CLASS*  SmioDeviceClass)
{
	INT32			i;
	//INT32			j;
	//SIG_ENUM		emRst; 
	INT32			iAddr;
	INT32			SmioStatus;
	INT32			iRoughDataPosition;
 	BYTE			cExistMaxAddr       = 0;	
	INT32			iSequence			= 0;

	for(iAddr = SMIO_ADDR_START; iAddr <= SMIO_ADDR_END; iAddr++)
	{
		/*	 
		RETURN:
 		INT32   0:  succeed	 1:no response working 
		*/
		for (i = 0; i < RS485_RECONNECT_TIMES; i++)
		{
			SmioStatus = SMIOGetVer( SmioDeviceClass->pCommPort->hCommPort,
								iAddr,
								(BYTE)SMIO_CID1_0XD1,
								(BYTE)SMIO_CID2_GETVER, 
								SMIOszVer);

			if (RESPONSE_OK == SmioStatus)
			{
				cExistMaxAddr = g_RS485Data.stSMSmplingCtrl.cExistMaxAddr;
				g_RS485Data.stSMSmplingCtrl.cSAMPExistAddr[cExistMaxAddr] = iAddr;
				g_RS485Data.stSMSmplingCtrl.cExistMaxAddr++;		
				//printf("\n  Reconfig   SMIO  iAddr = %d \n",iAddr);
				TRACE("\n  SMIO Reconfig  iAddr = %d \n",(int)iAddr);
				break;
			}
		}

		iRoughDataPosition = (iAddr - SMIO_ADDR_START) * SMIO_MAX_SIGNALS_NUM;
		
		//succeed
		if(SmioStatus == RESPONSE_OK)
		{	
			SmioDeviceClass->pRoughData[iRoughDataPosition + SMIO_SEQ_NUM].iValue 
				= iSequence;
			SmioDeviceClass->pRoughData[iRoughDataPosition + SMIO_COMM_STATUS].iValue 
				= SMIO_COMM_OK;
			SmioDeviceClass->pRoughData[iRoughDataPosition + SMIO_WORKING_ADDR].iValue 
				= iAddr;
			SmioDeviceClass->pRoughData[iRoughDataPosition + SMIO_WORKING_STATUS].iValue
				= SMIO_EQUIP_EXISTENT;
			SmioDeviceClass->pRoughData[SMIO_G_EXIST].iValue = SMIO_EQUIP_EXISTENT;
			SmioDeviceClass->pRoughData[SMIO_WORKING_GROUP_STATUS].iValue 
				= SMIO_EQUIP_EXISTENT;

			iSequence++;
		}
		else
		{
			SmioDeviceClass->pRoughData[iRoughDataPosition + SMIO_COMM_STATUS].iValue
				= SMIO_COMM_OK;
			SmioDeviceClass->pRoughData[iRoughDataPosition + SMIO_WORKING_STATUS].iValue 
				= SMIO_EQUIP_NOT_EXISTENT;
			SmioDeviceClass->pRoughData[SMIO_G_EXIST].iValue = SMIO_EQUIP_NOT_EXISTENT;
		}
	}

	return 0;
}


/*=============================================================================*
* FUNCTION: SMIOClrCommBreakTimes
* PURPOSE : Only one time communication ok clean the failt flag
* INPUT:	SmioDeviceClass		iAddr
*    
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32  SMIOClrCommBreakTimes(INT32 iAddr, RS485_DEVICE_CLASS*  SmioDeviceClass)
{
	INT32 iRoughPosition;
	iRoughPosition = (iAddr - SMIO_ADDR_START) * SMIO_MAX_SIGNALS_NUM;

	SmioDeviceClass->pRoughData[iRoughPosition + SMIO_COMM_STATUS].iValue 
		= SMIO_COMM_OK;

	SmioDeviceClass->pRoughData[iRoughPosition + SMIO_INTERRUPT_TIMES].iValue 
		= 0;
	return 0;
}
/*=============================================================================*
* FUNCTION: SMIOIncCommBreakTimes
* PURPOSE : Record communication failt time,if more than three means report failes
* INPUT:	RS485_DEVICE_CLASS*  SmioDeviceClass
*			INT32 iAddr
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32  SMIOIncCommBreakTimes(INT32 iAddr, RS485_DEVICE_CLASS*  SmioDeviceClass)
{
	INT32	iRoughPosition;
	iRoughPosition = (iAddr - SMIO_ADDR_START) * SMIO_MAX_SIGNALS_NUM;

#define  RS485_SMIO_INTERRUPT_TIMES 10

	if (SmioDeviceClass->pRoughData[iRoughPosition + SMIO_INTERRUPT_TIMES].iValue 
		>= RS485_SMIO_INTERRUPT_TIMES)
	{
		SmioDeviceClass->pRoughData[iRoughPosition + SMIO_COMM_STATUS].iValue
			= SMIO_COMM_FAIL;
	}
	else
	{
		if(SmioDeviceClass->pRoughData[iRoughPosition + SMIO_INTERRUPT_TIMES].iValue
			< RS485_SMIO_INTERRUPT_TIMES + 1)
		{
			SmioDeviceClass->pRoughData[iRoughPosition + SMIO_INTERRUPT_TIMES].iValue++;
		}
	}

	return 0;
}

/*=============================================================================*
* FUNCTION: SMIONeedCommProc
* PURPOSE : Check rs485Comm Attr,
*			Reopen the port if the port is not 19200 baud or not None-check
* INPUT: 
*    
* RETURN:
*     TRUE: need process FALSE:needn't process
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32	SMIONeedCommProc(void)
{
	if(g_RS485Data.CommPort.enumAttr		!= RS485_ATTR_NONE
		|| g_RS485Data.CommPort.enumBaud	!= RS485_ATTR_19200
		|| g_RS485Data.CommPort.bOpened		!= TRUE)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


/*=============================================================================*
* FUNCTION: SMACReopenPort
* PURPOSE :
* INPUT: 
*    
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMIOReopenPort(void)
{
	INT32			iErrCode;
	INT32			iOpenTimes;
	iOpenTimes		= 0;
	
	while (iOpenTimes < RS485_COMM_TRY_OPEN_TIMES)
	{
		if(g_RS485Data.CommPort.hCommPort && g_RS485Data.CommPort.bOpened)
		{
			RS485_Close(g_RS485Data.CommPort.hCommPort);
		}

		g_RS485Data.CommPort.hCommPort 
			= RS485_Open("19200, n, 8, 1", RS485_READ_WRITE_TIMEOUT, (int *)(&iErrCode));

		if(iErrCode == ERR_COMM_OK)
		{
			g_RS485Data.CommPort.bOpened = TRUE;
			g_RS485Data.CommPort.enumAttr = RS485_ATTR_NONE;
			g_RS485Data.CommPort.enumBaud = RS485_ATTR_19200;
			Sleep(5);
			break;
		}
		else
		{
			Sleep(1000);
			iOpenTimes++;
			g_RS485Data.CommPort.bOpened = FALSE;	
			AppLogOut("RS485_SMIO_ReOpen", APP_LOG_UNUSED,
				"[%s]-[%d] ERROR: failed to  open rs485Comm IN The SMIO ReopenPort \n\r", 
				__FILE__, __LINE__);
		}		
	}
	return 0;
}
/*=============================================================================*
* FUNCTION: bSmioCheckCommFail()
* PURPOSE : 
* INPUT:	
*     
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 bSmioCheckCommFail(RS485_DEVICE_CLASS*  SmioDeviceClass, INT32 SMIOUnitNo)
{
	INT32		i;
	INT32		iSmioStatus;
	BYTE		szSendStr[20] = {0};
	BYTE		abyRcvBuf[MAX_RECVBUFFER_NUM_SMIO] = {0};
	
	sprintf((CHAR *)szSendStr,
		"~%02X%02X%02X%02X%04X\0",
		0x20, 
		(unsigned int)SMIOUnitNo, 
		SMIO_CID1_0XD1,
		SMIO_CID2_GETVER, 
		0 );

	CheckSum(szSendStr);

	*(szSendStr + 17) =  byEOI;

	//RS485_ClrBuffer(SmioDeviceClass->pCommPort->hCommPort);

	TRACE(" ::: bCheck IO %d \n",(int)SMIOUnitNo);

	for (i = 0; i < 1; i++)
	{
		iSmioStatus = SMIOSendAndRead( SmioDeviceClass->pCommPort->hCommPort, 
						szSendStr, 
						abyRcvBuf, 
						18, 
						RS485_WAIT_VERY_SHORT_TIME,
						18);

		if (RESPONSE_OK == iSmioStatus)
		{
			break;
		}
	}
	return iSmioStatus;
}

/*=============================================================================*
 * FUNCTION: SMIOSample
 * PURPOSE : sampling smio all sinal info 
 * INPUT: 
 *    
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMIOSample(RS485_DEVICE_CLASS*  SmioDeviceClass)
{
	INT32			iAddr;
	INT32			iRoughDataPosition;

	if(SMIONeedCommProc())
	{
		SMIOReopenPort();
	}

	if(SmioDeviceClass->bNeedReconfig == TRUE)
	{
		SmioDeviceClass->bNeedReconfig = FALSE;
		SMIO_Reconfig(SmioDeviceClass);
		return TRUE;
	}

	//Block 500ms
	for(iAddr = SMIO_ADDR_START; iAddr <= SMIO_ADDR_END; iAddr++)
	{
		iRoughDataPosition = (iAddr - SMIO_ADDR_START) * SMIO_MAX_SIGNALS_NUM;

		if(SmioDeviceClass->pRoughData[iRoughDataPosition + SMIO_WORKING_STATUS].iValue 
			== SMIO_EQUIP_EXISTENT)
		{
			RUN_THREAD_HEARTBEAT();

			//Batt testing return SM;
			if (bBattTesting(TRUE))
			{
				//printf("\n$$$$$$$$$$  in  SMIOSample bBattTesting  ADDR = %d return \n",iAddr);
				return TRUE;
			}

			//CMD43 query one time
			if (SMIOSampleCmd43(iAddr, SmioDeviceClass))
			{
				SMIOClrCommBreakTimes(iAddr, SmioDeviceClass);
			}
			else
			{
				//because communication error needn't sampling
				SMIOIncCommBreakTimes(iAddr, SmioDeviceClass);
				continue;
			}
				
			if (!SampCheckSmAddrProcess(iAddr))
			{
				continue;
			}

			SMIOSampleCmd41(iAddr, SmioDeviceClass);
		
		}	
	}

	return TRUE;
}

/*****************************************************************************
	2010/12/07
	�����ͻ����ܣ�����SMIO����ǰ��䣬��ɨ�赽SMIO2��ʱ��ͽ�������������Ϊ���ڣ�
	Ȼ����������������������������Ԫ�ĸ���������������Ԫ�豸��Ϊ����
	������������� ����������������ı�ʱ����SMDU���е��豸��Ϣ�����ı�֪ͨ��WEB
*****************************************************************************/
static BOOL FuelIsExist(RS485_DEVICE_CLASS*  SmioDeviceClass)
{
	INT32	i,iRoughDataPosition;

	i = 1;//SMIO2

	iRoughDataPosition  = i * SMIO_MAX_SIGNALS_NUM;

	if(SmioDeviceClass->pRoughData[iRoughDataPosition + SMIO_WORKING_STATUS].iValue 
		== SMIO_EQUIP_EXISTENT)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
//
//static SIG_ENUM OBFuelGetManagementUnitNum(void)
//{
//    //INT32 iFuelUnitNum = 0;
//    INT32 iFuelGroupEquipId = 700;
//    INT32 iFuelUnitNumSigID = 2;
//    INT32 iFuelUnitNumSigType = 2;
//
//    //G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
//    static BOOL bInit = FALSE;
//    static BOOL bConfigured = TRUE;
//
//    if (!bInit)
//    {
//	EQUIP_INFO* pEquipInfo;
//	int  nBufLen;
//
//	if (DxiGetData(VAR_A_EQUIP_INFO,
//	    iFuelGroupEquipId,			
//	    0,		
//	    &nBufLen,			
//	    &pEquipInfo,			
//	    0) == ERR_DXI_INVALID_EQUIP_ID)
//	{
//	    bConfigured = FALSE;
//
//	    //give WARNING msg
//	    AppLogOut("RS485_PORT", APP_LOG_WARNING, 
//		"Fuel equipment not configured in the solution.");
//	}
//
//	bInit = TRUE;
//    }
//
//    if (!bConfigured)   //if not configured, the Unit number is zero
//    {
//	return 0;
//    }
//    //G3_OPT end
//
//    return GetEnumSigValue(iFuelGroupEquipId,
//	iFuelUnitNumSigType,
//	iFuelUnitNumSigID,
//	"Fuel Get Unit Number");
//}

static SIG_ENUM FuelGetManagementUnitNum(void)
{
	//INT32 iFuelUnitNum = 0;
	INT32 iFuelGroupEquipId = 700;
	INT32 iFuelUnitNumSigID = 1;
	INT32 iFuelUnitNumSigType = 2;

	//G3_OPT [loader], by Lin.Tao.Thomas, 2013-4
	static BOOL bInit = FALSE;
	static BOOL bConfigured = TRUE;

	if (!bInit)
	{
		EQUIP_INFO* pEquipInfo;
		int  nBufLen;

		if (DxiGetData(VAR_A_EQUIP_INFO,
			iFuelGroupEquipId,			
			0,		
			&nBufLen,			
			&pEquipInfo,			
			0) == ERR_DXI_INVALID_EQUIP_ID)
		{
			bConfigured = FALSE;

			//give WARNING msg
			AppLogOut("RS485_PORT", APP_LOG_WARNING, 
				"Fuel equipment not configured in the solution.");
		}

		bInit = TRUE;
	}

	if (!bConfigured)   //if not configured, the Unit number is zero
	{
		return 0;
	}
	//G3_OPT end

	return GetEnumSigValue(iFuelGroupEquipId,
							iFuelUnitNumSigType,
							iFuelUnitNumSigID,
							"Fuel Get Unit Number");
}

static void FuelSetExistAndCommStat(RS485_DEVICE_CLASS*  SmioDeviceClass,
							 ENUMSIGNALPROC EnumProc,
							 LPVOID lpvoid)
{
		INT32		i,iRoughDataPosition,iUnitIdx;
		RS485_VALUE	stTempbExist;
		SIG_ENUM	enumFuelUnitNum;	

		stTempbExist.iValue = SMIO_EQUIP_EXISTENT;


		//Fuel Group existence status!!!
		EnumProc(4040 + 17,			//100		Work status
				stTempbExist.fValue,
				lpvoid );


		/* ��OB�������λ��������λ��ͨѶ״̬�ݲ�����  LKF
		//Stuff Fuel Group communication status!!
		i = 1;//SMIO2
		iRoughDataPosition  = i * SMIO_MAX_SIGNALS_NUM;
		EnumProc(4040 + 16,			//99		Fuel Surveillance Failure
			SmioDeviceClass->pRoughData[iRoughDataPosition + SMIO_COMM_STATUS].fValue,
			lpvoid );
		*/
		
		//Get Fuel Unit Number!!
		enumFuelUnitNum = FuelGetManagementUnitNum();
		
		//Set Fuel unit equipment existence status and communication status!!
		for (i = 0; ((i < enumFuelUnitNum) && (enumFuelUnitNum <= 5)); i++)
		{
			EnumProc(4040 + 19 + i,			//100  Unit Exist status!!!
				stTempbExist.fValue,
				lpvoid );
		}
		
		stTempbExist.iValue = SMIO_EQUIP_NOT_EXISTENT;

		for (iUnitIdx = i; iUnitIdx < 5; iUnitIdx++)
		{
			EnumProc(4040 + 19 + iUnitIdx,			//100  Unit InExist status!!!
				stTempbExist.fValue,
				lpvoid );
		}

}

static void FuelSetAllInExistStat(RS485_DEVICE_CLASS*  SmioDeviceClass,
								 ENUMSIGNALPROC EnumProc,
								 LPVOID lpvoid)
{
	INT32		i = 0;
	RS485_VALUE	stTempbExist;
	//SIG_ENUM	enumFuelUnitNum;	
	UNUSED(SmioDeviceClass);

	stTempbExist.iValue = SMIO_EQUIP_NOT_EXISTENT;

	/* ��OB�������λ��������λ�����״̬�ݲ�����  LKF
	//Fuel Group!!! 
	EnumProc(4040 + 17,			//100		Work status
		stTempbExist.fValue,
		lpvoid );
	*/

	for (i = 0; i < 5; i++)
	{
		EnumProc(4040 + 19 + i,			//100  Unit InExist status!!!
			stTempbExist.fValue,
			lpvoid );
	}
	return;
}

static void FuelGetEquipIsChange(INT32* pBeEquipChange)
{
	static INT32 siOldEquipNum = 0;
	SIG_ENUM	iEquipNum;

	iEquipNum = FuelGetManagementUnitNum();
	
	if (siOldEquipNum != iEquipNum)
	{
		siOldEquipNum = iEquipNum;
		(*pBeEquipChange) = TRUE;
		return;
	}

	(*pBeEquipChange) = FALSE;	

	return;
}

static void FuelEquipStatusChangeProc(void)
{
	static INT32 iBeEquipInfoChange = FALSE;
	static	DWORD	s_dwChangeForWeb= 0;

	if (iBeEquipInfoChange)
	{
		s_dwChangeForWeb =
		    GetDwordSigValue(106,   //SMDU_GROUP_EQUIPID
		    0,   //SIG_TYPE_SAMPLING
		    2+1,  //CFG_CHANGED_SIG_ID
		    "SMIORS485");

		//for Web
		s_dwChangeForWeb++;
		if(s_dwChangeForWeb >= 255)
		{
			s_dwChangeForWeb = 0;
		}

		//printf("\n $$$$$$$$$ s_bDetectDIPChange == TRUE \n");

		SetDwordSigValue(106,	//SMDU_GROUP_EQUIPID
						0,		//SIG_TYPE_SAMPLING
						2 + 1,	//CFG_CHANGED_SIG_ID
						s_dwChangeForWeb,
						"SMIORS485");
		//for	LCD
		SetDwordSigValue(106,	//SMDU_GROUP_EQUIPID
						0,		//SIG_TYPE_SAMPLING
						2,		//CFG_CHANGED_SIG_ID
						1,
						"SMIORS485");

		iBeEquipInfoChange = FALSE;
	}
	else
	{
		FuelGetEquipIsChange(&iBeEquipInfoChange);
	}

	return;
}


/*=============================================================================*
 * FUNCTION: SMIO_StuffChannel
 * PURPOSE : stuff samping data to web or gc  from smio  
 * INPUT: 
 *    
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMIO_StuffChannel(RS485_DEVICE_CLASS*  SmioDeviceClass,
						ENUMSIGNALPROC EnumProc,
						LPVOID lpvoid)
{
	INT32		i;
	INT32		j;
	//INT32		k;
	INT32		iChannelPosition;
	INT32		iRoughDataPosition;
	INT32		iExistentSmioEquipNum;
	RS485_VALUE	stTempbExist;

	int		iFuelGroupCommState = SMIO_COMM_OK;
	int		iFuelGroupExistState = SMIO_EQUIP_EXISTENT;    
	/*
		SMIO_Sig  include  0x41 	0x43	
	*/
	RS485_CHN_TO_ROUGH_DATA  SMIO_Sig[]=
	{

		//iChannel					iRoughData
		{SMIO_CH_A_DINPUT1,			SMIO_A_DINPUT1},
		{SMIO_CH_A_DINPUT2,			SMIO_A_DINPUT2},
		{SMIO_CH_A_DINPUT3,			SMIO_A_DINPUT3},
		{SMIO_CH_A_DINPUT4,			SMIO_A_DINPUT4},
		{SMIO_CH_A_DINPUT5,			SMIO_A_DINPUT5},
		{SMIO_CH_A_DINPUT6,			SMIO_A_DINPUT6},
		{SMIO_CH_A_DINPUT7,			SMIO_A_DINPUT7},
		{SMIO_CH_ANOLOGINPUT1,		SMIO_ANOLOGINPUT1},
		{SMIO_CH_ANOLOGINPUT2,		SMIO_ANOLOGINPUT2},
		{SMIO_CH_ANOLOGINPUT3,		SMIO_ANOLOGINPUT3},
		{SMIO_CH_ANOLOGINPUT4,		SMIO_ANOLOGINPUT4},
		{SMIO_CH_ANOLOGINPUT5,		SMIO_ANOLOGINPUT5},
		{SMIO_CH_FREQUENCYINPUT,	SMIO_FREQUENCYINPUT},
		
		//0x43
		{SMIO_CH_RELAY1_STATUS,		SMIO_RELAY1_STATUS},
		{SMIO_CH_RELAY2_STATUS,		SMIO_RELAY2_STATUS},
		{SMIO_CH_RELAY3_STATUS,		SMIO_RELAY3_STATUS},
		
		{SMIO_CH_COMM_STATUS,		SMIO_COMM_STATUS},
		{SMIO_CH_EXIST_STATUS,		SMIO_WORKING_STATUS},
		{SMIO_CH_WORKING_ADDR,		SMIO_WORKING_ADDR},
		//0x43		cmd

		//add
		{SMIO_CH_END_FLAG,			SMIO_CH_END_FLAG},
		
	};
	

	//SMIO group Sig
	EnumProc(3999,
			SmioDeviceClass->pRoughData[SMIO_G_EXIST].iValue,
			lpvoid );

/*
	2010/12/03
	���Ҫ��SMIO����ǰ�ѷš���SMIO1��SMIO2Ҫ�����⴦��	
*/
	iExistentSmioEquipNum = 0;

	//stuff exist equip sig info	i === [0 --- 7]
	for(i = 0; i <= SMIO_ADDR_END - SMIO_ADDR_START; i++)//103-96 = 7
	{
		iRoughDataPosition  = i * SMIO_MAX_SIGNALS_NUM;

		if(SmioDeviceClass->pRoughData[iRoughDataPosition + SMIO_WORKING_STATUS].iValue 
			== SMIO_EQUIP_EXISTENT)
		{
			j = 0;

			while(SMIO_Sig[j].iChannel != SMIO_CH_END_FLAG)
			{
				/*
				2010/12/03
				���Ҫ��SMIO����ǰ�ѷš���SMIO1��SMIO2Ҫ�����⴦��	
				*/
				//iChannelPosition = iExistentSmioEquipNum * MAX_CHN_NUM_PER_SMIO;
				iChannelPosition = i * MAX_CHN_NUM_PER_SMIO;

				/*OB�п��ܴ�����λ�źţ����Զ���λ���ͨѶ״̬������ LKF*/
				if((i == 1) 
				    && (SMIO_Sig[j].iChannel == SMIO_CH_COMM_STATUS)
				    && (SmioDeviceClass->pRoughData[iRoughDataPosition + SMIO_COMM_STATUS].iValue == SMIO_COMM_FAIL))
				{
				    iFuelGroupCommState = SMIO_COMM_FAIL;
				}
				else
				{
				    EnumProc(iChannelPosition + SMIO_Sig[j].iChannel,
						SmioDeviceClass->pRoughData[iRoughDataPosition + SMIO_Sig[j].iRoughData].fValue,
						lpvoid );
				}

				j++;
			}
			//if  iExistentSmioEquipNum == 8 means all smio equip exist
			iExistentSmioEquipNum ++;//exist equip number. [0--8]
		}
		else	//���Ҫ��SMIO����ǰ�ѷš���SMIO1��SMIO2Ҫ�����⴦��
		{
			//������Σ������ڵĶ���д-9999 �����������ۣ��˴��ٶȺ��ԡ�
			iChannelPosition = i * MAX_CHN_NUM_PER_SMIO;

			stTempbExist.iValue = SMIO_SAMP_INVALID_VALUE;

			j = 0;
			while(SMIO_Sig[j].iChannel != SMIO_CH_END_FLAG)
			{
				/*OB�п��ܴ�����λ�źţ����Զ���λ��Ĵ��ڡ�ͨѶ״̬������  LKF*/
				if((i == 1) 
				   && ((SMIO_Sig[j].iChannel == SMIO_CH_COMM_STATUS)
				    || (SMIO_Sig[j].iChannel == SMIO_CH_EXIST_STATUS)))
				{
				    //
				}
				else
				{
				    EnumProc(iChannelPosition + SMIO_Sig[j].iChannel,
							stTempbExist.fValue,
							lpvoid );
				}

				j++;
			}

			//OB�п��ܴ�����λ�źţ����Զ���λ��Ĵ��ڡ�ͨѶ״̬������   LKF
			if(i == 1) 
			{
			    iFuelGroupExistState = SMIO_EQUIP_NOT_EXISTENT;
			    iFuelGroupCommState = SMIO_COMM_FAIL;	
			}
			else
			{
			    stTempbExist.iValue = SMIO_EQUIP_NOT_EXISTENT;
			    EnumProc(iChannelPosition + SMIO_CH_EXIST_STATUS,
						stTempbExist.fValue,
						lpvoid );	
			}
		}
	}

	
	//stuff group smio
	if(iExistentSmioEquipNum == 0)
	{
			stTempbExist.iValue = SMIO_EQUIP_NOT_EXISTENT;
			EnumProc(SMIO_GROUP_CHANNEL,//3999
					stTempbExist.fValue,
					lpvoid );	
	}
	else
	{
		stTempbExist.iValue = SMIO_EQUIP_EXISTENT;
		EnumProc(SMIO_GROUP_CHANNEL,//3999
				stTempbExist.fValue,
				lpvoid );	
	}

	//SMIO2 ����
	if(FuelIsExist(SmioDeviceClass))
	{
		FuelSetExistAndCommStat(SmioDeviceClass,
								EnumProc,
								lpvoid);

		//�ж��Ƿ���λ��ͨѶ״̬��SMIO2���ڣ�����ͨѶ�ϣ���ʱ��Ҫ��OB���Ƿ����
		stTempbExist.iValue = SMIO_COMM_OK;
		if(iFuelGroupCommState == SMIO_COMM_FAIL)
		{
		    if(g_SiteInfo.MbRoughDataShareForI2C[OB_EXIST_ST].iValue != SMIO_EQUIP_EXISTENT)
		    {
			stTempbExist.iValue = SMIO_COMM_FAIL;
		    }
		}
		EnumProc(4040 + 16,		
		    stTempbExist.fValue,
		    lpvoid );
#if	_RS485_SMIO_DEBUG
		printf(" Fuel Is Exist Comm State= %d \n", stTempbExist.iValue);
#endif
		
	}
	//SMIO2 ������
	else
	{
		FuelSetAllInExistStat(SmioDeviceClass,
							 EnumProc,
							 lpvoid);

		//�ж��Ƿ���λ�����״̬��SMIO2�����ڣ���ʱ��Ҫ��OB���Ƿ����
		stTempbExist.iValue = SMIO_EQUIP_NOT_EXISTENT;
		if(g_SiteInfo.MbRoughDataShareForI2C[OB_EXIST_ST].iValue == SMIO_EQUIP_EXISTENT)
		{
		    stTempbExist.iValue = SMIO_EQUIP_EXISTENT;
		}		
		EnumProc(4040 + 17,		
		    stTempbExist.fValue,
		    lpvoid );

		EnumProc(4040 + 16,		
		    stTempbExist.fValue,
		    lpvoid );
#if	_RS485_SMIO_DEBUG
		printf(" iFuelGroupExistState = %d \n", iFuelGroupExistState);
		printf(" iFuelGroupCommState = %d \n", iFuelGroupCommState);
		printf(" Fuel Is Not Exist  - Exist state = %d \n", stTempbExist.iValue);
#endif
	}

	FuelEquipStatusChangeProc();




	/*
	2010/12/03
	���Ҫ��SMIO����ǰ�ѷš���SMIO1��SMIO2Ҫ�����⴦��	
	*/
	stTempbExist.iValue = SMIO_EQUIP_NOT_EXISTENT;
	//Stuff not exist equip sig info		iExistentSmioEquipNum: [0--8]
	//if iExistentSmioEquipNum < 8 means  have any smio inexistence
	//if(iExistentSmioEquipNum < (SMIO_ADDR_END - SMIO_ADDR_START + 1)) 
	//{	
	//	for(k = iExistentSmioEquipNum; k <= SMIO_ADDR_END - SMIO_ADDR_START; k++)
	//	{
	//		j = 0;
	//		while(SMIO_Sig[j].iChannel != SMIO_CH_END_FLAG)
	//		{
	//			iChannelPosition = k * MAX_CHN_NUM_PER_SMIO;

	//			EnumProc(iChannelPosition + SMIO_Sig[j].iChannel,
	//				SmioDeviceClass->pRoughData[iRoughDataPosition + SMIO_Sig[j].iRoughData].fValue,
	//				lpvoid );
	//			j++;
	//		}

	//		iChannelPosition = k * MAX_CHN_NUM_PER_SMIO;
	//		EnumProc(iChannelPosition + SMIO_CH_EXIST_STATUS,
	//					stTempbExist.fValue,
	//					lpvoid );	
	//	}
	//}

	return 0;
}
/*=============================================================================*
* FUNCTION: SMIO_UnpackPrdctInfoAndBarCode
* PURPOSE :  Arrange sampling data by 0xEC And by 0x51 to  struct st_RecordProdcInfo
* INPUT: 
*     
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL void SMIO_UnpackPrdctInfoAndBarCode(PRODUCT_INFO * pInfo,BYTE *ReceiveFrame,BYTE *ReceiveFrameTwo)
{
	int i, iLen;
	BYTE szLenInfo[4] = {0};
	BYTE byMainVer, bySubVer;
	if (ReceiveFrame[7] == '0' && ReceiveFrame[8] == '4')
	{
		TRACE("Old module, not support product info!\n");
		pInfo->bSigModelUsed = FALSE;
		pInfo->szHWVersion[0] = 0;
		pInfo->szPartNumber[0] = 0;
		pInfo->szSerialNumber[0] = 0;
		pInfo->szSWVersion[0] = 0;
		return ;
	}
	//1.get length
	for (i = 0 ; i < 3; i++)
	{
		szLenInfo[i] = ReceiveFrame[10 + i];
	}
	sscanf((char*)szLenInfo, "%03x", (unsigned int *)(&iLen));
	//1.Serial number process
	for (i = 0; i < 11; i++)
	{
		if((SMIOMergeAsc((char*)(ReceiveFrame + 13 + i * 2))>=48 && SMIOMergeAsc((char*)(ReceiveFrame + 13 + i * 2))<=57)||
			(SMIOMergeAsc((char*)(ReceiveFrame + 13 + i * 2))>=65 && SMIOMergeAsc((char*)(ReceiveFrame + 13 + i * 2))<=90)||
			(SMIOMergeAsc((char*)(ReceiveFrame + 13 + i * 2))>=97 && SMIOMergeAsc((char*)(ReceiveFrame + 13 + i * 2))<=122))
		{
			pInfo->szSerialNumber[i] = SMIOMergeAsc((char*)(ReceiveFrame + 13 + i * 2));

		}
		else
		{
			pInfo->szSerialNumber[i] = ' ';
		}

	}

	//2.HW version process - the last three character
	//printf("\n PartNumber process = %s\n", (char *)Frame+13+iLen-6);
	for (i = 0; i < 3; i++)
	{
		if((SMIOMergeAsc((char*)(ReceiveFrame + 13 + iLen - 6 + i * 2))>=48 && SMIOMergeAsc((char*)(ReceiveFrame + 13 + iLen - 6 + i * 2))<=57)||
			(SMIOMergeAsc((char*)(ReceiveFrame + 13 + iLen - 6 + i * 2))>=65 && SMIOMergeAsc((char*)(ReceiveFrame + 13 + iLen - 6 + i * 2))<=90)||
			(SMIOMergeAsc((char*)(ReceiveFrame + 13 + iLen - 6 + i * 2))>=97 && SMIOMergeAsc((char*)(ReceiveFrame + 13 + iLen - 6 + i * 2))<=122))
		{
			pInfo->szHWVersion[i] = SMIOMergeAsc((char*)(ReceiveFrame + 13 + iLen - 6 + i * 2));
		}
		else
		{
			pInfo->szHWVersion[i] = ' ';

		}
	}

	//3.PartNumber process
	//printf("\n PartNumber process = %s\n", (char *)Frame+13+22);
	if ((iLen/2 - 14) > 16)
	{
		for (i = 0; i < 16; i++)
		{
			if((SMIOMergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)>=48 && SMIOMergeAsc((char*)(ReceiveFrame + 13 + 11 * 2 + i * 2))<=57)||
				(SMIOMergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)>=65 && SMIOMergeAsc((char*)(ReceiveFrame + 13 + 11 * 2 + i * 2))<=90)||
				(SMIOMergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)>=97 && SMIOMergeAsc((char*)(ReceiveFrame + 13 + 11 * 2 + i * 2))<=122))
			{
				pInfo->szPartNumber[i] = SMIOMergeAsc((char*)(ReceiveFrame + 13 + 11 * 2 + i * 2));

			}
			else
			{
				pInfo->szPartNumber[i] = ' ';
			}

		}
	}
	else
	{
		for (i = 0; i < iLen/2-14; i++)
		{
			if((SMIOMergeAsc((char*)(ReceiveFrame + 13 + 11 * 2 + i * 2))>=48 && SMIOMergeAsc((char*)(ReceiveFrame + 13 + 11 * 2 + i * 2))<=57)||
				(SMIOMergeAsc((char*)(ReceiveFrame + 13 + 11 * 2 + i * 2))>=65 && SMIOMergeAsc((char*)(ReceiveFrame + 13 + 11 * 2 + i * 2))<=90)||
				(SMIOMergeAsc((char*)(ReceiveFrame + 13 + 11 * 2 + i * 2))>=97 && SMIOMergeAsc((char*)(ReceiveFrame + 13 + 11 * 2 + i * 2))<=122))
			{
				pInfo->szPartNumber[i] = SMIOMergeAsc((char*)(ReceiveFrame + 13 + 11 * 2 + i * 2));

			}
			else
			{
				pInfo->szPartNumber[i] = ' ';
			}

		}
	}

	//4.SW version process -- using another frame
	byMainVer = SMIOMergeAsc((char*)(ReceiveFrameTwo + 40 + 13));
	bySubVer  = SMIOMergeAsc((char*)(ReceiveFrameTwo + 42 + 13));
	int iTempMainVer,iTempSubVer;
	iTempMainVer = byMainVer;
	iTempSubVer = bySubVer;

	if (iTempMainVer >= 0 && iTempMainVer <= 16 &&
		iTempSubVer >= 0 && iTempSubVer <= 16)
	{
		sprintf(pInfo->szSWVersion, "%d.%02d\0", byMainVer, bySubVer);
	}
	TRACE("SMIO BBU PartNumber:%s\n", pInfo->szPartNumber);
	TRACE("SMIO BBU SW version:%s\n", pInfo->szSWVersion);
	TRACE("SMIO BBU HW version:%s\n", pInfo->szHWVersion);
	TRACE("SMIO BBU Serial Number:%s\n", pInfo->szSerialNumber);
	return ;
}
/*=============================================================================*
* FUNCTION: SMIO_GetProdctInfo
* PURPOSE : Get SMIO Product Info 
* INPUT: 
*     
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void SMIO_GetProdctInfo(HANDLE hComm, int nUnitNo, void *pPI)
{
	UNUSED(hComm);
	UNUSED(nUnitNo);
	PRODUCT_INFO*		pInfo;
	INT32				i,k;
	INT32				iAddr;
	INT32				iSmioStat;
	INT32				iRoughDataPosition;
	char				szVer[3] = "20";
	BYTE				szSendStr[40] = {0};
	BYTE				ReceiveFrame[MAX_RECVBUFFER_NUM_SMIO] = {0}; 
	BYTE				ReceiveFrameTwo[MAX_RECVBUFFER_NUM_SMIO] ={0};

	pInfo			 = (PRODUCT_INFO *)pPI;
	pInfo			+= SMIO1_EID;
	RS485_DEVICE_CLASS*		pSmioDeviceClass	= g_RS485Data.pDeviceClass + DEVICE_CLASS_SMIO;
	
	if (pSmioDeviceClass->pRoughData == NULL)
	{
		TRACE(" G	BBU  GetProductInfo IN  SLAVE Mode  :  \n");	
		return;
	}

	if(SMIONeedCommProc())
	{
	    SMIOReopenPort();
	}

	//for(iAddr = SMIO_ADDR_START; iAddr <= SMIO_ADDR_END; iAddr++,pInfo++)
	for(iAddr = SMIO_ADDR_START; iAddr <= SMIO_ADDR_END; iAddr++)
	{
		iRoughDataPosition = (iAddr - SMIO_ADDR_START) * SMIO_MAX_SIGNALS_NUM;

		if(pSmioDeviceClass->pRoughData[iRoughDataPosition + SMIO_WORKING_STATUS].iValue 
			!= SMIO_EQUIP_EXISTENT)
		{
			/*
				2010/12/09����ȷ�ϲ���ǰ�ѷţ���
			*/
			pInfo->bSigModelUsed = FALSE;
			pInfo++;
			continue;
		}

		Sleep(100);

		//Get ENP BarCode
		sprintf((char *)szSendStr, 
			"~%s%02X%02X%02X%04X\0",szVer, (unsigned int)iAddr, SMIO_CID1_0XD1, 0xEC, 0);
		CheckSum(szSendStr);

		for (i = 0; i < 5; i++)
		{
			iSmioStat = SMIOSendAndRead(g_RS485Data.CommPort.hCommPort,
										szSendStr,
										ReceiveFrame,
										18,
										RS485_WAIT_LONG_TIME,
										4096);
			Sleep(100);

			if (RESPONSE_OK == iSmioStat)
			{
				pInfo->bSigModelUsed = TRUE;
				Sleep(30);
				//Sampling Product Info by 0x51
				sprintf((char *)szSendStr, 
					"~%s%02X%02X%02X%04X\0",szVer, (unsigned int)iAddr, SMIO_CID1_0XD1, 0x51, 0);
				CheckSum(szSendStr);

				for (k = 0; k < 5;k++)
				{
					iSmioStat = SMIOSendAndRead(g_RS485Data.CommPort.hCommPort,
											szSendStr,
											ReceiveFrameTwo,
											18,
											RS485_WAIT_LONG_TIME,
											4096);
					if (RESPONSE_OK == iSmioStat)
					{
						break;
					}
					else
					{
						//k++
					}
				}

				SMIO_UnpackPrdctInfoAndBarCode(pInfo, ReceiveFrame, ReceiveFrameTwo);
				break;
			}
			else
			{
				//i++
			}
		} 

		/*
			2010/12/09
			����ȷ�ϲ���ǰ�ѷ��ˣ������������һ�¡�
		*/
		pInfo++;//�����е����ﶼ�Ǵ��ڵ�
	}

	if (g_RS485Data.CommPort.bOpened)
	{
		RS485_Close(g_RS485Data.CommPort.hCommPort);
		g_RS485Data.CommPort.bOpened = FALSE;
		g_RS485Data.CommPort.enumAttr = 0;
		g_RS485Data.CommPort.enumBaud = 0;
	}

}

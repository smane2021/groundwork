/*============================================================================*
*         Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                          ALL RIGHTS RESERVED
*
*  PRODUCT  : Unified Data Type Definition
*
*  FILENAME : rs485_report.h
*  PURPOSE  : Define base data type.
*  
*  HISTORY  :
*    DATE            VERSION        AUTHOR            NOTE
*    2007-07-12      V1.0           IlockTeng         Created.    
*    2009-04-14     
*                                                     
*-----------------------------------------------------------------------------
*  GLOBAL VARIABLES
*    NAME                                    DESCRIPTION
*          
*      
*-----------------------------------------------------------------------------
*  GLOBAL FUNCTIONS
*    NAME                                    DESCRIPTION
*      
*    
*============================================================================*/



#ifndef __RS485_REPORT_MAIN_H
#define __RS485_REPORT_MAIN_H

#include <stdio.h>
#include "basetypes.h"
#include "public.h"
#include "err_code.h"
#include "pubfunc.h"
#include "string.h"

#define	 RS485_SAMP_INVALID_VALUE			(-9999)
#define	 CONFIG_FILE_SLAVE_REPORT	  "private/mstslvmode/slave_report.cfg"
#define	 RS485_REPORT_SIG_NUM								  255
#define  SYS_GROUP_DATA_MAP_NUM_KEY				"[SYS_GROUP_DATA_MAP_NUM]"
#define  SYS_GROUP_DATA_MAP_INFO_KEY			"[SYS_GROUP_DATA_MAP_INFO]"
#define  CTRL_DATA_MAP_NUM_KEY					"[CTRL_DATA_MAP_NUM]"
#define  CTRL_DATA_MAP_INFO_KEY					"[CTRL_DATA_MAP_INFO]"
#define  RECT_SINGLE_INFO_MAP_NUM_KEY			"[RECT_SINGLE_INFO_MAP_NUM]"
#define  RECT_SINGLE_MAP_INFO_KEY				"[RECT_SINGLE_MAP_INFO]"
#define	SLAVE_DXIGETSIG_SUCCESSFUL				0
#define SLAVE_ADDR_201_ENUM_0					0
#define SLAVE_ADDR_202_ENUM_1					1
#define SLAVE_ADDR_203_ENUM_2					2
#define SLAVE_ADDR_OFFSET_START					201
#define SLAVE_ADDR_ENUM_0_NUMBER_201			201
#define SLAVE_ADDR_ENUM_1_NUMBER_202			202
#define SLAVE_ADDR_ENUM_2_NUMBER_203			203
#define SLAVE_MAX_RECTIFIER_NUM					60
#define SLAVE_RECEIVE_BUF_MAX_NUM				4902
#define SLAVE_SEND_BUF_MAX_NUM					4902
#define SLAVE_MODULE_INFO_CHANGE_SIGID			37
#define RECT_GROUP_START						2
#define SLAVE_MODULE_INFO_CHANGE_SIG_TYPE		2
#define BATTERIER_GROUP							115
#define EQUIP_ID_RECT_G							2
#define SIG_TYPE_SETING							2
#define SIG_ID_SLV_CHANGE						37

#define BATTERIER_MANAGEMENT_STAT_SIG_ID		25
#define BATTERIER_SIG_TYPE						0
#define MAX_MUTEX_TIME_WAITING_TIME_OUT			100
#define ID_TIME_RPT								0

//control command sequence must synchronization
#define	CMD_DC_ON_OFF_CONTROL			0							//DC On/Off Control
#define	CMD_AC_ON_OFF_CONTROL			1							//AC On/Off Control
#define	CMD_LED_CONTROL					2							//AC On/Off Control
#define	CMD_DC_VOLTAGE_CONTROL			3							//DC voltage control
#define	CMD_DC_CURRENT_LIMIT_CONTROL	4							//Current limit control
#define	CMD_ALL_AC_ON_OFF_CONTROL		5							//All AC On/Off Control
#define	CMD_ALL_DC_ON_OFF_CONTROL		6							//All DC On/Off Control
#define	CMD_ALL_LED_CONTROL				7							//All LED Control	
#define	CMD_RESET_LOST_ALARM			8							//Reset rectifier lost alarm
#define	CMD_RESET_OSCILLATED_ALARM		9							//Reset Redundency Oscillated
#define	CMD_RESET_RECTIFIER				10							//Rectifier Reset
#define CMD_SET_AC_CURR_LIMIT			11
#define CMD_CTL_E_STOP					12
#define CMD_CLS_COMM_INTERR		13
#define CMD_FAN_SPEED			14
#define CMD_CONFIRM_POSITION			15
#define CMD_RECT_UNIT_POSITION		16
#define CMD_RECT_UNIT_PHASE		17
#define CMD_RESET_POSITION			18

#define SPLITTER		 ('\t')
#define bySOI           0x7E				//START OF INFORMATION
#define byEOI           0x0D				//END OF INFORMATION

#define SLAVECID1H            0X45			// EQUIP TYPE DESCRIPTION
#define SLAVECID1L            0X35			// EQUIP TYPE DESCRIPTION
#define SLAVECID1			  0xe5
union STRTOFLOAT
{
	float f_value;					// float
	BYTE  by_value[5];				// uchar
};

struct	Report_Signales_Info
{
	USHORT			Equip_Id;
	USHORT			Sig_Type;
	USHORT			Sig_Id;
	USHORT			ByteNum_Of_Sig;
};
typedef  struct	Report_Signales_Info	RS485_SLAVE_REPORT_SIG_INFO;



struct	Tag_Report_Signales_Info
{
	int		iAnologSigMapNum;
	RS485_SLAVE_REPORT_SIG_INFO		*pAnologSigMapInfo;	
	int		iCtrlSigMapNum;
	RS485_SLAVE_REPORT_SIG_INFO		*pCtrlSigMapInfo;
	int iRectSigMapNum;
	RS485_SLAVE_REPORT_SIG_INFO		*pRectSigMapInfo;
};
typedef struct Tag_Report_Signales_Info		RS485_REPORT_CFG_INFO;


struct Tag_Slave_Globals
{
	RS485_REPORT_CFG_INFO		Rs485_Slave_Cfg;
	//add
	BYTE RecData[SLAVE_RECEIVE_BUF_MAX_NUM];
	CHAR SendDataInfo[500];
	BYTE SendE1DataInfo[SLAVE_SEND_BUF_MAX_NUM];
	BYTE SendE3DataInfo[SLAVE_SEND_BUF_MAX_NUM];
	int iE1Totalbyte;
	int iE3Totalbyte;
	int Totalbyte;
	int iReceiveByteNum;
	//int  iRecordReturnFalseTimes;
	float fFloatChargeVoltageSigVelue;
	BOOL bConnectionToMaster;
	CHAR SlaveAddress;
	//BOOL SlaveModuleInfoChange;
};
typedef struct Tag_Slave_Globals	 SLAVE_GLOBALS_REPORT;

struct  _Rpt_CmdE1E3_response 
{
	BOOL	 bE1E3RPTRunning;
	INT32	 iSendLengh;
	INT32	 iPackDataFlagOne;
	INT32	 iPackDataFlagTwo;
	INT32	 iPackDataLenghOne;
	INT32	 iPackDataLenghTwo;
	HANDLE	 iMutexOneDataSyncLock;
	HANDLE	 iMutexE1TwoDataSyncLock;
	BYTE	 SendE1DataOne[SLAVE_SEND_BUF_MAX_NUM];
	BYTE	 SendE1DataTwo[SLAVE_SEND_BUF_MAX_NUM];
	HANDLE	 iMutexE3TwoDataSyncLock;
	INT32	 iPackE3DataLenghOne;
	INT32	 iPackE3DataLenghTwo;
	BYTE	 SendE3DataOne[SLAVE_SEND_BUF_MAX_NUM];
	BYTE	 SendE3DataTwo[SLAVE_SEND_BUF_MAX_NUM];
};
typedef struct _Rpt_CmdE1E3_response	 RPT_CMDE1E3_DATA;

enum YDN23PROTOCOLINFO_REPORT
{
	CHECKSUM_ERROR_YDN23_REPORT = 2,
	ADDR_YDN23_REPORT = 3,

	CID1H_YDN23_REPORT = 5,
	CID1L_YDN23_REPORT = 6,
	CID2_YDN23_REPORT	= 7,
	DATA_YDN23_REPORT = 13,
	//others will add here
};

//Following  all sig From StdEquip_Rectifier.cfg Value Type  is  Dword U
//enum  RECT_SIG_VALUE_DWORD_IDX
//{
//	RECT_POS_NO = 34,					//Rectifier Position No
//	RECT_SN		= 8,					//Rectifier SN
//	RECT_TOTAL_RUN_TIME = 9,			//Total Running Time
//	RECT_HIGH_SN = 26,					//Rectifier High SN
//	RECT_VERSION =27,					//Rectifier Version
//	RECT_BARCODE_1 = 37,				//Barcode1
//	RECT_BARCODE_2 = 38,				//Barcode2
//	RECT_BARCODE_3 = 39,				//Barcode3
//	RECT_BARCODE_4 = 40,				//Barcode4
//};

union _REPORTSTRTOFLOAT
{
	float f_value;       // ������
	BYTE  by_value[4];   // �ַ���
};
typedef union _REPORTSTRTOFLOAT	REPORTSTRTOFLOAT;
//#define	RS485_READ_WRITE_TIMEOUT	400	//ms
//static int iCtrlTimes = 0;

DWORD	RS485_RunAsSlave(void);
void SLAVE_GetProdctInfo(HANDLE hComm, int nUnitNo, void *pPI);

#endif






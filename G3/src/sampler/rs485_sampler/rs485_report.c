/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : rs485_report.c
*  CREATOR  : Ilock teng                DATE: 2008-06-10 17:04
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <sys/ioctl.h>
#include <termios.h>
#include "rs485_report.h"											
#include "public.h"										
#include "basetypes.h"
#include "rs485_main.h"
#include "rs485_comm.h"
static	INT32	siSlaveRectChage = 0;
static  RPT_CMDE1E3_DATA  st_RptE1E3Data;
//Record Slave equipment  Address from web	
INT32						g_SLAVEAddr;
SLAVE_GLOBALS_REPORT		g_Slave_Report;
extern 	RS485_SAMPLER_DATA	g_RS485Data;
LOCAL BYTE RPTLenCheckSum(WORD wLen);
/*=============================================================================*
* FUNCTION: RecvDataFromMASTER()
* PURPOSE  :Receive data of SM from 485 com
* RETURN   : int : byte number ,but if return -1  means  error
* ARGUMENTS:
*						CHAR*	sRecStr :		
*								hComm	 :	485 com handle 
*								iStrLen	 :
* CALLS    : 
* CALLED BY: 
*								DLLExport BOOL Query()
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
LOCAL INT32	RecvDataFromMASTER(HANDLE hComm, CHAR* sRecStr,  INT32 *piStrLen)
{
	INT32	iReadLen;		
	INT32	i		= 0;
	INT32	j		= 0;
	INT32	iHead	= -1;
	INT32	iTail	= -1;

	ASSERT(hComm != 0 );
	ASSERT(sRecStr != NULL);

	//Read the data from Smac
	iReadLen = RS485_ReadForSlave(hComm, (BYTE*)sRecStr, SLAVE_RECEIVE_BUF_MAX_NUM);
	//iReadLen   = read(hComm,sRecStr,SLAVE_RECEIVE_BUF_MAX_NUM);

	for (i = 0; i < iReadLen; i++)
	{
		if(bySOI == *(sRecStr + i))
		{
			iHead = i;
			break;
		}
	}

	if (iHead < 0)	//   no head
	{
		return FALSE;
	}

	for (i = iHead + 1; i < iReadLen; i++)
	{
		if(byEOI == *(sRecStr + i))
		{
			iTail = i;
			break;
		}
	}		

	if (iTail < 0)	//   no tail
	{
		return FALSE;
	}	

	*piStrLen = iTail - iHead + 1;

	if(iHead > 0)
	{
		for (j = iHead; j < *piStrLen; j++)
		{
			*(sRecStr + (j - iHead)) = *(sRecStr + j);
		}
	}

	*(sRecStr + (iTail - iHead + 2)) = '\0';
	return TRUE;

}
/*=============================================================================*
* FUNCTION: RecvDataFromMasterOrSlave()
* PURPOSE  :Receive data of SM from 485 com
* RETURN   : int : byte number ,but if return -1  means  error
* ARGUMENTS:
*								CHAR*	sRecStr :		
*								hComm	 :	485 com handle 
*								iStrLen	 :
* CALLS    : 
* CALLED BY: 
*								DLLExport BOOL Query()
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
static INT32 RecvDataFromMasterOrSlave(HANDLE hComm,
				       CHAR* sRecStr, 
				       INT32 iStrLen)
{
	INT32 i = 0;				//record accurate data's number
	INT32 j = 0;				//control the loop,avoid infinite loop
	ASSERT( sRecStr != NULL);
	UNUSED(iStrLen);

	//found		first	byte,if error return -1
	do
	{	
		if(RS485_Read(hComm, (BYTE*)sRecStr, 1) > 0)	
		{
			i++; 
		}
		else
		{
			j++;
			//delay 1	ms,continue
			Sleep(1);
		}
	}
	while(*(sRecStr + 0) != bySOI && (i < 99) && (j < 99));

	if(j > 99)
	{
		//Flush buffer
		//delay 1	ms,Continue to receive data in the next cycle
		Sleep(1);
		return -1;
	}

	//point to second	byte
	sRecStr++;
	i = 0;
	j = 0;	

	if(*(sRecStr - 1) == bySOI)
	{	
		//Until received  complete a data,if error return -1
		do
		{
			if(RS485_Read(hComm,(BYTE*)sRecStr,1) > 0)
			{
				i++; 
				sRecStr++;//note: "++"mustn't overflow
			}
			else
			{
				j++;
				//delay 1	ms,Continue
				Sleep(1);
			}				
		}
		while(*(sRecStr-1) != byEOI && (i< (SLAVE_RECEIVE_BUF_MAX_NUM - 1) )&& (j < 99 ));	
	}

	if(j > 99)
	{
		Sleep(1);
		//Continue to receive data in the next cycle
		return -1;
	}

	return (i+1);//REC BYTE NUM
}
/*=============================================================================*
* FUNCTION: SLAVE CheckConfig
* PURPOSE  :check in the configuration file signal,
*			 if mistake, record system log
* RETURN   : 0: succeed
* ARGUMENTS:
*			keyflag : 1 means get the signals from gc  
*					  2 means set a control signal to the  Rectifier
* CALLS    : 
* CALLED BY: 
*							LOCAL	VOID	LoadRs485 Report_Cfg_Proc
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-06		08:39
*==========================================================================*/
LOCAL	INT32 RS485RPT_CheckConfig(INT32 keyflag,
				   INT32 Equip_Id,
				   INT32 Sig_Type,
				   INT32 Sig_Id)
{
	SIG_BASIC_VALUE*	pSigValue;
	INT32				iBufLen;
	INT32				nError;
	VAR_VALUE_EX		sigValue;

	if(keyflag == 1)
	{
		nError = DxiGetData(VAR_A_SIGNAL_VALUE,
			Equip_Id,			
			DXI_MERGE_SIG_ID(Sig_Type,Sig_Id),		
			(int*)&iBufLen,			
			&pSigValue,			
			0);	

		if(nError != ERR_OK)
		{
			AppLogOut("SLAVE CheckConfig", 
				APP_LOG_ERROR, 
				"CheckConfig 11error,Sys restart!!!, EquipId = %d, SigType = %d, SigId = %d!\n", 
				Equip_Id,
				Sig_Type,
				Sig_Id); 

			//Main_ExitAbnormally(SYS_EXIT_BY_THREAD);	// will not return. 
		}	
	}
	else if(keyflag == 2)
	{
		nError = DxiSetData(VAR_A_SIGNAL_VALUE,
			Equip_Id,
			DXI_MERGE_SIG_ID(Sig_Type, Sig_Id),
			sizeof(VAR_VALUE_EX),
			&sigValue,
			0);	

		if(nError != ERR_OK)
		{
			AppLogOut("SLAVEC heckConfig", 
				APP_LOG_ERROR, 
				"CheckConfig 22error,Sys restart!!, EquipId = %d, SigType = %d, SigId = %d!\n", 
				Equip_Id,
				Sig_Type,
				Sig_Id); 

			//Main_ExitAbnormally(SYS_EXIT_BY_THREAD);	// will not return.//return
		}	
	}

	return 0;
}
/*=============================================================================*
* FUNCTION: RS485RPT_LoadGroupSysSigTableProc
* PURPOSE  :one  by one  parse data of a row	to	pStructData
* RETURN   : int : error code defined in the err_code.h , 0 for success
* ARGUMENTS:
*								szBuf   :	data		of		a	row
*								pStructData:	out ,loaded buffer    
* CALLS    : 
* CALLED BY: 
*							LOCAL	VOID	LoadRs485 Report_Cfg_Proc
*											(void *pCfg, void *pLoadToBuf)
* COMMENTS : [GET_DATA_MAP_INFO]
* CREATOR  : Ilock teng        DATE: 2009-05-06		08:39
*==========================================================================*/
LOCAL INT32 RS485RPT_LoadGroupSysSigTableProc(CHAR *szBuf,
					      RS485_SLAVE_REPORT_SIG_INFO *pStructData)
{
	USHORT	*uspField;
	ASSERT(szBuf);
	ASSERT(pStructData);		
	/* 1.jump signal name field,	unused */
	szBuf = Cfg_SplitStringEx(szBuf, &uspField, SPLITTER);
	/* 2.jump Equip Id	 field */
	szBuf = Cfg_SplitStringEx(szBuf, &uspField, SPLITTER);

	if (Cfg_IsEmptyField((char*)uspField))	//there is any NA
	{
		pStructData->Equip_Id = '\0';
	}
	else
	{
		pStructData->Equip_Id = atoi((char*)uspField);
	}

	/* 3.jump signal type	 field */
	szBuf = Cfg_SplitStringEx(szBuf, &uspField, SPLITTER);

	if (Cfg_IsEmptyField((char*)uspField))	//there is any NA
	{
		pStructData->Sig_Type = '\0';
	}
	else
	{
		pStructData->Sig_Type = atoi((char*)uspField);
	}

	/* 4.jump signal Id	 field */
	szBuf = Cfg_SplitStringEx(szBuf, &uspField, SPLITTER);

	if (Cfg_IsEmptyField((char*)uspField))	//there is any NA
	{
		pStructData->Sig_Id = '\0';
	}
	else
	{
		pStructData->Sig_Id = atoi((char*)uspField);
	}

	/* 5.jump ByteNum of Sig	 field */
	szBuf = Cfg_SplitStringEx(szBuf, &uspField, SPLITTER);
	pStructData->ByteNum_Of_Sig = atoi((char*)uspField);

	//check Config
	if(pStructData->Equip_Id != '\0' && pStructData->Sig_Id != '\0'\
		&&pStructData->Sig_Type != '\0')
	{
		RS485RPT_CheckConfig(1,pStructData->Equip_Id,pStructData->Sig_Type,pStructData->Sig_Id);
	}

	return   ERR_CFG_OK;
}

/*=============================================================================*
* FUNCTION: RS485RPT_LoadCtrlSigTableProc
* PURPOSE  :one  by one  parse data of a row to pStructData
* RETURN   : int : error code defined in the err_code.h , 0 for success
* ARGUMENTS:
*						szBuf   :	data		of		a	row
*						pStructData:	out ,loaded buffer    
* CALLS    : 
* CALLED BY: 
*							LOCAL	VOID	LoadRs485 Report_Cfg_Proc
*											(void *pCfg, void *pLoadToBuf)
* COMMENTS : [SET_DATA_MAP_INFO]
* CREATOR  : Ilock teng        DATE: 2009-05-06		08:39
*==========================================================================*/
LOCAL INT32 RS485RPT_LoadCtrlSigTableProc(CHAR *szBuf,
					  RS485_SLAVE_REPORT_SIG_INFO *pStructData)
{	
	USHORT	*uspField;
	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.jump signale name field,	unused */
	szBuf = Cfg_SplitStringEx(szBuf, &uspField, SPLITTER);

	/* 2.jump Equip Id	 field */
	szBuf = Cfg_SplitStringEx(szBuf, &uspField, SPLITTER);
	//pStructData->Equip_Id = atoi(uspField);

	if (Cfg_IsEmptyField((char*)uspField))	//there is any NA
	{
		pStructData->Equip_Id = '\0';
	}
	else
	{
		pStructData->Equip_Id = atoi((char*)uspField);
	}

	/* 3.jump signale type	 field */
	szBuf = Cfg_SplitStringEx(szBuf, &uspField, SPLITTER);
	pStructData->Sig_Type = atoi((char*)uspField);

	/* 4.jump signale Id	 field */
	szBuf = Cfg_SplitStringEx(szBuf, &uspField, SPLITTER);
	pStructData->Sig_Id = atoi((char*)uspField);

	/* 5.jump ByteNum of Sig	 field */
	szBuf = Cfg_SplitStringEx(szBuf, &uspField, SPLITTER);
	pStructData->ByteNum_Of_Sig = atoi((char*)uspField);

	//RS485RPT_CheckConfig(2,pStructData->Equip_Id,pStructData->Sig_Type,pStructData->Sig_Id);
	return   ERR_CFG_OK;
}
/*=============================================================================*
* FUNCTION: RS485RPT_LoadRectSigTableProc
* PURPOSE  : one  by one  parse data of a row	to	pStructData
* RETURN   : int : error code defined in the err_code.h , 0 for success
* ARGUMENTS:
*								szBuf   :	data		of		a	row
*								pStructData: out ,loaded buffer    
* CALLS    : 
* CALLED BY: 
*							LOCAL	VOID	LoadRs485 Report_Cfg_Proc
*											(void *pCfg, void *pLoadToBuf)
* COMMENTS : [SET_DATA_MAP_INFO]
* CREATOR  : Ilock teng        DATE: 2009-05-06		08:39
*==========================================================================*/
LOCAL INT32 RS485RPT_LoadRectSigTableProc(CHAR *szBuf,
					  RS485_SLAVE_REPORT_SIG_INFO *pStructData)
{			
	USHORT	*uspField;
	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.jump signale name field,	unused */
	szBuf = Cfg_SplitStringEx(szBuf, &uspField, SPLITTER);

	/* 2.jump Equip Id	 field */
	szBuf = Cfg_SplitStringEx(szBuf, &uspField, SPLITTER);
	//pStructData->Equip_Id = atoi(uspField);
	if (Cfg_IsEmptyField((char*)uspField))	//there is any NA
	{
		pStructData->Equip_Id = '\0';
	}
	else
	{
		pStructData->Equip_Id = atoi(uspField);
	}

	/* 3.jump signale type	 field */
	szBuf = Cfg_SplitStringEx(szBuf, &uspField, SPLITTER);
	pStructData->Sig_Type = atoi(uspField);

	/* 4.jump signale Id	 field */
	szBuf = Cfg_SplitStringEx(szBuf, &uspField, SPLITTER);
	pStructData->Sig_Id = atoi(uspField);
	if (Cfg_IsEmptyField((char*)uspField))	//there is any NA
	{
		pStructData->Sig_Id = '\0';
	}
	else
	{
		pStructData->Sig_Id = atoi((char*)uspField);
	}

	/* 5.jump ByteNum of Sig	 field */
	szBuf = Cfg_SplitStringEx(szBuf, &uspField, SPLITTER);
	pStructData->ByteNum_Of_Sig = atoi(uspField);

	if(pStructData->Equip_Id != '\0')
	{
		RS485RPT_CheckConfig(1,3,pStructData->Sig_Type,pStructData->Sig_Id);
	}

	return   ERR_CFG_OK;
}
/*=============================================================================*
* FUNCTION: LoadRs485Report_Cfg_Proc
* PURPOSE  :call	back funtion	 by  LoadRS485 SlaveModeConfig	 
*			 for in slave mode load report signale  info
* RETURN   : void
* ARGUMENTS:
*			pCfg   : contents of	 config
*			pLoadToBuf:	buffer    which loaded
* CALLS    : 
* CALLED BY: 
*							DLLExport BOOL Query()
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-06		08:39
*==========================================================================*/
LOCAL VOID LoadRs485Report_Cfg_Proc(VOID *pCfg, VOID *pLoadToBuf)
{
	RS485_REPORT_CFG_INFO	*pBuf;
	CONFIG_TABLE_LOADER			loader[RS485_REPORT_SIG_NUM];
	ASSERT(pCfg);
	ASSERT(pLoadToBuf);			
	pBuf = (RS485_REPORT_CFG_INFO *)pLoadToBuf;
	/* load type map table first */
	DEF_LOADER_ITEM(&loader[0],
		SYS_GROUP_DATA_MAP_NUM_KEY, 
		&(pBuf->iAnologSigMapNum), 
		SYS_GROUP_DATA_MAP_INFO_KEY, 
		&(pBuf->pAnologSigMapInfo), 
		RS485RPT_LoadGroupSysSigTableProc);
	DEF_LOADER_ITEM(&loader[1],
		CTRL_DATA_MAP_NUM_KEY, 
		&(pBuf->iCtrlSigMapNum), 
		CTRL_DATA_MAP_INFO_KEY, 
		&(pBuf->pCtrlSigMapInfo), 
		RS485RPT_LoadCtrlSigTableProc);
	DEF_LOADER_ITEM(&loader[2],
		RECT_SINGLE_INFO_MAP_NUM_KEY, 
		&(pBuf->iRectSigMapNum), 
		RECT_SINGLE_MAP_INFO_KEY, 
		&(pBuf->pRectSigMapInfo), 
		RS485RPT_LoadRectSigTableProc);

	//load		tables
	if (Cfg_LoadTables(pCfg, 3, loader) != ERR_CFG_OK)
	{
		return ;
	}

	return ;
}
/*=============================================================================*
* FUNCTION: RS485RPT_FreeCfgMemory
* PURPOSE  : in slave mode report data
* RETURN   : void
* ARGUMENTS:
* CALLS    : 
* CALLED BY: 
*							
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-05 13:59
*==========================================================================*/
INT32  RS485RPT_FreeCfgMemory(void)
{
	// &g_EsrGlobals.EEMModelConfig;
	RS485_REPORT_CFG_INFO *pMsConfig = &g_Slave_Report.Rs485_Slave_Cfg;

	if(pMsConfig->pAnologSigMapInfo)
	{
		DELETE(pMsConfig->pAnologSigMapInfo);
		pMsConfig->pAnologSigMapInfo = NULL;
	}

	if(pMsConfig->pCtrlSigMapInfo)
	{
		DELETE(pMsConfig->pCtrlSigMapInfo);
		pMsConfig->pCtrlSigMapInfo  = NULL;
	}

	if(pMsConfig->pRectSigMapInfo)
	{
		DELETE(pMsConfig->pRectSigMapInfo);
		pMsConfig->pRectSigMapInfo = NULL;
	}

	TRACE("\n\n *****  SlaveMaster Private Cfg Load Error Or Exit!! Free Memory ***\n");
	return 0;
}
/*=============================================================================*
* FUNCTION: LoadRS485SlaveModeConfig
* PURPOSE  : In slave mode load report signale  info
* RETURN   : UINT
* ARGUMENTS:
* CALLS    : 
* CALLED BY: 
*							DLLExport BOOL Query()
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-06		08:39
*==========================================================================*/
LOCAL UINT LoadRS485SlaveModeConfig(void)
{
	INT32	ret;
	CHAR	szCfgFileName[MAX_FILE_PATH]; 	

	Cfg_GetFullConfigPath(CONFIG_FILE_SLAVE_REPORT, 
		szCfgFileName,
		MAX_FILE_PATH);

	ret = Cfg_LoadConfigFile(szCfgFileName, 
		(LOAD_CONF_PROC)LoadRs485Report_Cfg_Proc,
		&g_Slave_Report.Rs485_Slave_Cfg);

	if (ret != ERR_CFG_OK)
	{
		RS485RPT_FreeCfgMemory();
		return ERR_CFG_FAIL;
	}

	return ERR_CFG_OK;
}
/*=============================================================================*
* FUNCTION: ClearBuffer(CHAR* Pbuffer)
* PURPOSE  :Clean buffer
* RETURN   : 
* ARGUMENTS:
*						CHAR*	Pbuffer :	IN	OUT
* CALLS    :			
* CALLED BY:			
*								
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
LOCAL VOID ClearBuffer(CHAR* Pbuffer,INT32 iStrLen)
{
	memset(Pbuffer,0, (unsigned int)iStrLen);
}
/*=============================================================================*
* FUNCTION:	RPTCheckSum()
* PURPOSE  :	Cumulative  result
* RETURN   :	void
* ARGUMENTS:
*				CHAR*	Frame   :IN OUT 
* CALLS    :						    
* CALLED BY:			
*								
* COMMENTS : 
* CREATOR  : copy from SMAC        DATE: 2009-05-08		14:39
*==========================================================================*/
static void RPTCheckSum( BYTE *Frame )
{
	INT32	i;   
	WORD	 R=0;
	INT32	nLen = (int)strlen( (char*)Frame );  

	for( i=1; i<nLen; i++ )
	{
		R += *(Frame+i);
	}

	sprintf( (char *)Frame+nLen, "%04X\r", (WORD)-R );
}
/*=============================================================================*
* FUNCTION: AscToHex()
* PURPOSE  :	AscToHex
* RETURN   :   BYTE : result of conversion
* ARGUMENTS:
*						CHAR*	p
* CALLS    :			AscToHex()				    
* CALLED BY:			
*								
* COMMENTS : 
*			modified by Thomas begin, support Product info, 2006-01-18
*			assistant function, used for process SW/HW version
* CREATOR  : copy from SMAC        DATE: 2009-05-08		14:39
*==========================================================================*/
BYTE AscToHex(char c)
{
	if (c >= '0' && c <= '9')
	{
		return c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		return c - 'A' + 10;
	}

	return 0xFF;
}
/*=============================================================================*
* FUNCTION: RPTHexToAsc
* PURPOSE  :hex convertor ascii
* RETURN   :BYTE
* ARGUMENTS:BYTE Hex
*   
* CALLS    : 
* CALLED BY:						
*							
*									
* COMMENTS : 
* CREATOR  : copy from smac        DATE: 2009-05-06		08:39
*==========================================================================*/
BYTE RPTHexToAsc(BYTE Hex)
{
	BYTE cTemp;
	if( Hex <= 9 && Hex >= 0 )
		cTemp = Hex+0x30;
	else
		cTemp = Hex-0x0a+'A';

	return cTemp;
}
/*=============================================================================*
* FUNCTION: RPTMergeAsc()
* PURPOSE  : assistant function, used to merge two ASCII chars
* RETURN   : BYTE : merge result
* ARGUMENTS:
*						CHAR*	p
* CALLS    :							    
* CALLED BY:			
*								
* COMMENTS : 
* CREATOR  : copy from SMAC        DATE: 2009-05-08		14:39
*==========================================================================*/
static BYTE RPTMergeAsc(char *p)
{
	BYTE byHi, byLo;
	byHi = AscToHex(p[0]);
	byLo = AscToHex(p[1]);

	//error data
	if (byHi == 0xFF || byLo == 0xFF)
	{
		return 0;
	}

	return (byHi*16 + byLo);
}
/*=============================================================================*
* FUNCTION: RPTLenCheckSum
* PURPOSE  :calculate checksum of length
* RETURN   : resulte of verify  data
* ARGUMENTS:
*				wLen: IN,  length of preparative check data	
* CALLS    : 
* CALLED BY:	Pack_Report_Data
*							
*									
* COMMENTS : 
* CREATOR  : copy from smac      DATE: 2009-05-06		08:39
*==========================================================================*/
LOCAL BYTE RPTLenCheckSum(WORD wLen)
{
	BYTE byLenCheckSum;
	if (wLen == 0)
	{
		return 0;
	}

	byLenCheckSum = 0;
	byLenCheckSum += wLen & 0x000F;         //ȡ���4��BIT
	byLenCheckSum += (wLen >> 4) & 0x000F;  //ȡ��4~7��BIT
	byLenCheckSum += (wLen >> 8) & 0x000F;  //ȡ��8~11��BIT
	byLenCheckSum %= 16;                    //ģ16
	byLenCheckSum = (~byLenCheckSum) + 1;   //ȡ����1
	byLenCheckSum &= 0x0F;                  //ֻȡ4��BIT

	return byLenCheckSum;
}
/*==========================================================================*
* FUNCTION : FixFloatDat
* PURPOSE  : eight floatvalue.by_value[i] to float type
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: char*  sStr : 
* RETURN   : float : 
* COMMENTS : 
* CREATOR  : kenn.wan                 DATE: 2008-08-15 14:55
*==========================================================================*/
static float FixFloatDat( char* sStr )
{
	INT32				i;
	REPORTSTRTOFLOAT		floatvalue;
	CHAR				cTemp;
	CHAR				cTempHi;
	CHAR				cTempLo;

	if (sStr[0] == 0x20)
	{
		return -9999.0f;
	}

	for (i=0; i<4; i++)
	{
#ifdef _CPU_BIG_ENDIAN		// ppc
#ifdef _SAMPLER_REPLACE_SSCANF
		sscanf((const char *)sStr+i*2, "%02x", 
			&floatvalue.by_value[3-i]);
#else
		int	c;
		sscanf((const char *)sStr+i*2, "%02x", 	&c);
		floatvalue.by_value[3-i] = (BYTE)c;
#endif
#else	// for x86
		//hi bit
		if((*(sStr + i * 2) > 0x40))
		{
			cTempHi = (*(sStr + i * 2) - 0x40) + 9;
		}
		else
		{
			cTempHi = (*(sStr + i * 2) - 0x30);
		}

		//low bit
		if((*(sStr + i * 2 + 1)) > 0x40)
		{
			cTempLo = (*(sStr + i * 2 + 1) - 0x40) + 9;
		}
		else
		{
			cTempLo = (*(sStr + i * 2 + 1) - 0x30);
		}

		cTemp = cTempHi * 16 + cTempLo;
		floatvalue.by_value[i] = cTemp;
#endif
	}
	return floatvalue.f_value;
}

/*=============================================================================*
* FUNCTION: RS485RPT_ProcessCtrlCmd()
* PURPOSE  :Textract control signal data field from frame and send to gc
* RETURN   :   VOID
* ARGUMENTS:
*				CHAR*	pcRecStr :	IN,	receive data from  MASTER	
*				CHAR*	SetSigCfg :	IN,	from config file
* CALLS    :			
*
* CALLED BY:			DLLExport BOOL Query()
*								
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
LOCAL VOID RS485RPT_ProcessCtrlCmd(RS485_REPORT_CFG_INFO *CtrlSigCfg, CHAR* pcRecStr)
{
	UNUSED(CtrlSigCfg);
	//CHAR						szSendStr[100];	
	CHAR						*pc_ControlDataFromMaster;
	BYTE						bModuleId;
	BYTE						bCommandId;
	BYTE						bBateryManagementStat;			
	float						f_ContrlData;
	RS485_REPORT_CFG_INFO		*pReportConfig;
	RS485_SLAVE_REPORT_SIG_INFO	*pReportSigInfo;
	pReportConfig				= &g_Slave_Report.Rs485_Slave_Cfg;
	pReportSigInfo				= pReportConfig->pCtrlSigMapInfo;
	//get slave address note:get address from app service 
	SIG_ENUM					enumSlaveAddr;
	SIG_ENUM					enumTemp;
	//INT32						iSlaveAddr;

	//get slave address note:get address from app service 
	enumSlaveAddr = GetEnumSigValue(RS485_GET_ADDR_EQUIPID,
		SIG_TYPE_SETTING,
		RS485_RUNNING_ADDR_SIGID,
		"485_SAMP");



	/*
	13: SOI VER ADR CID1 CID2/RTN	LENGTH
	15:	DATAF 
	DATAINFO:include  COMMAND TYPE and COMMAND DATAF(VER1.0 PROTOCOL)
	TYPE�� 1 BYTE	associate	 	2X = 2	  						
	DATAF��4 BYTE	associate		2X = 8
	*/
	//Point to the  COMMAND  ID
	pc_ControlDataFromMaster = pcRecStr + DATA_YDN23_REPORT;				
	/*
	bCommandId function is necessary to refer to Master/Slave 
	communication  protocol file.
	COMMAND  ID
	*/
	bCommandId = RPTMergeAsc(pc_ControlDataFromMaster);

	//Not process
	if(SLAVE_INVALID_CMDID == bCommandId)
	{
		return;
	}

	//Point to the MODULE    ID
	pc_ControlDataFromMaster = pcRecStr + DATA_YDN23_REPORT + 2;
	bModuleId				 = RPTMergeAsc(pc_ControlDataFromMaster);

	//Point to the	BATERY MANAGEMENT STATE
	pc_ControlDataFromMaster = pcRecStr + DATA_YDN23_REPORT + 2 + 2;
	bBateryManagementStat	 = RPTMergeAsc(pc_ControlDataFromMaster);
	SetEnumSigValue(BATTERIER_GROUP, 
		BATTERIER_SIG_TYPE,
		BATTERIER_MANAGEMENT_STAT_SIG_ID,
		bBateryManagementStat,
		"485_SAMP");

	//SIGINFO
	pc_ControlDataFromMaster = pcRecStr + DATA_YDN23_REPORT + 2 + 2 + 2;
	f_ContrlData			 = FixFloatDat(pc_ControlDataFromMaster);

	//COMMAND ID=20H   ����������Ч ���ӻ������2��3��4�źž�������
	//COMMAND ID=10H   ����ģ��DC���ؿ���
	//COMMAND ID=1FH   ����ģ��AC���ؿ���
	//COMMAND ID=11H   ����ģ��LED���ؿ���
	//COMMAND ID=2DH   �ӻ�ģ�������ѹ		MODULE ID ��Ч
	//COMMAND ID=2FH   �ӻ�ģ�����������	MODULE ID ��Ч
	//COMMAND ID=E5H   ����AC���ؿ���		MODULE ID ��Ч
	//COMMAND ID=EAH   ����DC���ؿ���		MODULE ID ��Ч
	//COMMAND ID=ECH   ����LED���ؿ���		MODULE ID ��Ч

	//must refer to slave_report.cfg  synchronization
	switch(bCommandId)
	{
	case 0x10://DC On/Off Control
		pReportSigInfo = pReportConfig->pCtrlSigMapInfo + CMD_DC_ON_OFF_CONTROL;
		enumTemp	   = (SIG_ENUM)f_ContrlData;
		SetEnumSigValue(bModuleId + RECT_GROUP_START + 1, 
			pReportSigInfo->Sig_Type,
			pReportSigInfo->Sig_Id,
			enumTemp,
			"485_SAMP");
		TRACE("\n  case 0x10:   Ctrl DC On/Off Control	::Sig_Id = %d Sig_Type =%d fData =%f\n",
			pReportSigInfo->Sig_Id,pReportSigInfo->Sig_Type,f_ContrlData);
		break;

	case 0x1f://AC On/Off Control
		pReportSigInfo = pReportConfig->pCtrlSigMapInfo + CMD_AC_ON_OFF_CONTROL;
		enumTemp	   = (SIG_ENUM)f_ContrlData;
		SetEnumSigValue(bModuleId + RECT_GROUP_START + 1, 
			pReportSigInfo->Sig_Type,
			pReportSigInfo->Sig_Id,
			enumTemp,
			"485_SAMP");

		TRACE("\n  case 0x1f:   Ctrl AC On/Off Control::Sig_Id = %d Sig_Type =%d fData =%f \n",
			pReportSigInfo->Sig_Id,pReportSigInfo->Sig_Type,f_ContrlData);
		break;

	case 0x11://LED Control
		pReportSigInfo = pReportConfig->pCtrlSigMapInfo + CMD_LED_CONTROL;
		enumTemp	   = (SIG_ENUM)f_ContrlData;
		SetEnumSigValue(bModuleId + RECT_GROUP_START + 1, 
			pReportSigInfo->Sig_Type,
			pReportSigInfo->Sig_Id,
			enumTemp,
			"485_SAMP");
		TRACE("\n  case 0x11:   Ctrl LED Control::Sig_Id = %d Sig_Type =%d fData =%f  \n",
			pReportSigInfo->Sig_Id,pReportSigInfo->Sig_Type,f_ContrlData);
		break;

	case 0x17://RECT Reset
		pReportSigInfo = pReportConfig->pCtrlSigMapInfo + CMD_RESET_RECTIFIER;
		enumTemp	   = (SIG_ENUM)f_ContrlData;
		SetEnumSigValue(bModuleId + RECT_GROUP_START + 1, 
			pReportSigInfo->Sig_Type,
			pReportSigInfo->Sig_Id,
			enumTemp,
			"485_SAMP");
		TRACE("\n  case 0x17:   Ctrl RECT Reset ::Sig_Id = %d Sig_Type =%d 	fData =%f \n",
			pReportSigInfo->Sig_Id,pReportSigInfo->Sig_Type,f_ContrlData);
		break;

	case 0x2d://DC voltage control
		pReportSigInfo = pReportConfig->pCtrlSigMapInfo + CMD_DC_VOLTAGE_CONTROL;
		SetFloatSigValue(RECT_GROUP_START, 
			pReportSigInfo->Sig_Type,
			pReportSigInfo->Sig_Id,
			f_ContrlData,
			"485_SAMP");

		TRACE("\n  case 0x2d:   CtrlDC voltage control::Sig_Id = %d Sig_Type =%d 	fData = %f \n",
			pReportSigInfo->Sig_Id, pReportSigInfo->Sig_Type, f_ContrlData);

		break;

	case 0x2f://Current limit control  Float	GROUP Sig
		pReportSigInfo = pReportConfig->pCtrlSigMapInfo + CMD_DC_CURRENT_LIMIT_CONTROL;
		SetFloatSigValue(RECT_GROUP_START, 
			pReportSigInfo->Sig_Type,
			pReportSigInfo->Sig_Id,
			f_ContrlData,
			"485_SAMP");

		TRACE("\n  case 0x2f:   Ctrl Current limit control ::Sig_Id = %d Sig_Type =%d  fData = %f \n",
			pReportSigInfo->Sig_Id, pReportSigInfo->Sig_Type, f_ContrlData);

		break;	

	case 0xe5://All AC On/Off Control
		pReportSigInfo = pReportConfig->pCtrlSigMapInfo + CMD_ALL_AC_ON_OFF_CONTROL;
		enumTemp	   = (SIG_ENUM)f_ContrlData;
		SetEnumSigValue(RECT_GROUP_START, 
			pReportSigInfo->Sig_Type,
			pReportSigInfo->Sig_Id,
			enumTemp,
			"485_SAMP");
		TRACE("\n  case 0xe5:   Ctrl All AC On/Off Control::Sig_Id = %d Sig_Type =%d  fData = %f\n",
			pReportSigInfo->Sig_Id, pReportSigInfo->Sig_Type, f_ContrlData);
		break;	

	case 0xea://All DC On/Off Control
		pReportSigInfo = pReportConfig->pCtrlSigMapInfo + CMD_ALL_DC_ON_OFF_CONTROL;
		enumTemp	   = (SIG_ENUM)f_ContrlData;
		SetEnumSigValue(RECT_GROUP_START, 
			pReportSigInfo->Sig_Type,
			pReportSigInfo->Sig_Id,
			enumTemp,
			"485_SAMP");

		TRACE("\n  case 0xea:   Ctrl All DC On/Off Control Sig_Id = %d Sig_Type =%d fData = %f \n",
			pReportSigInfo->Sig_Id, pReportSigInfo->Sig_Type, f_ContrlData);
		break;

	case 0xec://All LED Control	
		pReportSigInfo = pReportConfig->pCtrlSigMapInfo + CMD_ALL_LED_CONTROL;
		enumTemp	   = (SIG_ENUM)f_ContrlData;
		SetEnumSigValue(RECT_GROUP_START, 
			pReportSigInfo->Sig_Type,
			pReportSigInfo->Sig_Id,
			enumTemp,
			"485_SAMP");
		TRACE("\n  case 0xec:   Ctrl All LED Control Sig_Id = %d Sig_Type =%d	fData = %f \n",
			pReportSigInfo->Sig_Id, pReportSigInfo->Sig_Type, f_ContrlData);
		break;
	case 0xed:
		pReportSigInfo = pReportConfig->pCtrlSigMapInfo + CMD_RESET_LOST_ALARM;
		enumTemp	   = (SIG_ENUM)f_ContrlData;
		SetEnumSigValue(RECT_GROUP_START, 
			pReportSigInfo->Sig_Type,
			pReportSigInfo->Sig_Id,
			enumTemp,
			"485_SAMP");

		TRACE("\n  case 0xed:   Ctrl clear lost alarm :: Sig_Id = %d Sig_Type =%d fData = %f \n",
			pReportSigInfo->Sig_Id,pReportSigInfo->Sig_Type,f_ContrlData);
		break;

	case 0xef:
		pReportSigInfo = pReportConfig->pCtrlSigMapInfo + CMD_RESET_OSCILLATED_ALARM;
		enumTemp	   = (SIG_ENUM)f_ContrlData;
		SetEnumSigValue(RECT_GROUP_START, 
			pReportSigInfo->Sig_Type,
			pReportSigInfo->Sig_Id,
			enumTemp,
			"485_SAMP");

		TRACE("\n   case case 0xef::    Ctrl clear oscillated alarm:: Sig_Id = %d Sig_Type =%d  fData = %f\n",
			pReportSigInfo->Sig_Id,pReportSigInfo->Sig_Type,f_ContrlData);
		break;
	case 0xe7:
		pReportSigInfo = pReportConfig->pCtrlSigMapInfo + CMD_SET_AC_CURR_LIMIT;
		SetFloatSigValue(RECT_GROUP_START, 
			pReportSigInfo->Sig_Type,
			pReportSigInfo->Sig_Id,
			f_ContrlData,
			"485_SAMP");

		TRACE("\n   case case 0xe7::    Ctrl clear oscillated alarm:: Sig_Id = %d Sig_Type =%d  fData = %f\n",
			pReportSigInfo->Sig_Id,pReportSigInfo->Sig_Type,f_ContrlData);
		break;
	case 0xe8:
		pReportSigInfo = pReportConfig->pCtrlSigMapInfo + CMD_CTL_E_STOP;
		enumTemp	   = (SIG_ENUM)f_ContrlData;
		SetEnumSigValue(RECT_GROUP_START, 
			pReportSigInfo->Sig_Type,
			pReportSigInfo->Sig_Id,
			enumTemp,
			"485_SAMP");
		TRACE("\n   case case 0xe8::    Ctrl E STOP :: Sig_Id = %d Sig_Type =%d  fData = %f\n",
			pReportSigInfo->Sig_Id,pReportSigInfo->Sig_Type,f_ContrlData);
		break;
	case 0xe9:
		pReportSigInfo = pReportConfig->pCtrlSigMapInfo + CMD_CLS_COMM_INTERR;
		enumTemp	   = (SIG_ENUM)f_ContrlData;
		SetEnumSigValue(RECT_GROUP_START, 
			pReportSigInfo->Sig_Type,
			pReportSigInfo->Sig_Id,
			enumTemp,
			"485_SAMP");
		TRACE("\n   case case 0xe9::    Clear Slave Rect Comm Interr :: Sig_Id = %d Sig_Type =%d  fData = %f\n",
			pReportSigInfo->Sig_Id,pReportSigInfo->Sig_Type,f_ContrlData);
		break;
	case 0xeb:
		pReportSigInfo = pReportConfig->pCtrlSigMapInfo + CMD_FAN_SPEED;
		enumTemp	   = (SIG_ENUM)f_ContrlData;
		SetEnumSigValue(RECT_GROUP_START, 
			pReportSigInfo->Sig_Type,
			pReportSigInfo->Sig_Id,
			enumTemp,
			"485_SAMP");
		TRACE("\n   case case 0xeb::    Control Fan Speed :: Sig_Id = %d Sig_Type =%d  fData = %f\n",
			pReportSigInfo->Sig_Id,pReportSigInfo->Sig_Type,f_ContrlData);
		break;
	case 0xee:
		pReportSigInfo = pReportConfig->pCtrlSigMapInfo + CMD_CONFIRM_POSITION;
		enumTemp	   = (SIG_ENUM)f_ContrlData;
		SetEnumSigValue(RECT_GROUP_START, 
			pReportSigInfo->Sig_Type,
			pReportSigInfo->Sig_Id,
			1,
			"485_SAMP");
		SetDwordSigValue(RECT_GROUP_START, 
			SLAVE_MODULE_INFO_CHANGE_SIG_TYPE, 
			SLAVE_MODULE_INFO_CHANGE_SIGID,
			TRUE,
			"FOR_RS485");
		TRACE("\n   case case 0xee::    Confirm Rect Position :: Sig_Id = %d Sig_Type =%d  fData = %f\n",
			pReportSigInfo->Sig_Id,pReportSigInfo->Sig_Type,f_ContrlData);
		break;

	case 0xf3:
		pReportSigInfo = pReportConfig->pCtrlSigMapInfo + CMD_RESET_POSITION;
		enumTemp	   = (SIG_ENUM)f_ContrlData;
		SetEnumSigValue(RECT_GROUP_START, 
			pReportSigInfo->Sig_Type,
			pReportSigInfo->Sig_Id,
			1,
			"485_SAMP");
		printf("slave received %d %d\n",pReportSigInfo->Sig_Type,pReportSigInfo->Sig_Id);
		SetDwordSigValue(RECT_GROUP_START, 
			SLAVE_MODULE_INFO_CHANGE_SIG_TYPE, 
			SLAVE_MODULE_INFO_CHANGE_SIGID,
			TRUE,
			"FOR_RS485");
		TRACE("\n   case case 0xee::    reset Rect Position :: Sig_Id = %d Sig_Type =%d  fData = %f\n",
			pReportSigInfo->Sig_Id,pReportSigInfo->Sig_Type,f_ContrlData);
		break;

	case 0xf1:
		pReportSigInfo = pReportConfig->pCtrlSigMapInfo + CMD_RECT_UNIT_POSITION;
		enumTemp	   = (SIG_ENUM)f_ContrlData;
		SetDwordSigValue(bModuleId + RECT_GROUP_START + 1, 
			pReportSigInfo->Sig_Type,
			pReportSigInfo->Sig_Id,
			enumTemp,
			"485_SAMP");
		SetDwordSigValue(RECT_GROUP_START, 
			SLAVE_MODULE_INFO_CHANGE_SIG_TYPE, 
			SLAVE_MODULE_INFO_CHANGE_SIGID,
			TRUE,
			"FOR_RS485");
		TRACE("\n  case 0xf1:   Ctrl Rect Unit Position	::Sig_Id = %d Sig_Type =%d fData =%f\n",
			pReportSigInfo->Sig_Id,pReportSigInfo->Sig_Type,f_ContrlData);
		break;
	case 0xf2:
		pReportSigInfo = pReportConfig->pCtrlSigMapInfo + CMD_RECT_UNIT_PHASE;
		enumTemp	   = (SIG_ENUM)f_ContrlData;
		SetEnumSigValue(bModuleId + RECT_GROUP_START + 1, 
			pReportSigInfo->Sig_Type,
			pReportSigInfo->Sig_Id,
			enumTemp,
			"485_SAMP");
		TRACE("\n  case 0xf2:   Ctrl Rect Unit Phase	::Sig_Id = %d Sig_Type =%d fData =%f\n",
			pReportSigInfo->Sig_Id,pReportSigInfo->Sig_Type,f_ContrlData);
		break;

	default:
		break;
	}


	//iSlaveAddr = enumSlaveAddr + SLAVE_ADDR_OFFSET_START;

	//TRACE("\n !!!  &&&&&&&&&&&&&&&&&&  iSlaveAddr = %d \n",iSlaveAddr);

	//sprintf((CHAR *)szSendStr,
	//				"~%02X%02X%02X%02X%04X\0",
	//				0x20,
	//				iSlaveAddr, 
	//				0xe5,
	//				0x00,
	//				0);

	//RPTCheckSum(szSendStr);

	//RS485_Write(g_RS485Data.CommPort.hCommPort,szSendStr,strlen(szSendStr));

}
/*=============================================================================*
* FUNCTION: RS485RPT_ResponseError(CHAR Rtn)
* PURPOSE  :Response Error Code  by Rtn
* RETURN   :VOID
* ARGUMENTS:Rtn : IN, ERROR CODE
* CALLS    :			
* CALLED BY:SlaveParseData_Response()
*								
* COMMENTS :	Rtn:
*							0X01 VER ERROR
*							0X02 CHEKSUM ERROR
*							0X03 LCHEKSUM ERROR
*							0X04 CID2 ERROR
*							0X05 FORMAT ERROR
*							0X06 INVIABLE DATA ERROR
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
LOCAL VOID RS485RPT_ResponseError(CHAR Rtn)
{
	BYTE szSendStr[100]={0};
	BYTE lenChk;
	INT32 iSendlen;

	sprintf((char *)szSendStr,"~%02X%02X%02X%02X%04X\0",0x20, (unsigned int)g_SLAVEAddr, 0xe5, Rtn, 0);
	lenChk = RPTLenCheckSum((0*2))<<4;
	szSendStr[9] = RPTHexToAsc( (BYTE)((lenChk&0xf0)>>4) );
	szSendStr[10] = RPTHexToAsc((BYTE)(lenChk&0x0f));
	szSendStr[11] = RPTHexToAsc((BYTE)((0 & 0xf0)>>4));
	szSendStr[12] = RPTHexToAsc((BYTE)(0 & 0x0f));
	RPTCheckSum(szSendStr);										//Auto plus 4 byte
	iSendlen = 13 + 4 + 1;										//EOI~CID2+CHECKSUM+EOI
	//*((BYTE*)(szSendStr + iSendlen - 1)) = byEOI;							//EOI	
	szSendStr[iSendlen - 1] = byEOI;
	RS485_Write(g_RS485Data.CommPort.hCommPort,szSendStr,(int)(strlen((char*)szSendStr)));
}
/*=============================================================================*
* FUNCTION: SLAVEGet RectSig()
* PURPOSE  :Get  the data from gen_ctl  by  Dxiget
* RETURN   : 0 ok ,-1 fail
* ARGUMENTS:
*						CHAR*	gDataInfo :	IN	
*						CHAR*	pcDestStr :	OUT	 g_Slave_Report.SendDataInfo 
* CALLS    :			SlaveGetVer()				4FH
*						Slave GetAnolog()			E1H
*						SlaveControl()				EAH
* CALLED BY:			DLLExport BOOL Query()
*								
* COMMENTS : only	get the data and stuff to pcDestStr
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
LOCAL INT32 RS485RPT_GetRectSig(RS485_REPORT_CFG_INFO *gDataInfo, CHAR* pcDestStr)
{
	UNUSED(gDataInfo);
	CHAR*						PRecdata;
	INT32						i,j,l;
	INT32						nError;
	INT32						iBufLen;
	INT32						iRectifierModuleNum;
	INT32						iRectifierSigNum;
	INT32						iRectifier0XE3SigNum;
	INT32						iRectifier0XE1SigNum;
	SIG_BASIC_VALUE*			pSigValue;
	RS485_REPORT_CFG_INFO*		pReportConfig;	
	pReportConfig				= &g_Slave_Report.Rs485_Slave_Cfg;//slave_report.cfg
	RS485_SLAVE_REPORT_SIG_INFO*pRectSigInfo;		
	pRectSigInfo				= pReportConfig->pRectSigMapInfo;//[RECT_SINGLE_MAP_INFO]
	iRectifierSigNum			= pReportConfig->iRectSigMapNum;
	iRectifier0XE1SigNum		= 0;
	//find the place  point to Barcode1 in the  [RECT_SINGLE_MAP_INFO] by SECTION  FLAG	
	for(;;)
	{
		if((pRectSigInfo->ByteNum_Of_Sig == 0) 
			&& (pRectSigInfo->Sig_Id == '\0')) 
		{
			//point to next to SECTION FLAG	 of [RECT_SINGLE_MAP_INFO]  in the slave_report.cfg
			pRectSigInfo++;
			iRectifier0XE1SigNum++;
			break;
		}
		pRectSigInfo++;
		iRectifier0XE1SigNum++;
	}

	iRectifier0XE3SigNum = iRectifierSigNum - iRectifier0XE1SigNum;
	RS485_SLAVE_REPORT_SIG_INFO*  pTempRectSigInfo;
	g_Slave_Report.iE3Totalbyte	  = 0;
	iRectifierModuleNum = GetEnumSigValue(2,
		SIG_TYPE_SAMPLING,
		8,
		"485_SAMP");

	if (0 == iRectifierModuleNum)
	{
		return 0;//Needn't get rectifier sig OK
	}	

	if(iRectifierModuleNum > SLAVE_MAX_RECTIFIER_NUM)
	{
		iRectifierModuleNum = SLAVE_MAX_RECTIFIER_NUM;
	}

	//One rectifier module by One  process
	for(i = 0; i < iRectifierModuleNum; i++)
	{
		Sleep(20);
		//One Rectifier alone all sig 
		for(pTempRectSigInfo = pRectSigInfo, j = 0;
			j < iRectifier0XE3SigNum;
			pTempRectSigInfo++, j++)
		{
			//TRACE("\n RCT[%d]Sig Number j = %d ID=%d",i,j,pTempRectSigInfo->Sig_Id);
			if(pTempRectSigInfo->ByteNum_Of_Sig == 4)
			{
				nError = DxiGetData(VAR_A_SIGNAL_VALUE,
					i + 3,			
					DXI_MERGE_SIG_ID(pTempRectSigInfo->Sig_Type, pTempRectSigInfo->Sig_Id),		
					(int*)(&iBufLen),			
					&pSigValue,			
					0);

				if (pTempRectSigInfo->Sig_Type == 0 && pTempRectSigInfo->Sig_Id == 11)
				{
					//pSigValue switch to  the pcDestStr
					if(nError == ERR_DXI_OK)
					{
						switch(pSigValue->ucType)
						{
						case VAR_LONG:
							(*pcDestStr) = (0xff00 & ((pSigValue->varValue.lValue)*10))>>8;
							(*(pcDestStr+1)) = (0x00ff & ((pSigValue->varValue.lValue)*10));
							g_Slave_Report.iE3Totalbyte += (pTempRectSigInfo->ByteNum_Of_Sig)/2;
							pcDestStr += (pTempRectSigInfo->ByteNum_Of_Sig)/2;	
							/*if (i >= 56)
							{
							TRACE(" RCT[%d]Sig Number j = %d ID=%d lVal=%d\n",i,j,pTempRectSigInfo->Sig_Id,pSigValue->varValue.lValue);
							}*/

							break;

						case VAR_FLOAT:
							(*pcDestStr) = (0xff00 & (ULONG)((pSigValue->varValue.fValue)*10))>>8;
							(*(pcDestStr+1)) = (0x00ff & (ULONG)((pSigValue->varValue.fValue)*10));
							g_Slave_Report.iE3Totalbyte += pTempRectSigInfo->ByteNum_Of_Sig/2;
							pcDestStr += (pTempRectSigInfo->ByteNum_Of_Sig)/2;
							/*if (i >= 56)
							{
							TRACE(" RCT[%d]Sig Number j = %d ID=%d fVal=%d\n",i,j,pTempRectSigInfo->Sig_Id,pSigValue->varValue.fValue);
							}*/
							break;

						case VAR_UNSIGNED_LONG:
							(*pcDestStr) = (0xff00 & ((pSigValue->varValue.ulValue)*10))>>8;
							(*(pcDestStr+1)) = (0x00ff & ((pSigValue->varValue.ulValue)*10));
							g_Slave_Report.iE3Totalbyte += pTempRectSigInfo->ByteNum_Of_Sig/2;
							pcDestStr += (pTempRectSigInfo->ByteNum_Of_Sig)/2;
							/*if (i >= 56)
							{
							TRACE(" RCT[%d]Sig Number j = %d ID=%d ulVal=%d\n",i,j,pTempRectSigInfo->Sig_Id,pSigValue->varValue.ulValue);
							}*/
							break;

						case VAR_ENUM:
							pSigValue->varValue.enumValue;
							TRACE("\n	EEEEE	Report  i= %d   enumValue =%d \n",i,pSigValue->varValue.enumValue);
							break;

						default:		
							break;
						}
					}
					else
					{
						AppLogOut("SLAVE SLAV EGetAnolog  error",
							APP_LOG_UNUSED,
							"[%s]-[%d] ERROR: failed to get data by DxiGetData \n\r", 
							__FILE__, __LINE__);
						TRACE("\n*****GET [SYS_GROUP_DATA_MAP_NUM] SIG  ERROR *****\n");
						return -1;
					}	

					continue;
				}

				//pSigValue switch to  the pcDestStr
				if(nError == ERR_DXI_OK)
				{
					switch(pSigValue->ucType)
					{
					case VAR_LONG:
						PRecdata = (CHAR*)(&(pSigValue->varValue.lValue));
						/*if (i >= 56)
						{
						TRACE(" RCT[%d]Sig Number j = %d ID=%d lVal=%d\n",i,j,pTempRectSigInfo->Sig_Id,pSigValue->varValue.lValue);
						}*/
						break;

					case VAR_FLOAT:
						PRecdata = (CHAR*)(&(pSigValue->varValue.fValue));
						/*if (i >= 56)
						{
						TRACE(" RCT[%d]Sig Number j = %d ID=%d fVal=%d\n",i,j,pTempRectSigInfo->Sig_Id,pSigValue->varValue.fValue);
						}*/
						break;

					case VAR_UNSIGNED_LONG:
						PRecdata = (CHAR*)(&(pSigValue->varValue.ulValue));
						/*if (i >= 56)
						{
						TRACE(" RCT[%d]Sig Number j = %d ID=%d ulVal=%d\n",i,j,pTempRectSigInfo->Sig_Id,pSigValue->varValue.ulValue);
						}*/

						break;

					case VAR_ENUM:
						PRecdata = (CHAR*)(&(pSigValue->varValue.enumValue));
						//TRACE("\n Sig value: %d\n",pSigValue->varValue.enumValue);

						break;
					default:		
						break;
					}
				}
				else
				{
					//TRACE("\n#######  RPT  RPT if(nError !!!!== ERR_DXI_OK) \n");
					return -1;
				}

				for(l = 0; l < pTempRectSigInfo->ByteNum_Of_Sig; l++)
				{
#ifdef  _CPU_BIG_ENDIAN		// ppc
					*(pcDestStr + l) = *(PRecdata + 3 - l);
#else				// for x86
					*(pcDestStr + l) = *(PRecdata + l);
#endif
				}

				pcDestStr += pTempRectSigInfo->ByteNum_Of_Sig;//Point to next sig
				g_Slave_Report.iE3Totalbyte += pTempRectSigInfo->ByteNum_Of_Sig;
			}
			else if(pTempRectSigInfo->ByteNum_Of_Sig == 1)
			{
				//get sig info
				*(pcDestStr) =(BYTE)GetEnumSigValue(i + 3,
					pTempRectSigInfo->Sig_Type,
					pTempRectSigInfo->Sig_Id,
					"485_SAMP");
				/*if (i >= 56)
				{
				TRACE(" RCT[%d]Sig Number j = %d ID=%d EVal=%d\n",i,j,pTempRectSigInfo->Sig_Id,(*(pcDestStr)));
				}*/
				pcDestStr += pTempRectSigInfo->ByteNum_Of_Sig;//Point to next sig
				g_Slave_Report.iE3Totalbyte += pTempRectSigInfo->ByteNum_Of_Sig;
			}
		}
	}
	return 0;//get rectifier sig ok
}

/*=============================================================================*
* FUNCTION: SLAVE GetAnolog()
* PURPOSE  :Get  the data from gen_ctl  by  Dxiget
* RETURN   : 0 ok ,-1 fail
* ARGUMENTS:			[SYS_GROUP_DATA_MAP_INFO][RECT_SINGLE_MAP_INFO]
*						CHAR*	gDataInfo :	IN	
*						CHAR*	pcDestStr :	OUT	 g_Slave_Report. 
* CALLS    :			SlaveGetVer()				4FH
*						Slave GetAnolog()			E1H
*						SlaveControl()				EAH
* CALLED BY:			DLLExport BOOL Query()
*								
* COMMENTS : only	get the data and stuff to pcDestStr
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
LOCAL INT32 RS485RPT_E1GetGroupSysRectSig(RS485_REPORT_CFG_INFO *gDataInfo, CHAR* pcDestStr)
{
	UNUSED(gDataInfo);
	CHAR						*PRecdata;	
	INT32						i;
	INT32						j;
	INT32						k;
	INT32						iGroupSysSigNum;
	INT32						nError;
	INT32						iBufLen;
	INT32						iRectifierModuleNum;
	SIG_BASIC_VALUE*			pSigValue;
	RS485_REPORT_CFG_INFO		*pReportConfig;		
	RS485_SLAVE_REPORT_SIG_INFO	*pReportSigInfo;
	pReportConfig				= &g_Slave_Report.Rs485_Slave_Cfg; //slave_report.cfg
	pReportSigInfo				= pReportConfig->pAnologSigMapInfo;//[SYS_GROUP_DATA_MAP_INFO]
	iGroupSysSigNum				= pReportConfig->iAnologSigMapNum;
	g_Slave_Report.iE1Totalbyte	= 0;			
	//1.	stuff [SYS_GROUP_DATA_MAP_NUM] sig info 
	for (i = 0; i < iGroupSysSigNum; i++, pReportSigInfo++)
	{
		//there is some "NA NA NA", it means to stuff 0x00 in the advance hold .
		if((pReportSigInfo->Equip_Id		== '\0') 
			&& (pReportSigInfo->Sig_Type	== '\0')
			&& (pReportSigInfo->Sig_Id		== '\0'))
		{
			for(k = 0; k < pReportSigInfo->ByteNum_Of_Sig; k++)
			{
				*(pcDestStr + k)  = 0x00;//stuff 0x00
			}

			pcDestStr += pReportSigInfo->ByteNum_Of_Sig;
			g_Slave_Report.iE1Totalbyte += pReportSigInfo->ByteNum_Of_Sig;
			//Continue to next the signal in config file
			continue;
		}

		if(pReportSigInfo->ByteNum_Of_Sig == 4)
		{
			nError = DxiGetData(VAR_A_SIGNAL_VALUE,
				pReportSigInfo->Equip_Id,			
				DXI_MERGE_SIG_ID(pReportSigInfo->Sig_Type, pReportSigInfo->Sig_Id),		
				(int*)(&iBufLen),			
				&pSigValue,			
				0);

			//pSigValue switch to  the pcDestStr
			if(nError == ERR_DXI_OK)
			{
				switch(pSigValue->ucType)
				{
				case VAR_LONG:
					PRecdata = (CHAR*)&pSigValue->varValue.lValue;
					break;

				case VAR_FLOAT:
					PRecdata = (CHAR*)&pSigValue->varValue.fValue;
					//TRACE("\nReport  i= %d   fValue =%f \n",i,pSigValue->varValue.fValue);
					break;

				case VAR_UNSIGNED_LONG:
					PRecdata = (CHAR*)&pSigValue->varValue.ulValue;
					break;

				case VAR_ENUM:
					PRecdata = (CHAR*)&pSigValue->varValue.enumValue;
					//TRACE("\nReport  i= %d   enumValue =%d \n",i,pSigValue->varValue.enumValue);
					break;

				default:		
					break;
				}
			}
			else
			{
				AppLogOut("SLAVE SLAV EGetAnolog  error",
					APP_LOG_UNUSED,
					"[%s]-[%d] ERROR: failed to get data by DxiGetData \n\r", 
					__FILE__, __LINE__);
				TRACE("\n*****GET [SYS_GROUP_DATA_MAP_NUM] SIG  ERROR *****\n");
				return -1;
			}

			for(j = 0; j < pReportSigInfo->ByteNum_Of_Sig; j++)
			{
#ifdef  _CPU_BIG_ENDIAN		// ppc
				*(pcDestStr + j) = *(PRecdata + 3 - j);
#else	// for x86
				*(pcDestStr + j) = *(PRecdata + j);
#endif
			}

			g_Slave_Report.iE1Totalbyte += pReportSigInfo->ByteNum_Of_Sig;
			pcDestStr += pReportSigInfo->ByteNum_Of_Sig;


		}
		else if(pReportSigInfo->ByteNum_Of_Sig == 1)
		{
			*(pcDestStr) =(BYTE)GetEnumSigValue(pReportSigInfo->Equip_Id,
				pReportSigInfo->Sig_Type,
				pReportSigInfo->Sig_Id,
				"485_SAMP");

			//When Slave rectifier module info change here will clean change flag
			if ((pReportSigInfo->Equip_Id == RECT_GROUP_START)
				&& (pReportSigInfo->Sig_Id == SLAVE_MODULE_INFO_CHANGE_SIGID)
				&& (TRUE == *(pcDestStr)))
			{
				if (siSlaveRectChage > 2)
				{
					SetDwordSigValue(RECT_GROUP_START, 
						SLAVE_MODULE_INFO_CHANGE_SIG_TYPE, 
						SLAVE_MODULE_INFO_CHANGE_SIGID,
						FALSE,
						"FOR_RS485");
					TRACE("\n***This  is Clean the rectifier info change Flag ***\n");
					siSlaveRectChage = 0;
				}
			}
			else
			{	
			}

			g_Slave_Report.iE1Totalbyte += pReportSigInfo->ByteNum_Of_Sig;
			pcDestStr += pReportSigInfo->ByteNum_Of_Sig;
		}
	}//end of stuff [SYS_GROUP_DATA_MAP_NUM]

	//2.	[RECT_SINGLE_MAP_INFO] SECTION FLAG	NA	NA	NA	0
	RS485_SLAVE_REPORT_SIG_INFO		*pRectSigInfo;
	pRectSigInfo = pReportConfig->pRectSigMapInfo;//[RECT_SINGLE_MAP_INFO]
	//Get slave rectifier number
	iRectifierModuleNum = GetEnumSigValue(2,
		SIG_TYPE_SAMPLING,
		8,
		"485_SAMP");

	if(iRectifierModuleNum > SLAVE_MAX_RECTIFIER_NUM)
	{
		iRectifierModuleNum = SLAVE_MAX_RECTIFIER_NUM;
	}

	//stuff	[RECT_SINGLE_MAP_INFO]  sig	info 
	for(i = 0; i < iRectifierModuleNum; i++)
	{
		for(pRectSigInfo = pReportConfig->pRectSigMapInfo;
			((pRectSigInfo->ByteNum_Of_Sig != 0) 
			&& (pRectSigInfo->Sig_Id != '\0'));//SECTION FLAG end
		pRectSigInfo++)
		{
			if(pRectSigInfo->ByteNum_Of_Sig == 4)
			{
				nError = DxiGetData(VAR_A_SIGNAL_VALUE,
					i + 3,			
					DXI_MERGE_SIG_ID(pRectSigInfo->Sig_Type, pRectSigInfo->Sig_Id),		
					(int*)(&iBufLen),			
					&pSigValue,			
					0);

				if ((pRectSigInfo->Sig_Type == 0 && pRectSigInfo->Sig_Id == 4) ||
					(pRectSigInfo->Sig_Type == 0 && pRectSigInfo->Sig_Id == 6))
				{
					//pSigValue switch to  the pcDestStr
					if(nError == ERR_DXI_OK)
					{
						switch(pSigValue->ucType)
						{
						case VAR_LONG:
							(*pcDestStr) = (0xff00 & ((pSigValue->varValue.lValue)*10))>>8;
							(*(pcDestStr+1)) = (0x00ff & ((pSigValue->varValue.lValue)*10));
							g_Slave_Report.iE1Totalbyte += (pRectSigInfo->ByteNum_Of_Sig)/2;
							pcDestStr += (pRectSigInfo->ByteNum_Of_Sig)/2;	
							break;

						case VAR_FLOAT:
							(*pcDestStr) = (0xff00 & (ULONG)((pSigValue->varValue.fValue)*10))>>8;
							(*(pcDestStr+1)) = (0x00ff & (ULONG)((pSigValue->varValue.fValue)*10));
							g_Slave_Report.iE1Totalbyte += pRectSigInfo->ByteNum_Of_Sig/2;
							pcDestStr += (pRectSigInfo->ByteNum_Of_Sig)/2;
							//TRACE("\nReport  i= %d   fValue =%f \n",i,pSigValue->varValue.fValue);
							break;

						case VAR_UNSIGNED_LONG:
							(*pcDestStr) = (0xff00 & ((pSigValue->varValue.ulValue)*10))>>8;
							(*(pcDestStr+1)) = (0x00ff & ((pSigValue->varValue.ulValue)*10));
							g_Slave_Report.iE1Totalbyte += pRectSigInfo->ByteNum_Of_Sig/2;
							pcDestStr += (pRectSigInfo->ByteNum_Of_Sig)/2;

							break;

						default:		
							break;
						}
					}
					else
					{
						AppLogOut("SLAVE SLAV EGetAnolog  error",
							APP_LOG_UNUSED,
							"[%s]-[%d] ERROR: failed to get data by DxiGetData \n\r", 
							__FILE__, __LINE__);
						TRACE("\n*****GET [SYS_GROUP_DATA_MAP_NUM] SIG  ERROR *****\n");
						return -1;
					}	

					continue;
				}

				if(nError == ERR_DXI_OK)
				{
					switch(pSigValue->ucType)
					{
					case VAR_LONG:
						PRecdata = (CHAR*)&pSigValue->varValue.lValue;
						break;

					case VAR_FLOAT:
						PRecdata = (CHAR*)&pSigValue->varValue.fValue;
						break;

					case VAR_UNSIGNED_LONG:
						PRecdata = (CHAR*)&pSigValue->varValue.ulValue;
						break;

					default:		
						break;
					}
				}
				else
				{
					return -1;
				}

				for(j = 0; j < pRectSigInfo->ByteNum_Of_Sig; j++)
				{
#ifdef _CPU_BIG_ENDIAN	// ppc
					*(pcDestStr + j) = *(PRecdata + 3 - j);
#else	// for x86
					*(pcDestStr + j) = *(PRecdata + j);
#endif
				}
				pcDestStr += pRectSigInfo->ByteNum_Of_Sig;
				g_Slave_Report.iE1Totalbyte += pRectSigInfo->ByteNum_Of_Sig;
			}
			else if(pRectSigInfo->ByteNum_Of_Sig == 1)
			{
				//get sig info
				*(pcDestStr) =(BYTE)GetEnumSigValue(i + 3,
					pRectSigInfo->Sig_Type,
					pRectSigInfo->Sig_Id,
					"485_SAMP");
				pcDestStr += pRectSigInfo->ByteNum_Of_Sig;
				g_Slave_Report.iE1Totalbyte += pRectSigInfo->ByteNum_Of_Sig;
			}		
		}
	}//End of [RECT_SINGLE_MAP_INFO]

	return 0;
}

/*=============================================================================*
* FUNCTION: RS485RPT_ResponseGetVer()
* PURPOSE  :Response by CID2 (4FH,EAH) in Receiveed data from master   
* RETURN   : VOID
* ARGUMENTS:
* CALLS    :			
* CALLED BY:			SlaveParseData_Response()
*								
* COMMENTS :			4FH:GetVer
*						EAH:Control
*						GetVer and Control	unsed The same method Response!!!!
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
LOCAL VOID RS485RPT_ResponseGetVer(int DataCid2)
{
	BYTE szSendStr[100]={0};
	BYTE lenChk;
	INT32 iSendlen;
	UNUSED(DataCid2);
	sprintf((char *)szSendStr,
		"~%02X%02X%02X%02X%04X\0",
		0x20, 
		(unsigned int)g_SLAVEAddr,
		0xe5,
		0x00, 
		0);
	lenChk		= RPTLenCheckSum((0*2))<<4;
	szSendStr[9]	= RPTHexToAsc((lenChk&0xf0)>>4);
	szSendStr[10]	= RPTHexToAsc(lenChk&0x0f);
	szSendStr[11]	= RPTHexToAsc((0 & 0xf0)>>4);
	szSendStr[12]	= RPTHexToAsc(0 & 0x0f);
	RPTCheckSum(szSendStr);										//Auto plus 4 byte
	iSendlen = 13 + 4 + 1;										//EOI~CID2+CHECKSUM+EOI
	*(szSendStr + iSendlen - 1) = byEOI;							//EOI	
	//RS485_Send(h_485Com,szSendStr,strlen(szSendStr));
	RS485_Write(g_RS485Data.CommPort.hCommPort,
		szSendStr,
		strlen((char*)szSendStr));
}
/*=============================================================================*
* FUNCTION: RS485RPT_ResponseToMaster()
* PURPOSE  :Response by CID2 (E1H) in Receiveed data from master
* RETURN   : 
* ARGUMENTS:
* CALLS    :			
* CALLED BY:			SlaveParseData_Response()
*								
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
LOCAL VOID RS485RPT_E1ResponseToMaster(void)
{
	BYTE		szSendStr[4902]={0};
	INT32		lenChk,i;
	INT32		iSendlen;
	//1	    2	  2	   2	   2	    4
	//SOI  VER   ADR  CID1  CID2/RTN  LENGTH  
	sprintf((char *)szSendStr,
		"~%02X%02X%02X%02X%04X\0", 
		0x20,
		(unsigned int)g_SLAVEAddr,
		0xe5, 
		0x00, 
		0);

	lenChk = RPTLenCheckSum(((g_Slave_Report.iE1Totalbyte) * 2)) << 4;		
	//LENGTH 4 BYTE
	szSendStr[9] = RPTHexToAsc((lenChk & 0xf0)>>4);
	szSendStr[10] = RPTHexToAsc(lenChk & 0x0f);
	szSendStr[11] = RPTHexToAsc((((g_Slave_Report.iE1Totalbyte) * 2) & 0xf0)>>4);
	szSendStr[12] = RPTHexToAsc(((g_Slave_Report.iE1Totalbyte) * 2) & 0x0f);
	//DATAINFO from g_Slave_Report.. 
	for(i = 0; i < g_Slave_Report.iE1Totalbyte; i++)
	{
		sprintf((char*)(szSendStr + DATA_YDN23_REPORT + i * 2),
			"%02X\0",
			g_Slave_Report.SendE1DataInfo[i]);
	}
	//CHECKSUM
	RPTCheckSum(szSendStr);									//Auto plus 4 byte

	iSendlen = DATA_YDN23_REPORT 
		+ g_Slave_Report.iE1Totalbyte * 2 
		+ 4 + 1;
	*(szSendStr + iSendlen - 1) = byEOI;		//EOI

	RS485_Write(g_RS485Data.CommPort.hCommPort,
		szSendStr,
		iSendlen);//strlen(szSendStr)
	//g_Slave_Report.iE1Totalbyte = 0;
}
/*=============================================================================*
* FUNCTION: RS485RPT_ResponseToMaster()
* PURPOSE  :Response by CID2 (E1H) in Receiveed data from master
* RETURN   : 
* ARGUMENTS:
* CALLS    :			
* CALLED BY:			SlaveParseData_Response()
*								
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
LOCAL VOID RS485RPT_ResponseToMaster()
{
	BYTE		szSendStr[4902]={0};
	INT32		lenChk,i;
	INT32		iSendlen;
	//1	    2	  2	   2	   2	    4
	//SOI  VER   ADR  CID1  CID2/RTN  LENGTH  
	sprintf((char *)szSendStr,
		"~%02X%02X%02X%02X%04X\0", 
		0x20,
		(unsigned int)g_SLAVEAddr,
		0xe5, 
		0x00, 
		0);

	lenChk = RPTLenCheckSum(((g_Slave_Report.Totalbyte) * 2)) << 4;		
	//LENGTH 4 BYTE
	szSendStr[9] = RPTHexToAsc((lenChk & 0xf0)>>4);
	szSendStr[10] = RPTHexToAsc(lenChk & 0x0f);
	szSendStr[11] = RPTHexToAsc((((g_Slave_Report.Totalbyte) * 2) & 0xf0)>>4);
	szSendStr[12] = RPTHexToAsc(((g_Slave_Report.Totalbyte) * 2) & 0x0f);
	//DATAINFO from g_Slave_Report.. 
	for(i = 0; i < g_Slave_Report.Totalbyte; i++)
	{
		sprintf((char*)(szSendStr + DATA_YDN23_REPORT + i * 2),
			"%02X\0",
			g_Slave_Report.SendDataInfo[i]);
	}
	//CHECKSUM
	RPTCheckSum(szSendStr);									//Auto plus 4 byte
	/*
	DATA_YDN23:
	SOI---LENGTH	13
	g_Slave_Report.Totalbyte * 2:
	data by Dxiget for send 
	4:
	checksum is 4 byte
	1:
	EOI
	*/
	iSendlen = DATA_YDN23_REPORT 
		+ g_Slave_Report.Totalbyte * 2 
		+ 4 + 1;
	//#if		TST_RS485_DEBUG_REPORT
	//	TRACE("\n*** SENDlenght = %d   Totalbyte = %d \n",iSendlen, g_Slave_Report.Totalbyte);
	//#endif
	*(szSendStr + iSendlen - 1) = byEOI;		//EOI
	//#if		TST_RS485_DEBUG_REPORT
	//TRACE(" ****  SLAVEResponse 111 ToMaster DDDDD  **\n");
	//#endif
	RS485_Write(g_RS485Data.CommPort.hCommPort,
		szSendStr,
		iSendlen);//strlen(szSendStr)
	g_Slave_Report.Totalbyte = 0;
}
/*=============================================================================*
* FUNCTION: SLAVEControl()
* PURPOSE  :textract data field from frame and send to gc
* RETURN   : 
* ARGUMENTS:
*			CHAR*	pcRecStr :	IN,	receive data from  MASTER	
*			CHAR*	SetSigCfg :	IN,	from config file
* CALLS    :			
*
* CALLED BY:			DLLExport BOOL Query()
*								
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
//LOCAL VOID SLAVEControl(RS485_REPORT_CFG_INFO *SetSigCfg, CHAR* pcRecStr)
//{
//	CHAR ctmp;
//	CHAR cDataInfo[4];
//	CHAR *PcData;
//	float fRecData;
//	INT32 iRst;
//	RS485_REPORT_CFG_INFO	*pReportConfig;
//	RS485_SLAVE_REPORT_SIG_INFO	*pReportSigInfo;
//
//	//UNUSED(SetSigCfg);//Global
//
//	//1	  1	  1	  1	   1        2	   X	2   1
//	//1	  2	  2	  2	   2	    4	   2X	4   1
//	//SOI VER ADR CID1 CID2/RTN	LENGTH INFO	SUM	EOI
//	PcData = pcRecStr + 13 + 2;				/*
//											13: SOI VER ADR CID1 CID2/RTN	LENGTH
//											15:	DATAINFO include  COMMAND TYPE and COMMAND DATAF
//											COMMAND TYPE�� 1 BYTE	associate	 	2X=2	  						
//											COMMAND DATAF��4 BYTE	associate		2X=8
//											*/
//
//	fRecData = FixFloatDat(PcData);
//	pReportConfig = &g_Slave_Report.Rs485_Slave_Cfg;
//	pReportSigInfo = pReportConfig->pCtrlSigMapInfo;
//
//	switch(RPTMergeAsc(pcRecStr + 13))//COMMAND TYPE
//	{
//	case 0x80://set slave output voltage
//		
//		iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
//				pReportSigInfo->Equip_Id,
//				DXI_MERGE_SIG_ID(pReportSigInfo->Sig_Type, pReportSigInfo->Sig_Id),
//				sizeof(fRecData),
//				&fRecData,
//				0);
//
//		if(iRst != ERR_OK)
//		{
//			AppLogOut("Slave Report Control", 
//				APP_LOG_ERROR, 
//				"Slave DxiSetData error, EquipId = %d, SigType = %d, SigId = %d!\n", 
//				pReportSigInfo->Equip_Id,
//				pReportSigInfo->Sig_Type,
//				pReportSigInfo->Sig_Id); 
//		}
//		
//		break;
//
//	case 0x81://set slave output total current
//		
//		pReportSigInfo++;						//config info for  the total current
//		iRst = DxiSetData(VAR_A_SIGNAL_VALUE,
//							pReportSigInfo->Equip_Id,
//							DXI_MERGE_SIG_ID(pReportSigInfo->Sig_Type, pReportSigInfo->Sig_Id),
//							sizeof(fRecData),
//							&fRecData,
//							0);
//
//		if(iRst != ERR_OK)
//		{
//			AppLogOut("Slave Report Control", 
//				APP_LOG_ERROR, 
//				"Slave DxiSetData error, EquipId = %d, SigType = %d, SigId = %d!\n", 
//				pReportSigInfo->Equip_Id,
//				pReportSigInfo->Sig_Type,
//				pReportSigInfo->Sig_Id); 
//		}
//		
//		break;
//	default:
//		break;	
//	}
//}
/*=============================================================================*
* FUNCTION: RS485RPT_CheckAddr(CHAR* pcRecvStr)
* PURPOSE  :Check the slave addr
* RETURN   : Accord TRUE  ,Not accord FALSE
* ARGUMENTS:
*						CHAR*	pcRecStr :	IN	
*						
* CALLS    :		
*					
*					
* CALLED BY:			DLLExport BOOL Query()
*								
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
INT32	RS485RPT_CheckAddr(CHAR* pcRecvStr)
{
	if(g_SLAVEAddr != RPTMergeAsc(pcRecvStr + ADDR_YDN23_REPORT))
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}
/*=============================================================================*
* FUNCTION: RS485RPT_CheckEquipCid(CHAR* pcRecvStr)
* PURPOSE  :Check the slave Equip	Type	Cid
* RETURN   : Accord TRUE  ,Not accord FALSE
* ARGUMENTS:
*						CHAR*	pcRecStr :	IN	
*						
* CALLS    :		
*					
*					
* CALLED BY:			DLLExport BOOL Query()
*								
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
INT32	RS485RPT_CheckEquipCid(CHAR* pcRecvStr)
{
	//e5H		CID1
	if(SLAVECID1 != RPTMergeAsc(pcRecvStr + CID1H_YDN23_REPORT))
	{
		TRACE("\n ***** SLAVE  CID1  ERROR *****\n");
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}
/*=============================================================================*
* FUNCTION: RS485RPT_CheckRecvSum(CHAR* pcRecvStr)
* PURPOSE  :Check receive data sum 
* RETURN   : Accord TRUE  ,Not accord FALSE
* ARGUMENTS:
*						CHAR*	pcRecStr :	IN	
*						
* CALLS    :		
*					
*					
* CALLED BY:			DLLExport BOOL Query()
*								
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
INT32	RS485RPT_CheckRecvSum(CHAR* pcRecvStr)
{
	INT32		iLength;
	CHAR		cTmpRecCheck[5];
	/*
	copy checksum of receive data
	to cTmpRecCheck	
	*/
	iLength = (INT32)strlen((char*)pcRecvStr );	
	iLength = g_Slave_Report.iReceiveByteNum; 
	/*
	5: CHECKSUM 4 bytes + EOI 1 byte
	*/
	memcpy(cTmpRecCheck, pcRecvStr + iLength - 5, 4);					
	pcRecvStr[iLength - 5] = 0;
	//Repeat to counted the checksum,Auto plus 4 byte
	RPTCheckSum((BYTE*)pcRecvStr);		

	if((pcRecvStr[iLength - 2]	   != cTmpRecCheck[3]) 
		&& (pcRecvStr[iLength - 3] != cTmpRecCheck[2])
		&& (pcRecvStr[iLength - 4] != cTmpRecCheck[1]) 
		&& (pcRecvStr[iLength - 5] != cTmpRecCheck[0]))
	{
		//cheksum  error
		//TRACE("\n***** SLAVE  cheksum  error *****\n");
		//RS485RPT_ResponseError(0x02);
		return FALSE ;
	}
	else
	{
		return TRUE;
	}
}
/*=============================================================================*
* FUNCTION: RS485RPT_GetFloatChargeVoltage()
* PURPOSE  :Get float charge voltage sig value from master by 0xe1 cmd
* RETURN   : 
* ARGUMENTS:
*						CHAR*	pcRecStr :	IN	
*						
* CALLS    :			
*					
*					
* CALLED BY:			RS485RPT_ParseAndResponse
*								
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
LOCAL float RS485RPT_GetFloatChargeVoltage(CHAR*pcRecvStr)
{
	CHAR * pcPoint;
	//BATERYMANAGEMENTSTATE  
	pcPoint = pcRecvStr + DATA_YDN23_REPORT + 2;//refer to protocol
	return FixFloatDat(pcPoint);
}
/*=============================================================================*
* FUNCTION: RS485RPT_GetBarCode()
* PURPOSE  :Get slave Product Info 
* RETURN   : 
* ARGUMENTS:
*				CHAR*	pcDestStr :	OUT	
* CALLS    :			
*						
*					
* CALLED BY:			DLLExport BOOL Query()
*								
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
LOCAL BOOL RS485RPT_GetBarCode(CHAR* pcDestStr)
{
	ACU_PRODUCT_INFO sAcuProductInfo;
	//PRODUCT_INFO	sAcuSubPrdctInfo;
	INT32 i;
	INT32 nBufLen;
	//1��	szPartNumber 32�ֽ� 
	//2��	szHWVersion  32�ֽ�
	//3��	szSWVersion  32�ֽ�
	//4��	szSerialNumber 64�ֽ�
	g_Slave_Report.Totalbyte	= 0;
	memset(&sAcuProductInfo, 0, sizeof(sAcuProductInfo));

	nBufLen = sizeof(sAcuProductInfo);

	if (DxiGetData(VAR_ACU_PUBLIC_CONFIG,
		ACU_PRODUCT_INFO_GET, 
		0, 
		(int*)(&nBufLen),
		&(sAcuProductInfo),
		0) == ERR_DXI_OK)
	{
		//1.stuff to sending buffer  szPartNumber
		for (i = 0; i < 16; i++)
		{
			pcDestStr[g_Slave_Report.Totalbyte] = sAcuProductInfo.szPartNumber[i];
			g_Slave_Report.Totalbyte += 1;
		}
		//pcDestStr += 32;//Point to sending  buffer for szHWVersion

		//2.stuff to sending buffer  szHWVersion
		for (i = 0; i < 16; i++)
		{
			pcDestStr[g_Slave_Report.Totalbyte] = sAcuProductInfo.szHWRevision[i];
			g_Slave_Report.Totalbyte += 1;
		}
		//pcDestStr += 32;//Point to sending  buffer for szSWRevision

		//3.stuff to sending buffer  szSWRevision
		for (i = 0; i < 16; i++)
		{
			pcDestStr[g_Slave_Report.Totalbyte] = sAcuProductInfo.szSWRevision[i];
			g_Slave_Report.Totalbyte += 1;
		}
		//pcDestStr += 32;//Point to sending  buffer for szACUSerialNo

		for (i = 0; i < 32; i++)
		{
			pcDestStr[g_Slave_Report.Totalbyte] = sAcuProductInfo.szACUSerialNo[i];
			g_Slave_Report.Totalbyte += 1;
			//sAcuSubPrdctInfo->szSerialNumber[i] = sAcuProductInfo->szACUSerialNo[i];
		}

		return SLAVE_DXIGETSIG_SUCCESSFUL;
	}
	else
	{
		TRACE("\n!!!!!!!!!     DxiGetData(VAR_ACU_PUBLIC_CONFIG  error error  !!!!!!!  \n");
		return -1;
	}

}
/*=============================================================================*
* FUNCTION: RS485RPT_ParseAndResponse()
* PURPOSE  :Parse and Received data of master from 485 com
* RETURN   : 
* ARGUMENTS:
*						CHAR*	pcRecStr :	IN	
*						CHAR*	pcDestStr :	OUT	
* CALLS    :			SlaveGetVer()				4FH
*						Slave GetAnolog()			E1H
*						SlaveControl()				EAH
* CALLED BY:			DLLExport BOOL Query()
*								
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
LOCAL VOID RS485RPT_ParseAndResponse(CHAR*pcRecvStr)
{						
	HANDLE		hself;	
	INT32		iGetSlaveBCodeStat;
	INT32		iSlaveRctInfoChange;
	BYTE		bBateryManagementStat;	
	hself		= RunThread_GetId(NULL);

	ASSERT(pcRecvStr != NULL);

	if (!RS485RPT_CheckAddr(pcRecvStr))
	{
		return;
	}

	if (!RS485RPT_CheckEquipCid(pcRecvStr))
	{
		return;
	}

	if (!RS485RPT_CheckRecvSum(pcRecvStr))
	{
		return;
	}

	switch(RPTMergeAsc(pcRecvStr + CID2_YDN23_REPORT))		//CID2		
	{
	case 0x4f:			//Get ver

		//TRACE("******	case 0x4f:	******\n");
		RS485RPT_ResponseGetVer(0x4f);
		break;

	case 0xe1:	
		//TRACE("******	case 0xe1:	******\n");
		bBateryManagementStat = RPTMergeAsc((char*)(g_Slave_Report.RecData + DATA_YDN23_REPORT));

		SetEnumSigValue(BATTERIER_GROUP, 
			BATTERIER_SIG_TYPE,
			BATTERIER_MANAGEMENT_STAT_SIG_ID,
			bBateryManagementStat,
			"485_SAMP");

		iSlaveRctInfoChange = GetEnumSigValue(2,
			2,
			37,
			"485_SAMP");
		if (iSlaveRctInfoChange == TRUE)
		{
			siSlaveRectChage++;//In the RS485RPT_ E1GetGroupSysRectSig() Set 0
		}

		Timer_Reset(hself, ID_TIME_RPT);

		//Get siginfo2 refer to protocol
		//no used!
		//g_Slave_Report.fFloatChargeVoltageSigVelue = 
		//			RS485RPT_GetFloatChargeVoltage(g_Slave_Report.RecData);

		if(ERR_MUTEX_OK == Mutex_Lock(st_RptE1E3Data.iMutexE1TwoDataSyncLock,
			MAX_MUTEX_TIME_WAITING_TIME_OUT))
		{
			RS485_Write(g_RS485Data.CommPort.hCommPort,
				st_RptE1E3Data.SendE1DataTwo,
				st_RptE1E3Data.iPackDataLenghTwo);

			Mutex_Unlock(st_RptE1E3Data.iMutexE1TwoDataSyncLock);
		}
		else
		{
			TRACE("\n !! ####### E1		E1		E1 DATA NO  REDAY  ####### !!\n");
		}
		break;

	case 0xe3:

		TRACE("******	case   0xe3:	******\n");
		if(ERR_MUTEX_OK == Mutex_Lock(st_RptE1E3Data.iMutexE3TwoDataSyncLock,
			MAX_MUTEX_TIME_WAITING_TIME_OUT))
		{
			//TRACE("\n iLen:%d, %s \n",st_RptE1E3Data.iPackE3DataLenghTwo, st_RptE1E3Data.SendE3DataTwo);
			RS485_Write(g_RS485Data.CommPort.hCommPort,
				st_RptE1E3Data.SendE3DataTwo,
				st_RptE1E3Data.iPackE3DataLenghTwo);
			TRACE("\n	RPT Len=%d E3VAL=%s \n",st_RptE1E3Data.iPackE3DataLenghTwo,st_RptE1E3Data.SendE3DataTwo);


			Mutex_Unlock(st_RptE1E3Data.iMutexE3TwoDataSyncLock);
		}
		else
		{
			TRACE("\n !! ####### E3		E3		E3 DATA NOT REDAY  ####### !!\n");
		}
		break;

	case 0xec:
		//Get Barcode
		TRACE("******	case   0xec:	******\n");
		iGetSlaveBCodeStat = RS485RPT_GetBarCode(g_Slave_Report.SendDataInfo);

		if (SLAVE_DXIGETSIG_SUCCESSFUL == iGetSlaveBCodeStat)
		{
			RS485RPT_ResponseToMaster();
		}
		else
		{
			//if error must send to master some data.
			g_Slave_Report.Totalbyte = sizeof(ACU_PRODUCT_INFO);
			RS485RPT_ResponseToMaster();
		}
		break;

	case 0x45:
		//It sends some control	signal, refer to protocol
		TRACE("******	case   0x45: CTRL 	******\n");
		//It sends some control	signal, refer to protocol
		RS485RPT_ProcessCtrlCmd(&g_Slave_Report.Rs485_Slave_Cfg,
			(CHAR*)(g_Slave_Report.RecData));
		break;

	default:
		//Invalid cid2	
		//RS485RPT_ResponseError(0x04);
		AppLogOut("SLAVE receive cid2 error", 
			APP_LOG_UNUSED,
			"[%s]-[%d] ERROR: failed to invalid cid2 \n\r",
			__FILE__, __LINE__);
		break;		
	}
}
/*==========================================================================*
* FUNCTION : ReceiveReconfigCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-10-10 16:55
*==========================================================================*/
static BOOL ReceiveReconfigCmd(void)
{
	if(GetDwordSigValue(SYS_GROUP_EQUIPID, 
		SIG_TYPE_SETTING,
		RS485_SYS_RECONFIG_SIG_ID,
		"485_SAMP") > 0)
	{
		SetDwordSigValue(SYS_GROUP_EQUIPID, 
			SIG_TYPE_SETTING,
			RS485_SYS_RECONFIG_SIG_ID,
			0,
			"485_SAMP");
		return TRUE;
	}

	return FALSE;
}
/*=====================================================================*
* Function name: RS485WaitReadable
* Description  : wait RS485 data ready
* Arguments    : int fd	: 
*                int TimeOut: seconds 
* Return type  : int : 1: data ready, 0: timeout,Allow get mode or auto config
*
* Create       : Ilockteng		2009-05-19
* Comment(s)   : 
*--------------------------------------------------------------------*/
LOCAL INT32 RS485WaitReadable(INT32 fd, INT32 nmsTimeOut)
{
	fd_set readfd;
	struct timeval timeout;

	while (nmsTimeOut > 0)
	{
		// need add ClearWDT() here.
		if (nmsTimeOut < (5*1000))
		{
			timeout.tv_usec = nmsTimeOut % 1000 * 1000;		/* usec     */
			timeout.tv_sec  = nmsTimeOut / 1000;			/* seconds  */

			nmsTimeOut = 0;
		}
		else
		{
			timeout.tv_usec = 0;							/* usec     */
			timeout.tv_sec  = 5;							/* seconds  */

			RUN_THREAD_HEARTBEAT();

			nmsTimeOut -= (5 * 1000);
		}

	}

	FD_ZERO(&readfd);				/*  Initializes the set to the NULL set. */
	FD_SET(fd, &readfd);			/*  Adds descriptor s to set. */

	select(fd + 1, &readfd, NULL, NULL, &timeout);

	if(FD_ISSET(fd, &readfd))	
	{
		return TRUE;//1: data ready, can be read
	}
	else
	{
		return FALSE;//0: Data not ready, can't read!!but,Allow get mode or auto config
	}
}

/*=============================================================================*
* FUNCTION: RS485RPT_GetAddr
* PURPOSE  : Get slave equipment address from web
* RETURN   : void
* ARGUMENTS:
* CALLS    : 
* CALLED BY: 
*							DLLExport BOOL Query()
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-05 13:59
*==========================================================================*/
INT32 RS485RPT_GetAddr(void)
{
	SIG_ENUM		enumSlaveAddr;
	//get slave address note:get address from app service 
	enumSlaveAddr = GetEnumSigValue(RS485_GET_ADDR_EQUIPID,
		SIG_TYPE_SETTING,
		RS485_RUNNING_ADDR_SIGID,
		"485_SAMP");

	g_SLAVEAddr = enumSlaveAddr + SLAVE_ADDR_OFFSET_START;

	//TRACE("\n *****	$$	RS485RPT_GetAddr	= %d \n",g_SLAVEAddr);

	if((g_SLAVEAddr < SLAVE_ADDR_ENUM_0_NUMBER_201)
		|| (g_SLAVEAddr > SLAVE_ADDR_ENUM_2_NUMBER_203))
	{
		TRACE("\n****Please check the slaves address *****\n");
		return FALSE;
	}

	return TRUE;
}

/*=============================================================================*
* FUNCTION: RS485RPT_GetRunningMode
* PURPOSE  : Get slave equipment address from web
* RETURN   : SIG_ENUM
* ARGUMENTS:
* CALLS    : 
* CALLED BY: 
*							DLLExport BOOL Query()
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-05 13:59
*==========================================================================*/
SIG_ENUM  RS485RPT_GetRunningMode(void)
{
	return 	GetEnumSigValue(RS485_GET_MODE_EQUIPID,
		SIG_TYPE_SETTING,
		RS485_RUNNING_MODE_SIGID,
		"485_SAMP");
}
/*=============================================================================*
* FUNCTION: RS485RPT_NeedCommProc
* PURPOSE  :  Need process the rs485Comm port
* RETURN   : INT32
* ARGUMENTS:
* CALLS    : 
* CALLED BY: 
*							DLLExport BOOL Query()
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-05 13:59
*==========================================================================*/
INT32 RS485RPT_NeedCommProc(void)
{
	if(g_RS485Data.CommPort.enumAttr != RS485_ATTR_NONE
		|| g_RS485Data.CommPort.enumBaud != RS485_ATTR_19200
		|| g_RS485Data.CommPort.bOpened != TRUE)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
/*=============================================================================*
* FUNCTION: RS485RPT_ReopenPort
* PURPOSE  :  Repeat open the rs485comm
* RETURN   : INT32
* ARGUMENTS:
* CALLS    : 
* CALLED BY: 
*							DLLExport BOOL Query()
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-05 13:59
*==========================================================================*/
INT32 RS485RPT_ReopenPort(void)
{
	INT32			iErrCode;

	if(g_RS485Data.CommPort.hCommPort && g_RS485Data.CommPort.bOpened)
	{
		RS485_Close(g_RS485Data.CommPort.hCommPort);
	}

	g_RS485Data.CommPort.hCommPort 
		= RS485_Open("19200, n, 8, 1", RS485_READ_WRITE_TIMEOUT, (int*)(&iErrCode));

	if(iErrCode == ERR_COMM_OK)
	{
		g_RS485Data.CommPort.bOpened = TRUE;
		g_RS485Data.CommPort.enumAttr = RS485_ATTR_NONE;
		g_RS485Data.CommPort.enumBaud = RS485_ATTR_19200;
	}
	else
	{
		g_RS485Data.CommPort.bOpened = FALSE;	
		AppLogOut("report.c",
			APP_LOG_ERROR,
			"[%s]-[%d]ERROR:Failed to RS485RPT_ReopenPort\n\r", 
			__FILE__, __LINE__);

		TRACE("\n The  rs485 Comm fail to opening error\n");
		//return;
	}
	Sleep(1);
	return 0;
}

/*=============================================================================*
* FUNCTION: RS485RPT_ReadFromMaster
* PURPOSE  : Read Data from Master  
* RETURN   : > 0 means succeed 
* ARGUMENTS:
* CALLS    : 
* CALLED BY: 
*							DLLExport BOOL Query()
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-05 13:59
*==========================================================================*/
INT32 RS485RPT_ReadFromMaster()
{
	INT32			iReceiveCount;
	RS485_DRV*		pPort				= (RS485_DRV *)g_RS485Data.CommPort.hCommPort;
	INT32			fd					= pPort->fdSerial;

	g_Slave_Report.Totalbyte = 0;
	g_Slave_Report.iReceiveByteNum  = 0;
	iReceiveCount			 = -1;

	memset(g_Slave_Report.RecData, '\0', SLAVE_RECEIVE_BUF_MAX_NUM);

	if (RS485WaitReadable(fd, 800) > 0)
	{
		TRACE(" SLAVE SELECT OK !!!\n");
		if(!RecvDataFromMASTER(g_RS485Data.CommPort.hCommPort,
			(CHAR*)(g_Slave_Report.RecData),
			&iReceiveCount))
		{
			g_Slave_Report.Totalbyte = 0;
			memset(g_Slave_Report.RecData, '\0', SLAVE_RECEIVE_BUF_MAX_NUM);
			return	-1;
		}
		else
		{
			if (!RS485RPT_CheckAddr((CHAR*)(g_Slave_Report.RecData)))
			{
				memset(g_Slave_Report.RecData, '\0', SLAVE_RECEIVE_BUF_MAX_NUM);
				g_Slave_Report.Totalbyte = 0;
				return -1;//iRecordReturnFalseTimes++
			}
			else
			{	
				g_Slave_Report.iReceiveByteNum = iReceiveCount;
				return	iReceiveCount;
			}
		}
	}

	return	-1;
}

int GetE1Onedata(void)
{
	INT32 iRst;
	INT32		lenChk,i;
	INT32		iSendlen;

	memset(st_RptE1E3Data.SendE1DataOne,'\0',SLAVE_SEND_BUF_MAX_NUM);

	st_RptE1E3Data.iPackDataFlagOne = FALSE;
	iRst = RS485RPT_E1GetGroupSysRectSig(&g_Slave_Report.Rs485_Slave_Cfg,
		(CHAR*)(g_Slave_Report.SendE1DataInfo));	

	if (iRst == 0)
	{
		sprintf((char *)st_RptE1E3Data.SendE1DataOne,
			"~%02X%02X%02X%02X%04X\0", 
			0x20,
			(unsigned int)g_SLAVEAddr,
			0xe5, 
			0x00, 
			0);

		lenChk = RPTLenCheckSum(((g_Slave_Report.iE1Totalbyte) * 2)) << 4;	

		//LENGTH 4 BYTE
		st_RptE1E3Data.SendE1DataOne[9] = RPTHexToAsc((lenChk & 0xf0)>>4);
		st_RptE1E3Data.SendE1DataOne[10] = RPTHexToAsc((((g_Slave_Report.iE1Totalbyte) * 2) >> 8) & 0x0f);
		st_RptE1E3Data.SendE1DataOne[11] = RPTHexToAsc((((g_Slave_Report.iE1Totalbyte) * 2) & 0xf0)>>4);
		st_RptE1E3Data.SendE1DataOne[12] = RPTHexToAsc(((g_Slave_Report.iE1Totalbyte) * 2) & 0x0f);

		//DATAINFO from g_Slave_Report.. 
		for(i = 0; i < g_Slave_Report.iE1Totalbyte; i++)
		{
			sprintf((char*)(st_RptE1E3Data.SendE1DataOne + DATA_YDN23_REPORT + i * 2),
				"%02X\0",
				g_Slave_Report.SendE1DataInfo[i]);
		}

		//CHECKSUM
		RPTCheckSum(st_RptE1E3Data.SendE1DataOne);									//Auto plus 4 byte

		iSendlen = DATA_YDN23_REPORT 
			+ g_Slave_Report.iE1Totalbyte * 2 
			+ 4 + 1;
		*(st_RptE1E3Data.SendE1DataOne + iSendlen - 1) = byEOI;		//EOI

		st_RptE1E3Data.iPackDataFlagOne = TRUE;
		st_RptE1E3Data.iPackDataLenghOne = iSendlen;
		return TRUE;
	}
	else
	{
		st_RptE1E3Data.iPackDataFlagOne = FALSE;
		return FALSE;
	}

}

int  GetE3Onedata()
{
	INT32 iRst;
	INT32		lenChk,i;
	INT32		iSendlen;

	memset(st_RptE1E3Data.SendE3DataOne,'\0',SLAVE_SEND_BUF_MAX_NUM);

	iRst = RS485RPT_GetRectSig(&g_Slave_Report.Rs485_Slave_Cfg,
		(CHAR*)(g_Slave_Report.SendE3DataInfo));
	if (iRst == 0)
	{
		sprintf((char *)st_RptE1E3Data.SendE3DataOne,
			"~%02X%02X%02X%02X%04X\0", 
			0x20,
			(unsigned int)g_SLAVEAddr,
			0xe5, 
			0x00, 
			0);

		lenChk = RPTLenCheckSum(((g_Slave_Report.iE3Totalbyte) * 2)) << 4;	
		//LENGTH 4 BYTE
		st_RptE1E3Data.SendE3DataOne[9] = RPTHexToAsc((lenChk & 0xf0)>>4);
		st_RptE1E3Data.SendE3DataOne[10] = RPTHexToAsc((((g_Slave_Report.iE3Totalbyte) * 2) >> 8) & 0x0f);
		st_RptE1E3Data.SendE3DataOne[11] = RPTHexToAsc((((g_Slave_Report.iE3Totalbyte) * 2) & 0xf0)>>4);
		st_RptE1E3Data.SendE3DataOne[12] = RPTHexToAsc(((g_Slave_Report.iE3Totalbyte) * 2) & 0x0f);

		//DATAINFO from g_Slave_Report.. 
		for(i = 0; i < g_Slave_Report.iE3Totalbyte; i++)
		{
			sprintf((char*)(st_RptE1E3Data.SendE3DataOne + DATA_YDN23_REPORT + i * 2),
				"%02X\0",
				g_Slave_Report.SendE3DataInfo[i]);
		}

		//CHECKSUM
		RPTCheckSum(st_RptE1E3Data.SendE3DataOne);									//Auto plus 4 byte

		iSendlen = DATA_YDN23_REPORT 
			+ g_Slave_Report.iE3Totalbyte * 2 
			+ 4 + 1;

		*(st_RptE1E3Data.SendE3DataOne + iSendlen - 1) = byEOI;		//EOI

		st_RptE1E3Data.iPackE3DataLenghOne = iSendlen;
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

int RPT_PackE1E3Data()
{	
	INT32			iE1Rst;
	INT32			iE3Rst;
	INT32			hself;
	static	BOOL bCtrlTimes = TRUE;

	st_RptE1E3Data.iPackDataFlagOne = FALSE;
	st_RptE1E3Data.iPackDataFlagTwo = FALSE;
	st_RptE1E3Data.iPackDataLenghOne = 0;
	st_RptE1E3Data.iPackDataLenghTwo = 0;
	hself			= RunThread_GetId(NULL);
	iE1Rst			= GetE1Onedata();
	iE3Rst			= GetE3Onedata();

	//Get second E1data
	if (iE1Rst)
	{
		if (Mutex_Lock(st_RptE1E3Data.iMutexE1TwoDataSyncLock,
			MAX_MUTEX_TIME_WAITING_TIME_OUT) == ERR_MUTEX_OK)
		{
			//Copy E1 data to SendE1DataTwo
			memset(st_RptE1E3Data.SendE1DataTwo, '\0',SLAVE_SEND_BUF_MAX_NUM);

			st_RptE1E3Data.iPackDataFlagTwo = TRUE;
			st_RptE1E3Data.iPackDataLenghTwo = st_RptE1E3Data.iPackDataLenghOne;

			if (st_RptE1E3Data.iPackDataLenghOne < SLAVE_SEND_BUF_MAX_NUM)
			{
				memcpy(st_RptE1E3Data.SendE1DataTwo, st_RptE1E3Data.SendE1DataOne, (st_RptE1E3Data.iPackDataLenghOne));
			}
			else
			{
				memcpy(st_RptE1E3Data.SendE1DataTwo, st_RptE1E3Data.SendE1DataOne, SLAVE_SEND_BUF_MAX_NUM);
			}

			Mutex_Unlock(st_RptE1E3Data.iMutexE1TwoDataSyncLock);
		}
	}
	else
	{
		//Needn't process!!
	}

	if (iE3Rst)
	{
		if (Mutex_Lock(st_RptE1E3Data.iMutexE3TwoDataSyncLock,
			MAX_MUTEX_TIME_WAITING_TIME_OUT) == ERR_MUTEX_OK)
		{
			//Copy E3 data to SendE3DataTwo
			memset(st_RptE1E3Data.SendE3DataTwo, '\0',SLAVE_SEND_BUF_MAX_NUM);

			st_RptE1E3Data.iPackE3DataLenghTwo = st_RptE1E3Data.iPackE3DataLenghOne;

			if (st_RptE1E3Data.iPackE3DataLenghOne < SLAVE_SEND_BUF_MAX_NUM)
			{
				memcpy(st_RptE1E3Data.SendE3DataTwo, st_RptE1E3Data.SendE3DataOne, (st_RptE1E3Data.iPackE3DataLenghOne));
			}
			else
			{
				memcpy(st_RptE1E3Data.SendE3DataTwo, st_RptE1E3Data.SendE3DataOne, SLAVE_SEND_BUF_MAX_NUM);
			}

			Mutex_Unlock(st_RptE1E3Data.iMutexE3TwoDataSyncLock);
		}
	}
	else
	{
		//Needn't process!!
	}

	Sleep(20);

	while (!g_bExitNotification)	
	{
		st_RptE1E3Data.bE1E3RPTRunning = TRUE;

		if (RS485_RUN_MODE_SLAVE == g_RS485Data.enumRunningMode)
		{
			Sleep(100);
			RunThread_Heartbeat(hself);

			//Get first E1data
			iE1Rst = GetE1Onedata();

			if (iE1Rst)
			{
				//Get second E1data
				if (Mutex_Lock(st_RptE1E3Data.iMutexE1TwoDataSyncLock,
					MAX_MUTEX_TIME_WAITING_TIME_OUT) == ERR_MUTEX_OK)
				{
					//Copy E1 data to SendE1DataTwo
					memset(st_RptE1E3Data.SendE1DataTwo, '\0',SLAVE_SEND_BUF_MAX_NUM);

					st_RptE1E3Data.iPackDataFlagTwo = TRUE;
					st_RptE1E3Data.iPackDataLenghTwo = st_RptE1E3Data.iPackDataLenghOne;

					if (st_RptE1E3Data.iPackDataLenghOne < SLAVE_SEND_BUF_MAX_NUM)
					{
						memcpy(st_RptE1E3Data.SendE1DataTwo, st_RptE1E3Data.SendE1DataOne, (st_RptE1E3Data.iPackDataLenghOne));
					}
					else
					{
						memcpy(st_RptE1E3Data.SendE1DataTwo, st_RptE1E3Data.SendE1DataOne, SLAVE_SEND_BUF_MAX_NUM);
					}

					Mutex_Unlock(st_RptE1E3Data.iMutexE1TwoDataSyncLock);
				}
			}

			Sleep(100);

			if(bCtrlTimes)
			{
				Sleep(100);
				iE3Rst	= GetE3Onedata();

				if (iE3Rst)
				{
					if (Mutex_Lock(st_RptE1E3Data.iMutexE3TwoDataSyncLock,
						MAX_MUTEX_TIME_WAITING_TIME_OUT) == ERR_MUTEX_OK)
					{
						//Copy E3 data to SendE3DataTwo
						memset(st_RptE1E3Data.SendE3DataTwo, '\0',SLAVE_SEND_BUF_MAX_NUM);

						st_RptE1E3Data.iPackE3DataLenghTwo = st_RptE1E3Data.iPackE3DataLenghOne;

						if (st_RptE1E3Data.iPackE3DataLenghOne < SLAVE_SEND_BUF_MAX_NUM)
						{
							memcpy(st_RptE1E3Data.SendE3DataTwo, st_RptE1E3Data.SendE3DataOne, (st_RptE1E3Data.iPackE3DataLenghOne));
						}
						else
						{
							memcpy(st_RptE1E3Data.SendE3DataTwo, st_RptE1E3Data.SendE3DataOne, SLAVE_SEND_BUF_MAX_NUM);
						}

						Mutex_Unlock(st_RptE1E3Data.iMutexE3TwoDataSyncLock);
					}
				}
			}

			bCtrlTimes =(!bCtrlTimes);  
		}//end if(Slave mode)
		else
		{
			Sleep(300);
			RunThread_Heartbeat(hself);
		}
	}

	Mutex_Destroy(st_RptE1E3Data.iMutexE3TwoDataSyncLock);
	Mutex_Destroy(st_RptE1E3Data.iMutexE1TwoDataSyncLock);
	st_RptE1E3Data.bE1E3RPTRunning = FALSE;
	return 0;
}

/*=============================================================================*
* FUNCTION: RS485_TimeOutSync()
* PURPOSE  : In Slave mode report data
* RETURN   : void
* ARGUMENTS:
* CALLS    : 
* CALLED BY: 
*							DLLExport BOOL Query()
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-05 13:59
*==========================================================================*/
INT32 RS485_TimeOutSync()
{
	BYTE	bBateryManagementStat;	
	float	fFloatChargeVoltageSigVelue;
	float	fRctLimit = 121;
	if (RS485_RUN_MODE_SLAVE == g_RS485Data.enumRunningMode)
	{
		fFloatChargeVoltageSigVelue = GetFloatSigValue(BATTERIER_GROUP,
			2,			//SIGTYPE
			16,			//SIGID
			"485_SAMP");
#define  RECT_GROUP_EQUIP_ID	2
#define  RECT_G_SIG_TYPE		1
#define  RECT_SIG_ID			2

		SetFloatSigValue(RECT_GROUP_EQUIP_ID, 
			RECT_G_SIG_TYPE,								//SIGTYPE
			RECT_SIG_ID,									//SIGID
			fFloatChargeVoltageSigVelue,
			"485_SAMP");

		bBateryManagementStat = 0;										//float charge
		SetEnumSigValue(BATTERIER_GROUP, 
			BATTERIER_SIG_TYPE,
			BATTERIER_MANAGEMENT_STAT_SIG_ID,
			bBateryManagementStat,
			"485_SAMP");

		//2010/09/28��485����ͨ���ж������£����ӻ�ģ������������Ϊ121
		fRctLimit = 121;
		SetFloatSigValue(RECT_GROUP_EQUIP_ID, 
			RECT_G_SIG_TYPE,								//SIGTYPE 1 ctrl
			1,												//SIGID		Current limit control
			fRctLimit,
			"485_SAMP");
		TRACE("\n ****** Communication fails  with Master ******* \n");

	}
	else
	{
		//Sleep(100);
	}
	return 0;
}

static void WaitRPTthreadExit(void)
{
	//INT32	i;

	for (;;)
	{
		if (!st_RptE1E3Data.bE1E3RPTRunning)
		{
			return;
		}
		else
		{
			//wait RPT_PackE1E3Data  is Exist
		}
	}
}

/*=============================================================================*
* FUNCTION: RS485_RunAsSlave
* PURPOSE  : In Slave mode report data
* RETURN   : void
* ARGUMENTS:
* CALLS    : 
* CALLED BY: 
*							DLLExport BOOL Query()
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-05 13:59
*==========================================================================*/
extern BOOL	g_bExitNotification;
DWORD	RS485_RunAsSlave(void)
{
	//INT32			i;
	HANDLE			hself;
	HANDLE			iRst;
	INT32			iReceiveCount;

	hself			= RunThread_GetId(NULL);

__RPT_CREAT_LOCK:

	st_RptE1E3Data.iMutexE1TwoDataSyncLock = Mutex_Create(TRUE);
	if (st_RptE1E3Data.iMutexE1TwoDataSyncLock == NULL)
	{
		AppLogOut("Create lock",
			APP_LOG_ERROR, 
			"[%s]-[%d]ERROR:Failed to Mutex_Create Lock iMutexE1TwoDataSyncLock! \n\r", 
			__FILE__, __LINE__);
		Sleep(500);
		goto __RPT_CREAT_LOCK;
	}

	st_RptE1E3Data.iMutexE3TwoDataSyncLock = Mutex_Create(TRUE);
	if (st_RptE1E3Data.iMutexE3TwoDataSyncLock == NULL)
	{
		AppLogOut("Create lock",
			APP_LOG_ERROR, 
			"[%s]-[%d]ERROR:Failed to Mutex_Create Lock iMutexE3TwoDataSyncLock! \n\r", 
			__FILE__, __LINE__);
		Sleep(500);
		goto __RPT_CREAT_LOCK;
	}

	Sleep(20);

	//Load the private cfg
	if(LoadRS485SlaveModeConfig() != ERR_CFG_OK)
	{
		AppLogOut("Load Slave Cfg",
			APP_LOG_ERROR, 
			"[%s]-[%d]ERROR:Failed to load SLAVE Private Config!\n\r", 
			__FILE__, __LINE__);

		RS485RPT_FreeCfgMemory();

		return FALSE;
	}

	//Get address
	RS485RPT_GetAddr();


	iRst = RunThread_Create("RPT_PACK",
		(RUN_THREAD_START_PROC)RPT_PackE1E3Data,
		NULL,
		NULL,
		0);
	if (NULL == iRst)
	{
		AppLogOut("Creat RPT pack data",
			APP_LOG_ERROR, 
			"[%s]-[%d]ERROR:Failed to Creat RPT pack data!\n\r", 
			__FILE__, __LINE__);
		RS485RPT_FreeCfgMemory();
		return 1;
	}

	Sleep(50);
	RS485RPT_GetAddr();

	TRACE("\n     ## SLAVE THREAD  !!! OK !!! ## \n");

	Timer_Set(hself, ID_TIME_RPT, 60 * 1000, (ON_TIMER_PROC)RS485_TimeOutSync, NULL);
	RS485_TimeOutSync();

	while(!g_bExitNotification)	
	{
		//In  the  Slave  Mode And Rs485 Query process ok
		if((RS485_RUN_MODE_SLAVE == g_RS485Data.enumRunningMode)
			&& (!g_RS485Data.bNeedReInitModeModify))
		{
			//Heartbeat
			RunThread_Heartbeat(hself);	

			if(RS485RPT_NeedCommProc())
			{
				RS485RPT_ReopenPort();
			}

			//Get address
			RS485RPT_GetAddr();

			iReceiveCount = RS485RPT_ReadFromMaster();

			if (iReceiveCount > 0)//successful
			{
				RS485RPT_ParseAndResponse((CHAR*)(g_Slave_Report.RecData));	
			}
			else
			{
				//Time out will to RS485_TimeOutSync process!
			}

		}//End of in Slave mode
		else
		{
			//g_Slave_Report.bConnectionToMaster = FALSE;
			RunThread_Heartbeat(hself);	//Heartbeat
			Sleep(1000);
		}
	}//End will

	WaitRPTthreadExit();

	Timer_Kill(hself, ID_TIME_RPT);
	//Exit
	RS485RPT_FreeCfgMemory();
	return 0;
}

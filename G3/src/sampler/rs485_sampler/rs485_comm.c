/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : NGMC-HF(Next Generation Controller - High Feature)
*
*  FILENAME : rs485_comm.c
*  CREATOR  : Frank Cao                DATE: 2009-06-02 11:03
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/

#include <sys/file.h>	// for flock()

#include "stdsys.h"		/* all standard system head files			*/
#include "basetypes.h"
#include "pubfunc.h"
#include "rs485_comm.h"
#include "new.h"
#include "err_code.h"
//Frank Wu,20150318, for SoNick Battery
#include "rs485_main.h"
extern RS485_SAMPLER_DATA		g_RS485Data;

#ifndef INIT_TIMEOUTS

#define INIT_TIMEOUTS(to, rto, wto)	((to).nReadTimeout = (rto),		\
	(to).nWriteTimeout = (wto),	\
	(to).nIntervalTimeout = TIMEOUT_INTERVAL_CHAR)
#endif
extern void AppLogOut( const char *pszTaskName, int iLogLevel, 
		const char *pszFormat, ...);

//G3_OPT [debug info], by Lin.Tao.Thomas
//#define TRACE_COSTTIME_RS485
// add by wankun 2008/06/13
#define RTS_MAJOR			246  

#define IOC_SET_SEND		_IOW(RTS_MAJOR, 1, unsigned long)
#define IOC_SET_RECEIVE		_IOW(RTS_MAJOR, 2, unsigned long)
// ended by wankun 2008/06/13

/*==========================================================================*
* FUNCTION : cfsetdatabits
* PURPOSE  : set the data bits
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: struct termios  *options  : 
*            int             nDataBits : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Frank Mao                DATE: 2004-09-17 11:09
*==========================================================================*/
static int cfsetdatabits(struct termios *options, int nDataBits)
{
	int n;
	int nUnixDataBits[] = { CS5, CS6, CS7, CS8 };

	n = ((nDataBits >= 5) && (nDataBits <= 8)) ? nUnixDataBits[nDataBits-5] : CS8;

	options->c_cflag &= ~CSIZE;
	options->c_cflag |= n;

	return n;
}


/*==========================================================================*
* FUNCTION : cfsetparity
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: struct termios  *options : 
*            int             nParity  : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Frank Mao                DATE: 2004-09-17 11:10
*==========================================================================*/
static int cfsetparity(struct termios *options, int nParity)
{
	options->c_cflag &= ~PARENB; /* default, no parity */

	if (nParity == 1) // odd
	{
		options->c_cflag |= PARENB;
		options->c_cflag |= PARODD;
	}

	else if (nParity == 2) // even
	{
		options->c_cflag |= PARENB;
		options->c_cflag &= ~PARODD;
	}

	if (nParity != 0)	// enable parity, 2004-10-8
	{
		options->c_iflag |= INPCK;
	}
	else	// disable
	{
		options->c_iflag &= ~INPCK;
	}

	return nParity;
}

/*==========================================================================*
* FUNCTION : cfsetstopbits
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: struct termios  *options : 
*            int             nStop    : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Frank Mao                DATE: 2004-09-17 11:10
*==========================================================================*/
static int cfsetstopbits(struct termios *options, int nStop)
{
	if (nStop == 1)
	{
		options->c_cflag &= ~CSTOPB;    // default is 1 stop bits
	}
	else if (nStop == 2)
	{
		options->c_cflag |= CSTOPB;
	}

	return nStop;
}




// the linux supported baud rates of serial
struct SUnixBaudList
{
	unsigned long ulUnixBaud;
	int     nNormalBaud;
};

static struct SUnixBaudList s_UnixBaudList[] =
{
	{ B50	  ,  50 },
	{ B75	  ,  75 },
	{ B110	  ,  110 },
	{ B134	  ,  134 },
	{ B150	  ,  150 },
	{ B200	  ,  200 },
	{ B300	  ,  300 },
	{ B600	  ,  600 },
	{ B1200	  ,  1200 },
	{ B1800	  ,  1800 },
	{ B2400	  ,  2400 },
	{ B4800	  ,  4800 },
	{ B9600	  ,  9600 },
	{ B19200  ,  19200 },
	{ B38400  ,  38400 },
	{ B57600  ,  57600 },
	{ B115200 ,  115200 },
	{ B230400 ,  230400 },
	{ B460800 ,  460800 },
	{ B500000 ,  500000 },
	{ B576000 ,  576000 },
	{ B921600 ,  921600 },
	{ B1000000,  1000000 },
	{ B1152000,  1152000 },
	{ B1500000,  1500000 },
	{ B2000000,  2000000 },
	{ B2500000,  2500000 },
	{ B3000000,  3000000 },
	{ B3500000,  3500000 },
	{ B4000000,  4000000 },
	{ 0,         0       } /* end flag */
};

#define MIN_SERIAL_BAUD		(s_UnixBaudList[0].nNormalBaud)
#define	MAX_SERIAL_BAUD		(s_UnixBaudList[ITEM_OF(s_UnixBaudList) - 2].nNormalBaud)

static unsigned long ConvertBaudrateToUnix(int baud)
{
	struct SUnixBaudList    *p = s_UnixBaudList;

	while((p->nNormalBaud != baud) && (p->nNormalBaud != 0))
	{
		p ++;
	}

	return p->ulUnixBaud;
}

#ifndef	SPACE 
#define SPACE  0x20
#endif

#define COMMA_SEPARATOR		','
#define COLON_SERARATOR		':'

static char *SplitText(IN char *S, OUT char *D, IN int cSep);

/*==========================================================================*
* FUNCTION : SplitText
* PURPOSE  : split text S with separator cSep(,), and save the splitted text
*            to D.
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN char   *S   : the source string to be splitted
*            OUT char  *D   : to save the splitted string
*            IN int   cSep : the separator
* RETURN   : static char *: the string pointer is not splitted.
* COMMENTS : 
* CREATOR  : Mao Fuhua                DATE: 2004-09-16 20:19
*==========================================================================*/
static char *SplitText(IN char *S, OUT char *D, IN int cSep)
{
	int i = 0;

	// 1. get rid of the front space
	while(*S == SPACE)
	{
		S ++;
	}

	// 2. copy the non-space char
	while( *S && *S != (char)cSep)
	{
		D[i ++] = *S ++;
	}

	if (*S == (char)cSep)    // skip COMMA_SEPARATOR
	{
		S ++;
	}

	// 3. get rid of the rear space
	i --;
	while((i >= 0) && (D[i] == SPACE))
	{
		i --;
	}

	i ++;

	D[i]=0;

	return S;
}



/*==========================================================================*
* FUNCTION : ParseRs485Params
* PURPOSE  : Parse the baud rate, data bits and other attributes of the serial
*            from opening parameters
*            open comm with b,p,d,s format, for dialling port is b,p,d,s:phone
*            b=baud, e.g., 9600,
*            p=parity, n, o, e is allowed.
*            d=data bits, 5-8 is valid
*            s=stop bits, 1-2 is valid
*            for phone number, the '-','+',' ' are allowed in the phone number,
*            but they will be ignored.
* CALLS    : 
* CALLED BY: Serial_CommOpen
* ARGUMENTS: IN char               *pSettings : 
*            OUT SERIAL_BAUD_ATTR  *pAttr       : 
* RETURN   : static BOOL : TRUE for success, FALSE for error params
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-06-03 10:15
*==========================================================================*/
static BOOL ParseRs485Params(IN char *pSettings, 
							 OUT RS485_BAUD_ATTR *pAttr)
{

	// the settings like: 1000000,n,8,1:00867559876543211234567
#define MAX_SETTING_LEN		64	// 64 is enough!!
	char p[MAX_SETTING_LEN], *s = pSettings;

	if ((pSettings == NULL) 
		|| (*pSettings == 0) 
		|| strlen(pSettings) >= MAX_SETTING_LEN)	// too long.
	{
		return FALSE;
	}

	// get the baud rate set
	s = SplitText(s, p, COMMA_SEPARATOR);

	pAttr->nBaud = atoi(p);

	pAttr->ulUnixBaud = ConvertBaudrateToUnix(pAttr->nBaud);

	if (pAttr->ulUnixBaud == 0)
	{
		return FALSE;
	}

	// odd or even check
	s = SplitText(s, p, COMMA_SEPARATOR);
	if (p[1] != 0) // too long
	{
		return FALSE;
	}

	if ((p[0] == 'N') || (p[0] == 'n'))
	{
		pAttr->nOdd = 0;
	}
	else if ((p[0] == 'O') || (p[0] == 'o'))
	{
		pAttr->nOdd = 1;
	}
	else if ((p[0] == 'E') || (p[0] == 'e'))
	{
		pAttr->nOdd = 2;
	}
	else
	{
		return FALSE;
	}

	// data bit - only one byte 5-8
	s = SplitText(s, p, COMMA_SEPARATOR);
	if ((p[0] < '5') || (p[0] > '8') || (p[1] != 0))
	{
		return FALSE;
	}

	pAttr->nData = p[0] - '0';

	// stop bit

	s = SplitText(s, p, COMMA_SEPARATOR);

	if (((p[0] == '1') || (p[0] == '2')) && (p[1] == 0))
	{
		pAttr->nStop = p[0] - '0';  // 1 or 2 stop bits
	}
	// 1.5 stop bits
	else if ((p[0] == '1') && (p[1] == '.') && (p[2] == '5') && (p[3] == 0))
	{
		pAttr->nStop = 3;      // 1.5 stop bits
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}




/*==========================================================================*
* FUNCTION : RS485_SetAttr
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN int  fd     : Handle
*            char    *pAttr : 
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-06-03 11:01
*==========================================================================*/
BOOL RS485_SetAttr(IN int fd, IN char *pParam)
{
	struct termios options;

	RS485_BAUD_ATTR  Attr;

	ParseRs485Params(pParam, &Attr);

	/* Get the current options for the port */
	tcgetattr(fd, &options);

	cfsetispeed(&options, Attr.ulUnixBaud);	// Set the input baud rate
	cfsetospeed(&options, Attr.ulUnixBaud);	// set the output baud rate

	//TRACE("RS485 Baudrate is %d. \n\r", pAttr->nBaud);

	//Enable the receiver and set local mode
	options.c_cflag |= (CLOCAL | CREAD);

	//data bits
	cfsetdatabits(&options, Attr.nData);

	//stop bits
	cfsetstopbits(&options, Attr.nStop);

	//parity
	cfsetparity(&options, Attr.nOdd);

	//Disable hardware flow control 
	options.c_cflag &= ~CRTSCTS;

	//Enable data to be processed as raw input
	options.c_lflag &= ~(ICANON | ECHO | ISIG);

	//for can not send out the 0x11,0xD,0xA,0x13 chars.
	options.c_iflag  = 0;

	//Output	
	options.c_oflag &= ~OPOST;

	options.c_cc[VTIME] = 0;
	options.c_cc[VMIN]	= 0;		/* Update the options and do it NOW */

	//Set the new options for the port 
	if (tcsetattr(fd, TCSANOW, &options) != 0)
	{
		return FALSE;
	}

	return TRUE;
}






/*==========================================================================*
* FUNCTION : OpenRs485Dev
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN char  *pParam : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-06-03 14:16
*==========================================================================*/
static int OpenRs485Dev(IN char *pParam)
{
	int		fd;	// the file descriptor

	fd = open("/dev/ttyS2", O_RDWR | O_NOCTTY | O_NONBLOCK | O_EXCL); 
	if (fd < 0)
	{
		return -1;
	}

	//pAttr->nOdd = 0;
	if (!RS485_SetAttr(fd, pParam))
	{
		close(fd);
		return -1;
	}

	return fd;
}


/*==========================================================================*
* FUNCTION : Rs485_Open
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN char  *pOpenParams : the port settings as format:
*									   "buad,parity,data,stop", 
*                                     such as "9600,n,8,1". for detail 
*                                     please refer to the function
*                                     Serial_ParseOpenParams
*            IN int   nTimeout     : Open timeout in ms
*            OUT int  *pErrCode    : to save error code.
* RETURN   : HANDLE : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-06-02 14:02
*==========================================================================*/
HANDLE RS485_Open(IN char *pOpenParams,
				  IN int nTimeout,
				  OUT int *pErrCode)
{
	RS485_DRV *pPort = NEW(RS485_DRV, 1);
	int		  status;

	/* 1. get mem */
	if (pPort == NULL)
	{
		*pErrCode = ERR_COMM_NO_MEMORY;
		return	NULL;
	}

	memset(pPort, 0, sizeof(RS485_DRV));



	INIT_TIMEOUTS(pPort->toTimeouts, nTimeout, nTimeout);
	pPort->toTimeouts.nIntervalTimeout = nTimeout;


	// open the serial port.
	pPort->fdSerial = OpenRs485Dev(pOpenParams);
	if (pPort->fdSerial < 0)
	{
		DELETE(pPort);
		*pErrCode = ERR_COMM_OPENING_PORT;

		return	NULL;
	}

	//ioctl(pPort->fdSerial,TIOCMGET,&status); 
	//status &=~ TIOCM_RTS; 
	//ioctl(pPort->fdSerial,TIOCMSET,&status); //set RTS in receive mode
	 ioctl(gRtsHand, IOC_SET_RECEIVE, 0);

	//pPort->fdRtsFor485 = open("/dev/rts", O_RDWR | O_NOCTTY);
	//if (pPort->fdRtsFor485 < 0)
	//{
	//	DELETE(pPort);
	//	*pErrCode = ERR_COMM_OPENING_PORT;

	//	return	NULL;
	//}


	*pErrCode = ERR_COMM_OK;

	//Frank Wu,20150318, for SoNick Battery
	strncpyz(g_RS485Data.CommPort.szOpenParamStr, pOpenParams, sizeof(g_RS485Data.CommPort.szOpenParamStr));

	return (HANDLE)pPort;
}


/*==========================================================================*
* FUNCTION : RS485_ClrBuffer
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN HANDLE  hPort        : 
*            OUT char   *pBuffer     : 
*            IN int     nBytesToRead : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : IlockTeng                 DATE: 2009-06-02 16:49
*==========================================================================*/
INT32 RS485_ClrBuffer(IN HANDLE hPort)
{
	ASSERT(hPort);
	RS485_DRV *pPort     = (RS485_DRV *)hPort;
	int	 fd	 = pPort->fdSerial;
	BYTE cReceiveBuffer[4096 + 10] = {0};

	ioctl(gRtsHand, IOC_SET_RECEIVE, 0);
	read(fd, cReceiveBuffer, (size_t)4096);
	//TRACE("\n RS485_ClrBuffer  CLR  CLR = %s \n",cReceiveBuffer);
	//RS485_Read(hPort, cReceiveBuffer, 4802);
}

/*==========================================================================*
* FUNCTION : RS485_Read
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN HANDLE  hPort        : 
*            OUT char   *pBuffer     : 
*            IN int     nBytesToRead : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-06-02 16:49
*==========================================================================*/
int RS485_ReadForSlave(IN HANDLE hPort, OUT BYTE* pBuffer, IN int nBytesToRead)
{
	//BYTE *pBuf = pBuffer;
	RS485_DRV *pPort     = (RS485_DRV *)hPort;
	int				fd		   = pPort->fdSerial;
	int				iWantToReadnByte;
	int				nBytesRead = 0;
	int			    nTotalBytesRead = 0;	// total bytes of data being read
	int				nTimeout;
	int				status;
	iWantToReadnByte = nBytesToRead;

	//ioctl(fd,TIOCMGET,&status); 
	//status &=~ TIOCM_RTS; 
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	 ioctl(gRtsHand, IOC_SET_RECEIVE, 0);
	/* clear last error */
	pPort->nLastErrorCode = ERR_COMM_OK;

	// when data is NOT ready, the WaitFiledReadable() always return 1, 
	// but the read returns -1. so the WaitFiledReadable can NOT be used
	// in reading data from the ACU 485 Port.
	nTimeout = pPort->toTimeouts.nReadTimeout;

	while (nBytesToRead > 0)
	{
		nBytesRead = read(fd, pBuffer, (size_t)nBytesToRead);

		//TRACEX("To read %d bytes, this read %d bytes data, total %d.\n",
		//	nBytesToRead, nBytesRead, nTotalBytesRead);

		if (nBytesRead > 0)	/* we got some data */
		{
			// ok
			nBytesToRead	-= nBytesRead;
			pBuffer			+= nBytesRead;
			nTotalBytesRead += nBytesRead;

			if (nBytesToRead == 0)
			{
				//TRACEX("OK!!! To read %d bytes, this read %d bytes data, total %d.\n",
				//	nBytesToRead, nBytesRead, nTotalBytesRead);

				break;	// reading finished
			}

			// Need clear retry times? We don't clear it!
			// nRetryTimes = 0;
			nTimeout = pPort->toTimeouts.nIntervalTimeout;
		}

		else if (nBytesRead <= 0)
		{
			if (nTimeout <= 0)	// timeout
			{
				pPort->nLastErrorCode = ERR_COMM_TIMEOUT;

				//TRACEX("Read total %d bytes on timeout.\n",
				//	nTotalBytesRead);

				break;	// timeout
			}

			Sleep(5);

			nTimeout -= 20;
		}
	}

	return nTotalBytesRead;
}

/*==========================================================================*
* FUNCTION : RS485_Read
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN HANDLE  hPort        : 
*            OUT char   *pBuffer     : 
*            IN int     nBytesToRead : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-06-02 16:49
*==========================================================================*/
int RS485_Read(IN HANDLE hPort, OUT BYTE* pBuffer, IN int nBytesToRead)
{
	//BYTE *pBuf = pBuffer;
	RS485_DRV *pPort     = (RS485_DRV *)hPort;
	int				fd		   = pPort->fdSerial;
	int				iWantToReadnByte;
	int				nBytesRead = 0;
	int			    nTotalBytesRead = 0;	// total bytes of data being read
	int				nTimeout;
	int				status;
	iWantToReadnByte = nBytesToRead;

	//G3_OPT [debug info], by Lin.Tao.Thomas
#ifdef TRACE_COSTTIME_RS485
	struct timeval t_start,t_end; 
	long cost_time = 0; 
	gettimeofday(&t_start, NULL); 
	long start = ((long)t_start.tv_sec)*1000+(long)t_start.tv_usec/1000; 
	//printf("Start time of RS485_Read: %ld ms\n", start); 
#endif

	//ioctl(fd,TIOCMGET,&status); 
	//status &=~ TIOCM_RTS; 
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	 ioctl(gRtsHand, IOC_SET_RECEIVE, 0);

	/* clear last error */
	pPort->nLastErrorCode = ERR_COMM_OK;

	// when data is NOT ready, the WaitFiledReadable() always return 1, 
	// but the read returns -1. so the WaitFiledReadable can NOT be used
	// in reading data from the ACU 485 Port.
	nTimeout = pPort->toTimeouts.nReadTimeout;

	while (nBytesToRead > 0)
	{
		nBytesRead = read(fd, pBuffer, (size_t)nBytesToRead);

		//TRACEX("To read %d bytes, this read %d bytes data, total %d.\n",
		//	nBytesToRead, nBytesRead, nTotalBytesRead);

		if (nBytesRead > 0)	/* we got some data */
		{
			// ok
			nBytesToRead	-= nBytesRead;
			pBuffer			+= nBytesRead;
			nTotalBytesRead += nBytesRead;

			if (nBytesToRead == 0)
			{
				//TRACEX("OK!!! To read %d bytes, this read %d bytes data, total %d.\n",
				//	nBytesToRead, nBytesRead, nTotalBytesRead);

				break;	// reading finished
			}

			// Need clear retry times? We don't clear it!
			// nRetryTimes = 0;
			nTimeout = pPort->toTimeouts.nIntervalTimeout;
		}

		else if (nBytesRead <= 0)
		{
			if (nTimeout <= 0)	// timeout
			{
				pPort->nLastErrorCode = ERR_COMM_TIMEOUT;

				//TRACEX("Read total %d bytes on timeout.\n",
				//	nTotalBytesRead);

				break;	// timeout
			}

			if (iWantToReadnByte <= 1024)
			{
				// sleep a while to read.
				Sleep(20);
			}
			else if (iWantToReadnByte > 1024 && iWantToReadnByte <= 2000)
			{
				// sleep a while to read.
				Sleep(30);
			}
			else
			{
				// sleep a while to read.
				Sleep(40);//10 times * 35ms = 350ms	
			}
			nTimeout -= 20;
		}
	}

	//pBuf[nTotalBytesRead] = NULL;

	//TRACE("\n RRRR : %s \n",pBuf);

	//G3_OPT [debug info], by Lin.Tao.Thomas
#ifdef TRACE_COSTTIME_RS485
	//get end time 
	gettimeofday(&t_end, NULL); 
	long end = ((long)t_end.tv_sec)*1000+(long)t_end.tv_usec/1000; 
	cost_time = end - start; 
	printf("Cost time of RS485_Read: %ld ms\n", cost_time);
#endif

	return nTotalBytesRead;
}
/*==========================================================================*
* FUNCTION : b_DataEnough
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN HANDLE  hPort        : 
*            OUT char   *pBuffer     : 
*            IN int     nBytesToRead : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : Added by Ilock               DATE: 2010-03-28 16:49
*==========================================================================*/
#define		SM_BRC_BY_DATAEOI	0x0d
//Add by Ilock 2010/3/28
BOOL bDataEnough(BYTE* pRecvData,int nBytesRead)
{
	int i = 0;
	if (nBytesRead > 0)
	{
		for (i = (nBytesRead - 1); i >= 0; i--)
		{
			if ( SM_BRC_BY_DATAEOI == *(pRecvData + i))
			{
				return TRUE;
			}
			else
			{
				//Next Bytes Check !!!
			}
		}
	}
	else
	{
		//Needn't process!!!!!!
	}
	return FALSE;
}


/*==========================================================================*
* FUNCTION : RS485 _ReadForSMBRC
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN HANDLE  hPort        : 
*            OUT char   *pBuffer     : 
*            IN int     nBytesToRead : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : Added by Ilock               DATE: 2010-03-28 16:49
*==========================================================================*/
int RS485_ReadForSMBRC(IN HANDLE hPort, OUT BYTE* pBuffer, IN int nBytesToRead)
{
	//BYTE *pBuf = pBuffer;
	RS485_DRV *pPort     = (RS485_DRV *)hPort;
	int				fd		 = pPort->fdSerial;
	int				iWantToReadnByte = 0;
	int				nBytesRead = 0;
	int				nTotalBytesRead = 0;				// total bytes of data being read
	int				nTimeout = 0;
	int				status = 0;
	iWantToReadnByte = nBytesToRead;

#define  CLR_RECV_BUF_MAX_NUM	4096
	BYTE	RcvTempBuffer[CLR_RECV_BUF_MAX_NUM];

	BYTE *pBuf = pBuffer;

	//ioctl(fd, TIOCMGET, &status); 
	//status &= ~ TIOCM_RTS; 
	//ioctl(fd, TIOCMSET, &status);				//set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status);				//set RTS in receive mode
	 ioctl(gRtsHand, IOC_SET_RECEIVE, 0);

	/* clear last error */
	pPort->nLastErrorCode = ERR_COMM_OK;

	// when data is NOT ready, the WaitFiledReadable() always return 1, 
	// but the read returns -1. so the WaitFiledReadable can NOT be used
	// in reading data from the ACU 485 Port.
	nTimeout = pPort->toTimeouts.nReadTimeout;

	while (nBytesToRead > 0)
	{
		nBytesRead = read(fd, pBuffer, (size_t)nBytesToRead);

		if (nBytesRead > 0)	/* we got some data */
		{
			// ok
			pBuf = pBuffer;						//Point to start position of Current Receive Data
			nBytesToRead	-= nBytesRead;
			pBuffer		+= nBytesRead;				//Point to Next need position
			nTotalBytesRead += nBytesRead;

			if (nBytesToRead <= 0)
			{
				read(fd, RcvTempBuffer, CLR_RECV_BUF_MAX_NUM);//Clean the buffer from driver level
				break;						// reading finished
			}

			if (bDataEnough(pBuf, nBytesRead))
			{
				read(fd, RcvTempBuffer, CLR_RECV_BUF_MAX_NUM);//Clean the buffer from driver level
				break;
			}
			else
			{
				//Continue read!!!!
			}

			// Need clear retry times? We don't clear it!
			// nRetryTimes = 0;
			nTimeout = pPort->toTimeouts.nIntervalTimeout;
		}
		else if (nBytesRead <= 0)
		{
			if (nTimeout <= 0)	// timeout
			{
				pPort->nLastErrorCode = ERR_COMM_TIMEOUT;

				//TRACEX("Read total %d bytes on timeout.\n",
				//	nTotalBytesRead);

				break;	// timeout
			}

			// sleep a while to read.
			Sleep(20);

			nTimeout -= 20;
		}
	}

	return nTotalBytesRead;
}



/*==========================================================================*
* FUNCTION : RS485_Read
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN HANDLE  hPort        : 
*            OUT char   *pBuffer     : 
*            IN int     nBytesToRead : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : added                 DATE: 2010-03-11 16:49
*==========================================================================*/
int RS485_ReadForLDU(IN HANDLE hPort, OUT BYTE* pBuffer, IN int nBytesToRead)
{
	//BYTE *pBuf = pBuffer;
	RS485_DRV *pPort     = (RS485_DRV *)hPort;
	int				fd		   = pPort->fdSerial;
	int				iWantToReadnByte;
	int				nBytesRead = 0;
	int			    nTotalBytesRead = 0;	// total bytes of data being read
	int				nTimeout;
	int				status;
	iWantToReadnByte = nBytesToRead;

	//ioctl(fd,TIOCMGET,&status); 
	//status &=~ TIOCM_RTS; 
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	//ioctl(fd,TIOCMSET,&status); //set RTS in receive mode
	 ioctl(gRtsHand, IOC_SET_RECEIVE, 0);

	/* clear last error */
	pPort->nLastErrorCode = ERR_COMM_OK;

	// when data is NOT ready, the WaitFiledReadable() always return 1, 
	// but the read returns -1. so the WaitFiledReadable can NOT be used
	// in reading data from the ACU 485 Port.
	nTimeout = pPort->toTimeouts.nReadTimeout;

	while (nBytesToRead > 0)
	{
		nBytesRead = read(fd, pBuffer, (size_t)nBytesToRead);

		//TRACEX("To read %d bytes, this read %d bytes data, total %d.\n",
		//	nBytesToRead, nBytesRead, nTotalBytesRead);

		if (nBytesRead > 0)	/* we got some data */
		{
			// ok
			nBytesToRead	-= nBytesRead;
			pBuffer			+= nBytesRead;
			nTotalBytesRead += nBytesRead;

			if (nBytesToRead == 0)
			{
				//TRACEX("OK!!! To read %d bytes, this read %d bytes data, total %d.\n",
				//	nBytesToRead, nBytesRead, nTotalBytesRead);

				break;	// reading finished
			}

			// Need clear retry times? We don't clear it!
			// nRetryTimes = 0;
			nTimeout = pPort->toTimeouts.nIntervalTimeout;
		}

		else if (nBytesRead <= 0)
		{
			if (nTimeout <= 0)	// timeout
			{
				pPort->nLastErrorCode = ERR_COMM_TIMEOUT;

				//TRACEX("Read total %d bytes on timeout.\n",
				//	nTotalBytesRead);

				break;	// timeout
			}

			//if (iWantToReadnByte <= 1024)
			//{
			//	// sleep a while to read.
			Sleep(20);
			//}
			//else if (iWantToReadnByte > 1024 && iWantToReadnByte <= 2000)
			//{
			//	// sleep a while to read.
			//	Sleep(30);
			//}
			//else
			//{
			//	// sleep a while to read.
			//	Sleep(40);//10 times * 35ms = 350ms	
			//}
			nTimeout -= 20;
		}
	}

	//pBuf[nTotalBytesRead] = NULL;

	//TRACE("\n RRRR : %s \n",pBuf);

	return nTotalBytesRead;
}		



/* The "CommWrite" proc for comm driver */
/*==========================================================================*
* FUNCTION : Serial_CommWrite
* PURPOSE  : write a buffer to a port
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN HANDLE  hPort         : 
*            IN char    *pBuffer      : 
*            IN int     nBytesToWrite : 
* RETURN   : int : < 0 for error. <nBytesToWrite timeout, = nBytesToWrite ok
* COMMENTS : 
* CREATOR  : Frank Mao                DATE: 2004-09-13 21:36
*==========================================================================*/
int RS485_Write(IN HANDLE hPort, IN BYTE* pBuffer,	IN int nBytesToWrite)
{
	RS485_DRV *pPort = (RS485_DRV *)hPort;
	int				fd = pPort->fdSerial;
	//int				fdRts = pPort->fdRtsFor485;
	int				nBytesWritten = 0;
	int			    nTotalBytesWritten = 0;	// total bytes of data being read
	int				nRetryTimes = 0;
	int				rc;
	int				status;
	//DWORD           dwArg;

	//G3_OPT [debug info], by Lin.Tao.Thomas
#ifdef TRACE_COSTTIME_RS485
	struct timeval t_start,t_end; 
	long cost_time = 0; 
	gettimeofday(&t_start, NULL); 
	long start = ((long)t_start.tv_sec)*1000+(long)t_start.tv_usec/1000; 
#endif

	/* clear last error */
	pPort->nLastErrorCode = ERR_COMM_OK;

	rc = WaitFiledWritable(fd, pPort->toTimeouts.nReadTimeout);


	if( rc <= 0 )
	{
		/* 0: wait time out, -1: fd error on waiting    */
		pPort->nLastErrorCode = 
			(rc == 0) ? ERR_COMM_TIMEOUT  : ERR_COMM_WRITE_DATA;

		return 0;	// nothing has been read
	}

	//TRACE("Length is %d, baudrate is %d.\n", HIWORD(dwArg), LOWORD(dwArg));
	//set RTS in write mode
	//ioctl(fd,TIOCMGET,&status); 
	//status |=TIOCM_RTS;
	//if(ioctl(fd,TIOCMSET,&status) < 0)//ioctl(fdRts, IOC_SET_SEND, 0)
	//{
	//	pPort->nLastErrorCode = ERR_COMM_WRITE_DATA;
	//	AppLogOut("Rs485_comm.c",
	//		2, //APP_LOG_ERROR
	//		"[%s]-[%d]ERROR:Failed to ioctl(fd,TIOCMSET,&status) < 0!\n\r", 
	//		__FILE__, __LINE__);
	//	return 0;	// nothing has been write	
	//}
	ioctl(gRtsHand, IOC_SET_SEND, 0);
	//TRACE("[Serial_CommWrite] -- RTS Control!!!!!\n\r");
	//TRACE("[Serial_CommWrite] -- set RTS to 1!!!!!.\n\r");
	//TRACE("[Serial_CommWrite] -- write--- %s !!!!!.\n\r",pBuffer);

	Sleep( 5 );
	while (nBytesToWrite > 0)
	{
		nBytesWritten = write(fd, (void *)pBuffer, (size_t)nBytesToWrite);

		//nBytesWritten = write(fd, rbuf, 1);
		//nBytesToWrite = 1;

		if (nBytesWritten > 0)	/* we got some data */
		{
			// ok
			nBytesToWrite	   -= nBytesWritten;
			pBuffer			   += nBytesWritten;
			nTotalBytesWritten += nBytesWritten;

			if (nBytesToWrite == 0)
			{
				//TRACEX("[Serial_CommWrite] -- write finished !!!!!.\n\r");

				break;	// writing finished
			}

			// Need clear retry times? We don't clear it!
			// nRetryTimes = 0;
		}

		else if ((nBytesWritten < 0) && (errno != EAGAIN))		// read error?
		{
			nRetryTimes++;		// let's retry again
			if (nRetryTimes >= COM_RETRY_TIME)
			{
				pPort->nLastErrorCode = ERR_COMM_WRITE_DATA;

				break;	// error, quit now.
			}
		}

		// nothing received, data not ready? wait a while
		// if error, we also sleep a while and retry again.
		rc = WaitFiledWritable(fd, pPort->toTimeouts.nIntervalTimeout);
		if( rc <= 0 )
		{
			/* 0: wait time out, -1: fd error on waiting    */
			pPort->nLastErrorCode = (rc == 0) ? ERR_COMM_TIMEOUT
				: ERR_COMM_READ_DATA;
			break;
		}
	}
	// add by wankun 2008-06-13
	// in order to test the driver of 485 send over or not
	int iRtn = 0;
	while(!iRtn)
	{
		ioctl(fd, TIOCSERGETLSR, &iRtn);
		//printf("the status of 485 is %d (0 is not over)\n ",iRtn);
	}
	//send over then change the status of rts to receive state!
	//set RTS in receive mode	
	//status &=~ TIOCM_RTS;
	//ioctl(fd,TIOCMSET,&status);
	//ioctl(fd,TIOCMSET,&status);
	//ioctl(fd,TIOCMSET,&status);
	//ioctl(fd,TIOCMSET,&status);//ioctl(fdRts, IOC_SET_RECEIVE, 0);
	ioctl(gRtsHand, IOC_SET_RECEIVE, 0);
	//ended by wankun  2008-06-13

	//G3_OPT [debug info], by Lin.Tao.Thomas
#ifdef TRACE_COSTTIME_RS485
	//get end time 
	gettimeofday(&t_end, NULL); 
	long end = ((long)t_end.tv_sec)*1000+(long)t_end.tv_usec/1000; 
	cost_time = end - start; 
	printf("Cost time of RS485_write: %ld ms\n", cost_time);
#endif

	return nTotalBytesWritten;
}



/* The "CommClose" proc for comm driver */

/*==========================================================================*
* FUNCTION : RS485_Close
* PURPOSE  : Close an opened port and release the memory of the port
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN HANDLE  hPort : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-06-03 11:42
*==========================================================================*/
int RS485_Close(IN HANDLE hPort)
{
	RS485_DRV *pPort = (RS485_DRV *)hPort;

	if (hPort == NULL)
	{
		return ERR_COMM_PORT_HANDLE;
	}


	/* descrease the linkage, the pCurClients shall be deleted if no
	* any clients. But the server and clients share the same pCurClients,
	* and the initialing value of *pCurClients is 0, so the condition to
	* delete pCurClients is *pCurClients < 0.
	*/

	//close(pPort->fdRtsFor485);
	close(pPort->fdSerial);	// to close fd if it is not used yet.

	DELETE(pPort);

	return ERR_COMM_OK;
}



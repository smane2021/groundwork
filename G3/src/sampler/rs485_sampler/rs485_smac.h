/*============================================================================*
 *         Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : Unified Data Type Definition
 *
 *  FILENAME : rs485_smio.h
 *  PURPOSE  : Define base data type.
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2007-07-12      V1.0           IlockTeng         Created.    
 *    2009-04-14     
 *                                                     
 *-----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *          
 *      
 *-----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *      
 *    
 *============================================================================*/
#ifndef __RS485_SAMPLER_SMAC_H
#define __RS485_SAMPLER_SMAC_H

#include <stdio.h>
#include "basetypes.h"
#include "err_code.h"
#include "string.h"
#include "rs485_main.h"			


#define		FUNCTION_GENERAL_ERROR		-1
#define		NO_RESPONSE_BEXISTENCE		 1
#define		COMMUNICATION_ERROR			 2
#define		RESPONSE_OK					 0

#define		SMAC_CH_START				300
#define		SMAC_CH_END					599
#define		SMAC_CH_FIRST_END			449	

#define SMAC_CH_END_FLAG				-1

#define bySOI							 0x7E			//START OF INFORMATION
#define byEOI							 0x0D			//END OF INFORMATION
#define SMAC_GETVER_CID2				 0x4f
#define SMAC_CID1						 0xd3
#define SMAC_CID2_0x41					 0x41
#define SMAC_CID2_0x43					 0x43
#define SMAC_CID2_0x44					 0x44
#define SMAC_CID2_0x47					 0x47
#define SMAC_CID2_0x46					 0x46
#define MAX_NUM_SMAC					 2
#define SMAC_ADDR_START					 104
#define	SMAC_ADDR_105					 105
#define SMAC_ADDR_END					 108
#define MAX_CHN_NUM_PER_SMAC			 150
#define SMAC_CH_HAVE_DIESEL				 199
#define SMAC_SAMP_INVALID_VALUE			(-9999) //initialization data
#define SMAC_SAMP_FREQUENCY				3					//control  sample the frequency
#define SMAC_SHECKSUM_EOI				5
#define SMAC_LENGTH_OFFSET_ONE			1
#define SMAC_LENGTH_OFFSET_TWO			2
#define SMAC_LENGTH_OFFSET_THREE		3

#define DIESEL_BHAVE					1
#define DIESEL_NO_HAVE					0

#define SMAC_SAMPLE_OK					0
#define SMAC_SAMPLE_FAIL				1

//communication status	
#define	SMAC_COMM_OK					0
#define	SMAC_COMM_FAIL					1


//existent status
#define SMAC_EQUIP_EXISTENT				0
#define SMAC_EQUIP_NOT_EXISTENT			1

#define	MAX_CHN_DISTANCE_SMAC			42


#define MAX_RECVBUFFER_NUM_SMAC			4096	
//#define MAX_SENDBUFFER_NUM_SMAC		50
#define SMAC_NUM						4
#define SMAC_ONE_CMD_SIG_NUM			50

enum YDN23PROTOCOLINFO_SMAC
{
	CHECKSUM_ERROR_YDN23_SMAC = 2,
	ADDR_YDN23_SMAC = 3,
	
	CID1H_YDN23_SMAC = 5,
	CID1L_YDN23_SMAC = 6,
	CID2_YDN23_SMAC= 7,
	LENGTH_YDN23_SMAC = 9,
	DATA_YDN23_SMAC= 13,
	DATAINFO_YDN23_SMAC = 15,
	//others will add here
};


/*
	correlation	with 0x41	and	0x43	

*/
enum  ROUGH_DATA_IDX_SMAC
{	
	//0x41 DATAFLAG unused
	SMAC_OUT_PHASE_VLT_A,						
	SMAC_OUT_PHASE_VLT_B,						
	SMAC_OUT_PHASE_VLT_C,						
	SMAC_OUT_LINE_VLT_AB,					
	SMAC_OUT_LINE_VLT_BC,				
	SMAC_OUT_LINE_VLT_CA,		
	SMAC_OUT_CURRENT_A,					
	SMAC_OUT_CURRENT_B,						
	SMAC_OUT_CURRENT_C,					
	SMAC_FREQUENCY,								//Frequency
	SMAC_TOTAL_REAL_POWER,						//	Total Real Power
	SMAC_PHASE_A_REAL_POWER,					//	Phase A Real Power		
	SMAC_PHASE_B_REAL_POWER,					//	Phase B Real Power	
	SMAC_PHASE_CREAL_POWER,						//	Phase C Real Power
	SMAC_TOTAL_REACTIVE_POWER,					//	Total Reactive Power
	SMAC_PHASE_A_REACTIVE_POWER,				//	Phase A Reactive Power
	SMAC_PHASE_B_REACTIVE_POWER,				//	Phase B Reactive Power 
	SMAC_PHASE_CREACTIVE_POWER,					//	Phase C Reactive Power 
	SMAC_TOTAL_APPARENT_POWER,					//	Total Apparent Power
	SMAC_PHASE_A_APPARENT_POWER,				//	Phase A Apparent Power
	SMAC_PHASE_B_APPARENT_POWER,				//	Phase B Apparent Power
	SMAC_PHASE_C_APPARENT_POWER,				//	Phase C Apparent Power
	SMAC_POWER_FACTOR,							//	Power Factor	���๦������
	SMAC_PHASE_A_POWER_FACTOR,					//	Phase A Power Factor
	SMAC_PHASE_B_POWER_FACTOR,					//	Phase B Power Factor
	SMAC_PHASE_C_POWER_FACTOR,					//	Phase C Power Factor
	SMAC_PHASE_A_CURRENT_CREST_FACTOR,			//	Phase A Current Crest Factor Ia��������
	SMAC_PHASE_B_CURRENT_CREST_FACTOR,			//	Phase B Current Crest Factor IB��������
	SMAC_PHASE_C_CURRENT_CREST_FACTOR,			//	Phase C Current Crest Factor IC��������
	SMAC_PHASE_A_CURRENT_THD,					//Phase A Current THD	A�����Э������
	SMAC_PHASE_B_CURRENT_THD,					//Phase B Current THD	A�����Э������
	SMAC_PHASE_C_CURRENT_THD,					//Phase C Current THD	A�����Э������
	SMAC_PHASE_A_VOLT_THD,						//Phase A Voltage THD
	SMAC_PHASE_B_VOLT_THD,						//Phase B Voltage THD
	SMAC_PHASE_C_VOLT_THD,						//Phase C Voltage THD
	SMAC_TOTAL_REAL_ENERGY,						//Total Real Energy		
	SMAC_TOTAL_REACTIVE_ENERGY,					//Total Reactive Energy 
	SMAC_APPARENT_ENERGY,						//Total Apparent Energy 
	SMAC_AMBIENT_TEMPERATURE,					//Ambient Temperature 
	SMAC_BATTERY_START_VOLTAGE,					//Battery Voltage			������ص�ѹ
	//0---39



//0x43�����ȡ������

	//����ͨ������M=12
	SMAC_SWITCH_1,						
	SMAC_SWITCH_2,						
	SMAC_SWITCH_3,						
	SMAC_SWITCH_4,					
	SMAC_SWITCH_5,				
	SMAC_SWITCH_6,		
	SMAC_SWITCH_7,					
	SMAC_SWITCH_8,						
	SMAC_SWITCH_9,				
	SMAC_SWITCH_10,						
	SMAC_SWITCH_11,						
	SMAC_SWITCH_12,						
	SMAC_RELAY_1_STATUS,					
	SMAC_RELAY_2_STATUS,				
	SMAC_RELAY_3_STATUS,		
	SMAC_RELAY_4_STATUS,					
	SMAC_DIESEL_TEST_START_STOP_STAT,							
	SMAC_DIESEL_TEST_STOP_CAUSE,	
	//40------57

//0x44��ȡ�澯��Ϣ
	SMAC_ALARM_AC_PHASE_VOLT_A,		
	SMAC_ALARM_AC_PHASE_VOLT_B,
	SMAC_ALARM_AC_PHASE_VOLT_C,
	SMAC_ALARM_AC_CURRENT_A,
	SMAC_ALARM_AC_CURRENT_B,
	SMAC_ALARM_AC_CURRENT_C,
	SMAC_ALARM_AC_LIN_VOLT_AB,		 //�����ߵ�ѹAB
	SMAC_ALARM_AC_LIN_VOLT_BC,		 //�����ߵ�ѹBC
	SMAC_ALARM_AC_LIN_VOLT_CA,		 //�����ߵ�ѹCA
	SMAC_ALARM_TOTAL_REAL_POWER,	 //���й�����
	SMAC_ALARM_TOTAL_REACTIVE_POWER,	//���޹�����
	SMAC_ALARM_TOTAL_POWER_FACTOR,	 //�ܹ�������
	SMAC_ALARM_CURRENT_AVERAGE_THD,	 //����ƽ������
	SMAC_ALARM_VOLT_AVERAGE_THD,	
	SMAC_ALARM_FREQUENCY,
	SMAC_ALARM_AMBIENT_TEMPERATURE,
	SMAC_ALARM_BATTERY_VOLTAGE,
	SMAC_ALARM_HI_WATER_TEMPERATURE,
	SMAC_ALARM_LOW_OIL_PRESSURE,				//����ѹ�澯״̬
	SMAC_ALARM_LOW_OIL_POSITION,				//����λ�澯״̬
	SMAC_ALARM_DIESEL_GENERATOR_FAIL,			//Diesel Generator Fail
	//58--------78


//0x47��ȡϵͳ��������������
	SMAC_GETSYS_ALARM_PERCENTUM,				//�澯�ز�	
	SMAC_GETSYS_LINE_FASHION,
	SMAC_GETSYS_TEST_MAX_TIME,
	//79-------81


//0x46 ��ȡģ��������
	SMAC_GETANLG_AC_CURRENT_A_UPPER,		
	SMAC_GETANLG_AC_CURRENT_B_UPPER,
	SMAC_GETANLG_AC_CURRENT_C_UPPER,
	SMAC_GETANLG_AC_CURRENT_A_LOWER,		
	SMAC_GETANLG_AC_CURRENT_B_LOWER,
	SMAC_GETANLG_AC_CURRENT_C_LOWER,
	SMAC_GETANLG_AC_VOLT_A_UPPER,		
	SMAC_GETANLG_AC_VOLT_B_UPPER,
	SMAC_GETANLG_AC_VOLT_C_UPPER,
	SMAC_GETANLG_AC_VOLT_A_LOWER,		
	SMAC_GETANLG_AC_VOLT_B_LOWER,
	SMAC_GETANLG_AC_VOLT_C_LOWER,
	SMAC_GETANLG_AC_LINEVOLT_AB_UPPER,		
	SMAC_GETANLG_AC_LINEVOLT_BC_UPPER,
	SMAC_GETANLG_AC_LINEVOLT_CA_UPPER,
	SMAC_GETANLG_AC_LINEVOLT_AB_LOWER,		
	SMAC_GETANLG_AC_LINEVOLT_BC_LOWER,
	SMAC_GETANLG_AC_LINEVOLT_CA_LOWER,
	SMAC_GETANLG_FREQUENCY_UPPER,
	SMAC_GETANLG_FREQUENCY_LOWER,
	SMAC_GETANLG_TOTAL_REAL_POWER_UPPER,
	SMAC_GETANLG_TOTAL_REAL_POWER_LOWER,
	SMAC_GETANLG_TOTAL_POWER_FACTOR_LOWER,
	SMAC_GETANLG_CURRENT_AVERAGE_THD_UPPER,	 
	SMAC_GETANLG_VOLT_AVERAGE_THD_LOWER,		
	SMAC_GETANLG_TEMPERATURE_UPPER,
	SMAC_GETANLG_TEMPERATURE_LOWER,	
	SMAC_GETANLG_VOLTAGE_UPPER,
	SMAC_GETANLG_VOLTAGE_LOWER,	
	SMAC_GETANLG_CT_CHANGE,
	//81-------111

	SMAC_COMM_STATUS,
	SMAC_WORKING_STATUS,
	
	SMAC_HAVE_DIESEL,
	SMAC_HAVE_SMAC_ADDR,
	//SMAC_HAVE_SMAC106,
	SMAC_GROUP_WORKING_STATUS,

	SMAC_INTERRUPT_TIMES, 

	SMAC_DISEL_COMM_STAT,
	SMAC_DISEL_EXIST_STAT,
	//here add									
	SMAC_MAX_SIGNALS_NUM,
};




/*
	�˴���Ӧ�ϱ�ͨ����һ�����ĸ����ź�
	˳��Ҳ�ǰ����߼�ͨ������
*/
enum SMAC_SAMP_CHANNEL
{
	//SMAC
	SMAC_CH_OUT_PHASE_VLT_A = 300,										
	SMAC_CH_OUT_PHASE_VLT_B,						
	SMAC_CH_OUT_PHASE_VLT_C,						
	SMAC_CH_OUT_LINE_VLT_AB,					
	SMAC_CH_OUT_LINE_VLT_BC,				
	SMAC_CH_OUT_LINE_VLT_CA,		
	SMAC_CH_OUT_CURRENT_A,					
	SMAC_CH_OUT_CURRENT_B,						
	SMAC_CH_OUT_CURRENT_C,					
	SMAC_CH_FREQUENCY,								//Frequency
	SMAC_CH_TOTAL_REAL_POWER,						//	Total Real Power
	SMAC_CH_PHASE_A_REAL_POWER,						//	Phase A Real Power		
	SMAC_CH_PHASE_B_REAL_POWER,						//	Phase B Real Power	
	SMAC_CH_PHASE_CREAL_POWER,						//	Phase C Real Power
	SMAC_CH_TOTAL_REACTIVE_POWER,					//	Total Reactive Power
	SMAC_CH_PHASE_A_REACTIVE_POWER,					//	Phase A Reactive Power
	SMAC_CH_PHASE_B_REACTIVE_POWER,					//	Phase B Reactive Power 
	SMAC_CH_PHASE_CREACTIVE_POWER,					//	Phase C Reactive Power 
	SMAC_CH_TOTAL_APPARENT_POWER,					//	Total Apparent Power
	SMAC_CH_PHASE_A_APPARENT_POWER,					//	Phase A Apparent Power
	SMAC_CH_PHASE_B_APPARENT_POWER,					//	Phase B Apparent Power
	SMAC_CH_PHASE_C_APPARENT_POWER,					//	Phase C Apparent Power
	SMAC_CH_POWER_FACTOR,							//	Power Factor	���๦������
	SMAC_CH_PHASE_A_POWER_FACTOR,					//	Phase A Power Factor
	SMAC_CH_PHASE_B_POWER_FACTOR,					//	Phase B Power Factor
	SMAC_CH_PHASE_C_POWER_FACTOR,					//	Phase C Power Factor
	SMAC_CH_PHASE_A_CURRENT_CREST_FACTOR,			//	Phase A Current Crest Factor Ia��������
	SMAC_CH_PHASE_B_CURRENT_CREST_FACTOR,			//	Phase B Current Crest Factor IB��������
	SMAC_CH_PHASE_C_CURRENT_CREST_FACTOR,			//	Phase C Current Crest Factor IC��������
	SMAC_CH_PHASE_A_CURRENT_THD,					//Phase A Current THD	A�����Э������
	SMAC_CH_PHASE_B_CURRENT_THD,					//Phase B Current THD	A�����Э������
	SMAC_CH_PHASE_C_CURRENT_THD,					//Phase C Current THD	A�����Э������
	SMAC_CH_PHASE_A_VOLT_THD,						//Phase A Voltage THD
	SMAC_CH_PHASE_B_VOLT_THD,						//Phase B Voltage THD
	SMAC_CH_PHASE_C_VOLT_THD,						//Phase C Voltage THD
	SMAC_CH_TOTAL_REAL_ENERGY,						//Total Real Energy		
	SMAC_CH_TOTAL_REACTIVE_ENERGY,					//Total Reactive Energy 
	SMAC_CH_APPARENT_ENERGY,						//Total Apparent Energy 
	SMAC_CH_AMBIENT_TEMPERATURE,					//Ambient Temperature 
	SMAC_CH_BATTERY_START_VOLTAGE,					//Battery Voltage			������ص�ѹ

	SMAC_CH_COMM_STATUS = 420,
	SMAC_CH_EXIST_STATUS ,
	SMAC_CH_DISEL_STAT = 422,
	SMAC_CH_DISEL_COMM_STAT = 423,

	

/*****************************************************************************************/	
	//DIESEL
	DIESEL_CH_BATTERY_START_VOLTAGE = 339,
	DIESEL_CH_SMAC_SWITCH_2 = 341,				//0X43 SECOND Diesel Running
	DIESEL_CH_SMAC_SWITCH_3,				//Diesel Generator Connected Status
	DIESEL_CH_SMAC_RELAY_2_STATUS = 353,			//Relay2 Status
	DIESEL_CH_SMAC_RELAY_3_STATUS,			//Relay3 Status
	DIESEL_CH_SMAC_RELAY_4_STATUS,			//Relay4 Status
	DIESEL_CH_TEST_START_STOP_STAT = 356,
	DIESEL_CH_TEST_STOP_CAUSE	   = 357,
	DIESEL_CH_SMAC_ALARM_HI_WATER_TEMPERATURE = 375,//High Water Temp Status
	DIESEL_CH_SMAC_ALARM_LOW_OIL_PRESSURE,			//Low Oil Pressure Status
	DIESEL_CH_SMAC_ALARM_LOW_OIL_POSITION,			//Low Fuel Level Status
	DIESEL_CH_SMAC_ALARM_DIESEL_GENERATOR_FAIL,		//Diesel Generator Fail Status
	DIESEL_CH_SMAC_MAX_DURATION			 = 381,		//Max duration for Diesel Test
	DIESEL_CH_SMAC_GETANLG_VOLTAGE_LOWER = 410,			//Battery Voltage Limit		
	DIESEL_CH_SMAC_COMM_STATUS = 420, 
	DIESEL_CH_SMAC_WORKING_STATUS
	
};


union _SMACSTRTOFLOAT
{
	float f_value;       // ������
	BYTE  by_value[4];   // �ַ���
};
typedef union _SMACSTRTOFLOAT	SMACSTRTOFLOAT;

struct _SMAC_STRING_SIG_MAP
{
	int		SigRough;
	int		SigReceive;
};
typedef struct _SMAC_STRING_SIG_MAP	SMAC_STRING_SIG_MAP;


struct SMACSamplerData
{
	BOOL			bNeedReconfig;

	RS485_VALUE    aRoughDataSMAC[SMAC_NUM][SMAC_MAX_SIGNALS_NUM];
	//SAMPLING_VALUE	aRoughDataGroup[RS485GROUP_MAX_SIGNALS_NUM];
};

typedef struct SMACSamplerData	SMAC_SAMP_DATA;	




struct _SMACDATASTRUCT
{
    BYTE nType;     // ��������:0-������־,1-4BYTES(�޷��ţ�,2-4 bytes���з��ţ� 3-2Bytes
    int  nOffset;   // ������ʼ��ַ���ڱ����ݰ��е���ʼλ�ô�ͷ�ַ���ʼ��0��
    int  nScaleBit; // ���ڿ�����,��λ�ţ�ģ������������ϵ,��10���ȡ�
    int  nChanel;   // ϵͳ�����ͨ���ţ�����ģ��Ĳ���������ƫ�Ʊ�ʾ��    
};
typedef struct _SMACDATASTRUCT  SMACDATASTRUCT;


// ϵͳ������ģ�����������������
struct _STRUCTCMD
{
    int nCmdType;		 // ����ؼ���
    int nScale;			 // ����ϵ��
};
typedef struct _STRUCTCMD  STRUCTCMD;

//add 2010/12/31
struct _ST_SMAC_PARAM
{	
	INT32	 Sig_EquipId;
	INT32	 Sig_Type;
	INT32	 Sig_Id;
	INT32	 SignCmdNoIdx;
};
typedef struct _ST_SMAC_PARAM ST_SMAC_PARAM;

INT32 SMAC_InitRoughValue(void* pDevice);
INT32 SMACSample(void* pDevice);
INT32 SMAC_StuffChannel(RS485_DEVICE_CLASS*  SMACDeviceClass,ENUMSIGNALPROC EnumProc,LPVOID lpvoid);
void  SMAC_GetProdctInfo(HANDLE hComm, int nUnitNo, void *pPI);
INT32 SMACNeedCommProc(void);
INT32 SMACReopenPort(void);
#endif


/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : NGMC-HF(Next Generation Controller - High Feature)
*
*  FILENAME : re485_large_dus.h
*  CREATOR  : Frank Cao                DATE: 2009-06-16 13:24
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/
#ifndef __RS485_SAMPLER_LDU_H
#define __RS485_SAMPLER_LDU_H

#include <stdio.h>

#include "basetypes.h"
#include "err_code.h"
#include "pubfunc.h"
#include "string.h"

#include "rs485_main.h"

#define RS485_SAMPLER_TASK				"485_SAMP"

#define	LDU_SOI_BYTE					0x7e
#define	LDU_EOI_BYTE					0x0d
#define	LDU_INVALID_SIG_ID				0xFFFF
#define	LDU_RESERVE_SIG_ID				0xFFFE

#define LDU_CH_END_FLAG					-1
#define	LDU_NORMAL						0

#define LDU_MAX_EXIST_NUM				15	

#define	LDU_MAX_COMM_BREAK_TIMES		5

#define	LDU_MAX_ADDR_NUM				32

//0x40 -> 0xA0
#define LDU_ADDR_OFFSET					0//(0xA0 - 0x40)
#define	LDU_FST_ACD_ADDR				(LDU_ADDR_OFFSET + 64)
#define	LDU_FST_DCD_ADDR				(LDU_ADDR_OFFSET + 72)
#define	LDU_FST_ACDCD_ADDR				(LDU_ADDR_OFFSET + 80)
#define	LDU_FST_OTHER_DCD_ADDR			(LDU_ADDR_OFFSET + 88)
#define	LDU_LST_ADDR					(LDU_ADDR_OFFSET + 95)

#define	LDU_MAX_ACD_NUM					5
#define	LDU_MAX_DCD_NUM					10
#define	LDU_MAX_BATT_NUM				40
#define LDU_ACTUAL_MAX_BATT_NUM			20
#define	LDU_MAX_LVD_NUM					10	//There are 3 LVDs in one LVD equipment

#define	LDU_MAX_BATT_NUM_EACH_DCD		4

#define LDU_ACD_MAX_AC_INPUT			3
#define LDU_DCD_MAX_BATT_NUM			4
#define LDU_DCD_MAX_TEMP_NUM			3
#define LDU_DCD_MAX_LOAD_CURR_NUM		9
#define LDU_DCD_MAX_LOAD_FUSE_NUM		64

#define	LDU_EVEN						0
#define	LDU_ODD							1

#define	LDU_CMD_ID_GET_ALL				0x46
#define	LDU_CMD_ID_GET_SYS_CFG			0x35
#define	LDU_CMD_ID_GET_UP_DN_LMT_CFG	0x34
#define	LDU_CMD_ID_GET_SYS_PARAM		0x45
#define	LDU_CMD_ID_GET_UP_DN_LMT		0x44
#define	LDU_CMD_ID_SET_UP_DN_LMT		0x51
#define	LDU_CMD_ID_SET_SYS_PARAM		0x52
#define	LDU_CMD_ID_CONTROL_CMD			0x53
#define	LDU_CMD_ID_GET_ANALOG_CFG		0x32


#define	LDU_ACD_GROUP_EQUIP_ID			292
#define	LDU_ACD_UNIT_EQUIP_ID			293
#define	LDU_DCD_GROUP_EQUIP_ID			298
#define	LDU_DCD_UNIT_EQUIP_ID			299
#define	LDU_LVD_UNIT_EQUIP_ID			309
#define	LDU_BATT_UNIT_EQUIP_ID			319
#define	LDU_BATT_GROUP_EQUIP_ID			115

#define LDU_RECT_GROUP_EQUIP_ID			2
#define LDU_RECT_RATED_VOLTAGE_SIG_ID	21
#define LDU_RECT_RATED_VOLTAGE_TYPE_ID	0

#define LDU_LVD_GROUP_EQUIP_ID			186
#define LDU_24V_LVD1_DISC_SIG_ID		51
#define LDU_24V_LVD2_DIS_SIG_ID			56
#define LDU_48V_LVD1_DISC_SIG_ID		1
#define LDU_48V_LVD2_DIS_SIG_ID			6
#define LDU_LVD_DIS_SIG_TYPE			2


#define	LDU_AC_OVER_VOLT_SIG_ID			2
#define	LDU_AC_UNDER_VOLT_SIG_ID		3
#define	LDU_AC_P_FAIL_VOLT_SIG_ID		4
#define	LDU_AC_OVER_FREQ_SIG_ID			5
#define	LDU_AC_UNDER_FREQ_SIG_ID		6
#define	LDU_AC_CURR_COEF_SIG_ID			6	
#define	LDU_AC_INPUT_TYPE_SIG_ID		7
#define	LDU_AC_INPUT_NUM_SIG_ID			8
#define	LDU_AC_CURR_M_MODE_SIG_ID		9
#define	LDU_AC_OUTPUT_NUM_SIG_ID		10

#define	LDU_DC_OVER_VOLT_SIG_ID			15
#define	LDU_DC_UNDER_VOLT_SIG_ID		16
#define	LDU_BATT_OVER_VOLT_SIG_ID		7
#define	LDU_BATT_UNDER_VOLT_SIG_ID		8
#define	LDU_DC_OVER_TEMP1_SIG_ID		1
#define	LDU_DC_OVER_TEMP2_SIG_ID		2
#define	LDU_DC_OVER_TEMP3_SIG_ID		3
#define	LDU_DC_TEMP_COEF_SIG_ID			9
#define	LDU_DC_LOAD_CURR_COEF_SIG_ID	10
#define	LDU_DC_BATT_NUM_SIG_ID			11
#define	LDU_DC_TEMP_NUM_SIG_ID			12
#define	LDU_DC_BRANCH_CURR_COEF_SIG_ID	13
#define	LDU_DC_BRANCH_CURR_NUM_SIG_ID	15
#define	LDU_DC_FUSE_NUM_SIG_ID			16

#define	LDU_BT_CURR_COEF_SIG_ID			1
#define	LDU_BT_RATED_CAP_SIG_ID			10
#define	LDU_BTG_OVER_CURR_SIG_ID		45

//#define	LDU_DC_RATED_CAP_SIG_ID			17
#define	LDU_DC_RATED_CAP_SIG_ID			75

#define LDU_DCD_BATT_NUM_SIG_ID			11

#define	LDU_EQUIP_EXIST			0
#define	LDU_EQUIP_NOT_EXIST		1

#define LDU_COMM_OK				0
#define LDU_COMM_FAILURE		1

#define LDU_STATUS_OK			0
#define LDU_STATUS_ALARM		1


//Data type
enum LDU_SIGNAL_VALUE_TYPE_DEF
{
	LDU_VALUE_TYPE_BYTE = 0,
	LDU_VALUE_TYPE_WORD,
	LDU_VALUE_TYPE_FLOAT,

	LDU_VALUE_TYPE_INVALID,
};

enum LDU_FRAME_OFFSET_DEF
{
	FRAME_OFFSET_SOI = 0,
	FRAME_OFFSET_ADDR = 1,
	FRAME_OFFSET_CMD_ID = 3,
	FRAME_OFFSET_LENGTH = 5,
	FRAME_OFFSET_DATA = 9,
};


#define	LDU_MAX_CHN_NUM_EVERY_ACD	80
#define	LDU_MAX_CHN_NUM_EVERY_DCD	100
#define	LDU_MAX_CHN_NUM_EVERY_BATT	20
#define	LDU_MAX_CHN_NUM_EVERY_LVD	10

//The channel No. is defined in "RS485�ɼ�����.xls"
enum LDU_SAMPLING_CHANNEL
{
	LDU_S_CH_ACDG_WORK_STATUS = 7998,
	LDU_S_CH_DCDG_WORK_STATUS,

	LDU_S_CH_FIRST_CHN = 8000,							//First Channel No for Large DU

	LDU_S_CH_G_ACD_QTY,									//Qty of AC Distributions
	LDU_S_CH_G_AC_FAIL,									//AC Failure in All ACDs 

	LDU_S_CH_MAINS_1_UA = LDU_S_CH_FIRST_CHN + 51,		//Mains I Uab/Ua Voltage
	LDU_S_CH_MAINS_1_UB,								//Mains I Ubc/Ub Voltage
	LDU_S_CH_MAINS_1_UC,								//Mains I Uca/Uc Voltage
	LDU_S_CH_MAINS_2_UA,								//Mains II Uab/Ua Voltage
	LDU_S_CH_MAINS_2_UB,								//Mains II Ubc/Ub Voltage
	LDU_S_CH_MAINS_2_UC,								//Mains II Uca/Uc Voltage
	LDU_S_CH_MAINS_3_UA,								//Mains III Uab/Ua Voltage
	LDU_S_CH_MAINS_3_UB,								//Mains III Ubc/Ub Voltage
	LDU_S_CH_MAINS_3_UC,								//Mains III Uca/Uc Voltage
	LDU_S_CH_PA_CURRENT,								//A Phase Current
	LDU_S_CH_PB_CURRENT,								//B Phase Current
	LDU_S_CH_PC_CURRENT,								//C Phase Current
	LDU_S_CH_AC_FREQ,									//AC Frequency
	LDU_S_CH_INPUT_SW_MODE,								//Input Switch Mode
	LDU_S_CH_FAULT_LIGHT,								//Fault Light
	LDU_S_CH_MAINS_INPUT_NO,							//Mains Input No.
	LDU_S_CH_ACD_ADDR,									//AC Distribution Address
	LDU_S_CH_RESERVE2,									//Reserve
	LDU_S_CH_AC_OUTPUT1_ST,								//AC Output 1 State
	LDU_S_CH_AC_OUTPUT2_ST,								//AC Output 2 State
	LDU_S_CH_AC_OUTPUT3_ST,								//AC Output 3 State
	LDU_S_CH_AC_OUTPUT4_ST,								//AC Output 4 State
	LDU_S_CH_AC_OUTPUT5_ST,								//AC Output 5 State
	LDU_S_CH_AC_OUTPUT6_ST,								//AC Output 6 State
	LDU_S_CH_AC_OUTPUT7_ST,								//AC Output 7 State
	LDU_S_CH_AC_OUTPUT8_ST,								//AC Output 8 State
	LDU_S_CH_OVER_FREQ_ST,								//Over Frequency
	LDU_S_CH_UNDER_FREQ_ST,								//Under Frequency
	LDU_S_CH_INPUT_MCCB_ST,								//AC Input MCCB Trip
	LDU_S_CH_SPD_TRIP_ST,								//SPD Trip
	LDU_S_CH_OUTPUT_MCCB_ST,							//AC Output MCCB Trip
	LDU_S_CH_MAINS1_FAIL_ST,							//Mains I AC Failure
	LDU_S_CH_MAINS2_FAIL_ST,							//Mains II AC Failure
	LDU_S_CH_MAINS3_FAIL_ST,							//Mains II AC Failure
	LDU_S_CH_M1_UA_UNDER_VOLT,							//Mains I Uab/Ua Under Voltage
	LDU_S_CH_M1_UB_UNDER_VOLT,							//Mains I Ubc/Ub Under Voltage
	LDU_S_CH_M1_UC_UNDER_VOLT,							//Mains I Uca/Uc Under Voltage
	LDU_S_CH_M2_UA_UNDER_VOLT,							//Mains II Uab/Ua Under Voltage
	LDU_S_CH_M2_UB_UNDER_VOLT,							//Mains II Ubc/Ub Under Voltage
	LDU_S_CH_M2_UC_UNDER_VOLT,							//Mains II Uca/Uc Under Voltage
	LDU_S_CH_M3_UA_UNDER_VOLT,							//Mains III Uab/Ua Under Voltage
	LDU_S_CH_M3_UB_UNDER_VOLT,							//Mains III Ubc/Ub Under Voltage
	LDU_S_CH_M3_UC_UNDER_VOLT,							//Mains III Uca/Uc Under Voltage
	LDU_S_CH_M1_UA_OVER_VOLT,							//Mains I Uab/Ua Over Voltage
	LDU_S_CH_M1_UB_OVER_VOLT,							//Mains I Ubc/Ub Over Voltage
	LDU_S_CH_M1_UC_OVER_VOLT,							//Mains I Uca/Uc Over Voltage
	LDU_S_CH_M2_UA_OVER_VOLT,							//Mains II Uab/Ua Over Voltage
	LDU_S_CH_M2_UB_OVER_VOLT,							//Mains II Ubc/Ub Over Voltage
	LDU_S_CH_M2_UC_OVER_VOLT,							//Mains II Uca/Uc Over Voltage
	LDU_S_CH_M3_UA_OVER_VOLT,							//Mains III Uab/Ua Over Voltage
	LDU_S_CH_M3_UB_OVER_VOLT,							//Mains III Ubc/Ub Over Voltage
	LDU_S_CH_M3_UC_OVER_VOLT,							//Mains III Uca/Uc Over Voltage
	LDU_S_CH_M1_PHASE_A_FAIL,							//Mains I A Phase Failure
	LDU_S_CH_M1_PHASE_B_FAIL,							//Mains I B Phase Failure
	LDU_S_CH_M1_PHASE_C_FAIL,							//Mains I C Phase Failure
	LDU_S_CH_M2_PHASE_A_FAIL,							//Mains II A Phase Failure
	LDU_S_CH_M2_PHASE_B_FAIL,							//Mains II B Phase Failure
	LDU_S_CH_M2_PHASE_C_FAIL,							//Mains II C Phase Failure
	LDU_S_CH_M3_PHASE_A_FAIL,							//Mains III A Phase Failure
	LDU_S_CH_M3_PHASE_B_FAIL,							//Mains III B Phase Failure
	LDU_S_CH_M3_PHASE_C_FAIL,							//Mains III C Phase Failure
	LDU_S_CH_ACD_NO_RESPONSE,							//AC Distribution No Response
	LDU_S_CH_ACD_EXISTS,								//AC Distribution Exists
	LDU_S_CH_M1_UA_VOLT_ALM,							//Mains1 Uab/Ua Voltage Alarm
	LDU_S_CH_M1_UB_VOLT_ALM,							//Mains1 Ubc/Ub Voltage Alarm
	LDU_S_CH_M1_UC_VOLT_ALM,							//Mains1 Uca/Uc Voltage Alarm
	LDU_S_CH_M2_UA_VOLT_ALM,							//Mains2 Uab/Ua Voltage Alarm
	LDU_S_CH_M2_UB_VOLT_ALM,							//Mains2 Ubc/Ub Voltage Alarm
	LDU_S_CH_M2_UC_VOLT_ALM,							//Mains2 Uca/Uc Voltage Alarm
	LDU_S_CH_M3_UA_VOLT_ALM,							//Mains3 Uab/Ua Voltage Alarm
	LDU_S_CH_M3_UB_VOLT_ALM,							//Mains3 Ubc/Ub Voltage Alarm
	LDU_S_CH_M3_UC_VOLT_ALM,							//Mains3 Uca/Uc Voltage Alarm

	LDU_S_CH_G_DCD_FIRST_CHN = LDU_S_CH_FIRST_CHN + 450,
	LDU_S_CH_G_DCD_QTY,									//Qty of DC Distributions

	LDU_S_CH_TEMPERATURE1 = LDU_S_CH_G_DCD_FIRST_CHN + 51,	//Temperature 1
	LDU_S_CH_TEMPERATURE2,								//Temperature 2
	LDU_S_CH_TEMPERATURE3,								//Temperature 3
	LDU_S_CH_BUS_VOLT,									//Bus Voltage
	LDU_S_CH_LOAD_CURR,									//Load Current
	LDU_S_CH_BRANCH1_CURR,								//Branch 1 Current
	LDU_S_CH_BRANCH2_CURR,								//Branch 2 Current
	LDU_S_CH_BRANCH3_CURR,								//Branch 3 Current
	LDU_S_CH_BRANCH4_CURR,								//Branch 4 Current
	LDU_S_CH_BRANCH5_CURR,								//Branch 5 Current
	LDU_S_CH_BRANCH6_CURR,								//Branch 6 Current
	LDU_S_CH_BRANCH7_CURR,								//Branch 7 Current
	LDU_S_CH_BRANCH8_CURR,								//Branch 8 Current
	LDU_S_CH_BRANCH9_CURR,								//Branch 9 Current
	LDU_S_CH_DC_OVER_VOLT,								//DC Over Voltage
	LDU_S_CH_DC_UNDER_VOLT,								//DC Under Voltage
	LDU_S_CH_OVER_TEMP1,								//Temperature 1 Over Temperature
	LDU_S_CH_OVER_TEMP2,								//Temperature 2 Over Temperature
	LDU_S_CH_OVER_TEMP3,								//Temperature 3 Over Temperature
	LDU_S_CH_OUTPUT1_DISCON,							//DC Output 1 Disconnected
	LDU_S_CH_OUTPUT2_DISCON,							//DC Output 2 Disconnected
	LDU_S_CH_OUTPUT3_DISCON,							//DC Output 3 Disconnected
	LDU_S_CH_OUTPUT4_DISCON,							//DC Output 4 Disconnected
	LDU_S_CH_OUTPUT5_DISCON,							//DC Output 5 Disconnected
	LDU_S_CH_OUTPUT6_DISCON,							//DC Output 6 Disconnected
	LDU_S_CH_OUTPUT7_DISCON,							//DC Output 7 Disconnected
	LDU_S_CH_OUTPUT8_DISCON,							//DC Output 8 Disconnected
	LDU_S_CH_OUTPUT9_DISCON,							//DC Output 9 Disconnected
	LDU_S_CH_OUTPUT10_DISCON,							//DC Output 10 Disconnected
	LDU_S_CH_OUTPUT11_DISCON,							//DC Output 11 Disconnected
	LDU_S_CH_OUTPUT12_DISCON,							//DC Output 12 Disconnected
	LDU_S_CH_OUTPUT13_DISCON,							//DC Output 13 Disconnected
	LDU_S_CH_OUTPUT14_DISCON,							//DC Output 14 Disconnected
	LDU_S_CH_OUTPUT15_DISCON,							//DC Output 15 Disconnected
	LDU_S_CH_OUTPUT16_DISCON,							//DC Output 16 Disconnected
	LDU_S_CH_OUTPUT17_DISCON,							//DC Output 17 Disconnected
	LDU_S_CH_OUTPUT18_DISCON,							//DC Output 18 Disconnected
	LDU_S_CH_OUTPUT19_DISCON,							//DC Output 19 Disconnected
	LDU_S_CH_OUTPUT20_DISCON,							//DC Output 20 Disconnected
	LDU_S_CH_OUTPUT21_DISCON,							//DC Output 21 Disconnected
	LDU_S_CH_OUTPUT22_DISCON,							//DC Output 22 Disconnected
	LDU_S_CH_OUTPUT23_DISCON,							//DC Output 23 Disconnected
	LDU_S_CH_OUTPUT24_DISCON,							//DC Output 24 Disconnected
	LDU_S_CH_OUTPUT25_DISCON,							//DC Output 25 Disconnected
	LDU_S_CH_OUTPUT26_DISCON,							//DC Output 26 Disconnected
	LDU_S_CH_OUTPUT27_DISCON,							//DC Output 27 Disconnected
	LDU_S_CH_OUTPUT28_DISCON,							//DC Output 28 Disconnected
	LDU_S_CH_OUTPUT29_DISCON,							//DC Output 29 Disconnected
	LDU_S_CH_OUTPUT30_DISCON,							//DC Output 30 Disconnected
	LDU_S_CH_OUTPUT31_DISCON,							//DC Output 31 Disconnected
	LDU_S_CH_OUTPUT32_DISCON,							//DC Output 32 Disconnected
	LDU_S_CH_OUTPUT33_DISCON,							//DC Output 33 Disconnected
	LDU_S_CH_OUTPUT34_DISCON,							//DC Output 34 Disconnected
	LDU_S_CH_OUTPUT35_DISCON,							//DC Output 35 Disconnected
	LDU_S_CH_OUTPUT36_DISCON,							//DC Output 36 Disconnected
	LDU_S_CH_OUTPUT37_DISCON,							//DC Output 37 Disconnected
	LDU_S_CH_OUTPUT38_DISCON,							//DC Output 38 Disconnected
	LDU_S_CH_OUTPUT39_DISCON,							//DC Output 39 Disconnected
	LDU_S_CH_OUTPUT40_DISCON,							//DC Output 40 Disconnected
	LDU_S_CH_OUTPUT41_DISCON,							//DC Output 41 Disconnected
	LDU_S_CH_OUTPUT42_DISCON,							//DC Output 42 Disconnected
	LDU_S_CH_OUTPUT43_DISCON,							//DC Output 43 Disconnected
	LDU_S_CH_OUTPUT44_DISCON,							//DC Output 44 Disconnected
	LDU_S_CH_OUTPUT45_DISCON,							//DC Output 45 Disconnected
	LDU_S_CH_OUTPUT46_DISCON,							//DC Output 46 Disconnected
	LDU_S_CH_OUTPUT47_DISCON,							//DC Output 47 Disconnected
	LDU_S_CH_OUTPUT48_DISCON,							//DC Output 48 Disconnected
	LDU_S_CH_OUTPUT49_DISCON,							//DC Output 49 Disconnected
	LDU_S_CH_OUTPUT50_DISCON,							//DC Output 50 Disconnected
	LDU_S_CH_OUTPUT51_DISCON,							//DC Output 51 Disconnected
	LDU_S_CH_OUTPUT52_DISCON,							//DC Output 52 Disconnected
	LDU_S_CH_OUTPUT53_DISCON,							//DC Output 53 Disconnected
	LDU_S_CH_OUTPUT54_DISCON,							//DC Output 54 Disconnected
	LDU_S_CH_OUTPUT55_DISCON,							//DC Output 55 Disconnected
	LDU_S_CH_OUTPUT56_DISCON,							//DC Output 56 Disconnected
	LDU_S_CH_OUTPUT57_DISCON,							//DC Output 57 Disconnected
	LDU_S_CH_OUTPUT58_DISCON,							//DC Output 58 Disconnected
	LDU_S_CH_OUTPUT59_DISCON,							//DC Output 59 Disconnected
	LDU_S_CH_OUTPUT60_DISCON,							//DC Output 60 Disconnected
	LDU_S_CH_OUTPUT61_DISCON,							//DC Output 61 Disconnected
	LDU_S_CH_OUTPUT62_DISCON,							//DC Output 62 Disconnected
	LDU_S_CH_OUTPUT63_DISCON,							//DC Output 63 Disconnected
	LDU_S_CH_OUTPUT64_DISCON,							//DC Output 64 Disconnected
	LDU_S_CH_DCD_ADDR,									//DC Distribution Address
	LDU_S_CH_DC_SPD,									//DC SPD Alarm
	LDU_S_CH_RESEVER_5086,								//Reserve
	LDU_S_CH_DCD_NO_RESPONSE,							//DC Distribution No Response
	LDU_S_CH_DCD_EXISTS,								//DC Distribution Exists

	LDU_S_CH_G_BATT_FIRST_CHN = LDU_S_CH_FIRST_CHN + 1500,
	LDU_S_CH_G_BATT_QTY,								//Qty of Large DU Batteries

	LDU_S_CH_BATT_VOLT = LDU_S_CH_G_BATT_FIRST_CHN + 51,	//Battery Voltage
	LDU_S_CH_BATT_CURR,									//Battery Current
	LDU_S_CH_BATT_OVER_VOLT,							//Battery Over Voltage
	LDU_S_CH_BATT_UNDER_VOLT,							//Battery Under Voltage
	LDU_S_CH_BATT_OVER_CURR,							//Battery Over Cutrrent
	LDU_S_CH_BATT_FUSE_BREAK,							//Battery Route Interrupted
	LDU_S_CH_DCD_NO,									//DCD No that the batterry connect to
	LDU_S_CH_BATT_EXISTS,								//Battery Exists
	LDU_S_CH_BATT_CURR_COEF,							//Battery Current Coef
	LDU_S_CH_BATT_COMM_STS,								//3/5��� ���ͨ��״̬

	LDU_S_CH_G_LVD_FIRST_CHN = LDU_S_CH_FIRST_CHN + 1950,

	LDU_S_CH_LVD_EQUIP_QTY = LDU_S_CH_G_LVD_FIRST_CHN + 1,

	LDU_S_CH_LVD1_STATE = LDU_S_CH_G_LVD_FIRST_CHN + 51,	//LVD 1 State
	LDU_S_CH_LVD2_STATE,								//LVD 2 State
	LDU_S_CH_LVD3_STATE,								//LVD 3 State

	LDU_S_CH_LVD_COMM_STAT,
	LDU_S_CH_LVD_DCD_NO,
	LDU_S_CH_LVD_EQUIP_EXISTS,							//LVD Equipment Exists

	//add 2010/3/19 
	LDU_S_CH_G_DCD_LOAD_CURRENT			= 10885,
	LDU_S_CH_G_DCD_TOTAL_BATT_CURRENT	= 10886,
	LDU_S_CH_G_DCD_AVERAGE_VOLT			= 10887,


};

enum LDU_CONTROL_CHANNEL
{
	LDU_C_CH_FIRST_CHN = 4500,							//First Channel No for Large DU

	LDU_C_CH_FAULT_LIGHT = LDU_C_CH_FIRST_CHN + 51,		//Fault Light Control

	LDU_C_CH_LVD1 = LDU_C_CH_FIRST_CHN + 2001,			//LVD 1 Control
	LDU_C_CH_LVD2,										//LVD 2 Control
	LDU_C_CH_LVD3,										//LVD 3 Control
};



enum ROUGH_DATA_IDX_LDU_GROUP
{
	LDU_G_ACD_QTY = 0,									//Qty of AC Distributions
	LDU_G_AC_FAIL,										//AC Failure in All ACDs 
	LDU_G_DCD_QTY,										//Qty of DC Distributions
	LDU_G_BATT_QTY,										//Qty of Large DU Batteries
	LDU_G_LVD_QTY,
	LDU_G_ACDG_WORK_STATUS,
	LDU_G_DCDG_WORK_STATUS,

	//add 2010/3/19
	LDU_G_DCD_LOAD_CURRENT,
	LDU_G_DCD_TOTAL_BATT_CURRENT,
	LDU_G_DCD_AVERAGE_VOLT,

	LDU_G_MAX_SIGNALS_NUM,
}; 

enum ROUGH_DATA_IDX_LDU_ACD
{
	LDU_MAINS_1_UA = 0,									//Mains I Uab/Ua Voltage
	LDU_MAINS_1_UB,										//Mains I Ubc/Ub Voltage
	LDU_MAINS_1_UC,										//Mains I Uca/Uc Voltage
	LDU_MAINS_2_UA,										//Mains II Uab/Ua Voltage
	LDU_MAINS_2_UB,										//Mains II Ubc/Ub Voltage
	LDU_MAINS_2_UC,										//Mains II Uca/Uc Voltage
	LDU_MAINS_3_UA,										//Mains III Uab/Ua Voltage
	LDU_MAINS_3_UB,										//Mains III Ubc/Ub Voltage
	LDU_MAINS_3_UC,										//Mains III Uca/Uc Voltage
	LDU_PA_CURRENT,										//A Phase Current
	LDU_PB_CURRENT,										//B Phase Current
	LDU_PC_CURRENT,										//C Phase Current
	LDU_AC_FREQ,										//AC Frequency
	LDU_INPUT_SW_MODE,									//Input Switch Mode
	LDU_FAULT_LIGHT,									//Fault Light
	LDU_MAINS_INPUT_NO,									//Mains Input No.
	LDU_AC_OUTPUT1_ST,									//AC Output 1 State
	LDU_AC_OUTPUT2_ST,									//AC Output 2 State
	LDU_AC_OUTPUT3_ST,									//AC Output 3 State
	LDU_AC_OUTPUT4_ST,									//AC Output 4 State
	LDU_AC_OUTPUT5_ST,									//AC Output 5 State
	LDU_AC_OUTPUT6_ST,									//AC Output 6 State
	LDU_AC_OUTPUT7_ST,									//AC Output 7 State
	LDU_AC_OUTPUT8_ST,									//AC Output 8 State
	LDU_OVER_FREQ_ST,									//Over Frequency
	LDU_UNDER_FREQ_ST,									//Under Frequency
	LDU_INPUT_MCCB_ST,									//AC Input MCCB Trip
	LDU_SPD_TRIP_ST,									//SPD Trip
	LDU_OUTPUT_MCCB_ST,									//AC Output MCCB Trip
	LDU_MAINS1_FAIL_ST,									//Mains I AC Failure
	LDU_MAINS2_FAIL_ST,									//Mains II AC Failure
	LDU_MAINS3_FAIL_ST,									//Mains II AC Failure
	LDU_M1_UA_UNDER_VOLT,								//Mains I Uab/Ua Under Voltage
	LDU_M2_UA_UNDER_VOLT,								//Mains II Uab/Ua Under Voltage
	LDU_M3_UA_UNDER_VOLT,								//Mains III Uab/Ua Under Voltage
	LDU_M1_UB_UNDER_VOLT,								//Mains I Ubc/Ub Under Voltage
	LDU_M2_UB_UNDER_VOLT,								//Mains II Ubc/Ub Under Voltage
	LDU_M3_UB_UNDER_VOLT,								//Mains III Ubc/Ub Under Voltage
	LDU_M1_UC_UNDER_VOLT,								//Mains I Uca/Uc Under Voltage
	LDU_M2_UC_UNDER_VOLT,								//Mains II Uca/Uc Under Voltage
	LDU_M3_UC_UNDER_VOLT,								//Mains III Uca/Uc Under Voltage
	LDU_M1_UA_OVER_VOLT,								//Mains I Uab/Ua Over Voltage
	LDU_M2_UA_OVER_VOLT,								//Mains II Uab/Ua Over Voltage
	LDU_M3_UA_OVER_VOLT,								//Mains III Uab/Ua Over Voltage
	LDU_M1_UB_OVER_VOLT,								//Mains I Ubc/Ub Over Voltage
	LDU_M2_UB_OVER_VOLT,								//Mains II Ubc/Ub Over Voltage
	LDU_M3_UB_OVER_VOLT,								//Mains III Ubc/Ub Over Voltage
	LDU_M1_UC_OVER_VOLT,								//Mains I Uca/Uc Over Voltage
	LDU_M2_UC_OVER_VOLT,								//Mains II Uca/Uc Over Voltage
	LDU_M3_UC_OVER_VOLT,								//Mains III Uca/Uc Over Voltage
	LDU_M1_PHASE_A_FAIL,								//Mains I A Phase Failure
	LDU_M2_PHASE_A_FAIL,								//Mains II A Phase Failure
	LDU_M3_PHASE_A_FAIL,								//Mains III A Phase Failure
	LDU_M1_PHASE_B_FAIL,								//Mains I B Phase Failure
	LDU_M2_PHASE_B_FAIL,								//Mains II B Phase Failure
	LDU_M3_PHASE_B_FAIL,								//Mains III B Phase Failure
	LDU_M1_PHASE_C_FAIL,								//Mains I C Phase Failure
	LDU_M2_PHASE_C_FAIL,								//Mains II C Phase Failure
	LDU_M3_PHASE_C_FAIL,								//Mains III C Phase Failure
	LDU_ACD_NO_RESPONSE,								//AC Distribution No Response
	LDU_ACD_EXISTS,										//AC Distribution Exists
	LDU_ACD_ADDR,
	LDU_ACD_INPUT_NUM,
	LDU_ACD_INPUT_TYPE,
	LDU_ACD_CURR_COEF,			
	LDU_ACD_OUTPUT_NUM,			
	LDU_ACD_CURR_MEASURE_MODE,	
	LDU_ACD_COMM_BREAK_TIMES,
	LDU_ACD_OVER_VOLT_LMT,	
	LDU_ACD_UNDER_VOLT_LMT,	
	LDU_ACD_P_FAIL_LMT,		
	LDU_ACD_OVER_FREQ_LMT,	
	LDU_ACD_UNDER_FREQ_LMT,	
	LDU_ACD_IMBALANCE_LMT,	
	LDU_M1_UA_VOLT_ALM,									//Mains1 Uab/Ua Voltage Alarm
	LDU_M1_UB_VOLT_ALM,									//Mains1 Ubc/Ub Voltage Alarm
	LDU_M1_UC_VOLT_ALM,									//Mains1 Uca/Uc Voltage Alarm
	LDU_M2_UA_VOLT_ALM,									//Mains2 Uab/Ua Voltage Alarm
	LDU_M2_UB_VOLT_ALM,									//Mains2 Ubc/Ub Voltage Alarm
	LDU_M2_UC_VOLT_ALM,									//Mains2 Uca/Uc Voltage Alarm
	LDU_M3_UA_VOLT_ALM,									//Mains3 Uab/Ua Voltage Alarm
	LDU_M3_UB_VOLT_ALM,									//Mains3 Ubc/Ub Voltage Alarm
	LDU_M3_UC_VOLT_ALM,									//Mains3 Uca/Uc Voltage Alarm

	LDU_M1_INPUT_ST,
	LDU_M2_INPUT_ST,
	LDU_M3_INPUT_ST,


	LDU_MAX_ACD_SIGNALS_NUM,

};

enum ROUGH_DATA_IDX_LDU_DCD
{
	LDU_TEMPERATURE1 = 0,								//Temperature 1
	LDU_TEMPERATURE2,									//Temperature 2
	LDU_TEMPERATURE3,									//Temperature 3
	LDU_BUS_VOLT,										//Bus Voltage
	LDU_LOAD_CURR,										//Load Current
	LDU_BRANCH1_CURR,									//Branch 1 Current
	LDU_BRANCH2_CURR,									//Branch 2 Current
	LDU_BRANCH3_CURR,									//Branch 3 Current
	LDU_BRANCH4_CURR,									//Branch 4 Current
	LDU_BRANCH5_CURR,									//Branch 5 Current
	LDU_BRANCH6_CURR,									//Branch 6 Current
	LDU_BRANCH7_CURR,									//Branch 7 Current
	LDU_BRANCH8_CURR,									//Branch 8 Current
	LDU_BRANCH9_CURR,									//Branch 9 Current
	LDU_DC_OVER_VOLT,									//DC Over Voltage
	LDU_DC_UNDER_VOLT,									//DC Under Voltage
	LDU_OVER_TEMP1,										//Temperature 1 Over Temperature
	LDU_OVER_TEMP2,										//Temperature 2 Over Temperature
	LDU_OVER_TEMP3,										//Temperature 3 Over Temperature
	LDU_OUTPUT1_DISCON,									//DC Output 1 Disconnected
	LDU_OUTPUT2_DISCON,									//DC Output 2 Disconnected
	LDU_OUTPUT3_DISCON,									//DC Output 3 Disconnected
	LDU_OUTPUT4_DISCON,									//DC Output 4 Disconnected
	LDU_OUTPUT5_DISCON,									//DC Output 5 Disconnected
	LDU_OUTPUT6_DISCON,									//DC Output 6 Disconnected
	LDU_OUTPUT7_DISCON,									//DC Output 7 Disconnected
	LDU_OUTPUT8_DISCON,									//DC Output 8 Disconnected
	LDU_OUTPUT9_DISCON,									//DC Output 9 Disconnected
	LDU_OUTPUT10_DISCON,								//DC Output 10 Disconnected
	LDU_OUTPUT11_DISCON,								//DC Output 11 Disconnected
	LDU_OUTPUT12_DISCON,								//DC Output 12 Disconnected
	LDU_OUTPUT13_DISCON,								//DC Output 13 Disconnected
	LDU_OUTPUT14_DISCON,								//DC Output 14 Disconnected
	LDU_OUTPUT15_DISCON,								//DC Output 15 Disconnected
	LDU_OUTPUT16_DISCON,								//DC Output 16 Disconnected
	LDU_OUTPUT17_DISCON,								//DC Output 17 Disconnected
	LDU_OUTPUT18_DISCON,								//DC Output 18 Disconnected
	LDU_OUTPUT19_DISCON,								//DC Output 19 Disconnected
	LDU_OUTPUT20_DISCON,								//DC Output 20 Disconnected
	LDU_OUTPUT21_DISCON,								//DC Output 21 Disconnected
	LDU_OUTPUT22_DISCON,								//DC Output 22 Disconnected
	LDU_OUTPUT23_DISCON,								//DC Output 23 Disconnected
	LDU_OUTPUT24_DISCON,								//DC Output 24 Disconnected
	LDU_OUTPUT25_DISCON,								//DC Output 25 Disconnected
	LDU_OUTPUT26_DISCON,								//DC Output 26 Disconnected
	LDU_OUTPUT27_DISCON,								//DC Output 27 Disconnected
	LDU_OUTPUT28_DISCON,								//DC Output 28 Disconnected
	LDU_OUTPUT29_DISCON,								//DC Output 29 Disconnected
	LDU_OUTPUT30_DISCON,								//DC Output 30 Disconnected
	LDU_OUTPUT31_DISCON,								//DC Output 31 Disconnected
	LDU_OUTPUT32_DISCON,								//DC Output 32 Disconnected
	LDU_OUTPUT33_DISCON,								//DC Output 33 Disconnected
	LDU_OUTPUT34_DISCON,								//DC Output 34 Disconnected
	LDU_OUTPUT35_DISCON,								//DC Output 35 Disconnected
	LDU_OUTPUT36_DISCON,								//DC Output 36 Disconnected
	LDU_OUTPUT37_DISCON,								//DC Output 37 Disconnected
	LDU_OUTPUT38_DISCON,								//DC Output 38 Disconnected
	LDU_OUTPUT39_DISCON,								//DC Output 39 Disconnected
	LDU_OUTPUT40_DISCON,								//DC Output 40 Disconnected
	LDU_OUTPUT41_DISCON,								//DC Output 41 Disconnected
	LDU_OUTPUT42_DISCON,								//DC Output 42 Disconnected
	LDU_OUTPUT43_DISCON,								//DC Output 43 Disconnected
	LDU_OUTPUT44_DISCON,								//DC Output 44 Disconnected
	LDU_OUTPUT45_DISCON,								//DC Output 45 Disconnected
	LDU_OUTPUT46_DISCON,								//DC Output 46 Disconnected
	LDU_OUTPUT47_DISCON,								//DC Output 47 Disconnected
	LDU_OUTPUT48_DISCON,								//DC Output 48 Disconnected
	LDU_OUTPUT49_DISCON,								//DC Output 49 Disconnected
	LDU_OUTPUT50_DISCON,								//DC Output 50 Disconnected
	LDU_OUTPUT51_DISCON,								//DC Output 51 Disconnected
	LDU_OUTPUT52_DISCON,								//DC Output 52 Disconnected
	LDU_OUTPUT53_DISCON,								//DC Output 53 Disconnected
	LDU_OUTPUT54_DISCON,								//DC Output 54 Disconnected
	LDU_OUTPUT55_DISCON,								//DC Output 55 Disconnected
	LDU_OUTPUT56_DISCON,								//DC Output 56 Disconnected
	LDU_OUTPUT57_DISCON,								//DC Output 57 Disconnected
	LDU_OUTPUT58_DISCON,								//DC Output 58 Disconnected
	LDU_OUTPUT59_DISCON,								//DC Output 59 Disconnected
	LDU_OUTPUT60_DISCON,								//DC Output 60 Disconnected
	LDU_OUTPUT61_DISCON,								//DC Output 61 Disconnected
	LDU_OUTPUT62_DISCON,								//DC Output 62 Disconnected
	LDU_OUTPUT63_DISCON,								//DC Output 63 Disconnected
	LDU_OUTPUT64_DISCON,								//DC Output 64 Disconnected
	LDU_DCD_NO_RESPONSE,								//DC Distribution No Response
	LDU_DCD_EXISTS,										//DC Distribution Exists
	LDU_DCD_ADDR,
	LDU_DCD_DC_SPD,
	LDU_DCD_TEMP_NUM,
	LDU_DCD_BATT_NUM,
	LDU_DCD_BRANCH_CURR_NUM,
	LDU_DCD_FUSE_NUM,
	LDU_DCD_TEMP_COEF,			
	LDU_DCD_LOAD_CURR_COEF,		
	LDU_DCD_BRANCH_CURR_COEF,	
	LDU_DCD_OVER_TEMP1_LMT,	
	LDU_DCD_OVER_TEMP2_LMT,	
	LDU_DCD_OVER_TEMP3_LMT,	
	LDU_DCD_OVER_VOLT_LMT,	
	LDU_DCD_UNDER_VOLT_LMT,	
	LDU_DCD_AC_SWITCH_LOW,
	LDU_DCD_AC_SWITCH_HIGH,
	LDU_DCD_COMM_BREAK_TIMES,	

	LDU_MAX_DCD_SIGNALS_NUM,
};


enum ROUGH_DATA_IDX_LDU_BATT
{
	LDU_BATT_VOLT = 0,									//Battery Voltage
	LDU_BATT_CURR,										//Battery Current
	LDU_BATT_OVER_VOLT,									//Battery Over Voltage
	LDU_BATT_UNDER_VOLT,								//Battery Under Voltage
	LDU_BATT_OVER_CURR,									//Battery Over Cutrrent
	LDU_BATT_FUSE_BREAK,								//Battery Route Interrupted
	LDU_BATT_DCD_NO,									//DCD No that the batterry connect to
	LDU_BATT_EXISTS,									//Battery Exists
	LDU_BATT_CURR_COEF,			
	LDU_BATT_OVER_VOLT_LMT,	
	LDU_BATT_UNDER_VOLT_LMT,
	LDU_BATT_OVER_CURR_LMT,	

	LDU_BATT_COMM_STS,									//���LARGEDUͨ��״̬  3/5
	LDU_MAX_BATT_SIGNALS_NUM,

};

enum ROUGH_DATA_IDX_LDU_LVD
{
	LDU_LVD1_STATE = 0,									//LVD 1 State
	LDU_LVD2_STATE,										//LVD 2 State
	LDU_LVD3_STATE,										//LVD 3 State
	LDU_LVD_EQUIP_EXISTS,								//LVD Equipment Exists
	LDU_LVD_DCD_NO,										//DCD No that the batterry connect to
	LDU_LVD1_VOLT_LMT,									//Load LVD voltage
	LDU_LVD2_VOLT_LMT,									//Battery 1 LVD Voltage
	LDU_LVD3_VOLT_LMT,									//Battery 2 LVD Voltage
	LDU_COMM_STAT,
	LDU_MAX_LVD_SIGNALS_NUM,

};

enum LDU_EQUIP_TYPE
{
	LDU_EQUIP_TYPE_GROUP = 0,
	LDU_EQUIP_TYPE_ACD,			
	LDU_EQUIP_TYPE_DCD,			
	LDU_EQUIP_TYPE_BATT,	
	LDU_EQUIP_TYPE_LVD,

	LDU_EQUIP_TYPE_MAX_NUM,

};


struct	stLDU_SIG_INFO
{
	int		iRoughDataIdx;
	int		iOffsetStep;
	int		iMaxValue;
	int		iMinValue;
};
typedef struct stLDU_SIG_INFO LDU_SIG_INFO;


struct	stSEND_CMD
{
	BYTE	byAddr;			//Destination Address
	BYTE	byCmdId;		//Command ID
	WORD	wLduSigId;		//Signal Id, defined in protocol
	int		iValueType;		//The type of the signal to be sent, including BYTE��INT��FLOAT��INVALID
	float	fValue;			//The value of the signal to be sent
};
typedef struct stSEND_CMD LDU_SEND_CMD;

struct	stLDU_COMM_INFO
{
	int		aiSampleAllTimes[LDU_MAX_ADDR_NUM];
};
typedef struct stLDU_COMM_INFO LDU_COMM_INFO;

struct	stLDU_SIG_CFG_INFO
{
	int		iEquipType;
	int		iEquipIdx;
	int		iRoughDataIdx;
	WORD	wSigCode;
	WORD	wValueType;

};
typedef struct stLDU_SIG_CFG_INFO LDU_SIG_CFG_INFO;

struct	tagLDU_PARAM_UNIFY
{
	int		iEquipId;
	int		iEquipIdDifference;
	int		iSigId;
	int		iRoughData;
	BYTE	bySetCmdId;
	BYTE	byValType;
	WORD	wSigCode;
};
typedef struct tagLDU_PARAM_UNIFY LDU_PARAM_UNIFY_DEF;

struct	tagLDU_CHN_ROUGH_IDX
{
	int		iChnNo;
	int		iRoughIdx;
};
typedef struct tagLDU_CHN_ROUGH_IDX LDU_CHN_ROUGH_IDX;


struct tagCORRENLATION_SEQ
{
	INT32	iStartSeqNum;
	INT32	iSeqCount;
};

typedef struct tagCORRENLATION_SEQ	 CORRENLATION_SEQ;

struct tagLISTADDR
{
	INT32				iElemDataAddr;
	INT32				iExistNum;
	struct tagLISTADDR	*pNext;
};
typedef struct tagLISTADDR	 LISTADDR;

void LDU_Init(void*);
void LDU_Exit(void*);
void LDU_Reconfig(void*);
void LDU_Sample(void*);
void LDU_StuffChn(void*, ENUMSIGNALPROC, LPVOID);
void LDU_ParamUnify(void*);
void LDU_SendCtlCmd(void*, int, float, char*);
void LduSetEquipNoExist(void);

#endif //__RS485_SAMPLER_LDU_H


/*============================================================================*
 *         Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : Unified Data Type Definition
 *
 *  FILENAME : rs485_smbat.h
 *  PURPOSE  : Define base data type.
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2009-04-14      V1.0           IlockTeng         Created.    
 *         
 *                                                     
 *-----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *          
 *      
 *-----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *      
 *    
 *============================================================================*/
#ifndef __RS485_SAMPLER_SMBAT_H
#define __RS485_SAMPLER_SMBAT_H

#include <stdio.h>
#include "basetypes.h"
#include "err_code.h"
#include "pubfunc.h"
#include "string.h"
#include "rs485_main.h"				

//#define  COMMUNICATION_ERROR        2
//#define  FUNCTION_GENERAL_ERROR		-1
#define	 NO_RESPONSE_BEXISTENCE     1
#define  RESPONSE_OK                0

#define SMBAT_EQUIP_MAX_ADDR		12
#define SMBAT_CH_START				1500
#define SMBAT_CH_END				3499
#define SMBAT_CH_END_FLAG			-1
#define bySOI						0x7E			//START OF INFORMATION
#define byEOI						0x0D			//END OF INFORMATION
#define	SMBATCID1					0xd2
#define	SMBATCID2_GET_ANOLOG		0x41
#define	SMBATCID2_GET_VERSION		0x4F
#define	SMBATCID2_GET_SWITCH		0x43
#define SMBATCID2_SENT_CTRL			0x45
#define MAX_NUM_SMBAT				52
#define SMBAT_ADDR_START			9
#define SMBAT_ADDR_END				60

#define SMBAT_4802_STRING_NUM			1
#define SMBAT_4806_STRING_NUM			2
#define SMBAT_4812_STRING_NUM			4
#define SMBAT_2402_STRING_NUM			2
#define SMBAT_2406_STRING_NUM			4
#define SMBAT_2412_STRING_NUM			4

#define SMBAT4802_ADDR_START		9
#define SMBAT4802_ADDR_END			20
#define SMBAT4806_ADDR_START		21
#define SMBAT4806_ADDR_END			30
#define SMBAT4812_ADDR_START		31
#define SMBAT4812_ADDR_END			40
#define SMBAT2402_ADDR_START		41
#define SMBAT2402_ADDR_END			50
#define SMBAT2406_ADDR_START		51
#define SMBAT2406_ADDR_END			55
#define SMBAT2412_ADDR_START		56
#define SMBAT2412_ADDR_END			60
#define SMBAT_TIME_OUT				200
#define MAX_CHN_NUM_PER_SMBAT		100
#define	STRING_INDEX_PERTINENT		15				
#define SMBAT_SHECKSUM_EOI			5
#define SMBAT_SAMP_INVALID_VALUE	(-9999)			//initialization data
#define SMBAT_SAMP_FREQUENCY  3						//control  sample the frequency

#define SMBAT_SAMPLE_OK				0
#define SMBAT_SAMPLE_FAIL			1

//communication status	
#define	SMBAT_COMM_OK				0
#define	SMBAT_COMM_FAIL				1

//existent status
#define SMBAT_EQUIP_EXISTENT		0
#define SMBAT_EQUIP_NOT_EXISTENT	1

#define	MAX_CHN_DISTANCE_SMBAT		40
#define MAX_RECVBUFFER_NUM_SMBAT	4096	
#define MAX_SENDBUFFER_NUM_SMBAT	50
#define SMBAT_STRING_NUM		20

#define RS485_BATT_GROUP_EQP_ID		115
#define RS485_LST_BATT_NUM_SID		422

enum YDN23PROTOCOLINFO_SMBAT
{
	CHECKSUM_ERROR_YDN23_SMBAT = 2,
	ADDR_YDN23_SMBAT = 3,

	CID1H_YDN23_SMBAT = 5,
	CID1L_YDN23_SMBAT = 6,
	CID2_YDN23_SMBAT = 7,
	LENGTH_YDN23_SMBAT = 9,
	DATA_YDN23_SMBAT = 13,
	DATAINFO_YDN23_SMBAT = 15,
	//others will add here
};


/*
   command 0x41	and 	0x43
*/
enum  ROUGH_DATA_IDX_SMBAT
{	
	//0x41
	SMBAT_BLOCK_1_VOLT,					//	0Block 1 Voltage		BAT1
	SMBAT_BLOCK_2_VOLT,					//	1Block 2 Voltage		BAT2
	SMBAT_BLOCK_3_VOLT,					//	2Block 3 Voltage		BAT3
	SMBAT_BLOCK_4_VOLT,					//	3Block 4 Voltage		BAT4
	SMBAT_BLOCK_5_VOLT,					//	4Block 5 Voltage		BAT5
	SMBAT_BLOCK_6_VOLT,					//	5Block 6 Voltage		BAT6
	SMBAT_BLOCK_7_VOLT,					//	6Block 7 Voltage		BAT7
	SMBAT_BLOCK_8_VOLT,					//	7Block 8 Voltage		BAT8
	SMBAT_BLOCK_9_VOLT,					//	8Block 9 Voltage		BAT9
	SMBAT_BLOCK_10_VOLT,					//	9Block 10 Voltage		BAT10
	SMBAT_BLOCK_11_VOLT,					//	10Block 11 Voltage		BAT11
	SMBAT_BLOCK_12_VOLT,					//	11Block 12 Voltage		BAT12			
	SMBAT_BLOCK_13_VOLT,					//	12Block 13 Voltage		BAT13
	SMBAT_BLOCK_14_VOLT,					//	13Block 14 Voltage		BAT14
	SMBAT_BLOCK_15_VOLT,					//	14Block 15 Voltage		BAT15
	SMBAT_BLOCK_16_VOLT,					//	15Block 16 Voltage		BAT16
	SMBAT_BLOCK_17_VOLT,					//	16Block 17 Voltage		BAT17
	SMBAT_BLOCK_18_VOLT,					//	17Block 18 Voltage		BAT18
	SMBAT_BLOCK_19_VOLT,					//	18Block 19 Voltage		BAT19
	SMBAT_BLOCK_20_VOLT,					//	19Block 20 Voltage		BAT20
	SMBAT_BLOCK_21_VOLT,					//	20Block 21 Voltage		BAT21
	SMBAT_BLOCK_22_VOLT,					//	21Block 22 Voltage		BAT22
	SMBAT_BLOCK_23_VOLT,					//	22Block 23 Voltage		BAT23
	SMBAT_BLOCK_24_VOLT,					//	23Block 24 Voltage		BAT24
	SMBAT_BLOCK_25_VOLT,					//	24Block 25 Voltage		BAT25
	SMBAT_VOLTAGE,						//	25						AI1
	SMBAT_AI2,						//	26						AI2
	SMBAT_CURRENT,						//	27						AI3	
	SMBAT_AI4,						//  28						AI4
	SMBAT_AMBIENT_TEMPERATURE,				//	29						AI5 
	SMBAT_ACID_TEMPERATURE,					//	30						AI6
	SMBAT_AI7,						//  31						AI7


	//0x43
	SMBAT_BATTERY_LEAKAGE,					//	32						DI1
	SMBAT_LOW_ACID_LEVEL,					//	33						DI2
	SMBAT_BATTERY_DISCONNECTED,				//	34						DI3
	SMBAT_DI4,						//	35						DI4
	SMBAT_DI5,						//	36						DI5
	SMBAT_DI6,						//	37						DI6
	SMBAT_DI7,						//	38						DI7
	SMBAT_DI8,						//	39						DI8
	SMBAT_RELAY_1_STAT,					//	40						Relay 1 status
	SMBAT_RELAY_2_STAT,					//	41						Relay 2 status
	
	
	SMBAT_COMMUNICATE_STAT,					//Communicate Status
	SMBAT_WORKING_STAT,					//Working Status
	SMBAT_ADDRESS,						//Address

	SMBAT_BATTERY_STRING_NUM,			

	SMBAT_GROUP_WORKING_STAT,
	//SMBAT_VER,
	SMBAT_INTERRUPT_TIMES,
	
	SMBAT_STRING_NUM_OF_TYPE,				// battery's string number of one SMBAT Equipment
	
	//here add
	SMBAT_MAX_SIGNALS_NUM,
};

enum SMBAT_SAMP_CHANNEL
{
	SMBAT_CH_BLOCK_1_VOLT = 1500,			//Digital Input 1 and Analogue Input 6
	SMBAT_CH_BLOCK_2_VOLT,					//	1Block 2 Voltage
	SMBAT_CH_BLOCK_3_VOLT,					//	2Block 3 Voltage
	SMBAT_CH_BLOCK_4_VOLT,					//	3Block 4 Voltage
	SMBAT_CH_BLOCK_5_VOLT,					//	4Block 5 Voltage
	SMBAT_CH_BLOCK_6_VOLT,					//	5Block 6 Voltage
	SMBAT_CH_BLOCK_7_VOLT,					//	6Block 7 Voltage	
	SMBAT_CH_BLOCK_8_VOLT,					//	7Block 8 Voltage
	SMBAT_CH_BLOCK_9_VOLT,					//	8Block 9 Voltage
	SMBAT_CH_BLOCK_10_VOLT,					//	9Block 10 Voltage
	SMBAT_CH_BLOCK_11_VOLT,					//	10Block 11 Voltage
	SMBAT_CH_BLOCK_12_VOLT,					//	11Block 12 Voltage				
	SMBAT_CH_BLOCK_13_VOLT,					//	12Block 13 Voltage
	SMBAT_CH_BLOCK_14_VOLT,					//	13Block 14 Voltage
	SMBAT_CH_BLOCK_15_VOLT,					//	14Block 15 Voltage
	SMBAT_CH_BLOCK_16_VOLT,					//	15Block 16 Voltage
	SMBAT_CH_BLOCK_17_VOLT,					//	16Block 17 Voltage
	SMBAT_CH_BLOCK_18_VOLT,					//	17Block 18 Voltage
	SMBAT_CH_BLOCK_19_VOLT,					//	18Block 19 Voltage
	SMBAT_CH_BLOCK_20_VOLT,					//	19Block 20 Voltage
	SMBAT_CH_BLOCK_21_VOLT,					//	20Block 21 Voltage
	SMBAT_CH_BLOCK_22_VOLT,					//	21Block 22 Voltage
	SMBAT_CH_BLOCK_23_VOLT,					//	22Block 23 Voltage
	SMBAT_CH_BLOCK_24_VOLT,					//	23Block 24 Voltage
	SMBAT_CH_BLOCK_25_VOLT,					//	24Block 25 Voltage
	SMBAT_CH_VOLTAGE,							//	25AI1
	SMBAT_CH_AI2,								//	26AI2
	SMBAT_CH_CURRENT,							//	27AI3	
	SMBAT_CH_AI4,								//  28AI4
	SMBAT_CH_AMBIENT_TEMPERATURE,				//	29AI5 
	SMBAT_CH_ACID_TEMPERATURE,					//	30AI6
	SMBAT_CH_AI7,								//  31AI7

	//0x43
	SMBAT_CH_BATTERY_LEAKAGE,					//	32DI1
	SMBAT_CH_LOW_ACID_LEVEL,					//	33DI2
	SMBAT_CH_BATTERY_DISCONNECTED,				//	34DI3
	SMBAT_CH_DI4,								//	35DI4
	SMBAT_CH_DI5,								//	36DI5
	SMBAT_CH_DI6,								//	37DI6
	SMBAT_CH_DI7,								//	38DI7
	SMBAT_CH_DI8,								//	39DI8
	SMBAT_CH_RELAY_1_STAT,						//	40 Relay 1 status
	SMBAT_CH_RELAY_2_STAT,						//	41 Relay 2 status
	
	SMBAT_CH_COMMUNICATE_STAT,					//Communicate Status
	SMBAT_CH_EXIST_STAT,						//Working Status
	SMBAT_CH_ADDRESS,							//Address
};

#define ONE_STRINGSIGS_OFFSET41		0 //OneStringSigs
#define ONE_STRINGSIGS_OFFSET43		SMBAT_BATTERY_LEAKAGE


union _SMBATSTRTOFLOAT
{
	float f_value;       // float
	BYTE  by_value[4];   // byte
};
typedef union _SMBATSTRTOFLOAT	SMBATSTRTOFLOAT;

struct _SMBAT_STRING_SIG_MAP
{
	int		Total_4802SigMap;
	int		SubSigMap;
};
typedef struct _SMBAT_STRING_SIG_MAP	SMBAT_STRING_SIG_MAP;


struct SMBATSamplerData
{
	BOOL			bNeedReconfig;
	RS485_VALUE		OneStringSigs[SMBAT_MAX_SIGNALS_NUM];
	RS485_VALUE		aRoughDataSmbat[SMBAT_STRING_NUM][SMBAT_MAX_SIGNALS_NUM];
	INT32			iRecordAddress[SMBAT_EQUIP_MAX_ADDR + 1];
};

typedef struct SMBATSamplerData	SMBAT_SAMP_DATA;	

struct _SMBATDATASTRUCT
{
    BYTE nType;     // ��������:0-������־,1-4BYTES(�޷��ţ�,2-4 bytes���з��ţ� 3-2Bytes
    int  nOffset;   // ������ʼ��ַ���ڱ����ݰ��е���ʼλ�ô�ͷ�ַ���ʼ��0��
    int  nScaleBit; // ���ڿ�����,��λ�ţ�ģ������������ϵ,��10���ȡ�
    int  nChanel;   // ϵͳ�����ͨ���ţ�����ģ��Ĳ���������ƫ�Ʊ�ʾ��    
};
typedef struct _SMBATDATASTRUCT  SMBATDATASTRUCT;


VOID SMBAT_SendCtlCmd(RS485_DEVICE_CLASS*  SmbatDeviceClass,INT32 iChannelNo, float fParam,char *pString);	
INT32 SMBAT_InitRoughValue(void* pDevice);
INT32 SMBATSample(void* pDevice);
INT32 SMBAT_StuffChannel(RS485_DEVICE_CLASS*  SmbatDeviceClass,ENUMSIGNALPROC EnumProc,LPVOID lpvoid);
void SMBAT_GetProdctInfo(HANDLE hComm, int nUnitNo, void *pPI);
INT32	SMBATNeedCommProc(void);
INT32 SMBATReopenPort(void);
#endif

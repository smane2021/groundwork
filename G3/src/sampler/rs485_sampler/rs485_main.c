/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : NGMC-HF(Next Generation Controller - High Feature)
*
*  FILENAME : can_sampler_main.c
*  CREATOR  : Frank Cao                DATE: 2008-07-25 10:38
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/


#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <sys/ioctl.h>

#include <stdarg.h>

#include "rs485_main.h"
#include "rs485_comm.h"
#include "rs485_report.h"
#include "rs485_slaves.h"
#include "rs485_smio.h"
#include "rs485_smbat.h"
#include "rs485_smac.h"
#include "rs485_smdu.h"
#include "rs485_smbrc.h"
#include "rs485_large_dus.h"
#include "rs485_modbus.h"

int gRtsHand = NULL;
INT32	GetNeedProcAddr(void);
extern BOOL			g_bExitNotification;
RS485_SAMPLER_DATA		g_RS485Data;

RS485_DEVICE_CLASS g_aDeviceClass[DEVICE_CLASS_NUM] = 
{
	{													
		"Slaves",						/*pszName		*/
			&(g_RS485Data.CommPort),		/*pCommPort		*/
			NULL,							/*pRoughData	*/
			(DEVICE_CLASS_INIT)SLAVE_InitRoughValue,			/*pfnInit		*/
			NULL,							/*pfnExit;		*/
			NULL,							/*pfnParamUnify;*/
			NULL,							/*pfnReconfig;	*/
			NULL,							/*pfnSample;	*/
			NULL,							/*pfnStuffChn;	*/
			NULL,							/*pfnSendCmd;	*/
			NULL,							/*pCommInfo;	*/
			NULL,							/*pFalshData;	*/
			TRUE,							/*bNeedReconfig */
			0,
	},

	{													
		"LargeDus",						/*pszName		*/
			&(g_RS485Data.CommPort),		/*pCommPort		*/
			NULL,							/*pRoughData	*/
			(DEVICE_CLASS_INIT)LDU_Init,						/*pfnInit		*/
			NULL,							/*pfnExit;		*/
			NULL,							/*pfnParamUnify;*/
			NULL,							/*pfnReconfig;	*/
			NULL,							/*pfnSample;	*/
			NULL,							/*pfnStuffChn;	*/
			NULL,							/*pfnSendCmd;	*/
			NULL,							/*pCommInfo;	*/
			NULL,							/*pFalshData;	*/
			TRUE,							/*bNeedReconfig */
			0,
	},	

	{													
		"SMModulesAc",					/*pszName		*/
			&(g_RS485Data.CommPort),		/*pCommPort		*/
			NULL,							/*pRoughData	*/
			(DEVICE_CLASS_INIT)SMAC_InitRoughValue,			/*pfnInit		*/
			NULL,							/*pfnExit;		*/
			NULL,							/*pfnParamUnify;*/
			NULL,							/*pfnReconfig;	*/
			NULL,							/*pfnSample;	*/
			NULL,							/*pfnStuffChn;	*/
			NULL,							/*pfnSendCmd;	*/
			NULL,							/*pCommInfo;	*/
			NULL,							/*pFalshData;	*/
			TRUE,							/*bNeedReconfig */
			0,
	},

	{													
		"SMModulesIo",					/*pszName		*/
			&(g_RS485Data.CommPort),		/*pCommPort		*/
			NULL,							/*pRoughData	*/
			(DEVICE_CLASS_INIT)SMIO_InitRoughValue,			/*pfnInit		*/
			NULL,							/*pfnExit;		*/
			NULL,							/*pfnParamUnify;*/
			NULL,							/*pfnReconfig;	*/
			NULL,							/*pfnSample;	*/
			NULL,							/*pfnStuffChn;	*/
			NULL,							/*pfnSendCmd;	*/
			NULL,							/*pCommInfo;	*/
			NULL,							/*pFalshData;	*/
			TRUE,							/*bNeedReconfig */
			0,
	},

	{													
		"SMModulesBat",					/*pszName		*/
			&(g_RS485Data.CommPort),		/*pCommPort		*/
			NULL,							/*pRoughData	*/
			(DEVICE_CLASS_INIT)SMBAT_InitRoughValue,			/*pfnInit		*/
			NULL,							/*pfnExit;		*/
			NULL,							/*pfnParamUnify;*/
			NULL,							/*pfnReconfig;	*/
			NULL,							/*pfnSample;	*/
			NULL,							/*pfnStuffChn;	*/
			NULL,							/*pfnSendCmd;	*/
			NULL,							/*pCommInfo;	*/
			NULL,							/*pFalshData;	*/
			TRUE,							/*bNeedReconfig */
			0,
	},

	{													
		"SMModuleDu",					/*pszName		*/
			&(g_RS485Data.CommPort),		/*pCommPort		*/
			NULL,							/*pRoughData	*/
			(DEVICE_CLASS_INIT)SMDU_InitRoughValue,			/*pfnInit		*/
			NULL,							/*pfnExit;		*/
			NULL,							/*pfnParamUnify;*/
			NULL,							/*pfnReconfig;	*/
			NULL,							/*pfnSample;	*/
			NULL,							/*pfnStuffChn;	*/
			NULL,							/*pfnSendCmd;	*/
			NULL,							/*pCommInfo;	*/
			NULL,							/*pFalshData;	*/
			TRUE,							/*bNeedReconfig */
			0,
	},

	{													
		"SMBRC",					/*pszName		*/
			&(g_RS485Data.CommPort),		/*pCommPort		*/
			NULL,							/*pRoughData	*/
			(DEVICE_CLASS_INIT)SMBRC_InitRoughValue,			/*pfnInit		*/
			NULL,							/*pfnExit;		*/
			NULL,							/*pfnParamUnify;*/
			NULL,							/*pfnReconfig;	*/
			NULL,							/*pfnSample;	*/
			NULL,							/*pfnStuffChn;	*/
			NULL,							/*pfnSendCmd;	*/
			NULL,							/*pCommInfo;	*/
			NULL,							/*pFalshData;	*/
			TRUE,							/*bNeedReconfig */
			0,
	},

	{													
		    "MODBUS_Equip",					/*pszName		*/
			&(g_RS485Data.CommPort),		/*pCommPort		*/
			NULL,							/*pRoughData	*/
			MDB_Init,						/*pfnInit		*/
			NULL,							/*pfnExit;		*/
			NULL,							/*pfnParamUnify;*/
			NULL,							/*pfnReconfig;	*/
			NULL,							/*pfnSample;	*/
			NULL,							/*pfnStuffChn;	*/
			NULL,							/*pfnSendCmd;	*/
			NULL,							/*pCommInfo;	*/
			NULL,							/*pFalshData;	*/
			TRUE,							/*bNeedReconfig */
			0,
	},
};
/*==========================================================================*
* FUNCTION : RS485_StrExtract
* PURPOSE  : Extract a sub string from strSource
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN char*      strSource      : 
*            IN OUT char*  strDestination : 
*            IN char       cSeparator     : 
* RETURN   : int : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-06-02 10:45
*==========================================================================*/
int RS485_StrExtract(IN char* strSource,
					 char* strDestination, 
					 char cSeparator)
{
	int i = 0;

	if((!strSource) || (!strDestination))
	{
		return 0;
	}

	while(strSource[i] && (strSource[i] != cSeparator))
	{
		strDestination[i] = strSource[i];
		i++;
	}

	strDestination[i] = 0;

	if(strSource[i] && i)
	{
		return i + 1;
	}

	return 0;
}
/*==========================================================================*
* FUNCTION : RS485_FloatToString
* PURPOSE  : Transform a float to string
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: float  fInput    : 
*            BYTE*  strOutput : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-06-25 17:12
*==========================================================================*/
void RS485_FloatToString(float fInput, BYTE* strOutput)
{
	int						i;	
	RS485_FLOAT_STRING		unValue;

	unValue.fValue = fInput;

	for(i = 0; i < 4; i++)
	{		
		//strOutput[4 - i - 1] = unValue.abyValue[i];
		strOutput[i] = unValue.abyValue[i];//check check
	}

	return;
}

/*==========================================================================*
* FUNCTION : SmSampCtrlInit()
* PURPOSE  : INIT struct stSMSmplingCtrl
*            
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
*     
* RETURN   : DLLExport BOOL : 
* COMMENTS : 
* CREATOR  : IlockTeng               DATE: 2008-10-14 10:03
*==========================================================================*/
LOCAL INT32	SmSampCtrlInit(void)
{
	unsigned int  i = 0;
	g_RS485Data.stSMSmplingCtrl.bStartCtlFlag		= FALSE;
	g_RS485Data.stSMSmplingCtrl.cExistMaxAddr		= 0;
	g_RS485Data.stSMSmplingCtrl.cCrrentAddrIndex	= 0;
	g_RS485Data.stSMSmplingCtrl.cCrrentCtlIndex		= 0;
	g_RS485Data.stSMSmplingCtrl.cUrgencyIndex		= 0;

	for (i = 0; i < (unsigned int)strlen((char*)(g_RS485Data.stSMSmplingCtrl.cSAMPExistAddr)); i++)
	{
		g_RS485Data.stSMSmplingCtrl.cSAMPExistAddr[i] = 0xFF;
	}

	memset(g_RS485Data.stSMSmplingCtrl.cJustCtrl, 0, sizeof(g_RS485Data.stSMSmplingCtrl.cJustCtrl));

	for (i = 0; i < (unsigned int)strlen((char*)(g_RS485Data.stSMSmplingCtrl.cNeedSmpAddr)); i++)
	{
		g_RS485Data.stSMSmplingCtrl.cNeedSmpAddr[i] = 0xFF;
	}

	return TRUE;
}

/*==========================================================================*
* FUNCTION : SmSamp GetCtrlAddr
* PURPOSE  : 
*            
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
*     
* RETURN   : DLLExport BOOL : 
* COMMENTS : 
* CREATOR  : IlockTeng               DATE: 2008-10-14 10:03
*==========================================================================*/
INT32	SmGetCtrlAddrbyChann(INT32  iChannelNo)
{
	//DIESEL  ac
	if (iChannelNo >= 300 && iChannelNo <= 449)
	{
		return 104;
	}

	//SMIO
	if (iChannelNo >= 4000 && iChannelNo <= 4319)
	{
		return (96 + (iChannelNo - 4000)/40);
	}

	//SMBAT
	//if (iChannelNo > )
	//{
	//}

	//SMDU
	if (iChannelNo >= SMDU_CHNNL_CTRL_START && iChannelNo < 7999)
	{
		return (1 + (iChannelNo - SMDU_CHNNL_CTRL_START)/80);
	}

	return -1;
}

/*==========================================================================*
* FUNCTION : Check SmAddr
* PURPOSE  : 
*            
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
*     
* RETURN   : DLLExport BOOL : 
* COMMENTS : 
* CREATOR  : IlockTeng               DATE: 2008-10-14 10:03
*==========================================================================*/
INT32	CheckSmAddr(INT32  iSmAddr)
{
	unsigned int i= 0;

	for (i = 0;i < strlen((char*)(g_RS485Data.stSMSmplingCtrl.cJustCtrl)); i++)
	{
		if(iSmAddr == g_RS485Data.stSMSmplingCtrl.cJustCtrl[i])
		{
			return FALSE;
		}
	}

	return TRUE;
}
/*==========================================================================*
* FUNCTION : GetNeed ProcAddr()
* PURPOSE  : GET 5 address to g_RS485Data.stSMSmplingCtrl.cNeedSmpAddr
*			 GET 5 address to g_RS485Data.stSMSmplingCtrl.cUrgencySampAddr
*			 OUT: g_RS485Data.stSMSmplingCtrl.cNeedSmpAddr
*				  g_RS485Data.stSMSmplingCtrl.cUrgencySampAddr
*			 INT: g_RS485Data.stSMSmplingCtrl.cCrrentAddrIndex
*				  g_RS485Data.stSMSmplingCtrl.cJustCtrl
*            
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
*     
* RETURN   : DLLExport BOOL : 
* COMMENTS : 
* CREATOR  : IlockTeng               DATE: 2008-10-14 10:03
*==========================================================================*/
INT32	GetNeedProcAddr(void)
{
	BYTE	cCtrlAddrIndex;
	BYTE	cCrrentAddrIndex;
	BYTE	cUrgencyAddrIndex;
	BYTE	cExistMaxAddr;
	BYTE*	pcSAMPAddr;
	BYTE*	pcJustCtrlAddr;
	cCrrentAddrIndex	= g_RS485Data.stSMSmplingCtrl.cCrrentAddrIndex;
	cUrgencyAddrIndex	= g_RS485Data.stSMSmplingCtrl.cUrgencyIndex;

	cExistMaxAddr		= g_RS485Data.stSMSmplingCtrl.cExistMaxAddr;
	pcSAMPAddr			= g_RS485Data.stSMSmplingCtrl.cSAMPExistAddr;
	pcJustCtrlAddr		= g_RS485Data.stSMSmplingCtrl.cJustCtrl;

	if (g_RS485Data.stSMSmplingCtrl.bStartCtlFlag == FALSE
		|| g_RS485Data.stSMSmplingCtrl.cExistMaxAddr <= RS485_SM_SAMP_MAX_ADDR)
	{
		return 0;
	}

	//1 BY first Just Control Address
	for (cCtrlAddrIndex = 0; cCtrlAddrIndex < cExistMaxAddr; cCtrlAddrIndex++)
	{
		if (*(pcJustCtrlAddr + cCtrlAddrIndex) == 0)
		{
			if (cCtrlAddrIndex >= RS485_SM_SAMP_MAX_ADDR)
			{
				memcpy(g_RS485Data.stSMSmplingCtrl.cUrgencySampAddr,
					pcJustCtrlAddr + (cCtrlAddrIndex - RS485_SM_SAMP_MAX_ADDR),
					RS485_SM_SAMP_MAX_ADDR);
				//IN control function go up!! There is cut dowm !!
				memset(pcJustCtrlAddr + (cCtrlAddrIndex - RS485_SM_SAMP_MAX_ADDR),
					0,
					RS485_SM_SAMP_MAX_ADDR);
			}
			else
			{
				
				memcpy(g_RS485Data.stSMSmplingCtrl.cUrgencySampAddr,
					pcJustCtrlAddr,
					cCtrlAddrIndex);

				//IN control function go up!! There is cut dowm !!
				memset(pcJustCtrlAddr,
					0,
					cExistMaxAddr);
				
			}

			break;
		}
	}

	if (cCtrlAddrIndex == cExistMaxAddr)
	{
		if (cCtrlAddrIndex >= RS485_SM_SAMP_MAX_ADDR)
		{
			memcpy(g_RS485Data.stSMSmplingCtrl.cUrgencySampAddr,
				pcJustCtrlAddr + (cCtrlAddrIndex - RS485_SM_SAMP_MAX_ADDR),
				RS485_SM_SAMP_MAX_ADDR);

			memset(pcJustCtrlAddr + (cCtrlAddrIndex - RS485_SM_SAMP_MAX_ADDR),
				0,
				RS485_SM_SAMP_MAX_ADDR);
		}	
	}

	//2 BY Normal Sampling
	if (cCrrentAddrIndex + RS485_SM_SAMP_MAX_ADDR - cExistMaxAddr >= 0)
	{
		memcpy(g_RS485Data.stSMSmplingCtrl.cNeedSmpAddr,
			pcSAMPAddr + cCrrentAddrIndex,
			(unsigned int)(cExistMaxAddr - cCrrentAddrIndex));

		memcpy(g_RS485Data.stSMSmplingCtrl.cNeedSmpAddr + (cExistMaxAddr - cCrrentAddrIndex),
			pcSAMPAddr,
			(unsigned int)(RS485_SM_SAMP_MAX_ADDR - (cExistMaxAddr - cCrrentAddrIndex)));

		g_RS485Data.stSMSmplingCtrl.cCrrentAddrIndex 
			= RS485_SM_SAMP_MAX_ADDR - (cExistMaxAddr - cCrrentAddrIndex);
	}
	else
	{
		memcpy(g_RS485Data.stSMSmplingCtrl.cNeedSmpAddr,
			pcSAMPAddr + cCrrentAddrIndex,
			RS485_SM_SAMP_MAX_ADDR);

		g_RS485Data.stSMSmplingCtrl.cCrrentAddrIndex += 5;
	}

	return 0;
}


/*==========================================================================*
* FUNCTION : SmSamp CtrlProcess
* PURPOSE  : Check the iAddr be need Sampling.
*            IN:g_RS485Data.stSMSmplingCtrl.cUrgencySampAddr
*				g_RS485Data.stSMSmplingCtrl.cNeedSmpAddr
*				INT32 iAddr
*			 OUT:TRUE: be need process,  FALSE:needn't process!
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
*     
* RETURN   : DLLExport BOOL : 
* COMMENTS : 
* CREATOR  : IlockTeng               DATE: 2008-10-14 10:03
*==========================================================================*/
INT32	SampCheckSmAddrProcess(INT32 iAddr)
{
	//BYTE					cExistMaxAddr;
	//BYTE					cCrrentAddrIndex;
	BYTE*					pcSAMPAddr;
	BYTE*					pcUrgencySampAddr;
	INT32					iIndex;
	//static	INT32			si_RecordSMPTimes =0;

	if (g_RS485Data.stSMSmplingCtrl.bStartCtlFlag == FALSE
		|| g_RS485Data.stSMSmplingCtrl.cExistMaxAddr <= RS485_SM_SAMP_MAX_ADDR )
	{
		return TRUE;
	}

	pcSAMPAddr			= g_RS485Data.stSMSmplingCtrl.cNeedSmpAddr;
	pcUrgencySampAddr		= g_RS485Data.stSMSmplingCtrl.cUrgencySampAddr;

	for (iIndex = 0; iIndex < RS485_SM_SAMP_MAX_ADDR; iIndex++)
	{
		if (*(pcUrgencySampAddr + iIndex) == iAddr)
		{
			*(pcUrgencySampAddr + iIndex) = 0;
			return TRUE;
		}
	}

	if ((*(pcUrgencySampAddr + 4) != 0))
	{
		TRACE("\nThis Ctrl CMD too many:: So immediately exist Query !!\n");
	}

	if ((*(pcSAMPAddr) == iAddr)
		||(*(pcSAMPAddr + 1) == iAddr)
		||(*(pcSAMPAddr + 2) == iAddr)
		||(*(pcSAMPAddr + 3) == iAddr)
		||(*(pcSAMPAddr + 4) == iAddr))
	{
		TRACE("SM ADDR = %d \n",(int)iAddr);

		return TRUE;
	}
	else
	{
		return FALSE;
	}			
}

/*==========================================================================*
* FUNCTION : SmSet CtrlAddr
* PURPOSE  : 
*            
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
*     
* RETURN   : DLLExport BOOL : 
* COMMENTS : 
* CREATOR  : IlockTeng               DATE: 2008-10-14 10:03
*==========================================================================*/
INT32	SmSetCtrlAddr(INT32  iChannelNo)
{
	INT32	 iSmAddr;
	INT32	 iTempIndex;
	BYTE*	 pcJustCtrl;
	//INT32	 iCrrentCtlIndex;

	iSmAddr = SmGetCtrlAddrbyChann(iChannelNo);

	if (iSmAddr == -1)
	{
		return 0;
	}
	else
	{
		if (CheckSmAddr(iSmAddr))
		{
			pcJustCtrl = g_RS485Data.stSMSmplingCtrl.cJustCtrl;
			//iCrrentCtlIndex = g_RS485Data.stSMSmplingCtrl.cCrrentCtlIndex;

			for (iTempIndex = 0;
				iTempIndex < g_RS485Data.stSMSmplingCtrl.cExistMaxAddr;
				iTempIndex++)
			{
				if (*(pcJustCtrl + iTempIndex) == 0)
				{
					*(pcJustCtrl + iTempIndex) = iSmAddr;
					break;
				}
			}
		}
	}
	return 0;
}



/*==========================================================================*
* FUNCTION : CommunicationInfoInit()
* PURPOSE  : INIT struct stEquipCommInfo
*            
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: bNeedStuffFlag :TRUE Means Need stuff RoughData
*            iSampFrequenceCounter: Record  Frequence
*          
*            
* RETURN   : DLLExport BOOL : 
* COMMENTS : 
* CREATOR  : IlockTeng               DATE: 2008-10-14 10:03
*==========================================================================*/
LOCAL INT32	CommunicationInfoInit(void)
{
	INT32 i;

	for (i = 0; i < DEVICE_CLASS_NUM; i++)
	{
		g_RS485Data.stEquipCommInfo[i].bNeedStuffFlag = TRUE;
		g_RS485Data.stEquipCommInfo[i].iSampFrequenceCounter = 10;//first run must smpling for Scan
	}

	return 0;

}
/*==========================================================================*
* FUNCTION : bNeedSampling(INT32 DeviceClassNo)
* PURPOSE  : Check whether need Sampling for ctrl Frequence
*            
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: DeviceClassNo : This Flag to SM equipment type
*            
*          
*            
* RETURN   : DLLExport BOOL : TRUE need sampling  FALSE needn't sampling
* COMMENTS : 
* CREATOR  : IlockTeng               DATE: 2008-10-14 10:03
*==========================================================================*/
//LOCAL INT32  bNeedSampling(INT32 DeviceClassNo)
//{
//	extern  SLAVE_SAMPLER_DATA		g_SlaveSampData;
//	//There isn't SLAVE Equip,Needn't ctrl Frequency return ok
//	if (g_SlaveSampData.aRoughDataGroup[SLAVE_RS485_GROUP_WOKING_STATE].iValue 
//		== RS485_EQUIP_NOT_EXISTENT)
//	{
//		g_RS485Data.stEquipCommInfo[DEVICE_CLASS_SMAC].bNeedStuffFlag = TRUE;
//		g_RS485Data.stEquipCommInfo[DEVICE_CLASS_LARGEDU].bNeedStuffFlag = TRUE;
//		g_RS485Data.stEquipCommInfo[DEVICE_CLASS_SMIO].bNeedStuffFlag = TRUE;
//		g_RS485Data.stEquipCommInfo[DEVICE_CLASS_SMBAT].bNeedStuffFlag = TRUE;
//		return TRUE;
//	}
//
//	switch (DeviceClassNo)
//	{
//	case DEVICE_CLASS_LARGEDU:
//
//		if (g_RS485Data.stEquipCommInfo[DEVICE_CLASS_LARGEDU].iSampFrequenceCounter 
//			>= RS485_SMLDU_SAMP_FREQUENCE)//3
//		{
//			g_RS485Data.stEquipCommInfo[DEVICE_CLASS_LARGEDU].iSampFrequenceCounter
//				= 0;
//			g_RS485Data.stEquipCommInfo[DEVICE_CLASS_LARGEDU].bNeedStuffFlag = TRUE;
//			return TRUE;
//		}
//		else
//		{
//			g_RS485Data.stEquipCommInfo[DEVICE_CLASS_LARGEDU].iSampFrequenceCounter++;
//			g_RS485Data.stEquipCommInfo[DEVICE_CLASS_LARGEDU].bNeedStuffFlag = FALSE;
//			return FALSE;
//		}
//
//		break;
//
//	case DEVICE_CLASS_SMAC:
//
//		if (g_RS485Data.stEquipCommInfo[DEVICE_CLASS_SMAC].iSampFrequenceCounter 
//			>= RS485_SMAC_SAMP_FREQUENCE)//5
//		{
//			g_RS485Data.stEquipCommInfo[DEVICE_CLASS_SMAC].iSampFrequenceCounter
//				= 0;
//			g_RS485Data.stEquipCommInfo[DEVICE_CLASS_SMAC].bNeedStuffFlag = TRUE;
//			return TRUE;
//		}
//		else
//		{
//			g_RS485Data.stEquipCommInfo[DEVICE_CLASS_SMAC].iSampFrequenceCounter++;
//			g_RS485Data.stEquipCommInfo[DEVICE_CLASS_SMAC].bNeedStuffFlag = FALSE;
//			return FALSE;
//		}
//
//		break;
//
//	case DEVICE_CLASS_SMIO:
//
//		if (g_RS485Data.stEquipCommInfo[DEVICE_CLASS_SMIO].iSampFrequenceCounter 
//			>= RS485_SMIO_SAMP_FREQUENCE)//3
//		{
//			g_RS485Data.stEquipCommInfo[DEVICE_CLASS_SMIO].iSampFrequenceCounter
//				= 0;
//			g_RS485Data.stEquipCommInfo[DEVICE_CLASS_SMIO].bNeedStuffFlag = TRUE;
//			return TRUE;
//		}
//		else
//		{
//			g_RS485Data.stEquipCommInfo[DEVICE_CLASS_SMIO].iSampFrequenceCounter++;
//			g_RS485Data.stEquipCommInfo[DEVICE_CLASS_SMIO].bNeedStuffFlag = FALSE;
//			return FALSE;
//		}
//
//		break;
//
//	case DEVICE_CLASS_SMBAT:
//
//		if (g_RS485Data.stEquipCommInfo[DEVICE_CLASS_SMBAT].iSampFrequenceCounter 
//			>= RS485_SMBAT_SAMP_FREQUENCE)//5
//		{
//			g_RS485Data.stEquipCommInfo[DEVICE_CLASS_SMBAT].iSampFrequenceCounter
//				= 0;
//			g_RS485Data.stEquipCommInfo[DEVICE_CLASS_SMBAT].bNeedStuffFlag = TRUE;
//			return TRUE;
//		}
//		else
//		{
//			g_RS485Data.stEquipCommInfo[DEVICE_CLASS_SMBAT].iSampFrequenceCounter++;
//			g_RS485Data.stEquipCommInfo[DEVICE_CLASS_SMBAT].bNeedStuffFlag = FALSE;
//			return FALSE;
//		}
//
//		break;
//
//	default:
//#if TST_RS485_DEBUG
//		TRACE("\n This  is DeviceClassEuipment Type  Error\n");
//#endif
//		break;
//	}
//
//	return TRUE;
//
//}
/*==========================================================================*
* FUNCTION : Rs485QueryInit
* PURPOSE  : 1. Initialize rough data and some parameter. 
*            2. Read connecting information from flash. 
*            3. Read position information from flash and initialize position
*                information according to them.
*            4. Read AC phase information from flash and initialize AC phase
*               information for every rectifier according to them.
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   : static : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-06-30 15:47
*==========================================================================*/
static void Rs485QueryInit(void)
{
	
	int			iErrCode;
	g_RS485Data.pDeviceClass			= g_aDeviceClass;
	g_RS485Data.bNeedReInitModeModify	= FALSE;
	g_RS485Data.enumRunningMode			= GetEnumSigValue(RS485_GET_MODE_EQUIPID,
		SIG_TYPE_SETTING,
		RS485_RUNNING_MODE_SIGID,
		"485_SAMP");
	//Ctrl Frequence
	CommunicationInfoInit();

	//Open port
	g_RS485Data.CommPort.hCommPort 
		= RS485_Open("19200, n, 8, 1", RS485_READ_WRITE_TIMEOUT, &iErrCode);

	if(iErrCode == ERR_COMM_OK)
	{
		g_RS485Data.CommPort.bOpened = TRUE;
		g_RS485Data.CommPort.enumAttr = RS485_ATTR_NONE;
		g_RS485Data.CommPort.enumBaud = 19200;
	}
	else
	{
		g_RS485Data.CommPort.bOpened = FALSE;	
		AppLogOut("RS485_Open", APP_LOG_UNUSED,
			"[%s]-[%d] ERROR: failed to  open rs485Comm \n\r", 
			__FILE__, __LINE__);
	}

	switch(g_RS485Data.enumRunningMode)
	{
	case RS485_RUN_MODE_STANDALONE:
		{	
			//there isn't slaves in the STANDALONE mode

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SLAVE].pfnInit)
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SLAVE].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_SLAVE));
			}

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMAC].pfnInit)
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SMAC].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMAC));
			}

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBAT].pfnInit)
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBAT].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMBAT));
			}

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMIO].pfnInit)
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SMIO].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMIO));
			}

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMDU].pfnInit)
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SMDU].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMDU));
			}

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_LARGEDU].pfnInit)
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_LARGEDU].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_LARGEDU));
			}

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBRC].pfnInit)
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBRC].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMBRC));
			}

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_MDB].pfnInit)
			{
			    g_RS485Data.pDeviceClass[DEVICE_CLASS_MDB].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_MDB));
			}
		}
		break;

	case RS485_RUN_MODE_MASTER:
		{
			//there isn't smdu   in the MASTER mode

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SLAVE].pfnInit)
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SLAVE].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_SLAVE));
			}

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMAC].pfnInit)
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SMAC].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMAC));
			}

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBAT].pfnInit)
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBAT].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMBAT));
			}

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMIO].pfnInit)
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SMIO].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMIO));
			}

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMDU].pfnInit)
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SMDU].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMDU));
			}

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_LARGEDU].pfnInit)
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_LARGEDU].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_LARGEDU));
			}

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBRC].pfnInit)
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBRC].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMBRC));
			}

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_MDB].pfnInit)
			{
			    g_RS485Data.pDeviceClass[DEVICE_CLASS_MDB].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_MDB));
			}
		}
		break;

	case RS485_RUN_MODE_SLAVE:
		{
		}
		break;

	default:
		{
			TRACE("\n  ******  Please check the running mode  ******   \n");
		}
		break;
	}


	//Create the Slave thread
	g_RS485Data.hSlaveThread 
		= RunThread_Create("SlaveThread",
							(RUN_THREAD_START_PROC)RS485_RunAsSlave,//RS485_RunAsSlave,
							NULL,
							NULL,
							0);	

	if (!g_RS485Data.hSlaveThread)
	{
		AppLogOut("RS485_SAM", 
			APP_LOG_ERROR,
			"Failed to create the Slave thread\n");

		ASSERT(TRUE);
	}

	return;

}
/*==========================================================================*
* FUNCTION : Rs485ReInit
* PURPOSE  : 1. Initialize rough data and some parameter. 
*   
*  
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   : static : 
* COMMENTS : 
* CREATOR  : Ilock Teng               DATE: 2008-06-30 15:47
*==========================================================================*/
static void Rs485ReInit(SIG_ENUM enumRunningMode)
{
	//INT32						i;
	g_RS485Data.pDeviceClass = g_aDeviceClass;
	UNUSED(enumRunningMode);
	CommunicationInfoInit();

	if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SLAVE].pfnInit)
	{
		g_RS485Data.pDeviceClass[DEVICE_CLASS_SLAVE].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_SLAVE));
	}

	if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMAC].pfnInit)
	{
		g_RS485Data.pDeviceClass[DEVICE_CLASS_SMAC].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMAC));
	}

	if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBAT].pfnInit)
	{
		g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBAT].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMBAT));
	}

	if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMIO].pfnInit)
	{
		g_RS485Data.pDeviceClass[DEVICE_CLASS_SMIO].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMIO));
	}

	if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMDU].pfnInit)
	{
		g_RS485Data.pDeviceClass[DEVICE_CLASS_SMDU].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMDU));
	}

	if(g_RS485Data.pDeviceClass[DEVICE_CLASS_LARGEDU].pfnInit)
	{
		g_RS485Data.pDeviceClass[DEVICE_CLASS_LARGEDU].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_LARGEDU));
	}

	if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBRC].pfnInit)
	{
		g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBRC].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMBRC));
	}

	if(g_RS485Data.pDeviceClass[DEVICE_CLASS_MDB].pfnInit)
	{
	    g_RS485Data.pDeviceClass[DEVICE_CLASS_MDB].pfnInit((void*)(g_RS485Data.pDeviceClass + DEVICE_CLASS_MDB));
	}
}

/*==========================================================================*
* FUNCTION : Rs485QueryExit
* PURPOSE  : Exit handling
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-06-24 09:54
*==========================================================================*/
static void Rs485QueryExit(void)
{
	int			i;

	for(i = 0; i < DEVICE_CLASS_NUM; i++)
	{
		if(g_RS485Data.pDeviceClass[i].pfnExit)
		{
			g_RS485Data.pDeviceClass[i].pfnExit(g_RS485Data.pDeviceClass + i);
		}
	}

	//Close port
	if(g_RS485Data.CommPort.hCommPort && g_RS485Data.CommPort.bOpened)
	{
		RS485_Close(g_RS485Data.CommPort.hCommPort);
		g_RS485Data.CommPort.hCommPort = NULL;
		g_RS485Data.CommPort.bOpened = FALSE;
		g_RS485Data.CommPort.enumBaud  = 0;
	}

	return;
}
/*==========================================================================*
* FUNCTION : Rs485ParamUnify
* PURPOSE  : Ensure the patameters equal to data read from LargeDu
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 17:04
*==========================================================================*/
static void	Rs485ParamUnify(void)
{
	int			i;
	HANDLE		hself;

	//if(g_RS485Data.bNeedReInitModeModify)
	//{
	//	return; �ŵ�����ͬ��������ȥ������ΪSMAC����Ҫͬ����SMDU�Ĳ���ͬ��
	//}

	hself = RunThread_GetId(NULL);
	RunThread_Heartbeat(hself);

	if(RS485_RUN_MODE_SLAVE == g_RS485Data.enumRunningMode)
	{
		return;
	}

	for(i = 0; i < DEVICE_CLASS_NUM; i++)
	{
		if((DEVICE_CLASS_SLAVE == i) 
			|| (DEVICE_CLASS_SMAC == i)
			|| (DEVICE_CLASS_SMIO == i)
			|| (DEVICE_CLASS_SMBAT == i))
		{
			continue;
		}

		if(g_RS485Data.pDeviceClass[i].pfnParamUnify)
		{
			g_RS485Data.pDeviceClass[i].pfnParamUnify(g_RS485Data.pDeviceClass + i);
		}
	}

	return;
}
/*==========================================================================*
* FUNCTION : GetSlavePositionSig
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL :TRUE means Receive Reconfig Cmd from web 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-10-10 16:55
*==========================================================================*/
static BOOL GetSlavePositionSig()
{
	if(GetDwordSigValue(SYS_GROUP_EQUIPID, 
		SIG_TYPE_SETTING,
		RS485_SYS_RECONFIG_SIG_ID,
		"485_SAMP") > 0)
	{
		SetDwordSigValue(SYS_GROUP_EQUIPID, 
			SIG_TYPE_SETTING,
			RS485_SYS_RECONFIG_SIG_ID,
			0,
			"485_SAMP");

		return TRUE;
	}

	return FALSE;
}
/*==========================================================================*
* FUNCTION : GetModeAndRecvReConfigCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL :TRUE means Receive Reconfig Cmd from web 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-10-10 16:55
*==========================================================================*/
static BOOL GetModeAndRecvReConfigCmd(INT32 *s_iCounter)
{
	/*if(GetDwordSigValue(SYS_GROUP_EQUIPID, 
		SIG_TYPE_SETTING,
		RS485_SYS_RECONFIG_SIG_ID,
		"485_SAMP") > 0)
	{
		SetDwordSigValue(SYS_GROUP_EQUIPID, 
			SIG_TYPE_SETTING,
			RS485_SYS_RECONFIG_SIG_ID,
			0,
			"485_SAMP");
		*s_iCounter = 0;
		Sleep(10);
		g_RS485Data.enumRunningMode = GetEnumSigValue(RS485_GET_MODE_EQUIPID,
			SIG_TYPE_SETTING,
			RS485_RUNNING_MODE_SIGID,
			"485_SAMP");
		return TRUE;
	}*/

	return FALSE;
}

/*==========================================================================*
* FUNCTION : bBattTesting()
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : TRUE: Battery testing      FALSE:Battery not testing
* COMMENTS : 
* CREATOR  :                 DATE: 2008-10-10 16:55
*==========================================================================*/
INT32 bBattTesting(INT32 iInSmSamp)
{
	static int s_iCheckSmCount = 0;
	extern   SMDU_RS485_SAMPLER_DATA g_SmduSamplerData;
	extern	 SLAVE_SAMPLER_DATA		 g_SlaveSampData;

	//Battery testing ,don't Sampling SM
	if (GetEnumSigValue(1,
		0,
		60,
		"485_SAMPBATTTST"))
	{
		if(!iInSmSamp)
		{
			//�˺�������SM�Ĳɼ��ｫ++�����򲻽���++
			s_iCheckSmCount++;
		}

		if (s_iCheckSmCount <= 10)
		{
			TRACE("\n !!!!!!@@@@@@  Batt  Testing     !!!!@@\n");

			if (RS485_RUN_MODE_STANDALONE == g_RS485Data.enumRunningMode)
			{
				if (g_SmduSamplerData.aRoughDataSmdu[0][SMDU_GROUP_SM_ACTUAL_NUM].iValue > 0)
				{
					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}

			if(RS485_RUN_MODE_MASTER == g_RS485Data.enumRunningMode)
			{
				if (g_SlaveSampData.aRoughDataGroup[SLAVE_RS485_GROUP_WOKING_STATE].iValue 
					== SLAVE_EQUIP_EXISTENT)
				{
					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
		}
	}

	//Time out need Sampling SM
	if (s_iCheckSmCount > 11)
	{
		SetEnumSigValue(1,
			0,
			60,
			0,
			"485_SAMPBATTTST");

		s_iCheckSmCount = 0;
		return FALSE;
	}

	return FALSE;
}

#define	RS485_ST_NORMAL			0
#define	RS485_ST_INTERRUPT		1
/*==========================================================================*
* FUNCTION : Rs485Sample
* PURPOSE  : Sample once
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-25 10:39
*==========================================================================*/
static void	Rs485Sample(void)
{

	RS485_DEVICE_CLASS*		pSlaveDeviceClass	= g_RS485Data.pDeviceClass + DEVICE_CLASS_SLAVE;
	RS485_DEVICE_CLASS*		pSmacDeviceClass	= g_RS485Data.pDeviceClass + DEVICE_CLASS_SMAC;
	RS485_DEVICE_CLASS*		pSmbatDeviceClass	= g_RS485Data.pDeviceClass + DEVICE_CLASS_SMBAT;
	RS485_DEVICE_CLASS*		pSmioDeviceClass	= g_RS485Data.pDeviceClass + DEVICE_CLASS_SMIO;
	RS485_DEVICE_CLASS*		pSmduDeviceClass	= g_RS485Data.pDeviceClass + DEVICE_CLASS_SMDU;
	RS485_DEVICE_CLASS*		pLargeduDeviceClass = g_RS485Data.pDeviceClass + DEVICE_CLASS_LARGEDU;
	RS485_DEVICE_CLASS*		pSmbrcDeviceClass   = g_RS485Data.pDeviceClass + DEVICE_CLASS_SMBRC;
	RS485_DEVICE_CLASS*		pModbusDeviceClass = g_RS485Data.pDeviceClass + DEVICE_CLASS_MDB;
	static int iRetryTimes = 0;

	INT32  bBattTestStat;
	HANDLE hself;
	static BOOL bSendMsg = FALSE;
	static int iReplyTimes = 0;

	hself = RunThread_GetId(NULL);
	RunThread_Heartbeat(hself);


	bBattTestStat =  bBattTesting(FALSE);

	if(GetSlavePositionSig())
	{
		pSlaveDeviceClass->bNeedReconfig = TRUE;

	}

	if(g_RS485Data.bNeedReInitModeModify)
	{
		pModbusDeviceClass->bNeedReconfig = TRUE;
		return;
	}

	GetNeedProcAddr();

	switch(g_RS485Data.enumRunningMode)
	{
	case RS485_RUN_MODE_STANDALONE://There isn't Slave
		{	

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMDU].pfnSample)
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SMDU].pfnSample(pSmduDeviceClass);
			}
			
			
			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMAC].pfnSample && (!bBattTestStat))
			{  
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SMAC].pfnSample(pSmacDeviceClass);
			}

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBAT].pfnSample && (!bBattTestStat))
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBAT].pfnSample(pSmbatDeviceClass);
			}
			RunThread_Heartbeat(hself);

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMIO].pfnSample && (!bBattTestStat))
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SMIO].pfnSample(pSmioDeviceClass);
			}
			if((pSmacDeviceClass->pRoughData[SMAC_WORKING_STATUS].iValue != SMAC_EQUIP_EXISTENT)
				&& (pSmbatDeviceClass->pRoughData[SMBAT_GROUP_WORKING_STAT].iValue != SMBAT_EQUIP_EXISTENT)
				&& (pSmioDeviceClass->pRoughData[SMIO_WORKING_GROUP_STATUS].iValue != SMIO_EQUIP_EXISTENT)
				&& (pSmduDeviceClass->pRoughData[SMDU_GROUP_SM_STATE].iValue != SMDU_EQUIP_EXISTENT))
			{	
				if(g_RS485Data.pDeviceClass[DEVICE_CLASS_LARGEDU].pfnSample)
				{
					//12��30�� ȥ��Ldu
					//Here change rs485comm attribute
					g_RS485Data.pDeviceClass[DEVICE_CLASS_LARGEDU].pfnSample(pLargeduDeviceClass);
				}
			}

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBRC].pfnSample 
				&& (pSmbatDeviceClass->pRoughData[SMBAT_GROUP_WORKING_STAT].iValue != SMBAT_EQUIP_EXISTENT))
			{
				//printf("\n 111111111111111111111111111  \n");
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBRC].pfnSample(pSmbrcDeviceClass);
			}

			// Modbus equip
			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_MDB].pfnSample)
			{
			    g_RS485Data.pDeviceClass[DEVICE_CLASS_MDB].pfnSample(pModbusDeviceClass);
			}
			

		}
		break;

	case RS485_RUN_MODE_MASTER://There isn't Smdu
		{
			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SLAVE].pfnSample)
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SLAVE].pfnSample(pSlaveDeviceClass);
			}

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMAC].pfnSample && (!bBattTestStat))
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SMAC].pfnSample(pSmacDeviceClass);
			}

			RunThread_Heartbeat(hself);

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBAT].pfnSample && (!bBattTestStat))
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBAT].pfnSample(pSmbatDeviceClass);
			}

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMIO].pfnSample && (!bBattTestStat))
			{
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SMIO].pfnSample(pSmioDeviceClass);
			}

			if((pSmacDeviceClass->pRoughData[SMAC_WORKING_STATUS].iValue != SMAC_EQUIP_EXISTENT)
				&& (pSmbatDeviceClass->pRoughData[SMBAT_GROUP_WORKING_STAT].iValue != SMBAT_EQUIP_EXISTENT)
				&& (pSmioDeviceClass->pRoughData[SMIO_WORKING_GROUP_STATUS].iValue != SMIO_EQUIP_EXISTENT))
			{	
				//if(g_RS485Data.pDeviceClass[DEVICE_CLASS_LARGEDU].pfnSample
				//	&& bNeedSampling(DEVICE_CLASS_LARGEDU))
				if(g_RS485Data.pDeviceClass[DEVICE_CLASS_LARGEDU].pfnSample)
				{
					//12��30�� ȥ��Ldu
					//There change rs485comm attribute
					g_RS485Data.pDeviceClass[DEVICE_CLASS_LARGEDU].pfnSample(pLargeduDeviceClass);
				}
			}

			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBRC].pfnSample 
				&& (pSmbatDeviceClass->pRoughData[SMBAT_GROUP_WORKING_STAT].iValue != SMBAT_EQUIP_EXISTENT))
			{
				//printf("\n 111111111111111111111111111  \n");
				g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBRC].pfnSample(pSmbrcDeviceClass);
			}

			// Modbus equip
			if(g_RS485Data.pDeviceClass[DEVICE_CLASS_MDB].pfnSample)
			{
			    g_RS485Data.pDeviceClass[DEVICE_CLASS_MDB].pfnSample(pModbusDeviceClass);
			}
		}
		break;

	case RS485_RUN_MODE_SLAVE:
		{
			if(iReplyTimes == 5)
			{
				bSendMsg = FALSE;
				iReplyTimes++;
			}
			else
			{
				bSendMsg = TRUE;
				iReplyTimes++;
			}
		}	
		break;

	default:
		{
			TRACE("\n** Please check the master/slave running mode ***\n");
		}
		break;
	}
	
	if(!bSendMsg)
	{
		if(iRetryTimes > 1)
		{
			RUN_THREAD_MSG msgFindEquip;
			RUN_THREAD_MAKE_MSG(&msgFindEquip, NULL, MSG_485_SAMPLER_FINISH_SCAN, 0, 0);
			RunThread_PostMessage(-1, &msgFindEquip, FALSE);
			bSendMsg = TRUE;
			//printf("MSG_485_SAMPLER_FINISH_SCAN: -----------Send Message thru 485 Sample.------------------\n");
		}
		else
		{
			iRetryTimes++;
		}
	}


	return;
}
/*==========================================================================*
* FUNCTION : StuffChnAllEquipNotExist
* PURPOSE  : Stuff sampling channels
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: ENUMSIGNALPROC EnumProc : 
*            LPVOID  lpvoid   : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-25 10:40
*==========================================================================*/
static void StuffChnAllEquipInExistent(ENUMSIGNALPROC EnumProc,		//Callback function for stuffing channels
									   LPVOID lpvoid)				//Parameter of the callback function
{
	int			j = 0;
	RS485_VALUE		stTempEquipExist;	
	stTempEquipExist.iValue	= RS485_EQUIP_NOT_EXISTENT;

	StuffSlaveRectInExist(EnumProc, lpvoid);

	//Slave	
	EnumProc(SLAVE1_CH_EXIST_ST, 
		stTempEquipExist.fValue, 
		lpvoid );

	EnumProc(SLAVE2_CH_EXIST_ST, 
		stTempEquipExist.fValue, 
		lpvoid );

	EnumProc(SLAVE3_CH_EXIST_ST, 
		stTempEquipExist.fValue, 
		lpvoid );
	//Smbat
	for(j = 0;j < SMBAT_STRING_NUM;j++)
	{
		EnumProc(j * MAX_CHN_NUM_PER_SMBAT + SMBAT_CH_EXIST_STAT,
			stTempEquipExist.fValue,
			lpvoid );
	}

	//Smac
	for(j = 0;j < SMAC_NUM;j++)
	{
		EnumProc(j * MAX_CHN_NUM_PER_SMAC + SMAC_CH_EXIST_STATUS,
			stTempEquipExist.fValue,
			lpvoid );	
	}
	//2010/1/16
	//diesel  not have
	EnumProc(SMAC_CH_EXIST_STATUS + 1,
		stTempEquipExist.fValue,
		lpvoid );

	EnumProc(SMAC_CH_EXIST_STATUS + 2,
		stTempEquipExist.fValue,
		lpvoid );	

	//Smio
	for(j = 0;j < MAX_NUM_SMIO;j++)
	{
		EnumProc(j * MAX_CHN_NUM_PER_SMIO + SMIO_CH_EXIST_STATUS,
			stTempEquipExist.fValue,
			lpvoid );
	}

	EnumProc(3999, stTempEquipExist.fValue,lpvoid);//SMIO GROUP

	//AC METER
	for(j = 0;j < ACMETER_NUM_MAX;j++)
	{
	    EnumProc(j * RS485_MODBUS_ACMETER_SPACE + S_CH_ACMETER_EXISTENCE,
		stTempEquipExist.fValue,
		lpvoid );
	}
	

	//Smdu And Stuff Smdu group Sig
	for(j = 0; j < SMDU_MAX_NUM; j++)
	{
		EnumProc(j * SMDU_CHANNEL_DISTANCE + SM_CH_EXIST_ST,
			stTempEquipExist.fValue,
			lpvoid );	

		EnumProc(j * SMDU_CHANNEL_DISTANCE + SM_CH_EXIST_ST, 
			stTempEquipExist.fValue, 
			lpvoid );

		//stuff the battery inexistence
		EnumProc(j * SMDU_CHANNEL_DISTANCE + SM_CH_BATT1_EXIST_ST, 
			stTempEquipExist.fValue, 
			lpvoid );
		EnumProc(j * SMDU_CHANNEL_DISTANCE + SM_CH_BATT2_EXIST_ST, 
			stTempEquipExist.fValue, 
			lpvoid );
		EnumProc(j * SMDU_CHANNEL_DISTANCE + SM_CH_BATT3_EXIST_ST, 
			stTempEquipExist.fValue, 
			lpvoid );
		EnumProc(j * SMDU_CHANNEL_DISTANCE + SM_CH_BATT4_EXIST_ST, 
			stTempEquipExist.fValue, 
			lpvoid );
	}

	EnumProc(SM_CH_SMDU_GROUP_STATE,
		stTempEquipExist.fValue,
		lpvoid );

	LduSetEquipNoExist();
	//printf("*********		LduSetEquipNoExist		****\n\n");
	g_RS485Data.pDeviceClass[DEVICE_CLASS_LARGEDU].pfnStuffChn = LDU_StuffChn;
	if (g_RS485Data.pDeviceClass[DEVICE_CLASS_LARGEDU].pfnStuffChn != NULL)
	{
		//printf("*********		g_RS485Data.pDeviceClass  ****\n\n");
		g_RS485Data.pDeviceClass[DEVICE_CLASS_LARGEDU].pfnStuffChn(g_RS485Data.pDeviceClass 
			+ DEVICE_CLASS_LARGEDU, 
			EnumProc,
			lpvoid);
	}
}

/*==========================================================================*
* FUNCTION : Rs485StuffChn
* PURPOSE  : Stuff sampling channels
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: ENUMSIGNALPROC EnumProc : 
*            LPVOID  lpvoid   : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-25 10:40
*==========================================================================*/
static void Rs485StuffChn(ENUMSIGNALPROC EnumProc,		//Callback function for stuffing channels
						  LPVOID lpvoid)				//Parameter of the callback function
{
	int				i,j;
	//int				iRuningMode;
	RS485_VALUE		stTempEquipExist;	
	stTempEquipExist.iValue	= RS485_EQUIP_NOT_EXISTENT;

	//Stuff  Equip	InExistence	for	 Synchronization ReConfig
	if(g_RS485Data.bNeedReInitModeModify)
	{
		Rs485ReInit(g_RS485Data.enumRunningMode);
		for(i = 0; i < DEVICE_CLASS_NUM; i++)
		{
			if(g_RS485Data.pDeviceClass[i].pfnStuffChn)
			{
				TRACE("\n mode is modify  DEVICE_CLASS_NUM = %d \n",i);
				g_RS485Data.pDeviceClass[i].pfnStuffChn(g_RS485Data.pDeviceClass + i, 
					EnumProc,
					lpvoid);
			}
		}
		StuffChnAllEquipInExistent(EnumProc, lpvoid);
		return;
	}

	if(RS485_RUN_MODE_SLAVE == g_RS485Data.enumRunningMode)
	{
		Rs485ReInit(g_RS485Data.enumRunningMode);
		for(i = 0; i < DEVICE_CLASS_NUM; i++)
		{
			if(g_RS485Data.pDeviceClass[i].pfnStuffChn)
			{
				g_RS485Data.pDeviceClass[i].pfnStuffChn(g_RS485Data.pDeviceClass + i, 
					EnumProc,
					lpvoid);
			}
		}
		StuffChnAllEquipInExistent(EnumProc, lpvoid);
		return;
	}

	//Normal,Stuff sampling data  in the RoughData	
	for(i = 0; i < DEVICE_CLASS_NUM; i++)
	{
		//There isn't SLAVES in the STANDALONE mode
		if(RS485_RUN_MODE_STANDALONE == g_RS485Data.enumRunningMode)
		{
			//There isn't Slave in StandAlong running mode 
			if(DEVICE_CLASS_SLAVE == i)
			{
				if(g_RS485Data.pDeviceClass[i].pfnStuffChn)
				{
					g_RS485Data.pDeviceClass[i].pfnStuffChn(g_RS485Data.pDeviceClass + i, 
						EnumProc,
						lpvoid);
				}
				StuffSlaveRectInExist(EnumProc, lpvoid);
				//Slave	
				EnumProc(SLAVE1_CH_EXIST_ST, 
					stTempEquipExist.fValue, 
					lpvoid );
				EnumProc(SLAVE2_CH_EXIST_ST, 
					stTempEquipExist.fValue, 
					lpvoid );
				EnumProc(SLAVE3_CH_EXIST_ST, 
					stTempEquipExist.fValue, 
					lpvoid );
				continue;
			}
		}

		//There isn't SMDU in the MASTER mode
		if(RS485_RUN_MODE_MASTER == g_RS485Data.enumRunningMode)
		{
			if(DEVICE_CLASS_SMDU == i)
			{
				if(g_RS485Data.pDeviceClass[i].pfnStuffChn)
				{
					g_RS485Data.pDeviceClass[i].pfnStuffChn(g_RS485Data.pDeviceClass + i, 
						EnumProc,
						lpvoid);
				}

				//Smdu And Stuff Smdu group Sig
				for(j = 0; j < SMDU_MAX_NUM; j++)
				{
					EnumProc(j * SMDU_CHANNEL_DISTANCE + SM_CH_EXIST_ST,
						stTempEquipExist.fValue,
						lpvoid );	
					//Stuff the Battery Inexistence
					EnumProc(j * SMDU_CHANNEL_DISTANCE + SM_CH_BATT1_EXIST_ST, 
						stTempEquipExist.fValue, 
						lpvoid );
					EnumProc(j * SMDU_CHANNEL_DISTANCE + SM_CH_BATT2_EXIST_ST, 
						stTempEquipExist.fValue, 
						lpvoid );
					EnumProc(j * SMDU_CHANNEL_DISTANCE + SM_CH_BATT3_EXIST_ST, 
						stTempEquipExist.fValue, 
						lpvoid );
					EnumProc(j * SMDU_CHANNEL_DISTANCE + SM_CH_BATT4_EXIST_ST, 
						stTempEquipExist.fValue, 
						lpvoid );
				}
				EnumProc(SM_CH_SMDU_GROUP_STATE,
					stTempEquipExist.fValue,
					lpvoid );	
				continue;
			}
		}

		if(g_RS485Data.pDeviceClass[i].pfnStuffChn)
		{
			g_RS485Data.pDeviceClass[i].pfnStuffChn(g_RS485Data.pDeviceClass + i, 
				EnumProc,
				lpvoid);
		}
	}

	return;
}
/*==========================================================================*
* FUNCTION : TestTime
* PURPOSE  : for test  efficiency
*            
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
*            
*           
*            
* RETURN   : DLLExport BOOL : 
* COMMENTS : 
* CREATOR  :               DATE: 2008-06-24 10:03
*==========================================================================*/
//static void TestTime(int Position)
//{
//	struct timeval{
//		long tv_sec;  /*��*/
//		long tv_usec; /*΢��*/
//	};
//	struct timezone{
//		int tz_minuteswest; /*��Greenwich ʱ����˶��ٷ���*/
//		int tz_dsttime; /*�չ��Լʱ���״̬*/
//	};
//
//	struct timeval tv;
//	struct timezone tz;
//	gettimeofday(&tv, &tz);
//
//	//printf("Query Position  :: %d  Rs485 Sample tv_Sec; %d\n",Position,tv.tv_sec);
//	//printf("Query Position  :: %d  Rs485 Sample tv_Usec; %d\n",Position,tv.tv_usec);
//}

//���⴦�����
void RS485_SpecificProc(ENUMSIGNALPROC EnumProc,
						LPVOID lpvoid)
{
	if (NULL != g_RS485Data.pDeviceClass[DEVICE_CLASS_SMIO].pfnStuffChn)
	{
		//���⴦����Ϊ YGXIN Ҫ�����SMIO֮���������ͨ���� RELAY �Ĳ�������
		g_RS485Data.pDeviceClass[DEVICE_CLASS_SMIO].pfnStuffChn(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMIO, 
			EnumProc,
			lpvoid);
	}
}
/*==========================================================================*
* FUNCTION : Query
* PURPOSE  : This is the interface function to be called by Equipment
*            Monitoring Function periodically
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: HANDLE          hComm    : Handle of the port, unused here
*            int             nUnitNo  : Sampler address, unused here
*            ENUMSIGNALPROC  EnumProc : Callback function for stuffing channels
*            LPVOID          lpvoid   : Parameter of the callback function
* RETURN   : DLLExport BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-06-24 10:03
*==========================================================================*/
DLLExport BOOL Query(HANDLE hComm,				//Handle of the port, unused here
					 int iUnitNo,				//Sampler address, unused here
					 ENUMSIGNALPROC EnumProc,	//Callback function for stuffing channels
					 LPVOID lpvoid)				//Parameter of the callback function
{
	//time_t the_time, begin_time;

	//begin_time = time((time_t *)0);

	UNUSED(hComm);
	UNUSED(iUnitNo);
	//HANDLE				hself;
	static BOOL			s_bFirstRun = TRUE;

	//printf("********RS485 DEBUG: Query once  MODE = %d  start time =%d !\n",
	//							g_RS485Data.enumRunningMode,
	//							time(NULL));

	static int		s_iCounter = 0;

	if(g_bExitNotification)
	{
		Rs485QueryExit();
		return TRUE;
	}

	if(s_bFirstRun)
	{
		//Frank Wu,20150318, for SoNick Battery
		if(ERR_CFG_OK != MDBEXT_LoadCfg())
		{
			AppLogOut("RS485_SAM", APP_LOG_ERROR, "Failed to call MDBEXT_LoadCfg\n");
		}

		gRtsHand = open("/dev/rts1", O_RDWR);
		Rs485QueryInit();
		SmSampCtrlInit();
		s_bFirstRun = FALSE;
		g_RS485Data.iRunningFlag = RS485_FALG_RUNNING;
		g_RS485Data.bNeedReInitModeModify = FALSE;//��ȥ��������豸�����ڣ�
	}

	

	//Get running mode when found  repeat reconfig
	if(GetModeAndRecvReConfigCmd((INT32 *)(&s_iCounter)))
	{
		TRACE("\n	######  GetModeAndRecvReConfigCmd  #### \n");

		Rs485ReInit(g_RS485Data.enumRunningMode);
		SmSampCtrlInit();
		g_RS485Data.bNeedReInitModeModify = TRUE;
		g_RS485Data.bNeedGetAllBarcode = TRUE;
	}

	RS485_SpecificProc(EnumProc, lpvoid);

	Rs485ParamUnify();

	RunThread_Heartbeat(RunThread_GetId(NULL));

	Rs485Sample();

	RunThread_Heartbeat(RunThread_GetId(NULL));

	Rs485StuffChn(EnumProc, lpvoid);

	g_RS485Data.bNeedReInitModeModify = FALSE;

	if (s_iCounter == 3)//send msg to lcd only time
	{
		s_iCounter ++;//jump to if (s_iCounter != 3)
		RUN_THREAD_MSG msgFindEquip;
		RUN_THREAD_MAKE_MSG(&msgFindEquip, NULL, MSG_FIND_EQUIP, 0, 0);
		RunThread_PostMessage((HANDLE)(-1), &msgFindEquip, FALSE);
	}
	else if (s_iCounter < 3)
	{
		s_iCounter++;
	}

	g_RS485Data.stSMSmplingCtrl.bStartCtlFlag = TRUE;

	//printf("********RS485 DEBUG: Query once  MODE = %d  End time =%d !\n\n",
	//	g_RS485Data.enumRunningMode,
	//	time(NULL));

	//the_time = time((time_t *)0);

	//printf("Query all time is %lf(s)\n", difftime(the_time,begin_time));

	return TRUE;
}



#define	MAX_CHN_NO_SMMODULE		6450
#define	MAX_CHN_NO_LARGEDU		10900

#define	MAX_SIZE_OF_CTL_CMD_STR 128
/*==========================================================================*
* FUNCTION : Control
* PURPOSE  : This is the interface function to be called by Equipment
*            Monitoring Function
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: HANDLE hComm   : Handle of the port, unused here
*            int    iUnitNo : Sampler address, unused here
*            char*  strCmd  : String of the control command,
*                             for example,"5, 53.5", the number before comma
*                             is command No, the number before comma is parameter
* RETURN   : DLLExport BOOL : TRUE : The string is valid
*                             FALSE: The string is invalid
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-03 15:27
*==========================================================================*/
DLLExport BOOL Control(HANDLE hComm,	//Handle of the port, unused here
					   int iUnitNo,		//Sampler address, unused here
					   char* strCmd)	//String of the control command
{
	//time_t the_time, begin_time;

	//begin_time = time((time_t *)0);

	//printf("Control iUnitNo : %d",iUnitNo);

	UNUSED(hComm);
	UNUSED(iUnitNo);

	char				strExtracted[MAX_SIZE_OF_CTL_CMD_STR] = {0};
	int					iPosition;
	int					iCmdNo;			//rather is Control Channel No.
	float				fParam;
	//int					i;

	//To provent g_CanData.CanCommInfo.hCtrlCmdPool not be initialized
	if(RS485_FALG_RUNNING != g_RS485Data.iRunningFlag)
	{
		return FALSE;
	}

	char TempCharValue = ',';

	//Extract the command No.
	iPosition = RS485_StrExtract(strCmd, strExtracted, (char)TempCharValue);

	if((iPosition <= 0) || (iPosition > 10))
	{
		return FALSE;	
	}

	iCmdNo = atoi(strExtracted);


	SmSetCtrlAddr(iCmdNo);

	TempCharValue = ',';
	//Extract the parameter
	iPosition = RS485_StrExtract(strCmd + iPosition, strExtracted, (char)TempCharValue);

	if((iPosition <= 0) || (iPosition > 10))
	{
		return FALSE;	
	}
	fParam = atof(strExtracted);

	//printf("Control iCmdNo : %d",iCmdNo);

	if(iCmdNo < MAX_CHN_NO_SMMODULE)
	{
		if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMAC].pfnSendCmd)
		{
			g_RS485Data.pDeviceClass[DEVICE_CLASS_SMAC].pfnSendCmd(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMAC,
				iCmdNo, 
				fParam, 
				strCmd);//Notes!! Needn't + iPosition
		}

		if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMIO].pfnSendCmd)
		{
			g_RS485Data.pDeviceClass[DEVICE_CLASS_SMIO].pfnSendCmd(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMIO,
				iCmdNo, 
				fParam, 
				strCmd);//Notes!! Needn't + iPosition
		}

		if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBAT].pfnSendCmd)
		{
			g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBAT].pfnSendCmd(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMBAT,
				iCmdNo, 
				fParam, 
				strCmd + iPosition);
		}
	}
	//the_time = time((time_t *)0);

	//printf("Control all time is %lf(s)\n", difftime(the_time,begin_time));

	if(iCmdNo < MAX_CHN_NO_LARGEDU)
	{
		if(g_RS485Data.pDeviceClass[DEVICE_CLASS_LARGEDU].pfnSendCmd)
		{
			g_RS485Data.pDeviceClass[DEVICE_CLASS_LARGEDU].pfnSendCmd(g_RS485Data.pDeviceClass + DEVICE_CLASS_LARGEDU,
				iCmdNo, 
				fParam, 
				strCmd + iPosition);
		}
	}
	//the_time = time((time_t *)0);

	//printf("Control all time is %lf(s)\n", difftime(the_time,begin_time));


	if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SLAVE].pfnSendCmd)
	{
		g_RS485Data.pDeviceClass[DEVICE_CLASS_SLAVE].pfnSendCmd(g_RS485Data.pDeviceClass + DEVICE_CLASS_SLAVE,
			iCmdNo, 
			fParam, 
			strCmd + iPosition);
	}
	//the_time = time((time_t *)0);

	//printf("Control all time is %lf(s)\n", difftime(the_time,begin_time));

	if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMDU].pfnSendCmd)
	{
		g_RS485Data.pDeviceClass[DEVICE_CLASS_SMDU].pfnSendCmd(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMDU,
			iCmdNo, 
			fParam, 
			strCmd + iPosition);
	}
	//the_time = time((time_t *)0);

	//printf("Control all time is %lf(s)\n", difftime(the_time,begin_time));

	if(g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBRC].pfnSendCmd)
	{
		g_RS485Data.pDeviceClass[DEVICE_CLASS_SMBRC].pfnSendCmd(g_RS485Data.pDeviceClass + DEVICE_CLASS_SMBRC,
			iCmdNo, 
			fParam, 
			strCmd + iPosition);
	}
	//the_time = time((time_t *)0);

	//printf("Control all time is %lf(s)\n", difftime(the_time,begin_time));

	if(g_RS485Data.pDeviceClass[DEVICE_CLASS_MDB].pfnSendCmd)
	{
	    g_RS485Data.pDeviceClass[DEVICE_CLASS_MDB].pfnSendCmd(g_RS485Data.pDeviceClass + DEVICE_CLASS_MDB,
		iCmdNo, 
		fParam, 
		strCmd + iPosition);
	}

	//the_time = time((time_t *)0);

	//printf("Control all time is %lf(s)\n", difftime(the_time,begin_time));

	return TRUE;
}

//BOOL bNeedDelayOneTime()
//{
//	static int iDelayTime = 0;
//	if (iDelayTime < 1)
//	{
//		iDelayTime++;
//		return TRUE;
//	}
//	else
//	{
//		iDelayTime = 0;
//		return FALSE;
//	}
//}

static BOOL bNeedReGetProductInfo()
{
	//RS485_DEVICE_CLASS*		pSlaveDeviceClass	= g_RS485Data.pDeviceClass + DEVICE_CLASS_SLAVE;
	RS485_DEVICE_CLASS*		pSmacDeviceClass	= g_RS485Data.pDeviceClass + DEVICE_CLASS_SMAC;
	RS485_DEVICE_CLASS*		pSmbatDeviceClass	= g_RS485Data.pDeviceClass + DEVICE_CLASS_SMBAT;
	RS485_DEVICE_CLASS*		pSmioDeviceClass	= g_RS485Data.pDeviceClass + DEVICE_CLASS_SMIO;
	//RS485_DEVICE_CLASS*		pSmduDeviceClass	= g_RS485Data.pDeviceClass + DEVICE_CLASS_SMDU;
	//RS485_DEVICE_CLASS*		pLargeduDeviceClass = g_RS485Data.pDeviceClass + DEVICE_CLASS_LARGEDU;
	extern  SLAVE_SAMPLER_DATA		g_SlaveSampData;
	extern  SMBRC_SAMP_STDATA		g_SmbrcSampData;

	//˵���豸״̬������ȷ���϶���ȡProduct info ��������������Ҫ���»�ȡ
	if((pSmacDeviceClass->pRoughData[SMAC_WORKING_STATUS].iValue != SMAC_EQUIP_EXISTENT)
		&& (pSmbatDeviceClass->pRoughData[SMBAT_GROUP_WORKING_STAT].iValue != SMBAT_EQUIP_EXISTENT)
		&& (pSmioDeviceClass->pRoughData[SMIO_WORKING_GROUP_STATUS].iValue != SMIO_EQUIP_EXISTENT)
		&& (g_SlaveSampData.aRoughDataGroup[SLAVE_RS485_ACTUAL_NUM].iValue == 0)
		&& (g_SmbrcSampData.aRoughGroupData[SMBRC_G_EXIST_STAT].iValue == SMBRC_STAT_INEXISTENCE))
	{	
		return TRUE;//���еĲ�����
	}
	else
	{
		return FALSE;//�д��ڵ�
	}
}
/*==========================================================================*
* FUNCTION : GetProductInfo
* PURPOSE  : No used
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: HANDLE  hComm   : 
*            int     nUnitNo : 
*            void    *pPI    : 
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-08-06 10:40
*==========================================================================*/
DLLExport BOOL GetProductInfo(HANDLE hComm, int nUnitNo, void *pPI)
{
	INT32						j,m,k;
	INT32						iProdcInfoIndex;
	PRODUCT_INFO *				pInfo;
	static PRODUCT_INFO			st_RecordProdcInfo[END_EQUIP_ID];
	pInfo						= (PRODUCT_INFO*)pPI;
	iProdcInfoIndex				= 0;
	static BOOL	bsFirstRun		= TRUE;

	//printf("	GetProductInfo	nUnitNo=%d	\n",nUnitNo);

	struct _EquipIDIndex
	{
		INT32    iEquipId;
		INT32    iPstIndex;
	};

	typedef struct _EquipIDIndex  EQUIPID_INDEX;


	//printf("~~~~~~~~ RS485 Main Get ProductInfo~~~~~~~\n\n");

	EQUIPID_INDEX  stIndexById[] =
	{
		{259,		SMAC1_EID},//SMAC 1
		{260,		SMAC2_EID},//SMAC 2
		{706,		SMAC3_EID},//SMAC 2
		{707,		SMAC4_EID},//SMAC 2
		{264,		SMIO1_EID},//Main Switch
		{700,		SMIO2_EID},//SMIO 2 /* 2010/12/07���������������ܣ������豸ID{265,		SMIO2_EID} */
		{266,		SMIO3_EID},//SMIO 3
		{267,		SMIO4_EID},//SMIO 4
		{268,		SMIO5_EID},//SMIO 5
		{269,		SMIO6_EID},//SMIO 6
		{270,		SMIO7_EID},//SMIO 7
		{271,		SMIO8_EID},//SMIO 8
		{272,		SMBAT1_EID},//SMBattery1
		{273,		SMBAT2_EID},//SMBattery2
		{274,		SMBAT3_EID},//SMBattery3
		{275,		SMBAT4_EID},//SMBattery4
		{276,		SMBAT5_EID},//SMBattery5
		{277,		SMBAT6_EID},//SMBattery6
		{278,		SMBAT7_EID},//SMBattery7
		{279,		SMBAT8_EID},//SMBattery8
		{280,		SMBAT9_EID},//SMBattery9
		{281,		SMBAT10_EID},//SMBattery10
		{282,		SMBAT11_EID},//SMBattery11
		{283,		SMBAT12_EID},//SMBattery12
		{284,		SMBAT13_EID},
		{285,		SMBAT14_EID},
		{286,		SMBAT15_EID},
		{287,		SMBAT16_EID},
		{288,		SMBAT17_EID},
		{289,		SMBAT18_EID},
		{290,		SMBAT19_EID},
		{291,		SMBAT20_EID},
		//jump 5 largedu ac,10 largedu dc
		{639,		SLAVE1_EID},
		{640,		SLAVE2_EID},
		{641,		SLAVE3_EID},

		{668,		SMBRC1_EID},
		{669,		SMBRC2_EID},
		{670,		SMBRC3_EID},
		{671,		SMBRC4_EID},
		{672,		SMBRC5_EID},
		{673,		SMBRC6_EID},
		{674,		SMBRC7_EID},
		{675,		SMBRC8_EID},
		{676,		SMBRC9_EID},
		{677,		SMBRC10_EID},
		{678,		SMBRC11_EID},
		{679,		SMBRC12_EID},

		{3501,		ACMETER1_EID},
		{3502,		ACMETER2_EID},
		{3503,		ACMETER3_EID},
		{3504,		ACMETER4_EID},

		

		{END_EID,	END_EID}
	};

	//printf(" G	BBU  GetProductInfo !!!! EquipNo : %d \n",nUnitNo);	

	g_RS485Data.enumRunningMode	= GetEnumSigValue(RS485_GET_MODE_EQUIPID,
													SIG_TYPE_SETTING,
													RS485_RUNNING_MODE_SIGID,
													"485_SAMP");
	if (bsFirstRun)
	{
		g_RS485Data.bNeedGetAllBarcode = TRUE;
		bsFirstRun = FALSE;
	}
	else
	{
		//Continue
	}

	if (g_RS485Data.bNeedGetAllBarcode)
	{
		//Clean the struct st_RecordProdcInfo
		memset(st_RecordProdcInfo, 0, sizeof(PRODUCT_INFO) * (END_EQUIP_ID - 1));

		if (g_RS485Data.enumRunningMode == RS485_RUN_MODE_SLAVE)
		{
			TRACE(" G	BBU  GetProductInfo IN  SLAVE Mode  :  \n");	
			g_RS485Data.bNeedGetAllBarcode = TRUE;
			return TRUE;
		}
		else
		{
			SMAC_GetProdctInfo(hComm, nUnitNo, st_RecordProdcInfo);
			SMBAT_GetProdctInfo(hComm, nUnitNo, st_RecordProdcInfo);
			SMIO_GetProdctInfo(hComm, nUnitNo, st_RecordProdcInfo);
			SLAVE_GetProdctInfo(hComm, nUnitNo, st_RecordProdcInfo);			
			SMBRC_GetProdctInfo(hComm, nUnitNo, st_RecordProdcInfo);
			ACMETER_GetProdctInfo(hComm, nUnitNo, st_RecordProdcInfo);
		}

		if (283 < nUnitNo && nUnitNo < 309)
		{	
			pInfo->bSigModelUsed = FALSE;
			return TRUE;
		}

		for (j = 0; stIndexById[j].iEquipId != END_EID; j++)
		{
			if(stIndexById[j].iEquipId == nUnitNo)
			{
				iProdcInfoIndex = stIndexById[j].iPstIndex;	

				pInfo->bSigModelUsed = st_RecordProdcInfo[iProdcInfoIndex].bSigModelUsed;

				//*pInfo = st_RecordProdcInfo[iProdcInfoIndex];
				for (m = 0 ; m < 16; m++)
				{
					pInfo->szHWVersion[m] = st_RecordProdcInfo[iProdcInfoIndex].szHWVersion[m];
					pInfo->szPartNumber[m]= st_RecordProdcInfo[iProdcInfoIndex].szPartNumber[m];
					pInfo->szSWVersion[m]= st_RecordProdcInfo[iProdcInfoIndex].szSWVersion[m];
				}

				for (k = 0; k < 32; k++)
				{
					pInfo->szSerialNumber[k]= st_RecordProdcInfo[iProdcInfoIndex].szSerialNumber[k];
				}

				TRACE("G	BBU EquipNo :%d \n", nUnitNo);
				TRACE("G	BBU bSigModelUsed:%d\n", pInfo->bSigModelUsed);
				TRACE("G	BBU PartNumber:%s\n", pInfo->szPartNumber);
				TRACE("G	BBU SW version:%s\n", pInfo->szSWVersion);
				TRACE("G	BBU HW version:%s\n", pInfo->szHWVersion);
				TRACE("G	BBU Serial Number:%s\n", pInfo->szSerialNumber);				

				break;
			}
		}

		//Frank Wu,20150318, for SoNick Battery
		if(END_EID == stIndexById[j].iEquipId)//find none
		{
			MDBEXT_GetProdctInfo(hComm, nUnitNo, pInfo);
		}

		//�豸�������ڣ���
		if (bNeedReGetProductInfo())
		{
			g_RS485Data.bNeedGetAllBarcode = TRUE;
			return FALSE;
		}
		else
		{
			g_RS485Data.bNeedGetAllBarcode = FALSE;//ֻ����һ���ط���FALSE��������
		}

	}
	else
	{
		for (j = 0; stIndexById[j].iEquipId != END_EID; j++)
		{
			if(stIndexById[j].iEquipId == nUnitNo)
			{
				iProdcInfoIndex = stIndexById[j].iPstIndex;	
				//*pInfo = st_RecordProdcInfo[iProdcInfoIndex];
				pInfo->bSigModelUsed = st_RecordProdcInfo[iProdcInfoIndex].bSigModelUsed;
				for (m = 0 ; m < 16; m++)
				{
					pInfo->szHWVersion[m] = st_RecordProdcInfo[iProdcInfoIndex].szHWVersion[m];
					pInfo->szPartNumber[m]= st_RecordProdcInfo[iProdcInfoIndex].szPartNumber[m];
					pInfo->szSWVersion[m]= st_RecordProdcInfo[iProdcInfoIndex].szSWVersion[m];
				}

				for (k = 0; k < 32; k++)
				{
					pInfo->szSerialNumber[k]= st_RecordProdcInfo[iProdcInfoIndex].szSerialNumber[k];
				}

				//printf("G	BBU EquipNo :%d \n", nUnitNo);
				//printf("G	BBU bSigModelUsed:%d\n", pInfo->bSigModelUsed);
				//printf("G	BBU PartNumber:%s\n", pInfo->szPartNumber);
				//printf("G	BBU SW version:%s\n", pInfo->szSWVersion);
				//printf("G	BBU HW version:%s\n", pInfo->szHWVersion);
				//printf("G	BBU Serial Number:%s\n", pInfo->szSerialNumber);

				break;
			}
		}

		//Frank Wu,20150318, for SoNick Battery
		if(END_EID == stIndexById[j].iEquipId)//find none
		{
			MDBEXT_GetProdctInfo(hComm, nUnitNo, pInfo);
		}

	}

	if(g_RS485Data.CommPort.hCommPort && g_RS485Data.CommPort.bOpened)
	{
		RS485_Close(g_RS485Data.CommPort.hCommPort);
		g_RS485Data.CommPort.bOpened   = FALSE;
		g_RS485Data.CommPort.enumAttr  = 4;
		g_RS485Data.CommPort.enumBaud  = 0;
	}
	return TRUE;
}


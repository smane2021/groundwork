/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : NGMC-HF(Next Generation Controller - High Feature)
*
*  FILENAME : rs485_comm.h
*  CREATOR  : Frank Cao                DATE: 2009-06-02 11:37
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/


#ifndef _RS485_COMM_H_


#include <termios.h>

extern int gRtsHand;
struct _RS485_BAUD_ATTR
{
	int				nBaud;		// the baud rate
	int				nOdd;		// 0: none, 1: odd, 2: even
	int				nData;		// the data bits, 5-8
	int				nStop;		// the stop bits, 1-2
	unsigned long	ulUnixBaud;	// the unix defined baud rates.
};
typedef struct _RS485_BAUD_ATTR	RS485_BAUD_ATTR;


/* the struct of timeout */
struct RS485_Timeouts
{				
	int	nReadTimeout;	/* The total timeout on reading. -1: wait for ever	*/
	int	nWriteTimeout;	/* The total timeout on writing. -1: wait for ever	*/

	/* timeout between 2 chars after first 1 bytes sent out or received		*/
#define TIMEOUT_INTERVAL_CHAR	50 /*ms, minimum BPS is 1000/(50/10)=20bps	*/
	int	nIntervalTimeout;	/* the timeout between 2 char.					*/
};
typedef struct RS485_Timeouts		RS485_TIMEOUTS;

struct Rs485Driver	//	The HAL driver for direct serial port
{				
	int				nLastErrorCode;	//	NOTE: MUST BE THE FIRST FIELD! 
	//  the last error code.

	int				fdSerial;		//	the handle from open()
	//int				fdRtsFor485;	//	485 need open 2 devcies, this is the handle of second one
	RS485_TIMEOUTS	toTimeouts;		//	read and write timeout

	RS485_BAUD_ATTR	attr;			// the attr of the port.
};				
typedef struct Rs485Driver		RS485_DRV;

#define COM_RETRY_TIME		3		// the maximum retry time on error.

BOOL RS485_SetAttr(IN int fd, IN char *pParam);
HANDLE RS485_Open(IN char *pOpenParams,
				  IN int nTimeout,
				  OUT int *pErrCode);
int RS485_Read(IN HANDLE hPort, OUT BYTE *pBuffer, IN int nBytesToRead);
int RS485_ReadForSlave(IN HANDLE hPort, OUT BYTE* pBuffer, IN int nBytesToRead);
int RS485_Write(IN HANDLE hPort, IN BYTE *pBuffer,	IN int nBytesToWrite);
int RS485_Close(IN HANDLE hPort);
INT32 RS485_ClrBuffer(IN HANDLE hPort);

int RS485_ReadForSMBRC(IN HANDLE hPort, OUT BYTE* pBuffer, IN int nBytesToRead);

#ifndef INIT_TIMEOUTS

#define INIT_TIMEOUTS(to, rto, wto)	((to).nReadTimeout = (rto),		\
	(to).nWriteTimeout = (wto),	\
	(to).nIntervalTimeout = TIMEOUT_INTERVAL_CHAR)
#endif

#endif //_RS485_COMM_H_

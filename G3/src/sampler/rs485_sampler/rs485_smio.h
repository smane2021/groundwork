/*============================================================================*
 *         Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : Unified Data Type Definition
 *
 *  FILENAME : rs485_smio.h
 *  PURPOSE  : Define base data type.
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2009-04-14      V1.0           IlockTeng         Created.    
 *         
 *                                                     
 *-----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *          
 *      
 *-----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *      
 *    
 *============================================================================*/
#ifndef __RS485_SAMPLER_SMIO_H
#define __RS485_SAMPLER_SMIO_H

#include <stdio.h>
#include "basetypes.h"
#include "err_code.h"
#include "pubfunc.h"
#include "string.h"
#include "rs485_main.h"				

#define FUNCTION_GENERAL_ERROR		-1
#define	NO_RESPONSE_BEXISTENCE      1
#define COMMUNICATION_ERROR         2
#define RESPONSE_OK                 0
#define SMIO_GROUP_CHANNEL			3999
#define	SMIO_CH_START				4000
#define	SMIO_CH_END					4320
#define SMIO_CH_END_FLAG			-1
#define	SMIO_SHECKSUM_EOI			 5
#define bySOI						0x7E			//START OF INFORMATION
#define byEOI						0x0D			//END OF INFORMATION
#define SMIO_CID2_GETVER			0x4f
#define	SMIO_CID1_0XD1				0xd1
#define SMIO_CID2_0X41				0x41
#define	SMIO_CID2_0X45				0x45
#define	SMIO_CID2_0X43				0x43
#define	SMIO_CID2_41SIG_NUM			13
#define	SMIO_CID2_AD_INPUT			7

#define MAX_NUM_SMIO				8
#define SMIO_ADDR_START				96
#define SMIO_ADDR_END				103
#define SMIO_ADDR_NUM_96			96
#define SMIO_ADDR_NUM_97			97
#define SMIO_ADDR_NUM_98			98
#define SMIO_ADDR_NUM_99			99
#define SMIO_ADDR_NUM_100			100
#define SMIO_ADDR_NUM_101			101
#define SMIO_ADDR_NUM_102			102
#define SMIO_ADDR_NUM_103			103



#define MAX_CHN_NUM_PER_SMIO		40
#define SMIO_SAMP_INVALID_VALUE		(-9999)			//initialization data
#define SMIO_SAMP_FREQUENCY			3				//control  sample the frequency

#define SMIO_SAMPLE_OK				0
#define SMIO_SAMPLE_FAIL			1

//communication status	
#define	SMIO_COMM_OK				0
#define	SMIO_COMM_FAIL				1

//existent status
#define SMIO_EQUIP_EXISTENT			0
#define SMIO_EQUIP_NOT_EXISTENT		1

#define	MAX_CHN_DISTANCE_SMIO		40

#define MAX_RECVBUFFER_NUM_SMIO		4096	
#define MAX_SENDBUFFER_NUM_SMIO		50

enum YDN23PROTOCOLINFO_SMIO
{
	CHECKSUM_ERROR_YDN23_SMIO = 2,
	ADDR_YDN23_SMIO = 3,

	CID1H_YDN23_SMIO = 5,
	CID1L_YDN23_SMIO = 6,
	CID2_YDN23_SMIO	= 7,
	LENGTH_YDN23_SMIO = 9,
	DATA_YDN23_SMIO = 13,
	//others will add here
};


enum  ROUGH_DATA_IDX_SMIO
{	
	//0x41
	//SMIO_CHNUM = 0, // M = 13  unused		
	SMIO_A_DINPUT1 = 0,						
	SMIO_A_DINPUT2,				
	SMIO_A_DINPUT3,					
	SMIO_A_DINPUT4,						
	SMIO_A_DINPUT5,		 				
	SMIO_A_DINPUT6,				
	SMIO_A_DINPUT7,	
	SMIO_ANOLOGINPUT1,
	SMIO_ANOLOGINPUT2,
	SMIO_ANOLOGINPUT3,
	SMIO_ANOLOGINPUT4,
	SMIO_ANOLOGINPUT5,
	SMIO_FREQUENCYINPUT,					

	//0x43
	//SMIO_RELAY_NUM,//unused							
	SMIO_RELAY1_STATUS,
	SMIO_RELAY2_STATUS,				
	SMIO_RELAY3_STATUS,
	

	SMIO_COMM_STATUS,
	SMIO_WORKING_STATUS,
	SMIO_WORKING_ADDR,


	SMIO_WORKING_GROUP_STATUS,

	SMIO_INTERRUPT_TIMES,
	SMIO_G_EXIST,
	//here add
	SMIO_SEQ_NUM,
	SMIO_MAX_SIGNALS_NUM,
};


enum SMIO_SAMP_CHANNEL
{
	//0X41
	//SMIO_CH_NUM,			//unused	!!!
	SMIO_CH_A_DINPUT1 = 4000,//Digital Input 1 and Analogue Input 6
	SMIO_CH_A_DINPUT2,		//Digital Input 2 and Analogue Input 7
	SMIO_CH_A_DINPUT3,		//Digital Input 3 and Analogue Input 8
	SMIO_CH_A_DINPUT4,		//Digital Input 4 and Analogue Input 9
	SMIO_CH_A_DINPUT5,		//Digital Input 5 and Analogue Input 10
	SMIO_CH_A_DINPUT6,		//Digital Input 6 and Analogue Input 11
	SMIO_CH_A_DINPUT7,		//Digital Input 7 and Analogue Input 12
	SMIO_CH_ANOLOGINPUT1,	//Analogue Input 1
	SMIO_CH_ANOLOGINPUT2,	//Analogue Input 2
	SMIO_CH_ANOLOGINPUT3,	//Analogue Input 3
	SMIO_CH_ANOLOGINPUT4,	//Analogue Input 4
	SMIO_CH_ANOLOGINPUT5,	//Analogue Input 5
	SMIO_CH_FREQUENCYINPUT,	//Frequency Input ,

	//0X43
	//SMIO_CH_RELAY_NUM,	  //unused	!!!n=3
	SMIO_CH_RELAY1_STATUS,//Relay 1 Status
	SMIO_CH_RELAY2_STATUS,//Relay 2 Status
	SMIO_CH_RELAY3_STATUS,//Relay 3 Status

	//
	SMIO_CH_COMM_STATUS,
	SMIO_CH_EXIST_STATUS,
	SMIO_CH_WORKING_ADDR,
};


union _STRTOFLOAT
{
	float f_value;       
	BYTE  by_value[4];   
};
typedef union _STRTOFLOAT	STRTOFLOAT;


struct Smio_CommInfo
{
	BOOL bNeedReconfig;
	//used to control the cadence of cmd
	int	iCmdCounter[SMIO_ADDR_END - SMIO_ADDR_START + 1];
};

typedef struct Smio_CommInfo	SMIO_COMM_INFO;

struct SmioSamplerData
{
	BYTE			abyRcvBuf[MAX_RECVBUFFER_NUM_SMIO];	
	BYTE			abySendBuf[MAX_SENDBUFFER_NUM_SMIO];
	
	SMIO_COMM_INFO SmioCommInfo;
	
	RS485_VALUE    aRoughDataSmio[SMIO_ADDR_END - SMIO_ADDR_START + 1][SMIO_MAX_SIGNALS_NUM];
};

typedef struct SmioSamplerData	SMIO_SAMPLER_DATA;	

VOID SMIO_SendCtlCmd(RS485_DEVICE_CLASS*  SmioDeviceClass,INT32 iChannelNo, float fParam,char *pString);
INT32 SMIO_InitRoughValue(RS485_DEVICE_CLASS  *SmioDeviceClass);
INT32 SMIOSample(RS485_DEVICE_CLASS*  SmioDeviceClass);
INT32 SMIO_StuffChannel(RS485_DEVICE_CLASS*  SmioDeviceClass,ENUMSIGNALPROC EnumProc,LPVOID lpvoid);
void SMIO_GetProdctInfo(HANDLE hComm, int nUnitNo, void *pPI);
INT32	SMIONeedCommProc(void);
INT32 SMIOReopenPort(void);
INT32 SMIOSampleCmd43(INT32 iAddr,RS485_DEVICE_CLASS*  SmioDeviceClass);
#endif

/*============================================================================*
*         Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                          ALL RIGHTS RESERVED
*
*  PRODUCT  : Unified Data Type Definition
*
*  FILENAME : rs485_slaves.c
*  PURPOSE  : Sampling  slave and control slave
*  
*  HISTORY  :
*    DATE            VERSION        AUTHOR            NOTE
*    2007-07-12      V1.0           IlockTeng         Created.    
*    2009-04-14     
*                                                     
*-----------------------------------------------------------------------------
*  GLOBAL VARIABLES
*    NAME                                    DESCRIPTION
*          
*      
*-----------------------------------------------------------------------------
*  GLOBAL FUNCTIONS
*    NAME                                    DESCRIPTION
*      
*    
*============================================================================*/

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include	"rs485_slaves.h"
#include	"rs485_report.h"
#include	"public.h"													
#include	"rs485_comm.h"
LOCAL INT32 SLAVE_SampleCmdE3(RS485_DEVICE_CLASS* SlaveDeviceClass,
			      INT32 iAddr,
			      INT32 SequenceNum);
LOCAL INT32 SLAVE_SampleCmdE1(RS485_DEVICE_CLASS* SlaveDeviceClass,
			      INT32 iAddr,
			      INT32 iSlaveEquipNoSequenceNum);

LOCAL INT32	RecvDataFromSLAVE(HANDLE hComm, 
				  CHAR* sRecStr,  
				  INT32 *piStrLen,
				  INT32 iWantToReadnByte);
static BYTE SLAVEMergeAsc(char *p);

static int	iCurrentSlaveAddress = SLAVE_ADDR_START;

#define SLAVE_CH_END_FLAG				-1
SLAVE_SAMPLER_DATA					g_SlaveSampData;
extern RS485_SAMPLER_DATA				g_RS485Data;
/*=============================================================================*
* FUNCTION: RecvDataFromMasterOrSlave()
* PURPOSE  :Receive data of SM from 485 com
* RETURN   : int : byte number ,but if return -1  means  error
* ARGUMENTS:
*						CHAR*	sRecStr :		
*								hComm	 :	485 com handle 
*								iStrLen	 :
* CALLS    : 
* CALLED BY: 
*								DLLExport BOOL Query()
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
static INT32	RecvDataFromMasterOrSlave(	HANDLE hComm, CHAR* sRecStr,  INT32 iStrLen)
{
	INT32 i = 0;		//Record accurate data's number
	INT32 j = 0;		//Control the loop,avoid infinite loop

	ASSERT(sRecStr != NULL);
	UNUSED(iStrLen);

	//found	first  byte,if error return -1
	do
	{	
		if(RS485_Read(hComm,(BYTE *)sRecStr,1) > 0)	
		{
			i++; 
		}
		else
		{
			j++;
		}
	}
	while(*(sRecStr + 0) != bySOI && (i < 99) && (j < 99));

	if(j > 99)
	{
		Sleep(1);
		return -1;
	}

	//Point to second	byte
	sRecStr++;
	i = 0;
	j = 0;	

	if(*(sRecStr - 1) == bySOI)
	{	
		//Until received  complete a data,if error return -1
		do
		{
			if(RS485_Read(hComm,(BYTE *)sRecStr,1) > 0)
			{
				i++; 
				sRecStr++;//note: "++"mustn't overflow
			}
			else
			{
				j++;
			}				
		}
		while(*(sRecStr-1) != byEOI && (i< (MAX_RECTIFIERS_CHN_NUM_PER_SLAVE - 1) )&& (j < 99 ));	
	}

	if(j > 99)
	{
		Sleep(1);
		//Continue to receive data in the next cycle
		return -1;
	}

	return (i+1);//REC BYTE NUM
}
static	BYTE HexToAsc(IN BYTE Hex)
{
	BYTE cTemp;
	if( Hex<=9 && Hex>=0 )
		cTemp = Hex+0x30;
	else
		cTemp = Hex-0x0a+'A';

	return cTemp;
}
/*=============================================================================*
* FUNCTION: GetCtrlInfoByChannelNo
* PURPOSE : Get control sig from master by ChannelNo
* INPUT:	 iChannelNo
* OUT	:	 Dest
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: SLAVE_SendCtlCmd
*    
*	Refer to  the resource protocol
*============================================================================*/
LOCAL VOID GetCtrlInfoByChannelNo(int iChannelNo, SAMPLING_VALUE *Dest)
{
	//Slave 11
	if(iChannelNo >= SLAVE1_RECT_DC_ON_OFF_CTRL 
		&& iChannelNo <= SLAVE1_RECT_PHASE + MAX_RECT_RECT_CHN_PER_SLAVE * 100)
	{
		//Slave 1 rectifier	11900 to 16899
		if((iChannelNo - SLAVE1_RECT_DC_ON_OFF_CTRL)%MAX_RECT_RECT_CHN_PER_SLAVE == 0)
		{
			Dest[SLAVE_COMMAND_ID].iValue = 0x10;
			Dest[SLAVE_MODULE_ID].iValue  = (iChannelNo - SLAVE1_RECT_DC_ON_OFF_CTRL)/MAX_RECT_RECT_CHN_PER_SLAVE;
		}
		else if((iChannelNo - SLAVE1_RECT_AC_ON_OFF_CTRL)%MAX_RECT_RECT_CHN_PER_SLAVE == 0)
		{
			Dest[SLAVE_COMMAND_ID].iValue = 0x1F;
			Dest[SLAVE_MODULE_ID].iValue  = (iChannelNo - SLAVE1_RECT_AC_ON_OFF_CTRL)/MAX_RECT_RECT_CHN_PER_SLAVE;
		}
		else if((iChannelNo - SLAVE1_RECT_LED_CTRL)%MAX_RECT_RECT_CHN_PER_SLAVE == 0)
		{
			Dest[SLAVE_COMMAND_ID].iValue = 0x11;
			Dest[SLAVE_MODULE_ID].iValue  = (iChannelNo - SLAVE1_RECT_LED_CTRL)/MAX_RECT_RECT_CHN_PER_SLAVE;
		}
		else if((iChannelNo - SLAVE1_RECT_RESET) % MAX_RECT_RECT_CHN_PER_SLAVE == 0)
		{
			Dest[SLAVE_COMMAND_ID].iValue = 0x17;	
			Dest[SLAVE_MODULE_ID].iValue  = (iChannelNo - SLAVE1_RECT_RESET)/MAX_RECT_RECT_CHN_PER_SLAVE;
		}
		else if((iChannelNo - SLAVE1_RECT_POSITION) % MAX_RECT_RECT_CHN_PER_SLAVE == 0)
		{
			Dest[SLAVE_COMMAND_ID].iValue = 0xf1;	
			Dest[SLAVE_MODULE_ID].iValue  = (iChannelNo - SLAVE1_RECT_POSITION)/MAX_RECT_RECT_CHN_PER_SLAVE;
		}
		else if((iChannelNo - SLAVE1_RECT_PHASE) % MAX_RECT_RECT_CHN_PER_SLAVE == 0)
		{
			Dest[SLAVE_COMMAND_ID].iValue = 0xf2;	
			Dest[SLAVE_MODULE_ID].iValue  = (iChannelNo - SLAVE1_RECT_PHASE)/MAX_RECT_RECT_CHN_PER_SLAVE;
		}

		iCurrentSlaveAddress = SLAVE_ADDR_START;
	}

	//SLAVE 22
	if(iChannelNo >= SLAVE2_RECT_DC_ON_OFF_CTRL 
		&& iChannelNo <= SLAVE2_RECT_PHASE + MAX_RECT_RECT_CHN_PER_SLAVE * 100)
	{
		if((iChannelNo - SLAVE2_RECT_DC_ON_OFF_CTRL)%MAX_RECT_RECT_CHN_PER_SLAVE == 0)
		{
			//Slave 2 rectifier	17000 to 21999
			Dest[SLAVE_COMMAND_ID].iValue = 0x10;
			Dest[SLAVE_MODULE_ID].iValue  = (iChannelNo - SLAVE2_RECT_DC_ON_OFF_CTRL)/MAX_RECT_RECT_CHN_PER_SLAVE;
		}
		else if((iChannelNo - SLAVE2_RECT_AC_ON_OFF_CTRL)%MAX_RECT_RECT_CHN_PER_SLAVE == 0)
		{
			Dest[SLAVE_COMMAND_ID].iValue = 0x1F;
			Dest[SLAVE_MODULE_ID].iValue  = (iChannelNo - SLAVE2_RECT_AC_ON_OFF_CTRL)/MAX_RECT_RECT_CHN_PER_SLAVE;
		}
		else if((iChannelNo - SLAVE2_RECT_LED_CTRL)%MAX_RECT_RECT_CHN_PER_SLAVE == 0)
		{
			Dest[SLAVE_COMMAND_ID].iValue = 0x11;
			Dest[SLAVE_MODULE_ID].iValue  = (iChannelNo - SLAVE2_RECT_LED_CTRL)/MAX_RECT_RECT_CHN_PER_SLAVE;
		}
		else if((iChannelNo - SLAVE2_RECT_RESET) % MAX_RECT_RECT_CHN_PER_SLAVE == 0)
		{
			Dest[SLAVE_COMMAND_ID].iValue = 0x17;	
			Dest[SLAVE_MODULE_ID].iValue  = (iChannelNo - SLAVE2_RECT_RESET)/MAX_RECT_RECT_CHN_PER_SLAVE;
		}
		else if((iChannelNo - SLAVE2_RECT_POSITION) % MAX_RECT_RECT_CHN_PER_SLAVE == 0)
		{
			Dest[SLAVE_COMMAND_ID].iValue = 0xf1;	
			Dest[SLAVE_MODULE_ID].iValue  = (iChannelNo - SLAVE2_RECT_POSITION)/MAX_RECT_RECT_CHN_PER_SLAVE;
		}
		else if((iChannelNo - SLAVE2_RECT_PHASE) % MAX_RECT_RECT_CHN_PER_SLAVE == 0)
		{
			Dest[SLAVE_COMMAND_ID].iValue = 0xf2;	
			Dest[SLAVE_MODULE_ID].iValue  = (iChannelNo - SLAVE2_RECT_PHASE)/MAX_RECT_RECT_CHN_PER_SLAVE;
		}

		iCurrentSlaveAddress = SLAVE_ADDR_START + 1;
	}
	//SLAVE	3
	if(iChannelNo >= SLAVE3_RECT_DC_ON_OFF_CTRL 
		&& iChannelNo < SLAVE3_RECT_PHASE + MAX_RECT_RECT_CHN_PER_SLAVE * 100)
	{
		if((iChannelNo - SLAVE3_RECT_DC_ON_OFF_CTRL)%MAX_RECT_RECT_CHN_PER_SLAVE == 0)
		{
			//Slave 3 rectifier	22100 to 27099
			Dest[SLAVE_COMMAND_ID].iValue = 0x10;
			Dest[SLAVE_MODULE_ID].iValue  = (iChannelNo - SLAVE3_RECT_DC_ON_OFF_CTRL)/MAX_RECT_RECT_CHN_PER_SLAVE;
		}
		else if((iChannelNo - SLAVE3_RECT_AC_ON_OFF_CTRL)%MAX_RECT_RECT_CHN_PER_SLAVE == 0)
		{
			Dest[SLAVE_COMMAND_ID].iValue = 0x1F;
			Dest[SLAVE_MODULE_ID].iValue  = (iChannelNo - SLAVE3_RECT_AC_ON_OFF_CTRL)/MAX_RECT_RECT_CHN_PER_SLAVE;
		}
		else if((iChannelNo - SLAVE3_RECT_LED_CTRL) % MAX_RECT_RECT_CHN_PER_SLAVE == 0)
		{
			Dest[SLAVE_COMMAND_ID].iValue = 0x11;
			Dest[SLAVE_MODULE_ID].iValue  = (iChannelNo - SLAVE3_RECT_LED_CTRL)/MAX_RECT_RECT_CHN_PER_SLAVE;
		}
		else if((iChannelNo - SLAVE3_RECT_RESET) % MAX_RECT_RECT_CHN_PER_SLAVE == 0)
		{
			Dest[SLAVE_COMMAND_ID].iValue = 0x17;	
			Dest[SLAVE_MODULE_ID].iValue  = (iChannelNo - SLAVE3_RECT_RESET)/MAX_RECT_RECT_CHN_PER_SLAVE;
		}
		else if((iChannelNo - SLAVE3_RECT_POSITION) % MAX_RECT_RECT_CHN_PER_SLAVE == 0)
		{
			Dest[SLAVE_COMMAND_ID].iValue = 0xf1;	
			Dest[SLAVE_MODULE_ID].iValue  = (iChannelNo - SLAVE3_RECT_POSITION)/MAX_RECT_RECT_CHN_PER_SLAVE;
		}
		else if((iChannelNo - SLAVE3_RECT_PHASE) % MAX_RECT_RECT_CHN_PER_SLAVE == 0)
		{
			Dest[SLAVE_COMMAND_ID].iValue = 0xf2;	
			Dest[SLAVE_MODULE_ID].iValue  = (iChannelNo - SLAVE3_RECT_PHASE)/MAX_RECT_RECT_CHN_PER_SLAVE;
		}

		iCurrentSlaveAddress = SLAVE_ADDR_START + 2;
	}

}
/*=============================================================================*
* FUNCTION: CheckSum()
* PURPOSE : Cumulative  result
* INPUT:	 BYTE *Frame
*     
*
* RETURN:
*     static void
*
* CALLS: 
*     void
*
* CALLED BY: 
*    
*			 copy from SMAC        DATE: 2009-05-08		14:39
*============================================================================*/
static void CheckSum( BYTE *Frame )
{
	WORD R=0;

	int i, nLen = (int)strlen( (char*)Frame);  // ����ҪУ����ַ�������

	for( i=1; i < nLen; i++ )
	{
		R += *(Frame+i);
	}

	sprintf( (char *)Frame+nLen, "%04X\r", (WORD)-R );
}
/*=============================================================================*
* FUNCTION: LenCheckSum
* PURPOSE : calculate checksum of length
* INPUT: 
*		wLen: IN,  length of preparative check data	
*
* RETURN:
*    esulte of verify  data
*
* CALLS: 
*     void
*
* CALLED BY: 
*     Pack_Report_Data
*	   copy from smac      DATE: 2009-05-06		08:39
*============================================================================*/
LOCAL BYTE LenCheckSum(WORD wLen)
{
	BYTE byLenCheckSum;

	//������Ϊ0������Ҫ����
	if (wLen == 0)
		return 0;

	byLenCheckSum = 0;
	byLenCheckSum += wLen & 0x000F;         //ȡ���4��BIT
	byLenCheckSum += (wLen >> 4) & 0x000F;  //ȡ��4~7��BIT
	byLenCheckSum += (wLen >> 8) & 0x000F;  //ȡ��8~11��BIT
	byLenCheckSum %= 16;                    //ģ16
	byLenCheckSum = (~byLenCheckSum) + 1;   //ȡ����1
	byLenCheckSum &= 0x0F;                  //ֻȡ4��BIT

	return byLenCheckSum;
}
/*=============================================================================*
* FUNCTION: ClearBuffer(CHAR* Pbuffer)
* PURPOSE :clean bufferrs
* INPUT:	CHAR* Pbuffer
*     
*
* RETURN:
*     LOCAL VOID
*
* CALLS: 
*     void
*
* CALLED BY: 
*    
*			 Ilock teng        DATE: 2009-05-08		14:39
*============================================================================*/
LOCAL VOID ClearBuffer(CHAR* Pbuffer, INT32 iStrLen)
{
	memset(Pbuffer, '\0',(unsigned long)iStrLen);
}

LOCAL INT32 SLAVE_SendAndRead(
			      HANDLE	hComm,			 
			      BYTE		*sSendStr,		
			      BYTE		*abyRcvBuf,      
			      INT32		nSend,
			      BYTE      cWaitTime,
			      INT32		iWantToReadnByte);	

/*=============================================================================*
* FUNCTION: SLAVEGetAddrBySeqNum
* PURPOSE :
* INPUT: 
*     
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SLAVEGetAddrBySeqNum(INT32 iSeq)
{
	INT32	iAddr;
	INT32	iSlaveSequence;

	for(iAddr = SLAVE_ADDR_START; iAddr <= SLAVE_ADDR_END; iAddr++)
	{
		iSlaveSequence = 
			g_SlaveSampData.aRoughDataSlave[iAddr - SLAVE_ADDR_START][SLAVE_SEQ_NO].iValue ;

		if (iSlaveSequence == iSeq)
		{
			return iAddr;
		}
	}

	return -1;

}

/*=============================================================================*
* FUNCTION: SLAVE_SendCtlCmd
* PURPOSE : Get Conctrol command and copy to  g_SlaveSampData.abyCtrlBuf
* INPUT: 
*     
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
VOID SLAVE_SendCtlCmd(RS485_DEVICE_CLASS*  SlaveDeviceClass, 
		      INT32 iChannelNo,
		      float fParam)	
{
	INT32							j;
	BYTE							bBatteryManagementstate;
	TAGSM_VALUE						GetCtrlValues;
	TAGSM_VALUE						st_FloatChargeVoltage;
	INT32							iSendDataLength;
	INT32							iSlaveAddr;


	if (iChannelNo < 11500 || iChannelNo > 27100)
	{
		return;
	}

	if(SLAVE_MasterNeedCommProc())
	{
		SLAVE_MasterReopenPort();
	}

	ClearBuffer((CHAR*)(g_SlaveSampData.abySendBuf), MAX_SENDMASTERFRAMES_IN_BUFF);

	if (RS485_RUN_MODE_SLAVE == g_RS485Data.enumRunningMode)
	{
		g_SlaveSampData.aRoughDataSlave[0][SLAVE_CTRL_ADDR_FLAG].iValue 
			= SLAVE_NOT_CTRL_ADDR;
		return;
	}

	TRACE("#########SLAVE_  SendCtlCmd	########  iChannelNo = %d \n",(int)iChannelNo);

	if (iChannelNo < SLAVE_CURRENT_LIMIT_CTRL 
		|| (iChannelNo > (SLAVE3_RECT_PHASE + MAX_RECT_RECT_CHN_PER_SLAVE * 60)))
	{
		return;
	}

	//1. first of all, determine the ADDRESS for Stuff_YDN23SendCmd		
	if(SLAVE_CURRENT_LIMIT_CTRL <= iChannelNo 
		&& iChannelNo < SLAVE_CURRENT_LIMIT_CTRL + MAX_CHN_NUM_PER_SLAVE)
	{
		//11501--11600		slave 1
		g_SlaveSampData.aRoughDataSlave[0][SLAVE_CTRL_ADDR_FLAG].iValue 
			= SLAVE1_EQP_NO_SEQUENCE_NUM;
	}
	else if(SLAVE_CURRENT_LIMIT_CTRL + MAX_CHN_NUM_PER_SLAVE <= iChannelNo 
		&& iChannelNo < SLAVE_CURRENT_LIMIT_CTRL + 2 * MAX_CHN_NUM_PER_SLAVE)
	{
		//11601---11700		slave 2
		g_SlaveSampData.aRoughDataSlave[0][SLAVE_CTRL_ADDR_FLAG].iValue 
			= SLAVE2_EQP_NO_SEQUENCE_NUM;
	}
	else if(SLAVE_CURRENT_LIMIT_CTRL + 2 * MAX_CHN_NUM_PER_SLAVE <= iChannelNo 
		&& iChannelNo < SLAVE_CURRENT_LIMIT_CTRL + 3 * MAX_CHN_NUM_PER_SLAVE)
	{
		//11701---11800	slave 3
		g_SlaveSampData.aRoughDataSlave[0][SLAVE_CTRL_ADDR_FLAG].iValue 
			= SLAVE3_EQP_NO_SEQUENCE_NUM;
	}

	//Repeat begin to determine Address By rectifier sig
	if((SLAVE1_RECT_DC_ON_OFF_CTRL <= iChannelNo) 
		&& (SLAVE2_RECT_DC_ON_OFF_CTRL > iChannelNo))
	{
		//11901---17001		slave1 module sig info
		g_SlaveSampData.aRoughDataSlave[0][SLAVE_CTRL_ADDR_FLAG].iValue 
			= SLAVE1_EQP_NO_SEQUENCE_NUM;
	}
	else if((SLAVE2_RECT_DC_ON_OFF_CTRL <= iChannelNo)
		&& (SLAVE3_RECT_DC_ON_OFF_CTRL > iChannelNo))
	{
		//17001---22101		slave2 module sig info
		g_SlaveSampData.aRoughDataSlave[0][SLAVE_CTRL_ADDR_FLAG].iValue 
			= SLAVE2_EQP_NO_SEQUENCE_NUM;
	}
	else if((SLAVE3_RECT_DC_ON_OFF_CTRL <= iChannelNo) 
		&& (SLAVE3_RECT_DC_ON_OFF_CTRL_END > iChannelNo))
	{
		//22101---27099		slave3	module sig info
		g_SlaveSampData.aRoughDataSlave[0][SLAVE_CTRL_ADDR_FLAG].iValue 
			= SLAVE3_EQP_NO_SEQUENCE_NUM;
	}//End determine ADDR 

	//2. Second, process the control sig info ---- CMD_ID. MODULE_ID
	if(iChannelNo		== SLAVE_CURRENT_LIMIT_CTRL 
		|| iChannelNo	== SLAVE_CURRENT_LIMIT_CTRL + MAX_CHN_NUM_PER_SLAVE
		||iChannelNo	== SLAVE_CURRENT_LIMIT_CTRL + MAX_CHN_NUM_PER_SLAVE * 2)
	{
		//flag
		g_SlaveSampData.abyCtrlBuf[SLAVE_COMMAND_ID].iValue = 0x2F;//CTRL_COMMD_FLAG
		g_SlaveSampData.abyCtrlBuf[SLAVE_MODULE_ID].iValue = SLAVE_INVALID_RECT_MODULEID;
	}
	else if(iChannelNo == SLAVE_DC_VOLT_CTRL
		|| iChannelNo == SLAVE_DC_VOLT_CTRL + MAX_CHN_NUM_PER_SLAVE
		||iChannelNo == SLAVE_DC_VOLT_CTRL + MAX_CHN_NUM_PER_SLAVE * 2)
	{
		//flag
		g_SlaveSampData.abyCtrlBuf[SLAVE_COMMAND_ID].iValue = 0x2d;
		g_SlaveSampData.abyCtrlBuf[SLAVE_MODULE_ID].iValue = SLAVE_INVALID_RECT_MODULEID;
	}
	else if(iChannelNo == SLAVE_ALL_DC_ON_OFF_CTRL
		|| iChannelNo == SLAVE_ALL_DC_ON_OFF_CTRL + MAX_CHN_NUM_PER_SLAVE
		||iChannelNo == SLAVE_ALL_DC_ON_OFF_CTRL + MAX_CHN_NUM_PER_SLAVE * 2)
	{
		//flag
		g_SlaveSampData.abyCtrlBuf[SLAVE_COMMAND_ID].iValue =0xea ;
		g_SlaveSampData.abyCtrlBuf[SLAVE_MODULE_ID].iValue = SLAVE_INVALID_RECT_MODULEID;
	}
	else if(iChannelNo == SLAVE_ALL_AC_ON_OFF_CTRL
		|| iChannelNo == SLAVE_ALL_AC_ON_OFF_CTRL + MAX_CHN_NUM_PER_SLAVE
		||iChannelNo == SLAVE_ALL_AC_ON_OFF_CTRL + MAX_CHN_NUM_PER_SLAVE * 2)
	{
		g_SlaveSampData.abyCtrlBuf[SLAVE_COMMAND_ID].iValue = 0xe5 ;
		g_SlaveSampData.abyCtrlBuf[SLAVE_MODULE_ID].iValue = SLAVE_INVALID_RECT_MODULEID;
	}
	else if(iChannelNo == SLAVE_ALL_LED_CTRL
		|| iChannelNo == SLAVE_ALL_LED_CTRL + MAX_CHN_NUM_PER_SLAVE
		||iChannelNo == SLAVE_ALL_LED_CTRL + MAX_CHN_NUM_PER_SLAVE * 2)
	{
		g_SlaveSampData.abyCtrlBuf[SLAVE_COMMAND_ID].iValue = 0xec ;
		g_SlaveSampData.abyCtrlBuf[SLAVE_MODULE_ID].iValue = SLAVE_INVALID_RECT_MODULEID;
	}
	else if (iChannelNo == SLAVE_RESET_LOST_ALARM
		|| iChannelNo == SLAVE_RESET_LOST_ALARM + MAX_CHN_NUM_PER_SLAVE
		||iChannelNo == SLAVE_RESET_LOST_ALARM + MAX_CHN_NUM_PER_SLAVE * 2)
	{
		g_SlaveSampData.abyCtrlBuf[SLAVE_COMMAND_ID].iValue = 0xed;
		g_SlaveSampData.abyCtrlBuf[SLAVE_MODULE_ID].iValue = SLAVE_INVALID_RECT_MODULEID;
	}
	else if (iChannelNo == SLAVE_RESET_OSCILLATED_ALARM
		|| iChannelNo == SLAVE_RESET_OSCILLATED_ALARM + MAX_CHN_NUM_PER_SLAVE
		||iChannelNo == SLAVE_RESET_OSCILLATED_ALARM + MAX_CHN_NUM_PER_SLAVE * 2)
	{
		g_SlaveSampData.abyCtrlBuf[SLAVE_COMMAND_ID].iValue = 0xef;
		g_SlaveSampData.abyCtrlBuf[SLAVE_MODULE_ID].iValue = SLAVE_INVALID_RECT_MODULEID;
	}
	else if (iChannelNo == SLAVE_SET_IN_AC_CURRENT_LIMIT
		|| iChannelNo == SLAVE_SET_IN_AC_CURRENT_LIMIT + MAX_CHN_NUM_PER_SLAVE
		||iChannelNo == SLAVE_SET_IN_AC_CURRENT_LIMIT + MAX_CHN_NUM_PER_SLAVE * 2)
	{
		g_SlaveSampData.abyCtrlBuf[SLAVE_COMMAND_ID].iValue = 0xe7;
		g_SlaveSampData.abyCtrlBuf[SLAVE_MODULE_ID].iValue = SLAVE_INVALID_RECT_MODULEID;
	}
	else if (iChannelNo		== SLAVE_CTL_E_STOP
		||iChannelNo	== SLAVE_CTL_E_STOP + MAX_CHN_NUM_PER_SLAVE
		||iChannelNo	== SLAVE_CTL_E_STOP + MAX_CHN_NUM_PER_SLAVE * 2)
	{
		//ADD 2010/2/1
		g_SlaveSampData.abyCtrlBuf[SLAVE_COMMAND_ID].iValue = 0xe8;
		g_SlaveSampData.abyCtrlBuf[SLAVE_MODULE_ID].iValue = SLAVE_INVALID_RECT_MODULEID;
	}
	//added By Jimmy 2013.07.19
	else if (iChannelNo		== SLAVE_CLS_COMM_INTERR
		||iChannelNo	== SLAVE_CLS_COMM_INTERR + MAX_CHN_NUM_PER_SLAVE
		||iChannelNo	== SLAVE_CLS_COMM_INTERR + MAX_CHN_NUM_PER_SLAVE * 2)
	{
		//ADD 2010/2/1
		g_SlaveSampData.abyCtrlBuf[SLAVE_COMMAND_ID].iValue = 0xe9;
		g_SlaveSampData.abyCtrlBuf[SLAVE_MODULE_ID].iValue = SLAVE_INVALID_RECT_MODULEID;
	}
	else if (iChannelNo		== SLAVE_FAN_SPEED
		||iChannelNo	== SLAVE_FAN_SPEED + MAX_CHN_NUM_PER_SLAVE
		||iChannelNo	== SLAVE_FAN_SPEED + MAX_CHN_NUM_PER_SLAVE * 2)
	{
		//ADD 2010/2/1
		g_SlaveSampData.abyCtrlBuf[SLAVE_COMMAND_ID].iValue = 0xeb;
		g_SlaveSampData.abyCtrlBuf[SLAVE_MODULE_ID].iValue = SLAVE_INVALID_RECT_MODULEID;
	}
	else if (iChannelNo		== SLAVE_CONFIRM_POSITION
		||iChannelNo	== SLAVE_CONFIRM_POSITION + MAX_CHN_NUM_PER_SLAVE
		||iChannelNo	== SLAVE_CONFIRM_POSITION + MAX_CHN_NUM_PER_SLAVE * 2)
	{
		//ADD 2010/2/1
		g_SlaveSampData.abyCtrlBuf[SLAVE_COMMAND_ID].iValue = 0xee;
		g_SlaveSampData.abyCtrlBuf[SLAVE_MODULE_ID].iValue = SLAVE_INVALID_RECT_MODULEID;
	}
	else if (iChannelNo		== SLAVE_RESET_POSITION
		||iChannelNo	== SLAVE_RESET_POSITION + MAX_CHN_NUM_PER_SLAVE
		||iChannelNo	== SLAVE_RESET_POSITION + MAX_CHN_NUM_PER_SLAVE * 2)
	{
		//ADD 2016/09/01
		g_SlaveSampData.abyCtrlBuf[SLAVE_COMMAND_ID].iValue = 0xf3;
		g_SlaveSampData.abyCtrlBuf[SLAVE_MODULE_ID].iValue = SLAVE_INVALID_RECT_MODULEID;
	}

	TAGSM_VALUE uTempValue;
	//Rectifier Sig Info
	if((iChannelNo >= SLAVE1_RECT_DC_ON_OFF_CTRL 
		&& iChannelNo < SLAVE1_RECT_PHASE + MAX_RECT_RECT_CHN_PER_SLAVE * 100)
		||(iChannelNo >= SLAVE2_RECT_DC_ON_OFF_CTRL 
		&& iChannelNo < SLAVE2_RECT_PHASE + MAX_RECT_RECT_CHN_PER_SLAVE * 100)
		||(iChannelNo >= SLAVE3_RECT_DC_ON_OFF_CTRL 
		&& iChannelNo < SLAVE3_RECT_PHASE + MAX_RECT_RECT_CHN_PER_SLAVE * 100))
	{
		//Get slave RECT module ctrl sig info ,include COMMAND_ID  MODULE_ID	
		GetCtrlInfoByChannelNo(iChannelNo, g_SlaveSampData.abyCtrlBuf);
		//if(g_SlaveSampData.abyCtrlBuf[SLAVE_COMMAND_ID].iValue == 0xf1)
		//{
		//	TRACE("INTO There!!!\n");
		//	uTempValue.fValue = fParam;
		//	g_SlaveSampData.aRoughDataSlave[g_SlaveSampData.aRoughDataSlave[0][SLAVE_CTRL_ADDR_FLAG].iValue]
		//	[SLAVE_RCT_RECT_ID + g_SlaveSampData.abyCtrlBuf[SLAVE_MODULE_ID].iValue * MAX_RECTIFIER_0XE3_SIG_NUM].ulValue = uTempValue.ulValue;
		//	g_SlaveSampData.aRoughDataSlave[g_SlaveSampData.aRoughDataSlave[0][SLAVE_CTRL_ADDR_FLAG].iValue][SLAVE_SAMPLING_E3_FLAG].iValue = TRUE;
		//	
		//}
		//else if(g_SlaveSampData.abyCtrlBuf[SLAVE_COMMAND_ID].iValue == 0xf2)
		//{
		//	uTempValue.fValue = fParam;
		//	g_SlaveSampData.aRoughDataSlave[g_SlaveSampData.aRoughDataSlave[0][SLAVE_CTRL_ADDR_FLAG].iValue]
		//	[SLAVE_RECT_PHASE + g_SlaveSampData.abyCtrlBuf[SLAVE_MODULE_ID].iValue * MAX_RECTIFIER_0XE1_SIG_NUM].ulValue = uTempValue.ulValue;

		//}


	}


	//3. get  SLAVE_BATERY_MANAGEMENT_STATE
	bBatteryManagementstate = GetEnumSigValue(RS485_BATTERY_GROUP_EQUIP_ID,
		SIG_TYPE_SAMPLING,
		RS485_BATTERY_MANAGEMENT_SIG_ID,
		"485_SAMP");

	g_SlaveSampData.abyCtrlBuf[SLAVE_BATERY_MANAGEMENT_STATE].iValue 
		= bBatteryManagementstate;

	//4. Ctrl Signal Value
	g_SlaveSampData.abyCtrlBuf[SLAVE_CTRL_SIG_INFO].fValue = fParam;


	iSlaveAddr = SLAVEGetAddrBySeqNum(g_SlaveSampData.aRoughDataSlave[0][SLAVE_CTRL_ADDR_FLAG].iValue);

	if (iSlaveAddr == -1)
	{
		return;//Get Addr error
	}

	INT32 istrlen = RS485_SLAVE_SENT_CTRL_DATAINFO_LEN;
	sprintf((char*)g_SlaveSampData.abySendBuf, 
		"~%02X%02X%02X%02X%04X\0",
		0x20,
		(unsigned int)iSlaveAddr,
		0xe5,
		0x45, 
		(unsigned int)istrlen);

	INT32 iLenChk = LenCheckSum((WORD)(istrlen)) << 4;
	g_SlaveSampData.abySendBuf[9] = HexToAsc((BYTE)((iLenChk & 0xf0)>>4));
	g_SlaveSampData.abySendBuf[10] = HexToAsc((BYTE)((istrlen >> 8) & 0x0f));
	g_SlaveSampData.abySendBuf[11] = HexToAsc((BYTE) ((istrlen & 0xf0)>>4));
	g_SlaveSampData.abySendBuf[12] = HexToAsc((BYTE)(istrlen & 0x0f));
	INT32 OffsetData = DATA_YDN23_SLAVE;

	sprintf((char*)g_SlaveSampData.abySendBuf + OffsetData,
		"%02X\0",
		g_SlaveSampData.abyCtrlBuf[SLAVE_COMMAND_ID].iValue);
	OffsetData += 2;		

	//2.Stuff  MODULE_ID
	sprintf((char*)g_SlaveSampData.abySendBuf + OffsetData,
		"%02X\0",
		g_SlaveSampData.abyCtrlBuf[SLAVE_MODULE_ID].iValue);
	OffsetData += 2;

	//3.Stuff  BATERYMANAGEMENTSTATE
	sprintf((char*)g_SlaveSampData.abySendBuf + OffsetData,
		"%02X\0",
		g_SlaveSampData.abyCtrlBuf[SLAVE_BATERY_MANAGEMENT_STATE].iValue);
	OffsetData += 2;

	//4.Stuff  SigInfo1
	GetCtrlValues.fValue = g_SlaveSampData.abyCtrlBuf[SLAVE_CTRL_SIG_INFO].fValue;
	for(j = 0; j < 4; j++)
	{
		//One (four hex)float convert to eight (ascii-hex)bytes
		sprintf((char*)g_SlaveSampData.abySendBuf + OffsetData +j * 2 ,
			"%02X\0",
			GetCtrlValues.cValueStr[j]);
	}
	OffsetData += 8;

	//5.Stuff SigInfo2 Float Charge Voltage in the StdEquip_BatteryGroup.cfg
	st_FloatChargeVoltage.fValue = GetFloatSigValue(RS485_BATTERY_GROUP_EQUIP_ID,
		2, 
		16,
		"485_SAMP");

	for(j = 0; j < 4; j++)
	{
		//One (four hex)float convert to eight (ascii-hex)bytes
		sprintf((char*)g_SlaveSampData.abySendBuf + OffsetData +j * 2 ,
			"%02X\0",
			st_FloatChargeVoltage.cValueStr[j]);
	}

	//Auto plus 4 byte And EOI
	CheckSum(g_SlaveSampData.abySendBuf);

	iSendDataLength = strlen(g_SlaveSampData.abySendBuf);

	Sleep(800);
	RS485_Write(SlaveDeviceClass->pCommPort->hCommPort, 
		g_SlaveSampData.abySendBuf, 
		iSendDataLength);

	Sleep(170);
}
/*=============================================================================*
* FUNCTION: SLAVE_InitRoughValue
* PURPOSE : initialization  all SigInfo in the RoughData
* INPUT:	 g_RS485Data   RS485_VALUE*	pRoughData
*     
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SLAVE_InitRoughValue(void * p)
{
	INT32 i, j;
	RS485_DEVICE_CLASS*  SlaveDeviceClass	=  p;
	SlaveDeviceClass->pRoughData			= &(g_SlaveSampData.aRoughDataSlave);
	SlaveDeviceClass->pfnSample				= SLAVE_Sample;
	SlaveDeviceClass->pfnStuffChn			= SLAVE_StuffChannel;
	SlaveDeviceClass->pfnSendCmd			= SLAVE_SendCtlCmd;

	for(i = SLAVE_ADDR_START; i <= SLAVE_ADDR_END; i++)
	{
		for(j = 0; j < SLAVE_MAX_SIGNALS_NUM; j++)
		{		
			g_SlaveSampData.aRoughDataSlave[i - SLAVE_ADDR_START][j].iValue 
				= SLAVE_SAMP_INVALID_VALUE;//-9999
		}

		g_SlaveSampData.aRoughDataSlave[i - SLAVE_ADDR_START][SLAVE_MODULE_NUMBER].iValue 
			= 0;//-9999
		g_SlaveSampData.aRoughDataSlave[i - SLAVE_ADDR_START][SLAVE_COMMUNICATION_STAT].iValue 
			= 0;
		g_SlaveSampData.SlaveCommInfo.iCmdCounter[i - SLAVE_ADDR_START] 
		= SLAVE_SAMP_FREQUENCY;//3
		g_SlaveSampData.aRoughDataSlave[i - SLAVE_ADDR_START][SLAVE_INTERRUPT_TIMES].iValue
			= 0;
		g_SlaveSampData.aRoughDataSlave[i - SLAVE_ADDR_START][SLAVE_COMM_FAILES_TIME].iValue 
			= 0;
		g_SlaveSampData.aRoughDataSlave[i - SLAVE_ADDR_START][SLAVE_EXIST_ST].iValue 
			= SLAVE_EQUIP_NOT_EXISTENT;
	}

	for(j = 0;j < RS485GROUP_MAX_SIGNALS_NUM;j++)
	{
		g_SlaveSampData.aRoughDataGroup[j].iValue = SLAVE_SAMP_INVALID_VALUE;
	}

	g_SlaveSampData.aRoughDataGroup[SLAVE_RS485_ACTUAL_NUM].iValue  = 0;
	g_SlaveSampData.aRoughDataGroup[SLAVE_RS485_GROUP_WOKING_STATE].iValue 
		= SLAVE_EQUIP_NOT_EXISTENT;
	SlaveDeviceClass->bNeedReconfig	= TRUE;
	SlaveDeviceClass->iReconfigTimes = 0;

	return 0 ;
}

static BYTE SLAVEAscToHex(char c)
{
	if (c >= '0' && c <= '9')
	{
		return c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		return c - 'A' + 10;
	}

	return 0xFF;
}

/*=============================================================================*
* FUNCTION: SLAVEMergeAsc()
* PURPOSE : assistant function, used to merge two ASCII chars
* INPUT:	 CHAR*	p
*     
*
* RETURN:
*     static BYTE
*
* CALLS: 
*     void
*
* CALLED BY: 
*    
*			 copy from SMAC        DATE: 2009-05-08		14:39
*============================================================================*/
static BYTE SLAVEMergeAsc(char *p)
{
	BYTE byHi, byLo;

	byHi = SLAVEAscToHex((char)(p[0]));
	byLo = SLAVEAscToHex((char)(p[1]));

	//error data
	if (byHi == 0xFF || byLo == 0xFF)
	{
		return 0;
	}

	return (byHi*16 + byLo);
}

/*=============================================================================*
* FUNCTION: RS485WaitReadable
* PURPOSE : wait RS485 data ready
* INPUT: 
*     
*
* RETURN:
*     int : 1: data ready, 0: timeout,Allow get mode or auto config
*		
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*			Ilockteng		2009-05-19
*============================================================================*/
LOCAL INT32 RS485WaitReadable(INT32 fd, INT32 nmsTimeOut)
{
	fd_set readfd;
	struct timeval timeout;

	while (nmsTimeOut > 0)
	{
		if (nmsTimeOut < (5*1000))
		{
			timeout.tv_usec = nmsTimeOut%1000*1000;		/* usec     */
			timeout.tv_sec  = nmsTimeOut/1000;			/* seconds  */

			nmsTimeOut = 0;
		}
		else
		{
			timeout.tv_usec = 0;						/* usec     */
			timeout.tv_sec  = 5;						/* seconds  */

			RUN_THREAD_HEARTBEAT();

			nmsTimeOut -= (5*1000);
		}

	}


	FD_ZERO(&readfd);									/*  Initializes the set to the NULL set. */
	FD_SET(fd, &readfd);								/*  Adds descriptor s to set. */

	select(fd+1, &readfd, NULL, NULL, &timeout);

	if(FD_ISSET(fd, &readfd))	
	{
		//1: data ready, can be read
		return TRUE;									
	}
	else
	{
		//0: Data not ready, can't read!!but,Allow get mode or auto config
		return FALSE;
	}
}

/*=============================================================================*
* FUNCTION: SLAVE_Getver
* PURPOSE : Get version for scan 
* INPUT: 
*    
*
* RETURN:	SLAVE_SAMPLE_OK   SLAVE_SAMPLE_FAIL
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 SLAVE_Getver(HANDLE hComm, INT32 nUnitNo, BYTE CID1)
{
	INT32	 istrlen;
	INT32	 iReceiveNum;
	RS485_DRV *pPort = (RS485_DRV *)hComm;
	INT32	  fd = pPort->fdSerial;

	memset(g_SlaveSampData.abySendBuf,'\0',MAX_SENDMASTERFRAMES_IN_BUFF);
	ClearBuffer(g_SlaveSampData.abyRcvBuf, MAX_RECVMASTERFRAMES_IN_BUFF);

	//1	  1	  1	  1	   1        2	   X	2   1
	//1	  2	  2	  2	   2	    4	   2X	4   1
	//SOI VER ADR CID1 CID2/RTN	LENGTH INFO	SUM	EOI
	sprintf( (char*)g_SlaveSampData.abySendBuf,
		"~%02X%02X%02X%02X%04X\0",
		0x20,
		(unsigned int)nUnitNo, 
		(unsigned int)CID1, 
		0x4F, 
		0 );

	CheckSum( g_SlaveSampData.abySendBuf);

	istrlen = strlen(g_SlaveSampData.abySendBuf);

	*(g_SlaveSampData.abySendBuf + 17) = 0x0D;

	RS485_ClrBuffer(hComm);

	ClearBuffer(g_SlaveSampData.abyRcvBuf, MAX_RECVMASTERFRAMES_IN_BUFF);

	RS485_Write(hComm,
		g_SlaveSampData.abySendBuf,
		18);

	if (RS485WaitReadable(fd, 800) > 0 )
	{
		//Receive data from slave RecvDataFromSLAVE
		if(!RecvDataFromSLAVE(hComm, 
			g_SlaveSampData.abyRcvBuf, 
			&iReceiveNum,
			512))
		{
			g_SlaveSampData.aRoughDataSlave[nUnitNo - SLAVE_ADDR_START][SLAVE_COMMUNICATION_STAT].iValue
				= MASTERSLAVE_COMM_FAIL;		
			g_SlaveSampData.aRoughDataSlave[nUnitNo - SLAVE_ADDR_START][SLAVE_EXIST_ST].iValue 
				= SLAVE_EQUIP_NOT_EXISTENT;
			return SLAVE_SAMPLE_FAIL;	
		}
		else
		{	
			if(SLAVEMergeAsc(g_SlaveSampData.abyRcvBuf + ADDR_YDN23_SLAVE) == nUnitNo)
			{
				//There have equipment with  the nUnitNo,but,not include that failed to communication.
				g_SlaveSampData.aRoughDataSlave[nUnitNo - SLAVE_ADDR_START][SLAVE_EXIST_ST].iValue
					= SLAVE_EQUIP_EXISTENT;
				ClearBuffer(g_SlaveSampData.abySendBuf,MAX_SENDMASTERFRAMES_IN_BUFF);
				//ok normal result
				return SLAVE_SAMPLE_OK;
			}
			else
			{
				g_SlaveSampData.aRoughDataSlave[nUnitNo - SLAVE_ADDR_START][SLAVE_COMMUNICATION_STAT].iValue
					= MASTERSLAVE_COMM_FAIL;
				ClearBuffer(g_SlaveSampData.abyRcvBuf, MAX_RECVMASTERFRAMES_IN_BUFF);
				return SLAVE_SAMPLE_FAIL;
			}
		}
	}
	else
	{
		TRACE("\n $$$$$$$  This slave  RS485WaitReadable  IN  SLAVE Getver !!!\n");
		return SLAVE_SAMPLE_FAIL;
	}

}

/*=============================================================================*
* FUNCTION: SLAVE_Reconfig
* PURPOSE : scan slave equip ,record equip info 
* INPUT:	g_RS485Data
*   
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SLAVE_Reconfig(void * p)
{
	TRACE("\n into SLAVE_Reconfig \n");
	INT32				i;
	INT32				iAddr;
	SIG_ENUM			emRst;
	INT32				iSlaveNum		  = 0;
	RS485_DEVICE_CLASS*  SlaveDeviceClass = p;
	iSlaveNum							  = 0;
	static			int iRetryTimes = 0;


	for(iAddr = SLAVE_ADDR_START; iAddr <= SLAVE_ADDR_END; iAddr++)
	{
		for(i = 0; i < 5; i++)
		{
			emRst = SLAVE_Getver(SlaveDeviceClass->pCommPort->hCommPort,
				iAddr,
				(BYTE)0xe5);

			if(SLAVE_SAMPLE_OK == emRst)
			{
				TRACE("\n  SLAVE  Exist  SLAVE_ Reconfig  iAddr = %d \n",(int)iAddr);
				break;
			}
		}

		//TRACE("\n  SLAVE   SCAN  SCAN  iAddr = %d \n",iAddr);

		//0: SLAVE_SAMPLE_OK	 -1: SLAVE_SAMPLE_FAIL
		if(SLAVE_SAMPLE_OK == emRst)
		{
			//0  1  2
			g_SlaveSampData.aRoughDataSlave[iAddr - SLAVE_ADDR_START][SLAVE_SEQ_NO].iValue 
				= iSlaveNum;
			g_SlaveSampData.aRoughDataSlave[iAddr - SLAVE_ADDR_START][SLAVE_EXIST_ST].iValue 
				= SLAVE_EQUIP_EXISTENT;

			iSlaveNum++;//1,2,3
		}
		else
		{
			g_SlaveSampData.aRoughDataSlave[iAddr - SLAVE_ADDR_START][SLAVE_EXIST_ST].iValue 
				= SLAVE_EQUIP_NOT_EXISTENT;
		}

		if(iSlaveNum > MAX_NUM_SLAVE)
		{
			break;
		}
	}

	//Save slave actual  number
	g_SlaveSampData.aRoughDataGroup[SLAVE_RS485_ACTUAL_NUM].iValue = iSlaveNum;
	//TRACE("\n****   SLAVE_RS485_ACTUAL_NUM   = %d  ***\n",iSlaveNum);

	if(iSlaveNum > 0)
	{
		if(iSlaveNum == 3 || iRetryTimes >= 3)//All have been scanned
		{
			g_SlaveSampData.aRoughDataGroup[SLAVE_RS485_GROUP_WOKING_STATE].iValue 
				= SLAVE_EQUIP_EXISTENT;
			SlaveDeviceClass->iReconfigTimes = MAX_SLAVE_RETRY_TIMES;
		}
		else if(iRetryTimes < 3)
		{
			g_SlaveSampData.aRoughDataGroup[SLAVE_RS485_GROUP_WOKING_STATE].iValue 
				= SLAVE_EQUIP_NOT_EXISTENT;
			iRetryTimes++;
		}

		//if(iRetryTimes == 3 && iSlaveNum != 3)
		//{
		//	AppLogOut("Slave ResponeData", 
		//		APP_LOG_UNUSED,
		//		"Slave Number %d,Slave1 state is %d,Slave2 state is %d,Slave3 state is %d,\n",
		//		iSlaveNum, g_SlaveSampData.aRoughDataSlave[0][SLAVE_EXIST_ST].iValue,g_SlaveSampData.aRoughDataSlave[1][SLAVE_EXIST_ST].iValue,g_SlaveSampData.aRoughDataSlave[2][SLAVE_EXIST_ST].iValue);
		//	
		//	printf("Slave Number %d, Slave1 state is %d,Slave2 state is %d,Slave3 state is %d,\n",iSlaveNum, g_SlaveSampData.aRoughDataSlave[0][SLAVE_EXIST_ST].iValue,g_SlaveSampData.aRoughDataSlave[1][SLAVE_EXIST_ST].iValue,g_SlaveSampData.aRoughDataSlave[2][SLAVE_EXIST_ST].iValue);
		//}

	}
	else
	{
		g_SlaveSampData.aRoughDataGroup[SLAVE_RS485_GROUP_WOKING_STATE].iValue 
			= SLAVE_EQUIP_NOT_EXISTENT;
	}

	TRACE("\n Out SLAVE_Reconfig \n");

	return 0;
}

/*=============================================================================*
* FUNCTION: StrToUlong
* PURPOSE : eight byte convert to Ulong
* INPUT: 
*     
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*		kenn.wan                 DATE: 2008-08-15 14:55
*============================================================================*/
static ULONG StrToUlong( char* sStr )
{
	INT32					i;
	SLAVESTRTOULONG			Ulongvalue;
	unsigned  char			cTemp;
	unsigned  char			cTempHi;
	unsigned  char			cTempLo;

	for( i = 0; i < 4; i++ )
	{
#ifdef _CPU_BIG_ENDIAN		// ppc
#ifdef _SAMPLER_REPLACE_SSCANF
		//TRACE("------------------------1----------------------------\n");
		sscanf((const char *)sStr+i, "%x", 
			&floatvalue.by_value[3-i]);
#else
		int	c;
		//TRACE("------------------------2----------------------------\n");
		sscanf((const char *)sStr+i, "%x", 	&c);
		Ulongvalue.by_value[3-i] = (BYTE)c;
#endif
#else	// for x86
		//hi bit
		if((*(sStr + i * 2) > 0x40))
		{
			cTempHi = (*(sStr + i * 2) - 0x40) + 9;
		}
		else
		{
			cTempHi = (*(sStr + i * 2) - 0x30);
		}

		//low bit
		if((*(sStr + i * 2 + 1)) > 0x40)
		{
			cTempLo = (*(sStr + i * 2 + 1) - 0x40) + 9;
		}
		else
		{
			cTempLo = (*(sStr + i * 2 + 1) - 0x30);
		}

		cTemp = cTempHi * 16 + cTempLo;
		Ulongvalue.by_value[i] = cTemp;

#endif
	}

	return Ulongvalue.ul_value;
}


/*=============================================================================*
* FUNCTION: FixFloatDat
* PURPOSE : eight byte convert to float
* INPUT: 
*     
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*		kenn.wan                 DATE: 2008-08-15 14:55
*============================================================================*/
static float FixFloatDat( char* sStr )
{
	INT32					i;
	SLAVESTRTOFLOAT			floatvalue;
	CHAR					cTemp;
	CHAR					cTempHi;
	CHAR					cTempLo;

	for( i = 0; i < 4; i++ )
	{
#ifdef _CPU_BIG_ENDIAN		// ppc
#ifdef _SAMPLER_REPLACE_SSCANF
		//TRACE("------------------------1----------------------------\n");
		sscanf((const char *)sStr+i, "%x", 
			&floatvalue.by_value[3-i]);
#else
		int	c;
		//TRACE("------------------------2----------------------------\n");
		sscanf((const char *)sStr+i, "%x", 	&c);
		floatvalue.by_value[3-i] = (BYTE)c;
#endif
#else	// for x86
		//hi bit
		if((*(sStr + i * 2) > 0x40))
		{
			cTempHi = (*(sStr + i * 2) - 0x40) + 9;
		}
		else
		{
			cTempHi = (*(sStr + i * 2) - 0x30);
		}

		//low bit
		if((*(sStr + i * 2 + 1)) > 0x40)
		{
			cTempLo = (*(sStr + i * 2 + 1) - 0x40) + 9;
		}
		else
		{
			cTempLo = (*(sStr + i * 2 + 1) - 0x30);
		}

		cTemp = cTempHi * 16 + cTempLo;
		floatvalue.by_value[i] = cTemp;

#endif
	}

	return floatvalue.f_value;
}

/*=============================================================================*
* FUNCTION: Stuff_YDN23SendCmd
* PURPOSE : stuff control command to sending buffer
* INPUT: 
*  
*
* RETURN:
*			BYTE *pcSendData
*			g_SlaveSampData.abySendBuf
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*		pcSendData == NULL means send Data from gc by Dxiget and private cfg
*============================================================================*/
LOCAL INT32 Stuff_YDN23SendCmd(INT32 nUnitNo,
			       BYTE CID1,
			       BYTE CID2,
			       BYTE *pcSendData,
			       INT32 iSlaveEquipNoSequenceNum)
{
	UNUSED(pcSendData);
	UNUSED(iSlaveEquipNoSequenceNum);
	INT32							j;	
	INT32							istrlen;
	INT32							iLenChk;
	INT32							OffsetData;
	//Temporary variable for convert format.
	//TAGSM_VALUE						GetCtrlValues;
	TAGSM_VALUE						st_FloatChargeVoltage;
	INT32							iRowCtrlSignal;
	//Slave_Globals
	extern SLAVE_GLOBALS_REPORT		g_Slave_Report;
	RS485_REPORT_CFG_INFO			*pReportConfig;
	RS485_SLAVE_REPORT_SIG_INFO		*pCtrlSigInfo;
	pReportConfig = &g_Slave_Report.Rs485_Slave_Cfg;
	//Point to one row ctrl signal in the slave_report.cfg
	pCtrlSigInfo = pReportConfig->pCtrlSigMapInfo;
	//[CTRL_DATA_MAP_NUM] section row number, number of ctrl signal.
	iRowCtrlSignal = pReportConfig->iCtrlSigMapNum;//unused

	istrlen = 10;//RS485_SLAVE_SENT_CTRL_DATAINFO_LEN  ::BATERYMANAGEMENTSTATE   st_FloatChargeVoltage

	sprintf((char*)g_SlaveSampData.abySendBuf, 
		"~%02X%02X%02X%02X%04X\0",
		0x20,
		(unsigned int)nUnitNo,
		(unsigned int)CID1,
		(unsigned int)CID2, 
		(unsigned int)istrlen);

	iLenChk = LenCheckSum(istrlen) << 4;
	g_SlaveSampData.abySendBuf[9] = HexToAsc((iLenChk & 0xf0)>>4);
	g_SlaveSampData.abySendBuf[10] = HexToAsc(((istrlen) >> 8) & 0x0f);
	g_SlaveSampData.abySendBuf[11] = HexToAsc((istrlen & 0xf0)>>4);
	g_SlaveSampData.abySendBuf[12] = HexToAsc(istrlen & 0x0f);
	OffsetData = DATA_YDN23_SLAVE;

	//3.Stuff  BATERYMANAGEMENTSTATE
	g_SlaveSampData.abyCtrlBuf[SLAVE_BATERY_MANAGEMENT_STATE].iValue
		= GetEnumSigValue(RS485_BATTERY_GROUP_EQUIP_ID,
		SIG_TYPE_SAMPLING,
		RS485_BATTERY_MANAGEMENT_SIG_ID,
		"485_SAMP");

	sprintf((char*)g_SlaveSampData.abySendBuf + OffsetData,
		"%02X\0",
		g_SlaveSampData.abyCtrlBuf[SLAVE_BATERY_MANAGEMENT_STATE].iValue);
	OffsetData += 2;

	////4.Stuff  SigInfo1
	//GetCtrlValues.fValue = g_SlaveSampData.abyCtrlBuf[SLAVE_CTRL_SIG_INFO].fValue;
	//for(j = 0; j < 4; j++)
	//{
	//	//One (four hex)float convert to eight (ascii-hex)bytes
	//	sprintf((char*)g_SlaveSampData.abySendBuf + OffsetData +j * 2 ,
	//			"%02X\0",
	//			GetCtrlValues.cValueStr[j]);
	//}
	//OffsetData += 8;

	//5.Stuff SigInfo2 Float Charge Voltage in the StdEquip_BatteryGroup.cfg
	st_FloatChargeVoltage.fValue = GetFloatSigValue(RS485_BATTERY_GROUP_EQUIP_ID,2,16,"485_SAMP");
	for(j = 0; j < 4; j++)
	{
		//One (four hex)float convert to eight (ascii-hex)bytes
		sprintf((char*)g_SlaveSampData.abySendBuf + OffsetData +j * 2 ,
			"%02X\0",
			st_FloatChargeVoltage.cValueStr[j]);
	}

	//Auto plus 4 byte And EOI
	CheckSum(g_SlaveSampData.abySendBuf);	

	return 0;
}
/*=============================================================================*
* FUNCTION: SLAVE_UnpackCmdE1
* PURPOSE : arrange the receive data to RoughData
* INPUT: 
*  
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 SLAVE_UnpackCmdE1(INT32 iAddr, BYTE* pbyRcvBuf)
{
	INT32							i,j,iSysGroupSignalNum;
	INT32							iRectifierSigNum;
	INT32							iRectifierModuleNum;
	//Temporary variable for convert format.
	TAGSM_VALUE						TempAnologlValues;
	CHAR							*pc_Recdata;
	INT32							OffsetData;
	extern SLAVE_GLOBALS_REPORT		g_Slave_Report;//Slave_Globals
	RS485_REPORT_CFG_INFO*			pReportConfig;
	pReportConfig					= &g_Slave_Report.Rs485_Slave_Cfg;//slave_report.cfg
	RS485_SLAVE_REPORT_SIG_INFO*	pAnologSigInfo;//[SYS_GROUP_DATA_MAP_INFO]
	pAnologSigInfo					= pReportConfig->pAnologSigMapInfo;
	iSysGroupSignalNum				=  pReportConfig->iAnologSigMapNum;
	RS485_SLAVE_REPORT_SIG_INFO*	pRectSigInfo;	//[RECT_SINGLE_MAP_INFO]
	pRectSigInfo					= pReportConfig->pRectSigMapInfo;
	iRectifierSigNum				= pReportConfig->iRectSigMapNum;//unused
	//pc_Recdata point to INFO section of receive  the YDN23 data.
	OffsetData = DATA_YDN23_SLAVE;

	//1:[SYS_GROUP_DATA_MAP_INFO]			It is group  signal of slave 
	for(i = 0; i < iSysGroupSignalNum; i++, pAnologSigInfo++)
	{
		if(pAnologSigInfo->ByteNum_Of_Sig == 1)
		{
			pc_Recdata					= pbyRcvBuf + OffsetData;
			TempAnologlValues.cValue	= SLAVEMergeAsc(pc_Recdata);
			g_SlaveSampData.aRoughDataSlave[iAddr - SLAVE_ADDR_START][i].iValue
				= TempAnologlValues.cValue;
			OffsetData += 2;
		}
		else if(pAnologSigInfo->ByteNum_Of_Sig == 4)
		{
			pc_Recdata = pbyRcvBuf + OffsetData;
			TempAnologlValues.fValue = FixFloatDat(pc_Recdata);
			g_SlaveSampData.aRoughDataSlave[iAddr - SLAVE_ADDR_START][i].fValue
				= TempAnologlValues.fValue;

			OffsetData += 8;
		}
	}

	//2:[RECT_SINGLE_MAP_INFO]				It is rectifier signal of slave 
	iRectifierModuleNum = g_SlaveSampData.aRoughDataSlave
		[iAddr - SLAVE_ADDR_START][SLAVE_MODULE_NUMBER].iValue;

	if (iRectifierModuleNum > MAX_RECTIFIER_NUM)
	{
		iRectifierModuleNum = MAX_RECTIFIER_NUM;
	}

	//Arrange every modules by 0xe1 command sig info,must refer to  slave_report.cfg!!
	for(i = 0; i < iRectifierModuleNum; i++)
	{		
		j = 0;
		//SECTION	  FLAG		NA		NA				NA		0
		//point to  current limit point of [RECT_SINGLE_MAP_INFO]
		for(pRectSigInfo = pReportConfig->pRectSigMapInfo;
			((pRectSigInfo->ByteNum_Of_Sig != 0) && (pRectSigInfo->Sig_Id != '\0'));
			pRectSigInfo++)
		{
			if(pRectSigInfo->ByteNum_Of_Sig == 1)
			{
				pc_Recdata = pbyRcvBuf + OffsetData;
				TempAnologlValues.cValue = SLAVEMergeAsc(pc_Recdata);

				g_SlaveSampData.aRoughDataSlave[iAddr - SLAVE_ADDR_START]
				[SLAVE_RCT_CURRENT_LIMIT_POINT + i * MAX_RECTIFIER_0XE1_SIG_NUM + j].iValue 
					= TempAnologlValues.cValue;
				OffsetData += 2;
			}
			else if(pRectSigInfo->ByteNum_Of_Sig == 4)
			{
				pc_Recdata = pbyRcvBuf + OffsetData;

				if ((pRectSigInfo->Sig_Id == RECT_SN)
					||(pRectSigInfo->Sig_Id == RSRECT_TOTAL_RUN_TIME)
					||(pRectSigInfo->Sig_Id == RECT_HIGH_SN)
					||(pRectSigInfo->Sig_Id == RECT_VERSION)
					||(pRectSigInfo->Sig_Id == RECT_BARCODE_1)
					||(pRectSigInfo->Sig_Id == RECT_BARCODE_2)
					||(pRectSigInfo->Sig_Id == RECT_BARCODE_3)
					||(pRectSigInfo->Sig_Id == RECT_BARCODE_4)
					)
				{
					TempAnologlValues.ulValue = StrToUlong(pc_Recdata);
					g_SlaveSampData.aRoughDataSlave[iAddr - SLAVE_ADDR_START]
					[SLAVE_RCT_CURRENT_LIMIT_POINT + i * MAX_RECTIFIER_0XE1_SIG_NUM + j].ulValue
						=TempAnologlValues.ulValue;
					OffsetData += 8;
				}
				else if ((pRectSigInfo->Sig_Type == 0 && pRectSigInfo->Sig_Id == 4) ||
					(pRectSigInfo->Sig_Type == 0 && pRectSigInfo->Sig_Id == 6))
				{
					UCHAR Val_H = SLAVEMergeAsc(pc_Recdata);
					UCHAR Val_L = SLAVEMergeAsc(pc_Recdata + 2);
					g_SlaveSampData.aRoughDataSlave[iAddr - SLAVE_ADDR_START]
					[SLAVE_RCT_CURRENT_LIMIT_POINT + i * MAX_RECTIFIER_0XE1_SIG_NUM + j].fValue
						= ((Val_H << 8) + Val_L)/10.0;
					OffsetData += 4;
				}
				else
				{
					TempAnologlValues.fValue = FixFloatDat(pc_Recdata);
					g_SlaveSampData.aRoughDataSlave[iAddr - SLAVE_ADDR_START]
					[SLAVE_RCT_CURRENT_LIMIT_POINT + i * MAX_RECTIFIER_0XE1_SIG_NUM + j].fValue 
						= TempAnologlValues.fValue;
					OffsetData += 8;
				}
			}

			j++;//IN the RoughData  save next signal 
		}
	}

	if (g_SlaveSampData.aRoughDataSlave
		[iAddr - SLAVE_ADDR_START][SLAVE_MAINS_FAILURE].iValue == TRUE)
	{
		//�������е�ģ�齻��ͣ��
		for(i = 0; i < iRectifierModuleNum; i++)
		{
			g_SlaveSampData.aRoughDataSlave[iAddr - SLAVE_ADDR_START]
			[i * MAX_RECTIFIER_0XE1_SIG_NUM + SLAVE_RECT_AC_FAILURE].iValue
				= 0;
		}
	}

	return 0;
}
/*=============================================================================*
* FUNCTION: SLAVE_UnpackCmdE3
* PURPOSE : arrange the receive data to RoughData
* INPUT: 
*  
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 SLAVE_UnpackCmdE3(INT32 iAddr,BYTE* pbyRcvBuf)
{
	INT32							i,j;
	INT32							iRectifierSigNum;
	INT32							iRectifier0XE3SigNum;
	INT32							iRectifier0XE1SigNum;
	INT32							iRectifierModuleNum;
	//Temporary variable for convert format.
	TAGSM_VALUE						TempAnologlValues;
	CHAR*							pc_Recdata;
	INT32							OffsetData;
	extern SLAVE_GLOBALS_REPORT		g_Slave_Report;						//Slave_Globals
	RS485_REPORT_CFG_INFO*			pReportConfig;
	pReportConfig					= &g_Slave_Report.Rs485_Slave_Cfg;	//slave_report.cfg
	RS485_SLAVE_REPORT_SIG_INFO*	pRectSigInfo;						//[RECT_SINGLE_MAP_INFO]
	pRectSigInfo					= pReportConfig->pRectSigMapInfo;
	iRectifierSigNum				= pReportConfig->iRectSigMapNum;	
	iRectifier0XE1SigNum			= 0;
	//Control stuff rough data by sampling 0xe3
	g_SlaveSampData.aRoughDataSlave[iAddr - SLAVE_ADDR_START][SLAVE_SAMPLING_E3_FLAG].iValue 
		= TRUE;
	//pc_Recdata point to INFO section of receive  the YDN23 data.
	OffsetData						= DATA_YDN23_SLAVE;

	//find the place  point to Barcode1 in the  [RECT_SINGLE_MAP_INFO] by SECTION FLAG	
	for(;;)
	{
		if((pRectSigInfo->ByteNum_Of_Sig == 0) && (pRectSigInfo->Sig_Id == '\0')) //SECTION	  FLAG
		{
			//point to Barcode1 of [RECT_SINGLE_MAP_INFO]  in the slave_report.cfg
			pRectSigInfo++;
			iRectifier0XE1SigNum++;
			break;
		}
		pRectSigInfo++;
		iRectifier0XE1SigNum++;
	}

	RS485_SLAVE_REPORT_SIG_INFO* pTempRectSigInfo;
	//Samping by 0xe3 Signal number :please refer to slave_report.cfg
	iRectifier0XE3SigNum = iRectifierSigNum - iRectifier0XE1SigNum;
	//Rectifier modules  number
	iRectifierModuleNum = g_SlaveSampData.aRoughDataSlave
		[iAddr - SLAVE_ADDR_START][SLAVE_MODULE_NUMBER].iValue;

	if (iRectifierModuleNum > MAX_RECTIFIER_NUM)
	{
		iRectifierModuleNum = MAX_RECTIFIER_NUM;
	}

	//Every modules Arrange Sampling sig info by 0xe3 command ,must refer to  slave_report.cfg!!
	for(i = 0;i < iRectifierModuleNum; i++)
	{
		Sleep(2);
		//pRectSigInfo = pSaveRectSigInfo;
		//TRACE("\n Rectifier Number:%d", i);
		for(j = 0, pTempRectSigInfo = pRectSigInfo; 
			j < iRectifier0XE3SigNum; 
			pTempRectSigInfo++, j++)
		{
			//TRACE("\n Sig Number j = %d",j);
			if(pTempRectSigInfo->ByteNum_Of_Sig == 1)
			{
				pc_Recdata = pbyRcvBuf + OffsetData;
				TempAnologlValues.cValue = SLAVEMergeAsc(pc_Recdata);
				//MAX_RECTIFIER_0XE3_SIG_NUM  note����������
				g_SlaveSampData.aRoughDataSlave[iAddr - SLAVE_ADDR_START]
				[SLAVE_RCT_BARCODE_1 + i * MAX_RECTIFIER_0XE3_SIG_NUM + j].iValue 
					= TempAnologlValues.cValue;

				/*if (i >= 56)
				{
				TRACE(" RCT[%d]Sig Number j = %d ID=%d cVal=%d\n",i,j,pTempRectSigInfo->Sig_Id,TempAnologlValues.cValue);
				}*/

				OffsetData += 2;
			}
			else if(pTempRectSigInfo->ByteNum_Of_Sig == 4)
			{
				pc_Recdata = pbyRcvBuf + OffsetData;

				if ((0 == pTempRectSigInfo->Sig_Type) && (RECT_RATED_CURRENT == pTempRectSigInfo->Sig_Id))
				{
					UCHAR Val_H = SLAVEMergeAsc(pc_Recdata);
					UCHAR Val_L = SLAVEMergeAsc(pc_Recdata + 2);
					g_SlaveSampData.aRoughDataSlave[iAddr - SLAVE_ADDR_START]
					[SLAVE_RCT_BARCODE_1 + i * MAX_RECTIFIER_0XE3_SIG_NUM + j].fValue 
						= ((Val_H << 8) + Val_L)/10.0;

					/*if (i >= 56)
					{
					TRACE(" RCT[%d]Sig Number j = %d ID=%d fVal=%f\n",i,j,pTempRectSigInfo->Sig_Id,g_SlaveSampData.aRoughDataSlave[iAddr - SLAVE_ADDR_START][SLAVE_RCT_BARCODE_1 + i * MAX_RECTIFIER_0XE3_SIG_NUM + j].fValue);
					}*/

					OffsetData += 4;
					continue;
				}

				if ((pTempRectSigInfo->Sig_Id == RECT_SN)
					||(pTempRectSigInfo->Sig_Id == RSRECT_TOTAL_RUN_TIME)
					||(pTempRectSigInfo->Sig_Id == RECT_HIGH_SN)
					||(pTempRectSigInfo->Sig_Id == RECT_VERSION)
					||(pTempRectSigInfo->Sig_Id == RECT_BARCODE_1)
					||(pTempRectSigInfo->Sig_Id == RECT_BARCODE_2)
					||(pTempRectSigInfo->Sig_Id == RECT_BARCODE_3)
					||(pTempRectSigInfo->Sig_Id == RECT_BARCODE_4)
					)
				{
					TempAnologlValues.ulValue =	StrToUlong(pc_Recdata);

					g_SlaveSampData.aRoughDataSlave[iAddr - SLAVE_ADDR_START]
					[SLAVE_RCT_BARCODE_1 + i * MAX_RECTIFIER_0XE3_SIG_NUM + j].ulValue 
						= TempAnologlValues.ulValue;
					//if (i >= 56)
					//{
					//	TRACE(" RCT[%d]Sig Number j = %d ID=%d ulVal=%f\n",i,j,pTempRectSigInfo->Sig_Id,TempAnologlValues.ulValue);
					//}

				}
				else
				{
					TempAnologlValues.fValue = FixFloatDat(pc_Recdata);
					g_SlaveSampData.aRoughDataSlave[iAddr - SLAVE_ADDR_START]
					[SLAVE_RCT_BARCODE_1 + i * MAX_RECTIFIER_0XE3_SIG_NUM + j].fValue 
						= TempAnologlValues.fValue;
					/*if (i >= 56)
					{
					TRACE(" RCT[%d]Sig Number j = %d ID=%d fVal=%f\n",i,j,pTempRectSigInfo->Sig_Id,TempAnologlValues.fValue);
					}*/

				}

				OffsetData += 8;
			}
		}
	}

	return 0;
}
/*=============================================================================*
* FUNCTION: RecvDataFromSLAVE()
* PURPOSE  :Receive data of SM from 485 com
* RETURN   : int : byte number ,but if return -1  means  error
* ARGUMENTS:
*						CHAR*	sRecStr :		
*								hComm	 :	485 com handle 
*								iStrLen	 :
* CALLS    : 
* CALLED BY: 
*								DLLExport BOOL Query()
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
LOCAL INT32	RecvDataFromSLAVE(HANDLE hComm, CHAR* sRecStr,  INT32 *piStrLen, INT32 iWantToReadnByte)
{
	INT32	iReadLen;		
	INT32	i		= 0;
	INT32	j		= 0;
	INT32	iHead	= -1;
	INT32	iTail	= -1;

	ASSERT(hComm != 0 );
	ASSERT(sRecStr != NULL);

	//Read the data from Smac
	iReadLen = RS485_Read(hComm, (BYTE*)sRecStr, iWantToReadnByte);

	for (i = 0; i < iReadLen; i++)
	{
		if(bySOI == *(sRecStr + i))
		{
			iHead = i;
			break;
		}
	}

	if (iHead < 0)	//   no head
	{
		TRACE(" SLV RRR (iHead < 0) no head iReadLen =%d %s \n",(int)iReadLen,sRecStr);
		return FALSE;
	}

	for (i = iHead + 1; i < iReadLen; i++)
	{
		if(byEOI == *(sRecStr + i))
		{
			iTail = i;
			break;
		}
	}		

	if (iTail < 0)	//   no tail
	{
		TRACE(" SLV RRR (iTail < 0) no tail iReadLen =%d %s \n",(int)iReadLen,sRecStr);
		return FALSE;
	}	

	*piStrLen = iTail - iHead + 1;

	if(iHead > 0)
	{
		for (j = iHead; j < *piStrLen; j++)
		{
			*(sRecStr + (j - iHead)) = *(sRecStr + j);
		}
	}

	*(sRecStr + (iTail - iHead + 2)) = '\0';
	return TRUE;

}

/*=============================================================================*
* FUNCTION: SMACGetResponeData
* PURPOSE : send and receive data info
* INPUT: 
*     
*
* RETURN:
*     INT32   0:  succeed	1:no response working  2:fail to communication 
*			  -1:  function error
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*		Ilock Teng
*============================================================================*/
LOCAL INT32 SLAVE_SendAndRead(
			      HANDLE	hComm,			 
			      BYTE		*sSendStr,		
			      BYTE		*abyRcvBuf,      
			      INT32	nSend,
			      BYTE     cWaitTime,
			      INT32	iWantToReadnByte)					
{    
	UNUSED(cWaitTime);
	INT32		iReceiveNum;
	RS485_DRV *pPort = (RS485_DRV *)hComm;
	INT32	fd = pPort->fdSerial;
	ASSERT( hComm !=0 && sSendStr != NULL && abyRcvBuf != NULL);

	ClearBuffer((CHAR*)(g_SlaveSampData.abyRcvBuf), MAX_RECVMASTERFRAMES_IN_BUFF);

	RS485_ClrBuffer(hComm);
	Sleep(5);

	if(RS485_Write(hComm, sSendStr, nSend) != nSend)
	{
		//note	applog	!!
		AppLogOut("SMACGet ResponeData", 
			APP_LOG_UNUSED,
			"[%s]-[%d] ERROR: failed to  RS485_Write \n\r",
			__FILE__, __LINE__);

		TRACE("\n*****   !!!!! SLAVE  RS485_Write   ******\n");
		return  FUNCTION_GENERAL_ERROR;
	}

	Sleep(20);

	if (RS485WaitReadable(fd, 1100) > 0 )
	{
		if(RecvDataFromSLAVE(hComm, abyRcvBuf, &iReceiveNum,iWantToReadnByte))
		{
			//check cid1
			if( abyRcvBuf[5] != sSendStr[5] || abyRcvBuf[6] != sSendStr[6] )
			{
				TRACE("*****SLAVE  check cid1  error ******\n");
				return FUNCTION_GENERAL_ERROR;
			}
			else if( abyRcvBuf[3] != sSendStr[3] || abyRcvBuf[4] != sSendStr[4] )//check equip addr 
			{
				TRACE("\n*****SLAVE  check equip addr  error ******\n");
				return FUNCTION_GENERAL_ERROR;
			}
			else  if( abyRcvBuf[7] != '0' || abyRcvBuf[8] != '0' )//check RTN
			{
				TRACE("*****SLAVE check RTN  error ******\n");
				return FUNCTION_GENERAL_ERROR;
			}
			else
			{
				BYTE szCheckCode[5]={0};
				strncpy( szCheckCode, (BYTE*)abyRcvBuf + iReceiveNum - 5, 4 ); 
				abyRcvBuf[iReceiveNum - 5] = 0;
				CheckSum(abyRcvBuf);

				if( abyRcvBuf[iReceiveNum-2] == szCheckCode[3] && abyRcvBuf[iReceiveNum-3] == szCheckCode[2]
				&& abyRcvBuf[iReceiveNum-4] == szCheckCode[1] && abyRcvBuf[iReceiveNum-5] == szCheckCode[0] )
				{
					return RESPONSE_OK;
				}
				else
				{
					TRACE("****SLAVE  check sum error %s ******\n",abyRcvBuf);
					return FUNCTION_GENERAL_ERROR;
				}
			}
		}
		else
		{			
			TRACE("****SLAVE  else  read 0 ******\n");
			return  FUNCTION_GENERAL_ERROR;
		}
	}
	else
	{
		TRACE("\n*****SLAVE  RS485WaitReadable addr =%d read 0 **\n",(int)iTEST_Slave_addr);
		return  FUNCTION_GENERAL_ERROR;
	}
}


INT32  SLAVE_ClrCommBreakTimes(INT32 iAddr, RS485_DEVICE_CLASS*  SlaveDeviceClass);
/*=============================================================================*
* FUNCTION: SLAVE_SampleCmdE3
* PURPOSE : sampling slave's data, by command e3
* INPUT: 
*     
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 SLAVE_SampleCmdE3(RS485_DEVICE_CLASS* SlaveDeviceClass,
			      INT32 iAddr,
			      INT32 SequenceNum)
{
	TRACE("\n Into SLAVE_SampleCmdE3 \n");
	BYTE		SendBuf[100];
	INT32		i;
	INT32		iLenChk;
	INT32		iSlaveStat;
	INT32		iUnitNo = iAddr;
	INT32		istrlen = 0;
	UNUSED(SequenceNum);

	ClearBuffer(g_SlaveSampData.abyRcvBuf, MAX_RECVMASTERFRAMES_IN_BUFF);

	sprintf((char*)SendBuf, 
		"~%02X%02X%02X%02X%04X\0",
		0x20,
		(unsigned int)iUnitNo,
		0xe5, 
		0xe3,
		(unsigned int)istrlen);
	iLenChk = LenCheckSum((WORD)(0))<<4;
	//LENGTH 4 BYTE
	SendBuf[9]  = HexToAsc((iLenChk&0xf0)>>4);
	SendBuf[10] = HexToAsc((0 >> 8)&0x0f);
	SendBuf[11] = HexToAsc((istrlen & 0xf0)>>4);
	SendBuf[12] = HexToAsc(istrlen & 0x0f);
	CheckSum(SendBuf);

	for(i = 0; i < RS485_RECONNECT_TIMES; i++)
	{
		iSlaveStat = SLAVE_SendAndRead(SlaveDeviceClass->pCommPort->hCommPort,
			SendBuf,
			g_SlaveSampData.abyRcvBuf,
			18,
			(BYTE)(RS485_WAIT_VERY_LONG_TIME),
			MAX_RECVMASTERFRAMES_IN_BUFF);

		//TRACE("*******SLAVE SampleCmd E3 E3 E3 iSlaveStat = %d\n", (int)iSlaveStat);

		if (RESPONSE_OK == iSlaveStat)
		{
			break;
		}
	}


	//TRACE("\n Out SLAVE_SampleCmdE3 \n");

	if(RESPONSE_OK == iSlaveStat)
	{
		SLAVE_ClrCommBreakTimes(iAddr, SlaveDeviceClass);
		TRACE("\n	RCV LEN=%d E3VAL=%s \n",strlen(g_SlaveSampData.abyRcvBuf),g_SlaveSampData.abyRcvBuf);
		SLAVE_UnpackCmdE3(iUnitNo, g_SlaveSampData.abyRcvBuf);


		return 0;
	}
	else
	{
		return -1;
	}
}
/*=============================================================================*
* FUNCTION: SLAVE_SampleCmdE1
* PURPOSE : sampling slave's data, by command e1
* INPUT: 
*     
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 SLAVE_SampleCmdE1(RS485_DEVICE_CLASS* SlaveDeviceClass,
			      INT32 iAddr,
			      INT32 iSlaveEquipNoSequenceNum)
{
	TRACE("/n into SLAVE_SampleCmdE1/n");
	INT32					i;
	INT32					iSendDataLength;
	INT32					iUnitNo;
	INT32					iSlaveStat;
	iSendDataLength			= 0;
	iUnitNo					= iAddr;

	ClearBuffer((CHAR*)(g_SlaveSampData.abySendBuf), MAX_SENDMASTERFRAMES_IN_BUFF);
	ClearBuffer((CHAR*)(g_SlaveSampData.abyRcvBuf), MAX_RECVMASTERFRAMES_IN_BUFF);
	/*
	YDN23StuffSendCmd(INT32 nUnitNo,BYTE CID1,BYTE CID2,BYTE *pcSendData)
	NULL :send data from gc by private cfg
	stuff Ydn23 format data to g_SlaveSampData.abySendBuf
	*/
	//test
	iTEST_Slave_addr = iUnitNo;

	Stuff_YDN23SendCmd(iUnitNo,
		(BYTE)(0xe5),
		(BYTE)(0xe1),
		(BYTE)NULL,
		iSlaveEquipNoSequenceNum);

	iSendDataLength = (INT32)strlen((char *)(g_SlaveSampData.abySendBuf));

	for (i = 0; i < RS485_RECONNECT_TIMES; i++)
	{
		iSlaveStat = SLAVE_SendAndRead( SlaveDeviceClass->pCommPort->hCommPort,
			g_SlaveSampData.abySendBuf,
			g_SlaveSampData.abyRcvBuf,
			iSendDataLength,
			(BYTE)(RS485_WAIT_VERY_LONG_TIME),
			MAX_RECVMASTERFRAMES_IN_BUFF);

		if (RESPONSE_OK == iSlaveStat)
		{	
			break;
		}
	}
	TRACE("/n Out SLAVE_SampleCmdE1/n");
	if(RESPONSE_OK == iSlaveStat)
	{
		SLAVE_UnpackCmdE1(iUnitNo, g_SlaveSampData.abyRcvBuf);
		return TRUE;
	}
	else
	{
		TRACE("###########   SLAVE  SampleCmdE1 iUnitNo :%d	ERR		$$$$  \n",(int)iUnitNo);
		return FALSE;
	}
}
/*=============================================================================*
* FUNCTION: SLAVE_GetSequenceNoByAddr(INT32 Address)
* PURPOSE : Get SequenceNo by the Address for control cmd from SLAVE_SendCtlCmd
* INPUT: 
*    
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SLAVE_GetSequenceNoByAddr(INT32 Address)
{
	return g_SlaveSampData.aRoughDataSlave[Address - SLAVE_ADDR_START][SLAVE_SEQ_NO].iValue; 
}
/*=============================================================================*
* FUNCTION: SLAVE_MasterNeedCommProc()
* PURPOSE : Check 
* INPUT: 
*    
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32	SLAVE_MasterNeedCommProc(void)
{
	if(g_RS485Data.CommPort.enumAttr		!= RS485_ATTR_NONE
		|| g_RS485Data.CommPort.enumBaud	!= RS485_ATTR_19200
		|| g_RS485Data.CommPort.bOpened		!= TRUE)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
/*=============================================================================*
* FUNCTION: SLAVE_MasterReopenPort()
* PURPOSE : Repeat open rs485 Comm "19200, n, 8, 1"
* INPUT:	 void 	
*    
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SLAVE_MasterReopenPort(void)
{
	INT32	iErrCode;
	INT32	iOpenTimes;
	iOpenTimes = 0;

	while (iOpenTimes < RS485_COMM_TRY_OPEN_TIMES)
	{
		if(g_RS485Data.CommPort.hCommPort && g_RS485Data.CommPort.bOpened)
		{
			RS485_Close(g_RS485Data.CommPort.hCommPort);
		}

		g_RS485Data.CommPort.hCommPort 
			= RS485_Open("19200, n, 8, 1", (int)RS485_READ_WRITE_TIMEOUT, (int *)&iErrCode);

		if(iErrCode == ERR_COMM_OK)
		{
			g_RS485Data.CommPort.bOpened = TRUE;
			g_RS485Data.CommPort.enumAttr = RS485_ATTR_NONE;
			g_RS485Data.CommPort.enumBaud = RS485_ATTR_19200;
			Sleep(5);
			break;
		}
		else
		{
			Sleep(1000);
			iOpenTimes++;
			g_RS485Data.CommPort.bOpened = FALSE;
			AppLogOut("Slave.c",
				APP_LOG_ERROR,
				"[%s]-[%d]ERROR:Failed to SLAVE_ MasterReopenPort\n\r", 
				__FILE__, __LINE__);
		}
	}

	return 0;
}
/*=============================================================================*
* FUNCTION: SLAVE_ClrCommBreakTimes
* PURPOSE :	Only one time communication ok clean the failt flag
* INPUT:	RS485_DEVICE_CLASS*  SlaveDeviceClass
*			INT32 iAddr
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32  SLAVE_ClrCommBreakTimes(INT32 iAddr, RS485_DEVICE_CLASS*  SlaveDeviceClass)
{
	INT32 iRoughPosition;
	iRoughPosition = (iAddr - SLAVE_ADDR_START) * SLAVE_MAX_SIGNALS_NUM;
	SlaveDeviceClass->pRoughData[iRoughPosition + SLAVE_COMMUNICATION_STAT].iValue 
		= MASTERSLAVE_COMM_OK;
	SlaveDeviceClass->pRoughData[iRoughPosition + SLAVE_INTERRUPT_TIMES].iValue 
		= 0;
	return 0;
}
/*=============================================================================*
* FUNCTION: SLAVE_IncCommBreakTimes
* PURPOSE : Record communication fail time,if more than three means report fail
* INPUT:	INT32 iAddr
*			RS485_DEVICE_CLASS*  SlaveDeviceClass
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32  SLAVE_IncCommBreakTimes(INT32 iAddr, RS485_DEVICE_CLASS*  SlaveDeviceClass)
{
	INT32	iRoughPosition;
	iRoughPosition = (iAddr - SLAVE_ADDR_START) * SLAVE_MAX_SIGNALS_NUM;

	if (SlaveDeviceClass->pRoughData[iRoughPosition + SLAVE_INTERRUPT_TIMES].iValue 
		>= RS485_INTERRUPT_TIMES + 1)
	{
		SlaveDeviceClass->pRoughData[iRoughPosition + SLAVE_COMMUNICATION_STAT].iValue 
			= MASTERSLAVE_COMM_FAIL;
	}
	else
	{
		if(SlaveDeviceClass->pRoughData[iRoughPosition + SLAVE_INTERRUPT_TIMES].iValue
			< RS485_INTERRUPT_TIMES + 2)
		{
			SlaveDeviceClass->pRoughData[iRoughPosition + SLAVE_INTERRUPT_TIMES].iValue++;
		}	
	}
	return 0;
}
/*=============================================================================*
* FUNCTION: SLAVE_Sample
* PURPOSE : Sampling slave all Sig  by 0xe1 0xe3
* INPUT:	g_RS485Data
*    
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SLAVE_Sample(void * p)
{
#define	SMDU_GROUP_EQUIPID		106
	INT32									iAddr;
	INT32									iSlaveEquipNoSequenceNum;
	iSlaveEquipNoSequenceNum				= 0;
	RS485_DEVICE_CLASS*	SlaveDeviceClass	= p;
	static int			s_iFirstRun[3];

	static int			s_iFirstIn = 0;

	HANDLE hCmdThread;




	if(SLAVE_MasterNeedCommProc())
	{
		SLAVE_MasterReopenPort();
	}

	//printf("SlaveDeviceClass->iReconfigTimes = %d\n",SlaveDeviceClass->iReconfigTimes);

	if(SlaveDeviceClass->iReconfigTimes < MAX_SLAVE_RETRY_TIMES)
	{
		s_iFirstRun[0] = 0;
		s_iFirstRun[1] = 0;
		s_iFirstRun[2] = 0;
		//s_iFirstIn = 0;
		iCurrentSlaveAddress = SLAVE_ADDR_START;
		SlaveDeviceClass->bNeedReconfig = FALSE;
		SLAVE_Reconfig(SlaveDeviceClass);
		SlaveDeviceClass->iReconfigTimes++;
		return TRUE;
	}

	//if(SlaveDeviceClass->bNeedReconfig = TRUE)
	//{
	//	s_iFirstRun[0] = 0;
	//	s_iFirstRun[1] = 0;
	//	s_iFirstRun[2] = 0;
	//	SlaveDeviceClass->bNeedReconfig = FALSE;
	//	s_iFirstIn = 0;
	//	iCurrentSlaveAddress = SLAVE_ADDR_START;

	//}

	Sleep(50);
	if(s_iFirstIn < 2)
	{
		for(iAddr = SLAVE_ADDR_START; iAddr <= SLAVE_ADDR_END; iAddr++)
		{
			if(g_SlaveSampData.aRoughDataSlave[iAddr - SLAVE_ADDR_START][SLAVE_EXIST_ST].iValue
				!= SLAVE_EQUIP_NOT_EXISTENT)
			{		
				iSlaveEquipNoSequenceNum = SLAVE_GetSequenceNoByAddr(iAddr);//0 1 2

				if (s_iFirstRun[iAddr - SLAVE_ADDR_START] < 2)
				{
					RS485_ClrBuffer(SlaveDeviceClass->pCommPort->hCommPort);
					Sleep(50);
					//Slave rectifier info  change ,sampling 0xe3
					SLAVE_SampleCmdE3(SlaveDeviceClass,
						iAddr,
						iSlaveEquipNoSequenceNum);

					s_iFirstRun[iAddr - SLAVE_ADDR_START]++;
				}

				if(SLAVE_SampleCmdE1(SlaveDeviceClass, iAddr, iSlaveEquipNoSequenceNum))
				{
					SLAVE_ClrCommBreakTimes(iAddr, SlaveDeviceClass);

					//���ӻ��Ƿ�ģ����Ϣ�����ı䣬����ı佫�ɼ�E3���
					if(SLAVEMergeAsc((char *)(g_SlaveSampData.abyRcvBuf + DATA_YDN23_SLAVE)))
					{
						Sleep(50);
						s_iFirstRun[iAddr - SLAVE_ADDR_START] = 1;
						//Slave rectifier info  change ,sampling 0xe3
						SLAVE_SampleCmdE3(SlaveDeviceClass,
							iAddr,
							iSlaveEquipNoSequenceNum);

						//֪ͨһ��MASTER �дӻ�ģ����Ϣ�����ı� 2/4��
						SetDwordSigValue(2,
							2,
							37,
							1,
							"RS485SLVRCTCHANGE");

						//���½������web��ˢ������
						static	DWORD		s_dwChangeForWeb = 0;		
						static int iChangeTimes = 0;
						iChangeTimes++;
						s_dwChangeForWeb++;
						if(s_dwChangeForWeb >= 255)
						{
							s_dwChangeForWeb = 0;
						}

						if(iChangeTimes >= 2)
						{
							iChangeTimes = 0;
							SetDwordSigValue(SMDU_GROUP_EQUIPID, 
								SIG_TYPE_SAMPLING,
								3,
								s_dwChangeForWeb,
								"RS485SLVRCTCHANGE");
						}

					}
				}	
				else
				{
					SLAVE_IncCommBreakTimes(iAddr,SlaveDeviceClass);
				}
			}
		}

		s_iFirstIn++;
		return 0;

	}

	if(iCurrentSlaveAddress > SLAVE_ADDR_START + 2)
	{
		iCurrentSlaveAddress = SLAVE_ADDR_START;
	}


	if(g_SlaveSampData.aRoughDataSlave[iCurrentSlaveAddress - SLAVE_ADDR_START][SLAVE_EXIST_ST].iValue
		!= SLAVE_EQUIP_NOT_EXISTENT)
	{		
		iSlaveEquipNoSequenceNum = SLAVE_GetSequenceNoByAddr(iCurrentSlaveAddress);//0 1 2

		if (s_iFirstRun[iCurrentSlaveAddress - SLAVE_ADDR_START] < 2)
		{
			RS485_ClrBuffer(SlaveDeviceClass->pCommPort->hCommPort);
			Sleep(50);
			//Slave rectifier info  change ,sampling 0xe3
			SLAVE_SampleCmdE3(SlaveDeviceClass,
				iCurrentSlaveAddress,
				iSlaveEquipNoSequenceNum);

			s_iFirstRun[iCurrentSlaveAddress - SLAVE_ADDR_START]++;
		}

		if(SLAVE_SampleCmdE1(SlaveDeviceClass, iCurrentSlaveAddress, iSlaveEquipNoSequenceNum))
		{
			SLAVE_ClrCommBreakTimes(iCurrentSlaveAddress, SlaveDeviceClass);

			//���ӻ��Ƿ�ģ����Ϣ�����ı䣬����ı佫�ɼ�E3���
			if(SLAVEMergeAsc((char *)(g_SlaveSampData.abyRcvBuf + DATA_YDN23_SLAVE)))
			{
				Sleep(50);
				s_iFirstRun[iCurrentSlaveAddress - SLAVE_ADDR_START] = 1;
				//Slave rectifier info  change ,sampling 0xe3
				SLAVE_SampleCmdE3(SlaveDeviceClass,
					iCurrentSlaveAddress,
					iSlaveEquipNoSequenceNum);

				//֪ͨһ��MASTER �дӻ�ģ����Ϣ�����ı� 2/4��
				SetDwordSigValue(2,
					2,
					37,
					1,
					"RS485SLVRCTCHANGE");

				//���½������web��ˢ������
				static	DWORD		s_dwChangeForWeb = 0;		
				static int iChangeTimes = 0;
				iChangeTimes++;
				s_dwChangeForWeb++;
				if(s_dwChangeForWeb >= 255)
				{
					s_dwChangeForWeb = 0;
				}

				if(iChangeTimes >= 2)
				{
					iChangeTimes = 0;
				SetDwordSigValue(SMDU_GROUP_EQUIPID, 
					SIG_TYPE_SAMPLING,
					3,
					s_dwChangeForWeb,
					"RS485SLVRCTCHANGE");
				}

				/*RunThread_Heartbeat(RunThread_GetId(NULL));
				hCmdThread = RunThread_Create("Auto Config",
					(RUN_THREAD_START_PROC)DIX_AutoConfigMain_ForSlavePosition_ForWeb,
					(void*)NULL,
					(DWORD*)NULL,
					0);*/

			}
		}	
		else
		{
			SLAVE_IncCommBreakTimes(iCurrentSlaveAddress,SlaveDeviceClass);
		}
	}

	iCurrentSlaveAddress++;


	return 0;
}
/*=============================================================================*
* FUNCTION: GetNoExistSlaveAddr
* PURPOSE : get address with no exist slave equip 
* INPUT: 
*     
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 GetNoExistSlaveAddr(VOID)
{	
	INT32 i = 0;

	for(i = SLAVE_ADDR_START; i < SLAVE_ADDR_END; i++)
	{
		if(g_SlaveSampData.aRoughDataSlave[i-SLAVE_ADDR_START][SLAVE_EXIST_ST].iValue
			== SLAVE_EQUIP_NOT_EXISTENT)
		{
			return i - SLAVE_ADDR_START;
		}
	}
	return i;
}
/*=============================================================================*
* FUNCTION: GetSlaveAddrBySeqNo
* PURPOSE : according to iSeqNo  get slave addr
* INPUT: 
*    
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32  GetSlaveAddrBySeqNo(INT32 iSeqNo)
{
	INT32	i;

	for(i = SLAVE_ADDR_START; i <= SLAVE_ADDR_END; i++)
	{
		if(g_SlaveSampData.aRoughDataSlave[i - SLAVE_ADDR_START][SLAVE_SEQ_NO].iValue == iSeqNo)
		{
			return i - SLAVE_ADDR_START;
		}
	}
	return -1;
}
/*=============================================================================*
* FUNCTION: GetInvalidSlaveAddrBySeqNo
* PURPOSE : according to iSeqNo  get slave addr
* INPUT: 
*    
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32  GetInvalidSlaveAddrBySeqNo(INT32 iSeqNo)
{
	INT32	i;
	UNUSED(iSeqNo);
	for(i = SLAVE_ADDR_START; i <= SLAVE_ADDR_END; i++)
	{
		if(g_SlaveSampData.aRoughDataSlave[i - SLAVE_ADDR_START][SLAVE_SEQ_NO].iValue 
			== SLAVE_SAMP_INVALID_VALUE)
		{
			return i - SLAVE_ADDR_START;
		}
	}
	return -1;
}
void StuffSlaveRectInExist(ENUMSIGNALPROC EnumProc, LPVOID lpvoid)
{
	int					k,i;
	RS485_VALUE			stTempEquipExist;

	stTempEquipExist.iValue  = RS485_EQUIP_NOT_EXISTENT;

	for (i = 0; i < 3; i++)
	{
		for (k = 0; k < 100; k++)
		{
			EnumProc(SLAVE1_CH_RECT_EXIST_STAT + k * MAX_RECT_RECT_CHN_PER_SLAVE + MAX_RECTIFIERS_CHN_NUM_PER_SLAVE * i, 
				stTempEquipExist.fValue , 
				lpvoid );
		}
	}

	return;
}
/*=============================================================================*
* FUNCTION: SLAVE_StuffChannel
* PURPOSE : stuff sampling data  by channel map  to gc and web
* INPUT: 
*     
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
VOID SLAVE_StuffChannel(RS485_DEVICE_CLASS*  SlaveDeviceClass,
			ENUMSIGNALPROC EnumProc,
			LPVOID lpvoid)	
{
	INT32				i, j, k,m,l;
	INT32				iRoughDataIndex;
	INT32				iRectifierNum;
	INT32				iSlaveNum;
	INT32				iRectMdlSigChOfsetPost;
	INT32				iSlaveEquipExistCHStart;
	INT32				iSlaveEquipDisConnetion;
	INT32				iRctMdlCommFailCHStart;
	RS485_VALUE			stTempEquipExist;

	UNUSED(SlaveDeviceClass);

	CHANNEL_TO_ROUGH_DATA		SLAVE_0XE1SigOfGroupSys[] =
	{	
		//iChannel								iRoughData
		{SLAVE1_CH_SYSTEM_VOLTAGE,				SLAVE_SYSTEM_VOLTAGE},
		{SLAVE1_CH_MODULE_NUMBER,				SLAVE_MODULE_NUMBER},
		{SLAVE1_CH_RECTIFIER_LOST,				SLAVE_RECTIFIER_LOST},
		{SLAVE1_CH_ALL_RECTIFIER_NO_RESPONSE,	SLAVE_ALL_RECTIFIER_NO_RESPONSE	},

		{SLAVE1_CH_MODULE_OUTPUT_TOTAL_CURRENT,	SLAVE_OUTPUT_TOTAL_CURRENT},//add 2009/12/31
		{SLAVE1_CH_MAINS_FAILURE,				SLAVE_MAINS_FAILURE},//add 2009/12/15

		{SLAVE1_CH_USED_CAPACITY,	SLAVE_USED_CAPACITY},//add 2009/12/31
		{SLAVE1_CH_MAX_CAPACITY,	SLAVE_MAX_CAPACITY},//add 2009/12/15
		{SLAVE1_CH_MIN_CAPACITY,	SLAVE_MIN_CAPACITY},//add 2009/12/31
		{SLAVE1_CH_TOTAL_CURRENT,	SLAVE_TOTAL_CURRENT},//add 2009/12/15
		{SLAVE1_CH_COMM_NUMBERS,	SLAVE_COMM_NUMBERS},//add 2009/12/31
		{SLAVE1_CH_RATED_VOLTAGE,	SLAVE_RATED_VOLTAGE},
		{SLAVE1_CH_MULTI_RECT_FAILURE,	SLAVE_MULTI_RECT_FAILURE},

		{SLAVE1_CH_COMM_FAILES,					SLAVE_COMMUNICATION_STAT},
		{SLAVE1_CH_EXIST_ST,					SLAVE_EXIST_ST},

		{SLAVE_CH_END_FLAG,						SLAVE_CH_END_FLAG},

		//add	
	};

	CHANNEL_TO_ROUGH_DATA		SLAVE_0XE1SigOfRect[] =
	{	
		//iChannel											iRoughData
		{SLAVE1_CH_RECT_CURRENT_LIMIT_POINT,				SLAVE_RCT_CURRENT_LIMIT_POINT},
		{SLAVE1_CH_RECT_OUT_CURRENT,						SLAVE_RECT_CURRENT},
		{SLAVE1_CH_RECT_DC_ON_OFF_STAT,						SLAVE_RCT_RECTIFIER_DC_ON_OFF_STAT},
		{SLAVE1_CH_RECT_AC_ON_OFF_STAT,						SLAVE_RCT_RECTIFIER_AC_ON_OFF_STAT},
		{SLAVE1_CH_RECT_TOTAL_RUNING_TIME,					SLAVE_RCT_TOTAL_RUNNING_TIME},
		{SLAVE1_CH_RECT_FAILURE,							SLAVE_RCT_RECTIFIER_FAILURE	},
		{SLAVE1_CH_OVER_TEMPERATURE,						SLAVE_RECT_OVER_TEMPERATURE},
		{SLAVE1_CH_OVER_VOLTAGE,						SLAVE_RECT_OVER_VOLTAGE},
		{SLAVE1_CH_PROTECTION_STAT,							SLAVE_RECT_PROTECTED},
		{SLAVE1_CH_FAN_FAILURE_STAT,						SLAVE_RECT_FAN_FAILURE},
		{SLAVE1_CH_RECT_COMM_FAILURE,						SLAVE_RCT_RECTIFIER_COMMUNICATE_FAILURE},
		{SLAVE1_CH_RECT_POWER_LIMITED_FOR_RECT,				SLAVE_RECT_POWER_LINITED_FOR_RECT},

		{SLAVE1_CH_RECT_AC_FAILURE,							SLAVE_RECT_AC_FAILURE},//1��19��
		{SLAVE1_CH_RECT_VOLTAGE,							SLAVE_RECT_VOLTAGE},	//2��4��
		{SLAVE1_CH_RECT_EXIST_STAT,							SLAVE_RECT_EXISTENCE_STATE},

		{SLAVE1_CH_RECT_TEMP,						SLAVE_RECT_TEMP},
		{SLAVE1_CH_RECT_WORKIN,						SLAVE_RECT_WORKIN},
		{SLAVE1_CH_RECT_PHASE,						SLAVE_RECT_PHASE},
		{SLAVE1_CH_RECT_AC_VOLTAGE,					SLAVE_RECT_AC_INPUT_VOLT},

		{SLAVE_CH_END_FLAG,									SLAVE_CH_END_FLAG},

		//add	
	};

	CHANNEL_TO_ROUGH_DATA		SLAVE_0XE3SigOfRect[] =
	{	
		//iChannel											iRoughData
		{SLAVE1_CH_RECT_BARCODE_1,				SLAVE_RCT_BARCODE_1},
		{SLAVE1_CH_RECT_BARCODE_2,				SLAVE_RCT_BARCODE_2},
		{SLAVE1_CH_RECT_BATCODE_3,				SLAVE_RCT_BARCODE_3},
		{SLAVE1_CH_RECT_BATCODE_4,				SLAVE_RCT_BARCODE_4},
		{SLAVE1_CH_RECT_RATED_CURRENT,			SLAVE_RCT_RATED_CURREN	},
		{SLAVE1_CH_RECT_RATED_EFFICIENCY,		SLAVE_RCT_RECTIFIER_RATED_EFFICIENCY},
		{SLAVE1_CH_RECT_HI_SERIAL_NO,			SLAVE_RCT_RECT_HI_SN},					//Rectifier High SN
		{SLAVE1_CH_RECT_SERIAL_NO,				SLAVE_RCT_RECT_SN},						//Rectifier SN
		{SLAVE1_CH_RECT_VESION,					SLAVE_RCT_RECT_VESION},
		{SLAVE1_CH_RECT_ID,					SLAVE_RCT_RECT_ID},
		{SLAVE_CH_END_FLAG,						SLAVE_CH_END_FLAG},		
		//add	
	};

	//0 or 1 or 2 or 3
	iSlaveNum = g_SlaveSampData.aRoughDataGroup[SLAVE_RS485_ACTUAL_NUM].iValue;
	stTempEquipExist.iValue = RS485_EQUIP_NOT_EXISTENT;

	//iSlaveNum :    1 2 3
	for(i = 1; i <= iSlaveNum; i++)
	{
		iRoughDataIndex = GetSlaveAddrBySeqNo(i - 1);

		if(iRoughDataIndex == -1)
		{
			AppLogOut("GetSlave AddrBySeqNo",
				APP_LOG_UNUSED,
				"[%s]-[%d] ERROR: failed to get RoughDataIndex by SeqNo \n\r",
				__FILE__, __LINE__);
			return;
		}	

		//1)  Stuff   Slave Sys or Group Sig 
		j = 0;
		while(SLAVE_0XE1SigOfGroupSys[j].iChannel != SLAVE_CH_END_FLAG)
		{
			EnumProc(SLAVE_0XE1SigOfGroupSys[j].iChannel + (i - 1) * MAX_CHN_NUM_PER_SLAVE, 
				g_SlaveSampData.aRoughDataSlave[iRoughDataIndex][SLAVE_0XE1SigOfGroupSys[j].iRoughData].fValue, 
				lpvoid );

			//���ӻ���Ŀ����60����ʱ�����ݴ� ��Ϊ60��
			if ((SLAVE1_CH_MODULE_NUMBER + (i - 1) * MAX_CHN_NUM_PER_SLAVE) 
				== SLAVE_0XE1SigOfGroupSys[j].iChannel)
			{
				if (g_SlaveSampData.aRoughDataSlave[iRoughDataIndex][SLAVE_0XE1SigOfGroupSys[j].iRoughData].iValue > 60)
				{
					g_SlaveSampData.aRoughDataSlave[iRoughDataIndex][SLAVE_0XE1SigOfGroupSys[j].iRoughData].iValue = 60;

					EnumProc(SLAVE_0XE1SigOfGroupSys[j].iChannel + (i - 1) * MAX_CHN_NUM_PER_SLAVE, 
						g_SlaveSampData.aRoughDataSlave[iRoughDataIndex][SLAVE_0XE1SigOfGroupSys[j].iRoughData].fValue, 
						lpvoid );			
				}
			}

			j++;
		}

		//2) Stuff  Rectifier Module sig info of [RECT_SINGLE_MAP_INFO] by 0xe1  0xe3
		iRectifierNum = g_SlaveSampData.aRoughDataSlave[iRoughDataIndex][SLAVE_MODULE_NUMBER].iValue;
		if (iRectifierNum > MAX_RECTIFIER_NUM)
		{
			iRectifierNum = MAX_RECTIFIER_NUM;
		}

		for(k = 0; k < iRectifierNum; k++)
		{
			j = 0;
			while(SLAVE_0XE1SigOfRect[j].iChannel != SLAVE_CH_END_FLAG)
			{			
				EnumProc(SLAVE_0XE1SigOfRect[j].iChannel + k * MAX_RECT_RECT_CHN_PER_SLAVE + MAX_RECTIFIERS_CHN_NUM_PER_SLAVE * (i - 1), 
					g_SlaveSampData.aRoughDataSlave[iRoughDataIndex][SLAVE_0XE1SigOfRect[j].iRoughData + k * MAX_RECTIFIER_0XE1_SIG_NUM].fValue, 
					lpvoid );
				j++;						
			}

			//Stuff sampling rough data by 0xe3
			if(TRUE == g_SlaveSampData.aRoughDataSlave[iRoughDataIndex][SLAVE_SAMPLING_E3_FLAG].iValue)
			{	
				j = 0;
				while(SLAVE_0XE3SigOfRect[j].iChannel != SLAVE_CH_END_FLAG)
				{
					EnumProc(SLAVE_0XE3SigOfRect[j].iChannel + k * MAX_RECT_RECT_CHN_PER_SLAVE + MAX_RECTIFIERS_CHN_NUM_PER_SLAVE * (i-1), 
						g_SlaveSampData.aRoughDataSlave[iRoughDataIndex][SLAVE_0XE3SigOfRect[j].iRoughData+k*MAX_RECTIFIER_0XE3_SIG_NUM].fValue, 
						lpvoid );
					j++;
				}	
			}
		}
		g_SlaveSampData.aRoughDataSlave[iRoughDataIndex][SLAVE_SAMPLING_E3_FLAG].iValue = FALSE;


		//3) Stuff the Inexistent Rectifier Equipment
		for (k = iRectifierNum; k < 100; k++)
		{
			stTempEquipExist.iValue  = RS485_EQUIP_NOT_EXISTENT;
			j = 0;
			while(SLAVE_0XE1SigOfRect[j].iChannel != SLAVE_CH_END_FLAG)
			{			
				EnumProc(SLAVE_0XE1SigOfRect[j].iChannel + k * MAX_RECT_RECT_CHN_PER_SLAVE + MAX_RECTIFIERS_CHN_NUM_PER_SLAVE * (i - 1), 
					stTempEquipExist.fValue , 
					lpvoid );
				j++;						
			}

			///////////////////////////////////////////////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////2011-03-17/////////////////////////////////////////////
			///////////////////�˴������ǣ����ⷢ�ֽ���40��ģ�鵫��SITE invetory///////////////////
			//////////////////��ȴ��48��ģ�飬���8���Ǵ���ģ����Է�����δˢ�źŵ�ԭ��////////////
			stTempEquipExist.iValue  = RS485_SAMP_INVALID_VALUE;
			j = 0;
			while(SLAVE_0XE3SigOfRect[j].iChannel != SLAVE_CH_END_FLAG)
			{
				EnumProc(SLAVE_0XE3SigOfRect[j].iChannel + k * MAX_RECT_RECT_CHN_PER_SLAVE + MAX_RECTIFIERS_CHN_NUM_PER_SLAVE * (i-1), 
					stTempEquipExist.fValue , 
					lpvoid );
				j++;
			}	
		}

		if (0 == iRectifierNum)
		{
			stTempEquipExist.iValue  = RS485_EQUIP_NOT_EXISTENT;
			iSlaveEquipExistCHStart = SLAVE1_CH_RECT_EXIST_STAT 
				+ ((i - 1) * MAX_RECTIFIERS_CHN_NUM_PER_SLAVE);

			for (m = 0; m < 100; m++)
			{
				iRectMdlSigChOfsetPost = m * MAX_RECT_RECT_CHN_PER_SLAVE;
				EnumProc(iSlaveEquipExistCHStart + iRectMdlSigChOfsetPost, 
					stTempEquipExist.fValue, 
					lpvoid );
			}

			continue;
		}

		//4) Communication is fails process
		iSlaveEquipDisConnetion 
			= g_SlaveSampData.aRoughDataSlave[iRoughDataIndex][SLAVE_COMMUNICATION_STAT].iValue;

		if (MASTERSLAVE_COMM_FAIL == iSlaveEquipDisConnetion)
		{
			stTempEquipExist.iValue		 = MASTERSLAVE_COMM_FAIL;
			iRctMdlCommFailCHStart		 = SLAVE1_CH_RECT_COMM_FAILURE 
				+ ((i - 1) * MAX_RECTIFIERS_CHN_NUM_PER_SLAVE);

			if (g_SlaveSampData.aRoughDataSlave[iRoughDataIndex][SLAVE_COMM_FAILES_TIME].iValue > 1)
			{
				//After Stuffed Master/Slave COMM FAIL,Stuff RCT Modules Communication fail 
				for (l = 0; l < 100; l++)
				{
					iRectMdlSigChOfsetPost = l * MAX_RECT_RECT_CHN_PER_SLAVE;
					EnumProc(iRctMdlCommFailCHStart + iRectMdlSigChOfsetPost, 
						stTempEquipExist.fValue, 
						lpvoid );
				}
			}
			else
			{
				g_SlaveSampData.aRoughDataSlave[iRoughDataIndex][SLAVE_COMM_FAILES_TIME].iValue++;
			}
			continue;
		}
		else
		{
			g_SlaveSampData.aRoughDataSlave[iRoughDataIndex][SLAVE_COMMUNICATION_STAT].iValue = 0;
		}

	}//End for(i = 1; i <= iSlaveNum; i++)

	iSlaveNum += 1;
	//Stuff Inexistent Equip signal
	stTempEquipExist.iValue = RS485_EQUIP_NOT_EXISTENT;
	for(i = iSlaveNum; i <= MAX_NUM_SLAVE; i++)
	{
		iRoughDataIndex = GetInvalidSlaveAddrBySeqNo(iSlaveNum);

		j = 0;
		while(SLAVE_0XE1SigOfGroupSys[j].iChannel != SLAVE_CH_END_FLAG)
		{
			EnumProc(SLAVE_0XE1SigOfGroupSys[j].iChannel + (i - 1) * MAX_CHN_NUM_PER_SLAVE, 
				g_SlaveSampData.aRoughDataSlave[iRoughDataIndex][SLAVE_0XE1SigOfGroupSys[j].iRoughData].fValue, 
				lpvoid );

			if ((SLAVE1_CH_MODULE_NUMBER + (i - 1) * MAX_CHN_NUM_PER_SLAVE) 
				== SLAVE_0XE1SigOfGroupSys[j].iChannel)
			{
				if (g_SlaveSampData.aRoughDataSlave[iRoughDataIndex][SLAVE_0XE1SigOfGroupSys[j].iRoughData].iValue > 60)
				{
					g_SlaveSampData.aRoughDataSlave[iRoughDataIndex][SLAVE_0XE1SigOfGroupSys[j].iRoughData].iValue = 60;

					EnumProc(SLAVE_0XE1SigOfGroupSys[j].iChannel + (i - 1) * MAX_CHN_NUM_PER_SLAVE, 
						g_SlaveSampData.aRoughDataSlave[iRoughDataIndex][SLAVE_0XE1SigOfGroupSys[j].iRoughData].fValue, 
						lpvoid );			
				}
			}
			j++;
		}
		stTempEquipExist.iValue  = RS485_EQUIP_NOT_EXISTENT;
		iSlaveEquipExistCHStart = SLAVE1_CH_RECT_EXIST_STAT 
			+ ((i - 1) * MAX_RECTIFIERS_CHN_NUM_PER_SLAVE);

		for (m = 0; m < 100; m++)
		{
			iRectMdlSigChOfsetPost = m * MAX_RECT_RECT_CHN_PER_SLAVE;
			EnumProc(iSlaveEquipExistCHStart + iRectMdlSigChOfsetPost, 
				stTempEquipExist.fValue, 
				lpvoid );
		}

	}

	return;
}
/*=============================================================================*
* FUNCTION: SLAVE_UnpackBarCode
* PURPOSE : SLAVE unpack Product Info 
* INPUT: 
*     
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL void SLAVE_UnpackBarCode(PRODUCT_INFO * pInfo,BYTE *ReceiveFrame)
{
	//refer to protocol
	//1��	szPartNumber 32�ֽ� 
	//2��	szHWVersion  32�ֽ�
	//3��	szSWVersion  32�ֽ�
	//4��	szSerialNumber 64�ֽ�

	//1��	szPartNumber 
	INT32 i = 0;
	BYTE *pRecvData;
	pRecvData = ReceiveFrame;
	pRecvData += DATA_YDN23_SLAVE;

	for(i = 0; i < 16; i++)
	{
		pInfo->szPartNumber[i] = SLAVEMergeAsc((char*)(pRecvData + i * 2));
	}
	pRecvData += 32;
	//2��	szHWVersion
	for(i = 0; i < 16; i++)
	{
		pInfo->szHWVersion[i] = SLAVEMergeAsc((char*)(pRecvData + i * 2));
	}
	pRecvData += 32;
	//3��	szSWVersion
	for(i = 0; i < 16; i++)
	{
		pInfo->szSWVersion[i] = SLAVEMergeAsc((char*)(pRecvData + i * 2));
	}
	pRecvData += 32;
	//4��	szSerialNumber
	for(i = 0; i < 32; i++)
	{
		pInfo->szSerialNumber[i] = SLAVEMergeAsc((char*)(pRecvData + i * 2));
	}
	//pRecvData += 64;
	TRACE("SLAVE BBU PartNumber:%s\n", pInfo->szPartNumber);
	TRACE("SLAVE BBU SW version:%s\n", pInfo->szSWVersion);
	TRACE("SLAVE BBU HW version:%s\n", pInfo->szHWVersion);
	TRACE("SLAVE BBU Serial Number:%s\n", pInfo->szSerialNumber);
}
/*=============================================================================*
* FUNCTION: SLAVE_GetProdtInfo
* PURPOSE : Get SLAVE Product Info 
* INPUT: 
*     
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void SLAVE_GetProdctInfo(HANDLE hComm, int nUnitNo, void *pPI)
{
	UNUSED(hComm);
	UNUSED(nUnitNo);
	PRODUCT_INFO *pInfo;
	INT32 i = 0;
	INT32 iAddr;
	INT32 iSlaveStat;
	INT32 iSlaveExistStat;
	BYTE szSendStr[40] = {0};
	BYTE ReceiveFrame[1024] = {0}; 

	pInfo = (PRODUCT_INFO *)pPI;
	pInfo += SLAVE1_EID;

	if (g_RS485Data.enumRunningMode == RS485_RUN_MODE_SLAVE 
		|| RS485_RUN_MODE_STANDALONE == g_RS485Data.enumRunningMode)
	{
		TRACE(" G	BBU  SLAVE GetProductInfo IN  SLAVE Mode or STANDMode :  \n");	
		return;
	}

	if(SLAVE_MasterNeedCommProc())
	{
		SLAVE_MasterReopenPort();
	}


	for(iAddr = SLAVE_ADDR_START; iAddr <= SLAVE_ADDR_END; iAddr++)
	{
		iSlaveExistStat = g_SlaveSampData.aRoughDataSlave[iAddr - SLAVE_ADDR_START][SLAVE_EXIST_ST].iValue;

		if (SLAVE_EQUIP_NOT_EXISTENT == iSlaveExistStat)
		{
			continue;
		}

		sprintf((char*)szSendStr, 
			"~%02X%02X%02X%02X%04X\0",
			(unsigned int)0x20,
			(unsigned int)iAddr,
			(unsigned int)0xe5, 
			(unsigned int)0xec,
			0);

		CheckSum(szSendStr);

		for (i = 0; i < RS485_RECONNECT_TIMES + 3; i++)
		{
			iSlaveStat = SLAVE_SendAndRead(g_RS485Data.CommPort.hCommPort,
				szSendStr,
				ReceiveFrame,
				18,
				(BYTE)RS485_WAIT_LONG_TIME,
				(INT32)1024);

			if (RESPONSE_OK == iSlaveStat)
			{
				pInfo->bSigModelUsed = TRUE;
				SLAVE_UnpackBarCode(pInfo, ReceiveFrame);
				break;
			}
			else
			{
				//�ɲ��ɹ���������û�У����ܴӻ�ûдBARCODE
				pInfo->bSigModelUsed = TRUE;
			}
		}

		pInfo++;
		Sleep(500);
	}

	/*20131022 lkf ���ر�Ҳ��Ӱ�죬����Ч�ʸ�һ��
	if (g_RS485Data.CommPort.bOpened)
	{
	RS485_Close(g_RS485Data.CommPort.hCommPort);
	g_RS485Data.CommPort.bOpened = FALSE;
	g_RS485Data.CommPort.enumAttr = 0;
	g_RS485Data.CommPort.enumBaud = 0;
	}*/


}


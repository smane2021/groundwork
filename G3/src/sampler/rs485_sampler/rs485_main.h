/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : can_sampler_main.h
 *  CREATOR  : Frank Cao                DATE: 2008-06-10 17:04
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef __RS485_SAMPLER_MAIN_H
#define __RS485_SAMPLER_MAIN_H



#include <stdio.h>
#include "basetypes.h"
#include "public.h"
#include "err_code.h"
#include "pubfunc.h"
#include "string.h"
#include "../../mainapp/equip_mon/equip_mon.h"

#define TST_RS485_DEBUG			TRUE
#define TST_RS485_DEBUG_SLAVE	FALSE//FALSE
#define TST_RS485_DEBUG_REPORT	FALSE//FALSE
#define TST_RS485_DEBUG_SMIO	FALSE//FALSE
#define TST_RS485_DEBUG_SMAC	FALSE//FALSE
#define TST_RS485_DEBUG_SMBAT	FALSE//FALSE
#define TST_RS485_DEBUG_SMDU	FALSE//FALSE
//#include "can_rectifier.h"
#define DLLExport 
typedef BOOL (CALLBACK *ENUMSIGNALPROC)(int, float, LPVOID);

#define RS485_COMM_TRY_OPEN_TIMES				5

#define RS485_SMAC_SAMP_FREQUENCE				2	//SampFrequence
#define RS485_SMIO_SAMP_FREQUENCE				1
#define RS485_SMBAT_SAMP_FREQUENCE				2
#define RS485_SMLDU_SAMP_FREQUENCE				2

#define RS485_SM_SAMP_MAX_ADDR					5

#define RS485_SYS_RECONFIG_SIG_ID				90
#define	SYS_GROUP_EQUIPID						1

#define RS485_RUN_MODE_STANDALONE				0
#define RS485_RUN_MODE_MASTER					1
#define RS485_RUN_MODE_SLAVE					2

#define RS485_GET_MODE_EQUIPID					1
#define RS485_RUNNING_MODE_SIGID				180

#define RS485_GET_ADDR_EQUIPID					1
#define RS485_RUNNING_ADDR_SIGID				91

#define RS485_EQUIP_EXISTENT					0
#define RS485_EQUIP_NOT_EXISTENT				1

#define RS485_WAIT_LONG_TIME					0	
#define RS485_WAIT_VERY_LONG_TIME				3
#define RS485_WAIT_SHORT_TIME					1	
#define RS485_WAIT_VERY_SHORT_TIME				2

#define RS485_COMM_NORMAL						0
#define RS485_COMM_ABNORMALITY					1


#define RS485_SAMP_INVALID_VALUE			(-9999)

#define RS485_FALG_RUNNING					0xa5a5

#define	RS485_SYS_GROUP_EQUIPID				1

#define	RS485_SMDU1_BATT_FUSE_EQUIPID		160

#define	RS485_SMDU1_LVD_EQUIPID		188

#define RS485_WORK_STATUS_SIG_ID		100

#define RS485_RECONFIG_SIG_ID				90

#define RS485_SAMPLE_OK						0
#define RS485_SAMPLE_FAIL					1
#define RS485_WAIT_TIME_OUT					500
#define RS485_INTERRUPT_TIMES				3
#define RS485_RECONNECT_TIMES				2 // Rect group4 can not display on production information,so add retry times.

#define SLAVE_INVALID_CMDID					0x20

#define  RS485_LOG_OUT(iLevel, OutString) \
	AppLogOut("485_SAMP", (iLevel), "%s", OutString)

#define RS485_SAMPLER_DATA_RECORDS			1
#define RS485_SAMPLER_DATA_FREQUENCY		1000.0


union unRS485_FLOAT_STRING
{
	float	fValue;
	ULONG	ulValue;
	BYTE	abyValue[4];
};
typedef union unRS485_FLOAT_STRING RS485_FLOAT_STRING;

union unRS485_VALUE
{
	float	fValue;
	int		iValue;
	UINT	uiValue;
	ULONG	ulValue;
	DWORD	dwValue;
};
typedef union unRS485_VALUE RS485_VALUE;



enum DEVICE_CLASS_NO
{
	DEVICE_CLASS_SLAVE = 0,
	DEVICE_CLASS_LARGEDU,
	DEVICE_CLASS_SMAC,//DEVICE_CLASS_SMMODULE	
	DEVICE_CLASS_SMIO,
	DEVICE_CLASS_SMBAT,
	DEVICE_CLASS_SMDU,
	DEVICE_CLASS_SMBRC,

	DEVICE_CLASS_MDB,

	DEVICE_CLASS_NUM,
};

enum  DEVICE_EQUIP_ID
{
	SMAC1_EID = 0,
	SMAC2_EID,
	SMAC3_EID,
	SMAC4_EID,
	SMIO1_EID,
	SMIO2_EID,
	SMIO3_EID,
	SMIO4_EID,
	SMIO5_EID,
	SMIO6_EID,
	SMIO7_EID,
	SMIO8_EID,
	SMBAT1_EID,
	SMBAT2_EID,
	SMBAT3_EID,
	SMBAT4_EID,
	SMBAT5_EID,
	SMBAT6_EID,
	SMBAT7_EID,
	SMBAT8_EID,
	SMBAT9_EID,
	SMBAT10_EID,
	SMBAT11_EID,
	SMBAT12_EID,
	SMBAT13_EID,
	SMBAT14_EID,
	SMBAT15_EID,
	SMBAT16_EID,
	SMBAT17_EID,
	SMBAT18_EID,
	SMBAT19_EID,
	SMBAT20_EID,
						//AC Distribution1 293 TOTAL 16
						//AC Distribution2
						//AC Distribution3
						//AC Distribution4
						//AC Distribution5
						//DC Distribution10 308 TOTAL 16
	SLAVE1_EID = SMBAT20_EID + 5 + 10,//5: LARGE DC 10
	SLAVE2_EID,
	SLAVE3_EID,

	SMBRC1_EID,
	SMBRC2_EID,
	SMBRC3_EID,
	SMBRC4_EID,
	SMBRC5_EID,
	SMBRC6_EID,
	SMBRC7_EID,
	SMBRC8_EID,
	SMBRC9_EID,
	SMBRC10_EID,
	SMBRC11_EID,
	SMBRC12_EID,

	ACMETER1_EID,
	ACMETER2_EID,
	ACMETER3_EID,
	ACMETER4_EID,

	END_EQUIP_ID,
	END_EID = -1,
};

#define	RS485_RECV_BUF_LEN			4896
#define	RS485_SEND_BUF_LEN			1024

#define	RS485_READ_WRITE_TIMEOUT	200	//ms

#define RS485_ATTR_19200			19200
#define RS485_ATTR_9600				9600
#define	RS485_ATTR_NONE				0
#define	RS485_ATTR_ODD				1
#define	RS485_ATTR_EVEN				2

struct stRS485_COMM_PORT
{
	HANDLE			hCommPort;
	SIG_ENUM		enumAttr;	// 0: none, 1: odd, 2: even
	SIG_ENUM		enumBaud;
	BOOL			bOpened;
	BYTE			abySendBuf[RS485_SEND_BUF_LEN];
	BYTE			abyRcvBuf[RS485_RECV_BUF_LEN];

	//Frank Wu,20150318, for SoNick Battery
#define RS485_OPEN_PARAM_STR_MAX		64
	char			szOpenParamStr[RS485_OPEN_PARAM_STR_MAX];
};	
typedef struct stRS485_COMM_PORT	RS485_COMM_PORT;

typedef void (*DEVICE_CLASS_INIT)(void*);
typedef void (*DEVICE_CLASS_EXIT)(void*);
typedef void (*DEVICE_CLASS_RECONFIG)(void*);
typedef void (*DEVICE_CLASS_SAMPLE)(void*);
typedef void (*DEVICE_CLASS_STUFF_CHN)(void*, ENUMSIGNALPROC, LPVOID);
typedef void (*DEVICE_CLASS_PARAM_UNIFY)(void*);
typedef void (*DEVICE_CLASS_SEND_CMD)(void*, int, float, char*);


struct stRS485_DEVICE_CLASS
{
	char*						pszName;
	RS485_COMM_PORT*			pCommPort;
	RS485_VALUE*				pRoughData;
	DEVICE_CLASS_INIT			pfnInit;
	DEVICE_CLASS_EXIT			pfnExit;
	DEVICE_CLASS_PARAM_UNIFY	pfnParamUnify;
	DEVICE_CLASS_RECONFIG		pfnReconfig;
	DEVICE_CLASS_SAMPLE			pfnSample;
	DEVICE_CLASS_STUFF_CHN		pfnStuffChn;
	DEVICE_CLASS_SEND_CMD		pfnSendCmd;
	void*						pCommInfo;
	void*						pFalshData;
	BOOL						bNeedReconfig;
	int				iReconfigTimes;

};
typedef struct stRS485_DEVICE_CLASS	RS485_DEVICE_CLASS;

struct stRS485_COMM_INFO
{
	CHAR	*cEquipName;
	INT32	iSampFrequenceCounter;
	INT32	iSampFrequence;
	BOOL	bNeedStuffFlag;
};
typedef struct stRS485_COMM_INFO RS485_COOMUNICATION_INFO;

struct stRS485_SMP_CTRL
{
	BOOL    bStartCtlFlag;
	BYTE	cExistMaxAddr;

	BYTE    cCrrentAddrIndex;
	BYTE	cSAMPExistAddr[35];	//2 smac 8 smio 12 smbat 8 smdu

	BYTE    cUrgencyIndex;
	BYTE	cUrgencySampAddr[35];

	BYTE    cCrrentCtlIndex;
	BYTE	cJustCtrl[35];
	BYTE	cNeedSmpAddr[5];

};
typedef struct stRS485_SMP_CTRL RS485_SMP_STRL;



struct stRS485_SAMPLER_DATA
{
	int					iRunningFlag;	//to sync between Contrl and Query
	RS485_COMM_PORT		CommPort;
	RS485_DEVICE_CLASS*	pDeviceClass;
	RS485_COOMUNICATION_INFO stEquipCommInfo[DEVICE_CLASS_NUM];
	RS485_SMP_STRL			stSMSmplingCtrl;
	HANDLE				hSlaveThread;
	SIG_ENUM			enumRunningMode; 
	/* 
	if detected the bNeedReInit flag  
	in the query,delay 4000ms at once!	
	after 4000ms g_RS485Data.bNeedReInit
	= FALSE  in the rs485_main.c�� 
	*/
	BOOL				bNeedReInitModeModify;
	BOOL				bNeedGetAllBarcode;
};
typedef struct stRS485_SAMPLER_DATA	RS485_SAMPLER_DATA;

//struct stRS485_SAMPLER_DATA
//{
//	int					iRunningFlag;	//to sync between Contrl and Query
//	RS485_COMM_PORT		CommPort;
//	RS485_DEVICE_CLASS*	pDeviceClass;
//	RS485_COOMUNICATION_INFO stEquipCommInfo[DEVICE_CLASS_NUM];
//	HANDLE				hSlaveThread;
//	SIG_ENUM			enumRunningMode; 
//	/* 
//			if detected the bNeedReInit flag  
//			in the query,delay 4000ms at once!	
//			after 4000ms g_RS485Data.bNeedReInit
//			= FALSE  in the rs485_main.c�� 
//	*/
//	BOOL				bNeedReInitModeModify;
//	BOOL				bNeedGetAllBarcode;
//};
//typedef struct stRS485_SAMPLER_DATA	RS485_SAMPLER_DATA;


struct	tagRS485_CHN_ROUGH_DATA_IDX
{
	int		iChannel;
	int		iRoughData;
};

typedef struct tagRS485_CHN_ROUGH_DATA_IDX RS485_CHN_TO_ROUGH_DATA;


struct	tagRS485_PARAM_UNIFY_IDX
{
	int		iEquipId;
	int		iEquipIdDifference;
	int		iSigId;				//for -48V system parameter
	int		iSigId24V;			//for 24V system parameter
	int		iRoughData;
};

typedef struct tagRS485_PARAM_UNIFY_IDX RS485_PARAM_UNIFY_IDX;

void RS485_FloatToString(float fInput, BYTE* strOutput);
int RS485_StrExtract(char* strSource,
					char* strDestination, 
					char cSeparator);

INT32 bBattTesting(INT32 iInSmSamp);

INT32	SampCheckSmAddrProcess(INT32 iAddr);

extern INT32 StrToK( char *S, char *D, char cSep );
extern WORD MakeDataLen( int nLen );

BYTE AscToHex(char c);


const char *log_file_name;
const char *log_fun_name;
int  log_line_number; 

struct list_head {
	struct list_head *next, *prev;
};

struct log_item
{
	struct list_head  loglh;

	int   loglevel;
	int   linenum;

	char  filename[32];  
	char  funname[32];

	char  DateTime[32];

	char *logcontent;
};

typedef struct log_item log_item_t;

void TXYPrint(int level, const char* format, ...);


#define TXYPRINTF	\
	do{log_file_name = __FILE__; log_fun_name = __FUNCTION__; log_line_number = __LINE__;}while(0); TXYPrint


#endif



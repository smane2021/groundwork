/*============================================================================*
*         Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                          ALL RIGHTS RESERVED
*
*  PRODUCT  : Unified Data Type Definition
*
*  FILENAME : rs485_smbrc.c
*  PURPOSE  : Define base data type.
*  
*  HISTORY  :
*    DATE            VERSION        AUTHOR            NOTE
*    2010-03-24      V0.0           IlockTeng         Created.        
*                                                     
*-----------------------------------------------------------------------------
*  GLOBAL VARIABLES
*    NAME                                    DESCRIPTION
*          
*      
*-----------------------------------------------------------------------------
*  GLOBAL FUNCTIONS
*    NAME                                    DESCRIPTION
*      
*    
*============================================================================*/
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <sys/ioctl.h>
#include <termios.h>											
#include	"public.h"										
#include	"basetypes.h"
#include	"rs485_main.h"
#include	"rs485_comm.h"
#include	"rs485_smbrc.h"
static void SMBRC_ParamUnify(RS485_DEVICE_CLASS*  pSMBRCDeviceClass);
//static void SMBRC_ClrResistTestInfo(void);
static void SetUrgencySamp0XE4Flag(BOOL bSetSamp0XE4Flag);
LOCAL INT32 SMBRC_NeedCommProc(void);
LOCAL INT32 SMBRC_ReopenPort(void);
static	BYTE SMBRCHexToAsc(int Hex);
LOCAL BYTE SMBRCLenCheckSum(int wLen);
LOCAL INT32  SMBRC_CheckStrSum(BYTE *abyRcvBuf, INT32 RecTotalLength);
//LOCAL void SMBAT_UnpackPrdctInfoAndBarCode(PRODUCT_INFO * pInfo,BYTE *ReceiveFrame,BYTE *ReceiveFrameTwo);
static void ClearOneStringAllAlarm(int iStringNo);
static void SMBRC_SetAlarm(int byAddr,
			   int byParmNo,
			   int AlarmType,
			   int StringNum,
			   int CellNum,
			   UINT iAlarmSigValue);
static void SMBRC_NotSampSigProc(void);
static BOOL SMBRC_bNeedGetResistTestData(void);
void ClearAllAlarm(void);
BOOL bNeedUrgencySamp0XE4(void);
static void SMBRC_RefreshStringSig(void);
BOOL bCtrlSampThresholdFrequency(void);
static BYTE SMBRC_AscToHex(BYTE c);

SMBRC_SAMP_STDATA			g_SmbrcSampData;
extern 	RS485_SAMPLER_DATA		g_RS485Data;

#define  SMBRC_TEST_FLAG  1

float roundf(float x);

/*=============================================================================*
* FUNCTION: MakeCheckSum()
* PURPOSE : Cumulative  result
* INPUT:	 BYTE *Frame
*     
*
* RETURN:
*     static void
*
* CALLS: 
*     void
*
* CALLED BY: 
*    
*			 Copy from SMAC        DATE: 2009-05-08		14:39
*============================================================================*/
static void SMBRC_MakeCheckSum( BYTE *Frame, int ibufferAllowMaxBytes)
{
	WORD R = 0;

	int i, nLen = (int)strlen( (char*)Frame );  

	if (ibufferAllowMaxBytes < (nLen + 4))
	{	
		AppLogOut("Make CheckSum", 
			APP_LOG_ERROR,
			"Make check sum length is error	");
		return;
	}


	for( i=1; i<nLen; i++ )
	{
		R += *(Frame+i);
	}
	sprintf( (char *)Frame+nLen, "%04X\r", (WORD)-R );
}

static BYTE SMBRC_AscToHex(BYTE c)
{
	if (c >= '0' && c <= '9')
	{
		return c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		return c - 'A' + 10;
	}

	return 0xFF;
}

/*=============================================================================*
* FUNCTION: SMBRC_MergeAsc()
* PURPOSE : assistant function, used to merge two ASCII chars
* INPUT:	 CHAR*	p
*     
*
* RETURN:
*     static BYTE
*
* CALLS: 
*     void
*
* CALLED BY: 
*    
*			 copy from SMAC        DATE: 2009-05-08		14:39
*============================================================================*/
static BYTE SMBRC_MergeAsc(BYTE *p)
{
	BYTE byHi, byLo;

	ASSERT(p);

	byHi = SMBRC_AscToHex((BYTE)(p[0]));
	byLo = SMBRC_AscToHex((BYTE)(p[1]));

	//error data
	if (byHi == 0xFF || byLo == 0xFF)
	{
		return 0;
	}

	return (byHi*16 + byLo);
}
/*=============================================================================*
* FUNCTION: SMBRC_ InitRoughValue
* PURPOSE : Initialization Smbrc RoughData
* INPUT:	 	
*   
*
* RETURN:
*     0
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMBRC_InitRoughValue(void* pDevice)
{
	INT32	i,j,k,m,n;
	RS485_DEVICE_CLASS*		pSMBRCDeviceClass = pDevice;

	pSMBRCDeviceClass->pfnParamUnify = (DEVICE_CLASS_PARAM_UNIFY)SMBRC_ParamUnify;
	pSMBRCDeviceClass->pfnSample	 = (DEVICE_CLASS_SAMPLE)SMBRC_Sample;

	pSMBRCDeviceClass->pfnStuffChn	 = (DEVICE_CLASS_STUFF_CHN)SMBRC_StuffChannel;
	pSMBRCDeviceClass->pfnExit		 = (DEVICE_CLASS_EXIT)SMBRC_Exit;
	pSMBRCDeviceClass->pfnSendCmd	 = (DEVICE_CLASS_SEND_CMD)SMBRC_SendCtrlCmd;
	pSMBRCDeviceClass->bNeedReconfig = TRUE;

#if SMBRC_TEST_FLAG
	//printf("\n ::::::::   this is running to SMBRC_InitRoughValue :::::\n");
#endif
	//Group sig
	for (i = 0; i < SMBRC_G_MAX_SIG; i++)
	{
		g_SmbrcSampData.aRoughGroupData[i].iValue = SMBRC_SIG_INVALID_VALUE;
	}

	g_SmbrcSampData.aRoughGroupData[SMBRC_G_EXIST_STAT].iValue = SMBRC_STAT_INEXISTENCE;
	g_SmbrcSampData.aRoughGroupData[SMBRC_G_EXIST_ADDR_NUM].iValue = 0;

	//unit sig
	for (j = 0; j < SMBRC_ADDR_MAX__NUM; j++)
	{
		for (k = 0; k < SMBRC_UNIT_MAX_SIG; k++)
		{
			g_SmbrcSampData.aRoughUnitData[j][k].iValue = SMBRC_SIG_INVALID_VALUE;
		}

		g_SmbrcSampData.aRoughUnitData[j][SMBRC_UNIT_EXIST_STAT].iValue
			= SMBRC_STAT_INEXISTENCE;
		g_SmbrcSampData.aRoughUnitData[j][SMBRC_UNIT_COMMFAIL_TIMES].iValue
			= 0;
	}

	//string sig
	for (m = 0; m < SMBRC_STRING_MAX_NUM; m++)
	{
		for (n = 0; n < SMBRC_STRING_MAX_SIG; n++)
		{
			g_SmbrcSampData.aRoughStringData[m][n].iValue = SMBRC_SIG_INVALID_VALUE;
		}

		g_SmbrcSampData.aRoughStringData[m][SMBRC_STRING_EXIST_STAT].iValue
			= SMBRC_STAT_INEXISTENCE;
	}

	pSMBRCDeviceClass->bNeedReconfig = TRUE;
	//2011/03/06�գ��Ż��ɼ�E4�����Ƶ�ʡ�
	SetUrgencySamp0XE4Flag(TRUE);
	//g_SmbrcSampData.bNeedSampCmd0XE4 = TRUE;
	return 0;
}
/*=============================================================================*
* FUNCTION: SMBRC _NeedCommProc
* PURPOSE : Check  the rs485Comm port
* INPUT: 
*    
* RETURN:
*      TRUE: need process		FALSE:needn't process
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 SMBRC_NeedCommProc()
{
	if(g_RS485Data.CommPort.enumAttr != RS485_ATTR_NONE
		|| g_RS485Data.CommPort.enumBaud != RS485_ATTR_19200	//2010/07/15
		//|| g_RS485Data.CommPort.enumBaud != RS485_ATTR_9600//2010/07/15
		|| g_RS485Data.CommPort.bOpened != TRUE)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
/*=============================================================================*
* FUNCTION: SMBRC _ReopenPort
* PURPOSE :
* INPUT: 
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 SMBRC_ReopenPort()
{
	INT32	iErrCode;
	INT32	iOpenTimes;
	iOpenTimes = 0;

	while (iOpenTimes < RS485_COMM_TRY_OPEN_TIMES)
	{
		if(g_RS485Data.CommPort.hCommPort && g_RS485Data.CommPort.bOpened)
		{
			RS485_Close(g_RS485Data.CommPort.hCommPort);
		}

		//2010/07/15
		g_RS485Data.CommPort.hCommPort 
			= RS485_Open("19200, n, 8, 1", RS485_READ_WRITE_TIMEOUT, (int *)(&iErrCode));

		//2010/07/15
		//g_RS485Data.CommPort.hCommPort 
		//	= RS485_Open("9600, n, 8, 1", RS485_READ_WRITE_TIMEOUT, &iErrCode);


		if(iErrCode == ERR_COMM_OK)
		{
			g_RS485Data.CommPort.bOpened = TRUE;
			g_RS485Data.CommPort.enumAttr = RS485_ATTR_NONE;
			g_RS485Data.CommPort.enumBaud = RS485_ATTR_19200;//2010/07/15
			//g_RS485Data.CommPort.enumBaud = RS485_ATTR_9600;
			Sleep(5);
			break;
		}
		else
		{
			Sleep(1000);
			iOpenTimes++;
			g_RS485Data.CommPort.bOpened = FALSE;
			AppLogOut("RS485_SMAC_ReOpen", APP_LOG_UNUSED,
				"[%s]-[%d] ERROR: failed to  open rs485Comm IN The SMBRC ReopenPort \n\r", 
				__FILE__, __LINE__);
		}
	}

	return TRUE;
}

static	BYTE SMBRCHexToAsc(int Hex)
{
	BYTE cTemp = 0;
	int iTempValue = 0;
	ASSERT(Hex <= 255);
	iTempValue = (int)Hex;
	if((iTempValue <= 9) && (iTempValue >= 0))
	{
		cTemp = Hex + 0x30;
	}
	else
	{
		cTemp = Hex - 0x0a + 'A';
	}
	return cTemp;
}

/*=============================================================================*
* FUNCTION: SMBRCLenCheckSum
* PURPOSE : calculate checksum of length
* INPUT: 
*		wLen: IN,  length of preparative check data	
*
* RETURN:
*    esulte of verify  data
*
* CALLS: 
*     void
*
* CALLED BY: 
*     Pack_Report_Data
*	   copy from smac      DATE: 2009-05-06		08:39
*============================================================================*/
LOCAL BYTE SMBRCLenCheckSum(int wLen)
{
	BYTE byLenCheckSum = 0;

	//������Ϊ0������Ҫ����
	if (wLen == 0)
		return 0;

	byLenCheckSum = 0;
	byLenCheckSum += wLen & 0x000F;         //ȡ���4��BIT
	byLenCheckSum += (wLen >> 4) & 0x000F;  //ȡ��4~7��BIT
	byLenCheckSum += (wLen >> 8) & 0x000F;  //ȡ��8~11��BIT
	byLenCheckSum %= 16;                    //ģ16
	byLenCheckSum = (~byLenCheckSum) + 1;   //ȡ����1
	byLenCheckSum &= 0x0F;                  //ֻȡ4��BIT

	return byLenCheckSum;
}

/*=============================================================================*
* FUNCTION: SMBRC	_PackAndSend
* PURPOSE : Packing the Cmd Data And Send to Slave equipment
* INPUT:	 	
*   
*
* RETURN:
*     0
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
static INT32 SMBRC_PackAndSend(HANDLE	hComm,
					INT32		iAddr,
					INT32		byCmmdId,
					INT32		byCid2,
					INT32		iSendLengh,
					INT32		iParameter)
{
	BYTE		szSendStr[SMBRC_SEND_BUFFER_NUM]	= {0};
	INT32		iLenChk = 0;
	WORD		istrlen = 0;
	BYTE		iTempValue = 0;
	
	memset(szSendStr, 0, SMBRC_SEND_BUFFER_NUM);

	UNUSED(iSendLengh);
	if (SMBRC_UNUSED_CMD_ID == byCmmdId)
	{
		sprintf((char *)szSendStr,
			"~%02X%02X%02X%02X%04X\0", 
			SMBRC_VERSION_VALUE,
			(unsigned int)iAddr, 
			SMBRC_EQUIP_TYPE_CID1, 
			(unsigned int)byCid2,
			0);
	}
	else
	{
		if (SMBRC_UNUSED_PARAMETER == iParameter)
		{
			istrlen = 2;
			iLenChk = ((INT32)SMBRCLenCheckSum(istrlen)) << 4;

			sprintf((char *)szSendStr,
				"~%02X%02X%02X%02X%04X%02X\0", 
				SMBRC_VERSION_VALUE,
				(unsigned int)iAddr, 
				SMBRC_EQUIP_TYPE_CID1, 
				(unsigned int)byCid2,
				0,		
				(unsigned int)byCmmdId);

			iTempValue = (BYTE)((iLenChk & 0xf0)>>4);
			szSendStr[9] = SMBRCHexToAsc(iTempValue);
			iTempValue = (BYTE)((istrlen >> 8) & 0x0f);
			szSendStr[10] = SMBRCHexToAsc(iTempValue);
			iTempValue = (BYTE)((istrlen & 0xf0)>>4);
			szSendStr[11] = SMBRCHexToAsc(iTempValue);
			iTempValue = (BYTE)(istrlen & 0x0f);
			szSendStr[12] = SMBRCHexToAsc(iTempValue);
		}
		else
		{
			istrlen = 4;
			iLenChk = SMBRCLenCheckSum(istrlen)<<4;

			sprintf((char *)szSendStr,
				"~%02X%02X%02X%02X%04X%02X%02X\0", 
				SMBRC_VERSION_VALUE,
				(unsigned int)iAddr, 
				SMBRC_EQUIP_TYPE_CID1, 
				(unsigned int)byCid2,
				0,			
				(unsigned int)byCmmdId,
				(unsigned int)iParameter);
			iTempValue = (BYTE)((iLenChk & 0xf0)>>4);
			szSendStr[9] = SMBRCHexToAsc(iTempValue);
			iTempValue = (BYTE)((istrlen >> 8) & 0x0f);
			szSendStr[10] = SMBRCHexToAsc(iTempValue);
			iTempValue = (BYTE)((istrlen & 0xf0)>>4);
			szSendStr[11] = SMBRCHexToAsc(iTempValue);
			iTempValue = (BYTE)(istrlen & 0x0f);
			szSendStr[12] = SMBRCHexToAsc(iTempValue);
		}
	}

	SMBRC_MakeCheckSum(szSendStr, SMBRC_SEND_BUFFER_NUM);

	RS485_ClrBuffer(hComm);

	if(RS485_Write(hComm, szSendStr, (int)(strlen((char*)szSendStr))) != (int)(strlen((char*)szSendStr)))
	{
		//note	applog	!!
		AppLogOut("SMBRC_SEND_CMD", APP_LOG_UNUSED, 
			"[%s]-[%d] ERROR: failed to  RS485_Write \n\r",
			__FILE__, __LINE__);

		return SMBRC_SEND_CMD_ERROR;
	}

	return SMBRC_SEND_CMD_OK;
}

static INT32 SMBRC_PackAndSendTwoByte(HANDLE		hComm,
									  INT32		iAddr,
									  INT32		byCmmdId,
									  INT32		byCid2,
									  INT32		iSendLengh,
									  UINT		iparam)
{
	BYTE		szSendStr[SMBRC_SEND_BUFFER_NUM] = {0};
	INT32		iLenChk = 0;
	INT32		istrlen = 0;
	UINT		iSendData = 0;
	iSendData = (UINT)iparam;
	UNUSED(iSendLengh);

	memset(szSendStr, 0, SMBRC_SEND_BUFFER_NUM);

	istrlen = 6;
	iLenChk = SMBRCLenCheckSum(istrlen)<<4;

	sprintf((char *)szSendStr,
		"~%02X%02X%02X%02X%04X%02X%02X%02X\0", 
		SMBRC_VERSION_VALUE,
		(unsigned int)iAddr, 
		SMBRC_EQUIP_TYPE_CID1, 
		(unsigned int)byCid2,
		0,			
		(unsigned int)byCmmdId,
		(BYTE)(iSendData >> 8),
		(BYTE)(iSendData & 0xff));

	szSendStr[9] = SMBRCHexToAsc((iLenChk & 0xf0)>>4);
	szSendStr[10] = SMBRCHexToAsc((istrlen >> 8) & 0x0f);
	szSendStr[11] = SMBRCHexToAsc((istrlen & 0xf0)>>4);
	szSendStr[12] = SMBRCHexToAsc(istrlen & 0x0f);

	SMBRC_MakeCheckSum(szSendStr, SMBRC_SEND_BUFFER_NUM);


	RS485_ClrBuffer(hComm);

	if(RS485_Write(hComm, szSendStr, (int)strlen((char*)szSendStr)) != (int)strlen((char*)szSendStr))
	{
		//note	applog	!!
		AppLogOut("SMBRC_SEND_CMD", APP_LOG_UNUSED, 
			"[%s]-[%d] ERROR: failed to  RS485_Write \n\r", 
			__FILE__, __LINE__);

		TRACE("\n*****   SMBRC  RS485_Write  FUNCTION ERROR ******\n");

		return SMBRC_SEND_CMD_ERROR;
	}

	return SMBRC_SEND_CMD_OK;
}

/*=============================================================================*
* FUNCTION:	SMBRC	_PackAndSend
* PURPOSE :	Read the data from Slave And Check it
* INPUT:	 	
*   
*
* RETURN:	Check Data ok :  TRUE     error :FALSE
*     0
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMBRC_ReadData(HANDLE	hComm,
				BYTE* PabyRcvBuf,
				INT32* piStrLen,
				INT32 iWantToReadnByte)
{
	INT32	iReadLen = 0;		
	INT32	i	= 0;
	INT32	j	= 0;
	INT32	iHead	= -1;
	INT32	iTail	= -1;
	ASSERT( hComm != NULL );
	ASSERT( PabyRcvBuf != NULL);

	//Read the data from SM-BRC
	//iReadLen = RS485_Read(hComm, PabyRcvBuf, iWantToReadnByte);

	iReadLen = RS485_ReadForSMBRC(hComm, PabyRcvBuf, iWantToReadnByte);

	for (i = 0; (i < iReadLen) && (i < SMBRC_RECV_BUF_MAX_NUM); i++)
	{
		if(SMBRC_BY_SOI == *(PabyRcvBuf + i))
		{
			iHead = i;
			break;
		}
	}

	if (iHead < 0)	//   no head
	{
		return FALSE;
	}

	for (i = iHead + 1; (i < iReadLen) && (i < SMBRC_RECV_BUF_MAX_NUM); i++)
	{
		if(SMBRC_BY_EOI == *(PabyRcvBuf + i))
		{
			iTail = i;
			break;
		}
	}	

	if (iTail < 0)	//   no tail
	{
		return FALSE;
	}	

	*piStrLen = iTail - iHead + 1;

	if(iHead > 0)
	{
		for (j = iHead; (j < (*piStrLen)) && (j < SMBRC_RECV_BUF_MAX_NUM); j++)
		{
			*(PabyRcvBuf + (j - iHead)) = *(PabyRcvBuf + j);
		}
	}

	*(PabyRcvBuf + (iTail - iHead + 2)) = '\0';
	return TRUE;
}



/*=============================================================================*
* FUNCTION:		SMBRC_RS485WaitReadable
* PURPOSE  :		wait RS485 data ready
* RETURN   :	int : 1: data ready, 0: timeout,Allow get mode or auto config
* ARGUMENTS:
*						int fd	: 
*						int TimeOut: seconds 
*			
* CALLS    : 
* CALLED BY: 
*								
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
LOCAL INT32 SMBRC_RS485WaitReadable(INT32 fd, INT32 nmsTimeOut)
{
	fd_set readfd;
	struct timeval timeout;

	while (nmsTimeOut > 0)
	{
		// need add ClearWDT() here.
		if (nmsTimeOut < (5 * 1000))
		{
			timeout.tv_usec = nmsTimeOut%1000*1000;					/* usec     */
			timeout.tv_sec  = nmsTimeOut/1000;					/* seconds  */
			nmsTimeOut = 0;
		}
		else
		{
			timeout.tv_usec = 0;							/* usec     */
			timeout.tv_sec  = 5;							/* seconds  */
			RUN_THREAD_HEARTBEAT();
			nmsTimeOut -= (5 * 1000);
		}

	}

	FD_ZERO(&readfd);										/*  Initializes the set to the NULL set. */
	FD_SET(fd, &readfd);									/*  Adds descriptor s to set. */

	select(fd + 1, &readfd, NULL, NULL, &timeout);

	if(FD_ISSET(fd, &readfd))	
	{
		return TRUE;										//1: data ready, can be read
	}
	else
	{
		return FALSE;										//0: Data not ready, can't read!!
	}
}
/*=============================================================================*
* FUNCTION: CheckStrLength
* PURPOSE : Check receive data length 
* INPUT: 
*     
*
* RETURN:
*     INT32   1:  succeed	0:  error  
*		
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*
*============================================================================*/
LOCAL  INT32 CheckStrLength(BYTE *abyRcvBuf, INT32 RecBufOffsetNum, INT32 RecDataLength)
{
	//WORD		wLength;
	BYTE		iLenChk;
	BYTE		c_CheckLength[5]	= {0};
	INT32		OffsetToLength		= RecBufOffsetNum;

	//repeat  length check
	//iLenChk	= SMBRCLenCheckSum(RecDataLength);
	iLenChk = SMBRCLenCheckSum(RecDataLength) << 4;

	c_CheckLength[0] = SMBRCHexToAsc((iLenChk & 0xf0)>>4);
	c_CheckLength[1] = SMBRCHexToAsc((RecDataLength >> 8) & 0x0f);
	c_CheckLength[2] = SMBRCHexToAsc((RecDataLength & 0xf0)>>4);
	c_CheckLength[3] = SMBRCHexToAsc(RecDataLength & 0x0f);

	if((abyRcvBuf[OffsetToLength + 3]		!= c_CheckLength[3]) //fourth byte of  length check
		&& (abyRcvBuf[OffsetToLength + 2]	!= c_CheckLength[2]) //third  byte of  length check
		&&(abyRcvBuf[OffsetToLength + 1]	!= c_CheckLength[1]) //second byte of  length check
		&& (abyRcvBuf[OffsetToLength]		!= c_CheckLength[0]))//first  byte of  length check
	{
		printf("\n	Length check is error!!	\n");
		return FALSE;
	}

	return TRUE;
}
/*=============================================================================*
* FUNCTION: Check StrSum
* PURPOSE : Check receive data sum 
* INPUT: 
*     
*
* RETURN:
*     INT32   1:  succeed	0:  error  
*		
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*
*============================================================================*/
LOCAL INT32  SMBRC_CheckStrSum(BYTE *abyRcvBuf, INT32 RecTotalLength)
{
	CHAR	 szCheckCode[5]	= {0};

	if (RecTotalLength <= 5 || (RecTotalLength >= SMBRC_RECV_BUF_MAX_NUM - 4))
	{
		return FALSE;
	}

	//copy receive checkSum to szCheckCode 
	strncpy(szCheckCode, (CHAR*)abyRcvBuf + (RecTotalLength - SMBRC_CHECKSUM_AND_EOI), 4); 
	abyRcvBuf[RecTotalLength - SMBRC_CHECKSUM_AND_EOI] = 0;

	//repeat check sum
	SMBRC_MakeCheckSum(abyRcvBuf, SMBRC_RECV_BUF_MAX_NUM);


	if((abyRcvBuf[RecTotalLength - 2]		== szCheckCode[3])  //fourth byte of check sum
		&& (abyRcvBuf[RecTotalLength - 3]	== szCheckCode[2])	//third	 byte of check sum
		&&(abyRcvBuf[RecTotalLength - 4]	== szCheckCode[1])	//second byte of check sum
		&& (abyRcvBuf[RecTotalLength - 5]	== szCheckCode[0]))	//first  byte of check sum
	{ 
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
/*=============================================================================*
* FUNCTION: bAvailabilityRecvData
* PURPOSE : Check receive data be Availability 
* INPUT: 
*     
*
* RETURN:
*     INT32   TRUE:  succeed	FALSE:  error  
*		
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*
*============================================================================*/
static BOOL bValidRecvData(int byAddr, int byCid1, int byCid2OrRtn, BYTE* pRecvData, INT32 iLengh)
{
	BYTE	ByCid1,ByCid2,ByXXAddr;
	INT32	iDataInfoLength;
	ASSERT(byAddr <= 255);

	ByCid1 = SMBRC_MergeAsc(pRecvData + SMBRC_CID_1_OFFSET);
	if (ByCid1 != byCid1)
	{
		//printf("\n!!!!!  IN The  bAvailabilityRecvData CID1 is ERROR!!!!!\n");
		return FALSE;
	}

	ByCid2 = SMBRC_MergeAsc(pRecvData + SMBRC_CID_2_OR_RTN_OFFSET);
	if (ByCid2 != byCid2OrRtn)
	{
		//printf("\n!!!!!  IN The  bAvailabilityRecvData RTN is ERROR!!!!!\n");
		return FALSE;
	}

	ByXXAddr = SMBRC_MergeAsc(pRecvData + SMBRC_ADDR_OFFSET);
	if (ByXXAddr != byAddr)
	{
		//printf("\n!!!!!  IN The  bAvailabilityRecvData Address is ERROR!!!!!\n");
		return FALSE;
	}
	
	iDataInfoLength = iLengh - SMBRC_CHECKSUM_AND_EOI - SMBRC_DATAINFO_START;
	if (!CheckStrLength(pRecvData, SMBRC_CHECKSUM_START, iDataInfoLength))
	{
		printf("\n!!!!!  IN The  bAvailabilityRecvData CheckLengh is ERROR pRecvData=%s !!!!!\n",pRecvData);
		return FALSE;
	}
	
	if(SMBRC_CheckStrSum(pRecvData, iLengh))
	{
		return TRUE;
	}
	else
	{
		//printf("\n!!!!!  IN The  bAvailabilityRecvData CheckSum is ERROR!!!!!\n");
		return FALSE;
	}
}


/*=============================================================================*
* FUNCTION: SMBRC	_GetBattNo
* PURPOSE : Analyse The Data then arrange to The RoughData 
* INPUT:	 	
*   
*
* RETURN:
*        Error:FALSE		ok:TRUE
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMBRC_GetBattNo(INT32 iVirtualAddr)
{
	INT32	i;
	INT32	iTotalExistBattNum = 0;
	ASSERT(iVirtualAddr < SMBRC_ADDR_MAX__NUM);

	for (i = 0; i < iVirtualAddr; i++)
	{
		if (g_SmbrcSampData.aRoughUnitData[i][SMBRC_UNIT_EXIST_STRING_NUM].iValue
			!= SMBRC_SIG_INVALID_VALUE)
		{
			iTotalExistBattNum += 
				g_SmbrcSampData.aRoughUnitData[i][SMBRC_UNIT_EXIST_STRING_NUM].iValue; 
		}
	}

	if (iTotalExistBattNum >= SMBRC_STRING_MAX_NUM)//�ݴ�
	{
		iTotalExistBattNum = 0;
	}

	return iTotalExistBattNum;
}

INT32 SMBRC_GetBattNo_sp(INT32 iVirtualAddr)
{
	INT32	i;
	INT32	iTotalExistBattNum = 0;
	ASSERT(iVirtualAddr < SMBRC_ADDR_MAX__NUM);

	for (i = 0; i < iVirtualAddr; i++)
	{
		if (g_SmbrcSampData.aRoughUnitData[i][SMBRC_UNIT_EXIST_STRING_NUM].iValue
			!= SMBRC_SIG_INVALID_VALUE)
		{
			iTotalExistBattNum += 
				g_SmbrcSampData.aRoughUnitData[i][SMBRC_UNIT_EXIST_STRING_NUM].iValue; 
		}
	}
	
	return iTotalExistBattNum;
}

/*=============================================================================*
* FUNCTION: FourBytesToUnit
* PURPOSE : Four Asccii-hex bytes convert to one unsigned integer
* INPUT:	 	
*   
*
* RETURN:
*        Error:FALSE		ok:TRUE
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL UINT FourBytesToUnit(BYTE* pbyRcvBuf)
{
	//INT32	i;
	UINT	iUnInteger;
	BYTE	byHiData, byLowData;
	ASSERT(pbyRcvBuf);
	BYTE*	pByReceiveData = pbyRcvBuf;

	pByReceiveData = pbyRcvBuf;
	byHiData  = SMBRC_MergeAsc(pByReceiveData);
	pByReceiveData = pbyRcvBuf + 2;
	byLowData = SMBRC_MergeAsc(pByReceiveData);

	iUnInteger = (((UINT)(byHiData) << 8)) + (UINT)byLowData;

	return iUnInteger;
}
/*=============================================================================*
* FUNCTION: SMBRC   _ProcInExitCell
* PURPOSE : Set inexistence Cells Sig -99999
* INPUT:	 	
*   
*
* RETURN:
*        Error:FALSE		ok:TRUE
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void SMBRC_ProcInExitCell(INT32 iBattNo, int byNumOfCells)
{
	INT32 i = 0;
	ASSERT(byNumOfCells <= SMBRC_CELL_MAX_NUM);
	ASSERT(iBattNo < SMBRC_STRING_MAX_NUM);

	for (i = byNumOfCells; i < SMBRC_CELL_MAX_NUM; i++)
	{
		g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_CELL_1_TEMPERAT + i].iValue
			= SMBRC_SIG_INVALID_VALUE;
		g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_CELL_1_VOLT + i].iValue
			= SMBRC_SIG_INVALID_VALUE;
	}

	return;
}
/*=============================================================================*
* FUNCTION: SMBRC   _ConvertTempt
* PURPOSE : integer to float by scale
* INPUT:	 	
*   
*
* RETURN:
*        Error:FALSE		ok:TRUE
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
float SMBRC_ConvertTempt(UINT iInteger)
{
	RS485_VALUE  unTempValue;
	/********************************************************************************************************
	Xiangyang,

	Per Robert request, I made change to the temperature reporting. 

	SM-BRC will report 0xffff (the maximum value) if the temperature probe is not connected. 
	Is there any special process if you receive the temperature value 0xffff?

	Right now, ACU+ displays 1 C instead of 63.0 C.

	Regards,

	*********************************************************************************************************/
	unTempValue.fValue = -273;
	//��СȪ���ʼ������δ�Ӵ�����������������¶���ֵΪ0XFFFF����������ת��Ϊ֮ͬǰ�Ĳ��ԣ�δ�Ӵ���������ʾ-273.���ﲻ��ʾ1C��
	if (0xffff == iInteger )
	{
		return unTempValue.fValue;
	}
	else
	{
		return ((float)iInteger / 1024);
	}
}
/*=============================================================================*
* FUNCTION: SMBRC   ConvertCrrent
* PURPOSE : integer to float by scale
* INPUT:	 	
*   
*
* RETURN:
*        Error:FALSE		ok:TRUE
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
int SMBRC_ConvertCrrent(UINT iInteger)
{
	return (((iInteger >> 15) & 0x01) ? (-1):(1)) * (iInteger & 0x7FFF);//1����0��
}
/*=============================================================================*
* FUNCTION: UnPack StringXX SigInfo
* PURPOSE : Analyse The Data then arrange to The RoughData 
* INPUT:	 	
*   
*
* RETURN:
*        Error:FALSE		ok:TRUE
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
static void UnPackStringXXSigInfo(int byAddr, BYTE* pbyRcvBuf, INT32 iBattNo, BYTE** ppbyRcvBuf)
{

	INT32	i = 0;
	float	fOverrallVolt = 0;
	BYTE	byNumOfCells = 0;
	BYTE*	pStringSigInfo = NULL;			//Must last set ppbyRcvBuf value!!!
	UINT	iVolt = 0;
	UINT	iTemperature = 0;
	INT32	iRippleCurrnt = 0;
	UINT	iStringCurrent = 0;
	INT32	iTempStringCurr = 0;
	ASSERT(pbyRcvBuf);
	pStringSigInfo = pbyRcvBuf;
	ASSERT(byAddr <= 255);
	ASSERT(iBattNo < SMBRC_STRING_MAX_NUM);

	if (iBattNo >= SMBRC_STRING_MAX_NUM)
	{
		AppLogOut("Smbrc", 
			APP_LOG_ERROR, 
			"The Smbrc Battery Number is too long!\n"); 
		iBattNo = 0;
		//return -1;������
	}
	else
	{
		//Normal,Continue
	}

	//1.Get String OverrallVolt					mV
	fOverrallVolt  = (float)FourBytesToUnit(pStringSigInfo) * SMBRC_MILLI_SCALE;
	g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_OVERALL_VOLT].fValue = fOverrallVolt;
	pStringSigInfo += 4;

	/*

	//1.9Э��

	//2.Get String StringCurrent  have Sign!!!	+-
	iStringCurrent = FourBytesToUnit(pStringSigInfo);
	iTempStringCurr = SMBRC_ConvertCrrent(iStringCurrent);
	if (iBattNo < SMBRC_STRING_MAX_NUM)//2010/07/29
	{
	g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_STRING_CURRENT].fValue
	= (float)iTempStringCurr;
	}
	pStringSigInfo += 4; 

	//printf("2 iBattNo=%d iTempStringCurr =%d\n",iBattNo,iTempStringCurr);


	//3.Get String FloatCurrent
	iFloatCurrent  = FourBytesToUnit(pStringSigInfo);
	if (iBattNo < SMBRC_STRING_MAX_NUM)//2010/07/29
	{
	g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_FLOAT_CURRENT].fValue
	= (float)iFloatCurrent;
	}
	pStringSigInfo += 4;
	//printf("3 iBattNo=%d iFloatCurrent =%d\n",iBattNo,iFloatCurrent);

	*/

	//2.0Э��
	iStringCurrent = SMBRC_StringToUint(pStringSigInfo);
	pStringSigInfo += 8;

	//���ǰ���Э�����ģ���
	iTempStringCurr = (((iStringCurrent >> 31) & 0x01) ? (-1):(1)) * (iStringCurrent & 0x7FFFFFFF);//1����0��
	//��ΪGC�����ʱ��Ҫ�ߵ�������
	g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_STRING_CURRENT].fValue = ((float)iTempStringCurr) * (-1) * SMBRC_MILLI_SCALE;
	

	//4.Get String RippleCurrnt	
	iRippleCurrnt  = FourBytesToUnit(pStringSigInfo);
	g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_RIPPLE_CURRENT].fValue = (float)iRippleCurrnt;
	pStringSigInfo += 4;

	//5.Get Number Of Cells
	byNumOfCells = SMBRC_MergeAsc(pStringSigInfo);
	g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_CELLS_NUM].iValue = byNumOfCells;
	pStringSigInfo += 2;
	ASSERT(byNumOfCells <= SMBRC_CELL_MAX_NUM);

	//6.Get some Cell VoltXX		mV
	for (i = 0; (i < byNumOfCells) && (byNumOfCells <= SMBRC_CELL_MAX_NUM); i++)
	{
		iVolt = FourBytesToUnit(pStringSigInfo);
		g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_CELL_1_VOLT + i].fValue = ((float)iVolt * SMBRC_MILLI_SCALE);
		pStringSigInfo += 4;
	}

	//7.Get some Cell TemperatureXX
	for (i = 0; (i < byNumOfCells) && (byNumOfCells <= SMBRC_CELL_MAX_NUM); i++)
	{
		iTemperature = FourBytesToUnit(pStringSigInfo);
		g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_CELL_1_TEMPERAT + i].fValue = SMBRC_ConvertTempt(iTemperature);
		pStringSigInfo += 4;
	}

	//8.Process Inexistence Cells Sig of String
	SMBRC_ProcInExitCell(iBattNo, byNumOfCells);	

	//9.Set the battery string exist status and communication status and address
	//of string and string sequence number
	if (iBattNo < SMBRC_STRING_MAX_NUM)//2010/07/29
	{
		g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_STRING_EXIST_STAT].iValue 
			= SMBRC_STAT_EXIST;
		g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_STRING_COMM_STAT].iValue 
			= SMBRC_COMM_STAT_OK;
		g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_STRING_ADDR_VALUE].iValue 
			= byAddr;
		g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_STRING_SEQ_NUM].iValue 
			= iBattNo;
	}
	//10.
	(*ppbyRcvBuf) = pStringSigInfo;//Point to Next String Sig Info
	return;
}
/*=============================================================================*
* FUNCTION: SMBRC	_ProcInexistDigital
* PURPOSE : Set the inexistence digital signal -99999
* INPUT:	 	
*   
*
* RETURN:
*       void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
static void SMBRC_ProcInexistDigitalIO(INT32 iVirAddr, INT32 iOutNumDigital, INT32 iInNumDigital)
{
	INT32  i = 0;
	ASSERT(iVirAddr < SMBRC_ADDR_MAX__NUM);
	for (i = iOutNumDigital; (i < SMBRC_DIGITAL_MAX_NUM) && (i >= 0); i++)
	{
		g_SmbrcSampData.aRoughUnitData[iVirAddr][SMBRC_DIGITAL_1_OUTPUT + i].iValue
			= SMBRC_SIG_INVALID_VALUE;
	}

	for (i = iInNumDigital; (i < SMBRC_DIGITAL_MAX_NUM) && (i >= 0); i++)
	{
		g_SmbrcSampData.aRoughUnitData[iVirAddr][SMBRC_DIGITAL_1_INPUT + i].iValue
			= SMBRC_SIG_INVALID_VALUE;
	}

	return;
}
/*=============================================================================*
* FUNCTION: SMBRC	_UnpackCmd42
* PURPOSE : Analyse The Data then arrange to The RoughData 
* INPUT:	 	
*   
*
* RETURN:
*        Error:FALSE		ok:TRUE
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32	SMBRC_UnpackCmd41(int byAddr, BYTE* abyRcvBuf, INT32 iRecvDataLengh)
{
	INT32	i;
	UINT	iAmbiTemperature;
	INT32	iNumOfDigitalOut;
	INT32	iNumOfDigitalIn;
	INT32	iBattNo;
	INT32	iStartBattNo;
	INT32	iNumOfStrings;
	BYTE*	PbyRcvBuf		= NULL;
	BYTE*	PNextStringSigInfor	= NULL;
	ASSERT(byAddr <= 255);
	BYTE	byVirtualAddr		= byAddr - SMBRC_ADDR_START;
	UNUSED(iRecvDataLengh);


	//1. Get BattNo of Smbrc Unit by Addr
	iStartBattNo = SMBRC_GetBattNo(byVirtualAddr);//first exist address must return 0
	ASSERT(iStartBattNo < SMBRC_STRING_MAX_NUM);
	ASSERT(byVirtualAddr < SMBRC_ADDR_MAX__NUM);

	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_UNIT_STRING_START_SEQ].iValue
		= iStartBattNo;

	//2. Get exist Number of string
	PbyRcvBuf = abyRcvBuf + SMBRC_DATAINFO_START;
	iNumOfStrings = SMBRC_MergeAsc(PbyRcvBuf);
	if (iNumOfStrings <= SMBRC_MAX_STRING_OF_EQUIP)
	{
		g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_UNIT_EXIST_STRING_NUM].iValue
			= iNumOfStrings;
	}
	else
	{
		g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_UNIT_EXIST_STRING_NUM].iValue
			= 0;			//= SMBRC_MAX_STRING_OF_EQUIP;
	}
	PbyRcvBuf += 2;				//Point to First Strings Sig Info

	//3 Get Strings Sig Info
	for (i = 0; 
		((0 < iNumOfStrings) && (iNumOfStrings <= SMBRC_MAX_STRING_OF_EQUIP)) 
		&& (i < iNumOfStrings); 
	i++)
	{
		switch (i)
		{
		case SMBRC_ONE_STRING_SEQ_OFFSET://Get String One
			iBattNo = SMBRC_ONE_STRING_SEQ_OFFSET + iStartBattNo;
			UnPackStringXXSigInfo(byAddr, PbyRcvBuf, iBattNo, &PNextStringSigInfor);
			PbyRcvBuf = PNextStringSigInfor;
			break;

		case SMBRC_TWO_STRING_SEQ_OFFSET://Get String Two
			iBattNo = SMBRC_TWO_STRING_SEQ_OFFSET + iStartBattNo;
			UnPackStringXXSigInfo(byAddr, PbyRcvBuf, iBattNo, &PNextStringSigInfor);
			PbyRcvBuf = PNextStringSigInfor;
			break;

		case SMBRC_THREE_STRING_SEQ_OFFSET://Get String Three
			iBattNo = SMBRC_THREE_STRING_SEQ_OFFSET + iStartBattNo;
			UnPackStringXXSigInfo(byAddr, PbyRcvBuf, iBattNo, &PNextStringSigInfor);
			PbyRcvBuf = PNextStringSigInfor;
			break;

		case SMBRC_FOUR_STRING_SEQ_OFFSET://Get String Four
			iBattNo = SMBRC_FOUR_STRING_SEQ_OFFSET + iStartBattNo;
			UnPackStringXXSigInfo(byAddr, PbyRcvBuf, iBattNo, &PNextStringSigInfor);
			PbyRcvBuf = PNextStringSigInfor;
			break;

		default:
			TRACE("\n::::::: iNumOfStrings is Error :::::::\n");
			break;							//Needn't process!!
		}
	}

	//4 Get Ambient temperature of SMBRC Unit
	PbyRcvBuf = PNextStringSigInfor;
	iAmbiTemperature = FourBytesToUnit(PbyRcvBuf);
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_AMBIENT_TEMPT_VALUE].fValue
		= SMBRC_ConvertTempt(iAmbiTemperature);
	PbyRcvBuf += 4;

	//EEM Ҫ��� ��Ԫ�Ļ����¶�  �ŵ���ش���һ��
	for (i = iStartBattNo; 
		i < g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_UNIT_EXIST_STRING_NUM].iValue 
		&& i < SMBRC_STRING_MAX_NUM; i++)
	{
		g_SmbrcSampData.aRoughStringData[i][SMBRC_STTING_AMB_TEMP].fValue = 
			g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_AMBIENT_TEMPT_VALUE].fValue;
	}


	//5.Get Number of digital inputs
	iNumOfDigitalIn = SMBRC_MergeAsc(PbyRcvBuf);
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_DIG_INPUT_NUM].iValue
		= iNumOfDigitalIn;
	PbyRcvBuf += 2;


	//6.Get Digital input Sig !!
	for (i = 0; 
		((0 < iNumOfDigitalIn) && (iNumOfDigitalIn <= SMBRC_DIGITAL_MAX_NUM))
		&& (i < iNumOfDigitalIn);
	i++)
	{
		switch (i)
		{
		case 0:
			g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_DIGITAL_1_INPUT].iValue
				= SMBRC_MergeAsc(PbyRcvBuf);
			PbyRcvBuf += 2;
			break;

		case 1:
			g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_DIGITAL_2_INPUT].iValue
				= SMBRC_MergeAsc(PbyRcvBuf);
			PbyRcvBuf += 2;
			break;

		case 2:
			g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_DIGITAL_3_INPUT].iValue
				= SMBRC_MergeAsc(PbyRcvBuf);
			PbyRcvBuf += 2;
			break;

		case 3:
			g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_DIGITAL_4_INPUT].iValue
				= SMBRC_MergeAsc(PbyRcvBuf);
			PbyRcvBuf += 2;
			break;

		default:
			break;
		}
	}

	//Inexist Digital input Sig Process!!
	for (i = iNumOfDigitalIn; i < SMBRC_DIGITAL_MAX_NUM; i++)
	{
		g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_DIGITAL_1_INPUT + i].iValue
			= SMBRC_SIG_INVALID_VALUE;
	}


	//7.Get Number of Digital Output
	iNumOfDigitalOut = SMBRC_MergeAsc(PbyRcvBuf);
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_DIG_OUTPUT_NUM].iValue
		= iNumOfDigitalOut;
	PbyRcvBuf += 2;

	//8.Get Digital Output Sig !!
	for (i = 0; 
		((0 < iNumOfDigitalOut) && (iNumOfDigitalOut <= SMBRC_DIGITAL_MAX_NUM))
		&& (i < iNumOfDigitalOut);
	i++)
	{
		switch (i)
		{
		case 0:
			g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_DIGITAL_1_OUTPUT].iValue
				= SMBRC_MergeAsc(PbyRcvBuf);
			PbyRcvBuf += 2;
			break;

		case 1:
			g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_DIGITAL_2_OUTPUT].iValue
				= SMBRC_MergeAsc(PbyRcvBuf);
			PbyRcvBuf += 2;
			break;

		case 2:
			g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_DIGITAL_3_OUTPUT].iValue
				= SMBRC_MergeAsc(PbyRcvBuf);
			PbyRcvBuf += 2;
			break;

		case 3:
			g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_DIGITAL_4_OUTPUT].iValue
				= SMBRC_MergeAsc(PbyRcvBuf);
			PbyRcvBuf += 2;
			break;

		default:
			break;
		}
	}

	//Inexist Digital Output Sig Process!!
	for (i = iNumOfDigitalOut; i < SMBRC_DIGITAL_MAX_NUM;i++)
	{
		g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_DIGITAL_1_OUTPUT + i].iValue
			= SMBRC_SIG_INVALID_VALUE;
	}

	//printf("\n :::::SMBRC_UnpackCmd41 :::::::::3:::::\n");
	SMBRC_ProcInexistDigitalIO(byVirtualAddr, iNumOfDigitalOut, iNumOfDigitalIn);

	//9.Get Hardware Operation status !!
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_HARDWARE_OPT_STAT].iValue
		= SMBRC_MergeAsc(PbyRcvBuf);
	PbyRcvBuf += 2;

	//10.Get  Battery String Configuration !!
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_UNIT_CFG_VALUE].iValue
		= SMBRC_MergeAsc(PbyRcvBuf);
	PbyRcvBuf += 2;

	//11.Get Resistance test interal
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_RESIST_INTERVAL].iValue
		= SMBRC_MergeAsc(PbyRcvBuf);
	PbyRcvBuf += 2;

	//That is all!!

	return TRUE;
}
/*=============================================================================*
* FUNCTION: SMBRC _SampleCmd42
* PURPOSE : Sampling Data from SMBRC By 0x42,then array  data to aRoughData
* INPUT:	 	
*   
*
* RETURN:
*     0
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMBRC_SampleGetAllCmd41(INT32 iAddr, RS485_DEVICE_CLASS*  pSMBRCDeviceClass)
{
	INT32	iReadLengh;
	INT32	iCmdStat = SMBRC_SEND_CMD_ERROR;
	BYTE	abyRcvBuf[SMBRC_RECV_BUF_MAX_NUM] = {0};
	HANDLE	hComm = NULL;
	hComm	 = pSMBRCDeviceClass->pCommPort->hCommPort;
	RS485_DRV*	pPort = (RS485_DRV *)hComm;
	INT32		fd = pPort->fdSerial;
	memset(abyRcvBuf, 0, SMBRC_RECV_BUF_MAX_NUM);

	RS485_ClrBuffer(hComm);

	iCmdStat = SMBRC_PackAndSend(hComm,
				iAddr,
				SMBRC_UNUSED_CMD_ID,
				SMBRC_GET_ALL_CID2,
				18,
				SMBRC_UNUSED_PARAMETER);

	if (SMBRC_SEND_CMD_OK == iCmdStat)
	{
		if (SMBRC_RS485WaitReadable(fd, 800))
		{	
			Sleep(10);				//�����Է��֣����������ݻ������ͷ~�����������

			if (SMBRC_ReadData(hComm, abyRcvBuf, &iReadLengh, SMBRC_RECV_BUF_MAX_NUM))
			{
				if (bValidRecvData(iAddr,
					SMBRC_EQUIP_TYPE_CID1,
					SMBRC_CID2_NORMAL_RTN,
					abyRcvBuf,
					iReadLengh))
				{
					SMBRC_UnpackCmd41(iAddr, abyRcvBuf, iReadLengh);
					return TRUE;
				}
				else
				{
					//printf("::::::: CMD41	invalid  data :::::::\n");
					return FALSE;
				}

			}
			else
			{
				//Check Data error!!
				//printf("::::::: CMD41	Read Data error :::::::\n");
				return FALSE;
			}
		}
		else
		{
			//printf("::::::: CMD41	No Response Time Out :::::::\n");
			return FALSE;
		}
	}
	else
	{
		//Send CMD error!!!!
		return FALSE;
	}
}
/*=============================================================================*
* FUNCTION: SMBRC	_bBattStringEnough
* PURPOSE : Check the exist battery be enough.battery string number max value 
*			is 20
* INPUT:	 	
*   
* RETURN:
*     0
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
static BOOL SMBRC_bBattStringEnough(int byVirtualAddr)
{
	INT32	iMaxExistString;

	
	iMaxExistString = SMBRC_GetBattNo_sp(byVirtualAddr);
	//C++ test, found the below code do not run,so change the up function.
	if (iMaxExistString >= SMBRC_STRING_MAX_NUM)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
/*=============================================================================*
* FUNCTION: SMBRC _Reconfig
* PURPOSE : Scan SM-BRC Hardware Equipment!!
* INPUT:	 	
*   
*
* RETURN:
*     0
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMBRC_Reconfig(RS485_DEVICE_CLASS*  pSMBRCDeviceClass)
{
	BYTE		byActualAddr = 0;
	INT32		i = 0;
	int		j = 0;
	INT32		iSmbrcSequence = 0;
	BOOL		bSampOK = FALSE;

	for (i = 0; i < SMBRC_ADDR_MAX__NUM; i++)
	{
		byActualAddr = i + SMBRC_ADDR_START;

		//Check the Battery string number!! Max 20!!
		if (SMBRC_bBattStringEnough(i))
		{
			break;
		}
		else
		{
			//Continue until to ADDR_MAX
		}

		for (j = 0; j < SMBRC_SCAN_TRY_TIMES; j++)
		{
			bSampOK = SMBRC_SampleGetAllCmd41(byActualAddr, pSMBRCDeviceClass);

			if (bSampOK)
			{
				SMBRC_SampleAlarmCmd44(byActualAddr, pSMBRCDeviceClass);
				break;
			}
			else
			{
				//Try again scan until times out
			}
		}//end for

		if (bSampOK)
		{
			//Note,String Numbers ....calculate in the SMBRC _UnpackCmd42 function
			g_SmbrcSampData.aRoughUnitData[i][SMBRC_UNIT_COMM_STAT].iValue = SMBRC_COMM_STAT_OK;
			g_SmbrcSampData.aRoughUnitData[i][SMBRC_UNIT_EXIST_STAT].iValue = SMBRC_STAT_EXIST;
			g_SmbrcSampData.aRoughUnitData[i][SMBRC_UNIT_SEQ_NUM].iValue	 = iSmbrcSequence;
			iSmbrcSequence++;
		}
		else
		{
			//Scan next hardware equipment
		}


	}//end for

	if (iSmbrcSequence)
	{
		g_SmbrcSampData.aRoughGroupData[SMBRC_G_EXIST_STAT].iValue	= SMBRC_STAT_EXIST;
		g_SmbrcSampData.aRoughGroupData[SMBRC_G_EXIST_ADDR_NUM].iValue  = iSmbrcSequence;
	}
	else
	{
		g_SmbrcSampData.aRoughGroupData[SMBRC_G_EXIST_STAT].iValue	= SMBRC_STAT_INEXISTENCE;
		g_SmbrcSampData.aRoughGroupData[SMBRC_G_EXIST_ADDR_NUM].iValue  = iSmbrcSequence;
	}

	return 0;
}

/*=============================================================================*
* FUNCTION: SMBRC _AnalyseAlarmFormat
* PURPOSE : Analyse The Alarm parameter format
* INPUT:	 	
*   
*
* RETURN:
*        Error:FALSE		ok:TRUE
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
static void SMBRC_AnalyseAlarmFormat(BYTE* pbyRcvBuf,
					BYTE* byParmNo,
					BYTE* AlarmType,
					BYTE* StringNum, 
					BYTE* CellNum,
					UINT* AlarmSigValue)
{
	//INT32 i;
	INT32 iAlarmParamFormat = 0;
	BYTE* pbyRcvData = NULL;
	ASSERT(pbyRcvBuf);
	pbyRcvData = pbyRcvBuf;

	//1.Get Parameter of alarm
	iAlarmParamFormat = FourBytesToUnit(pbyRcvData);
	pbyRcvData += 4;

	//2.Get Integer Value of Alarm
	(*AlarmSigValue) = FourBytesToUnit(pbyRcvData);
	pbyRcvData += 4;

	//Following refer to protocol document!!

	//3.Extract byParmNo  From iAlarmParamFormat
	(*byParmNo) = 0x0F & (iAlarmParamFormat >> SMBRC_PARAMNO_OFFSET_BY_ALARM); 

	//4.Extract AlarmType  From iAlarmParamFormat
	(*AlarmType) = 0x01 & (iAlarmParamFormat >> SMBRC_TYPE_OFFSET_BY_ALARM);

	//5.Extract StringNum  From iAlarmParamFormat
	(*StringNum) = 0x0F & (iAlarmParamFormat >> SMBRC_STRINGNUM_OFFSET_BY_ALARM);

	//6.Extract CellNum  From iAlarmParamFormat
	(*CellNum) = 0x7F & (iAlarmParamFormat >> SMBRC_CELLNUM_OFFSET_BY_ALARM);	

	return;
}
/*=============================================================================*
* FUNCTION: SMBRC _AnalyseAlarmFormat
* PURPOSE : Arrange The Alarm Value to aRoughData
* INPUT:	 	
*   
*
* RETURN:
*        void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
static void SMBRC_SetAlarm(int byAddr,
						   int byParmNo,
						   int AlarmType,
						   int StringNum,
						   int CellNum,
						   UINT iAlarmSigValue)
{
	//INT32	i;
	INT32	iBattNo = 0;
	INT32	iStartBattNo = 0;
	INT32	iCellSigValueOffset = 0;
	INT32	iCellAlarmSigOffset = 0;
	BYTE	byVirtualAddr = byAddr - SMBRC_ADDR_START;			//unit
	
	ASSERT(byVirtualAddr < SMBRC_ADDR_MAX__NUM);

	//Calculate Battery String Sequence No By SMBRCs Address!
	iStartBattNo = SMBRC_GetBattNo(byVirtualAddr);
	iBattNo		 = iStartBattNo + StringNum;
	ASSERT(iBattNo < SMBRC_STRING_MAX_NUM);

	//printf("!!!!!!  SetAlarm iBattNo=%d!! byParmNo=%d AlarmType=%d	\n\n",
	//			iBattNo,
	//			byParmNo,
	//			AlarmType);
	CellNum = MIN(24, CellNum);					//���24�ڣ��ݴ�

	switch(byParmNo)
	{
	case SMBRC_PARAMNO_CELL_VOLT://Cell Signal
		//CellNum  0-23 !!!
		iCellSigValueOffset = SMBRC_CELL_1_VOLT + CellNum;
		iCellAlarmSigOffset = SMBRC_CELL_1_VOLT_ALARM + CellNum;
		g_SmbrcSampData.aRoughStringData[iBattNo][iCellSigValueOffset].fValue
				= (float)iAlarmSigValue * SMBRC_MILLI_SCALE;
		g_SmbrcSampData.aRoughStringData[iBattNo][iCellAlarmSigOffset].iValue
				= AlarmType;

		break;

	case SMBRC_PARAMNO_CELL_TEMP://Cell Signal
		iCellSigValueOffset = SMBRC_CELL_1_TEMPERAT + CellNum;
		iCellAlarmSigOffset = SMBRC_CELL_1_TEMPT_ALARM + CellNum;
		g_SmbrcSampData.aRoughStringData[iBattNo][iCellSigValueOffset].fValue
				= SMBRC_ConvertTempt(iAlarmSigValue);
		g_SmbrcSampData.aRoughStringData[iBattNo][iCellAlarmSigOffset].iValue
				= AlarmType;
		break;

	case SMBRC_PARAMNO_CELL_RESIST://Cell Signal Not real time readings
		iCellAlarmSigOffset = SMBRC_CELL_1_RESIST_ALARM + CellNum;
		g_SmbrcSampData.aRoughStringData[iBattNo][iCellAlarmSigOffset].iValue
				= AlarmType;
		break;

	case SMBRC_PARAMNO_CELL_INTERCELL://Cell Signal	Not real time readings
		iCellAlarmSigOffset = SMBRC_CELL_1_INTER_ALARM + CellNum;
		g_SmbrcSampData.aRoughStringData[iBattNo][iCellAlarmSigOffset].iValue
				= AlarmType;
		break;

	case SMBRC_PARAMNO_OVERAL_VOLT://String Signal
		g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_OVERALL_VOLT].fValue
				= (float)iAlarmSigValue * SMBRC_MILLI_SCALE;
		g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_OVERALL_VOLT_ALARM].iValue
				= AlarmType;
		break;

	case SMBRC_PARAMNO_STRING_CURRENT://String Signal
		g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_STRING_CURRENT_ALARM].iValue
				= AlarmType;
		break;

	case SMBRC_PARAMNO_FLOAT_CURRENT://String Signal
		g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_FLOAT_CURRENT].fValue
				= SMBRC_ConvertCrrent(iAlarmSigValue);
		g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_FLOAT_CURRENT_ALARM].iValue
				= AlarmType;
		break;

	case SMBRC_PARAMNO_RIPPLE_CURRENT://String Signal
		g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_RIPPLE_CURRENT].fValue
				= SMBRC_ConvertCrrent(iAlarmSigValue);
		g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_RIPPLE_CURRENT_ALARM].iValue
				= AlarmType;
		break;

	case SMBRC_PARAMNO_AMBIENT_TEMP://Unit Signal
		g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_AMBIENT_TEMPT_VALUE].fValue
			= SMBRC_ConvertTempt(iAlarmSigValue);
		g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_AMBIENT_TEMPT_ALARM].iValue
			= AlarmType;
		break;

	case SMBRC_PARAMNO_CELL_TO_AMBIENT://Cell Signal  Not real time readings
		iCellAlarmSigOffset = SMBRC_CELL_1_TO_AMBIENT_ALARM + CellNum;
			g_SmbrcSampData.aRoughStringData[iBattNo][iCellAlarmSigOffset].iValue
				= AlarmType;
		break;

	default:
		//printf("\nThis is Invalid byParmNo in SMBRC _SetAlarm!!!\n");
		break;
	}
}
/*=============================================================================*
* FUNCTION: SMBRC	_UnpackAlarmCmd44
* PURPOSE : Analyse The Data then arrange to The RoughData 
* INPUT:	 	
*   
*
* RETURN:
*        Error:FALSE		ok:TRUE
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 SMBRC_UnpackAlarmCmd44(int byAddr, BYTE* pbyRcvBuf, INT32 iLengh)
{
	BYTE 		CellNum = 0;
	BYTE		byParmNo = 0;
	BYTE		AlarmType = 0;
	BYTE		StringNum = 0;
	BYTE*		pbyRecvData = NULL;
	BYTE*		pbyRcvDataCOPY = NULL;
	INT32		i = 0;
	INT32		iAlarmNum = 0;
	UINT		iAlarmSigValue = 0;
	INT32		iDataInfoLengh = 0;
	INT32		iReadAlarmNum = 0;
	ASSERT(pbyRcvBuf);


	//1.Get Number of Alarm
	pbyRecvData = pbyRcvBuf + SMBRC_DATAINFO_START;
	iAlarmNum   = SMBRC_MergeAsc(pbyRecvData);
	pbyRecvData += 2;


	//2.Fault handling
	iDataInfoLengh = iLengh - SMBRC_DATAINFO_START - SMBRC_CHECKSUM_AND_EOI;
	iReadAlarmNum = (iDataInfoLengh - 2) / SMBRC_ALARM_ASCII_HEX_LENGH;			//2: Number of Alarm

	if (iAlarmNum > iReadAlarmNum)
	{
		return -1;
	}
	else
	{
		//Normal
	}


	//3.First Clean the All Alarm of the address!.
	pbyRcvDataCOPY = pbyRecvData;
	INT32	iBattNo = 0;
	INT32	iStartBattNo = 0;
	BYTE	byVirtualAddr = byAddr - SMBRC_ADDR_START;//unit
	ASSERT(byVirtualAddr < SMBRC_ADDR_MAX__NUM);

	//Calculate Battery String Sequence No By SMBRCs Address!
	iStartBattNo = SMBRC_GetBattNo(byVirtualAddr);
	ASSERT(iStartBattNo < SMBRC_STRING_MAX_NUM);
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_AMBIENT_TEMPT_ALARM].iValue = SMBRC_SIG_INVALID_VALUE;

	for (i = 0; 
		i < g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_UNIT_EXIST_STRING_NUM].iValue
		&& (i < 4); //MAX 4 Strings
	i++)
	{
		iBattNo = iStartBattNo + i;
		ClearOneStringAllAlarm(iBattNo);
	}

	//4.One by One Analyse The Alarm Sig
	for(i = 0; i < iAlarmNum; i++)
	{
		//Get Element of Alarm
		SMBRC_AnalyseAlarmFormat(pbyRecvData,
			&byParmNo,
			&AlarmType,
			&StringNum,
			&CellNum,
			&iAlarmSigValue);

		SMBRC_SetAlarm(byAddr, byParmNo, AlarmType, StringNum, CellNum, iAlarmSigValue);
		pbyRecvData += SMBRC_ALARM_ASCII_HEX_LENGH;//Jump to next Alarm signal!!
	}

	return TRUE;
}
/*=============================================================================*
* FUNCTION: SMBRC _SampleAlarmCmd44
* PURPOSE : Sampling Data from SMBRC By 0x44,then array  data to aRoughData
* INPUT:	 	
*   
*
* RETURN:
*     TRUE:OK	FALSE:error
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMBRC_SampleAlarmCmd44(INT32 iAddr, RS485_DEVICE_CLASS*  pSMBRCDeviceClass)
{
	INT32	iReadLengh = 0;
	INT32	iCmdStat = SMBRC_SEND_CMD_ERROR;
	BYTE	abyRcvBuf[SMBRC_RECV_BUF_MAX_NUM] = {0};
	HANDLE	hComm = NULL;
	hComm	 = pSMBRCDeviceClass->pCommPort->hCommPort;
	RS485_DRV*	pPort = (RS485_DRV *)hComm;
	INT32		fd = pPort->fdSerial;

	memset(abyRcvBuf, 0, SMBRC_RECV_BUF_MAX_NUM);

	RS485_ClrBuffer(hComm);

	iCmdStat = SMBRC_PackAndSend(hComm,
				iAddr,
				SMBRC_UNUSED_CMD_ID,
				SMBRC_GET_ALARM_CID2,
				18,
				SMBRC_UNUSED_PARAMETER);

	if (SMBRC_SEND_CMD_OK == iCmdStat)
	{
		if (SMBRC_RS485WaitReadable(fd, 500))
		{
			Sleep(10);

			if (SMBRC_ReadData(hComm, abyRcvBuf, &iReadLengh, SMBRC_RECV_BUF_MAX_NUM))
			{
				if (bValidRecvData((BYTE)iAddr,
					SMBRC_EQUIP_TYPE_CID1,
					SMBRC_CID2_NORMAL_RTN,
					abyRcvBuf,
					iReadLengh))
				{
					SMBRC_UnpackAlarmCmd44(iAddr, abyRcvBuf, iReadLengh);
					return TRUE;
				}
				else
				{
					//printf(" ::::::::CMD44 invalid data :::::\n");
					return FALSE;
				}

			}
			else
			{
				//printf("::::::: CMD44	Read Data error :::::::\n");
				return FALSE;
			}
		}
		else
		{
			//printf(" :::::::: CMD44	No Response Time Out :::::\n");
			return FALSE;
		}
	}
	else
	{
		//Send CMD error!!!!
		return FALSE;
	}
}
/*=============================================================================*
* FUNCTION: SMBRC	_UnpackAlarmThresCmdE4
* PURPOSE : Analyse The Data then arrange to The RoughData 
* INPUT:	 	
*   
*
* RETURN:
*        Error:FALSE		ok:TRUE
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 SMBRC_UnpackAlarmThresCmdE4(int byAddr, BYTE* pbyRcvBuf, INT32 iLengh)
{

	BYTE*		pbyRecvData = NULL;
	UINT		iTemperature = 0;
	BYTE		byVirtualAddr = byAddr - SMBRC_ADDR_START;
	ASSERT(byVirtualAddr < SMBRC_ADDR_MAX__NUM);
	ASSERT(pbyRcvBuf);

	pbyRecvData = pbyRcvBuf + SMBRC_DATAINFO_START;
	UNUSED(iLengh);

	//1.Cell Voltage high(mv)
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_CELL_VOLTAGE_H].fValue 
		= (float)FourBytesToUnit(pbyRecvData) * SMBRC_MILLI_SCALE;
	pbyRecvData += 4;

	//2.Cell voltage low(mv)
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_CELL_VOLTAGE_L].fValue 
		= (float)FourBytesToUnit(pbyRecvData) * SMBRC_MILLI_SCALE;
	pbyRecvData += 4;

	//3.Cell temperature high
	iTemperature = FourBytesToUnit(pbyRecvData);
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_CELL_TEMPERATURE_H].fValue 
		= SMBRC_ConvertTempt(iTemperature);
	pbyRecvData += 4;

	//4.Cell temperature low
	iTemperature = FourBytesToUnit(pbyRecvData);
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_CELL_TEMPERATURE_L].fValue 
		= SMBRC_ConvertTempt(iTemperature);
	pbyRecvData += 4;


	//5.Overall voltage high(mv)
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_OVERRAL_VOLT_H].fValue 
		= (float)FourBytesToUnit(pbyRecvData) * SMBRC_MILLI_SCALE;
	pbyRecvData += 4;

	//6.Overall voltage low(mv)
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_OVERAL_VOLT_L].fValue 
		= (float)FourBytesToUnit(pbyRecvData) * SMBRC_MILLI_SCALE;
	pbyRecvData += 4;


	//7.String current high(A)
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_STRING_CURR_H].fValue 
		= (float)FourBytesToUnit(pbyRecvData);
	pbyRecvData += 4;

	//8.String current low(A)
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_STRING_CUUR_L].fValue 
		= (float)FourBytesToUnit(pbyRecvData);
	pbyRecvData += 4;


	//9.Float current high(mA)
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_FLOAT_CURR_H].fValue 
		= (float)FourBytesToUnit(pbyRecvData) * SMBRC_MILLI_SCALE;
	pbyRecvData += 4;
	
	//10.Float current low(mA)
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_FLOAT_CURR_L].fValue 
		= (float)FourBytesToUnit(pbyRecvData) * SMBRC_MILLI_SCALE;
	pbyRecvData += 4;


	//11.Ripple current high(A)
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_RIPPLE_CURR_H].fValue 
		= (float)FourBytesToUnit(pbyRecvData);
	pbyRecvData += 4;


	//12.Ripple current low(A)
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_RIPPLE_CURR_L].fValue 
		= (float)FourBytesToUnit(pbyRecvData);
	pbyRecvData += 4;


	//13.Ambient temperature high
	iTemperature = FourBytesToUnit(pbyRecvData);
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_AMBT_TEMP_H].fValue 
		= SMBRC_ConvertTempt(iTemperature);
	pbyRecvData += 4;

	//14.Ambient temperature low
	iTemperature = FourBytesToUnit(pbyRecvData);
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_AMBT_TEMP_L].fValue 
		= SMBRC_ConvertTempt(iTemperature);
	pbyRecvData += 4;

	//15.High resistance uoh
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_H_RESISTANCE].fValue 
		= (float)FourBytesToUnit(pbyRecvData) * SMBRC_MICRON_SCALE;
	pbyRecvData += 4;

	//16.Low resistance uoh
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_L_RESISTANCE].fValue 
		= (float)FourBytesToUnit(pbyRecvData) * SMBRC_MICRON_SCALE;
	pbyRecvData += 4;

	//17.High intercell uoh
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_H_INTERCELL].fValue 
		= (float)FourBytesToUnit(pbyRecvData) * SMBRC_MICRON_SCALE;
	pbyRecvData += 4;


	//18.Low intercell uoh
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_L_INTERCELL].fValue 
		= (float)FourBytesToUnit(pbyRecvData) * SMBRC_MICRON_SCALE;
	pbyRecvData += 4;


	//19.High cell to ambient
	iTemperature = FourBytesToUnit(pbyRecvData);
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_H_CELL_TO_AMBT].fValue 
		= SMBRC_ConvertTempt(iTemperature);
	pbyRecvData += 4;

	//20.Low cell to ambient
	iTemperature = FourBytesToUnit(pbyRecvData);
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_L_CELL_TO_AMBT].fValue 
		= SMBRC_ConvertTempt(iTemperature);
	pbyRecvData += 4;

	//21.Discharge trigger level(A)
	g_SmbrcSampData.aRoughUnitData[byVirtualAddr][SMBRC_DISCHARGE_TRIGGER_LEVEL].fValue 
		= (float)FourBytesToUnit(pbyRecvData);
	pbyRecvData += 4;

	return 0;
}

/*=============================================================================*
* FUNCTION: SMBRC _SampleAlarmThresCmdE4
* PURPOSE : Sampling Data from SMBRC By 0x44,then array  data to aRoughData
* INPUT:	 	
*			//2010/03/04��Ӳ���ͬ��������
*
* RETURN:
*     TRUE:OK	FALSE:error
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMBRC_SampleAlarmThresCmdE4(INT32 iAddr, RS485_DEVICE_CLASS*  pSMBRCDeviceClass)
{
	INT32	iReadLengh;
	INT32	iCmdStat = SMBRC_SEND_CMD_ERROR;
	BYTE	abyRcvBuf[SMBRC_RECV_BUF_MAX_NUM + 10] = {0};
	HANDLE	hComm = NULL;
	hComm	 = pSMBRCDeviceClass->pCommPort->hCommPort;
	RS485_DRV*	pPort = (RS485_DRV *)hComm;
	INT32		fd = pPort->fdSerial;

	memset(abyRcvBuf, 0, SMBRC_RECV_BUF_MAX_NUM);

	RS485_ClrBuffer(hComm);

	iCmdStat = SMBRC_PackAndSend(hComm,
		iAddr,
		SMBRC_UNUSED_CMD_ID,
		SMBRC_CID2_ALARM_THRES_CID2,
		18,
		SMBRC_UNUSED_PARAMETER);

	if (SMBRC_SEND_CMD_OK == iCmdStat)
	{
		if (SMBRC_RS485WaitReadable(fd, 1500))
		{
			Sleep(10);
			if (SMBRC_ReadData(hComm, abyRcvBuf, &iReadLengh, SMBRC_RECV_BUF_MAX_NUM))
			{
				if (bValidRecvData(iAddr,
					SMBRC_EQUIP_TYPE_CID1,
					SMBRC_CID2_NORMAL_RTN,
					abyRcvBuf,
					iReadLengh))
				{
					SMBRC_UnpackAlarmThresCmdE4(iAddr, abyRcvBuf, iReadLengh);
					return TRUE;
				}
				else
				{
					//printf(" ::::::::CMDE4 invalid data :::::\n");
					return FALSE;
				}

			}
			else
			{
				//printf("::::::: CMDE4	Read Data error :::::::\n");
				return FALSE;
			}
		}
		else
		{
			//printf(" :::::::: CMDE4	No Response Time Out :::::\n");
			return FALSE;
		}
	}
	else
	{
		//printf("\n	CMDE4 Sen error		!!!!\n");
		return FALSE;
	}
}

/*=============================================================================*
* FUNCTION: SMBRC	_StringToUint
* PURPOSE : Eight Ascii-hex convert to unsigned  integer
* INPUT:	 	
*   
* RETURN:
*        UINT
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
UINT SMBRC_StringToUint(BYTE* strInput)
{
	BYTE bTempData[5];
	ASSERT(strInput);
	bTempData[0] = SMBRC_MergeAsc(strInput);
	bTempData[1] = SMBRC_MergeAsc(strInput + 2);
	bTempData[2] = SMBRC_MergeAsc(strInput + 4);
	bTempData[3] = SMBRC_MergeAsc(strInput + 6);

	UINT uiValue = (((UINT)(bTempData[0])) << 24)
		+ (((UINT)(bTempData[1])) << 16)
		+ (((UINT)(bTempData[2])) << 8)
		+ ((UINT)(bTempData[3]));

	return uiValue;
}
/*=============================================================================*
* FUNCTION: SMBRC	_GetResistSigOfString
* PURPOSE : Get Resistance signal of one battery string and setting to 
*			newest space!!
* INPUT:	 	
*   
* RETURN:
*        UINT
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void SMBRC_GetResistSigOfString(BYTE* pbyRecvData,
				SMBRC_RST_STRING_TEST_DATA*	pstStringSigs,
				BYTE** ppRecvData,
				int byAddr,
				UINT uiStartTime,
				int bySequenceOfAddr)
{
	INT32	i;
	INT32	iCellNum;
	UNUSED(pstStringSigs);
	UNUSED(uiStartTime);

	ASSERT(bySequenceOfAddr <= 4);
	int iStartBattNo = 0;
	int iBattNo = 0;
	iStartBattNo = SMBRC_GetBattNo((byAddr - SMBRC_ADDR_START));
	iBattNo = iStartBattNo + bySequenceOfAddr;
	ASSERT(iStartBattNo < SMBRC_STRING_MAX_NUM);

	if (iBattNo >= SMBRC_STRING_MAX_NUM)
	{
		return;
	}

	//1.Get Number of Cells!! 
	iCellNum = SMBRC_MergeAsc(pbyRecvData);
	if (iCellNum >= SMBRC_CELL_MAX_NUM)
	{
		iCellNum = SMBRC_CELL_MAX_NUM;
	}
	pbyRecvData += 2;

	//3.Get Cell resistance XX
	for (i = 0; (i < iCellNum) && (i < SMBRC_CELL_MAX_NUM); i++)
	{
		//�� ohm
		g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_TEST_RESISTANCE_1 + i].fValue 
			= (float)FourBytesToUnit(pbyRecvData) * SMBRC_MICRON_SCALE;
		pbyRecvData += 4;
	}

	

	//4.Get Cell intercell XX
	for (i = 0; (i < (iCellNum - 1)) && (i < SMBRC_CELL_MAX_NUM); i++)
	{
		//�� ohm
		g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_TEST_INTERCELL_1 + i].fValue 
			= (float)FourBytesToUnit(pbyRecvData) * SMBRC_MICRON_SCALE;
		pbyRecvData += 4;
	}

	//5.Set  inexistence  Cell sig  -99999!!
	RS485_VALUE stTemp;
	stTemp.iValue = SMBRC_SIG_INVALID_VALUE;
	//2.2Э�飬
	for (i = iCellNum; i < SMBRC_CELL_MAX_NUM; i++)
	{	
		g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_TEST_INTERCELL_1 + i].fValue = stTemp.fValue;
		g_SmbrcSampData.aRoughStringData[iBattNo][SMBRC_TEST_RESISTANCE_1 + i].fValue  = stTemp.fValue;
	}


	//6.return Next String Data Point
	(*ppRecvData) = pbyRecvData;

}
/*=============================================================================*
* FUNCTION: SMBRC	_ClrResistTestInfo
* PURPOSE : Refresh the Resist Test information
* INPUT:	 	
*   
*
* RETURN:
*        Error:FALSE		ok:TRUE
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
/*=============================================================================*
* FUNCTION: SMBRC _Exit
* PURPOSE : Delete the SMBRC sampling controler space
* INPUT:	 	
*   
*
* RETURN:
*        Error:FALSE		ok:TRUE
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void SMBRC_Exit(RS485_DEVICE_CLASS*  pSMBRCDeviceClass)
{
	UNUSED(pSMBRCDeviceClass);
	//SMBRC_ClrResistTestInfo();
	return;
}

/*=============================================================================*
* FUNCTION: SMBRC _bNeedGetResistTestData
* PURPOSE : Check the Resistance status ,
*			if anyone status in progress,then Wait a Moment Get the test Data!!
* INPUT:	 	
*   
*
* RETURN:
*        Error:FALSE		ok:TRUE
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
static BOOL SMBRC_bNeedGetResistTestData()
{
	INT32	i;
	//INT32	byActualAddr;
	//static BOOL	bFirstRunning = TRUE;
	static BOOL	bInTestProgress = FALSE;


	for (i = 0; i < SMBRC_ADDR_MAX__NUM; i++)
	{
		if (SMBRC_STAT_EXIST == 
			g_SmbrcSampData.aRoughUnitData[i][SMBRC_UNIT_EXIST_STAT].iValue)
		{
			if (SMBRC_R_TEST_IN_PROGERES == 
				g_SmbrcSampData.aRoughUnitData[i][SMBRC_HARDWARE_OPT_STAT].iValue)
			{
				bInTestProgress = TRUE;
				//Needn't Get the resistance test data,Wait a Moment!!!!
				return FALSE;
			}
			else
			{
				//Don't process!!
			}
		}
		else
		{
			//Don't process!!
		}
	}

	if (bInTestProgress)
	{
		/*
			����������ʱ���ϲ�ƥ�䣬���ԼӸ���ʱ��
			��Ϊ�п��ܣ���ز���̫���ˣ�û��������ַ���ý��е�ز���
			���Ѿ������ˡ�
		*/
		bInTestProgress = FALSE;
		//SMBRC_ClrResistTestInfo();
		return TRUE;
	}
	else
	{
		//the all Smbrc not resistance test !!
		return FALSE;
	}

}

/*=============================================================================*
* FUNCTION: SMBRC _UnpackResistCmd44
* PURPOSE : Analyse The Data then Record it
* INPUT:	 	
*   
*
* RETURN:
*        Error:FALSE		ok:TRUE
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL INT32 SMBRC_UnpackResistCmd42(int byAddr, BYTE* pbyRcvBuf, INT32 iLengh)
{
	UINT		uiStartTime;
	BYTE *		pByRecvData;
	BYTE*		pNextStringRcvData;
	int		iStringSqcNo;
	INT32		iStringNum;
	//INT32		iStartBattNo;
	INT32		iVirtualAddr;
	//INT32		iOldTotalStringNum;

	SMBRC_RST_STRING_TEST_DATA*		pstCurrentString = NULL;

	iVirtualAddr = byAddr - SMBRC_ADDR_START;

	UNUSED(iLengh);
	//1.Get the resistance start time!!
	pByRecvData = pbyRcvBuf + SMBRC_DATAINFO_START;
	uiStartTime = SMBRC_StringToUint(pByRecvData);
	pByRecvData += 8;
	if ((0xffffffff == uiStartTime) || (0x00000000 == uiStartTime))
	{
		//There isn't Resistance test record 
		return -1;
	}

	//2. Get testing String number!!
	iStringNum		 = SMBRC_MergeAsc(pByRecvData);//value is [0--4]
	pByRecvData		 += 2;
	if (iStringNum >= 4)
	{
		iStringNum = 4;
	}

	//4.Get String N Sig XX Infor  One by One
	//2010-07-20 �޸ģ���Ϊ��������жδ���������
	for (iStringSqcNo = 0; iStringSqcNo < iStringNum && (iStringSqcNo < 4); iStringSqcNo++)
	{
		switch (iStringSqcNo)
		{
		case 0:
			SMBRC_GetResistSigOfString(pByRecvData, 
						pstCurrentString, 
						&pNextStringRcvData, 
						byAddr,
						uiStartTime,
						iStringSqcNo);
			pByRecvData = pNextStringRcvData;
			break;

		case 1:
			SMBRC_GetResistSigOfString(pByRecvData, 
							pstCurrentString, 
							&pNextStringRcvData, 
							byAddr,
							uiStartTime,
							iStringSqcNo);
			pByRecvData = pNextStringRcvData;
			break;

		case 2:
			SMBRC_GetResistSigOfString(pByRecvData, 
							pstCurrentString, 
							&pNextStringRcvData,
							byAddr,
							uiStartTime,
							iStringSqcNo);
			pByRecvData = pNextStringRcvData;
			break;

		case 3:
			SMBRC_GetResistSigOfString(pByRecvData, 
							pstCurrentString, 
							&pNextStringRcvData,
							byAddr,
							uiStartTime,
							iStringSqcNo);
			pByRecvData = pNextStringRcvData;
			break;

		default:
			break;
		}
	}

	return TRUE;
}

/*=============================================================================*
* FUNCTION: SMBRC _SampleGetResistCmd42
* PURPOSE : Get Resistance test Data from SMBRC By 0x42.
* INPUT:	 	
*   
*
* RETURN:
*     TRUE:OK	FALSE:error
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMBRC_SampleGetResistCmd42(INT32 iAddr, RS485_DEVICE_CLASS*  pSMBRCDeviceClass)
{
	INT32	iReadLengh;
	INT32	iCmdStat = SMBRC_SEND_CMD_ERROR;
	BYTE	abyRcvBuf[SMBRC_RECV_BUF_MAX_NUM] = {0};
	HANDLE	hComm;
	hComm	 = pSMBRCDeviceClass->pCommPort->hCommPort;
	RS485_DRV*	pPort				= (RS485_DRV *)hComm;
	INT32		fd					= pPort->fdSerial;

	memset(abyRcvBuf, 0, SMBRC_RECV_BUF_MAX_NUM);

	RS485_ClrBuffer(hComm);

	iCmdStat = SMBRC_PackAndSend(hComm,
		iAddr,
		SMBRC_GET_RESISTANCE,
		SMBRC_GET_TEST_DATA_CID2,
		20,
		SMBRC_UNUSED_PARAMETER);


	if (SMBRC_SEND_CMD_OK == iCmdStat)
	{
		if (SMBRC_RS485WaitReadable(fd, 500))
		{
			Sleep(10);
			if (SMBRC_ReadData(hComm, abyRcvBuf, &iReadLengh, SMBRC_RECV_BUF_MAX_NUM))
			{
				if (bValidRecvData(iAddr,
					SMBRC_EQUIP_TYPE_CID1,
					SMBRC_CID2_NORMAL_RTN,
					abyRcvBuf,
					iReadLengh))
				{
					SMBRC_UnpackResistCmd42(iAddr, abyRcvBuf, iReadLengh);
					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		//Send CMD error!!!!
		return FALSE;
	}
}

/*=============================================================================*
* FUNCTION: SMBRC _GetAllResistData
* PURPOSE : Get All Resistance test Data  By 0x42.
* INPUT:	 	
*   
*
* RETURN:
*     TRUE:OK	FALSE:error
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void SMBRC_GetAllResistData(RS485_DEVICE_CLASS*  pSMBRCDeviceClass)
{
	INT32	i,j;
	BOOL	bSampOk = FALSE;
	INT32	byActualAddr = 0;

	//printf("\n	This is into SMBRC _GetAllResistData function \n");

	for (i = 0; i < SMBRC_ADDR_MAX__NUM; i++)
	{
		if (SMBRC_STAT_EXIST == 
			g_SmbrcSampData.aRoughUnitData[i][SMBRC_UNIT_EXIST_STAT].iValue)
		{
			byActualAddr = i + SMBRC_ADDR_START;
			for (j = 0; j < SMBRC_SCAN_TRY_TIMES; j++)
			{
				bSampOk = SMBRC_SampleGetResistCmd42(byActualAddr, pSMBRCDeviceClass);

				if (bSampOk)
				{
					//printf("	Get Resist Addr=%d is Ok \n",byActualAddr);
					break;
				}
				else
				{	
					Sleep(150);
					//try again until times out
				}
			}	
		}
		else
		{
			//Needn't process!!
		}
	}

	//SMBRC_SetResistData();
	//printf("	Get Resist GetAll ResistData was Finished	\n");
	return;
}

/*=============================================================================*
* FUNCTION: SMBRC _IncCommBreakTimes
* PURPOSE : Increase the communication interrupt times of the SMBRC with
*            address byAddr
* INPUT:	 	
*   
*
* RETURN:
*     TRUE:OK	FALSE:error
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void SMBRC_IncCommBreakTimes(INT32 iVirtualAddr,RS485_DEVICE_CLASS*  pSMBRCDeviceClass)
{
	INT32	i;
	INT32	iStarBattNo;
	INT32	iExistBattNum;
	INT32	iCommFailTimes;
	UNUSED(pSMBRCDeviceClass);

	iCommFailTimes = g_SmbrcSampData.aRoughUnitData[iVirtualAddr][SMBRC_UNIT_COMMFAIL_TIMES].iValue;

	if (iCommFailTimes < SMBRC_COMM_FAIL_TRY_TIMES)
	{
		g_SmbrcSampData.aRoughUnitData[iVirtualAddr][SMBRC_UNIT_COMMFAIL_TIMES].iValue++;
	}
	else
	{
		//1.Unit Comm status
		g_SmbrcSampData.aRoughUnitData[iVirtualAddr][SMBRC_UNIT_COMM_STAT].iValue 
			= SMBRC_COMM_STAT_FAIL;
		//2.String Comm status
		iStarBattNo = SMBRC_GetBattNo(iVirtualAddr);
		iExistBattNum = g_SmbrcSampData.aRoughUnitData[iVirtualAddr][SMBRC_UNIT_EXIST_STRING_NUM].iValue;
		for (i = iStarBattNo; (i < iExistBattNum) && (i < SMBRC_STRING_MAX_NUM); i++)
		{
			g_SmbrcSampData.aRoughStringData[i][SMBRC_STRING_COMM_STAT].iValue = SMBRC_COMM_STAT_FAIL;
		}
	}

	return;	
}
/*=============================================================================*
* FUNCTION: SMBRC _ClrCommBreakTimes
* PURPOSE :  Clear the communication interrupt times of the SMBRC with
*            address byAddr
* INPUT:	 	
*   
*
* RETURN:
*     TRUE:OK	FALSE:error
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void SMBRC_ClrCommBreakTimes(INT32 iVirtualAddr,RS485_DEVICE_CLASS*  pSMBRCDeviceClass)
{
	INT32	i;
	INT32	iStarBattNo;
	INT32	iExistBattNum;
	UNUSED(pSMBRCDeviceClass);

	ASSERT(iVirtualAddr < SMBRC_ADDR_MAX__NUM);

	if (iVirtualAddr >= SMBRC_ADDR_MAX__NUM)
	{
		return;
	}
	//1.Unit Comm status
	g_SmbrcSampData.aRoughUnitData[iVirtualAddr][SMBRC_UNIT_COMMFAIL_TIMES].iValue = 0;
	g_SmbrcSampData.aRoughUnitData[iVirtualAddr][SMBRC_UNIT_COMM_STAT].iValue = SMBRC_COMM_STAT_OK;

	//2.String Comm status
	iStarBattNo = SMBRC_GetBattNo(iVirtualAddr);
	iExistBattNum = g_SmbrcSampData.aRoughUnitData[iVirtualAddr][SMBRC_UNIT_EXIST_STRING_NUM].iValue;
	for (i = iStarBattNo; (i < iExistBattNum) && (i < SMBRC_STRING_MAX_NUM); i++)
	{
		g_SmbrcSampData.aRoughStringData[i][SMBRC_STRING_COMM_STAT].iValue = SMBRC_COMM_STAT_OK;
	}
	return;
}
/*=============================================================================*
* FUNCTION: SMBRC _NotificationWebOrLcd
* PURPOSE :  Notification Web and Lcd to Get the Resistance test data
* INPUT:	 	
*   
*
* RETURN:
*     TRUE:OK	FALSE:error
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void  SMBRC_NotificationWebOrLcd()
{
	static DWORD s_dwChangeForWeb = 0;

	if (SMBRC_STAT_EXIST != g_SmbrcSampData.aRoughGroupData[SMBRC_G_EXIST_STAT].iValue)
	{
		return;
	}

	//for Web
	s_dwChangeForWeb++;
	if(s_dwChangeForWeb >= 255)
	{
		s_dwChangeForWeb = 0;
	}

	//SetDwordSigValue(SMBRC_GROUP_EQUIP_ID,		
	//				 SMBRC_SIG_SAMPLING_TYPE,		
	//				 SMBRC_SIG_WEBCFG_CHANGE_ID,		
	//				 s_dwChangeForWeb,
	//				 "SMBRC485");


	////for LCD
	//SetDwordSigValue(SMBRC_GROUP_EQUIP_ID,				
	//				 SMBRC_SIG_SAMPLING_TYPE,			
	//				 SMBRC_SIG_LCDCFG_CHANGE_ID,		
	//				 1,
	//				 "SMBRC485");

	SetDwordSigValue(SMDU_BRC_G_EQUIP_ID,		
		SMBRC_SIG_SAMPLING_TYPE,		
		SMBRC_SIG_WEBCFG_CHANGE_ID,		
		s_dwChangeForWeb,
		"SMBRC485");


	//for LCD
	SetDwordSigValue(SMDU_BRC_G_EQUIP_ID,				
		SMBRC_SIG_SAMPLING_TYPE,			
		SMBRC_SIG_LCDCFG_CHANGE_ID,		
		1,
		"SMBRC485");
	return ;
}

static BOOL CellAlarmValueIsHight(INT32 SigValue)
{
	if (SMBRC_ALARM_TYPE_HIGH == SigValue)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
static BOOL SigIsAlarm(INT32 SigValue)
{
	//Reference to the protocol 6.5 Get current alarm (0x44)
	if (SMBRC_ALARM_TYPE_LOW == SigValue || SMBRC_ALARM_TYPE_HIGH == SigValue)
	{
		return TRUE;
	}

	return FALSE;
}
/*=============================================================================*
* FUNCTION: SMBRC_NotSampSigProc
* PURPOSE : Calculate The not sampling data arrange to the byRoughData 
* INPUT:		
*
* RETURN:
*	SMBRC_HAVE_CELL_VOLT_ALARM,
*	SMBRC_HAVE_CELL_TEMPT_ALARM,
*	SMBRC_HAVE_CELL_RESIST_ALARM,
*	SMBRC_HAVE_CELL_INTER_ALARM,
*	SMBRC_HAVE_CELL_AMBIENT_ALARM,
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
static void SMBRC_NotSampSigProc()
{
	INT32	n, m;

	//string sig
	for (m = 0; m < SMBRC_STRING_MAX_NUM; m++)
	{
		//Every Time, First Set Signals not Alarm!!!
		g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_VOLT_ALARM_L].iValue = FALSE;
		g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_TEMPT_ALARM_L].iValue = FALSE;
		g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_RESIST_ALARM_L].iValue = FALSE;
		g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_INTER_ALARM_L].iValue = FALSE;
		g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_AMBIENT_ALARM_L].iValue = FALSE;

		g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_VOLT_ALARM_H].iValue = FALSE;
		g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_TEMPT_ALARM_H].iValue = FALSE;
		g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_RESIST_ALARM_H].iValue = FALSE;
		g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_INTER_ALARM_H].iValue = FALSE;
		g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_AMBIENT_ALARM_H].iValue = FALSE;

		//If The m  Battery String have Alarm,The Signals was Setted !! 
		if (SMBRC_STAT_EXIST == g_SmbrcSampData.aRoughStringData[m][SMBRC_STRING_EXIST_STAT].iValue)
		{
			for (n = SMBRC_CELL_1_VOLT_ALARM; n <= SMBRC_CELL_24_VOLT_ALARM; n++)
			{
				if (SigIsAlarm(g_SmbrcSampData.aRoughStringData[m][n].iValue))
				{
					if (CellAlarmValueIsHight(g_SmbrcSampData.aRoughStringData[m][n].iValue))
					{
						g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_VOLT_ALARM_H].iValue = TRUE;
					}
					else
					{
						g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_VOLT_ALARM_L].iValue = TRUE;
					}

					break;
				}
				else
				{
					//No process!!
				}
			}

			for(n = SMBRC_CELL_1_TEMPT_ALARM; n <= SMBRC_CELL_24_TEMPT_ALARM; n++)
			{
				if (SigIsAlarm(g_SmbrcSampData.aRoughStringData[m][n].iValue))
				{
					if (CellAlarmValueIsHight(g_SmbrcSampData.aRoughStringData[m][n].iValue))
					{
						g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_TEMPT_ALARM_H].iValue = TRUE;
					}
					else
					{
						g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_TEMPT_ALARM_L].iValue = TRUE;
					}

					break;
				}
				else
				{
					//No process!!
				}
			}

			for (n = SMBRC_CELL_1_RESIST_ALARM; n <= SMBRC_CELL_24_RESIST_ALARM; n++)
			{
				if (SigIsAlarm(g_SmbrcSampData.aRoughStringData[m][n].iValue))
				{
					if (CellAlarmValueIsHight(g_SmbrcSampData.aRoughStringData[m][n].iValue))
					{
						g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_RESIST_ALARM_H].iValue = TRUE;
					}
					else
					{
						g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_RESIST_ALARM_L].iValue = TRUE;
					}

					break;
				}
				else
				{
					//No process!!
				}
			}

			for (n = SMBRC_CELL_1_INTER_ALARM; n <= SMBRC_CELL_24_INTER_ALARM; n++)
			{
				if (SigIsAlarm(g_SmbrcSampData.aRoughStringData[m][n].iValue))
				{
					if (CellAlarmValueIsHight(g_SmbrcSampData.aRoughStringData[m][n].iValue))
					{
						g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_INTER_ALARM_H].iValue = TRUE;			
					}
					else
					{
						g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_INTER_ALARM_L].iValue = TRUE;
					}
					break;
				}
				else
				{
					//No process!!
				}
			}

			for (n = SMBRC_CELL_1_TO_AMBIENT_ALARM; n <= SMBRC_CELL_24_TO_AMBIENT_ALARM; n++)
			{
				if (SigIsAlarm(g_SmbrcSampData.aRoughStringData[m][n].iValue))
				{
					if (CellAlarmValueIsHight(g_SmbrcSampData.aRoughStringData[m][n].iValue))
					{
						g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_AMBIENT_ALARM_H].iValue = TRUE;	
					}
					else
					{
						g_SmbrcSampData.aRoughStringData[m][SMBRC_HAVE_CELL_AMBIENT_ALARM_L].iValue = TRUE;
					}
					break;
				}
				else
				{
					//No process!!
				}
			}
		}
		else
		{
			//No process!!!
		}
	}

	return;
}

//2010-08-01
void ClearAllAlarm()
{
	//��������и澯��Ȼ�����44����澯�ź���䣬Ȼ��Stuff��
	INT32	i,j;

	for (i = 0; i < SMBRC_STRING_MAX_NUM; i++)
	{
		for (j = SMBRC_OVERALL_VOLT_ALARM; j <= SMBRC_CELL_24_TO_AMBIENT_ALARM; j++)
		{
			g_SmbrcSampData.aRoughStringData[i][j].iValue = SMBRC_SIG_INVALID_VALUE;
		}
	}

	return;
}

static void ClearOneStringAllAlarm(int iStringNo)
{
	//��������и澯��Ȼ�����44����澯�ź���䣬Ȼ��Stuff��
	INT32	j;
	if (iStringNo > SMBRC_STRING_MAX_NUM)
	{
		return;
	}
	ASSERT(iStringNo < SMBRC_STRING_MAX_NUM);
	for (j = SMBRC_OVERALL_VOLT_ALARM; j <= SMBRC_CELL_24_TO_AMBIENT_ALARM; j++)
	{
		g_SmbrcSampData.aRoughStringData[iStringNo][j].iValue = SMBRC_SIG_INVALID_VALUE;
	}

	return;
}

static void SetUrgencySamp0XE4Flag(BOOL bSetSamp0XE4Flag)
{
	g_SmbrcSampData.bNeedSampCmd0XE4 = bSetSamp0XE4Flag;
}

BOOL bNeedUrgencySamp0XE4()
{
	if (g_SmbrcSampData.bNeedSampCmd0XE4)
	{
		SetUrgencySamp0XE4Flag(FALSE);
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

BOOL bCtrlSampThresholdFrequency()
{
	static INT32	siSampQueryCount = 0;
	static INT32	siFirstRungFlag = TRUE;

	if (siFirstRungFlag)
	{
		siFirstRungFlag = FALSE;
		return TRUE;
	}

	if (bNeedUrgencySamp0XE4())
	{
		return TRUE;
	}
	else
	{
		//No process!!
	}

	if (siSampQueryCount < 10)
	{
		siSampQueryCount++;
		return FALSE;
	}
	else
	{
		siSampQueryCount = 0;
		return TRUE;
	}
}
/*=============================================================================*
* FUNCTION: SMBRC_ Sample
* PURPOSE : Sampling Data from SMBRC,then array  data to aRoughData
* INPUT:	 	
*   
*
* RETURN:
*     0
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMBRC_Sample(RS485_DEVICE_CLASS*  pSMBRCDeviceClass)
{
	INT32		i;
	BYTE		byActualAddr = 0;	

	if (SMBRC_NeedCommProc())
	{
		SMBRC_ReopenPort();
	}

	if(((RS485_DEVICE_CLASS*)pSMBRCDeviceClass)->bNeedReconfig)
	{
		((RS485_DEVICE_CLASS*)pSMBRCDeviceClass)->bNeedReconfig = FALSE;
		SMBRC_Reconfig((RS485_DEVICE_CLASS*)pSMBRCDeviceClass);

		ClearAllAlarm();

		SMBRC_GetAllResistData((RS485_DEVICE_CLASS*)pSMBRCDeviceClass);
		SMBRC_NotificationWebOrLcd();
		return TRUE;
	}


	for (i = 0; i < SMBRC_ADDR_MAX__NUM; i++)
	{
		if (SMBRC_STAT_EXIST == 
			g_SmbrcSampData.aRoughUnitData[i][SMBRC_UNIT_EXIST_STAT].iValue)
		{
			//Battery  testing	return SMBRC;
			if (bBattTesting(TRUE))
			{
				//printf("\nThis is Batt Testing!!!!!!!\n");
				return TRUE;
			}
			else
			{
				//Continue!! Sampling SMBRC!!
			}

			//Get actual address!!
			byActualAddr = i + SMBRC_ADDR_START;
			if (!SMBRC_SampleGetAllCmd41(byActualAddr, (RS485_DEVICE_CLASS*)pSMBRCDeviceClass))
			{
				if (!SMBRC_SampleGetAllCmd41(byActualAddr, (RS485_DEVICE_CLASS*)pSMBRCDeviceClass))
				{
					SMBRC_IncCommBreakTimes(i, (RS485_DEVICE_CLASS*)pSMBRCDeviceClass);
				}
				else
				{
					//Continue
					SMBRC_ClrCommBreakTimes(i, (RS485_DEVICE_CLASS*)pSMBRCDeviceClass);
				}
			}
			else
			{
				SMBRC_ClrCommBreakTimes(i, (RS485_DEVICE_CLASS*)pSMBRCDeviceClass);
			}
			//printf("\n\n");
			if (!SMBRC_SampleAlarmCmd44(byActualAddr, (RS485_DEVICE_CLASS*)pSMBRCDeviceClass))
			{
				SMBRC_SampleAlarmCmd44(byActualAddr, (RS485_DEVICE_CLASS*)pSMBRCDeviceClass);
			}
			else
			{
				//No process
			}
		}
		else
		{
			//The Hardware Inexistence !!
		}
	}

	//For Param Unify Get the Alarm threshold Sig Value is from existance address SMBRC.
	if (bCtrlSampThresholdFrequency())
	{
		for (i = 0; i < SMBRC_ADDR_MAX__NUM; i++)
		{
			if (SMBRC_STAT_EXIST == 
				g_SmbrcSampData.aRoughUnitData[i][SMBRC_UNIT_EXIST_STAT].iValue)
			{
				Sleep(50);
				//Get actual address!!
				byActualAddr = i + SMBRC_ADDR_START;
				if (!SMBRC_SampleAlarmThresCmdE4(byActualAddr, (RS485_DEVICE_CLASS*)pSMBRCDeviceClass))
				{
					SMBRC_SampleAlarmThresCmdE4(byActualAddr, (RS485_DEVICE_CLASS*)pSMBRCDeviceClass);
				}
				else
				{
					//No process
				}			
			}
			else
			{
				//No process!
			}
		}//end for
		Sleep(200);
	}
	else
	{
		//Wait!,No process!!
	}

	SMBRC_NotSampSigProc();

	if(SMBRC_bNeedGetResistTestData())
	{
		//Get ALL Resist test data
		SMBRC_GetAllResistData((RS485_DEVICE_CLASS*)pSMBRCDeviceClass);
		SMBRC_NotificationWebOrLcd();
	}
	else
	{
		//Needn't process!!!
	}

	return 0;
}
/*=============================================================================*
* FUNCTION: SMBRC	_StuffChannel
* PURPOSE : Interface Function to stuff all sampling channels
* INPUT:	 	
*   
* RETURN:
*     0
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMBRC_StuffChannel(RS485_DEVICE_CLASS*  pSMBRCDeviceClass,
						 ENUMSIGNALPROC		  EnumProc,
						 LPVOID				  lpvoid)
{	
	INT32				i;
	INT32				j;
	INT32				iUnitSeqNum;
	//INT32				iRghIdx;
	RS485_VALUE			unTemp;
		
	UNUSED(pSMBRCDeviceClass);

	RS485_CHN_TO_ROUGH_DATA		SmbrcGroupSig[] =
	{
		{SMBRC_G_CH_CFG_VALUE,				SMBRC_G_CFG_VALUE,		},
		{SMBRC_G_CH_EXIST_STAT,				SMBRC_G_EXIST_STAT,		},
		{SMBRC_G_CH_EXIST_ADDR_NUM,			SMBRC_G_EXIST_ADDR_NUM,	},

		{SMBRC_CH_END,						SMBRC_CH_END,			},//end
	};

	RS485_CHN_TO_ROUGH_DATA		SmbrcUnitSig[] =
	{
		{SMBRC_U_CH_DIG_INPUT_NUM,			SMBRC_DIG_INPUT_NUM,		},
		{SMBRC_U_CH_DIGITAL_1_INPUT,		SMBRC_DIGITAL_1_INPUT,		},
		{SMBRC_U_CH_DIGITAL_2_INPUT,		SMBRC_DIGITAL_2_INPUT,		},
		{SMBRC_U_CH_DIGITAL_3_INPUT,		SMBRC_DIGITAL_3_INPUT,		},
		{SMBRC_U_CH_DIGITAL_4_INPUT,		SMBRC_DIGITAL_4_INPUT,		},
		{SMBRC_U_CH_DIG_OUTPUT_NUM,			SMBRC_DIG_OUTPUT_NUM,		},
		{SMBRC_U_CH_DIGITAL_1_OUTPUT,		SMBRC_DIGITAL_1_OUTPUT,		},
		{SMBRC_U_CH_DIGITAL_2_OUTPUT,		SMBRC_DIGITAL_2_OUTPUT,		},
		{SMBRC_U_CH_DIGITAL_3_OUTPUT,		SMBRC_DIGITAL_3_OUTPUT,		},
		{SMBRC_U_CH_DIGITAL_4_OUTPUT,		SMBRC_DIGITAL_4_OUTPUT,		},
		{SMBRC_U_CH_HARDWARE_OPT_STAT,		SMBRC_HARDWARE_OPT_STAT,	},
		{SMBRC_U_CH_RESIST_INTERVAL,		SMBRC_RESIST_INTERVAL,		},
		{SMBRC_U_CH_AMBIENT_TEMPT_VALUE,	SMBRC_AMBIENT_TEMPT_VALUE,	},
		{SMBRC_U_CH_AMBIENT_TEMPT_ALARM,	SMBRC_AMBIENT_TEMPT_ALARM,	},
		{SMBRC_U_CH_UNIT_CFG_VALUE,			SMBRC_UNIT_CFG_VALUE,		},
		{SMBRC_U_CH_UNIT_COMM_STAT,			SMBRC_UNIT_COMM_STAT,		},
		{SMBRC_U_CH_UNIT_EXIST_STAT,		SMBRC_UNIT_EXIST_STAT,		},	
		{SMBRC_U_CH_UNIT_EXIST_STRING_NUM,	SMBRC_UNIT_EXIST_STRING_NUM,},
		{SMBRC_U_CH_UNIT_SEQ_NUM,			SMBRC_UNIT_SEQ_NUM,			},	
		{SMBRC_U_CH_UNIT_STRING_START_SEQ,	SMBRC_UNIT_STRING_START_SEQ,},	

		{SMBRC_CH_END,						SMBRC_CH_END,				},//end
	};

	RS485_CHN_TO_ROUGH_DATA		SmbrcStringSig[] =
	{
		{SMBRC_S_CH_OVERALL_VOLT,				SMBRC_OVERALL_VOLT,			},
		{SMBRC_S_CH_STRING_CURRENT,				SMBRC_STRING_CURRENT,		},
		{SMBRC_S_CH_FLOAT_CURRENT,				SMBRC_FLOAT_CURRENT,		},
		{SMBRC_S_CH_RIPPLE_CURRENT,				SMBRC_RIPPLE_CURRENT,		},

		{SMBRC_S_CH_CELLS_NUM,					SMBRC_CELLS_NUM,			},
		//voltage
		{SMBRC_S_CH_CELL_1_VOLT,				SMBRC_CELL_1_VOLT,			},
		{SMBRC_S_CH_CELL_2_VOLT,				SMBRC_CELL_2_VOLT,			},
		{SMBRC_S_CH_CELL_3_VOLT,				SMBRC_CELL_3_VOLT,			},
		{SMBRC_S_CH_CELL_4_VOLT,				SMBRC_CELL_4_VOLT,			},
		{SMBRC_S_CH_CELL_5_VOLT,				SMBRC_CELL_5_VOLT,			},
		{SMBRC_S_CH_CELL_6_VOLT,				SMBRC_CELL_6_VOLT,			},
		{SMBRC_S_CH_CELL_7_VOLT,				SMBRC_CELL_7_VOLT,			},
		{SMBRC_S_CH_CELL_8_VOLT,				SMBRC_CELL_8_VOLT,			},
		{SMBRC_S_CH_CELL_9_VOLT,				SMBRC_CELL_9_VOLT,			},
		{SMBRC_S_CH_CELL_10_VOLT,				SMBRC_CELL_10_VOLT,			},
		{SMBRC_S_CH_CELL_11_VOLT,				SMBRC_CELL_11_VOLT,			},
		{SMBRC_S_CH_CELL_12_VOLT,				SMBRC_CELL_12_VOLT,			},
		{SMBRC_S_CH_CELL_13_VOLT,				SMBRC_CELL_13_VOLT,			},
		{SMBRC_S_CH_CELL_14_VOLT,				SMBRC_CELL_14_VOLT,			},
		{SMBRC_S_CH_CELL_15_VOLT,				SMBRC_CELL_15_VOLT,			},
		{SMBRC_S_CH_CELL_16_VOLT,				SMBRC_CELL_16_VOLT,			},
		{SMBRC_S_CH_CELL_17_VOLT,				SMBRC_CELL_17_VOLT,			},
		{SMBRC_S_CH_CELL_18_VOLT,				SMBRC_CELL_18_VOLT,			},
		{SMBRC_S_CH_CELL_19_VOLT,				SMBRC_CELL_19_VOLT,			},
		{SMBRC_S_CH_CELL_20_VOLT,				SMBRC_CELL_20_VOLT,			},
		{SMBRC_S_CH_CELL_21_VOLT,				SMBRC_CELL_21_VOLT,			},
		{SMBRC_S_CH_CELL_22_VOLT,				SMBRC_CELL_22_VOLT,			},
		{SMBRC_S_CH_CELL_23_VOLT,				SMBRC_CELL_23_VOLT,			},
		{SMBRC_S_CH_CELL_24_VOLT,				SMBRC_CELL_24_VOLT,			},
		//temperature
		{SMBRC_S_CH_CELL_1_TEMPERAT,			SMBRC_CELL_1_TEMPERAT,		},
		{SMBRC_S_CH_CELL_2_TEMPERAT,			SMBRC_CELL_2_TEMPERAT,		},
		{SMBRC_S_CH_CELL_3_TEMPERAT,			SMBRC_CELL_3_TEMPERAT,		},
		{SMBRC_S_CH_CELL_4_TEMPERAT,			SMBRC_CELL_4_TEMPERAT,		},
		{SMBRC_S_CH_CELL_5_TEMPERAT,			SMBRC_CELL_5_TEMPERAT,		},
		{SMBRC_S_CH_CELL_6_TEMPERAT,			SMBRC_CELL_6_TEMPERAT,		},
		{SMBRC_S_CH_CELL_7_TEMPERAT,			SMBRC_CELL_7_TEMPERAT,		},
		{SMBRC_S_CH_CELL_8_TEMPERAT,			SMBRC_CELL_8_TEMPERAT,		},
		{SMBRC_S_CH_CELL_9_TEMPERAT,			SMBRC_CELL_9_TEMPERAT,		},
		{SMBRC_S_CH_CELL_10_TEMPERAT,			SMBRC_CELL_10_TEMPERAT,		},
		{SMBRC_S_CH_CELL_11_TEMPERAT,			SMBRC_CELL_11_TEMPERAT,		},
		{SMBRC_S_CH_CELL_12_TEMPERAT,			SMBRC_CELL_12_TEMPERAT,		},
		{SMBRC_S_CH_CELL_13_TEMPERAT,			SMBRC_CELL_13_TEMPERAT,		},
		{SMBRC_S_CH_CELL_14_TEMPERAT,			SMBRC_CELL_14_TEMPERAT,		},
		{SMBRC_S_CH_CELL_15_TEMPERAT,			SMBRC_CELL_15_TEMPERAT,		},
		{SMBRC_S_CH_CELL_16_TEMPERAT,			SMBRC_CELL_16_TEMPERAT,		},
		{SMBRC_S_CH_CELL_17_TEMPERAT,			SMBRC_CELL_17_TEMPERAT,		},
		{SMBRC_S_CH_CELL_18_TEMPERAT,			SMBRC_CELL_18_TEMPERAT,		},
		{SMBRC_S_CH_CELL_19_TEMPERAT,			SMBRC_CELL_19_TEMPERAT,		},
		{SMBRC_S_CH_CELL_20_TEMPERAT,			SMBRC_CELL_20_TEMPERAT,		},
		{SMBRC_S_CH_CELL_21_TEMPERAT,			SMBRC_CELL_21_TEMPERAT,		},
		{SMBRC_S_CH_CELL_22_TEMPERAT,			SMBRC_CELL_22_TEMPERAT,		},
		{SMBRC_S_CH_CELL_23_TEMPERAT,			SMBRC_CELL_23_TEMPERAT,		},
		{SMBRC_S_CH_CELL_24_TEMPERAT,			SMBRC_CELL_24_TEMPERAT,		},
		//alarm
		{SMBRC_S_CH_OVERALL_VOLT_ALARM,			SMBRC_OVERALL_VOLT_ALARM,	},
		{SMBRC_S_CH_STRING_CURRENT_ALARM,		SMBRC_STRING_CURRENT_ALARM,	},
		{SMBRC_S_CH_FLOAT_CURRENT_ALARM,		SMBRC_FLOAT_CURRENT_ALARM,	},
		{SMBRC_S_CH_RIPPLE_CURRENT_ALARM,		SMBRC_RIPPLE_CURRENT_ALARM,	},
		{SMBRC_S_CH_CELL_AMBIENT_ALARM,			SMBRC_CELL_AMBIENT_ALARM,	},//-9999

		{SMBRC_S_CH_CELL_1_VOLT_ALARM,			SMBRC_CELL_1_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_2_VOLT_ALARM,			SMBRC_CELL_2_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_3_VOLT_ALARM,			SMBRC_CELL_3_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_4_VOLT_ALARM,			SMBRC_CELL_4_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_5_VOLT_ALARM,			SMBRC_CELL_5_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_6_VOLT_ALARM,			SMBRC_CELL_6_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_7_VOLT_ALARM,			SMBRC_CELL_7_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_8_VOLT_ALARM,			SMBRC_CELL_8_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_9_VOLT_ALARM,			SMBRC_CELL_9_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_10_VOLT_ALARM,			SMBRC_CELL_10_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_11_VOLT_ALARM,			SMBRC_CELL_11_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_12_VOLT_ALARM,			SMBRC_CELL_12_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_13_VOLT_ALARM,			SMBRC_CELL_13_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_14_VOLT_ALARM,			SMBRC_CELL_14_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_15_VOLT_ALARM,			SMBRC_CELL_15_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_16_VOLT_ALARM,			SMBRC_CELL_16_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_17_VOLT_ALARM,			SMBRC_CELL_17_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_18_VOLT_ALARM,			SMBRC_CELL_18_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_19_VOLT_ALARM,			SMBRC_CELL_19_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_20_VOLT_ALARM,			SMBRC_CELL_20_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_21_VOLT_ALARM,			SMBRC_CELL_21_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_22_VOLT_ALARM,			SMBRC_CELL_22_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_23_VOLT_ALARM,			SMBRC_CELL_23_VOLT_ALARM,	},
		{SMBRC_S_CH_CELL_24_VOLT_ALARM,			SMBRC_CELL_24_VOLT_ALARM,	},

		{SMBRC_S_CH_CELL_1_TEMPT_ALARM,			SMBRC_CELL_1_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_2_TEMPT_ALARM,			SMBRC_CELL_2_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_3_TEMPT_ALARM,			SMBRC_CELL_3_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_4_TEMPT_ALARM,			SMBRC_CELL_4_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_5_TEMPT_ALARM,			SMBRC_CELL_5_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_6_TEMPT_ALARM,			SMBRC_CELL_6_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_7_TEMPT_ALARM,			SMBRC_CELL_7_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_8_TEMPT_ALARM,			SMBRC_CELL_8_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_9_TEMPT_ALARM,			SMBRC_CELL_9_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_10_TEMPT_ALARM,		SMBRC_CELL_10_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_11_TEMPT_ALARM,		SMBRC_CELL_11_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_12_TEMPT_ALARM,		SMBRC_CELL_12_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_13_TEMPT_ALARM,		SMBRC_CELL_13_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_14_TEMPT_ALARM,		SMBRC_CELL_14_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_15_TEMPT_ALARM,		SMBRC_CELL_15_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_16_TEMPT_ALARM,		SMBRC_CELL_16_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_17_TEMPT_ALARM,		SMBRC_CELL_17_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_18_TEMPT_ALARM,		SMBRC_CELL_18_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_19_TEMPT_ALARM,		SMBRC_CELL_19_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_20_TEMPT_ALARM,		SMBRC_CELL_20_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_21_TEMPT_ALARM,		SMBRC_CELL_21_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_22_TEMPT_ALARM,		SMBRC_CELL_22_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_23_TEMPT_ALARM,		SMBRC_CELL_23_TEMPT_ALARM,	},
		{SMBRC_S_CH_CELL_24_TEMPT_ALARM,		SMBRC_CELL_24_TEMPT_ALARM,	},

		{SMBRC_S_CH_CELL_1_RESIST_ALARM,		SMBRC_CELL_1_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_2_RESIST_ALARM,		SMBRC_CELL_2_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_3_RESIST_ALARM,		SMBRC_CELL_3_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_4_RESIST_ALARM,		SMBRC_CELL_4_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_5_RESIST_ALARM,		SMBRC_CELL_5_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_6_RESIST_ALARM,		SMBRC_CELL_6_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_7_RESIST_ALARM,		SMBRC_CELL_7_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_8_RESIST_ALARM,		SMBRC_CELL_8_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_9_RESIST_ALARM,		SMBRC_CELL_9_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_10_RESIST_ALARM,		SMBRC_CELL_10_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_11_RESIST_ALARM,		SMBRC_CELL_11_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_12_RESIST_ALARM,		SMBRC_CELL_12_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_13_RESIST_ALARM,		SMBRC_CELL_13_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_14_RESIST_ALARM,		SMBRC_CELL_14_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_15_RESIST_ALARM,		SMBRC_CELL_15_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_16_RESIST_ALARM,		SMBRC_CELL_16_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_17_RESIST_ALARM,		SMBRC_CELL_17_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_18_RESIST_ALARM,		SMBRC_CELL_18_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_19_RESIST_ALARM,		SMBRC_CELL_19_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_20_RESIST_ALARM,		SMBRC_CELL_20_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_21_RESIST_ALARM,		SMBRC_CELL_21_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_22_RESIST_ALARM,		SMBRC_CELL_22_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_23_RESIST_ALARM,		SMBRC_CELL_23_RESIST_ALARM,	},
		{SMBRC_S_CH_CELL_24_RESIST_ALARM,		SMBRC_CELL_24_RESIST_ALARM,	},

		{SMBRC_S_CH_CELL_1_INTER_ALARM,			SMBRC_CELL_1_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_2_INTER_ALARM,			SMBRC_CELL_2_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_3_INTER_ALARM,			SMBRC_CELL_3_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_4_INTER_ALARM,			SMBRC_CELL_4_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_5_INTER_ALARM,			SMBRC_CELL_5_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_6_INTER_ALARM,			SMBRC_CELL_6_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_7_INTER_ALARM,			SMBRC_CELL_7_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_8_INTER_ALARM,			SMBRC_CELL_8_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_9_INTER_ALARM,			SMBRC_CELL_9_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_10_INTER_ALARM,		SMBRC_CELL_10_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_11_INTER_ALARM,		SMBRC_CELL_11_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_12_INTER_ALARM,		SMBRC_CELL_12_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_13_INTER_ALARM,		SMBRC_CELL_13_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_14_INTER_ALARM,		SMBRC_CELL_14_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_15_INTER_ALARM,		SMBRC_CELL_15_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_16_INTER_ALARM,		SMBRC_CELL_16_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_17_INTER_ALARM,		SMBRC_CELL_17_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_18_INTER_ALARM,		SMBRC_CELL_18_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_19_INTER_ALARM,		SMBRC_CELL_19_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_20_INTER_ALARM,		SMBRC_CELL_20_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_21_INTER_ALARM,		SMBRC_CELL_21_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_22_INTER_ALARM,		SMBRC_CELL_22_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_23_INTER_ALARM,		SMBRC_CELL_23_INTER_ALARM,	},
		{SMBRC_S_CH_CELL_24_INTER_ALARM,		SMBRC_CELL_24_INTER_ALARM,	},

		{SMBRC_S_CH_CELL_1_TO_AMBIENT_ALARM,	SMBRC_CELL_1_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_2_TO_AMBIENT_ALARM,	SMBRC_CELL_2_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_3_TO_AMBIENT_ALARM,	SMBRC_CELL_3_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_4_TO_AMBIENT_ALARM,	SMBRC_CELL_4_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_5_TO_AMBIENT_ALARM,	SMBRC_CELL_5_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_6_TO_AMBIENT_ALARM,	SMBRC_CELL_6_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_7_TO_AMBIENT_ALARM,	SMBRC_CELL_7_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_8_TO_AMBIENT_ALARM,	SMBRC_CELL_8_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_9_TO_AMBIENT_ALARM,	SMBRC_CELL_9_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_10_TO_AMBIENT_ALARM,	SMBRC_CELL_10_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_11_TO_AMBIENT_ALARM,	SMBRC_CELL_11_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_12_TO_AMBIENT_ALARM,	SMBRC_CELL_12_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_13_TO_AMBIENT_ALARM,	SMBRC_CELL_13_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_14_TO_AMBIENT_ALARM,	SMBRC_CELL_14_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_15_TO_AMBIENT_ALARM,	SMBRC_CELL_15_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_16_TO_AMBIENT_ALARM,	SMBRC_CELL_16_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_17_TO_AMBIENT_ALARM,	SMBRC_CELL_17_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_18_TO_AMBIENT_ALARM,	SMBRC_CELL_18_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_19_TO_AMBIENT_ALARM,	SMBRC_CELL_19_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_20_TO_AMBIENT_ALARM,	SMBRC_CELL_20_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_21_TO_AMBIENT_ALARM,	SMBRC_CELL_21_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_22_TO_AMBIENT_ALARM,	SMBRC_CELL_22_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_23_TO_AMBIENT_ALARM,	SMBRC_CELL_23_TO_AMBIENT_ALARM,},
		{SMBRC_S_CH_CELL_24_TO_AMBIENT_ALARM,	SMBRC_CELL_24_TO_AMBIENT_ALARM,},

		//
		{SMBRC_S_CH_STRING_COMM_STAT,			SMBRC_STRING_COMM_STAT,			},
		{SMBRC_S_CH_STRING_ADDR_VALUE,			SMBRC_STRING_ADDR_VALUE,		},
		{SMBRC_S_CH_STRING_SEQ_NUM,			SMBRC_STRING_SEQ_NUM,			},
		{SMBRC_S_CH_STRING_EXIST_STAT,			SMBRC_STRING_EXIST_STAT,		},


		//2010-07-22
		//2011/03/02� Ҫ�󽫸澯���ָߵ�
		{SMBRC_S_CH_HAVE_CELL_VOLT_ALARM_L,		SMBRC_HAVE_CELL_VOLT_ALARM_L,		},
		{SMBRC_S_CH_HAVE_CELL_TEMPT_ALARM_L,		SMBRC_HAVE_CELL_TEMPT_ALARM_L,		},
		{SMBRC_S_CH_HAVE_CELL_RESIST_ALARM_L,		SMBRC_HAVE_CELL_RESIST_ALARM_L,		},
		{SMBRC_S_CH_HAVE_CELL_INTER_ALARM_L,		SMBRC_HAVE_CELL_INTER_ALARM_L,		},
		{SMBRC_S_CH_HAVE_CELL_AMBIENT_ALARM_L,		SMBRC_HAVE_CELL_AMBIENT_ALARM_L,	},

		//2011/03/02� Ҫ�󽫸澯���ָߵ�
		{SMBRC_S_CH_HAVE_CELL_VOLT_ALARM_H,		SMBRC_HAVE_CELL_VOLT_ALARM_H,		},
		{SMBRC_S_CH_HAVE_CELL_TEMPT_ALARM_H,		SMBRC_HAVE_CELL_TEMPT_ALARM_H,		},
		{SMBRC_S_CH_HAVE_CELL_RESIST_ALARM_H,		SMBRC_HAVE_CELL_RESIST_ALARM_H,		},
		{SMBRC_S_CH_HAVE_CELL_INTER_ALARM_H,		SMBRC_HAVE_CELL_INTER_ALARM_H,		},
		{SMBRC_S_CH_HAVE_CELL_AMBIENT_ALARM_H,		SMBRC_HAVE_CELL_AMBIENT_ALARM_H,	},
		//����ź���Jimmy���ӵģ�EEMҪ��Ambient
		{SMBRC_S_CH_EEM_AMBIENT,			SMBRC_STTING_AMB_TEMP,			},


		/***************************Test Resistance**********************************/
		{SMBRC_S_CH_TEST_RESISTANCE_1,			SMBRC_TEST_RESISTANCE_1,	},
		{SMBRC_S_CH_TEST_RESISTANCE_2,			SMBRC_TEST_RESISTANCE_2,	},
		{SMBRC_S_CH_TEST_RESISTANCE_3,			SMBRC_TEST_RESISTANCE_3,	},
		{SMBRC_S_CH_TEST_RESISTANCE_4,			SMBRC_TEST_RESISTANCE_4,	},
		{SMBRC_S_CH_TEST_RESISTANCE_5,			SMBRC_TEST_RESISTANCE_5,	},
		{SMBRC_S_CH_TEST_RESISTANCE_6,			SMBRC_TEST_RESISTANCE_6,	},
		{SMBRC_S_CH_TEST_RESISTANCE_7,			SMBRC_TEST_RESISTANCE_7,	},
		{SMBRC_S_CH_TEST_RESISTANCE_8,			SMBRC_TEST_RESISTANCE_8,	},
		{SMBRC_S_CH_TEST_RESISTANCE_9,			SMBRC_TEST_RESISTANCE_9,	},
		{SMBRC_S_CH_TEST_RESISTANCE_10,			SMBRC_TEST_RESISTANCE_10,	},
		{SMBRC_S_CH_TEST_RESISTANCE_11,			SMBRC_TEST_RESISTANCE_11,	},
		{SMBRC_S_CH_TEST_RESISTANCE_12,			SMBRC_TEST_RESISTANCE_12,	},
		{SMBRC_S_CH_TEST_RESISTANCE_13,			SMBRC_TEST_RESISTANCE_13,	},
		{SMBRC_S_CH_TEST_RESISTANCE_14,			SMBRC_TEST_RESISTANCE_14,	},
		{SMBRC_S_CH_TEST_RESISTANCE_15,			SMBRC_TEST_RESISTANCE_15,	},
		{SMBRC_S_CH_TEST_RESISTANCE_16,			SMBRC_TEST_RESISTANCE_16,	},
		{SMBRC_S_CH_TEST_RESISTANCE_17,			SMBRC_TEST_RESISTANCE_17,	},
		{SMBRC_S_CH_TEST_RESISTANCE_18,			SMBRC_TEST_RESISTANCE_18,	},
		{SMBRC_S_CH_TEST_RESISTANCE_19,			SMBRC_TEST_RESISTANCE_19,	},
		{SMBRC_S_CH_TEST_RESISTANCE_20,			SMBRC_TEST_RESISTANCE_20,	},
		{SMBRC_S_CH_TEST_RESISTANCE_21,			SMBRC_TEST_RESISTANCE_21,	},
		{SMBRC_S_CH_TEST_RESISTANCE_22,			SMBRC_TEST_RESISTANCE_22,	},
		{SMBRC_S_CH_TEST_RESISTANCE_23,			SMBRC_TEST_RESISTANCE_23,	},
		{SMBRC_S_CH_TEST_RESISTANCE_24,			SMBRC_TEST_RESISTANCE_24,	},

		{SMBRC_S_CH_TEST_INTERCELL_1,			SMBRC_TEST_INTERCELL_1,		},
		{SMBRC_S_CH_TEST_INTERCELL_2,			SMBRC_TEST_INTERCELL_2,		},
		{SMBRC_S_CH_TEST_INTERCELL_3,			SMBRC_TEST_INTERCELL_3,		},
		{SMBRC_S_CH_TEST_INTERCELL_4,			SMBRC_TEST_INTERCELL_4,		},
		{SMBRC_S_CH_TEST_INTERCELL_5,			SMBRC_TEST_INTERCELL_5,		},
		{SMBRC_S_CH_TEST_INTERCELL_6,			SMBRC_TEST_INTERCELL_6,		},
		{SMBRC_S_CH_TEST_INTERCELL_7,			SMBRC_TEST_INTERCELL_7,		},
		{SMBRC_S_CH_TEST_INTERCELL_8,			SMBRC_TEST_INTERCELL_8,		},
		{SMBRC_S_CH_TEST_INTERCELL_9,			SMBRC_TEST_INTERCELL_9,		},
		{SMBRC_S_CH_TEST_INTERCELL_10,			SMBRC_TEST_INTERCELL_10,	},
		{SMBRC_S_CH_TEST_INTERCELL_11,			SMBRC_TEST_INTERCELL_11,	},
		{SMBRC_S_CH_TEST_INTERCELL_12,			SMBRC_TEST_INTERCELL_12,	},
		{SMBRC_S_CH_TEST_INTERCELL_13,			SMBRC_TEST_INTERCELL_13,	},
		{SMBRC_S_CH_TEST_INTERCELL_14,			SMBRC_TEST_INTERCELL_14,	},
		{SMBRC_S_CH_TEST_INTERCELL_15,			SMBRC_TEST_INTERCELL_15,	},
		{SMBRC_S_CH_TEST_INTERCELL_16,			SMBRC_TEST_INTERCELL_16,	},
		{SMBRC_S_CH_TEST_INTERCELL_17,			SMBRC_TEST_INTERCELL_17,	},
		{SMBRC_S_CH_TEST_INTERCELL_18,			SMBRC_TEST_INTERCELL_18,	},
		{SMBRC_S_CH_TEST_INTERCELL_19,			SMBRC_TEST_INTERCELL_19,	},
		{SMBRC_S_CH_TEST_INTERCELL_20,			SMBRC_TEST_INTERCELL_20,	},
		{SMBRC_S_CH_TEST_INTERCELL_21,			SMBRC_TEST_INTERCELL_21,	},
		{SMBRC_S_CH_TEST_INTERCELL_22,			SMBRC_TEST_INTERCELL_22,	},
		{SMBRC_S_CH_TEST_INTERCELL_23,			SMBRC_TEST_INTERCELL_23,	},
		{SMBRC_S_CH_TEST_INTERCELL_24,			SMBRC_TEST_INTERCELL_24,	},


		/***************************Test Resistance End******************************/
		{SMBRC_CH_END,							SMBRC_CH_END,					},//end
	};

	//return;

#if SMBRC_TEST_FLAG
	//printf("::::::::   this is running to SMBRC_StuffChannel :::::\n");
#endif

	//1.Stuff Group sig 
	i = 0;
	while(SmbrcGroupSig[i].iChannel != SMBRC_CH_END)
	{	
		EnumProc(SmbrcGroupSig[i].iChannel, 
			(float)(g_SmbrcSampData.aRoughGroupData[SmbrcGroupSig[i].iRoughData].fValue),
			lpvoid);
		i++;
	}



	//2.Stuff Unit sig
	iUnitSeqNum = 0;
	for (i = 0; i < SMBRC_ADDR_MAX__NUM; i++)
	{
		if (SMBRC_STAT_EXIST == 
			g_SmbrcSampData.aRoughUnitData[i][SMBRC_UNIT_EXIST_STAT].iValue)
		{
			j = 0;
			while(SmbrcUnitSig[j].iChannel != SMBRC_CH_END)
			{	
				EnumProc(SmbrcUnitSig[j].iChannel + (iUnitSeqNum * SMBRC_UNIT_CH_PER_DISTANCE), 
					(float)(g_SmbrcSampData.aRoughUnitData[i][SmbrcUnitSig[j].iRoughData].fValue),
					lpvoid);

				//printf("This is UNIT sig CH=%d D=%d F=%f\n",
				//	SmbrcUnitSig[j].iChannel + (iUnitSeqNum * SMBRC_UNIT_CH_PER_DISTANCE),
				//	g_SmbrcSampData.aRoughUnitData[i][SmbrcUnitSig[j].iRoughData].iValue,
				//	g_SmbrcSampData.aRoughUnitData[i][SmbrcUnitSig[j].iRoughData].fValue);

				j++;
			}
			iUnitSeqNum++;
		}
	}

	//printf("\n\n");

	//Stuff inexistence Unit sig
	for (i = iUnitSeqNum; i < SMBRC_ADDR_MAX__NUM; i++)
	{
		//���ԭ�������ݣ����������¶��ź�ʹ�á�
		unTemp.iValue = SMBRC_SIG_INVALID_VALUE;
		j = 0;
		while(SmbrcUnitSig[j].iChannel != SMBRC_CH_END)
		{	
			EnumProc(SmbrcUnitSig[j].iChannel + (i * SMBRC_UNIT_CH_PER_DISTANCE), 
				(float)(unTemp.fValue),
				lpvoid);

			j++;
		}
		//����豸�������ź�
		unTemp.iValue = SMBRC_STAT_INEXISTENCE;
		EnumProc(SMBRC_U_CH_UNIT_EXIST_STAT + (i * SMBRC_UNIT_CH_PER_DISTANCE), 
			(float)(unTemp.fValue),
			lpvoid);
	}

	//3.Stuff battery sig  20 strings  �Ѿ��������ڵ����Ϊ-9999 �� SMBRC_RefreshStringSig()����
	for (i = 0; i < SMBRC_STRING_MAX_NUM; i++)
	{
		j = 0;
		while(SmbrcStringSig[j].iChannel != SMBRC_CH_END)
		{	
			EnumProc(SmbrcStringSig[j].iChannel + (i * SMBRC_STRING_CH_PER_DISTANCE), 
				(float)(g_SmbrcSampData.aRoughStringData[i][SmbrcStringSig[j].iRoughData].fValue),
				lpvoid);

			//printf("This is BATT sig CH=%d D=%d F=%f\n",
			//	SmbrcStringSig[j].iChannel + (i * SMBRC_STRING_CH_PER_DISTANCE),
			//	g_SmbrcSampData.aRoughStringData[i][SmbrcStringSig[j].iRoughData].iValue,
			//	g_SmbrcSampData.aRoughStringData[i][SmbrcStringSig[j].iRoughData].fValue);

			j++;
		}
	}
	//printf("\n\n");
	return TRUE;
}

void SMBRC_SendTwoParam(int	byAddr, 
						RS485_DEVICE_CLASS*  pSMBRCDeviceClass,
						int	byCid2,
						int	byCmdId,
						UINT	uiParamater,
						INT32	iLengh)
{
	HANDLE	hComm;
	INT32	iCmdStat;
	INT32	iReadLengh = 0;
	BYTE	abyRcvBuf[SMBRC_RECV_BUF_MAX_NUM];
	hComm	 = pSMBRCDeviceClass->pCommPort->hCommPort;
	RS485_DRV*	pPort				= (RS485_DRV *)hComm;
	INT32		fd					= pPort->fdSerial;

	memset(abyRcvBuf, 0, SMBRC_RECV_BUF_MAX_NUM);

	iCmdStat = SMBRC_PackAndSendTwoByte(hComm,
					byAddr,
					byCmdId,
					byCid2,
					iLengh,
					uiParamater);


	if (SMBRC_SEND_CMD_OK == iCmdStat)
	{
		if (SMBRC_RS485WaitReadable(fd, 1000))
		{
			Sleep(10);
			SMBRC_ReadData(hComm, abyRcvBuf, &iReadLengh, SMBRC_RECV_BUF_MAX_NUM);
		}
		else
		{
			return;
		}
	}
	else
	{
		return;
	}
}
/*=============================================================================*
* FUNCTION: SMBRC	_SetParamOrCtrl
* PURPOSE : Set parameter test interval or battery config number to SMBRC
*			Or Control start/stop the resistance test!!!!
* INPUT:	 	
*   
* RETURN:
*     0
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void SMBRC_SetParamOrCtrl(int	byAddr, 
				RS485_DEVICE_CLASS*  pSMBRCDeviceClass,
				int	byCid2,
				int	byCmdId,
				INT32	iParamater,
				INT32	iLengh)
{
	HANDLE	hComm;
	INT32	iCmdStat;
	INT32	iReadLengh = 0;
	BYTE	abyRcvBuf[SMBRC_RECV_BUF_MAX_NUM];
	memset(abyRcvBuf, 0, SMBRC_RECV_BUF_MAX_NUM);

	hComm	 = pSMBRCDeviceClass->pCommPort->hCommPort;
	ASSERT(hComm);
	RS485_DRV*	pPort = (RS485_DRV *)hComm;
	INT32		fd = pPort->fdSerial;

	iCmdStat = SMBRC_PackAndSend(hComm,
					byAddr,
					byCmdId,
					byCid2,
					iLengh,
					iParamater);


	if (SMBRC_SEND_CMD_OK == iCmdStat)
	{
		if (SMBRC_RS485WaitReadable(fd, 500))
		{
			Sleep(10);
			SMBRC_ReadData(hComm, abyRcvBuf, &iReadLengh, SMBRC_RECV_BUF_MAX_NUM);
		}
		else
		{
			return;
		}
	}
	else
	{
		return;
	}
}
/*=============================================================================*
* FUNCTION: SMBRC	_RefreshStringSig
* PURPOSE : Clear the battery string sig information
* INPUT:	 	
*   
* RETURN:
*     0
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
static void SMBRC_RefreshStringSig()
{
	INT32	n, m;
	//string sig
	for (m = 0; m < SMBRC_STRING_MAX_NUM; m++)
	{
		for (n = 0; n < SMBRC_STRING_MAX_SIG; n++)
		{
			g_SmbrcSampData.aRoughStringData[m][n].iValue = SMBRC_SIG_INVALID_VALUE;
		}

		g_SmbrcSampData.aRoughStringData[m][SMBRC_STRING_EXIST_STAT].iValue
			= SMBRC_STAT_INEXISTENCE;
	}
}
/*=============================================================================*
* FUNCTION: SMBRC	_ParamUnify
* PURPOSE : Ensure the parameters equal to sampling data in rough data
* INPUT:	 	
*				����������źţ�����һ���ṹ���Ż�һ�£��������̫����
* RETURN:
*     0
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
#define	  BRC_CELL_VOLT_DIFF	    0.0001	
#define	  BRC_CELL_VOLT_NOT_EQUAL(f1, f2)	(ABS(((f1)-(f2)))>BRC_CELL_VOLT_DIFF)

#define	  BRC_OTHER_SIGNAL_DIFF	    0.1		//��Ϊ���þ���Ϊ1�����Բ���ȽϾ���Ϊ0.1
#define	  BRC_OTHER_SIGNAL_NOT_EQUAL(f1, f2)	(ABS(((f1)-(f2)))>BRC_OTHER_SIGNAL_DIFF)

#define	  SMBRC_MILLI_SCALE_1000	    1000

static void SMBRC_ParamUnify(RS485_DEVICE_CLASS*  pSMBRCDeviceClass)
{
	INT32	i;
	INT32	iSampCfg;
	INT32	iInternalCfg;
	INT32	iSampInterval;
	INT32	iInternalInterval;

	BYTE	byActualAddr;
	BOOL	bNeedSamp0XE4Flag = FALSE;
	BOOL	bNeedRefreshStringInfor = FALSE;

	float	fTemp;
	UINT	uiCellVoltagehigh;
	UINT	uiCellVoltagelow;
	UINT	uiCellTemphigh;
	UINT	uiCellTemplow;
	UINT	uiStringCurrhigh;
	UINT	uiRippleCurrhigh;
	UINT	uiStringCurrlow;
	UINT	uiRippleCurrlow;
	UINT	uiHighResistance;
	UINT	uiLowResistance;
	UINT	uiHighIntercell;
	UINT	uiLowIntercell;

	if (SMBRC_NeedCommProc())
	{
		SMBRC_ReopenPort();
	}

	//printf("\nUNIFY   START\n");

	if (SMBRC_STAT_EXIST 
		!= g_SmbrcSampData.aRoughGroupData[SMBRC_G_EXIST_STAT].iValue)
	{
		return;
	}

	for (i = 0; i < SMBRC_ADDR_MAX__NUM; i++)
	{
		if (SMBRC_STAT_EXIST == 
			g_SmbrcSampData.aRoughUnitData[i][SMBRC_UNIT_EXIST_STAT].iValue)
		{
			byActualAddr = i + SMBRC_ADDR_START;

			iSampCfg = g_SmbrcSampData.aRoughUnitData[i][SMBRC_UNIT_CFG_VALUE].iValue;
			iInternalCfg = GetDwordSigValue(SMBRC_GROUP_EQUIP_ID,
				SMBRC_SIG_SETTING_TYPE,
				SMBRC_SIG_CFG_ID,
				"RS485SMBRC");

			iSampInterval = g_SmbrcSampData.aRoughUnitData[i][SMBRC_RESIST_INTERVAL].iValue;
			iInternalInterval = GetDwordSigValue(SMBRC_GROUP_EQUIP_ID,
				SMBRC_SIG_SETTING_TYPE,
				SMBRC_SIG_INTEVEL_ID,
				"RS485SMBRC");
			if (iSampCfg != iInternalCfg) 
			{
				Sleep(500);
				SMBRC_SetParamOrCtrl(byActualAddr,
					(RS485_DEVICE_CLASS*)pSMBRCDeviceClass,
					SMBRC_SET_CFG_INTERVAL_CID2,
					SMBRC_CMD_SET_CFG,
					iInternalCfg,
					22);

				//printf("This is Cfg Unify iSampCfg=%d iInternalCfg=%d\n",iSampCfg,iInternalCfg);

				bNeedRefreshStringInfor = TRUE;
			}
			else
			{
				//Continue,There needn't patameter unify!!!
			}

			if (iSampInterval != iInternalInterval)
			{
				Sleep(500);
				SMBRC_SetParamOrCtrl(byActualAddr,
					(RS485_DEVICE_CLASS*)pSMBRCDeviceClass,
					SMBRC_SET_CFG_INTERVAL_CID2,
					SMBRC_CMD_SET_INTERVAL,
					iInternalInterval,
					22);
				//printf("This is Cfg Unify iSampInterval=%d iInternalInterval=%d\n",iSampInterval,iInternalInterval);
			}
			else
			{
				//Continue,There needn't parameter unify!!!!!!!
			}

			fTemp = GetFloatSigValue(SMBRC_GROUP_EQUIP_ID,
				SMBRC_SIG_SETTING_TYPE,
				SMBRC_CELL_VOLT_H_ID,
				"RS485SMBRC");
			
			if(BRC_CELL_VOLT_NOT_EQUAL((fTemp), (g_SmbrcSampData.aRoughUnitData[i][SMBRC_CELL_VOLTAGE_H].fValue)))
			{
				bNeedSamp0XE4Flag = TRUE;
				uiCellVoltagehigh = (UINT)(fTemp*SMBRC_MILLI_SCALE_1000);
				
				Sleep(500);
				SMBRC_SendTwoParam(byActualAddr,
					(RS485_DEVICE_CLASS*)pSMBRCDeviceClass,
					SMBRC_SET_ALARM_THRES_CID2,
					SMBRC_SET_THRES_SIG_CELL_VOLTAGE_H,
					uiCellVoltagehigh,
					24);
				//printf("This is Cfg Unify CELL_VOLT_H_ID=%f  11CELL_VOLT_H_ID=%f\n\n",fTemp,
				//			g_SmbrcSampData.aRoughUnitData[i][SMBRC_CELL_VOLTAGE_H].fValue);				
			}
			else
			{
				//No process!!
			}


			fTemp = GetFloatSigValue(SMBRC_GROUP_EQUIP_ID,
				SMBRC_SIG_SETTING_TYPE,
				SMBRC_CELL_VOLT_L_ID,
				"RS485SMBRC");
			
			if(BRC_CELL_VOLT_NOT_EQUAL((fTemp), (g_SmbrcSampData.aRoughUnitData[i][SMBRC_CELL_VOLTAGE_L].fValue)))
			{
				bNeedSamp0XE4Flag = TRUE;
				uiCellVoltagelow  = (UINT)(fTemp*SMBRC_MILLI_SCALE_1000);
				Sleep(500);
				SMBRC_SendTwoParam(byActualAddr,
					(RS485_DEVICE_CLASS*)pSMBRCDeviceClass,
					SMBRC_SET_ALARM_THRES_CID2,
					SMBRC_SET_THRES_SIG_CELL_VOLTAGE_L,
					uiCellVoltagelow,
					24);
				//printf("This is Cfg Unify CELL_VOLTAGE_L=%f  11CELL_VOLTAGE_L=%f\n\n",fTemp,
				//	g_SmbrcSampData.aRoughUnitData[i][SMBRC_CELL_VOLTAGE_L].fValue);				
			}
			else
			{
				//No process!!
			}


			fTemp = GetFloatSigValue(SMBRC_GROUP_EQUIP_ID,
				SMBRC_SIG_SETTING_TYPE,
				SMBRC_CELL_TEMP_H_ID,
				"RS485SMBRC");
			if (BRC_OTHER_SIGNAL_NOT_EQUAL(roundf(fTemp), roundf(g_SmbrcSampData.aRoughUnitData[i][SMBRC_CELL_TEMPERATURE_H].fValue)))
			{
				bNeedSamp0XE4Flag = TRUE;
				uiCellTemphigh	= (UINT)(roundf(fTemp) * 1024);
				Sleep(500);
				SMBRC_SendTwoParam(byActualAddr,
					(RS485_DEVICE_CLASS*)pSMBRCDeviceClass,
					SMBRC_SET_ALARM_THRES_CID2,
					SMBRC_SET_THRES_SIG_CELL_TEMPERATURE_H,
					uiCellTemphigh,
					24);
				//printf("This is Cfg Unify CELL_TEMPERATURE_H=%f  11CELL_TEMPERATURE_H=%f\n\n",fTemp,
				//	g_SmbrcSampData.aRoughUnitData[i][SMBRC_CELL_TEMPERATURE_H].fValue);
			}
			else
			{
				//No process!!
			}

			fTemp = GetFloatSigValue(SMBRC_GROUP_EQUIP_ID,
				SMBRC_SIG_SETTING_TYPE,
				SMBRC_CELL_TEMP_L_ID,
				"RS485SMBRC");
			if (BRC_OTHER_SIGNAL_NOT_EQUAL(roundf(fTemp), roundf(g_SmbrcSampData.aRoughUnitData[i][SMBRC_CELL_TEMPERATURE_L].fValue)))
			{
				bNeedSamp0XE4Flag = TRUE;
				uiCellTemplow = (UINT)(roundf(fTemp) * 1024);
				Sleep(500);
				SMBRC_SendTwoParam(byActualAddr,
					(RS485_DEVICE_CLASS*)pSMBRCDeviceClass,
					SMBRC_SET_ALARM_THRES_CID2,
					SMBRC_SET_THRES_SIG_CELL_TEMPERATURE_L,
					uiCellTemplow,
					24);
				//printf("This is Cfg Unify CELL_TEMPERATURE_L=%f  11CELL_TEMPERATURE_L=%f\n\n",fTemp,
				//	g_SmbrcSampData.aRoughUnitData[i][SMBRC_CELL_TEMPERATURE_L].fValue);
			}
			else
			{
				//No process!!
			}


			fTemp = GetFloatSigValue(SMBRC_GROUP_EQUIP_ID,
				SMBRC_SIG_SETTING_TYPE,
				SMBRC_STRING_CURR_HIGH_ID,
				"RS485SMBRC");
			if(BRC_OTHER_SIGNAL_NOT_EQUAL(roundf(fTemp), roundf(g_SmbrcSampData.aRoughUnitData[i][SMBRC_STRING_CURR_H].fValue)))
			{
				bNeedSamp0XE4Flag = TRUE;
				uiStringCurrhigh = (UINT)(roundf(fTemp));
				Sleep(500);
				SMBRC_SendTwoParam(byActualAddr,
					(RS485_DEVICE_CLASS*)pSMBRCDeviceClass,
					SMBRC_SET_ALARM_THRES_CID2,
					SMBRC_SET_THRES_SIG_STRING_CURR_H,
					uiStringCurrhigh,
					24);
				//printf("This is Cfg Unify SIG_STRING_CURR_H=%f  11SIG_STRING_CURR_H=%f\n\n",fTemp,
				//	g_SmbrcSampData.aRoughUnitData[i][SMBRC_STRING_CURR_H].fValue);
			}
			else
			{
				//No process!!
			}


			fTemp =  GetFloatSigValue(SMBRC_GROUP_EQUIP_ID,
				SMBRC_SIG_SETTING_TYPE,
				SMBRC_RIPPLE_CURR_HIGH_ID,
				"RS485SMBRC");
			if(BRC_OTHER_SIGNAL_NOT_EQUAL(roundf(fTemp), roundf(g_SmbrcSampData.aRoughUnitData[i][SMBRC_RIPPLE_CURR_H].fValue)))
			{
				bNeedSamp0XE4Flag = TRUE;
				uiRippleCurrhigh = (UINT)(roundf(fTemp));
				Sleep(500);
				SMBRC_SendTwoParam(byActualAddr,
					(RS485_DEVICE_CLASS*)pSMBRCDeviceClass,
					SMBRC_SET_ALARM_THRES_CID2,
					SMBRC_SET_THRES_SIG_RIPPLE_CURR_H,
					uiRippleCurrhigh,
					24);
				//printf("This is Cfg Unify SIG_RIPPLE_CURR_H=%f  11SIG_RIPPLE_CURR_H=%f\n\n",fTemp,
				//	g_SmbrcSampData.aRoughUnitData[i][SMBRC_RIPPLE_CURR_H].fValue);
			}
			else
			{
				//No process!!
			}

			fTemp = GetFloatSigValue(SMBRC_GROUP_EQUIP_ID,
			    SMBRC_SIG_SETTING_TYPE,
			    SMBRC_STRING_CURR_LOW_ID,
			    "RS485SMBRC");
			if(BRC_OTHER_SIGNAL_NOT_EQUAL(roundf(fTemp), roundf(g_SmbrcSampData.aRoughUnitData[i][SMBRC_STRING_CUUR_L].fValue)))
			{
			    bNeedSamp0XE4Flag = TRUE;
			    uiStringCurrlow = (UINT)(roundf(fTemp));
			    Sleep(500);
			    SMBRC_SendTwoParam(byActualAddr,
				(RS485_DEVICE_CLASS*)pSMBRCDeviceClass,
				SMBRC_SET_ALARM_THRES_CID2,
				SMBRC_SET_THRES_SIG_STRING_CURR_L,
				uiStringCurrlow,
				24);
			    //printf("This is Cfg Unify SIG_STRING_CURR_H=%f  11SIG_STRING_CURR_H=%f\n\n",fTemp,
			    //	g_SmbrcSampData.aRoughUnitData[i][SMBRC_STRING_CURR_H].fValue);
			}
			else
			{
			    //No process!!
			}


			fTemp =  GetFloatSigValue(SMBRC_GROUP_EQUIP_ID,
			    SMBRC_SIG_SETTING_TYPE,
			    SMBRC_RIPPLE_CURR_LOW_ID,
			    "RS485SMBRC");
			if(BRC_OTHER_SIGNAL_NOT_EQUAL(roundf(fTemp), roundf(g_SmbrcSampData.aRoughUnitData[i][SMBRC_RIPPLE_CURR_L].fValue)))
			{
			    bNeedSamp0XE4Flag = TRUE;
			    uiRippleCurrlow = (UINT)(roundf(fTemp));
			    Sleep(500);
			    SMBRC_SendTwoParam(byActualAddr,
				(RS485_DEVICE_CLASS*)pSMBRCDeviceClass,
				SMBRC_SET_ALARM_THRES_CID2,
				SMBRC_SET_THRES_SIG_RIPPLE_CURR_L,
				uiRippleCurrlow,
				24);
			    //printf("This is Cfg Unify SIG_RIPPLE_CURR_H=%f  11SIG_RIPPLE_CURR_H=%f\n\n",fTemp,
			    //	g_SmbrcSampData.aRoughUnitData[i][SMBRC_RIPPLE_CURR_H].fValue);
			}
			else
			{
			    //No process!!
			}


			fTemp = GetFloatSigValue(SMBRC_GROUP_EQUIP_ID,
				SMBRC_SIG_SETTING_TYPE,
				SMBRC_HIGH_RESISTANCE_ID,
				"RS485SMBRC");
			if(BRC_OTHER_SIGNAL_NOT_EQUAL(roundf(fTemp), roundf(g_SmbrcSampData.aRoughUnitData[i][SMBRC_H_RESISTANCE].fValue)))
			{
				bNeedSamp0XE4Flag = TRUE;
				uiHighResistance = (UINT)(roundf(fTemp));
				Sleep(500);
				SMBRC_SendTwoParam(byActualAddr,
					(RS485_DEVICE_CLASS*)pSMBRCDeviceClass,
					SMBRC_SET_ALARM_THRES_CID2,
					SMBRC_SET_THRES_SIG_H_RESISTANCE,
					uiHighResistance,
					24);
				//printf("This is Cfg Unify SIG_H_RESISTANCE=%f  11SIG_H_RESISTANCE=%f\n\n",fTemp,
				//	g_SmbrcSampData.aRoughUnitData[i][SMBRC_H_RESISTANCE].fValue);
			}
			else
			{
				//No process!!
			}


			fTemp = GetFloatSigValue(SMBRC_GROUP_EQUIP_ID,
				SMBRC_SIG_SETTING_TYPE,
				SMBRC_LOW_RESISTANCE_ID,
				"RS485SMBRC");			
			if(BRC_OTHER_SIGNAL_NOT_EQUAL(roundf(fTemp), roundf(g_SmbrcSampData.aRoughUnitData[i][SMBRC_L_RESISTANCE].fValue)))
			{
				bNeedSamp0XE4Flag = TRUE;
				uiLowResistance = (UINT)(roundf(fTemp));
				Sleep(500);
				SMBRC_SendTwoParam(byActualAddr,
					(RS485_DEVICE_CLASS*)pSMBRCDeviceClass,
					SMBRC_SET_ALARM_THRES_CID2,
					SMBRC_SET_THRES_SIG_L_RESISTANCE,
					uiLowResistance,
					24);
				//printf("This is Cfg Unify SIG_L_RESISTANCE=%f  11SIG_L_RESISTANCE=%f\n\n",fTemp,
				//	g_SmbrcSampData.aRoughUnitData[i][SMBRC_L_RESISTANCE].fValue);
			}
			else
			{
				//No process!!
			}


			fTemp = GetFloatSigValue(SMBRC_GROUP_EQUIP_ID,
				SMBRC_SIG_SETTING_TYPE,
				SMBRC_HIGH_INTERCELL_ID,
				"RS485SMBRC");
			if(BRC_OTHER_SIGNAL_NOT_EQUAL(roundf(fTemp), roundf(g_SmbrcSampData.aRoughUnitData[i][SMBRC_H_INTERCELL].fValue)))
			{
				bNeedSamp0XE4Flag = TRUE;
				uiHighIntercell = (UINT)(roundf(fTemp));
				Sleep(500);
				SMBRC_SendTwoParam(byActualAddr,
					(RS485_DEVICE_CLASS*)pSMBRCDeviceClass,
					SMBRC_SET_ALARM_THRES_CID2,
					SMBRC_SET_THRES_SIG_H_INTERCELL,
					uiHighIntercell,
					24);
				//printf("This is Cfg Unify SIG_H_INTERCEL=%f  11SIG_H_INTERCEL=%f\n\n",fTemp,
				//	g_SmbrcSampData.aRoughUnitData[i][SMBRC_H_INTERCELL].fValue);
			}
			else
			{
				//No process!!
			}

			fTemp = GetFloatSigValue(SMBRC_GROUP_EQUIP_ID,
				SMBRC_SIG_SETTING_TYPE,
				SMBRC_LOW_INTERCELL_ID,
				"RS485SMBRC");
			if(BRC_OTHER_SIGNAL_NOT_EQUAL(roundf(fTemp), roundf(g_SmbrcSampData.aRoughUnitData[i][SMBRC_L_INTERCELL].fValue)))
			{
				bNeedSamp0XE4Flag = TRUE;
				uiLowIntercell = (UINT)(roundf(fTemp));
				Sleep(500);
				SMBRC_SendTwoParam(byActualAddr,
					(RS485_DEVICE_CLASS*)pSMBRCDeviceClass,
					SMBRC_SET_ALARM_THRES_CID2,
					SMBRC_SET_THRES_SIG_L_INTERCELL,
					uiLowIntercell,
					24);
				//printf("This is Cfg Unify SIG_H_INTERCEL=%f  11SMBRC_L_INTERCELL=%f\n\n",fTemp,
				//	g_SmbrcSampData.aRoughUnitData[i][SMBRC_L_INTERCELL].fValue);
			}
			else
			{
				//No process!!
			}

			SetUrgencySamp0XE4Flag(bNeedSamp0XE4Flag);

		}//end if
		else
		{
			//Continue
		}
	}//END for

	if (bNeedRefreshStringInfor)
	{
		SMBRC_RefreshStringSig();	
		return;
	}
	else
	{
		return;
	}
}
/*=============================================================================*
* FUNCTION: SMBRC	_SendCtrlCmd
* PURPOSE : Send control command to every Smbrc equipment ,start/stop test!!!!
* INPUT:	 	
*   
* RETURN:
*     0
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void SMBRC_SendCtrlCmd(RS485_DEVICE_CLASS*  pSMBRCDeviceClass,
					   INT32 iChannelNo, 
					   float fParam,
					   char *pString)
{
	INT32	i;
	INT32	iCtrlParam;
	BYTE	byCmdId;
	BYTE	byActualAddr;

	UNUSED(pString);

	//printf("\n$$$$$$$ This is into SMBRC_SendCtrlCmd  function $$$$$$$ \n");

	if (SMBRC_CTRL_RESIST_TEST_CH == iChannelNo)
	{
		//1.Get COMMAND_ID
		iCtrlParam = (INT32)fParam;
		if (SMBRC_START_REIST_STAT == iCtrlParam)
		{
			byCmdId = 0x01;//refer to the protocol 6.6 
		}
		else if(SMBRC_STOP_RESIST_STAT == iCtrlParam)
		{
			byCmdId = 0x02;//refer to the protocol 6.7 
		}
		else
		{
			TRACE("\n:::::: This is invalid Ctrl Param :::::::\n");	
			return;
		}

		if (SMBRC_NeedCommProc())
		{
		    SMBRC_ReopenPort();
		}

		//printf("\n$$$$$$$ SMBRC_SendCtrlCmd $$$$$$$ \n");
		//2.Send the Command id
		for (i = 0; i < SMBRC_ADDR_MAX__NUM; i++)
		{
			if (SMBRC_STAT_EXIST == 
				g_SmbrcSampData.aRoughUnitData[i][SMBRC_UNIT_EXIST_STAT].iValue)
			{
			    //printf("\n$$$$$$$ SMBRC_SendCtrlCmd byActualAddr=%d, byCmdId=%d \n",byActualAddr,byCmdId);
				byActualAddr = i + SMBRC_ADDR_START;
				SMBRC_SetParamOrCtrl(byActualAddr,
					(RS485_DEVICE_CLASS*)pSMBRCDeviceClass,
					SMBRC_CTRL_RESIST_TEST_CID2,
					byCmdId,
					SMBRC_UNUSED_PARAMETER,
					20);
			}
			else
			{
				//Needn't process!!
			}
		}
	}
	else
	{
		return;
	}
}

//static BOOL SMBRC_StringSigIsValid(BYTE *ReceiveFrame)
//{
//	BYTE byRecData;
//
//	byRecData = SMBRC_MergeAsc(ReceiveFrame);
//
//	if (0x20 == byRecData)
//	{
//		return TRUE;
//	}
//	else
//	{
//		return FALSE;
//	}
//}

//LOCAL void SMBRC_UnpackPrdctInfo(PRODUCT_INFO * pInfo, BYTE *ReceiveFrame)
//{
//	INT32	i;
//	BYTE*	pRcvBuf;
//
//	memset(pInfo,0,sizeof(PRODUCT_INFO));
//
//	pInfo->bSigModelUsed = TRUE;
//
//	//pInfo->szPartNumber[16]
//	//pInfo->szHWVersion[16]
//	//pInfo->szSWVersion[16]
//	//pInfo->szSerialNumber[32]
//
//	pRcvBuf = ReceiveFrame + SMBRC_DATAINFO_START;
//
//	//Serial Number(25 bytes)
//	for (i = 0; i < 25; i++)
//	{
//		if (SMBRC_StringSigIsValid((pRcvBuf + i * 2)))
//		{
//			continue;
//		}
//
//		pInfo->szSerialNumber[i] = SMBRC_MergeAsc((pRcvBuf + i * 2));
//	}
//
//	//Model Number(25 bytes)
//	pRcvBuf = ReceiveFrame + SMBRC_DATAINFO_START + 50;
//	for (i = 0; i < 16; i++)
//	{
//		if (SMBRC_StringSigIsValid((pRcvBuf + i * 2)))
//		{
//			continue;
//		}
//
//		pInfo->szPartNumber[i] = SMBRC_MergeAsc((pRcvBuf + i * 2));
//	}
//
//	//PCB revision
//	pRcvBuf = ReceiveFrame + SMBRC_DATAINFO_START + 100;
//	for (i = 0; i < 1; i++)
//	{
//		if (SMBRC_StringSigIsValid((pRcvBuf + i * 2)))
//		{
//			continue;
//		}
//
//		pInfo->szHWVersion[i] = SMBRC_MergeAsc((pRcvBuf + i * 2));
//	}
//
//	//Firmware version
//	pRcvBuf = ReceiveFrame + SMBRC_DATAINFO_START + 100 + 2;
//	for (i = 0; i < 16; i++)
//	{
//		if (SMBRC_StringSigIsValid((pRcvBuf + i * 2)))
//		{
//			continue;
//		}
//
//		pInfo->szSWVersion[i] = SMBRC_MergeAsc((pRcvBuf + i * 2));
//	}
//	
//}
//
///*=============================================================================*
//* FUNCTION: SMbrcGetProdctInfo
//* PURPOSE : Get SMbrc Product Info 
//* INPUT: 
//*     
//*
//* RETURN:
//*     void
//*
//* CALLS: 
//*     void
//*
//* CALLED BY: 
//*     INT32 main(INT32 argc, CHAR *argv[])
//*
//*============================================================================*/
//void SMBRC_GetProdctInfo(HANDLE hComm, int nUnitNo, void *pPI)
//{
//	UNUSED(hComm);
//	UNUSED(nUnitNo);
//	HANDLE	hComm1;
//	INT32	iReadLengh;
//	INT32	iCmdStat = SMBRC_SEND_CMD_ERROR;
//	BYTE	abyRcvBuf[SMBRC_RECV_BUF_MAX_NUM] = {0};
//	RS485_DEVICE_CLASS*	pSmbrcDeviceClass   = g_RS485Data.pDeviceClass + DEVICE_CLASS_SMBRC;
//	hComm1	 = pSmbrcDeviceClass->pCommPort->hCommPort;
//
//	PRODUCT_INFO*		pInfo;
//	INT32				i,j,k;
//	INT32				iAddr;
//
//
//	if (g_RS485Data.enumRunningMode == RS485_RUN_MODE_SLAVE )
//	{
//		return;
//	}
//
//	pInfo = (PRODUCT_INFO *)pPI;
//	
//	pInfo += SMBRC1_EID;
//
//	for (i = 0; i < SMBRC_ADDR_MAX__NUM; i++)
//	{
//		if (SMBRC_STAT_EXIST == 
//			g_SmbrcSampData.aRoughUnitData[i][SMBRC_UNIT_EXIST_STAT].iValue)
//		{
//			Sleep(10);
//
//			iAddr = i + SMBRC_ADDR_START;
//			
//			for (j = 0; j < 5; j++)
//			{
//				iCmdStat = SMBRC_PackAndSend(hComm1,
//											iAddr,
//											SMBRC_UNUSED_CMD_ID,
//											SMBRC_CID2_GET_PRDC_INFO,
//											18,
//											SMBRC_UNUSED_PARAMETER);
//
//				if (SMBRC_SEND_CMD_OK == iCmdStat)
//				{
//					if (SMBRC_RS485WaitReadable(hComm1, 500))
//					{
//						if (bValidRecvData(iAddr,
//											SMBRC_EQUIP_TYPE_CID1,
//											SMBRC_CID2_NORMAL_RTN,
//											abyRcvBuf,
//											iReadLengh))
//						{
//							pInfo->bSigModelUsed = TRUE;
//							SMBRC_UnpackPrdctInfo(pInfo, abyRcvBuf);
//							pInfo++;//������ǰ�ѷ�
//
//							break;
//						}
//						else
//						{
//							TRACE("\n::::::: CMD54	invalid  data :::::::\n");
//							//return FALSE;
//						}
//					}
//					else
//					{
//						TRACE("\n::::::: CMD54	Read is Time out :::::::\n");
//					}
//				}
//				else
//				{
//					TRACE("\n::::::: CMD54	Sending is Error :::::::\n");
//				}
//			}//end for j < 5
//		}
//		else
//		{
//			TRACE("\n::::::: The SMBRC Unit is Inexistence:::::::\n");
//		}
//	}//end for i < SMBRC_ADDR_MAX__NUM
//	
//}

LOCAL void SMBRC_UnpackPrdctInfo(PRODUCT_INFO * pInfo, BYTE *ReceiveFrame)
{
	INT32	i;
	BYTE byMainVer, bySubVer;
	BYTE *pRecBuf;
	memset(pInfo,0,sizeof(PRODUCT_INFO));

	pInfo->bSigModelUsed = TRUE;

	pRecBuf = ReceiveFrame + SMBRC_DATAINFO_START;
	
	//1.�ɼ������� //Collector Name SMBRC
	for (i = 0; i < 10; i++)
	{
		if ((0x20 != SMBRC_MergeAsc(pRecBuf + i * 2)) ||  (0xff != SMBRC_MergeAsc(pRecBuf + i * 2)))
		{
			pInfo->szPartNumber[i] = SMBRC_MergeAsc(pRecBuf + i * 2);
		}
	}

	//2.SW version process -- using another frame 1.01��ʽ
	byMainVer = SMBRC_MergeAsc(ReceiveFrame + 40 + 13);
	bySubVer  = SMBRC_MergeAsc(ReceiveFrame + 42 + 13);
	if ((byMainVer >= 0 && byMainVer <= 16) &&
		(bySubVer >= 0 && bySubVer <= 16))
	{
		sprintf(pInfo->szSWVersion, "%d.%02d\0", byMainVer, bySubVer);
	}

	//3.��ƷӲ���汾 Product version
	byMainVer = SMBRC_MergeAsc(ReceiveFrame + 44 + 13);
	bySubVer  = SMBRC_MergeAsc(ReceiveFrame + 46 + 13);

	if (byMainVer <= 0x01)//˵����1.02��ʽ
	{
		sprintf(pInfo->szHWVersion, "%d.%02d\0", byMainVer, bySubVer);
	}
	else if ((byMainVer >= 0x41) && (byMainVer <= 0x5A))//A00��ʽ
	{
		pInfo->szHWVersion[0] = byMainVer;
		sprintf(pInfo->szHWVersion + 1, "%02d\0", bySubVer);
	}
	else
	{
		//The Product version is Error!!!!
	}

	//4.Product serial no.
	pRecBuf = ReceiveFrame + SMBRC_DATAINFO_START + 20 + 20 + 4 + 4;//Refer to The Protocol document
	for (i = 0; i < 20; i++)
	{
		if ((0x20 != SMBRC_MergeAsc(pRecBuf + i * 2))||  (0xff != SMBRC_MergeAsc(pRecBuf + i * 2)))
		{
			pInfo->szSerialNumber[i] = SMBRC_MergeAsc(pRecBuf + i * 2);
		}
	}

	//printf("SMBRC BBU PartNumber:%s\n", pInfo->szPartNumber);
	//printf("SMBRC BBU SW version:%s\n", pInfo->szSWVersion);
	//printf("SMBRC BBU HW version:%s\n", pInfo->szHWVersion);
	//printf("SMBRC BBU Serial Number:%s\n", pInfo->szSerialNumber);

}

void SMBRC_GetProdctInfo(HANDLE hComm, int nUnitNo, void *pPI)
{
	UNUSED(hComm);
	UNUSED(nUnitNo);
	HANDLE	hComm1;
	INT32	iReadLengh;
	INT32	iCmdStat = SMBRC_SEND_CMD_ERROR;
	BYTE	abyRcvBuf[SMBRC_RECV_BUF_MAX_NUM] = {0};
	RS485_DEVICE_CLASS*	pSmbrcDeviceClass   = g_RS485Data.pDeviceClass + DEVICE_CLASS_SMBRC;
	hComm1	 = pSmbrcDeviceClass->pCommPort->hCommPort;
	RS485_DRV*	pPort				= (RS485_DRV *)hComm1;
	INT32		fd					= pPort->fdSerial;

	PRODUCT_INFO*		pInfo;
	INT32				i,j;
	INT32				iAddr;


	if (g_RS485Data.enumRunningMode == RS485_RUN_MODE_SLAVE )
	{
		//printf("\n\n $$$$$$$	This is running as Slave mode	$$$ \n\n");
		return;
	}

	if (SMBRC_NeedCommProc())
	{
	    SMBRC_ReopenPort();
	}

	pInfo = (PRODUCT_INFO *)pPI;

	pInfo += SMBRC1_EID;

	//printf("\n\n\n $$$$$$  This is IN Func SMBRC GetProdctInfo \n\n\n");

	for (i = 0; i < SMBRC_ADDR_MAX__NUM; i++)
	{
		if (SMBRC_STAT_EXIST == 
			g_SmbrcSampData.aRoughUnitData[i][SMBRC_UNIT_EXIST_STAT].iValue)
		{
			Sleep(10);

			iAddr = i + SMBRC_ADDR_START;

			//printf("  !!This is  Get Prdct addr=%d\n",iAddr);

			for (j = 0; j < 5; j++)
			{
				iCmdStat = SMBRC_PackAndSend(hComm1,
					iAddr,
					SMBRC_UNUSED_CMD_ID,
					SMBRC_CID2_GET_PRDC_INFO,
					18,
					SMBRC_UNUSED_PARAMETER);

				if (SMBRC_SEND_CMD_OK == iCmdStat)
				{
					if (SMBRC_RS485WaitReadable(fd, 1000))
					{
						if (SMBRC_ReadData(hComm1, abyRcvBuf, &iReadLengh, SMBRC_RECV_BUF_MAX_NUM))
						{
							if (bValidRecvData(iAddr,
								SMBRC_EQUIP_TYPE_CID1,
								SMBRC_CID2_NORMAL_RTN,
								abyRcvBuf,
								iReadLengh))
							{
								pInfo->bSigModelUsed = TRUE;
								SMBRC_UnpackPrdctInfo(pInfo, abyRcvBuf);
								pInfo++;//������ǰ�ѷ�

								break;
							}
							else
							{
								pInfo->bSigModelUsed = FALSE;
								//printf("::::::: CMD54	invalid  data=%s :::::::\n",abyRcvBuf);
								//return FALSE;
							}
						}
						else
						{
							//printf("::::::: CMD54	Read is Error  :::::::\n");
						}

					}
					else
					{
						//printf("::::::: CMD54	Read is Time out :::::::\n");
					}
				}
				else
				{
					//printf("::::::: CMD54	Sending is Error :::::::\n");
				}
			}//end for j < 5
		}
		else
		{
			//printf("::::::: The SMBRC Unit is Inexistence:::::::\n");
		}
	}//end for i < SMBRC_ADDR_MAX__NUM

}
 

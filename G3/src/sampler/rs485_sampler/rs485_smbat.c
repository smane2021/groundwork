/*============================================================================*
*         Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                          ALL RIGHTS RESERVED
*
*  PRODUCT  : Unified Data Type Definition
*
*  FILENAME : rs485_smbat.c
*  PURPOSE  : sampling  smbat data and control smac equip
*  
*  HISTORY  :
*    DATE            VERSION        AUTHOR            NOTE
*    2007-07-12      V1.0           IlockTeng         Created.    
*    2009-04-14     
*                                                     
*-----------------------------------------------------------------------------
*  GLOBAL VARIABLES
*    NAME                                    DESCRIPTION
*          
*      
*-----------------------------------------------------------------------------
*  GLOBAL FUNCTIONS
*    NAME                                    DESCRIPTION
*      
*    
*============================================================================*/

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <math.h>
#include <time.h>
#include "rs485_smbat.h"
#include "rs485_comm.h"

extern 	RS485_SAMPLER_DATA	g_RS485Data;
SMBAT_SAMP_DATA				g_SmbatSampleData;
BYTE						c_SMBATszVer[5] = {0};   
LOCAL void SMBAT_UnpackPrdctInfoAndBarCode(PRODUCT_INFO * pInfo,BYTE *ReceiveFrame,BYTE *ReceiveFrameTwo);
void SMBAT_SetComStat(int iFirstStringNo, int Adrres, int iComStat,BOOL beNeedClrTimes,RS485_DEVICE_CLASS*  SmbatDeviceClass);


LOCAL SMBATDATASTRUCT s_Str41SigInfo[] =
{
	//ǰ������һ���ź� ����Э�� ����+17
	{ 1,   0, 1,  0 },  // ���ص����ѹ1
	{ 1,   8, 1,  1 },  // ���ص����ѹ2
	{ 1,  16, 1,  2 },  // ���ص����ѹ3
	{ 1,  24, 1,  3 },  // ���ص����ѹ4
	{ 1,  32, 1,  4 },  // ���ص����ѹ5
	{ 1,  40, 1,  5 },  // ���ص����ѹ6
	{ 1,  48, 1,  6 },  // ���ص����ѹ7
	{ 1,  56, 1,  7 },  // ���ص����ѹ8
	{ 1,  64, 1,  8 },  // ���ص����ѹ9
	{ 1,  72, 1,  9 },  // ���ص����ѹ10
	{ 1,  80, 1, 10 },  // ���ص����ѹ11
	{ 1,  88, 1, 11 },  // ���ص����ѹ12
	{ 1,  96, 1, 12 },  // ���ص����ѹ13
	{ 1, 104, 1, 13 },  // ���ص����ѹ14
	{ 1, 112, 1, 14 },  // ���ص����ѹ15
	{ 1, 120, 1, 15 },  // ���ص����ѹ16
	{ 1, 128, 1, 16 },  // ���ص����ѹ17
	{ 1, 136, 1, 17 },  // ���ص����ѹ18
	{ 1, 144, 1, 18 },  // ���ص����ѹ19
	{ 1, 152, 1, 19 },  // ���ص����ѹ20
	{ 1, 160, 1, 20 },  // ���ص����ѹ21
	{ 1, 168, 1, 21 },  // ���ص����ѹ22
	{ 1, 176, 1, 22 },  // ���ص����ѹ23
	{ 1, 184, 1, 23 },  // ���ص����ѹ24
	{ 1, 192, 1, 24 },  // ���ص����ѹ25
	//  { 1, 200, 1, 25 },  // �����ѹ�ĸ���(M<=2)
	{ 1, 202, 1, 25 },  // �������ܵ�ѹ1
	{ 1, 210, 1, 26 },  // �������ܵ�ѹ2
	//  { 1, 218, 1, 26 },  // ������ģ������ͨ������(N<=5)
	{ 1, 220, 1, 27 },  // ������ģ������ͨ��1
	{ 1, 228, 1, 28 },  // ������ģ������ͨ��2
	{ 1, 236, 1, 29 },  // ������ģ������ͨ��3  //BPY 2004.12.31
	{ 1, 244, 1, 30 },  // ������ģ������ͨ��4
	{ 1, 252, 1, 31 },  // ������ģ������ͨ��5

	{ 0,  0,  0,  0 }   // ����
};

// ��ȡSM-BAT��������״̬CID2=43
LOCAL SMBATDATASTRUCT s_Str43SigInfo[] =
{
	//ǰ������һ���ź� ����Э�� ����+17
	{ 3,  0,  1,  0 },  // ������ͨ��1״̬  00--�͵�ƽ��01--�ߵ�ƽ
	{ 3,  2,  1,  1 },  // ������ͨ��2״̬  00--�͵�ƽ��01--�ߵ�ƽ
	{ 3,  4,  1,  2 },  // ������ͨ��3״̬  00--�͵�ƽ��01--�ߵ�ƽ
	{ 3,  6,  1,  3 },  // ������ͨ��4״̬  00--�͵�ƽ��01--�ߵ�ƽ
	{ 3,  8,  1,  4 },  // ������ͨ��5״̬  00--�͵�ƽ��01--�ߵ�ƽ
	{ 3, 10,  1,  5 },  // ������ͨ��6״̬  00--�͵�ƽ��01--�ߵ�ƽ
	{ 3, 12,  1,  6 },  // ������ͨ��7״̬  00--�͵�ƽ��01--�ߵ�ƽ
	{ 3, 14,  1,  7 },  // ������ͨ��8״̬  00--�͵�ƽ��01--�ߵ�ƽ
	//�����̵�������N N =2
	{ 3, 16,  1,  8},   // Relay 1״̬  00--�͵�ƽ��01--�ߵ�ƽ  //BPY 2005.01.31       22->18
	{ 3, 18,  1,  9},   // Relay 2״̬  00--�͵�ƽ��01--�ߵ�ƽ  //BPY 2005.01.31       24->20

	{ 0,  0,  0,  0 }   // ����
};

/*=============================================================================*
* FUNCTION: SmbatGetEquipAddr
* PURPOSE : get smabat address by iChannelNo according to smbat sig map
* INPUT:	 INT32					iChannelNo	
*			 RS485_DEVICE_CLASS*	SmbatDeviceClass	
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SmbatGetEquipAddr(RS485_DEVICE_CLASS*  SmbatDeviceClass,INT32 iChannelNo)
{
	INT32 iStringIdxNo;
	INT32 iSmbatAddr;	
	INT32 iResidual;
	INT32 iStartPosition;
	//1500--1599	
	//1600--1699	
	//1700--1799	
	//1800--1899	
	//1900--1999	
	//2000--2099	
	//2100--2199
	//...........as 1501 means first map  0
	iResidual = iChannelNo % MAX_CHN_NUM_PER_SMBAT;
	//iChannelNo/100 -15
	iStringIdxNo = (iChannelNo - iResidual) / MAX_CHN_NUM_PER_SMBAT - STRING_INDEX_PERTINENT;
	iStartPosition = iStringIdxNo * SMBAT_MAX_SIGNALS_NUM;


	iSmbatAddr	 = SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_ADDRESS].iValue;
	return iSmbatAddr;
}

BYTE SMBATAscToHex(char c)
{
	if (c >= '0' && c <= '9')
	{
		return c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		return c - 'A' + 10;
	}

	return 0xFF;
}
/*=============================================================================*
* FUNCTION: SMBATMergeAsc()
* PURPOSE : assistant function, used to merge two ASCII chars
* INPUT:	 CHAR*	p
*     
*
* RETURN:	 BYTE : merge result
*     
*
* CALLS: 
*			
*
* CALLED BY: 
*			copy from SMAC        DATE: 2009-05-08		14:39
*
*============================================================================*/
static BYTE SMBATMergeAsc(char *p)
{
	BYTE byHi; 
	BYTE byLo;

	byHi = SMBATAscToHex(p[0]);
	byLo = SMBATAscToHex(p[1]);

	//error data
	if ((byHi == 0xFF) || (byLo == 0xFF))
	{
		return 0;
	}

	return (byHi * 16 + byLo);
}

/*=============================================================================*
* FUNCTION: SMBAT_InitRoughValue
* PURPOSE : initialization smbat sampler in RoughData buffer  all signal
* INPUT:	 void* pDevice
*     
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMBAT_InitRoughValue(void* pDevice)
{
	int i, j;
	//pDevice g_RS485Data.pDeviceClass + i
	RS485_DEVICE_CLASS*  p_SmbatDeviceClass =  pDevice;
	p_SmbatDeviceClass->pRoughData			= (RS485_VALUE*)(&(g_SmbatSampleData.aRoughDataSmbat[0][0]));							
	p_SmbatDeviceClass->pfnSample			= (DEVICE_CLASS_SAMPLE)SMBATSample;		
	p_SmbatDeviceClass->pfnStuffChn			= (DEVICE_CLASS_STUFF_CHN)SMBAT_StuffChannel;
	p_SmbatDeviceClass->pfnSendCmd			= (DEVICE_CLASS_SEND_CMD)SMBAT_SendCtlCmd;

	for (i = 0; i < SMBAT_STRING_NUM; i++)//[0 ---- 19]
	{
		for (j = 0; j < SMBAT_MAX_SIGNALS_NUM; j++)
		{		
			p_SmbatDeviceClass->pRoughData[i * SMBAT_MAX_SIGNALS_NUM + j].iValue 
				= SMBAT_SAMP_INVALID_VALUE;//-9999
		}

		p_SmbatDeviceClass->pRoughData[ i * SMBAT_MAX_SIGNALS_NUM + SMBAT_WORKING_STAT].iValue
			= SMBAT_EQUIP_NOT_EXISTENT;
		p_SmbatDeviceClass->pRoughData[i * SMBAT_MAX_SIGNALS_NUM + SMBAT_INTERRUPT_TIMES].iValue
			= 0;
		p_SmbatDeviceClass->pRoughData[i * SMBAT_MAX_SIGNALS_NUM + SMBAT_STRING_NUM_OF_TYPE].iValue
			= 0;
	}

	for (i = 0; i < SMBAT_EQUIP_MAX_ADDR; i++)
	{
		g_SmbatSampleData.iRecordAddress[i] = SMBAT_SAMP_INVALID_VALUE;
	}
	p_SmbatDeviceClass->pRoughData[SMBAT_BATTERY_STRING_NUM].iValue = 0;
	p_SmbatDeviceClass->pRoughData[SMBAT_GROUP_WORKING_STAT].iValue = SMBAT_EQUIP_NOT_EXISTENT;
	p_SmbatDeviceClass->pRoughData[SMBAT_STRING_NUM_OF_TYPE].iValue = 4;

	p_SmbatDeviceClass->bNeedReconfig = TRUE;
	return 0 ;
}


/*=============================================================================*
* FUNCTION: CheckSum()
* PURPOSE : Cumulative  result
* INPUT:	 
*			 CHAR*	Frame   :IN OUT 
*
* RETURN:
*			static void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     copy from SMAC        DATE: 2009-05-08		14:39
*
*============================================================================*/
static void SMBAT_CheckSum( BYTE *Frame )
{
	INT32	i;    
	WORD	R	 = 0;
	INT32   nLen = (int)strlen( (char*)Frame );  

	for ( i = 1; i < nLen; i++ )
	{
		R += *(Frame + i);
	}

	sprintf( (char *)Frame + nLen, "%04X\r", (WORD) - R );
}

/*=============================================================================*
* FUNCTION: RecvData FromSMBAT()
* PURPOSE  :receive data of SM from 485 com
* RETURN   : 
* ARGUMENTS:
*								CHAR*	sRecStr :		
*								hComm	 :	485 com handle 
*								iStrLen	 :
* CALLS    : 
* CALLED BY: 
*								DLLExport BOOL Query()
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
LOCAL BOOL	RecvDataFromSMBAT(HANDLE hComm, CHAR* sRecStr, INT32* piStrLen,INT32 iWantToReadnByte)
{
	INT32	iReadLen;		
	INT32	i		= 0;
	INT32	j		= 0;
	INT32	iHead	= -1;
	INT32	iTail	= -1;

	ASSERT(hComm != 0 );
	ASSERT(sRecStr != NULL);

	//Read the data from smbat
	//iReadLen = RS485_Read(hComm, sRecStr, iWantToReadnByte);
	//2011/02/16����˵������ͨ���жϣ�ʵ��û�취�ٽ����ݴ�
	iReadLen = RS485_ReadForSMBRC(hComm, (BYTE*)sRecStr, iWantToReadnByte);

	for (i = 0; i < iReadLen; i++)
	{
		if(bySOI == *(sRecStr + i))
		{
			iHead = i;
			break;
		}
	}

	if (iHead < 0)	//   no head
	{
		//TRACE(" BAT  BAT  BAT  RRR (iHead < 0)no head %d %s \n",iReadLen,sRecStr);
		return FALSE;
	}

	for (i = iHead + 1; i < iReadLen; i++)
	{
		if(byEOI == *(sRecStr + i))
		{
			iTail = i;
			break;
		}
	}		

	if (iTail < 0)	//   no tail
	{
		//TRACE(" BAT  BAT  BAT  RRR (iTail < 0)  no tail%d  %s \n",iReadLen,sRecStr);
		return FALSE;
	}	

	//strlength
	*piStrLen = iTail - iHead + 1;

	if(iHead > 0)
	{
		for (j = iHead; j < *piStrLen; j++)
		{
			*(sRecStr + (j - iHead)) = *(sRecStr + j);
		}
	}

	*(sRecStr + (iTail - iHead + 2)) = '\0';
	return TRUE;
}



/*=============================================================================*
* FUNCTION: RS485WaitReadable
* PURPOSE  : wait RS485 data ready
* RETURN   : int : 1: data ready, 0: timeout,Allow get mode or auto config
* ARGUMENTS:
*						int fd	: 	
*						int TimeOut: seconds 
* CALLS    : 
* CALLED BY: 
*								DLLExport BOOL Query()
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
LOCAL INT32 RS485WaitReadable(INT32 fd, INT32 nmsTimeOut)
{
	fd_set				readfd;
	struct	timeval		timeout;

	while (nmsTimeOut > 0)
	{
		// need add ClearWDT() here.
		if (nmsTimeOut < (5 * 1000))
		{
			timeout.tv_usec = nmsTimeOut % 1000 * 1000;		/* usec     */
			timeout.tv_sec  = nmsTimeOut / 1000;			/* seconds  */
			nmsTimeOut = 0;
		}
		else
		{
			timeout.tv_usec = 0;							/* usec     */
			timeout.tv_sec  = 5;							/* seconds  */
			RUN_THREAD_HEARTBEAT();
			nmsTimeOut -= (5 * 1000);
		}
	}

	FD_ZERO(&readfd);										/*  Initializes the set to the NULL set. */
	FD_SET(fd, &readfd);									/*  Adds descriptor s to set. */

	select(fd+1, &readfd, NULL, NULL, &timeout);

	if (FD_ISSET(fd, &readfd))	
	{
		return TRUE;										//1: data ready, can be read
	}
	else
	{
		return FALSE;										//0: Data not ready, can't read!!but,Allow get mode or auto config
	}
}

/*=============================================================================*
* FUNCTION: CheckStrLength
* PURPOSE : Check receive data length 
* INPUT:	BYTE *abyRcvBuf
*			INT32 RecBufOffsetNum,
*			INT32 RecDataLength
* RETURN:
*     INT32   1:  succeed	0:  error  
*		
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*
*============================================================================*/
LOCAL  INT32 CheckStrLength(BYTE *abyRcvBuf, INT32 RecBufOffsetNum, INT32 RecDataLength)
{
	WORD		wLength;
	CHAR		c_CheckLength[5]	= {0};
	INT32		OffsetToLength		= RecBufOffsetNum;

	//repeat  length check
	wLength							= MakeDataLen(RecDataLength);
	sprintf((char *)c_CheckLength,"%04X\0",wLength);

	if((abyRcvBuf[OffsetToLength + 3]		!= c_CheckLength[3]) //fourth byte of  length check
		&& (abyRcvBuf[OffsetToLength + 2]	!= c_CheckLength[2]) //third  byte of  length check
		&&(abyRcvBuf[OffsetToLength + 1]	!= c_CheckLength[1]) //second byte of  length check
		&& (abyRcvBuf[OffsetToLength]		!= c_CheckLength[0]))//first  byte of  length check
	{
		return FALSE;
	}

	return TRUE;
}
/*=============================================================================*
* FUNCTION: CheckStrSum
* PURPOSE : Check receive data sum 
* INPUT: 
*     
*
* RETURN:
*     INT32   1:  succeed	0:  error  
*		
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*
*============================================================================*/
LOCAL INT32  CheckStrSum(CHAR *abyRcvBuf, INT32 RecTotalLength)
{
	CHAR	 szCheckCode[5]	= {0};
	//copy receive checkSum to szCheckCode 
	strncpy(szCheckCode, (CHAR*)abyRcvBuf + RecTotalLength - SMBAT_SHECKSUM_EOI,4); 
	abyRcvBuf[RecTotalLength - SMBAT_SHECKSUM_EOI] = 0;

	//repeat check sum
	SMBAT_CheckSum((BYTE*)abyRcvBuf);

	if((abyRcvBuf[RecTotalLength - 2]		== szCheckCode[3])  //fourth byte of check sum
		&& (abyRcvBuf[RecTotalLength - 3]	== szCheckCode[2])	//third	 byte of check sum
		&&(abyRcvBuf[RecTotalLength - 4]	== szCheckCode[1])	//second byte of check sum
		&& (abyRcvBuf[RecTotalLength - 5]	== szCheckCode[0]))	//first  byte of check sum
	{ 
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*=============================================================================*
* FUNCTION: GetResponeData
* PURPOSE : send  data to smbat and receive data from smbat
* INPUT: 
*     
*
* RETURN:	RESPONSE_OK
*			NO_RESPONSE_BEXISTENCE
*			
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*copy from smio
*============================================================================*/
LOCAL INT32 SMBATSendAndRead(
			     HANDLE	hComm,			
			     BYTE		*sSendStr,		 
			     BYTE		*abyRcvBuf,      
			     INT32	nSend,			 
			     int		cWaitTime,
			     INT32	iWantToReadnByte)					
{  
	INT32		iReceiveNum;
	//INT32		iReceiveNumByClr;
	INT32		iDataInfoLength;
	RS485_DRV	*pPort			 = (RS485_DRV *)hComm;
	INT32		i_fd			 = pPort->fdSerial;
	INT32		iBlockTime		 = 500;

	ASSERT( (hComm != 0) && (sSendStr != NULL) && (abyRcvBuf != NULL));

	RS485_ClrBuffer(hComm);

	if (RS485_Write(hComm, sSendStr, nSend) != nSend)
	{
		//note	app  log	!!
		AppLogOut("SMBATGet ResponeData", 
			APP_LOG_UNUSED, 
			"[%s]-[%d] ERROR: failed to  RS485_Write \n\r",
			__FILE__, __LINE__);
		TRACE("\n*****   !!!!! SMBAT  RS485_Write   ******\n");
		return NO_RESPONSE_BEXISTENCE;
	}

	Sleep(5);

	if (RS485_COMM_ABNORMALITY != cWaitTime)
	{
		/*
		QUERY���20Sʱ��һ��������

		���������ж�ͨ��ʧ�ܵĴ���
		���ж�ͨ���жϵĴ����Ӵ�
		*/
		iBlockTime = 2700;
	}
	else//communication fail
	{
		iBlockTime = 200;
	}

	if (RS485WaitReadable(i_fd, iBlockTime) > 0 )
	{
		if (RecvDataFromSMBAT(hComm, (CHAR*)abyRcvBuf, &iReceiveNum,iWantToReadnByte))
		{
			//check cid1
			if ((abyRcvBuf[5] != sSendStr[5]) || (abyRcvBuf[6] != sSendStr[6]))
			{
				TRACE("\n*****SMBAT  check cid1  error ******\n");
				return NO_RESPONSE_BEXISTENCE;					
			}
			else if ((abyRcvBuf[3] != sSendStr[3]) || (abyRcvBuf[4] != sSendStr[4]))//check equip addr 
			{
				TRACE("\n*****SMBAT  check equip addr  error ******\n");
				return NO_RESPONSE_BEXISTENCE;						
			}
			else  if ((abyRcvBuf[7] != '0') || (abyRcvBuf[8] != '0') )//check RTN
			{
				TRACE("\n*****SMBAT check RTN  error ******\n");
				return NO_RESPONSE_BEXISTENCE;
			}
			else
			{
				//check  strlen		DATAINFO = TOTALNumb - 5 - 13 
				iDataInfoLength = iReceiveNum - SMBAT_SHECKSUM_EOI - DATA_YDN23_SMBAT;
				//	check  strlen 
				if (!CheckStrLength(abyRcvBuf, LENGTH_YDN23_SMBAT, iDataInfoLength))
				{
					TRACE("\n*****SMBAT  check StrLength error ******\n");
					return NO_RESPONSE_BEXISTENCE;
				}

				// check sum
				if(CheckStrSum((CHAR*)abyRcvBuf, iReceiveNum))
				{
					return RESPONSE_OK;

				}
				else
				{
					TRACE("\n*****SMBAT  check sum error ******\n");
					return NO_RESPONSE_BEXISTENCE;
				}
			}
		}
		else
		{
			//RS485_ClrBuffer(hComm);
			TRACE("\n*****SMBAT  else  read 0 ******\n");
			return NO_RESPONSE_BEXISTENCE;
		}
	}
	else
	{
		TRACE("\n*****SMBAT  RS485WaitReadable  read 0 ******\n");
		return NO_RESPONSE_BEXISTENCE;
	}
}
/*=============================================================================*
* FUNCTION: SMBATGetVer
* PURPOSE : get smbat version for scan smbat equip 
* INPUT: 
* RETURN:
*		szVer   OUT
*		INT32   0:  succeed	 1:fail to communication 
*	
*
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*copy from smio
*============================================================================*/
LOCAL INT32 SMBATGetVer( HANDLE hComm, INT32 nUnitNo, BYTE cCID1, BYTE cCID2, BYTE*szVer)
{
	BYTE	szSendStr[20]	= {0};
	CHAR	abyRcvBuf[MAX_RECVBUFFER_NUM_SMBAT] = {0};
	INT32	iSmbatStatus	= SMBAT_SAMP_INVALID_VALUE;

	sprintf( (char *)szSendStr, 
		"~%02X%02X%02X%02X%04X\0",
		0x20, 
		(unsigned int)nUnitNo, 
		cCID1, 
		cCID2, 
		0 );
	SMBAT_CheckSum( szSendStr );
	//1	  1	  1	  1	   1        2	   X	2   1
	//1	  2	  2	  2	   2	    4	   2X	4   1
	//SOI VER ADR CID1 CID2/RTN	LENGTH INFO	SUM	EOI
	*(szSendStr + 17) =  byEOI;
	/*
	RETURN:
	INT32   0:  succeed	 1:fail to communication 
	*/
	iSmbatStatus = SMBATSendAndRead( hComm, szSendStr, (BYTE*)abyRcvBuf, 18, RS485_COMM_NORMAL, 512);

	if(iSmbatStatus == RESPONSE_OK)
	{
		strncpy((char*)szVer, (char*)abyRcvBuf + 1, 2);
	}

	return  iSmbatStatus;
}
/*=============================================================================*
* FUNCTION: SMBATGetVer
* PURPOSE : get smbat version for scan smbat equip 
* INPUT: 
* RETURN:
*		szVer   OUT
*		INT32   0:  succeed	 1:fail to communication 
*	
*
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*copy from smio
*============================================================================*/
LOCAL INT32 SMBATCelerityGetVer( HANDLE hComm, INT32 nUnitNo, int cCID1, int cCID2, BYTE*szVer)
{
	BYTE	szSendStr[20]	= {0};
	CHAR	abyRcvBuf[MAX_RECVBUFFER_NUM_SMBAT] = {0};
	INT32	iReceiveNum;
	INT32	iDataInfoLength;

	sprintf( (char *)szSendStr, 
		"~%02X%02X%02X%02X%04X\0",
		0x20, 
		(unsigned int)nUnitNo, 
		cCID1, 
		cCID2, 
		0 );
	SMBAT_CheckSum( szSendStr );

	*(szSendStr + 17) =  byEOI;

	if (RS485_Write(hComm, szSendStr, 18) != 18)
	{
		//note	app  log	!!
		AppLogOut("SMBATGet Vertion", 
			APP_LOG_UNUSED, 
			"[%s]-[%d] ERROR: failed to  RS485_Write \n\r",
			__FILE__, __LINE__);
		return NO_RESPONSE_BEXISTENCE;
	}

	Sleep(500);

	if (RecvDataFromSMBAT(hComm, abyRcvBuf, &iReceiveNum,1024))
	{
		//check cid1
		if ((abyRcvBuf[5] != szSendStr[5]) || (abyRcvBuf[6] != szSendStr[6]))
		{
			TRACE("\n*****SMBAT  check cid1  error ******\n");
			return NO_RESPONSE_BEXISTENCE;					
		}
		else if ((abyRcvBuf[3] != szSendStr[3]) || (abyRcvBuf[4] != szSendStr[4]))//check equip addr 
		{
			TRACE("\n*****SMBAT  check equip addr  error ******\n");
			return NO_RESPONSE_BEXISTENCE;						
		}
		else  if ((abyRcvBuf[7] != '0') || (abyRcvBuf[8] != '0') )//check RTN
		{
			TRACE("\n*****SMBAT check RTN  error ******\n");
			return NO_RESPONSE_BEXISTENCE;
		}
		else
		{
			//check  strlen		DATAINFO = TOTALNumb - 5 - 13 
			iDataInfoLength = iReceiveNum - SMBAT_SHECKSUM_EOI - DATA_YDN23_SMBAT;
			//	check  strlen 
			if (!CheckStrLength((BYTE*)abyRcvBuf, LENGTH_YDN23_SMBAT, iDataInfoLength))
			{
				TRACE("\n*****SMBAT  check StrLength error ******\n");
				return NO_RESPONSE_BEXISTENCE;
			}

			// check sum
			if(CheckStrSum(abyRcvBuf, iReceiveNum))
			{
				strncpy((char*)szVer, (char*)abyRcvBuf + 1, 2);
				return RESPONSE_OK;
			}
			else
			{
				TRACE("\n*****SMBAT  check sum error ******\n");
				return NO_RESPONSE_BEXISTENCE;
			}
		}
	}
	else
	{
		TRACE("\n*****SMBAT  else  read 0 ******\n");
		return NO_RESPONSE_BEXISTENCE;
	}
}
/*=============================================================================*
* FUNCTION: SMBATSendControlCmd45
* PURPOSE : initialization
* INPUT: 
*     BOOL bDisableDog  // TRUE:disable the dog��FALSE:enable the dog
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
BOOL SMBATSendControlCmd45(
			   HANDLE hComm,			
			   INT32 nUnitNo,			
			   INT32 nChannelNo,		
			   INT32 nInvalid,			
			   INT32 nControlType,		
			   INT32 nPulseTime																				
			   )	
{
	INT32	SmbatStatus;
	//INT32	i;
	WORD	wLength;
	BYTE	szSendStr[100]						= {0};
	BYTE	abyRcvBuf[MAX_RECVBUFFER_NUM_SMBAT] = {0};    
	//BYTE	szVer[5]							= {0};  
	//INT32	CmdType								= 0;
	//INT32	nLen								= 0;
	SmbatStatus									= SMBAT_SAMP_INVALID_VALUE;

	//1500--1599	
	//1600--1699	
	//1700--1799	
	// .........���緢��1501�൱��1501%100
	nChannelNo -= (nChannelNo / 100) * 100;

	if((nChannelNo < 0) || (nChannelNo > 1))
	{
		return FALSE;
	}

	if((nInvalid < 0) || (nInvalid > 1))
	{
		return FALSE;
	}

	if(nPulseTime > 2600)
	{
		nPulseTime = 2600;
	}

	if(nPulseTime < 100)
	{
		nPulseTime = 100;
	}

	wLength = MakeDataLen( 6 );      

	nChannelNo = nChannelNo + 41;
	//nControlType      //ֻ��4�����0-��͵�ƽ(10H)��1-��ߵ�ƽ(11H)��2-������(12H)��3-������(13H)
	//ex��0,1,2,2000    ���ͣ���һ��Relay�������壬���Ϊ2s
	if(nInvalid == 0x01)
	{
		nControlType = nControlType + 0x10;				
	}
	else if(nInvalid == 0x00)
	{
		if(nControlType==0x00 || nControlType==0x02)
		{
			nControlType = nControlType + 0x11;				
		}
		else if(nControlType==0x01 || nControlType==0x03)
		{
			nControlType = nControlType + 0x0F;				
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}

	sprintf((char*)szSendStr,
		"~%s%02X%02X%02X%04X\0",
		c_SMBATszVer, 
		(unsigned int)nUnitNo, 
		SMBATCID1, 
		SMBATCID2_SENT_CTRL,
		wLength);
	//nChannelNo,
	//nControlType,
	//nPulseTime/100 );
	//02X%02X%02X
	sprintf((char*)(szSendStr + 13 ),
		"%02X\0",
		(unsigned int)nChannelNo);
	sprintf((char*)(szSendStr + 15),
		"%02X\0",
		(unsigned int)nControlType);
	sprintf((char*)(szSendStr + 17),
		"%02X\0",
		(unsigned int)(nPulseTime/100));

	SMBAT_CheckSum( szSendStr );

	if (SMBATNeedCommProc())
	{
		SMBATReopenPort();
	}

	SmbatStatus = SMBATSendAndRead( hComm, szSendStr,abyRcvBuf, 24, RS485_COMM_NORMAL, 512);
	return SmbatStatus;
}
/*=============================================================================*
* FUNCTION: SMBAT_SendCtlCmd
* PURPOSE : control smbat by iChannelNo
* INPUT: 
*    
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
VOID SMBAT_SendCtlCmd(RS485_DEVICE_CLASS*  SmbatDeviceClass,
		      INT32 iChannelNo,
		      float fParam,
		      CHAR *pString)	
{
	INT32	iUnitNo;
	INT32	iCtrlInvalidChannelNo;
	INT32   nPointStr;
	INT32   CtrlType;
	INT32	iPulseTime;	
	BYTE	sTarget[512]	= {0};
	nPointStr				= 0;

	if (RS485_RUN_MODE_SLAVE == g_RS485Data.enumRunningMode)
	{
		return;
	}

	if(iChannelNo < SMBAT_CH_START || SMBAT_CH_END < iChannelNo)
	{
		return ;//no SMBAT
	}

	//get addr
	iUnitNo = SmbatGetEquipAddr(SmbatDeviceClass, iChannelNo);
	//param4
	iCtrlInvalidChannelNo = (INT32)fParam;
	//param5	
	nPointStr = RS485_StrExtract(pString, sTarget, ',');

	if(sTarget[0] == 0)
	{
		return;
	}

	CtrlType = atoi((char*)sTarget);
	//param6
	RS485_StrExtract(pString + nPointStr, sTarget, ',');

	if(sTarget[0] == 0)
	{
		return;
	}

	iPulseTime = atoi((char*)sTarget);
	//send ctrl data to smbat
	SMBATSendControlCmd45(SmbatDeviceClass->pCommPort->hCommPort,
		iUnitNo,
		iChannelNo,
		iCtrlInvalidChannelNo,
		CtrlType,
		iPulseTime);
}

/*=============================================================================*
* FUNCTION: FixFloatDat
* PURPOSE : eight byte convert to float
* INPUT:	 char* sStr	
*     
*
* RETURN:
*			 float 
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*		kenn.wan                 DATE: 2008-08-15 14:55
*============================================================================*/
static float FixFloatDat( char* sStr )
{
	INT32					i;
	SMBATSTRTOFLOAT			floatvalue;
	CHAR					cTemp;
	CHAR					cTempHi;
	CHAR					cTempLo;

	for( i=0; i<4; i++ )
	{
#ifdef _CPU_BIG_ENDIAN		// ppc
#ifdef _SAMPLER_REPLACE_SSCANF
		//TRACE("------------------------1----------------------------\n");
		sscanf((const char *)sStr+i, "%x", 
			&floatvalue.by_value[3-i]);
#else
		int	c;
		//TRACE("------------------------2----------------------------\n");
		sscanf((const char *)sStr+i, "%x", 	&c);
		floatvalue.by_value[3-i] = (BYTE)c;
#endif
#else	// for x86
		//hi bit
		if((*(sStr + i * 2) > 0x40))
		{
			cTempHi = (*(sStr + i * 2) - 0x40) + 9;
		}
		else
		{
			cTempHi = (*(sStr + i * 2) - 0x30);
		}

		//low bit
		if((*(sStr + i * 2 + 1)) > 0x40)
		{
			cTempLo = (*(sStr + i * 2 + 1) - 0x40) + 9;
		}
		else
		{
			cTempLo = (*(sStr + i * 2 + 1) - 0x30);
		}

		cTemp = cTempHi * 16 + cTempLo;
		floatvalue.by_value[i] = cTemp;

#endif
	}

	return floatvalue.f_value;
}
/*=============================================================================*
* FUNCTION: SMBAT_GetAddrByEquipType
* PURPOSE : Scan first Adrress of smbat equip for every Type
* INPUT: 
*     
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMBAT_GetAddrByEquipType(void* pDevice, INT32* piAddrStart,INT32* piAddrEnd)
{
	UNUSED(pDevice);
	//RS485_DEVICE_CLASS* SmbatDeviceClass	= pDevice;
	INT32				SmbatStatus			= SMBAT_SAMP_INVALID_VALUE;
	INT32				iTryTimes			=0;

	//4802
	for (iTryTimes = 0; iTryTimes < 2; iTryTimes++)
	{
		SmbatStatus	= SMBATCelerityGetVer(g_RS485Data.CommPort.hCommPort,
			SMBAT4802_ADDR_START, 
			SMBATCID1,
			SMBATCID2_GET_VERSION,
			c_SMBATszVer
			);

		if (RESPONSE_OK == SmbatStatus)
		{
			*piAddrStart = SMBAT4802_ADDR_START;
			*piAddrEnd   = SMBAT4802_ADDR_END;
			return TRUE;
		}
	}
	//4806
	for (iTryTimes = 0; iTryTimes < 2; iTryTimes++)
	{
		//4806
		SmbatStatus	= SMBATCelerityGetVer(g_RS485Data.CommPort.hCommPort,
			SMBAT4806_ADDR_START, 
			(SMBATCID1),
			(SMBATCID2_GET_VERSION),
			c_SMBATszVer
			);

		if (RESPONSE_OK == SmbatStatus)
		{
			*piAddrStart = SMBAT4806_ADDR_START;
			*piAddrEnd    = SMBAT4806_ADDR_END;
			return TRUE;
		}
	}

	//4812
	for (iTryTimes = 0; iTryTimes < 2; iTryTimes++)
	{
		SmbatStatus	= SMBATCelerityGetVer(g_RS485Data.CommPort.hCommPort,
			SMBAT4812_ADDR_START, 
			SMBATCID1,
			SMBATCID2_GET_VERSION,
			c_SMBATszVer
			);

		if (RESPONSE_OK == SmbatStatus)
		{
			*piAddrStart = SMBAT4812_ADDR_START;
			*piAddrEnd    = SMBAT4812_ADDR_END;
			return TRUE;
		}
	}

	//2402
	for (iTryTimes = 0; iTryTimes < 2; iTryTimes++)
	{
		//2402
		SmbatStatus	= SMBATCelerityGetVer(g_RS485Data.CommPort.hCommPort,
			SMBAT2402_ADDR_START, 
			SMBATCID1,
			SMBATCID2_GET_VERSION,
			c_SMBATszVer
			);

		if (RESPONSE_OK == SmbatStatus)
		{
			*piAddrStart = SMBAT2402_ADDR_START;
			*piAddrEnd   = SMBAT2402_ADDR_END;
			return TRUE;
		}
	}

	for (iTryTimes = 0; iTryTimes < 2; iTryTimes++)
	{
		//2402
		SmbatStatus	= SMBATCelerityGetVer(g_RS485Data.CommPort.hCommPort,
			SMBAT2402_ADDR_START, 
			SMBATCID1,
			SMBATCID2_GET_VERSION,
			c_SMBATszVer
			);

		if (RESPONSE_OK == SmbatStatus)
		{
			*piAddrStart = SMBAT2402_ADDR_START;
			*piAddrEnd   = SMBAT2402_ADDR_END;
			return TRUE;
		}
	}
	//2406
	for (iTryTimes = 0; iTryTimes < 2; iTryTimes++)
	{
		SmbatStatus	= SMBATCelerityGetVer(g_RS485Data.CommPort.hCommPort,
			SMBAT2406_ADDR_START, 
			SMBATCID1,
			SMBATCID2_GET_VERSION,
			c_SMBATszVer
			);

		if (RESPONSE_OK == SmbatStatus)
		{
			*piAddrStart = SMBAT2406_ADDR_START;
			*piAddrEnd   = SMBAT2406_ADDR_END;
			return TRUE;
		}
	}	
	//2412
	for (iTryTimes = 0; iTryTimes < 2; iTryTimes++)
	{
		SmbatStatus	= SMBATCelerityGetVer(g_RS485Data.CommPort.hCommPort,
			SMBAT2412_ADDR_START, 
			SMBATCID1,
			SMBATCID2_GET_VERSION,
			c_SMBATszVer
			);

		if (RESPONSE_OK == SmbatStatus)
		{
			*piAddrStart = SMBAT2412_ADDR_START;
			*piAddrEnd   = SMBAT2412_ADDR_END;
			return TRUE;
		}
	}

	return FALSE;//failed
}
/*=============================================================================*
* FUNCTION: SMBATReconfig
* PURPOSE : scan smbat equip, record equip all signal info
* INPUT: 
*     
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMBATReconfig(void* pDevice)
{
	INT32				i;
	//INT32				j;
	INT32				iAddr;
	INT32				iAddrIndex;
	INT32				iAddrStart;
	INT32				iAddrEnd;
	INT32				iStartPosition;
	BOOL				ibhave48vstrings;
	RS485_DEVICE_CLASS* SmbatDeviceClass	= pDevice;
	INT32				SmbatStatus			= SMBAT_SAMP_INVALID_VALUE;
	INT32				iStringNum			= 0;
	ibhave48vstrings						= FALSE;					
	iAddrStart								= 0;
	iAddrEnd								= -1;
	iAddrIndex								= 0;
	BYTE					cExistMaxAddr   = 0;

	//First Get the already Connecting SMbat Equipment Address 
	if(SMBAT_GetAddrByEquipType(pDevice, &iAddrStart,&iAddrEnd))
	{
		//Start Scan 
		for (iAddr = iAddrStart; iAddr <= iAddrEnd; iAddr++)
		{
			for (i = 0; i < RS485_RECONNECT_TIMES; i++)
			{
				SmbatStatus	= SMBATCelerityGetVer(SmbatDeviceClass->pCommPort->hCommPort,
					iAddr, 
					SMBATCID1,
					SMBATCID2_GET_VERSION,
					c_SMBATszVer
					);

				if (SmbatStatus == RESPONSE_OK)
				{
					g_SmbatSampleData.iRecordAddress[iAddrIndex] = iAddr;
					iAddrIndex++;
					//printf("\n  RECONFIG   SMBAT iAddr = %d \n",iAddr);
					//TRACE("\n  SMBAT   SMBAT iAddr = %d \n",iAddr);
					cExistMaxAddr = g_RS485Data.stSMSmplingCtrl.cExistMaxAddr;
					g_RS485Data.stSMSmplingCtrl.cSAMPExistAddr[cExistMaxAddr] = iAddr;
					g_RS485Data.stSMSmplingCtrl.cExistMaxAddr++;

					break;
				}
			}

			//1.succeed
			if (SmbatStatus == RESPONSE_OK)
			{	
				if (iAddr >= SMBAT4802_ADDR_START && iAddr <= SMBAT4802_ADDR_END)//4802
				{	
					if (iStringNum >= SMBAT_STRING_NUM)//max 20 batteries string
					{	
						AppLogOut("SMBAT Reconfig", 
							APP_LOG_UNUSED,
							"[%s]-[%d] ERROR: failed to  iStringNum overflow,\
							Please check smmodules error \n\r", 
							__FILE__, __LINE__);	
						return -1;
					}

					ibhave48vstrings = TRUE;
					iStartPosition = iStringNum * SMBAT_MAX_SIGNALS_NUM;
					SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_COMMUNICATE_STAT].iValue = SMBAT_COMM_OK;
					SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_WORKING_STAT].iValue = SMBAT_EQUIP_EXISTENT;
					SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_ADDRESS].iValue = iAddr;
					SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_STRING_NUM_OF_TYPE].iValue = SMBAT_4802_STRING_NUM;
					iStringNum++;
				}//end	4802
				//else if (iAddr >= SMBAT4806_ADDR_START && iAddr <= SMBAT4812_ADDR_END)
				else if (iAddr >= SMBAT4806_ADDR_START && iAddr <= SMBAT4806_ADDR_END)
				{
					//4806:21 to 30    2 strings

					for (i = 0; i < SMBAT_4806_STRING_NUM; i++)
					{
						if(iStringNum >= SMBAT_STRING_NUM)//max 20 batteries string
						{	
							AppLogOut("SMBAT Reconfig", APP_LOG_UNUSED,
								"[%s]-[%d] ERROR: failed to  iStringNum overflow,\
								Please check sm modules error \n\r",
								__FILE__, __LINE__);	

							return -1;
						}

						ibhave48vstrings = TRUE;
						iStartPosition = iStringNum * SMBAT_MAX_SIGNALS_NUM;
						SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_COMMUNICATE_STAT].iValue = SMBAT_COMM_OK;
						SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_WORKING_STAT].iValue = SMBAT_EQUIP_EXISTENT;
						SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_ADDRESS].iValue = iAddr;
						SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_STRING_NUM_OF_TYPE].iValue = SMBAT_4806_STRING_NUM;
						iStringNum++;
					}
				}//end  21---30
				else if (iAddr >= SMBAT4812_ADDR_START && iAddr <= SMBAT4812_ADDR_END)
				{
					//4812:31 to 40   4 strings
					for (i = 0; i < SMBAT_4812_STRING_NUM; i++)
					{
						if(iStringNum >= SMBAT_STRING_NUM)//max 20 batteries string
						{	
							AppLogOut("SMBAT Reconfig", APP_LOG_UNUSED,
								"[%s]-[%d] ERROR: failed to  iStringNum overflow,\
								Please check sm modules error \n\r",
								__FILE__, __LINE__);	
							return -1;
						}
						ibhave48vstrings = TRUE;
						iStartPosition = iStringNum * SMBAT_MAX_SIGNALS_NUM;
						SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_COMMUNICATE_STAT].iValue = SMBAT_COMM_OK;
						SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_WORKING_STAT].iValue = SMBAT_EQUIP_EXISTENT;
						SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_ADDRESS].iValue = iAddr;
						SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_STRING_NUM_OF_TYPE].iValue = SMBAT_4812_STRING_NUM;
						iStringNum++;
					}

				}//end 31---40
				else if (iAddr >= SMBAT2402_ADDR_START && iAddr <= SMBAT2402_ADDR_END)
				{
					//2402:41 to 50  2 strings

					if (ibhave48vstrings)
					{
						AppLogOut("SMBAT Reconfig", APP_LOG_UNUSED, 
							"[%s]-[%d] ERROR: failed to At the same time, the existence of 48V and 24V,\
							Please check sm modules error \n\r", 
							__FILE__, __LINE__);	
						return -1;
					}

					for (i = 0; i < SMBAT_2402_STRING_NUM; i++)
					{
						if(iStringNum >= SMBAT_STRING_NUM)//max 20 batteries string
						{	
							AppLogOut("SMBAT Reconfig", APP_LOG_UNUSED,
								"[%s]-[%d] ERROR: failed to  iStringNum overflow,\
								Please check sm modules error \n\r",
								__FILE__, __LINE__);	
							return -1;
						}

						iStartPosition = iStringNum * SMBAT_MAX_SIGNALS_NUM;
						SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_COMMUNICATE_STAT].iValue = SMBAT_COMM_OK;
						SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_WORKING_STAT].iValue = SMBAT_EQUIP_EXISTENT;
						SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_ADDRESS].iValue = iAddr;
						SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_STRING_NUM_OF_TYPE].iValue = SMBAT_2402_STRING_NUM;
						iStringNum++;
					}			
				}
				else if (iAddr >= SMBAT2406_ADDR_START && iAddr <= SMBAT2406_ADDR_END)
				{
					if(ibhave48vstrings)
					{
						AppLogOut("SMBAT Reconfig", APP_LOG_UNUSED, 
							"[%s]-[%d] ERROR: failed to At the same time, the existence of 48V and 24V,\
							Please check smmodules error \n\r",
							__FILE__, __LINE__);	
						return -1;
					}

					for (i = 0; i < SMBAT_2406_STRING_NUM; i++)
					{
						if(iStringNum >= SMBAT_STRING_NUM)//max 20 batteries string
						{	
							AppLogOut("SMBAT Reconfig", APP_LOG_UNUSED, 
								"[%s]-[%d] ERROR: failed to  iStringNum overflow,\
								Please check sm modules error \n\r",
								__FILE__, __LINE__);	
							return -1;
						}

						iStartPosition = iStringNum * SMBAT_MAX_SIGNALS_NUM;
						SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_COMMUNICATE_STAT].iValue = SMBAT_COMM_OK;
						SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_WORKING_STAT].iValue = SMBAT_EQUIP_EXISTENT;
						SmbatDeviceClass->pRoughData[iStartPosition	+ SMBAT_ADDRESS].iValue = iAddr;
						SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_STRING_NUM_OF_TYPE].iValue = SMBAT_2406_STRING_NUM;
						iStringNum++;
					}
				}
				//2406��51 to 55 2412��56 to  60    4 strings
				else //if (iAddr >= SMBAT_2412_STRING_NUM && iAddr <= SMBAT2412_ADDR_END)
				{
					if(ibhave48vstrings)
					{
						AppLogOut("SMBAT Reconfig", APP_LOG_UNUSED, 
							"[%s]-[%d] ERROR: failed to At the same time, the existence of 48V and 24V,\
							Please check smmodules error \n\r",
							__FILE__, __LINE__);	
						return -1;
					}

					for (i = 0; i < SMBAT_2412_STRING_NUM; i++)
					{
						if(iStringNum >= SMBAT_STRING_NUM)//max 20 batteries string
						{	
							AppLogOut("SMBAT Reconfig", APP_LOG_UNUSED, 
								"[%s]-[%d] ERROR: failed to  iStringNum overflow,\
								Please check sm modules error \n\r",
								__FILE__, __LINE__);	
							return -1;
						}

						iStartPosition = iStringNum * SMBAT_MAX_SIGNALS_NUM;
						SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_COMMUNICATE_STAT].iValue = SMBAT_COMM_OK;
						SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_WORKING_STAT].iValue = SMBAT_EQUIP_EXISTENT;
						SmbatDeviceClass->pRoughData[iStartPosition	+ SMBAT_ADDRESS].iValue = iAddr;
						SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_STRING_NUM_OF_TYPE].iValue = SMBAT_2412_STRING_NUM;
						iStringNum++;
					}			
				}//end if(iAddr >= 51 && iAddr <= 60)
			}//end  if(SmbatStatus == RESPONSE_OK)
			else//if(SmbatStatus != RESPONSE_OK)
			{	
				//continue;
				//Needn't record ,continue scan
			}
		}//end for(iAddr = SMBAT_ADDR_START;iAddr <= SMBAT_ADDR_END; iAddr++)
	}

	//Record the total strings number
	SmbatDeviceClass->pRoughData[SMBAT_BATTERY_STRING_NUM].iValue = iStringNum;
	g_SmbatSampleData.iRecordAddress[iAddrIndex] = SMBAT_SAMP_INVALID_VALUE;

	//Record the group batteries strings info
	if(SmbatDeviceClass->pRoughData[SMBAT_BATTERY_STRING_NUM].iValue > 0)
	{
		SmbatDeviceClass->pRoughData[SMBAT_GROUP_WORKING_STAT].iValue = SMBAT_EQUIP_EXISTENT;
	}
	else
	{
		SmbatDeviceClass->pRoughData[SMBAT_GROUP_WORKING_STAT].iValue = SMBAT_EQUIP_NOT_EXISTENT;
		SmbatDeviceClass->pRoughData[SMBAT_BATTERY_STRING_NUM].iValue = 0;
	}

	return 0;
}

/*=============================================================================*
* FUNCTION: Fix_Data
* PURPOSE : ���Ѵ����ȫ�ֱ�������Frame[]�е����ݸ������ݽṹ��
����Ķ�Ӧ��ϵ���н���
* INPUT:	 Frame - ԭʼ��������, fData - �����������
*			 strData - ���ڽ������ݽ��������ݽṹ
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     
*		copy from smbat by bpy             
*============================================================================*/
void FixData(
	     RS485_VALUE*		fData,					// ����õ����ݻ�����
	     SMBATDATASTRUCT	strData[],				// Ҫ��������ݽṹ
	     BYTE*				Frame )					// Ҫ������ַ���
{
	CHAR	*sStop;   
	INT32	nLoop	 = 0;
	CHAR	sTemp[5] = {0};
	ASSERT(fData != NULL);

	while ( strData[nLoop].nType )						// ѭ��ֱ������������=0
	{
		switch ( strData[nLoop].nType )					// ����������������Ӧ����
		{
		case 1:											// 4�ֽ��޷�������������ʽ��
			(fData + strData[nLoop].nChanel)->fValue = 
				FixFloatDat( (char *)Frame + strData[nLoop].nOffset )/ strData[nLoop].nScaleBit;
			break;

		case 2:											// 4�ֽ��з�������������ʽ��
			strncpy( sTemp, (char*)Frame + strData[nLoop].nOffset, 4 );
			sTemp[4] = 0;
			(fData + strData[nLoop].nChanel)->fValue = (float)strtoul( sTemp, &sStop, 16 )
				/ strData[nLoop].nScaleBit;

			if((fData + strData[nLoop].nChanel)->ulValue > 0x8000 )
			{
				(fData + strData[nLoop].nChanel)->fValue 
					= 0x8000 - (fData + strData[nLoop].nChanel)->fValue;
			}
			break;

		case 3:
			// 2�ֽ�������ʽ��
			//��Ч�򲻲�Ϊ0x20, 0x20
			if( Frame[strData[nLoop].nOffset] == ' ')
			{
				(fData + strData[nLoop].nChanel)->ulValue = 0x20;
			}
			else
			{
				strncpy( sTemp, (char*)Frame+strData[nLoop].nOffset, 2 );
				sTemp[2] = 0;
				(fData + strData[nLoop].nChanel)->ulValue = strtoul( sTemp, &sStop, 16 );
			}
			break;

		default:
			break;
		}   //End Switch
		nLoop++;
	}   //End While
}

/*=============================================================================*
* FUNCTION: SMBATUnpackCmd41
* PURPOSE : arrange sampling data by 0x41 to RoughData
* INPUT: 
*			INT32					iStringNo,
*			INT32					iAddr,
*			RS485_DEVICE_CLASS*		SmbatDeviceClass, 
*			RS485_VALUE*				pReceiveData   
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMBATUnpackCmd41(INT32					iStringNo,
		       INT32					iAddr,
		       RS485_DEVICE_CLASS*		SmbatDeviceClass, 
		       RS485_VALUE*				pReceiveData)
{
	INT32					i;
	SMBAT_STRING_SIG_MAP	*pMap;
	INT32					iStartPosition;
	/**************************************4806*************************************************/
	SMBAT_STRING_SIG_MAP FirstString41Sig4806MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData 
		//right		pReceiveData
		//{SMBAT_VOLTAGE,								SMBAT_VOLTAGE},				//AI1
		//{SMBAT_CURRENT,								SMBAT_CURRENT},				//AI3
		//{SMBAT_AMBIENT_TEMPERATURE,					SMBAT_AMBIENT_TEMPERATURE},	//AI5
		//{SMBAT_ACID_TEMPERATURE,					SMBAT_ACID_TEMPERATURE},	//AI6
		//{SMBAT_BLOCK_1_VOLT,						SMBAT_BLOCK_1_VOLT},		//BAT 1
		//{SMBAT_BLOCK_2_VOLT,						SMBAT_BLOCK_2_VOLT},		//BAT 2
		//{SMBAT_BLOCK_3_VOLT,						SMBAT_BLOCK_3_VOLT},		//BAT 3
		//{SMBAT_BLOCK_4_VOLT,						SMBAT_BLOCK_4_VOLT},		//BAT 4
		//{SMBAT_BLOCK_13_VOLT,						SMBAT_BLOCK_13_VOLT},		//BAT 13
		//{SMBAT_BLOCK_14_VOLT,						SMBAT_BLOCK_14_VOLT},
		//{SMBAT_BLOCK_15_VOLT,						SMBAT_BLOCK_15_VOLT},
		//{SMBAT_BLOCK_16_VOLT,						SMBAT_BLOCK_16_VOLT},
		//{SMBAT_AI2,									SMBAT_AI2},

		{SMBAT_VOLTAGE,								SMBAT_VOLTAGE},				//AI1
		{SMBAT_CURRENT,								SMBAT_CURRENT},				//AI3
		{SMBAT_AMBIENT_TEMPERATURE,					SMBAT_AMBIENT_TEMPERATURE},	//AI5
		{SMBAT_ACID_TEMPERATURE,					SMBAT_ACID_TEMPERATURE},	//AI6
		{SMBAT_BLOCK_1_VOLT,						SMBAT_BLOCK_1_VOLT},		//BAT 1
		{SMBAT_BLOCK_2_VOLT,						SMBAT_BLOCK_2_VOLT},		//BAT 2
		{SMBAT_BLOCK_3_VOLT,						SMBAT_BLOCK_3_VOLT},		//BAT 3
		{SMBAT_BLOCK_4_VOLT,						SMBAT_BLOCK_4_VOLT},		//BAT 4
		{SMBAT_BLOCK_5_VOLT,						SMBAT_BLOCK_13_VOLT},		//BAT 13
		{SMBAT_BLOCK_6_VOLT,						SMBAT_BLOCK_14_VOLT},
		{SMBAT_BLOCK_7_VOLT,						SMBAT_BLOCK_15_VOLT},
		{SMBAT_BLOCK_8_VOLT,						SMBAT_BLOCK_16_VOLT},
		{SMBAT_AI2,									SMBAT_AI2},
		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP SecondString41Sig4806MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		//{SMBAT_VOLTAGE,								SMBAT_VOLTAGE},				//AI1
		//{SMBAT_AI4,									SMBAT_AI4},					//AI4
		//{SMBAT_AMBIENT_TEMPERATURE,					SMBAT_AMBIENT_TEMPERATURE},	//AI5
		//{SMBAT_AI7,									SMBAT_AI7},					//AI7
		//{SMBAT_BLOCK_6_VOLT,						SMBAT_BLOCK_6_VOLT},		//BAT 6
		//{SMBAT_BLOCK_7_VOLT,						SMBAT_BLOCK_7_VOLT},		//BAT 7
		//{SMBAT_BLOCK_8_VOLT,						SMBAT_BLOCK_8_VOLT},		//BAT 8
		//{SMBAT_BLOCK_9_VOLT,						SMBAT_BLOCK_9_VOLT},		//BAT 9
		//{SMBAT_BLOCK_18_VOLT,						SMBAT_BLOCK_18_VOLT},		//BAT 18
		//{SMBAT_BLOCK_19_VOLT,						SMBAT_BLOCK_19_VOLT},
		//{SMBAT_BLOCK_20_VOLT,						SMBAT_BLOCK_20_VOLT},
		//{SMBAT_BLOCK_21_VOLT,						SMBAT_BLOCK_21_VOLT},
		//{SMBAT_AI2,									SMBAT_AI2},

		{SMBAT_VOLTAGE,								SMBAT_VOLTAGE},				//AI1
		{SMBAT_CURRENT,								SMBAT_AI4},					//AI4
		{SMBAT_AMBIENT_TEMPERATURE,					SMBAT_AMBIENT_TEMPERATURE},	//AI5
		{SMBAT_ACID_TEMPERATURE,					SMBAT_AI7},					//AI7
		{SMBAT_BLOCK_1_VOLT,						SMBAT_BLOCK_6_VOLT},		//BAT 6
		{SMBAT_BLOCK_2_VOLT,						SMBAT_BLOCK_7_VOLT},		//BAT 7
		{SMBAT_BLOCK_3_VOLT,						SMBAT_BLOCK_8_VOLT},		//BAT 8
		{SMBAT_BLOCK_4_VOLT,						SMBAT_BLOCK_9_VOLT},		//BAT 9
		{SMBAT_BLOCK_5_VOLT,						SMBAT_BLOCK_18_VOLT},		//BAT 18
		{SMBAT_BLOCK_6_VOLT,						SMBAT_BLOCK_19_VOLT},
		{SMBAT_BLOCK_7_VOLT,						SMBAT_BLOCK_20_VOLT},
		{SMBAT_BLOCK_8_VOLT,						SMBAT_BLOCK_21_VOLT},
		{SMBAT_AI2,									SMBAT_AI2},

		{-1,-1}
	};

	/**************************************4812*************************************************/
	SMBAT_STRING_SIG_MAP FirstString41Sig4812MAP[] =
	{
		//left			SmbatDeviceClass->pRoughData
		//right			
		{SMBAT_BLOCK_1_VOLT,						SMBAT_BLOCK_1_VOLT},		//BAT 1
		{SMBAT_BLOCK_2_VOLT,						SMBAT_BLOCK_2_VOLT},		//BAT 2
		{SMBAT_BLOCK_3_VOLT,						SMBAT_BLOCK_13_VOLT},		//BAT 13
		{SMBAT_BLOCK_4_VOLT,						SMBAT_BLOCK_14_VOLT},
		//{SMBAT_AI2,							SMBAT_AI2},

		{SMBAT_VOLTAGE,							SMBAT_VOLTAGE},				//AI1
		{SMBAT_AI2,							SMBAT_AI2},				//AI2
		{SMBAT_CURRENT,							SMBAT_CURRENT},				//AI3
		{SMBAT_AMBIENT_TEMPERATURE,							SMBAT_AI7},				//AI7




		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP SecondString41Sig4812MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		{SMBAT_BLOCK_1_VOLT,						SMBAT_BLOCK_4_VOLT},				//BAT 4
		{SMBAT_BLOCK_2_VOLT,						SMBAT_BLOCK_5_VOLT},				//BAT 5
		{SMBAT_BLOCK_3_VOLT,						SMBAT_BLOCK_16_VOLT},
		{SMBAT_BLOCK_4_VOLT,						SMBAT_BLOCK_17_VOLT},
		//{SMBAT_AI2,							SMBAT_AI2},

		{SMBAT_VOLTAGE,							SMBAT_VOLTAGE},				//AI1
		{SMBAT_AI2,							SMBAT_AI2},				//AI2
		{SMBAT_CURRENT,							SMBAT_AI4},				//AI4
		{SMBAT_AMBIENT_TEMPERATURE,					SMBAT_AI7},				//AI7

		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP ThirdString41Sig4812MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		{SMBAT_BLOCK_1_VOLT,						SMBAT_BLOCK_7_VOLT},				//BAT 4
		{SMBAT_BLOCK_2_VOLT,						SMBAT_BLOCK_8_VOLT},				//BAT 5
		{SMBAT_BLOCK_3_VOLT,						SMBAT_BLOCK_19_VOLT},
		{SMBAT_BLOCK_4_VOLT,						SMBAT_BLOCK_20_VOLT},
		//{SMBAT_AI2,							SMBAT_AI2},

		{SMBAT_VOLTAGE,							SMBAT_VOLTAGE},				//AI1
		{SMBAT_AI2,							SMBAT_AI2},				//AI2
		{SMBAT_CURRENT,							SMBAT_AMBIENT_TEMPERATURE},		//AI5
		{SMBAT_AMBIENT_TEMPERATURE,					SMBAT_AI7},				//AI7

		{-1,-1}
	};


	SMBAT_STRING_SIG_MAP FourthString41Sig4812MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData

		{SMBAT_BLOCK_1_VOLT,						SMBAT_BLOCK_10_VOLT},				//BAT 4
		{SMBAT_BLOCK_2_VOLT,						SMBAT_BLOCK_11_VOLT},				//BAT 5
		{SMBAT_BLOCK_3_VOLT,						SMBAT_BLOCK_22_VOLT},
		{SMBAT_BLOCK_4_VOLT,						SMBAT_BLOCK_23_VOLT},


		{SMBAT_VOLTAGE,							SMBAT_VOLTAGE},				//AI1
		{SMBAT_AI2,							SMBAT_AI2},				//AI2
		{SMBAT_CURRENT,							SMBAT_ACID_TEMPERATURE},		//AI6
		{SMBAT_AMBIENT_TEMPERATURE,					SMBAT_AI7},				//AI7

		{-1,-1}
	};




	/**************************************2402*************************************************/
	SMBAT_STRING_SIG_MAP FirstString41Sig2402MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		{SMBAT_VOLTAGE,								SMBAT_VOLTAGE},				//AI1
		{SMBAT_CURRENT,								SMBAT_CURRENT},				//AI3
		{SMBAT_AMBIENT_TEMPERATURE,					SMBAT_AMBIENT_TEMPERATURE},	//AI5
		{SMBAT_ACID_TEMPERATURE,					SMBAT_ACID_TEMPERATURE},	//AI6
		{SMBAT_BLOCK_1_VOLT,						SMBAT_BLOCK_1_VOLT},		//BAT 1
		{SMBAT_BLOCK_2_VOLT,						SMBAT_BLOCK_2_VOLT},		//BAT 2
		{SMBAT_BLOCK_3_VOLT,						SMBAT_BLOCK_3_VOLT},		//BAT 
		{SMBAT_BLOCK_4_VOLT,						SMBAT_BLOCK_4_VOLT},
		{SMBAT_BLOCK_5_VOLT,						SMBAT_BLOCK_5_VOLT},
		{SMBAT_BLOCK_6_VOLT,						SMBAT_BLOCK_6_VOLT},
		{SMBAT_BLOCK_7_VOLT,						SMBAT_BLOCK_7_VOLT},
		{SMBAT_BLOCK_8_VOLT,						SMBAT_BLOCK_8_VOLT},
		{SMBAT_BLOCK_9_VOLT,						SMBAT_BLOCK_9_VOLT},
		{SMBAT_BLOCK_10_VOLT,						SMBAT_BLOCK_10_VOLT},
		{SMBAT_BLOCK_11_VOLT,						SMBAT_BLOCK_11_VOLT},
		{SMBAT_BLOCK_12_VOLT,						SMBAT_BLOCK_12_VOLT},
		{SMBAT_AI2,									SMBAT_AI2},

		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP SecondString41Sig2402MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		//{SMBAT_VOLTAGE,								SMBAT_VOLTAGE},				//AI1
		//{SMBAT_AI4,									SMBAT_AI4},					//AI4
		//{SMBAT_AMBIENT_TEMPERATURE,					SMBAT_AMBIENT_TEMPERATURE},	//AI5
		//{SMBAT_AI7,									SMBAT_AI7},					//AI7
		//{SMBAT_BLOCK_13_VOLT,						SMBAT_BLOCK_13_VOLT},		//BAT 13
		//{SMBAT_BLOCK_14_VOLT,						SMBAT_BLOCK_14_VOLT},		//BAT 14
		//{SMBAT_BLOCK_15_VOLT,						SMBAT_BLOCK_15_VOLT},		//BAT 13
		//{SMBAT_BLOCK_16_VOLT,						SMBAT_BLOCK_16_VOLT},		//BAT 13
		//{SMBAT_BLOCK_17_VOLT,						SMBAT_BLOCK_17_VOLT},		//BAT 13
		//{SMBAT_BLOCK_18_VOLT,						SMBAT_BLOCK_18_VOLT},		//BAT 13
		//{SMBAT_BLOCK_19_VOLT,						SMBAT_BLOCK_19_VOLT},		//BAT 13
		//{SMBAT_BLOCK_20_VOLT,						SMBAT_BLOCK_20_VOLT},		//BAT 13
		//{SMBAT_BLOCK_21_VOLT,						SMBAT_BLOCK_21_VOLT},		//BAT 13
		//{SMBAT_BLOCK_22_VOLT,						SMBAT_BLOCK_22_VOLT},		//BAT 13
		//{SMBAT_BLOCK_23_VOLT,						SMBAT_BLOCK_23_VOLT},		//BAT 13
		//{SMBAT_BLOCK_24_VOLT,						SMBAT_BLOCK_24_VOLT},		//BAT 13
		//{SMBAT_AI2,									SMBAT_AI2},

		{SMBAT_VOLTAGE,								SMBAT_VOLTAGE},				//AI1
		{SMBAT_CURRENT,								SMBAT_AI4},					//AI4
		{SMBAT_AMBIENT_TEMPERATURE,					SMBAT_AMBIENT_TEMPERATURE},	//AI5
		{SMBAT_ACID_TEMPERATURE,					SMBAT_AI7},					//AI7
		{SMBAT_BLOCK_1_VOLT,						SMBAT_BLOCK_13_VOLT},		//BAT 13
		{SMBAT_BLOCK_2_VOLT,						SMBAT_BLOCK_14_VOLT},		//BAT 14
		{SMBAT_BLOCK_3_VOLT,						SMBAT_BLOCK_15_VOLT},		//BAT 13
		{SMBAT_BLOCK_4_VOLT,						SMBAT_BLOCK_16_VOLT},		//BAT 13
		{SMBAT_BLOCK_5_VOLT,						SMBAT_BLOCK_17_VOLT},		//BAT 13
		{SMBAT_BLOCK_6_VOLT,						SMBAT_BLOCK_18_VOLT},		//BAT 13
		{SMBAT_BLOCK_7_VOLT,						SMBAT_BLOCK_19_VOLT},		//BAT 13
		{SMBAT_BLOCK_8_VOLT,						SMBAT_BLOCK_20_VOLT},		//BAT 13
		{SMBAT_BLOCK_9_VOLT,						SMBAT_BLOCK_21_VOLT},		//BAT 13
		{SMBAT_BLOCK_10_VOLT,						SMBAT_BLOCK_22_VOLT},		//BAT 13
		{SMBAT_BLOCK_11_VOLT,						SMBAT_BLOCK_23_VOLT},		//BAT 13
		{SMBAT_BLOCK_12_VOLT,						SMBAT_BLOCK_24_VOLT},		//BAT 13
		{SMBAT_AI2,									SMBAT_AI2},
		{-1,-1}
	};

	/**************************************2406*************************************************/
	SMBAT_STRING_SIG_MAP FirstString41Sig2406MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		//{SMBAT_VOLTAGE,								SMBAT_VOLTAGE},			//AI1
		//{SMBAT_CURRENT,								SMBAT_CURRENT},			//AI3
		//{SMBAT_AI7,									SMBAT_AI7},				//AI7
		//{SMBAT_BLOCK_1_VOLT,						SMBAT_BLOCK_1_VOLT},	//BAT 1
		//{SMBAT_BLOCK_2_VOLT,						SMBAT_BLOCK_2_VOLT},	//BAT 2
		//{SMBAT_BLOCK_13_VOLT,						SMBAT_BLOCK_13_VOLT},	//BAT 
		//{SMBAT_BLOCK_14_VOLT,						SMBAT_BLOCK_14_VOLT},
		//{SMBAT_AI2,									SMBAT_AI2},

		{SMBAT_VOLTAGE,								SMBAT_VOLTAGE},			//AI1
		{SMBAT_CURRENT,								SMBAT_CURRENT},			//AI3
		{SMBAT_AMBIENT_TEMPERATURE,					SMBAT_AI7},				//AI7
		{SMBAT_BLOCK_1_VOLT,						SMBAT_BLOCK_1_VOLT},	//BAT 1
		{SMBAT_BLOCK_2_VOLT,						SMBAT_BLOCK_2_VOLT},	//BAT 2
		{SMBAT_BLOCK_3_VOLT,						SMBAT_BLOCK_13_VOLT},	//BAT 
		{SMBAT_BLOCK_4_VOLT,						SMBAT_BLOCK_14_VOLT},
		{SMBAT_AI2,									SMBAT_AI2},
		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP SecondString41Sig2406MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		//{SMBAT_VOLTAGE,								SMBAT_VOLTAGE},			//AI1
		//{SMBAT_AI4,									SMBAT_AI4},				//AI4
		//{SMBAT_AI7,									SMBAT_AI7},				//AI7
		//{SMBAT_BLOCK_4_VOLT,						SMBAT_BLOCK_4_VOLT},	//BAT 4
		//{SMBAT_BLOCK_5_VOLT,						SMBAT_BLOCK_5_VOLT},	//BAT 5
		//{SMBAT_BLOCK_16_VOLT,						SMBAT_BLOCK_16_VOLT},	//BAT 16
		//{SMBAT_BLOCK_17_VOLT,						SMBAT_BLOCK_17_VOLT},	//BAT 17
		//{SMBAT_AI2,									SMBAT_AI2},

		{SMBAT_VOLTAGE,									SMBAT_VOLTAGE},			//AI1
		{SMBAT_CURRENT,									SMBAT_AI4},				//AI4
		{SMBAT_AMBIENT_TEMPERATURE,						SMBAT_AI7},				//AI7
		{SMBAT_BLOCK_1_VOLT,							SMBAT_BLOCK_4_VOLT},	//BAT 4
		{SMBAT_BLOCK_2_VOLT,							SMBAT_BLOCK_5_VOLT},	//BAT 5
		{SMBAT_BLOCK_3_VOLT,							SMBAT_BLOCK_16_VOLT},	//BAT 16
		{SMBAT_BLOCK_4_VOLT,							SMBAT_BLOCK_17_VOLT},	//BAT 17
		{SMBAT_AI2,										SMBAT_AI2},
		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP ThirdString41Sig2406MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		//{SMBAT_VOLTAGE,								SMBAT_VOLTAGE},				//AI1
		//{SMBAT_AMBIENT_TEMPERATURE,					SMBAT_AMBIENT_TEMPERATURE},	//AI5
		//{SMBAT_AI7,									SMBAT_AI7},					//AI7
		//{SMBAT_BLOCK_7_VOLT,						SMBAT_BLOCK_7_VOLT},		//BAT 7
		//{SMBAT_BLOCK_8_VOLT,						SMBAT_BLOCK_8_VOLT},		//BAT 8
		//{SMBAT_BLOCK_18_VOLT,						SMBAT_BLOCK_18_VOLT},		//BAT 18
		//{SMBAT_BLOCK_19_VOLT,						SMBAT_BLOCK_19_VOLT},		//BAT 19
		//{SMBAT_AI2,									SMBAT_AI2},

		{SMBAT_VOLTAGE,								SMBAT_VOLTAGE},				//AI1
		{SMBAT_CURRENT,								SMBAT_AMBIENT_TEMPERATURE},	//AI5
		{SMBAT_AMBIENT_TEMPERATURE,					SMBAT_AI7},					//AI7
		{SMBAT_BLOCK_1_VOLT,						SMBAT_BLOCK_7_VOLT},		//BAT 7
		{SMBAT_BLOCK_2_VOLT,						SMBAT_BLOCK_8_VOLT},		//BAT 8
		{SMBAT_BLOCK_3_VOLT,						SMBAT_BLOCK_18_VOLT},		//BAT 18
		{SMBAT_BLOCK_4_VOLT,						SMBAT_BLOCK_19_VOLT},		//BAT 19
		{SMBAT_AI2,									SMBAT_AI2},

		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP FourthString41Sig2406MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		//{SMBAT_VOLTAGE,								SMBAT_VOLTAGE},				//AI1
		//{SMBAT_ACID_TEMPERATURE,					SMBAT_ACID_TEMPERATURE},	//AI6
		//{SMBAT_AI7,									SMBAT_AI7},					//AI7
		//{SMBAT_BLOCK_10_VOLT,						SMBAT_BLOCK_10_VOLT},		//BAT 10
		//{SMBAT_BLOCK_11_VOLT,						SMBAT_BLOCK_11_VOLT},		//BAT 11
		//{SMBAT_BLOCK_21_VOLT,						SMBAT_BLOCK_21_VOLT},		//BAT 21
		//{SMBAT_BLOCK_22_VOLT,						SMBAT_BLOCK_22_VOLT},		//BAT 22
		//{SMBAT_AI2,									SMBAT_AI2},

		{SMBAT_VOLTAGE,							SMBAT_VOLTAGE},				//AI1
		{SMBAT_CURRENT,							SMBAT_ACID_TEMPERATURE},	//AI6
		{SMBAT_AMBIENT_TEMPERATURE,				SMBAT_AI7},					//AI7
		{SMBAT_BLOCK_1_VOLT,					SMBAT_BLOCK_10_VOLT},		//BAT 10
		{SMBAT_BLOCK_2_VOLT,					SMBAT_BLOCK_11_VOLT},		//BAT 11
		{SMBAT_BLOCK_3_VOLT,					SMBAT_BLOCK_21_VOLT},		//BAT 21
		{SMBAT_BLOCK_4_VOLT,					SMBAT_BLOCK_22_VOLT},		//BAT 22
		{SMBAT_AI2,								SMBAT_AI2},
		{-1,-1}
	};

	/**************************************2412*************************************************/
	SMBAT_STRING_SIG_MAP FirstString41Sig2412MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		//{SMBAT_VOLTAGE,								SMBAT_VOLTAGE},			//AI1
		//{SMBAT_CURRENT,								SMBAT_CURRENT},			//AI3
		//{SMBAT_AI7,									SMBAT_AI7},				//AI7
		//{SMBAT_BLOCK_1_VOLT,						SMBAT_BLOCK_1_VOLT},	//BAT 1
		//{SMBAT_BLOCK_13_VOLT,						SMBAT_BLOCK_13_VOLT},	//BAT 13
		//{SMBAT_AI2,									SMBAT_AI2},

		{SMBAT_VOLTAGE,								SMBAT_VOLTAGE},			//AI1
		{SMBAT_CURRENT,								SMBAT_CURRENT},			//AI3
		{SMBAT_AMBIENT_TEMPERATURE,					SMBAT_AI7},				//AI7
		{SMBAT_BLOCK_1_VOLT,						SMBAT_BLOCK_1_VOLT},	//BAT 1
		{SMBAT_BLOCK_2_VOLT,						SMBAT_BLOCK_13_VOLT},	//BAT 13
		{SMBAT_AI2,									SMBAT_AI2},
		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP SecondString41Sig2412MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		////right		pReceiveData
		//{SMBAT_VOLTAGE,								SMBAT_VOLTAGE},			//AI1
		//{SMBAT_AI4,									SMBAT_AI4},				//AI4
		//{SMBAT_AI7,									SMBAT_AI7},				//AI7
		//{SMBAT_BLOCK_3_VOLT,						SMBAT_BLOCK_3_VOLT},	//BAT 3
		//{SMBAT_BLOCK_15_VOLT,						SMBAT_BLOCK_15_VOLT},	//BAT 15
		//{SMBAT_AI2,									SMBAT_AI2},

		{SMBAT_VOLTAGE,									SMBAT_VOLTAGE},			//AI1
		{SMBAT_CURRENT,									SMBAT_AI4},				//AI4
		{SMBAT_AMBIENT_TEMPERATURE,						SMBAT_AI7},				//AI7
		{SMBAT_BLOCK_1_VOLT,							SMBAT_BLOCK_3_VOLT},	//BAT 3
		{SMBAT_BLOCK_2_VOLT,							SMBAT_BLOCK_15_VOLT},	//BAT 15
		{SMBAT_AI2,										SMBAT_AI2},

		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP ThirdString41Sig2412MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		//{SMBAT_VOLTAGE,								SMBAT_VOLTAGE},				//AI1
		//{SMBAT_AMBIENT_TEMPERATURE,					SMBAT_AMBIENT_TEMPERATURE},	//AI5
		//{SMBAT_AI7,									SMBAT_AI7},					//AI7
		//{SMBAT_BLOCK_5_VOLT,						SMBAT_BLOCK_5_VOLT},		//BAT 7
		//{SMBAT_BLOCK_17_VOLT,						SMBAT_BLOCK_17_VOLT},		//BAT 8
		//{SMBAT_AI2,									SMBAT_AI2},

		{SMBAT_VOLTAGE,								SMBAT_VOLTAGE},				//AI1
		{SMBAT_CURRENT,								SMBAT_AMBIENT_TEMPERATURE},	//AI5
		{SMBAT_AMBIENT_TEMPERATURE,					SMBAT_AI7},					//AI7
		{SMBAT_BLOCK_1_VOLT,						SMBAT_BLOCK_5_VOLT},		//BAT 7
		{SMBAT_BLOCK_2_VOLT,						SMBAT_BLOCK_17_VOLT},		//BAT 8
		{SMBAT_AI2,									SMBAT_AI2},

		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP FourthString41Sig2412MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		//{SMBAT_VOLTAGE,						SMBAT_VOLTAGE},			//AI1
		//{SMBAT_ACID_TEMPERATURE,					SMBAT_ACID_TEMPERATURE},	//AI6
		//{SMBAT_AI7,							SMBAT_AI7},			//AI7
		//{SMBAT_BLOCK_7_VOLT,						SMBAT_BLOCK_7_VOLT},		//BAT 10
		//{SMBAT_BLOCK_19_VOLT,						SMBAT_BLOCK_19_VOLT},		//BAT 11
		//{SMBAT_AI2,									SMBAT_AI2},

		{SMBAT_VOLTAGE,							SMBAT_VOLTAGE},			//AI1
		{SMBAT_CURRENT,							SMBAT_ACID_TEMPERATURE},	//AI6
		{SMBAT_AMBIENT_TEMPERATURE,					SMBAT_AI7},			//AI7
		{SMBAT_BLOCK_1_VOLT,						SMBAT_BLOCK_7_VOLT},		//BAT 10
		{SMBAT_BLOCK_2_VOLT,						SMBAT_BLOCK_19_VOLT},		//BAT 11
		{SMBAT_AI2,							SMBAT_AI2},

		{-1,-1}
	};

	//4802  directness copy 
	if((iAddr >= SMBAT4802_ADDR_START) && (iAddr <= SMBAT4802_ADDR_END))
	{
		iStartPosition = iStringNo * SMBAT_MAX_SIGNALS_NUM;
		for(i = 0;i <= SMBAT_AI7;i++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + i].fValue  = (pReceiveData + i)->fValue;
		}
	}
	else if((iAddr >= SMBAT4806_ADDR_START) && (iAddr <= SMBAT4806_ADDR_END))//4806	2  batteries strings
	{
		pMap = FirstString41Sig4806MAP;
		iStartPosition = iStringNo * SMBAT_MAX_SIGNALS_NUM;
		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].fValue
				= (pReceiveData + pMap->SubSigMap)->fValue;
		}

		pMap = SecondString41Sig4806MAP;
		iStartPosition = (iStringNo + 1) * SMBAT_MAX_SIGNALS_NUM;
		for(; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].fValue
				= (pReceiveData + pMap->SubSigMap)->fValue;
		}
	}
	else if((iAddr >= SMBAT4812_ADDR_START) && (iAddr <= SMBAT4812_ADDR_END))//4812	2  batteries strings
	{
		pMap = FirstString41Sig4812MAP;
		iStartPosition = iStringNo * SMBAT_MAX_SIGNALS_NUM;
		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].fValue
				= (pReceiveData + pMap->SubSigMap)->fValue;
		}	

		pMap = SecondString41Sig4812MAP;
		iStartPosition = (iStringNo + 1) * SMBAT_MAX_SIGNALS_NUM;
		for(; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].fValue
				= (pReceiveData + pMap->SubSigMap)->fValue;
		}


		pMap = ThirdString41Sig4812MAP;
		iStartPosition = (iStringNo + 2) * SMBAT_MAX_SIGNALS_NUM;
		for(; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].fValue
				= (pReceiveData + pMap->SubSigMap)->fValue;
		}

		pMap = FourthString41Sig4812MAP;
		iStartPosition = (iStringNo + 3) * SMBAT_MAX_SIGNALS_NUM;
		for(; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].fValue
				= (pReceiveData + pMap->SubSigMap)->fValue;
		}

	}
	else if((iAddr >= SMBAT2402_ADDR_START) && (iAddr <= SMBAT2402_ADDR_END))//2402	2  batteries strings
	{
		pMap = FirstString41Sig2402MAP;
		iStartPosition = iStringNo * SMBAT_MAX_SIGNALS_NUM;
		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].fValue
				= (pReceiveData + pMap->SubSigMap)->fValue;
		}	

		pMap = SecondString41Sig2402MAP;
		iStartPosition = (iStringNo + 1) * SMBAT_MAX_SIGNALS_NUM;
		for(; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].fValue
				= (pReceiveData + pMap->SubSigMap)->fValue;
		}		
	}
	else if((iAddr >= SMBAT2406_ADDR_START) && (iAddr <= SMBAT2406_ADDR_END))//2406 4 batteries strings
	{
		pMap = FirstString41Sig2406MAP;
		iStartPosition = iStringNo * SMBAT_MAX_SIGNALS_NUM;
		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].fValue
				= (pReceiveData + pMap->SubSigMap)->fValue;
		}	

		pMap = SecondString41Sig2406MAP;
		iStartPosition = (iStringNo + 1) * SMBAT_MAX_SIGNALS_NUM;
		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].fValue
				= (pReceiveData + pMap->SubSigMap)->fValue;
		}		

		pMap = ThirdString41Sig2406MAP;
		iStartPosition = (iStringNo + 2) * SMBAT_MAX_SIGNALS_NUM;
		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].fValue
				= (pReceiveData + pMap->SubSigMap)->fValue;
		}	

		pMap = FourthString41Sig2406MAP;
		iStartPosition = (iStringNo + 3) * SMBAT_MAX_SIGNALS_NUM;
		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].fValue
				= (pReceiveData + pMap->SubSigMap)->fValue;
		}	
	}
	else //if(iAddr >= SMBAT2412_ADDR_START && iAddr <= SMBAT2412_ADDR_END)//2412   4   batteries strings
	{
		pMap = FirstString41Sig2412MAP;
		iStartPosition = iStringNo * SMBAT_MAX_SIGNALS_NUM;
		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].fValue
				= (pReceiveData + pMap->SubSigMap)->fValue;
		}	

		pMap = SecondString41Sig2412MAP;
		iStartPosition = (iStringNo + 1) * SMBAT_MAX_SIGNALS_NUM;
		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].fValue
				= (pReceiveData + pMap->SubSigMap)->fValue;
		}		

		pMap = ThirdString41Sig2412MAP;
		iStartPosition = (iStringNo + 2) * SMBAT_MAX_SIGNALS_NUM;
		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].fValue
				= (pReceiveData + pMap->SubSigMap)->fValue;
		}	

		pMap = FourthString41Sig2412MAP;
		iStartPosition = (iStringNo + 3) * SMBAT_MAX_SIGNALS_NUM;
		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].fValue
				= (pReceiveData + pMap->SubSigMap)->fValue;
		}		
	}

	return 0;
}

/*=============================================================================*
* FUNCTION: SMBATUnpackCmd43
* PURPOSE : arrange sampling data by 0x43 to RoughData
* INPUT: 
*    
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMBATUnpackCmd43(INT32				iStringNo,
		       INT32				iAddr,
		       RS485_DEVICE_CLASS*  SmbatDeviceClass,
		       RS485_VALUE*			pReceiveData)
{
	INT32					i;
	SMBAT_STRING_SIG_MAP	*pMap;
	INT32					iStartPosition;
	/*
	�������һ�£�
	���������������ļ�������������⣬Э����ĳ���źŵ���ʲô�ź����ˣ�
	�����±߽ṹ���е��ұߵļ�ΪЭ�����źŵ�λ�á��Ѿ�����һ���� ROUGH_DATA_IDX_SMBAT�������Э���е�һһ��Ӧ��
	��Ҫ�������ΪROUGHDATA��������
	���ɼ������ź�pReceiveData �и����źžͿ��������������
	���Կ���������⣬�������˼�ǽ�pReceiveData��Э���е��ź���������NGC��Ϊ���źš�
	����		{SMBAT_BATTERY_DISCONNECTED,							SMBAT_DI7},				//DI7
	��˼�ǽ� Э���е�DI7�ź���������NGC��Ϊ�� ����µ��źš�
	���Խ�DI7�ź���䵽ROUGHDATA�е� SMBAT_BATTERY_DISCONNECTEDλ�ã�����������ôһ�����顣
	*/
	/**************************************4806*************************************************/
	SMBAT_STRING_SIG_MAP FirstString43Sig4806MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		//{SMBAT_BATTERY_LEAKAGE,								SMBAT_BATTERY_LEAKAGE},		//DI1
		//{SMBAT_LOW_ACID_LEVEL,								SMBAT_LOW_ACID_LEVEL},		//DI2
		//{SMBAT_BATTERY_DISCONNECTED,						SMBAT_BATTERY_DISCONNECTED},//DI3
		//{SMBAT_DI4,											SMBAT_DI4},					//DI4
		//{SMBAT_DI8,											SMBAT_DI8},					//DI8
		//{SMBAT_RELAY_1_STAT,								SMBAT_RELAY_1_STAT},		//Relay 1 status
		//{SMBAT_RELAY_2_STAT,								SMBAT_RELAY_2_STAT},		//Relay 2 status

		{SMBAT_BATTERY_LEAKAGE,								SMBAT_BATTERY_LEAKAGE},		//DI1
		{SMBAT_LOW_ACID_LEVEL,								SMBAT_LOW_ACID_LEVEL},		//DI2
		{SMBAT_BATTERY_DISCONNECTED,							SMBAT_BATTERY_DISCONNECTED},//DI3
		{SMBAT_DI4,									SMBAT_DI4},					//DI4
		{SMBAT_DI8,									SMBAT_DI8},					//DI8
		{SMBAT_RELAY_1_STAT,								SMBAT_RELAY_1_STAT},		//Relay 1 status
		{SMBAT_RELAY_2_STAT,								SMBAT_RELAY_2_STAT},		//Relay 2 status

		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP SecondString43Sig4806MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		//{SMBAT_DI5,											SMBAT_DI5},				//DI5
		//{SMBAT_DI6,											SMBAT_DI6},				//DI6
		//{SMBAT_DI7,											SMBAT_DI7},				//DI7
		//{SMBAT_DI4,											SMBAT_DI4},				//DI4
		//{SMBAT_DI8,											SMBAT_DI8},				//DI8
		//{SMBAT_RELAY_1_STAT,								SMBAT_RELAY_1_STAT},	//Relay 1 status
		//{SMBAT_RELAY_2_STAT,								SMBAT_RELAY_2_STAT},	//Relay 2 status

		{SMBAT_BATTERY_LEAKAGE,									SMBAT_DI5},				//DI5
		{SMBAT_LOW_ACID_LEVEL,									SMBAT_DI6},				//DI6
		{SMBAT_BATTERY_DISCONNECTED,							SMBAT_DI7},				//DI7
		{SMBAT_DI4,												SMBAT_DI4},				//DI4
		{SMBAT_DI8,												SMBAT_DI8},				//DI8
		{SMBAT_RELAY_1_STAT,									SMBAT_RELAY_1_STAT},	//Relay 1 status
		{SMBAT_RELAY_2_STAT,									SMBAT_RELAY_2_STAT},	//Relay 2 status
		{-1,-1}
	};

	/**************************************4812*************************************************/
	SMBAT_STRING_SIG_MAP FirstString43Sig4812MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		{SMBAT_BATTERY_LEAKAGE,							SMBAT_BATTERY_LEAKAGE},		//DI1  cmd43
		{SMBAT_LOW_ACID_LEVEL,							SMBAT_LOW_ACID_LEVEL},		//DI2   cmd43

		{SMBAT_RELAY_1_STAT,							SMBAT_RELAY_1_STAT},		//Relay 1 status
		{SMBAT_RELAY_2_STAT,							SMBAT_RELAY_2_STAT},		//Relay 2 status
		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP SecondString43Sig4812MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		{SMBAT_BATTERY_LEAKAGE,						SMBAT_BATTERY_DISCONNECTED},		//DI3  cmd43
		{SMBAT_LOW_ACID_LEVEL,								SMBAT_DI4},		//DI4  cmd43
		{SMBAT_RELAY_1_STAT,							SMBAT_RELAY_1_STAT},		//Relay 1 status
		{SMBAT_RELAY_2_STAT,							SMBAT_RELAY_2_STAT},		//Relay 2 status

		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP ThirdString43Sig4812MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		{SMBAT_BATTERY_LEAKAGE,							SMBAT_DI5},			//DI5  cmd43
		{SMBAT_LOW_ACID_LEVEL,							SMBAT_DI6},			//DI6  cmd43
		{SMBAT_RELAY_1_STAT,							SMBAT_RELAY_1_STAT},		//Relay 1 status
		{SMBAT_RELAY_2_STAT,							SMBAT_RELAY_2_STAT},		//Relay 2 status

		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP FourthString43Sig4812MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		{SMBAT_BATTERY_LEAKAGE,							SMBAT_DI7},			//DI7  cmd43
		{SMBAT_LOW_ACID_LEVEL,							SMBAT_DI8},			//DI8  cmd43
		{SMBAT_RELAY_1_STAT,							SMBAT_RELAY_1_STAT},		//Relay 1 status
		{SMBAT_RELAY_2_STAT,							SMBAT_RELAY_2_STAT},		//Relay 2 status

		{-1,-1}
	};









	/**************************************2402*************************************************/
	SMBAT_STRING_SIG_MAP FirstString43Sig2402MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		{SMBAT_BATTERY_LEAKAGE,								SMBAT_BATTERY_LEAKAGE},		//DI1
		{SMBAT_LOW_ACID_LEVEL,								SMBAT_LOW_ACID_LEVEL},		//DI2
		{SMBAT_BATTERY_DISCONNECTED,						SMBAT_BATTERY_DISCONNECTED},//DI3
		{SMBAT_DI4,											SMBAT_DI4},					//DI7
		{SMBAT_DI8,											SMBAT_DI8},					//DI8
		{SMBAT_RELAY_1_STAT,								SMBAT_RELAY_1_STAT},		//Relay 1 status
		{SMBAT_RELAY_2_STAT,								SMBAT_RELAY_2_STAT},		//Relay 2 status

		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP SecondString43Sig2402MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		//{SMBAT_DI5,											SMBAT_DI5},					//DI5
		//{SMBAT_DI6,											SMBAT_DI6},					//DI6
		//{SMBAT_DI7,											SMBAT_DI7},					//DI7
		//{SMBAT_DI8,											SMBAT_DI8},					//DI8
		//{SMBAT_RELAY_1_STAT,								SMBAT_RELAY_1_STAT},		//Relay 1 status
		//{SMBAT_RELAY_2_STAT,								SMBAT_RELAY_2_STAT},		//Relay 2 status
		{SMBAT_BATTERY_LEAKAGE,									SMBAT_DI5},					//DI5
		{SMBAT_LOW_ACID_LEVEL,									SMBAT_DI6},					//DI6
		{SMBAT_BATTERY_DISCONNECTED,							SMBAT_DI7},					//DI7
		{SMBAT_DI4,												SMBAT_DI4},					//DI8
		{SMBAT_DI8,												SMBAT_DI8},		//Relay 1 status
		{SMBAT_RELAY_1_STAT,									SMBAT_RELAY_1_STAT},		//Relay 2 status
		{SMBAT_RELAY_2_STAT,									SMBAT_RELAY_2_STAT},
		{-1,-1}
	};

	/**************************************2406*************************************************/
	SMBAT_STRING_SIG_MAP FirstString43Sig2406MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		{SMBAT_BATTERY_LEAKAGE,								SMBAT_BATTERY_LEAKAGE},		//DI1
		{SMBAT_LOW_ACID_LEVEL,								SMBAT_LOW_ACID_LEVEL},		//DI2
		{SMBAT_RELAY_1_STAT,								SMBAT_RELAY_1_STAT},		//Relay 1 status
		{SMBAT_RELAY_2_STAT,								SMBAT_RELAY_2_STAT},		//Relay 2 status
		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP SecondString43Sig2406MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		//{SMBAT_BATTERY_DISCONNECTED,						SMBAT_BATTERY_DISCONNECTED},	//DI3
		//{SMBAT_DI4,											SMBAT_DI4},						//DI4
		//{SMBAT_RELAY_1_STAT,								SMBAT_RELAY_1_STAT},			//Relay 1 status
		//{SMBAT_RELAY_2_STAT,								SMBAT_RELAY_2_STAT},			//Relay 2 status
		{SMBAT_BATTERY_LEAKAGE,							SMBAT_BATTERY_DISCONNECTED},	//DI3
		{SMBAT_LOW_ACID_LEVEL,							SMBAT_DI4},						//DI4
		{SMBAT_RELAY_1_STAT,							SMBAT_RELAY_1_STAT},			//Relay 1 status
		{SMBAT_RELAY_2_STAT,							SMBAT_RELAY_2_STAT},			//Relay 2 status
		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP ThirdString43Sig2406MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		//{SMBAT_DI5,											SMBAT_DI5},					//DI5
		//{SMBAT_DI6,											SMBAT_DI6},					//DI6
		//{SMBAT_RELAY_1_STAT,								SMBAT_RELAY_1_STAT},		//Relay 1 status
		//{SMBAT_RELAY_2_STAT,								SMBAT_RELAY_2_STAT},		//Relay 2 status
		{SMBAT_BATTERY_LEAKAGE,								SMBAT_DI5},					//DI5
		{SMBAT_LOW_ACID_LEVEL,								SMBAT_DI6},					//DI6
		{SMBAT_RELAY_1_STAT,								SMBAT_RELAY_1_STAT},		//Relay 1 status
		{SMBAT_RELAY_2_STAT,								SMBAT_RELAY_2_STAT},		//Relay 2 status
		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP FourthString43Sig2406MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		//{SMBAT_DI7,											SMBAT_DI7},					//AI1
		//{SMBAT_DI8,											SMBAT_DI8},					//AI6
		//{SMBAT_RELAY_1_STAT,								SMBAT_RELAY_1_STAT},		//Relay 1 status
		//{SMBAT_RELAY_2_STAT,								SMBAT_RELAY_2_STAT},		//Relay 2 status
		{SMBAT_BATTERY_LEAKAGE,									SMBAT_DI7},					//AI1
		{SMBAT_LOW_ACID_LEVEL,									SMBAT_DI8},					//AI6
		{SMBAT_RELAY_1_STAT,									SMBAT_RELAY_1_STAT},		//Relay 1 status
		{SMBAT_RELAY_2_STAT,									SMBAT_RELAY_2_STAT},		//Relay 2 status
		{-1,-1}
	};

	/**************************************2412*************************************************/
	SMBAT_STRING_SIG_MAP FirstString43Sig2412MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		{SMBAT_BATTERY_LEAKAGE,								SMBAT_BATTERY_LEAKAGE},		//DI1
		{SMBAT_LOW_ACID_LEVEL,								SMBAT_LOW_ACID_LEVEL},		//DI2
		{SMBAT_RELAY_1_STAT,								SMBAT_RELAY_1_STAT},		//Relay 1 status
		{SMBAT_RELAY_2_STAT,								SMBAT_RELAY_2_STAT},		//Relay 2 status
		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP SecondString43Sig2412MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		//{SMBAT_BATTERY_DISCONNECTED,						SMBAT_BATTERY_DISCONNECTED},//DI3
		//{SMBAT_DI4,											SMBAT_DI4},					//DI4
		//{SMBAT_RELAY_1_STAT,								SMBAT_RELAY_1_STAT},		//Relay 1 status
		//{SMBAT_RELAY_2_STAT,								SMBAT_RELAY_2_STAT},		//Relay 2 status
		{SMBAT_BATTERY_LEAKAGE,								SMBAT_BATTERY_DISCONNECTED},//DI3
		{SMBAT_LOW_ACID_LEVEL,								SMBAT_DI4},					//DI4
		{SMBAT_RELAY_1_STAT,								SMBAT_RELAY_1_STAT},		//Relay 1 status
		{SMBAT_RELAY_2_STAT,								SMBAT_RELAY_2_STAT},		//Relay 2 status
		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP ThirdString43Sig2412MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		//{SMBAT_DI5,											SMBAT_DI5},			//DI5
		//{SMBAT_DI6,											SMBAT_DI6},			//DI6
		//{SMBAT_RELAY_1_STAT,								SMBAT_RELAY_1_STAT},//Relay 1 status
		//{SMBAT_RELAY_2_STAT,								SMBAT_RELAY_2_STAT},//Relay 2 status
		{SMBAT_BATTERY_LEAKAGE,										SMBAT_DI5},			//DI5
		{SMBAT_LOW_ACID_LEVEL,										SMBAT_DI6},			//DI6
		{SMBAT_RELAY_1_STAT,										SMBAT_RELAY_1_STAT},//Relay 1 status
		{SMBAT_RELAY_2_STAT,										SMBAT_RELAY_2_STAT},//Relay 2 status
		{-1,-1}
	};

	SMBAT_STRING_SIG_MAP FourthString43Sig2412MAP[] =
	{
		//left		SmbatDeviceClass->pRoughData
		//right		pReceiveData
		//{SMBAT_DI7,											SMBAT_DI7},
		//{SMBAT_DI8,											SMBAT_DI8},
		//{SMBAT_RELAY_1_STAT,								SMBAT_RELAY_1_STAT},//Relay 1 status
		//{SMBAT_RELAY_2_STAT,								SMBAT_RELAY_2_STAT},//Relay 2 status
		{SMBAT_BATTERY_LEAKAGE,									SMBAT_DI7},
		{SMBAT_LOW_ACID_LEVEL,									SMBAT_DI8},
		{SMBAT_RELAY_1_STAT,									SMBAT_RELAY_1_STAT},//Relay 1 status
		{SMBAT_RELAY_2_STAT,									SMBAT_RELAY_2_STAT},//Relay 2 status
		{-1,-1}
	};

	if(iAddr >= SMBAT4802_ADDR_START && iAddr <= SMBAT4802_ADDR_END)//4802  copy
	{
		iStartPosition = iStringNo * SMBAT_MAX_SIGNALS_NUM;

		for(i = SMBAT_BATTERY_LEAKAGE;i <= SMBAT_RELAY_2_STAT;i++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + i].ulValue 
				= (pReceiveData + i)->ulValue;
		}
	}
	else if(iAddr >= SMBAT4806_ADDR_START && iAddr <= SMBAT4806_ADDR_END)//4806 2 batteries strings 
	{
		pMap = FirstString43Sig4806MAP;
		iStartPosition = iStringNo * SMBAT_MAX_SIGNALS_NUM;

		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].ulValue
				= (pReceiveData + pMap->SubSigMap)->ulValue;
		}

		pMap = SecondString43Sig4806MAP;
		iStartPosition = (iStringNo + 1) * SMBAT_MAX_SIGNALS_NUM;

		for(; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].ulValue
				= (pReceiveData + pMap->SubSigMap)->ulValue;
		}
	}
	else if(iAddr >= SMBAT4812_ADDR_START && iAddr <= SMBAT4812_ADDR_END)//4812	2 batteries strings 
	{
		pMap = FirstString43Sig4812MAP;
		iStartPosition = iStringNo * SMBAT_MAX_SIGNALS_NUM;
		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].ulValue 
				= (pReceiveData + pMap->SubSigMap)->ulValue;
		}	

		pMap = SecondString43Sig4812MAP;
		iStartPosition = (iStringNo + 1) * SMBAT_MAX_SIGNALS_NUM;
		for(; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].ulValue
				= (pReceiveData + pMap->SubSigMap)->ulValue;
		}

		pMap = ThirdString43Sig4812MAP;
		iStartPosition = (iStringNo + 2) * SMBAT_MAX_SIGNALS_NUM;
		for(; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].ulValue
				= (pReceiveData + pMap->SubSigMap)->ulValue;
		}

		pMap = FourthString43Sig4812MAP;
		iStartPosition = (iStringNo + 3) * SMBAT_MAX_SIGNALS_NUM;
		for(; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].ulValue
				= (pReceiveData + pMap->SubSigMap)->ulValue;
		}

	}
	else if(iAddr >= SMBAT2402_ADDR_START && iAddr <= SMBAT2402_ADDR_END)//2402	2 batteries strings 
	{
		pMap = FirstString43Sig2402MAP;
		iStartPosition = iStringNo * SMBAT_MAX_SIGNALS_NUM;

		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].ulValue 
				= (pReceiveData + pMap->SubSigMap)->ulValue;
		}	

		pMap = SecondString43Sig2402MAP;
		iStartPosition = (iStringNo + 1) * SMBAT_MAX_SIGNALS_NUM;

		for(; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].ulValue
				= (pReceiveData + pMap->SubSigMap)->ulValue;
		}		
	}
	else if(iAddr >= SMBAT2406_ADDR_START && iAddr <= SMBAT2406_ADDR_END)//2406   4 batteries strings 
	{
		pMap = FirstString43Sig2406MAP;
		iStartPosition = iStringNo * SMBAT_MAX_SIGNALS_NUM;

		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].ulValue
				= (pReceiveData + pMap->SubSigMap)->ulValue;
		}	

		pMap = SecondString43Sig2406MAP;
		iStartPosition = (iStringNo + 1) * SMBAT_MAX_SIGNALS_NUM;

		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].ulValue
				= (pReceiveData + pMap->SubSigMap)->ulValue;
		}		

		pMap = ThirdString43Sig2406MAP;
		iStartPosition = (iStringNo + 2) * SMBAT_MAX_SIGNALS_NUM;

		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].ulValue
				= (pReceiveData + pMap->SubSigMap)->ulValue;
		}	

		pMap = FourthString43Sig2406MAP;
		iStartPosition = (iStringNo + 3) * SMBAT_MAX_SIGNALS_NUM;

		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].ulValue
				= (pReceiveData + pMap->SubSigMap)->ulValue;
		}	
	}
	else //if(iAddr >= SMBAT2412_ADDR_START && iAddr <= SMBAT2412_ADDR_END)//2412  4 batteries strings 
	{
		pMap = FirstString43Sig2412MAP;
		iStartPosition = iStringNo * SMBAT_MAX_SIGNALS_NUM;

		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].ulValue 
				= (pReceiveData + pMap->SubSigMap)->ulValue;
		}	


		pMap = SecondString43Sig2412MAP;
		iStartPosition = (iStringNo + 1) * SMBAT_MAX_SIGNALS_NUM;

		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].ulValue
				= (pReceiveData + pMap->SubSigMap)->ulValue;
		}		

		pMap = ThirdString43Sig2412MAP;
		iStartPosition = (iStringNo + 2) * SMBAT_MAX_SIGNALS_NUM;

		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].ulValue 
				= (pReceiveData + pMap->SubSigMap)->ulValue;
		}	

		pMap = FourthString43Sig2412MAP;
		iStartPosition = (iStringNo + 3) * SMBAT_MAX_SIGNALS_NUM;

		for (; pMap->Total_4802SigMap >= 0; pMap++)
		{
			SmbatDeviceClass->pRoughData[iStartPosition + pMap->Total_4802SigMap].ulValue
				= (pReceiveData + pMap->SubSigMap)->ulValue;
		}		
	}

	return TRUE;
}

/*=============================================================================*
* FUNCTION: SMBATSampleCmd41
* PURPOSE : samping smbat signal info by 0x41 
* INPUT:   RS485_DEVICE_CLASS*  SmbatDeviceClass
*			INT32 iStringNo,
*			INT32 iAddr,
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMBATSampleCmd41(INT32 iStringNo, INT32 iAddr, RS485_DEVICE_CLASS*  SmbatDeviceClass)
{
	INT32			i;
	INT32			iSmbatStat;
	INT32			nUnitNo;
	INT32			PointStartPosition;
	INT32			iCommStat;
	BYTE			szSendStr[21]						= {0};
	BYTE			abyRcvBuf[MAX_RECVBUFFER_NUM_SMBAT] = {0};    
	nUnitNo												= iAddr;
	iSmbatStat											= SMBAT_SAMP_INVALID_VALUE;

	sprintf((char *)szSendStr, 
		"~%s%02X%02X%02X%04X\0", 
		c_SMBATszVer,
		(unsigned int)nUnitNo, 
		SMBATCID1, 
		SMBATCID2_GET_ANOLOG,
		0);

	SMBAT_CheckSum( szSendStr );
	//1	  1	  1	  1	   1        2	   X	2   1
	//1	  2	  2	  2	   2	    4	   2X	4   1
	//SOI VER ADR CID1 CID2/RTN	LENGTH INFO	SUM	EOI
	*(szSendStr + 17)	=   byEOI;
	PointStartPosition  =	iStringNo * SMBAT_MAX_SIGNALS_NUM;

	for (i = 0; i < RS485_RECONNECT_TIMES;i++)
	{
		iSmbatStat = SMBATSendAndRead(SmbatDeviceClass->pCommPort->hCommPort, 
			szSendStr,
			abyRcvBuf, 
			18,
			RS485_COMM_NORMAL,
			1024);

		iCommStat 
			= SmbatDeviceClass->pRoughData[PointStartPosition + SMBAT_COMMUNICATE_STAT].iValue;

		if ((RESPONSE_OK == iSmbatStat) || (iCommStat == SMBAT_COMM_FAIL))
		{
			break;	
		}
	}

	if(iSmbatStat == RESPONSE_OK)
	{
		FixData(g_SmbatSampleData.OneStringSigs + ONE_STRINGSIGS_OFFSET41,
			s_Str41SigInfo, 
			abyRcvBuf + DATAINFO_YDN23_SMBAT + 2
			);
		SMBATUnpackCmd41(iStringNo, nUnitNo, SmbatDeviceClass, g_SmbatSampleData.OneStringSigs);
		return TRUE;
	}
	else	//if (RESPONSE_OK != iSmbatStat)
	{
		//TRACE("\n  ***SMBAT  CID2  41 ERROR i = %d \n",i);
		return FALSE;
	}

}

/*=============================================================================*
* FUNCTION: SMBATSampleCmd43
* PURPOSE : samping smbat signal info by 0x43 
* INPUT:	 INT32 iStringNo,	
*			 INT32 iAddr
*			 RS485_DEVICE_CLASS*  SmbatDeviceClass
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMBATSampleCmd43(INT32 iStringNo,INT32 iAddr,RS485_DEVICE_CLASS*  SmbatDeviceClass)
{
	INT32   i;
	INT32	iSmbatStat;
	INT32   nUnitNo;
	INT32	PointStartPosition;
	nUnitNo										= iAddr;	
	BYTE	szSendStr[21]						= {0};
	CHAR	abyRcvBuf[MAX_RECVBUFFER_NUM_SMBAT] = {0};					 
	iSmbatStat									= SMBAT_SAMP_INVALID_VALUE;

	sprintf((char *)szSendStr,
		"~%s%02X%02X%02X%04X\0", 
		c_SMBATszVer, 
		(unsigned int)nUnitNo, 
		SMBATCID1, 
		SMBATCID2_GET_SWITCH,
		0
		);

	SMBAT_CheckSum( szSendStr );
	//1	  1	  1	  1	   1        2	   X	2   1
	//1	  2	  2	  2	   2	    4	   2X	4   1
	//SOI VER ADR CID1 CID2/RTN	LENGTH INFO	SUM	EOI
	*(szSendStr + 17) =  byEOI;
	PointStartPosition = iStringNo * SMBAT_MAX_SIGNALS_NUM;

	if (SmbatDeviceClass->pRoughData[PointStartPosition + SMBAT_COMMUNICATE_STAT].iValue
		== SMBAT_COMM_FAIL)
	{
		for (i = 0; i < 1; i++)
		{
			//2011/02/16����˵������ͨ���жϣ�ʵ��û�취�ٽ����ݴ�
			//iSmbatStat = SMBATSendAndRead(SmbatDeviceClass->pCommPort->hCommPort, 
			//							szSendStr,
			//							abyRcvBuf, 
			//							18,
			//							RS485_COMM_ABNORMALITY,//block 200ms
			//							0x1a + 18);
			iSmbatStat = SMBATSendAndRead(SmbatDeviceClass->pCommPort->hCommPort, 
				szSendStr,
				(BYTE*)abyRcvBuf, 
				18,
				RS485_COMM_ABNORMALITY,//block 200ms
				MAX_RECVBUFFER_NUM_SMBAT);

			if (RESPONSE_OK == iSmbatStat)
			{
				break;	
			}
		}
	}
	else
	{
		for (i = 0; i < 5; i++)
		{
			//2011/02/16����˵������ͨ���жϣ�ʵ��û�취�ٽ����ݴ�
			//iSmbatStat = SMBATSendAndRead(SmbatDeviceClass->pCommPort->hCommPort, 
			//								szSendStr,
			//								abyRcvBuf, 
			//								18,
			//								RS485_COMM_NORMAL,//block 700ms
			//								0x1a + 18);
			iSmbatStat = SMBATSendAndRead(SmbatDeviceClass->pCommPort->hCommPort, 
				szSendStr,
				(BYTE*)abyRcvBuf, 
				18,
				RS485_COMM_NORMAL,//block 700ms
				MAX_RECVBUFFER_NUM_SMBAT);
			if (RESPONSE_OK == iSmbatStat)
			{
				break;	
			}

			Sleep(1000);
		}
	}

	if (RESPONSE_OK != iSmbatStat)
	{
		//TRACE("\n  ***SMBAT CID2 43 ERROR i = %d \n",i);
		return FALSE;
	}
	else if(iSmbatStat == RESPONSE_OK)
	{
		SmbatDeviceClass->pRoughData[PointStartPosition + SMBAT_COMMUNICATE_STAT].iValue 
			= SMBAT_COMM_OK;

		FixData(g_SmbatSampleData.OneStringSigs + SMBAT_BATTERY_LEAKAGE,
			s_Str43SigInfo,
			(BYTE*)(abyRcvBuf + DATAINFO_YDN23_SMBAT + 2)
			);
		SMBATUnpackCmd43(iStringNo, nUnitNo, SmbatDeviceClass, g_SmbatSampleData.OneStringSigs);

		return TRUE;
	}
	return TRUE;
}

void SMBAT_SetComStat(int iFirstStringNo, int Adrres, int iComStat,BOOL beNeedClrTimes,RS485_DEVICE_CLASS*  SmbatDeviceClass)
{
	int i = 0;
	int iStringNumOFUnit = 1;//ֻҪ�����ˣ�������һ������ˣ��������������	4802
	INT32 PointStartPosition  =	iFirstStringNo * SMBAT_MAX_SIGNALS_NUM;
	SmbatDeviceClass->pRoughData[PointStartPosition + SMBAT_COMMUNICATE_STAT].iValue 
		= iComStat;//4802 1�����  ����ʲô�豸���е�һ���ġ�

	//2�����
	if(Adrres >= SMBAT4806_ADDR_START && Adrres <= SMBAT4806_ADDR_END)
	{
		iStringNumOFUnit = SMBAT_4806_STRING_NUM;
	}

	//4�����
	if ((Adrres >= SMBAT4812_ADDR_START && Adrres <= SMBAT4812_ADDR_END))
	{
		iStringNumOFUnit = SMBAT_4812_STRING_NUM;
	}

	//2�����
	if (Adrres >= SMBAT2402_ADDR_START && Adrres<= SMBAT2402_ADDR_END)
	{
		iStringNumOFUnit = SMBAT_2402_STRING_NUM;
	}

	//have 4 batteries strings 
	if((Adrres >= SMBAT2406_ADDR_START && Adrres <= SMBAT2406_ADDR_END))
	{
		iStringNumOFUnit = SMBAT_2406_STRING_NUM;
	}

	//4�����
	if((Adrres >= SMBAT2412_ADDR_START && Adrres <= SMBAT2412_ADDR_END))
	{
		iStringNumOFUnit = SMBAT_2412_STRING_NUM;
	}

	for (i = 0; i  < iStringNumOFUnit; i++)
	{
		PointStartPosition  = (iFirstStringNo + i) * SMBAT_MAX_SIGNALS_NUM;
		SmbatDeviceClass->pRoughData[PointStartPosition + SMBAT_COMMUNICATE_STAT].iValue 
			= iComStat;
		if (beNeedClrTimes)
		{
			SmbatDeviceClass->pRoughData[PointStartPosition + SMBAT_INTERRUPT_TIMES].iValue 
				= 0;
		}
	}

	return;
}

/*=============================================================================*
* FUNCTION: SMBATClrCommBreakTimes
* PURPOSE : Only one time communication ok clean the failt flag
* INPUT:	INT32 iStringNo,
*			RS485_DEVICE_CLASS*  SmbatDeviceClass
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32  SMBATClrCommBreakTimes(INT32 iStringNo, RS485_DEVICE_CLASS*  SmbatDeviceClass,INT32 SMBATUnitNo)
{
	//INT32 PointStartPosition  =	iStringNo * SMBAT_MAX_SIGNALS_NUM;
	//SmbatDeviceClass->pRoughData[PointStartPosition + SMBAT_COMMUNICATE_STAT].iValue 
	//	= SMBAT_COMM_OK;//4802 1�����
	SMBAT_SetComStat(iStringNo, SMBATUnitNo, SMBAT_COMM_OK, TRUE ,SmbatDeviceClass);
	return 0;
}

/*=============================================================================*
* FUNCTION: SMBATIncCommBreakTimes
* PURPOSE : Record communication failt time,if more than three means report failes
* INPUT:	INT32 iStringNo
*			RS485_DEVICE_CLASS*  SmbatDeviceClass
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32  SMBATIncCommBreakTimes(INT32 iStringNo, RS485_DEVICE_CLASS*  SmbatDeviceClass,INT32 SMBATUnitNo)
{
	INT32 PointStartPosition  =	iStringNo * SMBAT_MAX_SIGNALS_NUM;
	int i = 0;


	//2011/02/16����˵������ͨ���жϣ�ʵ��û�취�ٽ����ݴ�
	//#define  RS485_SMBAT_INTERRUPT_TIMES 10
#define  RS485_SMBAT_INTERRUPT_TIMES   25

	//ֻ�øõ�ַ�ĵ�һ����ص�ͨ��״̬Ϊͨ������
	if(SmbatDeviceClass->pRoughData[PointStartPosition + SMBAT_INTERRUPT_TIMES].iValue 
		>= RS485_SMBAT_INTERRUPT_TIMES)
	{
		SMBAT_SetComStat(iStringNo, SMBATUnitNo, SMBAT_COMM_FAIL, FALSE,SmbatDeviceClass);

		return 0;	
	}
	else
	{
		if(SmbatDeviceClass->pRoughData[PointStartPosition + SMBAT_INTERRUPT_TIMES].iValue
			< (RS485_SMBAT_INTERRUPT_TIMES + 1))
		{
			SmbatDeviceClass->pRoughData[PointStartPosition + SMBAT_INTERRUPT_TIMES].iValue ++;
		}
		return 0;
	}		
}

/*=============================================================================*
* FUNCTION: SMBATNeedCommProc
* PURPOSE : Need process the rs485Comm port
* INPUT: 
*    
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32	SMBATNeedCommProc(void)
{
	if(g_RS485Data.CommPort.enumAttr		!= RS485_ATTR_NONE
		|| g_RS485Data.CommPort.enumBaud	!= RS485_ATTR_19200
		|| g_RS485Data.CommPort.bOpened		!= TRUE)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*=============================================================================*
* FUNCTION: SMBATReopenPort
* PURPOSE :
* INPUT: 
*    
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMBATReopenPort(void)
{
	INT32				iErrCode;
	INT32				iOpenTimes;
	iOpenTimes = 0;

	while (iOpenTimes < RS485_COMM_TRY_OPEN_TIMES)
	{
		if(g_RS485Data.CommPort.hCommPort && g_RS485Data.CommPort.bOpened)
		{
			RS485_Close(g_RS485Data.CommPort.hCommPort);
		}

		g_RS485Data.CommPort.hCommPort 
			= RS485_Open("19200, n, 8, 1", RS485_READ_WRITE_TIMEOUT, (int*)(&iErrCode));

		if(iErrCode == ERR_COMM_OK)
		{
			g_RS485Data.CommPort.bOpened = TRUE;
			g_RS485Data.CommPort.enumAttr = RS485_ATTR_NONE;
			g_RS485Data.CommPort.enumBaud = RS485_ATTR_19200;
			Sleep(5);
			break;
		}
		else
		{
			Sleep(1000);
			iOpenTimes++;
			g_RS485Data.CommPort.bOpened = FALSE;
			AppLogOut("RS485_SMBAT_ReOpen", APP_LOG_UNUSED,
				"[%s]-[%d] ERROR: failed to  open rs485Comm IN The SMBAT ReopenPort \n\r", 
				__FILE__, __LINE__);
		}
	}
	return 0;
}
/*=============================================================================*
* FUNCTION: bSmbatCheckCommFail()
* PURPOSE : 
* INPUT:	
*     
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 bSmbatCheckCommFail(RS485_DEVICE_CLASS*  SmbatDeviceClass, INT32 SMBATUnitNo)
{
	BYTE	szSendStr[20]	= {0};
	CHAR	abyRcvBuf[MAX_RECVBUFFER_NUM_SMBAT] = {0};
	INT32	iSmbatStatus	= SMBAT_SAMP_INVALID_VALUE;
	INT32	i;

	//RS485_ClrBuffer(SmbatDeviceClass->pCommPort->hCommPort);

	sprintf( (char *)szSendStr, 
		"~%02X%02X%02X%02X%04X\0",
		0x20, 
		(unsigned int)SMBATUnitNo, 
		SMBATCID1,
		SMBATCID2_GET_VERSION,
		0 );
	SMBAT_CheckSum( szSendStr );

	*(szSendStr + 17) =  byEOI;

	//TRACE(" ::: bCheck BAT  %d \n",SMBATUnitNo);

	for (i = 0; i < 1; i++)
	{
		iSmbatStatus = SMBATSendAndRead(SmbatDeviceClass->pCommPort->hCommPort, 
			szSendStr, 
			(BYTE*)abyRcvBuf, 
			18,
			RS485_COMM_NORMAL,
			18);

		if (RESPONSE_OK == iSmbatStatus)
		{
			break;
		}
	}
	return iSmbatStatus;
}

int iCalNextSMBATStringNo(int iThisAdrres, int iCurrentStringNo)
{
	int iNextSMBATStringNo = iCurrentStringNo;


	if(iThisAdrres >= SMBAT4802_ADDR_START && iThisAdrres <= SMBAT4802_ADDR_END)
	{
		iNextSMBATStringNo += SMBAT_4802_STRING_NUM;	
	}
	//have 2 batteries strings 
	if(iThisAdrres >= SMBAT4806_ADDR_START && iThisAdrres <= SMBAT4806_ADDR_END)
	{
		iNextSMBATStringNo += SMBAT_4806_STRING_NUM;
	}

	if ((iThisAdrres >= SMBAT4812_ADDR_START && iThisAdrres <= SMBAT4812_ADDR_END))
	{
		iNextSMBATStringNo += SMBAT_4812_STRING_NUM;
	}

	if (iThisAdrres >= SMBAT2402_ADDR_START && iThisAdrres<= SMBAT2402_ADDR_END)
	{
		iNextSMBATStringNo += SMBAT_2402_STRING_NUM;
	}

	//have 4 batteries strings 
	if((iThisAdrres >= SMBAT2406_ADDR_START && iThisAdrres <= SMBAT2406_ADDR_END))
	{
		iNextSMBATStringNo += SMBAT_2406_STRING_NUM;
	}

	if((iThisAdrres >= SMBAT2412_ADDR_START && iThisAdrres <= SMBAT2412_ADDR_END))
	{
		iNextSMBATStringNo += SMBAT_2412_STRING_NUM;
	}


	if (iNextSMBATStringNo >= 0 && iNextSMBATStringNo < SMBAT_STRING_NUM)
	{
		return iNextSMBATStringNo;
	}
	else
	{
		return SMBAT_STRING_NUM + SMBAT_STRING_NUM;//Ŀ������ѭ��
	}

}
/*=============================================================================*
* FUNCTION: SMBATSample
* PURPOSE : samping smbat  all signal info  
* INPUT: 
*     
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     
*
*============================================================================*/
INT32 SMBATSample(void* pDevice)
{
	//INT32				i;
	//INT32				j;
	//INT32				k;
	INT32				iStartBattNoOfUnitNo;
	//INT32				iTmpAddress;
	INT32				SMBATUnitNo;
	INT32				PointStartPosition;
	INT32				iCountTimes = 0;
	RS485_DEVICE_CLASS* SmbatDeviceClass = pDevice;
	iStartBattNoOfUnitNo		= 0;
	BOOL				bSamplingFlag = FALSE;

	if (SMBATNeedCommProc())
	{
		SMBATReopenPort();
	}

	if(SmbatDeviceClass->bNeedReconfig == TRUE)
	{
		SmbatDeviceClass->bNeedReconfig = FALSE;
		SMBATReconfig(SmbatDeviceClass);
		return TRUE;
	}

	/*
	RoughData[0][SMBAT_BATTERY_STRING_NUM].ivalue record exist smbat number
	RoughData[0][SMBAT_ADDRESS].ivalue record smbat address
	iBlockTime = 700MS
	*/
	iStartBattNoOfUnitNo = 0;
	while(iStartBattNoOfUnitNo < SmbatDeviceClass->pRoughData[SMBAT_BATTERY_STRING_NUM].iValue)
	{
		PointStartPosition = iStartBattNoOfUnitNo * SMBAT_MAX_SIGNALS_NUM;

		iCountTimes++;
		RUN_THREAD_HEARTBEAT();

		//�ݴ�������������
		if (iCountTimes > (SMBAT_STRING_NUM * 3))
		{
			return FALSE;
		}

		if (iStartBattNoOfUnitNo > SMBAT_STRING_NUM + 1)
		{
			break;
		}

		if( SmbatDeviceClass->pRoughData[PointStartPosition + SMBAT_ADDRESS].iValue
			!= SMBAT_SAMP_INVALID_VALUE)
		{

			RUN_THREAD_HEARTBEAT();

			SMBATUnitNo = SmbatDeviceClass->pRoughData[PointStartPosition + SMBAT_ADDRESS].iValue;

			//Batt testing return SM;
			if (bBattTesting(TRUE))
			{
				//printf("\n$$$$$$$$$$  in  SMBATSample bBattTesting  ADDR =%d return \n",SMBATUnitNo);
				return TRUE;
			}

			//CMD43 query one time
			if (SMBATSampleCmd43(iStartBattNoOfUnitNo, SMBATUnitNo, SmbatDeviceClass))
			{
				SMBATClrCommBreakTimes(iStartBattNoOfUnitNo, SmbatDeviceClass,SMBATUnitNo);
			}
			else
			{
				//Because communication error needn't sampling!! Jump to next address
				SMBATIncCommBreakTimes(iStartBattNoOfUnitNo, SmbatDeviceClass,SMBATUnitNo);
				/***********************Jump to next address ***********************/
				iStartBattNoOfUnitNo = iCalNextSMBATStringNo(SMBATUnitNo, iStartBattNoOfUnitNo);

				continue;
			}

			if (!SampCheckSmAddrProcess(SMBATUnitNo))
			{
				bSamplingFlag = FALSE;
			}
			else
			{
				bSamplingFlag = TRUE;
			}

			if (bSamplingFlag)
			{
				SMBATSampleCmd41(iStartBattNoOfUnitNo, SMBATUnitNo, SmbatDeviceClass);
			}

			/***********************Jump to next address ***********************/
			iStartBattNoOfUnitNo = iCalNextSMBATStringNo(SMBATUnitNo, iStartBattNoOfUnitNo);

		}//end if
	}//end while

	return TRUE;
}


/*=============================================================================*
* FUNCTION: SMBAT_StuffChannel
* PURPOSE : stuff samping data from smbat to  web and gc 
* INPUT: 
*     
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMBAT_StuffChannel(RS485_DEVICE_CLASS* SmbatDeviceClass, 
			 ENUMSIGNALPROC EnumProc,
			 LPVOID lpvoid)
{
	INT32					i,j;
	INT32					iStartPosition;
	RS485_VALUE				stTempEquipExist;

	RS485_CHN_TO_ROUGH_DATA  SMBAT_Sig[]=
	{
		//iChannel						iRoughData
		{SMBAT_CH_BLOCK_1_VOLT,			SMBAT_BLOCK_1_VOLT},
		{SMBAT_CH_BLOCK_2_VOLT,			SMBAT_BLOCK_2_VOLT},
		{SMBAT_CH_BLOCK_3_VOLT,			SMBAT_BLOCK_3_VOLT},
		{SMBAT_CH_BLOCK_4_VOLT,			SMBAT_BLOCK_4_VOLT},
		{SMBAT_CH_BLOCK_5_VOLT,			SMBAT_BLOCK_5_VOLT},
		{SMBAT_CH_BLOCK_6_VOLT,			SMBAT_BLOCK_6_VOLT},
		{SMBAT_CH_BLOCK_7_VOLT,			SMBAT_BLOCK_7_VOLT},
		{SMBAT_CH_BLOCK_8_VOLT,			SMBAT_BLOCK_8_VOLT},
		{SMBAT_CH_BLOCK_9_VOLT,			SMBAT_BLOCK_9_VOLT},
		{SMBAT_CH_BLOCK_10_VOLT,		SMBAT_BLOCK_10_VOLT},
		{SMBAT_CH_BLOCK_11_VOLT,		SMBAT_BLOCK_11_VOLT},
		{SMBAT_CH_BLOCK_12_VOLT,		SMBAT_BLOCK_12_VOLT},
		{SMBAT_CH_BLOCK_13_VOLT,		SMBAT_BLOCK_13_VOLT},
		{SMBAT_CH_BLOCK_14_VOLT,		SMBAT_BLOCK_14_VOLT},
		{SMBAT_CH_BLOCK_15_VOLT,		SMBAT_BLOCK_15_VOLT},
		{SMBAT_CH_BLOCK_16_VOLT,		SMBAT_BLOCK_16_VOLT},
		{SMBAT_CH_BLOCK_17_VOLT,		SMBAT_BLOCK_17_VOLT},
		{SMBAT_CH_BLOCK_18_VOLT,		SMBAT_BLOCK_18_VOLT},
		{SMBAT_CH_BLOCK_19_VOLT,		SMBAT_BLOCK_19_VOLT},
		{SMBAT_CH_BLOCK_20_VOLT,		SMBAT_BLOCK_20_VOLT},
		{SMBAT_CH_BLOCK_21_VOLT,		SMBAT_BLOCK_21_VOLT},
		{SMBAT_CH_BLOCK_22_VOLT,		SMBAT_BLOCK_22_VOLT},
		{SMBAT_CH_BLOCK_23_VOLT,		SMBAT_BLOCK_23_VOLT},
		{SMBAT_CH_BLOCK_24_VOLT,		SMBAT_BLOCK_24_VOLT},
		{SMBAT_CH_BLOCK_25_VOLT,		SMBAT_BLOCK_25_VOLT},	
		{SMBAT_CH_VOLTAGE,				SMBAT_VOLTAGE},
		{SMBAT_CH_AI2,					SMBAT_AI2},
		{SMBAT_CH_CURRENT,				SMBAT_CURRENT},
		{SMBAT_CH_AI4,					SMBAT_AI4},
		{SMBAT_CH_AMBIENT_TEMPERATURE,	SMBAT_AMBIENT_TEMPERATURE},
		{SMBAT_CH_ACID_TEMPERATURE,		SMBAT_ACID_TEMPERATURE},
		{SMBAT_CH_AI7,					SMBAT_AI7},	

		//43
		{SMBAT_CH_BATTERY_LEAKAGE,		SMBAT_BATTERY_LEAKAGE},
		{SMBAT_CH_LOW_ACID_LEVEL,		SMBAT_LOW_ACID_LEVEL},
		{SMBAT_CH_BATTERY_DISCONNECTED,	SMBAT_BATTERY_DISCONNECTED},
		{SMBAT_CH_DI4,					SMBAT_DI4},
		{SMBAT_CH_DI5,					SMBAT_DI5},
		{SMBAT_CH_DI6,					SMBAT_DI6},
		{SMBAT_CH_DI7,					SMBAT_DI7},	
		{SMBAT_CH_DI8,					SMBAT_DI8},
		{SMBAT_CH_RELAY_1_STAT,			SMBAT_RELAY_1_STAT},
		{SMBAT_CH_RELAY_2_STAT,			SMBAT_RELAY_2_STAT},	
		{SMBAT_CH_COMMUNICATE_STAT,		SMBAT_COMMUNICATE_STAT},
		{SMBAT_CH_EXIST_STAT,			SMBAT_WORKING_STAT},
		{SMBAT_CH_ADDRESS,				SMBAT_ADDRESS},	

		//add
		{SMBAT_CH_END_FLAG,				SMBAT_CH_END_FLAG},

	};

	//There		isn't	smbat	equip
	if(SmbatDeviceClass->pRoughData[SMBAT_GROUP_WORKING_STAT].iValue == SMBAT_EQUIP_NOT_EXISTENT)
	{
		for(i = 0;i < SMBAT_STRING_NUM;i++)
		{
			//�����������ļ��������ź������Ч�źš�
			stTempEquipExist.iValue = SMBAT_SAMP_INVALID_VALUE;
			j = 0;
			while(SMBAT_Sig[j].iChannel != SMBAT_CH_END_FLAG)
			{
				EnumProc(i * MAX_CHN_NUM_PER_SMBAT + SMBAT_Sig[j].iChannel ,
					stTempEquipExist.fValue,
					lpvoid 
					);

				j++;
			}

			iStartPosition = i * SMBAT_MAX_SIGNALS_NUM;
			EnumProc(i * MAX_CHN_NUM_PER_SMBAT + SMBAT_CH_EXIST_STAT,
				SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_WORKING_STAT].fValue,
				lpvoid 
				);
		}
	}

	/*
	SmbatDeviceClass->pRoughData[SMBAT_BATTERY_STRING_NUM].iValue   [ 0 -- 20 ] 
	*/
	/******************************************************************************************
	Last SMBAT need special operation! 
	Eg:
	The Last SMBAT is Actual connection with 1 battery string, 
	But the SMBAT use to 2412, The 2412 type will report 4 strings information. 
	So, We need remove 3 strings by the setting signal RS485_LST_BATT_NUM_SID.
	*******************************************************************************************/
	int iNeedRemoveBatNum = 0;
	iStartPosition = 0;

	if (SmbatDeviceClass->pRoughData[SMBAT_BATTERY_STRING_NUM].iValue > 0)
	{
		ASSERT((SmbatDeviceClass->pRoughData[SMBAT_BATTERY_STRING_NUM].iValue - 1) >=0 
			&& (SmbatDeviceClass->pRoughData[SMBAT_BATTERY_STRING_NUM].iValue - 1) < SMBAT_STRING_NUM);

		iStartPosition = (SmbatDeviceClass->pRoughData[SMBAT_BATTERY_STRING_NUM].iValue - 1)* SMBAT_MAX_SIGNALS_NUM;


		//printf("\n	SMBAT_BATTERY_STRING_NUM=%d	SMBAT_STRING_NUM_OF_TYPE=%d	\n",
		//	SmbatDeviceClass->pRoughData[SMBAT_BATTERY_STRING_NUM].iValue,
		//	SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_STRING_NUM_OF_TYPE].iValue);

		if (SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_STRING_NUM_OF_TYPE].iValue > 0 
			&& SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_STRING_NUM_OF_TYPE].iValue <= 4)
		{
			int iGetBatNumOfLastAddr = 0;
			iGetBatNumOfLastAddr = GetDwordSigValue(RS485_BATT_GROUP_EQP_ID,
				SIG_TYPE_SETTING, 
				RS485_LST_BATT_NUM_SID, 
				"Get Batt Num of last SMBAT");

			//printf("\n	iGetBatNumOfLastAddr=%d		\n",	iGetBatNumOfLastAddr);

			if (iGetBatNumOfLastAddr > 0 &&
				iGetBatNumOfLastAddr <= 4
				&& SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_STRING_NUM_OF_TYPE].iValue >= iGetBatNumOfLastAddr)
			{
				iNeedRemoveBatNum = SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_STRING_NUM_OF_TYPE].iValue - iGetBatNumOfLastAddr;

			}
			else
			{
				//The parameter setting is error Or GetDwordSigValue failt !!
				iNeedRemoveBatNum = 0;//No process!
			}
		}
		else
		{
			iNeedRemoveBatNum = 0;//No process!
		}
	}
	else
	{
		iNeedRemoveBatNum = 0;//No process!
	}

	int iNeedStufMaxBattNum = SmbatDeviceClass->pRoughData[SMBAT_BATTERY_STRING_NUM].iValue - iNeedRemoveBatNum;
	iNeedStufMaxBattNum = MIN(iNeedStufMaxBattNum, SMBAT_STRING_NUM);
	iNeedStufMaxBattNum = MAX(iNeedStufMaxBattNum, 0);

	//printf("\n	iNeedRemoveBatNum=%d	\n",iNeedRemoveBatNum);

	for(i = 0; i < iNeedStufMaxBattNum; i++)
	{
		j = 0;
		iStartPosition = i * SMBAT_MAX_SIGNALS_NUM;

		while(SMBAT_Sig[j].iChannel != SMBAT_CH_END_FLAG)
		{
			EnumProc(i * MAX_CHN_NUM_PER_SMBAT + SMBAT_Sig[j].iChannel ,
				SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_Sig[j].iRoughData].fValue,
				lpvoid 
				);
			j++;
		}
	}

	//Continue,	every	times  stuff 20 string map	�����ڵ��豸
	//for(i = SmbatDeviceClass->pRoughData[SMBAT_BATTERY_STRING_NUM].iValue; i < SMBAT_STRING_NUM; i++)
	for(i = iNeedStufMaxBattNum; i < SMBAT_STRING_NUM; i++)
	{
		iStartPosition = i * SMBAT_MAX_SIGNALS_NUM;

		stTempEquipExist.iValue = SMBAT_SAMP_INVALID_VALUE;
		j = 0;
		while(SMBAT_Sig[j].iChannel != SMBAT_CH_END_FLAG)
		{
			EnumProc(i * MAX_CHN_NUM_PER_SMBAT + SMBAT_Sig[j].iChannel ,
				stTempEquipExist.fValue,
				lpvoid 
				);

			j++;
		}

		stTempEquipExist.iValue = SMBAT_SAMPLE_FAIL;
		EnumProc(i * MAX_CHN_NUM_PER_SMBAT + SMBAT_CH_EXIST_STAT,
			stTempEquipExist.fValue,//SmbatDeviceClass->pRoughData[iStartPosition + SMBAT_WORKING_STAT].fValue,
			lpvoid 
			);
	}

	return 0;
}

/*=============================================================================*
* FUNCTION: SMBAT_UnpackPrdctInfoAndBarCode
* PURPOSE :  Arrange sampling data by 0xEC And by 0x51 to  struct st_RecordProdcInfo
* INPUT: 
*     
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL void SMBAT_UnpackPrdctInfoAndBarCode(PRODUCT_INFO * pInfo,BYTE *ReceiveFrame,BYTE *ReceiveFrameTwo)
{
	int i, iLen;
	BYTE szLenInfo[4] = {0};
	BYTE byMainVer, bySubVer;
	//check if support Product Info
	if (ReceiveFrame[7] == '0' && ReceiveFrame[8] == '4')
	{
		TRACE("Old module, not support product info!\n");
		//old module, clear all data
		pInfo->bSigModelUsed = FALSE;
		pInfo->szHWVersion[0] = 0;
		pInfo->szPartNumber[0] = 0;
		pInfo->szSerialNumber[0] = 0;
		pInfo->szSWVersion[0] = 0;
		return;
	}
	//1.get length
	for (i = 0 ; i < 3; i++)
	{
		szLenInfo[i] = ReceiveFrame[10 + i];
	}
	sscanf((char*)szLenInfo, "%03x", &iLen);

	//1.Serial number process
	for (i = 0; i < 11; i++)
	{
		if((SMBATMergeAsc((char*)ReceiveFrame + 13 + i * 2)>=48 && SMBATMergeAsc((char*)ReceiveFrame + 13 + i * 2)<=57)||
			(SMBATMergeAsc((char*)ReceiveFrame + 13 + i * 2)>=65 && SMBATMergeAsc((char*)ReceiveFrame + 13 + i * 2)<=90)||
			(SMBATMergeAsc((char*)ReceiveFrame + 13 + i * 2)>=97 && SMBATMergeAsc((char*)ReceiveFrame + 13 + i * 2)<=122))
		{
			pInfo->szSerialNumber[i] = SMBATMergeAsc((char*)ReceiveFrame + 13 + i * 2);

		}
		else
		{
			pInfo->szSerialNumber[i] = ' ';
		}

	}

	//2.HW version process - the last three character
	//printf("\n PartNumber process = %s\n", (char *)Frame+13+iLen-6);
	for (i = 0; i < 3; i++)
	{
		if((SMBATMergeAsc((char*)ReceiveFrame + 13 + iLen - 6 + i * 2)>=48 && SMBATMergeAsc((char*)ReceiveFrame + 13 + iLen - 6 + i * 2)<=57)||
			(SMBATMergeAsc((char*)ReceiveFrame + 13 + iLen - 6 + i * 2)>=65 && SMBATMergeAsc((char*)ReceiveFrame + 13 + iLen - 6 + i * 2)<=90)||
			(SMBATMergeAsc((char*)ReceiveFrame + 13 + iLen - 6 + i * 2)>=97 && SMBATMergeAsc((char*)ReceiveFrame + 13 + iLen - 6 + i * 2)<=122))
		{
			pInfo->szHWVersion[i] = SMBATMergeAsc((char*)ReceiveFrame + 13 + iLen - 6 + i * 2);
		}
		else
		{
			pInfo->szHWVersion[i] = ' ';

		}
	}

	//3.PartNumber process
	//printf("\n PartNumber process = %s\n", (char *)Frame+13+22);
	if ((iLen/2 - 14) > 16)
	{
		for (i = 0; i < 16; i++)
		{
			if((SMBATMergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)>=48 && SMBATMergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)<=57)||
				(SMBATMergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)>=65 && SMBATMergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)<=90)||
				(SMBATMergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)>=97 && SMBATMergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)<=122))
			{
				pInfo->szPartNumber[i] = SMBATMergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2);

			}
			else
			{
				pInfo->szPartNumber[i] = ' ';
			}

		}
	}
	else
	{
		for (i = 0; i < iLen/2-14; i++)
		{
			if((SMBATMergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)>=48 && SMBATMergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)<=57)||
				(SMBATMergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)>=65 && SMBATMergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)<=90)||
				(SMBATMergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)>=97 && SMBATMergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)<=122))
			{
				pInfo->szPartNumber[i] = SMBATMergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2);

			}
			else
			{
				pInfo->szPartNumber[i] = ' ';
			}

		}
	}

	byMainVer = SMBATMergeAsc((char*)ReceiveFrameTwo + 40 + 13);
	bySubVer  = SMBATMergeAsc((char*)ReceiveFrameTwo + 42 + 13);
	int iTempMainVer = 0;
	int iTempSubVer = 0;
	iTempMainVer = byMainVer;
	iTempSubVer = bySubVer;

	if (iTempMainVer >= 0 && iTempMainVer <= 16 &&
		iTempSubVer >= 0 && iTempSubVer <= 16)
	{
		sprintf(pInfo->szSWVersion, "%d.%02d\0", byMainVer, bySubVer);
	}

	TRACE("SMBAT BBU PartNumber:%s\n", pInfo->szPartNumber);
	TRACE("SMBAT BBU SW version:%s\n", pInfo->szSWVersion);
	TRACE("SMBAT BBU HW version:%s\n", pInfo->szHWVersion);
	TRACE("SMBAT BBU Serial Number:%s\n", pInfo->szSerialNumber);
}
/*=============================================================================*
* FUNCTION: SMBAT_GetProdctInfo
* PURPOSE : Get SMBAT Product Info 
* INPUT: 
*     
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void SMBAT_GetProdctInfo(HANDLE hComm, int nUnitNo, void *pPI)
{
	UNUSED(hComm);
	UNUSED(nUnitNo);
	PRODUCT_INFO*			pInfo;
	RS485_DEVICE_CLASS*		pSmbatDeviceClass;
	INT32					i,j,k;
	INT32					iAddr,iAddrStart,iAddrEnd;
	INT32					iSmBATStat;
	char					szVer[3] = "20";
	BYTE					szSendStr[40] = {0};
	BYTE					ReceiveFrame[MAX_RECVBUFFER_NUM_SMBAT] = {0}; 
	BYTE					ReceiveFrameTwo[MAX_RECVBUFFER_NUM_SMBAT] ={0};

	iAddrStart				= 0;
	iAddrEnd				= -1;
	pInfo					=	(PRODUCT_INFO *)pPI;
	pInfo					+=	SMBAT1_EID;
	pSmbatDeviceClass		=	g_RS485Data.pDeviceClass + DEVICE_CLASS_SMBAT;

	if (pSmbatDeviceClass->pRoughData == NULL)
	{
		TRACE(" G	BBU  GetProductInfo IN  SLAVE Mode  :  \n");	
		return;
	}

	if (SMBATNeedCommProc())
	{
	    SMBATReopenPort();
	}

	//ȫ�Ǵ��ڵ��豸�����Զ�����ǰ�ѷŵġ�	
	for(j = 0; j < SMBAT_EQUIP_MAX_ADDR + 1; j++, pInfo++)
	{	
		if (SMBAT_SAMP_INVALID_VALUE == g_SmbatSampleData.iRecordAddress[j])
		{
			break;
		}

		iAddr = g_SmbatSampleData.iRecordAddress[j];//�����ŵĶ��Ǵ��ڵ��豸	

		//Get ENP BarCode
		sprintf((char *)szSendStr, 
			"~%s%02X%02X%02X%04X\0",szVer, (unsigned int)iAddr, SMBATCID1, 0xEC, 0);
		SMBAT_CheckSum(szSendStr);

		Sleep(10);

		for (i = 0; i < 5; i++)
		{
			iSmBATStat = SMBATSendAndRead(g_RS485Data.CommPort.hCommPort,
				szSendStr,
				ReceiveFrame,
				18,
				RS485_COMM_NORMAL,
				4802);

			Sleep(100);
			if (RESPONSE_OK == iSmBATStat)
			{
				pInfo->bSigModelUsed = TRUE;
				Sleep(20);
				//Sampling Product Info by 0x51
				sprintf((char *)szSendStr, 
					"~%s%02X%02X%02X%04X\0",szVer, (unsigned int)iAddr, SMBATCID1, 0x51, 0);
				SMBAT_CheckSum(szSendStr);

				for (k = 0; k < 5; k++)
				{
					iSmBATStat = SMBATSendAndRead(g_RS485Data.CommPort.hCommPort,
						szSendStr,
						ReceiveFrameTwo,
						18,
						RS485_COMM_NORMAL,
						4802);
					if (RESPONSE_OK == iSmBATStat)
					{
						break;
					}
					else
					{
						//k++
					}
				}

				SMBAT_UnpackPrdctInfoAndBarCode(pInfo, ReceiveFrame, ReceiveFrameTwo);

				break;
			}
			else
			{
				//i++
			}
		}
	}

	if (g_RS485Data.CommPort.bOpened)
	{
		RS485_Close(g_RS485Data.CommPort.hCommPort);
		g_RS485Data.CommPort.bOpened = FALSE;
		g_RS485Data.CommPort.enumAttr = 0;
		g_RS485Data.CommPort.enumBaud = 0;
	}
}

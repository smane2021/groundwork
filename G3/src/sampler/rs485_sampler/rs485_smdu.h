/*============================================================================*
 *         Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : Unified Data Type Definition
 *
 *  FILENAME : rs485_smdu.h
 *  PURPOSE  : Define base data type.
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2007-08-6       V1.0           IlockTeng         Created.    
 *       
 *                                                     
 *-----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *          
 *      
 *-----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *      
 *    
 *============================================================================*/
#ifndef __RS485_SAMPLER_SMDU_H
#define __RS485_SAMPLER_SMDU_H

#include <stdio.h>
#include "basetypes.h"
#include "err_code.h"
#include "pubfunc.h"
#include "string.h"
#include "rs485_main.h"	
//"../can_sampler/can_sampler_main.h"

#define		SMDU_NO_RESPONSE_BEXISTENCE			1
#define		SMDU_RESPONSE_OK					0
//existent status
#define		SMDU_SWITCH_OFF						1
#define		SMDU_SWITCH_ON						0
#define		SMDU_EQUIP_EXISTENT					0
#define		SMDU_EQUIP_NOT_EXISTENT				1
#define		SMDU_COMM_OK						0
#define		SMDU_COMM_FAIL						1
#define		SMDU_MAX_NUM						8
#define		SMDU_ADDR_START						1
#define		SMDU_ADDR_END						8
#define		SMDU_CHANNEL_DISTANCE				80	
#define		SMDU_BYSOI							0x7E			
#define		SMDU_BYEOI							0x0D			
#define		SMDUCID1							0xd4
#define		SMDUCID2_GETALL						0x42
#define		SMDUCID2_GETSFTVER					0x51
#define		SMDU_GET_BARCODE_CID1				0xe1
#define		SMDU_GET_BATCODE_CID2				0xfa
#define		SMDU_SET_CFG_DATA_CID1				0xe1			//for powerkit
#define		SMDU_SET_CFG_DATA_CID2				0xec			//for powerkit
#define		SMDU_GET_CFG_DATA_CID1				0xe1			//for powerkit
#define		SMDU_GET_CFG_DATA_CID2				0xeb			//for powerkit
#define		SMDUCID2_CONTROL					0x45
#define		MAX_SENDBUFFER_NUM_SMDU				50
#define		MAX_RECVBUFFER_NUM_SMDU				4096
#define		OLD_MAX_NUM_RECT					100
#define		OLD_MAX_NUM_SMDU					8
#define		OLD_MAX_NUM_CONVERT					50
#define		OLD_CAN_SAMPLER_DATA_RECORDS		1
#define		OLD_CAN_SAMPLER_DATA_FREQUENCY		1000.0
#define		OLD_CAN_SAMPLER_FLASH_DATA		"RS485_data.log"


#define		SMDU_SAMP_INVALID_VALUE				(-9999)			//initialization data
#define		SMDU_RS485_GROUP_EQUIPID			106
#define		CFG_RS485_CHANGED_SIG_ID			2
#define		SMDU_CFG_TYPE_FLOAT					0x01
#define		SMDU_CFG_SERIALNO_OVER_VOLT			2
#define		SMDU_CFG_SERIALNO_UNOVER_VOLT		0

#define		SMDU_CFG_SERIALNO_BATT1_OVER_CURR	4
#define		SMDU_CFG_SERIALNO_BATT2_OVER_CURR	6
#define		SMDU_CFG_SERIALNO_BATT3_OVER_CURR	8
#define		SMDU_CFG_SERIALNO_BATT4_OVER_CURR	10
//Folling are added by Jimmy 20110620 for CR
#define		SMDU_CFG_SERIALNO_SHUNT1_A	13
#define		SMDU_CFG_SERIALNO_SHUNT1_MV	14
#define		SMDU_CFG_SERIALNO_SHUNT2_A	15
#define		SMDU_CFG_SERIALNO_SHUNT2_MV	16
#define		SMDU_CFG_SERIALNO_SHUNT3_A	17
#define		SMDU_CFG_SERIALNO_SHUNT3_MV	18
#define		SMDU_CFG_SERIALNO_SHUNT4_A	19
#define		SMDU_CFG_SERIALNO_SHUNT4_MV	20
//#define		SMDU_CFG_SERIALNO_SHUNT5_A	51
//#define		SMDU_CFG_SERIALNO_SHUNT5_MV	52

#define		SMDU_CHNNL_CTRL_START				6500
#define		SMDU_RS485_CNSS_LVD1				6501
#define		SMDU_RS485_CNSS_LVD2				6502
//added by Jimmy for CR 20110621
#define		SMDU_RS485_CNSS_SHUNT1_MV			6503
#define		SMDU_RS485_CNSS_SHUNT1_A			6504
#define		SMDU_RS485_CNSS_SHUNT2_MV			6505
#define		SMDU_RS485_CNSS_SHUNT2_A			6506
#define		SMDU_RS485_CNSS_SHUNT3_MV			6507
#define		SMDU_RS485_CNSS_SHUNT3_A			6508
#define		SMDU_RS485_CNSS_SHUNT4_MV			6509
#define		SMDU_RS485_CNSS_SHUNT4_A			6510

#define		SMDU_OVER_CURRENT_VERY_HI			4
#define		SMDU_OVER_CURRENT_NORMAL_HI			2
#define		SMDU_OVER_CURRENT_VERY_LOW			3
#define		SMDU_OVER_CURRENT_NORMAL_LOW		1
#define		SMDU_SHECKSUM_EOI					5
#define		SMDU_MAX_TIMES_GET_ALL				10

#define		SMDU_BUS_BAR_VOLTAGE_ALARM_VERY_LOW		3
#define		SMDU_BUS_BAR_VOLTAGE_ALARM_NORMAL_LOW	1
#define		SMDU_BUS_BAR_VOLTAGE_ALARM_VERY_HI		4
#define		SMDU_BUS_BAR_VOLTAGE_ALARM_NORMAL_HI	2

//The following are used to tell System is running on 485 or Can
#define		SYS_EQUIP_ID		1
#define		SIG_TYPE_SAMPLING	0
#define		RUN_MODE_SIG_ID		97	
#define		RUN_MODE_IS_485		1
#define		RUN_MODE_IS_CAN		2
//#define		RUN_MODE_NOT_USED	0


enum YDN23PROTOCOLINFO_SMDU
{
	CHECKSUM_ERROR_YDN23_SMDU = 2,
	ADDR_YDN23_SMDU = 3,

	CID1H_YDN23_SMDU = 5,
	CID1L_YDN23_SMDU = 6,
	CID2_YDN23_SMDU	= 7,
	LENGTH_YDN23_SMDU = 9,
	DATA_YDN23_SMDU = 13,
	DATAF_YDN23_SMDU = 15,
	DATA_YDN23_SFT_VER_SMDU = 20 + 13,
	//others will add here
};

enum SMDU_SAMP_CHANNEL
{
	SM_CH_SMDU_GROUP_STATE = 6450,	//SM_DU Group State
	SM_CH_SMDU_NUM,					//SM_DU Board Number

	SM_CH_LOAD_CURR1 = 6501,			//Load 1 Current
	SM_CH_LOAD_CURR2,				//Load 2 Current
	SM_CH_LOAD_CURR3,				//Load 3 Current
	SM_CH_LOAD_CURR4,				//Load 4 Current
	SM_CH_BUS_VOLT,					//Bus Voltage
	SM_CH_CURR_LIMIT,				//Current Limitation
	SM_CH_BATT_CURR1,				//Battery 1 Current
	SM_CH_BATT_CURR2,				//Battery 2 Current
	SM_CH_BATT_CURR3,				//Battery 3 Current
	SM_CH_BATT_CURR4,				//Battery 4 Current
	SM_CH_TEMPERATURE1,				//Temperature 1
	SM_CH_TEMPERATURE2,				//Temperature 2
	SM_CH_BATT_VOLT1,				//Battery 1 Voltage
	SM_CH_BATT_VOLT2,				//Battery 2 Voltage
	SM_CH_BATT_VOLT3,				//Battery 3 Voltage
	SM_CH_BATT_VOLT4,				//Battery 4 Voltage
	SM_CH_TOTAL_RUN_TIME,			//Total Run Time in hour
	SM_CH_RESERVE,					//Reserve
	SM_CH_SERIAL_NO_HIGH,			//Serial No. High
	SM_CH_SERIAL_NO_LOW,			//Serial No. Low
	SM_CH_BARCODE1,					//Bar Code 1
	SM_CH_BARCODE2,					//Bar Code 2
	SM_CH_BARCODE3,					//Bar Code 3
	SM_CH_BARCODE4,					//Bar Code 4
	SM_CH_VERSION_NO,				//Version
	SM_CH_LOAD_FUSE16,				//Load Fuse 16
	SM_CH_LOAD_FUSE15,				//Load Fuse 15
	SM_CH_LOAD_FUSE14,				//Load Fuse 14
	SM_CH_LOAD_FUSE13,				//Load Fuse 13
	SM_CH_LOAD_FUSE12,				//Load Fuse 12
	SM_CH_LOAD_FUSE11,				//Load Fuse 11
	SM_CH_LOAD_FUSE10,				//Load Fuse 10
	SM_CH_LOAD_FUSE9,				//Load Fuse 9
	SM_CH_LOAD_FUSE8,				//Load Fuse 8
	SM_CH_LOAD_FUSE7,				//Load Fuse 7
	SM_CH_LOAD_FUSE6,				//Load Fuse 6
	SM_CH_LOAD_FUSE5,				//Load Fuse 5
	SM_CH_LOAD_FUSE4,				//Load Fuse 4
	SM_CH_LOAD_FUSE3,				//Load Fuse 3
	SM_CH_LOAD_FUSE2,				//Load Fuse 2
	SM_CH_LOAD_FUSE1,				//Load Fuse 1
	SM_CH_BATT_FUSE2,				//Battery Fuse 2
	SM_CH_BATT_FUSE1,				//Battery Fuse 1
	SM_CH_DC_VOLT_ST,				//DC Voltage Status
	SM_CH_LVD2_FAULT_ST,			//Lvd 2 FAULT Status
	SM_CH_LVD1_FAULT_ST,			//Lvd 1 FAULT Status
	SM_CH_LVD2_CMD_ST,				//Lvd 2 Command Status
	SM_CH_LVD1_CMD_ST,				//Lvd 1 Command Status
	SM_CH_BATT1_EXIST_ST,			//Is Battery 1 Existent
	SM_CH_BATT2_EXIST_ST,			//Is Battery 2 Existent
	SM_CH_BATT3_EXIST_ST,			//Is Battery 3 Existent
	SM_CH_BATT4_EXIST_ST,			//Is Battery 4 Existent
	SM_CH_BATT_FUSE4,				//Battery Fuse 4
	SM_CH_BATT_FUSE3,				//Battery Fuse 3
	SM_CH_EXIST_ST,					//Is Existent
	SM_CH_INTTERUPT_STATE,			//Communication Interrupt Times
	SM_CH_BATT1_CURR_ST,			//Battery 1 work state
	SM_CH_BATT2_CURR_ST,			//Battery 2 work state
	SM_CH_BATT3_CURR_ST,			//Battery 3 work state
	SM_CH_BATT4_CURR_ST,			//Battery 4 work state
	SM_CH_TOTAL_LOAD_CURR,			//All Load Current
	SM_CH_COMM_ADDR,				//Communication Address
	SM_CH_RATED_CAP,				//Battery Rated Capacity 
	SM_CH_OVER_VOLT,				//DC Over Voltage 
	SM_CH_UNDER_VOLT,				//DC Under Voltage
	SM_CH_BATT1_OVER_CURR,			//Battery 1 Over Current 
	SM_CH_BATT2_OVER_CURR,			//Battery 2 Over Current 
	SM_CH_BATT3_OVER_CURR,			//Battery 3 Over Current 
	SM_CH_BATT4_OVER_CURR,			//Battery 4 Over Current 

	SM_CH_SOFT_SWITCH = 6574,			//added by Jimmy for DipSwitch status of setting Shunt size
	//SM_CH_CURR5_ENABLE = 6581,			//added by Jimmy for enable curr5 setting Shunt size

	//SM_CH_SHUNT1_MV = 6570,				//added by Jimmy for samp 1~5 Shunt size
	//SM_CH_SHUNT1_A,
	//SM_CH_SHUNT2_MV,
	//SM_CH_SHUNT2_A,
	//SM_CH_SHUNT3_MV,
	//SM_CH_SHUNT3_A,
	//SM_CH_SHUNT4_MV,
	//SM_CH_SHUNT4_A,
	//SM_CH_SHUNT5_MV,
	//SM_CH_SHUNT5_A,	

	SM_CH_SHUNT1_VALUE_CHANGED = 6575,
	SM_CH_SHUNT2_VALUE_CHANGED,
	SM_CH_SHUNT3_VALUE_CHANGED,
	SM_CH_SHUNT4_VALUE_CHANGED,
	SM_CH_RS485_DC_FUSE_EXISTENCE = 6580,
	//SM_CH_SHUNT5_VALUE_CHANGED,
	SM_CH_BATT5_EXIST_ST = 10542,
};

enum RS485_SMDU_ROUGH_DATA_IDX
{

	
	/*****************ALL SIGNAL 0X42  ANOLOG SIGNAL********************/	
	//StdEquip_SMDUUnit.cfg
	SMDU_BUS_BAR_VOLTAGE = 0,
	//UNUSED
	SMDU_BATTERY_1_FUSE_VOLTG,
	SMDU_BATTERY_2_FUSE_VOLTG,
	SMDU_BATTERY_3_FUSE_VOLTG,
	SMDU_BATTERY_4_FUSE_VOLTG,
	//StdEquip_SMDUUnit.cfg
	SMDU_LOAD_CURRENT_1,	
	SMDU_LOAD_CURRENT_2,	
	SMDU_LOAD_CURRENT_3,	
	SMDU_LOAD_CURRENT_4,	
	SMDU_ALL_LOAD_CURRENT,	
	//SMDUBattery.cfg
	SMDU_BATTERY_CURRENT_1,
	SMDU_BATTERY_CURRENT_2,
	SMDU_BATTERY_CURRENT_3,
	SMDU_BATTERY_CURRENT_4,
	SMDU_BATTERY_VOLTAGE_1,
	SMDU_BATTERY_VOLTAGE_2,
	SMDU_BATTERY_VOLTAGE_3,
	SMDU_BATTERY_VOLTAGE_4,
	/*****************SET SIGNAL********************/	
	SMDU_BATTERY_RATED_CAP,
	SMDU_LVD_1_OFF_VOLTAGE,			//�µ��ѹ
	SMDU_LVD_1_ON_VOLTAGE,			//�ϵ��ѹ
	SMDU_LVD_2_OFF_VOLTAGE,			//�µ��ѹ
	SMDU_LVD_2_ON_VOLTAGE,			//�ϵ��ѹ
/*****************SWITCH SIGNAL********************/	
	SMDU_LOAD_FUSE_INPUT_1,
	SMDU_LOAD_FUSE_INPUT_2,
	SMDU_LOAD_FUSE_INPUT_3,
	SMDU_LOAD_FUSE_INPUT_4,
	SMDU_LOAD_FUSE_INPUT_5,
	SMDU_LOAD_FUSE_INPUT_6,
	SMDU_LOAD_FUSE_INPUT_7,
	SMDU_LOAD_FUSE_INPUT_8,
	SMDU_LOAD_FUSE_INPUT_9,
	SMDU_LOAD_FUSE_INPUT_10,
	SMDU_LOAD_FUSE_INPUT_11,
	SMDU_LOAD_FUSE_INPUT_12,
	SMDU_LOAD_FUSE_INPUT_13,
	SMDU_LOAD_FUSE_INPUT_14,
	SMDU_LOAD_FUSE_INPUT_15,
	SMDU_LOAD_FUSE_INPUT_16,
	SMDU_CONTACTOR_INPUT_1,//contact
	SMDU_CONTACTOR_INPUT_2,
/*****************DIP switch SIGNAL********************/	
	SMDU_DIP_SWITCH1_1,
	SMDU_DIP_SWITCH1_2,
	SMDU_DIP_SWITCH1_3,
	SMDU_DIP_SWITCH1_4,
	SMDU_DIP_SWITCH1_5,
	SMDU_DIP_SWITCH1_6,
	SMDU_DIP_SWITCH1_7,
	SMDU_DIP_SWITCH1_8,
	//
	SMDU_DIP_SWITCH2_1,
	SMDU_DIP_SWITCH2_2,
	SMDU_DIP_SWITCH2_3,
	SMDU_DIP_SWITCH2_4,
	SMDU_DIP_SWITCH2_5,
	SMDU_DIP_SWITCH2_6,
	SMDU_DIP_SWITCH2_7,
	SMDU_DIP_SWITCH2_8,
	//
	SMDU_DIP_SWITCH3_1,
	SMDU_DIP_SWITCH3_2,
	SMDU_DIP_SWITCH3_3,
	SMDU_DIP_SWITCH3_4,
	SMDU_DIP_SWITCH3_5,
	SMDU_DIP_SWITCH3_6,
	SMDU_DIP_SWITCH3_7,
	SMDU_DIP_SWITCH3_8,
	//unused
	SMDU_RUNNING_JUMP_STAT,
	//StdEquip_SMDULVD.cfg
	SMDU_LVD_1_STATUS,
	SMDU_LVD_2_STATUS,
	//StdEquip_SMDUDCFuse.cfg
	SMDU_LOAD_FUSE_1_ARLAM_STAT,
	SMDU_LOAD_FUSE_2_ARLAM_STAT,
	SMDU_LOAD_FUSE_3_ARLAM_STAT,
	SMDU_LOAD_FUSE_4_ARLAM_STAT,
	SMDU_LOAD_FUSE_5_ARLAM_STAT,
	SMDU_LOAD_FUSE_6_ARLAM_STAT,
	SMDU_LOAD_FUSE_7_ARLAM_STAT,
	SMDU_LOAD_FUSE_8_ARLAM_STAT,
	SMDU_LOAD_FUSE_9_ARLAM_STAT,
	SMDU_LOAD_FUSE_10_ARLAM_STAT,
	SMDU_LOAD_FUSE_11_ARLAM_STAT,
	SMDU_LOAD_FUSE_12_ARLAM_STAT,
	SMDU_LOAD_FUSE_13_ARLAM_STAT,
	SMDU_LOAD_FUSE_14_ARLAM_STAT,
	SMDU_LOAD_FUSE_15_ARLAM_STAT,
	SMDU_LOAD_FUSE_16_ARLAM_STAT,
	//StdEquip_SMDULVD.cfg
	SMDU_LVD_1_FAULT_STAT,//LVD1 Fault Status	
	SMDU_LVD_2_FAULT_STAT,//LVD2 Fault Status	

	SMDU_EQUIP_SELFONESELF_STAT,
	SMDU_LVD_1_OFF_ALARM,//LVD1 CMD Status
	SMDU_LVD_2_OFF_ALARM,
	SMDU_BUS_BAR_VOLTAGE_ALARM,
	
	//StdEquip_SMDUBattFuseUnit.cfg
	SMDU_BATTERY_FUSE_1_STAT,			//Battery Fuse1 Status	6543	E
	SMDU_BATTERY_FUSE_2_STAT,
	SMDU_BATTERY_FUSE_3_STAT,
	SMDU_BATTERY_FUSE_4_STAT,
	//SMDUBattery.cfg
	SMDU_CURRENT_OVER_CURRENT_1,		//Battery Over Current
	SMDU_CURRENT_OVER_CURRENT_2,
	SMDU_CURRENT_OVER_CURRENT_3,
	SMDU_CURRENT_OVER_CURRENT_4,

	//StdEquip_SMDUUnit.cfg
	SMDU_VERSION_NO,					//92	Version No.					6525			U
	SMDU_FEATURE,
	SMDU_SERIAL_NO_H,					//93	Serial No. High				6519			U
	SMDU_SERIAL_NO_L,					//94	Serial No. Low				6520			U
	SMDU_BAR_CODE_1,					//95	Bar Code 1					6521			U
	SMDU_BAR_CODE_2,					//96	Bar Code 2					6522			U
	SMDU_BAR_CODE_3,					//97	Bar Code 3		  			6523			U
	SMDU_BAR_CODE_4,					//98	Bar Code 4			  		6524			U


//record assistant info for smdu sampler
	SMDU_RS485_SEQ_NO,	
	SMDU_RS485_ADDRESS,
	SMDU_RS485_COMMUNICATION,
	SMDU_RS485_EXISTENCE,
	SMDU_BATT1_EXIST_ST,
	SMDU_BATT2_EXIST_ST,
	SMDU_BATT3_EXIST_ST,
	SMDU_BATT4_EXIST_ST,

// Sig by 0xeb for PowerKit
	SMDU_OVER_VOLT,
	SMDU_UNDER_VOLT,

	SM_BATT1_OVER_CURR,
	SM_BATT2_OVER_CURR,
	SM_BATT3_OVER_CURR,
	SM_BATT4_OVER_CURR,

	SMDU_GROUP_SM_ACTUAL_NUM = SMDU_UNDER_VOLT + 20,
	SMDU_GROUP_SM_STATE,
	SMDU_GROUP_SM_ALL_NO_RESPONSE,

	
	SMDU_INTERRUPT_TIMES,
	SMDU_SAMPLING_TIMES,

	//�����Ѿ��в���3�����źţ��������ﲻ�ظ�����
	//SMDU_DIP_SWITCH_FOR_SOFT_STATUS,	//added by Jimmy 2011/06/18  this signal is for status of soft-switch of setting shunt size��
	//SMDU_CURR5_ENABLE,			//added by Jimmy 2011/06/18  this signal is for enable disp curr5 setting
	//added for CR 20110620
	SMDU_SHUNT1_A,
	SMDU_SHUNT1_MV,
	SMDU_SHUNT2_A,
	SMDU_SHUNT2_MV,
	SMDU_SHUNT3_A,
	SMDU_SHUNT3_MV,
	SMDU_SHUNT4_A,
	SMDU_SHUNT4_MV,
	//SMDU_SHUNT5_A,
	//SMDU_SHUNT5_MV,	

	SMDU_SHUNT1_VALUE_CHANGED,
	SMDU_SHUNT2_VALUE_CHANGED,
	SMDU_SHUNT3_VALUE_CHANGED,
	SMDU_SHUNT4_VALUE_CHANGED,
	//SMDU_SHUNT5_VALUE_CHANGED,

	SMDU_RS485_DC_FUSE_EXISTENCE,
	SMDU_RS485_MAX_SIG_END,

};


struct _SMDUDATASTRUCT
{
    BYTE nType;				// ��������:0-������־,1-4BYTES(�޷��ţ�,2-4 bytes���з��ţ� 3-2Bytes
    int  nOffset;			// ������ʼ��ַ���ڱ����ݰ��е���ʼλ�ô�ͷ�ַ���ʼ��0��
    int  iRoughPosition;	// ϵͳ�����ͨ���ţ�����ģ��Ĳ���������ƫ�Ʊ�ʾ��    
};
typedef struct _SMDUDATASTRUCT  SMDUDATASTRUCT;

union _SMDUSTRTOFLOAT
{
	float f_value;       // ������
	BYTE  by_value[4];   // �ַ���
};
typedef union _SMDUSTRTOFLOAT	SMDUSTRTOFLOAT;



struct	tagOLD_PARAM_UNIFY_IDX
{
	int		iEquipId;
	int		iEquipIdDifference;
	int		iSigId;				//for -48V system parameter
	int		iSigId24V;			//for 24V system parameter
	int		iRoughData;
	UINT	uiSerialNo;
};

typedef struct tagOLD_PARAM_UNIFY_IDX RS485_OLD_PARAM_UNIFY_IDX;



struct	tagOLD_CAN_RECTIFIER_STORAGE
{
	int			iSeqNo;
	DWORD		dwAddress;
	DWORD		dwHighSn;
    DWORD		dwSerialNo;
	int			iPositionNo;
	int			iAcPhase;			//0: A phase, 1: B phase, 2: C phase
	BOOL		bExistence;
};
typedef struct tagOLD_CAN_RECTIFIER_STORAGE OLD_CAN_RECTIFIER_STORAGE;

struct	tagOLD_CAN_CONVERTER_STORAGE
{
	int			iSeqNo;
	DWORD		dwAddress;
	DWORD		dwHighSn;
    DWORD		dwSerialNo;
	int			iPositionNo;
	BOOL		bExistence;
};
typedef struct tagOLD_CAN_CONVERTER_STORAGE OLD_CAN_CONVERTER_STORAGE;

struct	tagOLD_CAN_SM_DU_STORAGE
{
	int			iSeqNo;
	DWORD		iAddress;
	DWORD		dwHighSn;
    DWORD		dwSerialNo;
	BOOL		bExistence;
};
typedef struct tagOLD_CAN_SM_DU_STORAGE OLD_CAN_SM_DU_STORAGE;

//tagCAN_FLASH_DATA
struct	tagRS485_FLASH_DATA
{
	//Ranged by address
	OLD_CAN_RECTIFIER_STORAGE	aRectifierInfo[OLD_MAX_NUM_RECT];
	OLD_CAN_CONVERTER_STORAGE	aConverterInfo[OLD_MAX_NUM_CONVERT];
	OLD_CAN_SM_DU_STORAGE		aSmduInfo[OLD_MAX_NUM_SMDU];
	DWORD						dwFirstTurnonFlag;
};
typedef struct tagRS485_FLASH_DATA RS485_FLASH_DATA;


struct stSMDUSamplerData
{
	BYTE				abyRcvBuf[MAX_RECVBUFFER_NUM_SMDU];	
	BYTE				abySendBuf[MAX_SENDBUFFER_NUM_SMDU];
	RS485_FLASH_DATA	OLD_CANFlashData;
	RS485_VALUE			aRoughDataSmdu[SMDU_MAX_NUM][SMDU_RS485_MAX_SIG_END];
	RS485_VALUE			aDIPRoughDataSmdu[SMDU_MAX_NUM][16];
};
typedef struct stSMDUSamplerData SMDU_RS485_SAMPLER_DATA;


INT32 SMDU_InitRoughValue(RS485_DEVICE_CLASS*  p_SmduDeviceClass);
INT32 SMDUSampleCmd42(INT32 iAddr, RS485_DEVICE_CLASS*  p_SmduDeviceClass);
INT32 SMDUSample(RS485_DEVICE_CLASS*  p_SmduDeviceClass);
INT32 SMDUSampleCmd51(INT32 iAddr, RS485_DEVICE_CLASS*  p_SmduDeviceClass);
INT32 SMDUUnpackCmdfa(INT32 iAddr, 
					  RS485_DEVICE_CLASS* p_SmduDeviceClass, 
					  BYTE* pReceiveData);

INT32	SMDUNeedCommProc(void);
INT32 SMDUReopenPort(void);
//static INT32	s_giGetCommBusMode;































































































































#endif


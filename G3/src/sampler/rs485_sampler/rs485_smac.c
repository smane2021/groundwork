/*============================================================================*
 *         Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : Unified Data Type Definition
 *
 *  FILENAME : rs485_smac.c
 *  PURPOSE  : Sampling  Smac data and control Smac equip
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2007-07-12      V1.0           IlockTeng         Created.    
 *    2009-04-14     
 *                                                     
 *-----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *          
 *      
 *-----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *      
 *    
 *============================================================================*/

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "rs485_smac.h"
#include "rs485_comm.h"

static BYTE SMAC_MergeAsc(char *p);
#define SMAC_SIG_END			-1
#define SMAC_CH_SIG_END			-1
//#define FLOAT_EQUAL(f1, f2)		(ABS(((f1)-(f2))) <= EPSILON)
BYTE							SMACszVer[5] = {0};   
SMAC_SAMP_DATA					g_SmacSampleData;
extern 	RS485_SAMPLER_DATA		g_RS485Data;


//Get sys Anolog CID2 = 41
LOCAL SMACDATASTRUCT s_Smac41SigInfo[] =
{
	//DATAFLAG
    { 1,   0, 1,  0 },  // ������ѹA
    { 1,   8, 1,  1 },  // ������ѹB
    { 1,  16, 1,  2 },  // ������ѹC
    { 1,  24, 1,  3 },  // ����ߵ�ѹAB 
    { 1,  32, 1,  4 },  // ����ߵ�ѹBC 
    { 1,  40, 1,  5 },  // ����ߵ�ѹCA
    { 1,  48, 1,  6 },  // �������A
    { 1,  56, 1,  7 },  // �������B
    { 1,  64, 1,  8 },  // �������C
    { 1,  72, 1,  9 },  // ���Ƶ��
    { 1,  80, 1, 10 },  // �������й�����
    { 1,  88, 1, 11 },  // A���й�����
    { 1,  96, 1, 12 },  // B���й�����
    { 1, 104, 1, 13 },  // C���й�����
    { 1, 112, 1, 14 },  // �������޹�����
    { 1, 120, 1, 15 },  // A���޹�����
    { 1, 128, 1, 16 },  // B���޹�����
    { 1, 136, 1, 17 },  // C���޹�����
    { 1, 144, 1, 18 },  // ���������ڹ���
    { 1, 152, 1, 19 },  // A�����ڹ���
    { 1, 160, 1, 20 },  // B�����ڹ���
    { 1, 168, 1, 21 },  // C�����ڹ���
    { 1, 176, 1, 22 },  // ���๦������
    { 1, 184, 1, 23 },  // A�๦������
    { 1, 192, 1, 24 },  // B�๦������
    { 1, 200, 1, 25 },  // C�๦������
    { 1, 208, 1, 26 },  // Ia��������
    { 1, 216, 1, 27 },  // Ib��������
    { 1, 224, 1, 28 },  // Ic��������
    { 1, 232, 1, 29 },  // A�����THD
    { 1, 240, 1, 30 },  // B�����THD
    { 1, 248, 1, 31 },  // C�����THD
    { 1, 256, 1, 32 },  // A���ѹTHD
    { 1, 264, 1, 33 },  // B���ѹTHD
    { 1, 272, 1, 34 },  // C���ѹTHD
    { 1, 280, 1, 35 },  // ���й�����
    { 1, 288, 1, 36 },  // ���޹�����
    { 1, 296, 1, 37 },  // �����ڵ���
    { 1, 304, 1, 38 },  // �¶�
    { 1, 312, 1, 39 },  // ������ص�ѹ
    
    { 0,  0,  0,  0 }   // ����
};

// 	CID2=43
LOCAL SMACDATASTRUCT s_Smac43OnOff[] =
{
    { 3,  0,  1,  0 },  // ������ͨ��1״̬  00--�͵�ƽ��01--�ߵ�ƽ
    { 3,  2,  1,  1 },  // ������ͨ��2״̬  00--�͵�ƽ��01--�ߵ�ƽ
    { 3,  4,  1,  2 },  // ������ͨ��3״̬  00--�͵�ƽ��01--�ߵ�ƽ
    { 3,  6,  1,  3 },  // ������ͨ��4״̬  00--�͵�ƽ��01--�ߵ�ƽ
    { 3,  8,  1,  4 },  // ������ͨ��5״̬  00--�͵�ƽ��01--�ߵ�ƽ
    { 3, 10,  1,  5 },  // ������ͨ��6״̬  00--�͵�ƽ��01--�ߵ�ƽ
    { 3, 12,  1,  6 },  // ������ͨ��7״̬  00--�͵�ƽ��01--�ߵ�ƽ
    { 3, 14,  1,  7 },  // ������ͨ��8״̬  00--�͵�ƽ��01--�ߵ�ƽ
    { 3, 16,  1,  8 },  // ������ͨ��9״̬  00--�͵�ƽ��01--�ߵ�ƽ
    { 3, 18,  1,  9 },  // ������ͨ��10״̬  00--�͵�ƽ��01--�ߵ�ƽ
    { 3, 20,  1,  10},  // ������ͨ��11״̬  00--�͵�ƽ��01--�ߵ�ƽ
    { 3, 22,  1,  11},  // ������ͨ��12״̬  00--�͵�ƽ��01--�ߵ�ƽ

    { 3, 24,  1,  12 },  // �̵���1״̬  00--�͵�ƽ��01--�ߵ�ƽ
    { 3, 26,  1,  13 },  // �̵���2״̬  00--�͵�ƽ��01--�ߵ�ƽ
    { 3, 28,  1,  14 },  // �̵���3״̬  00--�͵�ƽ��01--�ߵ�ƽ
    { 3, 30,  1,  15 },  // �̵���4״̬  00--�͵�ƽ��01--�ߵ�ƽ
    { 3, 32,  1,  16 },  // �ͻ���������/ֹͣ״̬  00--����ֹͣ��01--�ͻ���������
    { 3, 34,  1,  17 },  // �ͻ�����ʧ��  00--��������0--ʧ��
    
    { 0,  0,  0,  0 }   // ����
};

//GetAlarm		CID2 = 44
LOCAL SMACDATASTRUCT s_Smac44Alarm[] =
{   
    // 00����/01��������/02��������
    // 20δ��/F0����
    { 3,  0,  1,  0 },  // ���A���ѹ�澯״̬
    { 3,  2,  1,  1 },  // ���B���ѹ�澯״̬
    { 3,  4,  1,  2 },  // ���C���ѹ�澯״̬
    { 3,  6,  1,  3 },  // ���A������澯״̬
    { 3,  8,  1,  4 },  // ���B������澯״̬
    { 3, 10,  1,  5 },  // ���C������澯״̬

    { 3, 12,  1,  6 },  // ���AB�ߵ�ѹ�澯״̬
    { 3, 14,  1,  7 },  // ���BC�ߵ�ѹ�澯״̬
    { 3, 16,  1,  8 },  // ���CA�ߵ�ѹ�澯״̬
	
    { 3, 18,  1,  9 },  // ���й����ʸ澯״̬
    { 3, 20,  1, 10 },  // ���޹����ʸ澯״̬
    { 3, 22,  1, 11 },  // �ܹ��������澯״̬
    { 3, 24,  1, 12 },  // ����ƽ��THD�澯״̬
    { 3, 26,  1, 13 },  // ��ѹƽ��THD�澯״̬
    { 3, 28,  1, 14 },  // Ƶ�ʸ澯״̬
    { 3, 30,  1, 15 },  // �����¶ȸ澯״̬
    { 3, 32,  1, 16 },  // ��ص�ѹ�澯״̬
    { 3, 34,  1, 17 },  // ��ˮ�¸澯״̬
    { 3, 36,  1, 18 },  // ����ѹ�澯״̬
    { 3, 38,  1, 19 },  // ��ȼ��λ�澯״̬
    { 3, 40,  1, 20 },  // �ͻ����ϸ澯״̬
    
    { 0,  0,  0,  0 }   // ����
};


//struct _SMACDATASTRUCT
//{
//	BYTE nType;     // ��������:0-������־,1-4BYTES(�޷��ţ�,2-4 bytes���з��ţ� 3-2Bytes
//	int  nOffset;   // ������ʼ��ַ���ڱ����ݰ��е���ʼλ�ô�ͷ�ַ���ʼ��0��
//	int  nScaleBit; // ���ڿ�����,��λ�ţ�ģ������������ϵ,��10���ȡ�
//	int  nChanel;   // ϵͳ�����ͨ���ţ�����ģ��Ĳ���������ƫ�Ʊ�ʾ��    
//};
//typedef struct _SMACDATASTRUCT  SMACDATASTRUCT;

// Get sys parameter	CID2 = 47
LOCAL SMACDATASTRUCT	s_Smac47sys[] =
{
    { 3, 0, 1,  0 },  // �澯�ز�
    { 3, 2, 1,  1 },  // ���߷�ʽ
    { 3, 4, 1,  2 },  // �ͻ��������ʱ��
    
    { 0,  0,  0,  0 }   // ����
};

LOCAL SMACDATASTRUCT s_Smac46AnologParameter[] =
{
    { 1,   0, 1,  0 },  // ���������A����
    { 1,   8, 1,  1 },  // ���������B����
    { 1,  16, 1,  2 },  // ���������C����
    { 1,  24, 1,  3 },  // ���������A����
    { 1,  32, 1,  4 },  // ���������B����
    { 1,  40, 1,  5 },  // ���������C����
    { 1,  48, 1,  6 },  // ������ѹA����
    { 1,  56, 1,  7 },  // ������ѹB����
    { 1,  64, 1,  8 },  // ������ѹC����
    { 1,  72, 1,  9 },  // ������ѹA����
    { 1,  80, 1, 10 },  // ������ѹB����
    { 1,  88, 1, 11 },  // ������ѹC����
    { 1,  96, 1, 12 },  // ������ѹAB����
    { 1, 104, 1, 13 },  // ������ѹBC����
    { 1, 112, 1, 14 },  // ������ѹCA����
    { 1, 120, 1, 15 },  // ������ѹAB����
    { 1, 128, 1, 16 },  // ������ѹBC����
    { 1, 136, 1, 17 },  // ������ѹCA����
    { 1, 144, 1, 18 },  // Ƶ������
    { 1, 152, 1, 19 },  // Ƶ������
    { 1, 160, 1, 20 },  // ���й�����
    { 1, 168, 1, 21 },  // ���޹�����
    { 1, 176, 1, 22 },  // �ܹ�����������
    { 1, 184, 1, 23 },  // ����ƽ��THD����
    { 1, 192, 1, 24 },  // ��ѹƽ��THD����
    { 1, 200, 1, 25 },  // �¶�����
    { 1, 208, 1, 26 },  // �¶�����
    { 1, 216, 1, 27 },  // ��ص�ѹ����
    { 1, 224, 1, 28 },  // ��ص�ѹ����
    { 1, 232, 1, 29 },  // CT���
    
    { 0,  0,  0,  0 }   // ����
};

//CID2=0X45
STRUCTCMD		s_SmacCtrlCmd[] =
{
    //BPY 2004.12.23     
    //��������																					CID2=0X45
    { 0x01, 1 },   //  0-�ͻ��ػ�
    { 0x0E, 1 },   //  1-�ͻ����Կ���
    { 0x0F, 1 },   //  2-�ͻ���������
    { 0x02, 1 },   //  3-��2·�̵���
    { 0x03, 1 },   //  4-��3·�̵���
    { 0x04, 1 },   //  5-��4·�̵���
    //�����ۼ�ֵ��λ CID2=0XE9
    { 0x5A, 0xA5 },   //  6-�����ۼ�ֵ��λ
	//�ͻ�����ֹͣԭ��״̬��λ CID2=0XE6	//BPY 2005.04.12
    { 0x5A, 0xA5 },   //  7-�ͻ�����ֹͣԭ��״̬��λ
	
    //ϵͳ��������																				CID2=0X49
    { 0x80, 1 },   //  10-�澯�ز�
    { 0x81, 1 },   //  11-���߷�ʽ
    { 0x82, 1 },   //  12-�ͻ��������ʱ��

    //ģ������������																			CID2=0X48
    { 0x80, 1 },   //  20-���������A����
    { 0x81, 1 },   //  21-���������B����
    { 0x82, 1 },   //  22-���������C����
    { 0x83, 1 },   //  23-���������A����
    { 0x84, 1 },   //  24-���������B����
    { 0x85, 1 },   //  25-���������C����
    { 0x86, 1 },   //  26-������ѹA����
    { 0x87, 1 },   //  27-������ѹB����
    { 0x88, 1 },   //  28-������ѹC����
    { 0x89, 1 },   //  29-������ѹA����
    { 0x8A, 1 },   //  30-������ѹB����
    { 0x8B, 1 },   //  31-������ѹC����
    { 0x8C, 1 },   //  32-������ѹAB����
    { 0x8D, 1 },   //  33-������ѹBC����
    { 0x8E, 1 },   //  34-������ѹCA����
    { 0x8F, 1 },   //  35-������ѹAB����
    { 0x90, 1 },   //  36-������ѹBC����
    { 0x91, 1 },   //  37-������ѹCA����
    { 0x92, 1 },   //  38-Ƶ������
    { 0x93, 1 },   //  39-Ƶ������
    { 0x94, 1 },   //  40-���й�����
    { 0x95, 1 },   //  41-���޹�����
    { 0x96, 1 },   //  42-�ܹ�����������
    { 0x97, 1 },   //  43-����ƽ��THD����
    { 0x98, 1 },   //  44-��ѹƽ��THD����
    { 0x99, 1 },   //  45-�¶�����
    { 0x9A, 1 },   //  46-�¶�����
    { 0x9B, 1 },   //  47-��ص�ѹ����
    { 0x9C, 1 },   //  48-��ص�ѹ����
    { 0x9D, 1 },   //  49-CT���

    { 0,  0   }    // ����
};

/*=============================================================================*
 * FUNCTION: SMACGetEquipAddr
 * PURPOSE : get  smac address  by iChannelNo
 * INPUT:	 RS485_DEVICE_CLASS*  SmacDeviceClass, INT32 iChannelNo
 *     
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMACGetEquipAddr(RS485_DEVICE_CLASS*  SmacDeviceClass, INT32 iChannelNo)
{
	INT32 iAddr;
	INT32 iStartPosition;

	if ((SMAC_CH_START <= iChannelNo) && (iChannelNo <= SMAC_CH_FIRST_END))
	{
		iAddr = SmacDeviceClass->pRoughData[SMAC_HAVE_SMAC_ADDR].iValue;
		return iAddr;
	}
	else if((iChannelNo <= SMAC_CH_END) && (iChannelNo > SMAC_CH_FIRST_END))
	{
		iStartPosition = 1 * SMAC_MAX_SIGNALS_NUM;
		iAddr = SmacDeviceClass->pRoughData[iStartPosition + SMAC_HAVE_SMAC_ADDR].iValue;
		return iAddr;
	}
	else
	{
		AppLogOut("SmacGet EquipAddr", 
					APP_LOG_UNUSED, 
					"[%s]-[%d] ERROR: failed to  Smac address is error \n\r",
					__FILE__, __LINE__);
		return -1;
	}
}


/*=============================================================================*
 * FUNCTION: FloatToByte
 * PURPOSE : Added by HULONGWEN, 032214
 * INPUT:	 float fVal, BYTE* pTemp
 *     
 *
 * RETURN:
 *     LOCAL BYTE
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
LOCAL BYTE* FloatToByte(float fVal, BYTE* pTemp)
{
	int i = 0;
	char *pFloat;

	pFloat = (char*)&fVal;
	
	for(i=0; i<4; i++)
	{
#ifdef _CPU_BIG_ENDIAN		// ppc
		pTemp[i] = *(pFloat+3-i);
#else	// for x86
		pTemp[i] = *(pFloat+i);
#endif

	}
	return pTemp;
}

/*=============================================================================*
 * FUNCTION: CheckSum()
 * PURPOSE : Cumulative  result
 * INPUT: 
 *     OUT CHAR*	Frame
 *	   
 * RETURN:
 *     static void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     copy from SMAC        DATE: 2009-05-08		14:39
 *
 *============================================================================*/
static void CheckSum( BYTE *Frame )
{
	INT32	i;    
	WORD	R	 = 0;
    INT32   nLen = (int)strlen( (char*)Frame );  

    for ( i = 1; i < nLen; i++ )
	{
        R += *(Frame + i);
	}

    sprintf( (char *)Frame + nLen, "%04X\r", (WORD) - R );
}

/*=============================================================================*
 * FUNCTION: RecvDataFromSMAC()
 * PURPOSE  :receive data of SM from 485 com
 * RETURN   : int : byte number ,but if return -1  means  error
 * ARGUMENTS:
 *						CHAR*	sRecStr :		
 *								hComm	 :	485 com handle 
 *								iStrLen	 :
 * CALLS    : 
 * CALLED BY: 
 *								DLLExport BOOL Query()
 * COMMENTS : 
 * CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
 *==========================================================================*/
LOCAL INT32	RecvDataFromSMAC(HANDLE hComm, CHAR* sRecStr,  INT32 *piStrLen,INT32 iWantToReadnByte)
{
		INT32	iReadLen;		
		INT32	i		= 0;
		INT32	j		= 0;
		INT32	iHead	= -1;
		INT32	iTail	= -1;

	    ASSERT(hComm != 0 );
		ASSERT(sRecStr != NULL);

		//Read the data from Smac
		iReadLen = RS485_Read(hComm, (BYTE*)sRecStr, iWantToReadnByte);
		
		for (i = 0; i < iReadLen; i++)
		{
			if(bySOI == (BYTE)(*(sRecStr + i)))
			{
				iHead = i;
				break;
			}
		}

		if (iHead < 0)	//   no head
		{
			TRACE(" AC  AC  AC  RRR (iHead < 0)no head iReadLen =%d %s \n",(int)iReadLen,sRecStr);
			return FALSE;
		}
		
		for (i = iHead + 1; i < iReadLen; i++)
		{
			if(byEOI == *(sRecStr + i))
			{
				iTail = i;
				break;
			}
		}		
		
		if (iTail < 0)	//   no tail
		{
			TRACE(" AC  AC  AC  RRR (iTail < 0)no tail %d %s \n",(int)iReadLen,sRecStr);
			return FALSE;
		}	

		*piStrLen = iTail - iHead + 1;
		
		if(iHead > 0)
		{
			for (j = iHead; j < *piStrLen; j++)
			{
				*(sRecStr + (j - iHead)) = *(sRecStr + j);
			}
		}

		*(sRecStr + (iTail - iHead + 2)) = '\0';
		return TRUE;

}

/*=============================================================================*
 * FUNCTION:	RS485WaitReadable
 * PURPOSE  :	wait RS485 data ready
 * RETURN   :	int : 1: data ready, 0: timeout,Allow get mode or auto config
 * ARGUMENTS:
 *						int fd	: 
 *						int TimeOut: seconds 
 *			
 * CALLS    : 
 * CALLED BY: 
 *								
 * COMMENTS : 
 * CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
 *==========================================================================*/
LOCAL INT32 RS485WaitReadable(INT32 fd, INT32 nmsTimeOut)
{
    fd_set readfd;
    struct timeval timeout;

	while (nmsTimeOut > 0)
	{
		// need add ClearWDT() here.
		if (nmsTimeOut < (5*1000))
		{
			timeout.tv_usec = nmsTimeOut%1000*1000;			/* usec     */
			timeout.tv_sec  = nmsTimeOut/1000;				/* seconds  */

			nmsTimeOut = 0;
		}
		else
		{
			timeout.tv_usec = 0;							/* usec     */
			timeout.tv_sec  = 5;							/* seconds  */

			RUN_THREAD_HEARTBEAT();

			nmsTimeOut -= (5*1000);
		}

	}

	FD_ZERO(&readfd);										/*  Initializes the set to the NULL set. */
	FD_SET(fd, &readfd);									/*  Adds descriptor s to set. */

	select(fd+1, &readfd, NULL, NULL, &timeout);
	
	if(FD_ISSET(fd, &readfd))	
	{
		return TRUE;										//1: data ready, can be read
	}
	else
	{
		return FALSE;										//0: Data not ready, can't read!!
	}
}



/*=============================================================================*
* FUNCTION: CheckStrSum
* PURPOSE : Check receive data sum 
* INPUT:	CHAR *abyRcvBuf, INT32 RecTotalLength
*     
*
* RETURN:
*			LOCAL  INT32   1:  succeed	0:  error  
*		
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*
*============================================================================*/
LOCAL INT32  CheckStrSum(CHAR *abyRcvBuf, INT32 RecTotalLength)
{
	CHAR	 szCheckCode[5]	= {0};
	//copy receive checkSum to szCheckCode 
	strncpy(szCheckCode, (CHAR*)abyRcvBuf + RecTotalLength - SMAC_SHECKSUM_EOI,4); 
	abyRcvBuf[RecTotalLength - SMAC_SHECKSUM_EOI] = 0;
	//repeat check sum
	CheckSum((BYTE*)abyRcvBuf);

	if((abyRcvBuf[RecTotalLength - 2]		== szCheckCode[3])  //fourth byte of check sum
		&& (abyRcvBuf[RecTotalLength - 3]	== szCheckCode[2])	//third	 byte of check sum
		&&(abyRcvBuf[RecTotalLength - 4]	== szCheckCode[1])	//second byte of check sum
		&& (abyRcvBuf[RecTotalLength - 5]	== szCheckCode[0]))	//first  byte of check sum
	{ 
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
/*=============================================================================*
* FUNCTION: CheckStrLength
* PURPOSE : Check receive data length 
* INPUT:	BYTE *abyRcvBuf, INT32 RecBufOffsetNum, INT32 RecDataLength
*     
*
* RETURN:
*     INT32   1:  succeed	0:  error  
*		
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*
*============================================================================*/
LOCAL INT32 CheckStrLength(BYTE *abyRcvBuf, INT32 RecBufOffsetNum, INT32 RecDataLength)
{
	WORD		wLength;
	CHAR		c_CheckLength[5]	= {0};
	INT32		OffsetToLength		= RecBufOffsetNum;
	
	//repeat  length check
	wLength							= MakeDataLen(RecDataLength);
	sprintf((char *)c_CheckLength,"%04X\0",wLength);

	if((abyRcvBuf[OffsetToLength + SMAC_LENGTH_OFFSET_THREE]	!= c_CheckLength[3]) 
		&& (abyRcvBuf[OffsetToLength + SMAC_LENGTH_OFFSET_TWO]	!= c_CheckLength[2]) 
		&&(abyRcvBuf[OffsetToLength + SMAC_LENGTH_OFFSET_ONE]	!= c_CheckLength[1]) 
		&& (abyRcvBuf[OffsetToLength]							!= c_CheckLength[0]))
	{
		return FALSE;
	}

	return TRUE;
}
/*=============================================================================*
 * FUNCTION: SMACGetResponeData
 * PURPOSE : Send and receive data info
 * INPUT: 
 *     
 *
 * RETURN:
 *     INT32   0:  succeed	1: response error  
 *		
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32
 *copy from smio
 *============================================================================*/
LOCAL INT32 SMACSendAndRead(
                   HANDLE	hComm,			
                   BYTE		*sSendStr,		
                   BYTE		*abyRcvBuf,      
                   INT32	nSend,
		   int		cWaitTime,
		   int	iWantToReadnByte)					
{
		UNUSED(cWaitTime);
		INT32		iReceiveNum;
		INT32		iDataInfoLength;
		RS485_DRV	*pPort				= (RS485_DRV *)hComm;
		INT32		fd					= pPort->fdSerial; 
		ASSERT( hComm != 0 && sSendStr != NULL && abyRcvBuf != NULL);

		RS485_ClrBuffer(hComm);
		
		//printf("	SMAC Send DATA=%s \n",sSendStr);

		if(RS485_Write(hComm, sSendStr, nSend) != nSend)
		{
			//note	app log	!!
			AppLogOut("SMAC SendAndRead",
					APP_LOG_UNUSED, 
					"[%s]-[%d] ERROR: failed to  RS485_Write \n\r",
					__FILE__, __LINE__);

			TRACE("\n*****   !!!!! SMAC  RS485_Write   ******\n");

			return NO_RESPONSE_BEXISTENCE;
		}

		if (RS485WaitReadable(fd, 1000)  > 0 )
		{
			if(RecvDataFromSMAC(hComm, (CHAR*)abyRcvBuf, &iReceiveNum, iWantToReadnByte))
			{
				//check cid1
				if( abyRcvBuf[5] != sSendStr[5] || abyRcvBuf[6] != sSendStr[6] )
				{
					TRACE("\n*****SMAC  check cid1  error ******\n");
					return NO_RESPONSE_BEXISTENCE;
				}
				//check equip addr 
				else if( abyRcvBuf[3] != sSendStr[3] || abyRcvBuf[4] != sSendStr[4] )
				{
					TRACE("\n*****SMAC  check equip addr  error ******\n");
					return NO_RESPONSE_BEXISTENCE;
				}
				//check RTN
				else  if( abyRcvBuf[7] != '0' || abyRcvBuf[8] != '0' )
				{
					TRACE("\n*****SMAC check RTN  error ******\n");
					return NO_RESPONSE_BEXISTENCE;
				}
				else
				{
					//TOTALNumb - 5 - 13 = DATAINFO
					iDataInfoLength = iReceiveNum - SMAC_SHECKSUM_EOI - DATA_YDN23_SMAC;

					//	check  strlen 
					if (!CheckStrLength(abyRcvBuf, LENGTH_YDN23_SMAC, iDataInfoLength))
					{
						TRACE("\n*****SMAC  check StrLength error ******\n");
						return NO_RESPONSE_BEXISTENCE;
					}

					if(CheckStrSum((CHAR*)abyRcvBuf, iReceiveNum))
					{
						return RESPONSE_OK;

					}
					else
					{
						TRACE("\n*****SMAC  check sum error ******\n");
						return NO_RESPONSE_BEXISTENCE;
					}
				}
			}
			else
			{
				//RS485_ClrBuffer(hComm);
				TRACE("\n*****SMAC  else  read 0 ******\n");
				return NO_RESPONSE_BEXISTENCE;
			}
		}
		else
		{
			TRACE("\n*****SMAC  RS485WaitReadable ******\n");
			return NO_RESPONSE_BEXISTENCE;
		}
		
}
/*=============================================================================*
 * FUNCTION: SMACSendControlCmdxx
 * PURPOSE : Send to smac control command
 * INPUT: 
 *    
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *	   copy
 *
 *============================================================================*/
BOOL SMACSendControlCmdxx(
						HANDLE hComm,       // ͨѶ�ھ��
						int nUnitNo,        // �ɼ�����Ԫ��ַ
						int nCmdNo,         // �����       //-->ͨ����(0->14ͨ��,1->15ͨ��,2->16ͨ��,) Lintao���¶����ʽ  BPY 2005.01.06
						int nInvalid,       // ����ͨ����   //������Ч��Ч(1-��Ч��0����Ч)
						int nControlType,   // ��������     //ֻ��4�����0-��͵�ƽ��1-��ߵ�ƽ��2-�����壻3-������
						float fSetAnalogVal	//����ֵ
						)
{
    WORD	wLength; 
    INT32	nSendLen	= 0;
    //INT32	CmdType		= 0;
    //CHAR	szVer[5]	= {0};
    BYTE	Frame[MAX_RECVBUFFER_NUM_SMAC]	= {0};    
    BYTE	sCmdStr[40]	= {0};

    if( nCmdNo < 0 || nCmdNo > 50 )
    {
        return FALSE;
    }

	if( nCmdNo >= 0 && nCmdNo <= 5 )   // Remote Control
	{
		 wLength = MakeDataLen( 6 );

        if((INT32)fSetAnalogVal > 2600)
		{
            fSetAnalogVal = 2600;
		}
        if((INT32)fSetAnalogVal < 100)
		{
            fSetAnalogVal = 100;
		}
    
        if(nCmdNo==0 )
        {
            nCmdNo = 0x01;      //BPY 2005.03.11 CHANGED 00->01
        }
        else if(nCmdNo == 1 )
        {
            nCmdNo = 0x0E;
        }
        else if(nCmdNo == 2 )
        {
            nCmdNo = 0x0F;
        }
        else if((nCmdNo == 3) || (nCmdNo == 4) || (nCmdNo == 5))
        {
            nCmdNo = nCmdNo - 1;
        }
		//nControlType      ////ֻ��4�����0-��͵�ƽ(10H)��1-��ߵ�ƽ(11H)��2-������(12H)��3-������(13H)
		//ex��0,        1,      2,      2000    ���ͣ���һ��Relay�������壬���Ϊ2s
		//    channel   Valid   Type    pulse
		//ע�����nInvalidΪ1, nControlType������ֵ��
		//    ���nInvalidΪ0, nControlType��ֵ0,1������2��3����
        if(nInvalid == 0x01)
        {
                nControlType = nControlType + 0x10;				
        }
        else if(nInvalid == 0x00)
        {
            if((nControlType == 0x00) || (nControlType == 0x02))
            {
                nControlType = nControlType + 0x11;				
            }
            else if((nControlType == 0x01) || (nControlType == 0x03))
            {
                nControlType = nControlType + 0x0F;				
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
	    sprintf((CHAR *)sCmdStr,
				"~%s%02X%02X%02X%04X%02X%02X%02X\0",
				SMACszVer, 
				nUnitNo,
				SMAC_CID1,
				0x45, 
				wLength,
				nCmdNo,
				nControlType, 
				(unsigned int)fSetAnalogVal/100 );
		
		nSendLen = 6 + 18;  

	}
    // Energy Reset
    else if(nCmdNo == 6)   
    {
        wLength = MakeDataLen( 4 );      // ���㳤���ַ���
        sprintf((char *)sCmdStr,
				"~%s%02X%02X%02X%04X%02X%02X\0",
				SMACszVer,
				nUnitNo, 
				SMAC_CID1, 
				0xE9, 
				wLength, 
				s_SmacCtrlCmd[nCmdNo].nCmdType, 
				s_SmacCtrlCmd[nCmdNo].nScale);
        nSendLen = 4 + 18; // 
    }
    //BPY 2005.04.12 ADDED
    else if(nCmdNo == 7)   // Diesel test stop reason reset
    {
        wLength = MakeDataLen( 4 );      
        sprintf((char *)sCmdStr,
			"~%s%02X%02X%02X%04X%02X%02X\0",
            SMACszVer,
			nUnitNo,
			SMAC_CID1,
			0xE6, 
			wLength, 
			s_SmacCtrlCmd[nCmdNo].nCmdType,
			s_SmacCtrlCmd[nCmdNo].nScale); 
        nSendLen = 4 + 18;  
    }
	// System Para
    else if((nCmdNo >= 10) && (nCmdNo <= 12))   
    {
        wLength = MakeDataLen(4); 
	sprintf((char *)sCmdStr,
		"~%s%02X%02X%02X%04X%02X%02X\0",
		SMACszVer,
		nUnitNo, 
		SMAC_CID1,
		0x49, 
		wLength,
		s_SmacCtrlCmd[nCmdNo-2].nCmdType,
		(unsigned int)fSetAnalogVal);
        nSendLen = 4 + 18; 
    }
	// Analog Para
    else if((nCmdNo >= 20) && (nCmdNo < 50))   
    {     
        //CHAR		*pFloat;
        //INT32		i;
        BYTE		temp[5]	= {0};
		wLength				= MakeDataLen(10);  

	float fVal = (fSetAnalogVal*s_SmacCtrlCmd[nCmdNo-9].nScale);

	//Modified by HULONGWEN begin, 032214
	FloatToByte(fVal, temp);
        sprintf((char *)sCmdStr,
			"~%s%02X%02X%02X%04X%02X%02X%02X%02X%02X\0",
			SMACszVer,
			nUnitNo, 
			SMAC_CID1, 
			0x48,
			wLength,
			s_SmacCtrlCmd[nCmdNo-9].nCmdType,
			temp[0],
			temp[1],
			temp[2], 
			temp[3] );
        nSendLen = 10 + 18; 
    }

	CheckSum(sCmdStr);

	if (SMACNeedCommProc())
	{
		SMACReopenPort();
	}

	//RS485_ClrBuffer(hComm);
	SMACSendAndRead(hComm, sCmdStr, Frame, nSendLen, RS485_WAIT_VERY_SHORT_TIME, 18);

	return TRUE;
}


/*=============================================================================*
 * FUNCTION: SMAC_SendCtlCmd
 * PURPOSE : Send command to smac
 * INPUT: 
 *     BOOL bDisableDog  // TRUE:disable the dog��FALSE:enable the dog
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
VOID SMAC_SendCtlCmd(RS485_DEVICE_CLASS*  SmacDeviceClass,
					 INT32 iChannelNo, 
					 float fParam,
					 char *pString)	
{
	UNUSED(fParam);
	CHAR		sTarget[128] = {0};
	INT32		nPoint1		 = 0;
	INT32		param1		 = 0;
	INT32		nChanel		 = 0;
	INT32		nPoint		 = 0;
	INT32		nCmdNo		 = 0;
	INT32		iUnitNo;
	float		param2		 = 0.0f;

	if (RS485_RUN_MODE_SLAVE == g_RS485Data.enumRunningMode)
	{
		return;
	}

	if((iChannelNo < SMAC_CH_START) || (SMAC_CH_END < iChannelNo))
	{
		return ;//No SMBac
	}

	iUnitNo = SMAC_ADDR_START;
//#if  TST_RS485_DEBUG_SMAC
//	TRACE("\n ######## This is  iChannelNo = %d	\n",iChannelNo);
//	TRACE("\n ######## This is  SMAC_ADDR_START = %d	\n",iUnitNo);
//#endif
	//Get nCmdNo
	nPoint = RS485_StrExtract( pString, sTarget, ',');
	nCmdNo = (int)atoi(pString);
	nCmdNo -= 300;
	nPoint1 += nPoint;

	if(nCmdNo >= 0 && nCmdNo <= 5)  //ǰ��̵������ƣ�����Ϊ0,1,2,2000��ʽ
	{
		//ͨ����
		if( nPoint>0 )
		{
			nPoint = RS485_StrExtract( pString + nPoint1, sTarget, ',' );
			
			if( sTarget[0] == 0 )       //BPY 2005.01.06
			{
				return;
			}

			nChanel = atoi( sTarget );
		}
		nPoint1 += nPoint;

		//����1
		if( nPoint>0 )
		{
			nPoint = RS485_StrExtract(pString + nPoint1, sTarget, ',' );
			if( sTarget[0] == 0 )    //BPY 2005.01.06
			{
				return;
			}

			param1 = atoi( sTarget );
		}

		nPoint1 += nPoint;
		//����2
		if( nPoint>0 )
		{
			nPoint = RS485_StrExtract(pString + nPoint1, sTarget, ',' );
			if( sTarget[0] == 0 )    //BPY 2005.01.06
			{
				return;
			}

			param2 = (int)atof( sTarget );
		}
		else
		{
			param2 = (int)0.0f;
		}
	}
	else if(nCmdNo == 6)
	{
		//
	}
	else if(nCmdNo >= 10)   //���ò�������
	{
		//����2
		if( nPoint>0 )
		{
			nPoint = RS485_StrExtract(pString + nPoint1, sTarget, ',' );
			/*      if( sTarget[0] == 0 )    //BPY 2005.01.06
			{
			return FALSE;
			}
			*/
			param2 = atof(sTarget);
		}
		else
		{
			param2 = 0.0f;
		}

	}

	SMACSendControlCmdxx(SmacDeviceClass->pCommPort->hCommPort,
						iUnitNo,
						nCmdNo,
						nChanel,
						param1,
						param2);

	//return SetDev( hComm, nUnitNo, nCmdNo, nChanel, param1, param2 );


	//INT32	iUnitNo;
	//INT32   nPointStr;
	//INT32	nPoint					= 0;
	//INT32	nPoint1					= 0;
	//INT32	iCtrlInvalidChannelNo	= 0;
	//INT32   CtrlType				= 0;		//param1
	//float	iPulseTime				= 0.0f;;	//param2
	//BYTE	sTarget[128]			= {0};

	//if(iChannelNo <= SMAC_CH_START || SMAC_CH_END < iChannelNo)
	//{
	//	return ;//no SMBac
	//}

	//iUnitNo = SMACGetEquipAddr(SmacDeviceClass, iChannelNo);
	//iUnitNo = SMAC_ADDR_START;//only 104 have Diesel can control
	//if( iChannelNo >= SMAC_CH_START && iChannelNo <= SMAC_CH_FIRST_END)
	//{
	//	iChannelNo = iChannelNo - SMAC_CH_START;
	//}
	//else
	//{
	//	iChannelNo = iChannelNo - (SMAC_CH_FIRST_END + 1);
	//}

	//if(iChannelNo >= 0 && iChannelNo <= 5)
	//{
	//		//nChanel = atoi( sTarget );
	//		iCtrlInvalidChannelNo = (INT32)fParam;
	//		nPoint = RS485_StrExtract( pString, sTarget, ',');
	//		//param	1
	//		if( nPoint>0 )
	//		{
	//			nPoint = RS485_StrExtract(pString, sTarget, ',' );
	//			//BPY 2005.01.06
	//			if(sTarget[0] == 0)    
	//			{
	//				return FALSE;
	//			}
	//			//param1 = atoi( sTarget )
	//			CtrlType = atoi( sTarget );;
	//		}

	//		nPoint1 += nPoint;

	//		//param	2
	//		if( nPoint>0 )
	//		{
	//			nPoint = RS485_StrExtract( pString+nPoint1, sTarget, ',' );
	//			//BPY 2005.01.06
	//			if( sTarget[0] == 0 )   
	//			{
	//				return FALSE;
	//			}
	//			//param2 = atoi( sTarget );
	//			iPulseTime = (int)atof( sTarget );
	//		}
	//		else
	//		{
	//			iPulseTime = (INT32)0.0f;
	//		}
	//}
	//else  if(iChannelNo >= 10)   
	//{
	//	 iPulseTime = (INT32)fParam;
	//}

	//SMACSendControlCmdxx(SmacDeviceClass->pCommPort->hCommPort,
	//					iUnitNo,
	//					iChannelNo,
	//					iCtrlInvalidChannelNo,
	//					CtrlType,
	//					iPulseTime);
}

/*=============================================================================*
 * FUNCTION: SMACGetVer
 * PURPOSE : Get version  for scan smac equip
 * INPUT: 
 *    
 *
 * RETURN:
 *		szVer   OUT
 *		INT32   0:  succeed	 1:no response working 
 *			
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *  
 *				
 *============================================================================*/
LOCAL INT32 SMACGetVer( HANDLE hComm, 
				   INT32 nUnitNo,
				   int cCID1,
				   int cCID2, 
				   BYTE*szVer)
{
	BYTE	szSendStr[20]		= {0};
	CHAR	abyRcvBuf[MAX_RECVBUFFER_NUM_SMAC]		= {0};
	INT32	iSmacStatus			= SMAC_SAMP_INVALID_VALUE;
	sprintf( (char *)szSendStr, 
			"~%02X%02X%02X%02X%04X\0",
			0x20, 
			(unsigned int)nUnitNo,
			cCID1, 
			cCID2, 
			0 );
	CheckSum(szSendStr);
	//1	  1	  1	  1	   1        2	   X	2   1
	//1	  2	  2	  2	   2	    4	   2X	4   1
	//SOI VER ADR CID1 CID2/RTN	LENGTH INFO	SUM	EOI
	//*(szSendStr + 17) =  byEOI;
	iSmacStatus = SMACSendAndRead( hComm, szSendStr, ( BYTE*)abyRcvBuf, 18, RS485_WAIT_LONG_TIME, 512);
	
	if(iSmacStatus == RESPONSE_OK)
	{
		strncpy((char*)szVer, (CHAR*)(abyRcvBuf + 1), 2);
	}
  
	return  iSmacStatus;
}
/*=============================================================================*
* FUNCTION: SMAC_SetParam(RS485_DEVICE_CLASS* pDevice)
* PURPOSE : Ensure the parameters equal to sampling data in rough data
* INPUT:	 pDevice	
*   
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*	  add 2010/12/31
*============================================================================*/
INT32 SMAC_SetParam(RS485_DEVICE_CLASS* pDevice)
{
	RS485_DEVICE_CLASS*  SmacDeviceClass		= pDevice;
	INT32	 iCurrentSmpMaxTime, iMaxTimeFromWeb;
	float	 fCurrentSmpVolt,	fVoltFromWeb;
	INT32	nSendLen	= 0;
    WORD	wLength; 
	BYTE	sCmdStr[40]	= {0};
	BYTE	Frame[MAX_RECVBUFFER_NUM_SMAC]	= {0}; 
	static const INT32 iAddr = 104; 

	static const ST_SMAC_PARAM st_SmacParamUnify[2]=
	{
		/*About the nCmdNoIdx refer to function SMAC_SendCtlCmd()*/
		{261,2,1,12},//Max duration for Diesel Test
		{262,2,1,48} //Battery Voltage Limit
	};


	if (SMAC_EQUIP_EXISTENT != SmacDeviceClass->pRoughData[SMAC_DISEL_EXIST_STAT].iValue)
	{
		return 0;
	}
	else
	{
		//Only address 104 have diesel 
		iCurrentSmpMaxTime = SmacDeviceClass->pRoughData[SMAC_GETSYS_TEST_MAX_TIME].iValue;
		fCurrentSmpVolt	   = SmacDeviceClass->pRoughData[SMAC_GETANLG_VOLTAGE_LOWER].fValue;
		iMaxTimeFromWeb	   = GetDwordSigValue(st_SmacParamUnify[0].Sig_EquipId,
											  st_SmacParamUnify[0].Sig_Type,
											  st_SmacParamUnify[0].Sig_Id,
											  "RS485_SMAC");
		fVoltFromWeb	   = GetFloatSigValue(st_SmacParamUnify[1].Sig_EquipId, 
											  st_SmacParamUnify[1].Sig_Type,
											  st_SmacParamUnify[1].Sig_Id,
											  "RS485_SMAC");

		TRACE("	#########	SMAC Param Unify Sig MaxTime =%d  Volt =%f ########\n",(int)iMaxTimeFromWeb, fVoltFromWeb);

		//1 .First Sig process
		if(iCurrentSmpMaxTime != iMaxTimeFromWeb)
		{
			TRACE("\n	#########	SMAC Param Unify Sig MaxTime ########\n");
			SmacDeviceClass->pRoughData[SMAC_GETSYS_TEST_MAX_TIME].iValue = iMaxTimeFromWeb;

			memset(sCmdStr, 0, strlen((char*)sCmdStr));	

			wLength = MakeDataLen(4); 
			sprintf((char *)sCmdStr,
				"~%s%02X%02X%02X%04X%02X%02X\0",
				SMACszVer,
				(unsigned int)iAddr, 
				SMAC_CID1,
				0x49, 
				wLength,
				s_SmacCtrlCmd[st_SmacParamUnify[0].SignCmdNoIdx - 2].nCmdType,
				(unsigned int)iMaxTimeFromWeb);        
			nSendLen = 4 + 18; 
			CheckSum(sCmdStr);

			if (SMACNeedCommProc())
			{
				SMACReopenPort();
			}

			SMACSendAndRead(SmacDeviceClass->pCommPort->hCommPort, 
							sCmdStr,
							Frame, 
							nSendLen,
							RS485_WAIT_VERY_SHORT_TIME,
							18);
		}
		else
		{
			//Needn't process!!
		}

		//2 .Second Sig process
		//if (fCurrentSmpVolt != fVoltFromWeb)
		if (FLOAT_NOT_EQUAL(fCurrentSmpVolt, fVoltFromWeb))
		{
			TRACE("\n	#########!!	SMAC Param Unify Sig Volt ########\n");
			SmacDeviceClass->pRoughData[SMAC_GETANLG_VOLTAGE_LOWER].fValue = fVoltFromWeb;
			memset(sCmdStr,0,strlen((char *)sCmdStr));	

			BYTE		temp[5]	= {0};
			wLength				= MakeDataLen(10);  

			float fVal = (fVoltFromWeb*s_SmacCtrlCmd[st_SmacParamUnify[1].SignCmdNoIdx - 9].nScale);

			//Modified by HULONGWEN begin, 032214
			FloatToByte(fVal, temp);

			sprintf((char *)sCmdStr,
				"~%s%02X%02X%02X%04X%02X%02X%02X%02X%02X\0",
				SMACszVer,
				(unsigned int)iAddr, 
				SMAC_CID1, 
				0x48,
				wLength,
				s_SmacCtrlCmd[st_SmacParamUnify[1].SignCmdNoIdx - 9].nCmdType,
				temp[0],
				temp[1],
				temp[2], 
				temp[3] );
			nSendLen = 10 + 18; 
			CheckSum(sCmdStr);

			if (SMACNeedCommProc())
			{
				SMACReopenPort();
			}

			SMACSendAndRead(SmacDeviceClass->pCommPort->hCommPort,
				sCmdStr, 
				Frame, 
				nSendLen,
				RS485_WAIT_VERY_SHORT_TIME,
				18);
		}
		else
		{
			//Needn't process!!
		}
		return 0;
	}
}

/*=============================================================================*
 * FUNCTION: SMAC_InitRoughValue
 * PURPOSE : Initialization smac RoughData
 * INPUT:	 g_aDeviceClass	
 *   
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMAC_InitRoughValue(void* pDevice)
{
	INT32				i;
	INT32				j;
	INT32				iRoughStartPosition;
	//pDevice  g_RS485Data.pDeviceClass + i
	RS485_DEVICE_CLASS*  SmacDeviceClass		= pDevice;
	SmacDeviceClass->pfnSample			= (DEVICE_CLASS_SAMPLE)SMACSample;		
	SmacDeviceClass->pfnStuffChn			= (DEVICE_CLASS_STUFF_CHN)SMAC_StuffChannel;
	SmacDeviceClass->pfnSendCmd			= (DEVICE_CLASS_SEND_CMD)SMAC_SendCtlCmd;
	//SmacDeviceClass->pfnParamUnify			= SMAC_ParamUnify;

	SmacDeviceClass->pRoughData					= (RS485_VALUE*)(&(g_SmacSampleData.aRoughDataSMAC[0][0]));						

	for(i = 0; i < SMAC_NUM; i++)//[0 ---- 19]
	{
		iRoughStartPosition = i * SMAC_MAX_SIGNALS_NUM;

		for(j = 0; j < SMAC_MAX_SIGNALS_NUM; j++)
		{		
			SmacDeviceClass->pRoughData[iRoughStartPosition + j].iValue 
				= SMAC_SAMP_INVALID_VALUE;//-9999
		}
		SmacDeviceClass->pRoughData[iRoughStartPosition + SMAC_INTERRUPT_TIMES].iValue
				= 0;
		SmacDeviceClass->pRoughData[iRoughStartPosition + SMAC_WORKING_STATUS].iValue
				= SMAC_EQUIP_NOT_EXISTENT;	
	}

	SmacDeviceClass->pRoughData[SMAC_DISEL_EXIST_STAT].iValue
				= SMAC_EQUIP_NOT_EXISTENT;	

	SmacDeviceClass->pRoughData[SMAC_GROUP_WORKING_STATUS].iValue
				= SMAC_EQUIP_NOT_EXISTENT;	
	SmacDeviceClass->bNeedReconfig = TRUE;
	return 0 ;
}
/*=============================================================================*
 * FUNCTION: SMACReconfig
 * PURPOSE : Scan  the all smac equip
 * INPUT:	 g_aDeviceClass	
 *     
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMACReconfig(void* pDevice)
{
	INT32					i;
	//INT32					j;
	INT32					iAddr;
	INT32					iExistAddrNum;
	INT32					iStartRoughPosition;
	INT32					iRoughSequenceeIdx;
	RS485_DEVICE_CLASS*		SmacDeviceClass		= pDevice;
	INT32					SmacStatus			= SMAC_SAMP_INVALID_VALUE;
	BYTE					cExistMaxAddr       = 0;
	iExistAddrNum								= 0;
	iRoughSequenceeIdx							= 0;

	for (iAddr = SMAC_ADDR_START; iAddr <= SMAC_ADDR_END; iAddr++)
	{
		for (i = 0; i < RS485_RECONNECT_TIMES; i++)
		{
			SmacStatus = SMACGetVer(SmacDeviceClass->pCommPort->hCommPort,
							iAddr,
							SMAC_CID1,
							SMAC_GETVER_CID2,
							SMACszVer);

			if (RESPONSE_OK == SmacStatus)
			{
				TRACE("\n  SMAC   SMAC Reconfig  iAddr = %d \n",(int)iAddr);
				cExistMaxAddr = g_RS485Data.stSMSmplingCtrl.cExistMaxAddr;
				g_RS485Data.stSMSmplingCtrl.cSAMPExistAddr[cExistMaxAddr] = iAddr;
				g_RS485Data.stSMSmplingCtrl.cExistMaxAddr++;

				iExistAddrNum++;
				if (iExistAddrNum > SMAC_NUM)
				{
					//���֧��4��SMAC,��ɨ��ĵ�ַȷ��104----108����ʵ��ֻ�����ĸ��ռ䣬����Ҫ���ж�
					return TRUE;
				}

				break;
			}
		}

		//104:	diesel  and smac addr  �ͻ����⴦��һ�£�������ߵĴ���
		if ((SMAC_ADDR_START == iAddr) && (RESPONSE_OK == SmacStatus))
		{
			SmacDeviceClass->pRoughData[SMAC_HAVE_DIESEL].iValue	= DIESEL_BHAVE;	
			SmacDeviceClass->pRoughData[SMAC_COMM_STATUS].iValue	= SMAC_COMM_OK;			
			SmacDeviceClass->pRoughData[SMAC_HAVE_SMAC_ADDR].iValue = SMAC_ADDR_START;
			SmacDeviceClass->pRoughData[SMAC_WORKING_STATUS].iValue = SMAC_EQUIP_EXISTENT;
			SmacDeviceClass->pRoughData[SMAC_DISEL_EXIST_STAT].iValue = SMAC_EQUIP_EXISTENT;
			SmacDeviceClass->pRoughData[SMAC_DISEL_COMM_STAT].iValue = SMAC_COMM_OK;
			iRoughSequenceeIdx++;
			continue;
		}
		
		if((SMAC_ADDR_START == iAddr) && (NO_RESPONSE_BEXISTENCE == SmacStatus))
		{
			//diesel not exist flag 
			SmacDeviceClass->pRoughData[SMAC_HAVE_DIESEL].iValue = DIESEL_NO_HAVE;	
		}

		if (RESPONSE_OK == SmacStatus)
		{
			iStartRoughPosition = iRoughSequenceeIdx * SMAC_MAX_SIGNALS_NUM;

			SmacDeviceClass->pRoughData[iStartRoughPosition + SMAC_COMM_STATUS].iValue 
				= SMAC_COMM_OK;
			SmacDeviceClass->pRoughData[iStartRoughPosition + SMAC_HAVE_SMAC_ADDR].iValue 
				= iAddr;
			SmacDeviceClass->pRoughData[iStartRoughPosition + SMAC_WORKING_STATUS].iValue 
				= SMAC_EQUIP_EXISTENT;

			iRoughSequenceeIdx++;
		}
	
		//105:  smac	 addr	 105
		//if ((SMAC_ADDR_105 == iAddr) && (RESPONSE_OK == SmacStatus))
		//{
		//	//there have diesel_104
		//	if (DIESEL_BHAVE == SmacDeviceClass->pRoughData[SMAC_HAVE_DIESEL].iValue)
		//	{	
		//		iStartRoughPosition = 1 * SMAC_MAX_SIGNALS_NUM;
		//		SmacDeviceClass->pRoughData[iStartRoughPosition + SMAC_COMM_STATUS].iValue 
		//			= SMAC_COMM_OK;
		//		SmacDeviceClass->pRoughData[iStartRoughPosition + SMAC_HAVE_SMAC_ADDR].iValue 
		//			= SMAC_ADDR_105;
		//		SmacDeviceClass->pRoughData[iStartRoughPosition + SMAC_WORKING_STATUS].iValue 
		//			= SMAC_EQUIP_EXISTENT;
		//		return 0;//max 2 smac,end scan
		//	}
		//	else //if(DIESEL_NO_HAVE == SmacDeviceClass->pRoughData[SMAC_HAVE_DIESEL].iValue)
		//	{
		//		//there only smac_105,less than 2 smac ,so continue scan
		//		SmacDeviceClass->pRoughData[SMAC_WORKING_STATUS].iValue = SMAC_EQUIP_EXISTENT;
		//		SmacDeviceClass->pRoughData[SMAC_COMM_STATUS].iValue = SMAC_COMM_OK;
		//		SmacDeviceClass->pRoughData[SMAC_HAVE_SMAC_ADDR].iValue = SMAC_ADDR_105;
		//	}
		//}
		//else //if((SMAC_ADDR_105 == iAddr) && (NO_RESPONSE_BEXISTENCE == SmacStatus))
		//{
		//	//here needn't process;
		//}

		////106:	if running here means less than 2 smac
		//if ((SMAC_ADDR_END == iAddr) && (RESPONSE_OK == SmacStatus))
		//{
		//	//diesel address must is 104 ,so diesel stuff first RoughData map of smac
		//	if (DIESEL_BHAVE == SmacDeviceClass->pRoughData[SMAC_HAVE_DIESEL].iValue)
		//	{
		//		iStartRoughPosition = 1 * SMAC_MAX_SIGNALS_NUM;
		//		SmacDeviceClass->pRoughData[iStartRoughPosition + SMAC_COMM_STATUS].iValue 
		//			= SMAC_COMM_OK;	
		//		SmacDeviceClass->pRoughData[iStartRoughPosition + SMAC_HAVE_SMAC_ADDR].iValue 
		//			= SMAC_ADDR_END;			
		//		SmacDeviceClass->pRoughData[iStartRoughPosition + SMAC_WORKING_STATUS].iValue 
		//			= SMAC_EQUIP_EXISTENT;
		//	}
		//	else
		//	{	/*
		//			if haven't diesel,There may be smac_105,
		//			if there have smac_105,so smac_106 stuff
		//			second RoughData map of smac 
		//			means 1 * SMAC_MAX_SIGNALS_NUM 
		//		*/
		//		if (SmacDeviceClass->pRoughData[SMAC_HAVE_SMAC_ADDR].iValue == SMAC_ADDR_105)
		//		{
		//			iStartRoughPosition = 1 * SMAC_MAX_SIGNALS_NUM;
		//			SmacDeviceClass->pRoughData[iStartRoughPosition + SMAC_COMM_STATUS].iValue 
		//				= SMAC_COMM_OK;	
		//			SmacDeviceClass->pRoughData[iStartRoughPosition + SMAC_HAVE_SMAC_ADDR].iValue 
		//				= SMAC_ADDR_END;			
		//			SmacDeviceClass->pRoughData[iStartRoughPosition + SMAC_WORKING_STATUS].iValue 
		//				= SMAC_EQUIP_EXISTENT;
		//		}
		//		else//only 106,stuff first RoughData map
		//		{
		//			SmacDeviceClass->pRoughData[SMAC_COMM_STATUS].iValue = SMAC_COMM_OK;	
		//			SmacDeviceClass->pRoughData[SMAC_HAVE_SMAC_ADDR].iValue = SMAC_ADDR_END;	
		//			SmacDeviceClass->pRoughData[SMAC_WORKING_STATUS].iValue = SMAC_EQUIP_EXISTENT;
		//		}
		//	}
		//}
		//else//SMAC_ADDR_START----SMAC_ADDR_END
		//{
		//	//here needn't process;
		//}
	}//end for

	return 0;
}

/*=============================================================================*
 * FUNCTION: FixFloatDat
 * PURPOSE : Eight byte convert to float
 * INPUT:	 char* sStr	
 *     
 *
 * RETURN:
 *     static float
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *		kenn.wan                 DATE: 2008-08-15 14:55
 *============================================================================*/
static float FixFloatDat( char* sStr )
{
    INT32				i;
    SMACSTRTOFLOAT		floatvalue;
	CHAR				cTemp;
	CHAR				cTempHi;
	CHAR				cTempLo;

    if (sStr[0] == 0x20)
	{
        return -9999.0f;
	}

    for (i=0; i<4; i++)
    {
#ifdef _CPU_BIG_ENDIAN		// ppc
#ifdef _SAMPLER_REPLACE_SSCANF
        sscanf((const char *)sStr+i*2, "%02x", 
            &floatvalue.by_value[3-i]);
#else
        int	c;
        sscanf((const char *)sStr+i*2, "%02x", 	&c);
        floatvalue.by_value[3-i] = (BYTE)c;
#endif
#else	// for x86
		//hi bit
		if((*(sStr + i * 2) > 0x40))
		{
			cTempHi = (*(sStr + i * 2) - 0x40) + 9;
		}
		else
		{
			cTempHi = (*(sStr + i * 2) - 0x30);
		}

		//low bit
		if((*(sStr + i * 2 + 1)) > 0x40)
		{
			cTempLo = (*(sStr + i * 2 + 1) - 0x40) + 9;
		}
		else
		{
			cTempLo = (*(sStr + i * 2 + 1) - 0x30);
		}

		cTemp = cTempHi * 16 + cTempLo;
		floatvalue.by_value[i] = cTemp;
#endif
     }
    return floatvalue.f_value;
}



/*=============================================================================*
 * FUNCTION: FixFloatDat
 * PURPOSE : ���Ѵ����ȫ�ֱ�������Frame[]�е����ݸ������ݽṹ��
			 ����Ķ�Ӧ��ϵ���н���
 * INPUT:	 Frame - ԭʼ��������, fData - �����������
 *			 strData - ���ڽ������ݽ��������ݽṹ
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     
 *		copy from smac             
 *============================================================================*/
static void Fix_Data(
              RS485_VALUE*		fData,				// ����õ����ݻ�����
              SMACDATASTRUCT	strData[],			// Ҫ��������ݽṹ
              BYTE				*Frame )            // Ҫ������ַ���
{
    CHAR	*sStop;  
	INT32	iTempIdex;
	INT32	iBeUsedFlag;
	INT32	nLoop		= 0;
    CHAR	sTemp[5]	= {0};
    ASSERT(fData != NULL);

    while( strData[nLoop].nType )					// ѭ��ֱ������������=0
    {
        switch( strData[nLoop].nType )				// ����������������Ӧ����0-������־,1-4BYTES(�޷��ţ�,2-4 bytes���з��ţ� 3-2Bytes
        {
        case 1: // 4�ֽ��޷�������������ʽ��fData�������մ���ȥ��p ֱ��p�и�ͨ���������Ѿ����
			
			iBeUsedFlag = 0;
			for (iTempIdex = 0; iTempIdex < 8; iTempIdex++)
			{
				if(*(Frame + strData[nLoop].nOffset + iTempIdex) != 0x20)
				{
					iBeUsedFlag = 1;
					break;
				}
			}

			if(iBeUsedFlag == 1)
			{
				(fData + strData[nLoop].nChanel)->fValue = 
					FixFloatDat((CHAR *)Frame + strData[nLoop].nOffset ) / strData[nLoop].nScaleBit;
			}
			else
			{
				(fData + strData[nLoop].nChanel)->iValue = SMAC_SAMP_INVALID_VALUE;
			}

            break;
            
        case 2: // 4�ֽ��з�������������ʽ��ulValue
            strncpy( sTemp, (CHAR*)Frame+strData[nLoop].nOffset, 4 );
            sTemp[4] = 0;
            (fData + strData[nLoop].nChanel)->ulValue = 
				(float)strtoul( sTemp, &sStop, 16 ) / strData[nLoop].nScaleBit;

            if((fData + strData[nLoop].nChanel)->ulValue > 0x8000 )
			{
                (fData + strData[nLoop].nChanel)->ulValue = 
					0x8000 - (fData + strData[nLoop].nChanel)->ulValue;
			}
            break;

        case 3:
            // 2�ֽ�������ʽ��
            //��Ч�򲻲�Ϊ0x20

			if( Frame[strData[nLoop].nOffset] == ' ')
			{
				(fData + strData[nLoop].nChanel)->ulValue = 0x20;
			}
			else
			{
				strncpy( sTemp, (char*)Frame+strData[nLoop].nOffset, 2 );
				sTemp[2] = 0;
				(fData + strData[nLoop].nChanel)->ulValue = strtoul( sTemp, &sStop, 16 );
			}

			//if (*(Frame + strData[nLoop].nOffset) != 0x20)
			//{
			//	(fData + strData[nLoop].nChanel)->iValue = 
			//				SMAC_MergeAsc(Frame + strData[nLoop].nChanel);
			//}
			//else
			//{
			//	(fData + strData[nLoop].nChanel)->iValue 
			//		= SMAC_SAMP_INVALID_VALUE; 

			//}

            break;

        default:
            break;
        }   //End Switch
        
        nLoop++;
    }   //End While
}

/*=============================================================================*
 * FUNCTION: SMACUnpackCmd41
 * PURPOSE : Arrange sampling data by 0x41 to RouthData
 * INPUT: 
 *     
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
//INT32 SMACUnpackCmd41(INT32					iSmacEqpSeqNoIndex,
//					  INT32					iAddr,
//					  RS485_DEVICE_CLASS*	SmacDeviceClass,
//					  float*				pReceiveData
//					  )
//{
//	UNUSED(iSmacEqpSeqNoIndex);
//	UNUSED(pReceiveData);
//	UNUSED(iAddr);
//
//	RS485_CHN_TO_ROUGH_DATA Sig41[] =
//	{
//		//channel no								rough data
//		{SMAC_CH_OUT_PHASE_VLT_A,					SMAC_OUT_PHASE_VLT_A},
//		{SMAC_CH_OUT_PHASE_VLT_B,					SMAC_OUT_PHASE_VLT_B},	
//		{SMAC_CH_OUT_PHASE_VLT_C,					SMAC_OUT_PHASE_VLT_C},	
//		{SMAC_CH_OUT_LINE_VLT_AB,					SMAC_OUT_LINE_VLT_AB},
//		{SMAC_CH_OUT_LINE_VLT_BC,					SMAC_OUT_LINE_VLT_BC},
//		{SMAC_CH_OUT_LINE_VLT_CA,					SMAC_OUT_LINE_VLT_CA},
//		{SMAC_CH_OUT_CURRENT_A,						SMAC_OUT_CURRENT_A},
//		{SMAC_CH_OUT_CURRENT_B,						SMAC_OUT_CURRENT_B},
//		{SMAC_CH_OUT_CURRENT_C,						SMAC_OUT_CURRENT_C},
//		{SMAC_CH_FREQUENCY,							SMAC_FREQUENCY},
//		{SMAC_CH_TOTAL_REAL_POWER,					SMAC_TOTAL_REAL_POWER},
//		{SMAC_CH_PHASE_A_REAL_POWER,				SMAC_PHASE_A_REAL_POWER},
//		{SMAC_CH_PHASE_B_REAL_POWER,				SMAC_PHASE_B_REAL_POWER},
//		{SMAC_CH_PHASE_CREAL_POWER,					SMAC_PHASE_CREAL_POWER},
//		{SMAC_CH_TOTAL_REACTIVE_POWER,				SMAC_TOTAL_REACTIVE_POWER},
//		{SMAC_CH_PHASE_A_REACTIVE_POWER,			SMAC_PHASE_A_REACTIVE_POWER},
//		{SMAC_CH_PHASE_B_REACTIVE_POWER,			SMAC_PHASE_B_REACTIVE_POWER},
//		{SMAC_CH_PHASE_CREACTIVE_POWER,				SMAC_PHASE_CREACTIVE_POWER},
//		{SMAC_CH_TOTAL_APPARENT_POWER,				SMAC_TOTAL_APPARENT_POWER},
//		{SMAC_CH_PHASE_A_APPARENT_POWER,			SMAC_PHASE_A_APPARENT_POWER},
//		{SMAC_CH_PHASE_B_APPARENT_POWER,			SMAC_PHASE_B_APPARENT_POWER},
//		{SMAC_CH_PHASE_C_APPARENT_POWER,			SMAC_PHASE_C_APPARENT_POWER},
//		{SMAC_CH_POWER_FACTOR,						SMAC_POWER_FACTOR},
//		{SMAC_CH_PHASE_A_POWER_FACTOR,				SMAC_PHASE_A_POWER_FACTOR},
//		{SMAC_CH_PHASE_B_POWER_FACTOR,				SMAC_PHASE_B_POWER_FACTOR},
//		{SMAC_CH_PHASE_C_POWER_FACTOR,				SMAC_PHASE_C_POWER_FACTOR},
//		{SMAC_CH_PHASE_A_CURRENT_CREST_FACTOR,		SMAC_PHASE_A_CURRENT_CREST_FACTOR},
//		{SMAC_CH_PHASE_B_CURRENT_CREST_FACTOR,		SMAC_PHASE_B_CURRENT_CREST_FACTOR},
//		{SMAC_CH_PHASE_C_CURRENT_CREST_FACTOR,		SMAC_PHASE_C_CURRENT_CREST_FACTOR},
//		{SMAC_CH_PHASE_A_CURRENT_THD,				SMAC_PHASE_A_CURRENT_THD},
//		{SMAC_CH_PHASE_B_CURRENT_THD,				SMAC_PHASE_B_CURRENT_THD},
//		{SMAC_CH_PHASE_C_CURRENT_THD,				SMAC_PHASE_C_CURRENT_THD},
//		{SMAC_CH_PHASE_A_VOLT_THD,					SMAC_PHASE_A_VOLT_THD},
//		{SMAC_CH_PHASE_B_VOLT_THD,					SMAC_PHASE_B_VOLT_THD},
//		{SMAC_CH_PHASE_C_VOLT_THD,					SMAC_PHASE_C_VOLT_THD},
//		{SMAC_CH_TOTAL_REAL_ENERGY,					SMAC_TOTAL_REAL_ENERGY},
//		{SMAC_CH_TOTAL_REACTIVE_ENERGY,				SMAC_TOTAL_REACTIVE_ENERGY},
//		{SMAC_CH_APPARENT_ENERGY,					SMAC_APPARENT_ENERGY},
//		{SMAC_CH_AMBIENT_TEMPERATURE,				SMAC_AMBIENT_TEMPERATURE},
//		{SMAC_CH_BATTERY_START_VOLTAGE,				SMAC_BATTERY_START_VOLTAGE},
//		{SMAC_CH_COMM_STATUS,						SMAC_COMM_STATUS},
//		{SMAC_CH_EXIST_STATUS,					SMAC_WORKING_STATUS},
//
//		{SMAC_CH_SIG_END,							SMAC_SIG_END},
//	};
//}

/*=============================================================================*
 * FUNCTION: SMACSampleCmd41
 * PURPOSE : Sampling  smac data by 0x41
 * INPUT: 
 *     
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMACSampleCmd41(INT32 iSmacEqpSeqNoIndex, INT32 iAddr, RS485_DEVICE_CLASS* SmacDeviceClass)
{
	INT32			i;
	INT32			iSmacStat;
	INT32			nUnitNo;
	INT32			iStartRoughDataPosition;
	INT32			iCommunicationStat;
	BYTE			szSendStr[21]	= {0};
	BYTE			abyRcvBuf[MAX_RECVBUFFER_NUM_SMAC] = {0};   
	nUnitNo							= iAddr;
	iSmacStat						= SMAC_SAMP_INVALID_VALUE;

	sprintf((CHAR *)szSendStr,
					"~%s%02X%02X%02X%04X\0",
					SMACszVer,
					(unsigned int)nUnitNo, 
					SMAC_CID1,
					SMAC_CID2_0x41,
					0);

	CheckSum(szSendStr);

	*(szSendStr + 17) =  byEOI;

	iStartRoughDataPosition = iSmacEqpSeqNoIndex * SMAC_MAX_SIGNALS_NUM;

	for (i = 0; i < RS485_RECONNECT_TIMES;i++)
	{
		iSmacStat = SMACSendAndRead(SmacDeviceClass->pCommPort->hCommPort, 
									szSendStr,
									abyRcvBuf,
									18,
									RS485_WAIT_LONG_TIME,
									1024);					
		iCommunicationStat 
			= SmacDeviceClass->pRoughData[iStartRoughDataPosition + SMAC_COMM_STATUS].iValue; 

		if ((RESPONSE_OK == iSmacStat) || (iCommunicationStat == SMAC_COMM_FAIL))
		{
			break;
		}
	}

	if(RESPONSE_OK == iSmacStat)
	{
		//upackData
		Fix_Data((&g_SmacSampleData.aRoughDataSMAC[iSmacEqpSeqNoIndex][0] + SMAC_OUT_PHASE_VLT_A),
				s_Smac41SigInfo, 
				abyRcvBuf + DATAINFO_YDN23_SMAC);

		return TRUE;
	}
	else			//if (RESPONSE_OK != iSmacStat)
	{
		return FALSE;
	}

}

/*=============================================================================*
 * FUNCTION: SMACSampleCmd43
 * PURPOSE : sampling  smac data by 0x43
 * INPUT: 
 *     INT32 iSmacEqpSeqNoIndex,INT32 iAddr,RS485_DEVICE_CLASS* SmacDeviceClass
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMACSampleCmd43(INT32 iSmacEqpSeqNoIndex,INT32 iAddr,RS485_DEVICE_CLASS* SmacDeviceClass)
{
	INT32		i;
	INT32		iSmacStat;
	INT32		nUnitNo;
	INT32		nTempValue;
	INT32		iStartRoughDataPosition;
	RS485_VALUE*pDieselTestResult;
	RS485_VALUE*pfRoughData;
	BYTE		szSendStr[21]	= {0};
	BYTE		abyRcvBuf[MAX_RECVBUFFER_NUM_SMAC] = {0}; 
	nUnitNo						= iAddr;	
	iSmacStat					= SMAC_SAMP_INVALID_VALUE;

	sprintf((char *)szSendStr,
				"~%s%02X%02X%02X%04X\0", 
				SMACszVer,
				(unsigned int)nUnitNo, 
				SMAC_CID1, 
				SMAC_CID2_0x43,
				0);

	CheckSum( szSendStr );

	*(szSendStr + 17) =  byEOI;
	iStartRoughDataPosition = iSmacEqpSeqNoIndex * SMAC_MAX_SIGNALS_NUM;

	for (i = 0; i < RS485_RECONNECT_TIMES;i++)
	{
		iSmacStat = SMACSendAndRead(SmacDeviceClass->pCommPort->hCommPort,
									szSendStr,
									abyRcvBuf,
									18,
									RS485_WAIT_VERY_SHORT_TIME,
									1024);

		if (RESPONSE_OK == iSmacStat)
		{
			break;
		}
	}

	if (RESPONSE_OK != iSmacStat)
	{
		//TRACE("\n  ***SMAC CID2 43 ERROR i = %d \n",i);
		return FALSE;
	}

	//Range	the 	RoughData
	if(RESPONSE_OK == iSmacStat)
	{
		SmacDeviceClass->pRoughData[iStartRoughDataPosition + SMAC_COMM_STATUS].iValue
				= SMAC_COMM_OK;
		pfRoughData = &g_SmacSampleData.aRoughDataSMAC[iSmacEqpSeqNoIndex][0];
		//upackRoughData
		Fix_Data((pfRoughData + SMAC_SWITCH_1),s_Smac43OnOff,abyRcvBuf + DATAINFO_YDN23_SMAC + 2);

		//Relay 0-4, Stop reason of diesel test stop  refer to  ROUGH_DATA_IDX_SMAC
		for (i = SMAC_RELAY_1_STATUS; i < SMAC_DIESEL_TEST_STOP_CAUSE; i++)
		{
			//if (FLOAT_EQUAL((pfRoughData + i)->ulValue, 0x55))
			if((pfRoughData + i)->ulValue == 0x55)
			{
				(pfRoughData + i)->ulValue = 0x01;
			}
			else	//MFH: 
			{
				(pfRoughData + i)->ulValue = 0x00;
			}
		}

		pDieselTestResult = pfRoughData + SMAC_DIESEL_TEST_STOP_CAUSE;
		nTempValue= pDieselTestResult->ulValue;

		//if (*pDieselTestResult == 0), it is normal status
		for (i = 0; i < 8; i++)
		{
			if ((nTempValue & 0x01) != 0)
			{
				pDieselTestResult->ulValue = i + 1;
				break;
			}

			nTempValue = (nTempValue >> 1);
		}

		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


/*=============================================================================*
 * FUNCTION: SMACSampleCmd44
 * PURPOSE : Sampling  smac data by 0x44
 * INPUT: 
 *		INT32 iSmacEqpSeqNoIndex,INT32 iAddr,RS485_DEVICE_CLASS* SmacDeviceClass
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMACSampleCmd44(INT32 iSmacEqpSeqNoIndex,INT32 iAddr,RS485_DEVICE_CLASS* SmacDeviceClass)
{
  
	INT32			i;
	INT32			iSmacStat;
	INT32			nUnitNo;
	//INT32			nTempValue;
	INT32			iStartRoughDataPosition;
	RS485_VALUE*	pfRoughData;
	BYTE			szSendStr[21]	= {0};
	BYTE			abyRcvBuf[MAX_RECVBUFFER_NUM_SMAC] = {0};  
	nUnitNo							= iAddr;
	iSmacStat						= SMAC_SAMP_INVALID_VALUE;

	sprintf((char *)szSendStr,
			"~%s%02X%02X%02X%04X\0",
			SMACszVer,
			(unsigned int)nUnitNo, 
			SMAC_CID1,
			SMAC_CID2_0x44,
			0);

	CheckSum( szSendStr );

	*(szSendStr + 17) =  byEOI;
	iStartRoughDataPosition = iSmacEqpSeqNoIndex * SMAC_MAX_SIGNALS_NUM; 

	for (i = 0; i < RS485_RECONNECT_TIMES;i++)
	{
		iSmacStat = SMACSendAndRead(SmacDeviceClass->pCommPort->hCommPort, 
									szSendStr,
									abyRcvBuf,
									18,
									RS485_WAIT_VERY_SHORT_TIME,
									1024);

		if (RESPONSE_OK == iSmacStat)
		{
			break;	
		}
	}

	if (RESPONSE_OK != iSmacStat)
	{
		TRACE("\n  ***SMAC  CID2 44 ERROR i = %d \n",(int)i);
		return FALSE;
	}
	else if(RESPONSE_OK == iSmacStat)
	{	
		SmacDeviceClass->pRoughData[iStartRoughDataPosition + SMAC_COMM_STATUS].iValue
				= SMAC_COMM_OK;		
		//SmacDeviceClass->pRoughData point to g_SmacSampleData.aRoughDataSMAC[0][0]
		pfRoughData = SmacDeviceClass->pRoughData + iStartRoughDataPosition;

		//no DATAFLAG
		Fix_Data(pfRoughData + SMAC_ALARM_AC_PHASE_VOLT_A, 
				s_Smac44Alarm, 
				abyRcvBuf + DATAINFO_YDN23_SMAC);
		//refer to the protocol
		for (i = SMAC_ALARM_AC_PHASE_VOLT_A; i <= SMAC_ALARM_DIESEL_GENERATOR_FAIL; i++)
		{
			//if (FLOAT_EQUAL((pfRoughData + i)->ulValue, 0xF0))
			if ((pfRoughData + i)->ulValue == 0xF0)
			{
				(pfRoughData + i)->ulValue = 0x01;
			}	
		}

		return TRUE;
	}

	return TRUE;

}

/*=============================================================================*
 * FUNCTION: SMACSampleCmd47
 * PURPOSE : Sampling  smac data by 0x47
 * INPUT: 
 *     INT32 iSmacEqpSeqNoIndex,INT32 iAddr,RS485_DEVICE_CLASS* SmacDeviceClass
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMACSampleCmd47(INT32 iSmacEqpSeqNoIndex,INT32 iAddr,RS485_DEVICE_CLASS* SmacDeviceClass)
{
	INT32		i;
	INT32		iSmacStat;
	INT32		nUnitNo;
	//INT32		nTempValue;
	INT32		iStartRoughDataPosition;
	//float*		pDieselTestResult;
	RS485_VALUE* pfRoughData;
	BYTE		szSendStr[21]		= {0};
	BYTE		abyRcvBuf[MAX_RECVBUFFER_NUM_SMAC]		= {0};   
	nUnitNo							= iAddr;
	iSmacStat						= SMAC_SAMP_INVALID_VALUE;

	sprintf((char *)szSendStr,
				"~%s%02X%02X%02X%04X\0",
				SMACszVer,
				(unsigned int)nUnitNo, 
				SMAC_CID1,
				SMAC_CID2_0x47,
				0);

	CheckSum( szSendStr );
	*(szSendStr + 17) =  byEOI;
	iStartRoughDataPosition = iSmacEqpSeqNoIndex * SMAC_MAX_SIGNALS_NUM;

	for (i = 0; i < RS485_RECONNECT_TIMES;i++)
	{
		iSmacStat = SMACSendAndRead(SmacDeviceClass->pCommPort->hCommPort,
									szSendStr,
									abyRcvBuf, 
									18,
									RS485_WAIT_VERY_SHORT_TIME,
									0x06 + 18);

		if (RESPONSE_OK == iSmacStat)
		{
			break;	
		}
	}

	if(RESPONSE_OK == iSmacStat)
	{
		SmacDeviceClass->pRoughData[iStartRoughDataPosition + SMAC_COMM_STATUS].iValue
				= SMAC_COMM_OK;
		//SmacDeviceClass->pRoughData point to g_SmacSampleData.aRoughDataSMAC[0][0]
		pfRoughData = SmacDeviceClass->pRoughData + iStartRoughDataPosition;
		Fix_Data(pfRoughData + SMAC_GETSYS_ALARM_PERCENTUM,
				s_Smac47sys,
				abyRcvBuf + DATA_YDN23_SMAC);

		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


/*=============================================================================*
 * FUNCTION: SMACSampleCmd46
 * PURPOSE : Sampling  smac data by 0x46
 * INPUT: 
 *     INT32 iSmacEqpSeqNoIndex,INT32 iAddr,RS485_DEVICE_CLASS* SmacDeviceClass
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMACSampleCmd46(INT32 iSmacEqpSeqNoIndex,INT32 iAddr,RS485_DEVICE_CLASS* SmacDeviceClass)
{
	INT32			i;
	INT32			iSmacStat;
	INT32			nUnitNo;
	//INT32			nTempValue;
	INT32			iStartRoughDataPosition;
	//float*			pDieselTestResult;
	RS485_VALUE*		pfRoughData;
	BYTE			szSendStr[21]	= {0};
	BYTE			abyRcvBuf[MAX_RECVBUFFER_NUM_SMAC] = {0};
	nUnitNo							= iAddr;
	iSmacStat						= SMAC_SAMP_INVALID_VALUE;

	sprintf((char *)szSendStr,
					"~%s%02X%02X%02X%04X\0",
					SMACszVer,
					(unsigned int)nUnitNo, 
					SMAC_CID1,
					SMAC_CID2_0x46,
					0);

	CheckSum( szSendStr );
	*(szSendStr + 17) =  byEOI;
	iStartRoughDataPosition = iSmacEqpSeqNoIndex * SMAC_MAX_SIGNALS_NUM;

	for (i = 0; i < RS485_RECONNECT_TIMES;i++)
	{
		iSmacStat = SMACSendAndRead(SmacDeviceClass->pCommPort->hCommPort,
									szSendStr,
									abyRcvBuf, 
									18,
									RS485_WAIT_LONG_TIME,
									1024);

		if (RESPONSE_OK == iSmacStat)
		{
			break;	
		}
	}

	if (RESPONSE_OK != iSmacStat)
	{
		return FALSE;
		TRACE("\n  ***SMAC  CID2 46 ERROR i = %d \n", (int)i);
	}
	else if(RESPONSE_OK == iSmacStat)
	{
		SmacDeviceClass->pRoughData[iStartRoughDataPosition + SMAC_COMM_STATUS].iValue
				= SMAC_COMM_OK;		
		//SmacDeviceClass->pRoughData point to g_SmacSampleData.aRoughDataSMAC[0][0]
		pfRoughData = SmacDeviceClass->pRoughData + iStartRoughDataPosition;

		Fix_Data( pfRoughData + SMAC_GETANLG_AC_CURRENT_A_UPPER,
				  s_Smac46AnologParameter, 
				  abyRcvBuf + DATA_YDN23_SMAC);
		return TRUE;
	}
	return TRUE;
}
/*=============================================================================*
* FUNCTION: SMACIncCommBreakTimes
* PURPOSE : Record communication failt time,if more than three means report faile
* INPUT: 
*			INT32 iSmacEqpSeqNoIndex, RS485_DEVICE_CLASS* SmacDeviceClass
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32  SMACIncCommBreakTimes(INT32 iSmacEqpSeqNoIndex, RS485_DEVICE_CLASS* SmacDeviceClass)
{
	INT32 iStartRoughDataPosition = iSmacEqpSeqNoIndex * SMAC_MAX_SIGNALS_NUM;

#define  RS485_SMAC_INTERRUPT_TIMES 10

	if(SmacDeviceClass->pRoughData[iStartRoughDataPosition + SMAC_INTERRUPT_TIMES].iValue 
		>= RS485_SMAC_INTERRUPT_TIMES)
	{
		SmacDeviceClass->pRoughData[iStartRoughDataPosition + SMAC_COMM_STATUS].iValue
			= SMAC_COMM_FAIL;

		//There is diesel comm process!!
		if (SmacDeviceClass->pRoughData[SMAC_HAVE_SMAC_ADDR].iValue == SMAC_ADDR_START)
		{
			if (SmacDeviceClass->pRoughData[SMAC_COMM_STATUS].iValue == SMAC_COMM_FAIL)
			{
				SmacDeviceClass->pRoughData[SMAC_DISEL_COMM_STAT].iValue = SMAC_COMM_FAIL;
			}
		}

		return FALSE;	
	}
	else
	{
		if(SmacDeviceClass->pRoughData[iStartRoughDataPosition + SMAC_INTERRUPT_TIMES].iValue
			< (RS485_SMAC_INTERRUPT_TIMES + 1))
		{
			SmacDeviceClass->pRoughData[iStartRoughDataPosition + SMAC_INTERRUPT_TIMES].iValue++;
		}
		return FALSE;
	}
}

/*=============================================================================*
* FUNCTION: SMACClrCommBreakTimes
* PURPOSE : Only one time communication ok clean the fail flag
* INPUT: 
*			INT32 iSmacEqpSeqNoIndex, RS485_DEVICE_CLASS* SmacDeviceClass
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32  SMACClrCommBreakTimes(INT32 iSmacEqpSeqNoIndex, RS485_DEVICE_CLASS* SmacDeviceClass)
{
	INT32 iStartRoughDataPosition = iSmacEqpSeqNoIndex * SMAC_MAX_SIGNALS_NUM;
	SmacDeviceClass->pRoughData[iStartRoughDataPosition + SMAC_INTERRUPT_TIMES].iValue
		= 0;
	SmacDeviceClass->pRoughData[iStartRoughDataPosition + SMAC_COMM_STATUS].iValue
		= SMAC_COMM_OK;

	//There is diesel comm process!!
	if (SmacDeviceClass->pRoughData[SMAC_HAVE_SMAC_ADDR].iValue == SMAC_ADDR_START)
	{
		if (SmacDeviceClass->pRoughData[SMAC_COMM_STATUS].iValue == SMAC_COMM_OK)
		{
			SmacDeviceClass->pRoughData[SMAC_DISEL_COMM_STAT].iValue = SMAC_COMM_OK;
		}
	}
	return 0;
}

/*=============================================================================*
* FUNCTION: SMACNeedCommProc
* PURPOSE : Check  the rs485Comm port
* INPUT: 
*    
*
* RETURN:
*      TRUE: need process FALSE:needn't process
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32	SMACNeedCommProc(void)
{
	if(g_RS485Data.CommPort.enumAttr != RS485_ATTR_NONE
		|| g_RS485Data.CommPort.enumBaud != RS485_ATTR_19200
		|| g_RS485Data.CommPort.bOpened != TRUE)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*=============================================================================*
* FUNCTION: SMACReopenPort
* PURPOSE :
* INPUT: 
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMACReopenPort(void)
{
	INT32	iErrCode;
	INT32	iOpenTimes;
	iOpenTimes = 0;

	while (iOpenTimes < RS485_COMM_TRY_OPEN_TIMES)
	{
		if(g_RS485Data.CommPort.hCommPort && g_RS485Data.CommPort.bOpened)
		{
			RS485_Close(g_RS485Data.CommPort.hCommPort);
		}

		g_RS485Data.CommPort.hCommPort 
			= RS485_Open("19200, n, 8, 1", RS485_READ_WRITE_TIMEOUT, (int *)(&iErrCode));

		if(iErrCode == ERR_COMM_OK)
		{
			g_RS485Data.CommPort.bOpened = TRUE;
			g_RS485Data.CommPort.enumAttr = RS485_ATTR_NONE;
			g_RS485Data.CommPort.enumBaud = RS485_ATTR_19200;
			Sleep(5);
			break;
		}
		else
		{
			Sleep(1000);
			iOpenTimes++;
			g_RS485Data.CommPort.bOpened = FALSE;
			AppLogOut("RS485_SMAC_ReOpen", APP_LOG_UNUSED,
				"[%s]-[%d] ERROR: failed to  open rs485Comm IN The SMAC ReopenPort \n\r", 
				__FILE__, __LINE__);
		}
	}

	return 0;
}
/*=============================================================================*
* FUNCTION: bSmacCheckCommFail()
* PURPOSE : 
* INPUT:	
*     
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 bSmacCheckCommFail(void* pDevice,INT32 SMACUnitNo)
{
	BYTE	szSendStr[20]		= {0};
	CHAR	abyRcvBuf[MAX_RECVBUFFER_NUM_SMAC]	= {0};
	RS485_DEVICE_CLASS*		SmacDeviceClass		= pDevice;
	INT32	iSmacStatus							= SMAC_SAMP_INVALID_VALUE;
	INT32	i;

	sprintf( (char *)szSendStr, 
					"~%02X%02X%02X%02X%04X\0",
					0x20, 
					(unsigned int)SMACUnitNo,
					SMAC_CID1,
					SMAC_GETVER_CID2,
					0 );
	CheckSum( szSendStr );

	*(szSendStr + 17) =  byEOI;

	TRACE(" ::: bCheck AC %d \n",(int)SMACUnitNo);

	for (i = 0; i < 1; i++)
	{
		iSmacStatus = SMACSendAndRead(SmacDeviceClass->pCommPort->hCommPort,
										szSendStr, 
										(BYTE*)abyRcvBuf, 
										18,
										RS485_WAIT_VERY_SHORT_TIME,
										18);

		if (RESPONSE_OK == iSmacStatus)
		{
			break;
		}
	}
	
	return iSmacStatus;
}


/*=============================================================================*
 * FUNCTION: SMACSample
 * PURPOSE : Sampling  smac all sig info
 * INPUT:	 g_aDeviceClass
 *     
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMACSample(void* pDevice)
{
	//INT32					i;
	//INT32					j;
	//INT32					k;
	//INT32					iTmpAddress;
	INT32					SMACUnitNo;
	INT32					iSmacEqpSeqNoIndex;
	INT32					iSmacIndexMap;
	RS485_DEVICE_CLASS*		p_SmacDeviceClass	= pDevice;
	static BOOL				bFirstRunning		= TRUE;

	if (SMACNeedCommProc())
	{
		SMACReopenPort();
	}

	if(p_SmacDeviceClass->bNeedReconfig == TRUE)
	{
		SMAC_InitRoughValue(p_SmacDeviceClass);//It is must here that before  "bNeedReconfig = FALSE;"

		p_SmacDeviceClass->bNeedReconfig = FALSE;
		SMACReconfig(p_SmacDeviceClass);
		return TRUE;
	}

	//Run it when Power just start and Mode Modified
	if (bFirstRunning || g_RS485Data.bNeedReInitModeModify)
	{
		bFirstRunning = FALSE;
		SMAC_SetParam(p_SmacDeviceClass);
	}

	//Block 500ms
	for(iSmacEqpSeqNoIndex = 0; iSmacEqpSeqNoIndex < SMAC_NUM; iSmacEqpSeqNoIndex++)
	{
		iSmacIndexMap = iSmacEqpSeqNoIndex * SMAC_MAX_SIGNALS_NUM;
		
		if(p_SmacDeviceClass->pRoughData[iSmacIndexMap + SMAC_HAVE_SMAC_ADDR].iValue 
			!= SMAC_SAMP_INVALID_VALUE)
		{
			RUN_THREAD_HEARTBEAT();

			SMACUnitNo = p_SmacDeviceClass->pRoughData[iSmacIndexMap + SMAC_HAVE_SMAC_ADDR].iValue;

			//Batt testing return SM;
			if (bBattTesting(TRUE))
			{
				//printf("\n$$$$$$$$$$  in  SMACSample bBattTesting ADDR =%d return \n",SMACUnitNo);
				return 0;
			}

			//CMD47 query one time
			if (SMACSampleCmd47(iSmacEqpSeqNoIndex,SMACUnitNo,p_SmacDeviceClass))
			{
				SMACClrCommBreakTimes(iSmacEqpSeqNoIndex, p_SmacDeviceClass);
			}
			else
			{
				//because communication error needn't sampling
				SMACIncCommBreakTimes(iSmacEqpSeqNoIndex, p_SmacDeviceClass);
				continue;
			}
			
			if (!SampCheckSmAddrProcess(SMACUnitNo))
			{
				continue;
			}

			Sleep(110);
			SMACSampleCmd41(iSmacEqpSeqNoIndex, SMACUnitNo, p_SmacDeviceClass);
			Sleep(110);
			SMACSampleCmd43(iSmacEqpSeqNoIndex,SMACUnitNo,p_SmacDeviceClass);
			Sleep(110);			
			SMACSampleCmd44(iSmacEqpSeqNoIndex,SMACUnitNo,p_SmacDeviceClass);
			Sleep(110);
			SMACSampleCmd46(iSmacEqpSeqNoIndex,SMACUnitNo,p_SmacDeviceClass);
		}
	}

	return 0;
}
/*=============================================================================*
 * FUNCTION: SMAC_StuffChannel
 * PURPOSE : Stuff smac data info from RoughData to gc and web 
 * INPUT: 
 *     RS485_DEVICE_CLASS* SmacDeviceClass,ENUMSIGNALPROC EnumProc,LPVOID lpvoid
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMAC_StuffChannel(RS485_DEVICE_CLASS* SmacDeviceClass,ENUMSIGNALPROC EnumProc,LPVOID lpvoid)
{

	//INT32			i;
	INT32			j;
	//INT32			k;
	INT32			iRoughSequenceIdx;
	INT32			iRoughDataPosition;
	INT32			iChannelPosition;

	RS485_CHN_TO_ROUGH_DATA SMACSig41[]=
	{
		//channel no								rough data
		{SMAC_CH_OUT_PHASE_VLT_A,					SMAC_OUT_PHASE_VLT_A},
		{SMAC_CH_OUT_PHASE_VLT_B,					SMAC_OUT_PHASE_VLT_B},	
		{SMAC_CH_OUT_PHASE_VLT_C,					SMAC_OUT_PHASE_VLT_C},	
		{SMAC_CH_OUT_LINE_VLT_AB,					SMAC_OUT_LINE_VLT_AB},
		{SMAC_CH_OUT_LINE_VLT_BC,					SMAC_OUT_LINE_VLT_BC},
		{SMAC_CH_OUT_LINE_VLT_CA,					SMAC_OUT_LINE_VLT_CA},
		{SMAC_CH_OUT_CURRENT_A,						SMAC_OUT_CURRENT_A},
		{SMAC_CH_OUT_CURRENT_B,						SMAC_OUT_CURRENT_B},
		{SMAC_CH_OUT_CURRENT_C,						SMAC_OUT_CURRENT_C},
		{SMAC_CH_FREQUENCY,							SMAC_FREQUENCY},
		{SMAC_CH_TOTAL_REAL_POWER,					SMAC_TOTAL_REAL_POWER},
		{SMAC_CH_PHASE_A_REAL_POWER,				SMAC_PHASE_A_REAL_POWER},
		{SMAC_CH_PHASE_B_REAL_POWER,				SMAC_PHASE_B_REAL_POWER},
		{SMAC_CH_PHASE_CREAL_POWER,					SMAC_PHASE_CREAL_POWER},
		{SMAC_CH_TOTAL_REACTIVE_POWER,				SMAC_TOTAL_REACTIVE_POWER},
		{SMAC_CH_PHASE_A_REACTIVE_POWER,			SMAC_PHASE_A_REACTIVE_POWER},
		{SMAC_CH_PHASE_B_REACTIVE_POWER,			SMAC_PHASE_B_REACTIVE_POWER},
		{SMAC_CH_PHASE_CREACTIVE_POWER,				SMAC_PHASE_CREACTIVE_POWER},
		{SMAC_CH_TOTAL_APPARENT_POWER,				SMAC_TOTAL_APPARENT_POWER},
		{SMAC_CH_PHASE_A_APPARENT_POWER,			SMAC_PHASE_A_APPARENT_POWER},
		{SMAC_CH_PHASE_B_APPARENT_POWER,			SMAC_PHASE_B_APPARENT_POWER},
		{SMAC_CH_PHASE_C_APPARENT_POWER,			SMAC_PHASE_C_APPARENT_POWER},
		{SMAC_CH_POWER_FACTOR,						SMAC_POWER_FACTOR},
		{SMAC_CH_PHASE_A_POWER_FACTOR,				SMAC_PHASE_A_POWER_FACTOR},
		{SMAC_CH_PHASE_B_POWER_FACTOR,				SMAC_PHASE_B_POWER_FACTOR},
		{SMAC_CH_PHASE_C_POWER_FACTOR,				SMAC_PHASE_C_POWER_FACTOR},
		{SMAC_CH_PHASE_A_CURRENT_CREST_FACTOR,		SMAC_PHASE_A_CURRENT_CREST_FACTOR},
		{SMAC_CH_PHASE_B_CURRENT_CREST_FACTOR,		SMAC_PHASE_B_CURRENT_CREST_FACTOR},
		{SMAC_CH_PHASE_C_CURRENT_CREST_FACTOR,		SMAC_PHASE_C_CURRENT_CREST_FACTOR},
		{SMAC_CH_PHASE_A_CURRENT_THD,				SMAC_PHASE_A_CURRENT_THD},
		{SMAC_CH_PHASE_B_CURRENT_THD,				SMAC_PHASE_B_CURRENT_THD},
		{SMAC_CH_PHASE_C_CURRENT_THD,				SMAC_PHASE_C_CURRENT_THD},
		{SMAC_CH_PHASE_A_VOLT_THD,					SMAC_PHASE_A_VOLT_THD},
		{SMAC_CH_PHASE_B_VOLT_THD,					SMAC_PHASE_B_VOLT_THD},
		{SMAC_CH_PHASE_C_VOLT_THD,					SMAC_PHASE_C_VOLT_THD},
		{SMAC_CH_TOTAL_REAL_ENERGY,					SMAC_TOTAL_REAL_ENERGY},
		{SMAC_CH_TOTAL_REACTIVE_ENERGY,				SMAC_TOTAL_REACTIVE_ENERGY},
		{SMAC_CH_APPARENT_ENERGY,					SMAC_APPARENT_ENERGY},
		{SMAC_CH_AMBIENT_TEMPERATURE,				SMAC_AMBIENT_TEMPERATURE},
		{SMAC_CH_BATTERY_START_VOLTAGE,				SMAC_BATTERY_START_VOLTAGE},

		{SMAC_CH_COMM_STATUS,						SMAC_COMM_STATUS},
		{SMAC_CH_EXIST_STATUS,						SMAC_WORKING_STATUS},
		{SMAC_CH_SIG_END,							SMAC_SIG_END},
	};


	RS485_CHN_TO_ROUGH_DATA SMAC_DIESELSig4X[]=
	{
		//channel no								rough data
		{SMAC_CH_OUT_PHASE_VLT_A,					SMAC_OUT_PHASE_VLT_A},
		{SMAC_CH_OUT_PHASE_VLT_B,					SMAC_OUT_PHASE_VLT_B},	
		{SMAC_CH_OUT_PHASE_VLT_C,					SMAC_OUT_PHASE_VLT_C},	
		{SMAC_CH_OUT_LINE_VLT_AB,					SMAC_OUT_LINE_VLT_AB},
		{SMAC_CH_OUT_LINE_VLT_BC,					SMAC_OUT_LINE_VLT_BC},
		{SMAC_CH_OUT_LINE_VLT_CA,					SMAC_OUT_LINE_VLT_CA},
		{SMAC_CH_OUT_CURRENT_A,						SMAC_OUT_CURRENT_A},
		{SMAC_CH_OUT_CURRENT_B,						SMAC_OUT_CURRENT_B},
		{SMAC_CH_OUT_CURRENT_C,						SMAC_OUT_CURRENT_C},
		{SMAC_CH_FREQUENCY,							SMAC_FREQUENCY},
		{SMAC_CH_TOTAL_REAL_POWER,					SMAC_TOTAL_REAL_POWER},
		{SMAC_CH_PHASE_A_REAL_POWER,				SMAC_PHASE_A_REAL_POWER},
		{SMAC_CH_PHASE_B_REAL_POWER,				SMAC_PHASE_B_REAL_POWER},
		{SMAC_CH_PHASE_CREAL_POWER,					SMAC_PHASE_CREAL_POWER},
		{SMAC_CH_TOTAL_REACTIVE_POWER,				SMAC_TOTAL_REACTIVE_POWER},
		{SMAC_CH_PHASE_A_REACTIVE_POWER,			SMAC_PHASE_A_REACTIVE_POWER},
		{SMAC_CH_PHASE_B_REACTIVE_POWER,			SMAC_PHASE_B_REACTIVE_POWER},
		{SMAC_CH_PHASE_CREACTIVE_POWER,				SMAC_PHASE_CREACTIVE_POWER},
		{SMAC_CH_TOTAL_APPARENT_POWER,				SMAC_TOTAL_APPARENT_POWER},
		{SMAC_CH_PHASE_A_APPARENT_POWER,			SMAC_PHASE_A_APPARENT_POWER},
		{SMAC_CH_PHASE_B_APPARENT_POWER,			SMAC_PHASE_B_APPARENT_POWER},
		{SMAC_CH_PHASE_C_APPARENT_POWER,			SMAC_PHASE_C_APPARENT_POWER},
		{SMAC_CH_POWER_FACTOR,						SMAC_POWER_FACTOR},
		{SMAC_CH_PHASE_A_POWER_FACTOR,				SMAC_PHASE_A_POWER_FACTOR},
		{SMAC_CH_PHASE_B_POWER_FACTOR,				SMAC_PHASE_B_POWER_FACTOR},
		{SMAC_CH_PHASE_C_POWER_FACTOR,				SMAC_PHASE_C_POWER_FACTOR},
		{SMAC_CH_PHASE_A_CURRENT_CREST_FACTOR,		SMAC_PHASE_A_CURRENT_CREST_FACTOR},
		{SMAC_CH_PHASE_B_CURRENT_CREST_FACTOR,		SMAC_PHASE_B_CURRENT_CREST_FACTOR},
		{SMAC_CH_PHASE_C_CURRENT_CREST_FACTOR,		SMAC_PHASE_C_CURRENT_CREST_FACTOR},
		{SMAC_CH_PHASE_A_CURRENT_THD,				SMAC_PHASE_A_CURRENT_THD},
		{SMAC_CH_PHASE_B_CURRENT_THD,				SMAC_PHASE_B_CURRENT_THD},
		{SMAC_CH_PHASE_C_CURRENT_THD,				SMAC_PHASE_C_CURRENT_THD},
		{SMAC_CH_PHASE_A_VOLT_THD,					SMAC_PHASE_A_VOLT_THD},
		{SMAC_CH_PHASE_B_VOLT_THD,					SMAC_PHASE_B_VOLT_THD},
		{SMAC_CH_PHASE_C_VOLT_THD,					SMAC_PHASE_C_VOLT_THD},
		{SMAC_CH_TOTAL_REAL_ENERGY,					SMAC_TOTAL_REAL_ENERGY},
		{SMAC_CH_TOTAL_REACTIVE_ENERGY,				SMAC_TOTAL_REACTIVE_ENERGY},
		{SMAC_CH_APPARENT_ENERGY,					SMAC_APPARENT_ENERGY},
		{SMAC_CH_AMBIENT_TEMPERATURE,				SMAC_AMBIENT_TEMPERATURE},
		{SMAC_CH_BATTERY_START_VOLTAGE,				SMAC_BATTERY_START_VOLTAGE},
		
		//diesel sig
		{DIESEL_CH_SMAC_SWITCH_2,					SMAC_SWITCH_2},
		{DIESEL_CH_SMAC_SWITCH_3,					SMAC_SWITCH_3},
		{DIESEL_CH_SMAC_RELAY_2_STATUS,				SMAC_RELAY_2_STATUS},
		{DIESEL_CH_SMAC_RELAY_3_STATUS,				SMAC_RELAY_3_STATUS},
		{DIESEL_CH_SMAC_RELAY_4_STATUS,				SMAC_RELAY_4_STATUS},

		//added /09/12/25
		{DIESEL_CH_TEST_START_STOP_STAT,			SMAC_DIESEL_TEST_START_STOP_STAT},
		{DIESEL_CH_TEST_STOP_CAUSE,					SMAC_DIESEL_TEST_STOP_CAUSE},

		{DIESEL_CH_SMAC_ALARM_HI_WATER_TEMPERATURE,	SMAC_ALARM_HI_WATER_TEMPERATURE},
		{DIESEL_CH_SMAC_ALARM_LOW_OIL_PRESSURE,		SMAC_ALARM_LOW_OIL_PRESSURE},
		{DIESEL_CH_SMAC_ALARM_LOW_OIL_POSITION,		SMAC_ALARM_LOW_OIL_POSITION},
		{DIESEL_CH_SMAC_ALARM_DIESEL_GENERATOR_FAIL,SMAC_ALARM_DIESEL_GENERATOR_FAIL},

		{DIESEL_CH_SMAC_GETANLG_VOLTAGE_LOWER,		SMAC_GETANLG_VOLTAGE_LOWER},
		{DIESEL_CH_SMAC_MAX_DURATION,				SMAC_GETSYS_TEST_MAX_TIME},

		{SMAC_CH_COMM_STATUS,						SMAC_COMM_STATUS},
		{SMAC_CH_EXIST_STATUS,						SMAC_WORKING_STATUS},

		{SMAC_CH_DISEL_STAT,						SMAC_DISEL_EXIST_STAT},
		{SMAC_CH_DISEL_COMM_STAT,					SMAC_DISEL_COMM_STAT},
		// 0	----	39

		{SMAC_CH_SIG_END,							SMAC_SIG_END},
	};

	//Stuff  first smac equipment,if Address is 104 means have diesel
	RS485_VALUE			stTempbExist;
	stTempbExist.iValue		= SMAC_EQUIP_EXISTENT;
	iRoughDataPosition		= 0 * SMAC_MAX_SIGNALS_NUM;
	iChannelPosition		= 0 * MAX_CHN_NUM_PER_SMAC;


	for (iRoughSequenceIdx = 0; iRoughSequenceIdx < SMAC_NUM; iRoughSequenceIdx++)
	{
		iRoughDataPosition	= iRoughSequenceIdx * SMAC_MAX_SIGNALS_NUM;
		iChannelPosition	= iRoughSequenceIdx * MAX_CHN_NUM_PER_SMAC;	

		if (0 == iRoughSequenceIdx)//��Ϊ������ͻ����϶��ŵ���һ��������
		{
			if (SmacDeviceClass->pRoughData[iRoughDataPosition + SMAC_HAVE_SMAC_ADDR].iValue 
				== SMAC_ADDR_START)
			{
				stTempbExist.iValue		= SMAC_EQUIP_EXISTENT;
				EnumProc(SMAC_CH_HAVE_DIESEL, stTempbExist.fValue, lpvoid);
				
				//����һ�ű�����ͻ���
				j = 0;
				while(SMAC_DIESELSig4X[j].iChannel != SMAC_CH_SIG_END)
				{
					EnumProc(iChannelPosition + SMAC_DIESELSig4X[j].iChannel,
						SmacDeviceClass->pRoughData[iRoughDataPosition + SMAC_DIESELSig4X[j].iRoughData].fValue,
						lpvoid );
					j++;
				}
			}
			else
			{
				stTempbExist.iValue = SMAC_EQUIP_NOT_EXISTENT;
				//����ͻ������� 
				EnumProc(SMAC_CH_HAVE_DIESEL, stTempbExist.fValue, lpvoid);
				EnumProc(422, stTempbExist.fValue, lpvoid);
				
				j = 0;
				while(SMACSig41[j].iChannel != SMAC_CH_SIG_END)
				{
					EnumProc(iChannelPosition + SMACSig41[j].iChannel,
						SmacDeviceClass->pRoughData[iRoughDataPosition + SMACSig41[j].iRoughData].fValue,
						lpvoid);
					j++;
				}	
			}
		}
		else
		{
			//��  �ɼ��������õ�ʱ���Ѿ���RoughData ��Ϊ��-9999
			j = 0;
			while(SMACSig41[j].iChannel != SMAC_CH_SIG_END)
			{
				EnumProc(iChannelPosition + SMACSig41[j].iChannel,
					SmacDeviceClass->pRoughData[iRoughDataPosition + SMACSig41[j].iRoughData].fValue,
					lpvoid);
				j++;
			}	
		}
	
	}

	return 0;	
}
static BYTE SMAC_AscToHex(char c)
{
	if (c >= '0' && c <= '9')
	{
		return c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		return c - 'A' + 10;
	}

	return 0xFF;
}
/*=============================================================================*
* FUNCTION: SMAC_MergeAsc()
* PURPOSE : Assistant function, used to merge two ASCII chars
* INPUT: 
*			CHAR*	p
*
* RETURN:
*		static BYTE : merge result
*
* CALLS: 
*     void
*
* CALLED BY: 
*     
*		 copy from SMAC        DATE: 2009-05-08		14:39
*============================================================================*/
static BYTE SMAC_MergeAsc(char *p)
{
	BYTE byHi, byLo;
	byHi = SMAC_AscToHex(p[0]);
	byLo = SMAC_AscToHex(p[1]);

	//error data
	if (byHi == 0xFF || byLo == 0xFF)
	{
		return 0;
	}

	return (byHi*16 + byLo);
}
/*=============================================================================*
* FUNCTION: SMACUnpackPrdctInfoAndBarCode
* PURPOSE : Arrange sampling data by 0xEC And by 0x51 to  struct st_RecordProdcInfo
* INPUT: 
*     
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
LOCAL void SMACUnpackPrdctInfoAndBarCode(PRODUCT_INFO * pInfo,BYTE *ReceiveFrame,BYTE *ReceiveFrameTwo)
{
	int i, iLen;
	BYTE szLenInfo[4] = {0};
	BYTE byMainVer, bySubVer;
	if (ReceiveFrame[7] == '0' && ReceiveFrame[8] == '4')
	{
		TRACE("Old module, not support product info!\n");
		//old module, clear all data
		pInfo->bSigModelUsed = FALSE;
		pInfo->szHWVersion[0] = 0;
		pInfo->szPartNumber[0] = 0;
		pInfo->szSerialNumber[0] = 0;
		pInfo->szSWVersion[0] = 0;
		return;
	}
	//1.get length
	for (i = 0 ; i < 3; i++)
	{
		szLenInfo[i] = ReceiveFrame[10 + i];
	}
	sscanf((char*)szLenInfo, "%03x", &iLen);

	//1.Serial number process
	for (i = 0; i < 11; i++)
	{
		if((SMAC_MergeAsc((char*)(ReceiveFrame + 13 + i * 2))>=48 && SMAC_MergeAsc((char*)(ReceiveFrame + 13 + i * 2))<=57)||
			(SMAC_MergeAsc((char*)(ReceiveFrame + 13 + i * 2))>=65 && SMAC_MergeAsc((char*)(ReceiveFrame + 13 + i * 2))<=90)||
			(SMAC_MergeAsc((char*)(ReceiveFrame + 13 + i * 2))>=97 && SMAC_MergeAsc((char*)(ReceiveFrame + 13 + i * 2))<=122))
		{
			pInfo->szSerialNumber[i] = SMAC_MergeAsc((char*)(ReceiveFrame + 13 + i * 2));

		}
		else
		{
			pInfo->szSerialNumber[i] = ' ';
		}

	}

	//2.HW version process - the last three character
	//printf("\n PartNumber process = %s\n", (char *)Frame+13+iLen-6);
	for (i = 0; i < 3; i++)
	{
		if((SMAC_MergeAsc((char*)(ReceiveFrame + 13 + iLen - 6 + i * 2))>=48 && SMAC_MergeAsc((char*)(ReceiveFrame + 13 + iLen - 6 + i * 2))<=57)||
			(SMAC_MergeAsc((char*)(ReceiveFrame + 13 + iLen - 6 + i * 2))>=65 && SMAC_MergeAsc((char*)(ReceiveFrame + 13 + iLen - 6 + i * 2))<=90)||
			(SMAC_MergeAsc((char*)(ReceiveFrame + 13 + iLen - 6 + i * 2))>=97 && SMAC_MergeAsc((char*)(ReceiveFrame + 13 + iLen - 6 + i * 2))<=122))
		{
			pInfo->szHWVersion[i] = SMAC_MergeAsc((char*)(ReceiveFrame + 13 + iLen - 6 + i * 2));
		}
		else
		{
			pInfo->szHWVersion[i] = ' ';

		}
	}

	//3.PartNumber process
	//printf("\n PartNumber process = %s\n", (char *)Frame+13+22);
	if ((iLen/2 - 14) > 16)
	{
		for (i = 0; i < 16; i++)
		{
			if((SMAC_MergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)>=48 && SMAC_MergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)<=57)||
				(SMAC_MergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)>=65 && SMAC_MergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)<=90)||
				(SMAC_MergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)>=97 && SMAC_MergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)<=122))
			{
				pInfo->szPartNumber[i] = SMAC_MergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2);

			}
			else
			{
				pInfo->szPartNumber[i] = ' ';
			}

		}
	}
	else
	{
		for (i = 0; i < iLen/2-14; i++)
		{
			if((SMAC_MergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)>=48 && SMAC_MergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)<=57)||
				(SMAC_MergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)>=65 && SMAC_MergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)<=90)||
				(SMAC_MergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)>=97 && SMAC_MergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2)<=122))
			{
				pInfo->szPartNumber[i] = SMAC_MergeAsc((char*)ReceiveFrame + 13 + 11 * 2 + i * 2);

			}
			else
			{
				pInfo->szPartNumber[i] = ' ';
			}

		}
	}

	byMainVer = SMAC_MergeAsc((char*)ReceiveFrameTwo + 40 + 13);
	bySubVer  = SMAC_MergeAsc((char*)ReceiveFrameTwo + 42 + 13);
	int iTempMainVer,iTempSubVer;
	iTempMainVer = byMainVer;
	iTempSubVer = bySubVer;

	if ( ( ((int)iTempMainVer >= 0) && ((int)iTempMainVer <= 16) ) &&
		( ((int)iTempSubVer >= 0) && ((int)iTempSubVer <= 16) ) )
	{
		sprintf(pInfo->szSWVersion, "%d.%02d\0", byMainVer, bySubVer);
	}

	//printf("SMAC BBU PartNumber:%s\n", pInfo->szPartNumber);
	//printf("SMAC BBU SW version:%s\n", pInfo->szSWVersion);
	//printf("SMAC BBU HW version:%s\n", pInfo->szHWVersion);
	//printf("SMAC BBU Serial Number:%s\n", pInfo->szSerialNumber);
}
/*=============================================================================*
* FUNCTION: SMACGetProdctInfo
* PURPOSE : Get SMAC Product Info 
* INPUT: 
*     
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void SMAC_GetProdctInfo(HANDLE hComm, int nUnitNo, void *pPI)
{
	UNUSED(hComm);
	UNUSED(nUnitNo);
	PRODUCT_INFO *pInfo;
	INT32 i,k;
	INT32 iIdx;
	INT32 iAddr;
	INT32 iSmACStat;
	char szVer[3] = "20";
	BYTE szSendStr[40] = {0};
	BYTE ReceiveFrame[MAX_RECVBUFFER_NUM_SMAC] = {0}; 
	BYTE ReceiveFrameTwo[MAX_RECVBUFFER_NUM_SMAC] ={0};
	
	pInfo = (PRODUCT_INFO *)pPI;
	pInfo += SMAC1_EID;
	INT32					iSmacIndexMap;
	RS485_DEVICE_CLASS*		pSmacDeviceClass	= g_RS485Data.pDeviceClass + DEVICE_CLASS_SMAC;
	
	if (pSmacDeviceClass->pRoughData == NULL)
	{
		TRACE(" G	BBU  GetProductInfo IN  SLAVE Mode  :  \n");	
		return;
	}

	if (SMACNeedCommProc())
	{
	    SMACReopenPort();
	}

	//aRoughDataSMAC[SMAC_NUM][SMAC_MAX_SIGNALS_NUM]; SMAC_NUM = 2
	for(iIdx = 0; iIdx < SMAC_NUM; iIdx++)
	{
		iSmacIndexMap = iIdx * SMAC_MAX_SIGNALS_NUM;
		
		iAddr = pSmacDeviceClass->pRoughData[iSmacIndexMap + SMAC_HAVE_SMAC_ADDR].iValue;
		
		if(pSmacDeviceClass->pRoughData[iSmacIndexMap + SMAC_HAVE_SMAC_ADDR].iValue 
			== SMAC_SAMP_INVALID_VALUE)
		{
			continue;//��ǰ�ѷţ�pInfo��Ҫ��
		}
		
		Sleep(10);

		//Get ENP BarCode
		sprintf((char *)szSendStr, 
			"~%s%02X%02X%02X%04X\0",szVer, (unsigned int)iAddr, SMAC_CID1, 0xEC, 0);
		CheckSum(szSendStr);

		for (i = 0; i < 5; i++)
		{
			iSmACStat = SMACSendAndRead(g_RS485Data.CommPort.hCommPort,
										szSendStr,
										ReceiveFrame,
										18,
										RS485_WAIT_SHORT_TIME,
										4096);
			Sleep(100);

			if (RESPONSE_OK == iSmACStat)
			{
				pInfo->bSigModelUsed = TRUE;
				Sleep(30);
				//Sampling Product Info by 0x51
				sprintf((char *)szSendStr, 
					"~%s%02X%02X%02X%04X\0",szVer, (unsigned int)iAddr, SMAC_CID1, 0x51, 0);
				CheckSum(szSendStr);

				for (k = 0;k < 5; k++)
				{
					iSmACStat = SMACSendAndRead(g_RS485Data.CommPort.hCommPort,
											szSendStr,
											ReceiveFrameTwo,
											18,
											RS485_WAIT_SHORT_TIME,
											4096);

					if ((RESPONSE_OK == iSmACStat))
					{
						break;
					}
					else
					{
						//k ++
					}
				}

	
				SMACUnpackPrdctInfoAndBarCode(pInfo, ReceiveFrame, ReceiveFrameTwo);
				pInfo++;//��ǰ�ѷţ��������Զ��Ǵ��ڵĲ�++
				break;
			}
			else
			{
				//i++
			}
		}
	}

	if (g_RS485Data.CommPort.bOpened)
	{	
		RS485_Close(g_RS485Data.CommPort.hCommPort);
		g_RS485Data.CommPort.bOpened = FALSE;
		g_RS485Data.CommPort.enumAttr = 0;
		g_RS485Data.CommPort.enumBaud = 0;
	}
}


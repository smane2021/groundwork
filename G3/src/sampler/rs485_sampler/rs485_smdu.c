/*============================================================================*
 *         Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : Unified Data Type Definition
 *
 *  FILENAME : rs485_smdu.c
 *  PURPOSE  : Sampling  Smdu data and control Smdu equip
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2007-07-12      V1.0           IlockTeng         Created.    
 *    2009-04-14     
 *                                                     
 *-----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *          
 *      
 *-----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *      
 *    
 *============================================================================*/
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "rs485_smdu.h"
#include "rs485_comm.h"

#define SM_CH_END_FLAG			(-1)
extern 	RS485_SAMPLER_DATA		g_RS485Data;
SMDU_RS485_SAMPLER_DATA			g_SmduSamplerData;
BYTE							SMDUszVer[5] = {0}; 
LOCAL INT32 SMDUSendAndRead(
			    HANDLE	hComm,			 
			    BYTE		*sSendStr,		 
			    BYTE		*abyRcvBuf,      
			    INT32	nSend,			 
			    int     cWaitTime,
			    INT32	iWantToReadnByte);

LOCAL SMDUDATASTRUCT s_SmduCmd42SigInfo[] =
{
	//Anolog	signal
	{ 1,  0,	SMDU_BUS_BAR_VOLTAGE },
	{ 1,  8,	SMDU_BATTERY_1_FUSE_VOLTG},//unused
	{ 1,  16,	SMDU_BATTERY_2_FUSE_VOLTG},//unused
	{ 1,  24,	SMDU_BATTERY_3_FUSE_VOLTG},//unused
	{ 1,  32,	SMDU_BATTERY_4_FUSE_VOLTG},//unused
	{ 1,  40,	SMDU_LOAD_CURRENT_1},
	{ 1,  48,	SMDU_LOAD_CURRENT_2},
	{ 1,  56,	SMDU_LOAD_CURRENT_3},
	{ 1,  64,	SMDU_LOAD_CURRENT_4},
	{ 1,  72,	SMDU_ALL_LOAD_CURRENT},
	{ 1,  80,	SMDU_BATTERY_CURRENT_1},
	{ 1,  88,	SMDU_BATTERY_CURRENT_2},
	{ 1,  96,	SMDU_BATTERY_CURRENT_3},
	{ 1,  104,	SMDU_BATTERY_CURRENT_4},
	{ 1,  112,	SMDU_BATTERY_VOLTAGE_1},
	{ 1,  120,	SMDU_BATTERY_VOLTAGE_2},
	{ 1,  128,	SMDU_BATTERY_VOLTAGE_3},
	{ 1,  136,	SMDU_BATTERY_VOLTAGE_4},
	{ 1,  144,	SMDU_BATTERY_RATED_CAP},
	{ 1,  152,	SMDU_LVD_1_OFF_VOLTAGE},
	{ 1,  160,	SMDU_LVD_1_ON_VOLTAGE},
	{ 1,  168,	SMDU_LVD_2_OFF_VOLTAGE},
	{ 1,  176,	SMDU_LVD_2_ON_VOLTAGE},
	//Switch signal
	{ 3,  184,	SMDU_LOAD_FUSE_INPUT_1},
	{ 3,  186,	SMDU_LOAD_FUSE_INPUT_2},
	{ 3,  188,	SMDU_LOAD_FUSE_INPUT_3},
	{ 3,  190,	SMDU_LOAD_FUSE_INPUT_4},
	{ 3,  192,	SMDU_LOAD_FUSE_INPUT_5},
	{ 3,  194,	SMDU_LOAD_FUSE_INPUT_6},
	{ 3,  196,	SMDU_LOAD_FUSE_INPUT_7},
	{ 3,  198,	SMDU_LOAD_FUSE_INPUT_8},
	{ 3,  200,	SMDU_LOAD_FUSE_INPUT_9},
	{ 3,  202,	SMDU_LOAD_FUSE_INPUT_10},
	{ 3,  204,	SMDU_LOAD_FUSE_INPUT_11},
	{ 3,  206,	SMDU_LOAD_FUSE_INPUT_12},
	{ 3,  208,	SMDU_LOAD_FUSE_INPUT_13},
	{ 3,  210,	SMDU_LOAD_FUSE_INPUT_14},
	{ 3,  212,	SMDU_LOAD_FUSE_INPUT_15},
	{ 3,  214,	SMDU_LOAD_FUSE_INPUT_16},
	{ 3,  216,	SMDU_CONTACTOR_INPUT_1},
	{ 3,  218,	SMDU_CONTACTOR_INPUT_2},
	//DIP switch signal
	{ 3,  220,	SMDU_DIP_SWITCH1_1},
	{ 3,  222,	SMDU_DIP_SWITCH1_2},
	{ 3,  224,	SMDU_DIP_SWITCH1_3},
	{ 3,  226,	SMDU_DIP_SWITCH1_4},
	{ 3,  228,	SMDU_DIP_SWITCH1_5},
	{ 3,  230,	SMDU_DIP_SWITCH1_6},
	{ 3,  232,	SMDU_DIP_SWITCH1_7},
	{ 3,  234,	SMDU_DIP_SWITCH1_8},
	//Switch 2
	{ 3,  236,	SMDU_DIP_SWITCH2_1},
	{ 3,  238,	SMDU_DIP_SWITCH2_2},
	{ 3,  240,	SMDU_DIP_SWITCH2_3},
	{ 3,  242,	SMDU_DIP_SWITCH2_4},
	{ 3,  244,	SMDU_DIP_SWITCH2_5},
	{ 3,  246,	SMDU_DIP_SWITCH2_6},
	{ 3,  248,	SMDU_DIP_SWITCH2_7},
	{ 3,  250,	SMDU_DIP_SWITCH2_8},
	//Switch 3
	{ 3,  252,	SMDU_DIP_SWITCH3_1},
	{ 3,  254,	SMDU_DIP_SWITCH3_2},
	{ 3,  256,	SMDU_DIP_SWITCH3_3},
	{ 3,  258,	SMDU_DIP_SWITCH3_4},
	{ 3,  260,	SMDU_DIP_SWITCH3_5},
	{ 3,  262,	SMDU_DIP_SWITCH3_6},
	{ 3,  264,	SMDU_DIP_SWITCH3_7},
	{ 3,  266,	SMDU_DIP_SWITCH3_8},
	{ 3,  268,	SMDU_RUNNING_JUMP_STAT},
	{ 3,  270,	SMDU_LVD_1_STATUS},
	{ 3,  272,	SMDU_LVD_2_STATUS},
	{ 3,  274,	SMDU_LOAD_FUSE_1_ARLAM_STAT},
	{ 3,  276,	SMDU_LOAD_FUSE_2_ARLAM_STAT},
	{ 3,  278,	SMDU_LOAD_FUSE_3_ARLAM_STAT},
	{ 3,  280,	SMDU_LOAD_FUSE_4_ARLAM_STAT},
	{ 3,  282,	SMDU_LOAD_FUSE_5_ARLAM_STAT},
	{ 3,  284,	SMDU_LOAD_FUSE_6_ARLAM_STAT},
	{ 3,  286,	SMDU_LOAD_FUSE_7_ARLAM_STAT},
	{ 3,  288,	SMDU_LOAD_FUSE_8_ARLAM_STAT},
	{ 3,  290,	SMDU_LOAD_FUSE_9_ARLAM_STAT},
	{ 3,  292,	SMDU_LOAD_FUSE_10_ARLAM_STAT},
	{ 3,  294,	SMDU_LOAD_FUSE_11_ARLAM_STAT},
	{ 3,  296,	SMDU_LOAD_FUSE_12_ARLAM_STAT},
	{ 3,  298,	SMDU_LOAD_FUSE_13_ARLAM_STAT},
	{ 3,  300,	SMDU_LOAD_FUSE_14_ARLAM_STAT},
	{ 3,  302,	SMDU_LOAD_FUSE_15_ARLAM_STAT},
	{ 3,  304,	SMDU_LOAD_FUSE_16_ARLAM_STAT},
	{ 3,  306,	SMDU_LVD_1_FAULT_STAT},
	{ 3,  308,	SMDU_LVD_2_FAULT_STAT},
	{ 3,  310,	SMDU_EQUIP_SELFONESELF_STAT},
	{ 3,  312,	SMDU_LVD_1_OFF_ALARM},
	{ 3,  314,	SMDU_LVD_2_OFF_ALARM},
	{ 3,  316,	SMDU_BUS_BAR_VOLTAGE_ALARM},//6544 need  process
	{ 3,  318,	SMDU_BATTERY_FUSE_1_STAT},//6543
	{ 3,  320,	SMDU_BATTERY_FUSE_2_STAT},//6542
	{ 3,  322,	SMDU_BATTERY_FUSE_3_STAT},//6554
	{ 3,  324,	SMDU_BATTERY_FUSE_4_STAT},//6553
	{ 3,  326,	SMDU_CURRENT_OVER_CURRENT_1},//6557 need  process
	{ 3,  328,	SMDU_CURRENT_OVER_CURRENT_2},//6558
	{ 3,  330,	SMDU_CURRENT_OVER_CURRENT_3},//6559
	{ 3,  332,	SMDU_CURRENT_OVER_CURRENT_4},//6560


	//Last 28 byte by SMDUU npackCmdfa
    { 0,  0, 0 }   // end
};

/*=============================================================================*
 * FUNCTION: SMDUCheckSum()
 * PURPOSE : Cumulative  result
 * INPUT:	 BYTE *Frame
 *     
 *
 * RETURN:
 *     static void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *    
 *			 Copy from SMAC        DATE: 2009-05-08		14:39
 *============================================================================*/
static void SMDUCheckSum( BYTE *Frame )
{
    WORD R = 0;

    int i, nLen = (int)strlen( (char*)Frame );  

    for( i=1; i<nLen; i++ )
	{
        R += *(Frame+i);
	}
    sprintf( (char *)Frame+nLen, "%04X\r", (WORD)-R );
}
/*=============================================================================*
 * FUNCTION: RS485_WriteFlashData
 * PURPOSE : Write flash by Rs485 Smdu Sampler
 * INPUT: 
 *     
 *
 * RETURN:
 *     BOOL
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     
 *		Copy from Frank Cao 
 *============================================================================*/
BOOL RS485_WriteFlashData(OUT RS485_FLASH_DATA* pFlashData)
{
	HANDLE			hCanFlashData;
	hCanFlashData = DAT_StorageCreate(OLD_CAN_SAMPLER_FLASH_DATA, 
									sizeof(RS485_FLASH_DATA), 
									OLD_CAN_SAMPLER_DATA_RECORDS, 
									OLD_CAN_SAMPLER_DATA_FREQUENCY);
	
	if(!hCanFlashData)
	{
		//AppLogOut("RS485_SMDU", (APP_LOG_WARNING), "%s", "Failed to Write CAN_SAMPLER_DATA log!\n");
		return FALSE;
	}
	else
	{	
		BOOL			bRst;
		bRst = DAT_StorageWriteRecord(hCanFlashData,
									sizeof(RS485_FLASH_DATA),
									pFlashData);
		TRACE("RS485: Written flash once!!!\n ");

		if(!bRst)
		{
			return FALSE;
		}

		bRst = DAT_StorageClose(hCanFlashData);

		if(!bRst)
		{				
			//AppLogOut("RS485_SMDU", 
						//(APP_LOG_WARNING),
						//"%s", "Failed to Write CAN_SAMPLER_DATA log!\n"
						//);
			return FALSE;
		}
	}
	return TRUE;
}

/*=============================================================================*
 * FUNCTION: SMDU _RefreshFlashInfo
 * PURPOSE : Refresh flash
 * INPUT: 
 *     
 *
 * RETURN:
 *     static void 
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
static void SMDU_RefreshFlashInfo(void)
{
	int			i;

	for(i = 0; i < SMDU_MAX_NUM; i++)
	{
		g_SmduSamplerData.OLD_CANFlashData.aSmduInfo[i].dwHighSn
			= g_SmduSamplerData.aRoughDataSmdu[i][SMDU_SERIAL_NO_H].dwValue;
		g_SmduSamplerData.OLD_CANFlashData.aSmduInfo[i].dwSerialNo
			= g_SmduSamplerData.aRoughDataSmdu[i][SMDU_SERIAL_NO_L].dwValue;
		g_SmduSamplerData.OLD_CANFlashData.aSmduInfo[i].iSeqNo
			= g_SmduSamplerData.aRoughDataSmdu[i][SMDU_RS485_SEQ_NO].iValue;
		g_SmduSamplerData.OLD_CANFlashData.aSmduInfo[i].iAddress 
			= i;

		if(g_SmduSamplerData.OLD_CANFlashData.aSmduInfo[i].iSeqNo 
			== SMDU_SAMP_INVALID_VALUE)
		{
			g_SmduSamplerData.OLD_CANFlashData.aSmduInfo[i].bExistence = FALSE;
		}
		else
		{
			g_SmduSamplerData.OLD_CANFlashData.aSmduInfo[i].bExistence = TRUE;
		}
	}

	//interface function	hold
	//RS485_WriteFlashData(&(g_SmduSamplerData.OLD_CANFlashData));
}


/*=============================================================================*
 * FUNCTION: RS485_ReadFlashData
 * PURPOSE : Read flash by can Sampler
 * INPUT: 
 *     
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
static BOOL RS485_ReadFlashData(OUT RS485_FLASH_DATA* pFlashData)
{
	HANDLE			hCanFlashData;

	hCanFlashData = DAT_StorageOpen(CAN_SAMPLER_FLASH_DATA);//"can_data.log"	
	
	if(!hCanFlashData)
	{
		AppLogOut("RS485_SMDU", 
			(APP_LOG_WARNING), 
			"%s", "Failed to open CAN_SAMPLER_DATA log!\n"
			);
		return FALSE;
	}
	else
	{
		int		iStartRecordNo = 1;
		int		iRecords = 1;
		BOOL	bRst;
		
		bRst = DAT_StorageReadRecords(hCanFlashData,
									&iStartRecordNo,	//piStartRecordNo, 
									&iRecords,			//piRecords, 
									pFlashData,			//pBuff,
									FALSE,				//bActiveAlarm,
									FALSE);				//bAscending

		if(!bRst || (iRecords <= 0))
		{
			return FALSE;
		}

		bRst = DAT_StorageClose(hCanFlashData);

		if(!bRst)
		{				
			AppLogOut("RS485_SMDU",
				(APP_LOG_WARNING), 
				"%s", "Failed to close CAN_SAMPLER_DATA log!\n");	
			return FALSE;
		}
	}
	return TRUE;

}
/*=============================================================================*
 * FUNCTION: Get SmAddrBySeqNo
 * PURPOSE : Get smdu module address by SeqNo in the RoughData
 * INPUT:	 int iSeqNo
 *    
 *
 * RETURN:
 *     static int
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
//static int SMDUGetSmAddrBySeqNo(int iSeqNo)
//{
//	int		i;
//	
//	for(i = SMDU_ADDR_START; i <= SMDU_ADDR_END; i++)
//	{
//		if(g_SmduSamplerData.aRoughDataSmdu[i - SMDU_ADDR_START][SMDU_RS485_SEQ_NO].iValue
//			== iSeqNo)
//		{
//			return i;
//		}
//	}
//	
//	return -1;
//}

void Smdu_SetRunAs485Mode()
{
	SIG_ENUM	eRunningmode;	
	if (g_SmduSamplerData.aRoughDataSmdu[0][SMDU_GROUP_SM_ACTUAL_NUM].iValue > 0)
	{
		eRunningmode = RUN_MODE_IS_485;
		SetEnumSigValue(SYS_EQUIP_ID, SIG_TYPE_SAMPLING,RUN_MODE_SIG_ID,eRunningmode,"rs485 mode");		
	}
	else
	{
		//not process!!
	}

	return;
}


/*=============================================================================*
 * FUNCTION: SMDU _StuffChannel
 * PURPOSE : stuff samping data to web or gc  from smio  
 * INPUT:    RoughData[][] of the p_SmduDeviceClass
 *    
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMDU_StuffChannel(RS485_DEVICE_CLASS*  p_SmduDeviceClass,
						ENUMSIGNALPROC EnumProc,
						LPVOID lpvoid)
{
	INT32				i;
	INT32				j;
	INT32				iRghIdx;
	//INT32				iSmduNum = p_SmduDeviceClass->pRoughData[SMDU_GROUP_SM_ACTUAL_NUM].iValue;
	RS485_VALUE			stTempEquipExist;
	static	BOOL		s_bDetectDIPChange	= FALSE;	
	static	DWORD		s_dwChangeForWeb	= 0;

	if (s_bDetectDIPChange)
	{
		//for Web
		s_dwChangeForWeb++;

		if(s_dwChangeForWeb >= 255)
		{
			s_dwChangeForWeb = 0;
		}
		
		TRACE("\n $$$$$$$$$ s_bDetectDIPChange == TRUE \n");

		SetDwordSigValue(106,	//SMDU_GROUP_EQUIPID
						0,		//SIG_TYPE_SAMPLING
						2 + 1,	//CFG_CHANGED_SIG_ID
						s_dwChangeForWeb,
						"SMDURS485");

		s_bDetectDIPChange = FALSE; 
	}

	RS485_CHN_TO_ROUGH_DATA		SmduGroupSig[] =
	{
		{SM_CH_SMDU_GROUP_STATE,	SMDU_GROUP_SM_STATE,},
		{SM_CH_SMDU_NUM,			SMDU_GROUP_SM_ACTUAL_NUM,},

		{SM_CH_END_FLAG,			SM_CH_END_FLAG,			},
	};

	RS485_CHN_TO_ROUGH_DATA	SmduSig[]=
	{
		{SM_CH_LOAD_CURR1,			SMDU_LOAD_CURRENT_1,},
		{SM_CH_LOAD_CURR2,			SMDU_LOAD_CURRENT_2,},	
		{SM_CH_LOAD_CURR3,			SMDU_LOAD_CURRENT_3,},
		{SM_CH_LOAD_CURR4,			SMDU_LOAD_CURRENT_4,},
		{SM_CH_BUS_VOLT,			SMDU_BUS_BAR_VOLTAGE,},
		{SM_CH_BATT_CURR1,			SMDU_BATTERY_CURRENT_1,},
		{SM_CH_BATT_CURR2,			SMDU_BATTERY_CURRENT_2,},
		{SM_CH_BATT_CURR3,			SMDU_BATTERY_CURRENT_3,},
		{SM_CH_BATT_CURR4,			SMDU_BATTERY_CURRENT_4,},	
		{SM_CH_BATT_VOLT1,			SMDU_BATTERY_VOLTAGE_1,},
		{SM_CH_BATT_VOLT2,			SMDU_BATTERY_VOLTAGE_2,},
		{SM_CH_BATT_VOLT3,			SMDU_BATTERY_VOLTAGE_3,},
		{SM_CH_BATT_VOLT4,			SMDU_BATTERY_VOLTAGE_4,},	
		{SM_CH_SERIAL_NO_HIGH,		SMDU_SERIAL_NO_H,},
		{SM_CH_SERIAL_NO_LOW,		SMDU_SERIAL_NO_L,},
		{SM_CH_BARCODE1,			SMDU_BAR_CODE_1,},
		{SM_CH_BARCODE2,			SMDU_BAR_CODE_2,},
		{SM_CH_BARCODE3,			SMDU_BAR_CODE_3,},
		{SM_CH_BARCODE4,			SMDU_BAR_CODE_4,},
		{SM_CH_VERSION_NO,			SMDU_VERSION_NO,},
		{SM_CH_LOAD_FUSE16,			SMDU_LOAD_FUSE_16_ARLAM_STAT,},
		{SM_CH_LOAD_FUSE15,			SMDU_LOAD_FUSE_15_ARLAM_STAT,},
		{SM_CH_LOAD_FUSE14,			SMDU_LOAD_FUSE_14_ARLAM_STAT,},		
		{SM_CH_LOAD_FUSE13,			SMDU_LOAD_FUSE_13_ARLAM_STAT,},		
		{SM_CH_LOAD_FUSE12,			SMDU_LOAD_FUSE_12_ARLAM_STAT,},		
		{SM_CH_LOAD_FUSE11,			SMDU_LOAD_FUSE_11_ARLAM_STAT,},		
		{SM_CH_LOAD_FUSE10,			SMDU_LOAD_FUSE_10_ARLAM_STAT,},		
		{SM_CH_LOAD_FUSE9,			SMDU_LOAD_FUSE_9_ARLAM_STAT,},		
		{SM_CH_LOAD_FUSE8,			SMDU_LOAD_FUSE_8_ARLAM_STAT,},		
		{SM_CH_LOAD_FUSE7,			SMDU_LOAD_FUSE_7_ARLAM_STAT,},		
		{SM_CH_LOAD_FUSE6,			SMDU_LOAD_FUSE_6_ARLAM_STAT,},		
		{SM_CH_LOAD_FUSE5,			SMDU_LOAD_FUSE_5_ARLAM_STAT,},		
		{SM_CH_LOAD_FUSE4,			SMDU_LOAD_FUSE_4_ARLAM_STAT,},	
		{SM_CH_LOAD_FUSE3,			SMDU_LOAD_FUSE_3_ARLAM_STAT,},	
		{SM_CH_LOAD_FUSE2,			SMDU_LOAD_FUSE_2_ARLAM_STAT,},	
		{SM_CH_LOAD_FUSE1,			SMDU_LOAD_FUSE_1_ARLAM_STAT,},	
		{SM_CH_BATT_FUSE2,			SMDU_BATTERY_FUSE_2_STAT,},	
		{SM_CH_BATT_FUSE1,			SMDU_BATTERY_FUSE_1_STAT,},
		{SM_CH_DC_VOLT_ST,			SMDU_BUS_BAR_VOLTAGE_ALARM,},	
		//{SM_CH_LVD2_FAULT_ST,		SMDU_LVD_2_OFF_ALARM,},	//6545 
		//{SM_CH_LVD1_FAULT_ST,		SMDU_LVD_1_OFF_ALARM,},	//6546 
		//{SM_CH_LVD2_CMD_ST,			SMDU_LVD_2_FAULT_STAT,},//6547
		//{SM_CH_LVD1_CMD_ST,			SMDU_LVD_1_FAULT_STAT,},//6548
		{SM_CH_LVD2_FAULT_ST,		SMDU_LVD_2_FAULT_STAT,},//4		LVD2 Fault Status ::LVD2 back Sampling
		{SM_CH_LVD1_FAULT_ST,		SMDU_LVD_1_FAULT_STAT,},//						  ::LVD1 back Sampling
		{SM_CH_LVD2_CMD_ST,			SMDU_LVD_2_OFF_ALARM,},
		{SM_CH_LVD1_CMD_ST,			SMDU_LVD_1_OFF_ALARM,},

		{SM_CH_BATT1_EXIST_ST,		SMDU_BATT1_EXIST_ST,},
		{SM_CH_BATT2_EXIST_ST,		SMDU_BATT2_EXIST_ST,},
		{SM_CH_BATT3_EXIST_ST,		SMDU_BATT3_EXIST_ST,},
		{SM_CH_BATT4_EXIST_ST,		SMDU_BATT4_EXIST_ST,},		
		{SM_CH_BATT_FUSE4,			SMDU_BATTERY_FUSE_4_STAT,},	
		{SM_CH_BATT_FUSE3,			SMDU_BATTERY_FUSE_3_STAT,},	
		{SM_CH_EXIST_ST,			SMDU_RS485_EXISTENCE,},	
		{SM_CH_INTTERUPT_STATE,		SMDU_RS485_COMMUNICATION,},

		{SM_CH_BATT1_CURR_ST,		SMDU_CURRENT_OVER_CURRENT_1,},
		{SM_CH_BATT2_CURR_ST,		SMDU_CURRENT_OVER_CURRENT_2,},
		{SM_CH_BATT3_CURR_ST,		SMDU_CURRENT_OVER_CURRENT_3,},
		{SM_CH_BATT4_CURR_ST,		SMDU_CURRENT_OVER_CURRENT_4,},		
		//{SM_CH_BATT1_CURR_ST,		SMDU_BATT1_CURR_ST,		},
		//{SM_CH_BATT2_CURR_ST,		SMDU_BATT2_CURR_ST,		},
		//{SM_CH_BATT3_CURR_ST,		SMDU_BATT3_CURR_ST,		},
		//{SM_CH_BATT4_CURR_ST,		SMDU_BATT4_CURR_ST,		},
		{SM_CH_TOTAL_LOAD_CURR,		SMDU_ALL_LOAD_CURRENT,},
		{SM_CH_COMM_ADDR,			SMDU_RS485_ADDRESS,},
		
		/*
			��Ϊ����������SMDU��Ԫ������ҪGC���㣬�����������︳�����ֵ��������澯����
			Ȼ��GC�Լ����ٸ��丳ֵ
			����ע�⣬���ڶ������ļ���ֻ�и��źŵ��ź�IDΪ10���������ź�ID�ж�
			����䣬GC��DXISET
		*/

		//{SM_CH_RATED_CAP,			SMDU_BATTERY_RATED_CAP,},
		{SM_CH_OVER_VOLT,			SMDU_OVER_VOLT,},
		{SM_CH_UNDER_VOLT,			SMDU_UNDER_VOLT,},
		{SM_CH_BATT1_OVER_CURR,		SM_BATT1_OVER_CURR,},
		{SM_CH_BATT2_OVER_CURR,		SM_BATT2_OVER_CURR,},
		{SM_CH_BATT3_OVER_CURR,		SM_BATT3_OVER_CURR,},
		{SM_CH_BATT4_OVER_CURR,		SM_BATT4_OVER_CURR,},

		//added by Jimmy for CR (changing shunt para through ACU+ web)
		{SM_CH_SOFT_SWITCH,		SMDU_DIP_SWITCH3_1,},
		//{SM_CH_CURR5_ENABLE,		SMDU_CURR5_ENABLE,},

		//{SM_CH_SHUNT1_MV,		SMDU_SHUNT1_MV,},
		//{SM_CH_SHUNT1_A,		SMDU_SHUNT1_A,},
		//{SM_CH_SHUNT2_MV,		SMDU_SHUNT2_MV,},
		//{SM_CH_SHUNT2_A,		SMDU_SHUNT2_A,},
		//{SM_CH_SHUNT3_MV,		SMDU_SHUNT3_MV,},
		//{SM_CH_SHUNT3_A,		SMDU_SHUNT3_A,},
		//{SM_CH_SHUNT4_MV,		SMDU_SHUNT4_MV,},
		//{SM_CH_SHUNT4_A,		SMDU_SHUNT4_A,},
		//{SM_CH_SHUNT5_MV,		SMDU_SHUNT5_MV,},
		//{SM_CH_SHUNT5_A,		SMDU_SHUNT5_A,},

		{SM_CH_SHUNT1_VALUE_CHANGED,		SMDU_SHUNT1_VALUE_CHANGED,	},
		{SM_CH_SHUNT2_VALUE_CHANGED,		SMDU_SHUNT2_VALUE_CHANGED,	},
		{SM_CH_SHUNT3_VALUE_CHANGED,		SMDU_SHUNT3_VALUE_CHANGED,	},
		{SM_CH_SHUNT4_VALUE_CHANGED,		SMDU_SHUNT4_VALUE_CHANGED,	},
		//{SM_CH_SHUNT5_VALUE_CHANGED,		SMDU_SHUNT5_VALUE_CHANGED,	},
		
		{SM_CH_RS485_DC_FUSE_EXISTENCE,		SMDU_RS485_DC_FUSE_EXISTENCE,},

		{SM_CH_END_FLAG,		SM_CH_END_FLAG,},
	};

	//Stuff group signal
	i = 0;
	while(SmduGroupSig[i].iChannel != SM_CH_END_FLAG)
	{
		EnumProc(SmduGroupSig[i].iChannel, 
				p_SmduDeviceClass->pRoughData[SmduGroupSig[i].iRoughData].fValue, 
				lpvoid );
		i++;
	}
	//�����޸����㷨��������ǰ�� 20120602 Jimmy Wu
	//Stuff the channels for SMDU unit signals
	//for(i = 0; i < iSmduNum; i++)
	//{
	//	//INT32	iAddr = SMDUGetSmAddrBySeqNo(i);

	//	if(iAddr == -1)
	//	{
	//		break;
	//	}

	//	j = 0;
	//	while(SmduSig[j].iChannel != SM_CH_END_FLAG)
	//	{
	//		EnumProc(i * SMDU_CHANNEL_DISTANCE + SmduSig[j].iChannel, 
	//			g_SmduSamplerData.aRoughDataSmdu[iAddr - SMDU_ADDR_START][SmduSig[j].iRoughData].fValue, 
	//			lpvoid );
	//		j++;
	//	}
	//	
	//	//SM_CH_LOAD_CURR5  = 6506,
	//	//2010/06/09 485��֧��5·�����������ļ��������ˣ����Բ���ʾ��5·����
	//	stTempEquipExist.iValue = SMDU_SAMP_INVALID_VALUE;
	//	EnumProc(i * SMDU_CHANNEL_DISTANCE + 6506, 
	//					stTempEquipExist.fValue, 
	//					lpvoid );
	//	//2010/06/09
	//	//SM_CH_BATT5_EXIST_ST = 10542,
	//	EnumProc(i * SMDU_CHANNEL_DISTANCE + SM_CH_BATT5_EXIST_ST, 
	//		stTempEquipExist.fValue, 
	//		lpvoid );


	//	//Process DIP Swith for LCD and WEB
	//	for (iRghIdx = 0; iRghIdx < 16; iRghIdx++)
	//	{
	//		if (g_SmduSamplerData.aRoughDataSmdu[iAddr - SMDU_ADDR_START][SMDU_DIP_SWITCH2_1 + iRghIdx].iValue
	//			!= g_SmduSamplerData.aDIPRoughDataSmdu[iAddr - SMDU_ADDR_START][iRghIdx].iValue)
	//		{
	//			s_bDetectDIPChange = TRUE;

	//			//for LCD
	//			SetDwordSigValue(106,	//SMDU_GROUP_EQUIPID
	//							 0,		//SIG_TYPE_SAMPLING
	//							 2,		//CFG_CHANGED_SIG_ID
	//							 1,
	//							"SMDURS485");
	//			break;
	//		}
	//	}

	//	for (iRghIdx = 0; iRghIdx < 16; iRghIdx++)
	//	{
	//		g_SmduSamplerData.aDIPRoughDataSmdu[iAddr - SMDU_ADDR_START][iRghIdx].iValue
	//			= g_SmduSamplerData.aRoughDataSmdu[iAddr - SMDU_ADDR_START][iRghIdx + SMDU_DIP_SWITCH2_1].iValue;
	//	}
	//}

	//for(i = iSmduNum; i < SMDU_MAX_NUM; i++)
	//{
	//	stTempEquipExist.iValue = SMDU_SAMP_INVALID_VALUE;
	//	j = 0;
	//	while(SmduSig[j].iChannel != SM_CH_END_FLAG)
	//	{
	//		EnumProc(i * SMDU_CHANNEL_DISTANCE + SmduSig[j].iChannel, 
	//			stTempEquipExist.fValue, 
	//			lpvoid );
	//		j++;
	//	}

	//	stTempEquipExist.iValue = SMDU_EQUIP_NOT_EXISTENT;
	//	EnumProc(i * SMDU_CHANNEL_DISTANCE + SM_CH_EXIST_ST, 
	//		stTempEquipExist.fValue, 
	//		lpvoid );


	//	EnumProc(i * SMDU_CHANNEL_DISTANCE + SM_CH_RS485_DC_FUSE_EXISTENCE,//��˿�豸�Ĵ���״̬
	//		stTempEquipExist.fValue, 
	//		lpvoid );


	//	//Stuff the battery inexistence
	//	EnumProc(i * SMDU_CHANNEL_DISTANCE + SM_CH_BATT1_EXIST_ST, 
	//		stTempEquipExist.fValue, 
	//		lpvoid );
	//	EnumProc(i * SMDU_CHANNEL_DISTANCE + SM_CH_BATT2_EXIST_ST, 
	//		stTempEquipExist.fValue, 
	//		lpvoid );
	//	EnumProc(i * SMDU_CHANNEL_DISTANCE + SM_CH_BATT3_EXIST_ST, 
	//		stTempEquipExist.fValue, 
	//		lpvoid );
	//	EnumProc(i * SMDU_CHANNEL_DISTANCE + SM_CH_BATT4_EXIST_ST, 
	//		stTempEquipExist.fValue, 
	//		lpvoid );
	//	EnumProc(i * SMDU_CHANNEL_DISTANCE + SM_CH_BATT5_EXIST_ST, 
	//		stTempEquipExist.fValue, 
	//		lpvoid );
	//}
	for(i = 0; i < SMDU_MAX_NUM; i++)
	{
		if(g_SmduSamplerData.OLD_CANFlashData.aSmduInfo[i].bExistence)
		{
			j = 0;
			while(SmduSig[j].iChannel != SM_CH_END_FLAG)
			{
				EnumProc(i * SMDU_CHANNEL_DISTANCE + SmduSig[j].iChannel, 
					g_SmduSamplerData.aRoughDataSmdu[i][SmduSig[j].iRoughData].fValue, 
					lpvoid );
				j++;
			}
			//printf("\n[Stuffch]Batt Exist state: %d;%d;%d;%d\n",g_SmduSamplerData.aRoughDataSmdu[i][SMDU_BATT1_EXIST_ST].uiValue,
			//	g_SmduSamplerData.aRoughDataSmdu[i][SMDU_BATT2_EXIST_ST].uiValue,
			//	g_SmduSamplerData.aRoughDataSmdu[i][SMDU_BATT3_EXIST_ST].uiValue,
			//	g_SmduSamplerData.aRoughDataSmdu[i][SMDU_BATT4_EXIST_ST].uiValue);
			//SM_CH_LOAD_CURR5  = 6506,
			//2010/06/09 485��֧��5·�����������ļ��������ˣ����Բ���ʾ��5·����
			//stTempEquipExist.iValue = SMDU_EQUIP_NOT_EXISTENT;//SMDU_SAMP_INVALID_VALUE;
			//EnumProc(i * SMDU_CHANNEL_DISTANCE + 6506, 
			//				stTempEquipExist.fValue, 
			//				lpvoid );
			////2010/06/09
			////SM_CH_BATT5_EXIST_ST = 10542,
			//EnumProc(i * SMDU_CHANNEL_DISTANCE + SM_CH_BATT5_EXIST_ST, 
			//	stTempEquipExist.fValue, 
			//	lpvoid );


			//Process DIP Swith for LCD and WEB
			for (iRghIdx = 0; iRghIdx < 8; iRghIdx++)//ע�⣬ֻ��S2��S1��S3������
			{
				if (g_SmduSamplerData.aRoughDataSmdu[i][SMDU_DIP_SWITCH2_1 + iRghIdx].iValue
					!= g_SmduSamplerData.aDIPRoughDataSmdu[i][iRghIdx].iValue)
				{
					s_bDetectDIPChange = TRUE;

					//for LCD
					SetDwordSigValue(106,	//SMDU_GROUP_EQUIPID
									 0,		//SIG_TYPE_SAMPLING
									 2,		//CFG_CHANGED_SIG_ID
									 1,
									"SMDURS485");
					break;
				}
			}

			for (iRghIdx = 0; iRghIdx < 16; iRghIdx++)
			{
				g_SmduSamplerData.aDIPRoughDataSmdu[i][iRghIdx].iValue
					= g_SmduSamplerData.aRoughDataSmdu[i][iRghIdx + SMDU_DIP_SWITCH2_1].iValue;
			}
		}
		else
		{
			stTempEquipExist.iValue = SMDU_SAMP_INVALID_VALUE;
			j = 0;
			while(SmduSig[j].iChannel != SM_CH_END_FLAG)
			{
				EnumProc(i * SMDU_CHANNEL_DISTANCE + SmduSig[j].iChannel, 
					stTempEquipExist.fValue, 
					lpvoid );
				j++;
			}
			stTempEquipExist.iValue = SMDU_EQUIP_NOT_EXISTENT;
			EnumProc(i * SMDU_CHANNEL_DISTANCE + SM_CH_EXIST_ST, 
				stTempEquipExist.fValue, 
				lpvoid );


			EnumProc(i * SMDU_CHANNEL_DISTANCE + SM_CH_RS485_DC_FUSE_EXISTENCE,//��˿�豸�Ĵ���״̬
				stTempEquipExist.fValue, 
				lpvoid );


			//Stuff the battery inexistence
			EnumProc(i * SMDU_CHANNEL_DISTANCE + SM_CH_BATT1_EXIST_ST, 
				stTempEquipExist.fValue, 
				lpvoid );
			EnumProc(i * SMDU_CHANNEL_DISTANCE + SM_CH_BATT2_EXIST_ST, 
				stTempEquipExist.fValue, 
				lpvoid );
			EnumProc(i * SMDU_CHANNEL_DISTANCE + SM_CH_BATT3_EXIST_ST, 
				stTempEquipExist.fValue, 
				lpvoid );
			EnumProc(i * SMDU_CHANNEL_DISTANCE + SM_CH_BATT4_EXIST_ST, 
				stTempEquipExist.fValue, 
				lpvoid );
			EnumProc(i * SMDU_CHANNEL_DISTANCE + SM_CH_BATT5_EXIST_ST, 
				stTempEquipExist.fValue, 
				lpvoid );
		}
	}

	Smdu_SetRunAs485Mode();

	return 0;
}

/*=============================================================================*
 * FUNCTION: SMDUControlLvdCmd45
 * PURPOSE : control smdu by iChannelNo
 * INPUT: 
 *    
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMDUControlLvdCmd45(RS485_DEVICE_CLASS*  p_SmduDeviceClass, 
											  INT32 iAddr,
											  int iCommandGroup, 
											  int iCommandState)
{
	INT32			iSmduStatus;
	BYTE			szSendStr[25]	= {0};
	INT32			Length			= 0x04;
	WORD			wLength			= MakeDataLen(Length);

	//refer to smdu protocol 6.3 
    sprintf((CHAR *)szSendStr,
			"~%02X%02X%02X%02X%04X\0",
			0x20, 
			(unsigned int)iAddr, 
			SMDUCID1,
			SMDUCID2_CONTROL,
			wLength//%04X
			);
	//iCommandGroup,
	//iCommandState
	sprintf((char*)szSendStr + DATA_YDN23_SMDU ,"%02X\0",iCommandGroup);
	sprintf((char*)szSendStr + DATA_YDN23_SMDU + 2,"%02X\0",iCommandState);

	SMDUCheckSum(szSendStr );
	//1	  1	  1	  1	   1        2	   X	2   1
	//1	  2	  2	  2	   2	    4	   2X	4   1
	//SOI VER ADR CID1 CID2/RTN	LENGTH INFO	SUM	EOI
	//17+4
	*(szSendStr + 21) =  SMDU_BYEOI;

	iSmduStatus = SMDUSendAndRead( p_SmduDeviceClass->pCommPort->hCommPort,
									szSendStr,
									g_SmduSamplerData.abyRcvBuf,
									22,
									RS485_WAIT_VERY_SHORT_TIME,
									512);
	return iSmduStatus;
}
/*=============================================================================*
 * FUNCTION: SMDUGetSmduAddrByChnNo
 * PURPOSE : 
 * INPUT: 
 *    
 *
 * RETURN:
 *     static int
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
static int SMDUGetSmduAddrByChnNo(int iChannelNo, RS485_DEVICE_CLASS*  p_SmduDeviceClass)
{
	INT32		i;
	INT32		iPosition;

	int iSeqNo = (iChannelNo - SMDU_CHNNL_CTRL_START - 1) 
						/ SMDU_CHANNEL_DISTANCE;

	for(i = SMDU_ADDR_START; i <= SMDU_ADDR_END; i++)
	{
		iPosition = (i - SMDU_ADDR_START) * SMDU_RS485_MAX_SIG_END;
		if (iSeqNo == p_SmduDeviceClass->pRoughData[iPosition + SMDU_RS485_SEQ_NO].iValue)
		{
			return i;
		}
	}

	return 0;
}

/*=============================================================================*
 * FUNCTION: GetSmSeqNoByAddr
 * PURPOSE : Get seq by the SM module address
 * INPUT: 
 *    
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
//static int GetSmSeqNoByAddr(int iAddr)
//{	
//	INT32	iPosition;
//	iPosition = iAddr - SMDU_ADDR_START;
//	return g_SmduSamplerData.aRoughDataSmdu[iPosition][SMDU_RS485_SEQ_NO].iValue;
//}


#define	EQUIP_ID_SYS_GROUP		1
#define	EQUIP_ID_DC_UNIT		176
#define	EQUIP_ID_BATT_GROUP		115
#define	EQUIP_ID_SMDU_UNIT		107
#define	EQUIP_ID_SMDU_BATT1		126
#define	EQUIP_ID_SMDU_BATT2		127
#define	EQUIP_ID_SMDU_BATT3		128
#define	EQUIP_ID_SMDU_BATT4		129
#define	EQUIP_ID_SMDU_LVD		188

enum	SM_PARAM_SIG_ID
{
	SM_PARAM_SIG_ID_UNIT_RATED_CAP = 10,
	SM_PARAM_SIG_ID_RATED_CAP = 31,
	SM_PARAM_SIG_ID_OVER_VOLT = 5,
	SM_PARAM_SIG_ID_UNDER_VOLT = 6,
	SM_PARAM_SIG_ID_OVER_CURR = 45,
	SM_PARAM_SIG_ID_LVD1_VOLT = 1,
	SM_PARAM_SIG_ID_LVR1_VOLT = 2,
	SM_PARAM_SIG_ID_LVD2_VOLT = 6,
	SM_PARAM_SIG_ID_LVR2_VOLT = 7,
	SM_PARAM_SIG_ID_LVD1_VOLT24 = 51,
	SM_PARAM_SIG_ID_LVR1_VOLT24 = 52,
	SM_PARAM_SIG_ID_LVD2_VOLT24 = 56,
	SM_PARAM_SIG_ID_LVR2_VOLT24 = 57,
	
	SM_PARAM_SIG_ID_OVER_VOLT24 = 9,
	SM_PARAM_SIG_ID_UNDER_VOLT24 = 10,

	//added by Jimmy 20110616 for CR,setting Shunt size 1~5
	SM_PARAM_SIG_ID_UNIT_SHUNT1_VOLT = 21,
	SM_PARAM_SIG_ID_UNIT_SHUNT1_CURR = 22,
	SM_PARAM_SIG_ID_UNIT_SHUNT2_VOLT = 23,
	SM_PARAM_SIG_ID_UNIT_SHUNT2_CURR = 24,
	SM_PARAM_SIG_ID_UNIT_SHUNT3_VOLT = 25,
	SM_PARAM_SIG_ID_UNIT_SHUNT3_CURR = 26,
	SM_PARAM_SIG_ID_UNIT_SHUNT4_VOLT = 27,
	SM_PARAM_SIG_ID_UNIT_SHUNT4_CURR = 28,
	//SM_PARAM_SIG_ID_UNIT_SHUNT5_VOLT = 29,
	//SM_PARAM_SIG_ID_UNIT_SHUNT5_CURR = 30,

	SM_PARAM_SIG_ID_END_FLAG = -1,
};


#define RECTGROUPEQUIPID		2
#define RATED_VOLTAGE_SIG_ID	21//Rated Voltage

/*=============================================================================*
 * FUNCTION: SMDU SetCfgDataCmdec
 * PURPOSE : set cfg data for powerkit refer to protocol 6.15
 * INPUT: 
 *    
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMDUSetCfgDataCmdec(RS485_DEVICE_CLASS*  p_SmduDeviceClass, 
											  INT32 iAddr,
											  float fParam, 
											  INT32 SerialNo)
{
	INT32			iSmduStatus;
	SMDUSTRTOFLOAT	stConvertFtoc;
	BYTE			szSendStr[50]	= {0};
	BYTE			DataInfoType	= 0x01;//float
	BYTE			SerialNoHi		= 0x00;		
	BYTE			SerialNoLo		= SerialNo;	
	//datainfo length is 14
	BYTE			Length			= 0x0e;
	stConvertFtoc.f_value			= fParam;
	WORD			wLength			= MakeDataLen(Length);
	
	//refer to smdu protocol 6.15 for powerkit
    sprintf((CHAR *)szSendStr,
			"~%02X%02X%02X%02X%04X\0",
			0x20, 
			(unsigned int)iAddr, 
			SMDU_SET_CFG_DATA_CID1,
			SMDU_SET_CFG_DATA_CID2,
			wLength//%04X
		);

	//DataInfoType,
	//SerialNoHi,
	//SerialNoLo,
	//stConvertFtoc.by_value[0],
	//stConvertFtoc.by_value[1],
	//stConvertFtoc.by_value[2],
	//stConvertFtoc.by_value[3]

	sprintf((char*)(szSendStr + DATA_YDN23_SMDU),"%02X\0",(unsigned int)DataInfoType);
	sprintf((char*)szSendStr + DATA_YDN23_SMDU + 2,"%02X\0",SerialNoHi);
	sprintf((char*)szSendStr + DATA_YDN23_SMDU + 4,"%02X\0",SerialNoLo);
	sprintf((char*)szSendStr + DATA_YDN23_SMDU + 6,"%02X\0",stConvertFtoc.by_value[0]);
	sprintf((char*)szSendStr + DATA_YDN23_SMDU + 8,"%02X\0",stConvertFtoc.by_value[1]);
	sprintf((char*)szSendStr + DATA_YDN23_SMDU + 10,"%02X\0",stConvertFtoc.by_value[2]);
	sprintf((char*)szSendStr + DATA_YDN23_SMDU + 12,"%02X\0",stConvertFtoc.by_value[3]);

	SMDUCheckSum(szSendStr );
	//1	  1	  1	  1	   1        2	   X	2   1
	//1	  2	  2	  2	   2	    4	   2X	4   1
	//SOI VER ADR CID1 CID2/RTN	LENGTH INFO	SUM	EOI
	//17+14
	*(szSendStr + 31) =  SMDU_BYEOI;



	iSmduStatus = SMDUSendAndRead( p_SmduDeviceClass->pCommPort->hCommPort,
									szSendStr,
									g_SmduSamplerData.abyRcvBuf,
									32,
									RS485_WAIT_VERY_SHORT_TIME,
									512);

	return iSmduStatus;
}

//Copy from can_smdu.c added by yangguoxin
static void SM_Set_UnitCap(int iSeq)
{
	int		j; 
	float		fParam;

	if (iSeq < 0 || iSeq > 8 )
	{
		return;//Sequence Error SMDU inexistent
	}

	fParam = GetFloatSigValue(EQUIP_ID_SMDU_UNIT + iSeq, 
		SIG_TYPE_SETTING,
		SM_PARAM_SIG_ID_UNIT_RATED_CAP,
		"RS485SAMP");

	for(j = 0; j < 4; j++)
	{
		SetFloatSigValue(EQUIP_ID_SMDU_BATT1 + j + iSeq*4, 
			SIG_TYPE_SETTING,
			SM_PARAM_SIG_ID_UNIT_RATED_CAP,
			fParam,
			"RS485SAMP");

	}

}
/*=============================================================================*
 * FUNCTION: SMDU ParamUnify
 * PURPOSE : Ensure the parameters equal to sampling data in rough data
 * INPUT:	 the RoughData of p_SmduDeviceClass
 *    
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMDUParamUnify(RS485_DEVICE_CLASS*  p_SmduDeviceClass)
{
	INT32	i;
	INT32	j;
	INT32	iErrCode;
	INT32	iPosition;
	float	fRatedVolt;

	//��Ϊÿ�ζ�ͬ�������Ե��������þͲ���ͬ����
	if(g_RS485Data.bNeedReInitModeModify)
	{
		return 0;
	}
	//struct	tagOLD_PARAM_UNIFY_IDX
	//{
	//	int		iEquipId;
	//	int		iEquipIdDifference;
	//	int		iSigId;				//for -48V system parameter
	//	int		iSigId24V;			//for 24V system parameter
	//	int		iRoughData;
	//	UINT	uiSerialNo;
	//};

	//typedef struct tagOLD_PARAM_UNIFY_IDX RS485_OLD_PARAM_UNIFY_IDX;

	RS485_OLD_PARAM_UNIFY_IDX		SmParamIdx[] =
	{
		//{EQUIP_ID_BATT_GROUP,	0,	SM_PARAM_SIG_ID_RATED_CAP,	SM_PARAM_SIG_ID_RATED_CAP,		SMDU_BATTERY_RATED_CAP,				21,	},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_RATED_CAP,	SM_PARAM_SIG_ID_UNIT_RATED_CAP,	SMDU_BATTERY_RATED_CAP,			21,	},
		{EQUIP_ID_DC_UNIT,		0,	SM_PARAM_SIG_ID_OVER_VOLT,	SM_PARAM_SIG_ID_OVER_VOLT24,	SMDU_OVER_VOLT,						2,	},
		{EQUIP_ID_DC_UNIT,		0,	SM_PARAM_SIG_ID_UNDER_VOLT,	SM_PARAM_SIG_ID_UNDER_VOLT24,	SMDU_UNDER_VOLT,					0,	},	
		{EQUIP_ID_BATT_GROUP,	0,	SM_PARAM_SIG_ID_OVER_CURR,	SM_PARAM_SIG_ID_OVER_CURR,		SM_BATT1_OVER_CURR,					4,	},
		{EQUIP_ID_BATT_GROUP,	0,	SM_PARAM_SIG_ID_OVER_CURR,	SM_PARAM_SIG_ID_OVER_CURR,		SM_BATT2_OVER_CURR,					6,	},
		{EQUIP_ID_BATT_GROUP,	0,	SM_PARAM_SIG_ID_OVER_CURR,	SM_PARAM_SIG_ID_OVER_CURR,		SM_BATT3_OVER_CURR,					8,	},
		{EQUIP_ID_BATT_GROUP,	0,	SM_PARAM_SIG_ID_OVER_CURR,	SM_PARAM_SIG_ID_OVER_CURR,		SM_BATT4_OVER_CURR,					10,	},
		{EQUIP_ID_SMDU_LVD,		1,	SM_PARAM_SIG_ID_LVD1_VOLT,	SM_PARAM_SIG_ID_LVD1_VOLT24,	SMDU_LVD_1_OFF_VOLTAGE,				37,	},
		{EQUIP_ID_SMDU_LVD,		1,	SM_PARAM_SIG_ID_LVR1_VOLT,	SM_PARAM_SIG_ID_LVR1_VOLT24,	SMDU_LVD_1_ON_VOLTAGE,				38,	},
		{EQUIP_ID_SMDU_LVD,		1,	SM_PARAM_SIG_ID_LVD2_VOLT,	SM_PARAM_SIG_ID_LVD2_VOLT24,	SMDU_LVD_2_OFF_VOLTAGE,				39,	},
		{EQUIP_ID_SMDU_LVD,		1,	SM_PARAM_SIG_ID_LVR2_VOLT,	SM_PARAM_SIG_ID_LVR2_VOLT24,	SMDU_LVD_2_ON_VOLTAGE,				40,	},
		//Following are added by Jimmy for Setting shunt 1~4 parameters 20110620
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT1_CURR,	SM_PARAM_SIG_ID_UNIT_SHUNT1_CURR,	SMDU_SHUNT1_A,			13,		},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT1_VOLT,	SM_PARAM_SIG_ID_UNIT_SHUNT1_VOLT,	SMDU_SHUNT1_MV,			14,		},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT2_CURR,	SM_PARAM_SIG_ID_UNIT_SHUNT2_CURR,	SMDU_SHUNT2_A,			15,		},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT2_VOLT,	SM_PARAM_SIG_ID_UNIT_SHUNT2_VOLT,	SMDU_SHUNT2_MV,			16,		},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT3_CURR,	SM_PARAM_SIG_ID_UNIT_SHUNT3_CURR,	SMDU_SHUNT3_A,			17,		},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT3_VOLT,	SM_PARAM_SIG_ID_UNIT_SHUNT3_VOLT,	SMDU_SHUNT3_MV,			18,		},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT4_CURR,	SM_PARAM_SIG_ID_UNIT_SHUNT4_CURR,	SMDU_SHUNT4_A,			19,		},
		{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT4_VOLT,	SM_PARAM_SIG_ID_UNIT_SHUNT4_VOLT,	SMDU_SHUNT4_MV,			20,		},
		//{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT5_CURR,SM_PARAM_SIG_ID_UNIT_SHUNT5_CURR,	SMDU_SHUNT5_A,			SM_VAL_TYPE_W_SHUNT5_A,		},
		//{EQUIP_ID_SMDU_UNIT,	1,	SM_PARAM_SIG_ID_UNIT_SHUNT5_VOLT,SM_PARAM_SIG_ID_UNIT_SHUNT5_VOLT,	SMDU_SHUNT5_MV,			SM_VAL_TYPE_W_SHUNT5_MV,	},
		
		{0, 0, SM_PARAM_SIG_ID_END_FLAG, SM_PARAM_SIG_ID_END_FLAG, 0, 0,},
	};


	if(g_RS485Data.CommPort.enumAttr		!= RS485_ATTR_NONE
		||g_RS485Data.CommPort.enumBaud		!= RS485_ATTR_19200)
	{
		if(g_RS485Data.CommPort.hCommPort && g_RS485Data.CommPort.bOpened)
		{
			RS485_Close(g_RS485Data.CommPort.hCommPort);
		}

		g_RS485Data.CommPort.hCommPort 
			= RS485_Open("19200, n, 8, 1", RS485_READ_WRITE_TIMEOUT, (int*)(&iErrCode));

		if(iErrCode == ERR_COMM_OK)
		{
			g_RS485Data.CommPort.bOpened = TRUE;
			g_RS485Data.CommPort.enumAttr = RS485_ATTR_NONE;
			g_RS485Data.CommPort.enumBaud = RS485_ATTR_19200;
		}
		else
		{
			g_RS485Data.CommPort.bOpened = FALSE;	
			AppLogOut("SMDU UNIFY",
				APP_LOG_UNUSED, 
				"[%s]-[%d] ERROR: failed to  Open RS485 Comm \n\r",
				__FILE__, __LINE__);
			return -1;
		}
	}
	//������ǰ�ѣ���ע���ַ�Ĵ���,Jimmy Wu 20120602
	for(i = SMDU_ADDR_START; i <= SMDU_ADDR_END; i++)
	{
		//INT32	iSeqNo	= GetSmSeqNoByAddr(i);

		iPosition		= i - SMDU_ADDR_START;

		//if(iSeqNo != SMDU_SAMP_INVALID_VALUE)
		{
			if(g_SmduSamplerData.OLD_CANFlashData.aSmduInfo[iPosition].bExistence)
			{
				INT32 iRoughDataPosition = iPosition * SMDU_RS485_MAX_SIG_END;

				if (p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_INTERRUPT_TIMES].iValue > 0)
				{
					//TRACE("\n	$$$$ SMDU ADDR: %d COMM FAILS SMDUParamUnify  CONTINUE\n",i);
					continue;
				}
				//printf("\n******entering SM_Para_Unify successfully*******\n\n");
				//added by Jimmy default value set to 0 (0 for normal,1 for alarm)
				g_SmduSamplerData.aRoughDataSmdu[iPosition][SMDU_SHUNT1_VALUE_CHANGED].iValue = 0;
				g_SmduSamplerData.aRoughDataSmdu[iPosition][SMDU_SHUNT2_VALUE_CHANGED].iValue = 0;
				g_SmduSamplerData.aRoughDataSmdu[iPosition][SMDU_SHUNT3_VALUE_CHANGED].iValue = 0;
				g_SmduSamplerData.aRoughDataSmdu[iPosition][SMDU_SHUNT4_VALUE_CHANGED].iValue = 0;
				//g_SmduSamplerData.aRoughDataSmdu[iPosition][SMDU_SHUNT5_VALUE_CHANGED].iValue = 0;
				j = 0;
				while(SmParamIdx[j].iSigId != SM_PARAM_SIG_ID_END_FLAG)
				{
					int		iSigId;
					float	fParam;
					float	fSampleValue 
						= g_SmduSamplerData.aRoughDataSmdu[iPosition][SmParamIdx[j].iRoughData].fValue;
					
					fRatedVolt = GetFloatSigValue(RECTGROUPEQUIPID, 
												  SIG_TYPE_SAMPLING, 
												  RATED_VOLTAGE_SIG_ID,
												  "RS485SAMP");
					SIG_ENUM stVoltLevelState = GetEnumSigValue(1,2,224,"SMDU Get SysVolt Level");
					//if(fRatedVolt > 35.0)
					if(0 == stVoltLevelState)//48 V
					{
						iSigId = SmParamIdx[j].iSigId;
					}
					else
					{
						iSigId = SmParamIdx[j].iSigId24V;
				
					}
					/*
						��Ϊ����������SMDU��Ԫ������ҪGC���㣬�����������︳�����ֵ��������澯����
						Ȼ��GC�Լ����ٸ��丳ֵ
						����ע�⣬���ڶ������ļ���ֻ�и��źŵ��ź�IDΪ10���������ź�ID�ж�
					*/
					if ((SM_PARAM_SIG_ID_UNIT_RATED_CAP == SmParamIdx[j].iSigId))
					{
						fParam = 9998;//��WZPȷ�� ���9999
					}
					else
					{
						fParam = GetFloatSigValue(SmParamIdx[j].iEquipId 
													+ iPosition * SmParamIdx[j].iEquipIdDifference, 
													SIG_TYPE_SETTING,
													iSigId,
													"RS485SAMP");
					}
					//���¸��ݲ��뿪��״̬�����Ƿ�ִ�в���ͬ��,added by Jimmy 2011/06/19
					if(SmParamIdx[j].iEquipId == EQUIP_ID_SMDU_UNIT && SmParamIdx[j].iSigId >= SM_PARAM_SIG_ID_UNIT_SHUNT1_CURR && SmParamIdx[j].iSigId <= SM_PARAM_SIG_ID_UNIT_SHUNT4_VOLT)
					{
						if(FLOAT_NOT_EQUAL(fParam, fSampleValue))
						{
							switch (SmParamIdx[j].iSigId)
							{
							case SM_PARAM_SIG_ID_UNIT_SHUNT1_CURR:
							case SM_PARAM_SIG_ID_UNIT_SHUNT1_VOLT:
								g_SmduSamplerData.aRoughDataSmdu[iPosition][SMDU_SHUNT1_VALUE_CHANGED].iValue = 1;
								break;
							case SM_PARAM_SIG_ID_UNIT_SHUNT2_CURR:
							case SM_PARAM_SIG_ID_UNIT_SHUNT2_VOLT:
								g_SmduSamplerData.aRoughDataSmdu[iPosition][SMDU_SHUNT2_VALUE_CHANGED].iValue = 1;
								break;
							case SM_PARAM_SIG_ID_UNIT_SHUNT3_CURR:
							case SM_PARAM_SIG_ID_UNIT_SHUNT3_VOLT:
								g_SmduSamplerData.aRoughDataSmdu[iPosition][SMDU_SHUNT3_VALUE_CHANGED].iValue = 1;
								break;
							case SM_PARAM_SIG_ID_UNIT_SHUNT4_CURR:
							case SM_PARAM_SIG_ID_UNIT_SHUNT4_VOLT:
								g_SmduSamplerData.aRoughDataSmdu[iPosition][SMDU_SHUNT4_VALUE_CHANGED].iValue = 1;
								break;
							//case SM_PARAM_SIG_ID_UNIT_SHUNT5_CURR:
							//case SM_PARAM_SIG_ID_UNIT_SHUNT5_VOLT:
							//	g_SmduSamplerData.aRoughDataSmdu[iPosition][SMDU_SHUNT1_VALUE_CHANGED].iValue = 1;
							//	break;
							default:
								break;
							}
						}

						if(g_SmduSamplerData.aRoughDataSmdu[iPosition][SMDU_DIP_SWITCH3_1].iValue == 1) //0=on,1=off
						{
							j++;
							continue;
						}
						else
						{
							
						}
					}
					if(FLOAT_NOT_EQUAL(fParam, fSampleValue))
					{
						SMDUSetCfgDataCmdec(p_SmduDeviceClass,
							i,
							fParam,
							(INT32)(SmParamIdx[j].uiSerialNo));
					}

					j++;
				}
			}
		}
	}

	return 0;
}


/*=============================================================================*
* FUNCTION: SMDU _SendCtlCmd
* PURPOSE : control smio by iChannelNo
* INPUT: 
*    
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
VOID SMDU_SendCtlCmd(RS485_DEVICE_CLASS*  p_SmduDeviceClass,
		     INT32 iChannelNo, 
		     float fParam,
		     char *pString)
{
	BYTE	iCommandGroup;
	BYTE	iCommandState;
	INT32	iSmduAddr;
	UNUSED(pString);

	if (RS485_RUN_MODE_SLAVE == g_RS485Data.enumRunningMode)
	{
		return;
	}

	if (iChannelNo < SMDU_CHNNL_CTRL_START || (6573+ 8 * 80) < iChannelNo)//6573+ SmduNo * 80
	{
		return;
	}

	INT32	iSubChnNo = ((iChannelNo - SMDU_CHNNL_CTRL_START - 1)
		% SMDU_CHANNEL_DISTANCE) 
		+ SMDU_CHNNL_CTRL_START 
		+ 1;
	iSmduAddr = SMDUGetSmduAddrByChnNo(iChannelNo, p_SmduDeviceClass);


	if(SMDU_RS485_CNSS_LVD1 == iSubChnNo || SMDU_RS485_CNSS_LVD2 == iSubChnNo) //added by Jimmy for CR
	{
		//if ((iSubChnNo != SMDU_RS485_CNSS_LVD1)
		//	||(iSubChnNo != SMDU_RS485_CNSS_LVD2))
		//{
		//	return;
		//}

		//get address


		if(FLOAT_EQUAL(fParam, 1.0))
		{
			iCommandState = 0x01;//lvd Disconnection
		}
		else
		{
			iCommandState = 0x00;
		}

		iChannelNo -= (SMDU_CHNNL_CTRL_START + 1);	//��6501

		iChannelNo	= iChannelNo % SMDU_CHANNEL_DISTANCE;//ȡ����Ϊ0����ʾ�ܱ�6501����������һ��LVD
		if(0 == iChannelNo)
		{
			iCommandGroup = 0x01;
		}
		else
		{
			iCommandGroup = 0x02;
		}

		if(SMDUNeedCommProc())
		{
			SMDUReopenPort();
		}

		SMDUControlLvdCmd45(p_SmduDeviceClass,
			iSmduAddr,
			(int)iCommandGroup,
			(int)iCommandState);

	}
	else if(SMDU_RS485_CNSS_SHUNT1_MV <= iSubChnNo || SMDU_RS485_CNSS_SHUNT4_A >= iSubChnNo) //added by Jimmy for CR 20110621,following used for setting Shunt 1~5 para
	{
		int nSeriNo = iSubChnNo - SMDU_RS485_CNSS_SHUNT1_MV;
		//����ע�⣺��Ϊ485Э�������ǰ����CAN��ͬ��������Ҫ����˳��
		if(nSeriNo % 2 == 0)
		{
			nSeriNo = nSeriNo + SMDU_CFG_SERIALNO_SHUNT1_A + 1;
		}
		else
		{
			nSeriNo = nSeriNo + SMDU_CFG_SERIALNO_SHUNT1_A - 1;
		}
		SMDUSetCfgDataCmdec(p_SmduDeviceClass,
			iSmduAddr,
			fParam,
			nSeriNo);

	}

}
/*=============================================================================*
* FUNCTION: FixFloatDat
* PURPOSE : eight byte convert to float
* INPUT:	char* sStr
*     
*
* RETURN:
*     static float
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*		kenn.wan                 DATE: 2008-08-15 14:55
*============================================================================*/
static float FixFloatDat( char* sStr )
{
	INT32					i;
	SMDUSTRTOFLOAT			floatvalue;
	CHAR					cTemp;
	CHAR					cTempHi;
	CHAR					cTempLo;

	if (sStr[0] == 0x20)
	{
		return -9999.0f;
	}

	for (i=0; i<4; i++)
	{
#ifdef _CPU_BIG_ENDIAN		// ppc
#ifdef _SAMPLER_REPLACE_SSCANF
		sscanf((const char *)sStr+i*2, "%02x", 
			&floatvalue.by_value[3-i]);
#else
		int	c;
		sscanf((const char *)sStr+i*2, "%02x", 	&c);
		floatvalue.by_value[3-i] = (BYTE)c;
#endif
#else	// for x86
		//hi bit
		if((*(sStr + i * 2) > 0x40))
		{
			cTempHi = (*(sStr + i * 2) - 0x40) + 9;
		}
		else
		{
			cTempHi = (*(sStr + i * 2) - 0x30);
		}

		//low bit
		if((*(sStr + i * 2 + 1)) > 0x40)
		{
			cTempLo = (*(sStr + i * 2 + 1) - 0x40) + 9;
		}
		else
		{
			cTempLo = (*(sStr + i * 2 + 1) - 0x30);
		}

		cTemp = cTempHi * 16 + cTempLo;
		floatvalue.by_value[i] = cTemp;
#endif
	}
	return floatvalue.f_value;
}

/*=============================================================================*
 * FUNCTION: SMDU_InitRoughValue
 * PURPOSE : initialization  smdu sampler RoughData[][]
 * INPUT:	 p_SmduDeviceClass
 *     
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMDU_InitRoughValue(RS485_DEVICE_CLASS*  p_SmduDeviceClass)
{
	INT32	i;
	INT32	j;
	INT32	iRoughIndexPosition;	
	static	INT32 iFirstrunning = FALSE;

	p_SmduDeviceClass->pfnSample		= (DEVICE_CLASS_SAMPLE)SMDUSample;	
	p_SmduDeviceClass->pfnParamUnify	= (DEVICE_CLASS_PARAM_UNIFY)SMDUParamUnify;
	p_SmduDeviceClass->pfnSendCmd		= (DEVICE_CLASS_SEND_CMD)SMDU_SendCtlCmd;
	p_SmduDeviceClass->pfnStuffChn		= (DEVICE_CLASS_STUFF_CHN)SMDU_StuffChannel;
	p_SmduDeviceClass->pRoughData		= (RS485_VALUE*)(&(g_SmduSamplerData.aRoughDataSmdu[0][0]));
	
	//read param from flash		unused
	//RS485_ReadFlashData(&(g_SmduSamplerData.OLD_CANFlashData));
	for(i = 0; i < SMDU_MAX_NUM; i++)
	{
		iRoughIndexPosition = i * SMDU_RS485_MAX_SIG_END;

		for(j = 0; j < SMDU_RS485_MAX_SIG_END; j++)
		{
			p_SmduDeviceClass->pRoughData[iRoughIndexPosition + j].iValue 
				= SMDU_SAMP_INVALID_VALUE;
		}
	
		if(!iFirstrunning)
		{
			for(j = 0; j < 16; j++)
			{
				g_SmduSamplerData.aDIPRoughDataSmdu[i][j].iValue = SMDU_SAMP_INVALID_VALUE; 
			}

			iFirstrunning = TRUE;
		}
		//The following two are added by Jimmy 20110618 for CR
		p_SmduDeviceClass->pRoughData[iRoughIndexPosition +SMDU_DIP_SWITCH3_1].iValue = 1; //�������ʼ������״̬Ϊ1������ʾ		
		//p_SmduDeviceClass->pRoughData[iRoughIndexPosition +SMDU_CURR5_ENABLE].iValue = 0; //�������ʼ��ȷ��CAN����ʾ��5·������ϵ�����ã�����Ϊ0	

		p_SmduDeviceClass->pRoughData[iRoughIndexPosition + SMDU_RS485_EXISTENCE].iValue 
				= SMDU_EQUIP_NOT_EXISTENT;		
		p_SmduDeviceClass->pRoughData[iRoughIndexPosition + SMDU_BATT1_EXIST_ST].iValue 
				= SMDU_EQUIP_NOT_EXISTENT;		
		p_SmduDeviceClass->pRoughData[iRoughIndexPosition + SMDU_BATT2_EXIST_ST].iValue 
				= SMDU_EQUIP_NOT_EXISTENT;	
		p_SmduDeviceClass->pRoughData[iRoughIndexPosition + SMDU_BATT3_EXIST_ST].iValue 
				= SMDU_EQUIP_NOT_EXISTENT;	
		p_SmduDeviceClass->pRoughData[iRoughIndexPosition + SMDU_BATT4_EXIST_ST].iValue 
				= SMDU_EQUIP_NOT_EXISTENT;	

		p_SmduDeviceClass->pRoughData[iRoughIndexPosition + SMDU_SAMPLING_TIMES].iValue
				= 0;
	}

	for(i = 0; i < SMDU_MAX_NUM; i++)
	{
		g_SmduSamplerData.OLD_CANFlashData.aSmduInfo[i].iSeqNo = SMDU_SAMP_INVALID_VALUE;
		g_SmduSamplerData.OLD_CANFlashData.aSmduInfo[i].bExistence = FALSE;
	}

	p_SmduDeviceClass->pRoughData[SMDU_GROUP_SM_STATE].iValue 
				= SMDU_EQUIP_NOT_EXISTENT;
	p_SmduDeviceClass->pRoughData[SMDU_GROUP_SM_ACTUAL_NUM].iValue = 0;
	p_SmduDeviceClass->bNeedReconfig = TRUE;
	return 0;
}

/*=============================================================================*
 * FUNCTION: RecvData FromSMDU()
 * PURPOSE  :receive data of SM from 485 com
 * RETURN   : int : byte number ,but if return -1  means  error
 * ARGUMENTS:
 *								CHAR*	sRecStr :		
 *								hComm	 :	485 com handle 
 *								iStrLen	 :
 * CALLS    : 
 * CALLED BY: 
 *								DLLExport BOOL Query()
 * COMMENTS : 
 * CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
 *==========================================================================*/
LOCAL INT32	RecvDataFromSMDU(HANDLE hComm, CHAR* sRecStr, INT32* piStrLen,INT32 iWantToReadnByte)
{
		INT32	iReadLen;		
		INT32	i		= 0;
		INT32	j		= 0;
		INT32	iHead	= -1;
		INT32	iTail	= -1;
	    ASSERT( hComm != 0 );
		ASSERT( sRecStr != NULL);
		
		//Read the data from Smac
		iReadLen = RS485_Read(hComm, (BYTE*)sRecStr, iWantToReadnByte);
		
		for (i = 0; i < iReadLen; i++)
		{
			if(SMDU_BYSOI == *(sRecStr + i))
			{
				iHead = i;
				break;
			}
		}

		if (iHead < 0)	//   no head
		{
			//TRACE(" DU  DU  DU  RRR (iHead < 0)no head %d %s \n",iReadLen,sRecStr);
			return FALSE;
		}

		for (i = iHead + 1; i < iReadLen; i++)
		{
			if(SMDU_BYEOI == *(sRecStr + i))
			{
				iTail = i;
				break;
			}
		}	

		if (iTail < 0)	//   no tail
		{
			//TRACE(" DU  DU  DU  RRR (iTail < 0)  no tail%d  %s \n",iReadLen,sRecStr);
			return FALSE;
		}	

		*piStrLen = iTail - iHead + 1;
		
		if(iHead > 0)
		{
			for (j = iHead; j < *piStrLen; j++)
			{
				*(sRecStr + (j - iHead)) = *(sRecStr + j);
			}
		}

		*(sRecStr + (iTail - iHead + 2)) = '\0';
		return TRUE;
}

/*=============================================================================*
 * FUNCTION:	RS485WaitReadable
 * PURPOSE  :	wait RS485 data ready
 * RETURN   :	int : 1: data ready, 0: timeout,Allow get mode or auto config
 * ARGUMENTS:
 *						int fd	: 
 *						int TimeOut: seconds 
 *			
 * CALLS    : 
 * CALLED BY: 
 *								
 * COMMENTS : 
 * CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
 *==========================================================================*/
LOCAL INT32 RS485WaitReadable(INT32 fd, INT32 nmsTimeOut)
{
    fd_set readfd;
    struct timeval timeout;

	while (nmsTimeOut > 0)
	{
		// need add ClearWDT() here.
		if (nmsTimeOut < (5*1000))
		{
			timeout.tv_usec = nmsTimeOut%1000*1000;			/* usec     */
			timeout.tv_sec  = nmsTimeOut/1000;				/* seconds  */

			nmsTimeOut = 0;
		}
		else
		{
			timeout.tv_usec = 0;							/* usec     */
			timeout.tv_sec  = 5;							/* seconds  */

			RUN_THREAD_HEARTBEAT();

			nmsTimeOut -= (5*1000);
		}

	}

	FD_ZERO(&readfd);										/*  Initializes the set to the NULL set. */
	FD_SET(fd, &readfd);									/*  Adds descriptor s to set. */

	select(fd+1, &readfd, NULL, NULL, &timeout);
	
	if(FD_ISSET(fd, &readfd))	
	{
		return TRUE;										//1: data ready, can be read
	}
	else
	{
		return FALSE;										//0: Data not ready, can't read!!
	}
}

/*=============================================================================*
* FUNCTION: CheckStrLength
* PURPOSE : Check receive data length 
* INPUT: 
*     
*
* RETURN:
*     INT32   1:  succeed	0:  error  
*		
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*
*============================================================================*/
LOCAL  INT32 CheckStrLength(BYTE *abyRcvBuf, INT32 RecBufOffsetNum, INT32 RecDataLength)
{
	WORD		wLength;
	CHAR		c_CheckLength[5]	= {0};
	INT32		OffsetToLength		= RecBufOffsetNum;

	//repeat  length check
	wLength							= MakeDataLen(RecDataLength);
	sprintf((char *)c_CheckLength,"%04X\0",wLength);

	if((abyRcvBuf[OffsetToLength + 3]		!= c_CheckLength[3]) //fourth byte of  length check
		&& (abyRcvBuf[OffsetToLength + 2]	!= c_CheckLength[2]) //third  byte of  length check
		&&(abyRcvBuf[OffsetToLength + 1]	!= c_CheckLength[1]) //second byte of  length check
		&& (abyRcvBuf[OffsetToLength]		!= c_CheckLength[0]))//first  byte of  length check
	{
		return FALSE;
	}

	return TRUE;
}

/*=============================================================================*
* FUNCTION: SMDUCheckStrSum
* PURPOSE : Check receive data sum 
* INPUT: 
*     
*
* RETURN:
*     INT32   1:  succeed	0:  error  
*		
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*
*============================================================================*/
LOCAL INT32  SMDUCheckStrSum(CHAR *abyRcvBuf, INT32 RecTotalLength)
{
	CHAR	 szCheckCode[5]	= {0};
	//copy receive SMDUCheckSum to szCheckCode 
	strncpy(szCheckCode, (CHAR*)abyRcvBuf + RecTotalLength - SMDU_SHECKSUM_EOI, 4); 
	abyRcvBuf[RecTotalLength - SMDU_SHECKSUM_EOI] = 0;

	//repeat check sum
	SMDUCheckSum((BYTE*)abyRcvBuf);

	if((abyRcvBuf[RecTotalLength - 2]		== szCheckCode[3])  //fourth byte of check sum
		&& (abyRcvBuf[RecTotalLength - 3]	== szCheckCode[2])	//third	 byte of check sum
		&&(abyRcvBuf[RecTotalLength - 4]	== szCheckCode[1])	//second byte of check sum
		&& (abyRcvBuf[RecTotalLength - 5]	== szCheckCode[0]))	//first  byte of check sum
	{ 
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*=============================================================================*
 * FUNCTION: SMDU GetResponeData
 * PURPOSE : 
 * INPUT:	 BYTE		*sSendStr,		
 *     
 *
 * RETURN:
 *     INT32   0:  succeed	1:no response working  
 *			  -1:  function error
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32
 *copy from smio
 *============================================================================*/
LOCAL INT32 SMDUSendAndRead(
                   HANDLE	hComm,			 
                   BYTE		*sSendStr,		 
                   BYTE		*abyRcvBuf,      
                   INT32	nSend,			 
                   int     cWaitTime,
				   INT32	iWantToReadnByte)
{
		UNUSED(cWaitTime);
		INT32		iReceiveNum;
		//INT32		iReceiveNumByClr;
		INT32		iDataInfoLength;
		RS485_DRV*	pPort				= (RS485_DRV *)hComm;
		INT32		fd					= pPort->fdSerial;
		ASSERT( hComm !=0 && sSendStr != NULL && abyRcvBuf != NULL);

		//RS485_ClrBuffer(hComm);

		if(RS485_Write(hComm, sSendStr, nSend) != nSend)
		{
			//note	applog	!!
			AppLogOut("SMDU_GetResponeData", APP_LOG_UNUSED,
				  "[%s]-[%d] ERROR: failed to  RS485_Write \n\r",
				  __FILE__, __LINE__);

			TRACE("\n*****   !!!!! SMDU  RS485_Write   ******\n");
			//Sleep(50);
			return SMDU_NO_RESPONSE_BEXISTENCE;
		}

		//if (RS485_WAIT_LONG_TIME == cWaitTime)
		//{
		//	Sleep(350);
		//}
		//else if (RS485_WAIT_VERY_LONG_TIME == cWaitTime)
		//{
		//	Sleep(500);
		//}
		//else if (RS485_WAIT_SHORT_TIME == cWaitTime)
		//{
		//	Sleep(300);
		//}
		//else if (RS485_WAIT_VERY_SHORT_TIME == cWaitTime)
		//{
			Sleep(20);
		//}
	if (RS485WaitReadable(fd, 600) > 0 )
	{
		if (RecvDataFromSMDU(hComm, (CHAR*)abyRcvBuf, &iReceiveNum, iWantToReadnByte))
		{
			//check cid1
			if( abyRcvBuf[5] != sSendStr[5] || abyRcvBuf[6] != sSendStr[6] )
			{
				TRACE("\n*****SMDU  check cid1  error ******\n");
				return SMDU_NO_RESPONSE_BEXISTENCE;					
			}
			//check equip addr 
			else if( abyRcvBuf[3] != sSendStr[3] || abyRcvBuf[4] != sSendStr[4] )
			{
				TRACE("\n*****SMDU  check equip addr  error ******\n");
				return SMDU_NO_RESPONSE_BEXISTENCE;						
			}
			//check RTN
			else  if( abyRcvBuf[7] != '0' || abyRcvBuf[8] != '0' )
			{
				TRACE("\n*****SMDU check RTN  error ******\n");
				return SMDU_NO_RESPONSE_BEXISTENCE;
			}
			else
			{
				//check  strlen
				//TOTALNumb - 5 - 13 = DATAINFO
				iDataInfoLength = iReceiveNum - SMDU_SHECKSUM_EOI - DATA_YDN23_SMDU;

				//	check  strlen 
				if (!CheckStrLength(abyRcvBuf, LENGTH_YDN23_SMDU, iDataInfoLength))
				{
					TRACE("\n*****SMDU  check StrLength error ******\n");
					return SMDU_NO_RESPONSE_BEXISTENCE;
				}

				// check sum
				if(SMDUCheckStrSum((CHAR*)abyRcvBuf, iReceiveNum))
				{
					//TRACE("\n*****SMDU check SMDUCheckStrSum  error ******\n");
					return SMDU_RESPONSE_OK;

				}
				else
				{
					TRACE("\n*****SMDU  check sum error ******\n");
					return SMDU_NO_RESPONSE_BEXISTENCE;
				}
			}
		}
		else
		{
			//RS485_ClrBuffer(hComm);
			TRACE("\n*****SMDU  else  read 0 ******\n");
			return SMDU_NO_RESPONSE_BEXISTENCE;
		}
	}
	else
	{
		TRACE("\n*****SMDU  RS485WaitReadable read 0 ******\n");
		return SMDU_NO_RESPONSE_BEXISTENCE;
	}
}

/*=============================================================================*
 * FUNCTION: GetBarCode
 * PURPOSE :get vertion  for scan smio equip refer to protocol	6.12
 * INPUT: 
 *			
 *
 * RETURN:
 *		szVer   OUT
 *		INT32   0:  succeed	 1:no response working 
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32
 *
 *============================================================================*/
LOCAL INT32 SMDUSampleGetBarCodeCmdfa( HANDLE hComm,
									  INT32 nUnitNo,
									  int cCID1,
									  int cCID2, 
									  BYTE*szVer,
									  RS485_DEVICE_CLASS*  p_SmduDeviceClass)
{
	UNUSED(szVer);
	INT32		iSmduStatus;   
	BYTE		szSendStr[20] = {0};
	BYTE		abyRcvBuf[MAX_RECVBUFFER_NUM_SMDU] = {0};

	sprintf((CHAR *)szSendStr,
			"~%02X%02X%02X%02X%04X\0",
			0x20, 
			(unsigned int)nUnitNo, 
			cCID1,
			cCID2, 
			0 );

	SMDUCheckSum(szSendStr );
	//1	  1	  1	  1	   1        2	   X	2   1
	//1	  2	  2	  2	   2	    4	   2X	4   1
	//SOI VER ADR CID1 CID2/RTN	LENGTH INFO	SUM	EOI
	*(szSendStr + 17) =  SMDU_BYEOI;

	//TRACE("\n !!! GetBarCodeCmdfa  ADDR = %d\n",nUnitNo);

	iSmduStatus = SMDUSendAndRead( hComm, szSendStr,abyRcvBuf, 18, RS485_WAIT_LONG_TIME, 1024);

	//unpack the receive data for refresh flash
	if (SMDU_RESPONSE_OK == iSmduStatus)
	{
		SMDUUnpackCmdfa(nUnitNo, p_SmduDeviceClass,abyRcvBuf);
	}

	return iSmduStatus;
}

static BYTE SMDUAscToHex(char c)
{
	if (c >= '0' && c <= '9')
	{
		return c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		return c - 'A' + 10;
	}

	return 0xFF;
}
/*=============================================================================*
* FUNCTION: SMDUMergeAsc()
* PURPOSE : assistant function, used to merge two ASCII chars
* INPUT:	 CHAR*	p
*     
*
* RETURN:
*     static BYTE
*
* CALLS: 
*     void
*
* CALLED BY: 
*    
*			 copy from SMAC        DATE: 2009-05-08		14:39
*============================================================================*/
static BYTE SMDUMergeAsc(char *p)
{
	BYTE byHi, byLo;

	byHi = SMDUAscToHex(p[0]);
	byLo = SMDUAscToHex(p[1]);

	//error data
	if (byHi == 0xFF || byLo == 0xFF)
	{
		return 0;
	}

	return (byHi*16 + byLo);
}

/*=============================================================================*
 * FUNCTION: RS485_StringToUint
 * PURPOSE : 
 * INPUT:	BYTE* strInput
 *    
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     COPY FROM Frank Cao     
 *
 *============================================================================*/
static UINT RS485_StringToUint(BYTE* strInput)
{
	BYTE bTempData[5];
	bTempData[0] = SMDUMergeAsc((char*)strInput);
	bTempData[1] = SMDUMergeAsc((char*)strInput + 2);
	bTempData[2] = SMDUMergeAsc((char*)strInput + 4);
	bTempData[3] = SMDUMergeAsc((char*)strInput + 6);

	UINT uiValue = (((UINT)(bTempData[0])) << 24)
		+ (((UINT)(bTempData[1])) << 16)
		+ (((UINT)(bTempData[2])) << 8)
		+ ((UINT)(bTempData[3]));

	return uiValue;
}

// "3031303541" -> "0105A"
static BOOL RS485_AscStringToString(char *pSrc, int nSrcLen, char *pDes)
{
	if(nSrcLen % 2)
	{
		return FALSE;
	}

	int i;
	for(i = 0; i < nSrcLen / 2; i++)
	{
		*(pDes + i) = SMDUMergeAsc((char*)(pSrc + i * 2));
	}

	return TRUE;
}

/*=============================================================================*
 * FUNCTION: SMDU _UnpackCmd0xfa
 * PURPOSE : unpack, as bar code sigs for record it to flash
 * INPUT: 
 *    
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMDUUnpackCmdfa(INT32 iAddr, 
					  RS485_DEVICE_CLASS* p_SmduDeviceClass, 
					  BYTE* pReceiveData)
{
	//INT32		i;
	//INT32		iChanelNum;
	//INT32		iRoughDataPosition;
	//BYTE*		pRecvDataInfo;	
	//UINT		uiSerialNo;	

	//BYTE	byField, byType;
	//BYTE	byNodeType, byType2, byField1, byField0, byType1, byYear, byType0, byMonth, abyCode[25], byModuleVer;
	//WORD	wNumber;
	//iAddr   [1--8]

	INT32		i;
	//INT32		iChanelNum;
	INT32		iRoughDataPosition;
	BYTE*		pRecvDataInfo;	
	//UINT		uiSerialNo;	

	BYTE	byField;
	BYTE	byField1, byField0,  byYear,  byMonth, abyCode[28], byModuleVer;
	WORD	wNumber;
	//iAddr   [1--8]
	iRoughDataPosition	= (iAddr - 1) * SMDU_RS485_MAX_SIG_END;

	//Point to first sig
	pRecvDataInfo		= pReceiveData + DATA_YDN23_SMDU;
	//TRACE("pRecvDataInfo = %s \n", pRecvDataInfo);

#define BARCODE_LEN		28

	//��29���ֽ���0xFF
	BYTE *pDataEndFlag = pRecvDataInfo + BARCODE_LEN * 2;
	
	//��ʱ����
	/*ASSERT((pDataEndFlag[0] == 'F') && (pDataEndFlag[1] == 'F'));*/

	//0xFF֮ǰ3���ֽ��ǰ汾��Axx��Bxx
	BYTE *pVer = pDataEndFlag - 3 * 2;
	BYTE byVerChar = SMDUMergeAsc((char*)pVer);			// 'A'
	BYTE byVerNum1 = SMDUMergeAsc((char*)pVer + 2);		// '0'
	BYTE byVerNum2 = SMDUMergeAsc((char*)pVer + 4);		// '1'
	//��ʱ����
	//ASSERT((byVerChar >= 'A') && (byVerChar <= 'Z')
	//	&& (byVerNum1 >= '0') && (byVerNum1 <= '9')
	//	&& (byVerNum2 >= '0') && (byVerNum2 <= '9'));
	byModuleVer = (byVerChar - 'A') * 100 + (byVerNum1 - '0') * 10 + (byVerNum2 - '0');
	//TRACE("VER = %c%c%c, ModuleVer = %d \n", byVerChar, byVerNum1, byVerNum2, byModuleVer);

	BYTE *pDataEnd;
	for(pDataEnd = pRecvDataInfo; pDataEnd < pVer; pDataEnd++)
	{
		if((pDataEnd[0] == 'F') && (pDataEnd[1] == 'F'))
		{
			break;
		}
	}

	int nActLen = pDataEnd - pRecvDataInfo;
	//��ʱ����
	/*ASSERT((nActLen % 2) == 0);*/
	int nActBarcodeLen = nActLen / 2;	//�������汾��
	//��ʱ����
	/*ASSERT(nActBarcodeLen <= BARCODE_LEN);*/

	BYTE abyDataInfo[BARCODE_LEN + 1] = {0};
	RS485_AscStringToString((char*)pRecvDataInfo, nActLen, (char*)abyDataInfo);
	//TRACE("abyDataInfo = %s \n", abyDataInfo);
	//��ʱ����
	//ASSERT(nActBarcodeLen == strlen(abyDataInfo));

	//010510123451SM DUxxxyyyyy
	BYTE *pData = abyDataInfo;
	char acTemp[10] = {0};

	abyCode[1] = *pData;
	abyCode[2] = *(pData + 1);

	strncpy(acTemp, (char*)pData, 2);
	byField = atoi(acTemp);
	byField0 = byField & 0x03;	//��2λ
	byField1 = byField >> 2;	//��4λ
	pData += 2;

	strncpy(acTemp, (char*)pData, 2);
	byYear = atoi(acTemp);
	pData += 2;

	strncpy(acTemp, (char*)pData, 2);
	byMonth = atoi(acTemp);
	pData += 2;

	strncpy(acTemp, (char*)pData, 5);
	wNumber = atoi(acTemp);
	pData += 5;

	//TRACE("Field = %d, Year = %d, Number = %d \n", byField, byYear, wNumber);

	//��Ʒ����,ռ��Code12
	abyCode[12] = *pData;
	pData += 1;


	//ģ������, Code13 ~ code24
	for(i = 13; pData < abyDataInfo + nActBarcodeLen; pData++)
	{
		abyCode[i++] = *pData;
	}
	//TRACE("nActBarLen = %d, i = %d \n", nActBarcodeLen, i);
	//	ASSERT(i <= 25);
	for(; i < 25; i++)
	{
		abyCode[i] = 0x20;
	}

	//��ʼ��ͨ��
	UINT uiSnLow = (byField0 << 30) + (byYear << 24) + (byMonth << 16) + wNumber;
	//TRACE("SMDU SERIAL LOW=%08X \n", uiSnLow);

	p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_SERIAL_NO_L].uiValue
		= uiSnLow;

	p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_SERIAL_NO_H].uiValue
		//= (byNodeType << 12) + byField1;	//SMDU��TYPE
		= uiSnLow >> 30;

	p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_VERSION_NO].uiValue	
		= byModuleVer << 16;		//����������汾

	p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BAR_CODE_1].uiValue	
		= (abyCode[1] << 24) + (abyCode[2] << 16) + (abyCode[12] << 8) + abyCode[13];

	p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BAR_CODE_2].uiValue	
		= (abyCode[14] << 24) + (abyCode[15] << 16) + (abyCode[16] << 8) + abyCode[17];

	p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BAR_CODE_3].uiValue	
		= (abyCode[18] << 24) + (abyCode[19] << 16) + (abyCode[20] << 8) + abyCode[21];

	p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BAR_CODE_4].uiValue	
		= (abyCode[22] << 24) + (abyCode[23] << 16) + (abyCode[24] << 8) + 0x20;

	return 0;	

	//p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_FEATURE].uiValue
	//					= RS485_StringToUint(pRecvDataInfo);	
	////Point to next sig
	//pRecvDataInfo += 8;
	//uiSerialNo = RS485_StringToUint(pRecvDataInfo);
	//p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_SERIAL_NO_L].uiValue
	//					= uiSerialNo;
	//p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_SERIAL_NO_H].uiValue
	//					= uiSerialNo >> 30;

	////Point to next sig
	//pRecvDataInfo += 8;
	//p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_VERSION_NO].uiValue	
	//					= RS485_StringToUint(pRecvDataInfo);
	////Point to next sig
	//pRecvDataInfo += 8;
	//p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BAR_CODE_1].uiValue	
	//					= RS485_StringToUint(pRecvDataInfo);
	////Point to next sig
	//pRecvDataInfo += 8;
	//p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BAR_CODE_2].uiValue	
	//					= RS485_StringToUint(pRecvDataInfo);
	////Point to next sig
	//pRecvDataInfo += 8;
	//p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BAR_CODE_3].uiValue	
	//					= RS485_StringToUint(pRecvDataInfo);
	////Point to next sig
	//pRecvDataInfo += 8;
	//p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BAR_CODE_4].uiValue	
	//					= RS485_StringToUint(pRecvDataInfo);

}

/*=============================================================================*
 * FUNCTION: SMDU_Reconfig
 * PURPOSE : scan smdu equip info ,record info to RoughData
 * INPUT:	 p_SmduDeviceClass
 *    
 * RETURN:
 *			p_SmduDeviceClass
 *			INT32
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMDU_Reconfig(RS485_DEVICE_CLASS*  p_SmduDeviceClass)
{
	//SIG_ENUM		emRst; 
	INT32			i;
	//INT32			j;	
	INT32			iAddr;
	SIG_ENUM		enumTemp;
	INT32			iSmduNum;
	INT32			SmduStatus;
	INT32			iRoughDataPosition;
	//BYTE			cExistMaxAddr       = 0;	

	iSmduNum = 0;

	
	for (i = 0; i < SMDU_MAX_NUM; i++)
	{
		
		/****************************************************************************
					�����⴦���ԭ���ǣ���Solution�ļ��У�
					160							0
					161							1
					162			��ַδ������	2
					171
					172
					173
					174
					175									
		****************************************************************************/
		if (i > 2)
		{
			SetEnumSigValue((RS485_SMDU1_BATT_FUSE_EQUIPID + 11) + (i - 3), 
				SIG_TYPE_SAMPLING,
				RS485_WORK_STATUS_SIG_ID,
				RS485_EQUIP_NOT_EXISTENT,
				"RS485_SAMP");
		}
		else
		{
			SetEnumSigValue(RS485_SMDU1_BATT_FUSE_EQUIPID + i, 
				SIG_TYPE_SAMPLING,
				RS485_WORK_STATUS_SIG_ID,
				RS485_EQUIP_NOT_EXISTENT,
				"RS485_SAMP");
		}


		SetEnumSigValue(RS485_SMDU1_LVD_EQUIPID + i, 
					SIG_TYPE_SAMPLING,
					RS485_WORK_STATUS_SIG_ID,
					RS485_EQUIP_NOT_EXISTENT,
					"RS485_SAMP");
	}

	i = 0;
		
	for(iAddr = SMDU_ADDR_START; iAddr < SMDU_ADDR_START + SMDU_MAX_NUM; iAddr++)
	{
		for (i = 0; i < RS485_RECONNECT_TIMES; i++)
		{
			SmduStatus = SMDUSampleGetBarCodeCmdfa(
								p_SmduDeviceClass->pCommPort->hCommPort,
								iAddr,
								SMDU_GET_BARCODE_CID1,
								SMDU_GET_BATCODE_CID2, 
								SMDUszVer,
								p_SmduDeviceClass);

			if (SMDU_RESPONSE_OK == SmduStatus)
			{
				//printf("\n  Reconfig   SMDU  iAddr = %d \n",iAddr);
				//TRACE("\n  SMDU   SMDU  iAddr = %d \n",iAddr);

				SMDUSampleCmd51(iAddr, p_SmduDeviceClass);
				Sleep(20);
				//get DIP switch for smdu check batteries1234 exist
				//get all sig and unpack
				SMDUSampleCmd42(iAddr, p_SmduDeviceClass);

				break;
			}
			else
			{
				//printf("\n  Reconfig FAIL   SMDU  iAddr = %d \n",iAddr);
			}
		}

		iRoughDataPosition = (iAddr - SMDU_ADDR_START) * SMDU_RS485_MAX_SIG_END;

		if (SMDU_RESPONSE_OK == SmduStatus)
		{
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_RS485_COMMUNICATION].iValue
				= SMDU_COMM_OK;
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_RS485_EXISTENCE].iValue
				= SMDU_EQUIP_EXISTENT;
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_RS485_ADDRESS].iValue
				= iAddr;
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_RS485_SEQ_NO].iValue
				= iSmduNum;

			iSmduNum++;		
		}
	}//end for

	if(iSmduNum)
	{
		if(p_SmduDeviceClass->pRoughData[SMDU_GROUP_SM_ACTUAL_NUM].iValue != iSmduNum)
		{
			p_SmduDeviceClass->pRoughData[SMDU_GROUP_SM_ACTUAL_NUM].iValue = iSmduNum;

			SetDwordSigValue(SMDU_RS485_GROUP_EQUIPID, 
							SIG_TYPE_SAMPLING,
							CFG_RS485_CHANGED_SIG_ID,
							1,
							"RS485_SAMP");
		}

		p_SmduDeviceClass->pRoughData[SMDU_GROUP_SM_STATE].iValue 
				= SMDU_EQUIP_EXISTENT;

		/*
			2010/11/11
			�����������󣬲ɼ���CAN�ϵ�SMDU �ͽ�ϵͳ��183�ź���ΪCAN �ɼ�
			����ɼ��� 485�ϵ�SMDU �ͽ� ϵͳ��183�ź���Ϊ 485 �ɼ�
			���ǣ�����û���485�Ͻ���SMDU����CAN��Ҳ����SMDU�����ڲ���������
			����������޷�����
		*/
		enumTemp = 1;//The system running as 485 Mode!!
		SetEnumSigValue(SYS_GROUP_EQUIPID, SIG_TYPE_SETTING, 183, enumTemp, "485SMP Setting Bus Mode");
		
		//Refresh flash,record  smdu infomation
		//SMDU_RefreshFlashInfo();
		for(i = 0; i < SMDU_MAX_NUM; i++)
		{
			g_SmduSamplerData.OLD_CANFlashData.aSmduInfo[i].dwHighSn
				= g_SmduSamplerData.aRoughDataSmdu[i][SMDU_SERIAL_NO_H].dwValue;

			g_SmduSamplerData.OLD_CANFlashData.aSmduInfo[i].dwSerialNo
				= g_SmduSamplerData.aRoughDataSmdu[i][SMDU_SERIAL_NO_L].dwValue;

			g_SmduSamplerData.OLD_CANFlashData.aSmduInfo[i].iSeqNo
				= g_SmduSamplerData.aRoughDataSmdu[i][SMDU_RS485_SEQ_NO].iValue;

			g_SmduSamplerData.OLD_CANFlashData.aSmduInfo[i].iAddress 
				= i;

			if(g_SmduSamplerData.OLD_CANFlashData.aSmduInfo[i].iSeqNo 
				== SMDU_SAMP_INVALID_VALUE)
			{
				g_SmduSamplerData.OLD_CANFlashData.aSmduInfo[i].bExistence = FALSE;
			}
			else
			{
				g_SmduSamplerData.OLD_CANFlashData.aSmduInfo[i].bExistence = TRUE;
			}
		}
	}
	else
	{
		p_SmduDeviceClass->pRoughData[SMDU_GROUP_SM_STATE].iValue  
				= SMDU_EQUIP_NOT_EXISTENT;
	}

	return 0;
}

/*=============================================================================*
 * FUNCTION: SMDU UnpackCmd42
 * PURPOSE : arrange sampling data by 0x41 to  RoughData
 * INPUT: 
 *    
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     by smdu communication protocol Chapter 6.6
 *
 *============================================================================*/
INT32 SMDUUnpackCmd42(INT32					iAddr,
					  SMDUDATASTRUCT		strData[],
					  RS485_DEVICE_CLASS*	p_SmduDeviceClass,
					  BYTE*					pReceiveData)
{
	CHAR	*sStop;   
	INT32	nLoop			= 0;
	//INT32	iTemp			= 0;
	CHAR	sTemp[5]		= {0};
	INT32	iRoughDataIndx	= (iAddr - SMDU_ADDR_START) * SMDU_RS485_MAX_SIG_END;
	INT32	iPointFirstBattExist; 
	INT32	iPointSecondBattExist; 
	INT32	iTempIndex;
	INT32	ibUsedFlag;

	//stuff all receive sig to RoughData
    while( strData[nLoop].nType )					
    {
		//0-end flag,1-4betys(unsigned��,2-4 bytes��signed�� 3-2Bytes
        switch( strData[nLoop].nType )	
        {
        case 1: 
			//check the anolog signal be used
			ibUsedFlag = 0;	

			for (iTempIndex = 0; iTempIndex < 8; iTempIndex++)
			{
				if(*(pReceiveData + strData[nLoop].nOffset + iTempIndex) != 0x20)
				{
					ibUsedFlag = 1;
					break;
				}
			}

			if(ibUsedFlag == 1)
			{
				p_SmduDeviceClass->pRoughData[iRoughDataIndx + strData[nLoop].iRoughPosition].fValue
					= FixFloatDat((char*)pReceiveData + strData[nLoop].nOffset);
			}
			else
			{
				p_SmduDeviceClass->pRoughData[iRoughDataIndx + strData[nLoop].iRoughPosition].iValue
					= SMDU_SAMP_INVALID_VALUE;
			}

            break;
            
        case 2: 
            strncpy( sTemp, (CHAR*)pReceiveData + strData[nLoop].nOffset, 4 );
            sTemp[4]	= 0;
            p_SmduDeviceClass->pRoughData[iRoughDataIndx + strData[nLoop].iRoughPosition].iValue
						= strtoul( sTemp, &sStop, 16 );
            break;

        case 3:
            if(pReceiveData[strData[nLoop].nOffset] == ' ')
            {
                 p_SmduDeviceClass->pRoughData[iRoughDataIndx + strData[nLoop].iRoughPosition].uiValue 
					 =  0x20;
            }
            else
            {
                strncpy(sTemp, (CHAR*)pReceiveData + strData[nLoop].nOffset, 2 );
                sTemp[2] = 0;
                p_SmduDeviceClass->pRoughData[iRoughDataIndx + strData[nLoop].iRoughPosition].uiValue
					= strtoul(sTemp, &sStop, 16);
            }
            break;

        default:
            break;
        }   //End Switch
        
        nLoop++;
    }   //End While
	
	//1 Repeat alone process the DIP switch2  for Batteries be exist
	//Refer to SMDU Design Docment <<GCT�DZTJ�D01>> 16-Page !!!!
	//Check batterier 1 exist
	iPointFirstBattExist  = iRoughDataIndx + SMDU_DIP_SWITCH2_1;
	iPointSecondBattExist = iRoughDataIndx + SMDU_DIP_SWITCH2_2;
	if (p_SmduDeviceClass->pRoughData[iPointFirstBattExist].uiValue == SMDU_SWITCH_ON
		&&p_SmduDeviceClass->pRoughData[iPointSecondBattExist].uiValue == SMDU_SWITCH_OFF)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATT1_EXIST_ST].uiValue 
			= SMDU_EQUIP_NOT_EXISTENT;
	}
	else if (p_SmduDeviceClass->pRoughData[iPointFirstBattExist].uiValue == SMDU_SWITCH_OFF
		&&p_SmduDeviceClass->pRoughData[iPointSecondBattExist].uiValue == SMDU_SWITCH_ON)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATT1_EXIST_ST].uiValue 
			= SMDU_EQUIP_EXISTENT;
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_CURRENT_1].iValue
			= SMDU_SAMP_INVALID_VALUE;
	}
	else
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATT1_EXIST_ST].uiValue 
			= SMDU_EQUIP_NOT_EXISTENT;
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_CURRENT_1].iValue
			= SMDU_SAMP_INVALID_VALUE;
	}

	//Check batterier 2 exist
	iPointFirstBattExist  = iRoughDataIndx + SMDU_DIP_SWITCH2_3;
	iPointSecondBattExist = iRoughDataIndx + SMDU_DIP_SWITCH2_4;
	if (p_SmduDeviceClass->pRoughData[iPointFirstBattExist].uiValue == SMDU_SWITCH_ON
		&&p_SmduDeviceClass->pRoughData[iPointSecondBattExist].uiValue == SMDU_SWITCH_OFF)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATT2_EXIST_ST].uiValue 
			= SMDU_EQUIP_NOT_EXISTENT;
	}
	else if (p_SmduDeviceClass->pRoughData[iPointFirstBattExist].uiValue == SMDU_SWITCH_OFF
		&&p_SmduDeviceClass->pRoughData[iPointSecondBattExist].uiValue == SMDU_SWITCH_ON)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATT2_EXIST_ST].uiValue 
			= SMDU_EQUIP_EXISTENT;
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_CURRENT_2].iValue
			= SMDU_SAMP_INVALID_VALUE;
	}
	else
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATT2_EXIST_ST].uiValue 
			= SMDU_EQUIP_NOT_EXISTENT;
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_CURRENT_2].iValue
			= SMDU_SAMP_INVALID_VALUE;
	}

	//Check batterier 3 exist
	iPointFirstBattExist  = iRoughDataIndx + SMDU_DIP_SWITCH2_5;
	iPointSecondBattExist = iRoughDataIndx + SMDU_DIP_SWITCH2_6;
	if (p_SmduDeviceClass->pRoughData[iPointFirstBattExist].uiValue == SMDU_SWITCH_ON
		&&p_SmduDeviceClass->pRoughData[iPointSecondBattExist].uiValue == SMDU_SWITCH_OFF)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATT3_EXIST_ST].uiValue 
			= SMDU_EQUIP_NOT_EXISTENT;
	}
	else if (p_SmduDeviceClass->pRoughData[iPointFirstBattExist].uiValue == SMDU_SWITCH_OFF
		&&p_SmduDeviceClass->pRoughData[iPointSecondBattExist].uiValue == SMDU_SWITCH_ON)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATT3_EXIST_ST].uiValue 
			= SMDU_EQUIP_EXISTENT;
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_CURRENT_3].iValue
			= SMDU_SAMP_INVALID_VALUE;
	}
	else
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATT3_EXIST_ST].uiValue 
			= SMDU_EQUIP_NOT_EXISTENT;
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_CURRENT_3].iValue
			= SMDU_SAMP_INVALID_VALUE;
	}


	//Check batterier 4 exist
	iPointFirstBattExist  = iRoughDataIndx + SMDU_DIP_SWITCH2_7;
	iPointSecondBattExist = iRoughDataIndx + SMDU_DIP_SWITCH2_8;
	if (p_SmduDeviceClass->pRoughData[iPointFirstBattExist].uiValue == SMDU_SWITCH_ON
		&&p_SmduDeviceClass->pRoughData[iPointSecondBattExist].uiValue == SMDU_SWITCH_OFF)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATT4_EXIST_ST].uiValue 
			= SMDU_EQUIP_NOT_EXISTENT;
	}
	else if (p_SmduDeviceClass->pRoughData[iPointFirstBattExist].uiValue == SMDU_SWITCH_OFF
		&&p_SmduDeviceClass->pRoughData[iPointSecondBattExist].uiValue == SMDU_SWITCH_ON)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATT4_EXIST_ST].uiValue 
			= SMDU_EQUIP_EXISTENT;
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_CURRENT_4].iValue
			= SMDU_SAMP_INVALID_VALUE;
	}
	else
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATT4_EXIST_ST].uiValue 
			= SMDU_EQUIP_NOT_EXISTENT;
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_CURRENT_4].iValue
			= SMDU_SAMP_INVALID_VALUE;
	}
	//2 Repeat the 90---98 signal of 0x42 cmd ,refer to SMDU485 protocol and StdEquip_SMDUUnit.cfg 
	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BUS_BAR_VOLTAGE_ALARM].uiValue 
		== SMDU_BUS_BAR_VOLTAGE_ALARM_VERY_LOW)
	{	
		//less then very low limit
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BUS_BAR_VOLTAGE_ALARM].uiValue 
			= SMDU_BUS_BAR_VOLTAGE_ALARM_NORMAL_LOW;
	}
	else if(p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BUS_BAR_VOLTAGE_ALARM].uiValue 
		== SMDU_BUS_BAR_VOLTAGE_ALARM_VERY_HI)
	{
		//most then very high limit
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BUS_BAR_VOLTAGE_ALARM].uiValue 
			= SMDU_BUS_BAR_VOLTAGE_ALARM_NORMAL_HI;
	}

	//for channel No  6557,	sampling index	SMDU_CURRENT_OVER_CURRENT_1 in RoughData
	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_CURRENT_OVER_CURRENT_1].uiValue 
		== SMDU_OVER_CURRENT_VERY_LOW)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_CURRENT_OVER_CURRENT_1].uiValue 
			= SMDU_OVER_CURRENT_NORMAL_LOW;
	}
	else if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_CURRENT_OVER_CURRENT_1].uiValue 
		== SMDU_OVER_CURRENT_VERY_HI)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_CURRENT_OVER_CURRENT_1].uiValue 
			= SMDU_OVER_CURRENT_NORMAL_HI;
	}

	//for channel No 6558.	sampling index SMDU_CURRENT_OVER_CURRENT_2  in RoughData
	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_CURRENT_OVER_CURRENT_2].uiValue 
		== SMDU_OVER_CURRENT_VERY_LOW)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_CURRENT_OVER_CURRENT_2].uiValue 
			= SMDU_OVER_CURRENT_NORMAL_LOW;
	}
	else if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_CURRENT_OVER_CURRENT_2].uiValue 
		== SMDU_OVER_CURRENT_VERY_HI)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_CURRENT_OVER_CURRENT_2].uiValue 
			= SMDU_OVER_CURRENT_NORMAL_HI;
	}

	//for channel No 6559.	sampling index SMDU_CURRENT_OVER_CURRENT_3  in RoughData
	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_CURRENT_OVER_CURRENT_3].uiValue
		== SMDU_OVER_CURRENT_VERY_LOW)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_CURRENT_OVER_CURRENT_3].uiValue 
			= SMDU_OVER_CURRENT_NORMAL_LOW;
	}
	else if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_CURRENT_OVER_CURRENT_3].uiValue 
		== SMDU_OVER_CURRENT_VERY_HI)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_CURRENT_OVER_CURRENT_3].uiValue 
			= SMDU_OVER_CURRENT_NORMAL_HI;
	}

	//for channel No 6560.	sampling index SMDU_CURRENT_OVER_CURRENT_4  in RoughData
	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_CURRENT_OVER_CURRENT_4].uiValue 
		== SMDU_OVER_CURRENT_VERY_LOW)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_CURRENT_OVER_CURRENT_4].uiValue 
			= SMDU_OVER_CURRENT_NORMAL_LOW;
	}
	else if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_CURRENT_OVER_CURRENT_4].uiValue
		== SMDU_OVER_CURRENT_VERY_HI)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_CURRENT_OVER_CURRENT_4].uiValue 
			= SMDU_OVER_CURRENT_NORMAL_HI;
	}

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATTERY_FUSE_1_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATTERY_FUSE_1_STAT].uiValue = 1;
	}

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATTERY_FUSE_2_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATTERY_FUSE_2_STAT].uiValue = 1;
	}

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATTERY_FUSE_3_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATTERY_FUSE_3_STAT].uiValue = 1;
	}

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATTERY_FUSE_4_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_BATTERY_FUSE_4_STAT].uiValue = 1;
	}

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_1_ARLAM_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_1_ARLAM_STAT].uiValue = 1;
	}

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_2_ARLAM_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_2_ARLAM_STAT].uiValue = 1;
	}

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_3_ARLAM_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_3_ARLAM_STAT].uiValue = 1;
	}

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_4_ARLAM_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_4_ARLAM_STAT].uiValue = 1;
	}

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_5_ARLAM_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_5_ARLAM_STAT].uiValue = 1;
	}

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_6_ARLAM_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_6_ARLAM_STAT].uiValue = 1;
	}

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_7_ARLAM_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_7_ARLAM_STAT].uiValue = 1;
	}

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_8_ARLAM_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_8_ARLAM_STAT].uiValue = 1;
	}

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_9_ARLAM_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_9_ARLAM_STAT].uiValue = 1;
	}

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_10_ARLAM_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_10_ARLAM_STAT].uiValue = 1;
	}

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_11_ARLAM_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_11_ARLAM_STAT].uiValue = 1;
	}

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_12_ARLAM_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_12_ARLAM_STAT].uiValue = 1;
	}

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_13_ARLAM_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_13_ARLAM_STAT].uiValue = 1;
	}	

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_14_ARLAM_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_14_ARLAM_STAT].uiValue = 1;
	}	

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_15_ARLAM_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_15_ARLAM_STAT].uiValue = 1;
	}	

	if (p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_16_ARLAM_STAT].uiValue != 0)
	{
		p_SmduDeviceClass->pRoughData[iRoughDataIndx + SMDU_LOAD_FUSE_16_ARLAM_STAT].uiValue = 1;
	}	

	//SMDU_BATTERY_FUSE_1_STAT
	return 0;
}

/*=============================================================================*
 * FUNCTION: SMDU UnpackCmdeb
 * PURPOSE : arrange sampling data by 0x41 to  RoughData
 * INPUT:	 p_SmduDeviceClass
 *    
 *
 * RETURN:
 *			INT32
 *			RoughData of p_SmduDeviceClass
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *    
 *
 *============================================================================*/
INT32 SMDUUnpackCmdeb(BYTE	cCfgSigSerialNo,
					  INT32 iAddr,
					  RS485_DEVICE_CLASS* p_SmduDeviceClass, 
					  BYTE* pReceiveData)
{
		//SMDUSTRTOFLOAT	stConvertCtoF;
		BYTE*			pPointToDataInfo;
		INT32			iRoughDataPosition;
		float			fSigValueByEb;

		//1	  1	  1	  1	   1        2	   X	2   1
		//1	  2	  2	  2	   2	    4	   2X	4   1
		//SOI VER ADR CID1 CID2/RTN	LENGTH INFO	SUM	EOI
		pPointToDataInfo	= pReceiveData + DATA_YDN23_SMDU;
		fSigValueByEb		= FixFloatDat((char*)pPointToDataInfo);
		iRoughDataPosition	= (iAddr - SMDU_ADDR_START) * SMDU_RS485_MAX_SIG_END;

		if(SMDU_CFG_SERIALNO_UNOVER_VOLT == cCfgSigSerialNo)
		{
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_UNDER_VOLT].fValue
				= fSigValueByEb;
		}
		
		if(SMDU_CFG_SERIALNO_OVER_VOLT == cCfgSigSerialNo )
		{
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_OVER_VOLT].fValue
				= fSigValueByEb;		
		}

		if(SMDU_CFG_SERIALNO_BATT1_OVER_CURR == cCfgSigSerialNo )
		{
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SM_BATT1_OVER_CURR].fValue
				= fSigValueByEb;		
		}

		if(SMDU_CFG_SERIALNO_BATT2_OVER_CURR == cCfgSigSerialNo )
		{
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SM_BATT2_OVER_CURR].fValue
				= fSigValueByEb;		
		}

		if(SMDU_CFG_SERIALNO_BATT3_OVER_CURR == cCfgSigSerialNo )
		{
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SM_BATT3_OVER_CURR].fValue
				= fSigValueByEb;		
		}

		if(SMDU_CFG_SERIALNO_BATT4_OVER_CURR == cCfgSigSerialNo )
		{
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SM_BATT4_OVER_CURR].fValue
				= fSigValueByEb;		
		}
		//added By Jimmy for getting shunt 1~5 mV and A 20110620
		if(cCfgSigSerialNo >= SMDU_CFG_SERIALNO_SHUNT1_A && cCfgSigSerialNo <= SMDU_CFG_SERIALNO_SHUNT4_MV)
		{
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + cCfgSigSerialNo - SMDU_CFG_SERIALNO_SHUNT1_A + SMDU_SHUNT1_A].fValue
				= fSigValueByEb;		
		}
		return 0;
}


/*=============================================================================*
 * FUNCTION: SMDU SampleCmdeb
 * PURPOSE : get OverVoltage and UnoverVoltage Sig,sampling smdu by 0xeb.
 * INPUT: 
 *   
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMDUSampleCmdeb(INT32 iAddr,

					  RS485_DEVICE_CLASS*  p_SmduDeviceClass,
					  int cSigType,
					  int cCfgSigSerialNo)
{
	INT32	i;
	INT32	nUnitNo;
	INT32	iSmduStatus;
	//INT32	iSamplingTimes;
	//INT32	iRoughPosition;	
	INT32	iLenid			= 6;
	BYTE	szSendStr[30]	= {0};
	BYTE	abyRcvBuf[MAX_RECVBUFFER_NUM_SMDU] = {0};
	BYTE	SigSerialNoHi	= 0x00;
	BYTE	SigSerialNoLo	= cCfgSigSerialNo;
	nUnitNo					= iAddr;	
	WORD	wLength			= MakeDataLen(iLenid);
	
	sprintf((char *)szSendStr, 
					"~%02X%02X%02X%02X%04X\0",
					0x20,
					(unsigned int)nUnitNo, 
					SMDU_GET_CFG_DATA_CID1, 
					SMDU_GET_CFG_DATA_CID2,
					wLength);

	sprintf((char*)szSendStr + DATA_YDN23_SMDU,"%02X\0",cSigType);
	sprintf((char*)szSendStr + DATA_YDN23_SMDU + 2,"%02X\0",SigSerialNoHi);
	sprintf((char*)szSendStr + DATA_YDN23_SMDU + 4,"%02X\0",SigSerialNoLo);


	SMDUCheckSum( szSendStr );
	//1	  1	  1	  1	   1        2	   X	2   1
	//1	  2	  2	  2	   2	    4	   2X	4   1
	//SOI VER ADR CID1 CID2/RTN	LENGTH INFO	SUM	EOI
	*(szSendStr + 23) =  SMDU_BYEOI;//17+(datainfo)6
	
	for (i = 0; i < RS485_RECONNECT_TIMES; i++)
	{
		//TRACE("\n !!! Cmdeb	 ADDR = %d\n",nUnitNo);
		iSmduStatus = SMDUSendAndRead(p_SmduDeviceClass->pCommPort->hCommPort,
									szSendStr,
									abyRcvBuf,//g_SmduSamplerData.abyRcvBuf,
									24,
									RS485_WAIT_VERY_SHORT_TIME,
									512);

		if (SMDU_RESPONSE_OK == iSmduStatus)
		{
			break;
		}
	}


	if(iSmduStatus == SMDU_RESPONSE_OK)
	{
		SMDUUnpackCmdeb((BYTE)cCfgSigSerialNo,
						nUnitNo,
						p_SmduDeviceClass, 
						abyRcvBuf);//g_SmduSamplerData.abyRcvBuf);
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*=============================================================================*
* FUNCTION: SMDU UnpackCmd51
* PURPOSE : sampling smio by 0x42 chapter 6.6
* INPUT: 
*				
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMDUUnpackCmd51(INT32 iAddr, 
					  RS485_DEVICE_CLASS*  p_SmduDeviceClass,
					  BYTE*	pReceiveData)
{
	INT32	iRoughPosition;
	INT32	iVerH;
	INT32	iVerL;
	INT32	iSoftwareVertion;
	BYTE*	ppReceiveData;
	ppReceiveData = pReceiveData;
	iRoughPosition = (iAddr - SMDU_ADDR_START) * SMDU_RS485_MAX_SIG_END;
	pReceiveData  += DATA_YDN23_SFT_VER_SMDU;//Point to software vertion high
	iVerH = SMDUMergeAsc((char*)pReceiveData);
	pReceiveData += 2;//Point to version low
	iVerL = SMDUMergeAsc((char*)pReceiveData);
	iSoftwareVertion = (iVerH) * 100 + iVerL;

	//printf(" ##   iSoftwareVertion = %d  pReceiveData= %s \n",
	//	iSoftwareVertion,
	//	pReceiveData);

	p_SmduDeviceClass->pRoughData[iRoughPosition + SMDU_VERSION_NO].iValue += iSoftwareVertion;
	return 0;
}

/*=============================================================================*
* FUNCTION: SMDU SampleCmd51
* PURPOSE : sampling smio by 0x42 chapter 6.6
* INPUT: 
*				
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMDUSampleCmd51(INT32 iAddr, RS485_DEVICE_CLASS*  p_SmduDeviceClass)
{
	INT32	i;
	//INT32	nUnitNo;
	INT32	iSmduStatus;
	//INT32	iSamplingTimes;
	//INT32	iRoughPosition;	
	BYTE	szSendStr[50]	= {0};
	BYTE	abyRcvBuf[MAX_RECVBUFFER_NUM_SMDU] = {0};

	sprintf((char *)szSendStr,
					"~%02X%02X%02X%02X%04X\0", 
					0x20,
					(unsigned int)iAddr, 
					SMDUCID1, 
					SMDUCID2_GETSFTVER,
					0);

	SMDUCheckSum( szSendStr );

	for (i = 0; i < RS485_RECONNECT_TIMES; i++)
	{
		iSmduStatus = SMDUSendAndRead(p_SmduDeviceClass->pCommPort->hCommPort,
										szSendStr,
										abyRcvBuf,
										18,
										RS485_WAIT_LONG_TIME,
										1024);

		if (SMDU_RESPONSE_OK == iSmduStatus)
		{
			break;
		}
		else
		{
			//TRACE("\n !!! Cmd51  SAMPLING ERROR   ADDR = %d\n",iAddr);
		}
	}
	
	if (SMDU_RESPONSE_OK == iSmduStatus)
	{
		SMDUUnpackCmd51(iAddr,p_SmduDeviceClass,abyRcvBuf);
		return 0;
	}
	else
	{
		return -1;
	}
}
/*=============================================================================*
 * FUNCTION: SMDU SampleCmd42
 * PURPOSE : sampling smio by 0x42 chapter 6.6
 * INPUT: 
 *				
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMDUSampleCmd42(INT32 iAddr, RS485_DEVICE_CLASS*  p_SmduDeviceClass)
{
	INT32	i;
	INT32	nUnitNo;
	INT32	iSmduStatus;
	//INT32	iSamplingTimes;
	INT32	iRoughPosition;	
	BYTE	szSendStr[50]	= {0};
	BYTE	abyRcvBuf[MAX_RECVBUFFER_NUM_SMDU] = {0};
	nUnitNo					= iAddr;
	BYTE	Length			= 0x02;
	WORD	wLength			= MakeDataLen(Length);	
	
	sprintf((char *)szSendStr,
					"~%02X%02X%02X%02X%04X\0", 
					0x20,
					(unsigned int)nUnitNo, 
					SMDUCID1, 
					SMDUCID2_GETALL,
					wLength);
	//led coruscate/blink
	sprintf((char*)szSendStr + DATA_YDN23_SMDU,"%02X\0",0x01);

	SMDUCheckSum( szSendStr );
	//1	  1	  1	  1	   1        2	   X	2   1
	//1	  2	  2	  2	   2	    4	   2X	4   1
	//SOI VER ADR CID1 CID2/RTN	LENGTH INFO	SUM	EOI
	*(szSendStr + 19) =  SMDU_BYEOI;

	for (i = 0; i < RS485_RECONNECT_TIMES; i++)
	{
		//TRACE("\n !!! Cmd42  ADDR = %d\n",nUnitNo);
		iSmduStatus = SMDUSendAndRead(p_SmduDeviceClass->pCommPort->hCommPort,
										szSendStr,
										abyRcvBuf,//g_SmduSamplerData.abyRcvBuf,
										20,
										RS485_WAIT_LONG_TIME,
										1024);

		if (SMDU_RESPONSE_OK == iSmduStatus)
		{
			break;
		}
		else
		{
			//TRACE("\n !!! Cmd42  SAMPLING ERROR   ADDR = %d\n",nUnitNo);
		}
	}

	iRoughPosition = (nUnitNo - SMDU_ADDR_START) * SMDU_RS485_MAX_SIG_END;

	if(iSmduStatus == SMDU_RESPONSE_OK)
	{

		//Analyse the receive data by sm du communication protocol Chapter 6.6
		SMDUUnpackCmd42(nUnitNo,
						s_SmduCmd42SigInfo,
						p_SmduDeviceClass,
						abyRcvBuf + DATAF_YDN23_SMDU);//g_SmduSamplerData.abyRcvBuf + DATAF_YDN23_SMDU);
		
		//printf(" du ADDR %d  fValue:%f 42 ok \n",
		//		nUnitNo,
		//		p_SmduDeviceClass->pRoughData[iRoughPosition + SMDU_BATTERY_CURRENT_4].fValue);

		return TRUE;
	}	
	else//(RESPONSE_OK != iSmduStatus)
	{
		return FALSE;	
	}
}


/*=============================================================================*
* FUNCTION: SMIOClrCommBreakTimes
* PURPOSE : Only one time communication ok clean the failt flag
* INPUT:	p_SmduDeviceClass
*    
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32  SMDUClrCommBreakTimes(INT32 iAddr, RS485_DEVICE_CLASS*  p_SmduDeviceClass)
{
	INT32 iRoughPosition = (iAddr - SMDU_ADDR_START) * SMDU_RS485_MAX_SIG_END;
	p_SmduDeviceClass->pRoughData[iRoughPosition + SMDU_INTERRUPT_TIMES].iValue 
		= 0;
	p_SmduDeviceClass->pRoughData[iRoughPosition + SMDU_RS485_COMMUNICATION].iValue 
		= SMDU_COMM_OK;
	return 0;
}

/*=============================================================================*
* FUNCTION: SMDUIncCommBreakTimes
* PURPOSE : Record communication failt time,if more than three means report failes
* INPUT: 
*    
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32  SMDUIncCommBreakTimes(INT32 iAddr, RS485_DEVICE_CLASS*  p_SmduDeviceClass)
{
	INT32 iRoughPosition = (iAddr - SMDU_ADDR_START) * SMDU_RS485_MAX_SIG_END;

	if (p_SmduDeviceClass->pRoughData[iRoughPosition + SMDU_INTERRUPT_TIMES].iValue 
		>= RS485_INTERRUPT_TIMES)
	{
		p_SmduDeviceClass->pRoughData[iRoughPosition + SMDU_RS485_COMMUNICATION].iValue 
			= SMDU_COMM_FAIL;
		return FALSE;	
	}
	else
	{
		if(p_SmduDeviceClass->pRoughData[iRoughPosition + SMDU_INTERRUPT_TIMES].iValue
			< (RS485_INTERRUPT_TIMES + 1) )
		{
			p_SmduDeviceClass->pRoughData[iRoughPosition + SMDU_INTERRUPT_TIMES].iValue++;
		}
		return FALSE;
	}
}

/*=============================================================================*
* FUNCTION: SMDUNeedCommProc
* PURPOSE : Need process the rs485Comm port
* INPUT: 
*    
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32	SMDUNeedCommProc(void)
{
	if(g_RS485Data.CommPort.enumAttr		!= RS485_ATTR_NONE
		||g_RS485Data.CommPort.enumBaud		!= RS485_ATTR_19200
		||g_RS485Data.CommPort.bOpened		!= TRUE)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*=============================================================================*
* FUNCTION: SMDUReopenPort
* PURPOSE :
* INPUT: 
*    
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 SMDUReopenPort(void)
{
	INT32			iErrCode;
	INT32			iOpenTimes;
	iOpenTimes		= 0;

	while (iOpenTimes < RS485_COMM_TRY_OPEN_TIMES)
	{
		if(g_RS485Data.CommPort.hCommPort && g_RS485Data.CommPort.bOpened)
		{
			RS485_Close(g_RS485Data.CommPort.hCommPort);
		}

		g_RS485Data.CommPort.hCommPort 
			= RS485_Open("19200, n, 8, 1", RS485_READ_WRITE_TIMEOUT, (int*)(&iErrCode));

		if(iErrCode == ERR_COMM_OK)
		{
			g_RS485Data.CommPort.bOpened = TRUE;
			g_RS485Data.CommPort.enumAttr = RS485_ATTR_NONE;
			g_RS485Data.CommPort.enumBaud = RS485_ATTR_19200;
			break;
		}
		else
		{
			Sleep(1000);
			iOpenTimes++;
			g_RS485Data.CommPort.bOpened = FALSE;
			AppLogOut("RS485_SMDU_ReOpen", APP_LOG_UNUSED,
				"[%s]-[%d] ERROR: failed to  open rs485Comm IN The SMDU ReopenPort \n\r", 
				__FILE__, __LINE__);
		}
	}//end while

	return 0;
}

/*=============================================================================*
* FUNCTION: bSMDUCheckCommFail
* PURPOSE : 1:Check communication that if sampling the address
*			2:shake hands with SM-DU
* INPUT:	 RS485_DEVICE_CLASS*  p_SmduDeviceClass	INT32 iAddr
*    
*
* RETURN:
*     BOOL TRUE: successful  FALSE: fials
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
BOOL bSMDUCheckCommFail(INT32 iAddr, RS485_DEVICE_CLASS*  p_SmduDeviceClass)
{
	INT32				iRoughDataPosition;
	INT32				iSampCmdEbIdx;
	iRoughDataPosition	= (iAddr - SMDU_ADDR_START) * SMDU_RS485_MAX_SIG_END;
	iSampCmdEbIdx		= p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_SAMPLING_TIMES].iValue;	
	//get cfg signal for PowerKit refer to protocol chapter 6.14
	//Get UnoverVolt Sig, and unpack
	//TRACE(" ##### !!!!!! ##### shake hands iAddr = %d	 iSampCmdEbIdx = %d\n",
	//			iAddr,iSampCmdEbIdx);

	switch (iSampCmdEbIdx)
	{
		case (SMDU_CFG_SERIALNO_UNOVER_VOLT / 2):
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_SAMPLING_TIMES].iValue++;
			return SMDUSampleCmdeb(iAddr,	
						p_SmduDeviceClass,
						SMDU_CFG_TYPE_FLOAT,
						SMDU_CFG_SERIALNO_UNOVER_VOLT);
			
			break;

		case (SMDU_CFG_SERIALNO_OVER_VOLT / 2):
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_SAMPLING_TIMES].iValue++;
			return SMDUSampleCmdeb(iAddr,	
							p_SmduDeviceClass,
							SMDU_CFG_TYPE_FLOAT,
							SMDU_CFG_SERIALNO_OVER_VOLT);
	
			break;
		case (SMDU_CFG_SERIALNO_BATT1_OVER_CURR / 2):
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_SAMPLING_TIMES].iValue++;
			return SMDUSampleCmdeb(iAddr,	
						p_SmduDeviceClass,
						SMDU_CFG_TYPE_FLOAT,
						SMDU_CFG_SERIALNO_BATT1_OVER_CURR);
			break;
		case (SMDU_CFG_SERIALNO_BATT2_OVER_CURR / 2):
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_SAMPLING_TIMES].iValue++;
			return SMDUSampleCmdeb(iAddr,	
						p_SmduDeviceClass,
						SMDU_CFG_TYPE_FLOAT,
						SMDU_CFG_SERIALNO_BATT2_OVER_CURR);
			break;
		case (SMDU_CFG_SERIALNO_BATT3_OVER_CURR / 2):
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_SAMPLING_TIMES].iValue++;
			return SMDUSampleCmdeb(iAddr,	
						p_SmduDeviceClass,
						SMDU_CFG_TYPE_FLOAT,
						SMDU_CFG_SERIALNO_BATT3_OVER_CURR);
			break;
		case (SMDU_CFG_SERIALNO_BATT4_OVER_CURR / 2):
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_SAMPLING_TIMES].iValue++; //= 0;//Changed by Jimmy 20110621
			return	SMDUSampleCmdeb(iAddr,	
						p_SmduDeviceClass,
						SMDU_CFG_TYPE_FLOAT,
						SMDU_CFG_SERIALNO_BATT4_OVER_CURR);
			break;
		default:
			//added by Jimmy for Sampling Shunt 1~5 size
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_SAMPLING_TIMES].iValue = 0;
			BOOL bRetforShunt_Set = TRUE;
			//������ɼ�1��4·�ķ�����ϵ��
			BYTE bySeNo = 0;
			for(bySeNo = (BYTE)SMDU_CFG_SERIALNO_SHUNT1_A; bySeNo <= (BYTE)SMDU_CFG_SERIALNO_SHUNT4_MV; bySeNo++)
			{				
				if(SMDUSampleCmdeb(iAddr,	
					p_SmduDeviceClass,
					SMDU_CFG_TYPE_FLOAT,
					(char)bySeNo))
				{
					bRetforShunt_Set = FALSE;
				}
			}			
			//TRACE("smdu smpling  Idx Error \n");
			return bRetforShunt_Set;
			//return FALSE;
			break;
	}
}

void SM_DcFuseEquipExistProc(RS485_DEVICE_CLASS*  p_SmduDeviceClass)
{
	int		i,iAddr;
	int		iTemp;
	int		iRoughDataPosition;

	for(iAddr = SMDU_ADDR_START; iAddr <= SMDU_ADDR_END; iAddr++)
	{
		iRoughDataPosition = (iAddr - SMDU_ADDR_START) * SMDU_RS485_MAX_SIG_END;
		p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_RS485_DC_FUSE_EXISTENCE].iValue = SMDU_EQUIP_NOT_EXISTENT;
	}

	for(iAddr = SMDU_ADDR_START; iAddr <= SMDU_ADDR_END; iAddr++)
	{
		iRoughDataPosition = (iAddr - SMDU_ADDR_START) * SMDU_RS485_MAX_SIG_END;

		i = iAddr - SMDU_ADDR_START;//GetSmSeqNoByAddr(iAddr);

		if (SMDU_EQUIP_EXISTENT == p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_RS485_EXISTENCE].iValue)
		{
			if ( (SMDU_SAMP_INVALID_VALUE != p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_LOAD_CURRENT_1].iValue)
				|| (SMDU_SAMP_INVALID_VALUE != p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_LOAD_CURRENT_2].iValue)
				|| (SMDU_SAMP_INVALID_VALUE != p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_LOAD_CURRENT_3].iValue)
				|| (SMDU_SAMP_INVALID_VALUE != p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_LOAD_CURRENT_4].iValue))
			{
				p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_RS485_DC_FUSE_EXISTENCE].iValue = SMDU_EQUIP_EXISTENT;
			}
			else
			{
				//������һ���߼����� SWITCH2 ���� �ɶ�Ϊ0 ʱ����SMDU DC fuse �豸��Ϊ���ڡ�
				for (iTemp = 0; iTemp < 4; iTemp++)
				{
					if ((0 == g_SmduSamplerData.aRoughDataSmdu[iAddr - SMDU_ADDR_START][SMDU_DIP_SWITCH2_1 + iTemp * 2].iValue)
						&&(0 == g_SmduSamplerData.aRoughDataSmdu[iAddr - SMDU_ADDR_START][SMDU_DIP_SWITCH2_1 + iTemp * 2 + 1].iValue))
					{
						p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_RS485_DC_FUSE_EXISTENCE].iValue = SMDU_EQUIP_EXISTENT;
					}
					else
					{
						//
					}
				}
				//not process!
			}

			if ( SMDU_EQUIP_EXISTENT == p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BATT1_EXIST_ST].iValue
				||SMDU_EQUIP_EXISTENT == p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BATT2_EXIST_ST].iValue
				||SMDU_EQUIP_EXISTENT == p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BATT3_EXIST_ST].iValue
				||SMDU_EQUIP_EXISTENT == p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BATT4_EXIST_ST].iValue)
			{
				/****************************************************************************
					�����⴦���ԭ���ǣ���Solution�ļ��У�
							160							0
							161							1
							162			��ַδ������	2
							171
							172
							173
							174
							175									
				****************************************************************************/
				if (i > 2)
				{
					SetEnumSigValue((RS485_SMDU1_BATT_FUSE_EQUIPID + 11) + (i - 3), 
									SIG_TYPE_SAMPLING,
									RS485_WORK_STATUS_SIG_ID,
									RS485_EQUIP_EXISTENT,
									"CAN_SAMP");
				}
				else
				{
					SetEnumSigValue(RS485_SMDU1_BATT_FUSE_EQUIPID + i, 
									SIG_TYPE_SAMPLING,
									RS485_WORK_STATUS_SIG_ID,
									RS485_EQUIP_EXISTENT,
									"CAN_SAMP");
				}


				SetEnumSigValue(RS485_SMDU1_LVD_EQUIPID + i, 
								SIG_TYPE_SAMPLING,
								RS485_WORK_STATUS_SIG_ID,
								RS485_EQUIP_EXISTENT,
								"CAN_SAMP");

			}
			else if ( SMDU_EQUIP_EXISTENT != p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BATT1_EXIST_ST].iValue
				&& SMDU_EQUIP_EXISTENT != p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BATT2_EXIST_ST].iValue
				&& SMDU_EQUIP_EXISTENT != p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BATT3_EXIST_ST].iValue
				&& SMDU_EQUIP_EXISTENT != p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BATT4_EXIST_ST].iValue)
			{
				/****************************************************************************
				�����⴦���ԭ���ǣ���Solution�ļ��У�
							160							0
							161							1
							162			��ַδ������	2
							171
							172
							173
							174
							175									
				****************************************************************************/
				if (i > 2)
				{
					SetEnumSigValue((RS485_SMDU1_BATT_FUSE_EQUIPID + 11) + (i - 3), 
									SIG_TYPE_SAMPLING,
									RS485_WORK_STATUS_SIG_ID,
									RS485_EQUIP_NOT_EXISTENT,
									"CAN_SAMP");
				}
				else
				{
					SetEnumSigValue(RS485_SMDU1_BATT_FUSE_EQUIPID + i, 
										SIG_TYPE_SAMPLING,
										RS485_WORK_STATUS_SIG_ID,
										RS485_EQUIP_NOT_EXISTENT,
										"CAN_SAMP");
				}


				SetEnumSigValue(RS485_SMDU1_LVD_EQUIPID + i, 
								SIG_TYPE_SAMPLING,
								RS485_WORK_STATUS_SIG_ID,
								RS485_EQUIP_NOT_EXISTENT,
								"CAN_SAMP");

			}
			else
			{
				/****************************************************************************
				�����⴦���ԭ���ǣ���Solution�ļ��У�
						160							0
						161							1
						162			��ַδ������	2
						171
						172
						173
						174
						175									
				****************************************************************************/
				if (i > 2)
				{
					SetEnumSigValue((RS485_SMDU1_BATT_FUSE_EQUIPID + 11) + (i - 3), 
									SIG_TYPE_SAMPLING,
									RS485_WORK_STATUS_SIG_ID,
									RS485_EQUIP_NOT_EXISTENT,
									"CAN_SAMP");
				}
				else
				{
					SetEnumSigValue(RS485_SMDU1_BATT_FUSE_EQUIPID + i, 
									SIG_TYPE_SAMPLING,
									RS485_WORK_STATUS_SIG_ID,
									RS485_EQUIP_NOT_EXISTENT,
									"CAN_SAMP");
				}


				SetEnumSigValue(RS485_SMDU1_LVD_EQUIPID + i, 
								SIG_TYPE_SAMPLING,
								RS485_WORK_STATUS_SIG_ID,
								RS485_EQUIP_NOT_EXISTENT,
								"CAN_SAMP");

			}
		}
		else
		{
			//not process!!
		}
		

		
	}

	return ;
}


/*=============================================================================*
 * FUNCTION: SMDU Sample
 * PURPOSE : sampling smio all sinal info 
 * INPUT:	 RS485_DEVICE_CLASS*  p_SmduDeviceClass	
 *    
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 SMDUSample(RS485_DEVICE_CLASS*  p_SmduDeviceClass)
{
	INT32			iAddr;
	INT32			iRoughDataPosition;
	static	BOOL	s_bSampCtrlFlag = TRUE; 
	INT32			iExistEquipNum;
	INT32			iSquenceNum;

	if(SMDUNeedCommProc())
	{
		SMDUReopenPort();
	}

	if(p_SmduDeviceClass->bNeedReconfig == TRUE)
	{
		//printf("\nIn config Process for RS485-SMDU");
		p_SmduDeviceClass->bNeedReconfig = FALSE;
		//by CID2 0xfa	0x42  0x51 get Barcode and DIP switch and  software version
		SMDU_Reconfig(p_SmduDeviceClass);
		return 0;
	}

	//1 2 3 ... 8
	iExistEquipNum = p_SmduDeviceClass->pRoughData[SMDU_GROUP_SM_ACTUAL_NUM].iValue;	
	//address [1 --- 8]
	for(iAddr = SMDU_ADDR_START; iAddr <= SMDU_ADDR_END; iAddr++)
	{
		iRoughDataPosition = (iAddr - SMDU_ADDR_START) * SMDU_RS485_MAX_SIG_END;

		if(g_SmduSamplerData.OLD_CANFlashData.aSmduInfo[iAddr - SMDU_ADDR_START].bExistence)
		{			
			RUN_THREAD_HEARTBEAT();
			
			if(SMDUSampleCmd42(iAddr, p_SmduDeviceClass))
			{
				//printf("\n***********rs485 Sampling cmd 42 OK, addr is %d",iAddr);
				SMDUClrCommBreakTimes(iAddr, p_SmduDeviceClass);
			}
			else
			{
				//printf("\n***********rs485 Sampling cmd 42 Err!!!, addr is %d",iAddr);
			}

			//Connect with SM-DU address more than 4
			if ( iExistEquipNum > 4)
			{
				iSquenceNum = iAddr - SMDU_ADDR_START;//GetSmSeqNoByAddr(iAddr);//0 1 2 3 4 .....7
				
				//if (iSquenceNum == SMDU_SAMP_INVALID_VALUE)
				//{
				//	//error
				//	TRACE("\n!! Get Smdu Sequence number error by address !!\n");
				//	continue;
				//}

				if (s_bSampCtrlFlag)
				{
					//iSquenceNum :: 0 1 2 ...7
					if (iSquenceNum < 4)
					{
						if (bSMDUCheckCommFail(iAddr,p_SmduDeviceClass))
						{							
							SMDUClrCommBreakTimes(iAddr, p_SmduDeviceClass);
						}
						else
						{
							SMDUIncCommBreakTimes(iAddr, p_SmduDeviceClass);
						}
						
						continue;
					}
				}
				else
				{
					//no process
				}

				if (!s_bSampCtrlFlag)
				{
					if (iSquenceNum >= 4)
					{
						if (bSMDUCheckCommFail(iAddr,p_SmduDeviceClass))
						{							
							SMDUClrCommBreakTimes(iAddr, p_SmduDeviceClass);
						}
						else
						{
							SMDUIncCommBreakTimes(iAddr, p_SmduDeviceClass);
						}

						continue;
					}
				}
				else
				{
					//no process
				}
			}
			else//if ( iExistEquipNum < 4)
			{
				if (bSMDUCheckCommFail(iAddr,p_SmduDeviceClass))
				{
					SMDUClrCommBreakTimes(iAddr, p_SmduDeviceClass);
				}
				else
				{
					SMDUIncCommBreakTimes(iAddr, p_SmduDeviceClass);
				}
			}

			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_RS485_EXISTENCE].iValue
				= SMDU_EQUIP_EXISTENT;
		}
		else
		{
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BATT1_EXIST_ST].iValue 
				= SMDU_EQUIP_NOT_EXISTENT;
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BATT2_EXIST_ST].iValue 
				= SMDU_EQUIP_NOT_EXISTENT;
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BATT3_EXIST_ST].iValue 
				= SMDU_EQUIP_NOT_EXISTENT;
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_BATT4_EXIST_ST].iValue 
				= SMDU_EQUIP_NOT_EXISTENT;
			p_SmduDeviceClass->pRoughData[iRoughDataPosition + SMDU_RS485_EXISTENCE].iValue 
				= SMDU_EQUIP_NOT_EXISTENT;
		}
	}//end for

	//ֻ���ڱ��������е�ַ֮�󣬲��л�0��1����֤ÿ����ַ���л���ɼ���
	s_bSampCtrlFlag = !s_bSampCtrlFlag;
	
	SM_DcFuseEquipExistProc(p_SmduDeviceClass);

	return TRUE;
}


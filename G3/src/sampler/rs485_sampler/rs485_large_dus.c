#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
//#include <conio.h>
#include <math.h>
#include <time.h>

#include "rs485_main.h"
#include "rs485_comm.h"
#include "rs485_large_dus.h"
#include "rs485_smio.h"
#include "rs485_smbat.h"
#include "rs485_smac.h"
#include "rs485_smdu.h"



INT32		g_iFindEquipMaxNum;//added by IlockTeng
#define		BE_SORFT_SIMULATE	0//simulate
#define		TEST_BE_19200		1

LISTADDR* g_pListAddr;
extern int RS485_ReadForLDU(IN HANDLE hPort, OUT BYTE* pBuffer, IN int nBytesToRead);

//#define __TRACE_COMM_DATA	
//all rough data are in one space
static RS485_VALUE s_aLduRoughData[LDU_G_MAX_SIGNALS_NUM \
+ LDU_MAX_ACD_SIGNALS_NUM * LDU_MAX_ACD_NUM \
+ LDU_MAX_DCD_SIGNALS_NUM * LDU_MAX_DCD_NUM \
+ LDU_MAX_BATT_SIGNALS_NUM * LDU_MAX_BATT_NUM \
+ LDU_MAX_LVD_SIGNALS_NUM * LDU_MAX_LVD_NUM];

//The following is for unpacking the response of GetAll Command, including 
//s_aCfgSig, s_aAcVoltSig, s_aAcCurrSig, s_aFaultLight, s_aAcOutput, s_aAcFail, 
//s_aTemperature, s_aBattInfo, s_aBusVolt, s_aOverVolt, s_aBattOverVolt, and s_aOverTemperature
//Field Meanings: RoughDataIndex--IncrementForStringOffset--MaximumValue--MinimumValue
static LDU_SIG_INFO			s_aCfgSig[] = 
{
	{LDU_DCD_TEMP_NUM,				2,	3,	0,	},
	{LDU_DCD_BATT_NUM,				2,	4,	0,	},
	{LDU_DCD_BRANCH_CURR_NUM,		2,	9,	0,	},
	{LDU_DCD_FUSE_NUM,				2,	64,	0,	},
	{LDU_ACD_INPUT_NUM,				2,	3,	1,	},
	{LDU_ACD_INPUT_TYPE,			2,	2,	0,	},
	{LDU_RESERVE_SIG_ID,			2,	0,	0,	},
	{LDU_RESERVE_SIG_ID,			2,	0,	0,	},

	{LDU_INVALID_SIG_ID,			0,	0,	0,	},
};

static LDU_SIG_INFO			s_aAcVoltSig[] = 
{
	{LDU_MAINS_1_UA,				8,	0,	0,	},
	{LDU_MAINS_1_UB,				8,	0,	0,	},
	{LDU_MAINS_1_UC,				8,	0,	0,	},
	{LDU_MAINS_2_UA,				8,	0,	0,	},
	{LDU_MAINS_2_UB,				8,	0,	0,	},
	{LDU_MAINS_2_UC,				8,	0,	0,	},
	{LDU_MAINS_3_UA,				8,	0,	0,	},
	{LDU_MAINS_3_UB,				8,	0,	0,	},
	{LDU_MAINS_3_UC,				8,	0,	0,	},

	{LDU_INVALID_SIG_ID,			0,	0,	0,	},
};

static LDU_SIG_INFO			s_aAcCurrSig[] = 
{
	{LDU_PA_CURRENT,				8,	0,	0,	},
	{LDU_PB_CURRENT,				8,	0,	0,	},
	{LDU_PC_CURRENT,				8,	0,	0,	},
	{LDU_AC_FREQ,					8,	0,	0,	},

	{LDU_INVALID_SIG_ID,			0,	0,	0,	},
};

static LDU_SIG_INFO			s_aFaultLight[] = 
{
	{LDU_FAULT_LIGHT,				2,	0,	0,	},
	{LDU_M1_INPUT_ST,				2,	0,	0,	},
	{LDU_M2_INPUT_ST,				2,	0,	0,	},
	{LDU_M3_INPUT_ST,				2,	0,	0,	},

	{LDU_INVALID_SIG_ID,			0,	0,	0,	},
};


static LDU_SIG_INFO			s_aAcOutput[] = 
{
	{LDU_AC_OUTPUT1_ST,				2,	0,	0,	},
	{LDU_AC_OUTPUT2_ST,				2,	0,	0,	},
	{LDU_AC_OUTPUT3_ST,				2,	0,	0,	},
	{LDU_AC_OUTPUT4_ST,				2,	0,	0,	},
	{LDU_AC_OUTPUT5_ST,				2,	0,	0,	},
	{LDU_AC_OUTPUT6_ST,				2,	0,	0,	},
	{LDU_AC_OUTPUT7_ST,				2,	0,	0,	},
	{LDU_AC_OUTPUT8_ST,				2,	0,	0,	},
	{LDU_OVER_FREQ_ST,				2,	0,	0,	},
	{LDU_UNDER_FREQ_ST,				2,	0,	0,	},
	//{LDU_INPUT_MCCB_ST,			2,	0,	0,	},
	//{LDU_SPD_TRIP_ST,				2,	0,	0,	},
	//{LDU_OUTPUT_MCCB_ST,			2,	0,	0,	},

	{LDU_INVALID_SIG_ID,			0,	0,	0,	},
};

static LDU_SIG_INFO			s_aAcFail[] = 
{
	{LDU_MAINS1_FAIL_ST,			2,	0,	0,	},
	{LDU_MAINS2_FAIL_ST,			2,	0,	0,	},
	{LDU_MAINS3_FAIL_ST,			2,	0,	0,	},
	{LDU_M1_UA_UNDER_VOLT,			2,	0,	0,	},
	{LDU_M1_UB_UNDER_VOLT,			2,	0,	0,	},
	{LDU_M1_UC_UNDER_VOLT,			2,	0,	0,	},
	{LDU_M2_UA_UNDER_VOLT,			2,	0,	0,	},
	{LDU_M2_UB_UNDER_VOLT,			2,	0,	0,	},
	{LDU_M2_UC_UNDER_VOLT,			2,	0,	0,	},
	{LDU_M3_UA_UNDER_VOLT,			2,	0,	0,	},
	{LDU_M3_UB_UNDER_VOLT,			2,	0,	0,	},
	{LDU_M3_UC_UNDER_VOLT,			2,	0,	0,	},
	{LDU_M1_UA_OVER_VOLT,			2,	0,	0,	},
	{LDU_M1_UB_OVER_VOLT,			2,	0,	0,	},
	{LDU_M1_UC_OVER_VOLT,			2,	0,	0,	},
	{LDU_M2_UA_OVER_VOLT,			2,	0,	0,	},
	{LDU_M2_UB_OVER_VOLT,			2,	0,	0,	},
	{LDU_M2_UC_OVER_VOLT,			2,	0,	0,	},
	{LDU_M3_UA_OVER_VOLT,			2,	0,	0,	},
	{LDU_M3_UB_OVER_VOLT,			2,	0,	0,	},
	{LDU_M3_UC_OVER_VOLT,			2,	0,	0,	},
	{LDU_M1_PHASE_A_FAIL,			2,	0,	0,	},
	{LDU_M1_PHASE_B_FAIL,			2,	0,	0,	},
	{LDU_M1_PHASE_C_FAIL,			2,	0,	0,	},
	{LDU_M2_PHASE_A_FAIL,			2,	0,	0,	},
	{LDU_M2_PHASE_B_FAIL,			2,	0,	0,	},
	{LDU_M2_PHASE_C_FAIL,			2,	0,	0,	},
	{LDU_M3_PHASE_A_FAIL,			2,	0,	0,	},
	{LDU_M3_PHASE_B_FAIL,			2,	0,	0,	},
	{LDU_M3_PHASE_C_FAIL,			2,	0,	0,	},

	{LDU_INVALID_SIG_ID,			0,	0,	0,	},
};




static LDU_SIG_INFO			s_aTemperature[] = 
{
	{LDU_TEMPERATURE1, 				8,	0,	0,	},
	{LDU_TEMPERATURE2,				8,	0,	0,	},
	{LDU_TEMPERATURE3,				8,	0,	0,	},

	{LDU_INVALID_SIG_ID,			0,	0,	0,	},
};


static LDU_SIG_INFO			s_aBattInfo[] = 
{
	{LDU_BATT_VOLT,					8,	0,	0,	},
	{LDU_BATT_CURR,					8,	0,	0,	},

	{LDU_INVALID_SIG_ID,			0,	0,	0,	},
};

static LDU_SIG_INFO			s_aBusVolt[] = 
{
	{LDU_BUS_VOLT,					8,	0,	0,	},
	{LDU_LOAD_CURR,					8,	0,	0,	},
	{LDU_BRANCH1_CURR,				8,	0,	0,	},
	{LDU_BRANCH2_CURR,				8,	0,	0,	},
	{LDU_BRANCH3_CURR,				8,	0,	0,	},
	{LDU_BRANCH4_CURR,				8,	0,	0,	},
	{LDU_BRANCH5_CURR,				8,	0,	0,	},
	{LDU_BRANCH6_CURR,				8,	0,	0,	},
	{LDU_BRANCH7_CURR,				8,	0,	0,	},
	{LDU_BRANCH8_CURR,				8,	0,	0,	},
	{LDU_BRANCH9_CURR,				8,	0,	0,	},

	{LDU_INVALID_SIG_ID,			0,	0,	0,	},
};


static LDU_SIG_INFO			s_aOverVolt[] = 
{
	{LDU_DC_OVER_VOLT,				2,	0,	0,	},
	{LDU_DC_UNDER_VOLT,				2,	0,	0,	},

	{LDU_INVALID_SIG_ID,			0,	0,	0,	},
};


static LDU_SIG_INFO			s_aBattOverVolt[] = 
{
	{LDU_BATT_OVER_VOLT,			2,	0,	0,	},
	{LDU_BATT_UNDER_VOLT,			2,	0,	0,	},
	{LDU_BATT_OVER_CURR,			2,	0,	0,	},

	{LDU_INVALID_SIG_ID,			0,	0,	0,	},
};

static LDU_SIG_INFO			s_aOverTemperature[] = 
{
	{LDU_OVER_TEMP1,				2,	0,	0,	},
	{LDU_OVER_TEMP2,				2,	0,	0,	},
	{LDU_OVER_TEMP3,				2,	0,	0,	},

	{LDU_INVALID_SIG_ID,			0,	0,	0,	},
};

#define	LDU_MAX_TIMES_REOPEN	5
/*==========================================================================*
* FUNCTION : LduResetPort
* PURPOSE  : Reset the rs485 serial port
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: RS485_COMM_PORT*  pCommPort : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 14:07
*==========================================================================*/
static void LduResetPort(RS485_COMM_PORT* pCommPort)
{
	int			iErrCode, iOpenTimes;

	if(pCommPort->bOpened)
	{	 
		RS485_Close(pCommPort->hCommPort);
	}

	iOpenTimes = 0;
	//#if		TEST_BE_19200
	while(iOpenTimes < LDU_MAX_TIMES_REOPEN)
	{
		pCommPort->hCommPort 
			= RS485_Open("19200, n, 8, 1", RS485_READ_WRITE_TIMEOUT, &iErrCode);

		if(iErrCode == ERR_COMM_OK)
		{
			pCommPort->bOpened = TRUE;
			pCommPort->enumAttr = RS485_ATTR_NONE;
			pCommPort->enumBaud = RS485_ATTR_19200;
			break;
		}
		else
		{
			Sleep(1000);
			iOpenTimes++;
			pCommPort->bOpened = FALSE;
		}
	}
	//#elif	(!TEST_BE_19200)
	//	while(iOpenTimes < LDU_MAX_TIMES_REOPEN)
	//	{
	//		pCommPort->hCommPort 
	//			= RS485_Open("9600, n, 8, 1", RS485_READ_WRITE_TIMEOUT, &iErrCode);
	//
	//		if(iErrCode == ERR_COMM_OK)
	//		{
	//			pCommPort->bOpened = TRUE;
	//			pCommPort->enumAttr = RS485_ATTR_NONE;
	//			pCommPort->enumBaud = RS485_ATTR_9600;
	//			break;
	//		}
	//		else
	//		{
	//			Sleep(1000);
	//			iOpenTimes++;
	//			pCommPort->bOpened = FALSE;
	//		}
	//	}
	//#endif


	return;
}

/*=============================================================================*
* FUNCTION: RS485WaitReadable
* PURPOSE : wait RS485 data ready
* INPUT: 
*     
*
* RETURN:
*     int : 1: data ready, 0: timeout,Allow get mode or auto config
*		
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*			Ilockteng		2009-05-19
*============================================================================*/
LOCAL INT32 RS485WaitReadable(INT32 fd, INT32 nmsTimeOut)
{
	fd_set readfd;
	struct timeval timeout;

	while (nmsTimeOut > 0)
	{
		// need add ClearWDT() here.
		if (nmsTimeOut < (5*1000))
		{
			timeout.tv_usec = nmsTimeOut%1000*1000;			/* usec     */
			timeout.tv_sec  = nmsTimeOut/1000;				/* seconds  */

			nmsTimeOut = 0;
		}
		else
		{
			timeout.tv_usec = 0;							/* usec     */
			timeout.tv_sec  = 5;							/* seconds  */

			RUN_THREAD_HEARTBEAT();

			nmsTimeOut -= (5*1000);
		}

	}

	FD_ZERO(&readfd);										/*  Initializes the set to the NULL set. */
	FD_SET(fd, &readfd);									/*  Adds descriptor s to set. */

	select(fd+1, &readfd, NULL, NULL, &timeout);

	if(FD_ISSET(fd, &readfd))	
	{
		return TRUE;										//1: data ready, can be read
	}
	else
	{
		return FALSE;										//0: Data not ready, can't read!!but,Allow get mode or auto config
	}
}

/*==========================================================================*
* FUNCTION : LduReopenPort
* PURPOSE  : Reopen the port if the port is not openned or not odd-check
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: RS485_COMM_PORT*  pCommPort : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-23 11:28
*==========================================================================*/
static void LduReopenPort(RS485_COMM_PORT* pCommPort)
{

	//#if  TEST_BE_19200

	if((!(pCommPort->bOpened)) 
		|| (pCommPort->enumAttr != RS485_ATTR_NONE)
		||(pCommPort->enumBaud != RS485_ATTR_19200))
	{
		LduResetPort(pCommPort);
	}

	//#elif (!TEST_BE_19200)
	//
	//	if((!(pCommPort->bOpened)) 
	//		|| (pCommPort->enumAttr != RS485_ATTR_ODD)
	//		||(pCommPort->enumBaud != RS485_ATTR_9600))
	//	{
	//		LduResetPort(pCommPort);
	//	}
	//
	//#endif
	return;
}



/*==========================================================================*
* FUNCTION : LduGetRoughValue
* PURPOSE  : Get the value from rough data
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iType      : 
*            int  iEquipNo   : 
*            int  iSignalIdx : 
* RETURN   : static RS485_VALUE : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 09:55
*==========================================================================*/
static RS485_VALUE LduGetRoughValue(int iType, int iEquipNo, int iSignalIdx)
{
	RS485_VALUE			Rst;
	switch (iType)
	{
	case LDU_EQUIP_TYPE_GROUP:
		ASSERT((iEquipNo == 0) && (iSignalIdx < LDU_G_MAX_SIGNALS_NUM));
		Rst.iValue = s_aLduRoughData[iSignalIdx].iValue;
		break;

	case LDU_EQUIP_TYPE_ACD:
		ASSERT((iEquipNo < LDU_MAX_ACD_NUM) && (iSignalIdx < LDU_MAX_ACD_SIGNALS_NUM));
		Rst.iValue = s_aLduRoughData[LDU_G_MAX_SIGNALS_NUM \
			+ iEquipNo * LDU_MAX_ACD_SIGNALS_NUM \
			+ iSignalIdx].iValue;
		break;

	case LDU_EQUIP_TYPE_DCD:
		ASSERT((iEquipNo < LDU_MAX_DCD_NUM) && (iSignalIdx < LDU_MAX_DCD_SIGNALS_NUM));
		Rst.iValue = s_aLduRoughData[LDU_G_MAX_SIGNALS_NUM \
			+ LDU_MAX_ACD_NUM * LDU_MAX_ACD_SIGNALS_NUM \
			+ iEquipNo * LDU_MAX_DCD_SIGNALS_NUM \
			+ iSignalIdx].iValue;
		break;

	case LDU_EQUIP_TYPE_BATT:
		ASSERT((iEquipNo < LDU_MAX_BATT_NUM) && (iSignalIdx < LDU_MAX_BATT_SIGNALS_NUM));
		Rst.iValue = s_aLduRoughData[LDU_G_MAX_SIGNALS_NUM \
			+ LDU_MAX_ACD_NUM * LDU_MAX_ACD_SIGNALS_NUM \
			+ LDU_MAX_DCD_NUM * LDU_MAX_DCD_SIGNALS_NUM \
			+ iEquipNo * LDU_MAX_BATT_SIGNALS_NUM \
			+ iSignalIdx].iValue;
		break;

	case LDU_EQUIP_TYPE_LVD:
		ASSERT((iEquipNo < LDU_MAX_LVD_NUM) && (iSignalIdx < LDU_MAX_LVD_SIGNALS_NUM));
		Rst.iValue = s_aLduRoughData[LDU_G_MAX_SIGNALS_NUM \
			+ LDU_MAX_ACD_NUM * LDU_MAX_ACD_SIGNALS_NUM \
			+ LDU_MAX_DCD_NUM * LDU_MAX_DCD_SIGNALS_NUM \
			+ LDU_MAX_BATT_NUM * LDU_MAX_BATT_SIGNALS_NUM \
			+ iEquipNo * LDU_MAX_LVD_SIGNALS_NUM \
			+ iSignalIdx].iValue;
		break;

	default:
		break;

	}

	return Rst;
}


/*==========================================================================*
* FUNCTION : LduSetRoughValue
* PURPOSE  : Set a value into rough data
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int          iType      : 
*            int          iEquipNo   : 
*            int          iSignalIdx : 
*            RS485_VALUE  SetValue   : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 09:57
*==========================================================================*/
static void LduSetRoughValue(int iType, int iEquipNo, int iSignalIdx, RS485_VALUE SetValue)
{

	switch (iType)
	{
	case LDU_EQUIP_TYPE_GROUP:
		ASSERT((iEquipNo == 0) && (iSignalIdx < LDU_G_MAX_SIGNALS_NUM));
		s_aLduRoughData[iSignalIdx].iValue = SetValue.iValue;
		break;

	case LDU_EQUIP_TYPE_ACD:
		ASSERT((iEquipNo < LDU_MAX_ACD_NUM) && (iSignalIdx < LDU_MAX_ACD_SIGNALS_NUM));
		s_aLduRoughData[LDU_G_MAX_SIGNALS_NUM \
			+ iEquipNo * LDU_MAX_ACD_SIGNALS_NUM \
			+ iSignalIdx].iValue = SetValue.iValue;
		break;

	case LDU_EQUIP_TYPE_DCD:
		ASSERT((iEquipNo < LDU_MAX_DCD_NUM) && (iSignalIdx < LDU_MAX_DCD_SIGNALS_NUM));
		/*if(!((iEquipNo < LDU_MAX_DCD_NUM) && (iSignalIdx < LDU_MAX_DCD_SIGNALS_NUM)))
		{
		printf("Error iEquipNo = %d or iSignalIdx = %d\n", iEquipNo, iSignalIdx);
		break;
		}*/
		s_aLduRoughData[LDU_G_MAX_SIGNALS_NUM \
			+ LDU_MAX_ACD_NUM * LDU_MAX_ACD_SIGNALS_NUM \
			+ iEquipNo * LDU_MAX_DCD_SIGNALS_NUM \
			+ iSignalIdx].iValue = SetValue.iValue;
		break;

	case LDU_EQUIP_TYPE_BATT:
		ASSERT((iEquipNo < LDU_MAX_BATT_NUM) && (iSignalIdx < LDU_MAX_BATT_SIGNALS_NUM));
		s_aLduRoughData[LDU_G_MAX_SIGNALS_NUM \
			+ LDU_MAX_ACD_NUM * LDU_MAX_ACD_SIGNALS_NUM \
			+ LDU_MAX_DCD_NUM * LDU_MAX_DCD_SIGNALS_NUM \
			+ iEquipNo * LDU_MAX_BATT_SIGNALS_NUM \
			+ iSignalIdx].iValue = SetValue.iValue;
		break;

	case LDU_EQUIP_TYPE_LVD:
		ASSERT((iEquipNo < LDU_MAX_LVD_NUM) && (iSignalIdx < LDU_MAX_LVD_SIGNALS_NUM));
		/*if(!((iEquipNo < LDU_MAX_LVD_NUM) && (iSignalIdx < LDU_MAX_LVD_SIGNALS_NUM)))
		{
		printf("Error iEquipNo = %d or iSignalIdx = %d\n", iEquipNo, iSignalIdx);
		break;
		}*/

		s_aLduRoughData[LDU_G_MAX_SIGNALS_NUM \
			+ LDU_MAX_ACD_NUM * LDU_MAX_ACD_SIGNALS_NUM \
			+ LDU_MAX_DCD_NUM * LDU_MAX_DCD_SIGNALS_NUM \
			+ LDU_MAX_BATT_NUM * LDU_MAX_BATT_SIGNALS_NUM \
			+ iEquipNo * LDU_MAX_LVD_SIGNALS_NUM \
			+ iSignalIdx].iValue = SetValue.iValue;
		break;

	default:
		break;

	}

	return;
}



/*==========================================================================*
* FUNCTION : ByteToHexStr
* PURPOSE  : Transform  a Byte to hex string.
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE  byValue    : 
*            BYTE  *pbyHexStr : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-06-24 15:07
*==========================================================================*/
static void ByteToHexStr(IN BYTE byValue, OUT BYTE *pbyHexStr)
{
	BYTE	byHighHalf, byLowHalf;

	byHighHalf = (byValue & 0xF0) >> 4;
	byLowHalf = byValue & 0x0F;

	//Yes. the second char in the output string represents high half
	if(byHighHalf <= 9) 
	{
		*(pbyHexStr + 1) = byHighHalf + '0';
	}
	else if((byHighHalf >= 10) && (byHighHalf <= 15))
	{
		*(pbyHexStr + 1) = byHighHalf - 10 + 'A';
	}

	if(byLowHalf <= 9) 
	{
		*pbyHexStr = byLowHalf + '0';
	}
	else if((byLowHalf >= 10) && (byLowHalf <= 15))
	{
		*pbyHexStr = byLowHalf - 10 + 'A';
	}

	return;
}


/*==========================================================================*
* FUNCTION : HexStrToByte
* PURPOSE  : Transform a hex string to Byte
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN BYTE*  pbyHexStr : 
* RETURN   : static BYTE : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 10:00
*==========================================================================*/
static BYTE HexStrToByte(IN BYTE* pbyHexStr)
{
	BYTE	byHi, byLo;

	*pbyHexStr &= 0x7F;
	*(pbyHexStr + 1) &= 0x7F;

	byHi = 0;
	byLo = 0;

	//Yes. the second char in the input string represents high half
	if((*(pbyHexStr + 1) >= '0') && (*(pbyHexStr + 1) <= '9'))
	{
		byHi = *(pbyHexStr + 1) - '0';
	}
	else if((*(pbyHexStr + 1) >= 'A') && (*(pbyHexStr + 1) <= 'F'))
	{
		byHi = *(pbyHexStr + 1) - 'A' + 0x0A;
	}
	else if((*(pbyHexStr + 1) >= 'a') && (*(pbyHexStr + 1) <= 'f'))
	{
		byHi = *(pbyHexStr + 1) - 'a' + 0x0A;
	}


	if((*pbyHexStr >= '0') && (*pbyHexStr <= '9'))
	{
		byLo = *pbyHexStr - '0';
	}
	else if((*pbyHexStr >= 'A') && (*pbyHexStr <= 'F'))
	{
		byLo = *pbyHexStr - 'A' + 0x0A;
	}
	else if((*pbyHexStr >= 'a') && (*pbyHexStr <= 'f'))
	{
		byLo = *pbyHexStr - 'a' + 0x0A;
	}

	return (byHi << 4) + byLo; 
}



/*==========================================================================*
* FUNCTION : HexStrToFloat
* PURPOSE  : Transform a hex string to Float
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN BYTE*  pbyHexStr : 
* RETURN   : static float : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 10:01
*==========================================================================*/
static float HexStrToFloat(IN BYTE* pbyHexStr)
{
	RS485_FLOAT_STRING	unValue;
	int					i;

	for(i = 0; i < 4; i++)
	{
		unValue.abyValue[i] = HexStrToByte(pbyHexStr + i * 2);
	}

	return unValue.fValue; 
}


/*==========================================================================*
* FUNCTION : LduCrc12
* PURPOSE  : Calculate CRC12 code for pbyBuf with iLen length
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE*  pbyBuf : 
*            int    iLen   : 
* RETURN   : static WORD : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 10:01
*==========================================================================*/
#define	LDU_CRC_GEN_POLY	0x180D		//Generator Polynomial
static WORD	LduCrc12(BYTE* pbyBuf, int iLen)
{
	int		i;
	WORD	wCRC = 0;
	int		iByteOffset = 0;
	int		iBitOffset = 0;

	while(iByteOffset < iLen)
	{
		/*if the bit is 1, the corresponding CRC bit will be 1 */
		if(pbyBuf[iByteOffset] & (0x80 >> iBitOffset))
		{
			wCRC |= 0x0001;
		}

		if(wCRC >= 0x1000)
		{
			wCRC ^= LDU_CRC_GEN_POLY;
		}

		wCRC <<= 1;

		iBitOffset++;
		if(iBitOffset == 8)
		{
			iBitOffset = 0;
			iByteOffset++;
		}
	}

	/*Handle last 12 bits*/
	for(i = 0; i < 12; i++)
	{
		if(wCRC >= 0x1000)
		{
			wCRC ^= LDU_CRC_GEN_POLY;
		}

		wCRC <<= 1;
	}

	wCRC >>= 1;

	return wCRC;
}


/*==========================================================================*
* FUNCTION : LduParity
* PURPOSE  : Make the string meet parity rule
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE*  pbyValue : 
*            BYTE   byFlag   : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 10:56
*==========================================================================*/
void LduParity(BYTE* pbyValue, BYTE byFlag)
{
	int 	iCount = 0;
	int		i;

	byFlag &= 0x01;

	//Count the number of bit = 1
	for( i = 0; i < 8; i++)
	{
		if(((*pbyValue) << i ) & 0x80)
		{
			iCount++;
		}
	}

	//If last bit of iCount is 1, there are even number of bit = 1 in the byte
	if((iCount & 0x00000001) ^ byFlag)
	{
		(*pbyValue) |= 0x80;
	}
	return;
}


#define	LDU_MAX_SEND_TIMES 3
/*==========================================================================*
* FUNCTION : LduPackSendCmd
* PURPOSE  : Package a command frame anf send it
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*          pDevice  : 
*            LDU_SEND_CMD*  pSendCmd : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 10:58
*==========================================================================*/
static void LduPackSendCmd(void* pDevice, LDU_SEND_CMD* pSendCmd)
{
	RS485_DEVICE_CLASS*		pLdu = pDevice;

	int						i;
	int						iOffset = FRAME_OFFSET_DATA;
	int						iSendLen;
	int						iSendTimes;

	WORD					wValue;
	//BYTE					byValue;
	BYTE					abyFloatData[4];

	BYTE*					pbySenfBuf = pLdu->pCommPort->abySendBuf;


	pbySenfBuf[FRAME_OFFSET_SOI] = LDU_SOI_BYTE;

	ByteToHexStr(pSendCmd->byAddr, pbySenfBuf + FRAME_OFFSET_ADDR);		
	ByteToHexStr(pSendCmd->byCmdId, pbySenfBuf + FRAME_OFFSET_CMD_ID);

	if(pSendCmd->wLduSigId != LDU_INVALID_SIG_ID)
	{
		ByteToHexStr(pSendCmd->wLduSigId & 0x00ff, pbySenfBuf + iOffset);
		ByteToHexStr((pSendCmd->wLduSigId & 0xff00) >> 8, pbySenfBuf + iOffset + 2);

		iOffset += 4;
	}

	switch(pSendCmd->iValueType)
	{
	case LDU_VALUE_TYPE_BYTE:
		ByteToHexStr((BYTE)(pSendCmd->fValue), pbySenfBuf + iOffset);
		iOffset += 2;
		break;

	case LDU_VALUE_TYPE_WORD:
		ByteToHexStr((((WORD)pSendCmd->fValue) & 0xff00) >> 8, pbySenfBuf + iOffset + 2);
		ByteToHexStr(((WORD)pSendCmd->fValue) & 0x00ff, pbySenfBuf + iOffset);
		iOffset += 4;
		break;

	case LDU_VALUE_TYPE_FLOAT:
		RS485_FloatToString(pSendCmd->fValue, abyFloatData);
		ByteToHexStr(abyFloatData[0], pbySenfBuf + iOffset + 0);
		ByteToHexStr(abyFloatData[1], pbySenfBuf + iOffset + 2);
		ByteToHexStr(abyFloatData[2], pbySenfBuf + iOffset + 4);
		ByteToHexStr(abyFloatData[3], pbySenfBuf + iOffset + 6);
		iOffset += 8;
		break;
	default:
		break;
	}

	wValue = (WORD)(iOffset - FRAME_OFFSET_DATA);
	//ByteToHexStr(wValue >> 8, pbySenfBuf + FRAME_OFFSET_LENGTH);
	//ByteToHexStr(wValue & 0x00ff, pbySenfBuf + FRAME_OFFSET_LENGTH + 2);
	//Send Low Byte first, and then High byte
	ByteToHexStr(wValue & 0x00ff, pbySenfBuf + FRAME_OFFSET_LENGTH);
	ByteToHexStr(wValue >> 8, pbySenfBuf + FRAME_OFFSET_LENGTH + 2);


	//Calculate CRC12 code
	wValue = LduCrc12(pbySenfBuf + 1, iOffset - 1);
	ByteToHexStr(wValue & 0x00ff, pbySenfBuf + iOffset);
	ByteToHexStr(wValue >> 8, pbySenfBuf + iOffset + 2);

	iOffset += 4;

	pbySenfBuf[iOffset] = LDU_EOI_BYTE;

	iSendLen = iOffset + 1;

	//////////////////////////////////////////////////////////////////////////
#ifdef __TRACE_COMM_DATA
	pbySenfBuf[iSendLen] = NULL;
	TRACE("RS485::SendBuf is %s\n", pbySenfBuf);
#endif

	for(i = 0; i < iSendLen; i++)
	{
		//To make the number of (bit = 1) is even in every SOI/ADDR byte.
		if( i < FRAME_OFFSET_CMD_ID )
		{
			LduParity(pbySenfBuf + i, LDU_EVEN);
		}
		//To make the number of (bit = 1) is odd in others.
		else
		{
			LduParity(pbySenfBuf + i, LDU_ODD);
		}
	}

	//////////////////////////////////////////////////////////////////////////
#ifdef __TRACE_COMM_DATA
	//pbySenfBuf[iSendLen] = NULL;
	//TRACE("RS485::After Parity, SendBuf is %s\n", pbySenfBuf);
#endif

	iSendTimes = 0;
	while(iSendLen < RS485_Write(pLdu->pCommPort->hCommPort, pbySenfBuf, iSendLen))
	{
		iSendTimes++;
		if(iSendTimes > LDU_MAX_SEND_TIMES)
		{
			break;
		}
	}

	//Sleep(260);

	return;

}


/*==========================================================================*
* FUNCTION : LduGetAcdNo
* PURPOSE  : Get the ACD equipment No. whose addr is byAddr, or return the 
*			  available No. that can be found.
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*  pDevice : 
*            BYTE   byAddr  : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-06-30 13:35
*==========================================================================*/
static int LduGetAcdNo(BYTE byAddr)
{
	RS485_VALUE				valueExist, valueAddr;
	int						i;

	for(i = 0; i < LDU_MAX_ACD_NUM; i++)
	{
		valueExist = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_EXISTS);
		valueAddr = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_ADDR);

		if(valueExist.iValue == LDU_EQUIP_EXIST)
		{
			if(valueAddr.dwValue == byAddr)
			{
				return i;
			}
			else
			{
				continue;
			}
		}
		else
		{
			return i;
		}
	}

	return -1; // get no equip no..
}


/*==========================================================================*
* FUNCTION : LduGetDcdNo
* PURPOSE  : Get the DCD equipment No. whose addr is byAddr, or return the 
*			  available No. that can be found.
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE  byAddr : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:01
*==========================================================================*/
static int LduGetDcdNo(BYTE byAddr)
{
	RS485_VALUE				valueExist, valueAddr;
	int						i;

	for(i = 0; i < LDU_MAX_DCD_NUM; i++)
	{
		valueExist = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_EXISTS);
		valueAddr = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_ADDR);

		if(valueExist.iValue == LDU_EQUIP_EXIST)
		{
			if(valueAddr.dwValue == byAddr)
			{
				return i;
			}
			else
			{
				continue;
			}
		}
		else
		{
			return i;
		}
	}

	return -1; // no equip no. getted.
}


/*==========================================================================*
* FUNCTION : LduGetBattNo
* PURPOSE  : Get the first Battery String equipment No. which is in the DCD
*			  with iDcdEquipNo as equipment No.
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int  iDcdEquipNo : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:02
*==========================================================================*/
static int LduGetBattNo(int iDcdEquipNo)
{
	RS485_VALUE				valueBattNum;
	int						i;
	int						iBattEquipNo = 0;

	for(i = 0; i < iDcdEquipNo; i++)
	{
		valueBattNum = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_BATT_NUM);

		if((valueBattNum.iValue >= 0)
			&& (valueBattNum.iValue <= LDU_MAX_BATT_NUM_EACH_DCD))
		{
			iBattEquipNo +=  valueBattNum.iValue;
		}
	}

	return iBattEquipNo;
}


/*==========================================================================*
* FUNCTION : LduGetEquipNo
* PURPOSE  : Get the equipment No. by Address anf Equipment Type
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE  byAddr     : 
*            int   iEquipType : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:04
*==========================================================================*/
static int LduGetEquipNo(BYTE byAddr, int iEquipType)
{
	switch(iEquipType)
	{
	case LDU_EQUIP_TYPE_ACD:
		return LduGetAcdNo(byAddr);
	case LDU_EQUIP_TYPE_DCD:
		return LduGetDcdNo(byAddr);
	case LDU_EQUIP_TYPE_BATT:
		return LduGetBattNo(LduGetDcdNo(byAddr));
	case LDU_EQUIP_TYPE_LVD:
		return LduGetDcdNo(byAddr);
	default:
		return -1;
	}
}

#define	LDU_AC_INPUT_TYPE_SINGLE		0
/*==========================================================================*
* FUNCTION : LduUnpackAcdInfo
* PURPOSE  : Unpack response string of GetAll Command into ACD rough data 
*            for AC-LargeDU
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE*  pbyRcvBuf : 
*            BYTE   byAddr    : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:07
*==========================================================================*/
static BOOL LduUnpackAcdInfo(BYTE* pbyRcvBuf, BYTE byAddr)
{
	//RS485_DEVICE_CLASS*		pLdu = pDevice;
	RS485_VALUE				value;
	RS485_VALUE				tempValue;
	int						i, j, iAcInputNum, iAcInputType;
	int						iOffset = FRAME_OFFSET_DATA;

	int						iAcdEquipNo;

	iAcdEquipNo = LduGetAcdNo(byAddr);
	if(iAcdEquipNo < 0)
	{
		return FALSE;
	}

	//AC Input Number and AC Input Type
	//iOffset += LDU_OFFSET_AC_INPUT_NUM;
	i = 0;
	while(s_aCfgSig[i].iRoughDataIdx != LDU_INVALID_SIG_ID)
	{
		if((LDU_ACD_INPUT_NUM == s_aCfgSig[i].iRoughDataIdx)
			|| (LDU_ACD_INPUT_TYPE == s_aCfgSig[i].iRoughDataIdx))
		{
			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			if((value.iValue > s_aCfgSig[i].iMaxValue) 
				|| (value.iValue < s_aCfgSig[i].iMinValue))
			{
				return FALSE;
			}
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aCfgSig[i].iRoughDataIdx, value);
		}
		iOffset += s_aCfgSig[i].iOffsetStep;
		i++;
	}


	value.iValue = LDU_EQUIP_EXIST;
	LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_ACD_EXISTS, value);
	value.dwValue = byAddr;
	LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_ACD_ADDR, value);
	//printf(" Acd $$$$$$$$ iAcdEquipNo = %d addr = %d\n",iAcdEquipNo,byAddr);

	value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_ACD_INPUT_NUM);
	iAcInputNum = value.iValue;
	value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_ACD_INPUT_TYPE);
	iAcInputType = value.iValue;


	//Mains Voltage
	if(iAcInputType == LDU_AC_INPUT_TYPE_SINGLE)	//Single Phase
	{
		tempValue.iValue = RS485_SAMP_INVALID_VALUE;
		for(i = 0; i < iAcInputNum; i++)
		{
			value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcVoltSig[i * 3 + 0].iRoughDataIdx, value);
			iOffset += s_aAcVoltSig[i * 3 + 0].iOffsetStep;
			
			//增加屏蔽 从三相到单相的问题
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcVoltSig[i * 3 + 1].iRoughDataIdx, tempValue);
			//增加屏蔽 从三相到单相的问题
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcVoltSig[i * 3 + 2].iRoughDataIdx, tempValue);
		}
	}
	else											//Three Phase
	{
		for(i = 0; i < iAcInputNum; i++)
		{
			value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcVoltSig[i * 3 + 0].iRoughDataIdx, value);
			iOffset += s_aAcVoltSig[i * 3 + 0].iOffsetStep;

			value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcVoltSig[i * 3 + 1].iRoughDataIdx, value);
			iOffset += s_aAcVoltSig[i * 3 + 1].iOffsetStep;

			value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcVoltSig[i * 3 + 2].iRoughDataIdx, value);
			iOffset += s_aAcVoltSig[i * 3 + 2].iOffsetStep;
		}
	}

	//Three phase current and frequency
	i = 0;
	while(s_aAcCurrSig[i].iRoughDataIdx != LDU_INVALID_SIG_ID)
	{
		value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcCurrSig[i].iRoughDataIdx, value);
		iOffset += s_aAcCurrSig[i].iOffsetStep;

		i++;
	}


	//Fault Light and Input State
	value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
	LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aFaultLight[0].iRoughDataIdx, value);
	iOffset += s_aFaultLight[0].iOffsetStep;

	for(i = 0; i < iAcInputNum; i++)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aFaultLight[i + 1].iRoughDataIdx, value);
		iOffset += s_aFaultLight[i + 1].iOffsetStep;
	}

	//AC Output and Freq under/over
	i = 0;
	while(s_aAcOutput[i].iRoughDataIdx != LDU_INVALID_SIG_ID)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcOutput[i].iRoughDataIdx, value);
		iOffset += s_aAcOutput[i].iOffsetStep;

		i++;
	}

	//AC Input MCCB, SPD_TRIP, Output MCCB
	value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
	LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_INPUT_MCCB_ST, value);
	iOffset += 2;

	value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
	LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_SPD_TRIP_ST, value);
	iOffset += 2;

	value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
	LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_OUTPUT_MCCB_ST, value);
	iOffset += 2;


	//AC Main Fail
	for(i = 0; i < iAcInputNum; i++)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_MAINS1_FAIL_ST + i, value);
		iOffset += 2;
	}

	//AC Input Under Volt
	for(i = 0; i < iAcInputNum; i++)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UA_UNDER_VOLT + i, value);
		iOffset += 2;

		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UB_UNDER_VOLT + i, value);
		iOffset += 2;

		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UC_UNDER_VOLT + i, value);
		iOffset += 2;

	}

	//AC Input Over Volt
	for(i = 0; i < iAcInputNum; i++)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UA_OVER_VOLT + i, value);
		iOffset += 2;

		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UB_OVER_VOLT + i, value);
		iOffset += 2;

		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UC_OVER_VOLT + i, value);
		iOffset += 2;

	}

	//AC Input Phase Fail
	for(i = 0; i < iAcInputNum; i++)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_PHASE_A_FAIL + i, value);
		iOffset += 2;

		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_PHASE_B_FAIL + i, value);
		iOffset += 2;

		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_PHASE_C_FAIL + i, value);
		iOffset += 2;

	}

	return TRUE;
}



/*==========================================================================*
* FUNCTION : LduUnpackDcdInfo
* PURPOSE  : Unpack response string of GetAll Command into DCD rough data 
*            for DC-LargeDU
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE*  pbyRcvBuf : 
*            BYTE   byAddr    : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:09
*==========================================================================*/
static BOOL LduUnpackDcdInfo(BYTE* pbyRcvBuf, BYTE byAddr)
{
	RS485_VALUE		value;
	int				i, j, iTemperatureNum, iBattNum, iBranchCurrNum, iFuseNum;
	int				iOffset = FRAME_OFFSET_DATA;

	int				iDcdEquipNo;
	int				iBattEquipNo;
	int				iLvdEquipNo;

	iDcdEquipNo = LduGetDcdNo(byAddr);
	if(iDcdEquipNo < 0)
	{
		return FALSE;
	}

	iLvdEquipNo = iDcdEquipNo;
	iBattEquipNo = LduGetBattNo(iDcdEquipNo);

	//Configuration information
	i = 0;
	while(s_aCfgSig[i].iRoughDataIdx != LDU_INVALID_SIG_ID)
	{
		if((LDU_DCD_TEMP_NUM == s_aCfgSig[i].iRoughDataIdx)
			|| (LDU_DCD_BATT_NUM == s_aCfgSig[i].iRoughDataIdx)
			|| (LDU_DCD_BRANCH_CURR_NUM == s_aCfgSig[i].iRoughDataIdx)
			|| (LDU_DCD_FUSE_NUM == s_aCfgSig[i].iRoughDataIdx))
		{
			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			if((value.iValue > s_aCfgSig[i].iMaxValue) 
				|| (value.iValue < s_aCfgSig[i].iMinValue))
			{
				return FALSE;
			}
			LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, s_aCfgSig[i].iRoughDataIdx, value);
		}
		iOffset += s_aCfgSig[i].iOffsetStep;
		i++;
	}

	value.iValue = LDU_EQUIP_EXIST;
	LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_DCD_EXISTS, value);
	value.dwValue = byAddr;
	LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_DCD_ADDR, value);

	value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_DCD_TEMP_NUM);
	iTemperatureNum = value.iValue;
	value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_DCD_BATT_NUM);
	iBattNum = value.iValue;
	value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_DCD_BRANCH_CURR_NUM);
	iBranchCurrNum = value.iValue;
	value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_DCD_FUSE_NUM);
	iFuseNum = value.iValue;

	for(i = 0; i < iBattNum; i++)
	{
		value.iValue = LDU_EQUIP_EXIST;
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, LDU_BATT_EXISTS, value);
		value.iValue = iDcdEquipNo + 1;
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, LDU_BATT_DCD_NO, value);
	}

	value.iValue = LDU_EQUIP_EXIST;
	LduSetRoughValue(LDU_EQUIP_TYPE_LVD, iLvdEquipNo, LDU_LVD_EQUIP_EXISTS, value);
	value.dwValue = iDcdEquipNo + 1;
	LduSetRoughValue(LDU_EQUIP_TYPE_LVD, iLvdEquipNo, LDU_LVD_DCD_NO, value);

	//Temperature
	for(i = 0; i < iTemperatureNum; i++)
	{
		value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, s_aTemperature[i].iRoughDataIdx, value);
		iOffset += s_aTemperature[i].iOffsetStep;
	}

	//First 2 strings of Battery Voltage
	for(i = 0; (i < iBattNum) && (i < 2); i++)
	{
		value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, s_aBattInfo[0].iRoughDataIdx, value);
		iOffset += s_aBattInfo[0].iOffsetStep;
	}

	//First 2 strings of Battery Current
	for(i = 0; (i < iBattNum) && (i < 2); i++)
	{
		value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, s_aBattInfo[1].iRoughDataIdx, value);
		iOffset += s_aBattInfo[1].iOffsetStep;
	}

	//Bus voltage, Load Current and Branch Current
	value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
	LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, s_aBusVolt[0].iRoughDataIdx, value);
	iOffset += s_aBusVolt[0].iOffsetStep;
	value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
	LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, s_aBusVolt[1].iRoughDataIdx, value);
	iOffset += s_aBusVolt[1].iOffsetStep;

	for(i = 0; i < iBranchCurrNum; i++)
	{
		value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, s_aBusVolt[2 + i].iRoughDataIdx, value);
		iOffset += s_aBusVolt[2 + i].iOffsetStep;
	}

	//Second 2 strings of Battery Voltage
	for(i = 2; i < iBattNum; i++)
	{
		value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, s_aBattInfo[0].iRoughDataIdx, value);
		iOffset += s_aBattInfo[0].iOffsetStep;
	}

	//Second 2 strings of Battery Current
	for(i = 2; i < iBattNum; i++)
	{
		value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, s_aBattInfo[1].iRoughDataIdx, value);
		iOffset += s_aBattInfo[1].iOffsetStep;
	}

	//Over Voltage and Under Voltage
	i = 0;
	while(s_aOverVolt[i].iRoughDataIdx != LDU_INVALID_SIG_ID)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, s_aOverVolt[i].iRoughDataIdx, value);
		iOffset += s_aOverVolt[i].iOffsetStep;
		i++;
	}

	//Battery 1 / 2  Over Voltage, Under Voltage, Over Current
	for(i = 0; (i < iBattNum) && (i < 2); i++)
	{
		j = 0;
		while(s_aBattOverVolt[j].iRoughDataIdx != LDU_INVALID_SIG_ID)
		{
			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, s_aBattOverVolt[j].iRoughDataIdx, value);
			iOffset += s_aBattOverVolt[j].iOffsetStep;
			j++;
		}
	}


	//Over Temperature
	for(i = 0; i < iTemperatureNum; i++)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, s_aOverTemperature[i].iRoughDataIdx, value);
		iOffset += s_aOverTemperature[i].iOffsetStep;
	}


	//Fuse Break
	for(i = 0; i < iFuseNum; i++)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_OUTPUT1_DISCON + i, value);
		iOffset += 2;
	}

	//Battery 1 / 2  Fuse
	for(i = 0; (i < iBattNum) && (i < 2); i++)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, LDU_BATT_FUSE_BREAK, value);
		iOffset += 2;
	}

	//LVD1 state
	value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
	LduSetRoughValue(LDU_EQUIP_TYPE_LVD, iLvdEquipNo, LDU_LVD1_STATE, value);
	iOffset += 2;

	if(iBattNum)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_LVD, iLvdEquipNo, LDU_LVD2_STATE, value);
		iOffset += 2;
		if(iBattNum > 1)
		{
			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_LVD, iLvdEquipNo, LDU_LVD3_STATE, value);
			iOffset += 2;	
		}
	}

	//Battery 3 / 4  Over Voltage, Under Voltage, Over Current
	for(i = 2; i < iBattNum; i++)
	{
		j = 0;
		while(s_aBattOverVolt[j].iRoughDataIdx != LDU_INVALID_SIG_ID)
		{
			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, s_aBattOverVolt[j].iRoughDataIdx, value);
			iOffset += s_aBattOverVolt[j].iOffsetStep;
			j++;
		}
	}

	//Battery 3 / 4  Fuse
	for(i = 2; i < iBattNum; i++)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, LDU_BATT_FUSE_BREAK, value);
		iOffset += 2;
	}

	value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
	LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_DCD_DC_SPD, value);
	iOffset += 2;

	return TRUE;
}



/*==========================================================================*
* FUNCTION : LduUnpackAcdDcdInfo
* PURPOSE  : Unpack response string of GetAll Command into rough data 
*            for AC/DC-LargeDU
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE*  pbyRcvBuf : 
*            BYTE   byAddr    : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:12
*==========================================================================*/
static BOOL LduUnpackAcdDcdInfo(BYTE* pbyRcvBuf, BYTE byAddr)
{
	RS485_VALUE		value;
	int				i, j;
	int				iAcInputNum, iAcInputType;
	int				iTemperatureNum, iBattNum, iBranchCurrNum, iFuseNum;
	int				iOffset = FRAME_OFFSET_DATA;

	int				iAcdEquipNo;
	int				iDcdEquipNo;
	int				iBattEquipNo;
	int				iLvdEquipNo;

	iAcdEquipNo = LduGetAcdNo(byAddr);
	if(iAcdEquipNo < 0)
	{
		return FALSE;
	}	

	iDcdEquipNo = LduGetDcdNo(byAddr);
	if(iDcdEquipNo < 0)
	{
		return FALSE;
	}

	iLvdEquipNo = iDcdEquipNo;
	iBattEquipNo = LduGetBattNo(iDcdEquipNo);


	//AC Input Number and AC Input Type
	//iOffset += LDU_OFFSET_AC_INPUT_NUM;
	i = 0;
	while(s_aCfgSig[i].iRoughDataIdx != LDU_INVALID_SIG_ID)
	{
		// ACD config
		if((LDU_ACD_INPUT_NUM == s_aCfgSig[i].iRoughDataIdx)
			|| (LDU_ACD_INPUT_TYPE == s_aCfgSig[i].iRoughDataIdx))
		{
			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			if((value.iValue > s_aCfgSig[i].iMaxValue) 
				|| (value.iValue < s_aCfgSig[i].iMinValue))
			{
				return FALSE;
			}
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aCfgSig[i].iRoughDataIdx, value);
		}
		// DCD config
		else if((LDU_DCD_TEMP_NUM == s_aCfgSig[i].iRoughDataIdx)
			|| (LDU_DCD_BATT_NUM == s_aCfgSig[i].iRoughDataIdx)
			|| (LDU_DCD_BRANCH_CURR_NUM == s_aCfgSig[i].iRoughDataIdx)
			|| (LDU_DCD_FUSE_NUM == s_aCfgSig[i].iRoughDataIdx))
		{
			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			if((value.iValue > s_aCfgSig[i].iMaxValue) 
				|| (value.iValue < s_aCfgSig[i].iMinValue))
			{
				return FALSE;
			}
			LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, s_aCfgSig[i].iRoughDataIdx, value);
		}
		else
		{
			// LDU_RESERVE_SIG_ID, No action
		}

		//if(LDU_RESERVE_SIG_ID != s_aCfgSig[i].iRoughDataIdx)
		//{
		//	value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		//	if((value.iValue > s_aCfgSig[i].iMaxValue) 
		//		|| (value.iValue < s_aCfgSig[i].iMinValue))
		//	{
		//		return FALSE;
		//	}
		//	LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aCfgSig[i].iRoughDataIdx, value);
		//}
		iOffset += s_aCfgSig[i].iOffsetStep;
		i++;
	}

	value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_ACD_INPUT_NUM);
	iAcInputNum = value.iValue;
	value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_ACD_INPUT_TYPE);
	iAcInputType = value.iValue;
	value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_DCD_TEMP_NUM);
	iTemperatureNum = value.iValue;
	value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_DCD_BATT_NUM);
	iBattNum = value.iValue;
	value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_DCD_BRANCH_CURR_NUM);
	iBranchCurrNum = value.iValue;
	value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_DCD_FUSE_NUM);
	iFuseNum = value.iValue;

	value.iValue = LDU_EQUIP_EXIST;
	LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_ACD_EXISTS, value);
	value.dwValue = byAddr;
	LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_ACD_ADDR, value);
	//printf(" AcdDcd $$$$$$$$ iAcdEquipNo = %d addr = %d\n",iAcdEquipNo,byAddr);

	value.iValue = LDU_EQUIP_EXIST;
	LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_DCD_EXISTS, value);
	value.dwValue = byAddr;
	LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_DCD_ADDR, value);
	value.iValue = LDU_EQUIP_EXIST;
	LduSetRoughValue(LDU_EQUIP_TYPE_LVD, iLvdEquipNo, LDU_LVD_EQUIP_EXISTS, value);
	value.dwValue = iDcdEquipNo + 1;
	LduSetRoughValue(LDU_EQUIP_TYPE_LVD, iLvdEquipNo, LDU_LVD_DCD_NO, value);

	for(i = 0; i < iBattNum; i++)
	{
		value.iValue = LDU_EQUIP_EXIST;
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, LDU_BATT_EXISTS, value);
		value.iValue = iDcdEquipNo + 1;
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, LDU_BATT_DCD_NO, value);
	}


	//Mains Voltage
	if(iAcInputType == LDU_AC_INPUT_TYPE_SINGLE)	//Single Phase
	{
		for(i = 0; i < iAcInputNum; i++)
		{
			value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcVoltSig[i * 3 + 0].iRoughDataIdx, value);
			iOffset += s_aAcVoltSig[i * 3 + 0].iOffsetStep;

			value.iValue = RS485_SAMP_INVALID_VALUE;
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcVoltSig[i * 3 + 1].iRoughDataIdx, value);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcVoltSig[i * 3 + 2].iRoughDataIdx, value);
		}
		for(; i < LDU_ACD_MAX_AC_INPUT; i++)
		{
			value.iValue = RS485_SAMP_INVALID_VALUE;
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcVoltSig[i * 3 + 0].iRoughDataIdx, value);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcVoltSig[i * 3 + 1].iRoughDataIdx, value);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcVoltSig[i * 3 + 2].iRoughDataIdx, value);
		}
	}
	else											//Three Phase
	{
		for(i = 0; i < iAcInputNum; i++)
		{
			value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcVoltSig[i * 3 + 0].iRoughDataIdx, value);
			iOffset += s_aAcVoltSig[i * 3 + 0].iOffsetStep;

			value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcVoltSig[i * 3 + 1].iRoughDataIdx, value);
			iOffset += s_aAcVoltSig[i * 3 + 1].iOffsetStep;

			value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcVoltSig[i * 3 + 2].iRoughDataIdx, value);
			iOffset += s_aAcVoltSig[i * 3 + 2].iOffsetStep;
		}
		for(; i < LDU_ACD_MAX_AC_INPUT; i++)
		{
			value.iValue = RS485_SAMP_INVALID_VALUE;
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcVoltSig[i * 3 + 0].iRoughDataIdx, value);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcVoltSig[i * 3 + 1].iRoughDataIdx, value);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcVoltSig[i * 3 + 2].iRoughDataIdx, value);
		}
	}

	//Three phase current and frequency
	i = 0;
	while(s_aAcCurrSig[i].iRoughDataIdx != LDU_INVALID_SIG_ID)
	{
		value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcCurrSig[i].iRoughDataIdx, value);
		iOffset += s_aAcCurrSig[i].iOffsetStep;

		i++;
	}

	//Temperature
	for(i = 0; i < iTemperatureNum; i++)
	{
		value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, s_aTemperature[i].iRoughDataIdx, value);
		iOffset += s_aTemperature[i].iOffsetStep;
	}
	for(; i < LDU_DCD_MAX_TEMP_NUM; i++)
	{
		value.iValue = RS485_SAMP_INVALID_VALUE;
		LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, s_aTemperature[i].iRoughDataIdx, value);
	}

	//First 2 strings of Battery Voltage
	for(i = 0; (i < iBattNum) && (i < 2); i++)
	{
		value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, s_aBattInfo[0].iRoughDataIdx, value);
		iOffset += s_aBattInfo[0].iOffsetStep;
	}
	for(; i < 2; i++)
	{
		value.iValue = RS485_SAMP_INVALID_VALUE;
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, s_aBattInfo[0].iRoughDataIdx, value);
	}

	//First 2 strings of Battery Current
	for(i = 0; (i < iBattNum) && (i < 2); i++)
	{
		value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, s_aBattInfo[1].iRoughDataIdx, value);
		iOffset += s_aBattInfo[1].iOffsetStep;
	}
	for(; i < 2; i++)
	{
		value.iValue = RS485_SAMP_INVALID_VALUE;
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, s_aBattInfo[1].iRoughDataIdx, value);
	}

	//Bus voltage, Load Current and Branch Current
	value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
	LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, s_aBusVolt[0].iRoughDataIdx, value);
	iOffset += s_aBusVolt[0].iOffsetStep;
	value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
	LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, s_aBusVolt[1].iRoughDataIdx, value);
	iOffset += s_aBusVolt[1].iOffsetStep;

	for(i = 0; i < iBranchCurrNum; i++)
	{
		value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, s_aBusVolt[2 + i].iRoughDataIdx, value);
		iOffset += s_aBusVolt[2 + i].iOffsetStep;
	}
	for(; i < LDU_DCD_MAX_LOAD_CURR_NUM; i++)
	{
		value.iValue = RS485_SAMP_INVALID_VALUE;
		LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, s_aBusVolt[2 + i].iRoughDataIdx, value);
	}

	//Second 2 strings of Battery Voltage
	for(i = 2; i < iBattNum; i++)
	{
		value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, s_aBattInfo[0].iRoughDataIdx, value);
		iOffset += s_aBattInfo[0].iOffsetStep;
	}
	for(; i < LDU_DCD_MAX_BATT_NUM; i++)
	{
		value.iValue = RS485_SAMP_INVALID_VALUE;
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, s_aBattInfo[0].iRoughDataIdx, value);
	}

	//Second 2 strings of Battery Current
	for(i = 2; i < iBattNum; i++)
	{
		value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, s_aBattInfo[1].iRoughDataIdx, value);
		iOffset += s_aBattInfo[1].iOffsetStep;
	}
	for(; i < LDU_DCD_MAX_BATT_NUM; i++)
	{
		value.iValue = RS485_SAMP_INVALID_VALUE;
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, s_aBattInfo[1].iRoughDataIdx, value);
	}

	//Fault Light and Input State
	value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
	LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aFaultLight[0].iRoughDataIdx, value);
	iOffset += s_aFaultLight[0].iOffsetStep;

	for(i = 0; i < iAcInputNum; i++)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aFaultLight[i + 1].iRoughDataIdx, value);
		iOffset += s_aFaultLight[i + 1].iOffsetStep;
	}
	// Set other input status to OK
	for(; i < LDU_ACD_MAX_AC_INPUT; i++)
	{
		value.iValue = LDU_STATUS_OK;
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aFaultLight[i + 1].iRoughDataIdx, value);
	}

	//AC Output	and Over Freq, Under Freq
	i = 0;
	while(s_aAcOutput[i].iRoughDataIdx != LDU_INVALID_SIG_ID)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, s_aAcOutput[i].iRoughDataIdx, value);
		iOffset += s_aAcOutput[i].iOffsetStep;
		i++;
	}

	//母排过压、欠压
	//Over Voltage and Under Voltage
	i = 0;
	while(s_aOverVolt[i].iRoughDataIdx != LDU_INVALID_SIG_ID)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, s_aOverVolt[i].iRoughDataIdx, value);
		iOffset += s_aOverVolt[i].iOffsetStep;
		i++;
	}

	//Battery 1 / 2  Over Voltage, Under Voltage, Over Current
	for(i = 0; (i < iBattNum) && (i < 2); i++)
	{
		j = 0;
		while(s_aBattOverVolt[j].iRoughDataIdx != LDU_INVALID_SIG_ID)
		{
			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, s_aBattOverVolt[j].iRoughDataIdx, value);
			iOffset += s_aBattOverVolt[j].iOffsetStep;
			j++;
		}
	}
	for(; i < 2; i++)
	{
		value.iValue = LDU_STATUS_OK;
		j = 0;
		while(s_aBattOverVolt[j].iRoughDataIdx != LDU_INVALID_SIG_ID)
		{
			LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, s_aBattOverVolt[j].iRoughDataIdx, value);
			j++;
		}
	}


	//Over Temperature
	for(i = 0; i < iTemperatureNum; i++)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, s_aOverTemperature[i].iRoughDataIdx, value);
		iOffset += s_aOverTemperature[i].iOffsetStep;
	}
	for(; i < LDU_DCD_MAX_TEMP_NUM; i++)
	{
		value.iValue = LDU_STATUS_OK;
		LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, s_aOverTemperature[i].iRoughDataIdx, value);
	}


	//Fuse Break
	for(i = 0; i < iFuseNum; i++)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_OUTPUT1_DISCON + i, value);
		iOffset += 2;
	}
	for(; i < LDU_DCD_MAX_LOAD_FUSE_NUM; i++)
	{
		value.iValue = LDU_STATUS_OK;
		LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_OUTPUT1_DISCON + i, value);
	}

	//Battery 1 / 2  Fuse
	for(i = 0; (i < iBattNum) && (i < 2); i++)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, LDU_BATT_FUSE_BREAK, value);
		iOffset += 2;
	}
	for(; i < 2; i++)
	{
		value.iValue = LDU_STATUS_OK;
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, LDU_BATT_FUSE_BREAK, value);
	}

	//AC Input MCCB
	value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
	LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_INPUT_MCCB_ST, value);
	iOffset += 2;

	//SPD
	value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
	LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_SPD_TRIP_ST, value);
	iOffset += 2;

	//LVD1 state
	value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
	LduSetRoughValue(LDU_EQUIP_TYPE_LVD, iLvdEquipNo, LDU_LVD1_STATE, value);
	iOffset += 2;

	//LVD2,3
	if(iBattNum)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_LVD, iLvdEquipNo, LDU_LVD2_STATE, value);
		iOffset += 2;
		if(iBattNum > 1)
		{
			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_LVD, iLvdEquipNo, LDU_LVD3_STATE, value);
			iOffset += 2;	
		}
	}
	for(i = iBattNum + 1; i < 3; i++)
	{
		value.iValue = LDU_STATUS_OK;
		LduSetRoughValue(LDU_EQUIP_TYPE_LVD, iLvdEquipNo, LDU_LVD3_STATE, value);
	}

	//AC Main Fail
	for(i = 0; i < iAcInputNum; i++)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_MAINS1_FAIL_ST + i, value);
		iOffset += 2;
	}
	for(; i < LDU_ACD_MAX_AC_INPUT; i++)
	{
		value.iValue = LDU_STATUS_OK;
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_MAINS1_FAIL_ST + i, value);
	}

	//AC Input Under Volt
	for(i = 0; i < iAcInputNum; i++)
	{
		if(iAcInputType == LDU_AC_INPUT_TYPE_SINGLE)	//Single Phase
		{
			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UA_UNDER_VOLT + i, value);
			iOffset += 2;

			value.iValue = LDU_STATUS_OK;
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UB_UNDER_VOLT + i, value);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UC_UNDER_VOLT + i, value);
		}
		else //Three Phase
		{
			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UA_UNDER_VOLT + i, value);
			iOffset += 2;

			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UB_UNDER_VOLT + i, value);
			iOffset += 2;

			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UC_UNDER_VOLT + i, value);
			iOffset += 2;
		}
	}
	for(; i < LDU_ACD_MAX_AC_INPUT; i++)
	{
		value.iValue = LDU_STATUS_OK;
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UA_UNDER_VOLT + i, value);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UB_UNDER_VOLT + i, value);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UC_UNDER_VOLT + i, value);
	}

	//AC Input Over Volt
	for(i = 0; i < iAcInputNum; i++)
	{
		if(iAcInputType == LDU_AC_INPUT_TYPE_SINGLE)	//Single Phase
		{
			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UA_OVER_VOLT + i, value);
			iOffset += 2;

			value.iValue = LDU_STATUS_OK;
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UB_OVER_VOLT + i, value);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UC_OVER_VOLT + i, value);
		}
		else //Three Phase
		{
			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UA_OVER_VOLT + i, value);
			iOffset += 2;

			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UB_OVER_VOLT + i, value);
			iOffset += 2;

			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UC_OVER_VOLT + i, value);
			iOffset += 2;
		}
	}
	for(; i < LDU_ACD_MAX_AC_INPUT; i++)
	{
		value.iValue = LDU_STATUS_OK;
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UA_OVER_VOLT + i, value);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UB_OVER_VOLT + i, value);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_UC_OVER_VOLT + i, value);
	}

	//AC Input Phase Fail
	for(i = 0; i < iAcInputNum; i++)
	{
		if(iAcInputType == LDU_AC_INPUT_TYPE_SINGLE)	//Single Phase
		{
			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_PHASE_A_FAIL + i, value);
			iOffset += 2;

			value.iValue = LDU_STATUS_OK;
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_PHASE_B_FAIL + i, value);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_PHASE_C_FAIL + i, value);
		}
		else //Three Phase
		{
			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_PHASE_A_FAIL + i, value);
			iOffset += 2;

			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_PHASE_B_FAIL + i, value);
			iOffset += 2;

			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_PHASE_C_FAIL + i, value);
			iOffset += 2;
		}
	}
	for(; i < LDU_ACD_MAX_AC_INPUT; i++)
	{
		value.iValue = LDU_STATUS_OK;
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_PHASE_A_FAIL + i, value);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_PHASE_B_FAIL + i, value);
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_M1_PHASE_C_FAIL + i, value);
	}

	//Battery 3 / 4  Over Voltage, Under Voltage, Over Current
	for(i = 2; i < iBattNum; i++)
	{
		j = 0;
		while(s_aBattOverVolt[j].iRoughDataIdx != LDU_INVALID_SIG_ID)
		{
			value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
			LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, s_aBattOverVolt[j].iRoughDataIdx, value);
			iOffset += s_aBattOverVolt[j].iOffsetStep;
			j++;
		}
	}
	for(; i < LDU_DCD_MAX_BATT_NUM; i++)
	{
		j = 0;
		value.iValue = LDU_STATUS_OK;
		while(s_aBattOverVolt[j].iRoughDataIdx != LDU_INVALID_SIG_ID)
		{
			LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, s_aBattOverVolt[j].iRoughDataIdx, value);
			j++;
		}

	}

	//Battery 3 / 4  Fuse
	for(i = 2; i < iBattNum; i++)
	{
		value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, LDU_BATT_FUSE_BREAK, value);
		iOffset += 2;
	}
	for(; i < LDU_DCD_MAX_BATT_NUM; i++)
	{
		value.iValue = LDU_STATUS_OK;
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattEquipNo + i, LDU_BATT_FUSE_BREAK, value);
	}

	// DC SPD
	value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
	LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_DCD_DC_SPD, value);
	iOffset += 2;

	return TRUE;
}



#define	LDU_CHECK_SUM_LEN	4
/*==========================================================================*
* FUNCTION : LduUnpackForGetAll
* PURPOSE  : Unpack response string of GetAll Command into rough data 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE*          pbyRcvBuf : 
*            int            iRcvLen   : 
*            LDU_SEND_CMD*  pSendCmd  : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:13
*==========================================================================*/
static BOOL LduUnpackForGetAll(BYTE* pbyRcvBuf,
							   int iRcvLen,
							   LDU_SEND_CMD* pSendCmd)
{
	int						iDataLen, iRecvCRC;
	BYTE					byRtnId, byAddr;


	//Length check
	iDataLen = HexStrToByte(pbyRcvBuf + FRAME_OFFSET_LENGTH + 2);
	iDataLen = (iDataLen << 8) + HexStrToByte(pbyRcvBuf + FRAME_OFFSET_LENGTH);

	if(iRcvLen  != (iDataLen + FRAME_OFFSET_DATA + LDU_CHECK_SUM_LEN + 1))
	{
		return FALSE;
	}

	//CRC12 Check
	iRecvCRC = HexStrToByte(pbyRcvBuf + FRAME_OFFSET_DATA + iDataLen + 2);
	iRecvCRC = (iRecvCRC << 8) + HexStrToByte(pbyRcvBuf + FRAME_OFFSET_DATA + iDataLen);

	if( iRecvCRC != LduCrc12(pbyRcvBuf + FRAME_OFFSET_ADDR, iDataLen + 8 ) )
	{
		return FALSE;
	}

	//Address check
	byAddr = HexStrToByte(pbyRcvBuf + FRAME_OFFSET_ADDR);
	if(pSendCmd->byAddr != byAddr)
	{
		return FALSE;
	}

	// RTN check
	byRtnId = HexStrToByte(pbyRcvBuf + FRAME_OFFSET_CMD_ID);
	if(pSendCmd->byCmdId != byRtnId)	
	{
		return FALSE;
	}


	//ACD
	if((byAddr >= LDU_FST_ACD_ADDR) && (byAddr < LDU_FST_DCD_ADDR))
	{
		if(!LduUnpackAcdInfo(pbyRcvBuf, byAddr))
		{
			return FALSE;
		}

	}
	//DCD
	else if(((byAddr >= LDU_FST_DCD_ADDR) && (byAddr < LDU_FST_ACDCD_ADDR))
		|| ((byAddr >= LDU_FST_OTHER_DCD_ADDR) && (byAddr <= LDU_LST_ADDR)))
	{
		if(!LduUnpackDcdInfo(pbyRcvBuf, byAddr))
		{
			return FALSE;
		}
	}
	//ACD/DCD
	else if((byAddr >= LDU_FST_ACDCD_ADDR) && (byAddr < LDU_FST_OTHER_DCD_ADDR))
	{
		if(!LduUnpackAcdDcdInfo(pbyRcvBuf, byAddr))
		{
			return FALSE;
		}	
	}

	return TRUE;	
}


/*==========================================================================*
* FUNCTION : LduUnpackSigCode
* PURPOSE  : Unpack response string of GetSigCode Command into pwSigCode
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE*          pbyRcvBuf : 
*            WORD*          pwSigCode : 
*            int            iRcvLen   : 
*            int*           piSigNum  : 
*            LDU_SEND_CMD*  pSendCmd  : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:14
*==========================================================================*/
static BOOL LduUnpackSigCode(BYTE* pbyRcvBuf, 
							 WORD* pwSigCode,
							 int iRcvLen,
							 int* piSigNum,
							 LDU_SEND_CMD* pSendCmd)
{
	int			i, j, iDataLen, iRecvCRC;
	BYTE		byAddr, byRtnId;

	//Length check
	iDataLen = HexStrToByte(pbyRcvBuf + FRAME_OFFSET_LENGTH + 2);
	iDataLen = (iDataLen << 8) + HexStrToByte(pbyRcvBuf + FRAME_OFFSET_LENGTH);

	if(iRcvLen  != (iDataLen + FRAME_OFFSET_DATA + LDU_CHECK_SUM_LEN + 1))
	{
		return FALSE;
	}

	//CRC12 Check
	iRecvCRC = HexStrToByte(pbyRcvBuf + FRAME_OFFSET_DATA + iDataLen + 2);
	iRecvCRC = (iRecvCRC << 8) + HexStrToByte(pbyRcvBuf + FRAME_OFFSET_DATA + iDataLen);

	if( iRecvCRC != LduCrc12(pbyRcvBuf + FRAME_OFFSET_ADDR, iDataLen + 8 ) )
	{
		return FALSE;
	}

	//Address check
	byAddr = HexStrToByte(pbyRcvBuf + FRAME_OFFSET_ADDR);
	if(pSendCmd->byAddr != byAddr)
	{
		return FALSE;
	}

	byRtnId = HexStrToByte(pbyRcvBuf + FRAME_OFFSET_CMD_ID);
	if(pSendCmd->byCmdId != byRtnId)	
	{
		return FALSE;
	}

	for(i = 0, j = 0; i < iDataLen; i = i + 4, j++)
	{
		*(pwSigCode + j) = HexStrToByte(pbyRcvBuf + FRAME_OFFSET_DATA + i + 2);
		*(pwSigCode + j) = ((*(pwSigCode + j)) << 8) 
			+ HexStrToByte(pbyRcvBuf + FRAME_OFFSET_DATA + i);
	}

	*piSigNum = j;

	return TRUE;

}



/*==========================================================================*
* FUNCTION : LduReadOneFrame
* PURPOSE  : Read a frame with Head(0x7E) and Tail(0x0d)
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN void*    pDevice    : 
*            OUT BYTE**  ppbyRcvBuf : 
*            OUT int*    piReadLen  : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:17
*==========================================================================*/
static BOOL LduReadOneFrame(IN void* pDevice, 
							OUT BYTE** ppbyRcvBuf,
							OUT int* piReadLen)
{
	RS485_DEVICE_CLASS*		pLdu = pDevice;
	BYTE*					pbyRcvBuf = pLdu->pCommPort->abyRcvBuf;
	int						i, iReadLen;
	int						iHead = -1;
	int						iTail = -1;
	RS485_DRV*	pPort			  = (RS485_DRV *)pLdu->pCommPort->hCommPort;
	int						fd	  = pPort->fdSerial;

	//iReadLen = RS485_Read(pLdu->pCommPort->hCommPort, 
	//					pbyRcvBuf,
	//					RS485_RECV_BUF_LEN);

	if (RS485WaitReadable(fd, 500) > 0 )
	{
		iReadLen = RS485_ReadForLDU(pLdu->pCommPort->hCommPort, 
									pbyRcvBuf,
									RS485_RECV_BUF_LEN);

#ifdef __TRACE_COMM_DATA
		if(iReadLen == 0)
		{
			TRACE("RS485::Recv Nothing.\n");
			return FALSE;
		}
		else
		{
			pbyRcvBuf[iReadLen] = NULL;
			TRACE("RS485::Recv Data: %s\n", pbyRcvBuf);
		}
#endif

		for(i = 0; i < iReadLen; i++)
		{
			if(LDU_SOI_BYTE == *(pbyRcvBuf + i))
			{
				iHead = i;
				break;
			}
		}

		if(iHead < 0)	//   no head
		{
#ifdef __TRACE_COMM_DATA
			TRACE("RS485::No Recv Head.\n");
#endif
			return FALSE;
		}

		for(i = iHead + 1; i < iReadLen; i++)
		{
			if(LDU_EOI_BYTE == *(pbyRcvBuf + i))
			{
				iTail = i;
				break;
			}
		}

		if(iTail < 0)	//   no tail
		{
#ifdef __TRACE_COMM_DATA
			TRACE("RS485::No Recv Tail.\n");
#endif
			TRACE("\n LDU  IS  SAMPLING ERROR  \n");
			return FALSE;
		}

		*ppbyRcvBuf = pbyRcvBuf + iHead;
		*piReadLen = iTail - iHead + 1;

		return TRUE;
	}
	else
	{
		//
		TRACE("\n This is  Read no Responese !!!!\n");
		return FALSE;
	}

}



/*==========================================================================*
* FUNCTION : LduSampleGetAll
* PURPOSE  : Sample once with GetAll Command
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*  pDevice : 
*            BYTE   byAddr  : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:18
*==========================================================================*/
static BOOL	LduSampleGetAll(void* pDevice, BYTE byAddr)
{
	RS485_DEVICE_CLASS*		pLdu = pDevice;
	BYTE*					pbyRcvBuf;
	int						iReadLen = 0;
	LDU_SEND_CMD			SendCmd;

	SendCmd.byAddr = byAddr;
	SendCmd.byCmdId = LDU_CMD_ID_GET_ALL;
	SendCmd.wLduSigId = LDU_INVALID_SIG_ID;
	SendCmd.iValueType = LDU_VALUE_TYPE_INVALID;
	SendCmd.fValue = 0.0;

	LduPackSendCmd(pLdu, &SendCmd);

	if(!LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen))
	{
		return FALSE;
	}

	if(!LduUnpackForGetAll(pbyRcvBuf, iReadLen, &SendCmd))
	{
		return FALSE;
	}

	return TRUE;

}


/*==========================================================================*
* FUNCTION : LduGetOneCfgInfo
* PURPOSE  : Find and return a signal config info with wSigCode as SigCode
*            from pSigCfgInfo
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: WORD               wSigCode    : 
*            LDU_SIG_CFG_INFO*  pSigCfgInfo : 
* RETURN   : static LDU_SIG_CFG_INFO* : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:19
*==========================================================================*/
static LDU_SIG_CFG_INFO* LduGetOneCfgInfo(WORD wSigCode, 
										  LDU_SIG_CFG_INFO* pSigCfgInfo)
{
	int	i = 0;

	while((pSigCfgInfo + i)->iRoughDataIdx != LDU_INVALID_SIG_ID)
	{
		if((pSigCfgInfo + i)->wSigCode == wSigCode)
		{
			//printf("!!!!@@@@!!!!!!!  wSigCode=%4X \n",wSigCode);
			return pSigCfgInfo + i;
		}
		i++;
	}

	return NULL;

}


/*==========================================================================*
* FUNCTION : LduSampleGetUpDnLmt
* PURPOSE  : Sample once with GetUpDnLmt Command
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*  pDevice : 
*            BYTE   byAddr  : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:22
*==========================================================================*/
static BOOL LduSampleGetUpDnLmt(void* pDevice, BYTE byAddr)
{
	static LDU_SIG_CFG_INFO s_aLduUpDnInfo[] =
	{
		{LDU_EQUIP_TYPE_ACD,	0,	LDU_ACD_OVER_VOLT_LMT,		0x4601,	LDU_VALUE_TYPE_FLOAT,},	//交流输入过压告警点
		{LDU_EQUIP_TYPE_ACD,	0,	LDU_ACD_UNDER_VOLT_LMT,		0x4602,	LDU_VALUE_TYPE_FLOAT,},	//交流输入欠压告警点
		{LDU_EQUIP_TYPE_ACD,	0,	LDU_ACD_P_FAIL_LMT,			0x4603,	LDU_VALUE_TYPE_FLOAT,},	//电网缺相告警点
		{LDU_EQUIP_TYPE_ACD,	0,	LDU_ACD_OVER_FREQ_LMT,		0x4604,	LDU_VALUE_TYPE_FLOAT,},	//交流频率过高告警点
		{LDU_EQUIP_TYPE_ACD,	0,	LDU_ACD_UNDER_FREQ_LMT,		0x4605,	LDU_VALUE_TYPE_FLOAT,},	//交流频率过低告警点
		{LDU_EQUIP_TYPE_ACD,	0,	LDU_ACD_IMBALANCE_LMT,		0x4606,	LDU_VALUE_TYPE_FLOAT,},	//电网不平衡度告警点
		{LDU_EQUIP_TYPE_BATT,	0,	LDU_BATT_OVER_VOLT_LMT,		0x4607,	LDU_VALUE_TYPE_FLOAT,},	//电池组1过压告警点
		{LDU_EQUIP_TYPE_BATT,	0,	LDU_BATT_UNDER_VOLT_LMT,	0x4608,	LDU_VALUE_TYPE_FLOAT,},	//电池组1欠压告警点
		{LDU_EQUIP_TYPE_BATT,	0,	LDU_BATT_OVER_CURR_LMT,		0x4609,	LDU_VALUE_TYPE_FLOAT,},	//电池组1过流告警点
		{LDU_EQUIP_TYPE_BATT,	1,	LDU_BATT_OVER_VOLT_LMT,		0x460A,	LDU_VALUE_TYPE_FLOAT,},	//电池组2过压告警点
		{LDU_EQUIP_TYPE_BATT,	1,	LDU_BATT_UNDER_VOLT_LMT,	0x460B,	LDU_VALUE_TYPE_FLOAT,},	//电池组2欠压告警点
		{LDU_EQUIP_TYPE_BATT,	1,	LDU_BATT_OVER_CURR_LMT,		0x460C,	LDU_VALUE_TYPE_FLOAT,},	//电池组2过流告警点
		{LDU_EQUIP_TYPE_DCD,	0,	LDU_DCD_OVER_TEMP1_LMT,		0x460D,	LDU_VALUE_TYPE_FLOAT,},	//温度1路上限
		{LDU_EQUIP_TYPE_DCD,	0,	LDU_DCD_OVER_TEMP2_LMT,		0x460E,	LDU_VALUE_TYPE_FLOAT,},	//温度2路上限
		{LDU_EQUIP_TYPE_DCD,	0,	LDU_DCD_OVER_TEMP3_LMT,		0x460F,	LDU_VALUE_TYPE_FLOAT,},	//温度3路上限
		{LDU_EQUIP_TYPE_DCD,	0,	LDU_DCD_AC_SWITCH_LOW,		0x4614,	LDU_VALUE_TYPE_FLOAT,},	//市电切换低电压点*
		{LDU_EQUIP_TYPE_DCD,	0,	LDU_DCD_AC_SWITCH_HIGH,		0x4615,	LDU_VALUE_TYPE_FLOAT,},	//市电切换高电压点*
		{LDU_EQUIP_TYPE_LVD,	0,	LDU_LVD2_VOLT_LMT,			0x4616,	LDU_VALUE_TYPE_FLOAT,},	//电池组1保护点
		{LDU_EQUIP_TYPE_LVD,	0,	LDU_LVD3_VOLT_LMT,			0x4617,	LDU_VALUE_TYPE_FLOAT,},	//电池组2保护点
		{LDU_EQUIP_TYPE_LVD,	0,	LDU_LVD1_VOLT_LMT,			0x4618,	LDU_VALUE_TYPE_FLOAT,},	//负载下电点   
		{LDU_EQUIP_TYPE_DCD,	0,	LDU_DCD_OVER_VOLT_LMT,		0x4619,	LDU_VALUE_TYPE_FLOAT,},	//母排过压告警点
		{LDU_EQUIP_TYPE_DCD,	0,	LDU_DCD_UNDER_VOLT_LMT,		0x461A,	LDU_VALUE_TYPE_FLOAT,},	//母排欠压告警点

		{LDU_INVALID_SIG_ID,	0,	LDU_INVALID_SIG_ID,			0,		0,					},

	};

	RS485_DEVICE_CLASS*		pLdu = pDevice;
	BYTE*					pbyRcvBuf;
	int						iReadLen = 0;
	LDU_SEND_CMD			SendCmd;
	WORD					awSigCode[100];
	int						iSigNum;
	LDU_SIG_CFG_INFO*		pSigCfgInfo;
	RS485_VALUE				value;
	int						iOffset = FRAME_OFFSET_DATA;
	int						iEquipNo;
	int						i;

	SendCmd.byAddr = byAddr;
	SendCmd.byCmdId = LDU_CMD_ID_GET_UP_DN_LMT_CFG;
	SendCmd.wLduSigId = LDU_INVALID_SIG_ID;
	SendCmd.iValueType = LDU_VALUE_TYPE_INVALID;
	SendCmd.fValue = 0.0;

	LduPackSendCmd(pLdu, &SendCmd);

	if(!LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen))
	{
		return FALSE;
	}

	if(!LduUnpackSigCode(pbyRcvBuf, awSigCode, iReadLen, &iSigNum, &SendCmd))
	{
		return FALSE;
	}

	SendCmd.byAddr = byAddr;
	SendCmd.byCmdId = LDU_CMD_ID_GET_UP_DN_LMT;
	SendCmd.wLduSigId = LDU_INVALID_SIG_ID;
	SendCmd.iValueType = LDU_VALUE_TYPE_INVALID;
	SendCmd.fValue = 0.0;

	LduPackSendCmd(pLdu, &SendCmd);

	if(!LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen))
	{
		return FALSE;
	}


	//printf("\n ^^^^^^ LduSample GetUpDnLmt iSigNum =%d	^^^^^^ \n",iSigNum);

	for(i = 0; i < iSigNum; i++)
	{
		pSigCfgInfo = LduGetOneCfgInfo(awSigCode[i], s_aLduUpDnInfo);
		if(pSigCfgInfo)
		{
			iEquipNo = LduGetEquipNo(byAddr, pSigCfgInfo->iEquipType);
			if(pSigCfgInfo->wValueType == LDU_VALUE_TYPE_BYTE)
			{
				value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
				iOffset += 2;			
			}
			else if(pSigCfgInfo->wValueType == LDU_VALUE_TYPE_FLOAT)
			{
				value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
				iOffset += 8;			
			}
			LduSetRoughValue(pSigCfgInfo->iEquipType, 
				iEquipNo + pSigCfgInfo->iEquipIdx, 
				pSigCfgInfo->iRoughDataIdx, 
				value);
		}
		else
		{
			TRACE("RS485_LargeDU:Can NOT find config signal in LduUpDnInfo. SigCode = %X\n", awSigCode[i]);
		}
	}

	return TRUE;
}

/*==========================================================================*
* FUNCTION : LduSampleGetSysCfg
* PURPOSE  : Sample once with GetSysCfg Command
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*  pDevice : 
*            BYTE   byAddr  : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:23
*==========================================================================*/
static BOOL LduSampleGetSysCfg(void* pDevice, BYTE byAddr)
{
	static LDU_SIG_CFG_INFO s_aLduSysCfgInfo[] =
	{
		{LDU_EQUIP_TYPE_ACD,	0,	LDU_ACD_CURR_COEF,			0x4801,	LDU_VALUE_TYPE_FLOAT,	},	//交流电流互感器系数
		{LDU_EQUIP_TYPE_DCD,	0,	LDU_DCD_TEMP_COEF,			0x4802,	LDU_VALUE_TYPE_FLOAT,	},	//温度系数
		//{LDU_EQUIP_TYPE_DCD,	0,	LDU_DCD_LOAD_CURR_COEF,		0x4803,	LDU_VALUE_TYPE_FLOAT,	},	//母排电流传感器系数
		{LDU_EQUIP_TYPE_BATT,	0,	LDU_BATT_CURR_COEF,			0x4804,	LDU_VALUE_TYPE_FLOAT,	},	//电池组1电流传感器系数
		{LDU_EQUIP_TYPE_BATT,	1,	LDU_BATT_CURR_COEF,			0x4805,	LDU_VALUE_TYPE_FLOAT,	},	//电池组2电流传感器系数
		{LDU_EQUIP_TYPE_DCD,	0,	LDU_DCD_LOAD_CURR_COEF,		0x4806,	LDU_VALUE_TYPE_FLOAT,	},	//负载电流传感器系数
		{LDU_EQUIP_TYPE_DCD,	0,	LDU_DCD_BATT_NUM,			0x4807,	LDU_VALUE_TYPE_BYTE,	},	//电池组数
		{LDU_EQUIP_TYPE_DCD,	0,	LDU_DCD_TEMP_NUM,			0x4808,	LDU_VALUE_TYPE_BYTE,	},	//温度路数
		{LDU_EQUIP_TYPE_ACD,	0,	LDU_ACD_INPUT_TYPE,			0x4809,	LDU_VALUE_TYPE_BYTE,	},	//交流输入类型
		{LDU_EQUIP_TYPE_ACD,	0,	LDU_ACD_INPUT_NUM,			0x480A,	LDU_VALUE_TYPE_BYTE,	},	//交流输入路数
		{LDU_EQUIP_TYPE_DCD,	0,	LDU_DCD_BRANCH_CURR_NUM,	0x480D,	LDU_VALUE_TYPE_BYTE,	},	//分路电流路数
		{LDU_EQUIP_TYPE_DCD,	0,	LDU_DCD_FUSE_NUM,			0x480E,	LDU_VALUE_TYPE_BYTE,	},	//支路数
		{LDU_EQUIP_TYPE_ACD,	0,	LDU_ACD_OUTPUT_NUM,			0x4812,	LDU_VALUE_TYPE_BYTE,	},	//交流输出路数
		{LDU_EQUIP_TYPE_ACD,	0,	LDU_ACD_CURR_MEASURE_MODE,	0x4813,	LDU_VALUE_TYPE_BYTE,	},	//交流电流测量方式（0不测，1单相，2三相
		{LDU_EQUIP_TYPE_DCD,	0,	LDU_DCD_BRANCH_CURR_COEF,	0x4814,	LDU_VALUE_TYPE_FLOAT,	},	//分路电流传感器系数
		{LDU_EQUIP_TYPE_BATT,	2,	LDU_BATT_CURR_COEF,			0x4815,	LDU_VALUE_TYPE_FLOAT,	},	//电池组3电流传感器系数
		{LDU_EQUIP_TYPE_BATT,	3,	LDU_BATT_CURR_COEF,			0x4816,	LDU_VALUE_TYPE_FLOAT,	},	//电池组4电流传感器系数

		{LDU_INVALID_SIG_ID,	0,	LDU_INVALID_SIG_ID,			0,		0,						},

	};

	RS485_DEVICE_CLASS*		pLdu = pDevice;
	BYTE*					pbyRcvBuf;
	int						iReadLen = 0;
	LDU_SEND_CMD			SendCmd;
	WORD					awSigCode[100];
	int						iSigNum;
	LDU_SIG_CFG_INFO*		pSigCfgInfo;
	RS485_VALUE				value;
	int						iOffset = FRAME_OFFSET_DATA;
	int						iEquipNo;
	int						i;

	SendCmd.byAddr = byAddr;
	SendCmd.byCmdId = LDU_CMD_ID_GET_SYS_CFG;
	SendCmd.wLduSigId = LDU_INVALID_SIG_ID;
	SendCmd.iValueType = LDU_VALUE_TYPE_INVALID;
	SendCmd.fValue = 0.0;

	LduPackSendCmd(pLdu, &SendCmd);

	if(!LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen))
	{
		return FALSE;
	}

	if(!LduUnpackSigCode(pbyRcvBuf, awSigCode, iReadLen, &iSigNum, &SendCmd))
	{
		return FALSE;
	}

	SendCmd.byAddr = byAddr;
	SendCmd.byCmdId = LDU_CMD_ID_GET_SYS_PARAM;
	SendCmd.wLduSigId = LDU_INVALID_SIG_ID;
	SendCmd.iValueType = LDU_VALUE_TYPE_INVALID;
	SendCmd.fValue = 0.0;

	LduPackSendCmd(pLdu, &SendCmd);

	if(!LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen))
	{
		return FALSE;
	}



	for(i = 0; i < iSigNum; i++)
	{
		pSigCfgInfo = LduGetOneCfgInfo(awSigCode[i], s_aLduSysCfgInfo);
		if(pSigCfgInfo)
		{
			iEquipNo = LduGetEquipNo(byAddr, pSigCfgInfo->iEquipType);
			if(pSigCfgInfo->wValueType == LDU_VALUE_TYPE_BYTE)
			{
				value.iValue = HexStrToByte(pbyRcvBuf + iOffset);
				iOffset += 2;			
			}
			else if(pSigCfgInfo->wValueType == LDU_VALUE_TYPE_FLOAT)
			{
				value.fValue = HexStrToFloat(pbyRcvBuf + iOffset);
				iOffset += 8;			
			}
			LduSetRoughValue(pSigCfgInfo->iEquipType, 
				iEquipNo + pSigCfgInfo->iEquipIdx, 
				pSigCfgInfo->iRoughDataIdx, 
				value);
		}
		else
		{
			TRACE("RS485_LargeDU:Can NOT find config signal in LduSysCfgInfo. SigCode = %X\n", awSigCode[i]);
		}

	}

	return TRUE;
}


/*==========================================================================*
* FUNCTION : LduNeedSample
* PURPOSE  : Judge if a LargeDu with byAddr adress exists
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE  byAddr : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:24
*==========================================================================*/
static BOOL LduNeedSample(BYTE byAddr)
{
	int				i;
	RS485_VALUE		value;

	for(i = 0; i < LDU_MAX_ACD_NUM; i++)
	{
		value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_EXISTS);
		if(value.iValue == LDU_EQUIP_EXIST)
		{
			value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_ADDR);
			if(value.dwValue == byAddr)
			{
				return TRUE;
			}
		}
		else
		{
			break;
		}
	}

	for(i = 0; i < LDU_MAX_DCD_NUM; i++)
	{
		value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_EXISTS);
		if(value.iValue == LDU_EQUIP_EXIST)
		{
			value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_ADDR);
			if(value.dwValue == byAddr)
			{
				return TRUE;
			}
		}
		else
		{
			break;
		}
	}

	return FALSE;
}


/*==========================================================================*
* FUNCTION : GetLvdSeqNumByDCDSeqNum 
* PURPOSE  : Get Lvd Sequence Number By DCD Sequence number
*            address byAddr
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE  byAddr : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  :  add by IlockTeng                DATE: 2010-03-07 11:26
*==========================================================================*/
CORRENLATION_SEQ  GetBattSeqNumByDCDSeqNum(INT32 DCDSeqNum)
{
	INT32		i;
	INT32		LargeBattSeqNum;
	LargeBattSeqNum	= 0;

	CORRENLATION_SEQ	stCorrtSeq;	//Correlation Sequence number

	for (i = 0; i < DCDSeqNum; i++)
	{
		LargeBattSeqNum += s_aLduRoughData[LDU_G_MAX_SIGNALS_NUM \
			+ LDU_MAX_ACD_NUM * LDU_MAX_ACD_SIGNALS_NUM \
			+ i * LDU_MAX_DCD_SIGNALS_NUM \
			+ LDU_DCD_BATT_NUM].iValue; 
	}

	stCorrtSeq.iStartSeqNum		= LargeBattSeqNum;
	stCorrtSeq.iSeqCount		= s_aLduRoughData[LDU_G_MAX_SIGNALS_NUM \
		+ LDU_MAX_ACD_NUM * LDU_MAX_ACD_SIGNALS_NUM \
		+ DCDSeqNum * LDU_MAX_DCD_SIGNALS_NUM \
		+ LDU_DCD_BATT_NUM].iValue; 
	return stCorrtSeq;
}



/*==========================================================================*
* FUNCTION : LduIncCommBreakTimes
* PURPOSE  : Increase the communication interrupt times of the LargeDu with
*            address byAddr
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE  byAddr : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:26
*==========================================================================*/
static void LduIncCommBreakTimes(BYTE byAddr)
{
	int				i;
	RS485_VALUE		value;

	for(i = 0; i < LDU_MAX_ACD_NUM; i++)
	{
		value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_EXISTS);
		if(value.iValue == LDU_EQUIP_EXIST)
		{
			value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_ADDR);
			if(value.dwValue == byAddr)
			{
				value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_COMM_BREAK_TIMES);
				if(value.iValue < LDU_MAX_COMM_BREAK_TIMES)
				{
					value.iValue += 1; 
					LduSetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_COMM_BREAK_TIMES, value);
					break;
				}
				else	//Comm Failure
				{
					value.iValue = LDU_COMM_FAILURE;
					LduSetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_NO_RESPONSE, value);
				}
			}
		}
		else
		{
			break;
		}
	}

	for(i = 0; i < LDU_MAX_DCD_NUM; i++)
	{
		value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_EXISTS);
		if(value.iValue == LDU_EQUIP_EXIST)
		{
			value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_ADDR);
			if(value.dwValue == byAddr)
			{
				value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_COMM_BREAK_TIMES);
				if(value.iValue < LDU_MAX_COMM_BREAK_TIMES)
				{
					value.iValue += 1; 
					LduSetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_COMM_BREAK_TIMES, value);
					break;
				}
				else	//Comm Failure
				{
					value.iValue = LDU_COMM_FAILURE;
					LduSetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_NO_RESPONSE, value);

					//后加的LARGEDU的LVD通信状态依据DCD屏的相同
					//一个DCD屏有两个LVD，上层用不用是另外一回事，也就是没向前堆放。
					if ((i * 2 + 1) < LDU_MAX_LVD_NUM)//最多10个LVD
					{
						value.iValue = LDU_COMM_FAILURE;
						LduSetRoughValue(LDU_EQUIP_TYPE_LVD, i * 2, LDU_COMM_STAT, value);
						LduSetRoughValue(LDU_EQUIP_TYPE_LVD, i * 2 + 1, LDU_COMM_STAT, value);
					}

					//因为LARGEDU_BATT的存在状态是依据DCD的存在状态判断的，且其向前堆放了。
					//所以通信中断状态也与DCD的相同  3/5 
					INT32				iBattSeqNum;
					CORRENLATION_SEQ	stSeq;
					stSeq		= GetBattSeqNumByDCDSeqNum(i);
					//iBattSeqNum = stSeq.iStartSeqNum;
					for (iBattSeqNum = stSeq.iStartSeqNum; 
						(iBattSeqNum < (stSeq.iStartSeqNum + stSeq.iSeqCount)) && (stSeq.iSeqCount <= 4);
						iBattSeqNum++)
					{
						if (iBattSeqNum < LDU_MAX_BATT_NUM)//最多40个LARGE电池
						{
							value.iValue = LDU_COMM_FAILURE;
							LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattSeqNum, LDU_BATT_COMM_STS, value);
						}
					}
				}
			}
		}
		else
		{
			break;
		}
	}

	return;
}



/*==========================================================================*
* FUNCTION : LduClrCommBreakTimes
* PURPOSE  : Clear the communication interrupt times of the LargeDu with
*            address byAddr
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE  byAddr : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:27
*==========================================================================*/
static void LduClrCommBreakTimes(BYTE byAddr)
{
	int				i;
	RS485_VALUE		value;

	for(i = 0; i < LDU_MAX_ACD_NUM; i++)
	{
		value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_EXISTS);
		if(value.iValue == LDU_EQUIP_EXIST)
		{
			value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_ADDR);
			if(value.dwValue == byAddr)
			{
				value.iValue = 0;
				LduSetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_COMM_BREAK_TIMES, value);

				value.iValue = LDU_COMM_OK;
				LduSetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_NO_RESPONSE, value);

				break;
			}
		}
		else
		{
			break;
		}
	}

	for(i = 0; i < LDU_MAX_DCD_NUM; i++)
	{
		value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_EXISTS);
		if(value.iValue == LDU_EQUIP_EXIST)
		{
			value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_ADDR);
			if(value.dwValue == byAddr)
			{
				value.iValue = 0;
				LduSetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_COMM_BREAK_TIMES, value);

				value.iValue = LDU_COMM_OK;
				LduSetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_NO_RESPONSE, value);

				//增加LVD单元下的通信状态 3/5 因为LARGEDU的LVD存在状态是根据ACD判断的
				//所以LVD的通信状态也根据ACD的状态一致。不向前填充的
				if ((i * 2 + 1) < LDU_MAX_LVD_NUM)//最多10个LVD
				{
					value.iValue = LDU_COMM_OK;
					LduSetRoughValue(LDU_EQUIP_TYPE_LVD, i * 2,		LDU_COMM_STAT, value);
					LduSetRoughValue(LDU_EQUIP_TYPE_LVD, i * 2 + 1, LDU_COMM_STAT, value);
				}

				//向前填充的，且依据DCD屏的状态获取是第几个BATT且该地址存在几个BATT
				INT32					iBattSeqNum;
				CORRENLATION_SEQ		stSeq;
				stSeq			 = GetBattSeqNumByDCDSeqNum(i);
				//iBattSeqNum = stSeq.iStartSeqNum;
				for (iBattSeqNum = stSeq.iStartSeqNum; 
					(iBattSeqNum < (stSeq.iStartSeqNum + stSeq.iSeqCount)) && (stSeq.iSeqCount <= 4);
					iBattSeqNum++)
				{
					if (iBattSeqNum < LDU_MAX_BATT_NUM)//最多40个LARGE电池
					{
						value.iValue = LDU_COMM_OK;
						LduSetRoughValue(LDU_EQUIP_TYPE_BATT, iBattSeqNum, LDU_BATT_COMM_STS, value);
					}					
				}
				break;
			}
		}
		else
		{
			break;
		}
	}

	return;
}

/*==========================================================================*
* FUNCTION : LDU_Init
* PURPOSE  : Interface function to initialize itself
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*  pDevice : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-06-24 09:55
*==========================================================================*/
void LDU_Init(void* pDevice)
{
	static LDU_COMM_INFO	s_LduCommInfo;	

	RS485_DEVICE_CLASS*		pLdu = pDevice;
	int						i, j;
	RS485_VALUE				Value;


	pLdu->bNeedReconfig = TRUE;
	pLdu->pRoughData = s_aLduRoughData;
	pLdu->pCommInfo = &s_LduCommInfo;

	pLdu->pfnExit = LDU_Exit;
	pLdu->pfnReconfig = LDU_Reconfig;
	pLdu->pfnSample = LDU_Sample;
	pLdu->pfnStuffChn = LDU_StuffChn;
	pLdu->pfnParamUnify = LDU_ParamUnify;
	pLdu->pfnSendCmd = LDU_SendCtlCmd;


	Value.iValue = 0;
	LduSetRoughValue(LDU_EQUIP_TYPE_GROUP,
		0,
		LDU_G_ACD_QTY,
		Value);	
	LduSetRoughValue(LDU_EQUIP_TYPE_GROUP,
		0,
		LDU_G_DCD_QTY,
		Value);	
	LduSetRoughValue(LDU_EQUIP_TYPE_GROUP,
		0,
		LDU_G_BATT_QTY,
		Value);	
	LduSetRoughValue(LDU_EQUIP_TYPE_GROUP,
		0,
		LDU_G_LVD_QTY,
		Value);	

	//Below assign invalid value to sampling signals first
	Value.iValue = RS485_SAMP_INVALID_VALUE;
	for(i = 0; i < LDU_MAX_ACD_NUM; i++)
	{
		for(j = 0; j < LDU_MAX_ACD_SIGNALS_NUM; j++)
		{
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, i, j, Value);	
		}
	}

	for(i = 0; i < LDU_MAX_DCD_NUM; i++)
	{
		for(j = 0; j < LDU_MAX_DCD_SIGNALS_NUM; j++)
		{
			LduSetRoughValue(LDU_EQUIP_TYPE_DCD, i, j, Value);	
		}
	}

	for(i = 0; i < LDU_MAX_BATT_NUM; i++)
	{
		for(j = 0; j < LDU_MAX_BATT_SIGNALS_NUM; j++)
		{
			LduSetRoughValue(LDU_EQUIP_TYPE_BATT, i, j, Value);	
		}
	}

	for(i = 0; i < LDU_MAX_LVD_NUM; i++)
	{
		for(j = 0; j < LDU_MAX_LVD_SIGNALS_NUM; j++)
		{
			LduSetRoughValue(LDU_EQUIP_TYPE_LVD, i, j, Value);	
		}
	}

	for(i = 0; i < LDU_MAX_ADDR_NUM; i++)
	{
		s_LduCommInfo.aiSampleAllTimes[i] = 0;
	}

	//LduResetPort(pLdu->pCommPort);

	return;
}


/*==========================================================================*
* FUNCTION : LDU_Exit
* PURPOSE  : Interface function to exit
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*  pDevice : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:29
*==========================================================================*/
void LDU_Exit(void* pDevice)
{
	RS485_DEVICE_CLASS*			pLdu = pDevice;
	extern RS485_SAMPLER_DATA	g_RS485Data;

	if(pLdu->pCommPort->bOpened)
	{
		g_RS485Data.CommPort.bOpened = FALSE;
		RS485_Close(pLdu->pCommPort->hCommPort);
		pLdu->pCommPort->hCommPort = NULL;
	}

	return;
}
/*==========================================================================*
* FUNCTION : LduEquipNumCalc
* PURPOSE  : Calculate equipment number in rough data
* CALLS    : 
* CALLED BY: 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:30
*==========================================================================*/
static void LduEquipNumCalc()
{
	int				i, iNum;
	RS485_VALUE		value;

	//AC Distribution
	iNum = 0;
	for(i = 0; i < LDU_MAX_ACD_NUM; i++)
	{
		value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_ADDR);
		if(value.iValue != RS485_SAMP_INVALID_VALUE)
		{
			iNum++;
		}
		else
		{
			value.iValue = LDU_EQUIP_NOT_EXIST;
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_EXISTS, value);
		}
	}
	value.iValue = iNum;
	LduSetRoughValue(LDU_EQUIP_TYPE_GROUP, 0, LDU_G_ACD_QTY, value);

	//ACD Group Working status
	value.iValue = iNum > 0 ? LDU_EQUIP_EXIST : LDU_EQUIP_NOT_EXIST;
	LduSetRoughValue(LDU_EQUIP_TYPE_GROUP, 0, LDU_G_ACDG_WORK_STATUS, value);

	//DC Distribution
	iNum = 0;
	for(i = 0; i < LDU_MAX_DCD_NUM; i++)
	{
		value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_ADDR);
		if(value.iValue != RS485_SAMP_INVALID_VALUE)
		{
			iNum++;
		}
		else
		{
			value.iValue = LDU_EQUIP_NOT_EXIST;
			LduSetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_EXISTS, value);
			LduSetRoughValue(LDU_EQUIP_TYPE_LVD, i, LDU_LVD_EQUIP_EXISTS, value);
		}
	}

	value.iValue = iNum;
	LduSetRoughValue(LDU_EQUIP_TYPE_GROUP, 0, LDU_G_DCD_QTY, value);
	LduSetRoughValue(LDU_EQUIP_TYPE_GROUP, 0, LDU_G_LVD_QTY, value);

	//DCD Group Working status
	value.iValue = iNum > 0 ? LDU_EQUIP_EXIST : LDU_EQUIP_NOT_EXIST;
	LduSetRoughValue(LDU_EQUIP_TYPE_GROUP, 0, LDU_G_DCDG_WORK_STATUS, value);

	//Battery Strings
	//iNum = 0;
	//for(i = 0; i < LDU_MAX_BATT_NUM; i++)
	//{
	//	value = LduGetRoughValue(LDU_EQUIP_TYPE_BATT, i, LDU_BATT_EXISTS);
	//	if(value.iValue != LDU_EQUIP_EXIST)
	//	{
	//		iNum++;
	//	}
	//	else
	//	{
	//		value.iValue = LDU_EQUIP_NOT_EXIST;
	//		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, i, LDU_BATT_EXISTS, value);
	//	}
	//}

	return;

}

static void LduBattNumCalc()
{
	RS485_VALUE		value;
	value = LduGetRoughValue(LDU_EQUIP_TYPE_GROUP, 0, LDU_G_DCD_QTY);
	int iDCDNum = value.iValue;

	int i;
	int iBattNum = 0;
	for (i = 0; i < iDCDNum; i++)
	{
		iBattNum += (int)GetDwordSigValue(LDU_DCD_UNIT_EQUIP_ID + i, SIG_TYPE_SETTING, LDU_DCD_BATT_NUM_SIG_ID, RS485_SAMPLER_TASK);
	}

	//There are at most 20 batteries in all Large DUs
	if(iBattNum > LDU_ACTUAL_MAX_BATT_NUM)
	{
		iBattNum = LDU_ACTUAL_MAX_BATT_NUM;
	}

	value.iValue = iBattNum;
	LduSetRoughValue(LDU_EQUIP_TYPE_GROUP, 0, LDU_G_BATT_QTY, value);

	return;
}

#define	AC_INPUT_ON			1
#define	AC_INPUT_OFF		0
#define	AC_INPUT_NO_1ST		1
#define	AC_INPUT_NO_2ND		2
#define	AC_INPUT_NO_3TH		3
#define	AC_INPUT_NO_NONE	0

/*==========================================================================*
* FUNCTION : LduAcFailCalc
* PURPOSE  : Calculate Mains Failure in rough data
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:31
*==========================================================================*/
static void LduAcFailCalc(void)
{
	int				i, iNum;
	RS485_VALUE		value;
	BOOL			bAcMainsFail;

	for(i = 0; i < LDU_MAX_ACD_NUM; i++)
	{
		value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_ADDR);
		if(value.iValue == RS485_SAMP_INVALID_VALUE)
		{
			break;
		}

		value.iValue = AC_INPUT_NO_NONE;
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_MAINS_INPUT_NO, value);

		value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_M1_INPUT_ST);
		if(AC_INPUT_ON == value.iValue)
		{
			value.iValue = AC_INPUT_NO_1ST;
			LduSetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_MAINS_INPUT_NO, value);
		}
		else 
		{
			value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_M2_INPUT_ST);
			if(AC_INPUT_ON == value.iValue)
			{
				value.iValue = AC_INPUT_NO_2ND;
				LduSetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_MAINS_INPUT_NO, value);
			}
			else 
			{
				value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_M3_INPUT_ST);
				if(AC_INPUT_ON == value.iValue)
				{
					value.iValue = AC_INPUT_NO_3TH;
					LduSetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_MAINS_INPUT_NO, value);
				}
			}
		}
	}


	value = LduGetRoughValue(LDU_EQUIP_TYPE_GROUP, 0, LDU_G_ACD_QTY);
	if(!(value.iValue)) //No ACD
	{
		bAcMainsFail = FALSE;
	}
	else
	{
		int	iValidAcd = 0;

		bAcMainsFail = TRUE;
		for (i = 0; i < value.iValue; i++)
		{
			value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_COMM_BREAK_TIMES);
			if(value.iValue >= LDU_MAX_COMM_BREAK_TIMES)
			{
				//后改的只要有一个通信中断就不会告组的交流停电
				bAcMainsFail = FALSE;
				break;
			}

			iValidAcd++;
			value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_MAINS_INPUT_NO);
			if(((AC_INPUT_NO_1ST == value.iValue)
				&& (LDU_NORMAL == (LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_MAINS1_FAIL_ST)).iValue))
				|| ((AC_INPUT_NO_2ND == value.iValue)
				&& (LDU_NORMAL == (LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_MAINS2_FAIL_ST)).iValue))
				|| ((AC_INPUT_NO_3TH == value.iValue)
				&& (LDU_NORMAL == (LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_MAINS3_FAIL_ST)).iValue)))
			{
				bAcMainsFail = FALSE;
				break;
			}
		}

		if(!iValidAcd) //No ACD communicated
		{
			bAcMainsFail = FALSE;
		}
	}

	value.iValue = bAcMainsFail;
	LduSetRoughValue(LDU_EQUIP_TYPE_GROUP, 0, LDU_G_AC_FAIL, value);

	return;
}


/*==========================================================================*
* FUNCTION : LDU_Scan
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*  pDevice : 
*            BYTE   byAddr  : 
* RETURN   : BOOL : TRUE-Device Exist; FALSE-No Exist
* COMMENTS : 
* CREATOR  : Fan Xiansheng            DATE: 2009-09-08 11:25
*==========================================================================*/
BOOL LDU_Scan(void* pDevice, BYTE byAddr)
{
	RS485_DEVICE_CLASS*		pLdu = pDevice;
	BYTE*					pbyRcvBuf;
	int						iReadLen = 0;
	LDU_SEND_CMD			SendCmd;

	SendCmd.byAddr = byAddr;
	SendCmd.byCmdId = LDU_CMD_ID_GET_ANALOG_CFG;
	SendCmd.wLduSigId = LDU_INVALID_SIG_ID;
	SendCmd.iValueType = LDU_VALUE_TYPE_INVALID;
	SendCmd.fValue = 0.0;

	LduPackSendCmd(pLdu, &SendCmd);

	if(!LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen))
	{
		return FALSE;
	}


	int						iDataLen, iRecvCRC;
	BYTE					byRtnId;

	//Length check
	iDataLen = HexStrToByte(pbyRcvBuf + FRAME_OFFSET_LENGTH + 2);
	iDataLen = (iDataLen << 8) + HexStrToByte(pbyRcvBuf + FRAME_OFFSET_LENGTH);

	if(iReadLen  != (iDataLen + FRAME_OFFSET_DATA + LDU_CHECK_SUM_LEN + 1))
	{
		return FALSE;
	}

	//CRC12 Check
	iRecvCRC = HexStrToByte(pbyRcvBuf + FRAME_OFFSET_DATA + iDataLen + 2);
	iRecvCRC = (iRecvCRC << 8) + HexStrToByte(pbyRcvBuf + FRAME_OFFSET_DATA + iDataLen);

	if( iRecvCRC != LduCrc12(pbyRcvBuf + FRAME_OFFSET_ADDR, iDataLen + 8 ) )
	{
		return FALSE;
	}

	//Address check
	byAddr = HexStrToByte(pbyRcvBuf + FRAME_OFFSET_ADDR);
	if(SendCmd.byAddr != byAddr)
	{
		return FALSE;
	}

	// RTN check
	byRtnId = HexStrToByte(pbyRcvBuf + FRAME_OFFSET_CMD_ID);
	if(SendCmd.byCmdId != byRtnId)	
	{
		return FALSE;
	}

	return TRUE;

}

/*==========================================================================*
* FUNCTION : LDU_Reconfig
* PURPOSE  : Scan all addresses and record the result in Rough Data
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*  pDevice : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:32
*==========================================================================*/
void LDU_Reconfig(void* pDevice)
{
	RS485_DEVICE_CLASS*		pLdu = pDevice;
	BYTE					byAddr;
	BOOL					bFind;

	int i;
	RS485_VALUE				value;
	int						iAcdEquipNo, iDcdEquipNo;

	g_iFindEquipMaxNum			= 0;
	static BOOL sb_FirstRun = TRUE;

	LDU_Init(pDevice);

	LduResetPort(pLdu->pCommPort);


	for(byAddr = LDU_FST_ACD_ADDR; byAddr <= LDU_LST_ADDR; byAddr++)
	{
		bFind = FALSE;

		for(i = 0; i < 3; i++)
		{
			if(LDU_Scan(pLdu, byAddr))
			{
				bFind = TRUE;
				//printf("[RS485:LDU] Find a board! address = %d\n", byAddr);
				break;
			}
			else
			{
				Sleep(100);
			}
		}

		if(bFind)
		{
			//ACD
			if((byAddr >= LDU_FST_ACD_ADDR) && (byAddr < LDU_FST_DCD_ADDR))
			{
				iAcdEquipNo = LduGetAcdNo(byAddr);
				if(iAcdEquipNo < 0)
				{
					continue;
				}
				g_iFindEquipMaxNum++;
				value.iValue = LDU_EQUIP_EXIST;
				LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_ACD_EXISTS, value);
				value.dwValue = byAddr;
				LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_ACD_ADDR, value);
			}

			//DCD
			else if(((byAddr >= LDU_FST_DCD_ADDR) && (byAddr < LDU_FST_ACDCD_ADDR))
				|| ((byAddr >= LDU_FST_OTHER_DCD_ADDR) && (byAddr <= LDU_LST_ADDR)))
			{
				iDcdEquipNo = LduGetDcdNo(byAddr);
				if(iDcdEquipNo < 0)
				{
					continue;
				}

				g_iFindEquipMaxNum++;
				value.iValue = LDU_EQUIP_EXIST;
				LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_DCD_EXISTS, value);
				value.dwValue = byAddr;
				LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_DCD_ADDR, value);
			}

			//ACD/DCD
			else if((byAddr >= LDU_FST_ACDCD_ADDR) && (byAddr < LDU_FST_OTHER_DCD_ADDR))
			{
				iAcdEquipNo = LduGetAcdNo(byAddr);
				if(iAcdEquipNo < 0)
				{
					continue;
				}

				
				iDcdEquipNo = LduGetDcdNo(byAddr);
				if(iDcdEquipNo < 0)
				{
					continue;
				}

				g_iFindEquipMaxNum++;
				value.iValue = LDU_EQUIP_EXIST;
				LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_ACD_EXISTS, value);
				LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_DCD_EXISTS, value);
				value.dwValue = byAddr;
				LduSetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdEquipNo, LDU_ACD_ADDR, value);
				LduSetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_DCD_ADDR, value);
			}
		}

		if (g_iFindEquipMaxNum >= LDU_MAX_EXIST_NUM)//max value 15 connection with largedu
		{
			break;//stop scan
		}
	}

	LduEquipNumCalc();

	return;
}

#define	LDU_MAX_TIMES_GET_ALL		5
#define	LDU_MAX_TIMES_RST_PORT		30
/*==========================================================================*
* FUNCTION : GetNeedAddr AndFirstGetParam
* PURPOSE  : Get Current address to sampling
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*  pDevice : 
* RETURN   : FAIL:  FALSE		SUCCESSFUL:s_iCurrentAddr
* COMMENTS : 
* CREATOR  :    IlockTeng            DATE: 2010-03-11 11:34
*==========================================================================*/
LOCAL INT32 GetNeedAddrAndFirstGetParam(RS485_DEVICE_CLASS*	 pLdu)
{
	static INT32	s_iCurrentAddr = LDU_FST_ACD_ADDR - 1;
	const  INT32	iLargeduMaxNum = (LDU_LST_ADDR -  LDU_FST_ACD_ADDR + 1);
	INT32			iCountTimes	   = 0;
	BYTE			byAddr;
	static BOOL		bFirstRunning  = TRUE;
	RS485_VALUE		stGDCDExistStat;
	RS485_VALUE		stGACDExistStat;
	//LduSetRoughValue(LDU_EQUIP_TYPE_GROUP, 0, LDU_G_ACDG_WORK_STATUS, value);
	//LduSetRoughValue(LDU_EQUIP_TYPE_GROUP, 0, LDU_G_DCDG_WORK_STATUS, value);

	stGDCDExistStat = LduGetRoughValue(LDU_EQUIP_TYPE_GROUP,0,LDU_G_DCDG_WORK_STATUS);
	stGACDExistStat = LduGetRoughValue(LDU_EQUIP_TYPE_GROUP,0,LDU_G_ACDG_WORK_STATUS);
	
	//设备根本不存在
	if (LDU_EQUIP_NOT_EXIST == stGDCDExistStat.iValue 
		&& LDU_EQUIP_NOT_EXIST == stGACDExistStat.iValue)
	{
		return FALSE;
	}

	if (bFirstRunning)
	{
		bFirstRunning = FALSE;
		for(byAddr = LDU_FST_ACD_ADDR; byAddr <= LDU_LST_ADDR; byAddr++)
		{
			if(LduNeedSample(byAddr))
			{
				if(!LduSampleGetSysCfg(pLdu, byAddr))
				{
					LduSampleGetSysCfg(pLdu, byAddr);		
				}

				if(!LduSampleGetUpDnLmt(pLdu, byAddr))
				{
					LduSampleGetUpDnLmt(pLdu, byAddr);
				}
			}
		}

		return FALSE;
	}
	else
	{
		//
	}

	RUN_THREAD_HEARTBEAT();

	//If the s_iCurrentAddr is LDU_LST_ADDR + 1 must return FALSE,no problem!!
	while ((!LduNeedSample(++s_iCurrentAddr)))//Inexistence 
	{
		iCountTimes++;

		if (s_iCurrentAddr > LDU_LST_ADDR)
		{
			s_iCurrentAddr = LDU_FST_ACD_ADDR - 1;//Repeat cycle
		}
		else
		{
		}

		if (iCountTimes > iLargeduMaxNum)//All Inexistence
		{
			return FALSE;
		}
		else
		{
			//no process!!
		}
	}

	return s_iCurrentAddr;
}
/*==========================================================================*
* FUNCTION : LduNo	SampCalc
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*  pDevice : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Ilock              DATE: 2010-03-18 11:34
*==========================================================================*/
static void LduDCGroupSigCalc()
{
	INT32	i,j,min,k,t;
	float	fTotalLoadCrrent = 0;
	float	fTotalDCDCrrent  = 0;
	float	fAverageDCDVolt  = 0;
	RS485_VALUE		value;	
	float	fDCDBattVolt[LDU_MAX_DCD_NUM] = {0};

	//Get DCD Total Batt Current	!!
	for (i = 0; i < LDU_MAX_BATT_NUM; i++)
	{
		value = LduGetRoughValue(LDU_EQUIP_TYPE_BATT,i,LDU_BATT_EXISTS);
		if (LDU_EQUIP_EXIST == value.iValue)
		{
			value = LduGetRoughValue(LDU_EQUIP_TYPE_BATT,i,LDU_BATT_CURR);
			fTotalDCDCrrent += value.fValue;
		}
		else
		{
			//
		}
	}
	value.fValue = fTotalDCDCrrent;
	LduSetRoughValue(LDU_EQUIP_TYPE_GROUP, 0, LDU_G_DCD_TOTAL_BATT_CURRENT, value);
	
	//Get Total Load Current !!
	for(i = 0; i < LDU_MAX_DCD_NUM; i++)
	{
		value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_EXISTS);
		if(value.iValue == LDU_EQUIP_EXIST)
		{
			//Get one DCD Total Current
			value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_LOAD_CURR);
			fTotalLoadCrrent += value.fValue;
		}
		else
		{
			//
		}
	}
	value.fValue = fTotalLoadCrrent;
	LduSetRoughValue(LDU_EQUIP_TYPE_GROUP, 0, LDU_G_DCD_LOAD_CURRENT, value);

	//Get Bus Average Volt
	j = 0;
	for(i = 0; i < LDU_MAX_DCD_NUM; i++)
	{
		value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_ADDR);
		if(value.iValue == RS485_SAMP_INVALID_VALUE)
		{
			break;
		}
		else
		{
			value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_BUS_VOLT);
			fDCDBattVolt[j++] = value.fValue;
		}
	}

	//Sort the	fDCDBattVolt[i] by select way
	for (i = 0; i < j - 1; i++)//j array number
	{
		min = i;
		for (k = i + 1; k < j; k++)
		{
			if (*(fDCDBattVolt + k) < *(fDCDBattVolt + min))
			{   
				min = k; 
			}
		}  

		if (min != i) 
		{
			t = *(fDCDBattVolt + i);
			*(fDCDBattVolt + i) = *(fDCDBattVolt + min);
			*(fDCDBattVolt + min) = t;
		}
	}

	fAverageDCDVolt = fDCDBattVolt[j / 2];
	value.fValue = fAverageDCDVolt;
	LduSetRoughValue(LDU_EQUIP_TYPE_GROUP, 0, LDU_G_DCD_AVERAGE_VOLT, value);

}


/*==========================================================================*
* FUNCTION : LDU_Sample
* PURPOSE  : Interface Function of sampling data
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*  pDevice : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:34
*==========================================================================*/
void LDU_Sample(void* pDevice)
{
	RS485_DEVICE_CLASS*		pLdu = pDevice;
	LDU_COMM_INFO*			pLduCommInfo = pLdu->pCommInfo;
	BYTE					byAddr;
	BYTE					byNeedProcAddr;
	BOOL					bSampleOk = FALSE;
	static int				s_iCntForRstPort = 0;

	if(pLdu->bNeedReconfig || !pLdu->pCommPort->bOpened)
	{
		LDU_Reconfig(pLdu);
		pLdu->bNeedReconfig = FALSE;
	}

	LduReopenPort(pLdu->pCommPort);

	byNeedProcAddr = GetNeedAddrAndFirstGetParam(pLdu);

	if (byNeedProcAddr > 0)
	{
		if(!LduSampleGetSysCfg(pLdu, byNeedProcAddr))
		{
			LduSampleGetSysCfg(pLdu, byNeedProcAddr);		
		}

		if(!LduSampleGetUpDnLmt(pLdu, byNeedProcAddr))
		{
			LduSampleGetUpDnLmt(pLdu, byNeedProcAddr);
		}
	}
	else
	{
		//Get Address error!!!
	}

	for(byAddr = LDU_FST_ACD_ADDR; byAddr <= LDU_LST_ADDR; byAddr++)
	{
		if(LduNeedSample(byAddr))
		{
			if(!LduSampleGetAll(pLdu, byAddr))
			{
				LduIncCommBreakTimes(byAddr);
				continue;
			}
			else
			{
				bSampleOk = TRUE;
				LduClrCommBreakTimes(byAddr);			
			}	
			//pLduCommInfo->aiSampleAllTimes[byAddr - LDU_FST_ACD_ADDR]++;
		}	
	}

	LduAcFailCalc();
	LduBattNumCalc();
	LduDCGroupSigCalc();

	if(bSampleOk)
	{
		s_iCntForRstPort = 0;
	}
	else
	{
		s_iCntForRstPort++;
		if(s_iCntForRstPort > LDU_MAX_TIMES_RST_PORT)
		{
			LduResetPort(pLdu->pCommPort);

			//After Comm Failure for a while, Sampler will re-config and clear comm failure alarm, so need not re-config, only reset port
			//pLdu->bNeedReconfig = TRUE;
		}
	}

	return;
}



/*==========================================================================*
* FUNCTION : LDU_ AcdParamUnify
* PURPOSE  : Ensure the ACD parameters equal to sampling data in rough data
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*  pDevice : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:36
*==========================================================================*/
static void LDU_AcdParamUnify(void* pDevice)
{
	RS485_DEVICE_CLASS*		pLdu = pDevice;

	LDU_PARAM_UNIFY_DEF		LduAcdParamDef[] =
	{
		{LDU_ACD_GROUP_EQUIP_ID,	0,	LDU_AC_OVER_VOLT_SIG_ID,	LDU_ACD_OVER_VOLT_LMT,		LDU_CMD_ID_SET_UP_DN_LMT,	LDU_VALUE_TYPE_FLOAT,	0x4601,	},
		{LDU_ACD_GROUP_EQUIP_ID,	0,	LDU_AC_UNDER_VOLT_SIG_ID,	LDU_ACD_UNDER_VOLT_LMT,		LDU_CMD_ID_SET_UP_DN_LMT,	LDU_VALUE_TYPE_FLOAT,	0x4602,	},
		{LDU_ACD_GROUP_EQUIP_ID,	0,	LDU_AC_P_FAIL_VOLT_SIG_ID,	LDU_ACD_P_FAIL_LMT,			LDU_CMD_ID_SET_UP_DN_LMT,	LDU_VALUE_TYPE_FLOAT,	0x4603,	},	
		{LDU_ACD_GROUP_EQUIP_ID,	0,	LDU_AC_OVER_FREQ_SIG_ID,	LDU_ACD_OVER_FREQ_LMT,		LDU_CMD_ID_SET_UP_DN_LMT,	LDU_VALUE_TYPE_FLOAT,	0x4604,	},
		{LDU_ACD_GROUP_EQUIP_ID,	0,	LDU_AC_UNDER_FREQ_SIG_ID,	LDU_ACD_UNDER_FREQ_LMT,		LDU_CMD_ID_SET_UP_DN_LMT,	LDU_VALUE_TYPE_FLOAT,	0x4605,	},
		{LDU_ACD_UNIT_EQUIP_ID,		1,	LDU_AC_CURR_COEF_SIG_ID,	LDU_ACD_CURR_COEF,			LDU_CMD_ID_SET_SYS_PARAM,	LDU_VALUE_TYPE_FLOAT,	0x4801,	},
		{LDU_ACD_UNIT_EQUIP_ID,		1,	LDU_AC_INPUT_TYPE_SIG_ID,	LDU_ACD_INPUT_TYPE,			LDU_CMD_ID_SET_SYS_PARAM,	LDU_VALUE_TYPE_BYTE,	0x4809,	},
		{LDU_ACD_UNIT_EQUIP_ID,		1,	LDU_AC_INPUT_NUM_SIG_ID,	LDU_ACD_INPUT_NUM,			LDU_CMD_ID_SET_SYS_PARAM,	LDU_VALUE_TYPE_BYTE,	0x480A,	},
		{LDU_ACD_UNIT_EQUIP_ID,		1,	LDU_AC_CURR_M_MODE_SIG_ID,	LDU_ACD_CURR_MEASURE_MODE,	LDU_CMD_ID_SET_SYS_PARAM,	LDU_VALUE_TYPE_BYTE,	0x4813,	},
		{LDU_ACD_UNIT_EQUIP_ID,		1,	LDU_AC_OUTPUT_NUM_SIG_ID,	LDU_ACD_OUTPUT_NUM,			LDU_CMD_ID_SET_SYS_PARAM,	LDU_VALUE_TYPE_BYTE,	0x4812,	},

		{0,							0, LDU_INVALID_SIG_ID,			LDU_INVALID_SIG_ID,			0,							0,						0,		},
	};

	int			i, j;

	for(i = 0; i < LDU_MAX_ACD_NUM; i++)
	{
		RS485_VALUE		value;
		BYTE			byAddr;

		value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_ADDR);
		if(value.iValue == RS485_SAMP_INVALID_VALUE)
		{
			break;
		}
		else
		{
			byAddr = value.dwValue;
		}

		value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_COMM_BREAK_TIMES);
		if(value.iValue >= LDU_MAX_COMM_BREAK_TIMES)
		{
			continue;
		}

		j = 0;
		while(LduAcdParamDef[j].iSigId != LDU_INVALID_SIG_ID)
		{
			float	fParam;
			float	fSampleValue;

			DWORD	dwParam;
			DWORD	dwSampleValue;

			BOOL	bNeedSend;
			LDU_SEND_CMD			SendCmd;

			bNeedSend = FALSE;

			if(LduAcdParamDef[j].byValType == LDU_VALUE_TYPE_FLOAT)
			{
				value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LduAcdParamDef[j].iRoughData);
				fSampleValue = value.fValue;

				fParam = GetFloatSigValue(LduAcdParamDef[j].iEquipId 
											+ i * LduAcdParamDef[j].iEquipIdDifference, 
											SIG_TYPE_SETTING,
											LduAcdParamDef[j].iSigId,
											"485_SAMP");

				//说明这个LARGEDU扳子没回或未配置这个信号，则fSampleValue值也是0
				//FLOAT_NOT_EQUAL进不去，所以不需要同步！！
				if(FLOAT_NOT_EQUAL(fParam, fSampleValue))
				{
					bNeedSend = TRUE;
					SendCmd.fValue = fParam;
				}
			}
			else
			{
				value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, i, LduAcdParamDef[j].iRoughData);
				dwSampleValue = value.dwValue;

				dwParam = GetDwordSigValue(LduAcdParamDef[j].iEquipId 
											+ i * LduAcdParamDef[j].iEquipIdDifference, 
											SIG_TYPE_SETTING,
											LduAcdParamDef[j].iSigId,
											"485_SAMP");

				if(dwSampleValue != dwParam)
				{
					bNeedSend = TRUE;
					SendCmd.fValue = dwParam;
				}			
			}

			if(bNeedSend)
			{
				SendCmd.byAddr = byAddr;
				SendCmd.byCmdId = LduAcdParamDef[j].bySetCmdId;
				SendCmd.wLduSigId = LduAcdParamDef[j].wSigCode;
				SendCmd.iValueType = LduAcdParamDef[j].byValType;
				LduPackSendCmd(pLdu, &SendCmd);
				BYTE*					pbyRcvBuf;
				int						iReadLen = 0;
				LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen);

				//printf(" ~~~~~~~~~ ACD ACD   UNIFY  SigCode %4X ~~~~~~~~~~~~  \n",LduAcdParamDef[j].wSigCode);
			}

			j++;
		}
	}
}

//add 2010/3/19
void ClrBattExistStat()
{
	RS485_VALUE		value;
	INT32 i,j;
	value.iValue = LDU_EQUIP_NOT_EXIST;

	for(i = 0; i < LDU_MAX_BATT_NUM; i++)
	{
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, i, LDU_BATT_EXISTS, value);	
	}
}


/*==========================================================================*
* FUNCTION : LDU_ DcdParamUnify
* PURPOSE  : Ensure the DCD parameters equal to sampling data in rough data
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*  pDevice : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:38
*==========================================================================*/
static void LDU_DcdParamUnify(void* pDevice)
{
	RS485_DEVICE_CLASS*		pLdu = pDevice;
	RS485_VALUE		value222,value333;


	LDU_PARAM_UNIFY_DEF		LduDcdParamDef[] =
	{
		{LDU_DCD_GROUP_EQUIP_ID,	0,	LDU_DC_OVER_VOLT_SIG_ID,		LDU_DCD_OVER_VOLT_LMT,		LDU_CMD_ID_SET_UP_DN_LMT,	LDU_VALUE_TYPE_FLOAT,	0x4619,	},
		{LDU_DCD_GROUP_EQUIP_ID,	0,	LDU_DC_UNDER_VOLT_SIG_ID,		LDU_DCD_UNDER_VOLT_LMT,		LDU_CMD_ID_SET_UP_DN_LMT,	LDU_VALUE_TYPE_FLOAT,	0x461A,	},
		{LDU_DCD_UNIT_EQUIP_ID,		1,	LDU_DC_OVER_TEMP1_SIG_ID,		LDU_DCD_OVER_TEMP1_LMT,		LDU_CMD_ID_SET_UP_DN_LMT,	LDU_VALUE_TYPE_FLOAT,	0x460D,	},	
		{LDU_DCD_UNIT_EQUIP_ID,		1,	LDU_DC_OVER_TEMP2_SIG_ID,		LDU_DCD_OVER_TEMP2_LMT,		LDU_CMD_ID_SET_UP_DN_LMT,	LDU_VALUE_TYPE_FLOAT,	0x460E,	},
		{LDU_DCD_UNIT_EQUIP_ID,		1,	LDU_DC_OVER_TEMP3_SIG_ID,		LDU_DCD_OVER_TEMP3_LMT,		LDU_CMD_ID_SET_UP_DN_LMT,	LDU_VALUE_TYPE_FLOAT,	0x460F,	},
		{LDU_DCD_UNIT_EQUIP_ID,		1,	LDU_DC_TEMP_COEF_SIG_ID,		LDU_DCD_TEMP_COEF,			LDU_CMD_ID_SET_SYS_PARAM,	LDU_VALUE_TYPE_FLOAT,	0x4802,	},
		//added by Ilock teng
		//{LDU_DCD_UNIT_EQUIP_ID,		1,	LDU_DC_TEMP_COEF_SIG_ID,		LDU_DCD_TEMP_COEF,			LDU_CMD_ID_SET_SYS_PARAM,	LDU_VALUE_TYPE_BYTE,	0x4802,	},

		{LDU_DCD_UNIT_EQUIP_ID,		1,	LDU_DC_LOAD_CURR_COEF_SIG_ID,	LDU_DCD_LOAD_CURR_COEF,		LDU_CMD_ID_SET_SYS_PARAM,	LDU_VALUE_TYPE_FLOAT,	0x4806,	},
		{LDU_DCD_UNIT_EQUIP_ID,		1,	LDU_DC_BRANCH_CURR_COEF_SIG_ID,	LDU_DCD_BRANCH_CURR_COEF,	LDU_CMD_ID_SET_SYS_PARAM,	LDU_VALUE_TYPE_FLOAT,	0x4814,	},		
		{LDU_DCD_UNIT_EQUIP_ID,		1,	LDU_DC_BATT_NUM_SIG_ID,			LDU_DCD_BATT_NUM,			LDU_CMD_ID_SET_SYS_PARAM,	LDU_VALUE_TYPE_BYTE,	0x4807,	},
		{LDU_DCD_UNIT_EQUIP_ID,		1,	LDU_DC_TEMP_NUM_SIG_ID,			LDU_DCD_TEMP_NUM,			LDU_CMD_ID_SET_SYS_PARAM,	LDU_VALUE_TYPE_BYTE,	0x4808,	},
		{LDU_DCD_UNIT_EQUIP_ID,		1,	LDU_DC_BRANCH_CURR_NUM_SIG_ID,	LDU_DCD_BRANCH_CURR_NUM,	LDU_CMD_ID_SET_SYS_PARAM,	LDU_VALUE_TYPE_BYTE,	0x480D,	},
		{LDU_DCD_UNIT_EQUIP_ID,		1,	LDU_DC_FUSE_NUM_SIG_ID,			LDU_DCD_FUSE_NUM,			LDU_CMD_ID_SET_SYS_PARAM,	LDU_VALUE_TYPE_BYTE,	0x480E,	},	

		{0,							0,	LDU_INVALID_SIG_ID,				LDU_INVALID_SIG_ID,			0,							0,						0,		},
	};

	int			i, j;

	for(i = 0; i < LDU_MAX_DCD_NUM; i++)
	{
		RS485_VALUE		value;
		BYTE			byAddr;

		value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_ADDR);
		if(value.iValue == RS485_SAMP_INVALID_VALUE)
		{
			break;
		}
		else
		{
			byAddr = value.dwValue;
		}

		value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_COMM_BREAK_TIMES);
		if(value.iValue >= LDU_MAX_COMM_BREAK_TIMES)
		{
			continue;
		}

		j = 0;
		while(LduDcdParamDef[j].iSigId != LDU_INVALID_SIG_ID)
		{
			float	fParam;
			float	fSampleValue;

			DWORD	dwParam;
			DWORD	dwSampleValue;

			BOOL	bNeedSend;
			LDU_SEND_CMD			SendCmd;

			bNeedSend = FALSE;

			if(LduDcdParamDef[j].byValType == LDU_VALUE_TYPE_FLOAT)
			{
				value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LduDcdParamDef[j].iRoughData);
				fSampleValue = value.fValue;

				fParam = GetFloatSigValue(LduDcdParamDef[j].iEquipId 
											+ i * LduDcdParamDef[j].iEquipIdDifference, 
											SIG_TYPE_SETTING,
											LduDcdParamDef[j].iSigId,
											"485_SAMP");

				//说明这个LARGEDU扳子没回或未配置这个信号，则fSampleValue值也是0
				//FLOAT_NOT_EQUAL进不去，所以不需要同步！！
				if(FLOAT_NOT_EQUAL(fParam, fSampleValue))
				{
					bNeedSend = TRUE;
					SendCmd.fValue = fParam;
				


						value222.fValue = fParam;
						value333.fValue = fSampleValue;

						//printf("!!FLOAT CODE=%4X  DCDNo=%d  fParam=%f i%d fSampleValue=%f i%d\n",
						//										LduDcdParamDef[j].wSigCode,
						//										i,
						//										fParam,
						//										value222.iValue,
						//										fSampleValue,
						//										value333.iValue);
					
				}
			}
			else
			{
				value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LduDcdParamDef[j].iRoughData);
				dwSampleValue = value.dwValue;

				dwParam = GetDwordSigValue(LduDcdParamDef[j].iEquipId 
												+ i * LduDcdParamDef[j].iEquipIdDifference, 
												SIG_TYPE_SETTING,
												LduDcdParamDef[j].iSigId,
												"485_SAMP");

				if(dwSampleValue != dwParam)
				{

					value222.iValue = dwParam;
					value333.iValue = dwSampleValue;
					//printf("!!!DW CODE=%4X DCDNo=%d FParam=%f i%d FSampleValue=%f i%d\n",
					//									LduDcdParamDef[j].wSigCode,
					//									i,
					//									value222.fValue,
					//									dwParam,
					//									value333.fValue,
					//									dwSampleValue);
				
					if (LDU_DCD_BATT_NUM == LduDcdParamDef[j].iRoughData)
					{
						/*
						因为BATT的存在状态和数目是每次GETALL采集中刷新并置位的，
						而如果电池数增加了，则在GETALL中刷新了，但是如果电池数目
						减少了，则GETALL不会将已经置位的BATT存在状态清零，因为是向前
						堆放的。所以在这里全部置电池不存在，在GETTALL的时候再让其刷出来
						*/
						ClrBattExistStat();
					}
					bNeedSend = TRUE;
					SendCmd.fValue = dwParam;
				}			
			}

			if(bNeedSend)
			{
				SendCmd.byAddr = byAddr;
				SendCmd.byCmdId = LduDcdParamDef[j].bySetCmdId;
				SendCmd.wLduSigId = LduDcdParamDef[j].wSigCode;
				SendCmd.iValueType = LduDcdParamDef[j].byValType;
				LduPackSendCmd(pLdu, &SendCmd);
				
				//printf(" ~~~ DCDUNIFYCode=%4X  byAddr=%d byCmdId=%2X iValue=%d ~~~~  \n",
				//						LduDcdParamDef[j].wSigCode,
				//						byAddr,
				//						SendCmd.byCmdId,
				//						SendCmd.iValueType
				//);

				BYTE*					pbyRcvBuf;
				int						iReadLen = 0;
				LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen);
			}
			j++;
		}
	}
}


#define		SIG_CODE_BATT1_OVER_VOLT	0x4607
#define		SIG_CODE_BATT1_UNDER_VOLT	0x4608
#define		SIG_CODE_BATT1_OVER_CURR	0x4609
#define		SIG_CODE_BATT1_CURR_COEF	0x4804
#define		SIG_CODE_BATT2_CURR_COEF	0x4805
#define		SIG_CODE_BATT3_CURR_COEF	0x4815
#define		SIG_CODE_BATT4_CURR_COEF	0x4816

/*==========================================================================*
* FUNCTION : LDU_BattParamUnify
* PURPOSE  : Ensure the Battery parameters equal to sampling data in rough data
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*  pDevice : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:38
*==========================================================================*/
static void LDU_BattParamUnify(void* pDevice)
{
	RS485_DEVICE_CLASS*		pLdu = pDevice;
	int						i, j;
	int						iBattNo = 0;
	BYTE*					pbyRcvBuf;
	int						iReadLen = 0;
	float					fTemp;
	INT32					iTempBattNo;

	WORD					awSigCodeCurrCoef[] = 
	{
		SIG_CODE_BATT1_CURR_COEF,
		SIG_CODE_BATT2_CURR_COEF,
		SIG_CODE_BATT3_CURR_COEF,
		SIG_CODE_BATT4_CURR_COEF,
	};

	for(i = 0; i < LDU_MAX_DCD_NUM; i++)
	{
		RS485_VALUE		value;
		BYTE			byAddr;
		int				iBattNum;
		float			fParam;
		float			fSampleValue;

		value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_ADDR);
		if(value.iValue == RS485_SAMP_INVALID_VALUE)
		{
			break;
		}
		else
		{
			byAddr = value.dwValue;
			value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_BATT_NUM);
			iBattNum = value.iValue;
		}

		value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_COMM_BREAK_TIMES);
		if(value.iValue >= LDU_MAX_COMM_BREAK_TIMES)
		{
			continue;
		}

		//Here only Battery 1 Over Voltage, Under Voltage and Over Current shall be set, 
		//because other batteries' are invalid in Large DU
		if(iBattNum)
		{
			//容错，否则接上模拟软件设置错误，超过20将出错
			if (iBattNo >= LDU_ACTUAL_MAX_BATT_NUM)
			{
				return;
			}

			//Battery Over Voltage,  过压
			/*
				iBattNo： 每个DC屏的第一个电池，
				要结合下边一个for(j = 0; j < iBattNum; j++, iBattNo++)
				才能看明白，分析iBattNo++即可。
			*/
			value = LduGetRoughValue(LDU_EQUIP_TYPE_BATT, iBattNo, LDU_BATT_OVER_VOLT_LMT);
			fSampleValue = value.fValue;

			//fParam = GetFloatSigValue(LDU_DCD_GROUP_EQUIP_ID, 
			//							SIG_TYPE_SETTING,
			//							LDU_BATT_OVER_VOLT_SIG_ID,
			//							"485_SAMP");

			fParam = GetFloatSigValue(LDU_DCD_UNIT_EQUIP_ID + i,
										SIG_TYPE_SETTING,
										LDU_BATT_OVER_VOLT_SIG_ID,
										"485_SAMP");

			if(FLOAT_NOT_EQUAL(fParam, fSampleValue))
			{
				LDU_SEND_CMD			SendCmd;
				SendCmd.byAddr = byAddr;
				SendCmd.byCmdId = LDU_CMD_ID_SET_UP_DN_LMT;
				SendCmd.wLduSigId = SIG_CODE_BATT1_OVER_VOLT;
				SendCmd.iValueType = LDU_VALUE_TYPE_FLOAT;
				SendCmd.fValue = fParam;
				LduPackSendCmd(pLdu, &SendCmd);
				//printf("~~~~~~~~~~~~  LargeBatt UNIFY CODE %4X~~~~~~~~\n",SendCmd.wLduSigId);
				LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen);	
			}

			//Battery Under Voltage  欠压
			value = LduGetRoughValue(LDU_EQUIP_TYPE_BATT, iBattNo, LDU_BATT_UNDER_VOLT_LMT);
			fSampleValue = value.fValue;

			//fParam = GetFloatSigValue(LDU_DCD_GROUP_EQUIP_ID, 
			//							SIG_TYPE_SETTING,
			//							LDU_BATT_UNDER_VOLT_SIG_ID,
			//							"485_SAMP");

			fParam = GetFloatSigValue(LDU_DCD_UNIT_EQUIP_ID + i, 
										SIG_TYPE_SETTING,
										LDU_BATT_UNDER_VOLT_SIG_ID,
										"485_SAMP");

			if(FLOAT_NOT_EQUAL(fParam, fSampleValue))
			{
				LDU_SEND_CMD			SendCmd;
				SendCmd.byAddr = byAddr;
				SendCmd.byCmdId = LDU_CMD_ID_SET_UP_DN_LMT;
				SendCmd.wLduSigId = SIG_CODE_BATT1_UNDER_VOLT;
				SendCmd.iValueType = LDU_VALUE_TYPE_FLOAT;
				SendCmd.fValue = fParam;
				LduPackSendCmd(pLdu, &SendCmd);
				//printf("~~~~~~~~~~~~  LargeBatt UNIFY CODE %4X~~~~~~~~\n",SendCmd.wLduSigId);
				LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen);
			}

			//Battery Over Current
			value = LduGetRoughValue(LDU_EQUIP_TYPE_BATT, iBattNo, LDU_BATT_OVER_CURR_LMT);
			fSampleValue = value.fValue;

			fParam = GetFloatSigValue(LDU_BATT_GROUP_EQUIP_ID, 
										SIG_TYPE_SETTING,
										LDU_BTG_OVER_CURR_SIG_ID,
										"485_SAMP");					//First get Over Current by C10

			/*
				因为都是以第一串电池为主，所以现在将容量更改到DC单元中设置了。
				一个DC单元最多4电池，现在协议中只有电池1和2的过流点，最终讨论与
				在DC单元中添加一个电池容量设置，然后将4该DC屏下的各个电池的容量设置
				的与DC屏的一致。
			*/
			//fParam *= GetFloatSigValue(LDU_BATT_UNIT_EQUIP_ID + iBattNo, 
			//							SIG_TYPE_SETTING,
			//							LDU_BT_RATED_CAP_SIG_ID,
			//							"485_SAMP");					//then multiply it by Rated Capacity
			
			//1取DC单元的
			//fTemp = GetFloatSigValue(LDU_DCD_UNIT_EQUIP_ID + i, 
			//						SIG_TYPE_SETTING,
			//						LDU_DC_RATED_CAP_SIG_ID,
			//						"485_SAMP");					//then multiply it by Rated Capacity	
			
			//2010/07/09	更改为电池组里
			fTemp = GetFloatSigValue(LDU_BATT_GROUP_EQUIP_ID, 
									SIG_TYPE_SETTING,
									LDU_DC_RATED_CAP_SIG_ID,
									"485_SAMP");					//then multiply it by Rated Capacity	


			//printf("\nThis is Get DCD Setting signal Equip=%d \n\n",LDU_DCD_UNIT_EQUIP_ID + i);


			fParam *= fTemp;
			
			//2然后将电池组中的容量，再给DC屏下的电池设置下去。
			//从下边分析，iBattNo为每个DC屏的第一个电池的Sequence Number
			for (iTempBattNo = iBattNo; iTempBattNo < (iBattNo + iBattNum); iTempBattNo++)
			{
				SetFloatSigValue(LDU_BATT_UNIT_EQUIP_ID + iTempBattNo,
										SIG_TYPE_SETTING,
										LDU_BT_RATED_CAP_SIG_ID,
										fTemp,
										"485_SAMP");
				//printf("\nThis is Set LDU_BATT Cap  Equip=%d \n",LDU_BATT_UNIT_EQUIP_ID + iTempBattNo);
			}

			if(FLOAT_NOT_EQUAL(fParam, fSampleValue))
			{
				LDU_SEND_CMD			SendCmd;
				SendCmd.byAddr = byAddr;
				SendCmd.byCmdId = LDU_CMD_ID_SET_UP_DN_LMT;
				SendCmd.wLduSigId = SIG_CODE_BATT1_OVER_CURR;
				SendCmd.iValueType = LDU_VALUE_TYPE_FLOAT;
				SendCmd.fValue = fParam;
				LduPackSendCmd(pLdu, &SendCmd);
				//printf("~~~~~~~~~~~~  LargeBatt UNIFY CODE %4X~~~~~~~~\n",SendCmd.wLduSigId);
				LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen);
			}
		}

		for(j = 0; j < iBattNum; j++, iBattNo++)
		{
			//容错，否则接上模拟软件设置错误，超过20将出错
			if (iBattNo >= LDU_ACTUAL_MAX_BATT_NUM)
			{
				return;
			}

			//Battery Cuurent Coefficient
			value = LduGetRoughValue(LDU_EQUIP_TYPE_BATT, iBattNo, LDU_BATT_CURR_COEF);
			fSampleValue = value.fValue;

			fParam = (float)GetDwordSigValue(LDU_BATT_UNIT_EQUIP_ID + iBattNo, 
												SIG_TYPE_SETTING,
												LDU_BT_CURR_COEF_SIG_ID,
												"485_SAMP");

			if(FLOAT_NOT_EQUAL(fParam, fSampleValue))
			{
				LDU_SEND_CMD			SendCmd;
				SendCmd.byAddr = byAddr;
				SendCmd.byCmdId = LDU_CMD_ID_SET_SYS_PARAM;
				SendCmd.wLduSigId = awSigCodeCurrCoef[j];
				SendCmd.iValueType = LDU_VALUE_TYPE_FLOAT;
				SendCmd.fValue = fParam;
				LduPackSendCmd(pLdu, &SendCmd);
				//printf("~~~~~~~~~~~~  LargeBatt UNIFY CODE %4X~~~~~~~~\n",SendCmd.wLduSigId);
				LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen);
			}
		}
	}
}

/*==========================================================================*
* FUNCTION : LDU_LvdParamUnify
* PURPOSE  : Ensure the parameters equal to sampling data in rough data
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*  pDevice : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Ilock                DATE: 2010-03-17 11:39
*==========================================================================*/
static void LDU_LvdParamUnify(void* pDevice)
{

	int						i;
	int						iReadLen = 0;
	BOOL					b48VSysCfg = FALSE;
	RS485_VALUE				value;
	RS485_DEVICE_CLASS*		pLdu = pDevice;
	float					fInternalValue;//internal value
	BYTE*					pbyRcvBuf;
	BYTE					byAddr;
	LDU_SEND_CMD			SendCmd;

	fInternalValue = GetFloatSigValue(LDU_RECT_GROUP_EQUIP_ID,
									  LDU_RECT_RATED_VOLTAGE_TYPE_ID,
									  LDU_RECT_RATED_VOLTAGE_SIG_ID,
									  "LDU_SAMP_LVD");	
	if (fInternalValue > 35.0)
	{
		b48VSysCfg = TRUE;
	}
	else
	{
		b48VSysCfg = FALSE;
	}

	for(i = 0; i < LDU_MAX_DCD_NUM; i++)
	{
		value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_ADDR);
		if(value.iValue == RS485_SAMP_INVALID_VALUE)
		{
			break;
		}
		else
		{
			byAddr = value.dwValue;
		}

		value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_COMM_BREAK_TIMES);
		if(value.iValue >= LDU_MAX_COMM_BREAK_TIMES)
		{
			continue;
		}

		if (b48VSysCfg)
		{
			//48V	lvd1 parameter sync
			fInternalValue = GetFloatSigValue(LDU_LVD_GROUP_EQUIP_ID,
											  LDU_LVD_DIS_SIG_TYPE,
											  LDU_48V_LVD1_DISC_SIG_ID,
											  "LDU_SAMP_48VLVD1");
			value = LduGetRoughValue(LDU_EQUIP_TYPE_LVD, i, LDU_LVD2_VOLT_LMT);//LVD1 code 4616

			if(FLOAT_NOT_EQUAL(fInternalValue, value.fValue))	
			{
				SendCmd.byAddr		= byAddr;
				SendCmd.byCmdId		= LDU_CMD_ID_SET_UP_DN_LMT;
				SendCmd.iValueType  = LDU_VALUE_TYPE_FLOAT;
				SendCmd.wLduSigId	= 0x4616;
				SendCmd.fValue		= fInternalValue; 
				LduPackSendCmd(pLdu, &SendCmd);
				//printf("~~~~~~~~~~~~  Large LVD UNIFY CODE %4X~~~~~~~~\n",SendCmd.wLduSigId);
				LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen);
			}
			else
			{
				//Needn't process!!!
			}

			//48V	lvd2 parameter sync
			fInternalValue = GetFloatSigValue(LDU_LVD_GROUP_EQUIP_ID,
											  LDU_LVD_DIS_SIG_TYPE,
											  LDU_48V_LVD2_DIS_SIG_ID,
											  "LDU_SAMP_48VLVD2");
			value = LduGetRoughValue(LDU_EQUIP_TYPE_LVD, i, LDU_LVD1_VOLT_LMT);//LVD2 code 4618
			
			if (FLOAT_NOT_EQUAL(fInternalValue, value.fValue))
			{
				SendCmd.byAddr		= byAddr;
				SendCmd.byCmdId		= LDU_CMD_ID_SET_UP_DN_LMT;
				SendCmd.iValueType  = LDU_VALUE_TYPE_FLOAT;
				SendCmd.wLduSigId	= 0x4618;
				SendCmd.fValue		= fInternalValue; 
				LduPackSendCmd(pLdu, &SendCmd);
				//printf("~~~~~~~~~~~~  Large LVD UNIFY CODE %4X~~~~~~~~\n",SendCmd.wLduSigId);
				LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen);
			}
			else
			{
				//Needn't process!!!
			}
		}
		else
		{
			//24V	lvd1 parameter sync
			fInternalValue = GetFloatSigValue(LDU_LVD_GROUP_EQUIP_ID,
											  LDU_LVD_DIS_SIG_TYPE,
											  LDU_24V_LVD1_DISC_SIG_ID,
											  "LDU_SAMP_24VLVD1");
			value = LduGetRoughValue(LDU_EQUIP_TYPE_LVD, i, LDU_LVD2_VOLT_LMT);//LVD1 code 4616

			if(FLOAT_NOT_EQUAL(fInternalValue,value.fValue))	
			{
				SendCmd.byAddr		= byAddr;
				SendCmd.byCmdId		= LDU_CMD_ID_SET_UP_DN_LMT;
				SendCmd.iValueType  = LDU_VALUE_TYPE_FLOAT;
				SendCmd.wLduSigId	= 0x4616;
				SendCmd.fValue		= fInternalValue; 
				LduPackSendCmd(pLdu, &SendCmd);
				//printf("~~~~~~~~~~~~  Large 24V LVD UNIFY CODE %4X~~~~~~~~\n",SendCmd.wLduSigId);
				LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen);

			}
			else
			{
				//Needn't process!!!
			}

			//24V	lvd2 parameter sync
			fInternalValue = GetFloatSigValue(LDU_LVD_GROUP_EQUIP_ID,
											  LDU_LVD_DIS_SIG_TYPE,
											  LDU_24V_LVD2_DIS_SIG_ID,
											  "LDU_SAMP_24VLVD2");
			value = LduGetRoughValue(LDU_EQUIP_TYPE_LVD, i, LDU_LVD1_VOLT_LMT);//LVD2 code 4618

			if (FLOAT_NOT_EQUAL(fInternalValue,value.fValue))
			{
				SendCmd.byAddr		= byAddr;
				SendCmd.byCmdId		= LDU_CMD_ID_SET_UP_DN_LMT;
				SendCmd.iValueType  = LDU_VALUE_TYPE_FLOAT;
				SendCmd.wLduSigId	= 0x4618;
				SendCmd.fValue		= fInternalValue; 
				LduPackSendCmd(pLdu, &SendCmd);
				//printf("~~~~~~~~~~~~  Large 24V LVD UNIFY CODE %4X~~~~~~~~\n",SendCmd.wLduSigId);
				LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen);

			}
			else
			{
				//Needn't process!!!
			}
		}
	}
}

/*==========================================================================*
* FUNCTION : LDU_ParamUnify
* PURPOSE  : Ensure the parameters equal to sampling data in rough data
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*  pDevice : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:39
*==========================================================================*/
void LDU_ParamUnify(void* pDevice)
{
	RS485_DEVICE_CLASS*	pLdu = pDevice;
	LduReopenPort(pLdu->pCommPort);

	//#if (!BE_SORFT_SIMULATE)
	LDU_LvdParamUnify(pDevice);//add 2010/3/17

	LDU_AcdParamUnify(pDevice);
	LDU_DcdParamUnify(pDevice);
	LDU_BattParamUnify(pDevice);

	//#endif
	return;
}


#define	SIG_CODE_AC_FAULT_LIGHT_CTL		0x4A04
#define	SIG_CODE_LVD1_CTL				0x4A05
#define	SIG_CODE_LVD2_CTL				0x4A06

#define	SUB_CHN_NO_FAULT_LIGHT			1
#define	SUB_CHN_NO_LVD1_CTL				1
#define	SUB_CHN_NO_LVD2_CTL				2

/*==========================================================================*
* FUNCTION : LDU_SendCtlCmd
* PURPOSE  : Interface Function to send a command
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*  pDevice  : 
*            int    iChn     : 
*            float  fParam   : 
*            char*  strParam : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:39
*==========================================================================*/
void LDU_SendCtlCmd(void* pDevice, int iChn, float fParam, char* strParam)
{
	RS485_DEVICE_CLASS*		pLdu = pDevice;

	int						i;
	int						iLvdEquipNo;
	int						iAcdNo;
	int						iSubChnNo;
	INT32					iBattNum;
	INT32					iDcdEquipNo;

	RS485_VALUE				value;
	BYTE					byAddr;
	RS485_VALUE				valueAddr;
	LDU_SEND_CMD			SendCmd;
	BYTE*					pbyRcvBuf;
	int						iReadLen = 0;

	UNUSED(strParam);

	extern 	RS485_SAMPLER_DATA	g_RS485Data;

	if (RS485_RUN_MODE_SLAVE == g_RS485Data.enumRunningMode)
	{
		return;
	}


	LduReopenPort(pLdu->pCommPort);

	if((iChn > 8050) && (iChn < (8050 + 80 * 5)))
	{
		iAcdNo = (iChn - 8050) / 80;
		iSubChnNo = (iChn - 8050) % 80;
		if(iSubChnNo == SUB_CHN_NO_FAULT_LIGHT)
		{
			valueAddr = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, iAcdNo, LDU_ACD_ADDR);
			SendCmd.byAddr = valueAddr.dwValue;
			SendCmd.byCmdId = LDU_CMD_ID_CONTROL_CMD;
			SendCmd.wLduSigId = SIG_CODE_AC_FAULT_LIGHT_CTL;
			SendCmd.iValueType = LDU_VALUE_TYPE_BYTE;
			SendCmd.fValue = fParam;
			LduPackSendCmd(pLdu, &SendCmd);
			LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen);
		}
		return;
	}

	if((iChn > 10000) && (iChn < (10000 + 10 * 10)))
	{
		iLvdEquipNo = (iChn - 10000) / 10;
		iSubChnNo = (iChn - 10000) % 10;
		switch(iSubChnNo)
		{
		case SUB_CHN_NO_LVD1_CTL:
			//LVD equip No equals to DCD equip No
			valueAddr = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, iLvdEquipNo, LDU_DCD_ADDR);
			SendCmd.byAddr = valueAddr.dwValue;
			SendCmd.byCmdId = LDU_CMD_ID_CONTROL_CMD;
			SendCmd.wLduSigId = SIG_CODE_LVD1_CTL;
			SendCmd.iValueType = LDU_VALUE_TYPE_BYTE;
			SendCmd.fValue = fParam;
			LduPackSendCmd(pLdu, &SendCmd);
			LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen);
			break;

		case SUB_CHN_NO_LVD2_CTL:
			//LVD equip No equals to DCD equip No
			valueAddr = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, iLvdEquipNo, LDU_DCD_ADDR);
			SendCmd.byAddr = valueAddr.dwValue;
			SendCmd.byCmdId = LDU_CMD_ID_CONTROL_CMD;
			SendCmd.wLduSigId = SIG_CODE_LVD2_CTL;
			SendCmd.iValueType = LDU_VALUE_TYPE_BYTE;
			SendCmd.fValue = fParam;
			LduPackSendCmd(pLdu, &SendCmd);
			LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen);
			break;

		default: 
			break;
		}
	}

	//Group LVD CTRL
	if (10881 == iChn || 10882 == iChn)
	{
		iSubChnNo = iChn - 10880;

		//Every equipment ctrl!!
		for(i = 0; i < LDU_MAX_DCD_NUM; i++)
		{
			value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_ADDR);
			if(value.iValue == RS485_SAMP_INVALID_VALUE)
			{
				break;
			}
			else
			{
				byAddr = value.dwValue;
			}

			value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_COMM_BREAK_TIMES);
			if(value.iValue >= LDU_MAX_COMM_BREAK_TIMES)
			{
				continue;
			}

			switch(iSubChnNo)
			{
			case SUB_CHN_NO_LVD1_CTL:
				//LVD equip No equals to DCD equip No
				//valueAddr = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_ADDR);
				//SendCmd.byAddr = valueAddr.dwValue;

				SendCmd.byAddr	= byAddr;
				SendCmd.byCmdId = LDU_CMD_ID_CONTROL_CMD;
				SendCmd.wLduSigId = SIG_CODE_LVD1_CTL;
				SendCmd.iValueType = LDU_VALUE_TYPE_BYTE;
				SendCmd.fValue = fParam;
				LduPackSendCmd(pLdu, &SendCmd);
				LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen);
				//printf("\n	:::::::::::::::LVD1 CTL::::::::::	\n");

				break;

			case SUB_CHN_NO_LVD2_CTL:
				//LVD equip No equals to DCD equip No
				//valueAddr = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_ADDR);
				//SendCmd.byAddr = valueAddr.dwValue;
				iDcdEquipNo = LduGetDcdNo(byAddr);
				value  = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, iDcdEquipNo, LDU_DCD_BATT_NUM);
				iBattNum = value.iValue;

				if (0 == iBattNum)
				{
					/*
						后加上的3/31日
						因为如果当设置某个屏的LVD数目为0个时，将只采集LVD1的回采信号。
						所以尽管LVD2控制了，也报不出来。所以LARGEDU的设计意图即当电池串
						为0时，只可以控制LVD1
					*/
					break;
				}

				SendCmd.byAddr	= byAddr;
				SendCmd.byCmdId = LDU_CMD_ID_CONTROL_CMD;
				SendCmd.wLduSigId = SIG_CODE_LVD2_CTL;
				SendCmd.iValueType = LDU_VALUE_TYPE_BYTE;
				SendCmd.fValue = fParam;
				LduPackSendCmd(pLdu, &SendCmd);
				LduReadOneFrame(pLdu, &pbyRcvBuf, &iReadLen);
				//printf("\n	:::::::::::::::LVD2 CTL::::::::::	\n");

				break;

			default: 
				break;
			}
		}
	}
	else
	{
		//Needn't process!!!!
	}

	return;
}
/*==========================================================================*
* FUNCTION : LduSetEquipNoExist()
* PURPOSE  : Set LargeDU Equipment not exist when there is Sm equipment.
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
*            
*            
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : add by IlockTeng               DATE: 2009-07-21 11:40
*==========================================================================*/
void LduSetEquipNoExist(void)
{
	RS485_VALUE		value;
	INT32 i,j;
	value.iValue = LDU_EQUIP_NOT_EXIST;
	LduSetRoughValue(LDU_EQUIP_TYPE_GROUP, 0, LDU_G_ACDG_WORK_STATUS, value);
	LduSetRoughValue(LDU_EQUIP_TYPE_GROUP, 0, LDU_G_DCDG_WORK_STATUS, value);

	for(i = 0; i < LDU_MAX_ACD_NUM; i++)
	{
		//for(j = 0; j < LDU_MAX_ACD_SIGNALS_NUM; j++)
		//{
		LduSetRoughValue(LDU_EQUIP_TYPE_ACD, i, LDU_ACD_EXISTS, value);	
		//}
	}

	for(i = 0; i < LDU_MAX_DCD_NUM; i++)
	{
		//for(j = 0; j < LDU_MAX_DCD_SIGNALS_NUM; j++)
		//{
		LduSetRoughValue(LDU_EQUIP_TYPE_DCD, i, LDU_DCD_EXISTS, value);	
		//}
	}

	for(i = 0; i < LDU_MAX_BATT_NUM; i++)
	{
		//for(j = 0; j < LDU_MAX_BATT_SIGNALS_NUM; j++)
		//{
		LduSetRoughValue(LDU_EQUIP_TYPE_BATT, i, LDU_BATT_EXISTS, value);	
		//}
	}

	for(i = 0; i < LDU_MAX_LVD_NUM; i++)
	{
		//for(j = 0; j < LDU_MAX_LVD_SIGNALS_NUM; j++)
		//{
		LduSetRoughValue(LDU_EQUIP_TYPE_LVD, i, LDU_LVD_EQUIP_EXISTS, value);	
		//}
	}
}

/*==========================================================================*
* FUNCTION : LduBatt NumChangeProc()
* PURPOSE  : LduBattNum ChangeProc.
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
*            
*            
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : add by IlockTeng               DATE: 2010-03-19 11:40
*==========================================================================*/
static void LduBattNumChangeProc(INT32* pBeChange)
{
	static BOOL		sBeFirstRunning = TRUE;
	static INT32	siRecordQTYNum	= 0;
	static	DWORD	s_dwChangeForWeb= 0;
	RS485_VALUE		value;

	value = LduGetRoughValue(LDU_EQUIP_TYPE_GROUP, 0, LDU_G_BATT_QTY);

	if(sBeFirstRunning)
	{
		sBeFirstRunning = FALSE;
		siRecordQTYNum	= value.iValue;
		*pBeChange = TRUE;
		return;
	}
	else
	{
		value = LduGetRoughValue(LDU_EQUIP_TYPE_GROUP, 0, LDU_G_BATT_QTY);

		if (siRecordQTYNum != value.iValue)
		{
			siRecordQTYNum = value.iValue;
			*pBeChange = TRUE;

			//for Web
			//s_dwChangeForWeb++;
			//if(s_dwChangeForWeb >= 255)
			//{
			//	s_dwChangeForWeb = 0;
			//}
			//printf("\n $$$$$$$$$ s_bDetectDIPChange == TRUE \n");
			//SetDwordSigValue(106,	//SMDU_GROUP_EQUIPID
			//				0,		//SIG_TYPE_SAMPLING
			//				2 + 1,	//CFG_CHANGED_SIG_ID
			//				s_dwChangeForWeb,
			//				"SMDURS485");
			////for	LCD
			//SetDwordSigValue(106,	//SMDU_GROUP_EQUIPID
			//				0,		//SIG_TYPE_SAMPLING
			//				2,		//CFG_CHANGED_SIG_ID
			//				1,
			//				"SMDURS485");
		}
		else
		{
			//Not process!!!
		}
	}
}

//*==========================================================================*
//* FUNCTION : LduBatt StringNumShowSeting
//* PURPOSE  : 当LARGEDU 的电池分流器数量设置为几个时，我们就显示几个电池串，
//*			 尽管可能现在电池串的数目设置多于电池分流器数目，也不会显示多余出
//*			 来的大于分流器数目的电池串。但是协议该怎么传怎么传，采集里该怎么
//*			 判断怎么判断，只是在WEB上不让其显示，也就是只将大于分流器数目的
//*			 电池串的存在信号屏蔽即可。
//* CALLS    : 
//* CALLED BY: 
//* ARGUMENTS: 
//*            
//*            
//* RETURN   : void : 
//* COMMENTS : 
//* CREATOR  :  add by IlockTeng               DATE: 2010-03-19 11:40
//*==========================================================================*/
//void LduBattStringNumShowSeting(ENUMSIGNALPROC EnumProc, LPVOID lpvoid)
//{
//	BYTE	byAddr;
//	INT32	iDcdEquipNo;
//	INT32	iShuntDownNumOfDCD;
//	INT32	iBattEquipNo;
//
//	for (byAddr = LDU_FST_DCD_ADDR; byAddr < LDU_FST_ACDCD_ADDR; byAddr++)
//	{
//		iDcdEquipNo = LduGetDcdNo(byAddr);
//		if(iDcdEquipNo < 0)
//		{
//			continue;
//		}
//		iBattEquipNo = LduGetBattNo(iDcdEquipNo);
//
//		iShuntDownNumOfDCD = GetDwordSigValue();
//	}
//
//	for (byAddr = LDU_FST_OTHER_DCD_ADDR; byAddr <= LDU_LST_ADDR; byAddr++)
//	{
//		iDcdEquipNo = LduGetDcdNo(byAddr);
//		if(iDcdEquipNo < 0)
//		{
//			continue;
//		}
//		iBattEquipNo = LduGetBattNo(iDcdEquipNo);
//	}
//}

/*==========================================================================*
* FUNCTION : LDU_StuffChn
* PURPOSE  : Interface Function to stuff all sampling channels
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: void*           pDevice  : 
*            ENUMSIGNALPROC  EnumProc : 
*            LPVOID          lpvoid   : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2009-07-21 11:40
*==========================================================================*/
void LDU_StuffChn(void* pDevice, ENUMSIGNALPROC EnumProc, LPVOID lpvoid)
{
	extern 	RS485_SAMPLER_DATA		g_RS485Data;
	RS485_DEVICE_CLASS*		pSmacDeviceClass	= g_RS485Data.pDeviceClass + DEVICE_CLASS_SMAC;
	RS485_DEVICE_CLASS*		pSmbatDeviceClass	= g_RS485Data.pDeviceClass + DEVICE_CLASS_SMBAT;
	RS485_DEVICE_CLASS*		pSmioDeviceClass	= g_RS485Data.pDeviceClass + DEVICE_CLASS_SMIO;
	RS485_DEVICE_CLASS*		pSmduDeviceClass	= g_RS485Data.pDeviceClass + DEVICE_CLASS_SMDU;
	static INT32 iBeEquipInfoChange = FALSE;	
	static	DWORD	s_dwChangeForWeb= 0;

	if(RS485_RUN_MODE_MASTER == g_RS485Data.enumRunningMode 
		|| RS485_RUN_MODE_STANDALONE == g_RS485Data.enumRunningMode)
	{
		//if there is SM equipment,must Largedu equipment not exist!!
		if((pSmacDeviceClass->pRoughData[SMAC_WORKING_STATUS].iValue == RS485_EQUIP_EXISTENT)
			|| (pSmbatDeviceClass->pRoughData[SMBAT_GROUP_WORKING_STAT].iValue == RS485_EQUIP_EXISTENT)
			|| (pSmioDeviceClass->pRoughData[SMIO_WORKING_GROUP_STATUS].iValue == RS485_EQUIP_EXISTENT))
		{
			LDU_Init(pDevice);
			LduSetEquipNoExist();
		}
		else if(pSmduDeviceClass->pRoughData != NULL)
		{
			if(pSmduDeviceClass->pRoughData[SMDU_GROUP_SM_STATE].iValue == RS485_EQUIP_EXISTENT)
			{
				LDU_Init(pDevice);
				LduSetEquipNoExist();
			}
		}
	}

	//LduSetEquipNoExist();

	static LDU_CHN_ROUGH_IDX		s_aGroupSig[] =
	{
		{LDU_S_CH_G_ACD_QTY,		LDU_G_ACD_QTY,			},
		{LDU_S_CH_G_AC_FAIL,		LDU_G_AC_FAIL,			},
		{LDU_S_CH_G_DCD_QTY,		LDU_G_DCD_QTY,			},	
		{LDU_S_CH_G_BATT_QTY,		LDU_G_BATT_QTY,			},
		{LDU_S_CH_LVD_EQUIP_QTY,	LDU_G_LVD_QTY,			},

		//add 2010/3/19
		{LDU_S_CH_G_DCD_LOAD_CURRENT,		LDU_G_DCD_LOAD_CURRENT},
		{LDU_S_CH_G_DCD_TOTAL_BATT_CURRENT,	LDU_G_DCD_TOTAL_BATT_CURRENT},
		{LDU_S_CH_G_DCD_AVERAGE_VOLT,		LDU_G_DCD_AVERAGE_VOLT},

		{LDU_S_CH_ACDG_WORK_STATUS,	LDU_G_ACDG_WORK_STATUS,},
		{LDU_S_CH_DCDG_WORK_STATUS,	LDU_G_DCDG_WORK_STATUS,},

		{LDU_CH_END_FLAG,			LDU_CH_END_FLAG,		},
	};

	LDU_CHN_ROUGH_IDX		s_aAcSig[] =
	{
		{LDU_S_CH_MAINS_1_UA,		LDU_MAINS_1_UA,			},
		{LDU_S_CH_MAINS_1_UB,		LDU_MAINS_1_UB,			},
		{LDU_S_CH_MAINS_1_UC,		LDU_MAINS_1_UC,			},
		{LDU_S_CH_MAINS_2_UA,		LDU_MAINS_2_UA,			},
		{LDU_S_CH_MAINS_2_UB,		LDU_MAINS_2_UB,			},
		{LDU_S_CH_MAINS_2_UC,		LDU_MAINS_2_UC,			},
		{LDU_S_CH_MAINS_3_UA,		LDU_MAINS_3_UA,			},
		{LDU_S_CH_MAINS_3_UB,		LDU_MAINS_3_UB,			},
		{LDU_S_CH_MAINS_3_UC,		LDU_MAINS_3_UC,			},
		{LDU_S_CH_PA_CURRENT,		LDU_PA_CURRENT,			},
		{LDU_S_CH_PB_CURRENT,		LDU_PB_CURRENT,			},
		{LDU_S_CH_PC_CURRENT,		LDU_PC_CURRENT,			},
		{LDU_S_CH_AC_FREQ,			LDU_AC_FREQ,			},
		{LDU_S_CH_INPUT_SW_MODE,	LDU_INPUT_SW_MODE,		},
		{LDU_S_CH_FAULT_LIGHT,		LDU_FAULT_LIGHT,		},
		{LDU_S_CH_MAINS_INPUT_NO,	LDU_MAINS_INPUT_NO,		},
		{LDU_S_CH_AC_OUTPUT1_ST,	LDU_AC_OUTPUT1_ST,		},
		{LDU_S_CH_AC_OUTPUT2_ST,	LDU_AC_OUTPUT2_ST,		},
		{LDU_S_CH_AC_OUTPUT3_ST,	LDU_AC_OUTPUT3_ST,		},
		{LDU_S_CH_AC_OUTPUT4_ST,	LDU_AC_OUTPUT4_ST,		},
		{LDU_S_CH_AC_OUTPUT5_ST,	LDU_AC_OUTPUT5_ST,		},
		{LDU_S_CH_AC_OUTPUT6_ST,	LDU_AC_OUTPUT6_ST,		},
		{LDU_S_CH_AC_OUTPUT7_ST,	LDU_AC_OUTPUT7_ST,		},
		{LDU_S_CH_AC_OUTPUT8_ST,	LDU_AC_OUTPUT8_ST,		},
		{LDU_S_CH_OVER_FREQ_ST,		LDU_OVER_FREQ_ST,		},
		{LDU_S_CH_UNDER_FREQ_ST,	LDU_UNDER_FREQ_ST,		},
		{LDU_S_CH_INPUT_MCCB_ST,	LDU_INPUT_MCCB_ST,		},
		{LDU_S_CH_SPD_TRIP_ST,		LDU_SPD_TRIP_ST,		},
		{LDU_S_CH_OUTPUT_MCCB_ST,	LDU_OUTPUT_MCCB_ST,		},
		{LDU_S_CH_MAINS1_FAIL_ST,	LDU_MAINS1_FAIL_ST,		},
		{LDU_S_CH_MAINS2_FAIL_ST,	LDU_MAINS2_FAIL_ST,		},
		{LDU_S_CH_MAINS3_FAIL_ST,	LDU_MAINS3_FAIL_ST,		},
		{LDU_S_CH_M1_UA_UNDER_VOLT,	LDU_M1_UA_UNDER_VOLT,	},
		{LDU_S_CH_M1_UB_UNDER_VOLT,	LDU_M1_UB_UNDER_VOLT,	},
		{LDU_S_CH_M1_UC_UNDER_VOLT,	LDU_M1_UC_UNDER_VOLT,	},
		{LDU_S_CH_M2_UA_UNDER_VOLT,	LDU_M2_UA_UNDER_VOLT,	},
		{LDU_S_CH_M2_UB_UNDER_VOLT,	LDU_M2_UB_UNDER_VOLT,	},
		{LDU_S_CH_M2_UC_UNDER_VOLT,	LDU_M2_UC_UNDER_VOLT,	},
		{LDU_S_CH_M3_UA_UNDER_VOLT,	LDU_M3_UA_UNDER_VOLT,	},
		{LDU_S_CH_M3_UB_UNDER_VOLT,	LDU_M3_UB_UNDER_VOLT,	},
		{LDU_S_CH_M3_UC_UNDER_VOLT,	LDU_M3_UC_UNDER_VOLT,	},
		{LDU_S_CH_M1_UA_OVER_VOLT,	LDU_M1_UA_OVER_VOLT,	},
		{LDU_S_CH_M1_UB_OVER_VOLT,	LDU_M1_UB_OVER_VOLT,	},
		{LDU_S_CH_M1_UC_OVER_VOLT,	LDU_M1_UC_OVER_VOLT,	},
		{LDU_S_CH_M2_UA_OVER_VOLT,	LDU_M2_UA_OVER_VOLT,	},
		{LDU_S_CH_M2_UB_OVER_VOLT,	LDU_M2_UB_OVER_VOLT,	},
		{LDU_S_CH_M2_UC_OVER_VOLT,	LDU_M2_UC_OVER_VOLT,	},
		{LDU_S_CH_M3_UA_OVER_VOLT,	LDU_M3_UA_OVER_VOLT,	},
		{LDU_S_CH_M3_UB_OVER_VOLT,	LDU_M3_UB_OVER_VOLT,	},
		{LDU_S_CH_M3_UC_OVER_VOLT,	LDU_M3_UC_OVER_VOLT,	},
		{LDU_S_CH_M1_PHASE_A_FAIL,	LDU_M1_PHASE_A_FAIL,	},
		{LDU_S_CH_M1_PHASE_B_FAIL,	LDU_M1_PHASE_B_FAIL,	},
		{LDU_S_CH_M1_PHASE_C_FAIL,	LDU_M1_PHASE_C_FAIL,	},
		{LDU_S_CH_M2_PHASE_A_FAIL,	LDU_M2_PHASE_A_FAIL,	},
		{LDU_S_CH_M2_PHASE_B_FAIL,	LDU_M2_PHASE_B_FAIL,	},
		{LDU_S_CH_M2_PHASE_C_FAIL,	LDU_M2_PHASE_C_FAIL,	},
		{LDU_S_CH_M3_PHASE_A_FAIL,	LDU_M3_PHASE_A_FAIL,	},
		{LDU_S_CH_M3_PHASE_B_FAIL,	LDU_M3_PHASE_B_FAIL,	},
		{LDU_S_CH_M3_PHASE_C_FAIL,	LDU_M3_PHASE_C_FAIL,	},
		{LDU_S_CH_ACD_NO_RESPONSE,	LDU_ACD_NO_RESPONSE,	},
		{LDU_S_CH_ACD_EXISTS,		LDU_ACD_EXISTS,			},
		{LDU_S_CH_M1_UA_VOLT_ALM,	LDU_M1_UA_VOLT_ALM,		},
		{LDU_S_CH_M1_UB_VOLT_ALM,	LDU_M1_UB_VOLT_ALM,		},
		{LDU_S_CH_M1_UC_VOLT_ALM,	LDU_M1_UC_VOLT_ALM,		},
		{LDU_S_CH_M2_UA_VOLT_ALM,	LDU_M2_UA_VOLT_ALM,		},
		{LDU_S_CH_M2_UB_VOLT_ALM,	LDU_M2_UB_VOLT_ALM,		},
		{LDU_S_CH_M2_UC_VOLT_ALM,	LDU_M2_UC_VOLT_ALM,		},
		{LDU_S_CH_M3_UA_VOLT_ALM,	LDU_M3_UA_VOLT_ALM,		},
		{LDU_S_CH_M3_UB_VOLT_ALM,	LDU_M3_UB_VOLT_ALM,		},
		{LDU_S_CH_M3_UC_VOLT_ALM,	LDU_M3_UC_VOLT_ALM,		},
		{LDU_S_CH_ACD_ADDR,			LDU_ACD_ADDR,			},

		{LDU_CH_END_FLAG,			LDU_CH_END_FLAG,		},

	};

	LDU_CHN_ROUGH_IDX		s_aDcSig[] =
	{
		{LDU_S_CH_TEMPERATURE1,		LDU_TEMPERATURE1,		},
		{LDU_S_CH_TEMPERATURE2,		LDU_TEMPERATURE2,		},
		{LDU_S_CH_TEMPERATURE3,		LDU_TEMPERATURE3,		},
		{LDU_S_CH_BUS_VOLT,			LDU_BUS_VOLT,			},
		{LDU_S_CH_LOAD_CURR,		LDU_LOAD_CURR,			},
		{LDU_S_CH_BRANCH1_CURR,		LDU_BRANCH1_CURR,		},
		{LDU_S_CH_BRANCH2_CURR,		LDU_BRANCH2_CURR,		},
		{LDU_S_CH_BRANCH3_CURR,		LDU_BRANCH3_CURR,		},
		{LDU_S_CH_BRANCH4_CURR,		LDU_BRANCH4_CURR,		},
		{LDU_S_CH_BRANCH5_CURR,		LDU_BRANCH5_CURR,		},
		{LDU_S_CH_BRANCH6_CURR,		LDU_BRANCH6_CURR,		},
		{LDU_S_CH_BRANCH7_CURR,		LDU_BRANCH7_CURR,		},
		{LDU_S_CH_BRANCH8_CURR,		LDU_BRANCH8_CURR,		},
		{LDU_S_CH_BRANCH9_CURR,		LDU_BRANCH9_CURR,		},
		{LDU_S_CH_DC_OVER_VOLT,		LDU_DC_OVER_VOLT,		},
		{LDU_S_CH_DC_UNDER_VOLT,	LDU_DC_UNDER_VOLT,		},
		{LDU_S_CH_OVER_TEMP1,		LDU_OVER_TEMP1,			},
		{LDU_S_CH_OVER_TEMP2,		LDU_OVER_TEMP2,			},
		{LDU_S_CH_OVER_TEMP3,		LDU_OVER_TEMP3,			},
		{LDU_S_CH_OUTPUT1_DISCON,	LDU_OUTPUT1_DISCON,		},
		{LDU_S_CH_OUTPUT2_DISCON,	LDU_OUTPUT2_DISCON,		},
		{LDU_S_CH_OUTPUT3_DISCON,	LDU_OUTPUT3_DISCON,		},
		{LDU_S_CH_OUTPUT4_DISCON,	LDU_OUTPUT4_DISCON,		},
		{LDU_S_CH_OUTPUT5_DISCON,	LDU_OUTPUT5_DISCON,		},
		{LDU_S_CH_OUTPUT6_DISCON,	LDU_OUTPUT6_DISCON,		},
		{LDU_S_CH_OUTPUT7_DISCON,	LDU_OUTPUT7_DISCON,		},
		{LDU_S_CH_OUTPUT8_DISCON,	LDU_OUTPUT8_DISCON,		},
		{LDU_S_CH_OUTPUT9_DISCON,	LDU_OUTPUT9_DISCON,		},
		{LDU_S_CH_OUTPUT10_DISCON,	LDU_OUTPUT10_DISCON,	},
		{LDU_S_CH_OUTPUT11_DISCON,	LDU_OUTPUT11_DISCON,	},
		{LDU_S_CH_OUTPUT12_DISCON,	LDU_OUTPUT12_DISCON,	},
		{LDU_S_CH_OUTPUT13_DISCON,	LDU_OUTPUT13_DISCON,	},
		{LDU_S_CH_OUTPUT14_DISCON,	LDU_OUTPUT14_DISCON,	},
		{LDU_S_CH_OUTPUT15_DISCON,	LDU_OUTPUT15_DISCON,	},
		{LDU_S_CH_OUTPUT16_DISCON,	LDU_OUTPUT16_DISCON,	},
		{LDU_S_CH_OUTPUT17_DISCON,	LDU_OUTPUT17_DISCON,	},
		{LDU_S_CH_OUTPUT18_DISCON,	LDU_OUTPUT18_DISCON,	},
		{LDU_S_CH_OUTPUT19_DISCON,	LDU_OUTPUT19_DISCON,	},
		{LDU_S_CH_OUTPUT20_DISCON,	LDU_OUTPUT20_DISCON,	},
		{LDU_S_CH_OUTPUT21_DISCON,	LDU_OUTPUT21_DISCON,	},
		{LDU_S_CH_OUTPUT22_DISCON,	LDU_OUTPUT22_DISCON,	},
		{LDU_S_CH_OUTPUT23_DISCON,	LDU_OUTPUT23_DISCON,	},
		{LDU_S_CH_OUTPUT24_DISCON,	LDU_OUTPUT24_DISCON,	},
		{LDU_S_CH_OUTPUT25_DISCON,	LDU_OUTPUT25_DISCON,	},
		{LDU_S_CH_OUTPUT26_DISCON,	LDU_OUTPUT26_DISCON,	},
		{LDU_S_CH_OUTPUT27_DISCON,	LDU_OUTPUT27_DISCON,	},
		{LDU_S_CH_OUTPUT28_DISCON,	LDU_OUTPUT28_DISCON,	},
		{LDU_S_CH_OUTPUT29_DISCON,	LDU_OUTPUT29_DISCON,	},
		{LDU_S_CH_OUTPUT30_DISCON,	LDU_OUTPUT30_DISCON,	},
		{LDU_S_CH_OUTPUT31_DISCON,	LDU_OUTPUT31_DISCON,	},
		{LDU_S_CH_OUTPUT32_DISCON,	LDU_OUTPUT32_DISCON,	},
		{LDU_S_CH_OUTPUT33_DISCON,	LDU_OUTPUT33_DISCON,	},
		{LDU_S_CH_OUTPUT34_DISCON,	LDU_OUTPUT34_DISCON,	},
		{LDU_S_CH_OUTPUT35_DISCON,	LDU_OUTPUT35_DISCON,	},
		{LDU_S_CH_OUTPUT36_DISCON,	LDU_OUTPUT36_DISCON,	},
		{LDU_S_CH_OUTPUT37_DISCON,	LDU_OUTPUT37_DISCON,	},
		{LDU_S_CH_OUTPUT38_DISCON,	LDU_OUTPUT38_DISCON,	},
		{LDU_S_CH_OUTPUT39_DISCON,	LDU_OUTPUT39_DISCON,	},
		{LDU_S_CH_OUTPUT40_DISCON,	LDU_OUTPUT40_DISCON,	},
		{LDU_S_CH_OUTPUT41_DISCON,	LDU_OUTPUT41_DISCON,	},
		{LDU_S_CH_OUTPUT42_DISCON,	LDU_OUTPUT42_DISCON,	},
		{LDU_S_CH_OUTPUT43_DISCON,	LDU_OUTPUT43_DISCON,	},
		{LDU_S_CH_OUTPUT44_DISCON,	LDU_OUTPUT44_DISCON,	},
		{LDU_S_CH_OUTPUT45_DISCON,	LDU_OUTPUT45_DISCON,	},
		{LDU_S_CH_OUTPUT46_DISCON,	LDU_OUTPUT46_DISCON,	},
		{LDU_S_CH_OUTPUT47_DISCON,	LDU_OUTPUT47_DISCON,	},
		{LDU_S_CH_OUTPUT48_DISCON,	LDU_OUTPUT48_DISCON,	},
		{LDU_S_CH_OUTPUT49_DISCON,	LDU_OUTPUT49_DISCON,	},
		{LDU_S_CH_OUTPUT50_DISCON,	LDU_OUTPUT50_DISCON,	},
		{LDU_S_CH_OUTPUT51_DISCON,	LDU_OUTPUT51_DISCON,	},
		{LDU_S_CH_OUTPUT52_DISCON,	LDU_OUTPUT52_DISCON,	},
		{LDU_S_CH_OUTPUT53_DISCON,	LDU_OUTPUT53_DISCON,	},
		{LDU_S_CH_OUTPUT54_DISCON,	LDU_OUTPUT54_DISCON,	},
		{LDU_S_CH_OUTPUT55_DISCON,	LDU_OUTPUT55_DISCON,	},
		{LDU_S_CH_OUTPUT56_DISCON,	LDU_OUTPUT56_DISCON,	},
		{LDU_S_CH_OUTPUT57_DISCON,	LDU_OUTPUT57_DISCON,	},
		{LDU_S_CH_OUTPUT58_DISCON,	LDU_OUTPUT58_DISCON,	},
		{LDU_S_CH_OUTPUT59_DISCON,	LDU_OUTPUT59_DISCON,	},
		{LDU_S_CH_OUTPUT60_DISCON,	LDU_OUTPUT60_DISCON,	},
		{LDU_S_CH_OUTPUT61_DISCON,	LDU_OUTPUT61_DISCON,	},
		{LDU_S_CH_OUTPUT62_DISCON,	LDU_OUTPUT62_DISCON,	},
		{LDU_S_CH_OUTPUT63_DISCON,	LDU_OUTPUT63_DISCON,	},
		{LDU_S_CH_OUTPUT64_DISCON,	LDU_OUTPUT64_DISCON,	},
		{LDU_S_CH_DCD_NO_RESPONSE,	LDU_DCD_NO_RESPONSE,	},
		{LDU_S_CH_DCD_EXISTS,		LDU_DCD_EXISTS,			},
		{LDU_S_CH_DCD_ADDR,			LDU_DCD_ADDR,			},
		{LDU_S_CH_DC_SPD,			LDU_DCD_DC_SPD,			},

		{LDU_CH_END_FLAG,			LDU_CH_END_FLAG,		},
	};

	LDU_CHN_ROUGH_IDX		s_aBattSig[] =
	{
		{LDU_S_CH_BATT_VOLT,		LDU_BATT_VOLT,			},
		{LDU_S_CH_BATT_CURR,		LDU_BATT_CURR,			},
		{LDU_S_CH_BATT_OVER_VOLT,	LDU_BATT_OVER_VOLT,		},
		{LDU_S_CH_BATT_UNDER_VOLT,	LDU_BATT_UNDER_VOLT,	},
		{LDU_S_CH_BATT_OVER_CURR,	LDU_BATT_OVER_CURR,		},
		{LDU_S_CH_BATT_FUSE_BREAK,	LDU_BATT_FUSE_BREAK,	},
		{LDU_S_CH_DCD_NO,			LDU_BATT_DCD_NO,		},
		{LDU_S_CH_BATT_EXISTS,		LDU_BATT_EXISTS,		},
		{LDU_S_CH_BATT_CURR_COEF,	LDU_BATT_CURR_COEF,		},

		{LDU_S_CH_BATT_COMM_STS,	LDU_BATT_COMM_STS		},//3/5		添加电池的通信状态

		{LDU_CH_END_FLAG,			LDU_CH_END_FLAG,		},
	};


	LDU_CHN_ROUGH_IDX		s_aLvdSig[] =
	{
		{LDU_S_CH_LVD1_STATE,		LDU_LVD1_STATE,			},
		{LDU_S_CH_LVD2_STATE,		LDU_LVD2_STATE,			},
		{LDU_S_CH_LVD3_STATE,		LDU_LVD3_STATE,			},
		{LDU_S_CH_LVD_EQUIP_EXISTS,	LDU_LVD_EQUIP_EXISTS,	},
		{LDU_S_CH_LVD_DCD_NO,		LDU_LVD_DCD_NO,			},			
		{LDU_S_CH_LVD_COMM_STAT,	LDU_COMM_STAT			},
		{LDU_CH_END_FLAG,			LDU_CH_END_FLAG,		},
	};

	int				i, j;
	RS485_VALUE		value, valueNum;;
	UNUSED(pDevice);
	/***************Stuff Group**********************************/
	i = 0;
	while(s_aGroupSig[i].iChnNo != LDU_CH_END_FLAG)
	{	 
		value = LduGetRoughValue(LDU_EQUIP_TYPE_GROUP, 0, s_aGroupSig[i].iRoughIdx);
		EnumProc(s_aGroupSig[i].iChnNo, 
			value.fValue, 
			lpvoid);
		i++;
	}	 
	/************************************************************/

	/***************Stuff AC************************************/

	for(j = 0; j < LDU_MAX_ACD_NUM; j++)
	{
		i = 0;
		while(s_aAcSig[i].iChnNo != LDU_CH_END_FLAG)
		{	 
			value = LduGetRoughValue(LDU_EQUIP_TYPE_ACD, j, s_aAcSig[i].iRoughIdx);
			EnumProc(s_aAcSig[i].iChnNo + j * LDU_MAX_CHN_NUM_EVERY_ACD, 
				value.fValue, 
				lpvoid);

			if(LDU_ACD_EXISTS == s_aAcSig[i].iRoughIdx)
			{
				if (RS485_SAMP_INVALID_VALUE == value.iValue)
				{
					value.iValue = LDU_EQUIP_NOT_EXIST;
					EnumProc(s_aAcSig[i].iChnNo + j * LDU_MAX_CHN_NUM_EVERY_ACD, 
								value.fValue, 
								lpvoid);
				}
				//printf(" ########## this is stuff ACD STAT F=%f D=%d ##\n",value.fValue,value.iValue);
			}

			i++;
		}	
	}

	/********************************************************/


	/***************Stuff DC************************************/
	for(j = 0; j < LDU_MAX_DCD_NUM; j++)
	{
		i = 0;
		while(s_aDcSig[i].iChnNo != LDU_CH_END_FLAG)
		{	 
			value = LduGetRoughValue(LDU_EQUIP_TYPE_DCD, j, s_aDcSig[i].iRoughIdx);
			EnumProc(s_aDcSig[i].iChnNo + j * LDU_MAX_CHN_NUM_EVERY_DCD, 
				value.fValue, 
				lpvoid);

			if(LDU_DCD_EXISTS == s_aDcSig[i].iRoughIdx)
			{
				if (RS485_SAMP_INVALID_VALUE == value.iValue)
				{
					value.iValue = LDU_EQUIP_NOT_EXIST;
					EnumProc(s_aDcSig[i].iChnNo + j * LDU_MAX_CHN_NUM_EVERY_DCD, 
									value.fValue, 
									lpvoid);
				}
				//printf(" ########## this is stuff DCD STAT F=%f D=%d ##\n",value.fValue,value.iValue);
			}

			i++;
		}	
	}



	/***********************************************************/


	/***************Stuff BATT************************************/

	//for(j = 0; j < LDU_MAX_BATT_NUM; j++)LDU_ACTUAL_MAX_BATT_NUM
	for(j = 0; j < LDU_ACTUAL_MAX_BATT_NUM; j++)
	{
		i = 0;
		while(s_aBattSig[i].iChnNo != LDU_CH_END_FLAG)
		{	 
			value = LduGetRoughValue(LDU_EQUIP_TYPE_BATT, j, s_aBattSig[i].iRoughIdx);
			EnumProc(s_aBattSig[i].iChnNo + j * LDU_MAX_CHN_NUM_EVERY_BATT, 
				value.fValue, 
				lpvoid);

			if(LDU_BATT_EXISTS == s_aBattSig[i].iRoughIdx)
			{
				if (RS485_SAMP_INVALID_VALUE == value.iValue)
				{
					value.iValue = LDU_EQUIP_NOT_EXIST;
					EnumProc(s_aBattSig[i].iChnNo + j * LDU_MAX_CHN_NUM_EVERY_BATT, 
						value.fValue, 
						lpvoid);
				}
				//printf(" ########## this is stuff BATT STAT F=%f D=%d ##\n",value.fValue,value.iValue);
			}

			i++;
		}	
		
	}

	/***********************************************************/


	/***************Stuff LVD************************************/
	//LVD Equipment Unit
	for(j = 0; j < LDU_MAX_LVD_NUM; j++)
	{
		i = 0;
		while(s_aLvdSig[i].iChnNo != LDU_CH_END_FLAG)
		{	 
			value = LduGetRoughValue(LDU_EQUIP_TYPE_LVD, j, s_aLvdSig[i].iRoughIdx);
			EnumProc(s_aLvdSig[i].iChnNo + j * LDU_MAX_CHN_NUM_EVERY_LVD, 
				value.fValue, 
				lpvoid);

			if(LDU_LVD_EQUIP_EXISTS == s_aLvdSig[i].iRoughIdx)
			{
				if (RS485_SAMP_INVALID_VALUE == value.iValue)
				{
					value.iValue = LDU_EQUIP_NOT_EXIST;
					EnumProc(s_aLvdSig[i].iChnNo + j * LDU_MAX_CHN_NUM_EVERY_LVD, 
							value.fValue, 
							lpvoid);
				}
				//printf(" ########## this is stuff LVD STAT F=%f D=%d ##\n",value.fValue,value.iValue);
			}

			i++;
		}	
	}

	if (iBeEquipInfoChange)
	{
		//for Web
		s_dwChangeForWeb++;
		if(s_dwChangeForWeb >= 255)
		{
			s_dwChangeForWeb = 0;
		}

		//printf("\n $$$$$$$$$ s_bDetectDIPChange == TRUE \n");

		SetDwordSigValue(106,	//SMDU_GROUP_EQUIPID
						0,		//SIG_TYPE_SAMPLING
						2 + 1,	//CFG_CHANGED_SIG_ID
						s_dwChangeForWeb,
						"SMDURS485");
		//for	LCD
		SetDwordSigValue(106,	//SMDU_GROUP_EQUIPID
						0,		//SIG_TYPE_SAMPLING
						2,		//CFG_CHANGED_SIG_ID
						1,
						"SMDURS485");

		iBeEquipInfoChange = FALSE;
	}
	
	LduBattNumChangeProc(&iBeEquipInfoChange);

	//LduBattStringNumShowSeting();

	/***********************************************************/


	return;
}		 








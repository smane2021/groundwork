/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : 
 *
 *  FILENAME : 
 *  CREATOR  :                      DATE: 
 *  VERSION  : 
 *  PURPOSE  : 
 *				
 *  HISTORY  :	
 *				
 *==========================================================================*/
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
//#include <conio.h>
#include <math.h>
#include <time.h>

#include "rs485_main.h"
#include "rs485_comm.h"

#include "rs485_modbus.h"

#if 1
#define TRACE_MDB
#define TRACE_MDB2
#else
#define TRACE_MDB printf
#define TRACE_MDB2 printf
#define __PRINT_MDB_COMM_DATA
#endif

#define	MDB_SEND_BUF_LEN			16
#define	MDB_RECV_BUF_LEN			1024

/////////////////////////////////////////////////////
#define	MDBEXT_BUF_LEN				1024

#define BMS_GROUP_CHANNEL_OFFSET	40000

static BYTE sg_MdbExtSendBuffer[MDBEXT_BUF_LEN];
static BYTE sg_MdbExtRecvBuffer[MDBEXT_BUF_LEN];

static int MDBEXT_Init(IN void* pDevice);
static void MDBEXT_Exit(IN void* pDevice);
static BOOL MDBEXT_PortReady(IN RS485_COMM_PORT *pCommPort, IN int iGroup);
static int MDBEXT_Reconfig(IN void* pDevice);
static int MDBEXT_StuffChn(void* pDevice, ENUMSIGNALPROC EnumProc, LPVOID lpvoid);
static int MDBEXT_Sample(void* pDevice);
///////////////////////////////////////////////////

static BYTE g_MdbSendBuffer[MDB_SEND_BUF_LEN];
static BYTE g_MdbRecvBuffer[MDB_RECV_BUF_LEN];

#define MDB_CLEAR_SEND_BUF	(memset(g_MdbSendBuffer, 0, MDB_SEND_BUF_LEN))
#define MDB_CLEAR_RECV_BUF	(memset(g_MdbRecvBuffer, 0, MDB_RECV_BUF_LEN))


RS485_VALUE g_MdbGroupRoughData[MDB_GROUP_ROUGH_DATA_MAX];

static RS485_VALUE s_aMdbRoughData_ACMETER[ACMETER_NUM_MAX][ACMETER_ROUGH_DATA_MAX];

static MDB_INFO sg_stMdbInfo;

#define RS485_SAM_MDB_MODULE			"RS485_SAM_Modbus"

static int MDB_GetResponse(HANDLE hComm, BYTE *pBuf, int iToRead);
static BOOL MDB_SendCmd(HANDLE hComm, BYTE *pBuf, int nDataLen);

int MDB_GetManyData_DWORD(IN HANDLE hComm, IN BYTE nAddr, IN WORD wRegStart, IN WORD wDataSize, OUT DWORD *pwBuf);
int MDB_GetManyData(IN HANDLE hComm, IN BYTE nAddr, IN WORD wRegStart, IN WORD wDataSize, OUT WORD *pwBuf);
/*==========================================================================*
* FUNCTION : MDB_CRC
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/		 
static BYTE gabyCRCHi[] =
{
	0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x01,0xc0,
	0x80,0x41,0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,
	0x00,0xc1,0x81,0x40,0x00,0xc1,0x81,0x40,0x01,0xc0,
	0x80,0x41,0x01,0xc0,0x80,0x41,0x00,0xc1,0x81,0x40,
	0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x00,0xc1,
	0x81,0x40,0x01,0xc0,0x80,0x41,0x01,0xc0,0x80,0x41,
	0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x00,0xc1,
	0x81,0x40,0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,
	0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x01,0xc0,
	0x80,0x41,0x00,0xc1,0x81,0x40,0x00,0xc1,0x81,0x40,
	0x01,0xc0,0x80,0x41,0x01,0xc0,0x80,0x41,0x00,0xc1,
	0x81,0x40,0x01,0xc0,0x80,0x41,0x00,0xc1,0x81,0x40,
	0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x01,0xc0,
	0x80,0x41,0x00,0xc1,0x81,0x40,0x00,0xc1,0x81,0x40,
	0x01,0xc0,0x80,0x41,0x00,0xc1,0x81,0x40,0x01,0xc0,
	0x80,0x41,0x01,0xc0,0x80,0x41,0x00,0xc1,0x81,0x40,
	0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x01,0xc0,
	0x80,0x41,0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,
	0x00,0xc1,0x81,0x40,0x00,0xc1,0x81,0x40,0x01,0xc0,
	0x80,0x41,0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,
	0x01,0xc0,0x80,0x41,0x00,0xc1,0x81,0x40,0x01,0xc0,
	0x80,0x41,0x00,0xc1,0x81,0x40,0x00,0xc1,0x81,0x40,
	0x01,0xc0,0x80,0x41,0x01,0xc0,0x80,0x41,0x00,0xc1,
	0x81,0x40,0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,
	0x00,0xc1,0x81,0x40,0x01,0xc0,0x80,0x41,0x01,0xc0,
	0x80,0x41,0x00,0xc1,0x81,0x40
};

static BYTE gabyCRCLo[] =
{
	0x00,0xc0,0xc1,0x01,0xc3,0x03,0x02,0xc2,0xc6,0x06,
	0x07,0xc7,0x05,0xc5,0xc4,0x04,0xcc,0x0c,0x0d,0xcd,
	0x0f,0xcf,0xce,0x0e,0x0a,0xca,0xcb,0x0b,0xc9,0x09,
	0x08,0xc8,0xd8,0x18,0x19,0xd9,0x1b,0xdb,0xda,0x1a,
	0x1e,0xde,0xdf,0x1f,0xdd,0x1d,0x1c,0xdc,0x14,0xd4,
	0xd5,0x15,0xd7,0x17,0x16,0xd6,0xd2,0x12,0x13,0xd3,
	0x11,0xd1,0xd0,0x10,0xf0,0x30,0x31,0xf1,0x33,0xf3,
	0xf2,0x32,0x36,0xf6,0xf7,0x37,0xf5,0x35,0x34,0xf4,
	0x3c,0xfc,0xfd,0x3d,0xff,0x3f,0x3e,0xfe,0xfa,0x3a,
	0x3b,0xfb,0x39,0xf9,0xf8,0x38,0x28,0xe8,0xe9,0x29,
	0xeb,0x2b,0x2a,0xea,0xee,0x2e,0x2f,0xef,0x2d,0xed,
	0xec,0x2c,0xe4,0x24,0x25,0xe5,0x27,0xe7,0xe6,0x26,
	0x22,0xe2,0xe3,0x23,0xe1,0x21,0x20,0xe0,0xa0,0x60,
	0x61,0xa1,0x63,0xa3,0xa2,0x62,0x66,0xa6,0xa7,0x67,
	0xa5,0x65,0x64,0xa4,0x6c,0xac,0xad,0x6d,0xaf,0x6f,
	0x6e,0xae,0xaa,0x6a,0x6b,0xab,0x69,0xa9,0xa8,0x68,
	0x78,0xb8,0xb9,0x79,0xbb,0x7b,0x7a,0xba,0xbe,0x7e,
	0x7f,0xbf,0x7d,0xbd,0xbc,0x7c,0xb4,0x74,0x75,0xb5,
	0x77,0xb7,0xb6,0x76,0x72,0xb2,0xb3,0x73,0xb1,0x71,
	0x70,0xb0,0x50,0x90,0x91,0x51,0x93,0x53,0x52,0x92,
	0x96,0x56,0x57,0x97,0x55,0x95,0x94,0x54,0x9c,0x5c,
	0x5d,0x9d,0x5f,0x9f,0x9e,0x5e,0x5a,0x9a,0x9b,0x5b,
	0x99,0x59,0x58,0x98,0x88,0x48,0x49,0x89,0x4b,0x8b,
	0x8a,0x4a,0x4e,0x8e,0x8f,0x4f,0x8d,0x4d,0x4c,0x8c,
	0x44,0x84,0x85,0x45,0x87,0x47,0x46,0x86,0x82,0x42,
	0x43,0x83,0x41,0x81,0x80,0x40
};

static WORD	MDB_CRC(BYTE *pData, int len)
{
	BYTE i = (BYTE)len;
	BYTE  byCRCHi = 0xff;
	BYTE  byCRCLo = 0xff;
	BYTE  byIdx;
	WORD  crc;

	while(i--)
	{
		byIdx = byCRCHi ^ *pData++;
		byCRCHi = byCRCLo ^ gabyCRCHi[byIdx];
		byCRCLo = gabyCRCLo[byIdx];
	}

	crc = byCRCHi;
	crc <<= 8;
	crc += byCRCLo;

	return crc;
}


/*==========================================================================*
* FUNCTION : MDB_Init
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/		
void MDB_Init(void* pDevice)
{
    int	i;

    RS485_DEVICE_CLASS*		pMDB = pDevice;


    pMDB->pRoughData = g_MdbGroupRoughData;

    //pMDB->bNeedReconfig = TRUE;

    pMDB->pfnExit = MDB_Exit;
    pMDB->pfnReconfig = MDB_Reconfig;
    pMDB->pfnSample = MDB_Sample;
    pMDB->pfnStuffChn = MDB_StuffChn;
    pMDB->pfnParamUnify = MDB_ParamUnify;
    pMDB->pfnSendCmd = MDB_SendCtlCmd;	

    //Below assign invalid value to sampling signals first	
    for(i = 0; i < MDB_GROUP_ROUGH_DATA_MAX; i++)
    {
	g_MdbGroupRoughData[i].iValue = RS485_SAMP_INVALID_VALUE;
    }	

    ACMETER_Init(pDevice);

	if(MDB_ERROR_OK != MDBEXT_Init(pDevice))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, "Failed to call MDBEXT_Init\n");
	}


    //Special signals
    
    return;
}

/*==========================================================================*
* FUNCTION : MDB_Exit
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
void MDB_Exit(void* pDevice)
{
	RS485_DEVICE_CLASS*		pMdbDevice = pDevice;

	if(pMdbDevice->pCommPort->bOpened)
	{
		RS485_Close(pMdbDevice->pCommPort->hCommPort);
		pMdbDevice->pCommPort->bOpened = FALSE;
	}

	MDBEXT_Exit(pDevice);

	return;
}



/*==========================================================================*
* FUNCTION : MDB_PortReady
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
#define  MODBUS_MAX_TIMES_REOPEN	5
static BOOL MDB_PortReady(RS485_COMM_PORT *pCommPort)
{
	ASSERT(pCommPort);
	int	iOpenTimes;

	if(pCommPort->bOpened
		&& (pCommPort->enumAttr == RS485_ATTR_NONE)
		&& (pCommPort->enumBaud == RS485_ATTR_9600))
	{
		return TRUE;
	}

	if(pCommPort->bOpened)
	{
		RS485_Close(pCommPort->hCommPort);
		pCommPort->bOpened = FALSE;
	}

	iOpenTimes = 0;
	int iErrCode;
	while(iOpenTimes < MODBUS_MAX_TIMES_REOPEN)
	{
		pCommPort->hCommPort =
			RS485_Open("9600, n, 8, 1", RS485_READ_WRITE_TIMEOUT, &iErrCode);

		if(iErrCode == ERR_COMM_OK)
		{
			pCommPort->bOpened = TRUE;
			pCommPort->enumAttr = RS485_ATTR_NONE;
			pCommPort->enumBaud = RS485_ATTR_9600;
			
			TRACE_FILE_LINE_FUN_ENTER("Open RS485 port OK in MDB!");
			return TRUE;
		}
		else
		{
			Sleep(1000);
			TRACE_FILE_LINE_FUN_ENTER("Open RS485 port fail!");
			pCommPort->bOpened = FALSE;

			//return FALSE;
		}
		iOpenTimes++;
	}

	return TRUE;
}




/*==========================================================================*
* FUNCTION : MDB_Reconfig
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
void MDB_Reconfig(void* pDevice)
{
	RS485_DEVICE_CLASS*		pMDB = pDevice;


	MDB_Init(pDevice);
	MDB_PortReady(pMDB->pCommPort);

	//��ʼ��
	//RS485_InitSampCtrlList();
	
	//AC METER
	ACMETER_Reconfig(pDevice);

	MDBEXT_Reconfig(pDevice);
}

/*==========================================================================*
* FUNCTION : MDB_SampleManyData
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
static int MDB_SampleManyData(HANDLE hComm, REG_TO_ROUGH_MAP *pSampTab)
{
    int nSigNum = pSampTab->nSuccessiveNum;	

    RS485_VALUE	value;
    int i;

    int nResult = MDB_ERROR_OTHER;

    // Get data from comm port
    if((pSampTab->bySignalType == MDB_SIGNAL_TYPE_UINT32) 
	|| (pSampTab->bySignalType == MDB_SIGNAL_TYPE_INT32))
    {
	DWORD *pdwRegValues = NEW(DWORD, nSigNum);
	LOGOUT_NO_MEMORY(pdwRegValues);
	ZERO_POBJS(pdwRegValues, nSigNum);

	nResult = MDB_GetManyData_DWORD(hComm, pSampTab->byAddr, pSampTab->nStartRegAddr, nSigNum, pdwRegValues);
    	
	if(nResult != MDB_ERROR_OK)
	{
	    TRACE_ERROR_INT("MDB Sample Result = ", nResult);

	    DELETE_ITEM(pdwRegValues);
	    return nResult;
	}

	// Fill Rough Data	
	if(pSampTab->bySignalType == MDB_SIGNAL_TYPE_INT32)
	{
	    for(i = 0; i < nSigNum; i++)
	    {
		value.dwValue = *(pdwRegValues + i);
		(pSampTab->pRoughDataBuf + i)->iValue = value.iValue;
	    }
	}
	else  //MDB_SIGNAL_TYPE_UINT32
	{    			
	    for(i = 0; i < nSigNum; i++)
	    {
		value.iValue = (int)(*(pdwRegValues + i));
		*(pSampTab->pRoughDataBuf + i) = value;	
	    }	
	}

	DELETE_ITEM(pdwRegValues);
    }
    else	//WORD
    {
	WORD	*pwRegValues = NEW(WORD, nSigNum);
	LOGOUT_NO_MEMORY(pwRegValues);
	ZERO_POBJS(pwRegValues, nSigNum);

	nResult = MDB_GetManyData(hComm, pSampTab->byAddr, pSampTab->nStartRegAddr, nSigNum, pwRegValues);
    	
	if(nResult != MDB_ERROR_OK)
	{
	    TRACE_ERROR_INT("MDB Sample Result = ", nResult);

	    DELETE_ITEM(pwRegValues);
	    return nResult;
	}			

	// Fill Rough Data
	if(pSampTab->bySignalType == MDB_SIGNAL_TYPE_INT16)
	{
	    union unWORD_INT16
	    {		
		WORD	wValue;
		INT16	i16Value;
	    } unWord2Int16;

	    for(i = 0; i < nSigNum; i++)
	    {
		unWord2Int16.wValue = *(pwRegValues + i);
		(pSampTab->pRoughDataBuf + i)->iValue = unWord2Int16.i16Value;
	    }
	}
	else  //MDB_SIGNAL_TYPE_UINT16
	{	
	    for(i = 0; i < nSigNum; i++)
	    {
		value.iValue = (int)(*(pwRegValues + i));
		*(pSampTab->pRoughDataBuf + i) = value;	
	    }
	}

	DELETE_ITEM(pwRegValues);
    }

    return MDB_ERROR_OK;
}

/*==========================================================================*
* FUNCTION : MDB_StuffChn
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
void MDB_StuffChn(void* pDevice, ENUMSIGNALPROC EnumProc, LPVOID lpvoid)
{
	RS485_VALUE	 NumValue;
	
	NumValue.iValue = MDB_EQUIP_NOT_EXIST;

	MDBEXT_StuffChn(pDevice, EnumProc, lpvoid);

	//AC meter
	ACMETER_StuffChn(pDevice, EnumProc, lpvoid);
	
	//Group Signal		
	if(g_MdbGroupRoughData[MDB_GROUP_ACMETER_NUM].iValue > 0)
	{	
	    NumValue.iValue = MDB_EQUIP_EXIST;		    
	}
	EnumProc(S_CH_MDB_GROUP_EXIST, NumValue.fValue, lpvoid); 

	EnumProc(S_CH_MDB_GROUP_ACMETER_NUM, g_MdbGroupRoughData[MDB_GROUP_ACMETER_NUM].fValue, lpvoid);

}

/*==========================================================================*
* FUNCTION : MDB_ParamUnify
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
void MDB_ParamUnify(void* pDevice)
{
	RS485_DEVICE_CLASS *pMDB = pDevice;	
	
	if(g_MdbGroupRoughData[MDB_GROUP_ACMETER_NUM].iValue > 0)
	{
		MDB_PortReady(pMDB->pCommPort);
	}

	ACMETER_ParamUnify(pDevice);

	return;
}

/*==========================================================================*
* FUNCTION : MDB_Sample
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
void MDB_Sample(void* pDevice)
{
	RS485_DEVICE_CLASS *pMDB = pDevice;
	
	if(pMDB->bNeedReconfig)
	{		
		MDB_Reconfig(pDevice);	
		pMDB->bNeedReconfig = FALSE;
	}

	if(g_MdbGroupRoughData[MDB_GROUP_ACMETER_NUM].iValue > 0)
	{
		MDB_PortReady(pMDB->pCommPort);
	}

	ACMETER_Sample(pDevice);

	MDBEXT_Sample(pDevice);

}

/*==========================================================================*
* FUNCTION : MDB_SendCtlCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
void MDB_SendCtlCmd(void* pDevice, int iChn, float fParam, char* strParam)
{
	UNUSED(strParam);


	RS485_DEVICE_CLASS*		pMdb = pDevice;
	HANDLE	hComm = pMdb->pCommPort->hCommPort;



	if(g_MdbGroupRoughData[MDB_GROUP_ACMETER_NUM].iValue> 0)
	{
		MDB_PortReady(pMdb->pCommPort);
		
		ACMETER_SendCtlCmd(pDevice, iChn, fParam, strParam);
	}
	

	return;
}

/*==========================================================================*
* FUNCTION : MDB_SetOneData
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
int MDB_SetOneData(IN HANDLE hComm, IN BYTE nAddr, IN WORD wRegAddr, IN WORD wRegValue)
{

	MDB_CLEAR_SEND_BUF;
	BYTE *pBuf = g_MdbSendBuffer;
	BYTE *pSendBuf = pBuf;
	
	*pSendBuf++ = nAddr;
	*pSendBuf++ = MDB_FUN_CODE_SET_ONE_DATA;

	*pSendBuf++ = HIBYTE(wRegAddr);
	*pSendBuf++ = LOBYTE(wRegAddr);

	*pSendBuf++ = HIBYTE(wRegValue);
	*pSendBuf++ = LOBYTE(wRegValue);

	WORD wCRC = MDB_CRC(pBuf, 6);
	*pSendBuf++ = HIBYTE(wCRC);
	*pSendBuf++ = LOBYTE(wCRC);

	MDB_SendCmd(hComm, pBuf, 8);
	Sleep(500);

	// receive
	int nRecvBytes;

	MDB_CLEAR_RECV_BUF;
	BYTE *pRecvBuf = g_MdbRecvBuffer;
	MDB_GetResponse(hComm, pRecvBuf, MDB_RECV_BUF_LEN);

	// response of set command should be same with send command
	return (strncmp(pBuf, pRecvBuf, 8) == 0) ? MDB_ERROR_OK : MDB_ERROR_FAIL;

}

/*==========================================================================*
* FUNCTION : MDB_GetManyData
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
int	MDB_GetManyData(IN HANDLE hComm, IN BYTE nAddr, IN WORD wRegStart, IN WORD wDataSize, OUT WORD *pwBuf)
{

	MDB_CLEAR_SEND_BUF;
	BYTE *pBuf = g_MdbSendBuffer;
	BYTE *pSendBuf = pBuf;

	*pSendBuf++ = nAddr;
	
	*pSendBuf++ = MDB_FUN_CODE_GET_MANY_DATA;

	*pSendBuf++ = HIBYTE(wRegStart);
	*pSendBuf++ = LOBYTE(wRegStart);


	*pSendBuf++ = HIBYTE(wDataSize);
	*pSendBuf++ = LOBYTE(wDataSize);
	WORD wCRC = MDB_CRC(pBuf, 6);
	*pSendBuf++ = HIBYTE(wCRC);
	*pSendBuf++ = LOBYTE(wCRC);

		
	MDB_SendCmd(hComm, pBuf, 8);

	// receive

	MDB_CLEAR_RECV_BUF;
	BYTE *pRecvBuf = g_MdbRecvBuffer;
	int nRecvBytes = MDB_GetResponse(hComm, pRecvBuf, MDB_RECV_BUF_LEN);

	
	//for test
	//if(wRegStart == EBU_REG_BATT_STRING1_ERROR)
	/*if(wRegStart == EBU_REG_BATT_CURR)		
	{
		TRACE_MDB("\nAC Meter GetManyData recevied = %d: ",nRecvBytes );
		int j;
		for(j=0;j<nRecvBytes;j++)
			TRACE_MDB("%02X",*(pRecvBuf+j));
	}*/

	if(nRecvBytes < 7)
	{
		return MDB_ERROR_RESPOND_DATA_LEN;
	}

	// Check Address
	if(*pRecvBuf != nAddr)
	{
		return MDB_ERROR_ADDR;
	}

	
	

	// Check Function Code
	if(*(pRecvBuf + 1) != MDB_FUN_CODE_GET_MANY_DATA)
	{
		return MDB_ERROR_FUN_CODE;
	}

	// Check Data Length
	BYTE byDataLen = *(pRecvBuf + 2);

	if (nRecvBytes < byDataLen + 5)
	{
		return MDB_ERROR_RESPOND_DATA_LEN;
	}

	// Check CRC
	int iCrcStart = 3 + (int)byDataLen;
	WORD wReadCRC = MAKEWORD(*(pRecvBuf + iCrcStart + 1), *(pRecvBuf + iCrcStart));
	WORD wCalcCRC = MDB_CRC(pRecvBuf, byDataLen + 3);

	if(wReadCRC != wCalcCRC)
	{
		TRACE("CRC Error! Calc CRC = %04X\n", wCalcCRC);
		return MDB_ERROR_CRC;
	}

	if(byDataLen != (wDataSize*2))
	{
		return MDB_ERROR_RESPOND_DATA_LEN;
	}
	//������
	int i;
	BYTE *pData = pRecvBuf + 3;
	for(i = 0; i < (int)byDataLen / 2; i++)
	{
		*(pwBuf + i) = MAKEWORD(*(pData + i * 2 + 1), *(pData + i * 2));
	}


	return MDB_ERROR_OK;
}
/*==========================================================================*
* FUNCTION : MDB_GetManyData_DWORD
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
int	MDB_GetManyData_DWORD(IN HANDLE hComm, IN BYTE nAddr, IN WORD wRegStart, IN WORD wDataSize, OUT DWORD *pwBuf)
{
    RS485_FLOAT_STRING strUnionTemp;	

    MDB_CLEAR_SEND_BUF;
    BYTE *pBuf = g_MdbSendBuffer;
    BYTE *pSendBuf = pBuf;

    *pSendBuf++ = nAddr;

    *pSendBuf++ = MDB_FUN_CODE_GET_MANY_DATA;

    *pSendBuf++ = HIBYTE(wRegStart);
    *pSendBuf++ = LOBYTE(wRegStart);

    *pSendBuf++ = HIBYTE(wDataSize*2);
    *pSendBuf++ = LOBYTE(wDataSize*2);
    WORD wCRC = MDB_CRC(pBuf, 6);
    *pSendBuf++ = HIBYTE(wCRC);
    *pSendBuf++ = LOBYTE(wCRC);

    
    MDB_SendCmd(hComm, pBuf, 8);

    // receive

    MDB_CLEAR_RECV_BUF;
    BYTE *pRecvBuf = g_MdbRecvBuffer;
    int nRecvBytes = MDB_GetResponse(hComm, pRecvBuf, MDB_RECV_BUF_LEN);

    
    //for test
    //if(wRegStart == EBU_REG_BATT_STRING1_ERROR)
    /*if(wRegStart == EBU_REG_BATT_CURR)		
    {
	    TRACE_MDB("\nReceived AC METER = %d: ",nRecvBytes );
	    int j;
	    for(j=0;j<nRecvBytes;j++)
		    TRACE_MDB("%02X ",*(pRecvBuf+j));
    }*/

    if(nRecvBytes < 7)
    {
	    return MDB_ERROR_RESPOND_DATA_LEN;
    }

    // Check Address
    if(*pRecvBuf != nAddr)
    {
	    return MDB_ERROR_ADDR;
    }

    //for test
    /*if(wRegStart == EBU_REG_BATT_STRING1_ERROR)
    {
	    TRACE_MDB("\nReceived Batt Error: ");
	    int j;
	    for(j=0;j<nRecvBytes;j++)
		    TRACE_MDB("%02X",*(pRecvBuf+j));
    }*/


    // Check Function Code
    if(*(pRecvBuf + 1) != MDB_FUN_CODE_GET_MANY_DATA)
    {
	    return MDB_ERROR_FUN_CODE;
    }

    // Check Data Length
    BYTE byDataLen = *(pRecvBuf + 2);

    if (nRecvBytes < byDataLen + 5)
    {
	    return MDB_ERROR_RESPOND_DATA_LEN;
    }

    // Check CRC
    int iCrcStart = 3 + (int)byDataLen;
    WORD wReadCRC = MAKEWORD(*(pRecvBuf + iCrcStart + 1), *(pRecvBuf + iCrcStart));
    WORD wCalcCRC = MDB_CRC(pRecvBuf, byDataLen + 3);

    if(wReadCRC != wCalcCRC)
    {
	    TRACE("CRC Error! Calc CRC = %04X\n", wCalcCRC);
	    return MDB_ERROR_CRC;
    }

    if(byDataLen !=(wDataSize * 4))
    {
	    return MDB_ERROR_RESPOND_DATA_LEN;
    }

    //������
    int i;
    BYTE *pData = pRecvBuf + 3;
    for(i = 0; i < (int)byDataLen / 4; i++)
    {		
	strUnionTemp.abyValue[1] = *(pData + i * 4);
	strUnionTemp.abyValue[0] = *(pData + i * 4 + 1);
	strUnionTemp.abyValue[3] = *(pData + i * 4 + 2);
	strUnionTemp.abyValue[2] = *(pData + i * 4 + 3);	

        *(pwBuf + i) = strUnionTemp.ulValue;	

	//TRACE_MDB("\nReceived AC METER DWORD%d value= %04X : ", i, strUnionTemp.ulValue );
    }

    return MDB_ERROR_OK;
}

/*=============================================================================*
* FUNCTION: RS485WaitReadable
* PURPOSE : wait RS485 data ready
* INPUT: 
*     
*
* RETURN:
*     int : 1: data ready, 0: timeout,Allow get mode or auto config
*		
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*			Ilockteng		2009-05-19
*============================================================================*/
static int RS485WaitReadable(int fd, int nmsTimeOut)
{
    fd_set readfd;
    struct timeval timeout;

    while (nmsTimeOut > 0)
    {
	// need add ClearWDT() here.
	if (nmsTimeOut < (5*1000))
	{
	    timeout.tv_usec = nmsTimeOut%1000*1000;			/* usec     */
	    timeout.tv_sec  = nmsTimeOut/1000;				/* seconds  */

	    nmsTimeOut = 0;
	}
	else
	{
	    timeout.tv_usec = 0;							/* usec     */
	    timeout.tv_sec  = 5;							/* seconds  */

	    RUN_THREAD_HEARTBEAT();

	    nmsTimeOut -= (5*1000);
	}

    }

    FD_ZERO(&readfd);										/*  Initializes the set to the NULL set. */
    FD_SET(fd, &readfd);									/*  Adds descriptor s to set. */

    select(fd+1, &readfd, NULL, NULL, &timeout);

    if(FD_ISSET(fd, &readfd))	
    {
	return TRUE;										//1: data ready, can be read
    }
    else
    {
	return FALSE;										//0: Data not ready, can't read!!but,Allow get mode or auto config
    }
}
/*==========================================================================*
* FUNCTION : MDB_GetResponse
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
static int MDB_GetResponse(HANDLE hComm, BYTE *pBuf, int iToRead)
{
	RS485_DRV* pPort  = (RS485_DRV *)hComm;
	int fd = pPort->fdSerial;

	if (RS485WaitReadable(fd, 600) > 0 )
	{
		int nByteRead = RS485_Read(hComm, pBuf, iToRead);

#ifdef __PRINT_MDB_COMM_DATA
		//TRACE("MDB RECV:");
		int i;
		//for(i = 0; i < nByteRead; i++)
		//{
		//	TRACE(" %02X", *(pBuf + i));
		//}
		//TRACE("\n\n");
		//TRACE("Rec Data Length = %d\n\n",nByteRead);
#endif

		return nByteRead;
	}
	else
	{
		//��ʱ��������
		return 0;
	}

}

/*==========================================================================*
* FUNCTION : MDB_SendCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
static BOOL MDB_SendCmd(HANDLE hComm, BYTE *pBuf, int nDataLen)
{
#ifdef __PRINT_MDB_COMM_DATA
	TRACE("MDB SEND:");
	int i;
	for(i = 0; i < nDataLen; i++)
	{
		TRACE(" %02X", *(pBuf + i));
	}
	TRACE("\n");
#endif

	int nResult = RS485_Write(hComm, pBuf, nDataLen);
	//Sleep(200);

	return nResult == nDataLen;
}


/*==========================================================================*
* FUNCTION : ACMETER_Init
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  :               DATE: 
*==========================================================================*/
int ACMETER_Init(void* pDevice)
{
    int	i, j;
    RS485_VALUE	Value;
    Value.iValue = RS485_SAMP_INVALID_VALUE;

    for(i = 0; i < ACMETER_NUM_MAX; i++)
    {
	for(j = 0; j < ACMETER_ROUGH_DATA_MAX; j++)
	{
	    s_aMdbRoughData_ACMETER[i][j].iValue = Value.iValue;
	}
	s_aMdbRoughData_ACMETER[i][ACMETER_COMM_BREAK_TIMES].iValue = 0;
    }
}

/*==========================================================================*
* FUNCTION : ACMETER_Reconfig
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  :               DATE: 
*==========================================================================*/
void ACMETER_Reconfig(void* pDevice)
{
    RS485_DEVICE_CLASS*		pMDB = pDevice;
    HANDLE	hComm = pMDB->pCommPort->hCommPort;		

    BYTE	byAddr;
    WORD	wSoftVer;
    BOOL	bFind;
    int         i, iACMETERNum = 0;




    for(i = 0; i < ACMETER_NUM_MAX; i++)
    {
	s_aMdbRoughData_ACMETER[i][ACMETER_EXISTENCE].iValue = MDB_EQUIP_NOT_EXIST;
    }

   
    for(byAddr = ACMETER_ADDR_START; byAddr < ACMETER_ADDR_END; byAddr++)
    {
	bFind = FALSE;

	for(i = 0; i < 3; i++)
	{			
	    if(MDB_GetManyData(hComm, byAddr, ACMETER_REG_REVISION_CODE, 1, &wSoftVer) == MDB_ERROR_OK)
	    {
		    bFind = TRUE;				
		    break;
	    }
	}

	if(bFind)
	{
	    TRACE("Find ACMETER !!!! Addr is %d\n", byAddr);

	    s_aMdbRoughData_ACMETER[iACMETERNum][ACMETER_EXISTENCE].iValue = MDB_EQUIP_EXIST;
	    s_aMdbRoughData_ACMETER[iACMETERNum][ACMETER_ADDRESS].iValue = byAddr;
	    s_aMdbRoughData_ACMETER[iACMETERNum][Revision_code_ACMETER].iValue = wSoftVer;	    
	    iACMETERNum++;	    
	}
    }

    ASSERT(iACMETERNum <= ACMETER_NUM_MAX);


    g_MdbGroupRoughData[MDB_GROUP_ACMETER_NUM].iValue = iACMETERNum;		
}

/*==========================================================================*
* FUNCTION : ACMETER_Sample
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  :               DATE: 
*==========================================================================*/
void ACMETER_Sample(void* pDevice)
{
    RS485_DEVICE_CLASS *pMDB = pDevice;
    HANDLE	hComm = pMDB->pCommPort->hCommPort;
    int itempAddr;
    int iEquipmentId;
    int iSigValue;
    RS485_VALUE EbuNum, EbuAddr, EbuCommFail, Value;
	

    int i, j;

    static	int		iSFirstTimeProc = 0;

    //itempAddr = RS485_GetItemFromList();


    for(i = 0; i < g_MdbGroupRoughData[MDB_GROUP_ACMETER_NUM].iValue; i++)
    {
		
	//��ͬ�ź����͵��ź� Ҫ�ֳɲ�ͬTab������	
	//��ΪAC meterһ�����ֻ�ܻظ�22bytes�����ݣ����Զ��źų���Ϊ4bytes���źţ����ֻ������ȡ5���ź�
	REG_TO_ROUGH_MAP	SampleTab[] =
	{
	    {s_aMdbRoughData_ACMETER[i][ACMETER_ADDRESS].iValue, ACMETER_REG_VOLT_L1_N,	    MDB_SIGNAL_TYPE_INT32,	5,  &s_aMdbRoughData_ACMETER[i][VOLT_L1_N_ACMETER]},
	    {s_aMdbRoughData_ACMETER[i][ACMETER_ADDRESS].iValue, ACMETER_REG_VOLT_L3_L1,    MDB_SIGNAL_TYPE_INT32,	5,  &s_aMdbRoughData_ACMETER[i][VOLT_L3_L1_ACMETER]},
	    {s_aMdbRoughData_ACMETER[i][ACMETER_ADDRESS].iValue, ACMETER_REG_W_L2,	    MDB_SIGNAL_TYPE_INT32,	5,  &s_aMdbRoughData_ACMETER[i][WATT_L2_ACMETER]},
	    {s_aMdbRoughData_ACMETER[i][ACMETER_ADDRESS].iValue, ACMETER_REG_VAR_L1,	    MDB_SIGNAL_TYPE_INT32,	5,  &s_aMdbRoughData_ACMETER[i][VAR_L1_ACMETER]},
	    {s_aMdbRoughData_ACMETER[i][ACMETER_ADDRESS].iValue, ACMETER_REG_WATT_ACC,	    MDB_SIGNAL_TYPE_INT32,	5,  &s_aMdbRoughData_ACMETER[i][WATT_ACC_ACMETER]},	    
    		
	    {s_aMdbRoughData_ACMETER[i][ACMETER_ADDRESS].iValue, ACMETER_REG_PF_L1,	    MDB_SIGNAL_TYPE_INT16,	6,  &s_aMdbRoughData_ACMETER[i][PF_L1_ACMETER]},

	    {s_aMdbRoughData_ACMETER[i][ACMETER_ADDRESS].iValue, ACMETER_REG_DMD_W_ACC_MAX, MDB_SIGNAL_TYPE_INT32,	5,  &s_aMdbRoughData_ACMETER[i][DMD_WATT_ACC_MAX_ACMETER]},
	    {s_aMdbRoughData_ACMETER[i][ACMETER_ADDRESS].iValue, ACMETER_REG_KWH_P_PAR,	    MDB_SIGNAL_TYPE_INT32,	5,  &s_aMdbRoughData_ACMETER[i][KWH_P_PAR_ACMETER]},
	    {s_aMdbRoughData_ACMETER[i][ACMETER_ADDRESS].iValue, ACMETER_REG_KWH_P_T1,	    MDB_SIGNAL_TYPE_INT32,	5,  &s_aMdbRoughData_ACMETER[i][KWH_T1_ACMETER]},
	    {s_aMdbRoughData_ACMETER[i][ACMETER_ADDRESS].iValue, ACMETER_REG_KVARH_P_T2,    MDB_SIGNAL_TYPE_INT32,	5,  &s_aMdbRoughData_ACMETER[i][KVARH_T2_ACMETER]},
	    {s_aMdbRoughData_ACMETER[i][ACMETER_ADDRESS].iValue, ACMETER_REG_HOUR,	    MDB_SIGNAL_TYPE_INT32,	4,  &s_aMdbRoughData_ACMETER[i][HOUR_ACMETER]},	

	    {s_aMdbRoughData_ACMETER[i][ACMETER_ADDRESS].iValue, ACMETER_REG_VERSION_CODE,  MDB_SIGNAL_TYPE_INT16,	1,  &s_aMdbRoughData_ACMETER[i][Version_code_ACMETER]},
    		
	};	

	int nTabNum = sizeof(SampleTab) / sizeof(SampleTab[0]);

	int nCommResult;
    	
	//�ɼ�����
	for(j = 0; j < nTabNum; j++)
	{
	    nCommResult = MDB_SampleManyData(hComm, SampleTab + j);  //�Ѳɼ���ԭʼ������䵽s_aMdbRoughData_ACMETER
    		
	    if(nCommResult != MDB_ERROR_OK)
	    {
		    break;
	    }	

	    RUN_THREAD_HEARTBEAT();
	}

	//ͨѶ�жϵĴ���
	if(nCommResult != MDB_ERROR_OK)
	{
		if(s_aMdbRoughData_ACMETER[i][ACMETER_COMM_BREAK_TIMES].iValue<=MDB_COMM_FAIL_TIMES)
		{
			s_aMdbRoughData_ACMETER[i][ACMETER_COMM_BREAK_TIMES].iValue++;
		}
	}
	else
	{
	    s_aMdbRoughData_ACMETER[i][ACMETER_COMM_BREAK_TIMES].iValue = 0;
	}
	Value.iValue = (s_aMdbRoughData_ACMETER[i][ACMETER_COMM_BREAK_TIMES].iValue > MDB_COMM_FAIL_TIMES) ? MDB_COMM_FAILURE : MDB_COMM_OK;
    		
	s_aMdbRoughData_ACMETER[i][ACMETER_COMM_FAIL_STATUS].iValue = Value.iValue;
    }

}


/*==========================================================================*
* FUNCTION : ACMETER_StuffChn
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  :               DATE: 
*==========================================================================*/
void ACMETER_StuffChn(void* pDevice, ENUMSIGNALPROC EnumProc, LPVOID lpvoid)
{
    UNUSED(pDevice);

    static MDB_CHN_ROUGH_INDEX		s_aEBUSig[] =
    {
	{S_CH_VOLT_L1_N_ACMETER,		VOLT_L1_N_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_VOLT_L2_N_ACMETER,		VOLT_L2_N_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_VOLT_L3_N_ACMETER,		VOLT_L3_N_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_VOLT_L1_L2_ACMETER,		VOLT_L1_L2_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_VOLT_L2_L3_ACMETER,		VOLT_L2_L3_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_VOLT_L3_L1_ACMETER,		VOLT_L3_L1_ACMETER,			MDB_SCALE_TYPE_EN10},
    	
    	
	{S_CH_AMP_L1_ACMETER,			AMP_L1_ACMETER,				MDB_SCALE_TYPE_EN1000},
	{S_CH_AMP_L2_ACMETER,			AMP_L2_ACMETER,				MDB_SCALE_TYPE_EN1000},
	{S_CH_AMP_L3_ACMETER,			AMP_L3_ACMETER,				MDB_SCALE_TYPE_EN1000},
	{S_CH_WATT_L1_ACMETER,			WATT_L1_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_WATT_L2_ACMETER,			WATT_L2_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_WATT_L3_ACMETER,			WATT_L3_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_VA_L1_ACMETER,			VA_L1_ACMETER,				MDB_SCALE_TYPE_EN10},
	{S_CH_VA_L2_ACMETER,			VA_L2_ACMETER,				MDB_SCALE_TYPE_EN10},
	{S_CH_VA_L3_ACMETER,			VA_L3_ACMETER,				MDB_SCALE_TYPE_EN10},
	{S_CH_VAR_L1_ACMETER,			VAR_L1_ACMETER,				MDB_SCALE_TYPE_EN10},
	{S_CH_VAR_L2_ACMETER,			VAR_L2_ACMETER,				MDB_SCALE_TYPE_EN10},
	{S_CH_VAR_L3_ACMETER,			VAR_L3_ACMETER,				MDB_SCALE_TYPE_EN10},

	{S_CH_VOLT_L_N_ACC_ACMETER,		VOLT_L_N_ACC_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_VOLT_L_L_ACC_ACMETER,		VOLT_L_L_ACC_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_WATT_ACC_ACMETER,			WATT_ACC_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_VA_ACC_ACMETER,			VA_ACC_ACMETER,				MDB_SCALE_TYPE_EN10},
	{S_CH_VAR_ACC_ACMETER,			VAR_ACC_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_DMD_W_ACC_ACMETER,		DMD_W_ACC_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_DMD_VA_ACC_ACMETER,		DMD_VA_ACC_ACMETER,			MDB_SCALE_TYPE_EN10},
    	
	{S_CH_PF_L1_ACMETER,			PF_L1_ACMETER,				MDB_SCALE_TYPE_EN1000},
	{S_CH_PF_L2_ACMETER,			PF_L2_ACMETER,				MDB_SCALE_TYPE_EN1000},
	{S_CH_PF_L3_ACMETER,			PF_L3_ACMETER,				MDB_SCALE_TYPE_EN1000},
	{S_CH_PF_ACC_ACMETER,			PF_ACC_ACMETER,				MDB_SCALE_TYPE_EN1000},
	{S_CH_PHASE_SEQ_ACMETER,		PHASE_SEQ_ACMETER,			MDB_SCALE_TYPE_PHASE_SEQ},

	{S_CH_HZ_ACMETER,			HZ_ACMETER,				MDB_SCALE_TYPE_EN10},

	{S_CH_DMD_W_ACC_MAX_ACMETER,		DMD_WATT_ACC_MAX_ACMETER,		MDB_SCALE_TYPE_EN10},
	{S_CH_DMD_VA_ACC_MAX_ACMETER,		DMD_VA_ACC_MAX_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_DMD_A_MAX_ACMETER,		DMD_AMP_MAX_ACMETER,			MDB_SCALE_TYPE_EN1000},
	{S_CH_KWH_P_TOT_ACMETER,		KWH_P_TOT_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_KVARH_P_TOT_ACMETER,		KVARH_P_TOT_ACMETER,			MDB_SCALE_TYPE_EN10},
	
	{S_CH_KWH_P_PAR_ACMETER,		KWH_P_PAR_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_KVARH_P_PAR_ACMETER,		KVARH_P_PAR_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_KWH_P_L1_ACMETER,			KWH_L1_ACMETER,				MDB_SCALE_TYPE_EN10},
	{S_CH_KWH_P_L2_ACMETER,			KWH_L2_ACMETER,				MDB_SCALE_TYPE_EN10},
	{S_CH_KWH_P_L3_ACMETER,			KWH_L3_ACMETER,				MDB_SCALE_TYPE_EN10},
	
	{S_CH_KWH_P_T1_ACMETER,			KWH_T1_ACMETER,				MDB_SCALE_TYPE_EN10},
	{S_CH_KWH_P_T2_ACMETER,			KWH_T2_ACMETER,				MDB_SCALE_TYPE_EN10},
	{S_CH_KWH_P_T3_ACMETER,			KWH_T3_ACMETER,				MDB_SCALE_TYPE_EN10},
	{S_CH_KWH_P_T4_ACMETER,			KWH_T4_ACMETER,				MDB_SCALE_TYPE_EN10},
	{S_CH_KVARH_P_T1_ACMETER,		KVARH_T1_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_KVARH_P_T2_ACMETER,		KVARH_T2_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_KVARH_P_T3_ACMETER,		KVARH_T3_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_KVARH_P_T4_ACMETER,		KVARH_T4_ACMETER,			MDB_SCALE_TYPE_EN10},

	{S_CH_KWH_N_TOT_ACMETER,		KWH_N_TOT_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_KVARH_N_TOT_ACMETER,		KVARH_N_TOT_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_HOUR_ACMETER,			HOUR_ACMETER,				MDB_SCALE_TYPE_EN100},
	{S_CH_COUNTER1_ACMETER,			COUNTER1_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_COUNTER2_ACMETER,			COUNTER2_ACMETER,			MDB_SCALE_TYPE_EN10},
	{S_CH_COUNTER3_ACMETER,			COUNTER3_ACMETER,			MDB_SCALE_TYPE_EN10},


	{MDB_CH_END_FLAG,			MDB_CH_END_FLAG,			MDB_SCALE_TYPE_NORMAL},
    };

    int				i, j;
    RS485_VALUE	 ChnValue;
    double dfTempValue;
    for(j = 0; j < g_MdbGroupRoughData[MDB_GROUP_ACMETER_NUM].iValue; j++)
    {
	i = 0;
	while(s_aEBUSig[i].iChnNo != MDB_CH_END_FLAG)
	{	 
	    if(s_aEBUSig[i].nScaleType == MDB_SCALE_TYPE_EN10)
	    {
		dfTempValue = (double)s_aMdbRoughData_ACMETER[j][s_aEBUSig[i].iRoughIdx].iValue / 10;
		ChnValue.fValue = (float)dfTempValue;
	    }
	    else if(s_aEBUSig[i].nScaleType == MDB_SCALE_TYPE_EN100)
	    {
		dfTempValue = (double)s_aMdbRoughData_ACMETER[j][s_aEBUSig[i].iRoughIdx].iValue / 100;
		ChnValue.fValue = (float)dfTempValue;
	    }
	    else if(s_aEBUSig[i].nScaleType == MDB_SCALE_TYPE_EN1000)
	    {
		dfTempValue = (double)s_aMdbRoughData_ACMETER[j][s_aEBUSig[i].iRoughIdx].iValue / 1000;
		ChnValue.fValue = (float)dfTempValue;
	    }
	    else if(s_aEBUSig[i].nScaleType == MDB_SCALE_TYPE_PHASE_SEQ) 
	    {
		if(s_aMdbRoughData_ACMETER[j][s_aEBUSig[i].iRoughIdx].iValue == PH_SEQ_L1_L3_L2)
		{
		    ChnValue.iValue = PH_SEQ_L1_L3_L2_STUFFVALUE;
		}
		else  //PH_SEQ_L1_L2_L3
		{
		    ChnValue.iValue = PH_SEQ_L1_L2_L3_STUFFVALUE;	    
		}
	    }
	    else  // 1:1
	    {
		dfTempValue = (double)s_aMdbRoughData_ACMETER[j][s_aEBUSig[i].iRoughIdx].iValue;
		ChnValue.fValue = (float)dfTempValue;
	    }	    
    	
	    EnumProc(ACMETER_CHANNEL_SPACE * j + s_aEBUSig[i].iChnNo, ChnValue.fValue, lpvoid);
	    i++;
	}

	//���ͨ���жϺʹ���״̬
	EnumProc(ACMETER_CHANNEL_SPACE * j + S_CH_ACMETER_COMM_FAIL_STATUS, s_aMdbRoughData_ACMETER[j][ACMETER_COMM_FAIL_STATUS].fValue, lpvoid);
	EnumProc(ACMETER_CHANNEL_SPACE * j + S_CH_ACMETER_EXISTENCE, s_aMdbRoughData_ACMETER[j][ACMETER_EXISTENCE].fValue, lpvoid);
    }

    //Not exist equip handle
    for(; j < ACMETER_NUM_MAX; j++)
    {	
	//����״̬
	ChnValue.iValue = MDB_EQUIP_NOT_EXIST;
    	
	EnumProc(ACMETER_CHANNEL_SPACE * j + S_CH_ACMETER_EXISTENCE,
		ChnValue.fValue, lpvoid);

	//����Чֵ
	ChnValue.iValue = MDB_SAM_INVALID_VALUE;
	i = 0;
	while(s_aEBUSig[i].iChnNo != MDB_CH_END_FLAG)
	{
	    EnumProc(ACMETER_CHANNEL_SPACE * j + s_aEBUSig[i].iChnNo, ChnValue.fValue, lpvoid);
	    i++;
	}

	EnumProc(ACMETER_CHANNEL_SPACE * j + S_CH_ACMETER_COMM_FAIL_STATUS,
	    ChnValue.fValue, lpvoid);
    }
    return;
}


/*==========================================================================*
* FUNCTION : ACMETER_SendCtlCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
void ACMETER_SendCtlCmd(void* pDevice, int iChn, float fParam, char* strParam)
{
    UNUSED(strParam);
    int i, iEquipNo, iChnOffset;
    RS485_DEVICE_CLASS*		pMdb = pDevice;
    HANDLE	hComm = pMdb->pCommPort->hCommPort;
    WORD	wRegValue;
    BOOL	bFind;
    float	fValue;

    static MDB_CTRL_CHN_TO_REG	s_aMdbCtrlChnTab[] = 
    {
	//AC METER
	// tagACMETER_CONTROL_CHANNEL   tagACMETER_REGISTER
	//{EBU_C_CH_CELL_NUM,						EBU_REG_CELL_NUM,					MDB_SCALE_TYPE_EN10},
    	
	{MDB_CH_END_FLAG,	0,		MDB_SCALE_TYPE_NORMAL},
    };



    if((iChn >= MDB_START_CHN_ACMETER) && (iChn <= MDB_START_CHN_ACMETER + ACMETER_CHANNEL_SPACE * ACMETER_NUM_MAX))
    {
	iEquipNo = (iChn - MDB_START_CHN_ACMETER) / ACMETER_CHANNEL_SPACE;
	iChnOffset = iChn - MDB_START_CHN_ACMETER - ACMETER_CHANNEL_SPACE * iEquipNo;
    	
	i = 0;
	bFind = FALSE;
	while(s_aMdbCtrlChnTab[i].iCtrlChnNo != MDB_CH_END_FLAG)
	{
	    if(s_aMdbCtrlChnTab[i].iCtrlChnNo == iChnOffset)
	    {
		    break;
		    bFind = TRUE;				
		    //*pScaleType = s_aMdbCtrlChnTab[i].nScaleType;
		    //return s_aMdbCtrlChnTab[i].iRegNo;
	    }
	    i++;
	}
    	
	if(bFind)
	{
	    if(s_aMdbCtrlChnTab[i].nScaleType == MDB_SCALE_TYPE_EN10)
	    {
			    wRegValue = (WORD)(fValue * 10);
	    }
	    else if(s_aMdbCtrlChnTab[i].nScaleType == MDB_SCALE_TYPE_EN100)
	    {
			    wRegValue = (WORD)(fValue * 100);
	    }
	    else if(s_aMdbCtrlChnTab[i].nScaleType == MDB_SCALE_TYPE_EN1000)
	    {
			    wRegValue = (WORD)(fValue * 1000);
	    }
	    else //1:1
	    {
			    wRegValue = (WORD)(fValue);
	    }
    		
	    MDB_SetOneData(hComm, s_aMdbRoughData_ACMETER[iEquipNo][ACMETER_ADDRESS].iValue, s_aMdbCtrlChnTab[i].iRegNo, wRegValue);
	}
	else
	{
	    TRACE_FILE_LINE_FUN_ENTER("Can NOT find register by ctrl chn");
	    return;
	}
    	
    }
    else
    {
	TRACE_ERROR_INT("Error Ctrl Chn =", iChn);
	return;
    }


    return;
}

/*==========================================================================*
* FUNCTION : ACMETER_ParamUnify
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  :               DATE: 
*==========================================================================*/
void ACMETER_ParamUnify(void *pDevice)
{

    MDB_PARAM_UNIFY_ITEM AcmeterParamTab[] =
    {
	//{EQUIP_ID_ACMETER1,	SIG_TYPE_SETTING, SIG_ID_EBU_CELL_NUM,			VAR_LONG, 	EBU_CELL_NUM,				EBU_C_CH_CELL_NUM},
    };

    //int nSize = sizeof(AcmeterParamTab) / sizeof(AcmeterParamTab[0]);
    int nSize = 0;  //AC Meter ��ǰû����Ҫͬ���Ĳ���

    int i, j;
    MDB_PARAM_UNIFY_ITEM *pItem;
    int iSigValue;
    float fSigValue;
    int iEquipmentId;
    RS485_DEVICE_CLASS *pMDB = pDevice;	

    Sleep(1);

    for(i = 0; i < g_MdbGroupRoughData[MDB_GROUP_ACMETER_NUM].iValue; i++)
    {
	for(j = 0; j < nSize; j++)
	{
	    pItem = AcmeterParamTab + j;
	    iEquipmentId = EQUIP_ID_ACMETER1 + i;		
    		
	    if(s_aMdbRoughData_ACMETER[i][ACMETER_COMM_FAIL_STATUS].iValue == MDB_COMM_FAILURE)
	    {
		break;
	    }

	    if(pItem->nSigValueType == VAR_LONG)
	    {
		iSigValue = GetDwordSigValue(iEquipmentId, pItem->nSigType, pItem->nSigId, MDB_SAMPLER_TASK);				
		if(iSigValue != s_aMdbRoughData_ACMETER[i][pItem->nRoughIdx].iValue)
		{
		    ACMETER_SendCtlCmd(pDevice, 
					ACMETER_CHANNEL_SPACE * i + pItem->nCtrlChn, 
					(float)iSigValue, NULL);
		}
	    }
	    else if(pItem->nSigValueType == VAR_FLOAT)
	    {		
		fSigValue = GetFloatSigValue(iEquipmentId, pItem->nSigType, pItem->nSigId, MDB_SAMPLER_TASK);
		if(MDB_FLOAT_NOT_EQUAL(fSigValue, s_aMdbRoughData_ACMETER[i][pItem->nRoughIdx].fValue))
		{
		    ACMETER_SendCtlCmd(pDevice, 
		    		ACMETER_CHANNEL_SPACE * i + pItem->nCtrlChn,
				fSigValue, NULL);						
		}
	    }
	    else
	    {
		TRACE_ERROR_INT("Error SigValueType =", pItem->nSigValueType);
	    }

	}
    }

    Sleep(1);

}
/*==========================================================================*
* FUNCTION : ACMETER_GetProdctInfo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : 
* COMMENTS : 
* CREATOR  :               DATE: 
*==========================================================================*/
void ACMETER_GetProdctInfo(HANDLE hComm, int nUnitNo, void *pPI)
{
    UNUSED(hComm);
    UNUSED(nUnitNo);
    PRODUCT_INFO *pInfo;
    int i, j;

    pInfo = (PRODUCT_INFO *)pPI;
    pInfo += ACMETER1_EID;


    for(i = 0; i < g_MdbGroupRoughData[MDB_GROUP_ACMETER_NUM].iValue; i++)
    {
	pInfo->bSigModelUsed = TRUE;

	pInfo->szHWVersion[0] = 0;
	
	pInfo->szSerialNumber[0] = 0;
	sprintf(pInfo->szSWVersion, "%4.2f\0", (float)(s_aMdbRoughData_ACMETER[i][Revision_code_ACMETER].iValue)/100);
	
	//TRACE_MDB("\n\n AC METER Revision_code_ACMETER %d = %d \n", i, s_aMdbRoughData_ACMETER[i][Revision_code_ACMETER].iValue);
	//TRACE_MDB("%s\n", pInfo->szSWVersion);
	//TRACE_MDB("Version_code_ACMETER = %d\n", s_aMdbRoughData_ACMETER[i][Version_code_ACMETER].iValue);

	switch(s_aMdbRoughData_ACMETER[i][Version_code_ACMETER].iValue)
	{
	    case 0:
		sprintf(pInfo->szPartNumber, "%s", "EM24DINAV93XO2X");
		break;
	    case 1:
		sprintf(pInfo->szPartNumber, "%s", "EM24DINAV93XISX");
		break;
	    case 2:
		sprintf(pInfo->szPartNumber, "%s", "EM24DINAV53DO2X");
		break;
	    case 3:
		sprintf(pInfo->szPartNumber, "%s", "EM24DINAV53DISX");
		break;
	    case 4:
		sprintf(pInfo->szPartNumber, "%s", "EM24DINAV93XR2X");
		break;
	    case 5:
		sprintf(pInfo->szPartNumber, "%s", "EM24DINAV53DR2X");
		break;
	    default:
		pInfo->szPartNumber[0] = 0;
		break;
	}
	//TRACE_MDB("%s\n", pInfo->szPartNumber);

	pInfo++;
    }

    for(; i < g_MdbGroupRoughData[MDB_GROUP_ACMETER_NUM].iValue; i++)
    {
	pInfo->bSigModelUsed = FALSE;
	pInfo++;
    }
      
}


///////////////////////////////////// Parse Config file  ///////////////////////////
#define SPLITTER										('\t')
#define MDBEXT_CFG_SPEC_VAL_NA_STR						"NA"
#define MDBEXT_CFG_SPEC_VAL_NA_NUM						(-1)

static BOOL CheckNA(IN const char *szData)
{
	if(0 == stricmp(szData, MDBEXT_CFG_SPEC_VAL_NA_STR))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

static BOOL CheckYesOrNo(IN const char *szData)
{
	return ('Y' == *szData)
			|| ('y' == *szData)
			|| ('N' == *szData)
			|| ('n' == *szData);
}


static int ParseModbusGroupProc(IN char *szBuf, OUT MDB_CFG_GROUP_ITEM * pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseModbusGroupProc: ERROR\n",
			__FILE__, __LINE__);
		return 1;    /* not a num, error */
	}
	pStructData->iID = atoi(pField);
	/* validate the data */
	if(pStructData->iID <= 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseModbusGroupProc: ERROR\n",
			__FILE__, __LINE__);
		return 1;    /* not a num, error */
	}

	/* 2.Description field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField != NULL)
	{
		//pStructData->pszDescription = NEW_strdup(pField);
	}
	else
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseModbusGroupProc: ERROR\n",
			__FILE__, __LINE__);
		return 2;
	}

	/* 3.Config Table ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseModbusGroupProc: ERROR\n",
			__FILE__, __LINE__);
		return 3;    /* not a num, error */
	}
	pStructData->iConfigTableID = atoi(pField);
	/* validate the data */
	if(pStructData->iConfigTableID <= 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseModbusGroupProc: ERROR\n",
			__FILE__, __LINE__);
		return 3;    /* not a num, error */
	}

	/* 4.Group Equip ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseModbusGroupProc: ERROR\n",
			__FILE__, __LINE__);
		return 4;
	}
	//check whether or not data is "NA/Na/na/nA"
	if( CheckNA(pField) )
	{
		pStructData->iGroupEquipmentID = MDBEXT_CFG_SPEC_VAL_NA_NUM;
	}
	else
	{
		pStructData->iGroupEquipmentID = atoi(pField);
		if(pStructData->iGroupEquipmentID <= 0)
		{
			AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
				"[%s:%u]--ParseModbusGroupProc: ERROR\n",
				__FILE__, __LINE__);
			return 4;    /* not a num, error */
		}
	}

	/* 5.Group Channel Start */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseModbusGroupProc: ERROR\n",
			__FILE__, __LINE__);
		return 5;
	}
	//check whether or not data is "NA/Na/na/nA"
	if( CheckNA(pField) )
	{
		pStructData->iGroupChannelStart = MDBEXT_CFG_SPEC_VAL_NA_NUM;
	}
	else
	{
		pStructData->iGroupChannelStart = atoi(pField);
		if(pStructData->iGroupChannelStart <= 0)
		{
			AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
				"[%s:%u]--ParseModbusGroupProc: ERROR\n",
				__FILE__, __LINE__);
			return 5;    /* not a num, error */
		}
	}

	/* 6.Product Info Scan Method */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseModbusGroupProc: ERROR\n",
			__FILE__, __LINE__);
		return 6;    /* not a num, error */
	}
	pStructData->iProductInfoScanMethod = atoi(pField);
	/* validate the data */
	if(pStructData->iProductInfoScanMethod < 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseModbusGroupProc: ERROR\n",
			__FILE__, __LINE__);
		return 6;    /* not a num, error */
	}

	/* 7.Modbus Address Start */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseModbusGroupProc: ERROR\n",
			__FILE__, __LINE__);
		return 7;    /* not a num, error */
	}
	pStructData->iModbusAddrStart = atoi(pField);
	/* validate the data */
	if(pStructData->iModbusAddrStart < 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseModbusGroupProc: ERROR\n",
			__FILE__, __LINE__);
		return 7;    /* not a num, error */
	}

	/* 8.Enable */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((pField != NULL) && (*pField != 0))
	{
		/* validate the data */
		if( !CheckYesOrNo(pField) )
		{
			AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseModbusGroupProc: ERROR\n",
				__FILE__, __LINE__);
			return 8;
		}
	
		if( ('Y' == *pField) || ('y' == *pField) )
		{
			pStructData->bEnable = TRUE;
		}
		else if( ('N' == *pField) || ('n' == *pField) )
		{
			pStructData->bEnable = FALSE;
		}
		else
		{
			pStructData->bEnable = TRUE;
		}
	}
	else
	{
		pStructData->bEnable = TRUE;
	}


	return 0;
}


static int LoadModbusGroupCfgProc(IN void *pCfg, OUT void *pLoadToBuf)
{
	MDB_CFG_COMMON *pBuf;
	CONFIG_TABLE_LOADER loader[1];

	ASSERT(pCfg);
	ASSERT(pLoadToBuf);

	TRACE_MDB("LoadModbusGroupCfgProc start\n");
	pBuf = (MDB_CFG_COMMON *)pLoadToBuf;

	//1.Read tables: Page Info,Signal Info
	DEF_LOADER_ITEM(&loader[0],
		NULL,
		&(pBuf->iGroupNum),
		MDBEXT_CFG_MODBUS_GROUP,
		&(pBuf->pstGroupList),
		ParseModbusGroupProc);


	if (Cfg_LoadTables(pCfg, 1, loader) != ERR_CFG_OK)
	{
		return ERR_CFG_FAIL;
	}

	TRACE_MDB("LoadModbusGroupCfgProc end\n");
	return ERR_CFG_OK;	
}

///////////////////////////////// Parse Table  /////////////////////////////


static int ParseTableCommunicationProc(IN char *szBuf, OUT MDB_CFG_COMM_TABLE_ITEM * pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableCommunicationProc: ERROR\n",
			__FILE__, __LINE__);
		return 1;    /* not a num, error */
	}
	pStructData->iID = atoi(pField);
	/* validate the data */
	if(pStructData->iID <= 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableCommunicationProc: ERROR\n",
			__FILE__, __LINE__);
		return 1;    /* not a num, error */
	}

	/* 2.Description field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField != NULL)
	{
		//pStructData->pszDescription = NEW_strdup(pField);
	}
	else
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableCommunicationProc: ERROR\n",
			__FILE__, __LINE__);
		return 2;
	}

	/* 3.Port Number */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableCommunicationProc: ERROR\n",
			__FILE__, __LINE__);
		return 3;
	}
	//check whether or not data is "NA/Na/na/nA"
	if( CheckNA(pField) )
	{
		pStructData->iPortNum = MDBEXT_CFG_SPEC_VAL_NA_NUM;
	}
	else
	{
		pStructData->iPortNum = atoi(pField);
		if(pStructData->iPortNum < 0)
		{
			AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
				"[%s:%u]--ParseTableCommunicationProc: ERROR\n",
				__FILE__, __LINE__);
			return 3;    /* not a num, error */
		}
	}

	/* 4.Port Attribute */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField != NULL)
	{
		pStructData->pszPortAttr = NEW_strdup(pField);
	}
	else
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableCommunicationProc: ERROR\n",
			__FILE__, __LINE__);
		return 4;
	}

	/* 5.Open Timeout(Ms) */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableCommunicationProc: ERROR\n",
			__FILE__, __LINE__);
		return 5;    /* not a num, error */
	}
	pStructData->iOpenTimeout = atoi(pField);
	/* validate the data */
	if(pStructData->iOpenTimeout <= 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableCommunicationProc: ERROR\n",
			__FILE__, __LINE__);
		return 5;    /* not a num, error */
	}

	/* 6.Open Retry Max Count */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableCommunicationProc: ERROR\n",
			__FILE__, __LINE__);
		return 6;    /* not a num, error */
	}
	pStructData->iOpenRetryMaxCount = atoi(pField);
	/* validate the data */
	if(pStructData->iOpenRetryMaxCount <= 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableCommunicationProc: ERROR\n",
			__FILE__, __LINE__);
		return 6;    /* not a num, error */
	}

	/* 7.Open Retry Delay(Ms) */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableCommunicationProc: ERROR\n",
			__FILE__, __LINE__);
		return 7;    /* not a num, error */
	}
	pStructData->iOpenRetryDelay = atoi(pField);
	/* validate the data */
	if(pStructData->iOpenRetryDelay <= 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableCommunicationProc: ERROR\n",
			__FILE__, __LINE__);
		return 7;    /* not a num, error */
	}

	/* 8.Modbus Transmission Mode */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField != NULL)
	{
		pStructData->pszModbusTransMode = NEW_strdup(pField);
	}
	else
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableCommunicationProc: ERROR\n",
			__FILE__, __LINE__);
		return 8;
	}


	/* 9.Modbus Value OFF */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField != NULL)
	{
		pStructData->uiModbusValueOff = (unsigned int)strtoul(pField, NULL, 16);
	}
	else
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableCommunicationProc: ERROR\n",
			__FILE__, __LINE__);
		return 9;
	}

	/* 10.Modbus Value ON */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField != NULL)
	{
		pStructData->uiModbusValueOn = (unsigned int)strtoul(pField, NULL, 16);
	}
	else
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableCommunicationProc: ERROR\n",
			__FILE__, __LINE__);
		return 10;
	}

	return 0;
}

static int ParseTableRoughDataProc(IN char *szBuf, OUT MDB_CFG_ROUGH_DATA_TABLE_ITEM * pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableRoughDataProc: ERROR\n",
			__FILE__, __LINE__);
		return 1;    /* not a num, error */
	}
	pStructData->iID = atoi(pField);
	/* validate the data */
	if(pStructData->iID <= 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableRoughDataProc: ERROR\n",
			__FILE__, __LINE__);
		return 1;    /* not a num, error */
	}

	/* 2.Description field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField != NULL)
	{
		//pStructData->pszDescription = NEW_strdup(pField);
	}
	else
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableRoughDataProc: ERROR\n",
			__FILE__, __LINE__);
		return 2;
	}

	/* 3.Offset */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableRoughDataProc: ERROR\n",
			__FILE__, __LINE__);
		return 3;
	}
	pStructData->dfOffset = strtod(pField, NULL);

	/* 4.Scale */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableRoughDataProc: ERROR\n",
			__FILE__, __LINE__);
		return 4;
	}
	pStructData->dfScale = strtod(pField, NULL);

	/* 5.Default Value */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableRoughDataProc: ERROR\n",
			__FILE__, __LINE__);
		return 5;
	}
	pStructData->dfDefaultVal = strtod(pField, NULL);

	/* 6.Reg Addr Start */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableRoughDataProc: ERROR\n",
			__FILE__, __LINE__);
		return 6;
	}
	//check whether or not data is "NA/Na/na/nA"
	if( CheckNA(pField) )
	{
		pStructData->iRegAddrStart = MDBEXT_CFG_SPEC_VAL_NA_NUM;
	}
	else
	{
		pStructData->iRegAddrStart = atoi(pField);
		if(pStructData->iRegAddrStart < 0)
		{
			AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
				"[%s:%u]--ParseTableRoughDataProc: ERROR\n",
				__FILE__, __LINE__);
			return 6;    /* not a num, error */
		}
	}

	/* 7.Reg Quantity */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableRoughDataProc: ERROR\n",
			__FILE__, __LINE__);
		return 7;
	}
	//check whether or not data is "NA/Na/na/nA"
	if( CheckNA(pField) )
	{
		pStructData->iRegQuantity = MDBEXT_CFG_SPEC_VAL_NA_NUM;
	}
	else
	{
		pStructData->iRegQuantity = atoi(pField);
		if(pStructData->iRegQuantity <= 0)
		{
			AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
				"[%s:%u]--ParseTableRoughDataProc: ERROR\n",
				__FILE__, __LINE__);
			return 7;    /* not a num, error */
		}
	}

	return 0;
}

static int ParseTableSampleProc(IN char *szBuf, OUT MDB_CFG_SAMPLE_TABLE_ITEM * pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableSampleProc: ERROR\n",
			__FILE__, __LINE__);
		return 1;    /* not a num, error */
	}
	pStructData->iID = atoi(pField);
	/* validate the data */
	if(pStructData->iID <= 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableSampleProc: ERROR\n",
			__FILE__, __LINE__);
		return 1;    /* not a num, error */
	}

	/* 2.Description field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField != NULL)
	{
		//pStructData->pszDescription = NEW_strdup(pField);
	}
	else
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableSampleProc: ERROR\n",
			__FILE__, __LINE__);
		return 2;
	}

	/* 3.Sample Method */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableSampleProc: ERROR\n",
			__FILE__, __LINE__);
		return 3;    /* not a num, error */
	}
	pStructData->iSampleMethod = atoi(pField);
	/* validate the data */
	if(pStructData->iSampleMethod < 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableSampleProc: ERROR\n",
			__FILE__, __LINE__);
		return 3;    /* not a num, error */
	}

	/* 4.Modbus Function ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableSampleProc: ERROR\n",
			__FILE__, __LINE__);
		return 4;    /* not a num, error */
	}
	pStructData->iModbusFunID = atoi(pField);
	/* validate the data */
	if(pStructData->iModbusFunID <= 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableSampleProc: ERROR\n",
			__FILE__, __LINE__);
		return 4;    /* not a num, error */
	}

	/* 5. Modbus Data */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableSampleProc: ERROR\n",
			__FILE__, __LINE__);
		return 5;
	}
	//check whether or not data is "NA/Na/na/nA"
	if( CheckNA(pField) )
	{
		pStructData->iModbusData = MDBEXT_CFG_SPEC_VAL_NA_NUM;
	}
	else
	{
		if ((*pField != '0') || (*(pField + 1) != 'x' && *(pField + 1) != 'X'))
		{
			AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
				"[%s:%u]--ParseTableSampleProc: ERROR\n",
				__FILE__, __LINE__);
			return 5;
		}
		sscanf(pField, "%x", &pStructData->iModbusData);
	}
	
	/* 6.Split Count */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableSampleProc: ERROR\n",
			__FILE__, __LINE__);
		return 6;
	}
	//check whether or not data is "NA/Na/na/nA"
	if( CheckNA(pField) )
	{
		pStructData->iSplitCount = MDBEXT_CFG_SPEC_VAL_NA_NUM;
	}
	else
	{
		pStructData->iSplitCount = atoi(pField);
		if(pStructData->iSplitCount <= 0)
		{
			AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
				"[%s:%u]--ParseTableSampleProc: ERROR\n",
				__FILE__, __LINE__);
			return 6;    /* not a num, error */
		}
	}

	/* 7.Reg Addr Start */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableSampleProc: ERROR\n",
			__FILE__, __LINE__);
		return 7;
	}
	//check whether or not data is "NA/Na/na/nA"
	if( CheckNA(pField) )
	{
		pStructData->iRegAddrStart = MDBEXT_CFG_SPEC_VAL_NA_NUM;
	}
	else
	{
		pStructData->iRegAddrStart = atoi(pField);
		if(pStructData->iRegAddrStart <= 0)
		{
			AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
				"[%s:%u]--ParseTableSampleProc: ERROR\n",
				__FILE__, __LINE__);
			return 7;    /* not a num, error */
		}
	}

	/* 8.Reg Quantity */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableSampleProc: ERROR\n",
			__FILE__, __LINE__);
		return 8;
	}
	//check whether or not data is "NA/Na/na/nA"
	if( CheckNA(pField) )
	{
		pStructData->iRegQuantity = MDBEXT_CFG_SPEC_VAL_NA_NUM;
	}
	else
	{
		pStructData->iRegQuantity = atoi(pField);
		if(pStructData->iRegQuantity <= 0)
		{
			AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
				"[%s:%u]--ParseTableSampleProc: ERROR\n",
				__FILE__, __LINE__);
			return 8;    /* not a num, error */
		}
	}

	/* 9.Data Type */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableSampleProc: ERROR\n",
			__FILE__, __LINE__);
		return 9;    /* not a num, error */
	}
	pStructData->iDataType = atoi(pField);
	/* validate the data */
	if(pStructData->iDataType <= 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableSampleProc: ERROR\n",
			__FILE__, __LINE__);
		return 9;    /* not a num, error */
	}

	/* 10.Rough Data ID Start */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableSampleProc: ERROR\n",
			__FILE__, __LINE__);
		return 10;
	}
	//check whether or not data is "NA/Na/na/nA"
	if( CheckNA(pField) )
	{
		pStructData->iRoughDataIDStart = MDBEXT_CFG_SPEC_VAL_NA_NUM;
	}
	else
	{
		pStructData->iRoughDataIDStart = atoi(pField);
		if(pStructData->iRoughDataIDStart <= 0)
		{
			AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
				"[%s:%u]--ParseTableSampleProc: ERROR\n",
				__FILE__, __LINE__);
			return 10;    /* not a num, error */
		}
	}

	/* 11.Response Timeout(Ms) */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableSampleProc: ERROR\n",
			__FILE__, __LINE__);
		return 11;    /* not a num, error */
	}
	pStructData->iResponseTimeout = atoi(pField);
	/* validate the data */
	if(pStructData->iResponseTimeout < 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableSampleProc: ERROR\n",
			__FILE__, __LINE__);
		return 11;    /* not a num, error */
	}

	/* 12.Read Delay(Ms) */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableSampleProc: ERROR\n",
			__FILE__, __LINE__);
		return 12;    /* not a num, error */
	}
	pStructData->iReadDelay = atoi(pField);
	/* validate the data */
	if(pStructData->iReadDelay < 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableSampleProc: ERROR\n",
			__FILE__, __LINE__);
		return 12;    /* not a num, error */
	}

	/* 13.Response Key Separator */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableSampleProc: ERROR\n",
			__FILE__, __LINE__);
		return 13;
	}
	else
	{
		pStructData->pszResponseKeyStr = NEW_strdup(pField);
		//check whether or not data is "NA/Na/na/nA"
		if( CheckNA(pField) )
		{
			pStructData->pszResponseKeyStr[0] = 0;
		}
	}
	
	return 0;
}

static int ParseTableProductInfoProc(IN char *szBuf, OUT MDB_CFG_PRODUCT_INFO_TABLE_ITEM * pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableProductInfoProc: ERROR\n",
			__FILE__, __LINE__);
		return 1;    /* not a num, error */
	}
	pStructData->iID = atoi(pField);
	/* validate the data */
	if(pStructData->iID <= 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableProductInfoProc: ERROR\n",
			__FILE__, __LINE__);
		return 1;    /* not a num, error */
	}

	/* 2.Description field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField != NULL)
	{
		//pStructData->pszDescription = NEW_strdup(pField);
	}
	else
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableProductInfoProc: ERROR\n",
			__FILE__, __LINE__);
		return 2;
	}

	/* 3.Parse Method */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
/*	if (strcmp(pField,"SNNUM_STR") == 0)
	{
		pStructData->iSampleMethod = MDBEXT_RECONFIG_SNNUM_STR;
	}
	else if (strcmp(pField,"SWVER_STR") == 0)
	{
		pStructData->iSampleMethod = MDBEXT_RECONFIG_SWVER_STR;
	}
	else if (strcmp(pField,"HWVER_STR") == 0)
	{
		pStructData->iSampleMethod = MDBEXT_RECONFIG_HWVER_STR;
	}
	else if (strcmp(pField,"PROD_ID_STR") == 0)
	{
		pStructData->iSampleMethod = MDBEXT_RECONFIG_PROD_ID_STR;
	}
	else if (strcmp(pField,"PARSE_SNNUM_STR") == 0)
	{
		pStructData->iSampleMethod = MDBEXT_RECONFIG_PARSE_SNNUM_STR;
	}
	else if (strcmp(pField,"PARSE_SWVER_STR") == 0)
	{
		pStructData->iSampleMethod = MDBEXT_RECONFIG_PARSE_SWVER_STR;
	}
	else if (strcmp(pField,"PARSE_HWVER_STR") == 0)
	{
		pStructData->iSampleMethod = MDBEXT_RECONFIG_PARSE_HWVER_STR;
	}
	else if (strcmp(pField,"PARSE_PROD_ID_STR") == 0)
	{
		pStructData->iSampleMethod = MDBEXT_RECONFIG_PARSE_PROD_ID_STR;
	}
	else if (strcmp(pField,"SNNUM") == 0)
	{
		pStructData->iSampleMethod = MDBEXT_RECONFIG_SNNUM;
	}
	
	else if (strcmp(pField,"HWVER") == 0)
	{
		pStructData->iSampleMethod = MDBEXT_RECONFIG_HWVER;
	}
*/ 
	if ( strcmp(pField,"PARSE_STR") == 0 )
	{
		pStructData->iParseMethod = MDBEXT_RECONFIG_PARSE_STR;	
	}
	else if ( strcmp(pField,"PARSE_INT") == 0 )
	{
		pStructData->iParseMethod = MDBEXT_RECONFIG_PARSE_INT;	
	}	 	
	else if ( strcmp(pField,"PARSE_INT_BYTE") == 0 )
	{
		pStructData->iParseMethod = MDBEXT_RECONFIG_PARSE_INT_BYTE;	
	}	 	
	else
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableProductInfoProc: ERROR\n",
			__FILE__, __LINE__);
		return 3;    /* not a num, error */
	}	
	
	/* 4. Read Identifier */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableProductInfoProc: ERROR\n",
			__FILE__, __LINE__);
		return 4;
	}
	//check whether or not data is "NA/Na/na/nA"
	if( CheckNA(pField) )
	{
		pStructData->iReadIdentifier = MDBEXT_CFG_SPEC_VAL_NA_NUM;
	}
	else
	{
		if ((*pField != '0') || (*(pField + 1) != 'x' && *(pField + 1) != 'X'))
		{
			AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
				"[%s:%u]--ParseTableProductInfoProc: ERROR\n",
				__FILE__, __LINE__);
			return 4;
		}
		sscanf(pField, "%x", &pStructData->iReadIdentifier);
	}
	
	/* 5.Item Separator/Length */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableProductInfoProc: ERROR\n",
			__FILE__, __LINE__);
		return 5;
	}
	else
	{
		if ((*pField != '0') || (*(pField + 1) != 'x' && *(pField + 1) != 'X'))
		{
			AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
				"[%s:%u]--ParseTableProductInfoProc: ERROR\n",
				__FILE__, __LINE__);
			return 5;
		}
		sscanf(pField, "%x", &pStructData->iItemSeparator);
		TRACE_MDB("ParseTableProductInfoProc: item separator %02x\n", pStructData->iItemSeparator);
	}

	/* 6.Item Number */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableProductInfoProc: ERROR\n",
			__FILE__, __LINE__);
		return 6;
	}
	//check whether or not data is "NA/Na/na/nA"
	if( CheckNA(pField) )
	{
		pStructData->iItemNumber = MDBEXT_CFG_SPEC_VAL_NA_NUM;
	}
	else
	{
		pStructData->iItemNumber = atoi(pField);
		if(pStructData->iItemNumber <= 0)
		{
			AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
				"[%s:%u]--ParseTableProductInfoProc: ERROR\n",
				__FILE__, __LINE__);
			return 6;    /* not a num, error */
		}
	}
	
	/* 7.Item Offset */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableProductInfoProc: ERROR\n",
			__FILE__, __LINE__);
		return 7;
	}
	//check whether or not data is "NA/Na/na/nA"
	if( CheckNA(pField) )
	{
		pStructData->iItemOffset = 0;
	}
	else
	{
		pStructData->iItemOffset = atoi(pField);
		if(pStructData->iItemOffset <= 0)
		{
			AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
				"[%s:%u]--ParseTableProductInfoProc: ERROR\n",
				__FILE__, __LINE__);
			return 7;    /* not a num, error */
		}
	}
	
	return 0;
}

static int ParseTableStuffChanProc(IN char *szBuf, OUT MDB_CFG_STUFF_CHAN_TABLE_ITEM * pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableStuffChanProc: ERROR\n",
			__FILE__, __LINE__);
		return 1;    /* not a num, error */
	}
	pStructData->iID = atoi(pField);
	/* validate the data */
	if(pStructData->iID <= 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableStuffChanProc: ERROR\n",
			__FILE__, __LINE__);
		return 1;    /* not a num, error */
	}

	/* 2.Description field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField != NULL)
	{
		//pStructData->pszDescription = NEW_strdup(pField);
	}
	else
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableStuffChanProc: ERROR\n",
			__FILE__, __LINE__);
		return 2;
	}

	/* 3.Sample Channel Start */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableStuffChanProc: ERROR\n",
			__FILE__, __LINE__);
		return 3;    /* not a num, error */
	}
	pStructData->iSampleChanStart = atoi(pField);
	/* validate the data */
	if(pStructData->iSampleChanStart < 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableStuffChanProc: ERROR\n",
			__FILE__, __LINE__);
		return 3;    /* not a num, error */
	}

	/* 4.Sample Channel Quantity */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableStuffChanProc: ERROR\n",
			__FILE__, __LINE__);
		return 4;    /* not a num, error */
	}
	pStructData->iSampleChanQuantity = atoi(pField);
	/* validate the data */
	if(pStructData->iSampleChanQuantity <= 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableStuffChanProc: ERROR\n",
			__FILE__, __LINE__);
		return 4;    /* not a num, error */
	}

	/* 5.Channel Data Type */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField != 'L')
		&& (*pField != 'F')
		&& (*pField != 'U')
		&& (*pField != 'T')
		&& (*pField != 'E')
		&& ((*pField < '0') || (*pField > '9')))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableStuffChanProc: ERROR\n",
			__FILE__, __LINE__);
		return 5;    /* not a num, error */
	}

	if(*pField == 'L')
	{
		pStructData->iChanDataType = MDBEXT_CHAN_DATA_TYPE_LONG;
	}
	else if(*pField == 'F')
	{
		pStructData->iChanDataType = MDBEXT_CHAN_DATA_TYPE_FLOAT;
	}
	else if(*pField == 'U')
	{
		pStructData->iChanDataType = MDBEXT_CHAN_DATA_TYPE_ULONG;
	}
	else if(*pField == 'T')
	{
		pStructData->iChanDataType = MDBEXT_CHAN_DATA_TYPE_TIME;
	}
	else if(*pField == 'E')
	{
		pStructData->iChanDataType = MDBEXT_CHAN_DATA_TYPE_ENUM;
	}
	else//number
	{
		pStructData->iChanDataType = atoi(pField);
	}
	
	/* validate the data */
	if(pStructData->iChanDataType <= 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableStuffChanProc: ERROR\n",
			__FILE__, __LINE__);
		return 5;    /* not a num, error */
	}

	/* 6.Rough Data ID Start */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableStuffChanProc: ERROR\n",
			__FILE__, __LINE__);
		return 6;
	}
	//check whether or not data is "NA/Na/na/nA"
	if( CheckNA(pField) )
	{
		pStructData->iRoughDataIDStart = MDBEXT_CFG_SPEC_VAL_NA_NUM;
	}
	else
	{
		pStructData->iRoughDataIDStart = atoi(pField);
		if(pStructData->iRoughDataIDStart <= 0)
		{
			AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
				"[%s:%u]--ParseTableStuffChanProc: ERROR\n",
				__FILE__, __LINE__);
			return 6;    /* not a num, error */
		}
	}

	/* 7.Stuff Method */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField != 'D')//D/DATA
		&& (*pField != 'B')//B/BIT
		&& (*pField != 'A')//A/AVE
		&& (*pField != 'S')//S/SUM
		&& (*pField != 'N')//N/NUM
		&& (*pField != 'C')//C/COMM
		&& (*pField != 'E')//E/EXIST
		&& ((*pField < '0') || (*pField > '9')))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableStuffChanProc: ERROR\n",
			__FILE__, __LINE__);
		return 7;    /* not a num, error */
	}
	
	if(*pField == 'D')
	{
		pStructData->iStuffMethod = MDBEXT_STUFF_METHOD_DATA;
	}
	else if(*pField == 'B')
	{
		pStructData->iStuffMethod = MDBEXT_STUFF_METHOD_BIT;
	}
	else if(*pField == 'A')
	{
		pStructData->iStuffMethod = MDBEXT_GROUP_STUFF_METHOD_AVERAGE;
	}
	else if(*pField == 'S')
	{
		pStructData->iStuffMethod = MDBEXT_GROUP_STUFF_METHOD_SUMMER;
	}
	else if(*pField == 'N')
	{
		pStructData->iStuffMethod = MDBEXT_GROUP_STUFF_METHOD_NUMBER;
	}
	else if(*pField == 'C')
	{
		pStructData->iStuffMethod = MDBEXT_GROUP_STUFF_COMM_FAIL;
	}
	else if(*pField == 'E')
	{
		pStructData->iStuffMethod = MDBEXT_GROUP_STUFF_EXISTENCE;
	}
	else
	{
		pStructData->iStuffMethod = atoi(pField);
	}
	
	/* validate the data */
	if(pStructData->iStuffMethod < 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableStuffChanProc: ERROR\n",
			__FILE__, __LINE__);
		return 7;    /* not a num, error */
	}

	/* 8.Stuff Arg1 */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableStuffChanProc: ERROR\n",
			__FILE__, __LINE__);
		return 8;
	}
	//check whether or not data is "NA/Na/na/nA"
	if( CheckNA(pField) )
	{
		pStructData->fStuffArg1 = MDBEXT_CFG_SPEC_VAL_NA_NUM;
	}
	else
	{
		pStructData->fStuffArg1 = strtod(pField, NULL);
	}

	/* 9.Stuff Arg2 */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (NULL == pField)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableStuffChanProc: ERROR\n",
			__FILE__, __LINE__);
		return 9;
	}
	//check whether or not data is "NA/Na/na/nA"
	if( CheckNA(pField) )
	{
		pStructData->fStuffArg2 = MDBEXT_CFG_SPEC_VAL_NA_NUM;
	}
	else
	{
		pStructData->fStuffArg2 = strtod(pField, NULL);
	}

	return 0;
}

static int ParseTableControlProc(IN char *szBuf, OUT MDB_CFG_CONTROL_TABLE_ITEM * pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	UNUSED(pField);
	UNUSED(szBuf);
	UNUSED(pStructData);

	return 0;
}

static int ParseTableUnifyParamProc(IN char *szBuf, OUT MDB_CFG_UNIFY_PARAM_TABLE_ITEM * pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	UNUSED(pField);
	UNUSED(szBuf);
	UNUSED(pStructData);

	return 0;
}


static int ParseTableEquipProc(IN char *szBuf, OUT MDB_CFG_EQUIPMENT_TABLE_ITEM * pStructData)
{
	char *pField;

	ASSERT(szBuf);
	ASSERT(pStructData);

	/* 1.ID */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableEquipProc: ERROR\n",
			__FILE__, __LINE__);
		return 1;    /* not a num, error */
	}
	pStructData->iID = atoi(pField);
	/* validate the data */
	if(pStructData->iID <= 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableEquipProc: ERROR\n",
			__FILE__, __LINE__);
		return 1;    /* not a num, error */
	}

	/* 2.Description field */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if (pField != NULL)
	{
		//pStructData->pszDescription = NEW_strdup(pField);
	}
	else
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableEquipProc: ERROR\n",
			__FILE__, __LINE__);
		return 2;
	}

	/* 3.Equip ID Start */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableEquipProc: ERROR\n",
			__FILE__, __LINE__);
		return 3;    /* not a num, error */
	}
	pStructData->iEquipIDStart = atoi(pField);
	/* validate the data */
	if(pStructData->iEquipIDStart <= 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableEquipProc: ERROR\n",
			__FILE__, __LINE__);
		return 3;    /* not a num, error */
	}

	/* 4.Equip Quantity */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableEquipProc: ERROR\n",
			__FILE__, __LINE__);
		return 4;    /* not a num, error */
	}
	pStructData->iEquipQuantity = atoi(pField);
	/* validate the data */
	if(pStructData->iEquipQuantity <= 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableEquipProc: ERROR\n",
			__FILE__, __LINE__);
		return 4;    /* not a num, error */
	}

	/* 5.Channel Start */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableEquipProc: ERROR\n",
			__FILE__, __LINE__);
		return 5;    /* not a num, error */
	}
	pStructData->iChannelStart = atoi(pField);
	/* validate the data */
	if(pStructData->iChannelStart <= 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableEquipProc: ERROR\n",
			__FILE__, __LINE__);
		return 5;    /* not a num, error */
	}

	/* 6.Channel Quantity of Each Equip */
	szBuf = Cfg_SplitStringEx(szBuf, &pField, SPLITTER);
	if ((*pField < '0') || (*pField > '9'))
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableEquipProc: ERROR\n",
			__FILE__, __LINE__);
		return 6;    /* not a num, error */
	}
	pStructData->iChannelQuantity = atoi(pField);
	/* validate the data */
	if(pStructData->iChannelQuantity <= 0)
	{
		AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, 
			"[%s:%u]--ParseTableEquipProc: ERROR\n",
			__FILE__, __LINE__);
		return 6;    /* not a num, error */
	}

	return 0;
}

static int LoadModbusTableCfgProc(IN void *pCfg, OUT void *pLoadToBuf)
{
	MDB_CFG_COMMON			*pBuf;
	int						iTableCount = 8;
	CONFIG_TABLE_LOADER		loader[iTableCount];
	char					szaSegNameTable[iTableCount][MDBEXT_CFG_TABLE_NAME_MAX];
	int						iGroup = 0;

	ASSERT(pCfg);
	ASSERT(pLoadToBuf);

	TRACE_MDB("LoadModbusTableCfgProc start\n");
	pBuf = (MDB_CFG_COMMON *)pLoadToBuf;

	//TRACE_MDB("LoadModbusTableCfgProc iGroupNum=%d\n", pBuf->iGroupNum);
	if(pBuf->iGroupNum <= 0)
	{
		return ERR_CFG_FAIL;
	}


	pBuf->pstTableList = NEW(MDB_CFG_TABLE, pBuf->iGroupNum);
	if(pBuf->pstTableList == NULL)
	{
		return ERR_CFG_NO_MEMORY;
	}

	for(iGroup = 0; iGroup < pBuf->iGroupNum; iGroup++)
	{
		//0.MDBEXT_CFG_TABLE_COMMUNICATION
		snprintf(szaSegNameTable[0],
			MDBEXT_CFG_TABLE_NAME_MAX,
			MDBEXT_CFG_TABLE_COMMUNICATION,
			pBuf->pstGroupList[iGroup].iConfigTableID);

		DEF_LOADER_ITEM(&loader[0],
			NULL,
			&(pBuf->pstTableList[iGroup].stCfgCommTable.iNum),
			szaSegNameTable[0],
			&(pBuf->pstTableList[iGroup].stCfgCommTable.pstItem),
			ParseTableCommunicationProc);

		//1.MDBEXT_CFG_TABLE_ROUGH_DATA
		snprintf(szaSegNameTable[1],
			MDBEXT_CFG_TABLE_NAME_MAX,
			MDBEXT_CFG_TABLE_ROUGH_DATA,
			pBuf->pstGroupList[iGroup].iConfigTableID);

		DEF_LOADER_ITEM(&loader[1],
			NULL,
			&(pBuf->pstTableList[iGroup].stCfgRoughDataTable.iNum),
			szaSegNameTable[1],
			&(pBuf->pstTableList[iGroup].stCfgRoughDataTable.pstItem),
			ParseTableRoughDataProc);

		//2.MDBEXT_CFG_TABLE_SAMPLE
		snprintf(szaSegNameTable[2],
			MDBEXT_CFG_TABLE_NAME_MAX,
			MDBEXT_CFG_TABLE_SAMPLE,
			pBuf->pstGroupList[iGroup].iConfigTableID);

		DEF_LOADER_ITEM(&loader[2],
			NULL,
			&(pBuf->pstTableList[iGroup].stCfgSampleTable.iNum),
			szaSegNameTable[2],
			&(pBuf->pstTableList[iGroup].stCfgSampleTable.pstItem),
			ParseTableSampleProc);

		//3.MDBEXT_CFG_TABLE_STUFF_CHANNEL
		snprintf(szaSegNameTable[3],
			MDBEXT_CFG_TABLE_NAME_MAX,
			MDBEXT_CFG_TABLE_STUFF_CHANNEL,
			pBuf->pstGroupList[iGroup].iConfigTableID);

		DEF_LOADER_ITEM(&loader[3],
			NULL,
			&(pBuf->pstTableList[iGroup].stCfgStuffChanTable.iNum),
			szaSegNameTable[3],
			&(pBuf->pstTableList[iGroup].stCfgStuffChanTable.pstItem),
			ParseTableStuffChanProc);

		//4.MDBEXT_CFG_TABLE_CONTROL
		snprintf(szaSegNameTable[4],
			MDBEXT_CFG_TABLE_NAME_MAX,
			MDBEXT_CFG_TABLE_CONTROL,
			pBuf->pstGroupList[iGroup].iConfigTableID);

		DEF_LOADER_ITEM(&loader[4],
			NULL,
			&(pBuf->pstTableList[iGroup].stCfgControlTable.iNum),
			szaSegNameTable[4],
			&(pBuf->pstTableList[iGroup].stCfgControlTable.pstItem),
			ParseTableControlProc);

		//5.MDBEXT_CFG_TABLE_UNIFY_PARAM
		snprintf(szaSegNameTable[5],
			MDBEXT_CFG_TABLE_NAME_MAX,
			MDBEXT_CFG_TABLE_UNIFY_PARAM,
			pBuf->pstGroupList[iGroup].iConfigTableID);

		DEF_LOADER_ITEM(&loader[5],
			NULL,
			&(pBuf->pstTableList[iGroup].stCfgUnifyParamTable.iNum),
			szaSegNameTable[5],
			&(pBuf->pstTableList[iGroup].stCfgUnifyParamTable.pstItem),
			ParseTableUnifyParamProc);

		//6.MDBEXT_CFG_TABLE_EQUIPMENT
		snprintf(szaSegNameTable[6],
			MDBEXT_CFG_TABLE_NAME_MAX,
			MDBEXT_CFG_TABLE_EQUIPMENT,
			pBuf->pstGroupList[iGroup].iConfigTableID);

		DEF_LOADER_ITEM(&loader[6],
			NULL,
			&(pBuf->pstTableList[iGroup].stCfgEquipTable.iNum),
			szaSegNameTable[6],
			&(pBuf->pstTableList[iGroup].stCfgEquipTable.pstItem),
			ParseTableEquipProc);

		//7.MDBEXT_CFG_TABLE_PRODUCT_INFO
		snprintf(szaSegNameTable[7],
			MDBEXT_CFG_TABLE_NAME_MAX,
			MDBEXT_CFG_TABLE_PRODUCT_INFO,
			pBuf->pstGroupList[iGroup].iConfigTableID);

		DEF_LOADER_ITEM(&loader[7],
			NULL,
			&(pBuf->pstTableList[iGroup].stCfgProductInfoTable.iNum),
			szaSegNameTable[7],
			&(pBuf->pstTableList[iGroup].stCfgProductInfoTable.pstItem),
			ParseTableProductInfoProc);

		TRACE_MDB("\nLoadModbusTableCfgProc::iGroup=%d\n", iGroup);
		if (Cfg_LoadTables(pCfg, iTableCount, loader) != ERR_CFG_OK)
		{
			return ERR_CFG_FAIL;
		}
	}

	TRACE_MDB("LoadModbusTableCfgProc end\n");
	return ERR_CFG_OK;	
}



int MDBEXT_LoadCfg(void)
{
	MDB_INFO				*pstMdb = &sg_stMdbInfo;
	MDB_CFG_COMMON			*pstCfg = &pstMdb->stCommonCfg;
	MDB_CFG_TABLE			*pstCurCfgTable = NULL;
	int						iGroup;
	char					szFullPath[MDBEXT_PATH_STR_MAX];
	int						iRet, i;

	TRACE_MDB("MDBEXT_LoadCfg start\n");
	if(MDBEXT_FLAG_NONE != pstMdb->iLoadCfgFlag)//have been loaded
	{
		return ERR_CFG_OK;
	}

	pstMdb->iLoadCfgFlag = MDBEXT_FLAG_FAIL;//init to MDBEXT_FLAG_FAIL

	Cfg_GetFullConfigPath(MDBEXT_CFG_FILE, szFullPath, MAX_FILE_PATH);//private/modbus/ModbusR485Sample.cfg
	//TRACE_MDB("szFullPath=%s\n", szFullPath);
	//1. we must load [MODBUS_GROUP] at first
	iRet = Cfg_LoadConfigFile(szFullPath, LoadModbusGroupCfgProc, pstCfg);
	if (iRet != ERR_CFG_OK)
	{
		//TRACE_MDB("MDBEXT_LoadCfg::LoadModbusGroupCfgProc error\n");
		return iRet;
	}

	if(pstCfg->iGroupNum <= 0)//config item is null
	{
		pstMdb->iLoadCfgFlag = MDBEXT_FLAG_SUCCESS;//loading finish
		return ERR_CFG_OK;
	}

	//2. load all tables
	iRet = Cfg_LoadConfigFile(szFullPath, LoadModbusTableCfgProc, pstCfg);
	if (iRet != ERR_CFG_OK)
	{
		//TRACE_MDB("MDBEXT_LoadCfg::LoadModbusTableCfgProc error\n");
		return iRet;
	}

	//check config file
	for(iGroup = 0; iGroup < pstCfg->iGroupNum; iGroup++)
	{
		pstCurCfgTable = &pstCfg->pstTableList[iGroup];

		if((pstCurCfgTable->stCfgCommTable.iNum <= 0)
			|| (pstCurCfgTable->stCfgRoughDataTable.iNum <= 0)
			|| (pstCurCfgTable->stCfgSampleTable.iNum <= 0)
			|| (pstCurCfgTable->stCfgEquipTable.iNum <= 0)
			)
		{
			//TRACE_MDB("MDBEXT_LoadCfg::iNum error, stCfgCommTable=%d, stCfgRoughDataTable=%d, stCfgSampleTable=%d, stCfgEquipTable=%d\n",
			//	pstCurCfgTable->stCfgCommTable.iNum,
			//	pstCurCfgTable->stCfgRoughDataTable.iNum,
			//	pstCurCfgTable->stCfgSampleTable.iNum,
			//	pstCurCfgTable->stCfgEquipTable.iNum);
			return ERR_CFG_FAIL;
		}

		//check special item in rough data table 
		iRet = 0;
		for(i = pstCurCfgTable->stCfgRoughDataTable.iNum - 1;i >= 0; i--)
		{
			if(MDBEXT_RAUGH_DATA_ID_COMMFAIL == pstCurCfgTable->stCfgRoughDataTable.pstItem[i].iID)
			{
				iRet |= 1;
			}
			else if(MDBEXT_RAUGH_DATA_ID_EXISTENCE == pstCurCfgTable->stCfgRoughDataTable.pstItem[i].iID)
			{
				iRet |= 2;
			}
			else if(MDBEXT_RAUGH_DATA_ID_ADDRESS == pstCurCfgTable->stCfgRoughDataTable.pstItem[i].iID)
			{
				iRet |= 4;
			}
			else if(MDBEXT_RAUGH_DATA_ID_COMMBREAKTIMES == pstCurCfgTable->stCfgRoughDataTable.pstItem[i].iID)
			{
				iRet |= 8;
			}

			if(0xf == iRet)
			{
				break;
			}
		}
		if(iRet != 0xf)
		{
			TRACE_MDB("MDBEXT_LoadCfg::ERROR (iRet != 0xF)iRet=%0X\n", iRet);
			return ERR_CFG_FAIL;
		}
	}


	pstMdb->iLoadCfgFlag = MDBEXT_FLAG_SUCCESS;//loading finish
	TRACE_MDB("MDBEXT_LoadCfg end\n");
	return ERR_CFG_OK;
}


/*==========================================================================*
 * FUNCTION : ESR_AsciiHexToChar
 * PURPOSE  : get value from ascii hex. eg: "1A" = 1*16+10 =26
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char  *pStr : two char buffer
 * RETURN   : BYTE : the value
 * COMMENTS : assistant function
 * CREATOR  : LinTao                   DATE: 2004-10-12 19:24
 *==========================================================================*/
static BYTE AsciiHexToChar(IN const unsigned char *pStr)
{
	BYTE value;
	unsigned char c;

	ASSERT(pStr);

	c = pStr[0];
	if (c >= '0' && c <= '9')
	{
		c = c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		c = c - 'A' + 10;
	}
	value = c * 0x10;

	c = pStr[1];
	if (c >= '0' && c <= '9')
	{
		c = c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		c = c - 'A' + 10;
	}
	value += c;

	return value;
}

static BYTE MDBEXT_CalcLRC(IN const unsigned char * pStr, IN int iLen)
{
	BYTE result = 0;
	int i;

	iLen--;//make sure no flowing out

	//sum
	for (i = 0 ; i < iLen; i += 2) 
	{
		result += AsciiHexToChar(pStr + i);
	}

	//not
	result = ~result;

	//add one
	result++;

	return result;
}


static int MDBEXT_GetResponse(IN HANDLE hComm, IN BYTE *pBuf, IN int iToRead, IN int iTimeout, IN int iReadDelay)
{
	RS485_DRV* pPort  = (RS485_DRV *)hComm;
	int fd = pPort->fdSerial;

	if (RS485WaitReadable(fd, iTimeout) > 0 )
	{
		if(iReadDelay > 0)
		{
			Sleep((unsigned int)iReadDelay);
		}

		int nByteRead = RS485_Read(hComm, pBuf, iToRead);

#ifdef __PRINT_MDB_COMM_DATA
		printf("MDB RECV:");
		int i;
		for(i = 0; i < nByteRead; i++)
		{
			printf(" %c[%2x]", *(char *)(pBuf + i), *(pBuf + i));
		}
		printf("\n\n");
		printf("Rec Data Length = %d\n\n",nByteRead);
#endif

		return nByteRead;
	}
	else
	{
		//��ʱ��������
		return 0;
	}

}

#ifdef __PRINT_MDB_COMM_DATA

static void MDBEXT_PrintData(int iType)
{
	static int				s_iCount = 0;
	MDB_INFO				*pstMdb = &sg_stMdbInfo;
	MDB_CFG_COMMON			*pstCfg = &pstMdb->stCommonCfg;
	MDB_CFG_GROUP_ITEM		*pstCfgGroupList = pstCfg->pstGroupList, *pstCurCfgGroup = NULL;
	MDB_CFG_TABLE			*pstCfgTableList = pstCfg->pstTableList;
	MDB_CFG_TABLE			*pstCurCfgTable = NULL;
	MDB_RUNINFO_GROUP		*pstGroupList = pstMdb->pstGroupList, *pstCurGroup = NULL;
	MDB_RUNINFO_EQUIPMENT	*pstCurEquip = NULL;
	int						i, iGroup, iNum;

	if(iType == 1)
	{
		s_iCount++;
		TRACE_MDB("-------------[%d]start-----------------\n", s_iCount);
		TRACE_MDB("pstMdb->iGroupNum=%d, iLoadCfgFlag=%d, iInitFlag=%d\n", 
			pstMdb->iGroupNum, pstMdb->iLoadCfgFlag, pstMdb->iInitFlag);

		for(iGroup = 0; iGroup < pstMdb->iGroupNum; iGroup++)
		{
			pstCurGroup = &pstGroupList[iGroup];
			TRACE_MDB("pstCurGroup[%d]->iActiveEquipNum=%d, iEquipNum=%d\n",
				iGroup,
				pstCurGroup->iActiveEquipNum,
				pstCurGroup->iEquipNum);
			for(i = 0; i < pstCurGroup->iEquipNum; i++)
			{
				pstCurEquip = &pstCurGroup->pstEquipList[i];
				TRACE_MDB("pstCurEquip[%d]->iChannelStart=%d, iChannelEnd=%d, iEquipID=%d, iOffsetAddress=%d, iOffsetCommBreakTimes=%d, iOffsetCommFail=%d, iOffsetExistence=%d, iRoughDataNum=%d\n", 
					i,
					pstCurEquip->iChannelStart,
					pstCurEquip->iChannelEnd,
					pstCurEquip->iEquipID,
					pstCurEquip->iOffsetAddress,
					pstCurEquip->iOffsetCommBreakTimes,
					pstCurEquip->iOffsetCommFail,
					pstCurEquip->iOffsetExistence,
					pstCurEquip->iRoughDataNum);
			}
		}
		TRACE_MDB("-------------[%d]end ---------------\n", s_iCount);
	}
	else if(iType == 2)
	{
		s_iCount++;
		TRACE_MDB("-------------[%d]start-----------------\n", s_iCount);
		for(iGroup = 0; iGroup < pstMdb->iGroupNum; iGroup++)
		{
			pstCurCfgTable = &pstCfgTableList[iGroup];
			pstCurCfgGroup = &pstCfgGroupList[iGroup];
			//[MODBUS_GROUP]
			TRACE_MDB("MODBUS_GROUP[%d]->iID=%d, iConfigTableID=%d, iGroupEquipmentID=%d, "
				"iGroupChannelStart=%d, iProductInfoScanMethod=%d, iModbusAddrStart=%d\n",
				iGroup,
				pstCurCfgGroup->iID, 
				pstCurCfgGroup->iConfigTableID,
				pstCurCfgGroup->iGroupEquipmentID,
				pstCurCfgGroup->iGroupChannelStart,
				pstCurCfgGroup->iProductInfoScanMethod,
				pstCurCfgGroup->iModbusAddrStart);
			//COMMUNICATION_TABLE
			iNum = pstCurCfgTable->stCfgCommTable.iNum;
			for(i = 0; i < iNum; i++)
			{
				TRACE_MDB("stCfgCommTable([%d][%d/%d])->ID=%d,iPortNum=%d,pszPortAttr=%s,"
					"iOpenTimeout=%d,iOpenRetryMaxCount=%d,iOpenRetryDelay=%d,pszModbusTransMode=%s,"
					"uiModbusValueOff=%0x,uiModbusValueOn=%0x\n", 
					iGroup,
					i,
					iNum - 1,
					pstCurCfgTable->stCfgCommTable.pstItem[i].iID,
					pstCurCfgTable->stCfgCommTable.pstItem[i].iPortNum,
					pstCurCfgTable->stCfgCommTable.pstItem[i].pszPortAttr,
					pstCurCfgTable->stCfgCommTable.pstItem[i].iOpenTimeout,
					pstCurCfgTable->stCfgCommTable.pstItem[i].iOpenRetryMaxCount,
					pstCurCfgTable->stCfgCommTable.pstItem[i].iOpenRetryDelay,
					pstCurCfgTable->stCfgCommTable.pstItem[i].pszModbusTransMode,
					pstCurCfgTable->stCfgCommTable.pstItem[i].uiModbusValueOff,
					pstCurCfgTable->stCfgCommTable.pstItem[i].uiModbusValueOn
					);
			}

			//ROUGH_DATA_TABLE
			iNum = pstCurCfgTable->stCfgRoughDataTable.iNum;
			for(i = 0; i < iNum; i++)
			{
				TRACE_MDB("stCfgRoughDataTable([%d][%d/%d])->iID=%d,dfOffset=%f,"
					"dfScale=%f,dfDefaultVal=%f,iRegAddrStart=%d,iRegQuantity=%d\n",
					iGroup,
					i,
					iNum - 1,
					pstCurCfgTable->stCfgRoughDataTable.pstItem[i].iID,
					pstCurCfgTable->stCfgRoughDataTable.pstItem[i].dfOffset,
					pstCurCfgTable->stCfgRoughDataTable.pstItem[i].dfScale,
					pstCurCfgTable->stCfgRoughDataTable.pstItem[i].dfDefaultVal,
					pstCurCfgTable->stCfgRoughDataTable.pstItem[i].iRegAddrStart,
					pstCurCfgTable->stCfgRoughDataTable.pstItem[i].iRegQuantity
					);
			}

			//SAMPLE_TABLE
			iNum = pstCurCfgTable->stCfgSampleTable.iNum;
			for(i = 0; i < iNum; i++)
			{
				TRACE_MDB("stCfgSampleTable([%d][%d/%d])->iID=%d,iSampleMethod=%d,"
					"iModbusFunID=%d,iSplitCount=%d,iRegAddrStart=%d,iRegQuantity=%d,"
					"iDataType=%d,iRoughDataIDStart=%d,iResponseTimeout=%d,iReadDelay=%d,"
					"pszResponseKeyStr=%s\n", 
					iGroup,
					i,
					iNum - 1,
					pstCurCfgTable->stCfgSampleTable.pstItem[i].iID,
					pstCurCfgTable->stCfgSampleTable.pstItem[i].iSampleMethod,
					pstCurCfgTable->stCfgSampleTable.pstItem[i].iModbusFunID,
					pstCurCfgTable->stCfgSampleTable.pstItem[i].iSplitCount,
					pstCurCfgTable->stCfgSampleTable.pstItem[i].iRegAddrStart,
					pstCurCfgTable->stCfgSampleTable.pstItem[i].iRegQuantity,
					pstCurCfgTable->stCfgSampleTable.pstItem[i].iDataType,
					pstCurCfgTable->stCfgSampleTable.pstItem[i].iRoughDataIDStart,
					pstCurCfgTable->stCfgSampleTable.pstItem[i].iResponseTimeout,
					pstCurCfgTable->stCfgSampleTable.pstItem[i].iReadDelay,
					pstCurCfgTable->stCfgSampleTable.pstItem[i].pszResponseKeyStr
					);
			}

			//STUFF_CHANNEL_TABLE
			iNum = pstCurCfgTable->stCfgStuffChanTable.iNum;
			for(i = 0; i < iNum; i++)
			{
				TRACE_MDB("stCfgStuffChanTable([%d][%d/%d])->iID=%d,iSampleChanStart=%d,"
					"iSampleChanQuantity=%d,iChanDataType=%d,iRoughDataIDStart=%d,iStuffMethod=%d,fStuffArg1=%f,"
					"fStuffArg2=%f\n", 
					iGroup,
					i,
					iNum - 1,
					pstCurCfgTable->stCfgStuffChanTable.pstItem[i].iID,
					pstCurCfgTable->stCfgStuffChanTable.pstItem[i].iSampleChanStart,
					pstCurCfgTable->stCfgStuffChanTable.pstItem[i].iSampleChanQuantity,
					pstCurCfgTable->stCfgStuffChanTable.pstItem[i].iChanDataType,
					pstCurCfgTable->stCfgStuffChanTable.pstItem[i].iRoughDataIDStart,
					pstCurCfgTable->stCfgStuffChanTable.pstItem[i].iStuffMethod,
					pstCurCfgTable->stCfgStuffChanTable.pstItem[i].fStuffArg1,
					pstCurCfgTable->stCfgStuffChanTable.pstItem[i].fStuffArg2
					);
			}

			//EQUIPMENT_TABLE
			iNum = pstCurCfgTable->stCfgEquipTable.iNum;
			for(i = 0; i < iNum; i++)
			{
				TRACE_MDB("stCfgEquipTable([%d][%d/%d])->iID=%d,iEquipIDStart=%d,"
					"iEquipQuantity=%d,iChannelStart=%d,iChannelQuantity=%d\n", 
					iGroup,
					i,
					iNum - 1,
					pstCurCfgTable->stCfgEquipTable.pstItem[i].iID,
					pstCurCfgTable->stCfgEquipTable.pstItem[i].iEquipIDStart,
					pstCurCfgTable->stCfgEquipTable.pstItem[i].iEquipQuantity,
					pstCurCfgTable->stCfgEquipTable.pstItem[i].iChannelStart,
					pstCurCfgTable->stCfgEquipTable.pstItem[i].iChannelQuantity
					);
			}
		}
		TRACE_MDB("-------------[%d]end ---------------\n", s_iCount);
	}

}
#endif

static int MDBEXT_Init(IN void* pDevice)
{
	MDB_INFO					*pstMdb = &sg_stMdbInfo;
	MDB_CFG_COMMON				*pstCfg = &pstMdb->stCommonCfg;
	MDB_CFG_TABLE				*pstCfgTableList = pstCfg->pstTableList;
	MDB_CFG_TABLE				*pstCurCfgTable = NULL;
	MDB_RUNINFO_GROUP			*pstGroupList = NULL, *pstCurGroup = NULL;
	MDB_RUNINFO_EQUIPMENT		*pstCurEquip = NULL;
	int							iEquipNum = 0, iRoughDataNum = 0, iGroupNum = 0, iCount = 0;
	int							i, j, iGroup, iTemp;
	BOOL						bIsInitError = FALSE;

	UNUSED(pDevice);

	TRACE_MDB("MDBEXT_Init start\n");
	if(pstMdb->iLoadCfgFlag <= MDBEXT_FLAG_NONE)//config files must be loaded successfully at first
	{
		return MDB_ERROR_FAIL;
	}

	if(pstMdb->iInitFlag > MDBEXT_FLAG_NONE)//have been inited successfully
	{
		return MDB_ERROR_OK;
	}

	pstMdb->iInitFlag = MDBEXT_FLAG_FAIL;//init to MDBEXT_FLAG_FAIL


	//Equipment Num and Rough Data Num can't be 0
	iGroupNum = pstCfg->iGroupNum;
	if(iGroupNum <= 0)
	{
		pstMdb->iInitFlag = MDBEXT_FLAG_SUCCESS;
		pstMdb->pstGroupList = NULL;
		return MDB_ERROR_OK;
	}

	//malloc memory for Group run info 
	pstGroupList = NEW(MDB_RUNINFO_GROUP, iGroupNum);
	if(NULL == pstGroupList)
	{
		return MDB_ERROR_NO_MEMORY;
	}
	memset(pstGroupList, 0, iGroupNum*sizeof(MDB_RUNINFO_GROUP));

	bIsInitError = FALSE;
	for(iGroup = 0; iGroup < iGroupNum; iGroup++)
	{
		pstCurCfgTable = &pstCfgTableList[iGroup];
		pstCurGroup = &pstGroupList[iGroup];

		//compute total equipment number in equipment table
		iEquipNum = 0;
		for(i = 0; i < pstCurCfgTable->stCfgEquipTable.iNum; i++)
		{
			iEquipNum += pstCurCfgTable->stCfgEquipTable.pstItem[i].iEquipQuantity;
		}
		if(iEquipNum <= 0)//at least a equipment
		{
			bIsInitError = TRUE;
			break;
		}

		//malloc memory for Equipment run info in one Group
		pstCurGroup->pstEquipList = NEW(MDB_RUNINFO_EQUIPMENT, iEquipNum);
		if(NULL == pstCurGroup->pstEquipList)
		{
			bIsInitError = TRUE;
			break;
		}
		memset(pstCurGroup->pstEquipList, 0, iEquipNum*sizeof(MDB_RUNINFO_EQUIPMENT));
		pstCurGroup->iEquipNum = iEquipNum;

		//init Equipment data
		iCount = 0;
		for(i = 0; i < pstCurCfgTable->stCfgEquipTable.iNum; i++)
		{
			for(j = 0; j < pstCurCfgTable->stCfgEquipTable.pstItem[i].iEquipQuantity; j++)
			{
				pstCurEquip = &pstCurGroup->pstEquipList[iCount];

				pstCurEquip->iEquipID = pstCurCfgTable->stCfgEquipTable.pstItem[i].iEquipIDStart + j;
				pstCurEquip->iChannelStart = 
					pstCurCfgTable->stCfgEquipTable.pstItem[i].iChannelStart 
					+ j*pstCurCfgTable->stCfgEquipTable.pstItem[i].iChannelQuantity;
				pstCurEquip->iChannelEnd = 
					pstCurEquip->iChannelStart + pstCurCfgTable->stCfgEquipTable.pstItem[i].iChannelQuantity;

				iCount++;
			}
		}

		//rough data number in rough data table
		iRoughDataNum = pstCurCfgTable->stCfgRoughDataTable.iNum;
		if(iRoughDataNum <= 0)//at least a rough data
		{
			bIsInitError = TRUE;
			break;
		}

		//malloc memory for rough data run info in Equipment
		for(i = 0; i < pstCurGroup->iEquipNum; i++)
		{
			pstCurEquip = &pstCurGroup->pstEquipList[i];

			pstCurEquip->punRoughData = NEW(RS485_VALUE, iRoughDataNum);
			if(NULL == pstCurEquip->punRoughData)
			{
				bIsInitError = TRUE;
				break;
			}
			pstCurEquip->iRoughDataNum = iRoughDataNum;
			memset(pstCurEquip->punRoughData, 0, iRoughDataNum*sizeof(RS485_VALUE));

			//use default value in rough data table to init rough data
			for(j = 0; j < pstCurEquip->iRoughDataNum; j++)
			{
				pstCurEquip->punRoughData[j].fValue = pstCurCfgTable->stCfgRoughDataTable.pstItem[j].dfDefaultVal;

				//init special rough data index, for operating the special data with more faster speed
				iTemp = pstCurCfgTable->stCfgRoughDataTable.pstItem[j].iID;
				if(MDBEXT_RAUGH_DATA_ID_COMMFAIL == iTemp)
				{
					pstCurEquip->iOffsetCommFail = j;
				}
				else if(MDBEXT_RAUGH_DATA_ID_EXISTENCE == iTemp)
				{
					pstCurEquip->iOffsetExistence = j;
				}
				else if(MDBEXT_RAUGH_DATA_ID_ADDRESS == iTemp)
				{
					pstCurEquip->iOffsetAddress = j;
				}
				else if(MDBEXT_RAUGH_DATA_ID_COMMBREAKTIMES == iTemp)
				{
					pstCurEquip->iOffsetCommBreakTimes = j;
				}
			}

		}

		if(bIsInitError)
		{
			break;
		}
	}

	if(bIsInitError)
	{
		//if there are some errors, free memory
		for(iGroup = 0; iGroup < iGroupNum; iGroup++)
		{
			pstCurGroup = &pstGroupList[iGroup];
			for(i = 0; i < pstCurGroup->iEquipNum; i++)
			{
				pstCurEquip = &pstCurGroup->pstEquipList[i];
				SAFELY_DELETE(pstCurEquip->punRoughData);
			}
			SAFELY_DELETE(pstCurGroup->pstEquipList);
		}
		SAFELY_DELETE(pstGroupList);

		return MDB_ERROR_NO_MEMORY;
	}

	//store info to global var
	pstMdb->iGroupNum = iGroupNum;
	pstMdb->pstGroupList = pstGroupList;
	pstMdb->pbySendBuf = sg_MdbExtSendBuffer;
	pstMdb->pbyRecvBuf = sg_MdbExtRecvBuffer;
	pstMdb->iInitFlag = MDBEXT_FLAG_SUCCESS;
	TRACE_MDB("MDBEXT_Init end\n");


	//MDBEXT_PrintData(1);
	//MDBEXT_PrintData(2);


	return MDB_ERROR_OK;
}





static void MDBEXT_Exit(IN void* pDevice)
{
	MDB_INFO					*pstMdb = &sg_stMdbInfo;
	MDB_RUNINFO_GROUP			*pstGroupList = NULL, *pstCurGroup = NULL;
	MDB_RUNINFO_EQUIPMENT		*pstCurEquip = NULL;
	int							iGroup, i;

	UNUSED(pDevice);
	TRACE_MDB("MDBEXT_Exit start\n");

	pstGroupList = pstMdb->pstGroupList;

	for(iGroup = 0; iGroup < pstMdb->iGroupNum; iGroup++)
	{
		pstCurGroup = &pstGroupList[iGroup];
		for(i = 0; i < pstCurGroup->iEquipNum; i++)
		{
			pstCurEquip = &pstCurGroup->pstEquipList[i];
			SAFELY_DELETE(pstCurEquip->punRoughData);
		}
		SAFELY_DELETE(pstCurGroup->pstEquipList);
	}
	SAFELY_DELETE(pstGroupList);

	pstMdb->iGroupNum = 0;
	pstMdb->pstGroupList = NULL;
	pstMdb->pbySendBuf = NULL;
	pstMdb->pbyRecvBuf = NULL;
	pstMdb->iInitFlag = MDBEXT_FLAG_NONE;
	TRACE_MDB("MDBEXT_Exit end\n");

}


static BOOL MDBEXT_PortReady(IN RS485_COMM_PORT *pCommPort, IN int iGroup)
{
	MDB_INFO					*pstMdb = &sg_stMdbInfo;
	MDB_CFG_COMMON				*pstCfg = &pstMdb->stCommonCfg;
	MDB_CFG_TABLE				*pstCurCfgTable = NULL;
	MDB_CFG_COMM_TABLE_ITEM		*pstCfgCommTableItem = NULL;
	char						szCurPortParam[RS485_OPEN_PARAM_STR_MAX];
	char						*pcChr = NULL;
	int							iOpenTimes = 0, iErrCode = 0;
	int							i;

	ASSERT(pCommPort);

	TRACE_MDB("MDBEXT_PortReady start\n");
	if(pstMdb->iInitFlag <= MDBEXT_FLAG_NONE)//must be init at first
	{
		return FALSE;
	}
	//init pointer
	pstCurCfgTable = &pstCfg->pstTableList[iGroup];
	pstCfgCommTableItem = &pstCurCfgTable->stCfgCommTable.pstItem[0];

	//check whether or not port has been ready
	if(pCommPort->bOpened)
	{
		//remove space char from pCommPort->szOpenParamStr
		//eg: convert "19200, n, 8, 1" to "19200,n,8,1"
		pcChr = szCurPortParam;
		for(i = 0; i < (int)strlen(pCommPort->szOpenParamStr); i++)
		{
			if(' ' != pCommPort->szOpenParamStr[i])
			{
				*pcChr = pCommPort->szOpenParamStr[i];
				pcChr++;
			}
		}
		*pcChr = 0;//end with 0


		//TRACE_MDB("szCurPortParam=%s,pszPortAttr=%s\n", szCurPortParam, pstCfgCommTableItem->pszPortAttr);
		//compare port param
		if(0 == strnicmp(szCurPortParam, pstCfgCommTableItem->pszPortAttr, sizeof(szCurPortParam)))
		{
			return TRUE;//param no change
		}
	}
	TRACE_MDB("MDBEXT_PortReady open again\n");
	//start init port

	if(pCommPort->bOpened)
	{
		RS485_Close(pCommPort->hCommPort);
		pCommPort->bOpened = FALSE;
	}

	iOpenTimes = 0;
	while(iOpenTimes < pstCfgCommTableItem->iOpenRetryMaxCount)
	{
		pCommPort->hCommPort =
			RS485_Open(pstCfgCommTableItem->pszPortAttr, pstCfgCommTableItem->iOpenTimeout, &iErrCode);

		if(iErrCode == ERR_COMM_OK)
		{
			pCommPort->bOpened = TRUE;
			//set a special value "-1" for forcing other rs485 samplers open again
			//in rs485_modbus, just use pCommPort->szOpenParamStr to compare last opened status
			//the value pCommPort->szOpenParamStr will be setted in the function RS485_Open
			//don't use pCommPort->enumAttr and pCommPort->enumAttr
			pCommPort->enumAttr = -1;
			pCommPort->enumBaud = -1;

			TRACE_MDB("MDBEXT_PortReady successful\n");
			TRACE_FILE_LINE_FUN_ENTER("Open RS485 port OK in MDBEXT!");
			return TRUE;
		}
		else
		{
			Sleep((unsigned int)pstCfgCommTableItem->iOpenRetryDelay);
			TRACE_FILE_LINE_FUN_ENTER("Open RS485 port fail!");
			pCommPort->bOpened = FALSE;

			//TRACE_MDB("MDBEXT_PortReady failed\n");

			//return FALSE;
		}
		iOpenTimes++;
	}
	TRACE_MDB("MDBEXT_PortReady timeout\n");

	return TRUE;
}



static int MDBEXT_ExecFun17(IN HANDLE hComm,
							IN int iGroup,
							IN int iEquip,
							IN int iSampleItemOffset,
							IN int iAddr)
{
	MDB_INFO						*pstMdb = &sg_stMdbInfo;
	MDB_CFG_COMMON					*pstCfg = &pstMdb->stCommonCfg;
	MDB_CFG_TABLE					*pstCfgTableList = pstCfg->pstTableList;
	MDB_CFG_TABLE					*pstCurCfgTable = NULL;
	MDB_CFG_GROUP_ITEM				*pstCurCfgGroup = NULL;
	MDB_CFG_SAMPLE_TABLE_ITEM		*pstCurCfgSampleItem = NULL;
	MDB_RUNINFO_GROUP				*pstCurGroup = NULL;
	MDB_RUNINFO_EQUIPMENT			*pstCurEquip = NULL;
	BYTE							*pbyData = NULL, byTemp;
	int								iOneDataByteCount = 0, iBaseCharNum = 0, iDataByteCount = 0;
	int								i, iLen = 0;
	int								iTemp;
	WORD							wCRC;

	TRACE_MDB("MDBEXT_ExecFun17 start\n");

	////init pointer
	pstCurCfgGroup = &pstCfg->pstGroupList[iGroup];
	pstCurCfgTable = &pstCfgTableList[iGroup];
	pstCurCfgSampleItem = &pstCurCfgTable->stCfgSampleTable.pstItem[iSampleItemOffset];
	pstCurGroup = &pstMdb->pstGroupList[iGroup];
	pstCurEquip = &pstCurGroup->pstEquipList[iEquip];


	////select proper process method
	if(0 == stricmp(pstCurCfgTable->stCfgCommTable.pstItem[0].pszModbusTransMode, MDBEXT_TRANS_MODE_ASCII))
	{
		if(MDBEXT_DATATYPE_ID_10 == pstCurCfgSampleItem->iDataType)
		{
			iOneDataByteCount = pstCurCfgSampleItem->iDataType/10;//for MDBEXT_DATATYPE_ID_10, one data is one byte
		//start(1 char, ':'), address(2 char), function(2 char), data len(2 char), data(n*2 char), LRC(2 char), end(2 char, 'CR', 'LF')
			iBaseCharNum = 11;//start(1) + address(2) + function(2) + data len(2) + LRC(2) + end(2)
			//TRACE_MDB("MDBEXT_ExecFun17::iOneDataByteCount=%d, iBaseCharNum=%d\n",
			//	iOneDataByteCount,
			//	iBaseCharNum);

			//1. prepare data
			//pack data
			pbyData = pstMdb->pbySendBuf;
			iLen = snprintf((char *)pbyData,
				MDBEXT_BUF_LEN,
				":%02X%02X%02X%02X%02X%02X",
				(BYTE)iAddr,
				(BYTE)pstCurCfgSampleItem->iModbusFunID,
				0x00,  0x00,0x00,0x00
				);
			//pack LRC and stop char
			byTemp = MDBEXT_CalcLRC(pbyData + 1, iLen - 1);
			iLen += snprintf((char *)(pbyData + iLen),
				10,
				"%02X%c%c",
				byTemp,
				MDBEXT_ASCII_MODE_STOP_CHR_HI,
				MDBEXT_ASCII_MODE_STOP_CHR_LO);
			pbyData[iLen] = 0;//end with 0

			//TRACE_MDB("MDBEXT_ExecFun17::iLen=%d, packet=%s\n", iLen, (char *)pbyData);

			//2.send data
			if(FALSE == MDB_SendCmd(hComm, pbyData, iLen))
			{
				//TRACE_MDB("MDBEXT_ExecFun17::MDB_SendCmd ERROR\n");
				return MDB_ERROR_FAIL;
			}

			//3.receive response
			pbyData = pstMdb->pbyRecvBuf;
			iLen = MDBEXT_GetResponse(hComm,
				pbyData,
				MDBEXT_BUF_LEN,
				pstCurCfgSampleItem->iResponseTimeout,
				pstCurCfgSampleItem->iReadDelay);
			pbyData[iLen] = 0;//end with 0

			//TRACE_MDB("MDBEXT_ExecFun17::MDBEXT_GetResponse iLen=%d, iResponseTimeout=%d, iReadDelay=%d, packet=%s\n",
			//	iLen,
			//	pstCurCfgSampleItem->iResponseTimeout,
			//	pstCurCfgSampleItem->iReadDelay,
			//	(char *)pbyData);

			//a.check receive data len
			if(iLen < iBaseCharNum)
			{
				return MDB_ERROR_RESPOND_DATA_LEN;
			}
			//b.check start char
			//TRACE_MDB("start=%c\n", pbyData[0]);
			if(MDBEXT_ASCII_MODE_START_CHR != pbyData[0])
			{
				return MDB_ERROR_FAIL;
			}
			//c.check address
			byTemp = AsciiHexToChar(pbyData + 1);
			//TRACE_MDB("addr=%d\n", byTemp);
			if(byTemp != (unsigned int)iAddr)
			{
				return MDB_ERROR_ADDR;
			}
			//d.check function
			byTemp = AsciiHexToChar(pbyData + 3);
			//TRACE_MDB("fun=%d\n", byTemp);
			if(byTemp != (unsigned int)pstCurCfgSampleItem->iModbusFunID)
			{
				return MDB_ERROR_FUN_CODE;
			}
			//e.check data len
			iDataByteCount = AsciiHexToChar(pbyData + 5);
			//TRACE_MDB("data len=%d\n", iDataByteCount);
			if (iDataByteCount*2 + iBaseCharNum > iLen)//packet too short
			{
				return MDB_ERROR_RESPOND_DATA_LEN;
			}
			//f.Check LRC
			byTemp = AsciiHexToChar(pbyData + 7 + iDataByteCount*2);
			//TRACE_MDB("LRC=%d\n", byTemp);
			//TRACE_MDB("calc LRC=%d\n", MDBEXT_CalcLRC(pbyData + 1, 6 + iDataByteCount*2));
			if (byTemp != MDBEXT_CalcLRC(pbyData + 1, 6 + iDataByteCount*2))
			{
				return MDB_ERROR_CRC;
			}

			//parse data
			pbyData += 7;//move to start position of data filed 
			for(i = 0; i < iDataByteCount; i++)
			{
				byTemp = AsciiHexToChar(pbyData);//high byte
				pbyData += 2;
				pstMdb->pbySendBuf[i] = byTemp;//just store here for a short time
			}
			pstMdb->pbySendBuf[i] = 0;

			if(MDBEXT_SCAN_METHOD_SONICK_BATTERY == pstCurCfgGroup->iProductInfoScanMethod)
			{
				//data format:<0x02><0xFF>48TL200<Space>XXXXXXXXXX
				//get produce info for SoNick Battery
				if(i >= 20)
				{
					strncpyz(pstCurEquip->stPI.szPartNumber, (char *)pstMdb->pbySendBuf + 2, 8);//48TL200
					strncpyz(pstCurEquip->stPI.szSerialNumber, (char *)pstMdb->pbySendBuf + 10, 11);//XXXXXXXXXX
					//TRACE_MDB("szPartNumber=%s,szSerialNumber=%s\n",
					//	pstCurEquip->stPI.szPartNumber,
					//	pstCurEquip->stPI.szSerialNumber);
				}
				else
				{
					TRACE_MDB("MDBEXT_ExecFun17::ERROR pbySendBuf=%s\n", pstMdb->pbySendBuf);
				}
			}

			//check response string
			//TRACE_MDB("MDBEXT_ExecFun17::pbySendBuf=%s, pszResponseKeyStr=%s\n",
			//	pstMdb->pbySendBuf,
			//	pstCurCfgSampleItem->pszResponseKeyStr);

			iTemp = strlen(pstCurCfgSampleItem->pszResponseKeyStr);
			if(iTemp > 0)
			{
				if(iTemp > iDataByteCount)
				{
					return MDB_ERROR_OTHER;
				}
				else
				{
					for(i = 0; i <= iDataByteCount - iTemp; i++)
					{
						if((unsigned char)pstCurCfgSampleItem->pszResponseKeyStr[0] == (unsigned char)pstMdb->pbySendBuf[i])
						{
							if(0 == memcmp((void *)pstCurCfgSampleItem->pszResponseKeyStr, (void *)&pstMdb->pbySendBuf[i], iTemp))
							{
								break;
							}
						}
					}

					if(i > iDataByteCount - iTemp)//can't find key string in receive buffer
					{
						return MDB_ERROR_OTHER;
					}
				}
			}

			TRACE_MDB("MDBEXT_ExecFun17 OK\n");
			return MDB_ERROR_OK;
		}
	}
	else if(0 == stricmp(pstCurCfgTable->stCfgCommTable.pstItem[0].pszModbusTransMode, MDBEXT_TRANS_MODE_RTU))
	{
		if(MDBEXT_DATATYPE_ID_10 == pstCurCfgSampleItem->iDataType)
		{
			iOneDataByteCount = pstCurCfgSampleItem->iDataType/10;//for MDBEXT_DATATYPE_ID_10, one data is one byte
			//address(1 char), function(1 char), data len(1 char), data(n char), CRC(2 char)
			iBaseCharNum = 5;//address(1) + function(1) + data len(1) + CRC(2)
			//TRACE_MDB("MDBEXT_ExecFun17::iOneDataByteCount=%d, iBaseCharNum=%d\n",
			//	iOneDataByteCount,
			//	iBaseCharNum);
		
			//1. prepare data
			//pack data
			pbyData = pstMdb->pbySendBuf;
			iLen = 0;
			pbyData[iLen++] = (BYTE)iAddr;
			pbyData[iLen++] = (BYTE)pstCurCfgSampleItem->iModbusFunID;
			pbyData[iLen++] =0x00;
			pbyData[iLen++] =0x00;
			pbyData[iLen++] =0x00;
			pbyData[iLen++] =0x00;

			wCRC = MDB_CRC(pbyData, iLen);
			pbyData[iLen++] = HIBYTE(wCRC);
			pbyData[iLen++] = LOBYTE(wCRC);
			
			//TRACE_MDB("MDBEXT_ExecFun17::iLen=%d, packet=%s\n", iLen, (char *)pbyData);

			//2.send data
			if(FALSE == MDB_SendCmd(hComm, pbyData, iLen))
			{
				TRACE_MDB("MDBEXT_ExecFun17::MDB_SendCmd ERROR\n");
				return MDB_ERROR_FAIL;
			}

			//3.receive response
			pbyData = pstMdb->pbyRecvBuf;
			iLen = MDBEXT_GetResponse(hComm,
				pbyData,
				MDBEXT_BUF_LEN,
				pstCurCfgSampleItem->iResponseTimeout,
				pstCurCfgSampleItem->iReadDelay);
			pbyData[iLen] = 0;//end with 0

			//TRACE_MDB("MDBEXT_ExecFun17::MDBEXT_GetResponse iLen=%d, iResponseTimeout=%d, iReadDelay=%d, packet=%s\n",
			//	iLen,
			//	pstCurCfgSampleItem->iResponseTimeout,
			//	pstCurCfgSampleItem->iReadDelay,
			//	(char *)pbyData);
			
			//a.check receive data len
//			TRACE_MDB2("iLen=%d\n", iLen);
			if(iLen < iBaseCharNum)
			{
				TRACE_MDB2("Receive data length error, iLen=%d\n", iLen);
				return MDB_ERROR_RESPOND_DATA_LEN;
			}
			//b.check address
			byTemp = pbyData[0];
//			TRACE_MDB2("addr=%d\n", byTemp);
			if(byTemp != (unsigned int)iAddr)
			{
				TRACE_MDB2("Address error, addr=%d\n", byTemp);
				return MDB_ERROR_ADDR;
			}
			//c.check function
			byTemp = pbyData[1];
//			TRACE_MDB2("fun=%d\n", byTemp);
			if(byTemp != (unsigned int)pstCurCfgSampleItem->iModbusFunID)
			{
				TRACE_MDB2("Function code error, fun=%d\n", byTemp);
				return MDB_ERROR_FUN_CODE;
			}
			//d.check data len
			iDataByteCount = pbyData[2];
//			TRACE_MDB2("data len=%d\n", iDataByteCount);
			if (iDataByteCount + iBaseCharNum > iLen)//packet too short
			{
				TRACE_MDB2("Error packet length, data len=%d\n", iDataByteCount);
				return MDB_ERROR_RESPOND_DATA_LEN;
			}
			//e.Check CRC
			wCRC = MDB_CRC(pbyData, 3 + iDataByteCount);
			byTemp = pbyData[3 + iDataByteCount];
//			TRACE_MDB2("wCRC=%x,crc[1]=%2x\n", wCRC, byTemp);
			if (byTemp != HIBYTE(wCRC))
			{
				TRACE_MDB2("CRC error, wCRC=%x,crc[1]=%2x\n", wCRC, byTemp);
				return MDB_ERROR_CRC;
			}
			byTemp = pbyData[4 + iDataByteCount];
//			TRACE_MDB2("wCRC=%x,crc[0]=%2x\n", wCRC, byTemp);
			if (byTemp != LOBYTE(wCRC))
			{
				TRACE_MDB2("CRC error, wCRC=%x,crc[0]=%2x\n", wCRC, byTemp);
				return MDB_ERROR_CRC;
			}

			//parse data
			pbyData += 3;//move to start position of data filed 
			for(i = 0; i < iDataByteCount; i++)
			{
				pstMdb->pbySendBuf[i] = pbyData[i];//just store here for a short time
				
			}
			pstMdb->pbySendBuf[i] = 0;

			if(MDBEXT_SCAN_METHOD_NARADA_BMS == pstCurCfgGroup->iProductInfoScanMethod)
			{
				//get produce info for Narada BMS
				//data format:Model+��*��+SW Version+��*��+HW Version +��*��+ Serial Number+��*��
				//Model is 10 bytes at most. eg. 48NPFC70 - X
				//SW Version is 2 bytes. eg. 0x0A0A is V10.10
				//HW Version is 5 bytes. eg 0x010a0b0200 is V01.10.11.02.00
				//Serial Number is 20 bytes. eg. 14875113011800400025
				if(iDataByteCount >= 11)//Model+��*��
				{
					//find Model
					for(i = 0; i < 12; i++)
					{
						if('*' == pstMdb->pbySendBuf[i])
						{
							break;
						}
					}
					
					if(i < 12)//find char "*", so there is Model info
					{
						int iTempModelLen = i;
						char *pszProductInfo = pstMdb->pbySendBuf;
						
						if(iDataByteCount >= (iTempModelLen + 1) + (2 + 1) + (5 + 1) + 20)//len of "Model+��*��+SW Version+��*��+HW Version +��*��+ Serial Number"
						{
							//Model
							strncpyz(pstCurEquip->stPI.szPartNumber, pszProductInfo, iTempModelLen + 1);//48NPFC70 - X
							pszProductInfo += (iTempModelLen + 1);
							//SW Version
							snprintf(pstCurEquip->stPI.szSWVersion,
								sizeof(pstCurEquip->stPI.szSWVersion),
								"%d.%02d",
								pszProductInfo[0],
								pszProductInfo[1]);
							pszProductInfo += (2 + 1);
							//HW Version
							snprintf(pstCurEquip->stPI.szHWVersion,
								sizeof(pstCurEquip->stPI.szHWVersion),
								"%02d.%02d.%02d.%02d.%02d",
								pszProductInfo[0],
								pszProductInfo[1],
								pszProductInfo[2],
								pszProductInfo[3],
								pszProductInfo[4]);
							pszProductInfo += (5 + 1);
							//Serial Number
							strncpyz(pstCurEquip->stPI.szSerialNumber, pszProductInfo, 20 + 1);
							
							TRACE_MDB("szPartNumber=%s,szSWVersion=%s,szHWVersion=%s,szSerialNumber=%s\n",
								pstCurEquip->stPI.szPartNumber,
								pstCurEquip->stPI.szSWVersion,
								pstCurEquip->stPI.szHWVersion,
								pstCurEquip->stPI.szSerialNumber);
		AppLogOut("RS485_MDB", (APP_LOG_INFO), "Narada battery type=%s sw=%s hw=%s sn=%s\n", 
								pstCurEquip->stPI.szPartNumber,
								pstCurEquip->stPI.szSWVersion,
								pstCurEquip->stPI.szHWVersion,
								pstCurEquip->stPI.szSerialNumber);
								
						}
						
					}
				}
			}
			
			
			//check response string
			//TRACE_MDB("MDBEXT_ExecFun17::pbySendBuf=%s, pszResponseKeyStr=%s\n",
			//	pstMdb->pbySendBuf,
			//	pstCurCfgSampleItem->pszResponseKeyStr);
			//find key string in receive packets
			iTemp = strlen(pstCurCfgSampleItem->pszResponseKeyStr);
			if(iTemp > 0)
			{
				if(iTemp > iDataByteCount)
				{
					return MDB_ERROR_OTHER;
				}
				else
				{
					for(i = 0; i <= iDataByteCount - iTemp; i++)
					{
						if((unsigned char)pstCurCfgSampleItem->pszResponseKeyStr[0] == (unsigned char)pstMdb->pbySendBuf[i])
						{
							if(0 == memcmp((void *)pstCurCfgSampleItem->pszResponseKeyStr, (void *)&pstMdb->pbySendBuf[i], iTemp))
							{
								break;
							}
						}
					}

					if(i > iDataByteCount - iTemp)//can't find key string in receive buffer
					{
						return MDB_ERROR_OTHER;
					}
				}
			}

			TRACE_MDB("MDBEXT_ExecFun17 OK\n");
			return MDB_ERROR_OK;
		}
	}

	TRACE_MDB("MDBEXT_ExecFun17 END\n");
	return MDB_ERROR_OTHER;
}

static void CreateProductVersionStr(IN char *pBuf,
							IN int iLen,
							OUT char *szVersion)
{
	char str[10] = {0};
	int i;
	
	memset(szVersion, 0x00, MDBEXT_PRODUCT_SN_MAX_LEN);
	for ( i = 0 ; i < iLen ; i++ )
	{
		sprintf(str, "%d", pBuf[i]);
		strcat(szVersion, str);
		if ( i < (iLen - 1) )
		{
			strcat(szVersion, ".");
		}	
	}
	
}								

static void ParseProductInfo(IN char *pBuf,
							IN int iDataByteCount,
							IN int iGroup,
							IN int iEquip,
							IN int iSampleItemOffset)
{
	MDB_CFG_COMMON					*pstCfg = &sg_stMdbInfo.stCommonCfg;
	MDB_CFG_TABLE					*pstCfgTableList = pstCfg->pstTableList;
	MDB_CFG_TABLE					*pstCurCfgTable = NULL;
	MDB_CFG_GROUP_ITEM				*pstCurCfgGroup = NULL;
	MDB_CFG_SAMPLE_TABLE_ITEM		*pstCurCfgSampleItem = NULL;
	MDB_RUNINFO_EQUIPMENT			*pstCurEquip = NULL;
	MDB_CFG_PRODUCT_INFO_TABLE_ITEM *pstCurCfgProdInfoItem = NULL;
	int								i;
	char							*ptempBuf[MDBEXT_MAX_PRODUCT_INFO_ITEMS] = {0};
	int								tempBufLen[MDBEXT_MAX_PRODUCT_INFO_ITEMS] = {0};
	char 							szVersion[MDBEXT_PRODUCT_SN_MAX_LEN];
	
	////init pointer
	pstCurCfgGroup = &pstCfg->pstGroupList[iGroup];
	pstCurCfgTable = &pstCfgTableList[iGroup];
	pstCurCfgSampleItem = &pstCurCfgTable->stCfgSampleTable.pstItem[iSampleItemOffset];
	pstCurEquip = &sg_stMdbInfo.pstGroupList[iGroup].pstEquipList[iEquip];
	pstCurCfgProdInfoItem = &pstCurCfgTable->stCfgProductInfoTable.pstItem[0];
	
	TRACE_MDB("\nIn ParseProductInfo, Equip = %d\n", pstCurEquip->iEquipID);
	AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, " In ParseProductInfo");
	switch ( pstCurCfgGroup->iProductInfoScanMethod )
	{
		case MDBEXT_SCAN_METHOD_SONICK_BATTERY:
		{
			//data format:<0x02><0xFF>48TL200<Space>XXXXXXXXXX
			//get produce info for SoNick Battery
			if(i >= 20)
			{
				strncpyz(pstCurEquip->stPI.szPartNumber, (char *)pBuf + 2, 8);//48TL200
				strncpyz(pstCurEquip->stPI.szSerialNumber, (char *)pBuf + 10, 11);//XXXXXXXXXX
				//TRACE_MDB("szPartNumber=%s,szSerialNumber=%s\n",
				//	pstCurEquip->stPI.szPartNumber,
				//	pstCurEquip->stPI.szSerialNumber);
			}
			else
			{
				TRACE_MDB("ParseProductInfo::ERROR pBuf=%s\n", pBuf);
			}
			break;
		}
				
		case MDBEXT_SCAN_METHOD_SEPARATOR:
		{
			int iStrCount;
			char *pStrStart, *p;
			BOOL string_end_found = FALSE;
					
			p = pStrStart = ptempBuf[0] = pBuf;
			for ( i = 0, iStrCount = 0 ; i < iDataByteCount ; i++, p++)
			{
				TRACE_MDB("%02x ", *p);	
				if ( string_end_found )
				{
					iStrCount++;
					pStrStart = ptempBuf[iStrCount] = p;	
					string_end_found = FALSE;
				}
				if ( *p == (char)pstCurCfgProdInfoItem->iItemSeparator )
				{
					string_end_found = TRUE;
					*p = 0;
					tempBufLen[iStrCount] = (p - pStrStart);
					TRACE_MDB("\nParseProductInfo: String %d length = %d\n", iStrCount, tempBufLen[iStrCount]);
					AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "ParseProductInfo: String %d length = %d\n", iStrCount, tempBufLen[iStrCount]);
				}
			}
					
				// We do the following in case string terminator is not found
			if ( FALSE == string_end_found )
			{
				tempBufLen[iStrCount] = p - pStrStart;	
				TRACE_MDB("ParseProductInfo: String %d length = %d\n", iStrCount, tempBufLen[iStrCount]);
				AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "ParseProductInfo: String %d length = %d\n", iStrCount, tempBufLen[iStrCount]);
			}	

			for ( i = 0 ; i < pstCurCfgTable->stCfgProductInfoTable.iNum ; i++ )
			{
				pstCurCfgProdInfoItem = &pstCurCfgTable->stCfgProductInfoTable.pstItem[i];
						
					// Match Read Identifier if configured
				if ( (MDBEXT_CFG_SPEC_VAL_NA_NUM != pstCurCfgProdInfoItem->iReadIdentifier) &&
					(pstCurCfgProdInfoItem->iReadIdentifier != pstCurCfgSampleItem->iModbusData) )
				{
//					TRACE_MDB("ParseProductInfo::ERROR Read Identifier does not match\n");
//					AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "ParseProductInfo::ERROR Read Identifier does not match, Modbus Group %d  %04x  %04x\n", iGroup+1, pstCurCfgProdInfoItem->iReadIdentifier, pstCurCfgSampleItem->iModbusData);
					continue;
				}	
						
				switch ( i )
				{
					case MDBEXT_PRODUCT_ID_INDEX:
					{
						if ( MDBEXT_RECONFIG_PARSE_STR == pstCurCfgProdInfoItem->iParseMethod )
						{
							strcpy(pstCurEquip->stPI.szPartNumber, (char *)ptempBuf[pstCurCfgProdInfoItem->iItemNumber-1]+pstCurCfgProdInfoItem->iItemOffset);
							TRACE_MDB("ParseProductInfo: Product style is %s\n", pstCurEquip->stPI.szPartNumber);
							AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "Product style is %s\n", pstCurEquip->stPI.szPartNumber);
						}
						break;
					}
					
					case MDBEXT_PRODUCT_SN_INDEX:
					{
						if ( MDBEXT_RECONFIG_PARSE_STR == pstCurCfgProdInfoItem->iParseMethod )
						{
							strcpy(pstCurEquip->stPI.szSerialNumber, ptempBuf[pstCurCfgProdInfoItem->iItemNumber-1]+pstCurCfgProdInfoItem->iItemOffset);
							TRACE_MDB("ParseProductInfo: Product SN is %s\n", pstCurEquip->stPI.szSerialNumber);
							AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "Product SN is %s",pstCurEquip->stPI.szSerialNumber);
						}
						break;
					}	
					
					case MDBEXT_PRODUCT_SWVERSION_INDEX:
					{
						if ( MDBEXT_PRODUCT_SN_MAX_LEN < tempBufLen[pstCurCfgProdInfoItem->iItemNumber-1] )
						{
							ptempBuf[MDBEXT_PRODUCT_SN_MAX_LEN] = 0;	
						}	
								
						if ( MDBEXT_RECONFIG_PARSE_STR == pstCurCfgProdInfoItem->iParseMethod )
						{
							strcpy(pstCurEquip->stPI.szSWVersion, ptempBuf[pstCurCfgProdInfoItem->iItemNumber-1]+pstCurCfgProdInfoItem->iItemOffset);
							AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "Product SWVER is %s",pstCurEquip->stPI.szSWVersion);
						}
						else if ( MDBEXT_RECONFIG_PARSE_INT_BYTE == pstCurCfgProdInfoItem->iParseMethod )
						{
									
							CreateProductVersionStr(ptempBuf[pstCurCfgProdInfoItem->iItemNumber-1], tempBufLen[pstCurCfgProdInfoItem->iItemNumber-1],
												szVersion);
							strncpyz(pstCurEquip->stPI.szSWVersion, szVersion, sizeof(szVersion));		
							AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "Product SWVER is %s",pstCurEquip->stPI.szSWVersion);
						}	
						else if ( MDBEXT_RECONFIG_PARSE_INT  == pstCurCfgProdInfoItem->iParseMethod )
						{
									
						}	
						break;
					}	
					
					case MDBEXT_PRODUCT_HWVERSION_INDEX:
					{
						if ( MDBEXT_PRODUCT_SN_MAX_LEN < tempBufLen[pstCurCfgProdInfoItem->iItemNumber-1] )
						{
							ptempBuf[MDBEXT_PRODUCT_SN_MAX_LEN] = 0;	
						}	
								
						if ( MDBEXT_RECONFIG_PARSE_STR == pstCurCfgProdInfoItem->iParseMethod )
						{
							strcpy(pstCurEquip->stPI.szHWVersion, ptempBuf[pstCurCfgProdInfoItem->iItemNumber-1]+pstCurCfgProdInfoItem->iItemOffset);
							AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "Product HWVER is %s",pstCurEquip->stPI.szHWVersion);
						}
						else if ( MDBEXT_RECONFIG_PARSE_INT_BYTE == pstCurCfgProdInfoItem->iParseMethod )
						{
									
							CreateProductVersionStr(ptempBuf[pstCurCfgProdInfoItem->iItemNumber-1], tempBufLen[pstCurCfgProdInfoItem->iItemNumber-1],
												szVersion);
							strncpyz(pstCurEquip->stPI.szHWVersion, szVersion, sizeof(szVersion));		
							AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "Product HWVER is %s",pstCurEquip->stPI.szHWVersion);
						}	
						else if ( MDBEXT_RECONFIG_PARSE_INT == pstCurCfgProdInfoItem->iParseMethod )
						{
									
						}	
						break;
					}	
				}	
								
			}
					
			break;		
		}
				
		case MDBEXT_SCAN_METHOD_LENGTH:
		{
			break;
		}			
	}	// end of switch ( pstCurCfgGroup->iProductInfoScanMethod )	
	TRACE_MDB("\nLeaving ParseProductInfo\n");
	AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "Leaving ParseProductInfo");
}	


static int MDBEXT_ExecFuncProdInfo(IN HANDLE hComm,
							IN int iGroup,
							IN int iEquip,
							IN int iSampleItemOffset,
							IN int iAddr)
{
	MDB_INFO						*pstMdb = &sg_stMdbInfo;
	MDB_CFG_TABLE					*pstCurCfgTable = NULL;
	MDB_CFG_SAMPLE_TABLE_ITEM		*pstCurCfgSampleItem = NULL;
	BYTE							*pbyData = NULL, byTemp;
	int								iOneDataByteCount = 0, iBaseCharNum = 0, iDataByteCount = 0;
	int								i, iLen = 0;
	int								iTemp;
	WORD							wCRC;
	char							output[500];
	char							str[10];
	char							log_msg[100];
	int								log_msg_len;
	int								log_msg_count;
	int								data_offset;

	TRACE_MDB("MDBEXT_ExecFuncProductInfo start\n");
/*
	////init pointer
	pstCurCfgGroup = &pstCfg->pstGroupList[iGroup];
	pstCurCfgTable = &pstCfgTableList[iGroup];
	pstCurCfgSampleItem = &pstCurCfgTable->stCfgSampleTable.pstItem[iSampleItemOffset];
	pstCurGroup = &pstMdb->pstGroupList[iGroup];
	pstCurEquip = &pstCurGroup->pstEquipList[iEquip];
	pstCurCfgProdInfoItem = &pstCurCfgTable->stCfgProductInfoTable.pstItem[0];
*/
	pstCurCfgTable = &sg_stMdbInfo.stCommonCfg.pstTableList[iGroup];
	pstCurCfgSampleItem = &sg_stMdbInfo.stCommonCfg.pstTableList[iGroup].stCfgSampleTable.pstItem[iSampleItemOffset];

	////select proper process method
	if(0 == stricmp(pstCurCfgTable->stCfgCommTable.pstItem[0].pszModbusTransMode, MDBEXT_TRANS_MODE_ASCII))
	{
		if(MDBEXT_DATATYPE_ID_10 == pstCurCfgSampleItem->iDataType)
		{
			iOneDataByteCount = pstCurCfgSampleItem->iDataType/10;//for MDBEXT_DATATYPE_ID_10, one data is one byte
		//start(1 char, ':'), address(2 char), function(2 char), data len(2 char), data(n*2 char), LRC(2 char), end(2 char, 'CR', 'LF')
			iBaseCharNum = 11;//start(1) + address(2) + function(2) + data len(2) + LRC(2) + end(2)
			//TRACE_MDB("MDBEXT_ExecFuncProductInfo::iOneDataByteCount=%d, iBaseCharNum=%d\n",
			//	iOneDataByteCount,
			//	iBaseCharNum);

			//1. prepare data
			//pack data
			pbyData = pstMdb->pbySendBuf;
			if ( MDBEXT_CFG_SPEC_VAL_NA_NUM == pstCurCfgSampleItem->iModbusData )
			{
				iLen = snprintf((char *)pbyData,
					MDBEXT_BUF_LEN,
					":%02X%02X%02X%02X%02X%02X",
					(BYTE)iAddr,
					(BYTE)pstCurCfgSampleItem->iModbusFunID,
					0x00, 0x00, 0x00, 0x00);
			}		
			else
			{
				iLen = snprintf((char *)pbyData,
					MDBEXT_BUF_LEN,
					":%02X%02X%02X%02X%02X%02X",
					(BYTE)iAddr,
					(BYTE)pstCurCfgSampleItem->iModbusFunID,
					(BYTE)(pstCurCfgSampleItem->iModbusData >> 8),
					(BYTE)pstCurCfgSampleItem->iModbusData,
					0x00, 0x00);
			}			
			//pack LRC and stop char
			byTemp = MDBEXT_CalcLRC(pbyData + 1, iLen - 1);
			iLen += snprintf((char *)(pbyData + iLen),
				10,
				"%02X%c%c",
				byTemp,
				MDBEXT_ASCII_MODE_STOP_CHR_HI,
				MDBEXT_ASCII_MODE_STOP_CHR_LO);
			pbyData[iLen] = 0;//end with 0

			//TRACE_MDB("MDBEXT_ExecFuncProductInfo::iLen=%d, packet=%s\n", iLen, (char *)pbyData);

			//2.send data
			if(FALSE == MDB_SendCmd(hComm, pbyData, iLen))
			{
				//TRACE_MDB("MDBEXT_ExecFuncProductInfo::MDB_SendCmd ERROR\n");
				return MDB_ERROR_FAIL;
			}

			//3.receive response
			pbyData = pstMdb->pbyRecvBuf;
			iLen = MDBEXT_GetResponse(hComm,
				pbyData,
				MDBEXT_BUF_LEN,
				pstCurCfgSampleItem->iResponseTimeout,
				pstCurCfgSampleItem->iReadDelay);
			pbyData[iLen] = 0;//end with 0

			//TRACE_MDB("MDBEXT_ExecFuncProductInfo::MDBEXT_GetResponse iLen=%d, iResponseTimeout=%d, iReadDelay=%d, packet=%s\n",
			//	iLen,
			//	pstCurCfgSampleItem->iResponseTimeout,
			//	pstCurCfgSampleItem->iReadDelay,
			//	(char *)pbyData);

			//a.check receive data len
			if(iLen < iBaseCharNum)
			{
				return MDB_ERROR_RESPOND_DATA_LEN;
			}
			//b.check start char
			//TRACE_MDB("start=%c\n", pbyData[0]);
			if(MDBEXT_ASCII_MODE_START_CHR != pbyData[0])
			{
				return MDB_ERROR_FAIL;
			}
			//c.check address
			byTemp = AsciiHexToChar(pbyData + 1);
			//TRACE_MDB("addr=%d\n", byTemp);
			if(byTemp != (unsigned int)iAddr)
			{
				AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, "Modbus reply address error");
				return MDB_ERROR_ADDR;
			}
			//d.check function
			byTemp = AsciiHexToChar(pbyData + 3);
			//TRACE_MDB("fun=%d\n", byTemp);
			if(byTemp != (unsigned int)pstCurCfgSampleItem->iModbusFunID)
			{
				AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, "Modbus reply func code error, %0x",byTemp);
				return MDB_ERROR_FUN_CODE;
			}
			//e.check data len
			iDataByteCount = AsciiHexToChar(pbyData + 5);
			//TRACE_MDB("data len=%d\n", iDataByteCount);
			AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "In MDBEXT_ExecFuncProductInfo, Modbus PI response data len is %d",iDataByteCount);
			
			if (iDataByteCount*2 + iBaseCharNum > iLen)//packet too short
			{
				return MDB_ERROR_RESPOND_DATA_LEN;
			}
			//f.Check LRC
			byTemp = AsciiHexToChar(pbyData + 7 + iDataByteCount*2);
			//TRACE_MDB("LRC=%d\n", byTemp);
			//TRACE_MDB("calc LRC=%d\n", MDBEXT_CalcLRC(pbyData + 1, 6 + iDataByteCount*2));
			if (byTemp != MDBEXT_CalcLRC(pbyData + 1, 6 + iDataByteCount*2))
			{
				AppLogOut(MDBEXT_MOD_NAME, APP_LOG_ERROR, "Modbus reply crc error");
				return MDB_ERROR_CRC;
			}

			//parse data
			pbyData += 7;//move to start position of data filed 
			for(i = 0; i < iDataByteCount; i++)
			{
				byTemp = AsciiHexToChar(pbyData);//high byte
				pbyData += 2;
				pstMdb->pbySendBuf[i] = byTemp;//just store here for a short time
			}
			pstMdb->pbySendBuf[i] = 0;

			//check response string
			//TRACE_MDB("MDBEXT_ExecFuncProductInfo::pbySendBuf=%s, pszResponseKeyStr=%s\n",
			//	pstMdb->pbySendBuf,
			//	pstCurCfgSampleItem->pszResponseKeyStr);

			iTemp = strlen(pstCurCfgSampleItem->pszResponseKeyStr);
			if(iTemp > 0)
			{
				if(iTemp > iDataByteCount)
				{
					return MDB_ERROR_OTHER;
				}
				else
				{
					for(i = 0; i <= iDataByteCount - iTemp; i++)
					{
						if((unsigned char)pstCurCfgSampleItem->pszResponseKeyStr[0] == (unsigned char)pstMdb->pbySendBuf[i])
						{
							if(0 == memcmp((void *)pstCurCfgSampleItem->pszResponseKeyStr, (void *)&pstMdb->pbySendBuf[i], iTemp))
							{
								break;
							}
						}
					}

					if(i > iDataByteCount - iTemp)//can't find key string in receive buffer
					{
						return MDB_ERROR_OTHER;
					}
				}
			}
/*
			if(MDBEXT_SCAN_METHOD_SONICK_BATTERY == pstCurCfgGroup->iProductInfoScanMethod)
			{
				//data format:<0x02><0xFF>48TL200<Space>XXXXXXXXXX
				//get produce info for SoNick Battery
				if(i >= 20)
				{
					strncpyz(pstCurEquip->stPI.szPartNumber, (char *)pstMdb->pbySendBuf + 2, 8);//48TL200
					strncpyz(pstCurEquip->stPI.szSerialNumber, (char *)pstMdb->pbySendBuf + 10, 11);//XXXXXXXXXX
					//TRACE_MDB("szPartNumber=%s,szSerialNumber=%s\n",
					//	pstCurEquip->stPI.szPartNumber,
					//	pstCurEquip->stPI.szSerialNumber);
				}
				else
				{
					TRACE_MDB("MDBEXT_ExecFuncProductInfo::ERROR pbySendBuf=%s\n", pstMdb->pbySendBuf);
				}
			}
*/
	
			
			
			TRACE_MDB("MDBEXT_ExecFunProductInfo OK\n");
			return MDB_ERROR_OK;
		}
	}
	else if(0 == stricmp(pstCurCfgTable->stCfgCommTable.pstItem[0].pszModbusTransMode, MDBEXT_TRANS_MODE_RTU))
	{
		if(MDBEXT_DATATYPE_ID_10 == pstCurCfgSampleItem->iDataType)
		{
			data_offset = 0;							
			iOneDataByteCount = pstCurCfgSampleItem->iDataType/10;//for MDBEXT_DATATYPE_ID_10, one data is one byte
			//address(1 char), function(1 char), data len(1 char), data(n char), CRC(2 char)
			iBaseCharNum = 5;//address(1) + function(1) + data len(1) + CRC(2)
				// Patch for Narada BMS, GP
			//TRACE_MDB("MDBEXT_ExecFuncProductInfo::iOneDataByteCount=%d, iBaseCharNum=%d\n",
			//	iOneDataByteCount,
			//	iBaseCharNum);
		
			//1. prepare data
			//pack data
			pbyData = pstMdb->pbySendBuf;
			iLen = 0;
			pbyData[iLen++] = (BYTE)iAddr;
			pbyData[iLen++] = (BYTE)pstCurCfgSampleItem->iModbusFunID;
			
			if ( MDBEXT_CFG_SPEC_VAL_NA_NUM == pstCurCfgSampleItem->iModbusData )
			{
				pbyData[iLen++] = 0x00;
				pbyData[iLen++] = 0x00;
			}
			else
			{
				pbyData[iLen++] = (BYTE)(pstCurCfgSampleItem->iModbusData >> 8);
				pbyData[iLen++] = (BYTE)pstCurCfgSampleItem->iModbusData;
				iBaseCharNum += 2;	// ReadId related bytes
				data_offset = 2;
			}
					
			pbyData[iLen++] = 0x00;
			pbyData[iLen++] = 0x00;
			
			wCRC = MDB_CRC(pbyData, iLen);
			pbyData[iLen++] = HIBYTE(wCRC);
			pbyData[iLen++] = LOBYTE(wCRC);
			
			//TRACE_MDB("MDBEXT_ExecFuncProductInfo::iLen=%d, packet=%s\n", iLen, (char *)pbyData);

			//2.send data
			if(FALSE == MDB_SendCmd(hComm, pbyData, iLen))
			{
				TRACE_MDB("MDBEXT_ExecFuncProductInfo::MDB_SendCmd ERROR\n");
				return MDB_ERROR_FAIL;
			}

			memset(str, 0x00, sizeof(str));
			memset(output, 0x00, sizeof(output));
			for ( i = 0 ; i < iLen ; i++ )
			{
				sprintf(str, "%02x ", pbyData[i]);
				strcat(output, str);	
			}
 
			AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "Prod Id msg sent is %s\n", output);

			//3.receive response
			pbyData = pstMdb->pbyRecvBuf;
			iLen = MDBEXT_GetResponse(hComm,
				pbyData,
				MDBEXT_BUF_LEN,
				pstCurCfgSampleItem->iResponseTimeout,
				pstCurCfgSampleItem->iReadDelay);
			pbyData[iLen] = 0;//end with 0

			//TRACE_MDB("MDBEXT_ExecFuncProductInfo::MDBEXT_GetResponse iLen=%d, iResponseTimeout=%d, iReadDelay=%d, packet=%s\n",
			//	iLen,
			//	pstCurCfgSampleItem->iResponseTimeout,
			//	pstCurCfgSampleItem->iReadDelay,
			//	(char *)pbyData);
			
			//a.check receive data len
//			TRACE_MDB2("iLen=%d\n", iLen);
			if(iLen < iBaseCharNum)
			{
				TRACE_MDB2("Receive data length error, iLen=%d\n", iLen);
				AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "Receive data length error, iLen=%d\n", iLen);
				return MDB_ERROR_RESPOND_DATA_LEN;
			}
			
			memset(str, 0x00, sizeof(str));
			memset(output, 0x00, sizeof(output));
			for ( i = 0 ; i < iLen ; i++ )
			{
				sprintf(str, "%02x ", pbyData[i]);
				strcat(output, str);	
			}
			AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "MDBEXT_ExecFuncProductInfo:: Modbus Product Id reply length=%d\n", iLen);	

			log_msg_len = strlen(output);
			log_msg_count = (log_msg_len/60) + ( ((log_msg_len % 60) == 0) ? 0 : 1);
			if ( log_msg_count > 8 )
				log_msg_count = 8;
			for ( i = 0 ; i < log_msg_count ; i++ )
			{
				strncpy(log_msg, &output[i*60], 60);
				log_msg[60] = 0x00;
				if ( log_msg[0] )
					AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "%s\n", log_msg);
			}	
			
			//b.check address
			byTemp = pbyData[0];
//			TRACE_MDB2("addr=%d\n", byTemp);
			if(byTemp != (unsigned int)iAddr)
			{
				TRACE_MDB2("Address error, addr=%d\n", byTemp);
				AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "Address error, addr=%d\n", byTemp);
				return MDB_ERROR_ADDR;
			}
			//c.check function
			byTemp = pbyData[1];
//			TRACE_MDB2("fun=%d\n", byTemp);
			if(byTemp != (unsigned int)pstCurCfgSampleItem->iModbusFunID)
			{
				TRACE_MDB2("Function code error, fun=%d\n", byTemp);
				AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "Function code error, fun=%d\n", byTemp);
				return MDB_ERROR_FUN_CODE;
			}
			
			//d.check data len
			iDataByteCount = pbyData[2+data_offset];		
//			TRACE_MDB2("data len=%d\n", iDataByteCount);
			if (iDataByteCount + iBaseCharNum > iLen)//packet too short
			{
				TRACE_MDB2("Error packet length, data len=%d\n", iDataByteCount);
				AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "Error packet length, data len=%d\n", iDataByteCount);
				return MDB_ERROR_RESPOND_DATA_LEN;
			}
			//e.Check CRC
			wCRC = MDB_CRC(pbyData, 3 + data_offset + iDataByteCount);
			byTemp = pbyData[3 + data_offset + iDataByteCount];
//			TRACE_MDB2("wCRC=%x,crc[1]=%2x\n", wCRC, byTemp);
			if (byTemp != HIBYTE(wCRC))
			{
				TRACE_MDB2("CRC error, wCRC=%x,crc[1]=%2x\n", wCRC, byTemp);
				AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "CRC error, wCRC=%x,crc[1]=%2x\n", wCRC, byTemp);
				return MDB_ERROR_CRC;
			}
			byTemp = pbyData[4 + +data_offset + iDataByteCount];
//			TRACE_MDB2("wCRC=%x,crc[0]=%2x\n", wCRC, byTemp);
			if (byTemp != LOBYTE(wCRC))
			{
				TRACE_MDB2("CRC error, wCRC=%x,crc[0]=%2x\n", wCRC, byTemp);
				AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "CRC error, wCRC=%x,crc[0]=%2x\n", wCRC, byTemp);
				return MDB_ERROR_CRC;
			}

			//parse data
			pbyData += (3 + data_offset);	//move to start position of data filed 
			for(i = 0; i < iDataByteCount; i++)
			{
				pstMdb->pbySendBuf[i] = pbyData[i];//just store here for a short time
				
			}
			pstMdb->pbySendBuf[i] = 0;

			//check response string
			//TRACE_MDB("MDBEXT_ExecFuncProductInfo::pbySendBuf=%s, pszResponseKeyStr=%s\n",
			//	pstMdb->pbySendBuf,
			//	pstCurCfgSampleItem->pszResponseKeyStr);
			//find key string in receive packets
			iTemp = strlen(pstCurCfgSampleItem->pszResponseKeyStr);
			if(iTemp > 0)
			{
				if(iTemp > iDataByteCount)
				{
					return MDB_ERROR_OTHER;
				}
				else
				{
					for(i = 0; i <= iDataByteCount - iTemp; i++)
					{
						if((unsigned char)pstCurCfgSampleItem->pszResponseKeyStr[0] == (unsigned char)pstMdb->pbySendBuf[i])
						{
							if(0 == memcmp((void *)pstCurCfgSampleItem->pszResponseKeyStr, (void *)&pstMdb->pbySendBuf[i], iTemp))
							{
								AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "Response Key string detected\n");
								break;
							}
						}
					}

					if(i > iDataByteCount - iTemp)//can't find key string in receive buffer
					{
						AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "Cannot find response key string\n");
						return MDB_ERROR_OTHER;
					}
				}
			}

			ParseProductInfo(pstMdb->pbySendBuf, iDataByteCount, iGroup, iEquip, iSampleItemOffset);
			TRACE_MDB("MDBEXT_ExecFuncProductInfo OK\n");
			return MDB_ERROR_OK;
		}
	}

	TRACE_MDB("MDBEXT_ExecFuncProductInfo END\n");
	return MDB_ERROR_OTHER;
}

static int MDBEXT_Reconfig(IN void* pDevice)
{
	MDB_INFO						*pstMdb = &sg_stMdbInfo;
	MDB_CFG_COMMON					*pstCfg = &pstMdb->stCommonCfg;
	MDB_CFG_TABLE					*pstCfgTableList = pstCfg->pstTableList;
	MDB_CFG_TABLE					*pstCurCfgTable = NULL;
	MDB_CFG_GROUP_ITEM				*pstCurCfgGroup = NULL;
	MDB_CFG_SAMPLE_TABLE_ITEM		*pstCurCfgSampleItem = NULL;
	MDB_RUNINFO_GROUP				*pstCurGroup = NULL;
	MDB_RUNINFO_EQUIPMENT			*pstCurEquip = NULL;
	RS485_DEVICE_CLASS				*pMDB = (RS485_DEVICE_CLASS*)pDevice;
	BOOL							bFind;
	int								iAddr = 0, iAddrStart = 0, iAddrEnd = 0;
	int								i, j, iEquipCount, iGroup;
	HANDLE							hself;

	TRACE_MDB("MDBEXT_Reconfig start\n");
	AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "MDBEXT_Reconfig start\n");
	if(pstMdb->iInitFlag <= MDBEXT_FLAG_NONE)//must be init at first
	{
		return MDB_ERROR_FAIL;
	}

	hself = RunThread_GetId(NULL);


	//reconfig all equipments group
	for(iGroup = 0; iGroup < pstMdb->iGroupNum; iGroup++)
	{
		MDBEXT_PortReady(pMDB->pCommPort, iGroup);
		
		pstCurCfgGroup = &pstCfg->pstGroupList[iGroup];
		pstCurCfgTable = &pstCfgTableList[iGroup];
		pstCurGroup = &pstMdb->pstGroupList[iGroup];

		//1. use default value to reset rough data
		for(i = 0; i < pstCurGroup->iEquipNum; i++)
		{
			pstCurEquip = &pstCurGroup->pstEquipList[i];

			memset(&pstCurEquip->stPI, 0, sizeof(pstCurEquip->stPI));//reset produce info

			for(j = 0; j < pstCurEquip->iRoughDataNum; j++)
			{
				pstCurEquip->punRoughData[j].fValue = pstCurCfgTable->stCfgRoughDataTable.pstItem[j].dfDefaultVal;
			}
		}


		//2.scan equipment, then set MDBEXT_RAUGH_DATA_ID_EXISTENCE and MDBEXT_RAUGH_DATA_ID_ADDRESS
		if( !pstCurCfgGroup->bEnable )
		{
			iEquipCount = 0;
		}
		else
		{
			iAddrStart = pstCurCfgGroup->iModbusAddrStart;
			iAddrEnd = iAddrStart + pstCurGroup->iEquipNum;
			iEquipCount = 0;
			for(iAddr = iAddrStart; iAddr < iAddrEnd; iAddr++)
			{
				pstCurEquip = &pstCurGroup->pstEquipList[iEquipCount];

				//1. process each sample table item
				for(i = 0; i < pstCurCfgTable->stCfgSampleTable.iNum; i++)
				{
					pstCurCfgSampleItem = &pstCurCfgTable->stCfgSampleTable.pstItem[i];

					//check whether item needs be called in reconfig function 
					if( ! MDBEXT_SAMPLE_METHOD_IS_RECONFIG(pstCurCfgSampleItem->iSampleMethod))
					{
						continue;
					}

					RunThread_Heartbeat(hself);

					bFind = FALSE;
					for(j = 0; j < 3; j++)
					{
						if ( MDB_ERROR_OK == MDBEXT_ExecFuncProdInfo(pMDB->pCommPort->hCommPort,
								iGroup,
								iEquipCount,
								i,
								iAddr) )
						{
							bFind = TRUE;
							break;
						}	
 
					}

					if(bFind)
					{
						//TRACE_MDB("MDBEXT_Reconfig::Find iGroup=%d, iEquipCount=%d, iAddr=%d\n", iGroup, iEquipCount, iAddr);

						//set rough data MDBEXT_RAUGH_DATA_ID_EXISTENCE and MDBEXT_RAUGH_DATA_ID_ADDRESS
						pstCurEquip->punRoughData[pstCurEquip->iOffsetExistence].fValue = MDB_EQUIP_EXIST;
						pstCurEquip->punRoughData[pstCurEquip->iOffsetAddress].fValue = iAddr;
						AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "MDBEXT_Reconfig::Find iGroup=%d, iEquipCount=%d, iAddr=%d\n", iGroup+1, iEquipCount+1, iAddr);

					}

				}//for(i = 0; i < pstCurCfgTable->stCfgSampleTable.iNum; i++)

				if ( bFind )							
				{
					iEquipCount++;
				}										
				
			}//for(iAddr = iAddrStart; iAddr < iAddrEnd; iAddr++)
		}//( !pstCurCfgGroup->bEnable )


		pstCurGroup->iActiveEquipNum = iEquipCount;
		pstCurGroup->bOnlyStuffActiveEquip = FALSE;

		//special process for FIAMM Battery
		if(MDBEXT_SCAN_METHOD_SONICK_BATTERY == pstCurCfgGroup->iProductInfoScanMethod)
		{
#define MDBEXT_SPECIAL_FIAMM_ENABLE_EQUIP_ID		1
#define MDBEXT_SPECIAL_FIAMM_ENABLE_SIGTYPE_ID		SIG_TYPE_SAMPLING
#define MDBEXT_SPECIAL_FIAMM_ENABLE_SIG_ID			214

			SIG_ENUM enumTemp = (pstCurGroup->iActiveEquipNum > 0)? 1: 0;

			SetEnumSigValue(MDBEXT_SPECIAL_FIAMM_ENABLE_EQUIP_ID, 
				MDBEXT_SPECIAL_FIAMM_ENABLE_SIGTYPE_ID,
				MDBEXT_SPECIAL_FIAMM_ENABLE_SIG_ID,
				enumTemp,
				MDBEXT_MOD_NAME);
		}
	}

	//MDBEXT_PrintData(1);
	TRACE_MDB("MDBEXT_Reconfig end\n");
	AppLogOut(MDBEXT_MOD_NAME, APP_LOG_INFO, "MDBEXT_Reconfig end\n");
	return MDB_ERROR_OK;
}

static int MDBEXT_FindRoughDataOffset(IN MDB_CFG_ROUGH_DATA_TABLE *pstRoughDataTable, IN int iRoughDataID)
{
	int								iOffset = -1;
	int								iRoughDataNum = pstRoughDataTable->iNum;
	int								k;


	//find the rough data from rough data table
	//for finding the ID fast
	//if it is a normal ID, research ID from (iRoughDataID - 1) to end at first, 
	//then  from (iRoughDataID - 2) to 0
	if(iRoughDataID <= 0)
	{
		return iOffset;
	}
	else if(iRoughDataID <= iRoughDataNum)
	{
		//research ID from (iRoughDataID - 1) to end
		for(k = iRoughDataID - 1; k < iRoughDataNum; k++)
		{
			if(iRoughDataID == pstRoughDataTable->pstItem[k].iID)
			{
				iOffset = k;
				break;//find ID
			}
		}
		if(k == iRoughDataNum)//not find
		{
			//research ID from (iRoughDataID - 2) to 0
			for(k = iRoughDataID - 2; k >= 0; k--)
			{
				if(iRoughDataID == pstRoughDataTable->pstItem[k].iID)
				{
					iOffset = k;
					break;//find ID
				}
			}
		}
	}
	//if it is a special ID, research ID from the last one to the first
	else
	{
		for(k = iRoughDataNum - 1; k >= 0; k--)
		{
			if(iRoughDataID == pstRoughDataTable->pstItem[k].iID)
			{
				iOffset = k;
				break;//find ID
			}
		}
	}

	return iOffset;
}


static int MDBEXT_StuffChn(void* pDevice, ENUMSIGNALPROC EnumProc, LPVOID lpvoid)
{
	MDB_INFO						*pstMdb = &sg_stMdbInfo;
	MDB_CFG_COMMON					*pstCfg = &pstMdb->stCommonCfg;
	MDB_CFG_TABLE					*pstCfgTableList = pstCfg->pstTableList;
	MDB_CFG_TABLE					*pstCurCfgTable = NULL;
	MDB_CFG_GROUP_ITEM				*pstCfgGroupList = pstCfg->pstGroupList;
	MDB_CFG_GROUP_ITEM				*pstCurCfgGroup = NULL;
	MDB_CFG_STUFF_CHAN_TABLE_ITEM	*pstCurCfgStuffItem = NULL;
	MDB_RUNINFO_GROUP				*pstCurGroup = NULL;
	MDB_RUNINFO_EQUIPMENT			*pstCurEquip = NULL;
	int								iRoughDataNum, iTemp, iStartOffset;
	unsigned int					uiBitStart, uiBitCount, uiBitData;
	int								i, j, iGroup, iEquip;
	float							fTemp;
	VAR_VALUE						unTempVal;

	TRACE_MDB("MDBEXT_StuffChn start\n");
	UNUSED(pDevice);

	if(pstMdb->iInitFlag <= MDBEXT_FLAG_NONE)//must be init at first
	{
		return MDB_ERROR_FAIL;
	}

	//process each stuff channel table for all equipments of each group
	//process each group
	for(iGroup = 0; iGroup < pstMdb->iGroupNum; iGroup++)
	{
		pstCurCfgTable = &pstCfgTableList[iGroup];
		pstCurCfgGroup = &pstCfgGroupList[iGroup];
		pstCurGroup = &pstMdb->pstGroupList[iGroup];
		iRoughDataNum = pstCurCfgTable->stCfgRoughDataTable.iNum;

		//process each stuff channel table
		for(i = 0; i < pstCurCfgTable->stCfgStuffChanTable.iNum; i++)
		{
			pstCurCfgStuffItem = &pstCurCfgTable->stCfgStuffChanTable.pstItem[i];
			iStartOffset = MDBEXT_FindRoughDataOffset(&pstCurCfgTable->stCfgRoughDataTable,
				pstCurCfgStuffItem->iRoughDataIDStart);

			//process each stuff channel table item
			for(j = 0; j < pstCurCfgStuffItem->iSampleChanQuantity; j++)
			{
				if(pstCurCfgStuffItem->iStuffMethod >= MDBEXT_GROUP_STUFF_METHOD_START)
				{
					//iSampleChanQuantity is no sense for GROUP_STUFF_METHOD

					if(pstCurCfgGroup->iGroupChannelStart <= 0)//config args invalid
					{
						printf("MDBEXT_StuffChn::ERROR iGroup ID=%d, iGroupChannelStart=%d\n",
							pstCurCfgGroup->iID,
							pstCurCfgGroup->iGroupChannelStart);
						break;//move to next stuff item
					}

					//stuff Group sample channels
					fTemp = 0;
					if(MDBEXT_GROUP_STUFF_METHOD_AVERAGE == pstCurCfgStuffItem->iStuffMethod)
					{
						if(iStartOffset < 0)//find none
						{
							printf("MDBEXT_StuffChn::ERROR StuffItemID=%d, iRoughDataIDStart=%d, iStartOffset=%d\n",
								pstCurCfgStuffItem->iID,
								pstCurCfgStuffItem->iRoughDataIDStart,
								iStartOffset);
							break;
						}

						fTemp = 0;
						for(iEquip = 0; iEquip < pstCurGroup->iActiveEquipNum; iEquip++)
						{
							pstCurEquip = &pstCurGroup->pstEquipList[iEquip];
							fTemp += pstCurEquip->punRoughData[iStartOffset].fValue;
						}

						if(pstCurGroup->iActiveEquipNum > 0)
						{
							fTemp /= pstCurGroup->iActiveEquipNum;
						}
					}
					else if(MDBEXT_GROUP_STUFF_METHOD_SUMMER == pstCurCfgStuffItem->iStuffMethod)
					{
						if(iStartOffset < 0)//find none
						{
							printf("MDBEXT_StuffChn::ERROR StuffItemID=%d, iRoughDataIDStart=%d, iStartOffset=%d\n",
								pstCurCfgStuffItem->iID,
								pstCurCfgStuffItem->iRoughDataIDStart,
								iStartOffset);
							break;
						}

						fTemp = 0;
						for(iEquip = 0; iEquip < pstCurGroup->iActiveEquipNum; iEquip++)
						{
							pstCurEquip = &pstCurGroup->pstEquipList[iEquip];
							fTemp += pstCurEquip->punRoughData[iStartOffset].fValue;
						}
					}
					else if(MDBEXT_GROUP_STUFF_METHOD_NUMBER == pstCurCfgStuffItem->iStuffMethod)
					{
						fTemp = pstCurGroup->iActiveEquipNum;
					}
					else if(MDBEXT_GROUP_STUFF_COMM_FAIL == pstCurCfgStuffItem->iStuffMethod)
					{
						fTemp = MDB_COMM_FAILURE;
						for(iEquip = 0; iEquip < pstCurGroup->iActiveEquipNum; iEquip++)
						{
							pstCurEquip = &pstCurGroup->pstEquipList[iEquip];
							if(MDB_COMM_OK == (int)pstCurEquip->punRoughData[pstCurEquip->iOffsetCommFail].fValue)
							{
								fTemp = MDB_COMM_OK;
								break;
							}
						}
					}
					else if(MDBEXT_GROUP_STUFF_EXISTENCE == pstCurCfgStuffItem->iStuffMethod)
					{
						fTemp = (pstCurGroup->iActiveEquipNum > 0)? MDB_EQUIP_EXIST: MDB_EQUIP_NOT_EXIST;
					}
					else//undefined method
					{
						break;//move to next stuff item
					}

					switch(pstCurCfgStuffItem->iChanDataType)
					{
					case MDBEXT_CHAN_DATA_TYPE_LONG:
						{
							unTempVal.lValue = (long)fTemp;
							break;
						}
					case MDBEXT_CHAN_DATA_TYPE_FLOAT:
						{
							unTempVal.fValue = (float)fTemp;
							break;
						}
					case MDBEXT_CHAN_DATA_TYPE_ULONG:
						{
							unTempVal.ulValue = (unsigned long)fTemp;
						}
					case MDBEXT_CHAN_DATA_TYPE_TIME:
						{
							unTempVal.dtValue = (SIG_TIME )fTemp;
							break;
						}
					case MDBEXT_CHAN_DATA_TYPE_ENUM:
						{
							unTempVal.enumValue = (SIG_ENUM )fTemp;
							break;
						}
					default:
						{
							unTempVal.fValue = (float)fTemp;
							break;
						}
					}

					EnumProc(pstCurCfgGroup->iGroupChannelStart + pstCurCfgStuffItem->iSampleChanStart,
						unTempVal.fValue,
						lpvoid);
					TRACE_MDB("MDBEXT_StuffChn::stuff data chan=%d, value=%f, StuffItemID=%d, iChanDataType=%d\n", 
						pstCurCfgGroup->iGroupChannelStart + pstCurCfgStuffItem->iSampleChanStart,
						fTemp,
						pstCurCfgStuffItem->iID,
						pstCurCfgStuffItem->iChanDataType);
				}
				else
				{
					//stuff Equipment sample channels

					//check whether rough data offset is valid
					if(iStartOffset < 0)//find none
					{
						printf("MDBEXT_StuffChn::ERROR StuffItemID=%d, iRoughDataIDStart=%d, iStartOffset=%d\n",
							pstCurCfgStuffItem->iID,
							pstCurCfgStuffItem->iRoughDataIDStart,
							iStartOffset);
						break;
					}

					//compute stuffing equipment range
					iTemp = (pstCurGroup->bOnlyStuffActiveEquip)? pstCurGroup->iActiveEquipNum: pstCurGroup->iEquipNum;

					//stuff the rough data in each equipment
					for(iEquip = 0; iEquip < iTemp; iEquip++)
					{
						pstCurEquip = &pstCurGroup->pstEquipList[iEquip];

						if(MDBEXT_STUFF_METHOD_DATA == pstCurCfgStuffItem->iStuffMethod)
						{
							fTemp = pstCurEquip->punRoughData[iStartOffset + j].fValue;
						}
						else if(MDBEXT_STUFF_METHOD_BIT == pstCurCfgStuffItem->iStuffMethod)
						{
							uiBitStart = (unsigned int)pstCurCfgStuffItem->fStuffArg1;
							uiBitCount = (unsigned int)pstCurCfgStuffItem->fStuffArg2;
							uiBitData = (unsigned int)pstCurEquip->punRoughData[iStartOffset + j].fValue;

							if((uiBitStart > 31)
								|| (uiBitCount > 32))
							{//argument invalid
								break;
							}

							uiBitData = uiBitData >> uiBitStart;//clear all bits before start bit
							uiBitData = uiBitData << (32 - uiBitCount);//clear all bits after end bit
							uiBitData = uiBitData & 0xFFFFFFFF;//keep 32 bits valid
							uiBitData = uiBitData >> (32 - uiBitCount);//get all bits between start bit and end bit

							fTemp = uiBitData;
						}
						else//argument invalid
						{
							break;
						}

						switch(pstCurCfgStuffItem->iChanDataType)
						{
						case MDBEXT_CHAN_DATA_TYPE_LONG:
							{
								unTempVal.lValue = (long)fTemp;
								break;
							}
						case MDBEXT_CHAN_DATA_TYPE_FLOAT:
							{
								unTempVal.fValue = (float)fTemp;
								break;
							}
						case MDBEXT_CHAN_DATA_TYPE_ULONG:
							{
								unTempVal.ulValue = (unsigned long)fTemp;
							}
						case MDBEXT_CHAN_DATA_TYPE_TIME:
							{
								unTempVal.dtValue = (SIG_TIME )fTemp;
								break;
							}
						case MDBEXT_CHAN_DATA_TYPE_ENUM:
							{
								unTempVal.enumValue = (SIG_ENUM )fTemp;
								break;
							}
						default:
							{
								unTempVal.fValue = (float)fTemp;
								break;
							}
						}

						EnumProc(pstCurEquip->iChannelStart + pstCurCfgStuffItem->iSampleChanStart + j,
							unTempVal.fValue,
							lpvoid);
						TRACE_MDB("MDBEXT_StuffChn::stuff data chan=%d, value=%f, iRoughDataID=%d, iChanDataType=%d\n", 
							pstCurEquip->iChannelStart + pstCurCfgStuffItem->iSampleChanStart + j,
							fTemp,
							pstCurCfgStuffItem->iRoughDataIDStart + j,
							pstCurCfgStuffItem->iChanDataType);
					}
				}
			}
		}

		//we just need stuff all equipments for the first time
		pstCurGroup->bOnlyStuffActiveEquip = TRUE;

	}
	//MDBEXT_PrintData(1);
	TRACE_MDB("MDBEXT_StuffChn end\n");

	return MDB_ERROR_OK;
}




static int MDBEXT_ExecFun04(IN HANDLE hComm,
					IN int iGroup,
					IN int iEquip,
					IN int iSampleItemOffset,
					IN int iAddr,
					IN int iRegAddr,
					IN int iRegQuantity)
{
	MDB_INFO						*pstMdb = &sg_stMdbInfo;
	MDB_CFG_COMMON					*pstCfg = &pstMdb->stCommonCfg;
	MDB_CFG_TABLE					*pstCfgTableList = pstCfg->pstTableList;
	MDB_CFG_TABLE					*pstCurCfgTable = NULL;
	MDB_CFG_SAMPLE_TABLE_ITEM		*pstCurCfgSampleItem = NULL;
	MDB_RUNINFO_GROUP				*pstCurGroup = NULL;
	MDB_RUNINFO_EQUIPMENT			*pstCurEquip = NULL;
	BYTE							*pbyData = NULL, byTemp, byTemp2;
	int								iLen = 0, iOffset = -1, iOneDataByteCount = 0, iBaseCharNum = 0, iDataByteCount = 0;
	int								i, iTemp;
	WORD							wCRC;

	TRACE_MDB("MDBEXT_ExecFun04 start\n");

	//init pointer
	pstCurCfgTable = &pstCfgTableList[iGroup];
	pstCurGroup = &pstMdb->pstGroupList[iGroup];
	pstCurEquip = &pstCurGroup->pstEquipList[iEquip];
	pstCurCfgSampleItem = &pstCurCfgTable->stCfgSampleTable.pstItem[iSampleItemOffset];

	//find rough data offset
	iOffset = MDBEXT_FindRoughDataOffset(&pstCurCfgTable->stCfgRoughDataTable, pstCurCfgSampleItem->iRoughDataIDStart);
	if(iOffset < 0)
	{
		//TRACE_MDB("MDBEXT_ExecFun04::ERROR iRoughDataIDStart=%d\n", pstCurCfgSampleItem->iRoughDataIDStart);
		return MDB_ERROR_OTHER;
	}

	//select proper process method
	if(0 == stricmp(pstCurCfgTable->stCfgCommTable.pstItem[0].pszModbusTransMode, MDBEXT_TRANS_MODE_ASCII))
	{
		if((MDBEXT_DATATYPE_ID_20 == pstCurCfgSampleItem->iDataType)
			|| (MDBEXT_DATATYPE_ID_22 == pstCurCfgSampleItem->iDataType)
			|| (MDBEXT_DATATYPE_ID_21 == pstCurCfgSampleItem->iDataType)
			|| (MDBEXT_DATATYPE_ID_23 == pstCurCfgSampleItem->iDataType)
			)
		{
			iOneDataByteCount = pstCurCfgSampleItem->iDataType/10;//for MDBEXT_DATATYPE_ID_2X, one data is two bytes
		//start(1 char, ':'), address(2 char), function(2 char), data len(2 char), data(n*2 char), LRC(2 char), end(2 char, 'CR', 'LF')
			iBaseCharNum = 11;//start(1) + address(2) + function(2) + data len(2) + LRC(2) + end(2)
			//TRACE_MDB("MDBEXT_ExecFun04::iOffset=%d, iOneDataByteCount=%d, iBaseCharNum=%d\n",
			//	iOffset,
			//	iOneDataByteCount,
			//	iBaseCharNum);

			//1. prepare data
			//pack data
			pbyData = pstMdb->pbySendBuf;
			iLen = snprintf((char *)pbyData,
				MDBEXT_BUF_LEN,
				":%02X%02X%04X%04X",
				(BYTE)iAddr,
				(BYTE)pstCurCfgSampleItem->iModbusFunID,
				(UINT16)iRegAddr,
				(UINT16)iRegQuantity);
			//pack LRC and stop char
			byTemp = MDBEXT_CalcLRC(pbyData + 1, iLen - 1);
			iLen += snprintf((char *)(pbyData + iLen),
				10,
				"%02X%c%c",
				byTemp,
				MDBEXT_ASCII_MODE_STOP_CHR_HI,
				MDBEXT_ASCII_MODE_STOP_CHR_LO);
			pbyData[iLen] = 0;//end with 0

			//TRACE_MDB("MDBEXT_ExecFun04::iLen=%d, packet=%s\n", iLen, (char *)pbyData);

			//2.send data
			if(FALSE == MDB_SendCmd(hComm, pbyData, iLen))
			{
				TRACE_MDB("MDBEXT_ExecFun04::MDB_SendCmd ERROR\n");
				return MDB_ERROR_FAIL;
			}

			//3.receive response
			pbyData = pstMdb->pbyRecvBuf;
			iLen = MDBEXT_GetResponse(hComm,
				pbyData,
				MDBEXT_BUF_LEN,
				pstCurCfgSampleItem->iResponseTimeout,
				pstCurCfgSampleItem->iReadDelay);
			pbyData[iLen] = 0;//end with 0

			//TRACE_MDB("MDBEXT_ExecFun04::MDBEXT_GetResponse iLen=%d, iResponseTimeout=%d, iReadDelay=%d, packet=%s\n",
			//	iLen,
			//	pstCurCfgSampleItem->iResponseTimeout,
			//	pstCurCfgSampleItem->iReadDelay,
			//	(char *)pbyData);
			//a.check base len
			if(iLen < iBaseCharNum)
			{
				return MDB_ERROR_RESPOND_DATA_LEN;
			}
			//b.check start char
			//TRACE_MDB("start=%c\n", pbyData[0]);
			if(MDBEXT_ASCII_MODE_START_CHR != pbyData[0])
			{
				return MDB_ERROR_FAIL;
			}
			//c.check address
			byTemp = AsciiHexToChar(pbyData + 1);
			//TRACE_MDB("addr=%d\n", byTemp);
			if(byTemp != (unsigned int)iAddr)
			{
				return MDB_ERROR_ADDR;
			}
			//d.check function
			byTemp = AsciiHexToChar(pbyData + 3);
			//TRACE_MDB("fun=%d\n", byTemp);
			if(byTemp != (unsigned int)pstCurCfgSampleItem->iModbusFunID)
			{
				return MDB_ERROR_FUN_CODE;
			}
			//e.check data len
			iDataByteCount = AsciiHexToChar(pbyData + 5);
			//TRACE_MDB("data len=%d\n", iDataByteCount);
			if (iDataByteCount*2 + iBaseCharNum > iLen)//packet too short
			{
				return MDB_ERROR_RESPOND_DATA_LEN;
			}
			//f.Check LRC
			byTemp = AsciiHexToChar(pbyData + 7 + iDataByteCount*2);
			//TRACE_MDB("LRC=%d\n", byTemp);
			//TRACE_MDB("calc LRC=%d\n", MDBEXT_CalcLRC(pbyData + 1, 6 + iDataByteCount*2));
			if (byTemp != MDBEXT_CalcLRC(pbyData + 1, 6 + iDataByteCount*2))
			{
				return MDB_ERROR_CRC;
			}
			//g.check reg data
			if(iDataByteCount != iRegQuantity*iOneDataByteCount)
			{
				return MDB_ERROR_RESPOND_DATA_LEN;
			}

			if(MDBEXT_DATATYPE_ID_20 == pstCurCfgSampleItem->iDataType)
			{
				//parse data
				pbyData += 7;//move to start position of data filed 
				for(i = 0; i < iRegQuantity; i++)
				{
					byTemp2 = AsciiHexToChar(pbyData);//high byte
					pbyData += 2;
					byTemp = AsciiHexToChar(pbyData);//low byte
					pbyData += 2;

					iTemp = (INT16)(((unsigned char)byTemp2 << 8) + (unsigned char)byTemp);
					pstCurEquip->punRoughData[iOffset].fValue = 
						pstCurCfgTable->stCfgRoughDataTable.pstItem[iOffset].dfScale
						*(pstCurCfgTable->stCfgRoughDataTable.pstItem[iOffset].dfOffset + iTemp);

					iOffset++;//move to next rough data pos
				}
			}
			else if(MDBEXT_DATATYPE_ID_22 == pstCurCfgSampleItem->iDataType)
			{
				//parse data
				pbyData += 7;//move to start position of data filed 
				for(i = 0; i < iRegQuantity; i++)
				{
					byTemp2 = AsciiHexToChar(pbyData);//high byte
					pbyData += 2;
					byTemp = AsciiHexToChar(pbyData);//low byte
					pbyData += 2;

					iTemp = (UINT16)(((unsigned char)byTemp2 << 8) + (unsigned char)byTemp);
					pstCurEquip->punRoughData[iOffset].fValue = 
						pstCurCfgTable->stCfgRoughDataTable.pstItem[iOffset].dfScale
						*(pstCurCfgTable->stCfgRoughDataTable.pstItem[iOffset].dfOffset + iTemp);

					iOffset++;//move to next rough data pos
				}
			}
			else if(MDBEXT_DATATYPE_ID_21 == pstCurCfgSampleItem->iDataType)
			{
				//parse data
				pbyData += 7;//move to start position of data filed 
				for(i = 0; i < iRegQuantity; i++)
				{
					byTemp = AsciiHexToChar(pbyData);//low byte
					pbyData += 2;
					byTemp2 = AsciiHexToChar(pbyData);//high byte
					pbyData += 2;

					iTemp = (INT16)(((unsigned char)byTemp2 << 8) + (unsigned char)byTemp);
					pstCurEquip->punRoughData[iOffset].fValue = 
						pstCurCfgTable->stCfgRoughDataTable.pstItem[iOffset].dfScale
						*(pstCurCfgTable->stCfgRoughDataTable.pstItem[iOffset].dfOffset + iTemp);

					iOffset++;//move to next rough data pos
				}
			}
			else if(MDBEXT_DATATYPE_ID_23 == pstCurCfgSampleItem->iDataType)
			{
				//parse data
				pbyData += 7;//move to start position of data filed 
				for(i = 0; i < iRegQuantity; i++)
				{
					byTemp = AsciiHexToChar(pbyData);//low byte
					pbyData += 2;
					byTemp2 = AsciiHexToChar(pbyData);//high byte
					pbyData += 2;

					iTemp = (UINT16)(((unsigned char)byTemp2 << 8) + (unsigned char)byTemp);
					pstCurEquip->punRoughData[iOffset].fValue = 
						pstCurCfgTable->stCfgRoughDataTable.pstItem[iOffset].dfScale
						*(pstCurCfgTable->stCfgRoughDataTable.pstItem[iOffset].dfOffset + iTemp);

					iOffset++;//move to next rough data pos
				}
			}

			TRACE_MDB("MDBEXT_ExecFun04 OK\n");
			return MDB_ERROR_OK;
		}
	}
	else if(0 == stricmp(pstCurCfgTable->stCfgCommTable.pstItem[0].pszModbusTransMode, MDBEXT_TRANS_MODE_RTU))
	{
		if((MDBEXT_DATATYPE_ID_20 == pstCurCfgSampleItem->iDataType)
			|| (MDBEXT_DATATYPE_ID_22 == pstCurCfgSampleItem->iDataType)
			|| (MDBEXT_DATATYPE_ID_21 == pstCurCfgSampleItem->iDataType)
			|| (MDBEXT_DATATYPE_ID_23 == pstCurCfgSampleItem->iDataType)
			)
		{
			iOneDataByteCount = pstCurCfgSampleItem->iDataType/10;//for MDBEXT_DATATYPE_ID_2X, one data is two bytes
			//address(1 char), function(1 char), data len(1 char), data(n*2 char), CRC(2 char)
			iBaseCharNum = 5;//address(1) + function(1) + data len(1) + CRC(2)
			//TRACE_MDB("MDBEXT_ExecFun04::iOffset=%d, iOneDataByteCount=%d, iBaseCharNum=%d\n",
			//	iOffset,
			//	iOneDataByteCount,
			//	iBaseCharNum);

			//1. prepare data
			//pack data
			pbyData = pstMdb->pbySendBuf;
			iLen = 0;
			pbyData[iLen++] = (BYTE)iAddr;
			pbyData[iLen++] = (BYTE)pstCurCfgSampleItem->iModbusFunID;
			pbyData[iLen++] = (BYTE)((UINT16)iRegAddr >> 8);
			pbyData[iLen++] = (BYTE)((UINT16)iRegAddr);
			pbyData[iLen++] = (BYTE)((UINT16)iRegQuantity >> 8);
			pbyData[iLen++] = (BYTE)((UINT16)iRegQuantity);
			wCRC = MDB_CRC(pbyData, iLen);
			pbyData[iLen++] = HIBYTE(wCRC);
			pbyData[iLen++] = LOBYTE(wCRC);
			pbyData[iLen] = 0;//end with 0

			//TRACE_MDB("MDBEXT_ExecFun04::iLen=%d, packet=%s\n", iLen, (char *)pbyData);

			//2.send data
			if(FALSE == MDB_SendCmd(hComm, pbyData, iLen))
			{
				TRACE_MDB("MDBEXT_ExecFun04::MDB_SendCmd ERROR\n");
				return MDB_ERROR_FAIL;
			}

			//3.receive response
			pbyData = pstMdb->pbyRecvBuf;
			iLen = MDBEXT_GetResponse(hComm,
				pbyData,
				MDBEXT_BUF_LEN,
				pstCurCfgSampleItem->iResponseTimeout,
				pstCurCfgSampleItem->iReadDelay);
			pbyData[iLen] = 0;//end with 0

			//TRACE_MDB("MDBEXT_ExecFun04::MDBEXT_GetResponse iLen=%d, iResponseTimeout=%d, iReadDelay=%d, packet=%s\n",
			//	iLen,
			//	pstCurCfgSampleItem->iResponseTimeout,
			//	pstCurCfgSampleItem->iReadDelay,
			//	(char *)pbyData);
			
			//a.check base len
			TRACE_MDB2("iLen=%d\n", iLen);
			if(iLen < iBaseCharNum)
			{
				return MDB_ERROR_RESPOND_DATA_LEN;
			}
			//b.check address
			byTemp = pbyData[0];
			TRACE_MDB2("addr=%d\n", byTemp);
			if(byTemp != (unsigned int)iAddr)
			{
				return MDB_ERROR_ADDR;
			}
			//c.check function
			byTemp = pbyData[1];
			TRACE_MDB2("fun=%d\n", byTemp);
			if(byTemp != (unsigned int)pstCurCfgSampleItem->iModbusFunID)
			{
				return MDB_ERROR_FUN_CODE;
			}
			//d.check data len
			iDataByteCount = pbyData[2];
			TRACE_MDB2("data len=%d\n", iDataByteCount);
			if (iDataByteCount + iBaseCharNum > iLen)//packet too short
			{
				return MDB_ERROR_RESPOND_DATA_LEN;
			}
			//e.Check CRC
			wCRC = MDB_CRC(pbyData, 3 + iDataByteCount);
			byTemp = pbyData[3 + iDataByteCount];
			TRACE_MDB2("wCRC=%x,crc[1]=%2x\n", wCRC, byTemp);
			if (byTemp != HIBYTE(wCRC))
			{
				return MDB_ERROR_CRC;
			}
			byTemp = pbyData[4 + iDataByteCount];
			TRACE_MDB2("wCRC=%x,crc[0]=%2x\n", wCRC, byTemp);
			if (byTemp != LOBYTE(wCRC))
			{
				return MDB_ERROR_CRC;
			}
			//f.check reg data
			if(iDataByteCount != iRegQuantity*iOneDataByteCount)
			{
				return MDB_ERROR_RESPOND_DATA_LEN;
			}
			
			if(MDBEXT_DATATYPE_ID_20 == pstCurCfgSampleItem->iDataType)
			{
				//parse data
				pbyData += 3;//move to start position of data filed 
				for(i = 0; i < iRegQuantity; i++)
				{
					byTemp2 = *pbyData;//high byte
					pbyData++;
					byTemp = *pbyData;//low byte
					pbyData++;

					iTemp = (INT16)(((unsigned char)byTemp2 << 8) + (unsigned char)byTemp);
					pstCurEquip->punRoughData[iOffset].fValue = 
						pstCurCfgTable->stCfgRoughDataTable.pstItem[iOffset].dfScale
						*(pstCurCfgTable->stCfgRoughDataTable.pstItem[iOffset].dfOffset + iTemp);

					iOffset++;//move to next rough data pos
				}
			}
			else if(MDBEXT_DATATYPE_ID_22 == pstCurCfgSampleItem->iDataType)
			{
				//parse data
				pbyData += 3;//move to start position of data filed 
				for(i = 0; i < iRegQuantity; i++)
				{
					byTemp2 = *pbyData;//high byte
					pbyData++;
					byTemp = *pbyData;//low byte
					pbyData++;

					iTemp = (UINT16)(((unsigned char)byTemp2 << 8) + (unsigned char)byTemp);
					pstCurEquip->punRoughData[iOffset].fValue = 
						pstCurCfgTable->stCfgRoughDataTable.pstItem[iOffset].dfScale
						*(pstCurCfgTable->stCfgRoughDataTable.pstItem[iOffset].dfOffset + iTemp);

					iOffset++;//move to next rough data pos
				}
			}
			else if(MDBEXT_DATATYPE_ID_21 == pstCurCfgSampleItem->iDataType)
			{
				//parse data
				pbyData += 3;//move to start position of data filed 
				for(i = 0; i < iRegQuantity; i++)
				{
					byTemp = *pbyData;//low byte
					pbyData++;
					byTemp2 = *pbyData;//high byte
					pbyData++;

					iTemp = (INT16)(((unsigned char)byTemp2 << 8) + (unsigned char)byTemp);
					pstCurEquip->punRoughData[iOffset].fValue = 
						pstCurCfgTable->stCfgRoughDataTable.pstItem[iOffset].dfScale
						*(pstCurCfgTable->stCfgRoughDataTable.pstItem[iOffset].dfOffset + iTemp);

					iOffset++;//move to next rough data pos
				}
			}
			else if(MDBEXT_DATATYPE_ID_23 == pstCurCfgSampleItem->iDataType)
			{
				//parse data
				pbyData += 3;//move to start position of data filed 
				for(i = 0; i < iRegQuantity; i++)
				{
					byTemp = *pbyData;//low byte
					pbyData++;
					byTemp2 = *pbyData;//high byte
					pbyData++;

					iTemp = (UINT16)(((unsigned char)byTemp2 << 8) + (unsigned char)byTemp);
					pstCurEquip->punRoughData[iOffset].fValue = 
						pstCurCfgTable->stCfgRoughDataTable.pstItem[iOffset].dfScale
						*(pstCurCfgTable->stCfgRoughDataTable.pstItem[iOffset].dfOffset + iTemp);

					iOffset++;//move to next rough data pos
				}
			}

			TRACE_MDB("MDBEXT_ExecFun04 OK\n");
			return MDB_ERROR_OK;
		}
	}

	TRACE_MDB("MDBEXT_ExecFun04 end\n");
	return MDB_ERROR_OTHER;
}


static int MDBEXT_Sample(void* pDevice)
{
	MDB_INFO						*pstMdb = &sg_stMdbInfo;
	MDB_CFG_COMMON					*pstCfg = &pstMdb->stCommonCfg;
	MDB_CFG_TABLE					*pstCfgTableList = pstCfg->pstTableList;
	MDB_CFG_TABLE					*pstCurCfgTable = NULL;
	MDB_CFG_SAMPLE_TABLE_ITEM		*pstCurCfgSampleItem = NULL;
	MDB_RUNINFO_GROUP				*pstCurGroup = NULL;
	MDB_RUNINFO_EQUIPMENT			*pstCurEquip = NULL;
	RS485_DEVICE_CLASS				*pMDB = (RS485_DEVICE_CLASS*)pDevice;
	int								i, iGroup, iEquip, iCommResult, iAddr;
	int								iRegAddrStart, iRegQuantity, iSplitCount, iRestQuantity;
	HANDLE							hself;
	
	TRACE_MDB("MDBEXT_Sample start iInitFlag=%d\n", pstMdb->iInitFlag);

	if(pstMdb->iInitFlag <= MDBEXT_FLAG_NONE)//must be init at first
	{
		return MDB_ERROR_FAIL;
	}

	hself = RunThread_GetId(NULL);

	//only for test reconfig
	//struct stat stFileStat;
	//if(0 == stat("/var/TestScan", &stFileStat))//file exist
	//{
	//	unlink("/var/TestScan");
	//	MDBEXT_Reconfig(pDevice);
	//}


	//process each stuff channel table for all equipments of each group
	//process each group
	for(iGroup = 0; iGroup < pstMdb->iGroupNum; iGroup++)
	{
		MDBEXT_PortReady(pMDB->pCommPort, iGroup);

		pstCurCfgTable = &pstCfgTableList[iGroup];
		pstCurGroup = &pstMdb->pstGroupList[iGroup];

		//process each active equipments
		for(iEquip = 0; iEquip < pstCurGroup->iActiveEquipNum; iEquip++)
		{
			pstCurEquip = &pstCurGroup->pstEquipList[iEquip];
			iAddr = (int)pstCurEquip->punRoughData[pstCurEquip->iOffsetAddress].fValue;

			//1. process each sample table item
			for(i = 0; i < pstCurCfgTable->stCfgSampleTable.iNum; i++)
			{
				pstCurCfgSampleItem = &pstCurCfgTable->stCfgSampleTable.pstItem[i];

				//check whether item needs be called in sample function 
				if( ! MDBEXT_SAMPLE_METHOD_IS_SAMPLE(pstCurCfgSampleItem->iSampleMethod))
				{
					continue;
				}

				RunThread_Heartbeat(hself);

				//sample data from iRegAddrStart to (iRegAddrStart + iRestQuantity)
				//max reg quantity in each sample is iSplitCount
				iSplitCount = (pstCurCfgSampleItem->iSplitCount <= 0)?
					pstCurCfgSampleItem->iRegQuantity: pstCurCfgSampleItem->iSplitCount;
				iRegAddrStart = pstCurCfgSampleItem->iRegAddrStart - 1;//include iRegAddrStart
				iRestQuantity = pstCurCfgSampleItem->iRegQuantity;
				while(iRestQuantity > 0)
				{
					//compute current sample quantity
					if(iRestQuantity >= iSplitCount)
					{
						iRegQuantity = iSplitCount;
					}
					else
					{
						iRegQuantity = iRestQuantity;
					}
					//start current sample
					if((MDBEXT_FUN_CODE_04 == pstCurCfgSampleItem->iModbusFunID)
						//MDBEXT_FUN_CODE_03 and MDBEXT_FUN_CODE_04 both are the same processing procedure 
						|| (MDBEXT_FUN_CODE_03 == pstCurCfgSampleItem->iModbusFunID))
					{
						iCommResult = MDBEXT_ExecFun04(pMDB->pCommPort->hCommPort,
							iGroup,
							iEquip,
							i,
							iAddr,
							iRegAddrStart,
							iRegQuantity);
						if(MDB_ERROR_OK == iCommResult)
						{
							break;//stop sample current equipment
						}
						else
						{
							//TRACE_MDB("MDBEXT_Sample::ERROR iGroup=%d, iEquip=%d, SampleItem=%d, iRegAddrStart=%d. iRegQuantity=%d\n",
							//	iGroup,
							//	iEquip,
							//	i,
							//	iRegAddrStart,
							//	iRegQuantity);
						}

					}
					else
					{
						//TRACE_MDB("MDBEXT_Sample::ERROR iModbusFunID=%d\n", pstCurCfgSampleItem->iModbusFunID);
						iCommResult = MDB_ERROR_FUN_CODE;
						break;//stop sample current equipment
					}
					//move to next sample
					iRegAddrStart += iRegQuantity;
					iRestQuantity -= iRegQuantity;
				}//while(iRestQuantity > 0)

				if(iCommResult != MDB_ERROR_OK)
				{
					break;//stop sampling current equipment
				}
			}//for(i = 0; i < pstCurCfgTable->stCfgSampleTable.iNum; i++)


			//2.process communication failed
			if(iCommResult != MDB_ERROR_OK)
			{
				if((int)pstCurEquip->punRoughData[pstCurEquip->iOffsetCommBreakTimes].fValue <= MDB_COMM_FAIL_TIMES)
				{
					pstCurEquip->punRoughData[pstCurEquip->iOffsetCommBreakTimes].fValue += 1;
				}
			}
			else
			{
				pstCurEquip->punRoughData[pstCurEquip->iOffsetCommBreakTimes].fValue = 
					pstCurCfgTable->stCfgRoughDataTable.pstItem[pstCurEquip->iOffsetCommBreakTimes].dfDefaultVal;
			}

			if((int)pstCurEquip->punRoughData[pstCurEquip->iOffsetCommBreakTimes].fValue > MDB_COMM_FAIL_TIMES)
			{
				pstCurEquip->punRoughData[pstCurEquip->iOffsetCommFail].fValue = MDB_COMM_FAILURE;
			}
			else
			{
				pstCurEquip->punRoughData[pstCurEquip->iOffsetCommFail].fValue = MDB_COMM_OK;
			}

			//TRACE_MDB("MDBEXT_Sample:: iGroup=%d, iEquip=%d, CommBreakTimes=%f, CommFail=%f\n",
			//	iGroup,
			//	iEquip,
			//	pstCurEquip->punRoughData[pstCurEquip->iOffsetCommBreakTimes].fValue,
			//	pstCurEquip->punRoughData[pstCurEquip->iOffsetCommFail].fValue);

		}//for(iEquip = 0; iEquip < pstCurGroup->iActiveEquipNum; iEquip++)

	}//for(iGroup = 0; iGroup < pstMdb->iGroupNum; iGroup++)
	
	//MDBEXT_PrintData(1);
	TRACE_MDB("MDBEXT_Sample end\n");
	return MDB_ERROR_OK;
}



int MDBEXT_GetProdctInfo(IN HANDLE hComm, IN int iUnitNo, OUT void *pPI)
{
	MDB_INFO						*pstMdb = &sg_stMdbInfo;
	MDB_CFG_COMMON					*pstCfg = &pstMdb->stCommonCfg;
	MDB_CFG_TABLE					*pstCfgTableList = pstCfg->pstTableList;
	MDB_CFG_TABLE					*pstCurCfgTable = NULL;
	MDB_RUNINFO_GROUP				*pstCurGroup = NULL;
	MDB_RUNINFO_EQUIPMENT			*pstCurEquip = NULL;
	int								iGroup, iEquip;
	PRODUCT_INFO					*pInfo = (PRODUCT_INFO *)pPI;

	TRACE_MDB("MDBEXT_GetProdctInfo start iInitFlag=%d\n", pstMdb->iInitFlag);

	UNUSED(hComm);

	if(pstMdb->iInitFlag <= MDBEXT_FLAG_NONE)//must be init at first
	{
		return MDB_ERROR_FAIL;
	}

	for(iGroup = 0; iGroup < pstMdb->iGroupNum; iGroup++)
	{
		pstCurCfgTable = &pstCfgTableList[iGroup];
		pstCurGroup = &pstMdb->pstGroupList[iGroup];

		if(pstCurGroup->iEquipNum > 0)
		{
			//check whether or not iUnitNo belongs the below range
			if((iUnitNo >= pstCurGroup->pstEquipList[0].iEquipID)
				&& (iUnitNo <= pstCurGroup->pstEquipList[pstCurGroup->iEquipNum - 1].iEquipID))
			{
				//TRACE_MDB("MDBEXT_GetProdctInfo::iUnitNo=%d, iActiveEquipNum=%d\n", iUnitNo, pstCurGroup->iActiveEquipNum);

				//active equipments
				for(iEquip = 0; iEquip < pstCurGroup->iActiveEquipNum; iEquip++)
				{
					pstCurEquip = &pstCurGroup->pstEquipList[iEquip];
					if(pstCurEquip->iEquipID == iUnitNo)
					{
						memcpy(pInfo->szPartNumber, pstCurEquip->stPI.szPartNumber, sizeof(pInfo->szPartNumber));
						memcpy(pInfo->szSerialNumber, pstCurEquip->stPI.szSerialNumber, sizeof(pInfo->szSerialNumber));
						memcpy(pInfo->szHWVersion, pstCurEquip->stPI.szHWVersion, sizeof(pInfo->szHWVersion));
						memcpy(pInfo->szSWVersion, pstCurEquip->stPI.szSWVersion, sizeof(pInfo->szSWVersion));

						pInfo->bSigModelUsed = TRUE;

						//TRACE_MDB("MDBEXT_GetProdctInfo::find iUnitNo=%d, iEquip=%d, bSigModelUsed=%d\n",
						//	iUnitNo,
						//	iEquip,
						//	pInfo->bSigModelUsed);
						//TRACE_MDB("MDBEXT_GetProdctInfo::szPartNumber=%s, szSerialNumber=%s, szHWVersion=%s, szSWVersion=%s\n",
						//	pInfo->szPartNumber,
						//	pInfo->szSerialNumber,
						//	pInfo->szHWVersion,
						//	pInfo->szSWVersion);

						return MDB_ERROR_OK;
					}
				}
				//inactive equipments
				for(iEquip = pstCurGroup->iActiveEquipNum; iEquip < pstCurGroup->iEquipNum; iEquip++)
				{
					pstCurEquip = &pstCurGroup->pstEquipList[iEquip];
					if(pstCurEquip->iEquipID == iUnitNo)
					{
						pInfo->bSigModelUsed = FALSE;

						//TRACE_MDB("MDBEXT_GetProdctInfo::find iUnitNo=%d, iEquip=%d, bSigModelUsed=%d\n",
						//	iUnitNo,
						//	iEquip,
						//	pInfo->bSigModelUsed);

						return MDB_ERROR_OK;
					}
				}
			}
		}
	}

	//MDBEXT_PrintData(1);
	TRACE_MDB("MDBEXT_GetProdctInfo end\n");

	return MDB_ERROR_FAIL;
}


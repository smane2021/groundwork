/*============================================================================*
*         Copyright(c) 2020, Vertiv Tech Co., Ltd.
*                          ALL RIGHTS RESERVED
*
*  PRODUCT  : Unified Data Type Definition
*
*  FILENAME : rs485_slaves.h
*  PURPOSE  : Define base data type.
*  
*  HISTORY  :
*    DATE            VERSION        AUTHOR            NOTE
*    2009-04-14      V1.0           IlockTeng         Created.    
*         
*                                                     
*-----------------------------------------------------------------------------
*  GLOBAL VARIABLES
*    NAME                                    DESCRIPTION
*          
*      
*-----------------------------------------------------------------------------
*  GLOBAL FUNCTIONS
*    NAME                                    DESCRIPTION
*      
*    
*============================================================================*/
#ifndef __RS485_SAMPLER_SLAVE_H
#define __RS485_SAMPLER_SLAVE_H
#include <stdio.h>
#include "basetypes.h"
#include "err_code.h"
#include "pubfunc.h"
#include "string.h"
#include "rs485_main.h" 
#define FUNCTION_GENERAL_ERROR				-1
#define	NO_RESPONSE_BEXISTENCE				1
#define COMMUNICATION_ERROR					2
#define RESPONSE_OK							0
#define MAX_NUM_SLAVE						3
#define SLAVE_READ_BARCODE_TIMES			6	//For Get Successful BarCode when SLAVE boot later
#define SLAVE1_EQP_NO_SEQUENCE_NUM			0	//11500 to 11599		Slave 1
#define SLAVE2_EQP_NO_SEQUENCE_NUM			1	//11600 to 11699		Slave 2
#define SLAVE3_EQP_NO_SEQUENCE_NUM			2	//11700 to 11799		Slave 3
#define SLAVE_INVALID_RECT_MODULEID			255
#define SLAVE_NOT_CTRL_ADDR					4	//invalidation	addr max 3 slaves
#define SLAVE_ADDR_START					 201
#define SLAVE_ADDR_END						 203
#define SLAVE_SAMP_INVALID_VALUE			(-9999)//initialization data
#define SLAVE_SAMP_FREQUENCY				3		//control  sample the frequency
#define SLAVE_SAMPLE_OK						0
#define SLAVE_SAMPLE_FAIL					1
//communication status	
#define	MASTERSLAVE_COMM_OK					0
#define	MASTERSLAVE_COMM_FAIL				1
//existent status
#define SLAVE_EQUIP_EXISTENT				0
#define SLAVE_EQUIP_NOT_EXISTENT			1
#define MAX_RECTIFIER_NUM					60
#define MAX_RECTIFIER_0XE1_SIG_NUM			22
#define MAX_RECTIFIER_0XE3_SIG_NUM			12
#define	MAX_CHN_NUM_PER_SLAVE				100
#define	MAX_RECTIFIERS_CHN_NUM_PER_SLAVE	5100
#define	MAX_RECT_RECT_CHN_PER_SLAVE			50
#define RS485_BATTERY_GROUP_EQUIP_ID		115
#define RS485_BATTERY_MANAGEMENT_SIG_ID		25

#define MAX_SLAVE_RETRY_TIMES		20


/*
Refer to protocol 
1	COMMAND			 ID			CHAR		1
2	MODULE			 ID			CHAR		1
3	BATERY	MANAGEMENT STATE	CHAR		1
3	SIGINFO1					FLOAT		4
4	SIGINFO2					FLOAT		4
11 byte -->ascii 22 byte
*/
#define RS485_SLAVE_SENT_CTRL_DATAINFO_LEN	22 //added siginfo2::Float Charge Voltage 
// 18+2x  << 600	OK, it is received 2xINFO
#define MAX_RECVMASTERFRAMES_IN_BUFF		4896	
// 18+2x  << 100	OK, it is send 2xINFO 
#define MAX_SENDMASTERFRAMES_IN_BUFF		512

struct	tagCHANNEL_ROUGH_DATA_IDX
{
	int		iChannel;
	int		iRoughData;
};

typedef struct tagCHANNEL_ROUGH_DATA_IDX CHANNEL_TO_ROUGH_DATA;

enum RS485ROUGH_DATA_IDX_GROUP
{
	DATA_IDX_GROUP_START = 0,	
	SLAVE_RS485_ACTUAL_NUM,
	SLAVE_RS485_GROUP_WOKING_STATE,				  //SLAVE existent or no existent
	RS485GROUP_MAX_SIGNALS_NUM,
};

enum  ROUGH_DATA_IDX_SLAVE
{
	//[ANOLOG_DATA_MAP_INFO]
	SLAVE_MODULE_INFO_CHANGE = 0,				//slave module info changed
	SLAVE_ALARM_INFO_CHANGE,					//slave alarm info changed
	SLAVE_HOLDFLAG_ONE,							//the beforehand retain signal 1
	SLAVE_HOLDFLAG_TWO,							//the beforehand retain signal 2
	SLAVE_OUTPUT_TOTAL_CURRENT,
	SLAVE_MODULE_NUMBER,		 				//number of slave modules 
	SLAVE_SYSTEM_VOLTAGE,						//slave total available energy
	SLAVE_RECTIFIER_LOST,						//slave module output total current  
	SLAVE_ALL_RECTIFIER_NO_RESPONSE,			//slave module failure
	SLAVE_MAINS_FAILURE,						//Mains Failure  //add 2009/12/15
	SLAVE_USED_CAPACITY,
	SLAVE_MAX_CAPACITY,
	SLAVE_MIN_CAPACITY,
	SLAVE_TOTAL_CURRENT,
	SLAVE_COMM_NUMBERS,
	SLAVE_RATED_VOLTAGE,
	SLAVE_MULTI_RECT_FAILURE,


	//Intestine used 
	SLAVE_EXIST_ST,								//slave does not exist
	SLAVE_COMMUNICATION_STAT,					//communication fail
	SLAVE_COMM_FAILES_TIME,
	SLAVE_SEQ_NO,								/*
										save the existent SLAVE number 
										number is counterparts addr
										*/ 
	SLAVE_SAMPLING_E3_FLAG,							//SlaveSampleCmde2  if it is 1 need stuff sampling 0xe2 RoughData
	SLAVE_CTRL_ADDR_FLAG,							//indicate control which slave

	//˳��һ���������ļ���һ��
	//[RECT_SINGLE_MAP_INFO]
	SLAVE_RCT_CURRENT_LIMIT_POINT,					//current limit point
	SLAVE_RECT_CURRENT,
	SLAVE_RCT_RECTIFIER_DC_ON_OFF_STAT,				//Rectifier DC on/off status
	SLAVE_RCT_RECTIFIER_AC_ON_OFF_STAT,				//Rectifier AC on/off status
	SLAVE_RCT_TOTAL_RUNNING_TIME,					//Total running time
	SLAVE_RCT_RECTIFIER_FAILURE,					//Rectifier failure
	SLAVE_RECT_OVER_TEMPERATURE,					//Over temperature
	SLAVE_RECT_OVER_VOLTAGE,						//Over-voltage
	SLAVE_RECT_PROTECTED,							//Rectifier protected
	SLAVE_RECT_FAN_FAILURE,							//Fan failure
	SLAVE_RCT_RECTIFIER_COMMUNICATE_FAILURE,		//Rectifier communicate failure
	SLAVE_RECT_POWER_LINITED_FOR_RECT,				//Power limited for rectifier
	SLAVE_RECT_AC_FAILURE,							//AC failure status 1��19��
	SLAVE_RECT_VOLTAGE,								//2��4��
	SLAVE_RECT_EXISTENCE_STATE,						//Existence State

	SLAVE_RECT_TEMP,
	SLAVE_RECT_WORKIN,
	SLAVE_RECT_PHASE,
	SLAVE_RECT_AC_INPUT_VOLT,

	SLAVE_RECT_0XE1_END = SLAVE_RCT_CURRENT_LIMIT_POINT + MAX_RECTIFIER_0XE1_SIG_NUM * MAX_RECTIFIER_NUM,//100*15 

	SLAVE_RCT_BARCODE_1,							//Barcode1
	SLAVE_RCT_BARCODE_2,							//Barcode2
	SLAVE_RCT_BARCODE_3,							//Barcode3
	SLAVE_RCT_BARCODE_4,							//Barcode4
	SLAVE_RCT_RATED_CURREN,							//Rated curren
	SLAVE_RCT_RECTIFIER_RATED_EFFICIENCY,			//Rectifier rated efficiency
	SLAVE_RCT_RECT_HI_SN,							//Rectifier High SN
	SLAVE_RCT_RECT_SN,								//Rectifier SN
	SLAVE_RCT_RECT_VESION,
	SLAVE_RCT_RECT_ID,
	SLAVE_RECT_0XE2_END = SLAVE_RCT_BARCODE_1 + MAX_RECTIFIER_0XE3_SIG_NUM * MAX_RECTIFIER_NUM,
	SLAVE_INTERRUPT_TIMES,
	//here add

	SLAVE_MAX_SIGNALS_NUM,
};

enum  SLAVE_SAMP_CHANNEL
{
	//Slave		1	group 
	SLAVE1_CH_SYSTEM_VOLTAGE = 11501,
	SLAVE1_CH_MODULE_NUMBER,
	SLAVE1_CH_MODULE_OUTPUT_TOTAL_CURRENT,
	SLAVE1_CH_RECTIFIER_LOST,
	SLAVE1_CH_ALL_RECTIFIER_NO_RESPONSE,
	SLAVE1_CH_COMM_FAILES,
	SLAVE1_CH_EXIST_ST,
	SLAVE1_CH_MAINS_FAILURE,			//Mains Failure  //add 2009/12/15
	SLAVE1_CH_USED_CAPACITY,
	SLAVE1_CH_MAX_CAPACITY,
	SLAVE1_CH_MIN_CAPACITY,
	SLAVE1_CH_TOTAL_CURRENT,
	SLAVE1_CH_COMM_NUMBERS,	
	SLAVE1_CH_RATED_VOLTAGE,
	SLAVE1_CH_MULTI_RECT_FAILURE,


	//Slave		2	group 
	SLAVE2_CH_SYSTEM_VOLTAGE = 11601,
	SLAVE2_CH_MODULE_NUMBER,
	SLAVE2_CH_MODULE_OUTPUT_TOTAL_CURRENT,
	SLAVE2_CH_RECTIFIER_LOST,
	SLAVE2_CH_ALL_RECTIFIER_NO_RESPONSE,
	SLAVE2_CH_COMM_FAILES,
	SLAVE2_CH_EXIST_ST,
	SLAVE2_CH_MAINS_FAILURE,

	//Slave		3	group 
	SLAVE3_CH_SYSTEM_VOLTAGE = 11701,
	SLAVE3_CH_MODULE_NUMBER,
	SLAVE3_CH_MODULE_OUTPUT_TOTAL_CURRENT,
	SLAVE3_CH_RECTIFIER_LOST,
	SLAVE3_CH_ALL_RECTIFIER_NO_RESPONSE,
	SLAVE3_CH_COMM_FAILES,
	SLAVE3_CH_EXIST_ST,
	SLAVE3_CH_MAINS_FAILURE,

	//Slave 1	Rectifier	Current  Limitation
	SLAVE1_CH_RECT_CURRENT_LIMIT_POINT =	11903,				//current limit point11900 to 16899
	SLAVE1_CH_RECT_OUT_CURRENT		   =	11907,				//Actual Current
	SLAVE1_CH_RECT_REDUNDANCY_ALARM	   =	11943,				//Rectifier redundancy related alarm
	SLAVE1_CH_RECT_DC_ON_OFF_STAT	   =	11920,				//DC On/off status
	SLAVE1_CH_RECT_AC_ON_OFF_STAT	   =	11923,				//AC on/off status
	SLAVE1_CH_RECT_TOTAL_RUNING_TIME   =	11927,				//Total Run Time
	SLAVE1_CH_RECT_FAILURE			   =	11913,				//		Rectifier failure
	SLAVE1_CH_OVER_TEMPERATURE		   =	11925,				//Over temperature  Over temperature Status
	SLAVE1_CH_OVER_VOLTAGE			   =	11912,				//Over-voltage  DC Over Voltage Status
	SLAVE1_CH_PROTECTION_STAT		   =	11914,				//Rectifier protected		Protection Status
	SLAVE1_CH_FAN_FAILURE_STAT		   =	11915,				//Fan failure status
	SLAVE1_CH_RECT_COMM_FAILURE		   =	11910,				//Rectifier communicate failure
	SLAVE1_CH_RECT_AC_FAILURE		   =	11911,				//AC failure status 1��19��
	SLAVE1_CH_RECT_POWER_LIMITED_FOR_RECT=  11919,				//Power limited for rectifier
	SLAVE1_CH_RECT_VOLTAGE			   =	11901,				//2��4��
	SLAVE1_CH_RECT_EXIST_STAT		   =    11939,

	SLAVE1_CH_RECT_TEMP			=	11904,
	SLAVE1_CH_RECT_WORKIN			=	11922,
	SLAVE1_CH_RECT_PHASE			=	11933,
	SLAVE1_CH_RECT_AC_VOLTAGE		   =    11905,

	//Slave 2	Rectifier	Current  Limitation
	SLAVE2_CH_RECT_CURRENT_LIMIT_POINT =	17903,				//current limit point11900 to 16899
	SLAVE2_CH_RECT_REDUNDANCY_ALARM	   =	17943,				//Rectifier redundancy related alarm
	SLAVE2_CH_RECT_DC_ON_OFF_STAT	   =	17920,				//DC On/off status
	SLAVE2_CH_RECT_AC_ON_OFF_STAT	   =	17923,				//AC on/off status
	SLAVE2_CH_RECT_FAILURE			   =	17913,				//AC Failure		Rectifier failure
	SLAVE2_CH_RECT_COMM_FAILURE		   =	17910,				//Rectifier communicate failure

	//Slave 3	Rectifier	Current  Limitation
	SLAVE3_CH_RECT_CURRENT_LIMIT_POINT =	22103,				//current limit point11900 to 16899
	SLAVE3_CH_RECT_REDUNDANCY_ALARM	   =	22143,				//Rectifier redundancy related alarm
	SLAVE3_CH_RECT_DC_ON_OFF_STAT	   =	22120,				//DC On/off status
	SLAVE3_CH_RECT_AC_ON_OFF_STAT	   =	22123,				//AC on/off status
	SLAVE3_CH_RECT_FAILURE			   =	22113,				//AC Failure		Rectifier failure
	SLAVE3_CH_RECT_COMM_FAILURE		   =	22110,				//Rectifier communicate failure


	//Slave 1	Barcode1
	SLAVE1_CH_RECT_BARCODE_1		   =	11934,				//Barcode1
	SLAVE1_CH_RECT_BARCODE_2		   =	11935,				//Barcode2
	SLAVE1_CH_RECT_BATCODE_3		   =	11936,				//Barcode3
	SLAVE1_CH_RECT_BATCODE_4		   =	11937,				//Barcode4
	SLAVE1_CH_RECT_RATED_CURRENT	   =	11941,				//Rated curren
	SLAVE1_CH_RECT_RATED_EFFICIENCY	   =	11942,				//Rectifier rated efficiency
	SLAVE1_CH_RECT_HI_SERIAL_NO		   =	11909,				//High Serial No.	Rectifier High SN
	SLAVE1_CH_RECT_SERIAL_NO		   =	11908,				//Rectifier SN
	SLAVE1_CH_RECT_VESION			   =    11926,
	SLAVE1_CH_RECT_ID			   =    11932,
	SLAVE1_CH_RECT_TOTAL_RUNNING_TIME  =	11927,				//Total running time

	//Slave 2	Barcode1
	SLAVE2_CH_RECT_BARCODE_1		   =	17034,				//Barcode1
	SLAVE2_CH_RECT_BARCODE_2		   =	17035,				//Barcode2
	SLAVE2_CH_RECT_BATCODE_3		   =	17036,				//Barcode3
	SLAVE2_CH_RECT_BATCODE_4		   =	17037,				//Barcode4
	SLAVE2_CH_RECT_RATED_CURRENT	   =	17041,				//Rated curren
	SLAVE2_CH_RECT_RATED_EFFICIENCY	   =	17042,				//Rectifier rated efficiency
	SLAVE2_CH_RECT_TOTAL_RUNNING_TIME  =	17027,				//Total running time

	//Slave 3	Barcode1
	SLAVE3_CH_RECT_BARCODE_1		   =	22134,				//Barcode1
	SLAVE3_CH_RECT_BARCODE_2		   =	22135,				//Barcode2
	SLAVE3_CH_RECT_BATCODE_3		   =	22136,				//Barcode3
	SLAVE3_CH_RECT_BATCODE_4		   =	22137,				//Barcode4
	SLAVE3_CH_RECT_RATED_CURRENT	   =	22141,				//Rated curren
	SLAVE3_CH_RECT_RATED_EFFICIENCY	   =	22142,				//Rectifier rated efficiency
	SLAVE3_CH_RECT_TOTAL_RUNNING_TIME  =	22127,				//Total running time
};		

enum  SLAVE_SAMPLE_CTL_CHANNEL
{
	//group sig info
	SLAVE_CURRENT_LIMIT_CTRL = 11501,//Current limit control	
	SLAVE_DC_VOLT_CTRL,//DC voltage control
	SLAVE_ALL_DC_ON_OFF_CTRL,//All DC On/Off Control
	SLAVE_ALL_AC_ON_OFF_CTRL,//All AC On/Off Control
	SLAVE_ALL_LED_CTRL,//All LED Control
	SLAVE_RESET_LOST_ALARM,
	SLAVE_RESET_OSCILLATED_ALARM,
	SLAVE_SET_IN_AC_CURRENT_LIMIT,
	SLAVE_CTL_E_STOP,
	SLAVE_CLS_COMM_INTERR,
	SLAVE_FAN_SPEED,
	SLAVE_CONFIRM_POSITION,
	SLAVE_RESET_POSITION,

	//rectifier ctrl sig info
	SLAVE1_RECT_DC_ON_OFF_CTRL = 11901,//DC On/Off Control
	SLAVE1_RECT_AC_ON_OFF_CTRL,		  //AC On/Off Control
	SLAVE1_RECT_LED_CTRL,			  //LED Control
	SLAVE1_RECT_RESET,
	SLAVE1_RECT_POSITION,
	SLAVE1_RECT_PHASE,
	SLAVE2_RECT_DC_ON_OFF_CTRL = 17001,//DC On/Off Control
	SLAVE2_RECT_AC_ON_OFF_CTRL,		  //AC On/Off Control
	SLAVE2_RECT_LED_CTRL,			  //LED Control
	SLAVE2_RECT_RESET,
	SLAVE2_RECT_POSITION,
	SLAVE2_RECT_PHASE,
	SLAVE3_RECT_DC_ON_OFF_CTRL = 22101,//DC On/Off Control
	SLAVE3_RECT_AC_ON_OFF_CTRL,		  //AC On/Off Control
	SLAVE3_RECT_LED_CTRL,			  //LED Control
	SLAVE3_RECT_RESET,
	SLAVE3_RECT_POSITION,
	SLAVE3_RECT_PHASE,
	SLAVE3_RECT_DC_ON_OFF_CTRL_END = 27099,
};

enum   SLAVE_CTRL_COMMD
{
	SLAVE_COMMAND_ID = 0,
	SLAVE_MODULE_ID,
	SLAVE_BATERY_MANAGEMENT_STATE,//voltage
	SLAVE_CTRL_SIG_INFO,
	SLAVE_CTRL_END,
};

enum YDN23PROTOCOLINFO_SLAVE
{
	CHECKSUM_ERROR_YDN23_SLAVE		= 2,
	ADDR_YDN23_SLAVE				= 3,
	CID1H_YDN23_SLAVE				= 5,
	CID1L_YDN23_SLAVE				= 6,
	CID2_YDN23_SLAVE				= 7,
	DATA_YDN23_SLAVE				= 13,
	//others will add here
};

//Following  all sig From StdEquip_Rectifier.cfg Value Type  is  Dword U
enum  RECT_SIG_VALUE_DWORD_IDX
{

	RECT_SN		= 8,					//Rectifier SN
	RSRECT_TOTAL_RUN_TIME = 9,			//Total Running Time
	RECT_HIGH_SN = 26,					//Rectifier High SN
	RECT_VERSION =27,					//Rectifier Version
	RECT_BARCODE_1 = 37,				//Barcode1
	RECT_BARCODE_2 = 38,				//Barcode2
	RECT_BARCODE_3 = 39,				//Barcode3
	RECT_BARCODE_4 = 40,				//Barcode4
	RECT_RATED_CURRENT = 11,


};


/*
union unRS485_VALUE
{
float	fValue;
int		iValue;
UINT	uiValue;
ULONG	ulValue;
DWORD	dwValue;
};
typedef union unRS485_VALUE RS485_VALUE;
*/
union unSAMPLING_VALUE
{	
	char	cValue;
	float	fValue;
	int		iValue;
	UINT	uiValue;
	ULONG	ulValue;
	DWORD	dwValue;
};
typedef union unSAMPLING_VALUE SAMPLING_VALUE;

struct Slave_CommInfo
{
	BOOL bNeedReconfig;
	//used to control the cadence of cmd
	int	iCmdCounter[SLAVE_ADDR_END-SLAVE_ADDR_START];
};

typedef struct Slave_CommInfo	SLAVE_COMM_INFO;

struct SlaveSamplerData
{

	BYTE				abyRcvBuf[MAX_RECVMASTERFRAMES_IN_BUFF];	
	BYTE				abySendBuf[MAX_SENDMASTERFRAMES_IN_BUFF];
	SLAVE_COMM_INFO		SlaveCommInfo;
	SAMPLING_VALUE		abyCtrlBuf[SLAVE_CTRL_END]; 
	//MAX_NUM_SLAVE
	SAMPLING_VALUE		aRoughDataSlave[SLAVE_ADDR_END - SLAVE_ADDR_START + 2][SLAVE_MAX_SIGNALS_NUM];
	//RS485_VALUE			aRoughDataSlave[SLAVE_ADDR_END - SLAVE_ADDR_START + 1][SLAVE_MAX_SIGNALS_NUM];
	SAMPLING_VALUE		aRoughDataGroup[RS485GROUP_MAX_SIGNALS_NUM];
	//SLAVE_ROUGH_DATA	Slave_RoughData;
};

typedef struct SlaveSamplerData	SLAVE_SAMPLER_DATA;	

union tagSmValue	
{
	long			lValue;		
	float			fValue;		
	unsigned long	ulValue;	
	CHAR            cValueStr[4];
	CHAR            cValue;
};

typedef union tagSmValue TAGSM_VALUE;

union _SLAVESTRTOFLOAT
{
	float f_value;       // float
	BYTE  by_value[4];   // byte
};
typedef union _SLAVESTRTOFLOAT	SLAVESTRTOFLOAT;

union _SLAVESTRTOULONG
{
	unsigned long ul_value;			// float
	BYTE		by_value[4];		// byte
};
typedef union _SLAVESTRTOULONG	SLAVESTRTOULONG;


INT32 SLAVE_InitRoughValue(void *);
INT32 SLAVE_Sample(void *);
INT32 SLAVE_Reconfig(void *);
VOID SLAVE_StuffChannel(RS485_DEVICE_CLASS*  SlaveDeviceClass,
			ENUMSIGNALPROC EnumProc,
			LPVOID lpvoid);	


VOID SLAVE_SendCtlCmd(RS485_DEVICE_CLASS*  SlaveDeviceClass,
		      INT32 iChannelNo,
		      float fParam);
void StuffSlaveRectInExist(ENUMSIGNALPROC EnumProc, LPVOID lpvoid);
INT32	SLAVE_MasterNeedCommProc(void);
INT32	SLAVE_MasterReopenPort(void);
INT32	iTEST_Slave_addr;
#endif








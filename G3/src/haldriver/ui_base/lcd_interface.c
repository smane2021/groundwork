/*==========================================================================*
*    Copyright(c) 2019, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : ACU(Advanced Controller Unit)
*
*  FILENAME : lcd_interface.c
*  CREATOR  : HULONGWEN                DATE: 2004-10-08 16:48
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/

#include "stdsys.h"  
#include "public.h"  

//#include "lcd_interface.h"
//
//#include "lcd_font_lib.h"


static int g_nLcd_fd = 0;
static int g_nKey_fd = 0;
static int g_nAlarmLed_fd = 0;


static BOOL		g_bFlagLcdOpen = FALSE;

//The back light value of LCD
#define MAXIMUM_BRIGHT_VALUE	42
#define MINIMUM_BRIGHT_VALUE	11

#define LCD_BRIGHT_STEP			1

//The default back light value
static int		g_iBrightValue = 22;

//The on/off status of back light
#define OPEN_LIGHT_VALUE	0x40
#define CLOSE_LIGHT_VALUE	0

static BYTE		g_iLightValue	= OPEN_LIGHT_VALUE;

//Device file name of LCD and keyboard
#define LCD_DEVICE_NAME		"/dev/lcd"
#define LCD_KEY_NAME		"/dev/keypad"
#define ALARM_LED_NAME		"/dev/led"

//The maximum matrix of a char
#define A_CHAR_MAX_DOT_NUM	36

#define BUZZER_BEEP_TIMEOUT_UNIT	20//ms

static int g_nScreenWidth = 128;
static int g_nScreenHeight = 64;

static int g_nLcdRotation = 0;

//Frank Wu,20160127, for MiniNCU
#define LCD_INTERFACE_USE_UTF8_CODE


/*==========================================================================*
 * FUNCTION : InitLCD
 * PURPOSE  : Open LCD and keyboard, and set the operate mode of LCD
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 15:32
 *==========================================================================*/
int InitLCD(void)
{
	if(!g_bFlagLcdOpen)
	{
		g_nKey_fd = open(LCD_KEY_NAME, O_RDONLY|O_NONBLOCK);
		if(g_nKey_fd <= 0)
		{
			TRACE("[%s]--InitLCD: ERROR: failed to open /dev/keypad!\n\r",
				__FILE__);
			AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
				"[%s]--InitLCD: ERROR: failed to open /dev/keypad!\n\r", 
				__FILE__);
			g_nKey_fd = 0;
			return ERR_LCD_OPEN_KEY_DRIVER_FAILURE;
		}

		g_nLcd_fd = open(LCD_DEVICE_NAME, O_WRONLY);
		if(g_nLcd_fd <= 0)
		{
			TRACE("[%s]--InitLCD: ERROR: failed to open /dev/lcd!\n\r", __FILE__);
			AppLogOut(LCD_UI_TASK, APP_LOG_ERROR,
				"[%s]--InitLCD: ERROR: failed to open /dev/lcd!\n\r", __FILE__);
			
			g_nLcd_fd = 0;
			return ERR_LCD_OPEN_LCD_DRIVER_FAILURE;
		}
		
		g_bFlagLcdOpen = TRUE;


		int nRotation = ReadEnvironmentVar(ENV_VAR_LCD_ROTATION);
		if(nRotation >= 0 && nRotation <= 3)
		{
			SetLCDRotation(nRotation * 90);
		}
		else
		{
			SetLCDRotation(90);
		}
		
//Frank Wu,20160127, for MiniNCU
#ifdef _CODE_FOR_MINI
		SetScreenHeight(32);
#else
		int	nLcdHeight = ReadEnvironmentVar(ENV_VAR_LCD_HEIGHT);
		if((nLcdHeight >= 1) && (nLcdHeight <= 2))
		{
			SetScreenHeight(nLcdHeight * 64);
		}
		else
		{
			SetScreenHeight(64);	//Default Value
			//SetScreenHeight(128);	//Default Value for debug
		}
#endif


	}

	//Initialize lcd
	//ioctl(g_nLcd_fd, IOC_LCD_INIT,0);
	ioctl(g_nLcd_fd, IOC_GRAPHIC_MODE,0);
	ioctl(g_nLcd_fd, IOC_COPY_MODE,0);
	ioctl(g_nLcd_fd, IOC_SET_BL, g_iLightValue | g_iBrightValue);

	//Set LCD Rotation
	ioctl(g_nLcd_fd, IOC_SET_ROLL_DEGREE, g_nLcdRotation);

	//Light the green LED
	AlarmLedCtrl(LED_RUN, TRUE);
	//Frank Wu,20160127, for MiniNCU
	//enable keypad 
	ioctl(g_nKey_fd, IOC_KEY_EN, 0);

	return ERR_LCD_OK;
}



/*==========================================================================*
 * FUNCTION : CloseLcdDriver
 * PURPOSE  : Close lcd and key driver
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 18:31
 *==========================================================================*/
int CloseLcdDriver(void)
{
	if(g_bFlagLcdOpen == FALSE)
	{
		return ERR_LCD_DRIVER_NOT_OPEN;
	}


	if(g_nLcd_fd != 0)
	{
		close(g_nLcd_fd);
		g_nLcd_fd = 0;
	}

	if(g_nKey_fd != 0)
	{
		close(g_nKey_fd);
		g_nKey_fd = 0;
	}

	if(g_nAlarmLed_fd != 0)
	{
		close(g_nAlarmLed_fd);
		g_nAlarmLed_fd = 0;
	}

	g_bFlagLcdOpen = FALSE;

	return ERR_LCD_OK;
}


/*==========================================================================*
 * FUNCTION : ChangeDrawMode
 * PURPOSE  : Change the LCD operation mode, it has OR/COPY/AND modes etc..
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  nMode : The needed mode
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 15:35
 *==========================================================================*/
int ChangeDrawMode(int nMode)
{
	if(g_bFlagLcdOpen == FALSE)
	{
		InitLCD();
	}

	ioctl(g_nLcd_fd,(UINT)nMode,0);

	return ERR_LCD_OK;
}

/*==========================================================================*
 * FUNCTION : ClearScreen
 * PURPOSE  : Clear screen
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 18:31
 *==========================================================================*/
int ClearScreen(void)
{
	if(g_bFlagLcdOpen == FALSE)
	{
		InitLCD();
	}


	ioctl(g_nLcd_fd,IOC_LCD_CLEAR,0);//Clear screen


	return ERR_LCD_OK;
}


/*==========================================================================*
 * FUNCTION : Light
 * PURPOSE  : Open or close the back light
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BOOL  bLight : TRUE, open, FALSE, close
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 15:37
 *==========================================================================*/
int Light(BOOL bLight)
{
	if(g_bFlagLcdOpen == FALSE)
	{
		InitLCD();
	}

	g_iLightValue = bLight ? OPEN_LIGHT_VALUE : CLOSE_LIGHT_VALUE;

	ioctl(g_nLcd_fd, IOC_SET_BL, g_iLightValue | g_iBrightValue);

	return ERR_LCD_OK;
}


/*==========================================================================*
 * FUNCTION : AdjustBright
 * PURPOSE  : Adjust the brightness step by step
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BOOL  bAddBright : TRUE,brightness + 1, FALSE, -1
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 15:38
 *==========================================================================*/
int AdjustBright(BOOL bAddBright)
{
	if(g_bFlagLcdOpen == FALSE)
	{
		InitLCD();
	}

	g_iBrightValue = bAddBright ? (g_iBrightValue + LCD_BRIGHT_STEP)
		: (g_iBrightValue - LCD_BRIGHT_STEP);
		
	if(g_iBrightValue  > MAXIMUM_BRIGHT_VALUE)
	{
		g_iBrightValue = MAXIMUM_BRIGHT_VALUE;
	}
	else if(g_iBrightValue  < MINIMUM_BRIGHT_VALUE)
	{
		g_iBrightValue = MINIMUM_BRIGHT_VALUE;
	}

	//open the back light directly
	//to IOC_SET_BL, should fill back light switch and value.
	ioctl(g_nLcd_fd, IOC_SET_BL, g_iLightValue | g_iBrightValue);

	return ERR_LCD_OK;
}

/*==========================================================================*
 * FUNCTION : AdjustKeyParam
 * PURPOSE  : Adjust the key params(Rate and Delay), support two modes
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BOOL  bAccelerateFlag : TRUE: short Delay, FALSE: long delay
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 15:41
 *==========================================================================*/
int AdjustKeyParam(BOOL bAccelerateFlag)
{
	int nKeyParam;

	if(g_bFlagLcdOpen == FALSE)
	{
		InitLCD();
	}

	if (!bAccelerateFlag)
	{
		nKeyParam = ((12 << 8) + 4);

	}
	else
	{
		nKeyParam = ((12 << 8) + 15);
	}

	
	ioctl(g_nKey_fd, IOC_KEY_DELAY | IOC_KEY_RATE, nKeyParam);

	return ERR_LCD_OK;
}


/*==========================================================================*
 * FUNCTION : BuzzerBeep
 * PURPOSE  : Open or close the buzzer beep
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BOOL  bOpen : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 15:43
 *==========================================================================*/
int BuzzerBeep(BOOL bOpen)
{
	if(g_bFlagLcdOpen == FALSE)
	{
		InitLCD();
	}

	if(bOpen)
	{
		ioctl(g_nLcd_fd, IOC_BEEP_ON, 0);
	}
	else
	{
		ioctl(g_nLcd_fd, IOC_BEEP_OFF, 0);
	}

	return ERR_LCD_OK;
}

typedef struct
{
	BOOL bBeepFLag;
    int  nTimeout;

	time_t	tmStartBeep;

}BUZZER_TASK_PARAM;


static DWORD LCD_BuzzerBeepTask(IN BUZZER_TASK_PARAM *pParam)
{
	HANDLE			hSelf = RunThread_GetId(NULL);

	ASSERT(pParam);

	// main loop of buzzer beep task
	while (THREAD_IS_RUNNING(hSelf))
	{
		RunThread_Heartbeat(hSelf);	//heartbeat

		if (pParam->bBeepFLag)
		{
			ioctl(g_nLcd_fd, IOC_BEEP_ON, 0);

			if (pParam->nTimeout != -1)
			{
				if ((pParam->nTimeout) >= 1000)//If >= 1s
				{
					time_t	tmNow = time(NULL);

					if ((tmNow - pParam->tmStartBeep) >= (pParam->nTimeout)/1000)
					{
						pParam->bBeepFLag = FALSE;
						pParam->nTimeout = 0;
					}
					//Maybe modify time by user interface
					else if (tmNow < pParam->tmStartBeep)
					{
						pParam->tmStartBeep = tmNow;
					}
				}					
				else 
				{
					if (pParam->nTimeout > BUZZER_BEEP_TIMEOUT_UNIT)
					{
						pParam->nTimeout -= BUZZER_BEEP_TIMEOUT_UNIT;

					}
					else
					{
						pParam->bBeepFLag = FALSE;
						pParam->nTimeout = 0;
					}
				}
			}
		}
		else //if (!pParam->bBeepFLag)
		{
			ioctl(g_nLcd_fd, IOC_BEEP_OFF, 0);
		}

		Sleep(BUZZER_BEEP_TIMEOUT_UNIT);

	}

	return 0;
}

/*==========================================================================*
 * FUNCTION : BuzzerBeepCtrl
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BOOL  bOpen  : 
 *            int   nParam : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  :                  DATE: 2006-06-12 18:50
 *==========================================================================*/
int BuzzerBeepCtrl(BOOL bOpen, int nParam)
{
	static BOOL s_bBuzzerThreadExist = FALSE;
	static BUZZER_TASK_PARAM s_stBuzzerParam = {0, 0, 0};

	HANDLE			hBuzzerBeepThread = NULL;

	if(g_bFlagLcdOpen == FALSE)
	{
		InitLCD();
	}

	if (!s_bBuzzerThreadExist)
	{
		// create the Buzzer Beep thread
		hBuzzerBeepThread = RunThread_Create("Buzzer Beep thread",
			(RUN_THREAD_START_PROC)LCD_BuzzerBeepTask,
			(void *)(&s_stBuzzerParam),
			NULL,
			0);

		if (hBuzzerBeepThread == NULL)
		{
			AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
				"Fails on creating Buzzer Beep thread task.\n");
		}
		else
		{
			s_bBuzzerThreadExist = TRUE;

	extern BOOL SetBuzzerBeepThredID(HANDLE hThreadID);
			SetBuzzerBeepThredID(hBuzzerBeepThread);
		}
	}

	s_stBuzzerParam.bBeepFLag = bOpen;
	s_stBuzzerParam.nTimeout = nParam;

	if (bOpen)
	{
		ioctl(g_nLcd_fd, IOC_BEEP_ON, 0);

		s_stBuzzerParam.tmStartBeep = time(NULL);
	}

	return ERR_LCD_OK;
}


/*==========================================================================*
 * FUNCTION : AlarmLedCtrl
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int   nAlarmLed : 
 *            BOOL  bOpen     : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  :                  DATE: 2006-06-12 18:52
 *==========================================================================*/
int AlarmLedCtrl(int nAlarmLed, BOOL bOpen)
{
	BYTE		byAlarmLedValue = 0;

	if (g_nAlarmLed_fd == 0)
	{
		g_nAlarmLed_fd = open(ALARM_LED_NAME,O_RDWR);

		if(g_nAlarmLed_fd <=0)
		{
			TRACE("[%s]--InitLCD: ERROR: failed to open /dev/led!\n\r", 
				__FILE__);

			AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
				"[%s]--InitLCD: ERROR: failed to open /dev/led!\n\r", 
				__FILE__);

			g_nAlarmLed_fd = 0;

			return ERR_LCD_OPEN_LED_DRIVER_FAILURE;
		}
	}
	//Frank Wu,20160127, for MiniNCU
#if 0
	read(g_nAlarmLed_fd, &byAlarmLedValue, 1);

	if (bOpen)
	{
		byAlarmLedValue |= ((BYTE)nAlarmLed);
	}
	else
	{
		byAlarmLedValue &= (~((BYTE)nAlarmLed));
	}

	write(g_nAlarmLed_fd, &byAlarmLedValue, 1);
#else
	byAlarmLedValue = bOpen? LED_ON_VAL: LED_OFF_VAL;

	switch(nAlarmLed)
	{
	case LED_RUN:
		{
			ioctl(g_nAlarmLed_fd, IOCTL_SET_GLED, byAlarmLedValue);
			break;
		}
	case LED_YEL:
		{
			ioctl(g_nAlarmLed_fd, IOCTL_SET_YLED, byAlarmLedValue);
			break;
		}
	case LED_RED:
		{
			ioctl(g_nAlarmLed_fd, IOCTL_SET_RLED, byAlarmLedValue);
			break;
		}
	default:
		{
			break;
		}
	}
#endif

	return ERR_LCD_OK;
}


/*==========================================================================*
 * FUNCTION : DisCursor
 * PURPOSE  : Display cursor on appointed position
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  x         : 
 *            int  y         : 
 *            int  nDispType : The positive or reverse display mode
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 15:44
 *==========================================================================*/
int DisCursor(int x, int y, int	nDispType)
{

	//If need to modify cursor shape, change the matrix 
	static unsigned char s_CursorBmp[]=
	{
		0x00,0xFF,0xFF,0x00,0xFF,0xFF
	};

#define CURSOR_DOT_NUMBER sizeof(s_CursorBmp)

	LCD_DOT_STRU stLcdDot[CURSOR_DOT_NUMBER];

	int nCursorWidth = CURSOR_DOT_NUMBER/2;

	int i;


	if(g_bFlagLcdOpen == FALSE)
	{
		InitLCD();
	}

	for(i = 0; i < (int)CURSOR_DOT_NUMBER; i++)
	{
		LCD_DOT_STRU_ASSIGN(stLcdDot + i, 
			x + i % nCursorWidth, 
			y + i / nCursorWidth * BASE_HEIGHT,
			(nDispType ? (s_CursorBmp[i]) : (~(s_CursorBmp[i]))));
	}
	
	
	write(g_nLcd_fd, stLcdDot, sizeof(stLcdDot));


	return ERR_LCD_OK;
}



/*==========================================================================*
 * FUNCTION : LCD_DisplayBmp
 * PURPOSE  : Display the a B&W bmp img screen
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  x : 
 *            int  y : 
 *            char *pBmpImg : the B&W color bmp img
 *            int  nBmpSize : the byte of the pBmpImg
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 16:02
 *==========================================================================*/
int LCD_DisplayBmp(IN int x, IN int y, IN char *pBmpImg, IN int nBmpSize)
{
	int nRowSize = nBmpSize / SCREEN_WIDTH;
	LCD_DOT_STRU stLcdDot[SCREEN_WIDTH * SCREEN_HEIGHT / BASE_HEIGHT];
	ZERO_POBJS(stLcdDot, SCREEN_WIDTH * SCREEN_HEIGHT / BASE_HEIGHT);

	int i = 0, j = 0;

	if(g_bFlagLcdOpen == FALSE)
	{
		InitLCD();
	}

	ClearScreen();

	for(i = 0;i < nRowSize;i++)
	{
		for(j = 0;j < SCREEN_WIDTH;j++)
		{
			if (i * SCREEN_WIDTH + j >= (int)sizeof(stLcdDot))
			{
				break;//The bmp is too large
			}

			LCD_DOT_STRU_ASSIGN(stLcdDot + i * SCREEN_WIDTH + j, 
				x + j, 
				y + i * BASE_HEIGHT,
				pBmpImg[i * SCREEN_WIDTH + j]);
		}
	}


	write(g_nLcd_fd, stLcdDot, sizeof(stLcdDot));

	return ERR_LCD_OK;
}



#if 0	//reserved. please use: LCD_DisplayBmp
/*==========================================================================*
 * FUNCTION : DisWelcomeScreen
 * PURPOSE  : Display the welcome screen
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  x : 
 *            int  y : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 16:02
 *==========================================================================*/
int DisWelcomeScreen(int x,int y)
{
	unsigned char WelcomeBmp[]=
	{
		/*------------------------------------------------------------------------------
		The bitmap displays the Vertivv icon
		------------------------------------------------------------------------------*/
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x80,0xC0,
			0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x80,0xD0,0xF8,0xFE,0x9F,0xFB,
			0xF7,0xEF,0xDE,0xDC,0xB8,0xF0,0xE0,0xC0,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x38,
			0xFE,0xFF,0xEF,0xBC,0x7C,0x77,0xFF,0xEE,
			0xFE,0x3D,0xFF,0x77,0x4F,0x3C,0x1E,0x07,
			0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x01,0x03,0x07,0x0E,0x3E,0x7B,
			0x37,0x0F,0x07,0x03,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0xFE,0xFF,0x03,0x03,0xFF,0x35,0x35,
			0x35,0x37,0x05,0x03,0x00,0xFE,0xFE,0xE2,
			0xFC,0x7E,0xFC,0x70,0xE0,0x40,0x58,0xFC,
			0x2E,0xE2,0xC2,0x02,0xFE,0x00,0xFF,0xFF,
			0x03,0xCB,0x35,0x35,0x35,0x35,0x37,0x05,
			0x03,0x00,0xFF,0xFF,0x03,0x9F,0x21,0xA5,
			0x16,0x0A,0x62,0xB6,0x1C,0x14,0x00,0x12,
			0x32,0x22,0x7F,0x7D,0x32,0x72,0xA6,0x07,
			0xE6,0xC2,0x00,0xF8,0x06,0xF2,0xBF,0x07,
			0x02,0x02,0x07,0x9F,0x7A,0x66,0x06,0xF0,
			0x00,0xFF,0xFF,0x03,0x03,0x1E,0x74,0xB8,
			0x70,0x40,0x9E,0x7F,0x03,0xFF,0xFE,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x07,0x07,0x04,0x04,0x07,0x06,0x06,
			0x06,0x06,0x06,0x86,0x80,0x87,0x87,0x07,
			0x87,0x80,0x00,0x01,0x04,0x07,0x01,0x00,
			0x80,0x87,0x87,0x04,0x07,0x00,0x07,0x07,
			0x04,0x05,0x06,0x06,0x06,0x06,0x06,0x06,
			0x06,0x00,0x07,0x07,0x04,0x07,0x00,0xC0,
			0xC1,0x03,0x06,0x04,0x07,0x07,0x04,0x86,
			0x87,0x86,0x86,0x86,0x06,0x06,0x07,0x06,
			0x03,0x03,0x00,0x01,0x03,0x06,0x07,0x07,
			0x06,0x06,0x07,0x07,0x07,0x06,0x03,0x01,
			0x00,0x07,0x07,0x04,0x00,0x00,0x00,0x00,
			0x01,0x03,0x06,0x04,0x04,0x07,0x00,0x06,
			0x04,0x04,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x3F,0x3F,0x07,0x0F,0x3C,
			0x3F,0x3F,0x1C,0x1E,0x3E,0x2E,0x2E,0x0E,
			0x1F,0x3F,0x32,0x12,0x0E,0x3E,0x3C,0x3E,
			0x06,0x3E,0x3C,0x3E,0x06,0x1C,0x3E,0x22,
			0x3E,0x1E,0x0C,0x3E,0x3E,0x02,0x02,0x3F,
			0x3F,0x1C,0x3E,0x32,0x20,0x00,0x00,0x3F,
			0x3F,0x0C,0x0F,0x07,0x01,0x3E,0x36,0x22,
			0x3E,0x1E,0x02,0x1E,0x3C,0x3E,0x0E,0x3E,
			0x30,0x3E,0x06,0x1E,0x3E,0x2E,0x2E,0x0E,
			0x3E,0x3E,0x3E,0x02,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
	};


	LCD_DOT_STRU stLcdDot[SCREEN_WIDTH * SCREEN_HEIGHT / BASE_HEIGHT];


	int nBmpSize = sizeof(WelcomeBmp)/sizeof(char);

	int nRowSize = nBmpSize / SCREEN_WIDTH;

	int i = 0, j = 0;

	UNUSED(x);
	UNUSED(y);

	if(g_bFlagLcdOpen == FALSE)
	{
		InitLCD();
	}


	ClearScreen();

	for(i = 0;i < nRowSize;i++)
	{
		for(j = 0;j < SCREEN_WIDTH;j++)
		{
			if (i * SCREEN_WIDTH + j >= (int)sizeof(stLcdDot))
			{
				break;//The bmp is too large
			}

			LCD_DOT_STRU_ASSIGN(stLcdDot + i * SCREEN_WIDTH + j, 
				j, 
				i * BASE_HEIGHT,
				WelcomeBmp[i * SCREEN_WIDTH + j]);
		}
	}


	write(g_nLcd_fd, stLcdDot, sizeof(stLcdDot));

	return ERR_LCD_OK;
}
#endif //if 0

/*==========================================================================*
 * FUNCTION : ClearBar
 * PURPOSE  : Clear the appointed rect
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  x         : Rect start x position
 *            int  y         : Rect start y position
 *            int  nWidth    : Rect width
 *            int  nHeight   : Rect height
 *            int  nDispType : reverse mode
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 16:43
 *==========================================================================*/
int ClearBar( int x,
			 int y,
			 int nWidth,
			 int nHeight, 
			 int  nDispType)
{
	LCD_DOT_STRU* pLcdDot;

	int nRowSize = nHeight / BASE_HEIGHT;

	int i = 0, j = 0;

	ASSERT(y % 8 == 0 && nHeight % 8 == 0);

	ASSERT(x + nWidth <= SCREEN_WIDTH);
	ASSERT(y + nHeight <= SCREEN_HEIGHT);

	if(nWidth <=0 || nHeight <= 0)
	{
		return ERR_LCD_OK;
	}

	pLcdDot = NEW(LCD_DOT_STRU, nWidth * nHeight / BASE_HEIGHT);

	if (pLcdDot == NULL)
	{
		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
			"[%s]--DisString: ERROR: There is no enough memory! ", 
			__FILE__);

		return ERR_LCD_NO_MEMORY;
	}

	for(i = 0;i < nRowSize;i++)
	{
		for(j = 0;j < nWidth;j++)
		{
			LCD_DOT_STRU_ASSIGN(pLcdDot + i * nWidth + j, 
				x + j, 
				y + i * BASE_HEIGHT,
				nDispType ? 0x00 : 0xFF);
		}
	}

	write(g_nLcd_fd, pLcdDot, 
		(sizeof(LCD_DOT_STRU)) * nWidth * nHeight / BASE_HEIGHT);

	DELETE(pLcdDot);

	return ERR_LCD_OK;
}

//Frank Wu,20160127, for MiniNCU
#include "iconv.h"

#define LCD_INTERFACE_CODE_ASCII			"ascii"
#define LCD_INTERFACE_CODE_UTF8				"utf-8"
#define LCD_INTERFACE_CODE_UTF16			"utf-16"
#define LCD_INTERFACE_CODE_GBK				"GBK"
#define LCD_INTERFACE_CODE_BIG5				"BIG5"
#define LCD_INTERFACE_CODE_KOI8R			"KOI8-R"
#define LCD_INTERFACE_CODE_LATIN1			"latin-1"
#define LCD_INTERFACE_CODE_LATIN5			"latin5"
#define LCD_INTERFACE_CODE_LATIN9			"latin-9"
#define LCD_INTERFACE_CODE_LATIN10			"latin-10"

#define LCD_FONT_LIB_USE_ANSI_STR_MAX		256

static char g_s_szUTF8StrBuf[LCD_FONT_LIB_USE_ANSI_STR_MAX];
static char* g_s_pszCharsetMapTable[UI_BASE_FONT_CODE_MAX] = {
		LCD_INTERFACE_CODE_ASCII,
		LCD_INTERFACE_CODE_UTF8,
		LCD_INTERFACE_CODE_UTF16,
		LCD_INTERFACE_CODE_GBK,
		LCD_INTERFACE_CODE_BIG5,
		LCD_INTERFACE_CODE_KOI8R,
		LCD_INTERFACE_CODE_LATIN1,
		LCD_INTERFACE_CODE_LATIN5,
		LCD_INTERFACE_CODE_LATIN9,
		LCD_INTERFACE_CODE_LATIN10,
};

#define LCD_INTERFACE_GET_CODE_STR(iArgIndex)		((iArgIndex < UI_BASE_FONT_CODE_MAX)? \
													g_s_pszCharsetMapTable[iArgIndex]: g_s_pszCharsetMapTable[UI_BASE_FONT_CODE_ASCII])

/*==========================================================================*
* FUNCTION : code_convert
* PURPOSE  : Convert from code to another code 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: 
* RETURN   : t : 
* COMMENTS : 
*
* CREATOR  :Marco                  DATE: 2013-8-07 16:20
*==========================================================================*/
//代码转换:从一种编码转为另一种编码
static int code_convert(char *from_charset,char *to_charset,char *inbuf,int inlen,char *outbuf,int outlen)
{
	iconv_t cd;
	int rc;
	char **pin = &inbuf;
	char **pout = &outbuf;

	cd = iconv_open(to_charset,from_charset);
	if (cd==0) return -1;
	memset(outbuf,0,outlen);
	if (iconv(cd,pin,&inlen,pout,&outlen)==-1)
	{
		iconv_close(cd);
		return -1;
	}
	else
	{
		iconv_close(cd);
		return 0;
	}

}

static int ConvertUTF8to(char *to_charset, char *inbuf,int inlen,char *outbuf,int outlen)
{

	return code_convert(LCD_INTERFACE_CODE_UTF8, to_charset, inbuf, inlen, outbuf, outlen);
}



/*==========================================================================*
 * FUNCTION : DisString
 * PURPOSE  : Display string on the screen
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char*  pBuf      : The characters buffer
 *            int    nLength   : The characters number 
 *            int    x         : x pixel offset on the LCD screen
 *            int    y         : y pixel offset on the LCD screen
 *            char*  pLangCode : Language code
 *            int    nDispType : Display mode 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-11-12 18:29
 *==========================================================================*/
int DisString(char* pBuf,
		int		nLength,
		int		x,
		int		y,
		char*	pLangCode, 
		int		nDispType)
{
	UCHAR*		pRetMatrix = NULL;
	int			nReadTextNum = 0;

	FONT_SIZE	stFontSize;

	int			nDrawLength = 0;

	LCD_DOT_STRU*	pLcdDot = NULL;
	int				nDrawDotNum = 0;

	int i = 0;

	int	nError = ERR_LCD_OK;


#ifdef LCD_INTERFACE_USE_UTF8_CODE
	FONT_SIZE	stTempFontSize;
	int			iTempRet;

	memset(&stTempFontSize, 0, sizeof(stTempFontSize));
	g_s_szUTF8StrBuf[0] = '\0';

	iTempRet = GetFontSize(&stTempFontSize, pLangCode);
	if(ERR_LCD_OK == iTempRet)
	{
		iTempRet = ConvertUTF8to(LCD_INTERFACE_GET_CODE_STR(stTempFontSize.byFontCharCode),
			pBuf, nLength, g_s_szUTF8StrBuf, sizeof(g_s_szUTF8StrBuf));
		if(0 == iTempRet)
		{
			pBuf = g_s_szUTF8StrBuf;
			nLength = strlen(g_s_szUTF8StrBuf);
		}
		else
		{
			//printf("ConvertUTF8to %s failed!\n", LCD_INTERFACE_GET_CODE_STR(stTempFontSize.byFontCharCode));
		}
	}
#endif


	if(g_bFlagLcdOpen == FALSE)
	{
		InitLCD();
	}

	//ASSERT(x >= 0 && y >= 0 && x < SCREEN_WIDTH 
	//	&& y < SCREEN_HEIGHT && y % BASE_HEIGHT == 0);	

	if(!(x >= 0 && y >= 0 && x < SCREEN_WIDTH 
		&& y < SCREEN_HEIGHT && y % BASE_HEIGHT == 0))
	{
		return ERR_LCD_FAILURE;
	}

	pLcdDot = NEW(LCD_DOT_STRU, nLength * A_CHAR_MAX_DOT_NUM);

	if (pLcdDot == NULL)
	{
		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
			"[%s]--DisString: ERROR: There is no enough memory! ", 
			__FILE__);

		return ERR_LCD_NO_MEMORY;
	}


	while( (pBuf != NULL) && ( *pBuf != 0)
		&&(nReadTextNum = 
		GetFontMatrix((UCHAR*)pBuf, &pRetMatrix, &stFontSize,pLangCode)) != 0
		&& nDrawLength < nLength)
	{
		if(pRetMatrix == NULL)
		{
			continue;
		}

		if((x + stFontSize.byFontWidth) > SCREEN_WIDTH)
		{
			nError = ERR_LCD_EXCEED_RIGHT_BORDER;
			break;
		}


		for(i = 0;i < stFontSize.byFontSize;i++, pRetMatrix++)
		{

			LCD_DOT_STRU_ASSIGN(pLcdDot + nDrawDotNum + i, 
				x + i%(stFontSize.byFontWidth), 
				y + i/(stFontSize.byFontWidth) * BASE_HEIGHT,
				(nDispType ? (*pRetMatrix) : (~(*pRetMatrix))));

		}		

		pBuf += nReadTextNum;

		nDrawLength += nReadTextNum;

		nDrawDotNum += stFontSize.byFontSize;

		x += stFontSize.byFontWidth;
	}
#if 0
	LCD_DOT_STRU*	pLcdDotTemp = pLcdDot;
	for(i = 0; i < nDrawDotNum; i++)
	{
		printf("[%d] x=%04d,y=%04d,%2x\n", i, pLcdDotTemp->x, pLcdDotTemp->y,pLcdDotTemp->data);
		pLcdDotTemp++;
	}
#endif

	write(g_nLcd_fd, pLcdDot, nDrawDotNum * sizeof(LCD_DOT_STRU));


	DELETE(pLcdDot);

	return nError;
}


/*==========================================================================*
 * FUNCTION : GetWidthOfString
 * PURPOSE  : Get the display screen width of a appointed buffer and its length 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char*  pBuf      : 
 *            int    nLength   : 
 *            char*  pLangCode : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 16:49
 *==========================================================================*/
int GetWidthOfString(char*	pBuf, int nLength, char* pLangCode)
{
	UCHAR*		pRetMatrix = NULL;
	int			nReadTextNum = 0;

	FONT_SIZE	stFontSize;

	int			nDrawLength = 0;

	int			nDrawScreenWidth = 0;


#ifdef LCD_INTERFACE_USE_UTF8_CODE
	FONT_SIZE	stTempFontSize;
	int			iTempRet;

	memset(&stTempFontSize, 0, sizeof(stTempFontSize));
	g_s_szUTF8StrBuf[0] = '\0';

	iTempRet = GetFontSize(&stTempFontSize, pLangCode);
	if(ERR_LCD_OK == iTempRet)
	{
		iTempRet = ConvertUTF8to(LCD_INTERFACE_GET_CODE_STR(stTempFontSize.byFontCharCode),
			pBuf, nLength, g_s_szUTF8StrBuf, sizeof(g_s_szUTF8StrBuf));
		if(0 == iTempRet)
		{
			pBuf = g_s_szUTF8StrBuf;
			nLength = strlen(g_s_szUTF8StrBuf);
		}
		else
		{
			//printf("ConvertUTF8to %s failed!\n", LCD_INTERFACE_GET_CODE_STR(stTempFontSize.byFontCharCode));
		}
	}
#endif


	while( (pBuf != NULL) && ( *pBuf != 0)
		&&(nReadTextNum = 
		GetFontMatrix((UCHAR*)pBuf, &pRetMatrix, &stFontSize,pLangCode)) != 0
		&& nDrawLength < nLength)
	{
		ASSERT(pRetMatrix);

		pBuf += nReadTextNum;

		nDrawLength += nReadTextNum;

		nDrawScreenWidth += stFontSize.byFontWidth;

	}


	return nDrawScreenWidth;
}

#define KEY_NORMAL_PRESS_BUZEEER_BEEP_TIME		BUZZER_BEEP_TIMEOUT_UNIT * 3
#define KEY_ACCELERATE_PRESS_BUZEEER_BEEP_TIME	BUZZER_BEEP_TIMEOUT_UNIT * 1

int g_nKeyPressBuzzerBeepTime = KEY_NORMAL_PRESS_BUZEEER_BEEP_TIME;

/*==========================================================================*
 * FUNCTION : GetKey
 * PURPOSE  : Get the current pressed key
 *			  Considered key accelerated
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void : 
 * RETURN   : UCHAR : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 16:50
 *==========================================================================*/
UCHAR GetKey(void)
{
	/*UCHAR byKeyValue;

	static int s_nKeyAccelerateCount = 0;

	if(g_bFlagLcdOpen == FALSE)
	{
		InitLCD();
	}


	read(g_nKey_fd, &byKeyValue, 1);

	if(byKeyValue == 0)
	{
		s_nKeyAccelerateCount = 0;
	}
	else
	{
		s_nKeyAccelerateCount++;

		if(s_nKeyAccelerateCount >= 4)
		{
			AdjustKeyParam(TRUE);
		}
		else
		{
		AdjustKeyParam(FALSE);
		}
		}*/


	UCHAR byKeyValue;

	static int s_nKeyAccelerateCount = 0;

	static int s_nKeyZeroCount = 0;

	static BOOL s_bAccelateFlag = FALSE;

	if(g_bFlagLcdOpen == FALSE)
	{
		InitLCD();
	}

	read(g_nKey_fd, &byKeyValue, 1);

	if(byKeyValue == 0)
	{
		s_nKeyZeroCount++;
	}
	else
	{
		if (s_nKeyZeroCount < 5)
		{
			s_nKeyAccelerateCount++;
		}
		else
		{
			s_nKeyAccelerateCount = 0;
		}

		s_nKeyZeroCount = 0;		

		if(s_nKeyAccelerateCount >= 5)
		{
			if (!s_bAccelateFlag)
			{
				AdjustKeyParam(TRUE);

				s_bAccelateFlag = TRUE;

				g_nKeyPressBuzzerBeepTime = 
					KEY_ACCELERATE_PRESS_BUZEEER_BEEP_TIME;
			}
		}

	}

	if (s_nKeyZeroCount >= 5)
	{
		if (s_bAccelateFlag)
		{
			AdjustKeyParam(FALSE);

			s_bAccelateFlag = FALSE;

			g_nKeyPressBuzzerBeepTime = KEY_NORMAL_PRESS_BUZEEER_BEEP_TIME;
		}

		s_nKeyZeroCount = 0;
		s_nKeyAccelerateCount = 0;
	}


	//printf("byKeyValue = %d, s_nKeyZeroCount = %d s_nKeyAccelerateCount = %d\n", byKeyValue, s_nKeyZeroCount, s_nKeyAccelerateCount);
	//by HULONGWEN Because the key operation is too fast
	//Maybe need delay

	return byKeyValue;
}

/*==========================================================================*
 * FUNCTION : RegisterFontLib
 * PURPOSE  : Regist the supported languages
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int     nLangNum   : languages num need to be initted
 *            char**  ppLangCode : languages codes, include nLangNum languages
 * RETURN   : int : The success of failure flag for initted result,
 *			  ERR_LCD_OK is success.
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 11:50
 *==========================================================================*/
int RegisterFontLib(int nLangNum, char** ppLangCode, BOOL bRegFlag)
{
	if(bRegFlag)
	{
		return InitFontLib(nLangNum, ppLangCode);
	}
	else
	{
		return CloseFontLib(nLangNum, ppLangCode);
	}
}

/*==========================================================================*
 * FUNCTION : GetFontHeightFromCode
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char*  pLangCode : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  :                  DATE: 2006-06-12 19:00
 *==========================================================================*/
int GetFontHeightFromCode(char* pLangCode)
{
	return GetFontHeight(pLangCode);
}

/*==========================================================================*
 * FUNCTION : DisSpecifiedFlag
 * PURPOSE  : Display a specified flag on screen, the matrix in ascii lib
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char  cFlag : 
 *            int   x     : 
 *            int   y     : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 16:55
 *==========================================================================*/
int DisSpecifiedFlag(char cFlag, int x, int y)
{
	DisString(&cFlag,
		sizeof(cFlag),
		x,
		y,
		LCD_FIXED_LANGUAGE, 
		FLAG_DIS_NORMAL_TYPE);

	return ERR_LCD_OK;
}


/*==========================================================================*
 * FUNCTION : DisplayProgressBar
 * PURPOSE  : Display the progress bar
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  nItems  : 
 *            int  nCur    : 
 *            int  nTop    : 
 *            int  nBottom : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : HULONGWEN                DATE: 2004-12-04 16:56
 *==========================================================================*/
int DisplayProgressBar(int	nItems,
					   int	nCur,
					   int	nTop,
					   int	nBottom)
{
	unsigned char UpIconBmp[]=
	{
		0x00,0x60,0x78,0x7E,0x7F,0x7E,0x78,0x60
	};

	unsigned char DownIconBmp[]=
	{

		0x00,0x06,0x1E,0x7E,0xFE,0x7E,0x1E,0x06
	};


	LCD_DOT_STRU stLcdDot[ARROW_WIDTH * SCREEN_HEIGHT / BASE_HEIGHT];

	UCHAR sDisplayProgressDot[SCREEN_HEIGHT - BASE_HEIGHT * 2];

	UCHAR sDisplayProgressChar[(SCREEN_HEIGHT - BASE_HEIGHT * 2)/BASE_HEIGHT];

	int nCharOffset;


	int nRowSize;
	int nProgressHeight;

	int nDotStart, nDotNum;

	int i = 0, j = 0, k = 0;

	UNUSED(nCur);

	//If the region can place all the items, don't need progress bar
	if(nItems <= nBottom - nTop + 1)
	{
		ClearBar(SCREEN_WIDTH - ARROW_WIDTH, 
			0, 
			ARROW_WIDTH, 
			SCREEN_HEIGHT, 
			FLAG_DIS_NORMAL_TYPE);

		return ERR_LCD_OK;
	}

	//Top arrow
	for(i = 0, j = 0; j < ARROW_WIDTH; j++)
	{
		ASSERT(i * ARROW_WIDTH + j <= (int)sizeof(stLcdDot));

		LCD_DOT_STRU_ASSIGN(stLcdDot + i * ARROW_WIDTH + j, 
			SCREEN_WIDTH - ARROW_WIDTH + j, 
			i * BASE_HEIGHT,
			UpIconBmp[j]);
	}


	//Middow progress bar
	nRowSize =  SCREEN_HEIGHT / BASE_HEIGHT;

	memset(sDisplayProgressChar, 0, sizeof(sDisplayProgressChar));

	nProgressHeight = sizeof(sDisplayProgressDot);

	nDotStart = nProgressHeight * nTop / nItems;
	nDotNum = nProgressHeight * (nBottom - nTop + 1) / nItems;
	if(nDotNum < 1)		//The length of the bar is at least 1 pixel
	{
		nDotNum = 1;
	}

	for(k = 0; k < nProgressHeight; k++)
	{
		(k >= nDotStart && k < nDotStart + nDotNum) 
			? (sDisplayProgressDot[i] = 1)
			: (sDisplayProgressDot[i] = 0);


		nCharOffset = k / BASE_HEIGHT;

		sDisplayProgressChar[nCharOffset] |= 
			(sDisplayProgressDot[i] << (k % BASE_HEIGHT) );

	}

	for(i = 1, nRowSize -= 1; i < nRowSize;i++)
	{
		for(j = 0;j < ARROW_WIDTH;j++)
		{
			ASSERT(i * ARROW_WIDTH + j <= (int)sizeof(stLcdDot));

			LCD_DOT_STRU_ASSIGN(stLcdDot + i * ARROW_WIDTH + j, 
				SCREEN_WIDTH - ARROW_WIDTH + j,
				i * BASE_HEIGHT,
				(j == 0) 
				? 0x00 
				: sDisplayProgressChar[i - 1]);
		}
	}


	//Bottom arrow
	for(i = nRowSize, j = 0;j < ARROW_WIDTH;j++)
	{
		ASSERT(i * ARROW_WIDTH + j <= (int)sizeof(stLcdDot));

		LCD_DOT_STRU_ASSIGN(stLcdDot + i * ARROW_WIDTH + j, 
			SCREEN_WIDTH - ARROW_WIDTH + j, 
			i * BASE_HEIGHT,
			DownIconBmp[j]);
	}


	write(g_nLcd_fd, stLcdDot, sizeof(stLcdDot));

	return ERR_LCD_OK;

}

int DisplayProgressArrow(int	nItems,
					   int	nCur,
					   int	nTop,
					   int	nBottom)
{
	unsigned char UpIconBmp[]=
	{
		0x00,0x60,0x78,0x7E,0x7F,0x7E,0x78,0x60,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
	};

	unsigned char DownIconBmp[]=
	{
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x06,0x1E,0x7E,0xFE,0x7E,0x1E,0x06
			
	};

#define UP_ICON_WIDTH	(sizeof(UpIconBmp)/sizeof(char))
#define DOWN_ICON_WIDTH	(sizeof(DownIconBmp)/sizeof(char))

	BOOL bDrawOrClear = TRUE;

	LCD_DOT_STRU stLcdDot[UP_ICON_WIDTH + DOWN_ICON_WIDTH];

	int j = 0;

	UNUSED(nCur);

	//If the region can place all the items, don't need progress bar
	if(nItems <= nBottom - nTop + 1)
	{
		bDrawOrClear = FALSE;
	}

	//Top arrow
	for (j = 0; j < (int)UP_ICON_WIDTH; j++)
	{
		LCD_DOT_STRU_ASSIGN(stLcdDot + j, 
			SCREEN_WIDTH - ARROW_WIDTH + j % ARROW_WIDTH, 
			(j / ARROW_WIDTH) * BASE_HEIGHT,
			bDrawOrClear ? UpIconBmp[j] : 0x00);
	}

	//Top arrow
	for (j = 0; j < (int)DOWN_ICON_WIDTH; j++)
	{
		LCD_DOT_STRU_ASSIGN(stLcdDot + UP_ICON_WIDTH + j, 
			SCREEN_WIDTH - ARROW_WIDTH + j % ARROW_WIDTH, 
			SCREEN_HEIGHT - DOWN_ICON_WIDTH / ARROW_WIDTH * BASE_HEIGHT
			+ (j / ARROW_WIDTH) * BASE_HEIGHT,
			bDrawOrClear ? DownIconBmp[j] : 0x00);
	}


	write(g_nLcd_fd, stLcdDot, sizeof(stLcdDot));

	return ERR_LCD_OK;

}

//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

/*==========================================================================*
 * FUNCTION : SetLCDRotation
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iRotationAngle : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Fan Xiansheng            DATE: 2008-09-12 09:39
 *==========================================================================*/
int SetLCDRotation(int iRotationAngle)
{
	ASSERT(iRotationAngle % 90 == 0);

	

	if(g_bFlagLcdOpen == FALSE)
	{
		InitLCD();
	}
	g_nLcdRotation = iRotationAngle % 360;
	
	ioctl(g_nLcd_fd, IOC_SET_ROLL_DEGREE, iRotationAngle);

	
	return ERR_LCD_OK;
}

int GetLCDRotation(void)
{
	return g_nLcdRotation;
}


void SetScreenWidth(int nScreenWidth)
{
	ASSERT(nScreenWidth % BASE_HEIGHT == 0);
	g_nScreenWidth = nScreenWidth;
}

int GetScreenWidth(void)
{
	return g_nScreenWidth;
}


void SetScreenHeight(int nScreenHeight)
{
	ASSERT(nScreenHeight % BASE_HEIGHT == 0);
	if(nScreenHeight != 0)
	{
		g_nScreenHeight = nScreenHeight;
	}
}

int GetScreenHeight(void)
{
	return g_nScreenHeight;
}


void SetLCDBright(int nBright)
{
	if(nBright > MAXIMUM_BRIGHT_VALUE)
	{
		nBright = MAXIMUM_BRIGHT_VALUE;
	}
	else if(nBright < MINIMUM_BRIGHT_VALUE)
	{
		nBright = MINIMUM_BRIGHT_VALUE;
	}

	g_iBrightValue = nBright;

	ioctl(g_nLcd_fd, IOC_SET_BL, g_iLightValue | g_iBrightValue);

}

int GetLCDBright(void)
{
	return g_iBrightValue;
}




static WORD BoldDots(IN UCHAR ucOriginDots)
{
	int i;
	UCHAR ucMask = 0x01;
	UCHAR ucDotsHigh, ucDotsLow, ucBoldDotsHigh, ucBoldDotsLow;
	
	ucDotsHigh = ucOriginDots >> 4;
	ucDotsLow = ucOriginDots & 0x0F;
	ucBoldDotsHigh = 0;
	ucBoldDotsLow = 0;

	for(i = 0; i < 4; i++)
	{
		if(ucDotsHigh & (ucMask << i))
		{
			ucBoldDotsHigh |= (ucMask << (i * 2));
			ucBoldDotsHigh |= (ucMask << (i * 2 + 1));
		}
	}

	for(i = 0; i < 4; i++)
	{
		if(ucDotsLow & (ucMask << i))
		{
			ucBoldDotsLow |= (ucMask << (i * 2));
			ucBoldDotsLow |= (ucMask << (i * 2 + 1));
		}
	}

	return (ucBoldDotsHigh << 8) + ucBoldDotsLow;
}

/*==========================================================================*
 * FUNCTION : DispBigSizeString
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: char*  pBuf      : 
 *            int    nLength   : 
 *            int    x         : 
 *            int    y         : 
 *            char*  pLangCode : 
 *            int    nDispType : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Fan Xiansheng            DATE: 2008-09-12 09:38
 *==========================================================================*/
int DispBigSizeString(char* pBuf,
			  int		nLength,
			  int		x,
			  int		y,
			  char*		pLangCode, 
			  int		nDispType)
{
	UCHAR*		pRetMatrix = NULL;
	UCHAR		ucDataTemp, ucBigSizeDataHigh, ucBigSizeDataLow;
	WORD		wdBigSizeDateTemp;
	int			nReadTextNum = 0;

	FONT_SIZE	stFontSize;

	int			nDrawLength = 0;

	LCD_DOT_STRU*	pLcdDot = NULL;
	int				nDrawDotNum = 0;

	int i = 0;
	int	nDotStruPos = 0;
	int	xOffset, yOffset;

	int	nError = ERR_LCD_OK;

	if(g_bFlagLcdOpen == FALSE)
	{
		InitLCD();
	}
	
	
#ifdef LCD_INTERFACE_USE_UTF8_CODE
	FONT_SIZE	stTempFontSize;
	int			iTempRet;

	memset(&stTempFontSize, 0, sizeof(stTempFontSize));
	g_s_szUTF8StrBuf[0] = '\0';

	iTempRet = GetFontSize(&stTempFontSize, pLangCode);
	if(ERR_LCD_OK == iTempRet)
	{
		iTempRet = ConvertUTF8to(LCD_INTERFACE_GET_CODE_STR(stTempFontSize.byFontCharCode),
			pBuf, nLength, g_s_szUTF8StrBuf, sizeof(g_s_szUTF8StrBuf));
		if(0 == iTempRet)
		{
			pBuf = g_s_szUTF8StrBuf;
			nLength = strlen(g_s_szUTF8StrBuf);
		}
		else
		{
			//printf("ConvertUTF8to %s failed!\n", LCD_INTERFACE_GET_CODE_STR(stTempFontSize.byFontCharCode));
		}
	}
#endif


	ASSERT(x >= 0 && y >= 0 && x < SCREEN_WIDTH 
		&& y < SCREEN_HEIGHT && y % BASE_HEIGHT == 0);	


	pLcdDot = NEW(LCD_DOT_STRU, nLength * A_CHAR_MAX_DOT_NUM * 4);
	if (pLcdDot == NULL)
	{
		AppLogOut(LCD_UI_TASK, APP_LOG_ERROR, 
			"[%s]--DisString: ERROR: There is no enough memory! ", 
			__FILE__);

		return ERR_LCD_NO_MEMORY;
	}
	ZERO_POBJS(pLcdDot, nLength * A_CHAR_MAX_DOT_NUM * 4);


	while( (pBuf != NULL) && ( *pBuf != 0)
		&&(nReadTextNum = 
			GetFontMatrix((UCHAR*)pBuf, &pRetMatrix, &stFontSize, pLangCode)) != 0
		&& nDrawLength < nLength)
	{
		if(pRetMatrix == NULL)
		{
			continue;
		}

		if((x + stFontSize.byFontWidth) > SCREEN_WIDTH)
		{
			nError = ERR_LCD_EXCEED_RIGHT_BORDER;
			break;
		}

		for(i = 0;i < stFontSize.byFontSize;i++, pRetMatrix++)
		{
			ucDataTemp = nDispType ? (*pRetMatrix) : (~(*pRetMatrix));
			wdBigSizeDateTemp = BoldDots(ucDataTemp);

			ucBigSizeDataLow = (UCHAR)(wdBigSizeDateTemp >> 8);
			ucBigSizeDataHigh = (UCHAR)(wdBigSizeDateTemp & 0xFF);

			nDotStruPos = nDrawDotNum + i * 4;
			xOffset = i % stFontSize.byFontWidth * 2;
			yOffset = i / stFontSize.byFontWidth * 2;

			LCD_DOT_STRU_ASSIGN(pLcdDot + nDotStruPos, 
				x + xOffset, 
				y + yOffset * BASE_HEIGHT,
				ucBigSizeDataHigh);
			LCD_DOT_STRU_ASSIGN(pLcdDot + nDotStruPos + 1, 
				x + xOffset, 
				y + (yOffset + 1) * BASE_HEIGHT,
				ucBigSizeDataLow);
			LCD_DOT_STRU_ASSIGN(pLcdDot + nDotStruPos + 2, 
				x + xOffset + 1, 
				y + yOffset * BASE_HEIGHT,
				ucBigSizeDataHigh);
			LCD_DOT_STRU_ASSIGN(pLcdDot + nDotStruPos + 3, 
				x + xOffset + 1, 
				y + (yOffset + 1) * BASE_HEIGHT,
				ucBigSizeDataLow);

		}		

		pBuf += nReadTextNum;

		nDrawLength += nReadTextNum;

		nDrawDotNum += stFontSize.byFontSize * 4;

		x += stFontSize.byFontWidth * 2;

	}

	write(g_nLcd_fd, pLcdDot, nDrawDotNum * sizeof(LCD_DOT_STRU));

	DELETE(pLcdDot);

	return nError;
}

//读环境变量
/*==========================================================================*
* FUNCTION : ReadEnvironmentVar
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: char  *pEnvVarName : 
* RETURN   : static int : 
* COMMENTS : 
* CREATOR  : Fan Xiansheng            DATE: 2008-09-12 09:49
*==========================================================================*/
int ReadEnvironmentVar(char *pEnvVarName)
{

	/***********************************************/
	//读Lcd_mode时
	//0：表示要读取环境变量
	//stream = popen("./mnt/env_lcd 0 lcd_mode","r" );
	// lcd_mode 为要读取的环境变量名称
	//buf为读取的环境变量值为ASCII吗
	FILE   *stream; 
	char buf;

	char szSysCmd[200];
	snprintf(szSysCmd, 200, "%s %d %s", ENV_VAR_EXE_FILE, ENV_VAR_READ_FLAG, pEnvVarName);
	//TRACE("Read LCD Env Var cmd is %s \n", szSysCmd);

	stream = popen(szSysCmd,"r");

	fread(&buf, sizeof(char), sizeof(buf),stream);
	pclose( stream ); 

	//TRACE("The Value of Env Var [%s] is %02XH \n", pEnvVarName, buf);

	if(buf >= '0' && buf <= '9')
	{
		return buf - '0';
	}
	else
	{
		return -1;
	}

}


/*==========================================================================*
 * FUNCTION : SelfTestFunctionForDriver
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : Fan Xiansheng            DATE: 2008-09-12 09:37
 *==========================================================================*/
int SelfTestFunctionForDriver(void)
{

	return ERR_LCD_OK;
}

/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SCU Plus Rack_mounted
 *
 *  FILENAME : comm_scup_485.c
 *  CREATOR  : Frank Cao                DATE: 2006-08-09 10:47
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#include "stdsys.h"		/* all standard system head files			*/
#include <sys/file.h>	// for flock()
#include "basetypes.h"
#include "pubfunc.h"
#include "halcomm.h"
#include "new.h"
#include "err_code.h"

#include "../comm_std_serial/commserial.h"	//The private head file for this module




//#define ACU485_CommRead		HAL_CommRead

//#define INVALID_ACU485_SPEED		(-1)


/*==========================================================================*
 * FUNCTION : cfsetdatabits
 * PURPOSE  : set the data bits
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: struct termios  *options  : 
 *            int             nDataBits : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Mao                DATE: 2004-09-17 11:09
 *==========================================================================*/
static int cfsetdatabits(struct termios *options, int nDataBits)
{
    int n;
    int nUnixDataBits[] = { CS5, CS6, CS7, CS8 };

    n = ((nDataBits >= 5) && (nDataBits <= 8)) ? nUnixDataBits[nDataBits-5] : CS8;
    
    options->c_cflag &= ~CSIZE;
    options->c_cflag |= n;

    return n;
}


/*==========================================================================*
 * FUNCTION : cfsetparity
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: struct termios  *options : 
 *            int             nParity  : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Mao                DATE: 2004-09-17 11:10
 *==========================================================================*/
static int cfsetparity(struct termios *options, int nParity)
{
    options->c_cflag &= ~PARENB; /* default, no parity */

    if (nParity == 1) // odd
    {
        options->c_cflag |= PARENB;
        options->c_cflag |= PARODD;
    }

    else if (nParity == 2) // even
    {
        options->c_cflag |= PARENB;
        options->c_cflag &= ~PARODD;
    }

	if (nParity != 0)	// enable parity, 2004-10-8
	{
		options->c_iflag |= INPCK;
	}
	else	// disable
	{
		options->c_iflag &= ~INPCK;
	}

    return nParity;
}

/*==========================================================================*
 * FUNCTION : cfsetstopbits
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: struct termios  *options : 
 *            int             nStop    : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Frank Mao                DATE: 2004-09-17 11:10
 *==========================================================================*/
static int cfsetstopbits(struct termios *options, int nStop)
{
    if (nStop == 1)
        options->c_cflag &= ~CSTOPB;    // default is 1 stop bits
    else if (nStop == 2)
    {
        options->c_cflag |= CSTOPB;
    }
    
    return nStop;
}


/*==========================================================================*
 * FUNCTION : Serial_SetCommAttributes
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * Arguments: IN int fd	: the opened serial port descriptor
 *            IN SERIAL_BAUD_ATTR  *pAttr : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Frank Mao                DATE: 2004-09-17 11:12
 *==========================================================================*/
BOOL Serial_SetCommAttributes(IN int fd, IN SERIAL_BAUD_ATTR *pAttr)
{
    struct termios options;

    /* Get the current options for the port */
    tcgetattr(fd, &options);

    cfsetispeed(&options, pAttr->ulUnixBaud);	// Set the input baud rate
    cfsetospeed(&options, pAttr->ulUnixBaud);	// set the output baud rate

	//TRACE("RS485 Baudrate is %d. \n\r", pAttr->nBaud);

    /* Enable the receiver and set local mode */
    options.c_cflag |= (CLOCAL | CREAD);

    // data bits
	cfsetdatabits(&options, pAttr->nData);

    // stop bits
	cfsetstopbits(&options, pAttr->nStop);

    // parity
	//TRACE("pAttr->nOdd is %d. \n\r", pAttr->nOdd);

	if(pAttr->nOdd != 0)
	{
		pAttr->nOdd = 0;
	}
	cfsetparity(&options, pAttr->nOdd);
	//printf("______________________________________pAttr->nOdd is %d,%d,%d. \n\r",pAttr->nData,pAttr->nOdd, pAttr->nStop);
    options.c_cflag &= ~CRTSCTS;	/* Disable hardware flow control */  

    /* Enable data to be processed as raw input */
    options.c_lflag &= ~(ICANON | ECHO | ISIG);

	//add for the test result of YANGYUNLIANG. 
	//for can not send out the 0x11,0xD,0xA,0x13 chars.
	options.c_iflag  = 0;
	options.c_oflag &= ~OPOST;   /*Output*/	
	// end add for the test result of YANGYUNLIANG

    /* Set the new options for the port */
    if (tcsetattr(fd, TCSANOW, &options) != 0)
    {
        TRACE("[Serial_SetCommAttributes] -- set baud rate fail on %d.\n\r", errno);
        return FALSE;
    }

    return TRUE;
}


// the linux supported baud rates of serial
struct SUnixBaudList
{
    unsigned long ulUnixBaud;
    int     nNormalBaud;
};

static struct SUnixBaudList s_UnixBaudList[] =
{
    { B50	  ,  50 },
    { B75	  ,  75 },
    { B110	  ,  110 },
    { B134	  ,  134 },
    { B150	  ,  150 },
    { B200	  ,  200 },
    { B300	  ,  300 },
    { B600	  ,  600 },
    { B1200	  ,  1200 },
    { B1800	  ,  1800 },
    { B2400	  ,  2400 },
    { B4800	  ,  4800 },
    { B9600	  ,  9600 },
    { B19200  ,  19200 },
    { B38400  ,  38400 },
    { B57600  ,  57600 },
    { B115200 ,  115200 },
    { B230400 ,  230400 },
    { B460800 ,  460800 },
    { B500000 ,  500000 },
    { B576000 ,  576000 },
    { B921600 ,  921600 },
    { B1000000,  1000000 },
    { B1152000,  1152000 },
    { B1500000,  1500000 },
    { B2000000,  2000000 },
    { B2500000,  2500000 },
    { B3000000,  3000000 },
    { B3500000,  3500000 },
    { B4000000,  4000000 },
    { 0,         0       } /* end flag */
};

#define MIN_SERIAL_BAUD		(s_UnixBaudList[0].nNormalBaud)
#define	MAX_SERIAL_BAUD		(s_UnixBaudList[ITEM_OF(s_UnixBaudList)-2].nNormalBaud)

static unsigned long ConvertBaudrateToUnix(int baud)
{
    struct SUnixBaudList    *p = s_UnixBaudList;

    while((p->nNormalBaud != baud) && (p->nNormalBaud != 0))
        p ++;

//#ifdef _DEBUG
//    printf("[ConvertBaudrateToUnix] -- convert normal baud %d to unix got %lu\n",
//        baud, p->ulUnixBaud);
//#endif 	/*_DEBUG	

    return p->ulUnixBaud;
}

#ifndef	SPACE 
#define SPACE  0x20
#endif

#define COMMA_SEPARATOR		','
#define COLON_SERARATOR		':'

static char *SplitText(IN char *S, OUT char *D, IN int cSep);

/*==========================================================================*
 * FUNCTION : SplitText
 * PURPOSE  : split text S with separator cSep(,), and save the splitted text
 *            to D.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char   *S   : the source string to be splitted
 *            OUT char  *D   : to save the splitted string
 *            IN int   cSep : the separator
 * RETURN   : static char *: the string pointer is not splitted.
 * COMMENTS : 
 * CREATOR  : Mao Fuhua                DATE: 2004-09-16 20:19
 *==========================================================================*/
static char *SplitText(IN char *S, OUT char *D, IN int cSep)
{
	int i = 0;
	
    // 1. get rid of the front space
    while(*S == SPACE)
	{
        S ++;
	}

    // 2. copy the non-space char
	while( *S && *S != (char)cSep)
	{
		D[i ++] = *S ++;
	}

    if (*S == (char)cSep)    // skip COMMA_SEPARATOR
	{
        S ++;
	}

    // 3. get rid of the rear space
    i --;
    while((i >= 0) && (D[i] == SPACE))
	{
        i --;
	}

    i ++;

    D[i]=0;

	return S;
}


/*==========================================================================*
 * FUNCTION : Serial_ParseOpenParams
 * PURPOSE  : Parse the baud rate, data bits and other attributes of the serial
 *            from opening parameters
 *            open comm with b,p,d,s format, for dialling port is b,p,d,s:phone
 *            b=baud, e.g., 9600,
 *            p=parity, n, o, e is allowed.
 *            d=data bits, 5-8 is valid
 *            s=stop bits, 1-2 is valid
 *            for phone number, the '-','+',' ' are allowed in the phone number,
 *            but they will be ignored.
 * CALLS    : 
 * CALLED BY: Serial_CommOpen
 * ARGUMENTS: IN char               *pSettings : 
 *            OUT SERIAL_BAUD_ATTR  *pAttr       : 
 * RETURN   : static BOOL : TRUE for success, FALSE for error params
 * COMMENTS : 
 * CREATOR  : Mao Fuhua                DATE: 2004-09-16 20:14
 *==========================================================================*/
static BOOL Serial_ParseOpenParams(IN char	*pSettings, 
								   OUT SERIAL_BAUD_ATTR *pAttr)
{

	// the settings like: 1000000,n,8,1:00867559876543211234567
#define MAX_SETTING_LEN		64	// 64 is enough!!
    char p[MAX_SETTING_LEN], *s = pSettings;
	
    if ((pSettings == NULL) || (*pSettings == 0) 
		|| strlen(pSettings) >= MAX_SETTING_LEN)	// too long.
    {
        return FALSE;
    }

    // get the baud rate set
    s = SplitText(s, p, COMMA_SEPARATOR);

    pAttr->nBaud = atoi(p);

	pAttr->ulUnixBaud = ConvertBaudrateToUnix(pAttr->nBaud);

    if (pAttr->ulUnixBaud == 0)
    {
        return FALSE;
    }

    // odd or even check
    s = SplitText(s, p, COMMA_SEPARATOR);
    if (p[1] != 0) // too long
    {
        return FALSE;
    }

    if ((p[0] == 'N') || (p[0] == 'n'))
	{
        pAttr->nOdd = 0;
	}
    else if ((p[0] == 'O') || (p[0] == 'o'))
	{
        pAttr->nOdd = 1;
	}
    else if ((p[0] == 'E') || (p[0] == 'e'))
	{
        pAttr->nOdd = 2;
	}
    else
	{
        return FALSE;
	}
    
    // data bit - only one byte 5-8
    s = SplitText(s, p, COMMA_SEPARATOR);
    if ((p[0] < '5') || (p[0] > '8') || (p[1] != 0))
    {
        TRACE("[Serial_ParseOpenParams] -- error on parsing data bit.\n");
        return FALSE;
    }

    pAttr->nData = p[0] - '0';

    // stop bit

    s = SplitText(s, p, COMMA_SEPARATOR);

    if (((p[0] == '1') || (p[0] == '2')) && (p[1] == 0))
    {
        pAttr->nStop = p[0] - '0';  // 1 or 2 stop bits
    }
    // 1.5 stop bits
    else if ((p[0] == '1') && (p[1] == '.') && (p[2] == '5') && (p[3] == 0))
    {
        pAttr->nStop = 3;      // 1.5 stop bits
    }
    else
    {
        TRACE("[Serial_ParseOpenParams] -- error on parsing stop bit.\n");
        return FALSE;
    }
   
//printf("---------Open port ------------------------%d,%d,%d,%d",pAttr->nBaud,pAttr->nOdd,pAttr->nData,pAttr->nStop );
	return TRUE;
}



#define TTYS_ONLY_NAME			"ttyS"
#define TTYS_FULL_PREFIX		"/dev/ttyS"
#define TTYS_ALIAS_NAME			"COM"
#define CONST_STRLEN(const_str)		(sizeof(const_str)-1)

/*==========================================================================*
 * FUNCTION : Serial_GetDescriptor
 * PURPOSE  : Convert the non-standard serial device name such as ttyS0, ttyS1,
 *            to /dev/ttyS0, /dev/ttyS1..., and also convert COM1, COM2, ..., to
 *            /dev/ttyS0, /dev/ttyS1, and also convert "1", "2" to /dev/ttyS0, 
 *            /dev/ttyS1....
 * CALLS    : 
 * CALLED BY: Serial_CommOpen
 * ARGUMENTS: IN char   *pPortDescriptor : 
 *            OUT char  *pStdDescriptor  : 
 * RETURN   : static int : the actual serial port start from 0, -1 for error. 
 * COMMENTS : 
 * CREATOR  : Mao Fuhua                DATE: 2004-09-16 17:26
 *==========================================================================*/
static int Serial_GetDescriptor(IN char *pPortDescriptor,
								 OUT char *pStdDescriptor)
{
	char	*pComNum = NULL;
	int		nComNumStart, nComNum;

	// 1. Test if the name is full name or not. 
	//    this comparison is case sensitive
	if (strncmp(pPortDescriptor, TTYS_FULL_PREFIX,
		CONST_STRLEN(TTYS_FULL_PREFIX)) == 0)
	{
		// the full name is used.
		pComNum = pPortDescriptor + CONST_STRLEN(TTYS_FULL_PREFIX);
		nComNumStart = 0;	// /dev/ttySn, the n starts from 0.
	}

	// 2. to test if only the name, the /dev/ does not exist. 
	//    this comparison is case sensitive
	else if (strncmp(pPortDescriptor, TTYS_ONLY_NAME,
		CONST_STRLEN(TTYS_ONLY_NAME)) == 0)
	{
		// only the name is used.
		pComNum = pPortDescriptor + CONST_STRLEN(TTYS_ONLY_NAME);
		nComNumStart = 0;	// ttySn, the n starts from 0.
	}

	// 3. to the alias name COMx is used or not.
	//    this comparison is NOT case sensitive
	else if (strnicmp(pPortDescriptor, TTYS_ALIAS_NAME,
		CONST_STRLEN(TTYS_ALIAS_NAME)) == 0)
	{
		// the alias name is used.
		pComNum = pPortDescriptor + CONST_STRLEN(TTYS_ALIAS_NAME);
		nComNumStart = 1;	// COMn, the n starts from 1.
	}

	// 4. we consider only the port num such as 1, 2,... is passed in.
	else 
	{
		pComNum = pPortDescriptor;
		nComNumStart = 1;	// only the com num 'n', the n starts from 1.
	}

	// 5. to test the comm number is correct or not.
	//    the comm num shall be a digital string.
	//    ACU only support 0-9 serial port at tatol
	nComNum = (pComNum[0]-'0') - nComNumStart;	// the actual serial port num

	if ((nComNum < 0) || (pComNum[0] < '0') || (pComNum[0] > '9')
		|| (pComNum[1] != '\0'))
	{
		TRACE("[Serial_GetDescriptor] -- The port descriptor %s is invalid.\n",
			pPortDescriptor);

		return -1;
	}

    // 6. merge the full port name. The buffer is big enough.
	sprintf(pStdDescriptor, "%s%d",
		TTYS_FULL_PREFIX,	// /dev/ttyS
		nComNum);			// actual comm num from 0

	
	return nComNum;
}


//extern	BOOL ACU485_SetCommAttributes(IN int fd, IN SERIAL_BAUD_ATTR *pAttr);

/*==========================================================================*
 * FUNCTION : Serial_OpenPort
 * PURPOSE  : To open the serial port(pszComName), and set its 
 *            attributes(buad rate, data bits,and so on)
 * CALLS    : 
 * CALLED BY: Serial_CommOpen
 * ARGUMENTS: IN char              *pszComName : 
 *            IN SERIAL_BAUD_ATTR  *pAttr             : 
 * RETURN   : static int : the handle of the opened port
 * COMMENTS : 
 * CREATOR  : Frank Mao                DATE: 2004-09-17 10:55
 *==========================================================================*/
static int Serial_OpenPort(IN char *pszComName,
						   IN SERIAL_BAUD_ATTR *pAttr)
{
	int		fd;	// the file descriptor

    fd = open(pszComName, O_RDWR | O_NOCTTY | O_NONBLOCK | O_EXCL); 
    if (fd < 0)
    {
#ifdef _DEBUG
		perror("[Serial_OpenPort] -- open serial");
        TRACE("[Serial_OpenPort] -- open %s fail, got error %d.\n\r", 
            pszComName, errno);
#endif 	/*_DEBUG	*/
		return -1;
    }

	// lock the com to avoid other process open again
/*	if( flock( fd, LOCK_EX|LOCK_NB ) < 0 )
	{
#ifdef _DEBUG
		TRACE("[Serial_OpenPort] -- try to lock %s fail, got %d\n",
			pszComName, errno);
#endif 	

		close( fd );
		return -1;
	}
	*/
	//TRACE("Current pAttr->nOdd is %d.\n", pAttr->nOdd);
	//if(pAttr->nOdd != 1)
	//{
       // pAttr->nOdd = 0;
	//}
	pAttr->nOdd = 0;
    if (!Serial_SetCommAttributes(fd, pAttr))
    {
        close(fd);
        return -1;
    }

    return fd;
}

/*==========================================================================*
 * FUNCTION : Serial_CommOpen
 * PURPOSE  : To open the 485 serial communication port.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char   *pPortDescriptor : The serial descriptor in Linux, 
 *									       /dev/ttyS0, /dev/ttyS1,...
 *            IN char   *pOpenParams     : the port settings as format:
 *										   "buad,parity,data,stop", 
 *                                         such as "9600,n,8,1". for detail 
 *                                         please refer to the function
 *                                         Serial_ParseOpenParams
 *            IN DWORD  dwPortAttr       : B00=COMM_SERVER_MODE for server port
 *                                         B00=COMM_CLIENT_MODE for client port
 *            IN int    nTimeout         : Open timeout in ms
 *            OUT int   *pErrCode        : prt to save error code.
 * RETURN   : HANDLE : 
 * COMMENTS : 
 * CREATOR  : Frank Mao                DATE: 2004-09-11 17:55
 *==========================================================================*/
HANDLE Serial_CommOpen(
	IN char		*pPortDescriptor,
	IN char		*pOpenParams,
	IN DWORD	dwPortAttr,
	IN int		nTimeout,
	OUT int		*pErrCode)
{
	SERIAL_PORT_DRV *pPort = NEW(SERIAL_PORT_DRV, 1);
	// full port device name, like: /dev/ttyS0
	char	szFullDescriptor[sizeof(TTYS_FULL_PREFIX)+2];
	
	/* 1. get mem */
	if (pPort == NULL)
	{
		*pErrCode = ERR_COMM_NO_MEMORY;
		return	NULL;
	}

	memset(pPort, 0, sizeof(SERIAL_PORT_DRV));

	/* 2. parse descriptor */
	pPort->nSerialPort = Serial_GetDescriptor(pPortDescriptor, 
		szFullDescriptor);
	if (pPort->nSerialPort < 0)
	{

		DELETE(pPort);
		*pErrCode = ERR_COMM_OPENING_PARAM;

		return	NULL;
	}

	/* 2.1. parse comm settings */
	if (!Serial_ParseOpenParams(pOpenParams, &pPort->attr))
	{

		DELETE(pPort);
		*pErrCode = ERR_COMM_OPENING_PARAM;

		return	NULL;
	}

	INIT_TIMEOUTS(pPort->toTimeouts, nTimeout, nTimeout);
	
	pPort->pCurClients = NEW(int, 1);	
	if (pPort->pCurClients == NULL)
	{
		DELETE(pPort);
		*pErrCode = ERR_COMM_NO_MEMORY;

		return NULL;
	}

	*pPort->pCurClients = 0;	/*	no linkages now		*/

	// open the serial port.
	pPort->fdSerial = Serial_OpenPort(szFullDescriptor, &pPort->attr);
	if (pPort->fdSerial < 0)
	{

		DELETE(pPort->pCurClients);	
		DELETE(pPort);
	
		*pErrCode = ERR_COMM_OPENING_PORT;

		return	NULL;
	}

#ifdef _CODE_FOR_MINI
		//modified by kenn , mini only have one rs485, it's rts1
	pPort->fdRtsFor485 = open("/dev/rts1", O_RDWR | O_NOCTTY);
#else
	pPort->fdRtsFor485 = open("/dev/rts3", O_RDWR | O_NOCTTY);
#endif

	if (pPort->fdRtsFor485 < 0)
	{
#ifdef _DEBUG
		TRACE("[Serial_CommOpen] -- Failed to open RTS !!!!\n\r");
#endif 	/*_DEBUG	*/

		DELETE(pPort->pCurClients);	
		DELETE(pPort);
		*pErrCode = ERR_COMM_OPENING_PORT;

		return	NULL;
	}

#ifdef _DEBUG
		TRACE("[Serial_CommOpen] -- try to open RTS successfully!!!!\n\r");
#endif 	/*_DEBUG	*/

	/* to connect to a remote server, create a client port	*/
	if ((dwPortAttr & COMM_CLIENT_MODE) == COMM_CLIENT_MODE)
	{
		pPort->nWorkMode = COMM_OUTGOING_CLIENT;
	}

	/* to create a server port	*/
	else
	{
		pPort->nWorkMode = COMM_LOCAL_SERVER;
		pPort->nMaxClients = SERIAL_MAX_CLIENTS_DEFAULT;
	}

	*pErrCode = ERR_COMM_OK;

	return (HANDLE)pPort;
}

#define CLIENT_IS_READY(pPort)		1

/* The "CommAccept" proc for comm driver */
/*==========================================================================*
 * FUNCTION : Serial_CommAccept
 * PURPOSE  : To accept a client from serial port with server type.
 *            Actually, the serial is no server or client mode,
 *            it always works as end-end mode. the accept function is purely
 *            to keep the accordance with the work mode of the TCP/IP port.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN HANDLE  hPort : The server port
 * RETURN   : HANDLE : non-zero for successful, NULL for failure
 * COMMENTS : 
 * CREATOR  : Frank Mao                DATE: 2004-09-13 20:43
 *==========================================================================*/
HANDLE Serial_CommAccept(IN HANDLE hPort)
{
	SERIAL_PORT_DRV *pPort   = (SERIAL_PORT_DRV *)hPort;
	SERIAL_PORT_DRV *pClient = NULL;

	/* clear last error */
	pPort->nLastErrorCode = ERR_COMM_OK;

	Serial_SetCommAttributes(pPort->fdSerial, &pPort->attr);

	/* check the socket whether a server port or not,
 	 * only server port is allowed to call accept
	 */
	if (pPort->nWorkMode != COMM_LOCAL_SERVER)
	{
		pPort->nLastErrorCode = ERR_COMM_SUPPORT_ACCEPT;
		return NULL;
	}

	/* check the current client num exceeds the maximum connections or not */
	if (*pPort->pCurClients >= pPort->nMaxClients)
	{
		pPort->nLastErrorCode = ERR_COMM_MANY_CLIENTS;
		return NULL;
	}

	// For serial, the accept calling always return the handle directly	
	if (CLIENT_IS_READY(pPort))
	{
		/* Ok, create a client object */
		pClient = NEW(SERIAL_PORT_DRV,1);
		if (pClient == NULL)	/* out of memory error	*/
		{
			pPort->nLastErrorCode = ERR_COMM_NO_MEMORY;
			return NULL;
		}

		*pClient			= *pPort;	/* Copy all info from server port	*/
		pClient->nWorkMode	= COMM_INCOMING_CLIENT;
		(*pPort->pCurClients)++;		/* increase the current linkage		*/


	}


	return (HANDLE)pClient;
}


int Serial_CommRead(IN HANDLE hPort, OUT char *pBuffer, IN int nBytesToRead)
{
	SERIAL_PORT_DRV *pPort     = (SERIAL_PORT_DRV *)hPort;
	int				fd		   = pPort->fdSerial;
	int				nBytesRead = 0;
	int			    nTotalBytesRead = 0;	// total bytes of data being read
	int				nTimeout;
	int				rc;
	int				nRetryTimes = 0;

	/* clear last error */
	pPort->nLastErrorCode = ERR_COMM_OK;

	rc = WaitFiledReadable(fd, pPort->toTimeouts.nReadTimeout);

	// when data is NOT ready, the WaitFiledReadable() always return 1, 
	// but the read returns -1. so the WaitFiledReadable can NOT be used
	// in reading data from the ACU 485 Port.
	nTimeout = pPort->toTimeouts.nReadTimeout;

	if( rc <= 0 )
	{
		/* 0: wait time out, -1: fd error on waiting    */
		pPort->nLastErrorCode = 
			(rc == 0) ? ERR_COMM_TIMEOUT  : ERR_COMM_READ_DATA;

		return 0;	// nothing has been read
	}

    while (nBytesToRead > 0)
    {
		nBytesRead = read(fd, pBuffer, (size_t)nBytesToRead);

		if (nBytesRead > 0)	/* we got some data */
        {
            // ok
            nBytesToRead	-= nBytesRead;
            pBuffer			+= nBytesRead;
			nTotalBytesRead += nBytesRead;

			if (nBytesToRead == 0)
			{
				break;	// reading finished
			}

		}

		else if ((nBytesRead < 0) && (errno != EAGAIN))		// read error?
		{
			nRetryTimes++;		// let's retry again
			if (nRetryTimes >= COM_RETRY_TIME)
			{
				pPort->nLastErrorCode = ERR_COMM_READ_DATA;
				break;	// error, quit now.
			}
		}

		// nothing received, data not ready? wait a while
		// if error, we also sleep a while and retry again.
		rc = WaitFiledReadable(fd, pPort->toTimeouts.nIntervalTimeout);
		if( rc <= 0 )
		{
			/* 0: wait time out, -1: fd error on waiting    */
			pPort->nLastErrorCode = (rc == 0) ? ERR_COMM_TIMEOUT
				: ERR_COMM_READ_DATA;
			break;
		}

	}

	return nTotalBytesRead;
}
	
		



//int Serial_CommRead(IN HANDLE hPort, OUT char *pBuffer, IN int nBytesToRead)
//{
//	SERIAL_PORT_DRV *pPort     = (SERIAL_PORT_DRV *)hPort;
//	int				fd		   = pPort->fdSerial;
//	int				nBytesRead = 0;
//	int			    nTotalBytesRead = 0;	// total bytes of data being read
//	int				nRetryTimes = 0;
//	int				rc;
//
//	/* clear last error */
//	pPort->nLastErrorCode = ERR_COMM_OK;
//	
//	//TRACE("Start 485 read waiting, time is %1.3fs.\n",GetCurrentTime());
//	//rc = WaitFiledReadable(fd, pPort->toTimeouts.nReadTimeout);
//	//TRACE("End 485 read waiting, time is %1.3fs.\n",GetCurrentTime());
//
//	//if( rc <= 0 )
//	//{
//	//	/* 0: wait time out, -1: fd error on waiting    */
//	//	pPort->nLastErrorCode = 
//	//		(rc == 0) ? ERR_COMM_TIMEOUT  : ERR_COMM_READ_DATA;
//
//	//	return 0;	// nothing has been read
//	//}
//
//    while (nBytesToRead > 0)
//    {
////TRACE("Start 485 read , time is %1.3fs.\n",GetCurrentTime());
//		nBytesRead = read(fd, pBuffer, (size_t)nBytesToRead);
////("End %d times 485 read , time is %1.3fs., ReadBytes is %d\n",
//	 // nRetryTimes,GetCurrentTime(),nBytesRead);
//		if (nBytesRead > 0)	/* we got some data */
//        {
//            // ok
//            nBytesToRead	-= nBytesRead;
//            pBuffer			+= nBytesRead;
//			nTotalBytesRead += nBytesRead;
//
//			if (nBytesToRead == 0)
//			{
//				break;	// reading finished
//			}
//
//			// Need clear retry times? We don't clear it!
//			// nRetryTimes = 0;
//		}
//
//		else if ((nBytesRead < 0) && (errno != EAGAIN))		// read error?
//		{
//			nRetryTimes++;		// let's retry again
//			if (nRetryTimes >= COM_RETRY_TIME)
//			{
//				pPort->nLastErrorCode = ERR_COMM_READ_DATA;
//
//				break;	// error, quit now.
//			}
//		}
//
//		// nothing received, data not ready? wait a while
//		// if error, we also sleep a while and retry again.
//		//rc = WaitFiledReadable(fd, pPort->toTimeouts.nIntervalTimeout);
//		//if( rc <= 0 )
//		//{
//		//	/* 0: wait time out, -1: fd error on waiting    */
//		//	pPort->nLastErrorCode = (rc == 0) ? ERR_COMM_TIMEOUT
//		//		: ERR_COMM_READ_DATA;
//		//	break;
//		//}
//    }
//
//    return nTotalBytesRead;
//}

// add by wankun 2008/06/13

#ifdef _CODE_FOR_MINI
#define RTS_MAJOR			246 
#else
#define RTS_MAJOR			247  
#endif

#define IOC_SET_SEND  	              _IOW(RTS_MAJOR, 1, unsigned long)
#define IOC_SET_RECEIVE   	       _IOW(RTS_MAJOR, 2, unsigned long)
// ended by wankun 2008/06/13

/* The "CommWrite" proc for comm driver */
/*==========================================================================*
 * FUNCTION : Serial_CommWrite
 * PURPOSE  : write a buffer to a port
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN HANDLE  hPort         : 
 *            IN char    *pBuffer      : 
 *            IN int     nBytesToWrite : 
 * RETURN   : int : < 0 for error. <nBytesToWrite timeout, = nBytesToWrite ok
 * COMMENTS : 
 * CREATOR  : Frank Mao                DATE: 2004-09-13 21:36
 *==========================================================================*/
int Serial_CommWrite(IN HANDLE hPort, IN char *pBuffer,	IN int nBytesToWrite)
{
	SERIAL_PORT_DRV *pPort = (SERIAL_PORT_DRV *)hPort;
	int				fd = pPort->fdSerial;
	int				fdRts = pPort->fdRtsFor485;
	int				nBytesWritten = 0;
	int			    nTotalBytesWritten = 0;	// total bytes of data being read
	int				nRetryTimes = 0;
	int				rc;
	//DWORD           dwArg;
	
	/* clear last error */
	pPort->nLastErrorCode = ERR_COMM_OK;

	rc = WaitFiledWritable(fd, pPort->toTimeouts.nReadTimeout);
	

	//dwArg = MAKELONG(pPort->attr.nBaud, nBytesToWrite);

	if( rc <= 0 )
	{
		/* 0: wait time out, -1: fd error on waiting    */
		pPort->nLastErrorCode = 
			(rc == 0) ? ERR_COMM_TIMEOUT  : ERR_COMM_WRITE_DATA;

		return 0;	// nothing has been read
	}

	//TRACE("Length is %d, baudrate is %d.\n", HIWORD(dwArg), LOWORD(dwArg));

	if(ioctl(fdRts, IOC_SET_SEND, 0) < 0)
	{
		pPort->nLastErrorCode = ERR_COMM_WRITE_DATA;
		return 0;	// nothing has been write	
	}
	//TRACE("[Serial_CommWrite] -- RTS Control!!!!!\n\r");
	//TRACE("[Serial_CommWrite] -- set RTS to 1!!!!!.\n\r");
	//TRACE("[Serial_CommWrite] -- write--- %s !!!!!.\n\r",pBuffer);
	
	Sleep( 5 );
    while (nBytesToWrite > 0)
    {
        nBytesWritten = write(fd, (void *)pBuffer, (size_t)nBytesToWrite);

		//nBytesWritten = write(fd, rbuf, 1);
		//nBytesToWrite = 1;

        if (nBytesWritten > 0)	/* we got some data */
        {
            // ok
            nBytesToWrite	   -= nBytesWritten;
            pBuffer			   += nBytesWritten;
			nTotalBytesWritten += nBytesWritten;

			if (nBytesToWrite == 0)
			{
				//TRACEX("[Serial_CommWrite] -- write finished !!!!!.\n\r");

				break;	// writing finished
			}

			// Need clear retry times? We don't clear it!
			// nRetryTimes = 0;
		}

		else if ((nBytesWritten < 0) && (errno != EAGAIN))		// read error?
		{
			nRetryTimes++;		// let's retry again
			if (nRetryTimes >= COM_RETRY_TIME)
			{
				pPort->nLastErrorCode = ERR_COMM_WRITE_DATA;

				break;	// error, quit now.
			}
		}

		// nothing received, data not ready? wait a while
		// if error, we also sleep a while and retry again.
		rc = WaitFiledWritable(fd, pPort->toTimeouts.nIntervalTimeout);
		if( rc <= 0 )
		{
			/* 0: wait time out, -1: fd error on waiting    */
			pPort->nLastErrorCode = (rc == 0) ? ERR_COMM_TIMEOUT
				: ERR_COMM_READ_DATA;
			break;
		}
    }
	// add by wankun 2008-06-13
	// in order to test the driver of 485 send over or not
	int iRtn = 0;
	while(!iRtn)
	{
		ioctl(pPort->fdSerial, TIOCSERGETLSR, &iRtn);
		//printf("the status of 485 is %d (0 is not over)\n ",iRtn);
	}
	//send over then change the status of rts to receive state!
	ioctl(fdRts, IOC_SET_RECEIVE, 0);
	//ended by wankun  2008-06-13
    return nTotalBytesWritten;
}


/* The "CommControl" proc for comm driver */
/*==========================================================================*
 * FUNCTION : Serial_CommControl
 * PURPOSE  : To control a opened port with command nCmd.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN HANDLE    hPort       : 
 *            IN int       nCmd        : 
 *            IN OUT void  *pBuffer    : 
 *            IN int       nDataLength : 
 * RETURN   : int :ERR_OK for OK, else is error code
 * COMMENTS : 
 * CREATOR  : Frank Mao                DATE: 2004-09-13 21:45
 *==========================================================================*/
int Serial_CommControl(	IN HANDLE hPort, IN int	nCmd, 
					  IN OUT void *pBuffer,	IN int	nDataLength)
{
	/* Now we use switch to test the command, 
	 * we can optimize it if there are too many commands 
	 * which maybe make lower efficiency
	 */
	SERIAL_PORT_DRV *pPort = (SERIAL_PORT_DRV *)hPort;
	int nErrCode = ERR_COMM_OK;

	if (hPort == NULL)
	{
		return ERR_COMM_PORT_HANDLE;
	}

	switch (nCmd)
	{
	case COMM_GET_LAST_ERROR:	/* get the last error code	*/
		{
			ASSERT(pBuffer);
			ASSERT(nDataLength == sizeof(int));

			*(int *)pBuffer = pPort->nLastErrorCode;
		}

		break;

	case COMM_GET_PEER_INFO:	/* get the sender info		*/
		{
			COMM_PEER_INFO *pPeerInfo = (COMM_PEER_INFO *)pBuffer;

			ASSERT(pBuffer);
			ASSERT(nDataLength == sizeof(COMM_PEER_INFO));

			if (pPort->nWorkMode != COMM_LOCAL_SERVER)
			{
				pPeerInfo->nPeerPort = pPort->nSerialPort;
				pPeerInfo->nPeerType = pPort->nWorkMode;

				pPeerInfo->szAddr[0] = 0;
			}
			else
			{
				/* The server port does not support		*/
				nErrCode = ERR_COMM_CTRL_COMMAND;
			}
		}

		break;

	case COMM_SET_TIMEOUTS:		/* set the comm timeout in ms. */
		{	
			/* bBuffer=COMM_TIMEOUTS, nDataLength=sizeof(COMM_TIMEOUTS)		*/
			ASSERT(pBuffer);
			ASSERT(nDataLength == sizeof(COMM_TIMEOUTS));

			pPort->toTimeouts = *(COMM_TIMEOUTS *)pBuffer;
		}

		break;

	case COMM_GET_TIMEOUTS:		/* get the comm timeout in ms	*/
		{	
			/* bBuffer=COMM_TIMEOUTS, nDataLength=sizeof(COMM_TIMEOUTS)		*/
			ASSERT(pBuffer);
			ASSERT(nDataLength == sizeof(COMM_TIMEOUTS));

			*(COMM_TIMEOUTS *)pBuffer = pPort->toTimeouts;
		}
		break;

	case COMM_PURGE_TXCLEAR:	/* clear the transmit buffer	*/
		{
			if (pPort->nWorkMode != COMM_LOCAL_SERVER)
			{
//				tcflush(pPort->fdSerial,TCOFLUSH);
				Sleep(50);	// do NOTHING.
			}
			else
			{
				/* The server port does not support		*/
				nErrCode = ERR_COMM_CTRL_COMMAND;
			}
		}

		break;

	case COMM_PURGE_RXCLEAR:	/* clear the receive buffer		*/
		{
			if (pPort->nWorkMode != COMM_LOCAL_SERVER)
			{
				int		c;

//				tcflush(pPort->fdSerial,TCIFLUSH);
				while (read(pPort->fdSerial, &c, 1) > 0)
				{
					;
				}
			}
			else
			{
				/* The server port does not support		*/
				nErrCode = ERR_COMM_CTRL_COMMAND;
			}
		}

		break;

	case COMM_SET_MAX_CLIENTS:	/* set the maximum allowed clients	*/
		{
			ASSERT(pBuffer);
			ASSERT(nDataLength == sizeof(int));

			if (pPort->nWorkMode == COMM_LOCAL_SERVER)	/* Must be server	*/
			{	
				if ((*(int *)pBuffer > 0)				 /* client must > 0	*/
					&& (*(int *)pBuffer <= SERIAL_MAX_CLIENT_LIMITAION))
				{
					pPort->nMaxClients = *(int *)pBuffer;
				}
				else
				{
					nErrCode = ERR_COMM_CTRL_PARAMS;
				}
			}
			else
			{
				nErrCode = ERR_COMM_CTRL_COMMAND;
			}
		}

		break;

	case COMM_GET_MAX_CLIENTS:		/* get the maximum allowed clients */
		{
			ASSERT(pBuffer);
			ASSERT(nDataLength == sizeof(int));

			if (pPort->nWorkMode == COMM_LOCAL_SERVER) /* Must be server	*/
			{
				*(int *)pBuffer = pPort->nMaxClients;
			}
			else
			{
				nErrCode = ERR_COMM_CTRL_COMMAND;
			}
		}

		break;

		/* get the number of the current connected clients */
	case COMM_GET_CUR_CLIENTS:
		{
			ASSERT(pBuffer);
			ASSERT(nDataLength == sizeof(int));

			if (pPort->nWorkMode == COMM_LOCAL_SERVER) /* Must be server	*/
			{
				*(int *)pBuffer = *pPort->pCurClients;
			}
			else
			{
				nErrCode = ERR_COMM_CTRL_COMMAND;
			}
		}

		break;

	case COMM_GET_CLIENT_LIMITATION:	/* get the maximum client limitation */
		{
			ASSERT(pBuffer);
			ASSERT(nDataLength == sizeof(int));

			if (pPort->nWorkMode == COMM_LOCAL_SERVER) /* Must be server	*/
			{
				*(int *)pBuffer = SERIAL_MAX_CLIENT_LIMITAION;
			}
			else
			{
				nErrCode = ERR_COMM_CTRL_COMMAND;
			}
		}

		break;

	default:
		{
			nErrCode = ERR_COMM_CTRL_COMMAND;
		}
	}

	return nErrCode;
}

/* The "CommClose" proc for comm driver */
/*==========================================================================*
 * FUNCTION : CommClose
 * PURPOSE  : Close an opened port and release the memory of the port
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN HANDLE  hPort : 
 * RETURN   : int : ERR_COMM_OK or ERR_COMM_PORT_HANDLE
 * COMMENTS : 
 * CREATOR  : Frank Mao                 DATE: 2004-09-14 20:32
 *==========================================================================*/
int Serial_CommClose(IN HANDLE hPort)
{
	SERIAL_PORT_DRV *pPort = (SERIAL_PORT_DRV *)hPort;

	if (hPort == NULL)
	{
		return ERR_COMM_PORT_HANDLE;
	}


	/* descrease the linkage, the pCurClients shall be deleted if no
	 * any clients. But the server and clients share the same pCurClients,
	 * and the initialing value of *pCurClients is 0, so the condition to
	 * delete pCurClients is *pCurClients < 0.
	 */
	(*pPort->pCurClients)--;
	if (*pPort->pCurClients < 0)
	{
		close(pPort->fdRtsFor485);
		close(pPort->fdSerial);	// to close fd if it is not used yet.
		DELETE(pPort->pCurClients);
	}

	DELETE(pPort);

	return ERR_COMM_OK;
}



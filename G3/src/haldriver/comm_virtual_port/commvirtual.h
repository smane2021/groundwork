/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : commserial.h
 *  CREATOR  : Mao Fuhua                DATE: 2004-09-16 19:54
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#ifndef __COMMVIRUTAL_H__
#define __COMMVIRUTAL_H__

#include <termios.h>

/* 
 * If want to test the module with static mode,
 * please comment out the next line.
 */

#define _MAKE_SHARED_LIB

#ifdef _MAKE_SHARED_LIB

#define Virtual_CommOpen			HAL_CommOpen
#define Virtual_CommAccept			HAL_CommAccept
#define Virtual_CommRead			HAL_CommRead
#define Virtual_CommWrite			HAL_CommWrite
#define Virtual_CommControl			HAL_CommControl
#define Virtual_CommClose			HAL_CommClose

#endif //_MAKE_SHARED_LIB

#define COM_RETRY_TIMES  2
/**********************************/
#define VIRTUAL_MAX_CLIENTS_DEFAULT	1	/* The default VIRTUAL connections	*/
#define VIRTUAL_MAX_CLIENT_LIMITAION	1	/* only 1 connection is allowed		*/
#define VIRTUAL_PORT_HANDLE_VAL			999

struct SVirtualPortDriver	//	The HAL driver for direct serial port
{				
	int				nLastErrorCode;	//	NOTE: MUST BE THE FIRST FIELD! 
									//  the last error code.

	int				fdSerial;		//	the handle from open()
	//COMM_TIMEOUTS	toTimeouts;		//	read and write timeout
	int				nWorkMode;	//	see enum COMM_WORK_MODE_ENUM in "halcomm.h"
	int				nMaxClients;	//	always 1
	int				*pCurClients;	//	0 or 1.
	int				nVirtualPort;	// the serial port start from 0.
};		

typedef struct SVirtualPortDriver		VIRTUAL_PORT_DRV;

#endif /*__COMMSERIAL_H__*/

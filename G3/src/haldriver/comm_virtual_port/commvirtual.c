/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : comm_std_serial.c
 *  CREATOR  : Mao Fuhua                DATE: 2004-09-16 14:34
 *  VERSION  : V1.00
 *  PURPOSE  : The HAL communication driver for standard serial port
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include "stdsys.h"		/* all standard system head files			*/
#include <sys/file.h>	// for flock()
#include "basetypes.h"
#include "pubfunc.h"
#include "halcomm.h"
#include "new.h"
#include "err_code.h"

#include "commvirtual.h"	/* The private head file for this module	*/

#ifdef _DEBUG
//#define _DEBUG_VIRTUAL_PORT
#endif //_DEBUG

//the flag will be passed by compiler.
//#define _IMPL_DIAL_SERIAL	1// to implement the dialling serial

#define VIRTUAL_PORT_ID			6
#define CONST_STRLEN(const_str)		(sizeof(const_str)-1)

/*==========================================================================*
 * FUNCTION :Virtual_CommOpen
 * PURPOSE  : To open a standard serial communication port.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN char   *pPortDescriptor : The serial descriptor in Linux, 
 *									       /dev/ttyS0, /dev/ttyS1,...
 *            IN char   *pOpenParams     : the port settings as format:
 *										   "buad,parity,data,stop", 
 *                                         such as "9600,n,8,1". for detail 
 *                                         please refer to the function
 *                                         Serial_ParseOpenParams
 *            IN DWORD  dwPortAttr       : B00=COMM_SERVER_MODE for server port
 *                                         B00=COMM_CLIENT_MODE for client port
 *            IN int    nTimeout         : Open timeout in ms
 *            OUT int   *pErrCode        : prt to save error code.
 * RETURN   : HANDLE : 
 * COMMENTS : 
 * CREATOR  : Frank Mao                DATE: 2004-09-11 17:55
 *==========================================================================*/
HANDLE Virtual_CommOpen(
	IN char		*pPortDescriptor,
	IN char		*pOpenParams,
	IN DWORD	dwPortAttr,
	IN int		nTimeout,
	OUT int		*pErrCode)
{
	UNUSED(pOpenParams);
	UNUSED(nTimeout);
	VIRTUAL_PORT_DRV *pPort = NEW(VIRTUAL_PORT_DRV, 1);
	
	// full port device name, like: /dev/ttyS0
	char	szFullDescriptor[32];
	
	/* 1. get mem */
	if (pPort == NULL)
	{
		*pErrCode = ERR_COMM_NO_MEMORY;
		return	NULL;
	}

	memset(pPort, 0, sizeof(VIRTUAL_PORT_DRV));
	memset(szFullDescriptor, 0, 32);

	/* 2. parse descriptor */
	/* 2.1. parse comm settings */	
	//INIT_TIMEOUTS(pPort->toTimeouts, nTimeout, nTimeout);
	
	pPort->pCurClients = NEW(int, 1);	
	if (pPort->pCurClients == NULL)
	{
		DELETE(pPort);
		*pErrCode = ERR_COMM_NO_MEMORY;

		return NULL;
	}

	*pPort->pCurClients = 0;	/*	no linkages now		*/

	// open the serial port.
	pPort->fdSerial = VIRTUAL_PORT_HANDLE_VAL;


	/* to connect to a remote server, create a client port	*/	
	if ((dwPortAttr & COMM_CLIENT_MODE) == COMM_CLIENT_MODE)
	{		
		pPort->nWorkMode = COMM_OUTGOING_CLIENT;
	}	
	else /* to create a server port	*/
	{
		pPort->nWorkMode = COMM_LOCAL_SERVER;
		pPort->nMaxClients = VIRTUAL_MAX_CLIENTS_DEFAULT;
	}

	*pErrCode = ERR_COMM_OK;

	return (HANDLE)pPort;
}


/* The "CommAccept" proc for comm driver */
/*==========================================================================*
 * FUNCTION : Serial_CommAccept
 * PURPOSE  : To accept a client from serial port with server type.
 *            Actually, the serial is no server or client mode,
 *            it always works as end-end mode. the accept function is purely
 *            to keep the accordance with the work mode of the TCP/IP port.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN HANDLE  hPort : The server port
 * RETURN   : HANDLE : non-zero for successful, NULL for failure
 * COMMENTS : 
 * CREATOR  : Frank Mao                DATE: 2004-09-13 20:43
 *==========================================================================*/
HANDLE Virtual_CommAccept(IN HANDLE hPort)
{
	VIRTUAL_PORT_DRV *pPort   = (VIRTUAL_PORT_DRV *)hPort;
	VIRTUAL_PORT_DRV *pClient = NULL;

	/* clear last error */
	pPort->nLastErrorCode = ERR_COMM_OK;
	/* check the socket whether a server port or not,
 	 * only server port is allowed to call accept
	 */
	if (pPort->nWorkMode != COMM_LOCAL_SERVER)
	{
		pPort->nLastErrorCode = ERR_COMM_SUPPORT_ACCEPT;
		return NULL;
	}

	/* check the current client num exceeds the maximum connections or not */
	if (*pPort->pCurClients >= pPort->nMaxClients)
	{
		pPort->nLastErrorCode = ERR_COMM_MANY_CLIENTS;
		return NULL;
	}

	// For serial, the accept calling always return the handle directly	
	/* Ok, create a client object */
	pClient = NEW(VIRTUAL_PORT_DRV,1);
	if (pClient == NULL)	/* out of memory error	*/
	{
		pPort->nLastErrorCode = ERR_COMM_NO_MEMORY;
		return NULL;
	}

	*pClient			= *pPort;	/* Copy all info from server port	*/
	pClient->nWorkMode	= COMM_INCOMING_CLIENT;
	(*pPort->pCurClients)++;		/* increase the current linkage		*/

	return (HANDLE)pClient;
}


/* The "CommRead" proc for comm driver */
/*==========================================================================*
 * FUNCTION : CommRead
 * PURPOSE  : Read data from a opened serial port
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN HANDLE  hPort        : the opened port
 *            OUT char   *pBuffer     : The buffer to save received data
 *            IN int     nBytesToRead :
 * RETURN   : int : the actual read bytes.
 * COMMENTS : The caller shall check the bytes to read is equal to the returned
 *            bytes to decide the read action is successful or not.
 * CREATOR  : Frank Mao                DATE: 2004-09-13 21:17
 *==========================================================================*/
int Virtual_CommRead(IN HANDLE hPort, OUT char *pBuffer, IN int nBytesToRead)
{
	
    return nBytesToRead;
}

/* The "CommWrite" proc for comm driver */
/*==========================================================================*
 * FUNCTION : Virtual_CommWrite
 * PURPOSE  : write a buffer to a port
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN HANDLE  hPort         : 
 *            IN char    *pBuffer      : 
 *            IN int     nBytesToWrite : 
 * RETURN   : int : < 0 for error. <nBytesToWrite timeout, = nBytesToWrite ok
 * COMMENTS : 
 * CREATOR  : Frank Mao                DATE: 2004-09-13 21:36
 *==========================================================================*/
int Virtual_CommWrite(IN HANDLE hPort, IN char *pBuffer,	IN int nBytesToWrite)
{
	return nBytesToWrite;
}


/*==========================================================================*
 * FUNCTION :Virtual_CommControl
 * PURPOSE  : To control a opened port with command nCmd.
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN HANDLE    hPort       : 
 *            IN int       nCmd        : 
 *            IN OUT void  *pBuffer    : 
 *            IN int       nDataLength : 
 * RETURN   : int :ERR_OK for OK, else is error code
 * COMMENTS : 
 * CREATOR  : Frank Mao                DATE: 2004-09-13 21:45
 *==========================================================================*/
int Virtual_CommControl(IN HANDLE hPort, IN int	nCmd, 
					  IN OUT void *pBuffer,	IN int	nDataLength)
{
	/* Now we use switch to test the command, 
	 * we can optimize it if there are too many commands 
	 * which maybe make lower efficiency
	 */
	VIRTUAL_PORT_DRV *pPort = (VIRTUAL_PORT_DRV *)hPort;
	int nErrCode = ERR_COMM_OK;

	if (hPort == NULL)
	{
		return ERR_COMM_PORT_HANDLE;
	}

	switch (nCmd)
	{
	case COMM_GET_LAST_ERROR:	/* get the last error code	*/
		{
			ASSERT(pBuffer);
			ASSERT(nDataLength == sizeof(int));

			*(int *)pBuffer = pPort->nLastErrorCode;
		}

		break;

	case COMM_GET_PEER_INFO:	/* get the sender info		*/
		{
			COMM_PEER_INFO *pPeerInfo = (COMM_PEER_INFO *)pBuffer;

			ASSERT(pBuffer);
			ASSERT(nDataLength == sizeof(COMM_PEER_INFO));

			if (pPort->nWorkMode != COMM_LOCAL_SERVER)
			{
				pPeerInfo->nPeerPort = pPort->nVirtualPort;
				pPeerInfo->nPeerType = pPort->nWorkMode;
			}
			else
			{
				/* The server port does not support		*/
				nErrCode = ERR_COMM_CTRL_COMMAND;
			}
		}

		break;

	case COMM_PURGE_TXCLEAR:	/* clear the transmit buffer	*/
		{
			if (pPort->nWorkMode != COMM_LOCAL_SERVER)
			{
//				tcflush(pPort->fdSerial,TCOFLUSH);
				Sleep(50);	// do NOTHING.
			}
			else
			{
				/* The server port does not support		*/
				nErrCode = ERR_COMM_CTRL_COMMAND;
			}
		}

		break;
	default:
		{
			nErrCode = ERR_COMM_CTRL_COMMAND;
		}
	}

	return nErrCode;
}

/* The "CommClose" proc for comm driver */
/*==========================================================================*
 * FUNCTION : CommClose
 * PURPOSE  : Close an opened port and release the memory of the port
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: IN HANDLE  hPort : 
 * RETURN   : int : ERR_COMM_OK or ERR_COMM_PORT_HANDLE
 * COMMENTS : 
 * CREATOR  : Frank Mao                 DATE: 2004-09-14 20:32
 *==========================================================================*/
int Virtual_CommClose(IN HANDLE hPort)
{
	VIRTUAL_PORT_DRV *pPort = (VIRTUAL_PORT_DRV *)hPort;

	if (hPort == NULL)
	{
		return ERR_COMM_PORT_HANDLE;
	}

	/* descrease the linkage, the pCurClients shall be deleted if no
	 * any clients. But the server and clients share the same pCurClients,
	 * and the initialing value of *pCurClients is 0, so the condition to
	 * delete pCurClients is *pCurClients < 0.
	 */
	(*pPort->pCurClients)--;
	if (*pPort->pCurClients < 0)
	{
		pPort->fdSerial = 0;;	// to close fd if it is not used yet.
		DELETE(pPort->pCurClients);
	}

	DELETE(pPort);

	return ERR_COMM_OK;
}


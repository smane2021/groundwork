﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Battery Current				Batt Current		Courant batterie		Courant bat
2	32			15			Battery Rating(Ah)			Batt Rating(Ah)		Evaluation Batterie(Ah)		Evalua Batt(Ah)
3	32			15			Exceed Current Limit			Exceed Curr Lmt		Depacement limit.Courant	Depace I limit
4	32			15			Battery					Battery			Batterie			Batterie
5	32			15			Over Battery Current			Over Current		Depacement sur-courant		Depace sur-I
6	32			15			Battery Capacity (%)			Batt Cap (%)		Capacite batterie(%)		Capacite bat(%)
7	32			15			Battery Voltage				Batt Voltage		Tension batterie		Tension bat
8	32			15			Low Capacity				Low Capacity		Capacite basse			Capacite basse
9	32			15			On					On			Correct				Correct
10	32			15			Off					Off			Defaut				Defaut
11	32			15			Battery Fuse Voltage			Fuse Voltage		Seuil Detect V Def Protect Bat	V Def Prot Bat
12	32			15			Battery Fuse Status			Fuse Status		Etat Protection Bat		Etat Prot Bat
13	32			15			Fuse Alarm				Fuse Alarm		Alarm Protection Bat		AL Prot Bat
14	32			15			SMDUE Battery				SMDUE Battery		SMDUEBatteries			SMDUEBatteries
15	32			15			State					State			Etat				Etat
16	32			15			Off					Off			Arrêt				Arrêt
17	32			15			On					On			Marche				Marche
18	32			15			Switch					Switch			Commutateur			Commutateur
19	32			15			Battery Over Current			Batt Over Curr		Sur Courant Batterie		Sur I Batterie
20	32			15			Battery Management			Batt Management			Carte active dans Process	CI acti Process
21	32			15			Yes					Yes			Oui				Oui
22	32			15			No					No			Non				Non
23	32			15			Over Voltage Limit			Over Volt Limit		Tension haute Niveau 1		V haute Niv.1
24	32			15			Low Voltage Limit			Low Volt Limit		Tension basse niveau 1		V basse Niv.1
25	32			15			Battery Over Voltage			Over Voltage		Tension haute Niveau 2		V haute Niv.2
26	32			15			Battery Under Voltage			Under Voltage		Tension basse niveau 2		V basse Niv.2
27	32			15			Over Current				Over Current		Sur Courant Batterie		Sur I Batterie
28	32			15			Communication Fail			Comm Fail		Perte Communication		Perte Comm.
29	32			15			Times of Communication Fail			Times Comm Fail		Duree du defaut de communication		DureeDefCOM

44	32			15			Battery Temp Number			Batt Temp Num	N° de sonde batterie		N°sonde bat.

87	32			15			No					No		Aucune				Aucune

91	32			15			Temperature 1			Temperature 1		Température 1			Température 1
92	32			15			Temperature 2			Temperature 2		Température 2			Température 2
93	32			15			Temperature 3			Temperature 3		Température 3			Température 3
94	32			15			Temperature 4			Temperature 4		Température 4			Température 4
95	32			15			Temperature 5			Temperature 5		Température 5			Température 5
96	32			15			Rated Capacity				Rated Capacity		Capacité Estimée		Capa Estimée
97	32			15			Battery Temperature			Battery Temp		Température Batterie		Temp Bat
98	32			15			Battery Temperature Sensor		BattTemp Sensor		Capteur Température Batterie	Capt Temp Bat
99	32			15			None					None			Aucune				Aucune
100	32			15			Temperature 1			Temperature 1		Température 1			Température 1
101	32			15			Temperature 2			Temperature 2		Température 2			Température 2
102	32			15			Temperature 3			Temperature 3		Température 3			Température 3
103	32			15			Temperature 4			Temperature 4		Température 4			Température 4
104	32			15			Temperature 5			Temperature 5		Température 5			Température 5
105	32			15			Temperature 6			Temperature 6		Température 6			Température 6
106	32			15			Temperature 7			Temperature 7		Température 7			Température 7
107	32			15			Temperature 8			Temperature 8		Température 8			Température 8
108	32			15			Temperature 9			Temperature 9		Température 9			Température 9
109	32			15			Temperature 10			Temperature 10		Température 10			Température 10

110	32			15			SMDUE1 Battery1			SMDUE1 Battery1			SMDUE1Batteries1	SMDUE1Batt1
111	32			15			SMDUE1 Battery2			SMDUE1 Battery2			SMDUE1Batteries2	SMDUE1Batt2
112	32			15			SMDUE1 Battery3			SMDUE1 Battery3			SMDUE1Batteries3	SMDUE1Batt3
113	32			15			SMDUE1 Battery4			SMDUE1 Battery4			SMDUE1Batteries4	SMDUE1Batt4
114	32			15			SMDUE1 Battery5			SMDUE1 Battery5			SMDUE1Batteries5	SMDUE1Batt5
115	32			15			SMDUE1 Battery6			SMDUE1 Battery6			SMDUE1Batteries6	SMDUE1Batt6
116	32			15			SMDUE1 Battery7			SMDUE1 Battery7			SMDUE1Batteries7	SMDUE1Batt7
117	32			15			SMDUE1 Battery8			SMDUE1 Battery8			SMDUE1Batteries8	SMDUE1Batt8
118	32			15			SMDUE1 Battery9			SMDUE1 Battery9			SMDUE1Batteries9	SMDUE1Batt9
119	32			15			SMDUE1 Battery10		SMDUE1 Battery10		SMDUE1Batteries10	SMDUE1Batt10
120	32			15			SMDUE2 Battery1			SMDUE2 Battery1			SMDUE2Batteries1	SMDUE2Batt1
121	32			15			SMDUE2 Battery2			SMDUE2 Battery2			SMDUE2Batteries2	SMDUE2Batt2
122	32			15			SMDUE2 Battery3			SMDUE2 Battery3			SMDUE2Batteries3	SMDUE2Batt3
123	32			15			SMDUE2 Battery4			SMDUE2 Battery4			SMDUE2Batteries4	SMDUE2Batt4
124	32			15			SMDUE2 Battery5			SMDUE2 Battery5			SMDUE2Batteries5	SMDUE2Batt5
125	32			15			SMDUE2 Battery6			SMDUE2 Battery6			SMDUE2Batteries6	SMDUE2Batt6
126	32			15			SMDUE2 Battery7			SMDUE2 Battery7			SMDUE2Batteries7	SMDUE2Batt7
127	32			15			SMDUE2 Battery8			SMDUE2 Battery8			SMDUE2Batteries8	SMDUE2Batt8
128	32			15			SMDUE2 Battery9			SMDUE2 Battery9			SMDUE2Batteries9	SMDUE2Batt9
129	32			15			SMDUE2 Battery10		SMDUE2 Battery10		SMDUE2Batteries10	SMDUE2Batt10

130	32			15			Battery 1			Batt 1			Batterie 1			Batt 1	
131	32			15			Battery 2			Batt 2			Batterie 2			Batt 2	
132	32			15			Battery 3			Batt 3			Batterie 3			Batt 3	
133	32			15			Battery 4			Batt 4			Batterie 4			Batt 4	
134	32			15			Battery 5			Batt 5			Batterie 5			Batt 5	
135	32			15			Battery 6			Batt 6			Batterie 6			Batt 6	
136	32			15			Battery 7			Batt 7			Batterie 7			Batt 7	
137	32			15			Battery 8			Batt 8			Batterie 8			Batt 8	
138	32			15			Battery 9			Batt 9			Batterie 9			Batt 9	
139	32			15			Battery 10			Batt 10			Batterie 10			Batt 10
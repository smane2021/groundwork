﻿#
#  Locale language support:French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE			
1	32			15			Solar Converter Group			Solar ConvGrp		Groupe Convertisseur Solaire		Gr.Convert.Sol.
2	32			15			Total Current				Total Current		Courant Total				Courant Total
3	32			15			Average Voltage				Average Voltage		Tension Moyenne				Tension Moyenne
4	32			15			Solar Converter Capacity Used	Solar Cap Used		Capacité conv solaire utilisée	Cap solaire util
5	32			15			Maximum Used Capacity			Max Cap Used		Puissance Maximum Utilisé		P.Max.Utilisé
6	32			15			Minimum Used Capacity			Min Cap Used		Puissance Minimum Utilisé		P.Mini.Utilisé
7	36			15			Total Solar Converters Communicating	Num Solar Convs		Nombre Convertisseur Solaire		Nb.Conv.Sol.
8	32			15			Valid Solar Converter			Valid SolarConv		Nombre Conv Solaire Valide		Nb.Conv.Sol.Val
9	32			15			Number of Solar Converters		Num Solar Convs		Numéro Convertisseur Solaire		N°.Conv.Sol.
11	34			15			Solar Converter(s) Failure Status	SolarConv Fail		Multi Convertisseur en défaut		Multi Déf Conv
12	32			15			Solar Converter Current Limit		SolConvCurrLmt		Conv Solaire Lmt Courant		Conv.Sol.I Lim.
13	32			15			Solar Converter Trim			Solar Conv Trim		Version Convertisseur			Version Conv.
14	32			15			DC On/Off Control			DC On/Off Ctrl		Controle DC On/Off			Contr.DC On/Off
16	32			15			Solar Converter LED Control		SolConv LEDCtrl	Controle Led Convert.			Contr.Led Conv.
17	32			15			Fan Speed Control			Fan Speed Ctrl		Controle Vitesse Vent.Convert.	Cont.Vit.Vent.
18	32			15			Rated Voltage				Rated Voltage		Tension					Tension
19	32			15			Rated Current				Rated Current		Courant					Courant
20	32			15			High Voltage Shutdown Limit		HVSD Limit		Seuil Sur Tension			Seuil Sur U
21	32			15			Low Voltage Limit			Low Volt Limit		Limite Tension Basse			Lim U Basse
22	32			15			High Temperature Limit			High Temp Limit		Limite Température Haute		Lim Temp. Haute
24	32			15			HVSD Restart Time			HVSD Restart T		Temps Redém.Surtension		Tps Redém.SurU
25	32			15			Walk-In Time				Walk-In Time		Temporisation au Démarrage		Tempo Démarrage
26	32			15			Walk-In					Walk-In			Activation Tempo. Démarrage		Act.Tempo.Démar
27	32			15			Minimum Redundancy			Min Redundancy		Redondance Minimum			Redondance Min
28	32			15			Maximum Redundancy			Max Redundancy		Redondance Maximum			Redondance Max
29	32			15			Turn Off Delay				Turn Off Delay		Délais d'Arrêt				Délais Arrêt
30	32			15			Cycle Period				Cycle Period		Cycle périodique			Cycle périodi.
31	32			15			Cycle Activation Time			Cyc Active Time		Durée cycle activation			Durée cycl acti
33	32			15			Multiple Solar Converter Failure	MultSolConvFail		Défaut Plus 1 Convertisseur		Défaut +1 Conv.
36	32			15			Normal					Normal			Normal					Normal
37	32			15			Failure					Failure			Défaut					Défaut
38	32			15			Switch Off All				Switch Off All		Tous Redresseur sur Arrêt		Arrêt Tous Red.
39	32			15			Switch On All				Switch On All		Tous Redresseur en Marche		Tous Red Marche
42	32			15			All Flashing				All Flashing		Clignotante				Clignotante
43	32			15			Stop Flashing				Stop Flashing		Arrêt Clignotement			Arrêt Clignot.
44	32			15			Full Speed				Full Speed		Vitesse maximum				Vitesse max
45	32			15			Automatic Speed				Auto Speed		Vitesse Automatique			Vitesse Auto
46	32			32			Current Limit Control			Curr Limit Ctrl		Control Limitation courant		Cont.Lim.I
47	32			32			Full Capability Control			Full Cap Ctrl		Puisssance maximum			Puis. Maximum
54	32			15			Disabled				Disabled		Désactivé				Désactivé
55	32			15			Enabled					Enabled			Activé					Activé
68	32			15			ECO Mode				ECO Mode		Mode ECO				Mode ECO
73	32			15			No					No			Non					Non
74	32			15			Yes					Yes			Oui					Oui
77	32			15			Pre-CurrLimit Turn-On			Pre-Curr Limit		Pré-limitation au démarrage		Pré-limit Start
78	32			15			Solar Converter Power Type		SolConv PwrType		Type Convertisseur Solaire		Type Conv.Sol.
79	32			15			Double Supply				Double Supply		Double Entree AC			2 Entrees AC
80	32			15			Single Supply				Single Supply		Simple Entree AC			1 Entree AC
81	32			15			Last Solar Converter Quantity		LastSolConvQty		Dernière Quantitée Convertisseur	Dern.Quant.Con.
83	32			15			Solar Converter Lost			Solar Conv Lost		Perte Convertisseur			Perte Convert.
84	32			15			Clear Solar Converter Lost Alarm	ClearSolarLost	Reset Perte Convertisseur		Reset Pert.Con.
85	32			15			Clear					Clear			Reset Perte Convertisseur		Reset Perte Con
86	32			15			Confirm Solar Converter ID		Confirm ID		Confirmer N° Convertisseur		Conf N° Conv.
87	32			15			Confirm					Confirm			Confirmation				Confirmation
88	32			15			Best Operating Point			Best Oper Point		Meilleur Point de Fonct		Meil PointFonct
89	32			15			Solar Converter Redundancy		SolConv Redund		Redondance Convertisseur		Redondan.Conv.
90	32			15			Load Fluctuation Range			Fluct Range		Pourcentage Equilibrage			% Equilibrage
91	32			15			System Energy Saving Point		Energy Save Pt		Meilleur Point Fonct	Meil PointFonct
92	32			15			E-Stop Function				E-Stop Function		Fonction E-Stop				Fonction E-Stop
94	32			15			Single Phase				Single Phase		Entrée Monophasé			Entrée 1 Phase
95	32			15			Three Phases				Three Phases		Entrée Tri-Phasée			Entrée 3 Phases
96	32			15			Input Current Limit			Input Curr Lmt		Limit Courant AC			Limit I AC
97	32			15			Double Supply				Double Supply		Double Entrée AC			2 Entrées AC
98	32			15			Single Supply				Single Supply		Simple Entrée AC			1 Entrée AC
99	32			15			Small Supply				Small Supply		Petite Puissance			Petite Puissanc
100	32			15			Restart on HVSD				Restart on HVSD		Redémarrage Redresseur Surt DC		Dem Red Surt DC
101	32			15			Sequence Start Interval			Start Interval		Démarrage Séquentiel			Dem. Séquentiel
102	32			15			Rated Voltage				Rated Voltage		Tension de repli			Tension repli
103	32			15			Rated Current				Rated Current		Courant max de repli			Imax repli
104	32			15			All Solar Converters Comm Fail		SolarsComFail	Pas de Réponse Convertisseur		Pas Rép.Conv.
105	32			15			Inactive				Inactive		Désactive				Désactive
106	32			15			Active					Active			Activé					Activé
112	32			15			Rated Voltage (Internal Use)		Rated Voltage		Tension de repli(Système)		Tension repli
113	32			15			SolarConv Info Change(Internal)		InfoChange		Modification des Infos		Modif.des Infos
114	32			15			MixHE Power				MixHE Power		Mix Type Convertisseur			Mix Type Con.
115	32			15			Derated					Derated			Réduction				Réduction
116	32			15			Non-Derated				Non-Derated		Pas de Réduction			Pas Réduction
117	32			15			All Solar Converter ON Time		SolarConvONTime		Tous les Convert Solaire ON		Tous Conv. ON
118	32			15			All Solar Converter are On		AllSolarConvOn		Tous les Convert Solaire ON			Tous Conv. ON
119	39			15			Clear Solar Converter Comm Fail Alarm	ClrSolCommFail		Reset Alarme Convertisseur		Reset Al.Conv.
120	32			15			HVSD					HVSD			Surtension DC Active			Sur U DC Active
121	32			15			HVSD Voltage Difference			HVSD Volt Diff		Dif U Surtension			Dif U SurU
122	32			15			Total Rated Current			Total Rated Cur		Courant Total				Courant Total
123	32			15			Diesel Generator Power Limit		DG Pwr Lmt		Limitation Puissance G.E.		Limi.P.G.E.
124	32			15			Diesel Generator Digital Input		Diesel DI Input		Entrée Commande G.E.			Entré Comm.G.E.
125	32			15			Diesel Gen Power Limit Point Set	DG Pwr Lmt Pt		Puissance Maximum G.E.			P.Max.G.E.
126	32			15			None					None			Non					Non
140	32			15			Solar Converter Delta Voltage		SolConvDeltaVol		Solar Converter Delta Voltage		SolConvDeltaVol
141	32			15			Solar Status				Solar Status		Etat Solaire				Etat Solaire
142	32			15			Day					Day			Jour					Jour
143	32			15			Night					Night			Nuit					Nuit
144	32			15			Existence State				Existence State		Présent					Présent
145	32			15			Existent				Existent		Présent					Présent
146	32			15			Not Existent				Not Existent		Non Présent				Non Présent
147	32			15			Total Input Current			Input Current		Courant d'entrée total			CouranEntrTotal
148	32			15			Total Input Power			Input Power		Puissance active totale			W Active Totale	
149	32			15			Total Rated Current			Total Rated Cur		Courant nominal total		CourantNomTotal
150	32			15			Total Output Power			Output Power		Puissance de sortie totale			W Sortie Totale
151	32			15			Reset Solar Converter IDs		Reset Solar IDs		Reset Solar Convertisseur IDs	Reset Solar IDs
152	32			15			Clear Solar Converter Comm Fail		ClrSolCommFail		Clear Solar Converter Comm Fail		ClrSolarComFail
153	32			15			Solar Failure Min Time			SolFailMinTime		Temps minimal d'échec solaire		TemMind'écSol

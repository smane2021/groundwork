﻿
#Language resources file

[LOCAL_LANGUAGE]
fr

[LOCAL_LANGUAGE_VERSION]
1.00

[NUM_OF_PAGES]
39

[login.html:Number]
50

[login.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIP1					64			You are requesting access		Vous demandez l'acces
2		ID_TIP2					32			located at				localise à:
3		ID_TIP3					128			The user name and password for this device is set by the system administrator.		Nom d'utilisateur et mot de passe pour cet equipement est configure par l'administrateur
4		ID_LOGIN				32			Login				Login
5		ID_USER					32			User				Utilisateur
6		ID_PASSWD				32			Password				Mot de passe
7		ID_LOCAL_LANGUAGE			32			Français				Français
8		ID_SITE_NAME				32			Site Name				Nom site
9		ID_SYSTEM_NAME				32			System Name				Nom système
10		ID_PRODUCT_MODEL			32			Product Model			Modele produit
11		ID_SERIAL				32			Serial Number			Numero serie
12		ID_HW_VERSION				32			Hardware Version			Ver.Materiel
13		ID_SW_VERSION				32			Software Version			Ver.Logiciel
14		ID_CONFIG_VERSION			32			Config Version			Ver.Config.
15		ID_FORGET_PASSWD			32			Forgot Password			MotdepasseOublié
16		ID_ERROR0				64			Unknown Error										Erreur inconnue
17		ID_ERROR1				64			Successful											Success
18		ID_ERROR2				128			Wrong Credential or Radius Server is not reachable.	Des informations d'identification erronées ou Radius Server ne sont pas accessibles
19		ID_ERROR3				128			Wrong Credential or Radius Server is not reachable.	Des informations d'identification erronées ou Radius Server ne sont pas accessibles
20		ID_ERROR4				64			Failed to communicate with the application.							Echec de la connexion avec l'application
21		ID_ERROR5				64			Over 5 connections, please retry later.						5 tentatives refusees, essayez plus tard SVP
22		ID_ERROR6				128			Controller is starting, please retry later.							Demarrage du controleur, essayez plus tard SVP
23		ID_ERROR7				256			Automatic configuration in progress, please wait about 2-5 minutes.				Configuration automatique en cours, merci de patienter 2 à 5 minutes
24		ID_ERROR8				64			Controller in Secondary Mode.								Contrôleur en mode secondaire
25		ID_ERROR9				256			Monitoring has restarted and the password needs to be initialized.						La surveillance a redémarré et le mot de passe doit être initialisé.
25		ID_LOGIN1				32			Login				Login
26		ID_SELECT				32			Please select language				Selectionnez la langue SVP
27		ID_TIP4					32			Loading...				Chargement en cours…
28		ID_TIP5					256			Sorry, your browser doesn't support cookies or cookies are disabled. Please enable cookie or change your browser and login again.				Desole, votre navigateur n'est pas compatible ou les cookies sont invalidees
29		ID_TIP6					128			Data lost.<a href='login.html' style='margin:0 10px;'>Retry</a>				Donnees perdues <a href='login.html' style='margin:0 10px;'>
30		ID_TIP7					64			Controller is starting, please wait...						Le controleur est en cours de demarrage, patientez SVP
31		ID_TIP8					32			Logging In ...			Connexion en…
32		ID_TIP9					32			Login				Login
49		ID_FORGET_PASSWD			32			Forgot Password?						Mot de passe oublié?
50		ID_TIP10				64			Loading, please wait...				Chargement en cours, patientez SVP
51		ID_TIP11				64			Controller is in Secondary Mode.				Contrôleur en mode secondaire
52		ID_LOGIN_TITLE				32			Login-Vertiv G3						Login-Vertiv G3
53		ID_ALARMSOUNDOFF				32			Touch Screen To Silence Alm			Écran Tactile Faire Taire Alm
54		ID_PLANTLOAD				32			Plant Load			Charge de l'usine
55		ID_PLANTVOLTAGE				32			Plant Voltage			Tension de l'installation
56		ID_ALARMS				32			Alarms			Les alarmes
57		ID_OBSERVATION				32			Observation			Observation
58		ID_MAJOR				32			Major			Majeure
59		ID_CRITICAL				32			Critical			Critique
60		ID_ERROR10				128			Wrong Credential or Radius Server is not reachable.	Des informations d'identification erronées ou Radius Server ne sont pas accessibles
61		ID_ERROR11				128			Your account has been locked. Contact your local administrator for assistance.	Vôtre compte a été bloqué. Contactez votre administrateur local pour obtenir de l'aide.
62		ID_ERROR12				128			Your account has been temporarily locked for next 30 minutes.	Votre compte a été temporairement verrouillé pendant 30 minutes.

[index.html:Number]
147

[index.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	STRING				LOCAL
1		ID_ALL_ALARMS				32			All Alarms				Toutes les alarmes
2		ID_OBSERVATION				32			Observation				Observation
3		ID_MAJOR				32			Major				Majeure
4		ID_CRITICAL				32			Critical				Critique
5		ID_CONTROLLER_SPECIFICATIONS		32			Controller Specifications		Specification contrôleur
6		ID_SYSTEM_NAME				32			System Name				Nom système
7		ID_RECTIFIERS_NUMBER			32			Rectifiers				Redresseurs
8		ID_CONVERTERS_NUMBER			32			Converters				Convertisseurs
9		ID_SYSTEM_SPECIFICATIONS		32			System Specifications		Specifications du système
10		ID_PRODUCT_MODEL			32			Product Model			Modele produit
11		ID_SERIAL_NUMBER			32			Serial Number			Numero serie	
12		ID_HARDWARE_VERSION			32			Hardware Version			Ver.Materiel
13		ID_SOFTWARE_VERSION			32			Software Version			Ver.Logiciel
14		ID_CONFIGURATION_VERSION		32			Config Version			Ver.Config.
15		ID_HOME					32			Home				Menu principal
16		ID_SETTINGS				32			Settings				Configuration
17		ID_HISTORY_LOG				32			History Log				Journal historique
18		ID_SYSTEM_INVENTORY			32			System Inventory			Inventaire système
19		ID_ADVANCED_SETTING			32			Advanced Settings			Configuration avancee
20		ID_INDEX				32			Index				Index
21		ID_ALARM_LEVEL				32			Alarm Level				Niveau d'alarme
22		ID_RELATIVE				32			Relative Device			Equipement concerne
23		ID_SIGNAL_NAME				32			Signal Name				Nom de l'entree
24		ID_SAMPLE_TIME				32			Sample Time				Temps d'échantillonage
25		ID_WELCOME				32			Welcome				Bienvenue
26		ID_LOGOUT				32			SIGN OUT				Déconnexion
27		ID_AUTO_POPUP				32			Auto Popup				Popup Auto
28		ID_COPYRIGHT				64			2018 Vertiv Tech Co.,Ltd.All rights reserved.		2018 Vertiv Tech Co.,Ltd.All rights reserved.
29		ID_OA					32			Observation				Observation
30		ID_MA					32			Major				Majeure
31		ID_CA					32			Critical				Critique
32		ID_HOME1				32			Home				Menu principal
33		ID_ERROR0				32			Failed.				Echec
34		ID_ERROR1				32			Successful.				Success
35		ID_ERROR2				32			Failed. Conflicting setting.	Echec. Conflit de parametrage .
36		ID_ERROR3				32			Failed. No privileges.		Echec. Droits insuffisants .
37		ID_ERROR4				32			No information to send.		Pas d'information envoyées
38		ID_ERROR5				64			Failed. Controller is hardware protected.		Echec. Protection materielle du controleur
39		ID_ERROR6				64			Time expired. Please login again.				Temps dépassé. Connectez vous à nouveau. 
40		ID_HIS_ERROR0				32			Unknown error.				Erreur inconnue		
41		ID_HIS_ERROR1				32			Successful.					Success			
42		ID_HIS_ERROR2				32			No data.					Pas de donnée
43		ID_HIS_ERROR3				32			Failed					Echec				
44		ID_HIS_ERROR4				32			Failed. No privileges.			Echec. Droits insuffisants .			
45		ID_HIS_ERROR5				64			Failed to communicate with the controller.	Echec de communication avec le controleur.		
46		ID_HIS_ERROR6				64			Clear successful.				Succes de l'initialisation		
47		ID_HIS_ERROR7				64			Time expired. Please login again.		Temps dépassé. Connectez vous à nouveau. 
48		ID_WIZARD				32			Install Wizard				Installer Wizard
49		ID_SITE					16			Site					Site
50		ID_PEAK_CURR				32			Peak Current			Pointe de courant
51		ID_INDEX_TIPS1				64			Loading, please wait.			Chargement en cours, patientez SVP
52		ID_INDEX_TIPS2				128			Data format error, getting the data again...please wait.			Erreur de format de donnee, nouvelle demande de donnee…  veuillez patienter .
53		ID_INDEX_TIPS3				128			Fail to load the data, please check the network and data file.		Echec de chargement de donnée, verifier le reseau et le fichier de données .
54		ID_INDEX_TIPS4				128			Template webpage not exist or network error.			Page Web inexistante ou erreur réseau .
55		ID_INDEX_TIPS5				64			Data lost.			Donnees perdues .
56		ID_INDEX_TIPS6				64			Setting...please wait.			Configuration…veuillez patienter
57		ID_INDEX_TIPS7				64			Confirm logout?			Confirmation de la déconnexion?
58		ID_INDEX_TIPS8				64			Loading data, please wait.			Chargement donnée, patientez SVP
59		ID_INDEX_TIPS9				64			Please shut down the browser to return to the home page.		Fermer le navigateur pour revenir a la page d'acceuil .
60		ID_INDEX_TIPS10				16			OK				OK
61		ID_INDEX_TIPS11				16			Error:		Erreur:
62		ID_INDEX_TIPS12				16			Retry			Reessayer
63		ID_INDEX_TIPS13				16			Loading...			Chargement
64		ID_INDEX_TIPS14				64			Time expired. Please login again.			Temps dépassé. Connectez vous à nouveau 
65		ID_INDEX_TIPS15				32			Re-loading			Re-chargement
66		ID_INDEX_TIPS16				128			File does not exist or unable to load the file due to a network error.			Fichier inexistant ou indisponible à cause d'un probleme reseau .
67		ID_INDEX_TIPS17				32			id is \"			id is \"
68		ID_INDEX_TIPS18				32			Request error.			Erreur de requete .
69		ID_INDEX_TIPS19				32			Login again.			Nouvelle connexion  .
70		ID_INDEX_TIPS20				32			No Data			Pas de donnée
71		ID_INDEX_TIPS21				128			Can't be empty, please enter 0 or a positive number within range.		Valeur obligatoire, positive dans la plage, ou 0  .
72		ID_INDEX_TIPS22				256			Can't be empty, please enter 0 or an integer number or a float number within range.		Valeur obligatoire, entier, flottant dans la plage, ou 0  .
73		ID_INDEX_TIPS23				256			Can't be empty, please enter 0 or a positive number or a negative number within range.		Valeur obligatoire, nombre positif ou negatif dans la plage, ou 0 .
74		ID_INDEX_TIPS24				128			No change to the current value.			Pas de modification de la valeur courante .
75		ID_INDEX_TIPS25				128			Please input an email address in the form name@domain.			SVP, saisir adresse email au format nom@domaine .
76		ID_INDEX_TIPS26				128			Please enter an IP address in the form nnn.nnn.nnn.nnn.			SVP, saisir adresse IP au format nnn.nnn.nnn.nnn .
77		ID_INDEX_TIME1				16			January			Janvier
78		ID_INDEX_TIME2				16			February			Fevrier
79		ID_INDEX_TIME3				16			March			Mars
80		ID_INDEX_TIME4				16			April			Avril
81		ID_INDEX_TIME5				16			May				Mai
82		ID_INDEX_TIME6				16			June			Juin
83		ID_INDEX_TIME7				16			July			Juillet
84		ID_INDEX_TIME8				16			August			Aout
85		ID_INDEX_TIME9				16			September			Septembre
86		ID_INDEX_TIME10				16			October			Octobre
87		ID_INDEX_TIME11				16			November			Novembre
88		ID_INDEX_TIME12				16			December			Decembre
89		ID_INDEX_TIME13				16			Jan			Jan
90		ID_INDEX_TIME14				16			Feb			Feb
91		ID_INDEX_TIME15				16			Mar			Mar
92		ID_INDEX_TIME16				16			Apr			Avr
93		ID_INDEX_TIME17				16			May			Mai
94		ID_INDEX_TIME18				16			Jun			Jui
95		ID_INDEX_TIME19				16			Jul			Jui
96		ID_INDEX_TIME20				16			Aug			Aou
97		ID_INDEX_TIME21				16			Sep			Sep
98		ID_INDEX_TIME22				16			Oct			Oct
99		ID_INDEX_TIME23				16			Nov			Nov
100		ID_INDEX_TIME24				16			Dec			Dec
101		ID_INDEX_TIME25				16			Sunday			Dimanche
102		ID_INDEX_TIME26				16			Monday			Lundi
103		ID_INDEX_TIME27				16			Tuesday			Mardi
104		ID_INDEX_TIME28				16			Wednesday			Mercredi
105		ID_INDEX_TIME29				16			Thursday			Jeudi
106		ID_INDEX_TIME30				16			Friday			Vendredi
107		ID_INDEX_TIME31				16			Saturday			Samedi
108		ID_INDEX_TIME32				16			Sun			Dim
109		ID_INDEX_TIME33				16			Mon			Lun
110		ID_INDEX_TIME34				16			Tue			Mar
111		ID_INDEX_TIME35				16			Wed			Mer
112		ID_INDEX_TIME36				16			Thu			Jeu
113		ID_INDEX_TIME37				16			Fri			Ven
114		ID_INDEX_TIME38				16			Sat			Sam
115		ID_INDEX_TIME39				16			Sun			Dim
116		ID_INDEX_TIME40				16			Mon			Lun
117		ID_INDEX_TIME41				16			Tue			Mar
118		ID_INDEX_TIME42				16			Wed			Mer
119		ID_INDEX_TIME43				16			Thu			Jeu
120		ID_INDEX_TIME44				16			Fri			Ven
121		ID_INDEX_TIME45				16			Sat			Sam
122		ID_INDEX_TIME46				16			Current Time		Heure courante
123		ID_INDEX_TIME47				16			Confirm			Confirmer
124		ID_INDEX_TIME48				16			Time			Heure
125		ID_INDEX_TIME49				16			Hour			Heure
126		ID_INDEX_TIME50				16			Minute			Minute
127		ID_INDEX_TIME51				16			Second			Seconde
128		ID_MPPTS				32			Solar Converters		Convertisseurs solaires
129		ID_INDEX_TIPS27				32			Refresh			Rafraichir
130		ID_INDEX_TIPS28				32			There is no data to show.				Pas de data a afficher
131		ID_INDEX_TIPS29				128			Fail to connect to controller, please login again.		Echec de la connexion avec le controleur, SVP connectez vous a nouveau .
132		ID_INDEX_TIPS30				128			Link attribute \"data\" data structure error, should be {} object format.			Link attribute \"data\" data structure error, should be {} object format.
133		ID_INDEX_TIPS31				256			The format of \"validate\" is incorrect.  It should be have {} object format.  Please check the template file.			Format de \"validate\" incorrect. Devrait etre {} object format.  SVP, verifier le format
134		ID_INDEX_TIPS32				128			Data format error.			Erreur de format de donnée
135		ID_INDEX_TIPS33				64			Setting date error.			Erreur de format de date!
136		ID_INDEX_TIPS34				128			There is an error in the input parameter.  It should be in the format ({}).		Erreur dans la donnée saisie. Devrait etre au format ({})
137		ID_INDEX_TIPS35				64			Too many clicks. Please wait.			Attendre la fin de la mise a jour complète
138		ID_INDEX_TIPS36				32			Refresh webpage			Rafraichir la page web
139		ID_INDEX_TIPS37				32			Refresh console			Rafraichir l'écran
140		ID_INDEX_TIPS38				128			Special characters are not allowed.			Caractères speciaux interdits
141		ID_INDEX_TIPS39				64			The value can not be empty.			Valeur obligatoire .
142		ID_INDEX_TIPS40				64			The value must be a positive integer or 0.			Valeur doit etre un entier positif ou 0 .
143		ID_INDEX_TIPS41				64			The value must be a floating point number.			Valeur doit etre un réel positif ou 0 .
144		ID_INDEX_TIPS42				64			The value must be an integer.			Valeur doit etre un entier ou 0 .
145		ID_INDEX_TIPS43				64			The value is out of range.			Valeur hors plage .
146		ID_INDEX_TIPS44				32			Communication fail.			Defaut de communication .
147		ID_INDEX_TIPS45				64			Confirm the change to the control value?			Vous confirmez la modification de la valeur?
148		ID_INDEX_TIPS46				128			Time format error or time is beyond the range. The format should be: Year/Month/Day Hour/Minute/Second			Erreur de format ou l'heure est au-delà de la plage. Le format doit être: Année / Mois / Jour Heure / minute / seconde .
149		ID_INDEX_TIPS47				32			Unknown error.				Erreur inconnue.			
150		ID_INDEX_TIPS48				64			(Data is beyond the normal range.)	(Donnée hors plage .)
151		ID_INDEX_TIPS49				12			Date:				Date:
152		ID_INDEX_TIPS50				32			Temperature Trend Diagram		Courbe de temperature		
153		ID_INDEX_TIPS51				16			Tips					Conseil		
154		ID_TIPS1				32			Unknown error.			Erreur inconnue
155		ID_TIPS2				16			Successful.							Succes.
156		ID_TIPS3				128			Failed. Incorrect time setting.					Erreur. Heure configuree incorrecte.
157		ID_TIPS4				128			Failed. Incomplete information.				Erreur. Information incomplete.	
158		ID_TIPS5				64			Failed. No privileges.					Echec. Vous n'avez pas les droits.		
159		ID_TIPS6				128			Cannot be modified. Controller is hardware protected.	Modification impossible. Protection materielle du controleur.
160		ID_TIPS7				256			Secondary Server IP Address is incorrect. \nThe format should be :'nnn.nnn.nnn.nnn'.	Seconde adresse IP serveur incorrecte. \nformat est: 'nnn.nnn.nnn.nnn'.
161		ID_TIPS8				128			Format error! There is one blank between time and date.					Erreur de format! Il y a un espace entre l'heure et la date.
162		ID_TIPS9				128					Incorrect time interval. \nTime interval should be a positive integer.			Interval de temps incorrect \nInterval de temps doit etre positif
163		ID_TIPS10				128			Date should be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.			Saisir date entre '1970/01/01 00:00:00' et '2038/01/01 00:00:00''.
164		ID_TIPS11				128					Please clear the IP before time setting.							SVP - Effacer l'adresse IP avant de configurer l'heure 
165		ID_TIPS12				64			Time expired, please login again.						Temps expérisé, reconnectez-vous SVP
166		ID_TIPS13				16			Site						Site
167		ID_TIPS14				16			System Name						Nom Système
168		ID_TIPS15				32			Back to Battery List		Retour aux types de batteries
169		ID_TIPS16				64			Capacity Trend Diagram		Courbe de charge		
170		ID_INDEX_TIPS52				64			Set successfully. Controller is restarting, please wait		Configuration reussie, redemarrage controleur, patientez .
171		ID_INDEX_TIPS53				16			seconds.					Seconde.
172		ID_INDEX_TIPS54				64			Back to the login page, please wait.		Retour a la page d'acceuil, patientez SVP		
173		ID_INDEX_TIPS55				32			Confirm to be set to				Confirmer la configuration		
174		ID_INDEX_TIPS56				16			's value is					valeur est		
175		ID_INDEX_TIPS57				32			controller will restart.					controller va redemarrer		
176		ID_INDEX_TIPS58				64			Template error, please refresh the page.		Erreur de format, reafficher la page SVP		
177		ID_INDEX_TIPS59				64			[OK]Reconnect. [Cancel]Stop the page.		[OK]Reconnecter [Cancel]Effacer		
178		ID_ENGINEER				32			Engineer Settings				Parametres \"Ingenieur\"		
179		ID_INDEX_TIPS60				128			Jump to the engineer web pages, please use IE browser.		Aller aux pages web ingenieur, ouvrir votre navigateur SVP		
180		ID_INDEX_TIPS61				64			Jumping, please wait.		Redirection, patientez SVP.		
181		ID_INDEX_TIPS62				64			Fail to jump.		Echec de redirection		
182		ID_INDEX_TIPS63				64			Please select new alarm level.		Modifiez le niveau d'alarme SVP		
183		ID_INDEX_TIPS64				64			Please select new relay number.		Modifiez le numero de relais SVP		
184		ID_INDEX_TIPS65				64			After setting you need to wait		Patientez apres configuration		
185		ID_OA1					16			OA				OA
186		ID_MA1					16			MA				MA
187		ID_CA1					16			CA				CA
188		ID_INDEX_TIPS66				128			After restarting, please clear the browser cache and login again.		Suite redemarrage, initialiez le cache du navigateur		
189		ID_DOWNLOAD_ERROR0			32			Unknown error.								Erreur inconnue.		
190		ID_DOWNLOAD_ERROR1			64			File downloaded successfully.						Rapatriement fichier reussi		
191		ID_DOWNLOAD_ERROR2			64			Failed to download file.							Echec de rapatriement fichier		
192		ID_DOWNLOAD_ERROR3			64			Failed to download, the file is too large.				Echec de téléchargement, fichier trop gros.		
193		ID_DOWNLOAD_ERROR4			64			Failed. No privileges.							Echec. Vous n'avez pas les droits.		
194		ID_DOWNLOAD_ERROR5			64			Controller started successfully.						Demarrage controleur réussi		
195		ID_DOWNLOAD_ERROR6			64			File downloaded successfully.						Rapatriement fichier reussi		
196		ID_DOWNLOAD_ERROR7			64			Failed to download file.							Echec de rapatriement fichier		
197		ID_DOWNLOAD_ERROR8			64			Failed to upload file.							Echec d'envoi de fichier		
198		ID_DOWNLOAD_ERROR9			64			File downloaded successfully.						Rapatriement fichier reussi		
199		ID_DOWNLOAD_ERROR10			64			Failed to upload file. Hardware is protected.				Echec d'envoi de fichier. Cible protégée.		
200		ID_SYSTEM_VOLTAGE			32			Output Voltage			Tension de sortie
201		ID_SYSTEM_CURRENT			32			Output Current			Courant de sortie
202		ID_SYS_STATUS				32			System Status			Etats système	
203		ID_INDEX_TIPS67				64			There was an error on this page.		Erreur sur la page		
204		ID_INDEX_TIPS68				16			Error				Erreur
205		ID_INDEX_TIPS69				16			Line				Ligne		
206		ID_INDEX_TIPS70				64			Click OK to continue.		OK pour continuer		
207		ID_INDEX_TIPS71				64			Request error, please retry.	Echec de requete, essayez a nouveau.	
#//changed by Frank Wu,1/N/27,20140527, for power split
208		ID_INDEX_TIPS72				32				Please select						Selectionnez SVP
209		ID_INDEX_TIPS73				16				NA									NA
210		ID_INDEX_TIPS74				64				Please select equipment.			Selectionnez un equipement SVP
211		ID_INDEX_TIPS75				64				Please select signal type.			Selectionnez le type de signal SVP
212		ID_INDEX_TIPS76				64				Please select signal.				Selectionnez le signal SVP
213		ID_INDEX_TIPS77				64				Full name is too long				Nom complet trop long	
#//changed by Frank Wu,1/N/N,20140716, for loading different language css file, eg. skin.en.css
#//Note:please don't translate the special ID_CSS_LAN
214		ID_CSS_LAN					16				en									fr
215		ID_CON_VOLT				32				Converter Voltage				Tension convert
216		ID_CON_CURR				32				Converter Current				Courant convert
217		ID_INDEX_TIPS78				128				The High 2 Curr Limit Alarm must be larger than the High 1 Curr Limit Alarm				Haut 2 Courant Gravité dalarme doit être supérieure à Haut 1 Courant Gravité dalarme
218		ID_SITE_INFORMATION				32				Site Information			Information du site
219		ID_SITE_NAME				32			Site Name				Nom site
220		ID_SITE_LOCATION				32			Site Location			Localisation site
221		ID_ALARMSOUNDOFF				32			Touch Screen To Silence Alm			Écran Tactile Faire Taire Alm
222		ID_INDEX_TIPS79				128			Time format error or time is beyond the range. The format should be: Day/Month/Year Hour/Minute/Second			Erreur de format ou l'heure est au-delà de la plage. Le format doit être:Jour/Mois/Année Heure / minute / seconde .
223		ID_INDEX_TIPS80				128			Time format error or time is beyond the range. The format should be: Month/Day/Year Hour/Minute/Second			Erreur de format ou l'heure est au-delà de la plage. Le format doit être:Mois/Jour/Année Heure / minute / seconde .
224		ID_INDEX_TIPS81					16				en					fr
[tmp.index.html:Number]
22

[tmp.index.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	STRING				LOCAL
1		ID_SYSTEM				32			Power System			Système d'energie
2		ID_HYBRID				32			Energy Sources			Sources d'energie
3		ID_SOLAR				32			Solar				Solaire
4		ID_SOLAR_RECT				32			Solar Converter			Convertisseur solaire
5		ID_MAINS				32			Mains				Réseau 
6		ID_RECTIFIER				32			Rectifier				Redresseur
7		ID_DG					32			DG					DG
8		ID_CONVERTER				32			Converter				Convertisseur
9		ID_DC					32			DC					DC
10		ID_BATTERY				32			Battery				Batterie
11		ID_SYSTEM_VOLTAGE			32			Output Voltage			Tension de sortie
12		ID_SYSTEM_CURRENT			32			Output Current			Courant de sortie
13		ID_LOAD_TREND				32			Load Trend				Tendance de charge
14		ID_DC1					32			DC					DC
15		ID_AMBIENT_TEMP				32			Ambient Temp			Temperature ambiante
16		ID_PEAK_CURRENT				32			Peak Current			Pointe de courant
17		ID_AVERAGE_CURRENT			32			Average Current			Courant moyenne
18		ID_L1					32			R					R
19		ID_L2					32			S					S
20		ID_L3					32			T					T
21		ID_BATTERY1				32			Battery				Batterie
22		ID_TIPS					64			Data is beyond the normal range.	Donnée hors plage .
23		ID_CONVERTER1				32			Converter				Convertisseur
24		ID_SMIO					16			SMIO				SMIO
25		ID_TIPS2				32			No Sensor				Pas de capteur
26		ID_USER_DEF				32			User Define				Utilisateur défini
27		ID_CONSUM_MAP				32			Consumption Map			Vue consommation
28		ID_SAMPLE				32			Sample Signal			Entree échantillon
29		ID_DISTRI_BAY				32			Distribution Bay		Répartition Bay
30		ID_SOURCE				32			Source			La source
31		ID_SOLAR_RECT1				32			    Solar Converter			Solar Converter	
32		ID_SOLAR2				32			    Solar				Solaire
33		ID_CUSTOMINPUTSTATUS				32			    Custom Input Status				Statut d'entrée personnalisé

[tmp.hybrid.html:Number]
9

[tmp.hybrid.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	STRING				LOCAL
1		ID_MAINS				32			Mains				Réseau
2		ID_SOLAR				32			Solar				Solaire
3		ID_DG					32			DG					DG
4		ID_WIND					32			Wind				Eolien
5		ID_1_WEEK				32			This Week				Cette semaine
6		ID_2_WEEK				32			Last Week				Semaine derniere
7		ID_3_WEEK				32			Two Weeks Ago			Il y a 2 semaines
8		ID_4_WEEK				32			Three Weeks Ago			Il y a 3 semaines
9		ID_NO_DATA				32			No Data				Pas de donnée
10		ID_TIMEFORMAT				32			Date:MM-DD				date:MM-DD

[tmp.system_rectifier.html:Number]
16

[tmp.system_rectifier.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_RECTIFIER				32			Rectifier				Redresseur
2		ID_CONVERTER				32			Converter				Convertisseur
3		ID_SOLAR				32			Solar Converter			Convertisseur solaire
4		ID_VOLTAGE				32			Average Voltage			Tension moyenne
5		ID_CURRENT				32			Total Current			Courant total
6		ID_CAPACITY_USED			32			System Capacity Used		Taux d'utilisation du Système
7		ID_NUM_OF_RECT				32			Number of Rectifiers		Quantité de redresseurs
8		ID_TOTAL_COMM_RECT			64			Total Rectifiers Communicating	Nombre de redresseurs en communication
9		ID_MAX_CAPACITY				32			Max Used Capacity			Taux d'utilisation maxi 
10		ID_SIGNAL				32			Signal				Entree
11		ID_VALUE				32			Value				Valeur
12		ID_SOLAR1				32			Solar Converter				Convertisseur solaire
13		ID_CURRENT1				32			Total Current			Courant total
14		ID_RECTIFIER1				32			GI Rectifier				Redresseur GI
15		ID_RECTIFIER2				32			GII Rectifier				Redresseur GII
16		ID_RECTIFIER3				32			GIII Rectifier			Redresseur GIII
17		ID_RECT_SET				32			Rectifier Settings			Configuration redresseurs
18		ID_BACK					16			Back				Back

[tmp.system_rectifierS1.html:Number]
16

[tmp.system_rectifierS1.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_RECTIFIER				32			Rectifier				Redresseur
2		ID_CONVERTER				32			Converter				Convertisseur
3		ID_SOLAR				32			Solar Converter				Convertisseur solaire
4		ID_VOLTAGE				32			Average Voltage			Tension moyenne
5		ID_CURRENT				32			Total Current			Courant total
6		ID_CAPACITY_USED			32			System Capacity Used		Taux d'utilisation du Système
7		ID_NUM_OF_RECT				32			Number of Rectifiers		Quantité de redresseurs
8		ID_TOTAL_COMM_RECT			64			Number of Rects in communication	Quantité de redresseurs en communication
9		ID_MAX_CAPACITY				32			Max Used Capacity			Taux d'utilisation maxi 
10		ID_SIGNAL				32			Signal				Entree
11		ID_VALUE				32			Value				Valeur
12		ID_SOLAR1				32			Solar Converter			Convertisseur solaire
13		ID_CURRENT1				32			Total Current			Courant total
14		ID_RECTIFIER1				32			GI Rectifier				Redresseur GI
15		ID_RECTIFIER2				32			GII Rectifier				Redresseur GII
16		ID_RECTIFIER3				32			GIII Rectifier			Redresseur GIII
17		ID_RECT_SET				64			Secondary Rectifier Settings		Configuration redresseurs esclave
18		ID_BACK					16			Back				Back


[tmp.system_battery.html:Number]
10

[tmp.system_battery.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_BATTERY				32			Battery				Batterie
2		ID_MANAGE_STATE				32			Battery Management State		Mode de gestion batterie
3		ID_BATT_CURR				32			Total Battery Current		Courant batterie total
4		ID_REMAIN_TIME				32			Estimated Remaining Time		Autonomie restante estimee
5		ID_TEMP					32			Battery Temp			Temperature batterie
6		ID_SIGNAL				32			Signal					Entree
7		ID_VALUE				32			Value				Valeur
8		ID_SIGNAL1				32			Signal				Entree
9		ID_VALUE1				32			Value				Valeur
10		ID_TIPS					32			Back to Battery List		Retour aux types de batteries
11		ID_SIGNAL2				32			Signal				Entree
12		ID_VALUE2				32			Value				Valeur
13		ID_TIP1					64			Capacity Trend Diagram		Courbe de charge
14		ID_TIP2					64			Temperature Trend Diagram		Courbe de temperature
15		ID_TIP3					32			No Sensor		Pas de capteur
16		ID_EIB					16			EIB		EIB


[tmp.system_dc.html:Number]
5

[tmp.system_dc.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_DC					32			DC					DC
2		ID_SIGNAL				32			Signal				Entree
3		ID_VALUE				32			Value				Valeur
4		ID_SIGNAL1				32			Signal				Entree
5		ID_VALUE1				32			Value				Valeur
6		ID_SMDUH				32			SMDUH				SMDUH
7		ID_EIB					32			EIB					EIB
8		ID_DC_METER				32			DC Meter				Mesure DC
9		ID_SMDU					32			SMDU				SMDU
10		ID_SMDUP				32			SMDUP				SMDUP
11		ID_NO_DATA				32			No Data				Pas de donnée
12		ID_SMDUP1				32			SMDUP1				SMDUP1
13		ID_CABINET				32			Cabinet Map				Vue baie
14		ID_NARADA_BMS				32			BMS				BMS
15		ID_SMDUE				32			SMDUE				SMDUE
[tmp.history_alarmlog.html:Number]
17

[tmp.history_alarmlog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_ALARM				32			Alarm History Log			Journal historique alarmes
2		ID_BATT_TEST				32			Battery Test Log			Journal tests batteries
3		ID_EVENT				32			Event Log				Journal bord
4		ID_DATA					32			Data History Log			Journal historique mesures
5		ID_DEVICE				32			Device				Equipement
6		ID_FROM					32			From				Du
7		ID_TO					32			To					Au
8		ID_QUERY				32			Query				Requete
9		ID_UPLOAD				32			Upload				Charger vers
10		ID_TIPS					64			Displays the last 500 entries		Afficher les 500 derniers enregistrements
11		ID_INDEX				32			Index				Index
12		ID_DEVICE1				32			Device Name				Equipement
13		ID_SIGNAL				32			Signal Name				Nom de l'entree
14		ID_LEVEL				32			Alarm Level				Niveau d'alarme
15		ID_START				32			Start Time				Heure de debut
16		ID_END					32			End Time				Heure de fin
17		ID_ALL_DEVICE				32			All Devices				Tous les equipements
#//changed by Frank Wu,1/N/14,20140527, for system log
18		ID_SYSTEMLOG			32				System Log				Logging Système
19		ID_OA					32			OA				OA
20		ID_MA					32			MA				MA
21		ID_CA					32			CA				CA
22		ID_ALARM2				64			Alarm History Log			Journal d'historique des alarmes
23		ID_ALL_DEVICE2				32			    All Devices				Tous les dispositifs
[tmp.history_testlog.html:Number]
900

[tmp.history_testlog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS					64			Choose the last battery test	Selectionner le dernier test batterie
2		ID_SUMMARY_HEAD0			32			Start Time				Heure de debut
3		ID_SUMMARY_HEAD1			32			End Time				Heure de fin
4		ID_SUMMARY_HEAD2			32			Start Reason			Cause du lancement
5		ID_SUMMARY_HEAD3			32			End Reason				Cause de l'arret
6		ID_SUMMARY_HEAD4			32			Test Result			Resultat du test
7		ID_QUERY				32			Query				Requete
8		ID_UPLOAD				32			Upload				Charger vers
9		ID_START_REASON0			64			Start Planned Test			Planning de tests batterie	
10		ID_START_REASON1			64			Start Manual Test			Lancement test manuel	
11		ID_START_REASON2			64			Start AC Fail Test			Lancement test absence reseau	
12		ID_START_REASON3			64			Start Master Battery Test		Lancement test système maitre	
13		ID_START_REASON4			64			Start Test for Other Reasons			Raisons autres	
14		ID_END_REASON0				64			End Test Manually						Arret du test manuellement			
15		ID_END_REASON1				64			End Test for Alarm						Arret du test sur apparition alarme			
16		ID_END_REASON2				64			End Test for Test Time-Out					Arret du test sur temps dépassé		
17		ID_END_REASON3				64			End Test for Capacity Condition				rret du test sur capacite atteinte		
18		ID_END_REASON4				64			End Test for Voltage Condition				Arret du test sur tension atteinte	
19		ID_END_REASON5				64			End Test for AC Fail					Arret du test sur absence secteur		
20		ID_END_REASON6				64			End AC Fail Test for AC Restore				Arret du test absence secteur suite retour secteur		
21		ID_END_REASON7				64			End AC Fail Test for Being Disabled				Arret du test absence secteur suite inhibition			
22		ID_END_REASON8				64			End Master Battery Test					Arret test système maitre			
23		ID_END_REASON9				64			End PowerSplit BT for Auto to Manual		Arret test batterie Power Split suite retour mode manuel		
24		ID_END_REASON10				64			End PowerSplit Man-BT for Manual to Auto	Arret test batterie Power Split suite retour mode automatique			
25		ID_END_REASON11				64			End Test for Other Reasons			Arret pour raisons autres					
26		ID_RESULT0				32			No Result.			Pas de resultat de test
27		ID_RESULT1				16			Battery is OK			Batterie est OK
28		ID_RESULT2				32			Battery is Bad		Batterie en defaut	
29		ID_RESULT3				32			It's PowerSplit Test			Test Power Split
30		ID_RESULT4				16			Other results.			Autre resultat
31		ID_SUMMARY0				32			Index				Index
32		ID_SUMMARY1				32			Record Time				Enregistrement de l'heure
33		ID_SUMMARY2				32			System Voltage			Tension système
34		ID_HEAD0				32			Battery1 Current			Courant batterie 1
35		ID_HEAD1				32			Battery1 Voltage			Tension batterie 1
36		ID_HEAD2				32			Battery1 Capacity			Capacite batterie 1	
37		ID_HEAD3				32			Battery2 Current			Courant batterie 2
38		ID_HEAD4				32			Battery2 Voltage			Tension batterie 2
39		ID_HEAD5				32			Battery2 Capacity			Capacite batterie 2	
40		ID_HEAD6				32			EIB1Battery1 Current		EIB1 Courant batterie 1	
41		ID_HEAD7				32			EIB1Battery1 Voltage		EIB1 Tension batterie 1	
42		ID_HEAD8				32			EIB1Battery1 Capacity		EIB1 Capacite batterie 1
43		ID_HEAD9				32			EIB1Battery2 Current		EIB1 Courant batterie 2	
44		ID_HEAD10				32			EIB1Battery2 Voltage		EIB1 Tension batterie 2
45		ID_HEAD11				32			EIB1Battery2 Capacity		EIB1 Capacite batterie 2	
46		ID_HEAD12				32			EIB2Battery1 Current		EIB2 Courant batterie 1
47		ID_HEAD13				32			EIB2Battery1 Voltage		EIB2 Tension batterie 1
48		ID_HEAD14				32			EIB2Battery1 Capacity		EIB2 Capacite batterie 1
49		ID_HEAD15				32			EIB2Battery2 Current		EIB2 Courant batterie 2	
50		ID_HEAD16				32			EIB2Battery2 Voltage		EIB2 Tension batterie 2	
51		ID_HEAD17				32			EIB2Battery2 Capacity		EIB2 Capacite batterie 2	
52		ID_HEAD18				32			EIB3Battery1 Current		EIB3 Courant batterie 1
53		ID_HEAD19				32			EIB3Battery1 Voltage		EIB3 Tension batterie 1	
54		ID_HEAD20				32			EIB3Battery1 Capacity		EIB3 Capacite batterie 1	
55		ID_HEAD21				32			EIB3Battery2 Current		EIB3 Courant batterie 2
56		ID_HEAD22				32			EIB3Battery2 Voltage		EIB3 Tension batterie 2	
57		ID_HEAD23				32			EIB3Battery2 Capacity		EIB3 Capacite batterie 2	
58		ID_HEAD24				32			EIB4Battery1 Current		EIB4 Courant batterie 1	
59		ID_HEAD25				32			EIB4Battery1 Voltage		EIB4 Tension batterie 1	
60		ID_HEAD26				32			EIB4Battery1 Capacity		EIB4 Capacite batterie 1
61		ID_HEAD27				32			EIB4Battery2 Current		EIB4 Courant batterie 2
62		ID_HEAD28				32			EIB4Battery2 Voltage		EIB4 Tension batterie 2	
63		ID_HEAD29				32			EIB4Battery2 Capacity		EIB4 Capacite batterie 2
64		ID_HEAD30				32			SMDU1Battery1 Current		SMDU1 Courant batterie 1		
65		ID_HEAD31				32			SMDU1Battery1 Voltage		SMDU1 Tension batterie 1		
66		ID_HEAD32				32			SMDU1Battery1 Capacity	SMDU1 Capacite batterie 1		
67		ID_HEAD33				32			SMDU1Battery2 Current		SMDU1 Courant batterie 2		
68		ID_HEAD34				32			SMDU1Battery2 Voltage		SMDU1 Tension batterie 2			
69		ID_HEAD35				32			SMDU1Battery2 Capacity	SMDU1 Capacite batterie 2		
70		ID_HEAD36				32			SMDU1Battery3 Current		SMDU1 Courant batterie 3		
71		ID_HEAD37				32			SMDU1Battery3 Voltage		SMDU1 Tension batterie 3			
72		ID_HEAD38				32			SMDU1Battery3 Capacity	SMDU1 Capacite batterie 3		
73		ID_HEAD39				32			SMDU1Battery4 Current		SMDU1 Courant batterie 4		
74		ID_HEAD40				32			SMDU1Battery4 Voltage		SMDU1 Tension batterie 4			
75		ID_HEAD41				32			SMDU1Battery4 Capacity	SMDU1 Capacite batterie 4		
76		ID_HEAD42				32			SMDU2Battery1 Current		SMDU2 Courant batterie 1		
77		ID_HEAD43				32			SMDU2Battery1 Voltage		SMDU2 Tension batterie 1		
78		ID_HEAD44				32			SMDU2Battery1 Capacity	SMDU2 Capacite batterie 1		
79		ID_HEAD45				32			SMDU2Battery2 Current		SMDU2 Courant batterie 2		
80		ID_HEAD46				32			SMDU2Battery2 Voltage		SMDU2 Tension batterie 1		
81		ID_HEAD47				32			SMDU2Battery2 Capacity	SMDU2 Capacite batterie 1		
82		ID_HEAD48				32			SMDU2Battery3 Current		SMDU2 Courant batterie 3		
83		ID_HEAD49				32			SMDU2Battery3 Voltage		SMDU2 Tension batterie 3	
84		ID_HEAD50				32			SMDU2Battery3 Capacity	SMDU2 Capacite batterie 3		
85		ID_HEAD51				32			SMDU2Battery4 Current		SMDU2 Courant batterie 4		
86		ID_HEAD52				32			SMDU2Battery4 Voltage		SMDU2 Tension batterie 4		
87		ID_HEAD53				32			SMDU2Battery4 Capacity	SMDU2 Capacite batterie 4		
88		ID_HEAD54				32			SMDU3Battery1 Current		SMDU3 Courant batterie 1		
89		ID_HEAD55				32			SMDU3Battery1 Voltage		SMDU3 Tension batterie 1		
90		ID_HEAD56				32			SMDU3Battery1 Capacity	SMDU3 Capacite batterie 1			
91		ID_HEAD57				32			SMDU3Battery2 Current		SMDU3 Courant batterie 2		
92		ID_HEAD58				32			SMDU3Battery2 Voltage		SMDU3 Tension batterie 2		
93		ID_HEAD59				32			SMDU3Battery2 Capacity	SMDU3 Capacite batterie 2			
94		ID_HEAD60				32			SMDU3Battery3 Current		SMDU3 Courant batterie 3		
95		ID_HEAD61				32			SMDU3Battery3 Voltage		SMDU3 Tension batterie 3		
96		ID_HEAD62				32			SMDU3Battery3 Capacity	SMDU3 Capacite batterie 3			
97		ID_HEAD63				32			SMDU3Battery4 Current		SMDU3 Courant batterie 4		
98		ID_HEAD64				32			SMDU3Battery4 Voltage		SMDU3 Tension batterie 4		
99		ID_HEAD65				32			SMDU3Battery4 Capacity	SMDU3 Capacite batterie 4			
100		ID_HEAD66				32			SMDU4Battery1 Current		SMDU4 Courant batterie 1		
101		ID_HEAD67				32			SMDU4Battery1 Voltage		SMDU4 Tension batterie 1		
102		ID_HEAD68				32			SMDU4Battery1 Capacity	SMDU4 Capacite batterie 1			
103		ID_HEAD69				32			SMDU4Battery2 Current		SMDU4 Courant batterie 2		
104		ID_HEAD70				32			SMDU4Battery2 Voltage		SMDU4 Tension batterie 2		
105		ID_HEAD71				32			SMDU4Battery2 Capacity	SMDU4 Capacite batterie 2			
106		ID_HEAD72				32			SMDU4Battery3 Current		SMDU4 Courant batterie 3		
107		ID_HEAD73				32			SMDU4Battery3 Voltage		SMDU4 Tension batterie 3		
108		ID_HEAD74				32			SMDU4Battery3 Capacity	SMDU4 Capacite batterie 3		
109		ID_HEAD75				32			SMDU4Battery4 Current		SMDU4 Courant batterie 4		
110		ID_HEAD76				32			SMDU4Battery4 Voltage		SMDU4 Tension batterie 4		
111		ID_HEAD77				32			SMDU4Battery4 Capacity	SMDU4 Capacite batterie 4			
112		ID_HEAD78				32			SMDU5Battery1 Current		SMDU5 Courant batterie 1		
113		ID_HEAD79				32			SMDU5Battery1 Voltage		SMDU5 Tension batterie 1		
114		ID_HEAD80				32			SMDU5Battery1 Capacity	SMDU5 Capacite batterie 1			
115		ID_HEAD81				32			SMDU5Battery2 Current		SMDU5 Courant batterie 2		
116		ID_HEAD82				32			SMDU5Battery2 Voltage		SMDU5 Tension batterie 2		
117		ID_HEAD83				32			SMDU5Battery2 Capacity	SMDU5 Capacite batterie 2		
118		ID_HEAD84				32			SMDU5Battery3 Current		SMDU5 Courant batterie 3		
119		ID_HEAD85				32			SMDU5Battery3 Voltage		SMDU5 Tension batterie 3		
120		ID_HEAD86				32			SMDU5Battery3 Capacity	SMDU5 Capacite batterie 3			
121		ID_HEAD87				32			SMDU5Battery4 Current		SMDU5 Courant batterie 4		
122		ID_HEAD88				32			SMDU5Battery4 Voltage		SMDU5 Tension batterie 4		
123		ID_HEAD89				32			SMDU5Battery4 Capacity	SMDU5 Capacite batterie 4			
124		ID_HEAD90				32			SMDU6Battery1 Current		SMDU6 Courant batterie 1		
125		ID_HEAD91				32			SMDU6Battery1 Voltage		SMDU6 Tension batterie 1	
126		ID_HEAD92				32			SMDU6Battery1 Capacity	SMDU6 Capacite batterie 1		
127		ID_HEAD93				32			SMDU6Battery2 Current		SMDU6 Courant batterie 2		
128		ID_HEAD94				32			SMDU6Battery2 Voltage		SMDU6 Tension batterie 2		
129		ID_HEAD95				32			SMDU6Battery2 Capacity	SMDU6 Capacite batterie 2			
130		ID_HEAD96				32			SMDU6Battery3 Current		SMDU6 Courant batterie 3		
131		ID_HEAD97				32			SMDU6Battery3 Voltage		SMDU6 Tension batterie 3		
132		ID_HEAD98				32			SMDU6Battery3 Capacity	SMDU6 Capacite batterie 3			
133		ID_HEAD99				32			SMDU6Battery4 Current		SMDU6 Courant batterie 4		
134		ID_HEAD100				32			SMDU6Battery4 Voltage	SMDU6 Tension batterie 4		
135		ID_HEAD101				32			SMDU6Battery4 Capacity	SMDU6 Capacite batterie 4			
136		ID_HEAD102				32			SMDU7Battery1 Current		SMDU7 Courant batterie 1		
137		ID_HEAD103				32			SMDU7Battery1 Voltage		SMDU7 Tension batterie 1		
138		ID_HEAD104				32			SMDU7Battery1 Capacity	SMDU7 Capacite batterie 1			
139		ID_HEAD105				32			SMDU7Battery2 Current		SMDU7 Courant batterie 2		
140		ID_HEAD106				32			SMDU7Battery2 Voltage		SMDU7 Tension batterie 2		
141		ID_HEAD107				32			SMDU7Battery2 Capacity	SMDU7 Capacite batterie 2		
142		ID_HEAD108				32			SMDU7Battery3 Current		SMDU7 Courant batterie 3		
143		ID_HEAD109				32			SMDU7Battery3 Voltage		SMDU7 Tension batterie 3		
144		ID_HEAD110				32			SMDU7Battery3 Capacity	SMDU7 Capacite batterie 3		
145		ID_HEAD111				32			SMDU7Battery4 Current		SMDU7 Courant batterie 4		
146		ID_HEAD112				32			SMDU7Battery4 Voltage		SMDU7 Tension batterie 4		
147		ID_HEAD113				32			SMDU7Battery4 Capacity	SMDU7 Capacite batterie 4			
148		ID_HEAD114				32			SMDU8Battery1 Current		SMDU8 Courant batterie 1		
149		ID_HEAD115				32			SMDU8Battery1 Voltage		SMDU8 Tension batterie 1		
150		ID_HEAD116				32			SMDU8Battery1 Capacity	SMDU8 Capacite batterie 1		
151		ID_HEAD117				32			SMDU8Battery2 Current		SMDU8 Courant batterie 2		
152		ID_HEAD118				32			SMDU8Battery2 Voltage		SMDU8 Tension batterie 2		
153		ID_HEAD119				32			SMDU8Battery2 Capacity	SMDU8 Capacite batterie 2			
154		ID_HEAD120				32			SMDU8Battery3 Current		SMDU8 Courant batterie 3		
155		ID_HEAD121				32			SMDU8Battery3 Voltage		SMDU8 Tension batterie 3		
156		ID_HEAD122				32			SMDU8Battery3 Capacity	SMDU8 Capacite batterie 3			
157		ID_HEAD123				32			SMDU8Battery4 Current		SMDU8 Courant batterie 4		
158		ID_HEAD124				32			SMDU8Battery4 Voltage		SMDU8 Tension batterie 4	
159		ID_HEAD125				32			SMDU8Battery4 Capacity	SMDU8 Capacite batterie 4			
160		ID_HEAD126				32			EIB1Battery3 Current		EIB1 Courant batterie 3		
161		ID_HEAD127				32			EIB1Battery3 Voltage		EIB1 Tension batterie 3		
162		ID_HEAD128				32			EIB1Battery3 Capacity		EIB1 Capacite batterie 3		
163		ID_HEAD129				32			EIB2Battery3 Current		EIB2 Courant batterie 3		
164		ID_HEAD130				32			EIB2Battery3 Voltage		EIB2 Tension batterie 3		
165		ID_HEAD131				32			EIB2Battery3 Capacity		EIB2 Capacite batterie 3	
166		ID_HEAD132				32			EIB3Battery3 Current		EIB3 Courant batterie 3		
167		ID_HEAD133				32			EIB3Battery3 Voltage		EIB3 Tension batterie 3		
168		ID_HEAD134				32			EIB3Battery3 Capacity		EIB3 Capacite batterie 3	
169		ID_HEAD135				32			EIB4Battery3 Current		EIB4 Courant batterie 3		
170		ID_HEAD136				32			EIB4Battery3 Voltage		EIB4 Tension batterie 3		
171		ID_HEAD137				32			EIB4Battery3 Capacity		EIB4 Capacite batterie 3	
172		ID_HEAD138				32			EIB1Block1Voltage			EIB1 Tension bloc 1		
173		ID_HEAD139				32			EIB1Block2Voltage			EIB1 Tension bloc 2	
174		ID_HEAD140				32			EIB1Block3Voltage			EIB1 Tension bloc 3	
175		ID_HEAD141				32			EIB1Block4Voltage			EIB1 Tension bloc 4		
176		ID_HEAD142				32			EIB1Block5Voltage			EIB1 Tension bloc 5	
177		ID_HEAD143				32			EIB1Block6Voltage			EIB1 Tension bloc 6	
178		ID_HEAD144				32			EIB1Block7Voltage			EIB1 Tension bloc 7		
179		ID_HEAD145				32			EIB1Block8Voltage			EIB1 Tension bloc 8		
180		ID_HEAD146				32			EIB2Block1Voltage			EIB2 Tension bloc 1		
181		ID_HEAD147				32			EIB2Block2Voltage			EIB2 Tension bloc 2	
182		ID_HEAD148				32			EIB2Block3Voltage			EIB2 Tension bloc 3	
183		ID_HEAD149				32			EIB2Block4Voltage			EIB2 Tension bloc 4		
184		ID_HEAD150				32			EIB2Block5Voltage			EIB2 Tension bloc 5	
185		ID_HEAD151				32			EIB2Block6Voltage			EIB2 Tension bloc 6	
186		ID_HEAD152				32			EIB2Block7Voltage			EIB2 Tension bloc 7	
187		ID_HEAD153				32			EIB2Block8Voltage			EIB2 Tension bloc 8	
188		ID_HEAD154				32			EIB3Block1Voltage			EIB3 Tension bloc 1		
189		ID_HEAD155				32			EIB3Block2Voltage			EIB3 Tension bloc 2		
190		ID_HEAD156				32			EIB3Block3Voltage			EIB3 Tension bloc 3	
191		ID_HEAD157				32			EIB3Block4Voltage			EIB3 Tension bloc 4	
192		ID_HEAD158				32			EIB3Block5Voltage			EIB3 Tension bloc 5	
193		ID_HEAD159				32			EIB3Block6Voltage			EIB3 Tension bloc 6	
194		ID_HEAD160				32			EIB3Block7Voltage			EIB3 Tension bloc 7		
195		ID_HEAD161				32			EIB3Block8Voltage			EIB3 Tension bloc 8	
196		ID_HEAD162				32			EIB4Block1Voltage			EIB4 Tension bloc 1		
197		ID_HEAD163				32			EIB4Block2Voltage			EIB4 Tension bloc 2			
198		ID_HEAD164				32			EIB4Block3Voltage			EIB4 Tension bloc 3			
199		ID_HEAD165				32			EIB4Block4Voltage			EIB4 Tension bloc 4			
200		ID_HEAD166				32			EIB4Block5Voltage			EIB4 Tension bloc 5			
201		ID_HEAD167				32			EIB4Block6Voltage			EIB4 Tension bloc 6			
202		ID_HEAD168				32			EIB4Block7Voltage			EIB4 Tension bloc 7			
203		ID_HEAD169				32			EIB4Block8Voltage			EIB4 Tension bloc 8			
204		ID_HEAD170				32			Temperature1			Temperature 1			
205		ID_HEAD171				32			Temperature2			Temperature 2			
206		ID_HEAD172				32			Temperature3			Temperature 3			
207		ID_HEAD173				32			Temperature4			Temperature 4			
208		ID_HEAD174				32			Temperature5			Temperature 5			
209		ID_HEAD175				32			Temperature6			Temperature 6			
210		ID_HEAD176				32			Temperature7			Temperature 7		
211		ID_HEAD177				32			Battery1 Current			Courant batterie 1		
212		ID_HEAD178				32			Battery1 Voltage			Tension batterie 1		
213		ID_HEAD179				32			Battery1 Capacity			Capacite batterie 1		
214		ID_HEAD180				32			LargeDUBattery1 Current		LargeDU Courant Batterie1	
215		ID_HEAD181				32			LargeDUBattery1 Voltage		LargeDU Tension Batterie1	
216		ID_HEAD182				32			LargeDUBattery1 Capacity	LargeDU Capacite Batterie1	
217		ID_HEAD183				32			LargeDUBattery2 Current		LargeDU Courant Batterie2		
218		ID_HEAD184				32			LargeDUBattery2 Voltage		LargeDU Tension Batterie2		
219		ID_HEAD185				32			LargeDUBattery2 Capacity	LargeDU Capacite Batterie2	
220		ID_HEAD186				32			LargeDUBattery3 Current		LargeDU Courant Batterie3		
221		ID_HEAD187				32			LargeDUBattery3 Voltage		LargeDU Tension Batterie3		
222		ID_HEAD188				32			LargeDUBattery3 Capacity	LargeDU Capacite Batterie3	
223		ID_HEAD189				32			LargeDUBattery4 Current		LargeDU Courant Batterie4		
224		ID_HEAD190				32			LargeDUBattery4 Voltage		LargeDU Tension Batterie4	
225		ID_HEAD191				32			LargeDUBattery4 Capacity	LargeDU Capacite Batterie4	
226		ID_HEAD192				32			LargeDUBattery5 Current		LargeDU Courant Batterie5		
227		ID_HEAD193				32			LargeDUBattery5 Voltage		LargeDU Tension Batterie5		
228		ID_HEAD194				32			LargeDUBattery5 Capacity	LargeDU Capacite Batterie5	
229		ID_HEAD195				32			LargeDUBattery6 Current		LargeDU Courant Batterie6	
230		ID_HEAD196				32			LargeDUBattery6 Voltage		LargeDU Tension Batterie6		
231		ID_HEAD197				32			LargeDUBattery6 Capacity	LargeDU Capacite Batterie6
232		ID_HEAD198				32			LargeDUBattery7 Current		LargeDU Courant Batterie7		
233		ID_HEAD199				32			LargeDUBattery7 Voltage		LargeDU Tension Batterie7		
234		ID_HEAD200				32			LargeDUBattery7 Capacity	LargeDU Capacite Batterie7	
235		ID_HEAD201				32			LargeDUBattery8 Current		LargeDU Courant Batterie8		
236		ID_HEAD202				32			LargeDUBattery8 Voltage		LargeDU Tension Batterie8		
237		ID_HEAD203				32			LargeDUBattery8 Capacity	LargeDU Capacite Batterie8	
238		ID_HEAD204				32			LargeDUBattery9 Current		LargeDU Courant Batterie9		
239		ID_HEAD205				32			LargeDUBattery9 Voltage		LargeDU Tension Batterie9		
240		ID_HEAD206				32			LargeDUBattery9 Capacity	LargeDU Capacite Batterie9	
241		ID_HEAD207				32			LargeDUBattery10 Current		LargeDU Courant Batterie10		
242		ID_HEAD208				32			LargeDUBattery10 Voltage		LargeDU Tension Batterie10		
243		ID_HEAD209				32			LargeDUBattery10 Capacity	LargeDU Capacite Batterie10
244		ID_HEAD210				32			LargeDUBattery11 Current		LargeDU Courant Batterie11		
245		ID_HEAD211				32			LargeDUBattery11 Voltage		LargeDU Tension Batterie11		
246		ID_HEAD212				32			LargeDUBattery11 Capacity		LargeDU Capacite Batterie11
247		ID_HEAD213				32			LargeDUBattery12 Current		LargeDU Courant Batterie12		
248		ID_HEAD214				32			LargeDUBattery12 Voltage		LargeDU Tension Batterie12	
249		ID_HEAD215				32			LargeDUBattery12 Capacity		LargeDU Capacite Batterie12	
250		ID_HEAD216				32			LargeDUBattery13 Current		LargeDU Courant Batterie13		
251		ID_HEAD217				32			LargeDUBattery13 Voltage		LargeDU Tension Batterie13		
252		ID_HEAD218				32			LargeDUBattery13 Capacity		LargeDU Capacite Batterie13	
253		ID_HEAD219				32			LargeDUBattery14 Current		LargeDU Courant Batterie14		
254		ID_HEAD220				32			LargeDUBattery14 Voltage		LargeDU Tension Batterie14	
255		ID_HEAD221				32			LargeDUBattery14 Capacity		LargeDU Capacite Batterie14
256		ID_HEAD222				32			LargeDUBattery15 Current		LargeDU Courant Batterie15	
257		ID_HEAD223				32			LargeDUBattery15 Voltage		LargeDU Tension Batterie15	
258		ID_HEAD224				32			LargeDUBattery15 Capacity		LargeDU Capacite Batterie15
259		ID_HEAD225				32			LargeDUBattery16 Current		LargeDU Courant Batterie16		
260		ID_HEAD226				32			LargeDUBattery16 Voltage		LargeDU Tension Batterie16		
261		ID_HEAD227				32			LargeDUBattery16 Capacity		LargeDU Capacite Batterie16
262		ID_HEAD228				32			LargeDUBattery17 Current		LargeDU Courant Batterie17	
263		ID_HEAD229				32			LargeDUBattery17 Voltage		LargeDU Tension Batterie17		
264		ID_HEAD230				32			LargeDUBattery17 Capacity		LargeDU Capacite Batterie17
265		ID_HEAD231				32			LargeDUBattery18 Current		LargeDU Courant Batterie18		
266		ID_HEAD232				32			LargeDUBattery18 Voltage		LargeDU Tension Batterie18		
267		ID_HEAD233				32			LargeDUBattery18 Capacity		LargeDU Capacite Batterie18
268		ID_HEAD234				32			LargeDUBattery19 Current		LargeDU Courant Batterie19		
269		ID_HEAD235				32			LargeDUBattery19 Voltage		LargeDU Tension Batterie19		
270		ID_HEAD236				32			LargeDUBattery19 Capacity		LargeDU Capacite Batterie19	
271		ID_HEAD237				32			LargeDUBattery20 Current		LargeDU Courant Batterie20		
272		ID_HEAD238				32			LargeDUBattery20 Voltage		LargeDU Tension Batterie20	
273		ID_HEAD239				32			LargeDUBattery20 Capacity		LargeDU Capacite Batterie20
274		ID_HEAD240				32			Temperature8			Temperature8			
275		ID_HEAD241				32			Temperature9			Temperature9			
276		ID_HEAD242				32			Temperature10			Temperature10			
277		ID_HEAD243				32			SMBattery1 Current			SMBAT Courant Batt1		
278		ID_HEAD244				32			SMBattery1 Voltage			SMBAT Tension Batt1		
279		ID_HEAD245				32			SMBattery1 Capacity			SMBAT Capacite Batt1		
280		ID_HEAD246				32			SMBattery2 Current			SMBAT Courant Batt2			
281		ID_HEAD247				32			SMBattery2 Voltage			SMBAT Tension Batt2		
282		ID_HEAD248				32			SMBattery2 Capacity			SMBAT Capacite Batt2		
283		ID_HEAD249				32			SMBattery3 Current			SMBAT Courant Batt3			
284		ID_HEAD250				32			SMBattery3 Voltage			SMBAT Tension Batt3		
285		ID_HEAD251				32			SMBattery3 Capacity			SMBAT Capacite Batt3		
286		ID_HEAD252				32			SMBattery4 Current			SMBAT Courant Batt4		
287		ID_HEAD253				32			SMBattery4 Voltage			SMBAT Tension Batt4		
288		ID_HEAD254				32			SMBattery4 Capacity			SMBAT Capacite Batt4		
289		ID_HEAD255				32			SMBattery5 Current			SMBAT Courant Batt5			
290		ID_HEAD256				32			SMBattery5 Voltage			SMBAT Tension Batt5		
291		ID_HEAD257				32			SMBattery5 Capacity			SMBAT Capacite Batt5		
292		ID_HEAD258				32			SMBattery6 Current			SMBAT Courant Batt6			
293		ID_HEAD259				32			SMBattery6 Voltage			SMBAT Tension Batt6		
294		ID_HEAD260				32			SMBattery6 Capacity			SMBAT Capacite Batt6		
295		ID_HEAD261				32			SMBattery7 Current			SMBAT Courant Batt7			
296		ID_HEAD262				32			SMBattery7 Voltage			SMBAT Tension Batt7	
297		ID_HEAD263				32			SMBattery7 Capacity			SMBAT Capacite Batt7		
298		ID_HEAD264				32			SMBattery8 Current			SMBAT Courant Batt8			
299		ID_HEAD265				32			SMBattery8 Voltage			SMBAT Tension Batt8		
300		ID_HEAD266				32			SMBattery8 Capacity			SMBAT Capacite Batt8	
301		ID_HEAD267				32			SMBattery9 Current			SMBAT Courant Batt9			
302		ID_HEAD268				32			SMBattery9 Voltage			SMBAT Tension Batt9		
303		ID_HEAD269				32			SMBattery9 Capacity			SMBAT Capacite Batt9		
304		ID_HEAD270				32			SMBattery10 Current			SMBAT Courant Batt10		
305		ID_HEAD271				32			SMBattery10 Voltage			SMBAT Tension Batt10	
306		ID_HEAD272				32			SMBattery10 Capacity		SMBAT Capacite Batt10	
307		ID_HEAD273				32			SMBattery11 Current			SMBAT Courant Batt11		
308		ID_HEAD274				32			SMBattery11 Voltage			SMBAT Tension Batt11	
309		ID_HEAD275				32			SMBattery11 Capacity		SMBAT Capacite Batt11	
310		ID_HEAD276				32			SMBattery12 Current			SMBAT Courant Batt12		
311		ID_HEAD277				32			SMBattery12 Voltage			SMBAT Tension Batt12	
312		ID_HEAD278				32			SMBattery12 Capacity		SMBAT Capacite Batt12	
313		ID_HEAD279				32			SMBattery13 Current			SMBAT Courant Batt13		
314		ID_HEAD280				32			SMBattery13 Voltage			SMBAT Tension Batt13	
315		ID_HEAD281				32			SMBattery13 Capacity		SMBAT Capacite Batt13	
316		ID_HEAD282				32			SMBattery14 Current			SMBAT Courant Batt14		
317		ID_HEAD283				32			SMBattery14 Voltage			SMBAT Tension Batt14	
318		ID_HEAD284				32			SMBattery14 Capacity		SMBAT Capacite Batt14	
319		ID_HEAD285				32			SMBattery15 Current			SMBAT Courant Batt15		
320		ID_HEAD286				32			SMBattery15 Voltage			SMBAT Tension Batt15	
321		ID_HEAD287				32			SMBattery15 Capacity		SMBAT Capacite Batt15	
322		ID_HEAD288				32			SMBattery16 Current			SMBAT Courant Batt16		
323		ID_HEAD289				32			SMBattery16 Voltage			SMBAT Tension Batt16	
324		ID_HEAD290				32			SMBattery16 Capacity		SMBAT Capacite Batt16	
325		ID_HEAD291				32			SMBattery17 Current			SMBAT Courant Batt17		
326		ID_HEAD292				32			SMBattery17 Voltage			SMBAT Tension Batt17	
327		ID_HEAD293				32			SMBattery17 Capacity		SMBAT Capacite Batt17	
328		ID_HEAD294				32			SMBattery18 Current			SMBAT Courant Batt18		
329		ID_HEAD295				32			SMBattery18 Voltage			SMBAT Tension Batt18	
330		ID_HEAD296				32			SMBattery18 Capacity		SMBAT Capacite Batt18	
331		ID_HEAD297				32			SMBattery19 Current			SMBAT Courant Batt19		
332		ID_HEAD298				32			SMBattery19 Voltage			SMBAT Tension Batt19	
333		ID_HEAD299				32			SMBattery19 Capacity		SMBAT Capacite Batt19	
334		ID_HEAD300				32			SMBattery20 Current			SMBAT Courant Batt20		
335		ID_HEAD301				32			SMBattery20 Voltage			SMBAT Tension Batt20	
336		ID_HEAD302				32			SMBattery20 Capacity		SMBAT Capacite Batt20	
337		ID_HEAD303				32			SMDU1Battery5 Current		SMDU1 Courant Batterie5	
338		ID_HEAD304				32			SMDU1Battery5 Voltage		SMDU1 Tension Batterie5	
339		ID_HEAD305				32			SMDU1Battery5 Capacity		SMDU1 Capacite Batterie5	
340		ID_HEAD306				32			SMDU2Battery5 Current		SMDU2 Courant Batterie5
341		ID_HEAD307				32			SMDU2Battery5 Voltage		SMDU2 Tension Batterie5	
342		ID_HEAD308				32			SMDU2Battery5 Capacity		SMDU2 Capacite Batterie5	
343		ID_HEAD309				32			SMDU3Battery5 Current		SMDU3 Courant Batterie5
344		ID_HEAD310				32			SMDU3Battery5 Voltage		SMDU3 Tension Batterie5	
345		ID_HEAD311				32			SMDU3Battery5 Capacity		SMDU3 Capacite Batterie5	
346		ID_HEAD312				32			SMDU4Battery5 Current		SMDU4 Courant Batterie5	
347		ID_HEAD313				32			SMDU4Battery5 Voltage		SMDU4 Tension Batterie5	
348		ID_HEAD314				32			SMDU4Battery5 Capacity		SMDU4 Capacite Batterie5	
349		ID_HEAD315				32			SMDU5Battery5 Current		SMDU5 Courant Batterie5	
350		ID_HEAD316				32			SMDU5Battery5 Voltage		SMDU5 Tension Batterie5	
351		ID_HEAD317				32			SMDU5Battery5 Capacity		SMDU5 Capacite Batterie5	
352		ID_HEAD318				32			SMDU6Battery5 Current		SMDU6 Courant Batterie5	
353		ID_HEAD319				32			SMDU6Battery5 Voltage		SMDU6 Tension Batterie5	
354		ID_HEAD320				32			SMDU6Battery5 Capacity		SMDU6 Capacite Batterie5	
355		ID_HEAD321				32			SMDU7Battery5 Current		SMDU7 Courant Batterie5	
356		ID_HEAD322				32			SMDU7Battery5 Voltage		SMDU7 Tension Batterie5
357		ID_HEAD323				32			SMDU7Battery5 Capacity		SMDU7 Capacite Batterie5	
358		ID_HEAD324				32			SMDU8Battery5 Current		SMDU8 Courant Batterie5	
359		ID_HEAD325				32			SMDU8Battery5 Voltage		SMDU8 Tension Batterie5
360		ID_HEAD326				32			SMDU8Battery5 Capacity		SMDU8 Capacite Batterie5	
361		ID_HEAD327				32			SMBRCBattery1 Current		SMBRC Courant Batterie1	
362		ID_HEAD328				32			SMBRCBattery1 Voltage		SMBRC Tension Batterie1	
363		ID_HEAD329				32			SMBRCBattery1 Capacity		SMBRC Capacite Batterie1	
364		ID_HEAD330				32			SMBRCBattery2 Current		SMBRC Courant Batterie2	
365		ID_HEAD331				32			SMBRCBattery2 Voltage		SMBRC Tension Batterie2	
366		ID_HEAD332				32			SMBRCBattery2 Capacity		SMBRC Capacite Batterie2	
367		ID_HEAD333				32			SMBRCBattery3 Current		SMBRC Courant Batterie3	
368		ID_HEAD334				32			SMBRCBattery3 Voltage		SMBRC Tension Batterie3	
369		ID_HEAD335				32			SMBRCBattery3 Capacity		SMBRC Capacite Batterie3	
370		ID_HEAD336				32			SMBRCBattery4 Current		SMBRC Courant Batterie4	
371		ID_HEAD337				32			SMBRCBattery4 Voltage		SMBRC Tension Batterie4	
372		ID_HEAD338				32			SMBRCBattery4 Capacity		SMBRC Capacite Batterie4	
373		ID_HEAD339				32			SMBRCBattery5 Current		SMBRC Courant Batterie5	
374		ID_HEAD340				32			SMBRCBattery5 Voltage		SMBRC Tension Batterie5	
375		ID_HEAD341				32			SMBRCBattery5 Capacity		SMBRC Capacite Batterie5	
376		ID_HEAD342				32			SMBRCBattery6 Current		SMBRC Courant Batterie6	
377		ID_HEAD343				32			SMBRCBattery6 Voltage		SMBRC Tension Batterie6	
378		ID_HEAD344				32			SMBRCBattery6 Capacity		SMBRC Capacite Batterie6	
379		ID_HEAD345				32			SMBRCBattery7 Current		SMBRC Courant Batterie7	
380		ID_HEAD346				32			SMBRCBattery7 Voltage		SMBRC Tension Batterie7	
381		ID_HEAD347				32			SMBRCBattery7 Capacity		SMBRC Capacite Batterie7	
382		ID_HEAD348				32			SMBRCBattery8 Current		SMBRC Courant Batterie8	
383		ID_HEAD349				32			SMBRCBattery8 Voltage		SMBRC Tension Batterie8	
384		ID_HEAD350				32			SMBRCBattery8 Capacity		SMBRC Capacite Batterie8	
385		ID_HEAD351				32			SMBRCBattery9 Current		SMBRC Courant Batterie9	
386		ID_HEAD352				32			SMBRCBattery9 Voltage		SMBRC Tension Batterie9	
387		ID_HEAD353				32			SMBRCBattery9 Capacity		SMBRC Capacite Batterie9	
388		ID_HEAD354				32			SMBRCBattery10 Current		SMBRC Courant Batterie10
389		ID_HEAD355				32			SMBRCBattery10 Voltage		SMBRC Tension Batterie10
390		ID_HEAD356				32			SMBRCBattery10 Capacity		SMBRC Capacite Batterie10	
391		ID_HEAD357				32			SMBRCBattery11 Current		SMBRC Courant Batterie11	
392		ID_HEAD358				32			SMBRCBattery11 Voltage		SMBRC Tension Batterie11
393		ID_HEAD359				32			SMBRCBattery11 Capacity		SMBRC Capacite Batterie11
394		ID_HEAD360				32			SMBRCBattery12 Current		SMBRC Courant Batterie12	
395		ID_HEAD361				32			SMBRCBattery12 Voltage		SMBRC Tension Batterie12	
396		ID_HEAD362				32			SMBRCBattery12 Capacity		SMBRC Capacite Batterie12
397		ID_HEAD363				32			SMBRCBattery13 Current		SMBRC Courant Batterie13	
398		ID_HEAD364				32			SMBRCBattery13 Voltage		SMBRC Tension Batterie13	
399		ID_HEAD365				32			SMBRCBattery13 Capacity		SMBRC Capacite Batterie13	
400		ID_HEAD366				32			SMBRCBattery14 Current		SMBRC Courant Batterie14	
401		ID_HEAD367				32			SMBRCBattery14 Voltage		SMBRC Tension Batterie14	
402		ID_HEAD368				32			SMBRCBattery14 Capacity		SMBRC Capacite Batterie14	
403		ID_HEAD369				32			SMBRCBattery15 Current		SMBRC Courant Batterie15
404		ID_HEAD370				32			SMBRCBattery15 Voltage		SMBRC Tension Batterie15
405		ID_HEAD371				32			SMBRCBattery15 Capacity		SMBRC Capacite Batterie15	
406		ID_HEAD372				32			SMBRCBattery16 Current		SMBRC Courant Batterie16
407		ID_HEAD373				32			SMBRCBattery16 Voltage		SMBRC Tension Batterie16
408		ID_HEAD374				32			SMBRCBattery16 Capacity		SMBRC Capacite Batterie16	
409		ID_HEAD375				32			SMBRCBattery17 Current		SMBRC Courant Batterie17	
410		ID_HEAD376				32			SMBRCBattery17 Voltage		SMBRC Tension Batterie17	
411		ID_HEAD377				32			SMBRCBattery17 Capacity		SMBRC Capacite Batterie17	
412		ID_HEAD378				32			SMBRCBattery18 Current		SMBRC Courant Batterie18	
413		ID_HEAD379				32			SMBRCBattery18 Voltage		SMBRC Tension Batterie18	
414		ID_HEAD380				32			SMBRCBattery18 Capacity		SMBRC Capacite Batterie18	
415		ID_HEAD381				32			SMBRCBattery19 Current		SMBRC Courant Batterie19	
416		ID_HEAD382				32			SMBRCBattery19 Voltage		SMBRC Tension Batterie19	
417		ID_HEAD383				32			SMBRCBattery19 Capacity		SMBRC Capacite Batterie19	
418		ID_HEAD384				32			SMBRCBattery20 Current		SMBRC Courant Batterie20
419		ID_HEAD385				32			SMBRCBattery20 Voltage		SMBRC Tension Batterie20
420		ID_HEAD386				32			SMBRCBattery20 Capacity		SMBRC Capacite Batterie20
421		ID_HEAD387				32			SMBAT/BRC1 BLOCK1  Voltage		SMBAT/BRC1Tension Bloc1	
422		ID_HEAD388				32			SMBAT/BRC1 BLOCK2  Voltage		SMBAT/BRC1Tension Bloc2	
423		ID_HEAD389				32			SMBAT/BRC1 BLOCK3  Voltage		SMBAT/BRC1Tension Bloc3	
424		ID_HEAD390				32			SMBAT/BRC1 BLOCK4  Voltage		SMBAT/BRC1Tension Bloc4	
425		ID_HEAD391				32			SMBAT/BRC1 BLOCK5  Voltage		SMBAT/BRC1Tension Bloc5	
426		ID_HEAD392				32			SMBAT/BRC1 BLOCK6  Voltage		SMBAT/BRC1Tension Bloc6	
427		ID_HEAD393				32			SMBAT/BRC1 BLOCK7  Voltage		SMBAT/BRC1Tension Bloc7	
428		ID_HEAD394				32			SMBAT/BRC1 BLOCK8  Voltage		SMBAT/BRC1Tension Bloc8	
429		ID_HEAD395				32			SMBAT/BRC1 BLOCK9  Voltage		SMBAT/BRC1Tension Bloc9	
430		ID_HEAD396				32			SMBAT/BRC1 BLOCK10 Voltage		SMBAT/BRC1Tension Bloc10	
431		ID_HEAD397				32			SMBAT/BRC1 BLOCK11 Voltage		SMBAT/BRC1Tension Bloc11	
432		ID_HEAD398				32			SMBAT/BRC1 BLOCK12 Voltage		SMBAT/BRC1Tension Bloc12	
433		ID_HEAD399				32			SMBAT/BRC1 BLOCK13 Voltage		SMBAT/BRC1Tension Bloc13	
434		ID_HEAD400				32			SMBAT/BRC1 BLOCK14 Voltage		SMBAT/BRC1Tension Bloc14	
435		ID_HEAD401				32			SMBAT/BRC1 BLOCK15 Voltage		SMBAT/BRC1Tension Bloc15	
436		ID_HEAD402				32			SMBAT/BRC1 BLOCK16 Voltage		SMBAT/BRC1Tension Bloc16	
437		ID_HEAD403				32			SMBAT/BRC1 BLOCK17 Voltage		SMBAT/BRC1Tension Bloc17	
438		ID_HEAD404				32			SMBAT/BRC1 BLOCK18 Voltage		SMBAT/BRC1Tension Bloc18	
439		ID_HEAD405				32			SMBAT/BRC1 BLOCK19 Voltage		SMBAT/BRC1Tension Bloc19	
440		ID_HEAD406				32			SMBAT/BRC1 BLOCK20 Voltage		SMBAT/BRC1Tension Bloc20	
441		ID_HEAD407				32			SMBAT/BRC1 BLOCK21 Voltage		SMBAT/BRC1Tension Bloc21	
442		ID_HEAD408				32			SMBAT/BRC1 BLOCK22 Voltage		SMBAT/BRC1Tension Bloc22	
443		ID_HEAD409				32			SMBAT/BRC1 BLOCK23 Voltage		SMBAT/BRC1Tension Bloc23	
444		ID_HEAD410				32			SMBAT/BRC1 BLOCK24 Voltage		SMBAT/BRC1Tension Bloc24	
445		ID_HEAD411				32			SMBAT/BRC2 BLOCK1  Voltage		SMBAT/BRC2Tension Bloc1	
446		ID_HEAD412				32			SMBAT/BRC2 BLOCK2  Voltage		SMBAT/BRC2Tension Bloc2	
447		ID_HEAD413				32			SMBAT/BRC2 BLOCK3  Voltage		SMBAT/BRC2Tension Bloc3	
448		ID_HEAD414				32			SMBAT/BRC2 BLOCK4  Voltage		SMBAT/BRC2Tension Bloc4	
449		ID_HEAD415				32			SMBAT/BRC2 BLOCK5  Voltage		SMBAT/BRC2Tension Bloc5	
450		ID_HEAD416				32			SMBAT/BRC2 BLOCK6  Voltage		SMBAT/BRC2Tension Bloc6	
451		ID_HEAD417				32			SMBAT/BRC2 BLOCK7  Voltage		SMBAT/BRC2Tension Bloc7	
452		ID_HEAD418				32			SMBAT/BRC2 BLOCK8  Voltage		SMBAT/BRC2Tension Bloc8	
453		ID_HEAD419				32			SMBAT/BRC2 BLOCK9  Voltage		SMBAT/BRC2Tension Bloc9	
454		ID_HEAD420				32			SMBAT/BRC2 BLOCK10 Voltage		SMBAT/BRC2Tension Bloc10	
455		ID_HEAD421				32			SMBAT/BRC2 BLOCK11 Voltage		SMBAT/BRC2Tension Bloc11	
456		ID_HEAD422				32			SMBAT/BRC2 BLOCK12 Voltage		SMBAT/BRC2Tension Bloc12	
457		ID_HEAD423				32			SMBAT/BRC2 BLOCK13 Voltage		SMBAT/BRC2Tension Bloc13	
458		ID_HEAD424				32			SMBAT/BRC2 BLOCK14 Voltage		SMBAT/BRC2Tension Bloc14	
459		ID_HEAD425				32			SMBAT/BRC2 BLOCK15 Voltage		SMBAT/BRC2Tension Bloc15	
460		ID_HEAD426				32			SMBAT/BRC2 BLOCK16 Voltage		SMBAT/BRC2Tension Bloc16	
461		ID_HEAD427				32			SMBAT/BRC2 BLOCK17 Voltage		SMBAT/BRC2Tension Bloc17	
462		ID_HEAD428				32			SMBAT/BRC2 BLOCK18 Voltage		SMBAT/BRC2Tension Bloc18	
463		ID_HEAD429				32			SMBAT/BRC2 BLOCK19 Voltage		SMBAT/BRC2Tension Bloc19	
464		ID_HEAD430				32			SMBAT/BRC2 BLOCK20 Voltage		SMBAT/BRC2Tension Bloc20	
465		ID_HEAD431				32			SMBAT/BRC2 BLOCK21 Voltage		SMBAT/BRC2Tension Bloc21	
466		ID_HEAD432				32			SMBAT/BRC2 BLOCK22 Voltage		SMBAT/BRC2Tension Bloc22	
467		ID_HEAD433				32			SMBAT/BRC2 BLOCK23 Voltage		SMBAT/BRC2Tension Bloc23	
468		ID_HEAD434				32			SMBAT/BRC2 BLOCK24 Voltage		SMBAT/BRC2Tension Bloc24	
469		ID_HEAD435				32			SMBAT/BRC3 BLOCK1  Voltage		SMBAT/BRC3Tension Bloc1	
470		ID_HEAD436				32			SMBAT/BRC3 BLOCK2  Voltage		SMBAT/BRC3Tension Bloc2	
471		ID_HEAD437				32			SMBAT/BRC3 BLOCK3  Voltage		SMBAT/BRC3Tension Bloc3	
472		ID_HEAD438				32			SMBAT/BRC3 BLOCK4  Voltage		SMBAT/BRC3Tension Bloc4	
473		ID_HEAD439				32			SMBAT/BRC3 BLOCK5  Voltage		SMBAT/BRC3Tension Bloc5	
474		ID_HEAD440				32			SMBAT/BRC3 BLOCK6  Voltage		SMBAT/BRC3Tension Bloc6	
475		ID_HEAD441				32			SMBAT/BRC3 BLOCK7  Voltage		SMBAT/BRC3Tension Bloc7	
476		ID_HEAD442				32			SMBAT/BRC3 BLOCK8  Voltage		SMBAT/BRC3Tension Bloc8	
477		ID_HEAD443				32			SMBAT/BRC3 BLOCK9  Voltage		SMBAT/BRC3Tension Bloc9	
478		ID_HEAD444				32			SMBAT/BRC3 BLOCK10 Voltage		SMBAT/BRC3Tension Bloc10	
479		ID_HEAD445				32			SMBAT/BRC3 BLOCK11 Voltage		SMBAT/BRC3Tension Bloc11	
480		ID_HEAD446				32			SMBAT/BRC3 BLOCK12 Voltage		SMBAT/BRC3Tension Bloc12	
481		ID_HEAD447				32			SMBAT/BRC3 BLOCK13 Voltage		SMBAT/BRC3Tension Bloc13	
482		ID_HEAD448				32			SMBAT/BRC3 BLOCK14 Voltage		SMBAT/BRC3Tension Bloc14	
483		ID_HEAD449				32			SMBAT/BRC3 BLOCK15 Voltage		SMBAT/BRC3Tension Bloc15	
484		ID_HEAD450				32			SMBAT/BRC3 BLOCK16 Voltage		SMBAT/BRC3Tension Bloc16	
485		ID_HEAD451				32			SMBAT/BRC3 BLOCK17 Voltage		SMBAT/BRC3Tension Bloc17	
486		ID_HEAD452				32			SMBAT/BRC3 BLOCK18 Voltage		SMBAT/BRC3Tension Bloc18	
487		ID_HEAD453				32			SMBAT/BRC3 BLOCK19 Voltage		SMBAT/BRC3Tension Bloc19	
488		ID_HEAD454				32			SMBAT/BRC3 BLOCK20 Voltage		SMBAT/BRC3Tension Bloc20	
489		ID_HEAD455				32			SMBAT/BRC3 BLOCK21 Voltage		SMBAT/BRC3Tension Bloc21	
490		ID_HEAD456				32			SMBAT/BRC3 BLOCK22 Voltage		SMBAT/BRC3Tension Bloc22	
491		ID_HEAD457				32			SMBAT/BRC3 BLOCK23 Voltage		SMBAT/BRC3Tension Bloc23	
492		ID_HEAD458				32			SMBAT/BRC3 BLOCK24 Voltage		SMBAT/BRC3Tension Bloc24	
493		ID_HEAD459				32			SMBAT/BRC4 BLOCK1  Voltage		SMBAT/BRC4Tension Bloc1	
494		ID_HEAD460				32			SMBAT/BRC4 BLOCK2  Voltage		SMBAT/BRC4Tension Bloc2	
495		ID_HEAD461				32			SMBAT/BRC4 BLOCK3  Voltage		SMBAT/BRC4Tension Bloc3	
496		ID_HEAD462				32			SMBAT/BRC4 BLOCK4  Voltage		SMBAT/BRC4Tension Bloc4	
497		ID_HEAD463				32			SMBAT/BRC4 BLOCK5  Voltage		SMBAT/BRC4Tension Bloc5	
498		ID_HEAD464				32			SMBAT/BRC4 BLOCK6  Voltage		SMBAT/BRC4Tension Bloc6	
499		ID_HEAD465				32			SMBAT/BRC4 BLOCK7  Voltage		SMBAT/BRC4Tension Bloc7	
500		ID_HEAD466				32			SMBAT/BRC4 BLOCK8  Voltage		SMBAT/BRC4Tension Bloc8	
501		ID_HEAD467				32			SMBAT/BRC4 BLOCK9  Voltage		SMBAT/BRC4Tension Bloc9	
502		ID_HEAD468				32			SMBAT/BRC4 BLOCK10 Voltage		SMBAT/BRC4Tension Bloc10	
503		ID_HEAD469				32			SMBAT/BRC4 BLOCK11 Voltage		SMBAT/BRC4Tension Bloc11	
504		ID_HEAD470				32			SMBAT/BRC4 BLOCK12 Voltage		SMBAT/BRC4Tension Bloc12	
505		ID_HEAD471				32			SMBAT/BRC4 BLOCK13 Voltage		SMBAT/BRC4Tension Bloc13	
506		ID_HEAD472				32			SMBAT/BRC4 BLOCK14 Voltage		SMBAT/BRC4Tension Bloc14	
507		ID_HEAD473				32			SMBAT/BRC4 BLOCK15 Voltage		SMBAT/BRC4Tension Bloc15	
508		ID_HEAD474				32			SMBAT/BRC4 BLOCK16 Voltage		SMBAT/BRC4Tension Bloc16	
509		ID_HEAD475				32			SMBAT/BRC4 BLOCK17 Voltage		SMBAT/BRC4Tension Bloc17	
510		ID_HEAD476				32			SMBAT/BRC4 BLOCK18 Voltage		SMBAT/BRC4Tension Bloc18	
511		ID_HEAD477				32			SMBAT/BRC4 BLOCK19 Voltage		SMBAT/BRC4Tension Bloc19	
512		ID_HEAD478				32			SMBAT/BRC4 BLOCK20 Voltage		SMBAT/BRC4Tension Bloc20	
513		ID_HEAD479				32			SMBAT/BRC4 BLOCK21 Voltage		SMBAT/BRC4Tension Bloc21	
514		ID_HEAD480				32			SMBAT/BRC4 BLOCK22 Voltage		SMBAT/BRC4Tension Bloc22	
515		ID_HEAD481				32			SMBAT/BRC4 BLOCK23 Voltage		SMBAT/BRC4Tension Bloc23	
516		ID_HEAD482				32			SMBAT/BRC4 BLOCK24 Voltage		SMBAT/BRC4Tension Bloc24	
517		ID_HEAD483				32			SMBAT/BRC5 BLOCK1  Voltage		SMBAT/BRC5Tension Bloc1	
518		ID_HEAD484				32			SMBAT/BRC5 BLOCK2  Voltage		SMBAT/BRC5Tension Bloc2	
519		ID_HEAD485				32			SMBAT/BRC5 BLOCK3  Voltage		SMBAT/BRC5Tension Bloc3	
520		ID_HEAD486				32			SMBAT/BRC5 BLOCK4  Voltage		SMBAT/BRC5Tension Bloc4	
521		ID_HEAD487				32			SMBAT/BRC5 BLOCK5  Voltage		SMBAT/BRC5Tension Bloc5	
522		ID_HEAD488				32			SMBAT/BRC5 BLOCK6  Voltage		SMBAT/BRC5Tension Bloc6	
523		ID_HEAD489				32			SMBAT/BRC5 BLOCK7  Voltage		SMBAT/BRC5Tension Bloc7	
524		ID_HEAD490				32			SMBAT/BRC5 BLOCK8  Voltage		SMBAT/BRC5Tension Bloc8	
525		ID_HEAD491				32			SMBAT/BRC5 BLOCK9  Voltage		SMBAT/BRC5Tension Bloc9	
526		ID_HEAD492				32			SMBAT/BRC5 BLOCK10 Voltage		SMBAT/BRC5Tension Bloc10	
527		ID_HEAD493				32			SMBAT/BRC5 BLOCK11 Voltage		SMBAT/BRC5Tension Bloc11	
528		ID_HEAD494				32			SMBAT/BRC5 BLOCK12 Voltage		SMBAT/BRC5Tension Bloc12	
529		ID_HEAD495				32			SMBAT/BRC5 BLOCK13 Voltage		SMBAT/BRC5Tension Bloc13	
530		ID_HEAD496				32			SMBAT/BRC5 BLOCK14 Voltage		SMBAT/BRC5Tension Bloc14	
531		ID_HEAD497				32			SMBAT/BRC5 BLOCK15 Voltage		SMBAT/BRC5Tension Bloc15	
532		ID_HEAD498				32			SMBAT/BRC5 BLOCK16 Voltage		SMBAT/BRC5Tension Bloc16	
533		ID_HEAD499				32			SMBAT/BRC5 BLOCK17 Voltage		SMBAT/BRC5Tension Bloc17	
534		ID_HEAD500				32			SMBAT/BRC5 BLOCK18 Voltage		SMBAT/BRC5Tension Bloc18	
535		ID_HEAD501				32			SMBAT/BRC5 BLOCK19 Voltage		SMBAT/BRC5Tension Bloc19	
536		ID_HEAD502				32			SMBAT/BRC5 BLOCK20 Voltage		SMBAT/BRC5Tension Bloc20	
537		ID_HEAD503				32			SMBAT/BRC5 BLOCK21 Voltage		SMBAT/BRC5Tension Bloc21	
538		ID_HEAD504				32			SMBAT/BRC5 BLOCK22 Voltage		SMBAT/BRC5Tension Bloc22	
539		ID_HEAD505				32			SMBAT/BRC5 BLOCK23 Voltage		SMBAT/BRC5Tension Bloc23	
540		ID_HEAD506				32			SMBAT/BRC5 BLOCK24 Voltage		SMBAT/BRC5Tension Bloc24	
541		ID_HEAD507				32			SMBAT/BRC6 BLOCK1  Voltage		SMBAT/BRC6Tension Bloc1	
542		ID_HEAD508				32			SMBAT/BRC6 BLOCK2  Voltage		SMBAT/BRC6Tension Bloc2	
543		ID_HEAD509				32			SMBAT/BRC6 BLOCK3  Voltage		SMBAT/BRC6Tension Bloc3	
544		ID_HEAD510				32			SMBAT/BRC6 BLOCK4  Voltage		SMBAT/BRC6Tension Bloc4	
545		ID_HEAD511				32			SMBAT/BRC6 BLOCK5  Voltage		SMBAT/BRC6Tension Bloc5	
546		ID_HEAD512				32			SMBAT/BRC6 BLOCK6  Voltage		SMBAT/BRC6Tension Bloc6	
547		ID_HEAD513				32			SMBAT/BRC6 BLOCK7  Voltage		SMBAT/BRC6Tension Bloc7	
548		ID_HEAD514				32			SMBAT/BRC6 BLOCK8  Voltage		SMBAT/BRC6Tension Bloc8	
549		ID_HEAD515				32			SMBAT/BRC6 BLOCK9  Voltage		SMBAT/BRC6Tension Bloc9	
550		ID_HEAD516				32			SMBAT/BRC6 BLOCK10 Voltage		SMBAT/BRC6Tension Bloc10	
551		ID_HEAD517				32			SMBAT/BRC6 BLOCK11 Voltage		SMBAT/BRC6Tension Bloc11	
552		ID_HEAD518				32			SMBAT/BRC6 BLOCK12 Voltage		SMBAT/BRC6Tension Bloc12	
553		ID_HEAD519				32			SMBAT/BRC6 BLOCK13 Voltage		SMBAT/BRC6Tension Bloc13	
554		ID_HEAD520				32			SMBAT/BRC6 BLOCK14 Voltage		SMBAT/BRC6Tension Bloc14	
555		ID_HEAD521				32			SMBAT/BRC6 BLOCK15 Voltage		SMBAT/BRC6Tension Bloc15	
556		ID_HEAD522				32			SMBAT/BRC6 BLOCK16 Voltage		SMBAT/BRC6Tension Bloc16	
557		ID_HEAD523				32			SMBAT/BRC6 BLOCK17 Voltage		SMBAT/BRC6Tension Bloc17	
558		ID_HEAD524				32			SMBAT/BRC6 BLOCK18 Voltage		SMBAT/BRC6Tension Bloc18	
559		ID_HEAD525				32			SMBAT/BRC6 BLOCK19 Voltage		SMBAT/BRC6Tension Bloc19	
560		ID_HEAD526				32			SMBAT/BRC6 BLOCK20 Voltage		SMBAT/BRC6Tension Bloc20	
561		ID_HEAD527				32			SMBAT/BRC6 BLOCK21 Voltage		SMBAT/BRC6Tension Bloc21	
562		ID_HEAD528				32			SMBAT/BRC6 BLOCK22 Voltage		SMBAT/BRC6Tension Bloc22	
563		ID_HEAD529				32			SMBAT/BRC6 BLOCK23 Voltage		SMBAT/BRC6Tension Bloc23	
564		ID_HEAD530				32			SMBAT/BRC6 BLOCK24 Voltage		SMBAT/BRC6Tension Bloc24	
565		ID_HEAD531				32			SMBAT/BRC7 BLOCK1  Voltage		SMBAT/BRC7Tension Bloc1	
566		ID_HEAD532				32			SMBAT/BRC7 BLOCK2  Voltage		SMBAT/BRC7Tension Bloc2	
567		ID_HEAD533				32			SMBAT/BRC7 BLOCK3  Voltage		SMBAT/BRC7Tension Bloc3	
568		ID_HEAD534				32			SMBAT/BRC7 BLOCK4  Voltage		SMBAT/BRC7Tension Bloc4	
569		ID_HEAD535				32			SMBAT/BRC7 BLOCK5  Voltage		SMBAT/BRC7Tension Bloc5	
570		ID_HEAD536				32			SMBAT/BRC7 BLOCK6  Voltage		SMBAT/BRC7Tension Bloc6	
571		ID_HEAD537				32			SMBAT/BRC7 BLOCK7  Voltage		SMBAT/BRC7Tension Bloc7	
572		ID_HEAD538				32			SMBAT/BRC7 BLOCK8  Voltage		SMBAT/BRC7Tension Bloc8	
573		ID_HEAD539				32			SMBAT/BRC7 BLOCK9  Voltage		SMBAT/BRC7Tension Bloc9	
574		ID_HEAD540				32			SMBAT/BRC7 BLOCK10 Voltage		SMBAT/BRC7Tension Bloc10	
575		ID_HEAD541				32			SMBAT/BRC7 BLOCK11 Voltage		SMBAT/BRC7Tension Bloc11	
576		ID_HEAD542				32			SMBAT/BRC7 BLOCK12 Voltage		SMBAT/BRC7Tension Bloc12	
577		ID_HEAD543				32			SMBAT/BRC7 BLOCK13 Voltage		SMBAT/BRC7Tension Bloc13	
578		ID_HEAD544				32			SMBAT/BRC7 BLOCK14 Voltage		SMBAT/BRC7Tension Bloc14	
579		ID_HEAD545				32			SMBAT/BRC7 BLOCK15 Voltage		SMBAT/BRC7Tension Bloc15	
580		ID_HEAD546				32			SMBAT/BRC7 BLOCK16 Voltage		SMBAT/BRC7Tension Bloc16	
581		ID_HEAD547				32			SMBAT/BRC7 BLOCK17 Voltage		SMBAT/BRC7Tension Bloc17	
582		ID_HEAD548				32			SMBAT/BRC7 BLOCK18 Voltage		SMBAT/BRC7Tension Bloc18	
583		ID_HEAD549				32			SMBAT/BRC7 BLOCK19 Voltage		SMBAT/BRC7Tension Bloc19	
584		ID_HEAD550				32			SMBAT/BRC7 BLOCK20 Voltage		SMBAT/BRC7Tension Bloc20	
585		ID_HEAD551				32			SMBAT/BRC7 BLOCK21 Voltage		SMBAT/BRC7Tension Bloc21	
586		ID_HEAD552				32			SMBAT/BRC7 BLOCK22 Voltage		SMBAT/BRC7Tension Bloc22	
587		ID_HEAD553				32			SMBAT/BRC7 BLOCK23 Voltage		SMBAT/BRC7Tension Bloc23	
588		ID_HEAD554				32			SMBAT/BRC7 BLOCK24 Voltage		SMBAT/BRC7Tension Bloc24	
589		ID_HEAD555				32			SMBAT/BRC8 BLOCK1  Voltage		SMBAT/BRC8Tension Bloc1	
590		ID_HEAD556				32			SMBAT/BRC8 BLOCK2  Voltage		SMBAT/BRC8Tension Bloc2	
591		ID_HEAD557				32			SMBAT/BRC8 BLOCK3  Voltage		SMBAT/BRC8Tension Bloc3	
592		ID_HEAD558				32			SMBAT/BRC8 BLOCK4  Voltage		SMBAT/BRC8Tension Bloc4	
593		ID_HEAD559				32			SMBAT/BRC8 BLOCK5  Voltage		SMBAT/BRC8Tension Bloc5	
594		ID_HEAD560				32			SMBAT/BRC8 BLOCK6  Voltage		SMBAT/BRC8Tension Bloc6	
595		ID_HEAD561				32			SMBAT/BRC8 BLOCK7  Voltage		SMBAT/BRC8Tension Bloc7	
596		ID_HEAD562				32			SMBAT/BRC8 BLOCK8  Voltage		SMBAT/BRC8Tension Bloc8	
597		ID_HEAD563				32			SMBAT/BRC8 BLOCK9  Voltage		SMBAT/BRC8Tension Bloc9	
598		ID_HEAD564				32			SMBAT/BRC8 BLOCK10 Voltage		SMBAT/BRC8Tension Bloc10	
599		ID_HEAD565				32			SMBAT/BRC8 BLOCK11 Voltage		SMBAT/BRC8Tension Bloc11	
600		ID_HEAD566				32			SMBAT/BRC8 BLOCK12 Voltage		SMBAT/BRC8Tension Bloc12	
601		ID_HEAD567				32			SMBAT/BRC8 BLOCK13 Voltage		SMBAT/BRC8Tension Bloc13	
602		ID_HEAD568				32			SMBAT/BRC8 BLOCK14 Voltage		SMBAT/BRC8Tension Bloc14	
603		ID_HEAD569				32			SMBAT/BRC8 BLOCK15 Voltage		SMBAT/BRC8Tension Bloc15	
604		ID_HEAD570				32			SMBAT/BRC8 BLOCK16 Voltage		SMBAT/BRC8Tension Bloc16	
605		ID_HEAD571				32			SMBAT/BRC8 BLOCK17 Voltage		SMBAT/BRC8Tension Bloc17	
606		ID_HEAD572				32			SMBAT/BRC8 BLOCK18 Voltage		SMBAT/BRC8Tension Bloc18	
607		ID_HEAD573				32			SMBAT/BRC8 BLOCK19 Voltage		SMBAT/BRC8Tension Bloc19	
608		ID_HEAD574				32			SMBAT/BRC8 BLOCK20 Voltage		SMBAT/BRC8Tension Bloc20	
609		ID_HEAD575				32			SMBAT/BRC8 BLOCK21 Voltage		SMBAT/BRC8Tension Bloc21	
610		ID_HEAD576				32			SMBAT/BRC8 BLOCK22 Voltage		SMBAT/BRC8Tension Bloc22	
611		ID_HEAD577				32			SMBAT/BRC8 BLOCK23 Voltage		SMBAT/BRC8Tension Bloc23	
612		ID_HEAD578				32			SMBAT/BRC8 BLOCK24 Voltage		SMBAT/BRC8Tension Bloc24	
613		ID_HEAD579				32			SMBAT/BRC9 BLOCK1  Voltage		SMBAT/BRC9Tension Bloc1	
614		ID_HEAD580				32			SMBAT/BRC9 BLOCK2  Voltage		SMBAT/BRC9Tension Bloc2	
615		ID_HEAD581				32			SMBAT/BRC9 BLOCK3  Voltage		SMBAT/BRC9Tension Bloc3	
616		ID_HEAD582				32			SMBAT/BRC9 BLOCK4  Voltage		SMBAT/BRC9Tension Bloc4	
617		ID_HEAD583				32			SMBAT/BRC9 BLOCK5  Voltage		SMBAT/BRC9Tension Bloc5	
618		ID_HEAD584				32			SMBAT/BRC9 BLOCK6  Voltage		SMBAT/BRC9Tension Bloc6	
619		ID_HEAD585				32			SMBAT/BRC9 BLOCK7  Voltage		SMBAT/BRC9Tension Bloc7	
620		ID_HEAD586				32			SMBAT/BRC9 BLOCK8  Voltage		SMBAT/BRC9Tension Bloc8	
621		ID_HEAD587				32			SMBAT/BRC9 BLOCK9  Voltage		SMBAT/BRC9Tension Bloc9	
622		ID_HEAD588				32			SMBAT/BRC9 BLOCK10 Voltage		SMBAT/BRC9Tension Bloc10	
623		ID_HEAD589				32			SMBAT/BRC9 BLOCK11 Voltage		SMBAT/BRC9Tension Bloc11	
624		ID_HEAD590				32			SMBAT/BRC9 BLOCK12 Voltage		SMBAT/BRC9Tension Bloc12	
625		ID_HEAD591				32			SMBAT/BRC9 BLOCK13 Voltage		SMBAT/BRC9Tension Bloc13	
626		ID_HEAD592				32			SMBAT/BRC9 BLOCK14 Voltage		SMBAT/BRC9Tension Bloc14	
627		ID_HEAD593				32			SMBAT/BRC9 BLOCK15 Voltage		SMBAT/BRC9Tension Bloc15	
628		ID_HEAD594				32			SMBAT/BRC9 BLOCK16 Voltage		SMBAT/BRC9Tension Bloc16	
629		ID_HEAD595				32			SMBAT/BRC9 BLOCK17 Voltage		SMBAT/BRC9Tension Bloc17	
630		ID_HEAD596				32			SMBAT/BRC9 BLOCK18 Voltage		SMBAT/BRC9Tension Bloc18	
631		ID_HEAD597				32			SMBAT/BRC9 BLOCK19 Voltage		SMBAT/BRC9Tension Bloc19	
632		ID_HEAD598				32			SMBAT/BRC9 BLOCK20 Voltage		SMBAT/BRC9Tension Bloc20	
633		ID_HEAD599				32			SMBAT/BRC9 BLOCK21 Voltage		SMBAT/BRC9Tension Bloc21	
634		ID_HEAD600				32			SMBAT/BRC9 BLOCK22 Voltage		SMBAT/BRC9Tension Bloc22	
635		ID_HEAD601				32			SMBAT/BRC9 BLOCK23 Voltage		SMBAT/BRC9Tension Bloc23	
636		ID_HEAD602				32			SMBAT/BRC9 BLOCK24 Voltage		SMBAT/BRC9Tension Bloc24	
637		ID_HEAD603				32			SMBAT/BRC10 BLOCK1  Voltage		SMBAT/BRC10Tension Bloc1	
638		ID_HEAD604				32			SMBAT/BRC10 BLOCK2  Voltage		SMBAT/BRC10Tension Bloc2	
639		ID_HEAD605				32			SMBAT/BRC10 BLOCK3  Voltage		SMBAT/BRC10Tension Bloc3	
640		ID_HEAD606				32			SMBAT/BRC10 BLOCK4  Voltage		SMBAT/BRC10Tension Bloc4	
641		ID_HEAD607				32			SMBAT/BRC10 BLOCK5  Voltage		SMBAT/BRC10Tension Bloc5	
642		ID_HEAD608				32			SMBAT/BRC10 BLOCK6  Voltage		SMBAT/BRC10Tension Bloc6	
643		ID_HEAD609				32			SMBAT/BRC10 BLOCK7  Voltage		SMBAT/BRC10Tension Bloc7	
644		ID_HEAD610				32			SMBAT/BRC10 BLOCK8  Voltage		SMBAT/BRC10Tension Bloc8	
645		ID_HEAD611				32			SMBAT/BRC10 BLOCK9  Voltage		SMBAT/BRC10Tension Bloc9	
646		ID_HEAD612				32			SMBAT/BRC10 BLOCK10 Voltage		SMBAT/BRC10Tension Bloc10	
647		ID_HEAD613				32			SMBAT/BRC10 BLOCK11 Voltage		SMBAT/BRC10Tension Bloc11	
648		ID_HEAD614				32			SMBAT/BRC10 BLOCK12 Voltage		SMBAT/BRC10Tension Bloc12	
649		ID_HEAD615				32			SMBAT/BRC10 BLOCK13 Voltage		SMBAT/BRC10Tension Bloc13	
650		ID_HEAD616				32			SMBAT/BRC10 BLOCK14 Voltage		SMBAT/BRC10Tension Bloc14	
651		ID_HEAD617				32			SMBAT/BRC10 BLOCK15 Voltage		SMBAT/BRC10Tension Bloc15	
652		ID_HEAD618				32			SMBAT/BRC10 BLOCK16 Voltage		SMBAT/BRC10Tension Bloc16	
653		ID_HEAD619				32			SMBAT/BRC10 BLOCK17 Voltage		SMBAT/BRC10Tension Bloc17	
654		ID_HEAD620				32			SMBAT/BRC10 BLOCK18 Voltage		SMBAT/BRC10Tension Bloc18	
655		ID_HEAD621				32			SMBAT/BRC10 BLOCK19 Voltage		SMBAT/BRC10Tension Bloc19	
656		ID_HEAD622				32			SMBAT/BRC10 BLOCK20 Voltage		SMBAT/BRC10Tension Bloc20	
657		ID_HEAD623				32			SMBAT/BRC10 BLOCK21 Voltage		SMBAT/BRC10Tension Bloc21	
658		ID_HEAD624				32			SMBAT/BRC10 BLOCK22 Voltage		SMBAT/BRC10Tension Bloc22	
659		ID_HEAD625				32			SMBAT/BRC10 BLOCK23 Voltage		SMBAT/BRC10Tension Bloc23	
660		ID_HEAD626				32			SMBAT/BRC10 BLOCK24 Voltage		SMBAT/BRC10Tension Bloc24	
661		ID_HEAD627				32			SMBAT/BRC11 BLOCK1  Voltage		SMBAT/BRC11Tension Bloc1	
662		ID_HEAD628				32			SMBAT/BRC11 BLOCK2  Voltage		SMBAT/BRC11Tension Bloc2	
663		ID_HEAD629				32			SMBAT/BRC11 BLOCK3  Voltage		SMBAT/BRC11Tension Bloc3	
664		ID_HEAD630				32			SMBAT/BRC11 BLOCK4  Voltage		SMBAT/BRC11Tension Bloc4	
665		ID_HEAD631				32			SMBAT/BRC11 BLOCK5  Voltage		SMBAT/BRC11Tension Bloc5	
666		ID_HEAD632				32			SMBAT/BRC11 BLOCK6  Voltage		SMBAT/BRC11Tension Bloc6	
667		ID_HEAD633				32			SMBAT/BRC11 BLOCK7  Voltage		SMBAT/BRC11Tension Bloc7	
668		ID_HEAD634				32			SMBAT/BRC11 BLOCK8  Voltage		SMBAT/BRC11Tension Bloc8	
669		ID_HEAD635				32			SMBAT/BRC11 BLOCK9  Voltage		SMBAT/BRC11Tension Bloc9	
670		ID_HEAD636				32			SMBAT/BRC11 BLOCK10 Voltage		SMBAT/BRC11Tension Bloc10	
671		ID_HEAD637				32			SMBAT/BRC11 BLOCK11 Voltage		SMBAT/BRC11Tension Bloc11	
672		ID_HEAD638				32			SMBAT/BRC11 BLOCK12 Voltage		SMBAT/BRC11Tension Bloc12	
673		ID_HEAD639				32			SMBAT/BRC11 BLOCK13 Voltage		SMBAT/BRC11Tension Bloc13	
674		ID_HEAD640				32			SMBAT/BRC11 BLOCK14 Voltage		SMBAT/BRC11Tension Bloc14	
675		ID_HEAD641				32			SMBAT/BRC11 BLOCK15 Voltage		SMBAT/BRC11Tension Bloc15	
676		ID_HEAD642				32			SMBAT/BRC11 BLOCK16 Voltage		SMBAT/BRC11Tension Bloc16	
677		ID_HEAD643				32			SMBAT/BRC11 BLOCK17 Voltage		SMBAT/BRC11Tension Bloc17	
678		ID_HEAD644				32			SMBAT/BRC11 BLOCK18 Voltage		SMBAT/BRC11Tension Bloc18	
679		ID_HEAD645				32			SMBAT/BRC11 BLOCK19 Voltage		SMBAT/BRC11Tension Bloc19	
680		ID_HEAD646				32			SMBAT/BRC11 BLOCK20 Voltage		SMBAT/BRC11Tension Bloc20	
681		ID_HEAD647				32			SMBAT/BRC11 BLOCK21 Voltage		SMBAT/BRC11Tension Bloc21	
682		ID_HEAD648				32			SMBAT/BRC11 BLOCK22 Voltage		SMBAT/BRC11Tension Bloc22	
683		ID_HEAD649				32			SMBAT/BRC11 BLOCK23 Voltage		SMBAT/BRC11Tension Bloc23	
684		ID_HEAD650				32			SMBAT/BRC11 BLOCK24 Voltage		SMBAT/BRC11Tension Bloc24	
685		ID_HEAD651				32			SMBAT/BRC12 BLOCK1  Voltage		SMBAT/BRC12Tension Bloc1	
686		ID_HEAD652				32			SMBAT/BRC12 BLOCK2  Voltage		SMBAT/BRC12Tension Bloc2	
687		ID_HEAD653				32			SMBAT/BRC12 BLOCK3  Voltage		SMBAT/BRC12Tension Bloc3	
688		ID_HEAD654				32			SMBAT/BRC12 BLOCK4  Voltage		SMBAT/BRC12Tension Bloc4	
689		ID_HEAD655				32			SMBAT/BRC12 BLOCK5  Voltage		SMBAT/BRC12Tension Bloc5	
690		ID_HEAD656				32			SMBAT/BRC12 BLOCK6  Voltage		SMBAT/BRC12Tension Bloc6	
691		ID_HEAD657				32			SMBAT/BRC12 BLOCK7  Voltage		SMBAT/BRC12Tension Bloc7	
692		ID_HEAD658				32			SMBAT/BRC12 BLOCK8  Voltage		SMBAT/BRC12Tension Bloc8	
693		ID_HEAD659				32			SMBAT/BRC12 BLOCK9  Voltage		SMBAT/BRC12Tension Bloc9	
694		ID_HEAD660				32			SMBAT/BRC12 BLOCK10 Voltage		SMBAT/BRC12Tension Bloc10	
695		ID_HEAD661				32			SMBAT/BRC12 BLOCK11 Voltage		SMBAT/BRC12Tension Bloc11	
696		ID_HEAD662				32			SMBAT/BRC12 BLOCK12 Voltage		SMBAT/BRC12Tension Bloc12	
697		ID_HEAD663				32			SMBAT/BRC12 BLOCK13 Voltage		SMBAT/BRC12Tension Bloc13	
698		ID_HEAD664				32			SMBAT/BRC12 BLOCK14 Voltage		SMBAT/BRC12Tension Bloc14	
699		ID_HEAD665				32			SMBAT/BRC12 BLOCK15 Voltage		SMBAT/BRC12Tension Bloc15	
700		ID_HEAD666				32			SMBAT/BRC12 BLOCK16 Voltage		SMBAT/BRC12Tension Bloc16	
701		ID_HEAD667				32			SMBAT/BRC12 BLOCK17 Voltage		SMBAT/BRC12Tension Bloc17	
702		ID_HEAD668				32			SMBAT/BRC12 BLOCK18 Voltage		SMBAT/BRC12Tension Bloc18	
703		ID_HEAD669				32			SMBAT/BRC12 BLOCK19 Voltage		SMBAT/BRC12Tension Bloc19	
704		ID_HEAD670				32			SMBAT/BRC12 BLOCK20 Voltage		SMBAT/BRC12Tension Bloc20	
705		ID_HEAD671				32			SMBAT/BRC12 BLOCK21 Voltage		SMBAT/BRC12Tension Bloc21	
706		ID_HEAD672				32			SMBAT/BRC12 BLOCK22 Voltage		SMBAT/BRC12Tension Bloc22	
707		ID_HEAD673				32			SMBAT/BRC12 BLOCK23 Voltage		SMBAT/BRC12Tension Bloc23	
708		ID_HEAD674				32			SMBAT/BRC12 BLOCK24 Voltage		SMBAT/BRC12Tension Bloc24	
709		ID_HEAD675				32			SMBAT/BRC13 BLOCK1  Voltage		SMBAT/BRC13Tension Bloc1	
710		ID_HEAD676				32			SMBAT/BRC13 BLOCK2  Voltage		SMBAT/BRC13Tension Bloc2	
711		ID_HEAD677				32			SMBAT/BRC13 BLOCK3  Voltage		SMBAT/BRC13Tension Bloc3	
712		ID_HEAD678				32			SMBAT/BRC13 BLOCK4  Voltage		SMBAT/BRC13Tension Bloc4	
713		ID_HEAD679				32			SMBAT/BRC13 BLOCK5  Voltage		SMBAT/BRC13Tension Bloc5	
714		ID_HEAD680				32			SMBAT/BRC13 BLOCK6  Voltage		SMBAT/BRC13Tension Bloc6	
715		ID_HEAD681				32			SMBAT/BRC13 BLOCK7  Voltage		SMBAT/BRC13Tension Bloc7	
716		ID_HEAD682				32			SMBAT/BRC13 BLOCK8  Voltage		SMBAT/BRC13Tension Bloc8	
717		ID_HEAD683				32			SMBAT/BRC13 BLOCK9  Voltage		SMBAT/BRC13Tension Bloc9	
718		ID_HEAD684				32			SMBAT/BRC13 BLOCK10 Voltage		SMBAT/BRC13Tension Bloc10	
719		ID_HEAD685				32			SMBAT/BRC13 BLOCK11 Voltage		SMBAT/BRC13Tension Bloc11	
720		ID_HEAD686				32			SMBAT/BRC13 BLOCK12 Voltage		SMBAT/BRC13Tension Bloc12	
721		ID_HEAD687				32			SMBAT/BRC13 BLOCK13 Voltage		SMBAT/BRC13Tension Bloc13	
722		ID_HEAD688				32			SMBAT/BRC13 BLOCK14 Voltage		SMBAT/BRC13Tension Bloc14	
723		ID_HEAD689				32			SMBAT/BRC13 BLOCK15 Voltage		SMBAT/BRC13Tension Bloc15	
724		ID_HEAD690				32			SMBAT/BRC13 BLOCK16 Voltage		SMBAT/BRC13Tension Bloc16	
725		ID_HEAD691				32			SMBAT/BRC13 BLOCK17 Voltage		SMBAT/BRC13Tension Bloc17	
726		ID_HEAD692				32			SMBAT/BRC13 BLOCK18 Voltage		SMBAT/BRC13Tension Bloc18	
727		ID_HEAD693				32			SMBAT/BRC13 BLOCK19 Voltage		SMBAT/BRC13Tension Bloc19	
728		ID_HEAD694				32			SMBAT/BRC13 BLOCK20 Voltage		SMBAT/BRC13Tension Bloc20	
729		ID_HEAD695				32			SMBAT/BRC13 BLOCK21 Voltage		SMBAT/BRC13Tension Bloc21	
730		ID_HEAD696				32			SMBAT/BRC13 BLOCK22 Voltage		SMBAT/BRC13Tension Bloc22	
731		ID_HEAD697				32			SMBAT/BRC13 BLOCK23 Voltage		SMBAT/BRC13Tension Bloc23	
732		ID_HEAD698				32			SMBAT/BRC13 BLOCK24 Voltage		SMBAT/BRC13Tension Bloc24	
733		ID_HEAD699				32			SMBAT/BRC14 BLOCK1  Voltage		SMBAT/BRC14Tension Bloc1	
734		ID_HEAD700				32			SMBAT/BRC14 BLOCK2  Voltage		SMBAT/BRC14Tension Bloc2	
735		ID_HEAD701				32			SMBAT/BRC14 BLOCK3  Voltage		SMBAT/BRC14Tension Bloc3	
736		ID_HEAD702				32			SMBAT/BRC14 BLOCK4  Voltage		SMBAT/BRC14Tension Bloc4	
737		ID_HEAD703				32			SMBAT/BRC14 BLOCK5  Voltage		SMBAT/BRC14Tension Bloc5	
738		ID_HEAD704				32			SMBAT/BRC14 BLOCK6  Voltage		SMBAT/BRC14Tension Bloc6	
739		ID_HEAD705				32			SMBAT/BRC14 BLOCK7  Voltage		SMBAT/BRC14Tension Bloc7	
740		ID_HEAD706				32			SMBAT/BRC14 BLOCK8  Voltage		SMBAT/BRC14Tension Bloc8	
741		ID_HEAD707				32			SMBAT/BRC14 BLOCK9  Voltage		SMBAT/BRC14Tension Bloc9	
742		ID_HEAD708				32			SMBAT/BRC14 BLOCK10 Voltage		SMBAT/BRC14Tension Bloc10	
743		ID_HEAD709				32			SMBAT/BRC14 BLOCK11 Voltage		SMBAT/BRC14Tension Bloc11	
744		ID_HEAD710				32			SMBAT/BRC14 BLOCK12 Voltage		SMBAT/BRC14Tension Bloc12	
745		ID_HEAD711				32			SMBAT/BRC14 BLOCK13 Voltage		SMBAT/BRC14Tension Bloc13	
746		ID_HEAD712				32			SMBAT/BRC14 BLOCK14 Voltage		SMBAT/BRC14Tension Bloc14	
747		ID_HEAD713				32			SMBAT/BRC14 BLOCK15 Voltage		SMBAT/BRC14Tension Bloc15	
748		ID_HEAD714				32			SMBAT/BRC14 BLOCK16 Voltage		SMBAT/BRC14Tension Bloc16	
749		ID_HEAD715				32			SMBAT/BRC14 BLOCK17 Voltage		SMBAT/BRC14Tension Bloc17	
750		ID_HEAD716				32			SMBAT/BRC14 BLOCK18 Voltage		SMBAT/BRC14Tension Bloc18	
751		ID_HEAD717				32			SMBAT/BRC14 BLOCK19 Voltage		SMBAT/BRC14Tension Bloc19	
752		ID_HEAD718				32			SMBAT/BRC14 BLOCK20 Voltage		SMBAT/BRC14Tension Bloc20	
753		ID_HEAD719				32			SMBAT/BRC14 BLOCK21 Voltage		SMBAT/BRC14Tension Bloc21	
754		ID_HEAD720				32			SMBAT/BRC14 BLOCK22 Voltage		SMBAT/BRC14Tension Bloc22	
755		ID_HEAD721				32			SMBAT/BRC14 BLOCK23 Voltage		SMBAT/BRC14Tension Bloc23	
756		ID_HEAD722				32			SMBAT/BRC14 BLOCK24 Voltage		SMBAT/BRC14Tension Bloc24	
757		ID_HEAD723				32			SMBAT/BRC15 BLOCK1  Voltage		SMBAT/BRC15Tension Bloc1	
758		ID_HEAD724				32			SMBAT/BRC15 BLOCK2  Voltage		SMBAT/BRC15Tension Bloc2	
759		ID_HEAD725				32			SMBAT/BRC15 BLOCK3  Voltage		SMBAT/BRC15Tension Bloc3	
760		ID_HEAD726				32			SMBAT/BRC15 BLOCK4  Voltage		SMBAT/BRC15Tension Bloc4	
761		ID_HEAD727				32			SMBAT/BRC15 BLOCK5  Voltage		SMBAT/BRC15Tension Bloc5	
762		ID_HEAD728				32			SMBAT/BRC15 BLOCK6  Voltage		SMBAT/BRC15Tension Bloc6	
763		ID_HEAD729				32			SMBAT/BRC15 BLOCK7  Voltage		SMBAT/BRC15Tension Bloc7	
764		ID_HEAD730				32			SMBAT/BRC15 BLOCK8  Voltage		SMBAT/BRC15Tension Bloc8	
765		ID_HEAD731				32			SMBAT/BRC15 BLOCK9  Voltage		SMBAT/BRC15Tension Bloc9	
766		ID_HEAD732				32			SMBAT/BRC15 BLOCK10 Voltage		SMBAT/BRC15Tension Bloc10	
767		ID_HEAD733				32			SMBAT/BRC15 BLOCK11 Voltage		SMBAT/BRC15Tension Bloc11	
768		ID_HEAD734				32			SMBAT/BRC15 BLOCK12 Voltage		SMBAT/BRC15Tension Bloc12	
769		ID_HEAD735				32			SMBAT/BRC15 BLOCK13 Voltage		SMBAT/BRC15Tension Bloc13	
770		ID_HEAD736				32			SMBAT/BRC15 BLOCK14 Voltage		SMBAT/BRC15Tension Bloc14	
771		ID_HEAD737				32			SMBAT/BRC15 BLOCK15 Voltage		SMBAT/BRC15Tension Bloc15	
772		ID_HEAD738				32			SMBAT/BRC15 BLOCK16 Voltage		SMBAT/BRC15Tension Bloc16	
773		ID_HEAD739				32			SMBAT/BRC15 BLOCK17 Voltage		SMBAT/BRC15Tension Bloc17	
774		ID_HEAD740				32			SMBAT/BRC15 BLOCK18 Voltage		SMBAT/BRC15Tension Bloc18	
775		ID_HEAD741				32			SMBAT/BRC15 BLOCK19 Voltage		SMBAT/BRC15Tension Bloc19	
776		ID_HEAD742				32			SMBAT/BRC15 BLOCK20 Voltage		SMBAT/BRC15Tension Bloc20	
777		ID_HEAD743				32			SMBAT/BRC15 BLOCK21 Voltage		SMBAT/BRC15Tension Bloc21	
778		ID_HEAD744				32			SMBAT/BRC15 BLOCK22 Voltage		SMBAT/BRC15Tension Bloc22	
779		ID_HEAD745				32			SMBAT/BRC15 BLOCK23 Voltage		SMBAT/BRC15Tension Bloc23	
780		ID_HEAD746				32			SMBAT/BRC15 BLOCK24 Voltage		SMBAT/BRC15Tension Bloc24	
781		ID_HEAD747				32			SMBAT/BRC16 BLOCK1  Voltage		SMBAT/BRC16Tension Bloc1	
782		ID_HEAD748				32			SMBAT/BRC16 BLOCK2  Voltage		SMBAT/BRC16Tension Bloc2	
783		ID_HEAD749				32			SMBAT/BRC16 BLOCK3  Voltage		SMBAT/BRC16Tension Bloc3	
784		ID_HEAD750				32			SMBAT/BRC16 BLOCK4  Voltage		SMBAT/BRC16Tension Bloc4	
785		ID_HEAD751				32			SMBAT/BRC16 BLOCK5  Voltage		SMBAT/BRC16Tension Bloc5	
786		ID_HEAD752				32			SMBAT/BRC16 BLOCK6  Voltage		SMBAT/BRC16Tension Bloc6	
787		ID_HEAD753				32			SMBAT/BRC16 BLOCK7  Voltage		SMBAT/BRC16Tension Bloc7	
788		ID_HEAD754				32			SMBAT/BRC16 BLOCK8  Voltage		SMBAT/BRC16Tension Bloc8	
789		ID_HEAD755				32			SMBAT/BRC16 BLOCK9  Voltage		SMBAT/BRC16Tension Bloc9	
790		ID_HEAD756				32			SMBAT/BRC16 BLOCK10 Voltage		SMBAT/BRC16Tension Bloc10	
791		ID_HEAD757				32			SMBAT/BRC16 BLOCK11 Voltage		SMBAT/BRC16Tension Bloc11	
792		ID_HEAD758				32			SMBAT/BRC16 BLOCK12 Voltage		SMBAT/BRC16Tension Bloc12	
793		ID_HEAD759				32			SMBAT/BRC16 BLOCK13 Voltage		SMBAT/BRC16Tension Bloc13	
794		ID_HEAD760				32			SMBAT/BRC16 BLOCK14 Voltage		SMBAT/BRC16Tension Bloc14	
795		ID_HEAD761				32			SMBAT/BRC16 BLOCK15 Voltage		SMBAT/BRC16Tension Bloc15	
796		ID_HEAD762				32			SMBAT/BRC16 BLOCK16 Voltage		SMBAT/BRC16Tension Bloc16	
797		ID_HEAD763				32			SMBAT/BRC16 BLOCK17 Voltage		SMBAT/BRC16Tension Bloc17	
798		ID_HEAD764				32			SMBAT/BRC16 BLOCK18 Voltage		SMBAT/BRC16Tension Bloc18	
799		ID_HEAD765				32			SMBAT/BRC16 BLOCK19 Voltage		SMBAT/BRC16Tension Bloc19	
800		ID_HEAD766				32			SMBAT/BRC16 BLOCK20 Voltage		SMBAT/BRC16Tension Bloc20	
801		ID_HEAD767				32			SMBAT/BRC16 BLOCK21 Voltage		SMBAT/BRC16Tension Bloc21	
802		ID_HEAD768				32			SMBAT/BRC16 BLOCK22 Voltage		SMBAT/BRC16Tension Bloc22	
803		ID_HEAD769				32			SMBAT/BRC16 BLOCK23 Voltage		SMBAT/BRC16Tension Bloc23	
804		ID_HEAD770				32			SMBAT/BRC16 BLOCK24 Voltage		SMBAT/BRC16Tension Bloc24	
805		ID_HEAD771				32			SMBAT/BRC17 BLOCK1  Voltage		SMBAT/BRC17Tension Bloc1	
806		ID_HEAD772				32			SMBAT/BRC17 BLOCK2  Voltage		SMBAT/BRC17Tension Bloc2	
807		ID_HEAD773				32			SMBAT/BRC17 BLOCK3  Voltage		SMBAT/BRC17Tension Bloc3	
808		ID_HEAD774				32			SMBAT/BRC17 BLOCK4  Voltage		SMBAT/BRC17Tension Bloc4	
809		ID_HEAD775				32			SMBAT/BRC17 BLOCK5  Voltage		SMBAT/BRC17Tension Bloc5	
810		ID_HEAD776				32			SMBAT/BRC17 BLOCK6  Voltage		SMBAT/BRC17Tension Bloc6	
811		ID_HEAD777				32			SMBAT/BRC17 BLOCK7  Voltage		SMBAT/BRC17Tension Bloc7	
812		ID_HEAD778				32			SMBAT/BRC17 BLOCK8  Voltage		SMBAT/BRC17Tension Bloc8	
813		ID_HEAD779				32			SMBAT/BRC17 BLOCK9  Voltage		SMBAT/BRC17Tension Bloc9	
814		ID_HEAD780				32			SMBAT/BRC17 BLOCK10 Voltage		SMBAT/BRC17Tension Bloc10	
815		ID_HEAD781				32			SMBAT/BRC17 BLOCK11 Voltage		SMBAT/BRC17Tension Bloc11	
816		ID_HEAD782				32			SMBAT/BRC17 BLOCK12 Voltage		SMBAT/BRC17Tension Bloc12	
817		ID_HEAD783				32			SMBAT/BRC17 BLOCK13 Voltage		SMBAT/BRC17Tension Bloc13	
818		ID_HEAD784				32			SMBAT/BRC17 BLOCK14 Voltage		SMBAT/BRC17Tension Bloc14	
819		ID_HEAD785				32			SMBAT/BRC17 BLOCK15 Voltage		SMBAT/BRC17Tension Bloc15	
820		ID_HEAD786				32			SMBAT/BRC17 BLOCK16 Voltage		SMBAT/BRC17Tension Bloc16	
821		ID_HEAD787				32			SMBAT/BRC17 BLOCK17 Voltage		SMBAT/BRC17Tension Bloc17	
822		ID_HEAD788				32			SMBAT/BRC17 BLOCK18 Voltage		SMBAT/BRC17Tension Bloc18	
823		ID_HEAD789				32			SMBAT/BRC17 BLOCK19 Voltage		SMBAT/BRC17Tension Bloc19	
824		ID_HEAD790				32			SMBAT/BRC17 BLOCK20 Voltage		SMBAT/BRC17Tension Bloc20	
825		ID_HEAD791				32			SMBAT/BRC17 BLOCK21 Voltage		SMBAT/BRC17Tension Bloc21	
826		ID_HEAD792				32			SMBAT/BRC17 BLOCK22 Voltage		SMBAT/BRC17Tension Bloc22	
827		ID_HEAD793				32			SMBAT/BRC17 BLOCK23 Voltage		SMBAT/BRC17Tension Bloc23	
828		ID_HEAD794				32			SMBAT/BRC17 BLOCK24 Voltage		SMBAT/BRC17Tension Bloc24	
829		ID_HEAD795				32			SMBAT/BRC18 BLOCK1  Voltage		SMBAT/BRC18Tension Bloc1	
830		ID_HEAD796				32			SMBAT/BRC18 BLOCK2  Voltage		SMBAT/BRC18Tension Bloc2	
831		ID_HEAD797				32			SMBAT/BRC18 BLOCK3  Voltage		SMBAT/BRC18Tension Bloc3	
832		ID_HEAD798				32			SMBAT/BRC18 BLOCK4  Voltage		SMBAT/BRC18Tension Bloc4	
833		ID_HEAD799				32			SMBAT/BRC18 BLOCK5  Voltage		SMBAT/BRC18Tension Bloc5	
834		ID_HEAD800				32			SMBAT/BRC18 BLOCK6  Voltage		SMBAT/BRC18Tension Bloc6	
835		ID_HEAD801				32			SMBAT/BRC18 BLOCK7  Voltage		SMBAT/BRC18Tension Bloc7	
836		ID_HEAD802				32			SMBAT/BRC18 BLOCK8  Voltage		SMBAT/BRC18Tension Bloc8	
837		ID_HEAD803				32			SMBAT/BRC18 BLOCK9  Voltage		SMBAT/BRC18Tension Bloc9	
838		ID_HEAD804				32			SMBAT/BRC18 BLOCK10 Voltage		SMBAT/BRC18Tension Bloc10	
839		ID_HEAD805				32			SMBAT/BRC18 BLOCK11 Voltage		SMBAT/BRC18Tension Bloc11	
840		ID_HEAD806				32			SMBAT/BRC18 BLOCK12 Voltage		SMBAT/BRC18Tension Bloc12	
841		ID_HEAD807				32			SMBAT/BRC18 BLOCK13 Voltage		SMBAT/BRC18Tension Bloc13	
842		ID_HEAD808				32			SMBAT/BRC18 BLOCK14 Voltage		SMBAT/BRC18Tension Bloc14	
843		ID_HEAD809				32			SMBAT/BRC18 BLOCK15 Voltage		SMBAT/BRC18Tension Bloc15	
844		ID_HEAD810				32			SMBAT/BRC18 BLOCK16 Voltage		SMBAT/BRC18Tension Bloc16	
845		ID_HEAD811				32			SMBAT/BRC18 BLOCK17 Voltage		SMBAT/BRC18Tension Bloc17	
846		ID_HEAD812				32			SMBAT/BRC18 BLOCK18 Voltage		SMBAT/BRC18Tension Bloc18	
847		ID_HEAD813				32			SMBAT/BRC18 BLOCK19 Voltage		SMBAT/BRC18Tension Bloc19	
848		ID_HEAD814				32			SMBAT/BRC18 BLOCK20 Voltage		SMBAT/BRC18Tension Bloc20	
849		ID_HEAD815				32			SMBAT/BRC18 BLOCK21 Voltage		SMBAT/BRC18Tension Bloc21	
850		ID_HEAD816				32			SMBAT/BRC18 BLOCK22 Voltage		SMBAT/BRC18Tension Bloc22	
851		ID_HEAD817				32			SMBAT/BRC18 BLOCK23 Voltage		SMBAT/BRC18Tension Bloc23	
852		ID_HEAD818				32			SMBAT/BRC18 BLOCK24 Voltage		SMBAT/BRC18Tension Bloc24	
853		ID_HEAD819				32			SMBAT/BRC19 BLOCK1  Voltage		SMBAT/BRC19Tension Bloc1	
854		ID_HEAD820				32			SMBAT/BRC19 BLOCK2  Voltage		SMBAT/BRC19Tension Bloc2	
855		ID_HEAD821				32			SMBAT/BRC19 BLOCK3  Voltage		SMBAT/BRC19Tension Bloc3	
856		ID_HEAD822				32			SMBAT/BRC19 BLOCK4  Voltage		SMBAT/BRC19Tension Bloc4	
857		ID_HEAD823				32			SMBAT/BRC19 BLOCK5  Voltage		SMBAT/BRC19Tension Bloc5	
858		ID_HEAD824				32			SMBAT/BRC19 BLOCK6  Voltage		SMBAT/BRC19Tension Bloc6	
859		ID_HEAD825				32			SMBAT/BRC19 BLOCK7  Voltage		SMBAT/BRC19Tension Bloc7	
860		ID_HEAD826				32			SMBAT/BRC19 BLOCK8  Voltage		SMBAT/BRC19Tension Bloc8	
861		ID_HEAD827				32			SMBAT/BRC19 BLOCK9  Voltage		SMBAT/BRC19Tension Bloc9	
862		ID_HEAD828				32			SMBAT/BRC19 BLOCK10 Voltage		SMBAT/BRC19Tension Bloc10	
863		ID_HEAD829				32			SMBAT/BRC19 BLOCK11 Voltage		SMBAT/BRC19Tension Bloc11	
864		ID_HEAD830				32			SMBAT/BRC19 BLOCK12 Voltage		SMBAT/BRC19Tension Bloc12	
865		ID_HEAD831				32			SMBAT/BRC19 BLOCK13 Voltage		SMBAT/BRC19Tension Bloc13	
866		ID_HEAD832				32			SMBAT/BRC19 BLOCK14 Voltage		SMBAT/BRC19Tension Bloc14	
867		ID_HEAD833				32			SMBAT/BRC19 BLOCK15 Voltage		SMBAT/BRC19Tension Bloc15	
868		ID_HEAD834				32			SMBAT/BRC19 BLOCK16 Voltage		SMBAT/BRC19Tension Bloc16	
869		ID_HEAD835				32			SMBAT/BRC19 BLOCK17 Voltage		SMBAT/BRC19Tension Bloc17	
870		ID_HEAD836				32			SMBAT/BRC19 BLOCK18 Voltage		SMBAT/BRC19Tension Bloc18	
871		ID_HEAD837				32			SMBAT/BRC19 BLOCK19 Voltage		SMBAT/BRC19Tension Bloc19	
872		ID_HEAD838				32			SMBAT/BRC19 BLOCK20 Voltage		SMBAT/BRC19Tension Bloc20	
873		ID_HEAD839				32			SMBAT/BRC19 BLOCK21 Voltage		SMBAT/BRC19Tension Bloc21	
874		ID_HEAD840				32			SMBAT/BRC19 BLOCK22 Voltage		SMBAT/BRC19Tension Bloc22	
875		ID_HEAD841				32			SMBAT/BRC19 BLOCK23 Voltage		SMBAT/BRC19Tension Bloc23	
876		ID_HEAD842				32			SMBAT/BRC19 BLOCK24 Voltage		SMBAT/BRC19Tension Bloc24	
877		ID_HEAD843				32			SMBAT/BRC20 BLOCK1  Voltage		SMBAT/BRC20Tension Bloc1	
878		ID_HEAD844				32			SMBAT/BRC20 BLOCK2  Voltage		SMBAT/BRC20Tension Bloc2	
879		ID_HEAD845				32			SMBAT/BRC20 BLOCK3  Voltage		SMBAT/BRC20Tension Bloc3	
880		ID_HEAD846				32			SMBAT/BRC20 BLOCK4  Voltage		SMBAT/BRC20Tension Bloc4	
881		ID_HEAD847				32			SMBAT/BRC20 BLOCK5  Voltage		SMBAT/BRC20Tension Bloc5	
882		ID_HEAD848				32			SMBAT/BRC20 BLOCK6  Voltage		SMBAT/BRC20Tension Bloc6	
883		ID_HEAD849				32			SMBAT/BRC20 BLOCK7  Voltage		SMBAT/BRC20Tension Bloc7	
884		ID_HEAD850				32			SMBAT/BRC20 BLOCK8  Voltage		SMBAT/BRC20Tension Bloc8	
885		ID_HEAD851				32			SMBAT/BRC20 BLOCK9  Voltage		SMBAT/BRC20Tension Bloc9	
886		ID_HEAD852				32			SMBAT/BRC20 BLOCK10 Voltage		SMBAT/BRC20Tension Bloc10	
887		ID_HEAD853				32			SMBAT/BRC20 BLOCK11 Voltage		SMBAT/BRC20Tension Bloc11	
888		ID_HEAD854				32			SMBAT/BRC20 BLOCK12 Voltage		SMBAT/BRC20Tension Bloc12	
889		ID_HEAD855				32			SMBAT/BRC20 BLOCK13 Voltage		SMBAT/BRC20Tension Bloc13	
890		ID_HEAD856				32			SMBAT/BRC20 BLOCK14 Voltage		SMBAT/BRC20Tension Bloc14	
891		ID_HEAD857				32			SMBAT/BRC20 BLOCK15 Voltage		SMBAT/BRC20Tension Bloc15	
892		ID_HEAD858				32			SMBAT/BRC20 BLOCK16 Voltage		SMBAT/BRC20Tension Bloc16	
893		ID_HEAD859				32			SMBAT/BRC20 BLOCK17 Voltage		SMBAT/BRC20Tension Bloc17	
894		ID_HEAD860				32			SMBAT/BRC20 BLOCK18 Voltage		SMBAT/BRC20Tension Bloc18	
895		ID_HEAD861				32			SMBAT/BRC20 BLOCK19 Voltage		SMBAT/BRC20Tension Bloc19	
896		ID_HEAD862				32			SMBAT/BRC20 BLOCK20 Voltage		SMBAT/BRC20Tension Bloc20	
897		ID_HEAD863				32			SMBAT/BRC20 BLOCK21 Voltage		SMBAT/BRC20Tension Bloc21	
898		ID_HEAD864				32			SMBAT/BRC20 BLOCK22 Voltage		SMBAT/BRC20Tension Bloc22	
899		ID_HEAD865				32			SMBAT/BRC20 BLOCK23 Voltage		SMBAT/BRC20Tension Bloc23	
900		ID_HEAD866				32			SMBAT/BRC20 BLOCK24 Voltage		SMBAT/BRC20Tension Bloc24	
901		ID_TIPS1				32			Search for data			Recherche des données
902		ID_TIPS2				32			Please select row			Selectionner un champ SVP
903		ID_TIPS3				32			Please select column			Selectionner un colonne
904		ID_BATT_TEST				64			    Battery Test Log			Journal de test de la batterie
905		ID_TIPS4				32			Please select column			Selectionner un colonne
906		ID_TIPS5				32			Please select row			Selectionner un champ SVP
907		ID_HEAD867				32			SMBAT/BRC20 BLOCK1  Voltage		SMBAT/BRC20Tension Bloc1	
908		ID_HEAD868				32			SMBAT/BRC20 BLOCK2  Voltage		SMBAT/BRC20Tension Bloc2	
909		ID_HEAD869				32			SMBAT/BRC20 BLOCK3  Voltage		SMBAT/BRC20Tension Bloc3	
910		ID_HEAD870				32			SMBAT/BRC20 BLOCK4  Voltage		SMBAT/BRC20Tension Bloc4	
911		ID_HEAD871				32			SMBAT/BRC20 BLOCK5  Voltage		SMBAT/BRC20Tension Bloc5	
912		ID_HEAD872				32			SMBAT/BRC20 BLOCK6  Voltage		SMBAT/BRC20Tension Bloc6	
913		ID_HEAD873				32			SMBAT/BRC20 BLOCK7  Voltage		SMBAT/BRC20Tension Bloc7	
[tmp.history_eventlog.html:Number]
23

[tmp.history_eventlog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_INDEX				32			Index				Index
2		ID_EQUIP				32			Device Name			Nom de Equipement
3		ID_SIGNAL				32			Signal Name				Nom de l'entree
4		ID_CONTROL_VALUE			32			Value			Valeur 
5		ID_UNIT					32			Unit				Unite
6		ID_TIME					32			Time			Heure
7		ID_SENDER_NAME				32			Sender Name				Nom de l'expediteur
8		ID_FROM					32			From				Du
9		ID_TO					32			To					Au
10		ID_QUERY				32			Query				Requete
11		ID_UPLOAD				32			Upload				Charger vers
12		ID_TIPS					64			Displays the last 500 entries		Afficher les 500 derniers enregistrements
13		ID_QUERY_TYPE				16			Query Type				Type de requete
14		ID_EVENT_LOG				32			Event Log				Journaux des evenements
15		ID_SENDER_TYPE				32			Sender Type				Type d'expediteur
16		ID_CTL_RESULT0				64			Successful				Succes	
17		ID_CTL_RESULT1				64			No Memory				Pas de mémoire		
18		ID_CTL_RESULT2				64			Time Expired			Temps expiré	
19		ID_CTL_RESULT3				64			Failed				Defaillante	
20		ID_CTL_RESULT4				64			Communication Busy			Communication occupée		
21		ID_CTL_RESULT5				64			Control was suppressed.		Contrôle a été supprimer	
22		ID_CTL_RESULT6				64			Control was disabled.		Contrôle a été inhibe
23		ID_CTL_RESULT7				64			Control was canceled.		Contrôle a été efface
24		ID_EVENT_LOG2				32			Event Log				Journal des événements
25		ID_EVENT				64			    Event Log				Journal des événements

[tmp.history_datalog.html:Number]
13

[tmp.history_datalog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_DEVICE				32			Device				Equipement
2		ID_FROM					32			From				Du
3		ID_TO					32			To					Au
4		ID_QUERY				32			Query				Requete
5		ID_UPLOAD				32			Upload				Charger vers
6		ID_TIPS					64			Displays the last 500 entries		Afficher les 500 derniers enregistrements
7		ID_INDEX				32			Index				Index
8		ID_DEVICE1				32			Device Name				Nom de Equipement
9		ID_SIGNAL				32			Signal Name				Nom de l'entree
10		ID_VALUE				32			Value				Valeur
11		ID_UNIT					32			Unit				Unite
12		ID_TIME					32			Time				Heure
13		ID_ALL_DEVICE				32			All Devices				Tous les equipements
14		ID_DATA					32			    Data History Log			Historique des données
15		ID_ALL_DEVICE2				32			    All Devices				Tous les dispositifs

[tmp.setting_charge.html:Number]
20

[tmp.setting_charge.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
6		ID_SIGNAL				32			Signal				Entree
7		ID_VALUE				32			Value				Valeur
8		ID_TIME					32			Time Last Set			Derniere heure configuree
9		ID_SET_VALUE				32			Set Value				Valider valeur
10		ID_SET					32			Set					Valider 
11		ID_SUCCESS				32			Setting Success			Configuration reussie
12		ID_OVER_TIME				32			Login Time Expired			Temps de connexion ecoulee
13		ID_FAIL					32			Setting Fail			Echec de configuration
14		ID_NO_AUTHORITY				32			No privilege			Non autorise
15		ID_UNKNOWN_ERROR			32			Unknown Error			Erreur inconnue
16		ID_PROTECT				32			Write Protected			Protection en ecriture
17		ID_SET1					32			Set					Valider 
18		ID_CHARGE1				32			Battery Charge			Charge batterie
23		ID_NO_DATA				16			No data.				Pas de donnée




[tmp.system_converter.html:Number]
16

[tmp.system_converter.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_RECTIFIER				32			Rectifier				Redresseur
2		ID_CONVERTER				32			Converter				Convertisseur
3		ID_SOLAR				32			Solar Converter				Convertisseur solaire
4		ID_VOLTAGE				32			Average Voltage			Tension moyenne
5		ID_CURRENT				32			Total Current			Courant total
6		ID_CAPACITY_USED			32			System Capacity Used		Taux d'utilisation du Système
7		ID_NUM_OF_RECT				32			Number of Converters		Quantité de redresseurs
8		ID_TOTAL_COMM_RECT			64			Total Converters Communicating	Quantité de redresseurs en communication
9		ID_MAX_CAPACITY				32			Max Used Capacity			Taux d'utilisation maxi 
10		ID_SIGNAL				32			Signal				Entree
11		ID_VALUE				32			Value				Valeur
12		ID_SOLAR1				32			Solar Converter				Convertisseur solaire
13		ID_CURRENT1				32			Total Current			Courant total
14		ID_RECTIFIER1				32			GI Rectifier				Redresseur GI
15		ID_RECTIFIER2				32			GII Rectifier			Redresseur GII
16		ID_RECTIFIER3				32			GIII Rectifier			Redresseur GIII
#//changed by Frank Wu,2/2/30,20140527, for add single converter and single solar settings pages
17		ID_RECT_SET				32			Converter Settings			Configuration convertisseur
18		ID_BACK					16			Back				Back

[tmp.system_solar.html:Number]
16

[tmp.system_solar.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_RECTIFIER				32			Rectifier				Redresseur
2		ID_CONVERTER				32			Converter				Convertisseur
3		ID_SOLAR				32			Solar Converter				Convertisseur solaire
4		ID_VOLTAGE				32			Average Voltage			Tension moyenne
5		ID_CURRENT				32			Total Current			Courant total
6		ID_CAPACITY_USED			32			System Capacity Used		Taux d'utilisation du Système
7		ID_NUM_OF_RECT				64			Number of Solar Converters		Quantité de Convertisseur solaire
8		ID_TOTAL_COMM_RECT			64			Total Solar Converters Communicating	Quantité de Convertisseur solaire en communication
9		ID_MAX_CAPACITY				32			Max Used Capacity			Taux d'utilisation maxi
10		ID_SIGNAL				32			Signal				Entree
11		ID_VALUE				32			Value				Valeur
12		ID_SOLAR1				32			Solar Converter				Convertisseur solaire
13		ID_CURRENT1				32			Total Current			Courant total
14		ID_RECTIFIER1				32			GI Rectifier				Redresseur GI
15		ID_RECTIFIER2				32			GII Rectifier			Redresseur GII
16		ID_RECTIFIER3				32			GIII Rectifier			Redresseur GIII
#//changed by Frank Wu,3/3/30,20140527, for add single converter and single solar settings pages
17		ID_RECT_SET				64			Solar Converter Settings		Configuration convertisseur solaire
18		ID_BACK					16			Back				Back


[tmp.setting_eco.html:Number]
7

[tmp.setting_eco.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Entree
2		ID_VALUE				32			Value				Valeur
3		ID_TIME					32			Time Last Set			Derniere heure configuree
4		ID_SET_VALUE				32			Set Value				Valider valeur
5		ID_SET					32			Set					Valider
6		ID_SET1					32			Set					Valider
7		ID_ECO					16			ECO					ECO
8		ID_NO_DATA				16			No data.				Pas de donnée

[tmp.setting_lvd.html:Number]
6

[tmp.setting_lvd.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Entree
2		ID_VALUE				32			Value				Valeur
3		ID_TIME					32			Time Last Set			Derniere heure configuree
4		ID_SET_VALUE				32			Set Value				Valider valeur
5		ID_SET					32			Set					Valider
6		ID_SET1					32			Set					Valider
7		ID_NO_DATA				16			No data.				Pas de donnée

[tmp.setting_rectifiers.html:Number]
6

[tmp.setting_rectifiers.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Entree
2		ID_VALUE				32			Value				Valeur
3		ID_TIME					32			Time Last Set			Derniere heure configuree
4		ID_SET_VALUE				32			Set Value				Valider valeur
5		ID_SET					32			Set					Valider
6		ID_SET1					32			Set					Valider
7		ID_RECT					32			Rectifiers				Redresseur
8		ID_NO_DATA				16			No data.				Pas de donnée

[tmp.setting_batteryTest.html:Number]
7

[tmp.setting_batteryTest.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Entree
2		ID_VALUE				32			Value				Valeur
3		ID_TIME					32			Time Last Set			Derniere heure configuree
4		ID_SET_VALUE				32			Set Value				Valider valeur
5		ID_SET					32			Set					Valider
6		ID_SET1					32			Set					Valider
7		ID_BATT_TEST				32			Battery Test			Test batteries
8		ID_NO_DATA				16			No data.				Pas de donnée
[tmp.setting_temp.html:Number]
7

[tmp.setting_temp.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Entree
2		ID_VALUE				32			Value				Valeur
3		ID_TIME					32			Time Last Set			Derniere heure configuree
4		ID_SET_VALUE				32			Set Value				Valider valeur
5		ID_SET					32			Set					Valider
6		ID_SET1					32			Set					Valider
7		ID_TEMP					32			Temperature				Temperature
8		ID_NO_DATA				16			No data.				Pas de donnée
[tmp.setting_hybrid.html:Number]
7

[tmp.setting_hybrid.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Entree
2		ID_VALUE				32			Value				Valeur
3		ID_TIME					32			Time Last Set			Derniere heure configuree
4		ID_SET_VALUE				32			Set Value				Valider valeur
5		ID_SET					32			Set					Valider
6		ID_SET1					32			Set					Valider
7		ID_HYBRID				32			Generator			DG
8		ID_NO_DATA				16			No data.				Pas de donnée

[tmp.install_wizard.html:Number]
30

[tmp.install_wizard.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIME_SET				32			Time Settings			Configuration de l'heure
2		ID_GET_TIME				64			Get Local Time from Connected PC			Utiliser l'heure locale du PC connecte		
3		ID_SITE_SET				32			Site Settings			Configuration du site
4		ID_SIGNAL				32			Signal				Entree
5		ID_VALUE				32			Value				Valeur
6		ID_SET_VALUE				32			Set Value				Valider valeur
7		ID_SET					32			Set					Valider
8		ID_SIGNAL1				32			Signal				Entree
9		ID_VALUE1				32			Value				Valeur
10		ID_TIME1				32			Time Last Set			Derniere heure configuree
11		ID_SET_VALUE1				32			Set Value				Valider valeur
12		ID_SET1					32			Set					Valider
13		ID_SITE					32			Site Settings			Configuration du site
14		ID_WIZARD				32			Install Wizard			Installation Wizard
15		ID_SET2					32			Set					Valider
16		ID_SIGNAL_SET				32			Signal Settings			Configuration des entrées
17		ID_SET3					32			Set					Valider
18		ID_SET4					32			Set					Valider
19		ID_CHARGE				32			Battery Charge			Charge batterie
20		ID_ECO					32			ECO					ECO
21		ID_LVD					32			LVD					LVD
22		ID_QUICK_SET				32			Quick Settings			Parametrage rapide		
23		ID_TEMP					32			Temperature				Temperature
24		ID_RECT					32			Rectifiers				Redresseur
25		ID_CONVERTER				32			DC/DC Converters			Convertisseurs DC/DC 
26		ID_BATT_TEST				32			Battery Test			Test batteries
27		ID_TIME_CFG				32			Time Settings			Configuration de l'heure
28		ID_TIPS13				32			Site Name				Nom site
29		ID_TIPS14				32			Site Location			Localisation site
30		ID_TIPS15				32			System Name				Nom Système
31		ID_DEVICE				32			Device Name				Nom d'equipement
32		ID_SIGNAL				32			Signal Name				Nom d'Entree
33		ID_VALUE				32			Value				Valeur
34		ID_SETTING_VALUE			32			Set Value				Valider valeur
35		ID_SITE1				32			Site				Site
36		ID_POWER_SYS				32			System			Système d'energie
37		ID_USER_SET1				32			User Config1			Configuration 1 utilisateur
38		ID_USER_SET2				32			User Config2			Configuration 2 utilisateur
39		ID_USER_SET3				32			User Config3			Configuration 3 utilisateur
#//changed by Frank Wu,1/1/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
40		ID_MPPT					32					Solar				Solaire
41		ID_TIPS1				32			Unknown error.			Erreur inconnue
42		ID_TIPS2				16			Successful.					Succes
43		ID_TIPS3				128			Failed. Incorrect time setting.				Erreur. Format heure incorrecte.
44		ID_TIPS4				128			Failed. Incomplete information.				Erreur. Information incomplète.
45		ID_TIPS5				64			Failed. No privileges.					Erreur. Droits insuffisants.
46		ID_TIPS6				128			Cannot be modified. Controller is hardware protected.	Non modifiable. HW Controleur protégé.
47		ID_TIPS7				256			Secondary Server IP Address is incorrect. \nThe format should be :'nnn.nnn.nnn.nnn'.	Adresse IP secondaire incorrecte. Format est: 'nnn.nnn.nnn.nnn'
48		ID_TIPS8				128			Format error! There is one blank between time and date.					Erreur de format. Mettre espace entre Heure et Date.
49		ID_TIPS9				128					Incorrect time interval. \nTime interval should be a positive integer.			Intervalle de temps incorrect. Doit être un entier positif.
50		ID_TIPS10				128			Date should be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.			La date doit être entre '1970/01/01 00:00:00' and '2038/01/01 00:00:00'
51		ID_TIPS11				128					Please clear the IP before time setting.							Init adresse IP avant config SVP
52		ID_TIPS12				64			Time expired, please login again.						Temps expiré, reconnectez vous.


[tmp.setting_user.html:Number]


[tmp.setting_user.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_USER					32			User Info				Information utilisateur
2		ID_IPV4					32			Ethernet				Ethernet
3		ID_IPV6					32			IPV6				IPV6
4		ID_HLMS_CONF				32			Monitor Protocol			Protocole communication
5		ID_SITE_INFO				32			Site Info				Nom site
6		ID_TIME_CFG				32			Time Sync				Synchronisation horaire
7		ID_AUTO_CONFIG				32			Auto Config				Autoconfiguration
8		ID_OTHER				32			Other Settings			Autre configuration
9		ID_WEB_HEAD				32			User Information			Information utilisateur
10		ID_USER_NAME				32			User Name				Nom d'utilisateur
11		ID_USER_AUTHORITY			32			Privilege				Autorité
12		ID_USER_DELETE				32			Delete				Effacer
13		ID_MODIFY_ADD				64			Add or Modify User			Ajouter ou modifier un utilisateur
14		ID_USER_NAME1				32			User Name				Nom d'utilisateur
15		ID_USER_AUTHORITY1			32			Privilege				Autorité
16		ID_PASSWORD				32			Password				Mot de passe
17		ID_CONFIRM				32			Re-enter Password			        Retaper le mot de passe
18		ID_USER_ADD				32			Add					Ajouter
19		ID_USER_MODIFY				32			Modify				Modifier
20		ID_ERROR0				32			Unknown Error						Erreur inconnue				
21		ID_ERROR1				32			Successful							Succes				
22		ID_ERROR2				64			Failed. Incomplete information.				Erreur. Information incomplete.				
23		ID_ERROR3				64			Failed. The user name already exists.			Erreur. Utilisateur existe déjà.		
24		ID_ERROR4				64			Failed. No privilege.					Erreur. Non autorise.		
25		ID_ERROR5				64			Failed. Controller is hardware protected.			Erreur. Protection materielle du controleur.	
26		ID_ERROR6				64			Failed. You can only change your password.		Erreur. Vous pouvez seulement changer le mot de passe.		
27		ID_ERROR7				64			Failed. Deleting 'admin' is not allowed.			Erreur. Suppression ADMIN impossible.	
28		ID_ERROR8				64			Failed. Deleting a logged in user is not allowed.		Erreur. Suppression utilisateur connecte impossible.
29		ID_ERROR9				128			Failed. The user already exists.			Erreur.Utilisateur existe déjà.
30		ID_ERROR10				128			Failed. Too many users.					Erreur.Trop d'utilisateurs.
31		ID_ERROR11				128			Failed. The user does not exist.				Erreur. Trop d'utilisateurs.			
32		ID_AUTHORITY_LEVEL0			32			Browser				Nagigateur internet
33		ID_AUTHORITY_LEVEL1			32			Operator				Utilisateur
34		ID_AUTHORITY_LEVEL2			32			Engineer				Ingenieur
35		ID_AUTHORITY_LEVEL3			32			Administrator			Administrateur
36		ID_TIPS1				64			Please enter an user name.						SVP. Saisir un nom d'utilisateur.		
37		ID_TIPS2				128			The user name cannot be started or ended with spaces.		Pas d'espace en débout ou fin du nom d'utilisateur.			
38		ID_TIPS3				128			Passwords do not match.						Mot de passe inconnu..		
39		ID_TIPS4				128			Please remember the password entered.				Memoriser le mot de passe SVP.					
40		ID_TIPS5				128			Please enter password.						Saisir le mot de passe SVP.			
41		ID_TIPS6				128			Please remember the password entered.				Memoriser le mot de passe SVP.					
42		ID_TIPS7				128			Already exists. Please try again.					Existe déjà. Nouvel essai.		
43		ID_TIPS8				128			The user does not exist.						Utilisateur inconnu.	
44		ID_TIPS9				128			Please select a user.						SVP - selectionner un utilisateur.			
45		ID_TIPS10				128			The follow characters must not be included in user name, please try again.		Caracteres speciaux impossibles.
46		ID_TIPS11				32			Please enter password.						SVP. Saisir le mot de passe.
47		ID_TIPS12				32			Please Re-enter password.						Veuillez ressaisir le mot de passe
48		ID_RESET				16			Reset								Reset
49		ID_TIPS13				128			Only modifying password can be valid for account 'admin'.		Modification mot de passe par ADMIN seulement .
50		ID_TIPS14				128			User name or password can only be letter or number or '_'.		Format nom utilisateur et mot de passe avec lettres et chiffres ou \"-\" 
51		ID_TIPS15				32			Are you sure to modify		Etes-vous sur de modifier?
52		ID_TIPS16				32			's information?		's information?
53		ID_TIPS17				64			The password must contain at least six characters.		Au moins 6 caracteres pour le mot de passe
54		ID_TIPS18				64			OK to delete?		OK pour effacer?		
55		ID_ERROR12				64			Failed. Deleting 'engineer' is not allowed.			Echec. Suppression \"Ingenieur\" pas autorise		
56		ID_TIPS19				128			Only modifying password can be valid for account 'engineer'.		Modification de mot de passe par Ingénieur seulement		
#//changed by Frank Wu,1/N/15,20140527, for e-mail
57		ID_LCD_LOGIN				32			LCD Access Only		Accès LCD uniquement
58		ID_USER_CONTACT1		16				E-Mail								Email
59		ID_TIPS20				128				Valid E-Mail should contain the char '@', eg. name@vertiv.com			Format email: nom@vertiv.com
60		ID_RADIUS				64			Radius Server Settings			Paramètres du serveur Radius

61		ID_ENABLE_RADIUS			64			Enable Radius			Activer le rayon
62		ID_NASIDENTIFIER			64			NAS-Identifier			Identifiant NAS
63		ID_PRIMARY_SERVER			64			Primary Server			Serveur principal
64		ID_PRIMARY_PORT				64			Primary Port			Port principal
65		ID_SECONDARY_SERVER			64			Secondary Server	        Serveur secondaire
66		ID_SECONDARY_PORT			64			Secondary Port		        Port secondaire
67		ID_SECRET_KEY				64			Secret Key		        Clef secrète
68		ID_CONFIRM_SECRET			32			Confirm			        Confirmer
69		ID_SAVE					32			Save			        sauvegarder

70		ID_ERROR13				64			Enabled Radius Settings!					Paramètres de rayon activés!			

71		ID_ERROR14				64			Disabled Radius Settings!					Paramètres de rayon désactivés!

72		ID_TIPS21				128			Please enter an IP Address.  	Veuillez saisir une adresse IP.

73		ID_TIPS22				128			You have entered an invalid primary server IP! 		Vous avez entré une adresse IP de serveur principal non valide!

74		ID_TIPS23				128			You have entered an invalid secondary server IP!	Vous avez entré une adresse IP de serveur secondaire non valide!

75		ID_TIPS24				128			Are you sure to update Radius server configuration?	Êtes-vous sûr de mettre à jour la configuration du serveur Radius?

76		ID_TIPS25				128			Please Enter Port Number.	Veuillez entrer le numéro de port.

77		ID_TIPS26				128			Please enter secret key!	Veuillez entrer une clé secrète!

78		ID_TIPS27				128			Confirm secret key!		Confirmez la clé secrète!

79		ID_TIPS28				128			Secret key do not match.	La clé secrète ne correspond pas.

80		ENABLE_LCD_LOGIN			32			LCD Access Only			Accès LCD uniquement
81		ID_ACC_LOCK				32			Account Status						Statut du compte
82		ID_ERROR15				64			User account has been locked.				Le compte utilisateur a été verrouillé.
83		ID_LOGIN_LIMIT				32			Limit Login Attempts					Limiter les tentatives de connexion
84		ID_UNLOCK_ACCOUNT			32			Account Locked						Compte bloqué
85		ID_ACC_COUNT_EN				32			Login Attempts						Tentatives de connexion
86		ID_TIPS29				128			LCD Access with Limit Login Attempts or Strong Password is not allowed.		L'accès LCD avec des tentatives de connexion limitées ou un mot de passe fort n'est pas autorisé.
87		ID_ERROR16				64			User does not exist.					L'utilisateur n'existe pas.
88		ID_ERROR17				128			Local login only for a remote user is not allowed.	La connexion locale uniquement pour un utilisateur distant n'est pas autorisée.
89		ID_ERROR18				64			User account has been suspended for 30 minutes.		Le compte utilisateur a été suspendu pendant 30 minutes.
90		ID_TIPS30				64			The password must contain at least 16 characters.	Le mot de passe doit contenir au moins 16 caractères.
91		ID_TIPS31				128			Password must contain at least one upper, lower, numeric and special character.	Le mot de passe doit contenir au moins un caractère supérieur, inférieur, numérique et spécial.
92		ENABLE_STRONG_PASS			32			Strong Password			Mot de passe fort
93		ENABLE_STRONG_PASS1			32			Strong Password			Mot de passe fort
94		ID_TIPS32				128			The password must contain minimum 6 and maximum 13 characters.	Le mot de passe doit contenir au minimum 6 et au maximum 13 caractères.
95		ID_ERROR19				64			Strong password for 'admin' is not allowed.		Le mot de passe fort pour «admin» n'est pas autorisé.

[tmp.setting_ipv4.html:Number]
25

[tmp.setting_ipv4.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_IPV4					32			Ethernet				Ethernet
2		ID_SCUP_IP				32			IP Address				Adresse IP
3		ID_SCUP_MASK				32			Subnet Mask				Masque sous reseau
4		ID_SCUP_GATEWAY				32			Default Gateway			Passerelle par defaut
5		ID_ERROR0				32			Setting Failed.			Echec de configuration.	
6		ID_ERROR1				32			Successful.				Succes.	
7		ID_ERROR2				64			Failed. Incorrect input.				Erreur. saisie incorrecte.
8		ID_ERROR3				64			Failed. Incomplete information.			Erreur. Information incomplete.		
9		ID_ERROR4				64			Failed. No privilege.				Erreur. Non autorise.		
10		ID_ERROR5				128			Failed. Controller is hardware protected.		Erreur. Protection materielle du controleur.
11		ID_ERROR6				32			Failed. DHCP is ON.					Erreur. DHCP activee.
12		ID_TIPS0				64			Set Network Parameter														Configurer les parametres reseau.											
13		ID_TIPS1				128			Units IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171.					Adresse IP incorrecte. \nDoit être au format 'nnn.nnn.nnn.nnn'. Exemple 10.75.14.171.			
14		ID_TIPS2				128			Mask IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 255.255.0.0.						Masque reseau incorrect. \nDoit être au format 'nnn.nnn.nnn.nnn'. Exemple 255.255.0.0.				
15		ID_TIPS3				64			Units IP Address and Mask mismatch.													Adresse IP et Masque sous reseau incompatible.							
16		ID_TIPS4				128			Gateway IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171. Enter 0.0.0.0 for no gateway	Adresse IP passerelle incorrecte. \nShould be in format 'nnn.nnn.nnn.nnn'. Exemple 10.75.14.171. Enter 0.0.0.0 for no gateway .
17		ID_TIPS5				128			Units IP Address, Gateway, Mask mismatch. Enter Address again.									Adresse IP, Masque sous reseau et passerelle incompatible.						
18		ID_TIPS6				64			Please wait. Controller is rebooting.												Patientez SVP. Redemarrage controleur en cours.									
19		ID_TIPS7				128			Parameters have been modified.  Controller is rebooting...										Des parametres ont été modifies. Redemarrage controleur en cours.								
20		ID_TIPS8				64			Controller homepage will be refreshed.												Page principale du controleur sera mise à jour.										
21		ID_TIPS9				128			Confirm the change to the IP address?				Confirmation du changement d'adresse IP?
22		ID_SAVE					16			Save						Sauvegarder
23		ID_TIPS10				32			IP Address Error				Erreur adresse IP
24		ID_TIPS11				32			Subnet Mask Error				Erreur Masque sous reseau
25		ID_TIPS12				32			Default Gateway Error			Erreur adresse IP Passerelle
26		ID_USER					32			Users				Utilisateurs
27		ID_IPV4_1					32			IPV4				IPV4
28		ID_IPV6					32			IPV6				IPV6
29		ID_HLMS_CONF				32			Monitor Protocol			Protocole de communication
30		ID_SITE_INFO				32			Site Info				Nom de site
31		ID_AUTO_CONFIG				32			Auto Config				Autoconfiguration
32		ID_OTHER				32			Alarm Report			Report d'alarme
33		ID_NMS					32			SNMP				SNMP
34		ID_ALARM_SET				32			Alarms		Alarmes		
35		ID_CLEAR_DATA				32			Clear Data			Effacer les donnees		
36		ID_RESTORE_DEFAULT			32			Restore Defaults		Restaurer les valeurs par défaut		
37		ID_DOWNLOAD_UPLOAD			32			SW Maintenance			SW maintenance
38		ID_HYBRID				32			Generator				Groupe electrogène
39		ID_DHCP					16			IPV4 DHCP				IPV4 DHCP
40		ID_IP1					32			Server IP				Server IP
41		ID_TIPS13				16			Loading				Chargement
42		ID_TIPS14				16			Loading				Chargement
43		ID_TIPS15				32			failed, data format error!				Echec, erreur de format
44		ID_TIPS16				32			failed, please refresh the page!			Echec, reafficher la page SVP
45		ID_LANG					16			Language			Langue
#//changed by Frank Wu,2/2/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
46		ID_SHUNT_SET			32				Shunt				Shunt
#//changed by Frank Wu,1/N/35,20140527, for adding the the web setting tab page 'DI'
47		ID_DI_ALARM_SET			32				DI Alarms			Entrées Alarmes
#//changed by Frank Wu,1/1/N,00000000, for add PowerSplit
48		ID_POWER_SPLIT_SET		32				Power Split			Power Split
49		ID_IPV6_1			16				IPV6				IPV6
50		ID_LOCAL_IP			32				Link-Local Address		Link Locale Addresse
51		ID_GLOBAL_IP			32				IPV6 Address			Adresse globale
52		ID_SCUP_PREV			16				Subnet Prefix				Prefixe
53		ID_SCUP_GATEWAY1		16				Default Gateway				Passerelle
54		ID_SAVE1			16				Save				Sauvegarder
55		ID_DHCP1			16				IPV6 DHCP			IPV6 DHCP
56		ID_IP2				32				Server IP			Serveur IP
57		ID_TIPS17			64				Please fill in the correct IPV6 address.	Saisir adresse IPV6 correcte
58		ID_TIPS18			128				Prefix can not be empty or beyond the range.			Prefixe obligatoire
59		ID_TIPS19			64				Please fill in the correct IPV6 gateway.	Saisir adresse IPV6 passerelle


[tmp.setting_time_sync.html:Number]
29

[tmp.setting_time_sync.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIME_HEAD				32			Time Synchronization		Synchronisation horaire
2		ID_ZONE					64			Local Zone(for synchronization with time servers)				Zone horaire(pour la synchronisation avec les serveurs de temps)	
3		ID_GET_ZONE				32			Get Local Zone			Prendre locale locale
4		ID_SELECT1				64			Get time automatically from the following time servers.	Synchroniser l'heure sur les serveurs
5		ID_PRIMARY_SERVER			32			Primary Server IP						Adresse IP prioritaire			
6		ID_SECONDARY_SERVER			32			Secondary Server IP					Adresse IP secondaire			
7		ID_TIMER_INTERVAL			64			Interval to Adjust Time					Periode de reglage horaire			
8		ID_SPECIFY_TIME				32			Specify Time						Saisir l'heure		
9		ID_GET_TIME				32			Get Local Time from Connected PC						idem heure locale			
10		ID_DATE_TIME				32			Date & Time							Date & Heure			
11		ID_SUBMIT				16			Set								Valider				
12		ID_ERROR0				32			Unknown error.						Erreur inconue			
13		ID_ERROR1				16			Successful.							Succes				
14		ID_ERROR2				128			Failed. Incorrect time setting.				Erreur. Heure configuree incorrecte.		
15		ID_ERROR3				128			Failed. Incomplete information.				Erreur. Information incomplete.	
16		ID_ERROR4				64			Failed. No privilege.				Erreur. Non autorisé.			
17		ID_ERROR5				128			Cannot be modified. Controller is hardware protected.	Modification impossible. Protection materielle du controleur.
18		ID_MINUTE				16			min								Minute
19		ID_TIPS0				256			Primary Server IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'.	Adresse IP serveur prioritaire incorrecte \ndoit être au format 'nnn.nnn.nnn.nnn'.		
20		ID_TIPS1				256			Secondary Server IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'.	Adresse IP serveur secondaire incorrecte \ndoit être au format 'nnn.nnn.nnn.nnn'.		
21		ID_TIPS2				128			Incorrect time interval. \nTime interval should be a positive integer.			Interval de temps incorrect. \ndoit être entier positif.				
22		ID_TIPS3				128			Synchronizing time, please wait.								Synchronisation horaire. Veuillez patienter.					
23		ID_TIPS4				128			Incorrect format.  \nPlease enter the date as yyyy/mm/dd,e.g., 2000/09/30		Format incorrect. \nSVP, saisir la date au format: yyyy/mm/dd,e.g., 2000/09/30		
24		ID_TIPS5				128			Incorrect format.  \nPlease enter the time as  'hh:mm:ss', e.g., 8:23:08		Format incorrect. \nSVP, saisir l'heure au format: 'hh:mm:ss', e.g., 8:23:08	
25		ID_TIPS6				128			Date must be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.			Saisie obligatoire entre '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.
26		ID_TIPS7				128			The time server has been set. Time will be set by time server.				L'heure du serveur a été configuree. Mise a jour de l'heure à partir du serveur.
27		ID_TIPS8				128			Incorrect date and time format.								Format date et heure incorrect.
28		ID_TIPS9				128			Please clear the IP address before setting the time.						SVP effacer l'adresse IP avant de configurer l'heure
29		ID_TIPS10				64			Time expired, please login again.						Duree écoulée, SVP reconnectez vous


[tmp.system_inventory.html:Number]
6

[tmp.system_inventory.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_INVENTORY				32			System Inventory			Inventaire système
2		ID_EQUIP				32			Equipment				Equipement
3		ID_MODEL				32			Product Model			Modele produit
4		ID_REVISION				32			Hardware Revision			Rev.Materielle
5		ID_SERIAL				32			Serial Number		Numero serie
6		ID_SOFT_REVISION			32			Software Revision			Rev.Logicielle
7		ID_INVENTORY2				32			    System Inventory			Inventaire du système
[forgot_password.html:Number]
12

[forgot_password.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_FORGET_PASSWD			32			Forgot Password			Mot de passe oublié?
2		ID_FIND_PASSWD				32			Find Password			Mot de passe trouve
3		ID_INPUT_USER				32			Input User Name:			Saisie du nom d'utilisateur
4		ID_FIND_PASSWD1				32			Find Password			Mot de passe trouve
5		ID_RETURN				64			Back to Login Page			Retour vers page demande mot de passe, patientez 
6		ID_ERROR0				64			Unknown error.			Erreur inconnue
7		ID_ERROR1				128			Password has been sent to appointed mailbox.		Mot de passe envoye vers email configure
8		ID_ERROR2				64			No such user.			Utilsateur inconnu.
9		ID_ERROR3				64			No email address.			Pas d'adresse email
10		ID_ERROR4				32			Input User Name			Saisir Nom d'utilisateur
11		ID_ERROR5				32			Fail.				Defaillante
12		ID_ERROR6				32			Error data format.		Erreur de format de donnée
13		ID_USERNAME				64			    User Name			Nom d'utilisateur

[tmp.setting_other.html:Number]
61

[tmp.setting_other.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SMTP					16			SMTP				SMTP
2		ID_EMAIL				32			Email To				Email vers
3		ID_IP					32			Server IP			Serveur IP
4		ID_PORT					32			Server Port				Port serveur
5		ID_AUTHORIY				32			Privilege				Autorite
6		ID_ENABLE				32			Enabled				Valide
7		ID_DISABLE				32			Disabled			Invalide
8		ID_ACCOUNT				32			SMTP Account			Compte SMTP
9		ID_PASSWORD				32			SMTP Password			Mot de passe SMTP
10		ID_ALARM_REPORT				32			Alarm Report Level			Niveau report d&lsquo;alarme
11		ID_OA					32			All Alarms				Toutes les alarmes
12		ID_MA					32			Major and Critical Alarm		Alarmes majeure et critique
13		ID_CA					32			Critical Alarm			Alarme critique
14		ID_NONE					32			None				Aucune
15		ID_SET					32			Set					Valider
18		ID_LOAD					64			Loading data, please wait.		Chargement donnée, patientez SVP
19		ID_VPN					32			Open VPN				VPN ouvert
20		ID_ENABLE1				32			Enabled				Valide
21		ID_DISABLE1				32			Disabled			Invalide
22		ID_VPN_IP				32			VPN IP				IP VPN
23		ID_VPN_PORT				32			VPN Port				Port VPN
24		ID_VPN					16			VPN					VPN
25		ID_LOAD1				64			Loading data, please wait.	Chargement donnée, patientez SVP
26		ID_SET1					32			Set					Valider
27		ID_HANDPHONE1				32			Cell Phone Number 1			Numero de telephone 1
28		ID_HANDPHONE2				32			Cell Phone Number 2			Numero de telephone 2
29		ID_HANDPHONE3				32			Cell Phone Number 3			Numero de telephone 3
30		ID_ALARMLEVEL				32			Alarm Report Level			Niveau report d&lsquo;alarme
31		ID_OA1					64			All Alarms			Toutes les alarmes
32		ID_MA1					64			Major and Critical Alarm		Alarmes majeure et critique
33		ID_CA1					32			Critical Alarm			Alarme critique
34		ID_NONE1				32			None				Aucune
35		ID_SMS					16			SMS					SMS
36		ID_LOAD2				64			Loading data, please wait.	Chargement donnée, patientez SVP
37		ID_SET2					32			Set					Valider
38		ID_ERROR0				64			Unknown error.			Erreur inconnue
39		ID_ERROR1				32			Success				Succes
40		ID_ERROR2				128			Failure, administrator privilege required.	Erreur, niveau administrateur requis
41		ID_ERROR3				128			Failure, the user name already exists.	Erreur, l&lsquo;utilisateur existe déjà
42		ID_ERROR4				128			Failure, incomplete information.		Erreur, information incomplete
43		ID_ERROR5				128			Failure, NSC is hardware protected.		Erreur. Protection materielle du controleur.
44		ID_ERROR6				64			Failure, incorrect password.	Erreur, mot de passe incorrect
45		ID_ERROR7				128			Failure, deleting Admin is not allowed.	Erreur, suppression ADMIN impossible
46		ID_ERROR8				64			Failure, not enough space.			Erreur, espace insuffisant.
47		ID_ERROR9				128			Failure, user already existed.		Erreur, utilisateur existant
48		ID_ERROR10				64			Failure, too many users.			Erreur, Trop d&lsquo;utilisateur
49		ID_ERROR11				64			Failuer, user is not exist.			Erreur, utilisateur inexistant
50		ID_TIPS1				64			Please select the user.			SVP, selectionner un utilisateur
51		ID_TIPS2				64			Please fill in user name.			SVP, saisir le nom utilisateur
52		ID_TIPS3				64			Please fill in password.			SVP, saisir le mot de passe
53		ID_TIPS4				64			Please confirm password.			SVP, confirmer le mot de passe
54		ID_TIPS5				64			Password not consistent.			Mot de passe errone
55		ID_TIPS6				64			Please input an email address in the form name@domain.			SVP, saisir adresse email au format nom@domaine
56		ID_TIPS7				64			Please input the right IP.			Saisir adresse IP correcte SVP
57		ID_TIPS8				16			Loading					Chargement
58		ID_TIPS9				64			Failure, please refresh the page.		Erreur, reafficher la page SVP
59		ID_LOAD3				64			Loading data, please wait...		Chargement de données, patientez SVP
60		ID_LOAD4				64			Loading data, please wait...		Chargement de données, patientez SVP
61		ID_EMAIL1				32			Email From				Email de 
65		ID_TIPS14				64			Only numbers are permitted!			Seuls les chiffres sont autorisés .
66		ID_TIPS10				16			Loading				Loading
67		ID_TIPS12				64			failed, data format error!		failed, data format error!
68		ID_TIPS13				64			failed, please refresh the page!	failed, please refresh the page!


[tmp.setting_hlms_configuration.html:Number]
64

[tmp.setting_hlms_configuration.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_ESRTIPS1				64			Range 0-255				Plage 0-255
2		ID_ESRTIPS2				64			Range 0-600				Plage 0-600
3		ID_ESRTIPS3				64			Main Report Phone Number		Numero de telephone prioritaire
4		ID_ESRTIPS4				64			Second Report Phone Number	Numero de telephone secondaire
5		ID_ESRTIPS5				64			Callback Phone Number		Astreinte telephonique
6		ID_ESRTIPS6				64			Maximum Alarm Report Attempts	Nombre de tentatives maximum
7		ID_ESRTIPS7				64			Call Elapse Time			Duree entre tentatives
8		ID_YDN23TIPS1				64			Range 0-5				Plage 0-5
9		ID_YDN23TIPS2				64			Range 0-300				Plage 0-300
10		ID_YDN23TIPS3				64			First Report Phone Number		Numero telephone N°1
11		ID_YDN23TIPS4				64			Second Report Phone Number		Numero telephone N°2
12		ID_YDN23TIPS5				64			Third Report Phone Number		Numero telephone N°3
13		ID_YDN23TIPS6				64			Times of Dialing Attempt		Duree de la tentative
14		ID_YDN23TIPS7				64			Interval between Two Dialings	Interval entre 2 appels
15		ID_HLMSERRORS1				32			Successful.				Succes
16		ID_HLMSERRORS2				32			Failed.				Erreur
17		ID_HLMSERRORS3				64			Failed. ESR Service was exited.	Erreur. Planning invalide
18		ID_HLMSERRORS4				64			Failed. Invalid parameter.		Erreur. Parametre invalide
19		ID_HLMSERRORS5				64			Failed. Invalid data.		Erreur. Donnee invalide
20		ID_HLMSERRORS6				128			Cannot be modified. Controller is hardware protected.		Erreur. Protection materielle du controleur.
21		ID_HLMSERRORS7				128			Service is busy. Cannot change configuration at this time.		Traitement en cours. Modification de configuration impossible.
22		ID_HLMSERRORS8				128			Non-shared port already occupied.		Port de com déjà utilisé
23		ID_HLMSERRORS9				64			Failed. No privilege.			Erreur. Droits insuffisants.
24		ID_EEM					16			EEM						EEM
25		ID_YDN23				16			YDN23					YDN23
26		ID_MODBUS				16			Modbus					Modbus
27		ID_RESET				64			Valid after Restart				Valide apres redemarrage
28		ID_TYPE					32			Protocol Type				Type de protocole
29		ID_EEM1					16			EEM						EEM
30		ID_RSOC					16			RSOC					RSOC
31		ID_SOCTPE				16			SOC/TPE					SOC/TPE
32		ID_YDN231				16			YDN23					YDN23
33		ID_MEDIA				32			Protocol Media				Protocole de communication
34		ID_232					16			RS-232					RS-232
35		ID_MODEM				16			Modem					Modem
36		ID_ETHERNET				16			IPV4					IPV4
37		ID_ADRESS				32			Self Address				Adressage automatique
38		ID_CALLBACKEN				32			Callback Enabled				Astreinte valide
39		ID_REPORTEN				32			Report Enabled				Report N° valide
40		ID_ALARMREP				32			Alarm Reporting			Rapport d'alarme
41		ID_RANGE				32			Range 1-255					Plage 1-255
42		ID_SOCID				32			SOCID					SOCID
43		ID_RANGE1				32			Range 1-20479				Plage 1-20479
44		ID_RANGE2				32			Range 0-255					Plage 0-255
45		ID_RANGE3				32			Range 0-600s				Plage 0-600s
46		ID_IP1					32			Main Report IP			Adresse IP principale
47		ID_IP2					32			Second Report IP				Adresse IP secondaire
48		ID_SECURITYIP				32			Security Connection IP 1			Adresse securisee IP1
49		ID_SECURITYIP2				32			Security Connection IP 2			Adresse securisee IP2
50		ID_LEVEL				32			Safety Level				Niveau de securite
51		ID_TIPS1				64			All commands are available.			Toutes commandes autorisées
52		ID_TIPS2				128			Only read commands are available.		Commande de lecture validee
53		ID_TIPS3				128			Only the Call Back command is available.	Appel d'astreinte valide 
54		ID_TIPS4				64			Confirm to change the protocol to		Confirmer la modification de protocole
55		ID_SAVE					32			Save					Sauvegarder
56		ID_TIPS5				32			Switch Fail					Switch en defaut
57		ID_CCID					16			CCID					CCID
58		ID_TYPE1				32			Protocol					Protocole
59		ID_TIPS6				32			Port Parameter				Parametres du port
60		ID_TIPS7				128			Port Parameters & Phone Number	Parametres du port serie et numero de telephone
61		ID_TIPS8				32			TCP/IP Port Number				Numero de port TCP/IP
62		ID_TIPS9				128			Set successfully, controller is restarting, please wait		Configuration reussie, redemarrage controleur, patientez SVP
63		ID_TIPS10				64			Main Report Phone Number Error.		Main Report Phone Number Error.
64		ID_TIPS11				64			Second Report Phone Number Error.		Second Report Phone Number Error.
65		ID_ERROR10				32			Unknown error.			Erreur inconnue
66		ID_ERROR11				32			Successful.				Succes.
67		ID_ERROR12				32			Failed.				Echec.
68		ID_ERROR13				64			Insufficient privileges for this function.		Droits insuffisants pour cette fonction		
69		ID_ERROR14				32			No information to send.		Pas d'information envoyées
70		ID_ERROR15				64			Failed. Controller is hardware protected.		Echec. Protection materielle du controleur
71		ID_ERROR16				64			Time expired. Please login again.				Temps expiré. Reconnectez vous SVP
72		ID_TIPS12				64			Network Error		Erreur réseau
73		ID_TIPS13				64			CCID not recognized. Please enter a number.		CCID inconnue. Entrer le numero SVP		
74		ID_TIPS14				64			CCID					CCID
75		ID_TIPS15				64			Input Error					Erreur de saisie
76		ID_TIPS16				64			SOCID not recognized. Please enter a number.		SOCID inconnue. Entrer le numero SVP
77		ID_TIPS17				64			SOCID					SOCID
78		ID_TIPS18				64			Cannot be zero. Please enter a different number.		Doit etre different de 0. Entrer un numero different		
79		ID_TIPS19				64			SOCID			SOCID
80		ID_TIPS20				64			Input error.		Erreur de saisie.
81		ID_TIPS21				64			Unable to recognize maximum number of alarms.		Nombre d'alarmes inconnues		
82		ID_TIPS22				64			Unable to recognize maximum number of alarms.		Nombre d'alarmes inconnues		
83		ID_TIPS23				64			Maximum call elapse time is error.		Echec, tempo appel maximum ecoule		
84		ID_TIPS24				64			Maximum call elapse time is input error.		Erreur de valeur pour tempo appel maximum		
85		ID_TIPS25				64			Report IP not recognized.		IP de report inconnue		
86		ID_TIPS26				64			Report IP not recognized.		IP de report inconnue		
87		ID_TIPS27				64			Security IP not recognized.	IP de securite inconnue		
88		ID_TIPS28				64			Security IP not recognized.	IP de securite inconnue		
89		ID_TIPS29				64			Port input error.		Erreur de saisie de port		
90		ID_TIPS30				64			Port input error.		Erreur de saisie de port		
91		ID_IPV6					16			IPV6			IPV6
92		ID_TIPS31				32			[IPV6 Addr]:Port		[IPV6 Adr]:Port
93		ID_TIPS32				32			[IPV6 Addr]:Port		[IPV6 Adr]:Port
94		ID_TIPS33				32			[IPV6 Addr]:Port		[IPV6 Adr]:Port
95		ID_TIPS34				32			[IPV6 Addr]:Port		[IPV6 Adr]:Port
96		ID_TIPS35				32			IPV4 Addr:Port		IPV4 Addr:Port
97		ID_TIPS36				32			IPV4 Addr:Port		IPV4 Addr:Port
98		ID_TIPS37				32			IPV4 Addr:Port		IPV4 Addr:Port
99		ID_TIPS38				32			IPV4 Addr:Port		IPV4 Addr:Port
100		ID_TIPS39				64			Callback Phone Number Error.		Callback Phone Number Error.
101		ID_TL1					16			TL1					TL1
102		ID_PORT_ACT				32			Port Activation			Activation Port
103		ID_DISABLE				16			Disabled				Invalide
104		ID_ENABLE				16			Enabled				Valide
105		ID_ETHERNET1				16			IPV4				IPV4
106		ID_IPV6_1				16			IPV6			IPV6
107		ID_TIPS40				16			Access Port				Port Access
108		ID_RANGE4				32			Range 1024-65534					Plage 1024-65534
109		ID_TIPS41				32			Port Keep-alive			Port Keep-alive
110		ID_DISABLE1				16			Disabled				Invalide
111		ID_ENABLE1				16			Enabled				Valide
112		ID_TIPS42				32			Session Timeout			Expiration de la session
113		ID_RANGE5				32			Range 0-1440 minutes		Plage 0-1440 minutes
114		ID_TIPS43				32			Auto Login User			Auto Login Utilisateur
115		ID_TIPS44				32			System Identifier		System Identifier
116		ID_MEDIA1				32			Protocol Media			Protocole médias
117		ID_NA					16			None				Aucun
118		ID_TIPS45				32			Access Port Error			Port Erreur d'accès
119		ID_TIPS46				32			Session Timeout Error		Session Timeout Error
120		ID_TIPS47				32			System Identifier Error		Identifier System Error
121		ID_TL1_ERRORS1				32			Successful.				Succes.
122		ID_TL1_ERRORS2				32			Configuration fail.			Configuration sûr.
123		ID_TL1_ERRORS3				64			Failed. Service was exited.		Échoué. Le service a été sorti.
124		ID_TL1_ERRORS4				32			Invalid parameters.			Paramètres invalides
125		ID_TL1_ERRORS5				32			Invalid parameters.			Paramètres invalides
126		ID_TL1_ERRORS6				64			Fail to write to Flash.		Ne pas écrire à Flash.
127		ID_TL1_ERRORS7				32			Service is busy.			Le service est occupé
128		ID_TL1_ERRORS8				64			Port has already been occupied.	Port déjà utilisé.
129		ID_TL1_ERRORS9				64			Port has already been occupied.	Port déjà utilisé.
130		ID_TL1_ERRORS10				64			Port has already been occupied.	Port déjà utilisé.
131		ID_TL1_ERRORS11				64			Port has already been occupied.	Port déjà utilisé.
132		ID_TL1_ERRORS12				64			The TL1 feature is not available.	La fonction de TL1 ne sont pas disponibles.
133		ID_TIPS48				64			    All commands are available.			Toutes commandes autorisées.
134		ID_TIPS49				128			    Only read commands are available.		Commande de lecture validee.
135		ID_TIPS50				128			    Only the Call Back command is available.	Appel d'astreinte valide.
136		ID_NA2					16			None				Aucun
[tmp.setting_nms.html:Number]
64

[tmp.setting_nms.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TRAP_LEVEL1				16			Not Used				Non Utilisée
2		ID_TRAP_LEVEL2				32			All Alarms				Toutes les alarmes
3		ID_TRAP_LEVEL3				16			Major Alarms			Alarmes Majeures		
4		ID_TRAP_LEVEL4				32			Critical Alarms			Alarmes Critiques		
5		ID_NMS_TRAP				32			Accepted Trap Level			Niveau de Trap autorise		
6		ID_SET_TRAP				16			Set					Valider
7		ID_NMSV2_CONF				32			NMSV2 Configuration			NMSV2 configuration	
8		ID_NMS_IP				32			NMS IP				NMS IP
9		ID_NMS_PUBLIC				32			Public Community			Communautée Publique		
10		ID_NMS_PRIVATE				32			Private Community			Communautée Privée		
11		ID_TRAP_ENABLE				16			Trap Enabled				Trap actives		
12		ID_DELETE				16			Delete				Effacer
13		ID_NMS_IP1				32			NMS IP				NMS IP
14		ID_NMS_PUBLIC1				32			Public Community			Communautée Publique		
15		ID_NMS_PRIVATE1				32			Private Community			Communautée Privée		
16		ID_TRAP_ENABLE1				16			Trap Enabled				Trap actives		
17		ID_DISABLE				16			Disabled				Invalide
18		ID_ENABLE				16			Enabled				Valide
19		ID_ADD					16			Add					Ajouter
20		ID_MODIFY				16			Modify				Modifier
21		ID_RESET				16			Reset				Reset
22		ID_NMSV3_CONF				32			NMSV3 Configuration			NMSV3 configuration	
23		ID_NMS_TRAP_LEVEL0			32			NoAuthNoPriv			NoAuthNoPriv		
24		ID_NMS_TRAP_LEVEL1			32			AuthNoPriv				AuthNoPriv		
25		ID_NMS_TRAP_LEVEL2			32			AuthPriv				AuthPriv		
26		ID_NMS_USERNAME				32			User Name				Nom d&lsquo;utilisateur
27		ID_NMS_DES				32			Priv Password DES			Mot de passe Priv DES		
28		ID_NMS_MD5				32			Auth Password MD5			Mot de passe Auth MD5		
29		ID_V3TRAP_ENABLE			16			Trap Enabled				Trap actives
30		ID_NMS_TRAP_IP				16			Trap IP				Trap IP
31		ID_NMS_V3TRAP_LEVE			32			Trap Security Level			Niveau de securite Trap		
32		ID_V3DELETE				16			Delete				Effacer
33		ID_NMS_USERNAME1			32			User Name				Nom d&lsquo;utilisateur
34		ID_NMS_DES1				32			Priv Password DES			Mot de passe Priv DES		
35		ID_NMS_MD51				32			Auth Password MD5			Mot de passe Auth MD5		
36		ID_V3TRAP_ENABLE1			16			Trap Enabled				Trap actives
37		ID_NMS_TRAP_IP1				16			Trap IP				Trap IP
38		ID_NMS_V3TRAP_LEVE1			32			Trap Security Level			Niveau de securite Trap		
39		ID_DISABLE1				16			Disabled				Invalide
40		ID_ENABLE1				16			Enabled				Valide
41		ID_ADD1					16			Add					Ajouter
42		ID_MODIFY1				16			Modify				Modifier
43		ID_RESET1				16			Reset				Reset
44		ID_ERROR0				32			Unknown error.			Erreur inconnue.
45		ID_ERROR1				32			Successful.				Succes.
46		ID_ERROR2				64			Failed. Controller is hardware protected.	Echec. Protection materielle du controleur
47		ID_ERROR3				64			SNMPV3 functions are not enabled.			Fonctions SNMPV3 invalides		
48		ID_ERROR4				64			Insufficient privileges for this function.		Pas les droits pour cette fonction		
49		ID_ERROR5				128			Failed. Maximum number (16 for SNMPV2 accounts, 5 for SNMPV3 accounts) exceeded.		Echec. Nombre trop grand (16 comptes pour SNMPV2, 5 pour SNMPV3)		
50		ID_TIPS1				64			Please select an NMS before continuing.		Selectionner le NMS avant de continuer SVP		
51		ID_TIPS2				64			IP address is not valid.		Adresse IP inconnue		
52		ID_TIPS3				128			Only numbers, letters and '_' are permitted, can't be over 16 characters or blanks included.		Format: nombre, lettre, et &lsquo;_&rsquo; autorisés, 16 caractères maxi, espaces compris	
53		ID_TIPS4				128			Only numbers, letters and '_' are permitted, the length of characters is between 8 and 16.	Entre 8 et 16 caracteres (espaces compris). Nombre, Lettre et &lsquo;_&rsquo; autorises.		
54		ID_NMS_PUBLIC2				32			Public Community			Public Community		
55		ID_NMS_PRIVATE2				32			Private Community			Private Community		
56		ID_NMS_USERNAME2			32			User Name				Nom d&lsquo;utilisateur
57		ID_NMS_DES2				32			Priv Password DES			Mot de passe Priv DES		
58		ID_NMS_MD52				32			Auth Password MD5			Mot de passe Auth MD5		
59		ID_LOAD					16			Loading				Chargement		
60		ID_LOAD1				16			Loading				Chargement		
61		ID_TIPS5				64			Failed, data format error.		Echec, erreur de format de données
62		ID_TIPS6				64			Failed. Please refresh the page.	Echec, rafraichir la page SVP
63		ID_DISABLE2				16			Disabled				Invalide
64		ID_ENABLE2				16			Enabled				Valide
65		ID_DISABLE3				16			Disabled				Invalide
66		ID_ENABLE3				16			Enabled				Valide
67		ID_IPV6					64			IPV6 address is not valid.		Adresse IPV6 n&lsquo;est pas valide
68		ID_IPV6_ADDR				64			IPV6				IPV6
69		ID_IPV6_ADDR1				64			IPV6				IPV6
70		ID_DISABLE2				16			    Disabled				Invalide
71		ID_DISABLE3				16			    Disabled				Invalide
72		ID_DISABLE4				16			Disabled				Invalide

[tmp.setting_system_rect.html:Number]
64

[tmp.setting_system_rect.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Entree
2		ID_VALUE				32			Value				Valeur
3		ID_TIME					32			Time Last Set			Derniere heure configuree
4		ID_SET_VALUE				32			Set Value				Valider valeur
5		ID_SET					32			Set					Valider
6		ID_SET1					32			Set					Valider

[tmp.system_dg.html:Number]
64

[tmp.system_dg.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Entree
2		ID_VALUE				32			Value				Valeur
3		ID_DG					32			DG					DG
4		ID_SIGNAL1				32			Signal				Entree
5		ID_VALUE1				32			Value				Valeur
6		ID_NO_DATA				32			No Data				Pas de donnée


[tmp.system_ac.html:Number]
64

[tmp.system_ac.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Entree
2		ID_VALUE				32			Value				Valeur
3		ID_SIGNAL1				32			Signal				Entree
4		ID_VALUE1				32			Value				Valeur
5		ID_SMAC					32			SMAC				SMAC
6		ID_AC_METER				32			AC Meter				Mesure AC
7		ID_NO_DATA				32			No Data				Pas de donnée
8		ID_AC					32			AC					AC
9		ID_AC_UNIT				32			AC Unit				Unité AC

[tmp.system_smio.html:Number]
64

[tmp.system_smio.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Entree
2		ID_VALUE				32			Value				Valeur
3		ID_SIGNAL1				32			Signal				Entree
4		ID_VALUE1				32			Value				Valeur
5		ID_NO_DATA				32			No Data				Pas de donnée

[tmp.setting_alarm_equipment.html:Number]
1

[tmp.setting_alarm_equipment.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS					128			Please select equipment type	Selectionner un type d&lsquo;equipement SVP		

[tmp.setting_alarm_content.html:Number]
20

[tmp.setting_alarm_content.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS1					32		New Alarm Level			Nouveau niveau d&lsquo;alarme	
2		ID_TIPS2					32		Please select			Selectionner SVP		
3		ID_NA						16		NA					NA
4		ID_OA						16		OA					OA
5		ID_MA						16		MA					MA
6		ID_CA						16		CA					CA
7		ID_TIPS3					32		New Relay Number			Nouveau numero de relai		
8		ID_TIPS4					32		Please select			Selectionner SVP		
9		ID_SET						16		Set					Valider
10		ID_INDEX					16		Index				Index
11		ID_NAME						16		Name				Nom
12		ID_LEVEL					32		Alarm Level				Niveau d&lsquo;alarme
13		ID_ALARMREG					32		Relay Number			Nombre de relais		
14		ID_MODIFY					32		Modify				Modifier
15		ID_NA1						16		NA					NA
16		ID_OA1						16		OA					OA
17		ID_MA1						16		MA					MA
18		ID_CA1						16		CA					CA
19		ID_MODIFY1					32		Modify				Modifier
20		ID_TIPS5					32		No Data				Pas de donnée
21		ID_NA2						16		None				Aucun
22		ID_NA3						16		None				Aucun
23		ID_NA4						16		NA				Aucun
24		ID_TIPS6					32		    Please select				Selectionner SVP
25		ID_TIPS7					32		    Please select			Selectionner SVP
26		ID_NA5						16		None				Aucun	

[tmp.setting_converter.html:Number]
6

[tmp.setting_converter.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Entree
2		ID_VALUE				32			Value				Valeur
3		ID_TIME					32			Time Last Set			Derniere heure configuree
4		ID_SET_VALUE				32			Set Value				Valider valeur
5		ID_SET					32			Set					Valider
6		ID_SET1					32			Set					Valider
7		ID_CONVERTER				32			Converter				Convertisseur
8		ID_NO_DATA				16			No data.				Pas de donnée

[tmp.setting_clear_data.html:Number]
14

[tmp.setting_clear_data.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_CLEAR				32			Clear Data				Effacer les données
2		ID_HISTORY_ALARM			64			Alarm History			Historique des alarmes		
3		ID_HISTORY_DATA				64			Data History			Historique des donnees		
4		ID_HISTORY_CONTROL			64			Event Log			Journal des evenements		
5		ID_HISTORY_BATTERY			64			Battery Test Log			Journal des tests batteries
6		ID_HISTORY_DISEL_TEST			64			Diesel Test Log			Journal des tests GE		
7		ID_CLEAR1				16			Clear				Effacer		
8		ID_ERROR0				64			Failed to clear data.				Echec d'effacement		
9		ID_ERROR1				64			Cleared.						Efface		
10		ID_ERROR2				64			Unknown error.					Erreur inconnue.			
11		ID_ERROR3				128			Failed. No privilege.				Echec. Pas les droits.		
12		ID_ERROR4				64			Failed to communicate with the controller.		Defaut de communication avec le controleur.		
13		ID_ERROR5				64			Failed. Controller is hardware protected.		Echec. Protection materielle du controleur .
14		ID_TIPS1				64			Clearing...please wait.				Initialisation…patientez SVP		
15		ID_HISTORY_ALARM2			64			Alarm History			Historique des alarmes
[tmp.setting_upload_download.html:Number]
14

[tmp.setting_upload_download.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_HEAD					128			Restore Factory Defaults						Restaurer les parametres usine		
2		ID_TIPS0				128			Restore default configuration? The system will reboot.		Restaurer la configuration par defaut? Le Système redemarrera.		
3		ID_RESTORE_DEFAULT			64			Restore Defaults							retour aux parametres par defaut		
4		ID_TIPS1				128			Restore default config will cause system to reboot, are you sure?	Restaurer la configuration par defaut? Le Système redemarrera.		
5		ID_TIPS2				128			Cannot be restored. Controller is hardware protected.		Restauration impossible. Hardware Controller protege.		
6		ID_TIPS3				128			Are you sure you want to reboot the controller?			Etes-vous sur de redemarrer le controleur?		
7		ID_TIPS4				128			Failed. No privilege.						Echec. Pas les droits.		
8		ID_START_SCUP				32			Reboot controller							Redemarrage du controleur		
9		ID_TIPS5				64			Restoring defaults...please wait.					Restauration des parametres par defaut... Patientez SVP		
10		ID_TIPS6				64			Rebooting controller...please wait.					Reboot controleur…Patientez SVP		
11		ID_HEAD1				64			Upload/Download							Chargement / Rapatriement fichier		
12		ID_TIPS7				128			Upload/Download needs to stop the Controller. Do you want to stop the Controller?		Arret controleur pendant Updload/Download. Voulez vous arreter le controleur?		
13		ID_CLOSE_SCUP				32			Stop Controller				Arret du controleur		
14		ID_HEAD2				64			Upload/Download File			Chargement / Rapatriement fichier		
15		ID_TIPS8				256			Caution: Only the file SettingParam.run or files with extension .tar or .tar.gz can be downloaded. If the downloaded file is NOT correct, the Controller will run abnormally. You must hit the START CONTROLLER button before leaving this screen.		Avertissement: format de fichier à télécharger est "SettingParam.run" ou fichier avec extension .tar or .tar.gz
16		ID_FILE1				32			Select File					Selectionner un fichier		
17		ID_TIPS9				32			Browse...					Parcourir…		
18		ID_DOWNLOAD				64			Download to Controller			Chargement vers controleur		
19		ID_CONFIG_TAR				32			Configuration Package			Package Configuration		
20		ID_LANG_TAR				32			Language Package				Package Langue		
21		ID_UPLOAD				64			Upload to Computer				Rapatriement vers PC		
22		ID_STARTSCUP				64			Start Controller				Demarrage Controleur		
23		ID_STARTSCUP1				64			Start Controller				Demarrage Controleur		
24		ID_TIPS10				64			Stop controller...please wait.		Arret controleur…Patientez SVP		
25		ID_TIPS11				64			A file name is required.		Nom de fichier demande		
26		ID_TIPS12				64			Are you sure you want to download?		Etes-vous sur de vouloir charger le controleur?		
27		ID_TIPS13				64			Downloading...please wait.			Chargement…Patientez SVP		
28		ID_TIPS14				128			Incorrect file type or file name contains invalid characters.			Type de fichier incorrect ou mauvais nom.		
29		ID_TIPS15				32			please wait...				patientez SVP...
30		ID_TIPS16				128			Are you sure you want to start the controller?		Etes-vous sur de vouloir demarrer le controleur?		
31		ID_TIPS17				64			Controller is rebooting...			Contoleur en cours de demarrage…		
32		ID_FILE0				32			File in controller			Fichier dans controleur		
33		ID_CLOSE_ACU				32			Auto Config												Autoconfiguration								
34		ID_ERROR0				32			Unknown error.												Erreur inconnue.									
35		ID_ERROR1				128			Auto configuration started. Please wait.									Auto configuration started. Please wait.						
36		ID_ERROR2				64			Failed to get.												Erreur pendant requete.									
37		ID_ERROR3				64			Insufficient privileges to stop the controller.								Droits insuffisants pour arreter le controleur	
38		ID_ERROR4				64			Failed to communicate with the controller.									Failed to communicate with the controller.							
39		ID_AUTO_CONFIG1				256			The controller will auto configure then restart. Please wait 2-5 minutes.	Le controleur s'auto configurera au demarrage. Patientez 2-5 min SVP		
40		ID_TIPS					128			This function will automatically configure SM units and modbus devices that have been connected to RS485 bus.	Cette function configurera automatiquement les unitées SM et modbus qui sont connectés au RS485.									
41		ID_HEAD3				32			Auto Config											Autoconfiguration
42		ID_TIPS18				64			Confirm auto configuration?										Vous confirmez l'auto configuration?
50		ID_TIPS3				128			Set successfully, controller is restarting, please wait		Configuration reussie, redemarrage controleur, patientez .
51		ID_TIPS4				16			seconds.				Seconde!
52		ID_TIPS5				64			Returning to login page. Please wait.		Retournez au menu principal. Patientez SVP		
59		ID_ERROR5				32			Unknown error.												Erreur inconnue.											
60		ID_ERROR6				128			Controller was stopped successfully. You can upload/download the file.						Arret controleur		
61		ID_ERROR7				64			Failed to stop the controller.										Echec de l'arret du controleur		
62		ID_ERROR8				64			Insufficient privileges to stop the controller.								Droits insuffisants pour arreter le controleur		
63		ID_ERROR9				64			Failed to communicate with the controller.									Failed to communicate with the controller.			
64		ID_GET_PARAM				32			Retrieve SettingParam.tar											Recuperation de SettingParam.run		
65		ID_TIPS19				128			Retrieve the current settings of the controller's adjustable parameters.					Recuperation de la configuration des parametres du controleur		
66		ID_RETRIEVE				32			Retrieve File												Recuperation du fichier		
67		ID_ERROR10				32			Unknown error.												Erreur inconnue.	
68		ID_ERROR11				128			Retrieval successful.											Recuperation reussie		
69		ID_ERROR12				64			Failed to get.												Erreur pendant requete.
70		ID_ERROR13				64			Insufficient privileges to stop the controller.									Droits insuffisants pour arreter le controleur		
71		ID_ERROR14				64			Failed to communicate with the controller.								Echec de communication avec le controleur.	
72		ID_TIPS20				32			Please wait...												Patientez SVP…
73		ID_DOWNLOAD_ERROR0			32			Unknown error.								Erreur inconnue.	
74		ID_DOWNLOAD_ERROR1			64			File downloaded successfully.						Download réussi.		
75		ID_DOWNLOAD_ERROR2			64			Failed to download file.							Download échoué		
76		ID_DOWNLOAD_ERROR3			64			Failed to download, the file is too large.				Download échoué, fichier trop grand
77		ID_DOWNLOAD_ERROR4			64			Failed. No privileges.							Erreur. Droits insuffisants.	
78		ID_DOWNLOAD_ERROR5			64			Controller started successfully.						Démarrage controleur réussi.	
79		ID_DOWNLOAD_ERROR6			64			File downloaded successfully.						Download réussi.	
80		ID_DOWNLOAD_ERROR7			64			Failed to download file.							Download échoué		
81		ID_DOWNLOAD_ERROR8			64			Failed to upload file.							Upload échoué	
82		ID_DOWNLOAD_ERROR9			64			File downloaded successfully.						Download réussi.	
83		ID_DOWNLOAD_ERROR10			64			Failed to upload file. Hardware is protected.				Upload échoué, protection HW
84		ID_CONFIG_TAR2				32			Configuration Package			Package Configuration
85		ID_DIAGNOSE					32			Retrieve Diagnostics Package		Récupérer le package de diagnostics
86		ID_TIPS21					128			Retrieve a diagnostics package to help troubleshoot controller issues			Récupérer un package de diagnostics pour aider à résoudre les problèmes de contrôleur
87		ID_RETRIEVE1				32			Retrieve File						Récupérer un fichier

[tmp.setting_power_system.html:Number]
14

[tmp.setting_power_system.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Entree
2		ID_VALUE				32			Value				Valeur
3		ID_TIME					32			Time Last Set			Derniere heure configuree
4		ID_SET_VALUE				32			Set Value				Valider valeur
5		ID_SET					32			Set					Valider
6		ID_SET1					32			Set					Valider
7		ID_NO_DATA				16			No data.				Pas de donnée
#8		ID_EIB					32			EIB Equipment				Equipement EIB		
#9		ID_SMDU					32			SMDU Equipment				Equipement SMDU		
#10		ID_LVD_GROUP				32			LVD Group				Groupe LVD

#changed by Frank Wu,1/1/32,20140312, for adding new web pages to the tab of "power system"
11		ID_SYSTEM_GROUP				32				Power System			Système d'energie
12		ID_AC_GROUP					32				AC Group		Groupe AC
13		ID_AC_UNIT					32				AC Equipment		Equipement AC
14		ID_ACMETER_GROUP			32				ACMeter Group			Groupe ACMeter 
15		ID_ACMETER_UNIT				32				ACMeter Equipment		Equipement ACMeter
16		ID_DC_UNIT					32				DC Equipment		Equipement DC
17		ID_DCMETER_GROUP			32				DCMeter Group			Groupe DCMeter
18		ID_DCMETER_UNIT				32				DCMeter Equipment		Equipement DCMeter
19		ID_LVD_GROUP				32				LVD Group			Groupe LVD
20		ID_DIESEL_GROUP				32				Diesel Group			Groupe Diesel
21		ID_DIESEL_UNIT				32				Diesel Equipment		Equipement Diesel
22		ID_FUEL_GROUP				32				Fuel Group				Groupe Fuel
23		ID_FUEL_UNIT				32				Fuel Equipment			Equipement Fuel
24		ID_IB_GROUP					32				IB Group		Groupe IB
25		ID_IB_UNIT					32				IB Equipment		Equipement IB
26		ID_EIB_GROUP				32				EIB Group			Groupe EIB
27		ID_EIB_UNIT					32				EIB Equipment		Equipement EIB
28		ID_OBAC_UNIT				32				OBAC Equipment			Equipement OBAC 
29		ID_OBLVD_UNIT				32				OBLVD Equipment			Equipement OBLVD 
30		ID_OBFUEL_UNIT				32				OBFuel Equipment		Equipement OBFuel
31		ID_SMDU_GROUP				32				SMDU Group			Groupe SMDU
32		ID_SMDU_UNIT				32				SMDU Equipment			Equipement SMDU
33		ID_SMDUP_GROUP				32				SMDUP Group			Groupe SMDUP
34		ID_SMDUP_UNIT				32				SMDUP Equipment			Equipement SMDUP
35		ID_SMDUH_GROUP				32				SMDUH Group			Groupe SMDUH
36		ID_SMDUH_UNIT				32				SMDUH Equipment			Equipement SMDUH
37		ID_SMBRC_GROUP				32				SMBRC Group			Groupe SMBRC
38		ID_SMBRC_UNIT				32				SMBRC Equipment			Equipement SMBRC
39		ID_SMIO_GROUP				32				SMIO Group			Groupe SMIO
40		ID_SMIO_UNIT				32				SMIO Equipment			Equipement SMIO
41		ID_SMTEMP_GROUP				32				SMTemp Group			Groupe SMTemp
42		ID_SMTEMP_UNIT				32				SMTemp Equipment		Equipement SMTemp
43		ID_SMAC_UNIT				32				SMAC Equipment			Equipement SMAC
44		ID_SMLVD_UNIT				32				SMDU-LVD Equipment			Equipement SMDU-LVD
45		ID_LVD3_UNIT				32				LVD3 Equipment			Equipement LVD3
46		ID_SELECT_TYPE				48				Please select equipment type	Selectionner type Eqt SVP
47		ID_EXPAND				32				Expand				Ajouter
48		ID_COLLAPSE				32				Collapse			Reduire
49		ID_OBBATTFUSE_UNIT			32				OBBattFuse Equipment		OBBattFusible Eqt
50		ID_NARADA_BMS_UNIT			32				BMS			BMS
51		ID_NARADA_BMS_GROUP			32				BMS Group		Groupe BMS

[tmp.setting_language.html:Number]
14

[tmp.setting_language.html]
#Sequence ID  RES               MAX_LEN_OF_STRING     ENGLISH           LOCAL
1		ID_TIPS1		16		Local Language									Langue Locale
2		ID_TIPS2		64		Please select language								Selectionnez la langue SVP
3		ID_GERMANY_TIP		64		Kontroller wird mit deutscher Sprache neustarten.				Kontroller wird mit deutscher Sprache neustarten.
4		ID_GERMANY_TIP1		128		Ausgewählte Sprache identisch mit lokaler Sprache, keine Änderung notwendig.	Ausgewählte Sprache identisch mit lokaler Sprache, keine Änderung notwendig.
5		ID_SPAISH_TIP		64		La controladora se va a reiniciar en Español.					La controladora se va a reiniciar en Español.
6		ID_SPAISH_TIP1		64		El idioma seleccionado es el mismo. No se necesita cambiar.			El idioma seleccionado es el mismo. No se necesita cambiar.
7		ID_FRANCE_TIP		64		Le controleur  redemarrera en Français.						Le controleur  redemarrera en Français.
8		ID_FRANCE_TIP1		128		La langue selectionnée est la langue locale, pas besoin de changer.		La langue selectionnée est la langue locale, pas besoin de changer.
9		ID_ITALIAN_TIP		64		Il controllore ripartirà in Italiano.						Il controllore ripartirà in Italiano.
10		ID_ITALIAN_TIP1		128		La Lingua selezionata è lo stessa della lingua locale, non serve cambiare.	La Lingua selezionata è lo stessa della lingua locale, non serve cambiare.
11		ID_RUSSIAN_TIP		128		Контроллер будет перезагружен на русском языке.					Контроллер будет перезагружен на русском языке.
12		ID_RUSSIAN_TIP1		128		Выбранный язык такой же, как и локальный, без необходимости изменения.		Выбранный язык такой же, как и локальный, без необходимости изменения.
13		ID_TURKEY_TIP		64		Controller will restart in Turkish.						Controller will restart in Turkish.
14		ID_TURKEY_TIP1		64		Selected language is same as local language, no need to change.			Selected language is same as local language, no need to change.
15		ID_TW_TIP		64		监控将会重启, 更改成\"繁体中文\".						监控将会重启, 更改成\"繁体中文\".
16		ID_TW_TIP1		64		选择的语言与本地语言一致, 无需更改.						选择的语言与本地语言一致, 无需更改.
17		ID_ZH_TIP		64		监控将会重启, 更改成\"简体中文\".						监控将会重启, 更改成\"简体中文\".
18		ID_ZH_TIP1		64		选择的语言与本地语言一致, 无需更改.						选择的语言与本地语言一致, 无需更改.
19		ID_SET			32		Set										Valider
20		ID_LANGUAGETIPS				16			    Language			La langue
21		ID_GERMANY				16			    Germany			Allemagne
22		ID_SPAISH				16			    Spain			Espagne
23		ID_FRANCE				16			    France			France
24		ID_ITALIAN				16			    Italy			Italie
25		ID_CHINA				16			    China			Chine
26		ID_CHINA2				16			    China			Chine
[tmp.system_udef.html:Number]
14

[tmp.system_udef.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_NO_DATA				16			No data				Pas de donnée
2		ID_USER_DEF				32			User Define				Utilisateur défini
3		ID_SIGNAL				32			Signal				Signal
4		ID_VALUE				32			Value				Valeur
5		ID_SIGNAL1				32			Signal				Signal
6		ID_VALUE1				32			Value				Valeur

[tmp.udef_setting.html:Number]
14

[tmp.udef_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_USER_SET				32			User Config			Configuration utilisateur
2		ID_SIGNAL				32			Signal				Signal
3		ID_VALUE				32			Value				Valeur
4		ID_TIME					32			Time Last Set			Derniere heure configuree
5		ID_SET_VALUE				32			Set Value				Valider valeur 
6		ID_SET					32			Set					Valider
7		ID_SET1					32			Set					Valider
8		ID_NO_DATA				16			No data				Pas de donnée

[tmp.udef_setting_2.html:Number]
14

[tmp.udef_setting_2.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_USER_SET				32			User Config2			Configuration 2 utilisateur
2		ID_SIGNAL				32			Signal				Signal
3		ID_VALUE				32			Value				Valeur
4		ID_TIME					32			Time Last Set			Derniere heure configuree
5		ID_SET_VALUE				32			Set Value				Valider valeur
6		ID_SET					32			Set					Valider
7		ID_SET1					32			Set					Valider
8		ID_NO_DATA				16			No data				Pas de donnée

[tmp.udef_setting_3.html:Number]
14

[tmp.udef_setting_3.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_USER_SET				16			User Config3			Config 3 Utilisa
2		ID_SIGNAL				32			Signal				Signal
3		ID_VALUE				32			Value				Valeur
4		ID_TIME					32			Time Last Set			Derniere heure configuree
5		ID_SET_VALUE				32			Set Value				Valider valeur
6		ID_SET					32			Set					Valider
7		ID_SET1					32			Set					Valider
8		ID_NO_DATA				16			No data				Pas de donnée


#//changed by Frank Wu,3/3/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
[tmp.setting_mppt.html:Number]
14

[tmp.setting_mppt.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL						32					Signal				Signal
2		ID_VALUE						32					Value				Valeur
3		ID_TIME							32					Time Last Set		Derniere heure configuree
4		ID_SET_VALUE					32					Set Value			Valider valeur
5		ID_SET							32					Set					Valider
6		ID_SET1							32					Set					Valider
7		ID_NO_DATA						32					No Data				Pas de donnée
8		ID_MPPT							32					Solar				Solaire

[tmp.shunt_setting.html:Number]
14

[tmp.shunt_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL						32					Signal				Signal
2		ID_VALUE						32					Value				Valeur
3		ID_TIME							32					Time Last Set		Derniere heure configuree
4		ID_SET_VALUE					32					Set Value			Valider valeur
5		ID_SET							32					Set					Valider
6		ID_SET1							32					Set					Valider
7		ID_NO_DATA						32					No Data				Pas de donnée
8		ID_SHUNT_SET					32					Shunt				Shunt
9		ID_EQUIP						32					Equipment			Equipement


#//changed by Frank Wu,4/4/30,20140527, for add single converter and single solar settings pages
[tmp.single_converter_setting.html:Number]
6

[tmp.single_converter_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL					32						Signal				Signal
2		ID_VALUE					32						Value				Valeur
3		ID_TIME						32						Time Last Set		Derniere heure configuree
4		ID_SET_VALUE				32						Set Value			Valider valeur
5		ID_SET						32						Set					Valider
6		ID_SET1						32						Set					Valider
7		ID_RECT						32						Converter			Convertisseurs
8		ID_NO_DATA					16						No data.			Pas de donnée

[tmp.single_solar_setting.html:Number]
6

[tmp.single_solar_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL					32						Signal				Valeur
2		ID_VALUE					32						Value				Valeur
3		ID_TIME						32						Time Last Set		Derniere heure configuree
4		ID_SET_VALUE				32						Set Value			Valider valeur
5		ID_SET						32						Set					Valider
6		ID_SET1						32						Set					Valider
7		ID_RECT						32						Solar Conv			Conver Solaire
8		ID_NO_DATA					16						No data.			Pas de donnée

[tmp.cabinet_branch.html:Number]
6

[tmp.cabinet_branch.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS1				32			Please select			Selectionnez SVP
2		ID_TIPS2				32			Please select			Selectionnez SVP
3		ID_TIPS3				32			Please select			Selectionnez SVP
4		ID_TIPS4				32			Please select			Selectionnez SVP
[tmp.cabinet_map.html:Number]
6

[tmp.cabinet_map.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_MAJOR				16			Major				Majeure
2		ID_OBS					16			Observation				Observation
3		ID_NORMAL				16			Normal				Normal
4		ID_NO_DATA				16			No Data				Pas de donnée
5		ID_SELECT				32			Please select			Selectionnez SVP
6		ID_NO_DATA1				16			No Data				Pas de donnée


[tmp.cabinet_set.html:Number]
6

[tmp.cabinet_set.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SET					16			Set					Valider
2		ID_BRANCH				16			Branches				Branches
3		ID_TIPS					16			is selected				est selectionnée
4		ID_RESET				32			Reset				Reset
5		ID_TIPS1				32			Show Designation			Montrer Designation
6		ID_TIPS2				32			Close Detail			Detail
9		ID_TIPS3				32			Success to select.			Selection réussie.
10		ID_TIPS4				32			Click to delete			Cliquer pour selectionner
11		ID_TIPS5				128			Please select a cabinet to set.	Selectionner la baie a configurer
12		ID_TIPS6				64			This Cabinet has been set		La baie a été configuree
13		ID_TIPS7				64			branches, confirm to cancel?	Branches, confirmer l'effacement?
14		ID_TIPS8				32			Please select			Selectionnez SVP
15		ID_TIPS9				32			no allocation			Pas d'Affectation
16		ID_TIPS10				128			Please add more branches for the selected cabinet.		Ajouter plus de branches pour la baie selectionnee SVP
17		ID_TIPS11				64			Success to deselect.		Deselection reussie
18		ID_TIPS12				64			Reset cabinet...please wait.	Reset de la baie… Patientez SVP
19		ID_TIPS13				64			Reset Successful, please wait.	Reset réussie, patientez SVP
20		ID_ERROR1				16			Failed.				Echec
21		ID_ERROR2				16			Successful.				Réussie
22		ID_ERROR3				32			Insufficient privileges.		Droits insuffisants
23		ID_ERROR4				64			Failed. Controller is hardware protected.		Echec. Controleur protége contre l'ecriture
24		ID_TIPS14				32			Exceed 20 branches.			Supérieur à 20 branches
25		ID_TIPS15				128			More than 16 characters, or you have input invalid characters.		Plus de 16 caractères, ou vous avez entrée des caractères non valides.
26		ID_TIPS16				256			Alarm level value can only be integer or just a decimal, and Alarm Level2 must be larger than or equal to Alarm Level1.	Niveau de priorité d'alarme peut seulement etre entier ou decimal.
27		ID_TIPS17				32			Show Parameter			Montrer paramètre
28		ID_SET1					16			Designate				designer
29		ID_SET2					16			Set					Valider
30		ID_NAME					16			Name				Nom
31		ID_LEVEL1				32			Alarm Level1			Niveau 1 Alarme
32		ID_LEVEL2				32			Alarm Level2			Niveau 2 Alarme
33		ID_RATING				32			Rating Current			Courant nominal
34		ID_TIPS18				128			Rating current must be from 0 to 10000, and can only have one decimal at most.		Plage de courant nominal de 0 à 10000, 1 decimal maxi
35		ID_SET3					16			Set					Valider
36		ID_TIPS19				64			Confirm to reset the current cabinet?	confirmer le reset de la baie?

#//changed by Frank Wu,2/N/35,20140527, for adding the the web setting tab page 'DI'
[tmp.setting_di_content.html:Number]
6

[tmp.setting_di_content.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS1					32			Signal Full Name				Nom signal complet
2		ID_TIPS2					32			Signal Abbr Name			Nom signal abrege
3		ID_TIPS3					32			New Alarm Level					Nouveau niveau d&lsquo;alarme
4		ID_TIPS4					32			Please select					Selectionnez SVP
5		ID_TIPS5					32			New Relay Number				Nouveau numero de relai
6		ID_TIPS6					32			Please select					Selectionnez SVP
7		ID_TIPS7					32			New Alarm State					Nouveau etat d&lsquo;alarme
8		ID_TIPS8					32			Please select					Selectionnez SVP
9		ID_NA						16			NA								NA
10		ID_OA						16			OA								OA
11		ID_MA						16			MA								MA
12		ID_CA						16			CA								CA
13		ID_NA2						16			None							Aucun
14		ID_LOW						16			Low								Bas
15		ID_HIGH						16			High							Haut
16		ID_SET						16			Set								Valider
17		ID_TITLE					16			DI Alarms						Entrees alarmes
18		ID_INDEX					16			Index							Index
19		ID_EQUIPMENT				32			Equipment Name					Nom equipement
20		ID_SIGNAL_NAME				32			Signal Name						Nom du signal
21		ID_ALARM_LEVEL				32			Alarm Level						Niveau d&lsquo;alarme
22		ID_ALARM_STATE				32			Alarm State						Etat d&lsquo;alarme
23		ID_REPLAY_NUMBER			32			Alarm Relay						Relai alarme
24		ID_MODIFY					32			Modify							Modifier
25		ID_NA1						16			NA								NA
26		ID_OA1						16			OA								OA
27		ID_MA1						16			MA								MA
28		ID_CA1						16			CA								CA
29		ID_LOW1						16			Low								Bas
30		ID_HIGH1					16			High							Haut
31		ID_NA3						16			None							Aucun
32		ID_MODIFY1					32			Modify							Modifier
33		ID_NO_DATA					32			No Data							Pas de donnée
34		ID_NA4						16			NA							NA
35		ID_POWER_SYSTEM					16			Power System						Sis Energía
36		ID_DO_NCU				32			NCU				NCU
37		ID_TIPS9					32			Please select					Selectionnez SVP
38		ID_TIPS10					32			Please select					Selectionnez SVP
39		ID_TIPS11					32			Please select					Selectionnez SVP
40		ID_NA5						16			NA							NA
#//changed by Frank Wu,2/N/14,20140527, for system log
[tmp.history_systemlog.html:Number]
23

[tmp.history_systemlog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_INDEX				32			Index				Index
2		ID_TASK_NAME			32				Task Name				Nom de tache
3		ID_INFO_LEVEL			32				Info Level				Niveau d'info
4		ID_LOG_TIME				32			Time				Heure
5		ID_INFORMATION			32				Information				Information
8		ID_FROM					32			From				De
9		ID_TO					32			To					à
10		ID_QUERY				32			Query				Requete
11		ID_UPLOAD				32			Upload				Chargement
12		ID_TIPS					64			Displays the last 500 entries	Afficher les 500 derniers enregistrements
13		ID_QUERY_TYPE				16			Query Type				Type de requete
14		ID_SYSTEM_LOG				16			System Log				Journal système
16		ID_CTL_RESULT0				64			Successful				Reussie		
17		ID_CTL_RESULT1				64			No Memory				Pas de mémoire		
18		ID_CTL_RESULT2				64			Time Expired			Temps expiré		
19		ID_CTL_RESULT3				64			Failed				Echec		
20		ID_CTL_RESULT4				64			Communication Busy			Communication occupée		
21		ID_CTL_RESULT5				64			Control was suppressed.		Contrôle supprimé	
22		ID_CTL_RESULT6				64			Control was disabled.		Contrôle desactivé	
23		ID_CTL_RESULT7				64			Control was canceled.		Contrôle annulé
24		ID_SYSTEM_LOG2				32			System Log				Journal du système
25		ID_SYSTEMLOG			32				System Log				Journal du système
#//changed by Frank Wu,2/N/27,20140527, for power split
[tmp.setting_power_split.html:Number]
6

[tmp.setting_power_split.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_LVD1						32			LVD1				LVD1
2		ID_LVD2						32			LVD2				LVD2
3		ID_LVD3						32			LVD3				LVD3
4		ID_BATTERY_TEST					32			BATTERY_TEST			TEST BATTERIE
5		ID_EQUALIZE_CHARGE				32			EQUALIZE_CHARGE			CHARGE EGALISATION
6		ID_SAMP_TYPE					16			Sample				Echantillon
7		ID_CTRL_TYPE					16			Control				Contrôle
8		ID_SET_TYPE					16			Setting				Configuration
9		ID_ALARM_TYPE					16			Alarm				Alarme
10		ID_SLAVE					16			SLAVE				ESCLAVE
11		ID_MASTER					16			MASTER				MAITRE
12		ID_SELECT					32			Please select			Selectionnez SVP
13		ID_NA						16			NA				NA
14		ID_EQUIP					16			Equipment			Equipement
15		ID_SIG_TYPE					16			Signal Type			Type de signal
16		ID_SIG						16			Signal				Signal
17		ID_MODE						32			Power Split Mode		Mode Power Split
18		ID_SET						16			Set				Valider
19		ID_SET1						16			Set				Valider
20		ID_TITLE					32			Power Split			Power Split
21		ID_INDEX					16			Index				Index
22		ID_SIG_NAME					32			Signal				Signal
23		ID_EQUIP_NAME					32			Equipment			Equipement
24		ID_SIG_TYPE					32			Signal Type			Type de signal
25		ID_SIG_NAME1					32			Signal Name			Nom de signal
26		ID_MODIFY					32			Modify				Modifier
27		ID_MODIFY1					32			Modify				Modifier
28		ID_NO_DATA					32			No Data				Pas de donnée

[tmp.batt_setting.html:Number]
7

[tmp.batt_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Signal
2		ID_VALUE				32			Value				Valeur
3		ID_TIME					32			Time Last Set			Derniere heure configuree
4		ID_SET_VALUE				32			Set Value				Valider valeur
5		ID_SET					32			Set					Valider
6		ID_SET1					32			Set					Valider
7		ID_BACK					16			Back				Back
8		ID_NO_DATA				32			No Data	Pas de donnée
[tmp.system_lvd_fuse.html:Number]
7

[tmp.system_lvd_fuse.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Signal
2		ID_VALUE				32			Value				Valeur
3		ID_TIME					32			Time Last Set			Derniere heure configuree
4		ID_SET_VALUE				32			Set Value				Valider valeur
5		ID_SET					32			Set					Valider
6		ID_SET1					32			Set					Valider
7		ID_BACK					16			Back				Back
8		ID_NO_DATA				32			No Data				Pas de donnée
9		ID_SIGNAL1				32			Signal				Signal
10		ID_VALUE1				32			Value				Valeur

[tmp.system_battery_tabs.html:Number]
7

[tmp.system_battery_tabs.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_BATTERY				32			Battery				batterie
2		ID_BATT_FUSE_GROUP			32			Battery Fuse Group			Groupe Fus Batt
3		ID_BATT_FUSE				32			Battery Fuse			Batterie Fus
4		ID_LVD_GROUP				32			LVD Group				Groupe LVD
5		ID_LVD_UNIT				32			LVD Unit				Module LVD
6		ID_SMDU_BATT_FUSE			32			SMDU Battery Fuse			SMDU Fus Batterie
7		ID_SMDU_LVD				32			SMDU LVD				SMDU LVD
8		ID_LVD3_UNIT				32			LVD3 Unit			Module LVD3
#//changed by Frank Wu,N/N/N,20140613, for two tabs
[tmp.setting_tabs.html:Number]
7

[tmp.setting_tabs.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_CHARGE				32			Battery Charge			Charge batterie
2		ID_ECO					32			ECO				ECO
3		ID_LVD					32			LVD				LVD
4		ID_QUICK_SET				32			Quick Settings			Config rapide
5		ID_TEMP					32			Temperature			Temperature
6		ID_RECT					32			Rectifiers			Redresseur
7		ID_CONVERTER				32			DC/DC Converters		Convertisseurs DC/DC
8		ID_BATT_TEST				32			Battery Test			Test batterie
9		ID_TIME_CFG				32			Time Settings			Config heure
10		ID_USER_DEF_SET_1			32			User Config1			Configuration 1 utilisateur
11		ID_USER_SET2				32			User Config2			Configuration 2 utilisateur
12		ID_USER_SET3				32			User Config3			Configuration 3 utilisateur
13		ID_MPPT					32			Solar				Solaire
14		ID_POWER_SYS				32			System				Système
15		ID_CABINET_SET				32			Set Cabinet			Config Sys
16		ID_USER_SET4			32			User Config4			Utilisateur Config4

[tmp.system_tabs_a.html:Number]
7

[tmp.system_tabs_a.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_RECTIFIER				32			Rectifier				Redresseur
2		ID_RECTIFIER1				32			GI Rectifier			Redresseur GI 
3		ID_RECTIFIER2				32			GII Rectifier			Redresseur GII 
4		ID_RECTIFIER3				32			GIII Rectifier			Redresseur GIII 
5		ID_CONVERTER				32			Converter				Convertisseurs
6		ID_SOLAR				32			Solar Converter			Convertisseur solaire

[tmp.consumption_map.html:Number]
7

[tmp.consumption_map.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS1				32			branch number			N° Branche
2		ID_TIPS2				32			total current			Courant total
3		ID_TIPS3				32			total power				Puissance totale
4		ID_TIPS4				32			total energy			Energie totale
5		ID_TIPS5				32			peak power last 24h			Pic de puiss dernieres 24H
6		ID_TIPS6				32			peak power last week		Pic de puiss semaine derniere 
7		ID_TIPS7				32			peak power last month		Pic de puiss mois dernier

[tmp.system_dc_smdup.html:Number]
7

[tmp.system_dc_smdup.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Signal
2		ID_VALUE				32			Value				Valeur
3		ID_SIGNAL1				32			Signal				Signal
4		ID_VALUE1				32			Value				Valeur
5		ID_NO_DATA				32			No Data				Pas de donnée

[tmp.adv_setting_tabs.html:Number]
25

[tmp.adv_setting_tabs.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_IPV4					32			Ethernet				Ethernet
2		ID_SCUP_IP				32			IP Address				IP Address
3		ID_SCUP_MASK				32			Subnet Mask				Subnet Mask		
4		ID_SCUP_GATEWAY				32			Default Gateway			Default Gateway	
5		ID_ERROR0				32			Setting Failed.			Configuration echouee..			
6		ID_ERROR1				32			Successful.				Reussie.		
7		ID_ERROR2				64			Failed. Incorrect input.				Echec. Saisie incorrecte.			
8		ID_ERROR3				64			Failed. Incomplete information.			Echec. Saisie incomplete.		
9		ID_ERROR4				64			Failed. No privilege.				Echec. Droits insuffisants.			
10		ID_ERROR5				128			Failed. Controller is hardware protected.		Echec. Protection hardware.	
11		ID_ERROR6				32			Failed. DHCP is ON.					Echec. DHCP ON.				
12		ID_TIPS0				32			Set Network Parameter														Config parametres reseau											
13		ID_TIPS1				128			Units IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171.					Adresse IP incorrecte. \format 'nnn.nnn.nnn.nnn'. Exemple 10.75.14.171.				
14		ID_TIPS2				128			Mask IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 255.255.0.0.						Masque IP incorrect. \format 'nnn.nnn.nnn.nnn'. Exemple 255.255.0.0.				
15		ID_TIPS3				64			Units IP Address and Mask mismatch.													Adresse IP & Masque incompatible.												
16		ID_TIPS4				128			Gateway IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171. Enter 0.0.0.0 for no gateway	Adresse IP passerelle incorrecte. \format 'nnn.nnn.nnn.nnn'. Exemple 10.75.14.171. Saisir 0.0.0.0 si pas de passerelle.
17		ID_TIPS5				128			Units IP Address, Gateway, Mask mismatch. Enter Address again.									Adresse IP & passerelle & masque incompatible.							
18		ID_TIPS6				64			Please wait. Controller is rebooting.												Patientez. Redémarrage NCU..											
19		ID_TIPS7				128			Parameters have been modified.  Controller is rebooting...										Parametres modifies.Redémarrage NCU.								
20		ID_TIPS8				64			Controller homepage will be refreshed.												Page d'acceuil  sera reaffichee											
21		ID_TIPS9				128			Confirm the change to the IP address?				Confirmation adresse IP?
22		ID_SAVE					16			Save					Sauver			
23		ID_TIPS10				32			IP Address Error				Erreur adresse IP		
24		ID_TIPS11				32			Subnet Mask Error				Erreur masque sous reseau		
25		ID_TIPS12				32			Default Gateway Error			Erreur passerelle par defaut	
26		ID_USER					32			Users					Utilisateurs			
27		ID_IPV4_1					32		IPV4					IPV4			
28		ID_IPV6					32			IPV6					IPV6			
29		ID_HLMS_CONF				32			Monitor Protocol				Protocole supervision		
30		ID_SITE_INFO				32			Site Info					Info site			
31		ID_AUTO_CONFIG				32			Auto Config					Auto config		
32		ID_OTHER				32			Alarm Report				Report alarme		
33		ID_NMS					32			SNMP					SNMP			
34		ID_ALARM_SET				32			Alarms					Alarmes			
35		ID_CLEAR_DATA				16			Clear Data					RAZ donnees			
36		ID_RESTORE_DEFAULT			32			Restore Defaults				Restaurer donnees		
37		ID_DOWNLOAD_UPLOAD			32			SW Maintenance				Maintenance logiciel		
38		ID_HYBRID				32			Generator					Groupe Elect			
39		ID_DHCP					16			IPV4 DHCP					IPV4 DHCP			
40		ID_IP1					32			Server IP					Serveur IP			
41		ID_TIPS13				16			Loading					Chargement		
42		ID_TIPS14				16			Loading					Chargement		
43		ID_TIPS15				32			failed, data format error!				Echec, erreur de format!		
44		ID_TIPS16				32			failed, please refresh the page!			Echec, reafficher la page SVP!
45		ID_LANG					16			Language			Langage
#//changed by Frank Wu,2/2/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
46		ID_SHUNT_SET			32				Shunts				Shunts
#//changed by Frank Wu,1/N/35,20140527, for adding the the web setting tab page 'DI'
47		ID_DI_ALARM_SET			32				DI Alarms			Entree alarme
#//changed by Frank Wu,1/1/N,00000000, for add PowerSplit
48		ID_POWER_SPLIT_SET		32				Power Split			Split de puissance
49		ID_IPV6_1			16				IPV6				IPV6
50		ID_LOCAL_IP			32				Link-Local Address		Adresse lien  local	
51		ID_GLOBAL_IP			32				IPV6 Address			IPV6 Address		
52		ID_SCUP_PREV			32				Subnet Prefix			Prefixe sous reseau		
53		ID_SCUP_GATEWAY1		32				Default Gateway			Passerelle par defaut	
54		ID_SAVE1			16				Save				Sauver			
55		ID_DHCP1			16				IPV6 DHCP			IPV6 DHCP		
56		ID_IP2				32				Server IP			Serveur IP		
57		ID_TIPS17			64				Please fill in the correct IPV6 address.	Saisir adresse IPV6 correcte SVP.	
58		ID_TIPS18			128				Prefix can not be empty or beyond the range.	Le prefixe ne peut etre vide ou hors plage.
59		ID_TIPS19			64				Please fill in the correct IPV6 gateway.	Saisir adresse passerelle IPV6 correcte SVP.	
60		ID_TL1_GROUP			16				TL1 AID Group					TL1 AID Groupe					
61		ID_TL1_SIGNAL			16				TL1 AID Signal					TL1 AID Entree					
62		ID_DO_RELAY				32			DO(relays)						DO(Relais)
63		ID_SHUNTS_SET				32			Shunts						Shunts
64		ID_CR_SET				32			Fuses						Fusibles
65		ID_CUSTOM_INPUT				32			Custom Inputs						Entrées personnalisées
66		ID_ANALOG_SET				32			Analogs						Analogique
																
[tmp.setting_TL1_group.html:Number]
25

[tmp.setting_TL1_group.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_AID_GROUP				16			AID Group				Groupe AID		
2		ID_AID_NAME				16			AID Name				Nom AID		
3		ID_AID_PREFIX				32			Sub AID Name Prefix			Nom prefixe sous AID
4		ID_AID_TYPE				16			AID Type				Type AID		
5		ID_EQPT					16			EQPT				EQPT		
6		ID_ENV					16			ENV					ENV			
7		ID_AID_INDEX				16			index				Indexe		
8		ID_SUBMIT				16			Submit				Envoi	
12		ID_ERROR3				64			Insufficient privileges for this function.		Droits insuffisants
13		ID_ERROR4				32			Invalid character(s).			Caractère invalide		
14		ID_ERROR5				32			Can't be empty.			Ne peut être vierge			
15		ID_AID_NAME1				16			AID Name				Nom AID				
16		ID_AID_PREFIX1				32			Sub AID Name Prefix			Prefix Nom Sub AID		
17		ID_AID_TYPE1				16			AID Type				Type AID				
18		ID_TL1_ERRORS1				32			Successful.				Reussie				
19		ID_TL1_ERRORS2				32			Configuration fail.			Echec de configuration		
20		ID_TL1_ERRORS3				64			Failed. Service was exited.		Echec. Service interrompu.	
21		ID_TL1_ERRORS4				32			Invalid parameters.			Paramètres invalides			
22		ID_TL1_ERRORS5				32			Invalid parameters.			Paramètres invalides			
23		ID_TL1_ERRORS6				64			Fail to write to Flash.		Echec d'écriture en Flash		
24		ID_TL1_ERRORS7				32			Service is busy.			Service occupé.			
25		ID_TL1_ERRORS8				64			Port has already been occupied.		Port déjà utilisé.
26		ID_TL1_ERRORS9				64			Port has already been occupied.		Port déjà utilisé.
27		ID_TL1_ERRORS10				64			Port has already been occupied.		Port déjà utilisé.
28		ID_TL1_ERRORS11				64			Port has already been occupied.		Port déjà utilisé.
29		ID_TL1_ERRORS12				64			The TL1 feature is not available.	TL1 indisponible.
30		ID_ERROR6				128			The length sum of AID name and Sub AID name prefix exceed range.	Depassement taille nom AID et prefixe nom Sub AID


[tmp.setting_TL1_signal.html:Number]
25

[tmp.setting_TL1_signal.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_AID_GROUP				16			AID Group				Groupe AID			
2		ID_AID_PREFIX				32			Sub AID Name Prefix			Nom prefixe sous AID
3		ID_AID_INDEX				16			index				Indexe			
4		ID_AID_NAME				16			AID Name				Nom AID		
8		ID_ERROR3				64			Insufficient privileges for this function.		Droits insuffisants.
9		ID_ERROR4				32			Invalid character(s).			Caractère invalide		
10		ID_ERROR5				32			Can't be empty.			Ne peut être vierge			
11		ID_ACTIVE				16			Signal Active			Entree active		
12		ID_SAMPLE				16			Sample Signal			Entree dispo		
13		ID_ALARM				16			Alarm Signal			Entree alarme		
14		ID_SETTING				16			Setting Signal			Config entree		
15		ID_CON_TYPE				32			Condition Type			Type config	
16		ID_CON_DESC				32			Condition Description		Description condition
17		ID_NOTI_CODE				32			Notification Code			Code notification		
18		ID_SERV_CODE				32			Service Affect Code			Code service affecte		
19		ID_MONI_FORT				32			Monitor Format			Format supervision	
20		ID_MONI_TYPE				32			Monitor Type			Type supervision		
21		ID_SUBMIT				16			Submit				Envoi		
22		ID_TIPS1				64			Please Select AID Group and Equip		Selectionner Groupe AID & Eqt SVP
23		ID_TIPS2				32			Please Select Signal		Selectionnez signal SVP
24		ID_TIPS3				16			Please Select			Selectionnez SVP	
25		ID_EQUIP_LIST				16			Equip List				Liste equipement	
26		ID_TIPS4				16			Please Select			Selectionnez SVP
27		ID_TIPS5				64			First select AID Group, then select Equip.		Selectionner Groupe AID puis Eqt
28		ID_TL1_ERRORS1				32			Successful.				Reussie.			
29		ID_TL1_ERRORS2				32			Configuration fail.			Echec configuration.		
30		ID_TL1_ERRORS3				64			Failed. Service was exited.		Echec. Service interrompu.
31		ID_TL1_ERRORS4				32			Invalid parameters.			Parametres invalides.		
32		ID_TL1_ERRORS5				32			Invalid parameters.			Parametres invalides.	
33		ID_TL1_ERRORS6				64			Fail to write to Flash.		Echec d'écriture en Flash.
34		ID_TL1_ERRORS7				32			Service is busy.			Service occupé.		
35		ID_TL1_ERRORS8				64			Port has already been occupied.	Port déjà utilisé.
36		ID_TL1_ERRORS9				64			Port has already been occupied.		Port déjà utilisé..
37		ID_TL1_ERRORS10				64			Port has already been occupied.		Port déjà utilisé..
38		ID_TL1_ERRORS11				64			Port has already been occupied.		Port déjà utilisé..
39		ID_TL1_ERRORS12				64			Error, TL1 is an option and is not active.	Erreur, option TL1 inactive.
[tmp.setting_relay_content.html:Number]
12

[tmp.setting_relay_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_DO_RELAY				32			DO(Relays)				DO(Relais)
2		ID_SIGNAL_NAME				32			Signal Name				Nom du signal
3		ID_VALUE				32			Value						Valeur
4		ID_SET_VALUE			32			Set Value					Valider valeur
5		ID_SET					32			Set							Valider
6		ID_SET1					32			Set							Valider
7		ID_SET2					32			Set							Valider
8		ID_SET3					32			Set							Valider
9		ID_MODIFY				32			Modify							Modifier
10		ID_MODIFY1				32			Modify							Modifier
11		ID_NO_DATA				32			No Data							Pas de donnée
12		ID_TIPS1				32			Signal Full Name				Signal Nom Complet
13		ID_INPUTTIP				32			Max Characters:20				Caractère Max:20
14		ID_INPUTTIP2				32			Max Characters:32				Caractère Max:32
15		ID_INPUTTIP3				32			Max Characters:16				Caractère Max:16
16		ID_DO_NCU				32			NCU						NCU		
17		ID_DO_POEWRSYSTEM			32			Power System					Power System	
18		ID_INPUTTIP4				32			Max Characters:20				Max Characters:20

[tmp.setting_shunts_content.html:Number]
11

[tmp.setting_shunts_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_REFERENCE				32			Reference				Référence
2		ID_SIGNAL_SHUNTNAME				32			Shunt Name				Nom Shunt
3		ID_MODIFY				32			Modify						Modifier
4		ID_VIEW				32			View					Vue
5		ID_FULLSC				32			Full Scale Current					Pleine Echelle Courant
6		ID_FULLSV			32			Full Scale Voltage					Pleine Echelle Tension
7		ID_BREAK_VALUE				32			Break Value					Pause Valeur
8		ID_CURRENT_SETTING				32			Current Setting					Paramètre Courant
9		ID_NEW_SETTING				32			New Setting					Nouveau Cadre
10		ID_RANGE				32			Range					Gamme
11		ID_SETAS				32			Set As					Définir Comme
12		ID_SIGNAL_FULL_NAME				32			Signal Full Name					Signal Nom Complet
13		ID_SIGNAL_ABBR_NAME				32			Signal Abbr Name					Nom du Signal Abbr
14		ID_SHUNT1				32			Shunt 1					Shunt 1
15		ID_SHUNT2				32			Shunt 2					Shunt 2
16		ID_SHUNT3				32			Shunt 3					Shunt 3
17		ID_SHUNT4				32			Shunt 4					Shunt 4
18		ID_SHUNT5				32			Shunt 5					Shunt 5
19		ID_SHUNT6				32			Shunt 6					Shunt 6
20		ID_SHUNT7				32			Shunt 7					Shunt 7
21		ID_SHUNT8				32			Shunt 8					Shunt 8
22		ID_SHUNT9				32			Shunt 9					Shunt 9
23		ID_SHUNT10				32			Shunt 10					Shunt 10
24		ID_SHUNT11				32			Shunt 11					Shunt 11
25		ID_SHUNT12				32			Shunt 12					Shunt 12
26		ID_SHUNT13				32			Shunt 13					Shunt 13
27		ID_SHUNT14				32			Shunt 14					Shunt 14
28		ID_SHUNT15				32			Shunt 15					Shunt 15
29		ID_SHUNT16				32			Shunt 16					Shunt 16
30		ID_SHUNT17				32			Shunt 17					Shunt 17
31		ID_SHUNT18				32			Shunt 18					Shunt 18
32		ID_SHUNT19				32			Shunt 19					Shunt 19
33		ID_SHUNT20				32			Shunt 20					Shunt 20
34		ID_SHUNT21				32			Shunt 21					Shunt 21
35		ID_SHUNT22				32			Shunt 22					Shunt 22
36		ID_SHUNT23				32			Shunt 23					Shunt 23
37		ID_SHUNT24				32			Shunt 24					Shunt 24
38		ID_SHUNT25				32			Shunt 25					Shunt 25
39		ID_LOADSHUNT				32			Load Shunt					Chargez Shunt
40		ID_BATTERYSHUNT				32			Battery Shunt					Batterie Shunt
41		ID_TIPS1					32			Please select					Selectionnez SVP
42		ID_TIPS2					32			Please select					Selectionnez SVP
43		ID_NA						16			NA								NA
44		ID_OA						16			OA								OA
45		ID_MA						16			MA								MA
46		ID_CA						16			CA								CA
47		ID_NA2						16			None							Aucune
48		ID_NOTUSEd						16			Not Used							Non Utilisé
49		ID_GENERL						16			General							Général
50		ID_LOAD						16			Load							Charge
51		ID_BATTERY						16			Battery							Batterie
52		ID_NA3						16			NA								NA
53		ID_OA2						16			OA								OA
54		ID_MA2						16			MA								MA
55		ID_CA2						16			CA								CA
56		ID_NA4						16			None							Aucune
57		ID_MODIFY				32			Modify						Modifier
58		ID_VIEW				32			View					Vue
59		ID_SET						16			set							Valider
60		ID_NA1						16			None							Aucune
61		ID_Severity1				32			Alarm Relay				Relais d'alarme
62		ID_Relay1				32			Alarm Severity				Gravité d'alarme
63		ID_Severity2				32			Alarm Relay				Relais d'alarme
64		ID_Relay2				32			Alarm Severity				Gravité d'alarme
65		ID_Alarm				32			Alarm				Alarme
66		ID_Alarm2				32			Alarm				Alarme
67		ID_INPUTTIP				32			Max Characters:20					Caractère Max:20
68		ID_INPUTTIP2				32			Max Characters:20					Caractère Max:20
69		ID_INPUTTIP3				32			Max Characters:8					Caractère Max:8
70		ID_FULLSV				32			Full Scale Voltage				Pleine Echelle Courant
71		ID_FULLSC				32			Full Scale Current				Pleine Echelle Tension
72		ID_BREAK_VALUE2				32			Break Value					Pause Valeur
73		ID_High1CLA				32			High 1 Curr Limit Alarm				Alarme Haut 1 Limite Courant
74		ID_High1CAS				32			High 1 Curr Alarm Severity			Haut 1 Courant Gravité alarme
75		ID_High1CAR				32			High 1 Curr Alarm Relay				Haut 1 Courant Relais alarme
76		ID_High2CLA				32			High 2 Curr Limit Alarm				Alarme Haut 2 Limite Courant
77		ID_High2CAS				32			High 2 Curr Alarm Severity			Haut 2 Courant Gravité dalarme
78		ID_High2CAR				32			High 2 Curr Alarm Relay				Haut 2 Courant Relais alarme
79		ID_HI1CUR				32			Hi1Cur				Ha1Cou
80		ID_HI2CUR				32			Hi2Cur				Ha2Cou
81		ID_HI1CURRENT				32			High 1 Curr				Haut 1 Cour
82		ID_HI2CURRENT				32			High 2 Curr				Haut 2 Cour
83		ID_NA4					16			NA								NA


[tmp.setting_fuse_content.html:Number]
12

[tmp.setting_fuse_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL_NAME				32			Signal Name				Nom du signal
2		ID_MODIFY				32			Modify				Modifier
3		ID_SET				32			Set				Valider
4		ID_TIPS				32			Signal Full Name				Signal Nom Complet
5		ID_MODIFY1				32			Modify				Modifier
6		ID_TIPS2				32			Signal Abbr Name				Nom du Signal Abbr
7		ID_INPUTTIP				32			Max Characters:20				Caractère Max:20
8		ID_INPUTTIP2				32			Max Characters:32				Caractère Max:32
9		ID_INPUTTIP3				32			Max Characters:15				Caractère Max:15
10		ID_NO_DATA				32			No Data				Pas de donnée
[tmp.shunts_data_content.html:Number]
11
[tmp.shunts_data_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_NA5						16			NA								NA
2		ID_REFERENCE2				32			Reference				Référence
3		ID_SIGNAL_SHUNTNAME2				32			Shunt Name				Nom Shunt
4		ID_MODIFY2				32			Modify						Modifier
5		ID_VIEW2				32			View					Vue
6		ID_MODIFY3				32			Modify						Modifier
7		ID_VIEW3				32			View					Vue
8		ID_BATTERYSHUNT2				32			Battery Shunt					Batterie Shunt
9		ID_LOADSHUNT2				32			Load Shunt					Chargez Shunt
10		ID_BATTERYSHUNT2				32			Battery Shunt					Batterie Shunt



[tmp.system_source_smdu.html:Number]
1
[tmp.system_source_smdu.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Signal
2		ID_VALUE				32			Value				Valeur
3		ID_SIGNAL1				32			Signal				Signal
4		ID_VALUE1				32			Value				Valeur
5		ID_NO_DATA				32			No Data				Pas de donnée

[tmp.system_source.html:Number]
1
[tmp.system_source.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Signal
2		ID_VALUE				32			Value				Valeur
3		ID_SIGNAL1				32			Signal				Signal
4		ID_VALUE1				32			Value				Valeur
5		ID_NO_DATA				32			No Data				Pas de donnée

[tmp.setting_custominputs.html:Number]
1
[tmp.setting_custominputs.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_REFERENCE				32			Reference				Référence
2		ID_VOLTAGE_INPUT				32			Voltage Input				Entrée de tension
3		ID_FUSE_ALARM_INPUT				32			Fuse Alarm Input				Entrée dalarme de fusible
4		ID_ANALOG_INPUT				32			Analog Input				Valeur
5		ID_MODIFY				32			Modify				Modifier
6		ID_MODIFY2				32			Modify				Modifier
7		ID_ADVANCEDSETTING_ANALOGS				32			See Advanced Settings > Analogs				Voir Paramètres avancés> Analogs
8		ID_ADVANCEDSETTING_ANALOGS2				32			See Advanced Settings > Analogs				Voir Paramètres avancés> Analogs
9		ID_ADVANCEDSETTING_ANALOGS3				32			See Advanced Settings > Analogs				Voir Paramètres avancés> Analogs
10		ID_SETTINGS_FUSES				32			See Advanced Settings > Fuses				Voir Paramètres Réglages> Fusibles
11		ID_ADVANCESSETTING_SHUNTS				32			See Advanced Settings > Shunts				Voir Paramètres avancés> Shunts
12		ID_SETTINGS_TEMPERATURE				32			See Settings > Temp Probes				Voir Réglages> Sondes Temp
13		ID_FUNCTION				32			Function				Une fonction
14		ID_CURRENT_SETTING				32			Current Setting				Paramètre actuel
15		ID_NEW_SETTING				32			New Setting				Nouveau cadre
16		ID_VOLTAGE_INPUT2				32			Voltage Input				Entrée de tension
17		ID_FUSE_ALARM_INPUT2				32			Fuse Alarm Input				Entrée dalarme de fusible
18		ID_ANALOG_INPUT2				32			Analog Input				Entrée analogique
19		ID_SET				32			Set				Ensemble

[tmp.setting_analog.html:Number]
1
[tmp.setting_analog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_REFERENCE					32			Reference				Référence
2		ID_SIGNALFULLNAME				32			Signal Full Name			Nom complet du signal
3		ID_MODIFY						32			Modify					Modifier
4		ID_SIGNAL					32			Signal				Señal
5		ID_CURRENT_SETTING				32			Current Setting			Paramètre actuel
6		ID_NEW_CURRENT						32			New Current					Nouveau courant
7		ID_RANGE						32			Range					Intervalle
8		ID_SIGNAL_FULL_NAME				32			Signal Full Name					Signal Nom Complet
9		ID_SIGNAL_ABBR_NAME				32			Signal Abbr Name					Nom du Signal Abbr
10		ID_LOW1_ALARM_LIMIT				32			Low 1 Alarm Limit					Limite dalarme basse 1
11		ID_LOW1_ALARM_SEVERITY				32			Low 1 Alarm Severity					Gravité dalarme faible 1
12		ID_LOW1_ALARM_RELAY				32			Low 1 Alarm Relay					Relais dalarme faible 1
13		ID_LOW2_ALARM_LIMIT				32			Low 2 Alarm Limit					Limite dalarme basse 2
14		ID_LOW2_ALARM_SEVERITY				32			Low 2 Alarm Severity					2 gravité de lalarme faible
15		ID_LOW2_ALARM_RELAY				32			Low 2 Alarm Relay					Bas relais dalarme 2
16		ID_HIGH1_ALARM_LIMIT				32			High 1 Alarm Limit					Haute 1 limite dalarme
17		ID_HIGH1_ALARM_SEVERITY				32			High 1 Alarm Severity					Gravité dalarme élevée 1
18		ID_HIGH1_ALARM_RELAY				32			High 1 Alarm Relay					Relais dalarme 1 haut
19		ID_HIGH2_ALARM_LIMIT				32			High 2 Alarm Limit					Limite dalarme haute 2
20		ID_HIGH2_ALARM_SEVERITY				32			High 2 Alarm Severity					Gravité élevée de lalarme 2
21		ID_HIGH2_ALARM_RELAY				32			High 2 Alarm Relay					Haut relais dalarme 2
22		ID_SET							32			Set							Ensemble
23		ID_NA							32			NA							NA
24		ID_OA							32			OA							OA
25		ID_MA							32			MA							MA
26		ID_CA							32			CA							CA
27		ID_NA2							32			NA							NA
28		ID_OA2							32			OA							OA
29		ID_MA2							32			MA							MA
30		ID_CA2							32			CA							CA
31		ID_NA3							32			NA							NA
32		ID_OA3							32			OA							OA
33		ID_MA3							32			MA							MA
34		ID_CA3							32			CA							CA
35		ID_NA4							32			NA							NA
36		ID_OA4							32			OA							OA
37		ID_MA4							32			MA							MA
38		ID_CA4							32			CA							CA
39		ID_MODIFY2						32			Modify					Modifier
40		ID_INPUTTIP						32			Max Characters:25				Caractère Max:25
41		ID_INPUTTIP2						32			Max Characters:11				Caractère Max:11
42		ID_NA5							32			None							Aucune
43		ID_NA6							32			None							Aucune
44		ID_NA7							32			None							Aucune
45		ID_NA8							32			None							Aucune
46		ID_TIPS1							32			Please select					Selectionnez SVP
47		ID_TIPS2							32			Please select					Selectionnez SVP
48		ID_TIPS3							32			Please select					Selectionnez SVP
49		ID_TIPS4							32			Please select					Selectionnez SVP
50		ID_LOW							32			Low					Faible
51		ID_HIGH							32			High					Haute
52		ID_LOW2							32			Lo					Fa
53		ID_HIGH2							32			Hi					Ha
54		ID_SIGNAL_UNITS							32			Signal Units					Unités de signal
55		ID_INPUTTIP3							32			Max Characters:8					Caractère Max:8
56		ID_SIGNAL_X1							32			X1					X1
57		ID_SIGNAL_Y1							32			Y1(value at X1)					Y1 (valeur à X1)
58		ID_SIGNAL_X2							32			X2					X2
59		ID_SIGNAL_Y2							32			Y2(value at X2)					Y2 (valeur à X2)
60		ID_NO_DATA				64			No SMDUE inputs being used at this time				Aucune entrée SMDUE utilisée actuellement
61		ID_VOLTAGE			32			Voltage					Tension
62		ID_CURTRAN			32			Current Transducer					Transducteur de courant
63		ID_VOLTRAN				32			Voltage Transducer				Transducteur de tension

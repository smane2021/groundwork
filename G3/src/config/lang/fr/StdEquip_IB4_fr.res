﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			IB2-4			IB2-4			IB2-4			IB2-4
8		32			15			Digital Input 1			Digital Input 1			Entree TOR 1		Entree TOR 1
9		32			15			Digital Input 2			Digital Input 2			Entree TOR 2		Entree TOR 2
10		32			15			Digital Input 3			Digital Input 3			Entree TOR 3		Entree TOR 3
11		32			15			Digital Input 4			Digital Input 4			Entree TOR 4		Entree TOR 4
12		32			15			Digital Input 5			Digital Input 5			Entree TOR 5		Entree TOR 5
13		32			15			Digital Input 6			Digital Input 6			Entree TOR 6		Entree TOR 6
14		32			15			Digital Input 7			Digital Input 7			Entree TOR 7		Entree TOR 7
15		32			15			Digital Input 8			Digital Input 8			Entree TOR 8		Entree TOR 8
16		32			15			Not Active			Not Active		Non Actif			Non Actif
17		32			15			Active				Active			Actif			Actif
18		32			15			DO1-Relay Output		DO1-RelayOutput		Sortie DO1 relais	Sortie DO1 Rel
19		32			15			DO2-Relay Output		DO2-RelayOutput		Sortie DO2 relais	Sortie DO2 Rel
20		32			15			DO3-Relay Output		DO3-RelayOutput		Sortie DO3 relais	Sortie DO3 Rel
21		32			15			DO4-Relay Output		DO4-RelayOutput		Sortie DO4 relais	Sortie DO4 Rel
22		32			15			DO5-Relay Output		DO5-RelayOutput		Sortie DO5 relais	Sortie DO5 Rel
23		32			15			DO6-Relay Output		DO6-RelayOutput		Sortie DO6 relais	Sortie DO6 Rel
24		32			15			DO7-Relay Output		DO7-RelayOutput		Sortie DO7 relais	Sortie DO7 Rel
25		32			15			DO8-Relay Output		DO8-RelayOutput		Sortie DO8 relais	Sortie DO8 Rel
26		32			15			Digital Input 1 Alarm State	DI1 Alarm State		Etat ETOR 1		Etat ETOR 1
27		32			15			Digital Input 2 Alarm State	DI2 Alarm State		Etat ETOR 2		Etat ETOR 2
28		32			15			Digital Input 3 Alarm State	DI3 Alarm State		Etat ETOR 3		Etat ETOR 3
29		32			15			Digital Input 4 Alarm State	DI4 Alarm State		Etat ETOR 4		Etat ETOR 4
30		32			15			Digital Input 5 Alarm State	DI5 Alarm State		Etat ETOR 5		Etat ETOR 5
31		32			15			Digital Input 6 Alarm State	DI6 Alarm State		Etat ETOR 6		Etat ETOR 6
32		32			15			Digital Input 7 Alarm State	DI7 Alarm State		Etat ETOR 7		Etat ETOR 7
33		32			15			Digital Input 8 Alarm State	DI8 Alarm State		Etat ETOR 8		Etat ETOR 8
34		32			15			State			State			Etat			Etat
35		32			15			Communication Fail		Comm Fail		Defaut Carte IB		Defaut Carte IB
36		32			15			Barcode			Barcode			Code Barre		Code Barre
37		32			15			On			On			Marche			Marche
38		32			15			Off			Off			Arret			Arret
39		32			15			High			High			Niveau Haut		Niveau Haut
40		32			15			Low			Low			Niveau Bas		Niveau Bas
41		32			15			Digital Input 1 Alarm		DI1 Alarm		Alarme Entree 1	Alarme ETOR 1
42		32			15			Digital Input 2 Alarm		DI2 Alarm		Alarme Entree 2	Alarme ETOR 2
43		32			15			Digital Input 3 Alarm		DI3 Alarm		Alarme Entree 3	Alarme ETOR 3
44		32			15			Digital Input 4 Alarm		DI4 Alarm		Alarme Entree 4	Alarme ETOR 4
45		32			15			Digital Input 5 Alarm		DI5 Alarm		Alarme Entree 5	Alarme ETOR 5
46		32			15			Digital Input 6 Alarm		DI6 Alarm		Alarme Entree 6	Alarme ETOR 6
47		32			15			Digital Input 7 Alarm		DI7 Alarm		Alarme Entree 7	Alarme ETOR 7
48		32			15			Digital Input 8 Alarm		DI8 Alarm		Alarme Entree 8	Alarme ETOR 8
78		32			15			IB2-4 DO1 Test			IB2-4 DO1 Test			Test relais1		Test relais1
79		32			15			IB2-4 DO2 Test			IB2-4 DO2 Test			Test relais2		Test relais2
80		32			15			IB2-4 DO3 Test			IB2-4 DO3 Test			Test relais3		Test relais3
81		32			15			IB2-4 DO4 Test			IB2-4 DO4 Test			Test relais4		Test relais4
82		32			15			IB2-4 DO5 Test			IB2-4 DO5 Test			Test relais5		Test relais5
83		32			15			IB2-4 DO6 Test			IB2-4 DO6 Test			Test relais6		Test relais6
84		32			15			IB2-4 DO7 Test			IB2-4 DO7 Test			Test relais7		Test relais7
85		32			15			IB2-4 DO8 Test			IB2-4 DO8 Test			Test relais8		Test relais8
86		32			15			Temp1			Temp1			Temp 1		Temp 1
87		32			15			Temp2			Temp2			Temp 2		Temp 2

﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#


[LOCALE_LANGUAGE]
fr



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			Temperature 1			Temperature 1			Température 1			Temp 1
2		32			15			Temperature 2			Temperature 2			Température 2			Temp 2
3		32			15			Temperature 3			Temperature 3			Température 3			Temp 3
4		32			15			DC Voltage			DC Voltage		Tension DC			Tension DC
5		32			15			Load Current			Load Current		Courant de Charge		I Charge
6		32			15			Branch 1 Current		Branch 1 Curr		Courant Branche 1		I Branche1
7		32			15			Branch 2 Current		Branch 2 Curr		Courant Branche 2		I Branche2
8		32			15			Branch 3 Current		Branch 3 Curr		Courant Branche 3		I Branche3
9		32			15			Branch 4 Current		Branch 4 Curr		Courant Branche 4		I Branche4
10		32			15			Branch 5 Current		Branch 5 Curr		Courant Branche 5		I Branche5
11		32			15			Branch 6 Current		Branch 6 Curr		Courant Branche 6		I Branche6
12		32			15			DC Over Voltage			DC Over Volt	Sous Tension DC			Sous Tension DC
13		32			15			DC Under Voltage		DC Under Volt		Sur Tension DC			Sur Tension DC
14		32			15			Temperature 1 Over Temperature	T1 Over Temp		Température Haute 1		Temp Haute 1
15		32			15			Temperature 2 Over Temperature	T2 Over Temp		Température Haute 2		Temp Haute 2
16		32			15			Temperature 3 Over Temperature	T3 Over Temp	Température Haute 3		Temp Haute 3
17		32			15			DC Output 1 Disconnected	Output 1 Discon		Sortie DC 1 Ouverte		Sortie DC1 Ouv
18		32			15			DC Output 2 Disconnected	Output 2 Discon		Sortie DC 2 Ouverte		Sortie DC2 Ouv
19		32			15			DC Output 3 Disconnected	Output 3 Discon		Sortie DC 3 Ouverte		Sortie DC3 Ouv
20		32			15			DC Output 4 Disconnected	Output 4 Discon		Sortie DC 4 Ouverte		Sortie DC4 Ouv
21		32			15			DC Output 5 Disconnected	Output 5 Discon		Sortie DC 5 Ouverte	Sortie DC5 Ouv
22		32			15			DC Output 6 Disconnected	Output 6 Discon		Sortie DC 6 Ouverte	Sortie DC6 Ouv
23		32			15			DC Output 7 Disconnected	Output 7 Discon		Sortie DC 7 Ouverte	Sortie DC7 Ouv
24		32			15			DC Output 8 Disconnected	Output 8 Discon		Sortie DC 8 Ouverte	Sortie DC8 Ouv
25		32			15			DC Output 9 Disconnected	Output 9 Discon		Sortie DC 9 Ouverte	Sortie DC9 Ouv
26		32			15			DC Output 10 Disconnected	Output10 Discon		Sortie DC 10 Ouverte	Sortie DC10 Ouv
27		32			15			DC Output 11 Disconnected	Output11 Discon		Sortie DC 11 Ouverte	Sortie DC11 Ouv
28		32			15			DC Output 12 Disconnected	Output12 Discon		Sortie DC 12 Ouverte	Sortie DC12 Ouv
29		32			15			DC Output 13 Disconnected	Output13 Discon		Sortie DC 13 Ouverte	Sortie DC13 Ouv
30		32			15			DC Output 14 Disconnected	Output14 Discon		Sortie DC 14 Ouverte	Sortie DC14 Ouv
31		32			15			DC Output 15 Disconnected	Output15 Discon		Sortie DC 15 Ouverte	Sortie DC15 Ouv
32		32			15			DC Output 16 Disconnected	Output16 Discon		Sortie DC 16 Ouverte	Sortie DC16 Ouv
33		32			15			DC Output 17 Disconnected	Output17 Discon		Sortie DC 17 Ouverte	Sortie DC17 Ouv
34		32			15			DC Output 18 Disconnected	Output18 Discon		Sortie DC 18 Ouverte	Sortie DC18 Ouv
35		32			15			DC Output 19 Disconnected	Output19 Discon		Sortie DC 19 Ouverte	Sortie DC19 Ouv
36		32			15			DC Output 20 Disconnected	Output20 Discon		Sortie DC 20 Ouverte	Sortie DC20 Ouv
37		32			15			DC Output 21 Disconnected	Output21 Discon		Sortie DC 21 Ouverte	Sortie DC21 Ouv
38		32			15			DC Output 22 Disconnected	Output22 Discon		Sortie DC 22 Ouverte	Sortie DC22 Ouv
39		32			15			DC Output 23 Disconnected	Output23 Discon		Sortie DC 23 Ouverte	Sortie DC23 Ouv
40		32			15			DC Output 24 Disconnected	Output24 Discon		Sortie DC 24 Ouverte	Sortie DC24 Ouv
41		32			15			DC Output 25 Disconnected	Output25 Discon		Sortie DC 25 Ouverte	Sortie DC25 Ouv
42		32			15			DC Output 26 Disconnected	Output26 Discon		Sortie DC 26 Ouverte	Sortie DC26 Ouv
43		32			15			DC Output 27 Disconnected	Output27 Discon		Sortie DC 27 Ouverte	Sortie DC27 Ouv
44		32			15			DC Output 28 Disconnected	Output28 Discon		Sortie DC 28 Ouverte	Sortie DC28 Ouv
45		32			15			DC Output 29 Disconnected	Output29 Discon		Sortie DC 29 Ouverte	Sortie DC29 Ouv
46		32			15			DC Output 30 Disconnected	Output30 Discon		Sortie DC 30 Ouverte	Sortie DC30 Ouv
47		32			15			DC Output 31 Disconnected	Output31 Discon		Sortie DC 31 Ouverte	Sortie DC31 Ouv
48		32			15			DC Output 32 Disconnected	Output32 Discon		Sortie DC 32 Ouverte	Sortie DC32 Ouv
49		32			15			DC Output 33 Disconnected	Output33 Discon		Sortie DC 33 Ouverte	Sortie DC33 Ouv
50		32			15			DC Output 34 Disconnected	Output34 Discon		Sortie DC 34 Ouverte	Sortie DC34 Ouv
51		32			15			DC Output 35 Disconnected	Output35 Discon		Sortie DC 35 Ouverte	Sortie DC35 Ouv
52		32			15			DC Output 36 Disconnected	Output36 Discon		Sortie DC 36 Ouverte	Sortie DC36 Ouv
53		32			15			DC Output 37 Disconnected	Output37 Discon		Sortie DC 37 Ouverte	Sortie DC37 Ouv
54		32			15			DC Output 38 Disconnected	Output38 Discon		Sortie DC 38 Ouverte	Sortie DC38 Ouv
55		32			15			DC Output 39 Disconnected	Output39 Discon		Sortie DC 39 Ouverte	Sortie DC39 Ouv
56		32			15			DC Output 40 Disconnected	Output40 Discon		Sortie DC 40 Ouverte	Sortie DC40 Ouv
57		32			15			DC Output 41 Disconnected	Output41 Discon		Sortie DC 41 Ouverte	Sortie DC41 Ouv
58		32			15			DC Output 42 Disconnected	Output42 Discon		Sortie DC 42 Ouverte	Sortie DC42 Ouv
59		32			15			DC Output 43 Disconnected	Output43 Discon		Sortie DC 43 Ouverte	Sortie DC43 Ouv
60		32			15			DC Output 44 Disconnected	Output44 Discon		Sortie DC 44 Ouverte	Sortie DC44 Ouv
61		32			15			DC Output 45 Disconnected	Output45 Discon		Sortie DC 45 Ouverte	Sortie DC45 Ouv
62		32			15			DC Output 46 Disconnected	Output46 Discon		Sortie DC 46 Ouverte	Sortie DC46 Ouv
63		32			15			DC Output 47 Disconnected	Output47 Discon		Sortie DC 47 Ouverte	Sortie DC47 Ouv
64		32			15			DC Output 48 Disconnected	Output48 Discon		Sortie DC 48 Ouverte	Sortie DC48 Ouv
65		32			15			DC Output 49 Disconnected	Output49 Discon		Sortie DC 49 Ouverte	Sortie DC49 Ouv
66		32			15			DC Output 50 Disconnected	Output50 Discon		Sortie DC 50 Ouverte	Sortie DC50 Ouv
67		32			15			DC Output 51 Disconnected	Output51 Discon		Sortie DC 51 Ouverte	Sortie DC51 Ouv
68		32			15			DC Output 52 Disconnected	Output52 Discon		Sortie DC 52 Ouverte	Sortie DC52 Ouv
69		32			15			DC Output 53 Disconnected	Output53 Discon		Sortie DC 53 Ouverte	Sortie DC53 Ouv
70		32			15			DC Output 54 Disconnected	Output54 Discon		Sortie DC 54 Ouverte	Sortie DC54 Ouv
71		32			15			DC Output 55 Disconnected	Output55 Discon		Sortie DC 55 Ouverte	Sortie DC55 Ouv
72		32			15			DC Output 56 Disconnected	Output56 Discon		Sortie DC 56 Ouverte	Sortie DC56 Ouv
73		32			15			DC Output 57 Disconnected	Output57 Discon		Sortie DC 57 Ouverte	Sortie DC57 Ouv
74		32			15			DC Output 58 Disconnected	Output58 Discon		Sortie DC 58 Ouverte	Sortie DC58 Ouv
75		32			15			DC Output 59 Disconnected	Output59 Discon		Sortie DC 59 Ouverte	Sortie DC59 Ouv
76		32			15			DC Output 60 Disconnected	Output60 Discon		Sortie DC 60 Ouverte	Sortie DC60 Ouv
77		32			15			DC Output 61 Disconnected	Output61 Discon		Sortie DC 61 Ouverte	Sortie DC61 Ouv
78		32			15			DC Output 62 Disconnected	Output62 Discon		Sortie DC 62 Ouverte	Sortie DC62 Ouv
79		32			15			DC Output 63 Disconnected	Output63 Discon		Sortie DC 63 Ouverte	Sortie DC63 Ouv
80		32			15			DC Output 64 Disconnected	Output64 Discon		Sortie DC 64 Ouverte	Sortie DC64 Ouv
81		32			15			LVD1 State			LVD1 State		Etat LVD1			Etat LVD1
82		32			15			LVD2 State			LVD2 State		Etat LVD2			Etat LVD2
83		32			15			LVD3 State			LVD3 State		Etat LVD3			Etat LVD3
84		32			15			Communication Fail			Comm Fail		Defaut de communication			Defaut COM
85		32			15			LVD1				LVD1			LVD1				LVD1
86		32			15			LVD2				LVD2			LVD2				LVD2
87		32			15			LVD3				LVD3			LVD3				LVD3
88		32			15			Temperature 1 High Limit	Temp 1 Hi Limit			Limite haute température 1	Lim Haute Temp1
89		32			15			Temperature 2 High Limit	Temp 2 Hi Limit		Limite haute température 2	Lim Haute Temp2
90		32			15			Temperature 3 High Limit	Temp 3 Hi Limit		Limite haute température 3	Lim Haute Temp3
91		32			15			LVD1 Limit			LVD1 Limit		Limite LVD1			Limite LVD1
92		32			15			LVD2 Limit			LVD2 Limit		Limite LVD2			Limite LVD2
93		32			15			LVD3 Limit			LVD3 Limit		Limite LVD3			Limite LVD3
94		32			15			Battery Over Voltage Limit	BatOverVoltLmt		Niveau Sur Tension		Sur Tension
95		32			15			Battery Under Voltage Limit	BatUnderVoltLmt		Niveau Sous Tension		Sous Tension
96		32			15			Temperature Coefficient		Temp Coeff		Coefficient Température		Coeff Temp
97		32			15			Current Sensor Coefficient	Sensor Coeff	Coefficient Capteur courant	Coef Capteur I
98		32			15			Number of Battery		Num of Battery		Numéro Batterie			Numéro Batterie	
99		32			15			Temperature Number		Temp Number		Numéro Capteur Température	Num.Capt.Temp.
100		32			15			Branch Current Coefficient	BranchCurrCoeff		Coef Courant Branche		Coef I-Branche
101		32			15			Distribution Address		Distri Address			Adresse Distribution		Adress Distrib
102		32			15			Current Measurement Output Num	Curr Output Num	Num Sortie Mesure Courant	Sortie Mesure I
103		32			15			Number of Output		Num of Output		Numéro de Sortie DC		Num Sortie DC
104		32			15			DC Over Voltage			DC Over Volt		Sur Tension DC			Sur Tension
105		32			15			DC Under Voltage		DC Under Volt		Sous Tension DC			Sous Tension
106		32			15			DC Output 1 Disconnected	Output1 Discon		Sortie DC 1 Ouverte	Sortie DC1 Ouv
107		32			15			DC Output 2 Disconnected	Output2 Discon		Sortie DC 2 Ouverte	Sortie DC2 Ouv
108		32			15			DC Output 3 Disconnected	Output3 Discon		Sortie DC 3 Ouverte	Sortie DC3 Ouv
109		32			15			DC Output 4 Disconnected	Output4 Discon		Sortie DC 4 Ouverte	Sortie DC4 Ouv
110		32			15			DC Output 5 Disconnected	Output5 Discon		Sortie DC 5 Ouverte	Sortie DC5 Ouv
111		32			15			DC Output 6 Disconnected	Output6 Discon		Sortie DC 6 Ouverte	Sortie DC6 Ouv
112		32			15			DC Output 7 Disconnected	Output7 Discon		Sortie DC 7 Ouverte	Sortie DC7 Ouv
113		32			15			DC Output 8 Disconnected	Output8 Discon		Sortie DC 8 Ouverte	Sortie DC8 Ouv
114		32			15			DC Output 9 Disconnected	Output9 Discon		Sortie DC 9 Ouverte	Sortie DC9 Ouv
115		32			15			DC Output 10 Disconnected	Output10 Discon		Sortie DC 10 Ouverte	Sortie DC10 Ouv
116		32			15			DC Output 11 Disconnected	Output11 Discon		Sortie DC 11 Ouverte	Sortie DC11 Ouv
117		32			15			DC Output 12 Disconnected	Output12 Discon		Sortie DC 12 Ouverte	Sortie DC12 Ouv
118		32			15			DC Output 13 Disconnected	Output13 Discon		Sortie DC 13 Ouverte	Sortie DC13 Ouv
119		32			15			DC Output 14 Disconnected	Output14 Discon		Sortie DC 14 Ouverte	Sortie DC14 Ouv
120		32			15			DC Output 15 Disconnected	Output15 Discon		Sortie DC 15 Ouverte	Sortie DC15 Ouv
121		32			15			DC Output 16 Disconnected	Output16 Discon		Sortie DC 16 Ouverte	Sortie DC16 Ouv
122		32			15			DC Output 17 Disconnected	Output17 Discon		Sortie DC 17 Ouverte	Sortie DC17 Ouv
123		32			15			DC Output 18 Disconnected	Output18 Discon		Sortie DC 18 Ouverte	Sortie DC18 Ouv
124		32			15			DC Output 19 Disconnected	Output19 Discon		Sortie DC 19 Ouverte	Sortie DC19 Ouv
125		32			15			DC Output 20 Disconnected	Output20 Discon		Sortie DC 20 Ouverte	Sortie DC20 Ouv
126		32			15			DC Output 21 Disconnected	Output21 Discon		Sortie DC 21 Ouverte	Sortie DC21 Ouv
127		32			15			DC Output 22 Disconnected	Output22 Discon		Sortie DC 22 Ouverte	Sortie DC22 Ouv
128		32			15			DC Output 23 Disconnected	Output23 Discon		Sortie DC 23 Ouverte	Sortie DC23 Ouv
129		32			15			DC Output 24 Disconnected	Output24 Discon		Sortie DC 24 Ouverte	Sortie DC24 Ouv
130		32			15			DC Output 25 Disconnected	Output25 Discon		Sortie DC 25 Ouverte	Sortie DC25 Ouv
131		32			15			DC Output 26 Disconnected	Output26 Discon		Sortie DC 26 Ouverte	Sortie DC26 Ouv
132		32			15			DC Output 27 Disconnected	Output27 Discon		Sortie DC 27 Ouverte	Sortie DC27 Ouv
133		32			15			DC Output 28 Disconnected	Output28 Discon		Sortie DC 28 Ouverte	Sortie DC28 Ouv
134		32			15			DC Output 29 Disconnected	Output29 Discon		Sortie DC 29 Ouverte	Sortie DC29 Ouv
135		32			15			DC Output 30 Disconnected	Output30 Discon		Sortie DC 30 Ouverte	Sortie DC30 Ouv
136		32			15			DC Output 31 Disconnected	Output31 Discon		Sortie DC 31 Ouverte	Sortie DC31 Ouv
137		32			15			DC Output 32 Disconnected	Output32 Discon		Sortie DC 32 Ouverte	Sortie DC32 Ouv
138		32			15			DC Output 33 Disconnected	Output33 Discon		Sortie DC 33 Ouverte	Sortie DC33 Ouv
139		32			15			DC Output 34 Disconnected	Output34 Discon		Sortie DC 34 Ouverte	Sortie DC34 Ouv
140		32			15			DC Output 35 Disconnected	Output35 Discon		Sortie DC 35 Ouverte	Sortie DC35 Ouv
141		32			15			DC Output 36 Disconnected	Output36 Discon		Sortie DC 36 Ouverte	Sortie DC36 Ouv
142		32			15			DC Output 37 Disconnected	Output37 Discon		Sortie DC 37 Ouverte	Sortie DC37 Ouv
143		32			15			DC Output 38 Disconnected	Output38 Discon		Sortie DC 38 Ouverte	Sortie DC38 Ouv
144		32			15			DC Output 39 Disconnected	Output39 Discon		Sortie DC 39 Ouverte	Sortie DC39 Ouv
145		32			15			DC Output 40 Disconnected	Output40 Discon		Sortie DC 40 Ouverte	Sortie DC40 Ouv
146		32			15			DC Output 41 Disconnected	Output41 Discon		Sortie DC 41 Ouverte	Sortie DC41 Ouv
147		32			15			DC Output 42 Disconnected	Output42 Discon		Sortie DC 42 Ouverte	Sortie DC42 Ouv
148		32			15			DC Output 43 Disconnected	Output43 Discon		Sortie DC 43 Ouverte	Sortie DC43 Ouv
149		32			15			DC Output 44 Disconnected	Output44 Discon		Sortie DC 44 Ouverte	Sortie DC44 Ouv
150		32			15			DC Output 45 Disconnected	Output45 Discon		Sortie DC 45 Ouverte	Sortie DC45 Ouv
151		32			15			DC Output 46 Disconnected	Output46 Discon		Sortie DC 46 Ouverte	Sortie DC46 Ouv
152		32			15			DC Output 47 Disconnected	Output47 Discon		Sortie DC 47 Ouverte	Sortie DC47 Ouv
153		32			15			DC Output 48 Disconnected	Output48 Discon		Sortie DC 48 Ouverte	Sortie DC48 Ouv
154		32			15			DC Output 49 Disconnected	Output49 Discon		Sortie DC 49 Ouverte	Sortie DC49 Ouv
155		32			15			DC Output 50 Disconnected	Output50 Discon		Sortie DC 50 Ouverte	Sortie DC50 Ouv
156		32			15			DC Output 51 Disconnected	Output51 Discon		Sortie DC 51 Ouverte	Sortie DC51 Ouv
157		32			15			DC Output 52 Disconnected	Output52 Discon		Sortie DC 52 Ouverte	Sortie DC52 Ouv
158		32			15			DC Output 53 Disconnected	Output53 Discon		Sortie DC 53 Ouverte	Sortie DC53 Ouv
159		32			15			DC Output 54 Disconnected	Output54 Discon		Sortie DC 54 Ouverte	Sortie DC54 Ouv
160		32			15			DC Output 55 Disconnected	Output55 Discon		Sortie DC 55 Ouverte	Sortie DC55 Ouv
161		32			15			DC Output 56 Disconnected	Output56 Discon		Sortie DC 56 Ouverte	Sortie DC56 Ouv
162		32			15			DC Output 57 Disconnected	Output57 Discon		Sortie DC 57 Ouverte	Sortie DC57 Ouv
163		32			15			DC Output 58 Disconnected	Output58 Discon		Sortie DC 58 Ouverte	Sortie DC58 Ouv
164		32			15			DC Output 59 Disconnected	Output59 Discon		Sortie DC 59 Ouverte	Sortie DC59 Ouv
165		32			15			DC Output 60 Disconnected	Output60 Discon		Sortie DC 60 Ouverte	Sortie DC60 Ouv
166		32			15			DC Output 61 Disconnected	Output61 Discon		Sortie DC 61 Ouverte	Sortie DC61 Ouv
167		32			15			DC Output 62 Disconnected	Output62 Discon		Sortie DC 62 Ouverte	Sortie DC62 Ouv
168		32			15			DC Output 63 Disconnected	Output63 Discon		Sortie DC 63 Ouverte	Sortie DC63 Ouv
169		32			15			DC Output 64 Disconnected	Output64 Discon		Sortie DC 64 Ouverte	Sortie DC64 Ouv
170		32			15			Communication Fail			Comm Fail		Defaut de communication			Defaut COM
171		32			15			LVD1				LVD1			LVD1				LVD1
172		32			15			LVD2				LVD2			LVD2				LVD2
173		32			15			LVD3				LVD3			LVD3				LVD3
174		32			15			Temperature 1 Over Temperature	T1 Over Temp		Température Haute 1		Temp Haute 1
175		32			15			Temperature 2 Over Temperature	T2 Over Temp	Température Haute 2		Temp Haute 2
176		32			15			Temperature 3 Over Temperature	T3 Over Temp		Température Haute 3		Temp Haute 3
177		32			15			LargeDU DC Distribution		DC Distribution		Distribution DC grande		Distrib grande
178		32			15			Temperature 1 Low Limit		Temp1 Low Limit		Limite Basse Température 1	Lim Basse Temp1
179		32			15			Temperature 2 Low Limit		Temp2 Low Limit		Limite Basse Température 2	Lim Basse Temp2
180		32			15			Temperature 3 Low Limit		Temp3 Low Limit		Limite Basse Température 3	Lim Basse Temp3
181		32			15			Temperature 1 Under Temperature	T1 Under Temp		Température Basse 1		Temp Basse1
182		32			15			Temperature 2 Under Temperature	T2 Under Temp		Température Basse 2		Temp Basse2
183		32			15			Temperature 3 Under Temperature	T3 Under Temp		Température Basse 3		Temp Basse3
184		32			15			Temperature 1 Alarm		Temp1 Alarm		Alarme température 1		Alarme temp 1
185		32			15			Temperature 2 Alarm		Temp2 Alarm		Alarme température 2		Alarme temp 2
186		32			15			Temperature 3 Alarm		Temp3 Alarm		Alarme température 3		Alarme temp 3
187		32			15			Voltage Alarm			Voltage Alarm		Alarme tension			Alarme tension
188		32			15			No Alarm			No Alarm		Aucune Alarme			Aucune Alarme
189		32			15			Over Temperature		Over Temp		Température Haute		Temp Haute
190		32			15			Under Temperature		Under Temp		Température Basse		Temp Basse
191		32			15			No Alarm			No Alarm		Aucune Alarme			Aucune Alarme
192		32			15			Over Temperature		Over Temp		Température Haute		Temp Haute
193		32			15			Under Temperature		Under Temp		Température Basse		Temp Basse
194		32			15			No Alarm			No Alarm		Aucune Alarme			Aucune Alarme
195		32			15			Over Temperature		Over Temp	Température Haute		Temp Haute
196		32			15			Under Temperature		Under Temp		Température Basse		Temp Basse
197		32			15			No Alarm			No Alarm		Aucune Alarme			Aucune Alarme
198		32			15			Over Voltage			Over Voltage		Sur Tension DC			Sur Tension
199		32			15			Under Voltage			Under Voltage		Sous Tension DC			Sous Tension
200		32			15			Voltage Alarm			Voltage Alarm		Alarme tension			Alarme tension
201		32			15			DC Distr Communication Fail	DCD Comm Fail		Pas de réponse Distribution	Pas de réponse
202		32			15			Normal				Normal			Normal				Normal
203		32			15			Failure				Failure			Défaut				Défaut
204		32			15			DC Distr Communication Fail	DCD Comm Fail		Pas de réponse Distribution	Pas de réponse
205		32			15			On				On			Connecté			Connecté
206		32			15			Off				Off			Déconnecté			Déconnecté
207		32			15			On				On			Connecté			Connecté
208		32			15			Off				Off			Déconnecté			Déconnecté
209		32			15			On				On			Connecté			Connecté
210		32			15			Off				Off			Déconnecté			Déconnecté
211		32			15			Temperature 1 Sensor Failure	T1 Sensor Fail		Défaut Capteur Température 1	Déf.Capt.Temp.1
212		32			15			Temperature 2 Sensor Failure	T2 Sensor Fail		Défaut Capteur Température 2	Déf.Capt.Temp.2
213		32			15			Temperature 3 Sensor Failure	T3 Sensor Fail			Défaut Capteur Température 3	Déf.Capt.Temp.3
214		32			15			Connected			Connected		Fermé				Fermé
215		32			15			Disconnected			Disconnected		Ouvert				Ouvert
216		32			15			Connected			Connected		Fermé				Fermé
217		32			15			Disconnected			Disconnected		Ouvert				Ouvert
218		32			15			Connected			Connected		Fermé				Fermé
219		32			15			Disconnected			Disconnected		Ouvert				Ouvert
220		32			15			Normal				Normal			Normal				Normal
221		32			15			DC Distr Communication Fail	DCD Comm Fail	Pas de réponse Distribution	Pas de réponse
222		32			15			Normal				Normal			Normal				Normal
223		32			15			Alarm				Alarm			Alarme				Alarme
224		32			15			Branch 7 Current		Branch 7 Curr		Courant Branche 7		I Branche7
225		32			15			Branch 8 Current		Branch 8 Curr		Courant Branche 8		I Branche8
226		32			15			Branch 9 Current		Branch 9 Curr		Courant Branche 9		I Branche9
227		32			15			Existence State			Existence State		Détection			Détection
228		32			15			Existent			Existent		Présent				Présent	
229		32			15			Not Existent			Not Existent		Non Présent			Non Présent
230		32			15			LVD Number			LVD Number		Numéro de LVDs			Numéro LVDs
231		32			15			0				0			0				0
232		32			15			1				1			1				1
233		32			15			2				2			2				2
234		32			15			3				3			3				3
235		32			15			Battery Shutdown Number		Batt SD Num		Numéro de batterie à ouvrir	N bat ouvrir
236		32			15			Rated Capacity			Rated Capacity		Estimation Capacité		Estim. Capa.

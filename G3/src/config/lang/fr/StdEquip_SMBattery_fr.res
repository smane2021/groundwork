﻿#
#  Locale language support:french
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN						ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE			
1		32			15			Battery Current						Batt Current		Courant batterie		I batterie
2		32			15			Battery Capacity					Batt Capacity		Capacité batterie		Capacité bat.
3		32			15			Battery Voltage						Batt Voltage		Tension batterie		V batterie
4		32			15			Ambient Temperature					Ambient Temp		Température ambiante		T ambiante
5		32			15			Acid Temperature					Acid Temp		Température acide		T acide
6		32			15			Total Time Batt Temp GT 30 deg.C			ToT GT 30 deg.C		Temps total Temp. batt. sup 30C	Tmp Tot sup 30C
7		32			15			Total Time Batt Temp LT 10 deg.C			ToT LT 10 deg.C		Temps total Temp. batt. inf 10C	Tmp Tot inf 10C
8		32			15			Battery Block 1 Voltage					Block 1 Volt	Tension bloc 1			Tension bloc 1
9		32			15			Battery Block 2 Voltage					Block 2 Volt	Tension bloc 2			Tension bloc 2
10		32			15			Battery Block 3 Voltage					Block 3 Volt	Tension bloc 3			Tension bloc 3
11		32			15			Battery Block 4 Voltage					Block 4 Volt	Tension bloc 4			Tension bloc 4
12		32			15			Battery Block 5 Voltage					Block 5 Volt	Tension bloc 5			Tension bloc 5
13		32			15			Battery Block 6 Voltage					Block 6 Volt	Tension bloc 6			Tension bloc 6
14		32			15			Battery Block 7 Voltage					Block 7 Volt	Tension bloc 7			Tension bloc 7
15		32			15			Battery Block 8 Voltage					Block 8 Volt	Tension bloc 8			Tension bloc 8
16		32			15			Battery Block 9 Voltage					Block 9 Volt	Tension bloc 9			Tension bloc 9
17		32			15			Battery Block 10 Voltage				Block 10 Volt		Tension bloc 10			Tension bloc 10
18		32			15			Battery Block 11 Voltage				Block 11 Volt		Tension bloc 11			Tension bloc 11
19		32			15			Battery Block 12 Voltage				Block 12 Volt		Tension bloc 12			Tension bloc 12
20		32			15			Battery Block 13 Voltage				Block 13 Volt		Tension bloc 13			Tension bloc 13
21		32			15			Battery Block 14 Voltage				Block 14 Volt		Tension bloc 14			Tension bloc 14
22		32			15			Battery Block 15 Voltage				Block 15 Volt		Tension bloc 15			Tension bloc 15
23		32			15			Battery Block 16 Voltage				Block 16 Volt		Tension bloc 16			Tension bloc 16
24		32			15			Battery Block 17 Voltage				Block 17 Volt		Tension bloc 17			Tension bloc 17
25		32			15			Battery Block 18 Voltage				Block 18 Volt		Tension bloc 18			Tension bloc 18
26		32			15			Battery Block 19 Voltage				Block 19 Volt		Tension bloc 19			Tension bloc 19
27		32			15			Battery Block 20 Voltage				Block 20 Volt		Tension bloc 20			Tension bloc 20
28		32			15			Battery Block 21 Voltage				Block 21 Volt		Tension bloc 21			Tension bloc 21
29		32			15			Battery Block 22 Voltage				Block 22 Volt		Tension bloc 22			Tension bloc 22
30		32			15			Battery Block 23 Voltage				Block 23 Volt		Tension bloc 23			Tension bloc 23
31		32			15			Battery Block 24 Voltage				Block 24 Volt		Tension bloc 24			Tension bloc 24
32		32			15			Battery Block 25 Voltage				Block 25 Volt		Tension bloc 25			Tension bloc 25
33		32			15			Shunt Voltage						Shunt Voltage		Tension de shunt		V shunt
34		32			15			Battery Leakage						Batt Leakage		Fuite batterie			Fuite batterie
35		32			15			Low Acid Level						Low Acid Level		Niveau acide bas		Niv. acide bas
36		32			15			Battery Disconnected					Batt Disconnec		Batterie déconnectée		Bat déconnectée
37		32			15			Battery High Temperature				Batt High Temp			Température haute batterie	Temp. haute bat
38		32			15			Battery Low Temperature					Batt Low Temp		Température basse batterie	Temp. basse bat
39		32			15			Block Voltage Difference				Block Volt Diff		Différence tension de blocs	Diff V blocs
40		32			15			Battery Shunt Size					Batt Shunt Size		Taille shunt batterie		Taille shnt bat
41		32			15			Block Voltage Difference				Block Volt Diff		Différence tension de blocs	Diff V blocs
42		32			15			Battery High Temp Limit					High Temp Limit		Limite Temp. haute batterie	Lim T Haut bat
43		32			15			Batt High Temp Limit Hysteresis				Batt HiTemp Hys		Limite hyst Temp. haute bat	Lim T Haut hys
44		32			15			Battery Low Temp Limit				Low Temp Limit		Limite Temp. basse batterie	Lim T bas bat
45		32			15			Batt Low Temp Limit Hysteresis				Batt LoTemp Hys		Limite hyst Temp. basse bat	Lim T bas hys
46		32			15			Exceed Batt Current Limit				Over Curr Limit	Limite courant excessif		Lim I excés
47		32			15			Battery Leakage						Battery Leakage		Fuite batterie			Fuite batterie
48		32			15			Low Acid Level						Low Acid Level		Niveau acide bas		Niv. acide bas
49		32			15			Battery Disconnected					Batt Disconnec		Batterie déconnectée		Bat déconnectée
50		32			15			Battery High Temperature				Batt High Temp		Température haute batterie	Temp. haute bat
51		32			15			Battery Low Temperature					Batt Low Temp		Température basse batterie	Temp. basse bat
52		32			15			Cell Voltage Difference					Cell Volt Diff		Différence tension de blocs	Diff V blocs
53		32			15			SM Unit Fail						SM Unit Fail		Défaut Module SM		Déf Module SM
54		32			15			Battery Disconnected					Batt Disconnec		Batterie déconnectée		Bat déconnectée
55		32			15			No							No			Non				Non
56		32			15			Yes							Yes			Oui				Oui
57		32			15			No							No			Non				Non
58		32			15			Yes							Yes			Oui				Oui
59		32			15			No							No			Non				Non
60		32			15			Yes							Yes			Oui				Oui
61		32			15			No							No			Non				Non
62		32			15			Yes							Yes			Oui				Oui
63		32			15			No							No			Non				Non
64		32			15			Yes							Yes			Oui				Oui
65		32			15			No							No			Non				Non
66		32			15			Yes							Yes			Oui				Oui
67		32			15			No							No			Non				Non
68		32			15			Yes							Yes			Oui				Oui
69		32			15			SM Battery						SM Battery		SM Batterie				SM Bat
70		32			15			Over Battery Current					Over Batt Curr		Surcourant batterie		Surcourant bat
71		32			15			Battery Capacity (%)					Batt Cap (%)	Capacité batterie(%)		Capacité bat(%)
72		32			15			SMBAT Fail						SMBAT Fail		Défaut SM batterie		Déf SM bat
73		32			15			No							No			Non				Non
74		32			15			Yes							Yes			Oui				Oui
75		32			15			AI 4							AI 4			AI 4				AI 4
76		32			15			AI 7							AI 7			AI 7				AI 7
77		32			15			DI 4							DI 4			DI 4				DI 4
78		32			15			DI 5							DI 5			DI 5				DI 5
79		32			15			DI 6							DI 6			DI 6				DI 6
80		32			15			DI 7							DI 7			DI 7				DI 7
81		32			15			DI 8							DI 8			DI 8				DI 8
82		32			15			Relay 1 Status						Relay 1 Status		Etat relais 1			Etat relais 1
83		32			15			Relay 2 Status						Relay 2 Status		Etat relais 2			Etat relais 2
84		32			15			No							No			Non				Non
85		32			15			Yes							Yes			Oui				Oui
86		32			15			No							No			Non				Non
87		32			15			Yes							Yes			Oui				Oui
88		32			15			No							No			Non				Non
89		32			15			Yes							Yes			Oui				Oui
90		32			15			No							No			Non				Non
91		32			15			Yes							Yes			Oui				Oui
92		32			15			No							No			Non				Non
93		32			15			Yes							Yes			Oui				Oui
94		32			15			Off							Off			Fermé				Fermé
95		32			15			On							On			Ouvert				Ouvert
96		32			15			Off							Off			Fermé				Fermé
97		32			15			On							On			Ouvert				Ouvert
98		32			15			Relay 1 On/Off					Relay 1 On/Off		Relais 1 ouvert/fermé		K 1 O/F
99		32			15			Relay 2 On/Off					Relay 2 On/Off		Relais 2 ouvert/fermé		K 2 O/F
100		32			15			AI 2							AI 2			AI 2				AI 2
101		32			15			Battery Temperature Sensor Fail			T Sensor Fail		Défaut capteur Temp. batterie	Déf capteur T
102		32			15			Low Capacity						Low Capacity		Capacite basse			Capa Basse
103		32			15			Existence State						Existence State		Détection			Détection
104		32			15			Existent						Existent		Présent				Présent
105		32			15			Not Existent						Not Existent		Non Présent			Non Présent
106		32			15			Battery Communication Fail				Batt Comm Fail		Defaut de comunication batteries		Defaut COM Batt
107		32			15			Communication OK					Comm OK		Communication OK			COMM OK
108		32			15			Communication Fail					Comm Fail	Defaut de comunication		Defaut de COM
109		32			15			Rated Capacity						Rated Capacity		Estimation Capacité		Estim.Capa.
110		32			15			Battery Management					Batt Management		Batterie Management		Bat.Management
111		32			15			SM Batt Temp High Limit					SMBat TempHiLmt		SMBAT Température batterie Haute	SM T.Batt Haute
112		32			15			SM Batt Temp Low Limit					SMBat TempLoLmt		SMBAT Température batterie Basse	SM T.Batt Basse
113		32			15			SM Batt Temp						SM Bat Temp		Capteur Température		Capt.Temp.
114		32			15			Battery Communication Fail				Batt Comm Fail		Defaut de comunication batteries		Defaut COM Batt
115		32			15			Battery Temp not Used					Bat Temp No Use		Aucun Capteur Température	AucunCapt.Temp.

﻿#
#  Locale language support:French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Bus Bar Voltage				Bus Bar Voltage		Tension Bus Barre		V Bus Barre
2	32			15			Channel 1 Current			Channel1 Curr		Courant 1			Courant 1
3	32			15			Channel 2 Current			Channel2 Curr		Courant 2			Courant 2
4	32			15			Channel 3 Current			Channel3 Curr		Courant 3			Courant 3
5	32			15			Channel 4 Current			Channel4 Curr		Courant 4			Courant 4
6	32			15			Channel 1 Energy Consumption		Channel1 Energy		Puissance 1			Puissance 1
7	32			15			Channel 2 Energy Consumption		Channel2 Energy		Puissance 2			Puissance 2
8	32			15			Channel 3 Energy Consumption		Channel3 Energy		Puissance 3			Puissance 3
9	32			15			Channel 4 Energy Consumption		Channel4 Energy		Puissance 4			Puissance 4

10	32			15			Channel 1			Channel1	Lecture I 1		Lecture I1
11	32			15			Channel 2			Channel2		Lecture I 2		Lecture I2
12	32			15			Channel 3			Channel3		Lecture I 3		Lecture I3
13	32			15			Channel 4			Channel4		Lecture I 4		Act.Lecture I4

14	32			15			Clear Channel 1 Energy			ClrChan1Energy		Reset Compteur Puissance 1	Reset Compt.P 1
15	32			15			Clear Channel 2 Energy			ClrChan2Energy		Reset Compteur Puissance 2	Reset Compt.P 2
16	32			15			Clear Channel 3 Energy			ClrChan3Energy		Reset Compteur Puissance 3	Reset Compt.P 3
17	32			15			Clear Channel 4 Energy			ClrChan4Energy		Reset Compteur Puissance 4	Reset Compt.P 4

18	32			15			Shunt 1 Voltage				Shunt1 Volt		Tension Shunt 1			V Shunt 1
19	32			15			Shunt 1 Current				Shunt1 Curr		Courant Shunt 1			I Shunt 1
20	32			15			Shunt 2 Voltage				Shunt2 Volt		Tension Shunt 2			V Shunt 2
21	32			15			Shunt 2 Current				Shunt2 Curr		Courant Shunt 2			I Shunt 2
22	32			15			Shunt 3 Voltage				Shunt3 Volt		Tension Shunt 3			V Shunt 3
23	32			15			Shunt 3 Current				Shunt3 Curr		Courant Shunt 3			I Shunt 3
24	32			15			Shunt 4 Voltage				Shunt4 Volt		Tension Shunt 4			V Shunt 4
25	32			15			Shunt 4 Current				Shunt4 Curr		Courant Shunt 4			I Shunt 4

26	32			15			Enabled					Enabled			Actif				Actif
27	32			15			Disabled				Disabled		Inactif				Inactif

28	32			15			Existence State				Exist State		Fonction Presente		Fonct.Presente
29	32			15			Communication Fail			Comm Fail		Defaut Communication		Defaut Com.

30	32			15			DC Meter				DC Meter		Mesure DC			Mesure DC

31	32			15			Clear					Clear			Efface				Efface

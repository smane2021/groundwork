﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15		Phase A Voltage			Phase A Voltage		Tension phase A			Tension phase A
2	32			15			Phase B Voltage		Phase B Voltage	Tension phase B			Tension phase B
3	32			15			Phase C Voltage		Phase C Voltage	Tension phase C			Tension phase C
4	32			15			Line AB Voltage		Line AB Voltage	Tension AB			Tension  AB
5	32			15			Line BC Voltage		Line BC Voltage	Tension BC			Tension  BC
6	32			15			Line CA Voltage		Line CA Voltage	Tension CA			Tension  CA
7	32			15			Phase A Current		Phase A Current	Courant phase A			Courant phase A
8	32			15			Phase B Current		Phase B Current	Courant phase B			Courant phase B
9	32			15			Phase C Current		Phase C Current	Courant phase C			Courant phase C
10	32			15			AC Frequency		AC Frequency		Frequence			Frequence
11	32			15			Total Real Power		Total Real Pwr			Puissance reelle totale		P. Reelle total
12	32			15			Phase A Real Power		PH-A Real Power	Puissance reelle phase A	P. Reelle ph.A
13	32			15			Phase B Real Power		PH-B Real Power	Puissance reelle phase B	P. Reelle ph.B
14	32			15			Phase C Real Power		PH-C Real Power	Puissance reelle phase C	P. Reelle ph.C
15	32			15			Total Reactive Power		Total React Pwr		Puissance reactive totale	P. React totale
16	32			15			Phase A Reactive Power		PH-A React Pwr		Puissance reactive phase A	P. React ph A
17	32			15			Phase B Reactive Power		PH-B React Pwr		Puissance reactive phase B	P. React ph B
18	32			15			Phase C Reactive Power		PH-C React Pwr		Puissance reactive phase C	P. React ph C
19	32			15			Total Apparent Power		Total Appar Pwr		Puissance apparante totale	P. App totale
20	32			15			Phase A Apparent Power		PH-A Appar Pwr	Puissance apparante phase A	P. App phase A
21	32			15			Phase B Apparent Power		PH-B Appar Pwr	Puissance apparante phase B	P. App phase B
22	32			15			Phase C Apparent Power		PH-C Appar Pwr	Puissance apparante phase C	P. App phase C
23	32			15			Power Factor		Power Factor		Facteur de puissance		Facteur de P
24	32			15			Phase A Power Factor		PH-A Pwr Factor		Facteur de puissance phase A	Facteur P phA
25	32			15			Phase B Power Factor		PH-B Pwr Factor		Facteur de puissance phase B	Facteur P phB
26	32			15			Phase C Power Factor		PH-C Pwr Factor		Facteur de puissance phase C	Facteur P phC
27	32			15			Phase A Current Crest Factor		Ia Crest Factor		Facteur de crête phase A	Facteur crêteIa
28	32			15			Phase B Current Crest Factor		Ib Crest Factor		Facteur de crête phase B	Facteur crêteIb
29	32			15			Phase C Current Crest Factor		Ic Crest Factor		Facteur de crête phase C	Facteur crêteIc
30	32			15			Phase A Current THD		PH-A Curr THD		Courant THD phase A		Courant THD A
31	32			15			Phase B Current THD		PH-B Curr THD		Courant THD phase B		Courant THD B
32	32			15			Phase C Current THD		PH-C Curr THD		Courant THD phase C		Courant THD C
33	32			15			Phase A Voltage THD		PH-A Volt THD		Tension THD phase A		Tension THD A
34	32			15			Phase B Voltage THD		PH-B Volt THD		Tension THD phase B		Tension THD B
35	32			15			Phase C Voltage THD		PH-C Volt THD		Tension THD phase C		Tension THD C
36	32			15			Total Real Energy		Tot Real Energy		Energie reelle totale		E reelle totale
37	32			15			Total Reactive Energy		Tot Reac Energy		Energie reactive totale		E react. totale
38	32			15			Total Apparent Energy		Tot Appa Energy		Energie apparante totale	E app. totale
39	32			15		Ambient Temperature		Ambient Temp		Temperature ambiante		Temp. ambiante
40	32			15			Nominal Line Voltage			Nominal L-Volt		Tension de ligne nominale	V ligne nominal
41	32			15			Nominal Phase Voltage			Nominal PH-Volt		Tension secteur nominale	VAC nominal
42	32			15			Nominal Frequency			Nominal Freq			Frequence nominale		F. nominale
43	32			15			Mains Failure Alarm Limit 1		Mains Fail Alm1		Seuil 1 alarme def. AC		Seuil 1 Alrm AC
44	32			15			Mains Failure Alarm Limit 2	Mains Fail Alm2		Seuil 2 alarme def. AC		Seuil 2 Alrm AC
45	32			15			Voltage Alarm Limit 1			Volt Alm Limit1		Seuil 1 alarme tension		Seuil 1 Al V
46	32			15			Voltage Alarm Limit 2			Volt Alm Limit2		Seuil 2 alarme tension		Seuil 2 Al V
47	32			15			Frequency Alarm Limit			Freq Alm Limit		Seuil alarme frequence		Seuil Al F
48	32			15			High Temperature Alarm Limit	High Temp Limit		Limite haute temperature	Limite Haute T
49	32			15			Low Temperature Alarm Limit		Low Temp Limit		Limite basse temperature	Limite Basse T
50	32			15			Supervision Fail			SupervisionFail		Defaut supervision		Def supervision
51	32			15			Line AB Over Voltage 1			L-AB Over Volt1		Haute Tension ligne AB		Haute V AB
52	32			15			Line AB Over Voltage 2			L-AB Over Volt2	Tres haute Tension ligne AB	Tres haute V AB
53	32			15			Line AB Under Voltage 1			L-AB UnderVolt1		Basse Tension ligne AB		Basse V BC
54	32			15			Line AB Under Voltage 2			L-AB UnderVolt2		Tres Basse Tension ligne AB	Tres Basse V AB
55	32			15			Line BC Over Voltage 1			L-BC Over Volt1	Haute Tension ligne BC		Haute V BC
56	32			15			Line BC Over Voltage 2			L-BC Over Volt2	Tres haute Tension ligne BC	Tres haute V BC
57	32			15			Line BC Under Voltage 1			L-BC UnderVolt1		Basse Tension ligne BC		Basse V BC
58	32			15			Line BC Under Voltage 2			L-BC UnderVolt2		Tres Basse Tension ligne BC	Tres Basse V BC
59	32			15			Line CA Over Voltage 1			L-CA Over Volt1	Haute Tension ligne CA		Haute V CA
60	32			15			Line CA Over Voltage 2			L-CA Over Volt2	Tres haute Tension ligne CA	Tres haute V CA
61	32			15			Line CA Under Voltage 1			L-CA UnderVolt1		Basse Tension ligne CA		Basse V CA
62	32			15			Line CA Under Voltage 2			L-CA UnderVolt2		Tres Basse Tension ligne CA	Tres Basse V CA
63	32			15			Phase A Over Voltage 1			PH-A Over Volt1	Haute tension phase A		Haute V phA
64	32			15			Phase A Over Voltage 2			PH-A Over Volt2	Tres haute tension phase A	Tres haut V phA
65	32			15			Phase A Under Voltage 1			PH-A UnderVolt1		Basse tension phase A		Basse V phA
66	32			15			Phase A Under Voltage 2			PH-A UnderVolt2		Tres basse tension phase A	Tres bas V phA
67	32			15			Phase B Over Voltage 1			PH-B Over Volt1	Haute tension phase B		Haute V phB
68	32			15			Phase B Over Voltage 2			PH-B Over Volt2	Tres haute tension phase B	Tres haut V phB
69	32			15			Phase B Under Voltage 1			PH-B UnderVolt1		Basse tension phase B		Basse V phB
70	32			15			Phase B Under Voltage 2			PH-B UnderVolt2		Tres basse tension phase B	Tres bas V phB
71	32			15			Phase C Over Voltage 1			PH-C Over Volt1	Haute tension phase C		Haute V phC
72	32			15			Phase C Over Voltage 2			PH-C Over Volt2	Tres haute tension phase C	Tres haut V phC
73	32			15			Phase C Under Voltage 1			PH-C UnderVolt1		Basse tension phase C		Basse V phC
74	32			15			Phase C Under Voltage 2			PH-C UnderVolt2		Tres basse tension phase C	Tres bas V phC
75	32			15			Mains Failure				Mains Fail		Defaut secteur			Defaut secteur
76	32			15			Severe Mains Failure			SevereMainsFail		Defaut secteur severe		Def severe sect
77	32			15			High Frequency				High Frequency		Frequence haute			F haute
78	32			15			Low Frequency				Low Frequency		Frequence basse			F basse
79	32			15			High Temperature			High Temp		Temperature haute		T haute
80	32			15			Low Temperature				Low Temperature		Temperature basse		T basse
81	32			15			OB AC Unit				OB AC Unit		Info secteur			Info secteur
82	32			15			Supervision Fail			SupervisionFail	Defaut supervision		Def supervision
83	32			15			No					No			Non				Non
84	32			15			Yes					Yes			Oui				Oui
85	32			15			Phase A Mains Failure Counter		PH-A Fail Count	Cpt de defauts secteur phase A	Cpt def sec phA
86	32			15			Phase B Mains Failure Counter		PH-B Fail Count	Cpt de defauts secteur phase B	Cpt def sec phB
87	32			15			Phase C Mains Failure Counter		PH-C Fail Count	Cpt de defauts secteur phase C	Cpt def sec phC
88	32			15			Frequency Failure Counter		Freq Fail Count		Compteur de defauts frequence	Cpt defauts F
89	32			15			Reset Phase A Mains Fail Counter	RstPH-AFailCnt	Reset compteur def secteur phA	Rst def sec phA
90	32			15			Reset Phase B Mains Fail Counter	RstPH-BFailCnt	Reset compteur def secteur phB	Rst def sec phB
91	32			15			Reset Phase C Mains Fail Counter	RstPH-CFailCnt	Reset compteur def secteur phC	Rst def sec phC
92	32			15			Reset Frequency Fail Counter		RstFreqFailCnt		Reset compteur defaut frequence		Rst cpt Def F
93	32			15			Current Alarm Limit			Curr Alm Limit		Seuil  alarme courant		Seuil Def I
94	32			15			Phase A  High Current			PH-A High Curr		Surcourant phase A		Surcourant phA
95	32			15			Phase B  High Current			PH-B High Curr		Surcourant phase B		Surcourant phB
96	32			15			Phase C  High Current			PH-C High Curr		Surcourant phase C		Surcourant phC
97	32			15			State					State			Etat				Etat
98	32			15			Off					Off			Arrêt				Arrêt
99	32			15			On					On			Marche				Marche
100	32			15			State					State			Etat				Etat
101	32			15			Existent				Existent		Presente			Presente
102	32			15		Not Existent				Not Existent		Non Presente			Non Presente
103	32			15			AC Type					AC Type			Type Raccordement		Type Raccord.
104	32			15			None					None			Aucune				Aucune
105	32			15			Single Phase				Single Phase		Entree Mono Phase		Entree Mono Ph
106	32			15			Three Phases				Three Phases		Entree Tri Phasee		Entree Tri Ph
107	32			15			Mains Failure (Single)			Mains Failure		Coupure 1 phase			Coupure 1 phase
108	32			15			Severe Mains Failure (Single)		SevereMainsFail			Coupure Secteur			Coupure Secteur

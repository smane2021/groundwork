﻿#
#  Locale language support:French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			BMS				BMS		BMS			BMS
2	32			15			Rated Capacity				Rated Capacity		Capacite batt		Capa batt
3	32			15			Communication Fail			Comm Fail		Communication HS		Com HS
4	32			15			Existence State				Existence State		Etat presence		Etat presence
5	32			15			Existent				Existent		Present		Present
6	32			15			Not Existent				Not Existent		Absent			Absent
7	32			15			Battery Voltage				Batt Voltage		Tension batterie		Tension Bat
8	32			15			Battery Current				Batt Current		Courant batterie	Courant Bat
9	32			15			Battery Rating (Ah)			Batt Rating(Ah)		Capacite batt (Ah)		Capa batt
10	32			15			Battery Capacity (%)			Batt Cap (%)		Batt Cap (%)		Batt Cap (%)
11	32			15			Bus Voltage				Bus Voltage		Tension Bus		Tension Bus
12	32			15			Battery Center Temperature(AVE)		Batt Temp(AVE)		Batt Temp(MOY)		Batt Temp(MOY)
13	32			15			Ambient Temperature			Ambient Temp		Temp ambiante		Temp Amb
29	32			15			Yes					Yes			Oui			Oui
30	32			15			No					No			Non		Non
31	32			15			Single Over Voltage Alarm		SingleOverVolt		Surtension Elt	Surtension Elt
32	32			15			Single Under Voltage Alarm		SingleUnderVolt		Soustension Elt		SoustensionElt
33	32			15			Whole Over Voltage Alarm		WholeOverVolt			Surtension Br-Batt		Sur_V Br-Batt
34	32			15			Whole Under Voltage Alarm		WholeUnderVolt				Soustension Br-Batt		Sous_V Br-Batt
35	32			15			Charge Over Current Alarm		ChargeOverCurr		Surcourant charge		Sur_I  charge
36	32			15			Discharge Over Current Alarm		DisCharOverCurr		Surcourant decharge		Sur_I  decharge
37	32			15			High Battery Temperature Alarm		HighBattTemp		Temp haute batterie		Temp haute batt
38	32			15			Low Battery Temperature Alarm		LowBattTemp		Temp basse batterie		Temp basse batt
39	32			15			High Ambient Temperature Alarm		HighAmbientTemp		Temp amb haute		Temp amb haute 
40	32			15			Low Ambient Temperature Alarm		LowAmbientTemp		Temp amb basse		Temp amb basse
41	32			15			High PCB Temperature Alarm		HighPCBTemp			Alarme temp haute PCB	AlmtempbassePCB
42	32			15			Low Battery Capacity Alarm		LowBattCapacity			Alarme capa basse batterie		Al capa bas
43	32			15			High Voltage Difference Alarm		HighVoltDiff		Al difference tension haute		Al diff_V_haute
44	32			15			Single Over Voltage Protect		SingOverVProt		Sur_V_Prot_Elt		Sur_V_Prot_Elt
45	32			15			Single Under Voltage Protect		SingUnderVProt	Sous_V_Prot_Elt		Sous_V_Prot_Elt
46	32			15			Whole Over Voltage Protect		WholeOverVProt		Surt_V_Br_Batt_Prot		SurtV_BrBatProt
47	32			15			Whole Under Voltage Protect		WholeUnderVProt				Soust_V_Br_Batt_Prot		SouV_BrBatProt
48	32			15			Short Circuit Protect			ShortCircProt		Protect court-circuit		CourtCircprot
49	32			15			Over Current Protect			OverCurrProt		Prot_Sur_I		Prot_Sur_I
50	32			15			Charge High Temperature Protect		CharHiTempProt		Prot_TempHaute en charge		Prot_T°H_Charg
51	32			15			Charge Low Temperature Protect		CharLoTempProt		Prot_TempBasse en charge		Prot_T°B_Charg
52	32			15			Discharge High Temp Protect		DischHiTempProt		Prot_TempHaute endecharge	Prot_T H_DeChar
53	32			15			Discharge Low Temp Protect		DischLoTempProt		Prot_TempHaute en charge		Prot_T H_DeChar
54	32			15			Front End Sample Comm Fault		SampCommFault			Samp Comm HS	Samp Comm HS
55	32			15			Temp Sensor Disconnect Fault		TempSensDiscFlt		Al Capt Temp deconnecte		Al Capt T_decon
56	32			15			Charging				Charging		Charge		Charge
57	32			15			Discharging				Discharging		Decharge			Decharge
58	32			15			Charging MOS Breakover			CharMOSBrkover		Charging MOS Breakover		CharMOSBrkover
59	32			15			Dischanging MOS Breakover		DischMOSBrkover		Dischanging MOS Breakover		DischMOSBrkover
60	32			15			Communication Address			CommAddress			Adresse Communication		Adr-Comm
61	32			15			Current Limit Activation		CurrLmtAct		Limitation de courant active		I-Limit_Activ




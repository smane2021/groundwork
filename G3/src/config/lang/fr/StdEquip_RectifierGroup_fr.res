﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN			FULL_IN_LOCALE			ABBR_IN_LOCALE			
1	32			15			Rectifier Group					Rect Group			Groupe redresseur		Grp redresseur
2	32			15			Total Current					Tot Rect Curr			Courant total			Courant Red.
3	32			15			Average Voltage					Rect Voltage			Tension moyenne			Tension Red.
4	32			15			Rectifier Capacity Used			Rect Cap Used			Capacité de redresseur utilisée		Capuchon droit utilisé
5	32			15			Maximum Capacity Used			Max Cap Used			Max Taux de charge Red		Max Taux charge
6	32			15			Minimum Capacity Used			Min Cap Used			Min Taux de charge Red		Min Taux charge
7	32			15			Total Rectifiers Communicating		Num Rects Comm			Nbr redresseur en communication	Nbr Red en com.
8	32			15			Valid Rectifiers			Valid Rects			Redresseur valide		Red.valide
9	32			15			Number of Rectifiers			Num of Rects			Nombre de redresseurs		Nbre Red.
10	32			15			Rectifier AC Fail State			Rect AC Fail		Défaut AC redresseur		Défaut AC red
11	32			15			Rectifier(s) Fail Status		Rect(s) Fail			Multi Redresseur en défaut	Multi Déf Red
12	32			15			Rectifier Current Limit			Rect Curr Limit			Limitation courant		Lim courant
13	32			15			Rectifier Trim				Rectifier Trim			Ajust. tension			Ajust. tension
14	32			15			DC On/Off Control				DC On/Off Ctrl			DC On/Off			DC On/Off
15	32			15			AC On/Off Control				AC On/Off Ctrl			AC On/Off			AC On/Off
16	32			15			Rectifiers LED Control			Rect LED Ctrl			Test leds			Test leds
17	32			15			Fan Speed Control				Fan Speed Ctrl			Vitesse max ventil		Vit. max ventil
18	32			15			Rated Voltage					Rated Voltage			Tension de repli		Tension repli
19	32			15			Rated Current					Rated Current			Courant max de repli		Imax repli
20	32			15			HVSD Limit				HVSD Limit			Seuil sur tension		seuil surU
21	32			15			Low Voltage Limit			Low Volt Limit			limite tension basse		lim tens. basse
22	32			15			High Temperature Limit			High Temp Limit				limite temp. haute		lim temp. haute
24	32			15			HVSD Restart Time			HVSD Restart T			Timeout blocage sur-tension	TO blocage surU
25	32			15			Walk-In Time				Walk-In Time			Temporisation de démarrage	Tempo démarrage
26	32			15			Walk-In					Walk-In			Activation Tempo. Démarrage	Act.Tempo.Démar
27	32			15			Minimum Redundancy				Min Redundancy			Redondance min			Redondance min
28	32			15			Maximum Redundancy				Max Redundancy			Redondance max			Redondance max
29	32			15			Turn Off Delay				Turn Off Delay			Délais Arrêt			Délais Arrêt
30	32			15			Cycle Period				Cycle Period			Cycle périodique		Cycle périodi.
31	32			15			Cycle Activation Time			Cyc Active Time				Durée cycle activation		Durée cycl acti
32	32			15			Rectifier AC Fail				Rect AC Fail			Défaut AC redresseur		Défaut AC red
33	32			15			Multiple Rectifiers Fail		Multi-Rect Fail			Plus 1 redresseur en défaut	Défaut +1 Red
36	32			15			Normal						Normal				Normal				Normal
37	32			15			Fail						Fail				Défaut				Défaut
38	32			15			Switch Off All					Switch Off All			Tous sur arrêt			Arrêt Tous Red.
39	32			15			Switch On All					Switch On All			Tous en fonction		Tous fonction
42	32			15			All Flashing				All Flashing			Clignotante			Clignotante
43	32			15			Stop Flashing				Stop Flashing			Fixe				Fixe
44	32			15			Full Speed					Full Speed			Vitesse maximum			Vitesse max
45	32			15			Automatic Speed					Auto Speed			Vitesse Automatique		Vitesse Auto
46	32			32			Current Limit Control				Curr Limit Ctrl			Control Limitation courant	Cont.Lim.I
47	32			32			Full Capability Control			Full Cap Ctrl			Puisssance maximum		Puis. Maximum
54	32			15			Disabled					Disabled			Désactivé			Désactivé
55	32			15			Enabled						Enabled				Activé				Activé
68	32			15			ECO Mode					ECO Mode			Mode ECO			Mode ECO
72	32			15			Turn On when AC Over Voltage		Turn On ACOverV			Redresseur ON si Surtension AC	Red  ON Surt AC
73	32			15			No						No				Non				Non
74	32			15			Yes						Yes				Oui				Oui
77	32			15			Pre-CurrLimit Turn-On			Pre-Curr Limit				Pré-limitation au démarrage		Pré-limit Start
78	32			15			Rectifier Power Type			Rect Power Type			Type redresseur			Type red
79	32			15			Double Supply					Double Supply			Double Entree AC		2 Entrees AC	
80	32			15			Single Supply					Single Supply			Simple Entree AC		1 Entree AC
81	32			15			Last Rectifiers Quantity		Last Rects Qty			Dernière Quantitée Redresseur	Dern Quant Red
82	32			15			Rectifier Lost					Rectifier Lost			Perte Redresseur		Perte Red
83	32			15			Rectifier Lost					Rectifier Lost			Perte Redresseur		Perte Red
84	32			15			Clear Rectifier Lost Alarm		ClrRectLost				Reset Perte Redresseur		Reset Perte Red
85	32			15			Clear					Yes				Reset				Reset
86	32			15			Confirm Rectifier ID/Phase		Confirm ID/PH		Confirm Position Phase/Red	Confirm Posi Ph
87	32			15			Confirm						Confirm				Confirmation			Confirmation
88	32			15			Best Operating Point			Best Oper Point			Meilleur Point de fonctionnement	Meil PointFonct
89	32			15			Rectifier Redundancy				Rect Redundancy			Redondance redresseur		Redondance red
90	32			15			Load Fluctuation Range				Fluct Range			Pourcentage Equilibrage		% Equilibrage
91	32			15			System Energy Saving Point		Energy Save Pt			Meilleur Point de fonctionnement	Meil PointFonct
92	32			15			E-Stop Function					E-Stop Function			Fonction E-Stop			Fonction E-Stop
93	32			15			AC Phases					AC Phases			Nb Phases			Nb Phases
94	32			15			Single Phase					Single Phase			Entrée Monophasé		Entrée 1 Phase
95	32			15			Three Phases					Three Phases			Entrée Tri-Phasee		Entrée 3 Phases
96	32			15			Input Current Limit			Input Curr Lmt			Limit Courant AC		Limit I AC
97	32			15			Double Supply					Double Supply			Double Entrée AC		2 Entrées AC
98	32			15			Single Supply					Single Supply			Simple Entrée AC		1 Entrée AC
99	32			15			Small Supply					Small Supply			Petite Puissance		Petite Puissanc
100	32			15			Restart on HVSD				Restart on HVSD			Redémarrage Red Surtention DC	Dem Red Surt DC
101	32			15			Sequence Start Interval				Start Interval			Démarrage Séquentiel		Dem. Séquentiel
102	32			15			Rated Voltage					Rated Voltage			Tension de repli		Tension repli
103	32			15			Rated Current					Rated Current			Courant max de repli		Imax repli
104	32			15			All Rectifiers Comm Fail			AllRectCommFail			Pas de Réponse Redresseurs	Pas de Rép.Red.
105	32			15			Inactive					Inactive			Désactive			Désactive
106	32			15			Active						Active				Active				Active
107	32			15			ECO Active		ECO Active			Activation ECO Mode		Activ ECO Mode
108	32			15			ECO Cycle Alarm				ECO Cycle Alarm			Oscill ECO Mode		Oscill ECO Mode
109	32			15			Reset Cycle Alarm			ClrCycleAlm			Reset Oscill ECO Mode	Reset Oscill
110	32			15			HVSD Limit (24V)			HVSD Limit			Tension Max(24V)		V Max(24V)
111	32			15			Rectifier Trim (24V)			Rectifier Trim				Tension Regul(24V)		V Regul(24V)
112	32			15			Rated Voltage (Internal Use)		Rated Voltage			Rated Voltage(Internal Use)	Rated Voltage
113	32			15			Rect Info Change (Internal)	RectInfo Change				Information Redresseur Change	Info Red Change
114	32			15			MixHE Power					MixHE Power			Mix Type Redresseur		Mix Type Red
115	32			15			Derated						Derated				Réduction			Réduction
116	32			15			Non-Derated				Non-Derated			Pas de Réduction		Pas Réduction
117	32			15			All Rects ON Time			Rects ON Time		Durée de déshumidification	Durée déshumidi
118	32			15			All Rectifiers are On			All Rects On		Red en déshumidification	Red déshumidifi
119	32			15			Clear Rect Comm Fail Alarm		ClrRectCommFail		Reset Alarme Perte Redresseur	Reset Perte Red
120	32			15			HVSD					HVSD			Surtension DC Active		Sur U DC Active
121	32			15			HVSD Voltage Difference				HVSD Volt Diff			Dif U Surtension		Dif U SurU
122	32			15			Total Rated Current		Total Rated Cur			Courant Total			Courant Total
123	32			15			Diesel Generator Power Limit		DG Pwr Lmt			Limitation Puissance sur  GE	Limit P sur GE
124	32			15			Diesel Generator Digital Input		Diesel DI Input			Entrée Signal GE		Entrée SignalGE
125	32			15			Diesel Gen Power Limit Point Set	DG Pwr Lmt Pt			Limite Puissance sur GE		Lim P sur GE
126	32			15			None						None				Aucune				Aucune
127	32			15			Digital Input 1				Digital Input 1					DI 1				DI 1
128	32			15			Digital Input 2				Digital Input 2					DI 2				DI 2
129	32			15			Digital Input 3				Digital Input 3					DI 3				DI 3
130	32			15			Digital Input 4				Digital Input 4					DI 4				DI 4
131	32			15			Digital Input 5				Digital Input 5					DI 5				DI 5
132	32			15			Digital Input 6				Digital Input 6					DI 6				DI 6
133	32			15			Digital Input 7				Digital Input 7					DI 7				DI 7
134	32			15			Digital Input 8				Digital Input 8					DI 8				DI 8
135	32			15			Current Limit Point				Curr Limit Pt			Limite Courant			Limite I
136	32			15			Current Limit				Current Limit		Limite Courant Active		Limit I Act
137	32			15			Maximum Current Limit Value			Max Curr Limit			Courant Max Limit		Courant Max Lmt 
138	32			15			Default Current Limit Point			Def Curr Lmt Pt			Défaut Limite Courant		Def Limite I
139	32			15			Minimize Current Limit Value		Min Curr Limit			Limitation Courant Min		Limit.Cour.Min

140	32			15			AC Power Limit Mode			AC Power Lmt			Mode Limitation Puissance AC	Mode Lim.P.AC
141	32			15			A						A				A				A
142	32			15			B						B				B				B
143	32			15			Existence State				Existence State		Etat existant		Etat existant
144	32			15			Existent				Existent		Existant			Existant
145	32			15			Not Existent				Not Existent		Inexistant			Inexistant
146	32			15			Total Output Power			Output Power		Puissance de sortie totale	W Sortie totale	
147	32			15			Total Slave Rated Current		Total RatedCurr		Courant total de l'esclave	I total esclave
148	32			15			Reset Rectifier IDs		Reset Rect IDs		Reset Redresseur IDs	Reset Redr IDs
149	32			15			HVSD Voltage Difference (24V)			HVSD Volt Diff		HVSD ecart tension (24V)	HVSD Volt Diff
#changed by Frank Wu,28/30,20140217, for upgrading software of rectifier---start---
150	32			15			Normal Update				Normal Update		Mise a jour standard	M a J standard
151	32			15			Force Update				Force Update		Mise a jour forcee	M à J forcee
152	32			15			Update OK Number			Update OK Num		Nombre de M a J OK	Nb Mise a J OK
153	32			15			Update State				Update State		Status de version	Status version
154	32			15			Updating					Updating			Mise a jour	Mise a jour
155	32			15			Normal Successful			Normal Success		Normale reussie	Normale reussie
156	32			15			Normal Failed				Normal Fail			Normale en echec	Normale echec
157	32			15			Force Successful			Force Success		Forcee reussie	Forcee reussie
158	32			15			Force Failed				Force Fail			Forcee en echec	Forcee en echec
159	32			15			Communication Time-Out		Comm Time-Out		Communication Time-Out	Comm Time-Out
160	32			15			Open File Failed			Open File Fail		Echec ouverture fichier	Echec ouv fich
#changed by Frank Wu,28/30,20140217, for upgrading software of rectifier---end---
#161	32			15			Total current Level1			Current Level1		Niveau 1 Courant Total		Niveau 1 Courant
#162	32			15			Total current Level2			Current Level2		Niveau 2 Courant Total			Niveau 2 Courant

163	32			15			Old Firmware Rec On Can1		Old FW On Can1		Vieux Firmware redresseur Can1			Vieux FM Can1
164	32			15			Old Firmware Rec On Can2		Old FW On Can2		Vieux Firmware redresseur Can2			Vieux FM Can2
165	32			15			Comm Rectifier On CAN1			Com Rec On CAN1		Red communication sur CAN1		RedComSurCAN1
166	32			15			Comm Rectifier On CAN2			Com Rec On CAN2		Red communication sur CAN2		RedComSurCAN2
167	32			15			Rectifiers Limited(CAN1)		Rect Lmt(CAN1)			Redresseurs Limités(CAN1)	Red Lmt(CAN1)
168	32			15			Rectifiers Limited(CAN2)		Rect Lmt(CAN2)			Redresseurs Limités(CAN2)	Red Lmt(CAN2)
169	32			15			Load-share Problem(CAN1)		Load-share Prob			Problème Partage Charge(CAN1)				ProbPartagCharg
170	32			15			Load-share Problem(CAN2)		Load-share Prob			Problème Partage Charge(CAN2)				ProbPartagCharg
171	64			64		The limited number of Rectifier in the system is 60 on CAN1 Bus.	The limited number of Rectifier in the system is 60 on CAN1 Bus.	Le nombre limité redresseur dans système est 60 sur CAN1 Bus.		Le nombre limité redresseur dans système est 60 sur CAN1 Bus.
172	64			64		The limited number of Rectifier in the system is 60 on CAN2 Bus.	The limited number of Rectifier in the system is 60 on CAN2 Bus.	Le nombre limité redresseur dans système est 60 sur CAN2 Bus.		Le nombre limité redresseur dans système est 60 sur CAN2 Bus.
173	64			64		Load-share problem due to rectifiers with old firmware on CAN1.		Load-share problem due to rectifiers with old firmware on CAN1.		Problème partage charge redresseurs avec ancien FM sur CAN1.		Problème partage charge redresseurs avec ancien FM sur CAN1.
174	64			64		Load-share problem due to rectifiers with old firmware on CAN2.		Load-share problem due to rectifiers with old firmware on CAN2.		Problème partage charge redresseurs avec ancien FM sur CAN2.		Problème partage charge redresseurs avec ancien FM sur CAN2.	

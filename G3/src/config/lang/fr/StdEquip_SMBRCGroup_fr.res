﻿#
#  Locale language support: french
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			SMBRC Group				SMBRC Group		Grupo SMBRC			Grupo SMBRC
2		32			15			Standby					Standby		Alarme				Alarme
3		32			15			Refresh				Refresh			Rafraichir			Rafraichir
4		32			15			Setting Refresh			Setting Refresh		Rafraichir Configuration	Rafraich.Config
5		32			15			E-Stop					E-Stop		Fonction Arrêt D'Urgence	Fonct AU
6		32			15			Yes				Yes			Oui				Oui
7		32			15			Existence State			Existence State		Détection			Détection
8		32			15			Existent			Existent		Présent				Présent
9		32			15			Not Existent				Not Existent		Absent				Absent
10		32			15			Num of SMBRCs				Num of SMBRCs	Numéro de SMBRCs		Numéro SMBRCs
11		32			15			SMBRC Config Changed			Cfg Changed		Configuration SMBRC		Config SMBRC
12		32			15			Not Changed			Not Changed		Aucun changement		Aucun changem.
13		32			15			Changed				Changed			Changement			Changement
14		32			15			SMBRC Configuration			SMBRC Cfg	Configuration SMBRC		Config SMBRC
15		32			15			SMBRC Configuration Number		SMBRC Cfg Num		Numéro Config SMBRC		Num Cfg SMBRC
16		32			15			SMBRC Interval				SMBRC Interval		Intervale de Test SMBRC		Intervale Test
17		32			15			SMBRC Reset Test			SMBRC Rst Test		Résistance de Test SMBRC	Résistance Test
18		32			15			Start				Start			Start				Start
19		32			15			Stop				Stop			Stop				Stop
20		32			15			Cell Voltage High			Cell Volt High		Tension Haute Cellule		V Haute Cellule
21		32			15			Cell Voltage Low			Cell Volt Low		Tension Basse Cellule		V Basse Cellule
22		32			15			Cell Temperature High			Cell Temp High		Température Haute Cellule	Temp Haute Cell
23		32			15			Cell Temperature Low			Cell Temp Low		Température Basse Cellule	Temp Basse Cell
24		32			15			String Current High			String Curr Hi		Haut Courant Branche		Haut I Branche
25		32			15			Ripple Current High			Ripple Curr Hi		Haut Courant Branche		Haut I Branche
26		32			15			High Cell Resistance			Hi Cell Resist		Résistance Haute Cellule	Rés Haute Cel
27		32			15			Low Cell Resistance			Low Cell Resist		Résistance Basse Cellule	Rés Basse Cel
28		32			15			High Intercell Resistance		Hi InterResist		Résistance Haute Inter Cellule	RésHautInterCel
29		32			15			Low Intercell Resistance		Low InterResist		Résistance Basse Inter Cellule	RésBasInterCel
30		32			15			String Current Low			String Curr Low				I Branche Batt faible	I Br_Bat faible
31		32			15			Ripple Current Low			Ripple Curr Low				Bruit residuel I faible	Bruit I faible


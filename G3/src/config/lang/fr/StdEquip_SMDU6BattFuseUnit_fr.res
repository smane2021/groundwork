﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE			
1		32			15			Fuse 1 Voltage		Fuse 1 Voltage		Tension Détection Protec 1	V Protec 1
2		32			15			Fuse 2 Voltage		Fuse 2 Voltage		Tension Détection Protec 2	V Protec 2
3		32			15			Fuse 3 Voltage		Fuse 3 Voltage		Tension Détection Protec 3	V Protec 3
4		32			15			Fuse 4 Voltage		Fuse 4 Voltage		Tension Détection Protec 4	V Protec 4
5		32			15			Batt Fuse 1 Alarm	Batt Fuse 1 Alm		Alarme Protec Bat 1		Al Protec Bat1
6		32			15			Batt Fuse 2 Alarm	Batt Fuse 2 Alm		Alarme Protec Bat 2		Al Protec Bat2
7		32			15			Batt Fuse 3 Alarm	Batt Fuse 3 Alm		Alarme Protec Bat 3		Al Protec Bat3
8		32			15			Batt Fuse 4 Alarm	Batt Fuse 4 Alm		Alarme Protec Bat 4		Al Protec Bat4
9		32			15			SMDU6 Battery Fuse Unit	SMDU6 Bat Fuse		Carte SMDU6 Bat Unit		CI SMDU6 Bat
10		32			15			On			On			Presente			Presente
11		32			15			Off			Off			Absente				Absente
12		32			15			Fuse 1 Status		Fuse 1 Status		Etat Protec Bat 1		Etat Protec 1
13		32			15			Fuse 2 Status		Fuse 2 Status		Etat Protec Bat 2		Etat Protec 2
14		32			15			Fuse 3 Status		Fuse 3 Status		Etat Protec Bat 3		Etat Protec 3
15		32			15			Fuse 4 Status		Fuse 4 Status		Etat Protec Bat 4		Etat Protec 4
16		32			15			State			State			Etat				Etat
17		32			15			Normal			Normal			Normal				Normal
18		32			15			Low			Low			Bas				Bas
19		32			15			High			High			Haut				Haut
20		32			15			Very Low		Very Low		Tres Bas			Tres Bas
21		32			15			Very High		Very High		Tres Haut			Tres Haut
22		32			15			On			On			On				On
23		32			15			Off			Off			Off				Off
24		32			15			Communication Fail	Comm Fail			Erreur Communication		Erreur Comm.
25		32			15			Times of Communication Fail	Times Comm Fail	Interrupt Times			Interrupt Times
26		32			15			Fuse 5 Status		Fuse 5 Status		Etat Protec Bat 5		Etat Protec 5	
27		32			15			Fuse 6 Status		Fuse 6 Status		Etat Protec Bat 6		Etat Protec 6	
28		32			15			Batt Fuse 5 Alarm	Batt Fuse 5 Alm		Alarme Protec Bat 5		Al Protec Bat5	
29		32			15			Batt Fuse 6 Alarm	Batt Fuse 6 Alm		Alarme Protec Bat 6		Al Protec Bat6

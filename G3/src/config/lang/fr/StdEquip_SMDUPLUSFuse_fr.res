﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr



[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15		Fuse 1			Fuse 1			Fusible 1			Fusible 1
2	32			15		Fuse 2			Fuse 2			Fusible 2			Fusible 2
3	32			15		Fuse 3			Fuse 3			Fusible 3			Fusible 3
4	32			15		Fuse 4			Fuse 4			Fusible 4			Fusible 4
5	32			15		Fuse 5			Fuse 5			Fusible 5			Fusible 5
6	32			15		Fuse 6			Fuse 6			Fusible 6			Fusible 6
7	32			15		Fuse 7			Fuse 7			Fusible 7			Fusible 7
8	32			15		Fuse 8			Fuse 8			Fusible 8			Fusible 8
9	32			15		Fuse 9			Fuse 9			Fusible 9			Fusible 9
10	32			15		Fuse 10			Fuse 10			Fusible 10			Fusible 10
11	32			15		Fuse 11			Fuse 11			Fusible 11			Fusible 11
12	32			15		Fuse 12			Fuse 12			Fusible 12			Fusible 12
13	32			15		Fuse 13			Fuse 13			Fusible 13			Fusible 13
14	32			15		Fuse 14			Fuse 14			Fusible 14			Fusible 14
15	32			15		Fuse 15			Fuse 15			Fusible 15			Fusible 15
16	32			15		Fuse 16			Fuse 16			Fusible 16			Fusible 16
17	32			15		SMDUP DC Fuse		SMDUP DC Fuse		Fusible DC SMDU+		Fus DC SMDU+
18	32			15			State			State			Etat				Etat
19	32			15			Off			Off			Off				Off
20	32			15			On			On			On				On
21	32			15		Fuse 1 Alarm		DC Fuse 1 Alm		Alarme Fusible 1		Alarme fus1
22	32			15		Fuse 2 Alarm		DC Fuse 2 Alm		Alarme Fusible 2		Alarme fus2
23	32			15		Fuse 3 Alarm		DC Fuse 3 Alm		Alarme Fusible 3		Alarme fus3
24	32			15		Fuse 4 Alarm		DC Fuse 4 Alm		Alarme Fusible 4		Alarme fus4
25	32			15		Fuse 5 Alarm		DC Fuse 5 Alm		Alarme Fusible 5		Alarme fus5
26	32			15		Fuse 6 Alarm		DC Fuse 6 Alm		Alarme Fusible 6		Alarme fus6
27	32			15		Fuse 7 Alarm		DC Fuse 7 Alm		Alarme Fusible 7		Alarme fus7
28	32			15		Fuse 8 Alarm		DC Fuse 8 Alm		Alarme Fusible 8		Alarme fus8
29	32			15		Fuse 9 Alarm		DC Fuse 9 Alm		Alarme Fusible 9		Alarme fus9
30	32			15		Fuse 10 Alarm		DC Fuse 10 Alm		Alarme Fusible 10		Alarme fus10
31	32			15		Fuse 11 Alarm		DC Fuse 11 Alm		Alarme Fusible 11		Alarme fus11
32	32			15		Fuse 12 Alarm		DC Fuse 12 Alm		Alarme Fusible 12		Alarme fus12
33	32			15		Fuse 13 Alarm		DC Fuse 13 Alm		Alarme Fusible 13		Alarme fus13
34	32			15		Fuse 14 Alarm		DC Fuse 14 Alm		Alarme Fusible 14		Alarme fus14
35	32			15		Fuse 15 Alarm		DC Fuse 15 Alm		Alarme Fusible 15		Alarme fus15
36	32			15		Fuse 16 Alarm		DC Fuse 16 Alm		Alarme Fusible 16		Alarme fus16
37	32			15			Times of Communication Fail		Times Comm Fail	Duree du defaut de communication			DureeDefCOM
38	32			15			Communication Fail			Comm Fail		Defaut de comunication	Defaut de COM
39	32			15			Fuse 17			Fuse 17			Fusible 17			Fusible 17
40	32			15			Fuse 18			Fuse 18			Fusible 18			Fusible 18
41	32			15			Fuse 19			Fuse 19			Fusible 19			Fusible 19
42	32			15			Fuse 20			Fuse 20			Fusible 20			Fusible 20
43	32			15			Fuse 21			Fuse 21			Fusible 21			Fusible 21
44	32			15			Fuse 22			Fuse 22			Fusible 22			Fusible 22
45	32			15			Fuse 23			Fuse 23			Fusible 23			Fusible 23
46	32			15			Fuse 24			Fuse 24			Fusible 24			Fusible 24
47	32			15			Fuse 25			Fuse 25			Fusible 25			Fusible 25
48	32			15		Fuse 17 Alarm		DC Fuse 17 Alm		Alarme Fusible 17		Alarme fus17
49	32			15		Fuse 18 Alarm		DC Fuse 18 Alm		Alarme Fusible 18		Alarme fus18
50	32			15		Fuse 19 Alarm		DC Fuse 19 Alm		Alarme Fusible 19		Alarme fus19
51	32			15		Fuse 20 Alarm		DC Fuse 20 Alm		Alarme Fusible 20		Alarme fus20
52	32			15		Fuse 21 Alarm		DC Fuse 21 Alm		Alarme Fusible 21		Alarme fus21
53	32			15		Fuse 22 Alarm		DC Fuse 22 Alm		Alarme Fusible 22		Alarme fus22
54	32			15		Fuse 23 Alarm		DC Fuse 23 Alm		Alarme Fusible 23		Alarme fus23
55	32			15		Fuse 24 Alarm		DC Fuse 24 Alm		Alarme Fusible 24		Alarme fus24
56	32			15		Fuse 25 Alarm		DC Fuse 25 Alm		Alarme Fusible 25		Alarme fus25

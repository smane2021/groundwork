﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
fr



[RES_INFO]
#R_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32		15			GroupII Rectifier		GroupII Rect		Groupe II Redresseur		Groupe II red
2		32		15			DC Status			DC Status		Etat DC				Etat DC
3		32		15			DC Output Voltage		DC Voltage			Tension DC			Tension DC
4		32		15			Origin Current			Origin Current		Origine Courant			Origine Courant
5		32		15			Temperature			Temperature		Température			Température
6		32		15			Used Capacity			Used Capacity		Taux de charge Red		Taux charge Red
7		32		15			AC Input Voltage		AC Voltage		Tension secteur			Tension secteur
8		32		15			DC Output Current		DC Current			Courant				Courant
9		32		15			SN				SN		SN				SN
10		32		15			Total Running Time		Running Time		Temps de fonct.			Temps de fonct.
11		32		15			Communication Fail Count		Comm Fail Count	Defaut de communication		Defaut COM
12		32		15			Derated by AC			Derated by AC		Derating AC			Derating AC
13		32		15			Derated by Temp			Derated by Temp			Derating oC			Derating oC
14		32		15			Derated				Derated			Derating par red		Derating/Red
15		32		15			Full Fan Speed			Full Fan Speed		Vitesse max ventil		Vit. max ventil
16		32		15			Walk-In		Walk-In		Fonction. active		Fonc.active
17		32		15			AC On/Off			AC On/Off		Etat primaire			Etat primaire
18		32		15			Current Limit			Curr Limit		Limitation courant		Lim Courant
19		32		15			Voltage High Limit		Volt Hi-limit		limit tension haute		Lim Tens. Haute
20		32		15			AC Input Status			AC Status		Defaut redresseur secteur	Def Red secteur
21		32		15			Rect Temperature High		Rect Temp High		Defaut red sur temperature	Def Red Sur T°
22		32		15			Rectifier Fail			Rect Fail		Defaut redresseur		Def redresseur
23		32		15			Rectifier Protected		Rect Protected		Protection redresseur		Protection red
24		32		15			Fan Fail		Fan Fail		Defaut redresseur ventilateur	Def Red ventil
25		32		15			Current Limit State		CurrLimit State			Limit.courant redresseur	Limit.I Red
26		32		15			EEPROM Fail		EEPROM Fail	Defaut EEPROM			Defaut EEPROM
27		32		15			DC On/Off Control		DC On/Off Ctrl		DC On/Off			DC On/Off
28		32		15			AC On/Off Control		AC On/Off Ctrl		AC On/Off			AC On/Off
29		32		15			LED Control			LED Control		Etat led			Etat led
30		32		15			Rectifier Reset			Rectifier Reset			RAZ redresseur			RAZ red
31		32		15			AC Input Fail	AC Fail		Defaut redresseur secteur	Def secteur Red
34		32		15			Over Voltage			Over Voltage	Surtension			Surtension 
37		32		15			Current Limit			Current Limit		Limitation Courant		Limit courant
39		32		15			Normal				Normal			Normal				Normal
40		32		15			Limited				Limited			Limitation			Limitation
45		32		15			Normal				Normal			Normal				Normal
46		32		15			Full				Full			Maximum				Max
47		32		15			Disabled			Disabled		Desactive			Desactive
48		32		15			Enabled				Enabled			Active				Active
49		32		15			On				On			Marche				Marche
50		32		15			Off				Off			Arrêt				Arrêt
51		32		15			Normal				Normal			Normal				Normal
52		32		15			Fail			Fail		Defaut				Defaut
53		32		15			Normal				Normal			Normal				Normal
54		32		15			Over Temperature		Over Temp		Sur temperature			Sur temp.
55		32		15			Normal				Normal			Normal				Normal
56		32		15			Fail				Fail			Defaut				Defaut
57		32		15			Normal				Normal			Normal				Normal
58		32		15			Protected			Protected		Protection			Protection
59		32		15			Normal				Normal			Normal				Normal
60		32		15			Fail				Fail			Defaut				Defaut
61		32		15			Normal				Normal			Normal				Normal
62		32		15			Alarm				Alarm			Alarme				Alarme
63		32		15			Normal				Normal			Normal				Normal
64		32		15			Fail				Fail			Defaut				Defaut
65		32		15			Off				Off			Arrêt				Arrêt
66		32		15			On				On			Marche				Marche
67		32		15			Off				Off			Arrêt				Arrêt
68		32		15			On				On			Marche				Marche
69		32		15			LED Control			LED Control		Flash				Flash
70		32		15			Cancel				Cancel			Annule				Annule
71		32		15			Off				Off			Arrêt				Arrêt
72		32		15			Reset				Reset			RAZ				RAZ
73		32		15			Open Rectifier			Rectifier On		Marche				Marche
74		32		15			Close Rectifier			Rectifier Off		Arrêt				Arrêt
75		32		15			Off				Off			Arrêt				Arrêt
76		32		15			LED Control			LED Control			Test Led			Test Led
77		32		15			Rectifier Reset			Rectifier Reset			Reset Redresseur		Reset Red.
80		32		15			Rectifier Communication Fail	Rect Comm Fail		Defaut Communication		Defaut Comm.
84		32		15			Rectifier High SN		Rect High SN		Redresseur No Serie		Red No Serie
85		32		15			Rectifier Version		Rect Version		Version Redresseur		Vers Red
86		32		15			Rectifier Part Number		Rect Part Num		Code Redresseur			Code Red
87		32		15			Current Share State		CurrShare State	Courant Equilibrage Red		I Equil Red
88		32		15			Current Share Alarm		CurrShare Alarm		Alarme Courant Equilibrage Red	AL I Equil Red
89		32		15			Over Voltage			Over Voltage	Tension Surtension Red		V Surt Red
90		32		15			Normal				Normal			Normal				Normal
91		32		15			Over Voltage			Over Voltage	Surtension			Surtension
92		32		15			Line AB Voltage			Line AB Volt		Tension Phase AB		V Phase AB
93		32		15			Line BC Voltage			Line BC Volt		Tension Phase BC		V Phase BC
94		32		15			Line CA Voltage			Line CA Volt		Tension Phase CA		V Phase CA
95		32		15			Low Voltage			Low Voltage		Tension AC Basse		V AC Basse
96		32		15			Low AC Voltage Protection	Low AC Protect		Tension AC Tres Basse		V AC Tres Basse
97		32		15			Rectifier ID		Rectifier ID		ID Redreseur		ID Red.
98		32		15			DC Output Turned Off		DC Output Off		Salida CC apagada		Salida CC Desc
99		32		15			Rectifier Phase			Rect Phase		Phase Redresseur		Phase Red
100		32		15			A				A			A				A
101		32		15			B				B			B				B
102		32		15			C				C			C				C
103		32		15			Severe Sharing CurrAlarm	SevereCurrShare			Defaut Equilibrage Courant	Def Equil I
104		32		15			Barcode 1			Barcode 1		Code Bar 1			Code Bar 1
105		32		15			Barcode 2			Barcode 2		Code Bar 2			Code Bar 2
106		32		15			Barcode 3			Barcode 3		Code Bar 3			Code Bar 3
107		32		15			Barcode 4			Barcode 4		Code Bar 4			Code Bar 4
108		32		15			Rectifier Communication Fail		Rect Comm Fail	Defaut communication redresseurs		DefComRedress
109		32		15			No				No			Non				Non
110		32		15			Yes				Yes			Oui				Oui
111		32		15			Existence State			Existence State		Module Présent			Module Présent
113		32		15			Comm OK				Comm OK				Communication correcte		Comm OK
114		32		15			All Rectifiers Comm Fail			AllRectCommFail		Aucune communication		Aucune Comm.
115		32		15			Communication Fail			Comm Fail		Defaut de communication			Defaut de COM
116		32		15			Rated Current			Rated Current		Courant moyen			Courant moyen
117		32		15			Efficiency			Efficiency		Rendement			Rendement
118		32		15			LT 93				LT 93			Inférieur à 93			Inférieur à 93
119		32		15			GT 93			GT 93			Suppérieur à 93			Suppérieur à 93
120		32		15			GT 95			GT 95			Suppérieur à 95			Suppérieur à 95
121		32		15			GT 96			GT 96			Suppérieur à 96			Suppérieur à 96
122		32		15			GT 97			GT 97			Suppérieur à 97			Suppérieur à 97
123		32		15			GT 98			GT 98			Suppérieur à 98			Suppérieur à 98
124		32		15			GT 99			GT 99			Suppérieur à 99			Suppérieur à 99
125		32		15			Redundancy Related Alarm	Redundancy Alm	Alarme Redondance		Al Redondance
126		32		15		Rect HVSD Status		HVSD Status	Etat Sur Tension		Etat Sur U
276		32		15			EStop/EShutdown			EStop/EShutdown		EStop/EShutdown			EStop/EShutdown

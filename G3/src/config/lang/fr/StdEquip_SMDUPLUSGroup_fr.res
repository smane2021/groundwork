﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			SMDUP Group			SMDUP Group		Groupe SMDUP			Groupe SMDUP
2		32			15			Standby				Standby			Arrêt				Arrêt
3		32			15			Refresh				Refresh			Rafraichir			Rafraichir
4		32			15			Setting Refresh			Setting Refresh		Rafraichir Configuration	Rafraich.Config
5		32			15			E-Stop			E-Stop				E-Stop			E-Stop	
6		32			15			Yes				Yes			Oui				Oui
7		32			15			Existence State			Existence State		Détection			Détection
8		32			15			Existent			Existent		Présent				Présent
9		32			15			Not Existent		Not Existent		Absent				Absent
10		32			15			Number of SMDUPs	Num of SMDUPs		Numéro de SMDUP			Numéro SMDUP
11		32			15			SMDU Config Changed	Config Changed	Configuration SMDUP		Config SMDUP
12		32			15			Not Changed			Not Changed		Aucun changement		Aucun changem.
13		32			15			Changed				Changed			Changement			Changement
14		32			15			SMDUP Total Curr			SMDUP Total Curr			Courant total		Courant total


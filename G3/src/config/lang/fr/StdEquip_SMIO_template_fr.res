﻿#
#  Locale language support:french
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE			
1		32			15			Analog Input 1				Analog Input 1			Entrée analogique 1			E Ana 1				
2		32			15			Analog Input 2				Analog Input 2			Entrée analogique 2			E Ana 2				
3		32			15			Analog Input 3				Analog Input 3			Entrée analogique 3			E Ana 3				
4		32			15			Analog Input 4				Analog Input 4			Entrée analogique 4			E Ana 4				
5		32			15			Analog Input 5				Analog Input 5			Entrée analogique 5			E Ana 5				
6		32			15			Frequency Input			Frequency Input		Entrée à impulsion			E.impulsion			
7		32			15			Digital Input 1			Digital Input 1		Entrée Tout Ou Rien 1			E Tor 1				
8		32			15			Digital Input 2			Digital Input 2		Entrée Tout Ou Rien 2			E Tor 2				
9		32			15			Digital Input 3			Digital Input 3		Entrée Tout Ou Rien 3			E Tor 3				
10		32			15			Digital Input 4			Digital Input 4		Entrée Tout Ou Rien 4			E Tor 4				
11		32			15			Digital Input 5			Digital Input 5		Entrée Tout Ou Rien 5			E Tor 5				
12		32			15			Digital Input 6			Digital Input 6		Entrée Tout Ou Rien 6			E Tor 6				
13		32			15			Digital Input 7			Digital Input 7		Entrée Tout Ou Rien 7			E Tor 7				
14		32			15			Relay 1 Status			Relay 1 Status		Etat relais 1				Etat K 1			
15		32			15			Relay 2 Status			Relay 2 Status		Etat relais 2				Etat K 2			
16		32			15			Relay 3 Status			Relay 3 Status		Etat relais 3				Etat K 3			
17		32			15			Relay 1 On/Off			Relay 1 On/Off		Relais 1 ouvert/fermé			K 1 O/F				
18		32			15			Relay 2 On/Off			Relay 2 On/Off		Relais 2 ouvert/fermé			K 2 O/F				
19		32			15			Relay 3 On/Off			Relay 3 On/Off		Relais 3 ouvert/fermé			K 3 O/F				
23		32			15			High Analog Input 1 Limit		Hi AI 1 Limit		Limite haute E Ana 1			Lim H E Ana 1			
24		32			15			Low Analog Input 1 Limit		Low AI 1 Limit		Limite basse E Ana 1			Lim B E Ana 1			
25		32			15			High Analog Input 2 Limit		Hi AI 2 Limit		Limite haute E Ana 2			Lim H E Ana 2			
26		32			15			Low Analog Input 2 Limit		Low AI 2 Limit		Limite basse E Ana 2			Lim B E Ana 2			
27		32			15			High Analog Input 3 Limit		Hi AI 3 Limit		Limite haute E Ana 3			Lim H E Ana 3			
28		32			15			Low Analog Input 3 Limit		Low AI 3 Limit		Limite basse E Ana 3			Lim B E Ana 3			
29		32			15			High Analog Input 4 Limit		Hi AI 4 Limit		Limite haute E Ana 4			Lim H E Ana 4			
30		32			15			Low Analog Input 4 Limit		Low AI 4 Limit		Limite basse E Ana 4			Lim B E Ana 4			
31		32			15			High Analog Input 5 Limit		Hi AI 5 Limit		Limite haute E Ana 5			Lim H E Ana 5			
32		32			15			Low Analog Input 5 Limit		Low AI 5 Limit		Limite basse E Ana 5			Lim B E Ana 5			
33		32			15			High Frequency Limit	High Freq Limit		Limite haute E.Fréquence		Lim H E Fréq.			
34		32			15			Low Frequency Limit	Low Freq Limit		Limite basse E.Fréquence		Lim B E Fréq.			
35		32			15			High Analog Input 1 Alarm		Hi AI 1 Alarm		Alarme haute E Ana 1			Alr H E Ana 1			
36		32			15			Low Analog Input 1 Alarm		Low AI 1 Alarm		Alarme basse E Ana 1			Alr B E Ana 1			
37		32			15			High Analog Input 2 Alarm		Hi AI 2 Alarm		Alarme haute E Ana 2			Alr H E Ana 2			
38		32			15			Low Analog Input 2 Alarm		Low AI 2 Alarm		Alarme basse E Ana 2			Alr B E Ana 2			
39		32			15			High Analog Input 3 Alarm		Hi AI 3 Alarm		Alarme haute E Ana 3			Alr H E Ana 3			
40		32			15			Low Analog Input 3 Alarm		Low AI 3 Alarm		Alarme basse E Ana 3			Alr B E Ana 3			
41		32			15			High Analog Input 4 Alarm		Hi AI 4 Alarm		Alarme haute E Ana 4			Alr H E Ana 4			
42		32			15			Low Analog Input 4 Alarm		Low AI 4 Alarm		Alarme basse E Ana 4			Alr B E Ana 4			
43		32			15			High Analog Input 5 Alarm		Hi AI 5 Alarm		Alarme haute E Ana 5			Alr H E Ana 5			
44		32			15			Low Analog Input 5 Alarm		Low AI 5 Alarm		Alarme basse E Ana 5			Alr B E Ana 5			
45		32			15			High Frequency Input Alarm		Hi Freq In Alm		Alarme haute E Fréquence		Alr H E Freq.			
46		32			15			Low Frequency Input Alarm		Low Freq In Alm		Alarme basse E Fréquence		Alr B E Fréq.			
47		32			15			Off				Off		Fermé					Fermé				
48		32			15			On					On			Ouvert					Ouvert				
49		32			15			Off				Off		Fermé					Fermé				
50		32			15			On					On			Ouvert					Ouvert				
51		32			15			Off				Off		Fermé					Fermé				
52		32			15			On					On			Ouvert					Ouvert				
53		32			15			Off				Off		Fermé					Fermé				
54		32			15			On					On			Ouvert					Ouvert				
55		32			15			Off				Off		Fermé					Fermé				
56		32			15			On					On			Ouvert					Ouvert				
57		32			15			Off				Off		Fermé					Fermé				
58		32			15			On					On			Ouvert					Ouvert				
59		32			15			Off				Off		Fermé					Fermé				
60		32			15			On					On			Ouvert					Ouvert				
61		32			15			Off				Off		Fermé					Fermé				
62		32			15			On					On			Ouvert					Ouvert				
63		32			15			Off				Off		Fermé					Fermé				
64		32			15			On					On			Ouvert					Ouvert				
65		32			15			Off				Off		Fermé					Fermé				
66		32			15			On					On			Ouvert					Ouvert				
67		32			15			Off				Off		Fermé					Fermé				
68		32			15			On					On			Ouvert					Ouvert				
69		32			15			Off				Off		Fermé					Fermé				
70		32			15			On					On			Ouvert					Ouvert				
71		32			15			Off				Off			Fermé					Fermé				
72		32			15			On					On			Ouvert					Ouvert				
73		32			15			SMIO Generic Unit			SMIO Unit		SMIO					SMIO				
74		32			15			SMIO Failure				SMIO Fail		Erreur SMIO				Erreur SMIO			
75		32			15			SMIO Failure				SMIO Fail		Erreur SMIO				Erreur SMIO			
76		32			15			No					No			Non					Non				
77		32			15			Yes					Yes			Oui					Oui				
78		32			15			Control					Control			Controle				Controle

﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
fr



[RES_INFO]
#RES_ID		MAX_LEN_FULL	MAX_LEN_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32		15		Converter			Converter		Convertisseur			Convertisseur
2		32		15		Output Voltage			Output Voltage		Tension de sortie		V sortie
3		32		15		Output Current			Output Current		Courant Utilisation		Courant
4		32		15		Temperature			Temperature		Temperature			Temperature
5		32		15		Converter High Serial Number	Conv High SN	N° de serie			N° de serie
6		32		15		Converter SN			Converter SN		N° de serie			N° de serie
7		32		15		Total Running Time		Running Time		Temps de fonctionnement		Temps fonction.
8		32		15		Converter ID Overlap		Conv ID Overlap				Erreur Adressage Convert.	Err.Adr.Conver.
9		32		15		Converter Identification Status	Conv ID Status			Identification des conv.	Ident.DesConv.
10		32		15		Fan Full Speed Status		Fan Full Speed				Vitesse Max des ventilateurs		Vit.Max.Ventil
11		32		15			EEPROM Fail Status		EEPROM Fail		Défaut EEPROM			Défaut EEPROM
12		32		15			Thermal Shutdown Status		Thermal SD		Défaut Température		Défaut Temp.
13		32		15		Input Low Voltage Status	Input Low Volt				Tension d'entrée Basse		V entrée Basse
14		32		15		High Ambient Temperature Status	High Amb Temp			Température Ambiante Haute	Temp.Amb.Haute
15		32		15		Walk-In				Walk-In			Etat fonctionnement		Etat fonct.
16		32		15		On/Off Status			On/Off Status		Etat Marche/Arrêt		Marche/Arrêt 
17		32		15		Stopped Status			Stopped Status		Arrêt				Arrêt
18		32		15			Power Limit			Power Limit		Dérating en température		Dérating temp.	
19		32		15		Over Voltage Status (DC)	Over Volt (DC)		Surtension DC			Surtension DC	
20		32		15		Fan Fail Status			Fan Fail		Défaut Ventilateur		Défaut Venti.
21		32		15		Converter Fail Status		Converter Fail		Défaut convertiseur		Défaut conv.
22		32		15		Barcode 1			Barcode 1		Barre code 1			Barre code 1
23		32		15		Barcode 2			Barcode 2		Barre code 2			Barre code 2
24		32		15		Barcode 3			Barcode 3		Barre code 3			Barre code 3
25		32		15		Barcode 4			Barcode 4		Barre code 4			Barre code 4
26		32		15		EStop/EShutdown Status		EStop/EShutdown			Arrêt d'urgence			Arrêt d'urgence	
27		32		15		Communication Status		Comm Status		Etat Communication		Etat Comm.
28		32		15		Existence State			Existence State		Detection			Detection
29		32		15		DC On/Off Control		DC On/Off Ctrl		Control CC			Control CC
30		32		15		Converter Reset			Converter Reset		Reset Surtension		Reset SurV
31		32		15		LED Control			LED Control		Controle LED			Controle LED
32		32		15		Converter Reset			Converter Reset		Reset Convertisseur		Reset Convert.
33		32		15		AC Input Fail			AC Input Fail		Défaut AC			Défaut AC
34		32		15		Over Voltage			Over Voltage		Surtension			Surtension
37		32		15		Current Limit			Current Limit		Limitation de courant		Limit courant	
39		32		15		Normal				Normal			Normal				Normal
40		32		15		Limited				Limited			Limitation			Limitation
45		32		15		Normal				Normal			Normal				Normal
46		32		15		Full				Full			Complet				Complet	
47		32		15		Disabled			Disabled		Inactif				Inactif	
48		32		15		Enabled				Enabled			Actif				Actif
49		32		15		On				On			Oui				Oui
50		32		15		Off				Off			Non				Non
51		32		15		Normal				Normal			Normal				Normal
52		32		15		Fail				Fail			Défaut				Défaut
53		32		15		Normal				Normal			Normal				Normal
54		32		15		Over Temperature		Over Temp		Température Haute		Temp. Haute
55		32		15		Normal				Normal			Normal				Normal
56		32		15		Fail				Fail			Défaut				Défaut
57		32		15		Normal				Normal			Normal				Normal
58		32		15		Protected			Protected		Protegido			Protegido
59		32		15		Normal				Normal			Normal				Normal
60		32		15		Fail				Fail			Défaut				Défaut
61		32		15			Normal				Normal			Normal				Normal
62		32		15		Alarm				Alarm			Défaut				Défaut
63		32		15		Normal				Normal			Normal				Normal
64		32		15		Fail				Fail		Défaut				Défaut
65		32		15		Off				Off			Arrêt				Off
66		32		15		On				On			Marche				On
67		32		15		Reset				Reset		Reset				Reset
68		32		15			Normal				Normal				Normal				Normal
69		32		15		Flash				Flash			Intermittent			Intermittent
70		32		15		Stop Flashing			Stop Flashing		Arrête de Clignoter			Arrête Clign.
71		32		15		Off				Off			Arrêt				Arrêt
72		32		15		Reset				Reset			Reset				Reset	
73		32		15		Converter On			Converter On			Marche Convertisseur		Marche Conv.
74		32		15		Converter Off			Converter Off		Arrêt Conv.		Arrêt Conv.
75		32		15		Off				Off			Non				Non
76		32		15		LED Control			LED Control			Intermittent			Intermittent
77		32		15		Converter Reset			Converter Reset			Réinitialisation Convertisseur	Réinit Convert	
80		32		15		Communication Fail		Comm Fail		Defaut de communication		Defaut de COM
84		32		15		Converter High Serial Number	Conv High SN		Dernier N° série		DernierN°série
85		32		15		Converter Version		Conv Version		Version Convertisseur		Version Conv.
86		32		15		Converter Part Number		Conv Part Num		Code Produit Convert.		Code Produit
87		32		15		Current Share State		Current Share		Etat Equilibrage Courant	Etat Equil. I
88		32		15		Current Share Alarm		Curr Share Alm			Alarme Equilibrage Courant		Al Equil. I
89		32		15		HVSD Alarm			HVSD Alarm		Surtension			Surtension
90		32		15		Normal				Normal			Normal				Normal
91		32		15		Over Voltage			Over Voltage		Soustension			Soustension
92		32		15		Line AB Voltage			Line AB Volt		Tension Phase 1			V Phase 1	
93		32		15		Line BC Voltage			Line BC Volt		Tension Phase 2			V Phase 2
94		32		15		Line CA Voltage			Line CA Volt		Tension Phase 3			V Phase 3
95		32		15		Low Voltage			Low Voltage		Sous tension			Sous tension
96		32		15			AC Under Voltage Protection	U-Volt Protect		Protection sous-tension CA	Prot sous-U CA
97		32		15		Converter ID			Converter ID		Position Convertisseur		Position Conv.
98		32		15		DC Output Shut Off		DC Output Off		Arrêt Sortie DC			Arrêt Sortie DC
99		32		15		Converter Phase			Converter Phase		Phase Convertisseur		Phase Convert.
100		32		15		A				A			A				A
101		32		15		B				B			B				B
102		32		15		C				C			C				C
103		32		15		Severe Sharing Current Alarm	SevereCurrShare		Defaut Equilibrage Courant	Défaut Equil. I
104		32		15		Barcode 1			Barcode 1		Barre code 1			Barre code 1
105		32		15		Barcode 2			Barcode 2		Barre code 2			Barre code 2
106		32		15		Barcode 3			Barcode 3		Barre code 3			Barre code 3
107		32		15		Barcode 4			Barcode 4		Barre code 4			Barre code 4
108		32		15		Converter Communication Fail	Conv Comm Fail		Défaut Convertisseur		Défaut Convert.
109		32		15		No				No			Non				Non
110		32		15		Yes				Yes			Oui				Oui
111		32		15		Existence State			Existence State		Etat existant			Etat existant
112		32		15		Converter Fail			Converter Fail		Défaut Convertisseur		Défaut Convert.
113		32		15		Communication OK		Comm OK			Communication correcte		Comm. Correcte
114		32		15		All Converters Comm Fail	AllConvCommFail		Aucune réponse			Aucune réponse
115		32		15		Communication Fail		Comm Fail		Defaut de communication		Defaut de COM
116		32		15		Valid Rated Current		Rated Current		Courant nominal valide			CourantNomValid
117		32		15		Efficiency			Efficiency		Rendement			Rendement
118		32		15			Input Rated Voltage		Input RatedVolt		Tension d'entrée nominale			TensioEntreeNom	
119		32		15			Output Rated Voltage		OutputRatedVolt		Tension de sortie nominale			TensioSortieNom
120		32		15			LT 93				LT 93			Inférieur à 93			Inférieur à 93
121		32		15			GT 93				GT 93			Suppérieur à 93			Suppérieur à 93
122		32		15			GT 95				GT 95			Suppérieur à 95			Suppérieur à 95
123		32		15			GT 96				GT 96			Suppérieur à 96			Suppérieur à 96
124		32		15			GT 97				GT 97			Suppérieur à 97			Suppérieur à 97
125		32		15			GT 98				GT 98			Suppérieur à 98			Suppérieur à 98
126		32		15			GT 99				GT 99			Suppérieur à 99			Suppérieur à 99

276		32		15		EStop/EShutdown			EStop/EShutdown		Arrêt d'Urgence			Arrêt d'Urgence	
277		32		15		Fan Fail			Fan Fail	Défaut Ventilateur		Défaut Ventil.
278		32		15		Input Low Voltage		Input Low Volt		Sous Tension d'Entrée		Sous U Entrée
279		32		15		Converter ID			Converter ID	N° convertisseur		N° conv.

280		32		15		EEPROM Fail			EEPROM Fail		Défaut EEPROM			Défaut EEPROM	
281		32		15		Thermal Shutdown		Thermal SD		Protection Température		Protect Temp.
282		32		15		High Temperature		High Temp		Température Haute		Temp. Haute
283		32		15		Thermal Power Limit		Therm Power Lmt		Limitation Puiss. Température	Limit P Temp
284		32		15		Fan Fail			Fan Fail		Défaut Ventilateur		Défaut Ventil.
285		32		15		Converter Fail			Converter Fail		Défaut Convertisseur		Défaut Convert.
286		32		15		Mod ID Overlap			Mod ID Overlap		Chevauchement N° ID		Chevauche N° ID
287		32		15		Low Input Volt			Low Input Volt		Sous tension d'entrée		SousV Entrée
288		32		15		Under Voltage			Under Voltage		Sous-tension			Sous-tension 
289		32		15		Over Voltage			Over Voltage		Sur-tension			Sur tension
290		32		15		Over Current			Over Current		Sur Courant			Sur Courant
291		32		15			GT 94				GT 94			Suppérieur à 94			Suppérieur à 94
292		32		15			Under Voltage(24V)		Under Volt(24V)		Sous-tension (24V)				Soustension
293		32		15			Over Voltage(24V)		Over Volt(24V)		Surtension (24V)				Surtension
294	32		15			Converter Summary Alarm		ConvSummaryAlm		Converter Sommaire Alarme				Conv.Somm.Alm

﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
fr



[RES_INFO]
#R_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32		15			Solar Converter				Solar Conv		Convertisseur Solaire			Convert.Sol.
2		32		15			DC Status				DC Status		Etat DC					Etat DC	
3		32		15			DC Output Voltage			DC Voltage		Tension DC				Tension DC
4		32		15			Origin Current				Origin Current		Origine du Courant			Origine I
5		32		15			Temperature				Temperature		Temperature				Temperature
6		32		15			Used Capacity				Used Capacity		Taux de Charge				Taux Charge
7		32		15			Input Voltage				Input Voltage		Tension d'Entrée			Tension Entrée
8		32		15			DC Output Current			DC Current		Courant de Sortie			Courant Sortie
9		32		15			SN					SN			Numéro de Série				Numéro Série
10		32		15			Total Running Time			Running Time		Temps de Fonctionnement			Temps Fonct.
11		32		15			Communication Fail Count		Comm Fail Count		Compteur de Défaut Communication	Compt.Déf.Com.
13		32		15			Derated by Temp				Derated by Temp		Derating Température			Derating oC
14		32		15			Derated					Derated			Puissance Reduite			Puis. Reduite
15		32		15			Full Fan Speed				Full Fan Speed		Vitesse Ventilateur Maximum		Vit.Vent.Max.
16		32		15			Walk-In				Walk-In		Démarrage				Démarrage
18		32		15			Current Limit				Current Limit		Limitation Courant			Limit.Courant
19		32		15			Voltage High Limit			Volt Hi-limit		Limite Tension Haute			Limite V Haute
20		32		15			Solar Status				Solar Status		Etat Entrée DC				V DC Entrée
21		32		15			Solar Converter Temperature High	SolarConvTempHi		Defaut Conv Temp Haute		Def Conv Sur T°
22		32		15			Solar Converter Fail			SolarConv Fail		Defaut Convertisseur			Def.Convert.
23		32		15			Solar Converter Protected		SolarConv Prot		Protection Convertisseur		Protect.Conv.
24		32		15			Fan Fail			Fan Fail	Defaut Ventilateur Convertisseur	Def.Ventil.
25		32		15			Current Limit State			Curr Lmt State		Limit.courant Convertisseur		Limit.I Conv.
26		32		15			EEPROM Fail			EEPROM Fail	Defaut EEPROM				Defaut EEPROM
27		32		15			DC On/Off Control			DC On/Off Ctrl		DC On/Off				DC On/Off
29		32		15			LED Control				LED Control		Etat led				Etat led
30		32		15			Solar Converter Reset			SolarConvReset		RAZ Convertisseur			RAZ Conv.
31		32		15			Input Fail			Input Fail		Defaut Converisseur			Def Conv.
34		32		15			Over Voltage				Over Voltage		Surtension				Surtension
37		32		15			Current Limit				Current Limit		Limitation Courant			Limit.Courant
39		32		15			Normal					Normal			Normal					Normal
40		32		15			Limited					Limited			Limitation				Limitation
45		32		15			Normal					Normal			Normal					Normal
46		32		15			Full					Full			Maximum					Max
47		32		15			Disabled				Disabled		Desactive				Desactive
48		32		15			Enabled					Enabled			Active					Active
49		32		15			On					On			Marche					On
50		32		15			Off					Off			Arrêt					Off
51		32		15			Day					Day			Normal					Normal
52		32		15			Fail				Fail			Defaut					Defaut
53		32		15			Normal					Normal			Normal					Normal
54		32		15			Over Temperature			Over Temp		Temperature Haute			Temp.Haute
55		32		15			Normal					Normal			Normal					Normal
56		32		15			Fail				Fail			Defaut					Defaut
57		32		15			Normal					Normal			Normal					Normal
58		32		15			Protected				Protected		Protection				Protection
59		32		15			Normal					Normal			Normal					Normal
60		32		15			Fail				Fail			Defaut					Defaut
61		32		15			Normal					Normal			Normal					Normal
62		32		15			Alarm					Alarm			Alarme					Alarme
63		32		15			Normal					Normal			Normal					Normal
64		32		15			Fail				Fail		Defaut					Defaut
65		32		15			Off					Off			Arrêt					Off
66		32		15			On					On			Marche					On
67		32		15			Off					Off			Arrêt					Off
68		32		15			On					On			Marche					On
69		32		15			LED Control				LED Control		Flash					Flash
70		32		15			Cancel					Cancel			Annule					Annule
71		32		15			Off					Off			Arrêt					Off
72		32		15			Reset					Reset			RAZ					RAZ
73		32		15			Solar Converter On			Solar Conv On		Marche					Marche
74		32		15			Solar Converter Off			Solar Conv Off		Arrêt					Arrêt
75		32		15			Off					Off			Arrêt					Arrêt
76		32		15			LED Control				LED Control		Test Led				Test Led
77		32		15			Solar Converter Reset			SolarConv Reset		Reset Convertisseur			Reset Convert.
80		34		15			Solar Converter Comm Fail	SolConvCommFail			Defaut Communication			Defaut Comm.
84		32		15			Solar Converter High SN			SolConvHighSN		Numéro Serie				No Serie
85		32		15			Solar Converter Version			SolarConv Ver		Version					Version
86		32		15			Solar Converter Part Number		SolConv PartNum		Code Produit				Code Produit
87		32		15			Current Share State			Current Share		Courant Equilibrage			I Equillibrage
88		32		15			Current Share Alarm			Curr Share Alm		Alarme Courant Equilibrage		AL I Equil
89		32		15			Over Voltage				Over Voltage		Tension Surtension			V Surt
90		32		15			Normal					Normal			Normal					Normal
91		32		15			Over Voltage				Over Voltage		Surtension				Surtension
95		32		15			Low Voltage				Low Voltage		Tension d'Entrée Basse			V In Basse
96		32		15			Input Under Voltage Protection	Low Input	Protection V Entrée Basse	Protect.VIn Bas
97		32		15			Solar Converter ID			Solar Conv ID		Position Convertisseur			Position Conv.
98		32		15			DC Output Shut Off			DC Output Off		Arrêt Etage DC				Arrêt Etage DC 
99		32		15			Solar Converter Phase			SolarConvPhase		Phase Convertisseur			Phase Conv.
103		32		15			Severe Current Share Alarm		SevereCurrShare		Alarme Défaur Equi Courant	Al.Déf.Equi.I
104		32		15			Barcode 1				Barcode 1		Code Bar 1				Code Bar 1
105		32		15			Barcode 2				Barcode 2		Code Bar 2				Code Bar 2
106		32		15			Barcode 3				Barcode 3		Code Bar 3				Code Bar 3
107		32		15			Barcode 4				Barcode 4		Code Bar 4				Code Bar 4
108		34		15			Solar Converter Comm Fail	SolConvComFail		Défaut Communication			Défaut Com.
109		32		15			No					No			Non					Non
110		32		15			Yes					Yes			Oui					Oui
111		32		15			Existence State				Existence State		Module Présent				Module Présent
113		32		15			Comm OK					Comm OK			Communication correcte			Comm OK
114		32		15			All Solar Converters Comm Fail	All Comm Fail		Aucune communication			Aucune Comm.
115		32		15			Communication Fail			Comm Fail		Pas de Réponse				Pas de Réponse
116		32		15			Valid Rated Current		Rated Current		Courant moyen		Courant moyen
117		32		15			Efficiency		Efficiency		Rendement				Rendement
118		32		15			LT 93				LT 93			Inférieur à 93				Inférieur à 93
119		32		15			GT 93				GT 93			Suppérieur à 93				Suppérieur à 93
120		32		15			GT 95				GT 95			Suppérieur à 95				Suppérieur à 95
121		32		15			GT 96				GT 96			Suppérieur à 96				Suppérieur à 96
122		32		15			GT 97				GT 97			Suppérieur à 97				Suppérieur à 97
123		32		15			GT 98				GT 98			Suppérieur à 98				Suppérieur à 98
124		32		15			GT 99				GT 99			Suppérieur à 99				Suppérieur à 99
125		32		15			Solar Converter HVSD Status			HVSD Status		Etat Convertisseur Tension			Etat Conv. U
126		32		15			Solar Converter Reset			SolConvResetEEM		Reset Convertisseur			Reset Conv.
127		32		15			Night					Night			Nuit					Nuit
128		32		15			Input Current				Input Current		Courant d'entré				Courant d'entré
129		32		15			Input Power			Input Power		Entrée alim			Entrée alim
130		32		15			Input Not DC			Input Not DC		Pas de DC			Pas de DC
131		32		15			Output Power			Output Power		Sortie alim			Sortie alim

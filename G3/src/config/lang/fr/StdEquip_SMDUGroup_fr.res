﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32		15			SMDU Group			SMDU Group		SM-DU Groupe			SM-DU Groupe
2		32		15			Standby				Standby			Alarme				Alarme
3		32		15			Refresh				Refresh			Rafraichir			Rafraichir
4		32		15			Setting Refresh		Setting Refresh		Rafraichir les donnees		Rafrai donnees
5		32		15			E-Stop		E-Stop			E-Stop		E-Stop
6		32		15			Yes				Yes			Oui				Oui
7		32		15			Existence State			Existence State		Carte Presente			Carte Présente
8		32		15			Existent			Existent		Présente			Presente
9		32		15			Not Existent		Not Existent		Inexistant			Inexistant
10		32		15			Number of SMDUs		Num of SMDUs		N Carte SDMU			N Carte SDMU
11		32		15			SMDU Config Changed		Cfg Changed		Changement Config SDMU		Modif.Conf.SDMU
12		32		15			Not Changed			Not Changed		Pas de Changement		Pas de Modif.
13		32		15			Changed				Changed			Changement			Modification

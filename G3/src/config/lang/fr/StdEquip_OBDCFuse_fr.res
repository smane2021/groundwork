﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1		32			15			Fuse 1				Fuse 1			Fusible 1		Fusible 1
2		32			15			Fuse 2				Fuse 2			Fusible 2		Fusible 2
3		32			15			Fuse 3				Fuse 3			Fusible 3		Fusible 3
4		32			15			Fuse 4				Fuse 4			Fusible 4		Fusible 4
5		32			15			Fuse 5				Fuse 5			Fusible 5		Fusible 5
6		32			15			Fuse 6				Fuse 6			Fusible 6		Fusible 6
7		32			15			Fuse 7				Fuse 7			Fusible 7		Fusible 7
8		32			15			Fuse 8				Fuse 8			Fusible 8		Fusible 8
9		32			15			Fuse 9				Fuse 9			Fusible 9		Fusible 9
10		32			15			Auxiliary Load Fuse		Aux Load Fuse		Fusible Auxiliaire	Fus. Aux.
11		32			15			Fuse 1 Alarm			Fuse 1 Alarm		Alarme Fusible 1	Alarme Fus1
12		32			15			Fuse 2 Alarm			Fuse 2 Alarm		Alarme Fusible 2	Alarme Fus2
13		32			15			Fuse 3 Alarm			Fuse 3 Alarm		Alarme Fusible 3	Alarme Fus3
14		32			15			Fuse 4 Alarm			Fuse 4 Alarm		Alarme Fusible 4	Alarme Fus4
15		32			15			Fuse 5 Alarm			Fuse 5 Alarm		Alarme Fusible 5	Alarme Fus5
16		32			15			Fuse 6 Alarm			Fuse 6 Alarm		Alarme Fusible 6	Alarme Fus6
17		32			15			Fuse 7 Alarm			Fuse 7 Alarm		Alarme Fusible 7	Alarme Fus7
18		32			15			Fuse 8 Alarm			Fuse 8 Alarm		Alarme Fusible 8	Alarme Fus8
19		32			15			Fuse 9 Alarm			Fuse 9 Alarm		Alarme Fusible 9	Alarme Fus9
20		32			15			Auxiliary Load Fuse Alarm	AuxLoadFuseAlm			Alarme Fusible Aux	Alarme Fus Aux
21		32			15			On				On			Marche			Marche
22		32			15			Off				Off			Arrêt			Arrêt
23		32			15			DC Fuse Unit			DC Fuse Unit		Unite Fusible DC	Unite Fus DC
24		32			15			Fuse 1 Voltage			Fuse 1 Voltage		Tension Detec.Fuse 1	V Detec.Fuse 1
25		32			15			Fuse 2 Voltage			Fuse 2 Voltage		Tension Detec.Fuse 2	V Detec.Fuse 2
26		32			15			Fuse 3 Voltage			Fuse 3 Voltage		Tension Detec.Fuse 3	V Detec.Fuse 3
27		32			15			Fuse 4 Voltage			Fuse 4 Voltage		Tension Detec.Fuse 4	V Detec.Fuse 4
28		32			15			Fuse 5 Voltage			Fuse 5 Voltage		Tension Detec.Fuse 5	V Detec.Fuse 5
29		32			15			Fuse 6 Voltage			Fuse 6 Voltage		Tension Detec.Fuse 6	V Detec.Fuse 6
30		32			15			Fuse 7 Voltage			Fuse 7 Voltage		Tension Detec.Fuse 7	V Detec.Fuse 7
31		32			15			Fuse 8 Voltage			Fuse 8 Voltage		Tension Detec.Fuse 8	V Detec.Fuse 8
32		32			15			Fuse 9 Voltage			Fuse 9 Voltage		Tension Detec.Fuse 9	V Detec.Fuse 9
33		32			15			Fuse 10 Voltage			Fuse 10 Voltage		Tension Detec.Fuse 10	V Detec.Fuse 10
34		32			15			Fuse 10				Fuse 10			Fusible 10		Fusible 10
35		32			15			Fuse 10 Alarm			Fuse 10 Alarm		Alarme Fusible 10	Alarme Fus10
36		32			15			State				State			Etat			Etat
37		32			15			Communication Fail		Comm Fail		Defaut de communication			Defaut COM
38		32			15			No				No			Non			Non
39		32			15			Yes				Yes			Oui			Oui
40	32			15		Fuse 11				Fuse 11			Fusible 11			Fusible 11
41	32			15		Fuse 12				Fuse 12			Fusible 12			Fusible 12
42	32			15		Fuse 11 Alarm			Fuse 11 Alarm		Alarme Fusible 11	Alarme Fus11
43	32			15		Fuse 12 Alarm			Fuse 12 Alarm		Alarme Fusible 12	Alarme Fus12


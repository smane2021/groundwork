﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr



[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Fuse 1			Fuse 1			Fusible 1			Fusible 1
2	32			15			Fuse 2			Fuse 2			Fusible 2			Fusible 2
3	32			15			Fuse 3			Fuse 3			Fusible 3			Fusible 3
4	32			15			Fuse 4			Fuse 4			Fusible 4			Fusible 4
5	32			15			Fuse 5			Fuse 5			Fusible 5			Fusible 5
6	32			15			Fuse 6			Fuse 6			Fusible 6			Fusible 6
7	32			15			Fuse 7			Fuse 7			Fusible 7			Fusible 7
8	32			15			Fuse 8			Fuse 8			Fusible 8			Fusible 8
9	32			15			Fuse 9			Fuse 9			Fusible 9			Fusible 9
10	32			15			Fuse 10			Fuse 10			Fusible 10			Fusible 10
11	32			15			Fuse 11			Fuse 11			Fusible 11			Fusible 11
12	32			15			Fuse 12			Fuse 12			Fusible 12			Fusible 12
13	32			15			Fuse 13			Fuse 13			Fusible 13			Fusible 13
14	32			15			Fuse 14			Fuse 14			Fusible 14			Fusible 14
15	32			15			Fuse 15			Fuse 15			Fusible 15			Fusible 15
16	32			15			Fuse 16			Fuse 16			Fusible 16			Fusible 16
17	32			15		SMDUP12 DC Fuse		SMDUP12 DC Fuse		Fusible DC SMDU+12		Fus DC SMDU+12
18	32			15			State			State			Etat				Etat
19	32			15			Off			Off			Off				Off
20	32			15			On			On			On				On
21	32			15			DC Fuse 1 Alarm			Fuse 1 Alm		Alarme Fusible 1		Alm fus1
22	32			15			DC Fuse 2 Alarm			Fuse 2 Alm		Alarme Fusible 2		Alm fus2
23	32			15			DC Fuse 3 Alarm			Fuse 3 Alm		Alarme Fusible 3		Alm fus3
24	32			15			DC Fuse 4 Alarm			Fuse 4 Alm		Alarme Fusible 4		Alm fus4
25	32			15			DC Fuse 5 Alarm			Fuse 5 Alm		Alarme Fusible 5		Alm fus5
26	32			15			DC Fuse 6 Alarm			Fuse 6 Alm		Alarme Fusible 6		Alm fus6
27	32			15			DC Fuse 7 Alarm			Fuse 7 Alm		Alarme Fusible 7		Alm fus7
28	32			15			DC Fuse 8 Alarm			Fuse 8 Alm		Alarme Fusible 8		Alm fus8
29	32			15			DC Fuse 9 Alarm			Fuse 9 Alm		Alarme Fusible 9		Alm fus9
30	32			15			DC Fuse 10 Alarm		Fuse 10 Alm		Alarme Fusible 10		Alm fus10
31	32			15			DC Fuse 11 Alarm		Fuse 11 Alm		Alarme Fusible 11		Alm fus11
32	32			15			DC Fuse 12 Alarm		Fuse 12 Alm		Alarme Fusible 12		Alm fus12
33	32			15			DC Fuse 13 Alarm		Fuse 13 Alm		Alarme Fusible 13		Alm fus13
34	32			15			DC Fuse 14 Alarm		Fuse 14 Alm		Alarme Fusible 14		Alm fus14
35	32			15			DC Fuse 15 Alarm		Fuse 15 Alm		Alarme Fusible 15		Alm fus15
36	32			15			DC Fuse 16 Alarm		Fuse 16 Alm		Alarme Fusible 16		Alm fus16
37	32			15			Times of Communication Fail		Times Comm Fail	Duree du defaut de communication			DureeDefCOM
38	32			15			Communication Fail			Comm Fail		Defaut de comunication	Defaut de COM
39	32			15		Fuse 17			Fuse 17			Fusible 17			Fusible 17
40	32			15		Fuse 18			Fuse 18			Fusible 18			Fusible 18
41	32			15		Fuse 19			Fuse 19			Fusible 19			Fusible 19
42	32			15		Fuse 20			Fuse 20			Fusible 20			Fusible 20
43	32			15		Fuse 21			Fuse 21			Fusible 21			Fusible 21
44	32			15		Fuse 22			Fuse 22			Fusible 22			Fusible 22
45	32			15		Fuse 23			Fuse 23			Fusible 23			Fusible 23
46	32			15		Fuse 24			Fuse 24			Fusible 24			Fusible 24
47	32			15		Fuse 25			Fuse 25			Fusible 25			Fusible 25
48	32			15		DC Fuse 17 Alarm		Fuse 17 Alm		Alarme Fusible 17		Alm fus17
49	32			15		DC Fuse 18 Alarm		Fuse 18 Alm		Alarme Fusible 18		Alm fus18
50	32			15		DC Fuse 19 Alarm		Fuse 19 Alm		Alarme Fusible 19		Alm fus19
51	32			15		DC Fuse 20 Alarm		Fuse 20 Alm		Alarme Fusible 20		Alm fus20
52	32			15		DC Fuse 21 Alarm		Fuse 21 Alm		Alarme Fusible 21		Alm fus21
53	32			15		DC Fuse 22 Alarm		Fuse 22 Alm		Alarme Fusible 22		Alm fus22
54	32			15		DC Fuse 23 Alarm		Fuse 23 Alm		Alarme Fusible 23		Alm fus23
55	32			15		DC Fuse 24 Alarm		Fuse 24 Alm		Alarme Fusible 24		Alm fus24
56	32			15		DC Fuse 25 Alarm		Fuse 25 Alm		Alarme Fusible 25		Alm fus25


﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			EIB-4			EIB-4			EIB-4			EIB-4
9		32			15			Bad Battery Block	Bad Batt Block		Bloc Bat HS		Bloc Bat HS
10		32			15			Load 1				Load 1			Charge 1		Charge 1
11		32			15			Load 2				Load 2			Charge 2		Charge 2
12		32			15			DO1-Relay Output		DO1-RelayOutput		Sortie DO1 relais	Sortie DO1 Rel
13		32			15			DO2-Relay Output		DO2-RelayOutput		Sortie DO2 relais	Sortie DO2 Rel
14		32			15			DO3-Relay Output		DO3-RelayOutput		Sortie DO3 relais	Sortie DO3 Rel
15		32			15			DO4-Relay Output		DO4-RelayOutput		Sortie DO4 relais	Sortie DO4 Rel
16		32			15			EIB Communication Fail		EIB Comm Fail		Defaut Communication EIB		Def Com EIB
17		32			15			State			State			Etat			Etat
18		32			15			Shunt 2 Full Current		Shunt 2 Current		I Shunt 2		I Shunt 2
19		32			15			Shunt 3 Full Current		Shunt 3 Current			I Shunt 3		I Shunt 3
20		32			15			Shunt 2 Full Voltage		Shunt 2 Voltage		V Shunt 2		V Shunt 2
21		32			15			Shunt 3 Full Voltage		Shunt 3 Voltage		V Shunt 3		V Shunt 3
22		32			15			Load 1			Load 1		Shunt utilisation 1			Shunt util. 1
23		32			15			Load 2			Load 2		Shunt utilisation 2			Shunt util. 2
24		32			15			Enabled				Enabled				Active			Active
25		32			15			Disabled				Disabled			Inactive		Inactive
26		32			15			Active				Active		Actif			Actif
27		32			15			Not Active			Not Active			Non Actif			Non Actif
28		32			15			State			State			Etat			Etat
29		32			15			No			No			Non			Non
30		32			15			Yes			Yes			Oui			Oui
31		32			15			EIB Communication Fail		EIB Comm Fail		Defaut Communication EIB		Def Com EIB
32		32			15			Voltage 1		Voltage 1		Tension 1		Tension 1
33		32			15			Voltage 2		Voltage 2		Tension 2		Tension 2
34		32			15			Voltage 3		Voltage 3		Tension 3		Tension 3
35		32			15			Voltage 4		Voltage 4		Tension 4		Tension 4
36		32			15			Voltage 5		Voltage 5		Tension 5		Tension 5
37		32			15			Voltage 6		Voltage 6		Tension 6		Tension 6
38		32			15			Voltage 7		Voltage 7		Tension 7		Tension 7
39		32			15			Voltage 8		Voltage 8		Tension 8		v 8
40		32			15			Number of Battery Shunts	Num Batt Shunts			Numero Bat		Numero Bat
41		32			15			0			0			0			0
42		32			15			1			1			1			1
43		32			15			2			2			2			2
44		32			15			Load 3			Load 3		Util 3		Util 3
45		32			15			3			3			3			3
46		32			15			Number of Load Shunts		Num Load Shunts		N° Utilisation		N° Util.
47		32			15			Shunt 1 Full Current		Shunt 1 Current		I Shunt 1		I Shunt 1
48		32			15			Shunt 1 Full Voltage		Shunt 1 Voltage			V Shunt 1		V Shunt 1
49		32			15			Voltage Type		Voltage Type		Type tension		Type tension
50		32			15			48 (Block4)			48 (Block4)			48(Block4)		48(Block4)
51		32			15			Mid Point			Mid Point		Point Milieu		Point Milieu
52		32			15			24 (Block2)			24 (Block2)		24(Block2)		24(Block2)
53		32			15			Block Voltage Diff (12V)	Blk V Diff(12V)		Difference tension(12V)	Diff V (12V)
54		32			15			DO5-Relay Output		DO5-RelayOutput		Sortie DO5 relais	Sortie DO5 Rel
55		32			15			Block Voltage Diff (Mid)	Blk V Diff(Mid)		Diff tension(Point Milieu)	Diff.V.(Centre)
56		32			15			Block In-Use Num		Block In-Use		Nombre de blocs utilisés	Nombre de blocs
78		32			15			EIB-4 DO1 Test			EIB-4 DO1 Test	Test DO1			Test DO1
79		32			15			EIB-4 DO2 Test			EIB-4 DO2 Test	Test DO2			Test DO2
80		32			15			EIB-4 DO3 Test			EIB-4 DO3 Test	Test DO3			Test DO3
81		32			15			EIB-4 DO4 Test			EIB-4 DO4 Test	Test DO4			Test DO4
82		32			15			EIB-4 DO5 Test			EIB-4 DO5 Test	Test DO5			Test DO5
83		32			15			Not Used				Not Used				Non Utilisée				Non Utilisée
84		32			15			General				General				General	General
85		32			15			Load						Load						Charge	Charge
86		32			15			Battery					Battery					Batterie				Batterie
87		32			15			Shunt1 Set As			Shunt1SetAs			Config shunt 1	Conf SH1 
88		32			15			Shunt2 Set As			Shunt2SetAs			Config shunt 2	Conf SH2 
89		32			15			Shunt3 Set As			Shunt3SetAs			Config shunt 3	Conf SH3 
90		32			15			Shunt 1		Shunt 1		Lecture shunt1	Lecture SH1
91		32			15			Shunt 2		Shunt 2		Lecture shunt2	Lecture SH2
92		32			15			Shunt 3		Shunt 3		Lecture shunt3	Lecture SH3
93		32			15			Temperature1				Temp1			Température 1			Temp1
94		32			15			Temperature2				Temp2			Température 2			Temp2
95		32			15			Source 1		Source 1		Source 1				Source 1
96		32			15			Source 2		Source 2		Source 2				Source 2
97		32			15			Source 3		Source 3		Source 3				Source 3
500		32			15			Current1 Break Size		Curr1 Brk Size			I1 Taille de la pause		I1TaillePause
501		32			15			Current1 High 1 Current Limit	Curr1 Hi1 Lmt			I1 Haute 1 limite de courant	I1Haute1LmtCour
502		32			15			Current1 High 2 Current Limit	Curr1 Hi2 Lmt			I1 Haute 2 limite de courant	I1Haute2LmtCour
503		32			15			Current2 Break Size		Curr2 Brk Size			I2 Taille de la pause		I2TaillePause	
504		32			15			Current2 High 1 Current Limit	Curr2 Hi1 Lmt			I2 Haute 1 limite de courant	I2Haute1LmtCour	
505		32			15			Current2 High 2 Current Limit	Curr2 Hi2 Lmt			I2 Haute 2 limite de courant	I2Haute2LmtCour	
506		32			15			Current3 Break Size		Curr3 Brk Size			I3 Taille de la pause		I3TaillePause
507		32			15			Current3 High 1 Current Limit	Curr3 Hi1 Lmt			I3 Haute 1 limite de courant	I3Haute1LmtCour	
508		32			15			Current3 High 2 Current Limit	Curr3 Hi2 Lmt			I3 Haute 2 limite de courant	I3Haute2LmtCour	
509		32			15			Current1 High 1 Current	Curr1 Hi 1				I1 Courant élevé1		I1Courélevé1	
510		32			15			Current1 High 2 Current	Curr1 Hi 2				I1 Courant élevé2		I1Courélevé2	
511		32			15			Current2 High 1 Current	Curr2 Hi 1				I2 Courant élevé1		I2Courélevé1	
512		32			15			Current2 High 2 Current	Curr2 Hi 2				I2 Courant élevé2		I2Courélevé2	
513		32			15			Current3 High 1 Current	Curr3 Hi 1				I3 Courant élevé1		I3Courélevé1	
514		32			15			Current3 High 2 Current	Curr3 Hi 2				I3 Courant élevé2		I3Courélevé2	

550		32			15			Source			Source				Source			Source

﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			Battery Current				Battery Current			Courant				Courant
2		32			15			Battery Rating(Ah)			Batt Rating(Ah)		Capacite batterie(Ah)		Cap bat(Ah)
3		32			15			Battery Current Limit Exceeded		Ov Batt Cur Lmt			Dépassement limit.Courant	Dépasse I lmt
4		32			15			Battery					Battery			Batterie			Batterie
5		32			15			Battery Over Current			Batt Over Curr		Dépassement limit.Courant	Dépasse I lmt
6		32			15			Battery Capacity (%)			Batt Cap(%)		Capacite batterie(%)		Capacite bat(%)
7		32			15			Battery Voltage				Battery Voltage		Tension batterie		Tension bat
8		32			15			Battery Low Capacity			Batt Low Cap		Capacité basse			Capacité basse
9		32			15			Battery Fuse Failure			Batt Fuse Fail		Défaut Protection Bat		Déf Protect Bat
10		32			15			DC Distribution Sequence Number		DC Dist Seq Num	Défaut Protection DC		Déf Protect DC
11		32			15			Battery Over Voltage			Batt Over Volt		Surtension Batterie		Sur U Bat
12		32			15			Battery Under Voltage			Batt Under Volt		Soustension Batterie		Sous U Bat
13		32			15			Battery Over Current			Batt Over Curr		Sur Courant Batterie		Sur I Bat
14		32			15			Battery Fuse Failure			Batt Fuse Fail		Défaut Protection Bat		Déf Protect Bat
15		32			15			Battery Over Voltage			Batt Over Volt		Sur tension Batterie		Sur U Bat
16		32			15			Battery Under Voltage			Batt Under Volt			Sous tension Batterie		Sous U Bat
17		32			15			Battery Over Current			Batt Over Curr		Sur Courant Batterie		Sur I Bat
18		32			15			LargeDU Battery				LargeDU Batt		Batterie Grand DU		Bat Grand DU
19		32			15			Battery Shunt Coefficient		Bat Shunt Coeff		Compensation Température	Comp. Temp
20		32			15			Over Voltage Limit			Over Volt Limit			Seuil Sur Tension		Seuil Sur U
21		32			15			Low Voltage Limit			Low Volt Limit		Seuil Sous Tension		Seuil Sous U
22		32			15			Battery Communication Fail		Batt Comm Fail		Pas de Réponse Batterie		Pas Rép.Bat.
23		32			15			Communication OK			Comm OK		Communication OK			COMM OK
24		32			15			Communication Fail			Comm Fail		Defaut de communication			Defaut COM
25		32			15			Communication Fail			Comm Fail		Defaut de communication			Defaut COM
26		32			15			Existence State				Existence State		Détection			Détection
27		32			15			Existent				Existent		Présent				Présent
28		32			15			Not Existent				Not Existent		Non Présent			Non Présent
29		32			15			Rated Capacity				Rated Capacity		Estimation Capacité		Estim.Capa.
30		32			15			Battery Management		Batt Management		Gestion Batteries		Gestion Bat
31		32			15			Enabled					Enabled			Valide			Valide
32		32			15			Disabled				Disabled		Invalide		Invalide
97		32			15			Battery Temperature			Battery Temp		Température Batterie		Temp Batterie
98		32			15			Battery Temperature Sensor		Bat Temp Sensor			No Capteur Temperature Batterie	NCapt.Temp.Bat.
99		32			15			None					None			Aucun				Aucun
100		32			15			Temperature 1				Temperature 1			Capteur Température 1		Capt.Temp.1
101		32			15			Temperature 2				Temperature 2		Capteur Température 2		Capt.Temp.2
102		32			15			Temperature 3				Temperature 3			Capteur Température 3		Capt.Temp.3
103		32			15			Temperature 4				Temperature 4		Capteur Température 4		Capt.Temp.4
104		32			15			Temperature 5				Temperature 5			Capteur Température 5		Capt.Temp.5
105		32			15			Temperature 6				Temperature 6			Capteur Température 6		Capt.Temp.6
106		32			15			Temperature 7				Temperature 7			Capteur Température 7		Capt.Temp.7
107		32			15			Temperature 8				Temperature 8			Capteur Température 8		Capt.Temp.8
108		32			15			Temperature 9				Temperature 9			Capteur Température 9		Capt.Temp.9
109		32			15			Temperature 10				Temperature 10			Capteur Température 10		Capt.Temp.10

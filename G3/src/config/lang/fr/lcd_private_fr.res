﻿#
#  Locale language support:French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# ABBR_IN_EN: Abbreviated English name
# ABBR_IN_LOCALE: Abbreviated locale name
# ITEM_DESCRIPTION: The description of the resource item
#
[LOCALE_LANGUAGE]
fr

#1. Define the number of the self define multi language display items
[SELF_DEFINE_LANGUAGE_ITEM_NUMBER]
58

[SELF_DEFINE_LANGUAGE_ITEM_INFO]
#Sequence ID	#RES_ID		MAX_LEN_OF_BYTE_ABBR	ABBR_IN_EN		ABBR_IN_LOCALE		ITEM_DESCRIPTION
1		1		32			Main Menu		Menu Principal		Main Menu
2		2		32			Status			Etat			Running Info
3		3		32			Manual			Manuel			Maintain
4		4		32			Settings		Contrôle		Parameter Set
5		5		32			ECO Mode		Mode ECO		Energy Saving Parameter Set
6		6		32			Quick Settings		Config Rapid		Quick Settings Menu
7		7		32			Quick Settings		Config Rapid		Quick Settings Menu
8		8		32			Test Menu 1		Menu Test 1		Menu for self test
9		9		32			Test Menu 2		Menu Test 2		Menu for self test
10		10		32			Man/Auto Set		Manuel/Auto		Man/Auto Set in Maintain SubMenu
#
11		11		32			Select User		Utilisateur		Select user in password input screen
12		12		32			Enter Password		Mot de passe		Enter password in password input screen
#
13		13		32			Slave Settings		Config Esclaves		Slave Parameter Set
#
21		21		32			Active Alarms		Alarme Active		Active Alarms
22		22		32			Alarm History		Histor. Alarm		Alarm History
23		23		32			No Active Alarm		Pas d'Alarm Act.	No Active Alarm
24		24		32			No Alarm History	Pas Histor Alarm	No Alarm History
#
31		31		32			Acknowledge Info	Info utilisateur		Acknowledge Info
32		32		32			ENT Confirm		ENT Confirme 		ENT to run
33		33		32			ESC Cancel		ESC Sortir		ESC Quit
34		34		32			Prompt Info		Prompt Info		Prompt Info
35		35		32			Password Error		Erreur Mot Passe	Password Error!
36		36		32			ESC or ENT Ret		ESC ou ENTRER		ESC or ENT Ret
37		37		32			No Privilege		Pas de Droit		No Privilege
38		38		32			No Item Info		Pas d'Info.		No Item Info
39		39		32			Switch to Next		Equip. Suivant		Switch To Next equip
40		40		32			Switch to Prev		Equip. Precedent	Switch To Previous equip
41		41		32			Disabled Set		Desac. Param.		Disabled Set
42		42		32			Disabled Ctrl		Desac. Control		Disabled Ctrl
43		43		32			Conflict Setting	Conflit Config		Conflict setting of signal relationship
44		44		32			Failed to Set		Action refusee		Failed to Control or set
45		45		32			HW Protect		HW Protection		Hardware Protect status
46		46		32			Reboot System		Red Système		Reboot System
47		47		32			App is Auto		App est Auto		App is Auto configing
48		48		32			Configuring		Configuration		App is Auto configing
49		49		32			Copying File		Copie de Fichier	Copy config file
50		50		32			Please wait...		Attendez SVP		Please Wait...
51		51		32			Switch to Set		Change Niveau		Switch to Set Alarm Level or Grade
52		52		32			DHCP is Open		DHCP est ouvert
53		53		32			Cannot set.		Param Impossible
54		54		32			Download Entire		Transmition Comp	Download Config File completely
55		55		32			Reboot Validate		RebootPour Valid	Validate after reboot
#
#
#以下为Barcode信号，其ID不得大于256
61		61		32			Sys Inventory		Invent Sys		Product info of Devices
62		62		32			Device Name		Nom du Produit		Device Name
63		63		23			Part Number		Code Produit		Part Number
64		64		32			Product Ver		Version Produit		HW Version
65		65		32			SW Version		Version SW		SW Version
66		66		32			Serial Number		Numero Serie		Serial Number
#
#Language Name and Reboot Validate Prompt for Language Select Screen
80		80		32			French			Français		French
81		81		32			Reboot Validate		Valide apres redemarrage		Reboot Validate
#
82		82		32			Alm Severity		Niveau Alarme		Alarm Grade
83		83		32			None			Aucun			No Alarm
84		84		32			Observation		Observation			Observation alarm
85		85		32			Major			Mineure			Major alarm
86		86		32			Critical		Majeure				Critical alarm
#											
87		87		32			Alarm Relay		Relais Alarm		Alarm Relay Settings
88		88		32			None			Aucun			No Relay Output
89		89		32			Relay 1			Relais 1		Relay Output 1 of IB
90		90		32			Relay 2			Relais 2		Relay Output 2 of IB
91		91		32			Relay 3			Relais 3		Relay Output 3 of IB
92		92		32			Relay 4			Relais 4		Relay Output 4 of IB
93		93		32			Relay 5			Relais 5		Relay Output 5 of IB
94		94		32			Relay 6			Relais 6		Relay Output 6 of IB
95		95		32			Relay 7			Relais 7		Relay Output 7 of IB
96		96		32			Relay 8			Relais 8		Relay Output 8 of IB
97		97		32			Relay 9			Relais 9		Relay Output 1 of EIB
98		98		32			Relay 10		Relais 10		Relay Output 2 of EIB
99		99		32			Relay 11		Relais 11		Relay Output 3 of EIB
100		100		32			Relay 12		Relais 12		Relay Output 4 of EIB
101		101		32			Relay 13		Relais 13		Relay Output 5 of EIB
#
102		102		32			Alarm Param		Param. Alarme		Alarm Param
103		103		32			Alarm Voice		Son Alarme		Alarm Voice
104		104		32			Block Alarm		Blocage Alarme		Block Alarm
105		105		32			Clr Alm Hist		Effac.Hist.Alar		Clear History alarm
106		106		32			Yes			Oui			Yes
107		107		32			No			Non			No
#
108		108		32			Alarm Voltage		V alarme		Alarm Voltage Level of IB
#
#
121		121		32			Sys Settings		Param Système		System Param
122		122		32			Language		Langue			Current language displayed in LCD screen
123		123		32			Time Zone		Fuseau Horaire		Time Zone
124		124		32			Date			Date			Set ACU+ Date, according to time zone
125		125		32			Time			Heure			Set Time, accoring to time zone
126		126		32			Reload Config		Recharge Config		Reload Default Configuration
127		127		32			Keypad Voice		Son Touche		Keypad Voice
128		128		32			Download Config		Download Config		Download config file
129		129		32			Auto Config		Config. Auto.		Auto config
#
#
141		141		32			Communication		Communication		Communication Parameter
#
142		142		32			DHCP			DHCP			DHCP Function
143		143		32			IP Address		Adresse IP		IP Address of ACU+
144		144		32			Subnet Mask		Masque Reseau		Subnet Mask of ACU+
145		145		32			Default Gateway		Passerelle IP		Default Gateway of ACU+
#
146		146		32			Self Address		Adressage Auto		Self Addr
147		147		32			Port Type		Type connect.		Connection Mode
148		148		32			Port Param		Param. Port		Port Parameter
149		149		32			Alarm Report 		Rep.Alarm		Alarm Report 
150		150		32			Dial Times 		Dial Temps		Dialing Attempt Times
151		151		32			Dial Interval		Dial Interval		Dialing Interval
152		152		32			1st Phone Num		1er N° Appel		First Call Back Phone  Num
153		153		32			2nd Phone  Num		2ndr N° Appel		Second Call Back Phone  Num
154		154		32			3rd Phone  Num		3ieme N° Appel		Third Call Back Phone  Num
#
161		161		32			Enabled			Activée			Enable DHCP
162		162		32			Disabled		Désactivé				Disable DHCP
163		163		32			Error			Erreur			DHCP function error
164		164		32			RS-232			RS-232			YDN23 Connection Mode RS-232
165		165		32			Modem			Modem			YDN23 Connection Mode MODEM
166		166		32			Ethernet		Ethernet		YDN23 Connection Mode Ethernet
167		167		32			5050			5050			Ethernet Port Number
168		168		32			2400,n,8,1		2400,n,8,1		Serial Port Parameter
169		169		32			4800,n,8,1		4800,n,8,1		Serial Port Parameter
170		170		32			9600,n,8,1		9600,n,8,1		Serial Port Parameter
171		171		32			19200,n,8,1		19200,n,8,1		Serial Port Parameter
172		172		32			38400,n,8,1		38400,n,8,1		Serial Port Parameter
#
# The next level of Battery Group
201		201		32			Basic			Basique			Sub Menu Resouce of BattGroup Para Setting
202		202		32			Charge			Charge			Sub Menu Resouce of BattGroup Para Setting
203		203		32			Test			Test			Sub Menu Resouce of BattGroup Para Setting
204		204		32			Temp Comp		Compensat. Temp		Sub Menu Resouce of BattGroup Para Setting
205		205		32			Capacity		Capacite		Sub Menu Resouce of BattGroup Para Setting
# The next level of Power System
206		206		32			General			General			Sub Menu Resouce of PowerSystem Para Setting
207		207		32			Power Split		Power Split		Sub Menu Resouce of PowerSystem Para Setting
208		208		32			Temp Probe(s)		Sonde (s) Temp		Sub Menu Resouce of PowerSystem Para Setting
# ENERGY SAVING
209		209		32			ECO Mode		Mode ECO		Sub Menu Resouce of ENERGY SAVING
#
#以下用于：在默认屏按ESC，显示设备及配置信息
301		301		32			Serial Num		N° Serie		Serial Number
302		302		32			HW Ver			HW Ver			Hardware Version
303		303		32			SW Ver			SW Ver			Software Version
304		304		32			MAC Addr		Adr MAC:		MAC Addr
305		305		32			File Sys		Syst rev:		File System Revision
306		306		32			Device Name		Nom l'app		Product Model
307		307		32			Config			Config:			Solution Config File Version
#
#
501		501		32			LCD Size		Resol Ecran		Set the LCD Height
502		502		32			128x64			128x64			Set the LCD Height to 128 X 64
503		503		32			128x128			128x128			Set the LCD Height to 128 X 128
504		504		32			LCD Rotation		Rotat. Ecran		Set the LCD Rotation
505		505		32			0 deg			0º			Set the LCD Rotation to 0 degree
506		506		32			90 deg			90º			Set the LCD Rotation to 90 degree
507		507		32			180 deg			180º			Set the LCD Rotation to 180 degree
508		508		32			270 deg			270º			Set the LCD Rotation to 270 degree
#
#
601		601		32			All Rect Ctrl		Control Ts Red
602		602		32			All Rect Set		Param. Ts Red
#
621		621		32			Rectifier		Redresseur
622		622		32			Battery			Batteries
623		623		32			LVD			LVD
624		624		32			Rect AC			CA
625		625		32			Converter		Convertisseur
626		626		32			SMIO			SMIO
627		627		32			Diesel			G Electrogène
628		628		32			Rect Group 2		GroupeRed2
629		629		32			Rect Group 3		GroupeRed3
630		630		32			Rect Group 4		GroupeRed4
631		631		32			All Conv Ctrl		Control Convert
632		632		32			All Conv Set		Param Convert
633		633		32			SMDU			SMDU
#
1001		1001		32			Auto/Manual		Manuel/Auto
1002		1002		32			ECO Mode Set		Mode ECO Set
1003		1003		32			FLT/EQ Voltage		FLT/EQ Tension
1004		1004		32			FLT/EQ Change		FLT/EQ Change
1005		1005		32			Temp Comp		Temp Comp
1006		1006		32			Work Mode Set		Mode Travail Set
1007		1007		32			Maintenance		Entretien
1008		1008		32			Energy Saving		Eco Energie
1009		1009		32			Alarm Settings		Param Alarm
1010		1010		32			Rect Settings		Param Rect
1011		1011		32			Batt Settings		Param Batt
1012		1012		32			Batt1 Settings		Param Batt1
1013		1013		32			Batt2 Settings		Param Batt2
1014		1014		32			LVD Settings		Param LVD
1015		1015		32			AC Settings		Param AC
1016		1016		32			Template 1		Modèle 1
1017		1017		32			Template 2		Modèle 2
1018		1018		32			Template N		Modèle N
#
1101		1101		32			Batt1			Batt1
1102		1102		32			Batt2			Batt2
1103		1103		32			Comp			Comp
1104		1104		32			Amb			Amb
1105		1105		32			Remain			Rester
1106		1106		32			RectNum			RectNum


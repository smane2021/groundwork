﻿#
#  Locale language support:french
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			Voltage L1-N		Volt L1-N		Tension L1-N		Tension L1-N
2		32			15			Voltage L2-N		Volt L2-N		Tension L2-N		Tension L2-N
3		32			15			Voltage L3-N		Volt L3-N		Tension L3-N		Tension L3-N
4		32			15			Voltage L1-L2		Volt L1-L2		Tension L1-L2		Tension L1-L2
5		32			15			Voltage L2-L3		Volt L2-L3		Tension L2-L3		Tension L2-L3
6		32			15			Voltage L3-L1		Volt L3-L1		Tension L3-L1		Tension L3-L1
7		32			15			Current L1		Curr L1			Courant L1			Courant L1
8		32			15			Current L2		Curr L2			Courant L2			Courant L2
9		32			15			Current L3		Curr L3			Courant L3			Courant L3
10		32			15			Watt L1			Watt L1			Watts L1			Watts L1
11		32			15			Watt L2			Watt L2			Watts L2			Watts L2
12		32			15			Watt L3			Watt L3			Watts L3			Watts L3

13		32			15			VA L1			VA L1			VA L1			VA L1
14		32			15			VA L2			VA L2			VA L2			VA L2
15		32			15			VA L3			VA L3			VA L3			VA L3

16		32			15			VAR L1			VAR L1			VAR L1			VAR L1
17		32			15			VAR L2			VAR L2			VAR L2			VAR L2
18		32			15			VAR L3			VAR L3			VAR L3			VAR L3

19		32			15			AC Frequency		AC Frequency		Frequence AC		Frequence AC



20		32			15			Communication State	Comm State		Etat de la communication		Etat COM
21		32			15			Existence State		Existence State		Etat existant		Etat existant

22		32			15			AC Meter		AC Meter		Mesure AC		Mesure AC


23		32			15			On					On			on			on
24		32			15			Off					Off			off			off

25		32			15			Existent		Existent		Existant			Existant
26		32			15			Not Existent		Not Existent		Inexistant			Inexistant
27		32			15			Communication Fail			Comm Fail		Defaut de communication		Defaut COM

28		32			15			V L-N ACC		V L-N ACC		V L-N ACC		V L-N ACC
29		32			15			V L-L ACC		V L-L ACC		V L-L ACC		V L-L ACC
30		32			15			Watt ACC		Watt ACC		Watt ACC		Watt ACC
31		32			15			VA ACC			VA ACC			VAR ACC			VAR ACC
32		32			15			VAR ACC			VAR ACC			VAR ACC			VAR ACC
33		32			15			DMD Watt ACC		DMD Watt ACC		DMD Watt ACC		DMD Watt ACC
34		32			15			DMD VA ACC		DMD VA ACC		DMD VA ACC		DMD VA ACC
35		32			15			PF L1			PF L1			PF L1			PF L1
36		32			15			PF L2			PF L2			PF L2			PF L2
37		32			15			PF L3			PF L3			PF L3			PF L3
38		32			15			PF ACC			PF ACC			PF ACC			PF ACC
39		32			15			Phase Sequence		Phase Sequence		Ordres des phases		Ordre Phase
40		32			15			L1-L2-L3		L1-L2-L3		L1-L2-L3		L1-L2-L3
41		32			15			L1-L3-L2		L1-L3-L2		L1-L3-L2		L1-L3-L2


42	32			15		Nominal Line Voltage			NominalLineVolt		Tension nominale reseau		TensNomReseauAC
43	32			15		Nominal Phase Voltage			Nominal PH-Volt		Tension nominale Phase		TensNomPhase
44	32			15		Nominal Frequency			Nominal Freq		Frequence nominale		Hz nominale
45	32			15		Mains Failure Alarm Limit 1		Mains Fail Alm1		Seuil alarme 1 défaut secteur		SeuilAl1DefSect
46	32			15		Mains Failure Alarm Limit 2		Mains Fail Alm2		Seuil alarme 2 défaut secteur		SeuilAl2DefSect
47	32			15		Frequency Alarm Limit			Freq Alarm Lmt		Seuil alarme 1 fréquence secteur		SeuilAl1 HzSect
48	32			15			Current Alarm Limit			Curr Alm Limit		Seuil sur courant		SeuilSurCourant

51	32			15		Line AB Over Voltage 1			L-AB Over Volt1		Surtension 1 L-AB		Surtension1L-AB
52	32			15			Line AB Over Voltage 2			L-AB Over Volt2		Surtension 2 L-AB		Surtension2L-AB
53	32			15			Line AB Under Voltage 1			L-AB UnderVolt1		Soustension 1 L-AB		Soustensi.1L-AB
54	32			15			Line AB Under Voltage 2			L-AB UnderVolt2		Soustension 2 L-AB		Soustensi.2L-AB
55	32			15			Line BC Over Voltage 1			L-BC Over Volt1		Surtension 1 L-BC		Surtension1L-BC
56	32			15			Line BC Over Voltage 2			L-BC Over Volt2		Surtension 2 L-BC		Surtension2L-BC
57	32			15			Line BC Under Voltage 1			L-BC UnderVolt1		Soustension 1 L-BC		Soustensi.1L-BC
58	32			15			Line BC Under Voltage 2			L-BC UnderVolt2		Soustension 2 L-BC		Soustensi.2L-BC
59	32			15			Line CA Over Voltage 1			L-CA Over Volt1		Surtension 1 L-CA		Surtension1L-CA
60	32			15			Line CA Over Voltage 2			L-CA Over Volt2		Surtension 2 L-CA		Surtension2L-CA
61	32			15			Line CA Under Voltage 1			L-CA UnderVolt1		Soustension 1 L-CA		Soustensi.1L-CA
62	32			15			Line CA Under Voltage 2			L-CA UnderVolt2		Soustension 2 L-CA		Soustensi.2L-CA
63	32			15			Phase A Over Voltage 1			PH-A Over Volt1		Surtension1 phase A		Surtension1 PhA
64	32			15			Phase A Over Voltage 2			PH-A Over Volt2		Surtension2 phase A		Surtension2 PhA
65	32			15			Phase A Under Voltage 1			PH-A UnderVolt1		Soustension1 phase A	Soustensi.1 PhA
66	32			15			Phase A Under Voltage 2			PH-A UnderVolt2		Soustension2 phase A	Soustensi.2 PhA
67	32			15			Phase B Over Voltage 1			PH-B Over Volt1		Surtension1 phase B		Surtension1 PhB
68	32			15			Phase B Over Voltage 2			PH-B Over Volt2		Surtension2 phase B		Surtension2 PhB
69	32			15			Phase B Under Voltage 1			PH-B UnderVolt1		Soustension1 phase B	Soustensi.1 PhB
70	32			15			Phase B Under Voltage 2			PH-B UnderVolt2		Soustension2 phase B	Soustensi.2 PhB
71	32			15			Phase C Over Voltage 1			PH-C Over Volt1		Surtension1 phase C		Surtension1 PhC
72	32			15			Phase C Over Voltage 2			PH-C Over Volt2		Surtension2 phase C		Surtension2 PhC
73	32			15			Phase C Under Voltage 1			PH-C UnderVolt1		Soustension1 phase C	Soustensi.1 PhC
74	32			15			Phase C Under Voltage 2			PH-C UnderVolt2		Soustension2 phase C	Soustensi.2 PhC
75	32			15			Mains Failure				Mains Failure		Defaut secteur		Defaut secteur
76	32			15			Severe Mains Failure			SevereMainsFail		Defaut secteur critique		DefSectCritique
77	32			15			High Frequency				High Frequency		Haute frequence		Haute frequence
78	32			15			Low Frequency				Low Frequency		Basse frequence		Basse frequence
79	32			15			Phase A High Current			PH-A High Curr		Surcourant Phase A			Surcourant PhA
80	32			15			Phase B High Current			PH-B High Curr		Surcourant Phase B			Surcourant PhB
81	32			15			Phase C High Current			PH-C High Curr		Surcourant Phase C			Surcourant PhC

82	32			15			DMD W MAX				DMD W MAX		DMD W MAX		DMD W MAX
83	32			15			DMD VA MAX				DMD VA MAX		DMD VA MAX		DMD VA MAX
84	32			15			DMD A MAX				DMD A MAX		DMD A MAX		DMD A MAX
85	32			15			KWH(+) TOT				KWH(+) TOT		KWH(+) TOT		KWH(+) TOT
86	32			15			KVARH(+) TOT				KVARH(+) TOT		KVARH(+) TOT		KVARH(+) TOT
87	32			15			KWH(+) PAR				KWH(+) PAR		KWH(+) PAR		KWH(+) PAR
88	32			15			KVARH(+) PAR				KVARH(+) PAR		KVARH(+) PAR		KVARH(+) PAR
89	32			15			KWH(+) L1				KWH(+) L1		KWH(+) L1		KWH(+) L1
90	32			15			KWH(+) L2				KWH(+) L2		KWH(+) L2		KWH(+) L2
91	32			15			KWH(+) L3				KWH(+) L3		KWH(+) L3		KWH(+) L3
92	32			15			KWH(+) T1				KWH(+) T1		KWH(+) T1		KWH(+) T1
93	32			15			KWH(+) T2				KWH(+) T2		KWH(+) T2		KWH(+) T2
94	32			15			KWH(+) T3				KWH(+) T3		KWH(+) T3		KWH(+) T3
95	32			15			KWH(+) T4				KWH(+) T4		KWH(+) T4		KWH(+) T4
96	32			15			KVARH(+) T1				KVARH(+) T1		KVARH(+) T1		KVARH(+) T1
97	32			15			KVARH(+) T2				KVARH(+) T2		KVARH(+) T2		KVARH(+) T2
98	32			15			KVARH(+) T3				KVARH(+) T3		KVARH(+) T3		KVARH(+) T3
99	32			15			KVARH(+) T4				KVARH(+) T4		KVARH(+) T4		KVARH(+) T4

100	32			15			KWH(-) TOT				KWH(-) TOT		KWH(-) TOT		KWH(-) TOT
101	32			15			KVARH(-) TOT				KVARH(-) TOT		KVARH(-) TOT		KVARH(-) TOT
102	32			15			HOUR					HOUR			HEURE			HEURE
103	32			15			COUNTER 1				COUNTER 1		COMPTEUR 1		COMPTEUR 1
104	32			15			COUNTER 2				COUNTER 2		COMPTEUR 2		COMPTEUR 2
105	32			15			COUNTER 3				COUNTER 3		COMPTEUR 3		COMPTEUR 3

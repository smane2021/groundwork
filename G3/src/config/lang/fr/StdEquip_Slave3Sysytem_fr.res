﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			System Voltage				System Voltage		Tension Système			Tension Système
2	32			15			Rectifiers Number			Num of G3Rect		Nombre de redressseurs		Nb. Redresseur
3	32			15			Total Rectifiers Current					Total Rect Curr			Courant Total Redresseur	I Total Red.
4	32			15			Rectifier Lost				Rectifier Lost		Perte Redresseur		Perte Red.
5	32			15			All Rectifiers Comm Fail		AllRectCommFail		Aucune Réponse Redresseur	Aucun.Répon.Red 
6	32			15			Comm Fail					Comm Fail		Défaut Communication		Défaut Comm.
7	32			15			Existence State				Existence State		Détection			Détection
8	32			15			Existent				Existent		Présent				Présent	
9	32			15			Not Existent						Not Existent		Non Présent			Non Présent
10	32			15			Normal					Normal			Normal				Normal
11	32			15			Fail						Fail			Défaut				Défaut
12	32			15			Rectifier Current Limit					Current Limit		Limitation Courant Redresseur	Limit. I Red.
13	32			15			Rectifiers Trim				Rect Trim		Contrôle tension Redresseur	Contrôle U Red
14	32			15			DC On/Off Control			DC On/Off Ctrl		Contrôle Etage DC Red		Contr.Et DC Red
15	32			15			AC On/Off Control			AC On/Off Ctrl		Contrôle Etage AC Red		Contr.Et AC Red
16	32			15			G3 Rectifiers LED Control		Rects LED Ctrl		Contrôle LEDs			Contrôle LEDs
17	32			15			Switch Off All				Switch Off All		Tous sur arrêt			Arrêt Tous Red.
18	32			15			Switch On All				Switch On All		Tous en fonction		Tous fonction
19	32			15			All					All		Clignotante			Clignotante
20	32			15			Stop Flashing						Stop Flashing		Fixe				Fixe
21	32			32			Current Limit Control					Curr Limit Ctrl		Control Limitation courant	Cont.Lim.I
22	32			32			Full Capability Control					Full Capab Ctrl		Puisssance maximum		Puis. Maximum
23	32			15			G3 Clear Rectifier Lost Alarm		G3 ClrRectLost			Reset Perte Redresseur		Reset Perte Red
24	32			15			G3 Reset Cycle Alarm			G3 ClrCycleAlm		Reset Oscill Redondance Red	Reset Oscill
25	32			15			Yes					Yes			Effacement			Effacement
26	32			15			Rectifier Group III			Rect Group III		Groupe Redresseur III		Groupe Red. III
27	32			15			E-Stop Function				E-Stop Function		Fonction Arrêt d'Urgence	Arrêt Urgence
36	32			15			Normal					Normal			Normal				Normal
37	32			15			Failure					Failure			Défaut				Défaut
38	32			15			Switch Off All				Switch Off All		Tous sur arrêt			Arrêt Tous Red.
39	32			15			Switch On All				Switch On All		Tous en fonction		Tous fonction
83	32			15			No					No			Non				Non
84	32			15			Yes					Yes			Oui				Oui
96	32			15			Input Current Limit			Input Curr Lmt		Limitation Courant d'Entrée	Limit I Entrée
97	32			15			Mains Failure				Mains Failure		Défaut Secteur			Défaut Secteur
98	32			15			G3 Clear Rect Comm Fail Alarm		G3 ClrCommFail		Init defaut com redresseur	InitDefComRed     
99	32			15			Rectifier Capacity Used			Rect Cap Used		Capacité de red utilisée			Cap Rect util	
100	32			15			Maximum Used Capacity			Max Cap Used		Capacité maximum utilisée			CapacMaxiUtilis	
101	32			15			Minimum Used Capacity			Min Cap Used		Capacité minimum utilisée			CapacMiniUtilis	
102	32			15			Total Rated Current		Total Rated Cur		Courant nominal total		CourantNomTotal
103	32			15			Total Rectifiers Communicating		Num Rects Comm		Totale redresseurs communication	QteTotRedEnCom    
104	32			15			Rated Voltage				Rated Voltage		Tension de repli			Tension repli
105	32			15			G3 Fan Speed Control			Fan Speed Ctrl		Controle ventilateurs			Controle Ventl	
106	32			15			Full Speed				Full Speed		Vitesse Max des ventilateurs				Vit.Max.Ventil	
107	32			15			Automatic Speed				Auto Speed		Vitesse Auto			Vitesse Auto	
108	32			15			G3 Confirm Rectifier ID/Feed		Confirm ID/Feed		Confirm Position Phase/Red		Confirm ID/PH	
109	32			15			Yes					Yes			Oui				Oui	
110	32			15			Multiple Rectifiers Fail		Multi-Rect Fail		Multi Redresseur en défaut		Multi Déf Red              
111	32			15			Total Output Power			OutputPower		Puissance de sortie totale			W - TotalSortie
112	32			15			G3 Reset Rectifier IDs			G3 ResetRectIDs		Reset G3 Redresseur IDs		Reset G3RedrIDs

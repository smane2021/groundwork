﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			SMTemp5 Temperature 1			SMTemp5 Temp1	SMTemp5 Temperature 1			SMTemp5 Temp1
2	32			15			SMTemp5 Temperature 2			SMTemp5 Temp2	SMTemp5 Temperature 2			SMTemp5 Temp2
3	32			15			SMTemp5 Temperature 3			SMTemp5 Temp3	SMTemp5 Temperature 3			SMTemp5 Temp3
4	32			15			SMTemp5 Temperature 4			SMTemp5 Temp4	SMTemp5 Temperature 4			SMTemp5 Temp4
5	32			15			SMTemp5 Temperature 5			SMTemp5 Temp5	SMTemp5 Temperature 5			SMTemp5 Temp5
6	32			15			SMTemp5 Temperature 6			SMTemp5 Temp6	SMTemp5 Temperature 6			SMTemp5 Temp6
7	32			15			SMTemp5 Temperature 7			SMTemp5 Temp7	SMTemp5 Temperature 7			SMTemp5 Temp7
8	32			15			SMTemp5 Temperature 8			SMTemp5 Temp8	SMTemp5 Temperature 8			SMTemp5 Temp8
9	32			15			Temperature Probe 1 Status		Probe1 Status		Etat sonde Temperature 1		Etat Temp T1 
10	32			15			Temperature Probe 2 Status		Probe2 Status		Etat sonde Temperature 2		Etat Temp T2
11	32			15			Temperature Probe 3 Status		Probe3 Status		Etat sonde Temperature 3		Etat Temp T3
12	32			15			Temperature Probe 4 Status		Probe4 Status		Etat sonde Temperature 4		Etat Temp T4
13	32			15			Temperature Probe 5 Status		Probe5 Status		Etat sonde Temperature 5		Etat Temp T5
14	32			15			Temperature Probe 6 Status		Probe6 Status		Etat sonde Temperature 6		Etat Temp T6
15	32			15			Temperature Probe 7 Status		Probe7 Status		Etat sonde Temperature 7		Etat Temp T7
16	32			15			Temperature Probe 8 Status		Probe8 Status		Etat sonde Temperature 8		Etat Temp T8

17	32			15			Normal					Normal			Normal					Normal
18	32			15			Shorted					Shorted			Court circuit				Court circuit
19	32			15			Open					Open			Ouverte					Ouverte
20	32			15			Not Installed				Not Installed		Pas installée				Pas installée

21	32			15			Module ID Overlap			ID Overlap	Chevauchement ID module			Err ID module
22	32			15			AD Converter Failure			AD Conv Fail		Erreur adresse Convertisseur		Err ad Convert
23	32			15			SMTemp EEPROM Failure			EEPROM Fail		SMTemp Erreur EEPROM			Err EEPROM

24	32			15			Communication Fail			Comm Fail		Pas de Réponse			Pas de Réponse
25	32			15			Existence State				Exist State		Etat Présent				Etat Présent
26	32			15			Communication Fail			Comm Fail		Pas de Réponse			Pas de Réponse

27	32			15			Temperature Probe 1 Shorted		Probe1 Short		Court circuit Sonde T1			Court CircuitT1
28	32			15			Temperature Probe 2 Shorted		Probe2 Short		Court circuit Sonde T2			Court CircuitT2
29	32			15			Temperature Probe 3 Shorted		Probe3 Short		Court circuit Sonde T3			Court CircuitT3
30	32			15			Temperature Probe 4 Shorted		Probe4 Short		Court circuit Sonde T4			Court CircuitT4
31	32			15			Temperature Probe 5 Shorted		Probe5 Short		Court circuit Sonde T5			Court CircuitT5
32	32			15			Temperature Probe 6 Shorted		Probe6 Short		Court circuit Sonde T6			Court CircuitT6
33	32			15			Temperature Probe 7 Shorted		Probe7 Short		Court circuit Sonde T7			Court CircuitT7
34	32			15			Temperature Probe 8 Shorted		Probe8 Short		Court circuit Sonde T8			Court CircuitT8

35	32			15			Temperature Probe 1 Open		Probe1 Open		Sonde T1 Ouverte			T1 Ouverte
36	32			15			Temperature Probe 2 Open		Probe2 Open		Sonde T2 Ouverte			T2 Ouverte
37	32			15			Temperature Probe 3 Open		Probe3 Open		Sonde T3 Ouverte			T3 Ouverte
38	32			15			Temperature Probe 4 Open		Probe4 Open		Sonde T4 Ouverte			T4 Ouverte
39	32			15			Temperature Probe 5 Open		Probe5 Open		Sonde T5 Ouverte			T5 Ouverte
40	32			15			Temperature Probe 6 Open		Probe6 Open		Sonde T6 Ouverte			T6 Ouverte
41	32			15			Temperature Probe 7 Open		Probe7 Open		Sonde T7 Ouverte			T7 Ouverte
42	32			15			Temperature Probe 8 Open		Probe8 Open		Sonde T8 Ouverte			T8 Ouverte

43	32			15			SMTemp 5				SMTemp 5		SMTemp 5				SMTemp 5
44	32			15			Abnormal				Abnormal		Défaut					Défaut

45	32			15			Clear					Clear			Effacer					Effacer
46	32			15			Clear Probe Alarm			Clr Probe Alm		Effacement alarme sonde			Eff Al sonde

51	32			15			Temperature 1 Assign Equipment		T1 Assign Equip	Utilisation Sonde Temp 1		Util Temp T1
54	32			15			Temperature 2 Assign Equipment		T2 Assign Equip	Utilisation Sonde Temp 2		Util Temp T2
57	32			15			Temperature 3 Assign Equipment		T3 Assign Equip	Utilisation Sonde Temp 3		Util Temp T3
60	32			15			Temperature 4 Assign Equipment		T4 Assign Equip	Utilisation Sonde Temp 4		Util Temp T4
63	32			15			Temperature 5 Assign Equipment		T5 Assign Equip	Utilisation Sonde Temp 5		Util Temp T5
66	32			15			Temperature 6 Assign Equipment		T6 Assign Equip	Utilisation Sonde Temp 6		Util Temp T6
69	32			15			Temperature 7 Assign Equipment		T7 Assign Equip	Utilisation Sonde Temp 7		Util Temp T7
72	32			15			Temperature 8 Assign Equipment		T8 Assign Equip	Utilisation Sonde Temp 8		Util Temp T8

150	32			15			None					None			Aucune					Aucune
151	32			15			Ambient					Ambient			Ambiance				Ambiance
152	32			15			Battery					Battery			Batterie				Batterie

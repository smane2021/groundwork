﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support           
# FULL_IN_LOCALE2: Full name in locale2 language 
# ABBR_IN_LOCALE2: Abbreviated locale2 name      
#
[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID	MAX_LEN_FULL	MAX_LEN_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32		15		Battery Voltage				Battery Voltage		Tension batterie			Tension bat
2	32		15		Total Battery Current			Total Batt Curr		Courant batterie Total			I bat Total
3	32		15		Battery Temperature			Battery Temp		Temperature batterie			Temp. bat
4	32		15		Short BOD Total Time			Short BOD Time		Durée TotalDecharge peu Profonde	Dech. peu Prof.
5	32		15		Long BOD Total Time			Long BOD Time		Duréee Total Decharge Profonde		Dech. Prof.
6	32		15		Full BOD Total Time			Full BOD Time		Durée decharge Totale			Durée Dech.Tot.
7	32		15		Short BOD Counter			ShortBODCounter		Compt. Decharge peu Profonde		Nb Dech.peuProf
8	32		15		Long BOD Counter			LongBODCounter		Compt. Decharge Profonde		Nb Dech. Prof.
9	32		15		Full BOD Counter			FullBODCounter		Compt. Decharge Totale			Nb Dech. Totale
14	32		15		End Test on Voltage			End Test Volt		Test tension d arret			Test tens.arret 
15	32		15		Discharge Current Imbalance		Dsch Curr Imb		Desequilibre courant decharge		Desequil Idech
19	32		15		Abnormal Battery Current		AbnormalBatCurr			Erreur courant batterie		Err courant bat
21	32		15		System Current Limit Active		SystemCurrLimit	Limitation de IBat Active		Lim. IBat Acti.
23	32		15		Equalize/Float Charge Control		EQ/FLT Control		Mode Boost/Floating Charge		ModeBoost/Float
25	32		15		Battery Test Control			BattTestControl		Test Batterie				test Bat
30	32		15		Number of Battery Blocks		Num Batt Blocks		Nombre de blocs batterie		Nbr blocs batt
31	32		15		End Test Time				End Test Time		Durée max de test batterie		Durée max test
32	32		15		End Test Voltage			End Test Volt		Tension fin de test batterie		Tension fintest
33	32		15		End Test Capacity			EndTestCapacity		Capacite fin de test batterie		Capa. fintest
34	32		15		Constant Current Test			Const Curr Test		Validation test courant constant	Test I const 
35	32		15		Constant Current Test Current		ConstCurrT Curr		Courant constant			I constant
37	32		15		AC Fail Test				AC Fail Test		Validation test abs secteur		Test abs AC
38	32		15		Short Test				Short Test		Validation test court			Test court
39	32		15		Short Test Cycle			ShortTest Cycle		Test court. periodicite			Tst court perio
40	32		15		Short Test Max Difference Curr		Max Diff Curr		Test court.Variation max courant	Dif Imax Tcourt
41	32		15		Short Test Time				Short Test Time		Durée du test court			Durée tst court
42	32		15		Float Charge Voltage			Float Voltage		Tension floating			Tens. floating
43	32		15		Equalize Charge Voltage			EQ Voltage		Tension egalisation			Tens.egalisat

44	32		15		Maximum Equalize Charge Time		Maximum EQ Time		Durée maximum en egalisation		Durée max egali
45	32		15		Equalize Stop Current			EQ Stop Curr		Egalisation. Courant mini.		Imin egalis	
46	32		15		Equalize Stop Delay Time		EQ Stop Delay		Egalisation. Durée/courant mini		Durée Imin egal
47	32		15		Automatic Equalize			Auto EQ			Charge en egalisation auto		Egalisa auto
48	32		15		Equalize Start Current			EQ Start Curr		Courant. Passage float-egalis		I Float-Egalise
49	32		15		Equalize Start Capacity			EQ Start Cap		Capacite. Passage float-egalis		C.Float-Egalise
50	32		15		Cyclic Equalize				Cyc EQ		Egalisation periodique. Valid		Valid Egal Per	
51	32		15		Cyclic Equalize Interval		Cyc EQ Interval		Egalisation Periodicite			Periode egalis	
52	32		15		Cyclic Equalize Duration	Cyc EQ Duration		Egalisation periodique. Durée		Durée Egal Per
53	32		20		Temperature Compensation Center		TempComp Center		TC nominale de compensat		Tnom Compensat	
54	32		15		Temp Comp Coefficient (slope)		Temp Comp Coeff		Coefficient de temperature		Coef.Comp. TC
55	32		15		Battery Current Limit			Batt Curr Limit		Limitation courant batterie		Lim I batterie
56	32		15		Battery Type Number			Batt Type Num		Type batterie No.			Type batt No
57	32		15		Rated Capacity				Rated Capacity		Capacite batterie installee		Cap Batt instal	
58	32		15		Charging Efficiency			Charging Eff		Coefficent capacite			Coef capacite
59	32		15		Time to 0.1C10 Discharge Current	Time 0.1C10		Courbe de dech. Tps a 0.1C10		TpsDechar0.1C10
60	32		15		Time to 0.2C10 Discharge Current	Time 0.2C10		Courbe de dech. Tps a 0.2C10		TpsDechar0.2C10
61	32		15		Time to 0.3C10 Discharge Current	Time 0.3C10		Courbe de dech. Tps a 0.3C10		TpsDechar0.3C10
62	32		15		Time to 0.4C10 Discharge Current	Time 0.4C10		Courbe de dech. Tps a 0.4C10		TpsDechar0.4C10
63	32		15		Time to 0.5C10 Discharge Current	Time 0.5C10		Courbe de dech. Tps a 0.5C10		TpsDechar0.5C10
64	32		15		Time to 0.6C10 Discharge Current	Time 0.6C10		Courbe de dech. Tps a 0.6C10		TpsDechar0.6C10
65	32		15		Time to 0.7C10 Discharge Current	Time 0.7C10		Courbe de dech. Tps a 0.7C10		TpsDechar0.7C10
66	32		15		Time to 0.8C10 Discharge Current	Time 0.8C10		Courbe de dech. Tps a 0.8C10		TpsDechar0.8C10
67	32		15		Time to 0.9C10 Discharge Current	Time 0.9C10		Courbe de dech. Tps a 0.9C10		TpsDechar0.9C10
68	32		15		Time to 1.0C10 Discharge Current	Time 1.0C10		Courbe de dech. Tps a 1.0C10		TpsDechar1.0C10
70	32		15		Temperature Sensor Failure		TempSensorFail		Defaut capteur temperature batt		Def captr T°Bat
71	32		15		High Temperature 1			High Temp 1		Temperature 1 Haute			Temp 1. Haute
72	32		15		High Temperature 2			High Temp 2		Temperature 2 Haute			Temp 2. Haute
73	32		15		Low Temperature				Low Temperature		Temperature Basse			Temp. Basse
74	32		15		Planned Battery Test Running		PlanBattTestRun		Test Batterie Auto en cours		Tst bat cours
77	32		15		Short Battery Test Running		ShortBatTestRun		Test Court batterie en cours		TstCourt.cours
81	32		15		Automatic Equalize			Auto EQ				Charge egalisation automatique		Egalis auto
83	32		15		Abnormal Battery Current		Abnl Batt Curr		Courant batterie anormal		I bat. anormal
84	32		15		Temperature Compensation Active		TempComp Active		Compensation temperature active		Comp T°C active
85	32		15		Battery Current Limit Active		Batt Curr Limit	Limitation courant bat. active		Lim Ibat active
86	32		15		Battery Charge Prohibited Alarm		BattChgProhiAlm		Alarme bat. charge interdite		ChargeInterdite
87	32		15		No					No			Non					Non
88	32		15		Yes					Yes			Oui					Oui
90	32		15		None					None			Aucune					Aucune
91	32		15		Temperature 1				Temperature 1		température1			Temp1
92	32		15		Temperature 2				Temperature 2		température2			Temp2
93	32		15		Temperature 3				Temperature 3		température3			Temp3
94	32		15		Temperature 4				Temperature 4		température4			Temp4
95	32		15		Temperature 5				Temperature 5		température5			Temp5
96	32		15		Temperature 6				Temperature 6		température6			Temp6
97	32		15		Temperature 7				Temperature 7		température7			Temp7
98	32		15		0					0			0					0
99	32		15		1					1			1					1
100	32		15		2					2			2					2
113	32		15		Float Charge				Float Charge		Charge en floating		Floating
114	32		15		Equalize Charge				EQ Charge		Charge en egalisation		Egalisation
121	32		15		Disabled				Disabled			Desactive			Desactive
122	32		15		Enabled					Enabled			Active				Active
136	32		15		Record Threshold			RecordThreshold		Seuil enregistrement		Seuil enregistr
137	32		15		Remaining Time				Remaining		Estimation Autonomie Bat	Estim. AutoBat
138	32		15		Battery Management State		Batt Management		Etat de la batterie		Etat batterie
139	32		15		Float Charge				Float Charge		Charge en floating		Charge floating
140	32		15		Short Test				Short Test		Test court			Test court
141	32		15		Equalize for Test			EQ for Test		Test egalisation		Test egalis.
142	32		15		Manual Test				Manual Test		Test manuel			test manuel
143	32		15		Planned Test				Planned Test		Test planifie			Test plan.
144	32		15		AC Fail Test				AC Fail Test		Decharge batterie		Decharge batt
145	32		15		AC Fail					AC Fail			Defaut Abs Secteur		Def Abs Secteur
146	32		15		Manual Equalize				Manual EQ		Manuel egalisation		Manuel egalis.
147	32		15		Auto Equalize				Auto EQ		Auto egalisation		Auto egalis.
148	32		15		Cyclic Equalize		Cyclic EQ			Egalisation Periodique		Egalis. Perio.
152	32		15		Over Current Limit			Over Curr Lmt		Seuil Sur-intensite		Seuil Sur-I
153	32		15		Stop Battery Test			Stop Batt Test		Arret test batterie		Arret test bat
154	32		15		Battery Group				Battery Group		Groupe batterie			Grp batterie
157	32		15		Master Battery Test			Master Bat Test	Batterie maître en test		Bat maît.en tst
158	32		15		Master Equalize			Master EQ		Maître en egalisation			Maître egalis
165	32		15		Test Voltage Level			Test Volt Level			Tension niveau test		Tension niv tst
166	32		15		Bad Battery				Bad Battery		Batterie défectueuse		Défaut batterie
168	32		15		Clear Bad Battery Alarm			Clr Bad Bat Alm		Acquit défaut batterie		Acq défaut bat.
170	32		15		Cyclic Equalize Start Time		Cyc EQ Time		Temps de debut cyclique BC	Temps cyclique
171	32		15		Valid EQ Enable				Valid EQ		Valide BC activer		Valide BC
172	32		15		Start Battery Test			Start Batt Test		Démarrage test batterie		Lance test batt
173	32		15		Stop					Stop			Arrêt				Arrêt
174	32		15		Start					Start			Démarrage			Démarrage
175	32		15		Number of Planned Tests per Year	Planned Tests		Nbr de Test Plannifie par An	Nb Test Prog/An
176	32		15		Planned Test 1 (MM-DD Hr)		Test1 (M-D Hr)		Test Plannifie 1(M-D H)		Test 1(M-D H)
177	32		15		Planned Test 2 (MM-DD Hr)		Test2 (M-D Hr)		Test Plannifie 2(M-D H)		Test 2(M-D H)  
178	32		15		Planned Test 3 (MM-DD Hr)		Test3 (M-D Hr)		Test Plannifie 3(M-D H)		Test 3(M-D H)  
179	32		15		Planned Test 4 (MM-DD Hr)		Test4 (M-D Hr)		Test Plannifie 4(M-D H)		Test 4(M-D H)
180	32		15		Planned Test 5 (MM-DD Hr)		Test5 (M-D Hr)		Test Plannifie 5(M-D H)		Test 5(M-D H)
181	32		15		Planned Test 6 (MM-DD Hr)		Test6 (M-D Hr)		Test Plannifie 6(M-D H)		Test 6(M-D H)
182	32		15		Planned Test 7 (MM-DD Hr)		Test7 (M-D Hr)		Test Plannifie 7(M-D H)		Test 7(M-D H)
183	32		15		Planned Test 8 (MM-DD Hr)		Test8 (M-D Hr)		Test Plannifie 8(M-D H)		Test 8(M-D H)
184	32		15		Planned Test 9 (MM-DD Hr)		Test9 (M-D Hr)		Test Plannifie 9(M-D H)		Test 9(M-D H)
185	32		15		Planned Test 10 (MM-DD Hr)		Test10 (M-D Hr)		Test Plannifie 10(M-D H)	Test 10(M-D H)
186	32		15		Planned Test 11 (MM-DD Hr)		Test11 (M-D Hr)		Test Plannifie 11(M-D H)	Test 11(M-D H)
187	32		15		Planned Test 12 (MM-DD Hr)		Test12 (M-D Hr)		Test Plannifie 12(M-D H)	Test 12(M-D H)
188	32		15		Reset Battery Capacity			Reset Batt Cap		RAZ Capacite Batterie		RAZ Capa Bat
191	32		15		Clear Abnormal Bat Current Alarm	Clr AbnlCur Alm			RAZ Alarme Anormale I Batterie		RAZ Alr I Bat
192	32		15		Clear Discharge Curr Imbalance		Clr Cur Imb Alm			RAZ Déséquilibrage Courant	RAZ DéséqI
193	32		15		Expected Current Limit			ExpectedCurrLmt		Limitation Courant Prevue	Lim I Prevue
194	32		15		Battery Test Running			BatTestRunning		Test Batterie  en cours		Tst Bat cours
195	32		15		Low Capacity Point			Low Capacity Pt			Seuil Capacite Basse		Capacite Basse
196	32		15		Battery Discharge			Battery Disch		Decharge Batterie		Decharge Bat.
197	32		15		Over Voltage				Over Voltage		Sur Tension			Sur Tension
198	32		15		Low Voltage				Low Voltage		Sous Tension			Sous Tension
200	32		15		Number of Battery Shunts		Num Batt Shunts		Nombre de shunts batteries	Nb shunts bat
201	32		15		Imbalance Protection			Imb Protection		Protect. Desequil. I batterie	Prot.Deseq.IBat
202	32		15		Temp Compensation Probe Number		TempComp Sensor		Sonde Temp pour Compensation	Sond.Temp.Comp.
203	32		15		EIB Battery Number			EIB Battery Num			Nb Carte Batterie		No Cart.Bat.
204	32		15		Normal					Normal			Normal				Normal
205	32		15		Special for NA				Special for NA			Special for NA			Special
206	32		15		Battery Voltage Type			Batt Volt Type		Tension Bloc Batterie		V Bloc Bat.
207	32		15		Voltage on Very High Batt Temp		VoltVHiBatTemp		Vbat température très haute	Vbat Haute Temp
#208	32		15		Current Limited				Current Limited		Activation Limit Courant	Activ.Limit. Ibat
209	32		15		Battery Temperature Probe Number	BatTempProbeNum		Sonde Temperature Compensation		Sond.Temp.Comp
212	32		15		Action on Very High Battery Temp	Action on VHBT		Action Sur Temp Tres Haute	Act.Temp.Tres H
213	32		15		Disabled				Disabled		Désactive			Désactive
214	32		15		Lowering Voltage			Lowering Volt		Diminution de la Tension	Diminut. VFloat
215	32		15		Disconnection	Disconnection		Deconnecté		Deconnec.
216	32		15		Reconnection Temperature		ReconnectTemp		Temperature de reconnexion	Temp.Recon.
217	32		15		Very Hi Temp Volt Setpoint (24V)	VHigh Temp Volt		Tension Tres Haute Temp(24V)		VTresHTemp(24V)
218	32		15		Float Charge Voltage (24V)		Float Voltage		Tension Floating (24V)		V Float. (24V)
219	32		15		Equalize Charge Voltage (24V)		EQ Voltage		Tension Egualisation (24V)		V Egual. (24V)
220	32		15		Test Voltage Limit (24V)		Test Volt Lmt		Tension Test Bat (24V)		V Tst Bat.(24V)
221	32		15		Test End Voltage (24V)			Test End Volt		Tension Fin Test Bat. (24V)	V Fin Ts(24V)
222	32		15		Current Limited				Current Limited			Arret Limitation Courant Bat	Arret Lim.I Bat
223	32		15		Batt Voltage for North America		Batt Volt NA		Tension Bat pour Norteamérica		TensBat USA
224	32		15		Battery Changed				Battery Changed		Remplacement batteries			Rempl.Batt.
225	32		15		Battery Test Lowest Capacity		BattTestLowCap	Capacitéminimum pour test bat		Capa.MinTestBat
226	32		15		Temperature 8				Temperature 8			Température8				Température8
227	32		15		Temperature 9				Temperature 9			Température9				Température9
228	32		15		Temperature 10				Temperature 10			Température10				Température10
229	32		15		LargeDU Rated Capacity			Rated Capacity		CapacitéLargeUD			Capa.LargeUD
230	32		15		Clear Battery Test Fail Alarm		ClrBatTestFail		RAZ Test Bat Négatif			RAZTestBatNég
231	32		15		Battery Test Fail			BatteryTestFail		Test Batterie Négatif			Test Bat.Nég.
232	32		15		Maximum					Maximum				Max					Max
233	32		15		Average					Average			Moyenne					Moyenne
234	32		15		Average SMBRC				Average SMBRC		Moyenne SMBRC				Moyenne SMBRC
235	32		15		Compensation Temperature		Comp Temp		Température Batterie			Temp.Batt.
236	32		15		Comp Temp High1			Comp Temp Hi1		Seuil Haut Temp1 Bat			Seuil H.Temp1.
237	32		15		Comp Temp Low				Comp Temp Low		Seuil Bas Temp Bat			Seuil B.Temp.
238	32		15		Comp Temp High1			Comp Temp Hi1		Seuil Haut Temp1 Bat			Seuil H.Temp1.
239	32		15		Comp Temp Low				Comp Temp Low		Seuil Bas Temp Bat			Seuil B.Temp.
240	32		15		Comp Temp High2			Comp Temp Hi2		Seuil Haut Temp2 Bat			Seuil H.Temp2.
241	32		15		Comp Temp High2			Comp Temp Hi2		Seuil Haut Temp2 Bat			Seuil H.Temp2.
242	32		15		Compensation Sensor Fault		CompTempFail		Défaut Capteur Temp.			Déf.Capt.Temp.
243	32		15		Calculate Battery Current		Calc Batt Curr		Calcul Courant Batterie			Calcul I Batt.

244	32		15		Start Equalize Charge (for EEM)	Star EQ(EEM)		Charge en egalisation(EEM)		Egalisa.(EEM)    
245	32		15		Start Float Charge (for EEM)	Star FLT(EEM)		Charge en floating(EEM)			Floating(EEM)    
246	32		15		Start Resistance Test(for EEM)		StartTest(EEM)		Start Résistance Test(EEM)		StartTest(EEM)  
247	32		15		Stop Resistance Test(for EEM)		Stop Test(EEM)		Stop Résistance Test(EEM)		StopTest(EEM)  
248	32		15		Reset Batt Cap(Internal Use)	ResetCap(Int)	RAZ Capacite Batterie(Internal)		RAZCapaBat(INT)
249	32		15		Reset Battery Capacity(for EEM)		ResetCap(EEM)	RAZ Capacite Batterie(EEM)		RAZCapaBat(EEM)
250	32		15		Clear Bad Battery Alarm(for EEM)	ClrAlm(EEM)	Acquit défaut batterie(EEM)		Acqdéf.bat(EEM)	

251	32		15		System Temp1		System T1		Système T1				Système T1 
252	32		15		System Temp2		System T2		Système T2				Système T2 
253	32		15		System Temp3		System T3	Système T3				Système T3
254	32		15		IB2-1 Temp1		IB2-1 T1		IB2-1 Température 1			IB2-1-Temp1
255	32		15		IB2-1 Temp2		IB2-1 T2		IB2-1 Température 2			IB2-1-Temp2
256	32		15		EIB-1 Temp1		EIB-1 T1		EIB-1 Température 1			EIB-1-Temp1
257	32		15		EIB-1 Temp2		EIB-1 T2		EIB-1 Température 2			EIB-1-Temp2
258	32		15		SMTemp1 Temp1				SMTemp1 T1		SMTemp1-T1				SMTemp1-T1
259	32		15		SMTemp1 Temp2				SMTemp1 T2		SMTemp1-T2				SMTemp1-T2
260	32		15		SMTemp1 Temp3				SMTemp1 T3		SMTemp1-T3				SMTemp1-T3
261	32		15		SMTemp1 Temp4				SMTemp1 T4		SMTemp1-T4				SMTemp1-T4
262	32		15		SMTemp1 Temp5				SMTemp1 T5		SMTemp1-T5				SMTemp1-T5
263	32		15		SMTemp1 Temp6				SMTemp1 T6		SMTemp1-T6				SMTemp1-T6
264	32		15		SMTemp1 Temp7				SMTemp1 T7		SMTemp1-T7				SMTemp1-T7
265	32		15		SMTemp1 Temp8				SMTemp1 T8		SMTemp1-T8				SMTemp1-T8
266	32		15		SMTemp2 Temp1				SMTemp2 T1		SMTemp2-T1				SMTemp2-T1
267	32		15		SMTemp2 Temp2				SMTemp2 T2		SMTemp2-T2				SMTemp2-T2
268	32		15		SMTemp2 Temp3				SMTemp2 T3		SMTemp2-T3				SMTemp2-T3
269	32		15		SMTemp2 Temp4				SMTemp2 T4		SMTemp2-T4				SMTemp2-T4
270	32		15		SMTemp2 Temp5				SMTemp2 T5		SMTemp2-T5				SMTemp2-T5
271	32		15		SMTemp2 Temp6				SMTemp2 T6		SMTemp2-T6				SMTemp2-T6
272	32		15		SMTemp2 Temp7				SMTemp2 T7		SMTemp2-T7				SMTemp2-T7
273	32		15		SMTemp2 Temp8				SMTemp2 T8		SMTemp2-T8				SMTemp2-T8
274	32		15		SMTemp3 Temp1				SMTemp3 T1		SMTemp3-T1				SMTemp3-T1
275	32		15		SMTemp3 Temp2				SMTemp3 T2		SMTemp3-T2				SMTemp3-T2
276	32		15		SMTemp3 Temp3				SMTemp3 T3		SMTemp3-T3				SMTemp3-T3
277	32		15		SMTemp3 Temp4				SMTemp3 T4		SMTemp3-T4				SMTemp3-T4
278	32		15		SMTemp3 Temp5				SMTemp3 T5		SMTemp3-T5				SMTemp3-T5
279	32		15		SMTemp3 Temp6				SMTemp3 T6		SMTemp3-T6				SMTemp3-T6
280	32		15		SMTemp3 Temp7				SMTemp3 T7		SMTemp3-T7				SMTemp3-T7
281	32		15		SMTemp3 Temp8				SMTemp3 T8		SMTemp3-T8				SMTemp3-T8
282	32		15		SMTemp4 Temp1				SMTemp4 T1		SMTemp4-T1				SMTemp4-T1
283	32		15		SMTemp4 Temp2				SMTemp4 T2		SMTemp4-T2				SMTemp4-T2
284	32		15		SMTemp4 Temp3				SMTemp4 T3		SMTemp4-T3				SMTemp4-T3
285	32		15		SMTemp4 Temp4				SMTemp4 T4		SMTemp4-T4				SMTemp4-T4
286	32		15		SMTemp4 Temp5				SMTemp4 T5		SMTemp4-T5				SMTemp4-T5
287	32		15		SMTemp4 Temp6				SMTemp4 T6		SMTemp4-T6				SMTemp4-T6
288	32		15		SMTemp4 Temp7				SMTemp4 T7		SMTemp4-T7				SMTemp4-T7
289	32		15		SMTemp4 Temp8				SMTemp4 T8		SMTemp4-T8				SMTemp4-T8
290	32		15		SMTemp5 Temp1		SMTemp5 T1	SMTemp5-T1				SMTemp5-T1
291	32		15		SMTemp5 Temp2		SMTemp5 T2	SMTemp5-T2				SMTemp5-T2
292	32		15		SMTemp5 Temp3		SMTemp5 T3	SMTemp5-T3				SMTemp5-T3
293	32		15		SMTemp5 Temp4		SMTemp5 T4	SMTemp5-T4				SMTemp5-T4
294	32		15		SMTemp5 Temp5		SMTemp5 T5	SMTemp5-T5				SMTemp5-T5
295	32		15		SMTemp5 Temp6		SMTemp5 T6	SMTemp5-T6				SMTemp5-T6
296	32		15		SMTemp5 Temp7		SMTemp5 T7	SMTemp5-T7				SMTemp5-T7
297	32		15		SMTemp5 Temp8		SMTemp5 T8	SMTemp5-T8				SMTemp5-T8
298	32		15		SMTemp6 Temp1		SMTemp6 T1	SMTemp6-T1				SMTemp6-T1
299	32		15		SMTemp6 Temp2		SMTemp6 T2	SMTemp6-T2				SMTemp6-T2
300	32		15		SMTemp6 Temp3		SMTemp6 T3	SMTemp6-T3				SMTemp6-T3
301	32		15		SMTemp6 Temp4		SMTemp6 T4	SMTemp6-T4				SMTemp6-T4
302	32		15		SMTemp6 Temp5		SMTemp6 T5	SMTemp6-T5				SMTemp6-T5
303	32		15		SMTemp6 Temp6		SMTemp6 T6	SMTemp6-T6				SMTemp6-T6
304	32		15		SMTemp6 Temp7		SMTemp6 T7	SMTemp6-T7				SMTemp6-T7
305	32		15		SMTemp6 Temp8		SMTemp6 T8	SMTemp6-T8				SMTemp6-T8
306	32		15		SMTemp7 Temp1		SMTemp7 T1	SMTemp7-T1				SMTemp7-T1
307	32		15		SMTemp7 Temp2		SMTemp7 T2	SMTemp7-T2				SMTemp7-T2
308	32		15		SMTemp7 Temp3		SMTemp7 T3	SMTemp7-T3				SMTemp7-T3
309	32		15		SMTemp7 Temp4		SMTemp7 T4	SMTemp7-T4				SMTemp7-T4
310	32		15		SMTemp7 Temp5		SMTemp7 T5	SMTemp7-T5				SMTemp7-T5
311	32		15		SMTemp7 Temp6		SMTemp7 T6	SMTemp7-T6				SMTemp7-T6
312	32		15		SMTemp7 Temp7		SMTemp7 T7	SMTemp7-T7				SMTemp7-T7
313	32		15		SMTemp7 Temp8		SMTemp7 T8	SMTemp7-T8				SMTemp7-T8
314	32		15		SMTemp8 Temp1		SMTemp8 T1	SMTemp8-T1				SMTemp8-T1
315	32		15		SMTemp8 Temp2		SMTemp8 T2	SMTemp8-T2				SMTemp8-T2
316	32		15		SMTemp8 Temp3		SMTemp8 T3	SMTemp8-T3				SMTemp8-T3
317	32		15		SMTemp8 Temp4		SMTemp8 T4	SMTemp8-T4				SMTemp8-T4
318	32		15		SMTemp8 Temp5		SMTemp8 T5	SMTemp8-T5				SMTemp8-T5
319	32		15		SMTemp8 Temp6		SMTemp8 T6	SMTemp8-T6				SMTemp8-T6
320	32		15		SMTemp8 Temp7		SMTemp8 T7	SMTemp8-T7				SMTemp8-T7
321	32		15		SMTemp8 Temp8		SMTemp8 T8	SMTemp8-T8				SMTemp8-T8
322	32		15		Minimum			Minimum		Min					Min

#---------------------------------------12345678901234567890123456789012--------123456789012345---------12345678901234567890123456789012--------123456789012345
351	32		15		System Temp1 High 2	System T1 Hi2		Haute2 T1		Haute2 T1
352	32		15		System Temp1 High 1	System T1 Hi1		Haute1 T1		Haute1 T1
353	32		15		System Temp1 Low	System T1 Low		Basse T1			Basse Temp1
354	32		15		System Temp2 High 2	System T2 Hi2		Haute2 T2		Haute2 T2
355	32		15		System Temp2 High 1	System T2 Hi1		Haute1 T2		Haute1 T2
356	32		15		System Temp2 Low	System T2 Low		Basse T2			Basse Temp2
357	32		15		System Temp3 High 2	System T3 Hi2		Haute2 T3		Haute2 T3
358	32		15		System Temp3 High 1	System T3 Hi1		Haute1 T3		Haute1 T3
359	32		15		System Temp3 Low	System T3 Low		Basse T3		Basse T3 
360	32		15		IB2-1 Temp1 High 2	IB2-1 T1 Hi2		Haute2 IB2-1 T1		Haute2 IB2-1 T1
361	32		15		IB2-1 Temp1 High 1	IB2-1 T1 Hi1		Haute1 IB2-1 T1		Haute1 IB2-1 T1
362	32		15		IB2-1 Temp1 Low		IB2-1 T1 Low		IB2-1 Basse T1		IB2-1 Basse T1
363	32		15		IB2-1 Temp2 High 2	IB2-1 T2 Hi2		Haute2 IB2-1 T2		Haute2 IB2-1 T2
364	32		15		IB2-1 Temp2 High 1	IB2-1 T2 Hi1		Haute1 IB2-1 T2		Haute1 IB2-1 T2
365	32		15		IB2-1 Temp2 Low		IB2-1 T2 Low		IB2-1 Basse T2		IB2-1 Basse T2
366	32		15		EIB-1 Temp1 High 2	EIB-1 T1 Hi2		EIB-1 Haute2 T1		EIB-1 Haute2 T1
367	32		15		EIB-1 Temp1 High 1	EIB-1 T1 Hi1		EIB-1 Haute1 T1		EIB-1 Haute1 T1
368	32		15		EIB-1 Temp1 Low		EIB-1 T1 Low		EIB-1 Basse T1		EIB-1 Basse T1
369	32		15		EIB-1 Temp2 High 2	EIB-1 T2 Hi2		EIB-1 Haute2 T2		EIB-1 Haute2 T2
370	32		15		EIB-1 Temp2 High 1	EIB-1 T2 Hi1		EIB-1 Haute1 T2		EIB-1 Haute1 T2
371	32		15		EIB-1 Temp2 Low		EIB-1 T2 Low		EIB-1 Basse T2		EIB-1 Basse T2
372	32		15		SMTemp1 Temp1 High 2	SMTemp1 T1 Hi2		SMTemp1 Haute2 T1		SMTemp1 H2 T1
373	32		15		SMTemp1 Temp1 High 1	SMTemp1 T1 Hi1		SMTemp1 Haute1 T1		SMTemp1 H1 T1
374	32		15		SMTemp1 Temp1 Low	SMTemp1 T1 Low		SMTemp1 Basse T1		SMTemp1BasseT1
375	32		15		SMTemp1 Temp2 High 2	SMTemp1 T2 Hi2		SMTemp1 Haute2 T2		SMTemp1 H2 T2
376	32		15		SMTemp1 Temp2 High 1	SMTemp1 T2 Hi1		SMTemp1 Haute1 T2		SMTemp1 H1 T2
377	32		15		SMTemp1 Temp2 Low	SMTemp1 T2 Low		SMTemp1 Basse T2		SMTemp1BasseT2
378	32		15		SMTemp1 Temp3 High 2	SMTemp1 T3 Hi2	SMTemp1 Haute2 T3		SMTemp1 H2 T3
379	32		15		SMTemp1 Temp3 High 1	SMTemp1 T3 Hi1		SMTemp1 Haute1 T3		SMTemp1 H1 T3
380	32		15		SMTemp1 Temp3 Low	SMTemp1 T3 Low		SMTemp1 Basse T3		SMTemp1BasseT3
381	32		15		SMTemp1 Temp4 High 2	SMTemp1 T4 Hi2		SMTemp1 Haute2 T4		SMTemp1 H2 T4
382	32		15		SMTemp1 Temp4 High 1	SMTemp1 T4 Hi1		SMTemp1 Haute1 T4		SMTemp1 H1 T4
383	32		15		SMTemp1 Temp4 Low	SMTemp1 T4 Low		SMTemp1 Basse T4		SMTemp1BasseT4
384	32		15		SMTemp1 Temp5 High 2	SMTemp1 T5 Hi2		SMTemp1 Haute2 T5		SMTemp1 H2 T5
385	32		15		SMTemp1 Temp5 High 1	SMTemp1 T5 Hi1		SMTemp1 Haute1 T5		SMTemp1 H1 T5
386	32		15		SMTemp1 Temp5 Low	SMTemp1 T5 Low		SMTemp1 Basse T5		SMTemp1BasseT5
387	32		15		SMTemp1 Temp6 High 2	SMTemp1 T6 Hi2		SMTemp1 Haute2 T6		SMTemp1 H2 T6
388	32		15		SMTemp1 Temp6 High 1	SMTemp1 T6 Hi1		SMTemp1 Haute1 T6		SMTemp1 H1 T6
389	32		15		SMTemp1 Temp6 Low	SMTemp1 T6 Low		SMTemp1 Basse T6		SMTemp1BasseT6
390	32		15		SMTemp1 Temp7 High 2	SMTemp1 T7 Hi2		SMTemp1 Haute2 T7		SMTemp1 H2 T7
391	32		15		SMTemp1 Temp7 High 1	SMTemp1 T7 Hi1		SMTemp1 Haute1 T7		SMTemp1 H1 T7
392	32		15		SMTemp1 Temp7 Low	SMTemp1 T7 Low		SMTemp1 Basse T7		SMTemp1BasseT7
393	32		15		SMTemp1 Temp8 High 2	SMTemp1 T8 Hi2		SMTemp1 Haute2 T8		SMTemp1 H2 T8
394	32		15		SMTemp1 Temp8 High 1	SMTemp1 T8 Hi1		SMTemp1 Haute1 T8		SMTemp1 H1 T8
395	32		15		SMTemp1 Temp8 Low	SMTemp1 T8 Low		SMTemp1 Basse T8		SMTemp1BasseT8
396	32		15		SMTemp2 Temp1 High 2	SMTemp2 T1 Hi2		SMTemp2 Haute2 T1		SMTemp2 H2 T1
397	32		15		SMTemp2 Temp1 High 1	SMTemp2 T1 Hi1		SMTemp2 Haute1 T1		SMTemp2 H1 T1
398	32		15		SMTemp2 Temp1 Low	SMTemp2 T1 Low		SMTemp2 Basse T1		SMTemp2BasseT1
399	32		15		SMTemp2 Temp2 High 2	SMTemp2 T2 Hi2		SMTemp2 Haute2 T2		SMTemp2 H2 T2
400	32		15		SMTemp2 Temp2 High 1	SMTemp2 T2 Hi1		SMTemp2 Haute1 T2		SMTemp2 H1 T2
401	32		15		SMTemp2 Temp2 Low	SMTemp2 T2 Low		SMTemp2 Basse T2		SMTemp2BasseT2
402	32		15		SMTemp2 Temp3 High 2	SMTemp2 T3 Hi2	SMTemp2 Haute2 T3		SMTemp2 H2 T3
403	32		15		SMTemp2 Temp3 High 1	SMTemp2 T3 Hi1		SMTemp2 Haute1 T3		SMTemp2 H1 T3
404	32		15		SMTemp2 Temp3 Low	SMTemp2 T3 Low		SMTemp2 Basse T3		SMTemp2BasseT3
405	32		15		SMTemp2 Temp4 High 2	SMTemp2 T4 Hi2		SMTemp2 Haute2 T4		SMTemp2 H2 T4
406	32		15		SMTemp2 Temp4 High 1	SMTemp2 T4 Hi1		SMTemp2 Haute1 T4		SMTemp2 H1 T4
407	32		15		SMTemp2 Temp4 Low	SMTemp2 T4 Low		SMTemp2 Basse T4		SMTemp2BasseT4
408	32		15		SMTemp2 Temp5 High 2	SMTemp2 T5 Hi2		SMTemp2 Haute2 T5		SMTemp2 H2 T5
409	32		15		SMTemp2 Temp5 High 1	SMTemp2 T5 Hi1		SMTemp2 Haute1 T5		SMTemp2 H1 T5
410	32		15		SMTemp2 Temp5 Low	SMTemp2 T5 Low		SMTemp2 Basse T5		SMTemp2BasseT5
411	32		15		SMTemp2 Temp6 High 2	SMTemp2 T6 Hi2		SMTemp2 Haute2 T6		SMTemp2 H2 T6
412	32		15		SMTemp2 Temp6 High 1	SMTemp2 T6 Hi1		SMTemp2 Haute1 T6		SMTemp2 H1 T6
413	32		15		SMTemp2 Temp6 Low	SMTemp2 T6 Low		SMTemp2 Basse T6		SMTemp2BasseT6
414	32		15		SMTemp2 Temp7 High 2	SMTemp2 T7 Hi2		SMTemp2 Haute2 T7		SMTemp2 H2 T7
415	32		15		SMTemp2 Temp7 High 1	SMTemp2 T7 Hi1		SMTemp2 Haute1 T7		SMTemp2 H1 T7
416	32		15		SMTemp2 Temp7 Low	SMTemp2 T7 Low		SMTemp2 Basse T7		SMTemp2BasseT7
417	32		15		SMTemp2 Temp8 High 2	SMTemp2 T8 Hi2		SMTemp2 Haute2 T8		SMTemp2 H2 T8
418	32		15		SMTemp2 Temp8 High 1	SMTemp2 T8 Hi1		SMTemp2 Haute1 T8		SMTemp2 H1 T8
419	32		15		SMTemp2 Temp8 Low	SMTemp2 T8 Low		SMTemp2 Basse T8		SMTemp2BasseT8	
420	32		15		SMTemp3 Temp1 High 2	SMTemp3 T1 Hi2		SMTemp3 Haute2 T1		SMTemp3 H2 T1
421	32		15		SMTemp3 Temp1 High 1	SMTemp3 T1 Hi1		SMTemp3 Haute1 T1		SMTemp3 H1 T1
422	32		15		SMTemp3 Temp1 Low	SMTemp3 T1 Low		SMTemp3 Basse T1		SMTemp3BasseT1
423	32		15		SMTemp3 Temp2 High 2	SMTemp3 T2 Hi2		SMTemp3 Haute2 T2		SMTemp3 H2 T2
424	32		15		SMTemp3 Temp2 High 1	SMTemp3 T2 Hi1		SMTemp3 Haute1 T2		SMTemp3 H1 T2
425	32		15		SMTemp3 Temp2 Low	SMTemp3 T2 Low		SMTemp3 Basse T2		SMTemp3BasseT2
426	32		15		SMTemp3 Temp3 High 2	SMTemp3 T3 Hi2	SMTemp3 Haute2 T3		SMTemp3 H2 T3
427	32		15		SMTemp3 Temp3 High 1	SMTemp3 T3 Hi1		SMTemp3 Haute1 T3		SMTemp3 H1 T3
428	32		15		SMTemp3 Temp3 Low	SMTemp3 T3 Low		SMTemp3 Basse T3		SMTemp3BasseT3
429	32		15		SMTemp3 Temp4 High 2	SMTemp3 T4 Hi2		SMTemp3 Haute2 T4		SMTemp3 H2 T4
430	32		15		SMTemp3 Temp4 High 1	SMTemp3 T4 Hi1		SMTemp3 Haute1 T4		SMTemp3 H1 T4
431	32		15		SMTemp3 Temp4 Low	SMTemp3 T4 Low		SMTemp3 Basse T4		SMTemp3BasseT4
432	32		15		SMTemp3 Temp5 High 2	SMTemp3 T5 Hi2		SMTemp3 Haute2 T5		SMTemp3 H2 T5
433	32		15		SMTemp3 Temp5 High 1	SMTemp3 T5 Hi1		SMTemp3 Haute1 T5		SMTemp3 H1 T5
434	32		15		SMTemp3 Temp5 Low	SMTemp3 T5 Low		SMTemp3 Basse T5		SMTemp3BasseT5
435	32		15		SMTemp3 Temp6 High 2	SMTemp3 T6 Hi2		SMTemp3 Haute2 T6		SMTemp3 H2 T6
436	32		15		SMTemp3 Temp6 High 1	SMTemp3 T6 Hi1	SMTemp3 Haute1 T6		SMTemp3 H1 T6
437	32		15		SMTemp3 Temp6 Low	SMTemp3 T6 Low		SMTemp3 Basse T6		SMTemp3BasseT6
438	32		15		SMTemp3 Temp7 High 2	SMTemp3 T7 Hi2		SMTemp3 Haute2 T7		SMTemp3 H2 T7
439	32		15		SMTemp3 Temp7 High 1	SMTemp3 T7 Hi1		SMTemp3 Haute1 T7		SMTemp3 H1 T7
440	32		15		SMTemp3 Temp7 Low	SMTemp3 T7 Low		SMTemp3 Basse T7		SMTemp3BasseT7
441	32		15		SMTemp3 Temp8 High 2	SMTemp3 T8 Hi2		SMTemp3 Haute2 T8		SMTemp3 H2 T8
442	32		15		SMTemp3 Temp8 High 1	SMTemp3 T8 Hi1		SMTemp3 Haute1 T8		SMTemp3 H1 T8
443	32		15		SMTemp3 Temp8 Low	SMTemp3 T8 Low		SMTemp3 Basse T8		SMTemp3BasseT8
444	32		15		SMTemp4 Temp1 High 2	SMTemp4 T1 Hi2		SMTemp4 Haute2 T1		SMTemp4 H2 T1
445	32		15		SMTemp4 Temp1 High 1	SMTemp4 T1 Hi1		SMTemp4 Haute1 T1		SMTemp4 H1 T1
446	32		15		SMTemp4 Temp1 Low	SMTemp4 T1 Low		SMTemp4 Basse T1		SMTemp4BasseT1
447	32		15		SMTemp4 Temp2 High 2	SMTemp4 T2 Hi2		SMTemp4 Haute2 T2		SMTemp4 H2 T2
448	32		15		SMTemp4 Temp2 High 1	SMTemp4 T2 Hi1		SMTemp4 Haute1 T2		SMTemp4 H1 T2
449	32		15		SMTemp4 Temp2 Low	SMTemp4 T2 Low		SMTemp4 Basse T2		SMTemp4BasseT2
450	32		15		SMTemp4 Temp3 High 2	SMTemp4 T3 Hi2	SMTemp4 Haute2 T3		SMTemp4 H2 T3
451	32		15		SMTemp4 Temp3 High 1	SMTemp4 T3 Hi1		SMTemp4 Haute1 T3		SMTemp4 H1 T3
452	32		15		SMTemp4 Temp3 Low	SMTemp4 T3 Low		SMTemp4 Basse T3		SMTemp4BasseT3
453	32		15		SMTemp4 Temp4 High 2	SMTemp4 T4 Hi2		SMTemp4 Haute2 T4		SMTemp4 H2 T4
454	32		15		SMTemp4 Temp4 High 1	SMTemp4 T4 Hi1		SMTemp4 Haute1 T4		SMTemp4 H1 T4
455	32		15		SMTemp4 Temp4 Low	SMTemp4 T4 Low		SMTemp4 Basse T4		SMTemp4BasseT4
456	32		15		SMTemp4 Temp5 High 2	SMTemp4 T5 Hi2		SMTemp4 Haute2 T5		SMTemp4 H2 T5
457	32		15		SMTemp4 Temp5 High 1	SMTemp4 T5 Hi1		SMTemp4 Haute1 T5		SMTemp4 H1 T5
458	32		15		SMTemp4 Temp5 Low	SMTemp4 T5 Low		SMTemp4 Basse T5		SMTemp4BasseT5
459	32		15		SMTemp4 Temp6 High 2	SMTemp4 T6 Hi2		SMTemp4 Haute2 T6		SMTemp4 H2 T6
460	32		15		SMTemp4 Temp6 High 1	SMTemp4 T6 Hi1	SMTemp4 Haute1 T6		SMTemp4 H1 T6
461	32		15		SMTemp4 Temp6 Low	SMTemp4 T6 Low		SMTemp4 Basse T6		SMTemp4BasseT6
462	32		15		SMTemp4 Temp7 High 2	SMTemp4 T7 Hi2		SMTemp4 Haute2 T7		SMTemp4 H2 T7
463	32		15		SMTemp4 Temp7 High 1	SMTemp4 T7 Hi1		SMTemp4 Haute1 T7		SMTemp4 H1 T7
464	32		15		SMTemp4 Temp7 Low	SMTemp4 T7 Low		SMTemp4 Basse T7		SMTemp4BasseT7
465	32		15		SMTemp4 Temp8 High 2	SMTemp4 T8 Hi2		SMTemp4 Haute2 T8		SMTemp4 H2 T8
466	32		15		SMTemp4 Temp8 Hi1	SMTemp4 T8 Hi1		SMTemp4 Haute1 T8		SMTemp4 H1 T8
467	32		15		SMTemp4 Temp8 Low	SMTemp4 T8 Low		SMTemp4 Basse T8		SMTemp4BasseT8
468	32		15		SMTemp5 Temp1 High 2	SMTemp5 T1 Hi2		SMTemp5 Haute2 T1		SMTemp5 H2 T1
469	32		15		SMTemp5 Temp1 High 1	SMTemp5 T1 Hi1		SMTemp5 Haute1 T1		SMTemp5 H1 T1
470	32		15		SMTemp5 Temp1 Low	SMTemp5 T1 Low		SMTemp5 Basse T1		SMTemp5BasseT1
471	32		15		SMTemp5 Temp2 High 2	SMTemp5 T2 Hi2		SMTemp5 Haute2 T2		SMTemp5 H2 T2
472	32		15		SMTemp5 Temp2 High 1	SMTemp5 T2 Hi1		SMTemp5 Haute1 T2		SMTemp5 H1 T2
473	32		15		SMTemp5 Temp2 Low	SMTemp5 T2 Low		SMTemp5 Basse T2		SMTemp5BasseT2
474	32		15		SMTemp5 Temp3 High 2	SMTemp5 T3 Hi2	SMTemp5 Haute2 T3		SMTemp5 H2 T3
475	32		15		SMTemp5 Temp3 High 1	SMTemp5 T3 Hi1		SMTemp5 Haute1 T3		SMTemp5 H1 T3
476	32		15		SMTemp5 Temp3 Low	SMTemp5 T3 Low		SMTemp5 Basse T3		SMTemp5BasseT3
477	32		15		SMTemp5 Temp4 High 2	SMTemp5 T4 Hi2		SMTemp5 Haute2 T4		SMTemp5 H2 T4
478	32		15		SMTemp5 Temp4 High 1	SMTemp5 T4 Hi1		SMTemp5 Haute1 T4		SMTemp5 H1 T4
479	32		15		SMTemp5 Temp4 Low	SMTemp5 T4 Low		SMTemp5 Basse T4		SMTemp5BasseT4
480	32		15		SMTemp5 Temp5 High 2	SMTemp5 T5 Hi2		SMTemp5 Haute2 T5		SMTemp5 H2 T5
481	32		15		SMTemp5 Temp5 High 1	SMTemp5 T5 Hi1		SMTemp5 Haute1 T5		SMTemp5 H1 T5
482	32		15		SMTemp5 Temp5 Low	SMTemp5 T5 Low		SMTemp5 Basse T5		SMTemp5BasseT5
483	32		15		SMTemp5 Temp6 High 2	SMTemp5 T6 Hi2		SMTemp5 Haute2 T6		SMTemp5 H2 T6
484	32		15		SMTemp5 Temp6 High 1	SMTemp5 T6 Hi1	SMTemp5 Haute1 T6		SMTemp5 H1 T6
485	32		15		SMTemp5 Temp6 Low	SMTemp5 T6 Low		SMTemp5 Basse T6		SMTemp5BasseT6
486	32		15		SMTemp5 Temp7 High 2	SMTemp5 T7 Hi2		SMTemp5 Haute2 T7		SMTemp5 H2 T7
487	32		15		SMTemp5 Temp7 High 1	SMTemp5 T7 Hi1		SMTemp5 Haute1 T7		SMTemp5 H1 T7
488	32		15		SMTemp5 Temp7 Low	SMTemp5 T7 Low		SMTemp5 Basse T7		SMTemp5BasseT7
489	32		15		SMTemp5 Temp8 High 2	SMTemp5 T8 Hi2		SMTemp5 Haute2 T8		SMTemp5 H2 T8
490	32		15		SMTemp5 Temp8 High 1	SMTemp5 T8 Hi1		SMTemp5 Haute1 T8		SMTemp5 H1 T8
491	32		15		SMTemp5 Temp8 Low	SMTemp5 T8 Low		SMTemp5 Basse T8		SMTemp5BasseT8
492	32		15		SMTemp6 Temp1 High 2	SMTemp6 T1 Hi2		SMTemp6 Haute2 T1		SMTemp6 H2 T1
493	32		15		SMTemp6 Temp1 High 1	SMTemp6 T1 Hi1		SMTemp6 Haute1 T1		SMTemp6 H1 T1
494	32		15		SMTemp6 Temp1 Low	SMTemp6 T1 Low		SMTemp6 Basse T1		SMTemp6BasseT1
495	32		15		SMTemp6 Temp2 High 2	SMTemp6 T2 Hi2		SMTemp6 Haute2 T2		SMTemp6 H2 T2
496	32		15		SMTemp6 Temp2 High 1	SMTemp6 T2 Hi1		SMTemp6 Haute1 T2		SMTemp6 H1 T2
497	32		15		SMTemp6 Temp2 Low	SMTemp6 T2 Low		SMTemp6 Basse T2		SMTemp6BasseT2
498	32		15		SMTemp6 Temp3 High 2	SMTemp6 T3 Hi2	SMTemp6 Haute2 T3		SMTemp6 H2 T3
499	32		15		SMTemp6 Temp3 High 1	SMTemp6 T3 Hi1		SMTemp6 Haute1 T3		SMTemp6 H1 T3
500	32		15		SMTemp6 Temp3 Low	SMTemp6 T3 Low		SMTemp6 Basse T3		SMTemp6BasseT3
501	32		15		SMTemp6 Temp4 High 2	SMTemp6 T4 Hi2		SMTemp6 Haute2 T4		SMTemp6 H2 T4
502	32		15		SMTemp6 Temp4 High 1	SMTemp6 T4 Hi1		SMTemp6 Haute1 T4		SMTemp6 H1 T4
503	32		15		SMTemp6 Temp4 Low	SMTemp6 T4 Low		SMTemp6 Basse T4		SMTemp6BasseT4
504	32		15		SMTemp6 Temp5 High 2	SMTemp6 T5 Hi2		SMTemp6 Haute2 T5		SMTemp6 H2 T5
505	32		15		SMTemp6 Temp5 High 1	SMTemp6 T5 Hi1		SMTemp6 Haute1 T5		SMTemp6 H1 T5
506	32		15		SMTemp6 Temp5 Low	SMTemp6 T5 Low		SMTemp6 Basse T5		SMTemp6BasseT5
507	32		15		SMTemp6 Temp6 High 2	SMTemp6 T6 Hi2		SMTemp6 Haute2 T6		SMTemp6 H2 T6
508	32		15		SMTemp6 Temp6 High 1	SMTemp6 T6 Hi1	SMTemp6 Haute1 T6		SMTemp6 H1 T6
509	32		15		SMTemp6 Temp6 Low	SMTemp6 T6 Low		SMTemp6 Basse T6		SMTemp6BasseT6
510	32		15		SMTemp6 Temp7 High 2	SMTemp6 T7 Hi2		SMTemp6 Haute2 T7		SMTemp6 H2 T7
511	32		15		SMTemp6 Temp7 High 1	SMTemp6 T7 Hi1		SMTemp6 Haute1 T7		SMTemp6 H1 T7
512	32		15		SMTemp6 Temp7 Low	SMTemp6 T7 Low		SMTemp6 Basse T7		SMTemp6BasseT7
513	32		15		SMTemp6 Temp8 High 2	SMTemp6 T8 Hi2		SMTemp6 Haute2 T8		SMTemp6 H2 T8
514	32		15		SMTemp6 Temp8 High 1	SMTemp6 T8 Hi1		SMTemp6 Haute1 T8		SMTemp6 H1 T8
515	32		15		SMTemp6 Temp8 Low	SMTemp6 T8 Low		SMTemp6 Basse T8		SMTemp6BasseT8
516	32		15		SMTemp7 Temp1 High 2	SMTemp7 T1 Hi2		SMTemp7 Haute2 T1		SMTemp7 H2 T1
517	32		15		SMTemp7 Temp1 High 1	SMTemp7 T1 Hi1		SMTemp7 Haute1 T1		SMTemp7 H1 T1
518	32		15		SMTemp7 Temp1 Low	SMTemp7 T1 Low		SMTemp7 Basse T1		SMTemp7BasseT1
519	32		15		SMTemp7 Temp2 High 2	SMTemp7 T2 Hi2		SMTemp7 Haute2 T2		SMTemp7 H2 T2
520	32		15		SMTemp7 Temp2 High 1	SMTemp7 T2 Hi1		SMTemp7 Haute1 T2		SMTemp7 H1 T2
521	32		15		SMTemp7 Temp2 Low	SMTemp7 T2 Low		SMTemp7 Basse T2		SMTemp7BasseT2
522	32		15		SMTemp7 Temp3 High 2	SMTemp7 T3 Hi2	SMTemp7 Haute2 T3		SMTemp7 H2 T3
523	32		15		SMTemp7 Temp3 High 1	SMTemp7 T3 Hi1		SMTemp7 Haute1 T3		SMTemp7 H1 T3
524	32		15		SMTemp7 Temp3 Low	SMTemp7 T3 Low		SMTemp7 Basse T3		SMTemp7BasseT3
525	32		15		SMTemp7 Temp4 High 2	SMTemp7 T4 Hi2		SMTemp7 Haute2 T4		SMTemp7 H2 T4
526	32		15		SMTemp7 Temp4 High 1	SMTemp7 T4 Hi1		SMTemp7 Haute1 T4		SMTemp7 H1 T4
527	32		15		SMTemp7 Temp4 Low	SMTemp7 T4 Low		SMTemp7 Basse T4		SMTemp7BasseT4
528	32		15		SMTemp7 Temp5 High 2	SMTemp7 T5 Hi2		SMTemp7 Haute2 T5		SMTemp7 H2 T5
529	32		15		SMTemp7 Temp5 High 1	SMTemp7 T5 Hi1		SMTemp7 Haute1 T5		SMTemp7 H1 T5
530	32		15		SMTemp7 Temp5 Low	SMTemp7 T5 Low		SMTemp7 Basse T5		SMTemp7BasseT5
531	32		15		SMTemp7 Temp6 High 2	SMTemp7 T6 Hi2		SMTemp7 Haute2 T6		SMTemp7 H2 T6
532	32		15		SMTemp7 Temp6 High 1	SMTemp7 T6 Hi1	SMTemp7 Haute1 T6		SMTemp7 H1 T6
533	32		15		SMTemp7 Temp6 Low	SMTemp7 T6 Low		SMTemp7 Basse T6		SMTemp7BasseT6
534	32		15		SMTemp7 Temp7 High 2	SMTemp7 T7 Hi2		SMTemp7 Haute2 T7		SMTemp7 H2 T7
535	32		15		SMTemp7 Temp7 High 1	SMTemp7 T7 Hi1		SMTemp7 Haute1 T7		SMTemp7 H1 T7
536	32		15		SMTemp7 Temp7 Low	SMTemp7 T7 Low		SMTemp7 Basse T7		SMTemp7BasseT7
537	32		15		SMTemp7 Temp8 High 2	SMTemp7 T8 Hi2		SMTemp7 Haute2 T8		SMTemp7 H2 T8
538	32		15		SMTemp7 Temp8 High 1	SMTemp7 T8 Hi1		SMTemp7 Haute1 T8		SMTemp7 H1 T8
539	32		15		SMTemp7 Temp8 Low	SMTemp7 T8 Low		SMTemp7 Basse T8		SMTemp7BasseT8
540	32		15		SMTemp8 Temp1 High 2	SMTemp8 T1 Hi2		SMTemp8 Haute2 T1		SMTemp8 H2 T1
541	32		15		SMTemp8 Temp1 High 1	SMTemp8 T1 Hi1		SMTemp8 Haute1 T1		SMTemp8 H1 T1
542	32		15		SMTemp8 Temp1 Low	SMTemp8 T1 Low		SMTemp8 Basse T1		SMTemp8BasseT1
543	32		15		SMTemp8 Temp2 High 2	SMTemp8 T2 Hi2		SMTemp8 Haute2 T2		SMTemp8 H2 T2
544	32		15		SMTemp8 Temp2 High 1	SMTemp8 T2 Hi1		SMTemp8 Haute1 T2		SMTemp8 H1 T2
545	32		15		SMTemp8 Temp2 Low	SMTemp8 T2 Low		SMTemp8 Basse T2		SMTemp8BasseT2
546	32		15		SMTemp8 Temp3 High 2	SMTemp8 T3 Hi2	SMTemp8 Haute2 T3		SMTemp8 H2 T3
547	32		15		SMTemp8 Temp3 High 1	SMTemp8 T3 Hi1		SMTemp8 Haute1 T3		SMTemp8 H1 T3
548	32		15		SMTemp8 Temp3 Low	SMTemp8 T3 Low		SMTemp8 Basse T3		SMTemp8BasseT3
549	32		15		SMTemp8 Temp4 High 2	SMTemp8 T4 Hi2		SMTemp8 Haute2 T4		SMTemp8 H2 T4
550	32		15		SMTemp8 Temp4 High 1	SMTemp8 T4 Hi1		SMTemp8 Haute1 T4		SMTemp8 H1 T4
551	32		15		SMTemp8 Temp4 Low	SMTemp8 T4 Low		SMTemp8 Basse T4		SMTemp8BasseT4
552	32		15		SMTemp8 Temp5 High 2	SMTemp8 T5 Hi2		SMTemp8 Haute2 T5		SMTemp8 H2 T5
553	32		15		SMTemp8 Temp5 High 1	SMTemp8 T5 Hi1		SMTemp8 Haute1 T5		SMTemp8 H1 T5
554	32		15		SMTemp8 Temp5 Low	SMTemp8 T5 Low		SMTemp8 Basse T5		SMTemp8BasseT5
555	32		15		SMTemp8 Temp6 High 2	SMTemp8 T6 Hi2		SMTemp8 Haute2 T6		SMTemp8 H2 T6
556	32		15		SMTemp8 Temp6 High 1	SMTemp8 T6 Hi1	SMTemp8 Haute1 T6		SMTemp8 H1 T6
557	32		15		SMTemp8 Temp6 Low	SMTemp8 T6 Low		SMTemp8 Basse T6		SMTemp8BasseT6
558	32		15		SMTemp8 Temp7 High 2	SMTemp8 T7 Hi2		SMTemp8 Haute2 T7		SMTemp8 H2 T7
559	32		15		SMTemp8 Temp7 High 1	SMTemp8 T7 Hi1		SMTemp8 Haute1 T7		SMTemp8 H1 T7
560	32		15		SMTemp8 Temp7 Low	SMTemp8 T7 Low		SMTemp8 Basse T7		SMTemp8BasseT7
561	32		15		SMTemp8 Temp8 High 2	SMTemp8 T8 Hi2		SMTemp8 Haute2 T8		SMTemp8 H2 T8
562	32		15		SMTemp8 Temp8 High 1	SMTemp8 T8 Hi1		SMTemp8 Haute1 T8		SMTemp8 H1 T8
563	32		15		SMTemp8 Temp8 Low	SMTemp8 T8 Low		SMTemp8 Basse T8		SMTemp8BasseT8
#---------------------------------------12345678901234567890123456789012--------123456789012345---------12345678901234567890123456789012--------123456789012345
564	32		15		Temp Comp Voltage Clamp		Temp Comp Clamp		ESNA mode de compensation		ESNA mode Comp
565	32		15		Temp Comp Max Voltage		Temp Comp Max V		ESNA Tension haute compensation		ESNA VHautComp.
566	32		15		Temp Comp Min Voltage		Temp Comp Min V		ESNA Tension basse compensation		ESNA V basComp.
 
567	32		15		BTRM Temperature		BTRM Temp		BTRM Temperatura			BTRM Temp 
568	32		15		BTRM Temp High 2		BTRM Temp High2		BTRM Temp  haute 2			BTRM T Haut2
569	32		15		BTRM Temp High 1		BTRM Temp High1	BTRM Temp haute 1			BTRM T Haut1

570	32		15		Temp Comp Max Voltage (24V)		Temp Comp Max V		Tension Max Compensation (24V)		Vmax Comp(24V)
571	32		15		Temp Comp Min Voltage (24V)		Temp Comp Min V		Tension Min Compensation (24V)		Vmin Comp(24V)
572	32		15		BTRM Temp Sensor		BTRM TempSensor			BTRM Capteur Temperature		BTRM Capt. Temp
573	32		15		BTRM Sensor Fail		BTRM TempFail								BTRM Defaut Capteur Temperature	BTRM Def Temp
574	32		15		Li-Ion Group Avg Temperature	LiBatt AvgTemp						Groupe Temp Batterie Li-Ion	Gr.Temp.Li-Ion
575	32		15		Number of Installed Batteries	Num Installed						Nombre branche batterie			Nb.branche bat.
576	32		15		Number of Disconnected Batteries	NumDisconnected					Nombre de Contacteur Batterie		Nb.Contact.Bat.
577	32		15		Inventory Update In Process	InventUpdating						Inventaire en Cours			Invent.en Cours
578	32		15		Number of Comm Fail Batt	Num Comm Fail	Nombre Défaut Comm. Batt.	Nb.Déf.Com.Bat.
579	32		15		Li-Ion Battery Lost		LiBatt Lost		Perte Batterie Li-IOn			PerteBat.Li-IOn
580	32		15		System Battery Type		Sys Batt Type		Type batterie				Type batterie
581	32		15		1 Li-Ion Battery Disconnect	1 LiBattDiscon				Batterie Li-Ion 1 Décon.	Li-Ion1 Décon.
582	32		15		2+Li-Ion Battery Disconnect	2+LiBattDiscon				Batterie Li-Ion 2 Décon.	Li-Ion2 Décon.
583	32		15		1 Li-Ion Battery No Reply	1 LiBattNoReply				Batterie Li-Ion 1 Défaut Comm		Li-Ion1Déf.Comm
584	32		15		2+Li-Ion Battery No Reply	2+LiBattNoReply				Batterie Li-Ion 2 Défaut Comm		Li-Ion2Déf.Comm
585	32		15		Clear Li-Ion Battery Lost	Clr LiBatt Lost			Reset Perte Batterie Li-Ion		Res.Bat.Li-Ion
586	32		15		Clear				Clear			Efface					Efface       
587	32		15		Float Charge Voltage(Solar)	Float Volt(S)	Tension de Floating (MPMT)		V Float.(MPMT)		
588	32		15		Equalize Charge Voltage(Solar)	EQ Voltage(S)	Tension d'égalisation(MPMT)		V Egal.(MPMT)			
589	32		15		Float Charge Voltage(RECT)	Float Volt(R)	Tension de Floating (Red)		V Float.(Red)			
590	32		15		Equalize Charge Voltage(RECT)	EQ Voltage(R)	Tension d'égalisation(Red)		V Egal.(Red)		
591	32		15		Active Battery Current Limit	ABCL Point		Limitation Courant Batt.		Act.Lim.IBat
592	32		15		Clear Li Battery CommInterrupt	ClrLiBatComFail		Reset Défaut Comm Batt. Li-Ion		Déf.Com.Li-Ionl		
593	32		15		ABCL is active			ABCL Active			Limit. Courant Batt. Active		Lim.I Bat.Act.          
594	32		15		Last SMBAT Battery Number	Last SMBAT Num			Numero dernier SMBAT		NumdernierSMBAT	
595	32		15		Voltage Adjust Gain		VoltAdjustGain		Réglage du Gain tension		ReglGainTension       
596	32		15		Curr Limited Mode		Curr Limit Mode				Mode limitation de courant		ModeLimitCouran       
597	32		15		Current				Current				Courant		Courant       
598	32		15		Voltage				Voltage				Tension		Tension           
599	32		15		Battery Charge Prohibited Status		Charge Prohibit		Charge batteries interdite			ChargBatInter
600	32		15		Battery Charge Prohibited Alarm			Charge Prohibit		Alarme charge batterie interdite			AlChgBatInter	
601	32		15		Battery Lower Capacity		Lower Capacity		Capacité basse batterie		Capa Basse Batt       
602	32		15		Charge Current			Charge Current	Courant de recharge			Courant Recharg
603	32		15		Upper Limit			Upper Lmt				Limite Haute			LimitHaut
604	32		15		Stable Range Upper Limit	Stable Up Lmt		Limite haute de plage stable	Plage H Stable 
605	32		15		Stable Range Lower Limit	Stable Low Lmt		Limite basse de plage stable	Plage B Stable
606	32		15		Speed Set Point			Speed Set Point					Conf valeur Vitesse		Conf val Vitess
607	32		15		Slow Speed Coefficient		Slow Coeff							Coef vitesse basse		Coef V_bas
608	32		15		Fast Speed Coefficient		Fast Coeff							Coef vitesse haute		Coef V_haut
609	32		15		Min Amplitude			Min Amplitude								Amplitude minimum		Amplitude Min
610	32		15		Max Amplitude			Max Amplitude								Amplitude maximum		Amplitude Max
611	32		15		Cycle Number			Cycle Num								Nombre de cycle			Nombre de cycle
612	32		15		Battery Temp Summary Alarm	BattTempSummAlm	Batterie Temp. Alarme Sommaire		BattTempAlmSomm
613	32		15		EQTemp Comp Coefficient		EQCompCoeff			EQ Temp Coefficient Comp		EQCompCoeff																		

620	32		15		IB2-2 Temp1		IB2-2 T1		IB2-2 Température 1			IB2-2-Temp1
621	32		15		IB2-2 Temp2		IB2-2 T2		IB2-2 Température 2			IB2-2-Temp2
622	32		15		EIB-2 Temp1		EIB-2 T1		EIB-2 Température 1			EIB-2-Temp1
623	32		15		EIB-2 Temp2		EIB-2 T2		EIB-2 Température 2			EIB-2-Temp2

624	32		15		IB2-2 Temp1 High 2	IB2-2 T1 Hi2		Haute2 IB2-2 T1		Haute2 IB2-2 T1
625	32		15		IB2-2 Temp1 High 1	IB2-2 T1 Hi1		Haute1 IB2-2 T1		Haute1 IB2-2 T1
626	32		15		IB2-2 Temp1 Low		IB2-2 T1 Low		IB2-2 Basse T1		IB2-2 Basse T1
627	32		15		IB2-2 Temp2 High 2	IB2-2 T2 Hi2		Haute2 IB2-2 T2		Haute2 IB2-2 T2
628	32		15		IB2-2 Temp2 High 1	IB2-2 T2 Hi1		Haute1 IB2-2 T2		Haute1 IB2-2 T2
629	32		15		IB2-2 Temp2 Low		IB2-2 T2 Low		IB2-2 Basse T2		IB2-2 Basse T2
630	32		15		EIB-2 Temp1 High 2	EIB-2 T1 Hi2		EIB-2 Haute2 T1		EIB-2 Haute2 T1
631	32		15		EIB-2 Temp1 High 1	EIB-2 T1 Hi1		EIB-2 Haute1 T1		EIB-2 Haute1 T1
632	32		15		EIB-2 Temp1 Low		EIB-2 T1 Low		EIB-2 Basse T1		EIB-2 Basse T1
633	32		15		EIB-2 Temp2 High 2	EIB-2 T2 Hi2		EIB-2 Haute2 T2		EIB-2 Haute2 T2
634	32		15		EIB-2 Temp2 High 1	EIB-2 T2 Hi1		EIB-2 Haute1 T2		EIB-2 Haute1 T2
635	32		15		EIB-2 Temp2 Low		EIB-2 T2 Low		EIB-2 Basse T2		EIB-2 Basse T2

685	32		15		Temp Comp Off When		TC Off When		Comp Temp Off Lorsque		CT Off Lorsque
686	32		15		All Probes Fail			All Probes Fail		Toutes les sondes échouent	Tout Sond Echo
687	32		15		Any Probe Fails			Any Probe Fails		Toute sonde échoue		ToutSondeEcho
688	32		15		Temp Compensation Enabled	Temp Comp		Compensation Temp activé	Comp Temp
689	32		15		Temp Comp Threshold Enabled	TC Threshold		Seuil temp comp activé		Seuil Comp Temp
690	32		15		Temp for Temp Comp Off		Off Temp		Temp pour Temp Comp Off		Temp Off
691	32		15		Temp for Temp Comp On		On Temp			Temp pour Temp Comp On		Temp On

701	32		15		SMDUE1 Temp1		SMDUE1 T1		SMDUE1温度1		SMDUE1温度1
702	32		15		SMDUE1 Temp2		SMDUE1 T2		SMDUE1温度2		SMDUE1温度2
703	32		15		SMDUE1 Temp3		SMDUE1 T3		SMDUE1温度3		SMDUE1温度3
704	32		15		SMDUE1 Temp4		SMDUE1 T4		SMDUE1温度4		SMDUE1温度4
705	32		15		SMDUE1 Temp5		SMDUE1 T5		SMDUE1温度5		SMDUE1温度5
706	32		15		SMDUE1 Temp6		SMDUE1 T6		SMDUE1温度6		SMDUE1温度6
707	32		15		SMDUE1 Temp7		SMDUE1 T7		SMDUE1温度7		SMDUE1温度7
708	32		15		SMDUE1 Temp8		SMDUE1 T8		SMDUE1温度8		SMDUE1温度8
709	32		15		SMDUE1 Temp9		SMDUE1 T9		SMDUE1温度9		SMDUE1温度9
710	32		15		SMDUE1 Temp10		SMDUE1 T10	SMDUE1温度10		SMDUE1温度10
711	32		15		SMDUE2 Temp1		SMDUE2 T1		SMDUE2温度1		SMDUE2温度1
712	32		15		SMDUE2 Temp2		SMDUE2 T2		SMDUE2温度2		SMDUE2温度2
713	32		15		SMDUE2 Temp3		SMDUE2 T3		SMDUE2温度3		SMDUE2温度3
714	32		15		SMDUE2 Temp4		SMDUE2 T4		SMDUE2温度4		SMDUE2温度4
715	32		15		SMDUE2 Temp5		SMDUE2 T5		SMDUE2温度5		SMDUE2温度5
716	32		15		SMDUE2 Temp6		SMDUE2 T6		SMDUE2温度6		SMDUE2温度6
717	32		15		SMDUE2 Temp7		SMDUE2 T7		SMDUE2温度7		SMDUE2温度7
718	32		15		SMDUE2 Temp8		SMDUE2 T8		SMDUE2温度8		SMDUE2温度8
719	32		15		SMDUE2 Temp9		SMDUE2 T9		SMDUE2温度9		SMDUE2温度9
720	32		15		SMDUE2 Temp10		SMDUE2 T10	SMDUE2温度10		SMDUE2温度10
721		32		15		SMDUE1 Temp1 High 2	SMDUE1 T1 Hi2		SMDUE1 Temp 1 Très haute		SMDUE1 T1 THau
722		32		15		SMDUE1 Temp1 High 1	SMDUE1 T1 Hi1		SMDUE1 Temp 1 Haute			SMDUE1 T1 HauT
723		32		15		SMDUE1 Temp1 Low		SMDUE1 T1 Low		SMDUE1 Temp 1 Basse			SMDUE1 T1 Bass
724		32		15		SMDUE1 Temp2 High 2	SMDUE1 T2 Hi2		SMDUE1 Temp 2 Très haute		SMDUE1 T2 THau
725		32		15		SMDUE1 Temp2 High 1	SMDUE1 T2 Hi1		SMDUE1 Temp 2 Haute			SMDUE1 T2 HauT
726		32		15		SMDUE1 Temp2 Low		SMDUE1 T2 Low		SMDUE1 Temp 2 Basse			SMDUE1 T2 Bass
727		32		15		SMDUE1 Temp3 High 2	SMDUE1 T3 Hi2		SMDUE1 Temp 3 Très haute		SMDUE1 T3 THau
728		32		15		SMDUE1 Temp3 High 1	SMDUE1 T3 Hi1		SMDUE1 Temp 3 Haute			SMDUE1 T3 HauT
729		32		15		SMDUE1 Temp3 Low		SMDUE1 T3 Low		SMDUE1 Temp 3 Basse			SMDUE1 T3 Bass
730		32		15		SMDUE1 Temp4 High 2	SMDUE1 T4 Hi2		SMDUE1 Temp 4 Très haute		SMDUE1 T4 THau
731		32		15		SMDUE1 Temp4 High 1	SMDUE1 T4 Hi1		SMDUE1 Temp 4 Haute			SMDUE1 T4 HauT
732		32		15		SMDUE1 Temp4 Low		SMDUE1 T4 Low		SMDUE1 Temp 4 Basse			SMDUE1 T4 Bass
733		32		15		SMDUE1 Temp5 High 2	SMDUE1 T5 Hi2		SMDUE1 Temp 5 Très haute		SMDUE1 T5 THau
734		32		15		SMDUE1 Temp5 High 1	SMDUE1 T5 Hi1		SMDUE1 Temp 5 Haute			SMDUE1 T5 HauT
735		32		15		SMDUE1 Temp5 Low		SMDUE1 T5 Low		SMDUE1 Temp 5 Basse			SMDUE1 T5 Bass
736		32		15		SMDUE1 Temp6 High 2	SMDUE1 T6 Hi2		SMDUE1 Temp 6 Très haute		SMDUE1 T6 THau
737		32		15		SMDUE1 Temp6 High 1	SMDUE1 T6 Hi1		SMDUE1 Temp 6 Haute			SMDUE1 T6 HauT
738		32		15		SMDUE1 Temp6 Low		SMDUE1 T6 Low		SMDUE1 Temp 6 Basse			SMDUE1 T6 Bass
739		32		15		SMDUE1 Temp7 High 2	SMDUE1 T7 Hi2		SMDUE1 Temp 7 Très haute		SMDUE1 T7 THau
740		32		15		SMDUE1 Temp7 High 1	SMDUE1 T7 Hi1		SMDUE1 Temp 7 Haute			SMDUE1 T7 HauT
741		32		15		SMDUE1 Temp7 Low		SMDUE1 T7 Low		SMDUE1 Temp 7 Basse			SMDUE1 T7 Bass
742		32		15		SMDUE1 Temp8 High 2	SMDUE1 T8 Hi2		SMDUE1 Temp 8 Très haute		SMDUE1 T8 THau
743		32		15		SMDUE1 Temp8 High 1	SMDUE1 T8 Hi1		SMDUE1 Temp 8 Haute			SMDUE1 T8 HauT
744		32		15		SMDUE1 Temp8 Low		SMDUE1 T8 Low		SMDUE1 Temp 8 Basse			SMDUE1 T8 Bass
745		32		15		SMDUE1 Temp9 High 2	SMDUE1 T9 Hi2		SMDUE1 Temp 9 Très haute		SMDUE1 T9 THau
746		32		15		SMDUE1 Temp9 High 1	SMDUE1 T9 Hi1		SMDUE1 Temp 9 Haute			SMDUE1 T9 HauT
747		32		15		SMDUE1 Temp9 Low		SMDUE1 T9 Low		SMDUE1 Temp 9 Basse			SMDUE1 T9 Bass
748		32		15		SMDUE1 Temp10 High 2	SMDUE1 T10 Hi2		SMDUE1 Temp 10 Très haute		SMDUE1 T10 THau
749		32		15		SMDUE1 Temp10 High 1	SMDUE1 T10 Hi1		SMDUE1 Temp 10 Haute			SMDUE1 T10 HauT
750		32		15		SMDUE1 Temp10 Low	SMDUE1 T10 Low		SMDUE1 Temp 10 Basse			SMDUE1 T10 Bass
751		32		15		SMDUE2 Temp1 High 2	SMDUE2 T1 Hi2		SMDUE2 Temp 1 Très haute		SMDUE2 T1 THau
752		32		15		SMDUE2 Temp1 High 1	SMDUE2 T1 Hi1		SMDUE2 Temp 1 Haute			SMDUE2 T1 HauT
753		32		15		SMDUE2 Temp1 Low		SMDUE2 T1 Low		SMDUE2 Temp 1 Basse			SMDUE2 T1 Bass
754		32		15		SMDUE2 Temp2 High 2	SMDUE2 T2 Hi2		SMDUE2 Temp 2 Très haute		SMDUE2 T2 THau
755		32		15		SMDUE2 Temp2 High 1	SMDUE2 T2 Hi1		SMDUE2 Temp 2 Haute			SMDUE2 T2 HauT
756		32		15		SMDUE2 Temp2 Low		SMDUE2 T2 Low		SMDUE2 Temp 2 Basse			SMDUE2 T2 Bass
757		32		15		SMDUE2 Temp3 High 2	SMDUE2 T3 Hi2		SMDUE2 Temp 3 Très haute		SMDUE2 T3 THau
758		32		15		SMDUE2 Temp3 High 1	SMDUE2 T3 Hi1		SMDUE2 Temp 3 Haute			SMDUE2 T3 HauT
759		32		15		SMDUE2 Temp3 Low		SMDUE2 T3 Low		SMDUE2 Temp 3 Basse			SMDUE2 T3 Bass
760		32		15		SMDUE2 Temp4 High 2	SMDUE2 T4 Hi2		SMDUE2 Temp 4 Très haute		SMDUE2 T4 THau
761		32		15		SMDUE2 Temp4 High 1	SMDUE2 T4 Hi1		SMDUE2 Temp 4 Haute			SMDUE2 T4 HauT
762		32		15		SMDUE2 Temp4 Low		SMDUE2 T4 Low		SMDUE2 Temp 4 Basse			SMDUE2 T4 Bass
763		32		15		SMDUE2 Temp5 High 2	SMDUE2 T5 Hi2		SMDUE2 Temp 5 Très haute		SMDUE2 T5 THau
764		32		15		SMDUE2 Temp5 High 1	SMDUE2 T5 Hi1		SMDUE2 Temp 5 Haute			SMDUE2 T5 HauT
765		32		15		SMDUE2 Temp5 Low		SMDUE2 T5 Low		SMDUE2 Temp 5 Basse			SMDUE2 T5 Bass
766		32		15		SMDUE2 Temp6 High 2	SMDUE2 T6 Hi2		SMDUE2 Temp 6 Très haute		SMDUE2 T6 THau
767		32		15		SMDUE2 Temp6 High 1	SMDUE2 T6 Hi1		SMDUE2 Temp 6 Haute			SMDUE2 T6 HauT
768		32		15		SMDUE2 Temp6 Low		SMDUE2 T6 Low		SMDUE2 Temp 6 Basse			SMDUE2 T6 Bass
769		32		15		SMDUE2 Temp7 High 2	SMDUE2 T7 Hi2		SMDUE2 Temp 7 Très haute		SMDUE2 T7 THau
770		32		15		SMDUE2 Temp7 High 1	SMDUE2 T7 Hi1		SMDUE2 Temp 7 Haute			SMDUE2 T7 HauT
771		32		15		SMDUE2 Temp7 Low		SMDUE2 T7 Low		SMDUE2 Temp 7 Basse			SMDUE2 T7 Bass
772		32		15		SMDUE2 Temp8 High 2	SMDUE2 T8 Hi2		SMDUE2 Temp 8 Très haute		SMDUE2 T8 THau
773		32		15		SMDUE2 Temp8 High 1	SMDUE2 T8 Hi1		SMDUE2 Temp 8 Haute			SMDUE2 T8 HauT
774		32		15		SMDUE2 Temp8 Low		SMDUE2 T8 Low		SMDUE2 Temp 8 Basse			SMDUE2 T8 Bass
775		32		15		SMDUE2 Temp9 High 2	SMDUE2 T9 Hi2		SMDUE2 Temp 9 Très haute		SMDUE2 T9 THau
776		32		15		SMDUE2 Temp9 High 1	SMDUE2 T9 Hi1		SMDUE2 Temp 9 Haute			SMDUE2 T9 HauT
777		32		15		SMDUE2 Temp9 Low		SMDUE2 T9 Low		SMDUE2 Temp 9 Basse			SMDUE2 T9 Bass
778		32		15		SMDUE2 Temp10 High 2	SMDUE2 T10 Hi2		SMDUE2 Temp 10 Très haute		SMDUE2 T10 THau
779		32		15		SMDUE2 Temp10 High 1	SMDUE2 T10 Hi1		SMDUE2 Temp 10 Haute			SMDUE2 T10 HauT
780		32		15		SMDUE2 Temp10 Low	SMDUE2 T10 Low		SMDUE2 Temp 10 Basse			SMDUE2 T10 Bass

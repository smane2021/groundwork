﻿#
#  Locale language support:french
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			SMDUH Group		SMDUH Group		Groupe SMDUH			Groupe SMDUH
2		32			15			Standby		Standby		Standby			Standby
3		32			15			Refresh			Refresh			Actualiser		Actualiser
4		32			15			Setting Refresh		Setting Refresh		Paramètre Actualiser		Para Actualiser
5		32			15			E-Stop		E-Stop			E-Stop		E-Stop
6		32			15			Yes			Yes		Oui			Oui
7		32			15			Existence State		Existence State		Etat existant		Etat existant
8		32			15			Existent		Existent		Existant			Existant
9		32			15			Not Existent		Not Existent		Inexistant			Inexistant
10		32			15			Number of SMDUHs		Num of SMDUHs		Quantité de SMDUH		Qte de SMDUH
11		32			15			SMDUH Config Changed	Cfg Changed		Config SMDUH modifiee		Conf SMDUHModif
12		32			15			Not Changed		Not Changed		Non modifie			Non modifie
13		32			15			Changed			Changed			Modifie			Modifie


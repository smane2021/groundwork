﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32			15			Bus Bar Voltage			Bus Bar Voltage		Tension Système				Tension Système
2	32			15			Load 1					Load 1			Charge 1				Charge 1
3	32			15			Load 2					Load 2			Charge 2				Charge 2
4	32			15			Load 3					Load 3			Charge 3				Charge 3
5	32			15			Load 4					Load 4			Charge 4				Charge 4
6	32			15			Load Fuse 1				Load Fuse 1				Protection Distrib DC 1			Prot Dist DC 1
7	32			15			Load Fuse 2				Load Fuse 2				Protection Distrib DC 2			Prot Dist DC 2
8	32			15			Load Fuse 3				Load Fuse 3				Protection Distrib DC 3			Prot Dist DC 3
9	32			15			Load Fuse 4				Load Fuse 4				Protection Distrib DC 4			Prot Dist DC 4
10	32			15			Load Fuse 5				Load Fuse 5				Protection Distrib DC 5			Prot Dist DC 5
11	32			15			Load Fuse 6				Load Fuse 6				Protection Distrib DC 6			Prot Dist DC 6
12	32			15			Load Fuse 7				Load Fuse 7				Protection Distrib DC 7			Prot Dist DC 7
13	32			15			Load Fuse 8				Load Fuse 8				Protection Distrib DC 8			Prot Dist DC 8
14	32			15			Load Fuse 9				Load Fuse9				Protection Distrib DC 9			Prot Dist DC 9
15	32			15			Load Fuse 10				Load Fuse 10		Protection Distrib DC 10		Prot Dist DC 10
16	32			15			Load Fuse 11				Load Fuse 11		Protection Distrib DC 11		Prot Dist DC 11
17	32			15			Load Fuse 12				Load Fuse 12		Protection Distrib DC 12		Prot Dist DC 12
18	32			15			Load Fuse 13				Load Fuse 13		Protection Distrib DC 13		Prot Dist DC 13
19	32			15			Load Fuse 14				Load Fuse 14		Protection Distrib DC 14		Prot Dist DC 14
20	32			15			Load Fuse 15				Load Fuse 15		Protection Distrib DC 15		Prot Dist DC 15
21	32			15			Load Fuse 16				Load Fuse 16		Protection Distrib DC 16		Prot Dist DC 16
22	32			15			Run Time			Run Time		Temps de Fonctionnement			Temps Fonct.
23	32			15			LV Disconnect 1 Control			LVD 1 Control		Control LVD1				Control LVD1
24	32			15			LV Disconnect 2 Control			LVD 2 Control		Control LVD2				Control LVD2
25	32			15			LV Disconnect 1 Voltage			LVD 1 Voltage		V deconnect LVD1			V deconnec LVD1
26	32			15			LV Resconnect 1 Voltage			LVR 1 Voltage		V Connect LVD1				V Connect LVD1
27	32			15			LV Disconnect 2 Voltage			LVD 2 Voltage		V deconnect LVD2			V deconnec LVD2
28	32			15			LV Resconnect 2 Voltage			LVR 2 Voltage		V Connect LVD2				V Connect LVD2
29	32			15			On				On			Ferme					Ferme
30	32			15			Off				Off			Ouvert					Ouvert
31	32			15			Normal				Normal			Normal					Normal
32	32			15			Error				Error			Erreur					Erreur
33	32			15			On				On			Fermer					Fermer
34	32			15			Fuse 1 Alarm				Fuse 1 Alarm	Alarme Protec Distrib DC 1		AL Dist DC 1
35	32			15			Fuse 2 Alarm				Fuse 2 Alarm	Alarme Protec Distrib DC 2		AL Dist DC 2
36	32			15			Fuse 3 Alarm				Fuse 3 Alarm	Alarme Protec Distrib DC 3		AL Dist DC 3
37	32			15			Fuse 4 Alarm				Fuse 4 Alarm	Alarme Protec Distrib DC 4		AL Dist DC 4
38	32			15			Fuse 5 Alarm				Fuse 5 Alarm	Alarme Protec Distrib DC 5		AL Dist DC 5
39	32			15			Fuse 6 Alarm				Fuse 6 Alarm	Alarme Protec Distrib DC 6		AL Dist DC 6
40	32			15			Fuse 7 Alarm				Fuse 7 Alarm	Alarme Protec Distrib DC 7		AL Dist DC 7
41	32			15			Fuse 8 Alarm				Fuse 8 Alarm	Alarme Protec Distrib DC 8		AL Dist DC 8
42	32			15			Fuse 9 Alarm				Fuse 9 Alarm	Alarme Protec Distrib DC 9		AL Dist DC 9
43	32			15			Fuse 10 Alarm				Fuse 10 Alarm		Alarme Protec Distrib DC 10		AL Dist DC 10
44	32			15			Fuse 11 Alarm				Fuse 11 Alarm		Alarme Protec Distrib DC 11		AL Dist DC 11
45	32			15			Fuse 12 Alarm				Fuse 12 Alarm		Alarme Protec Distrib DC 12		AL Dist DC 12
46	32			15			Fuse 13 Alarm				Fuse 13 Alarm		Alarme Protec Distrib DC 13		AL Dist DC 13
47	32			15			Fuse 14 Alarm				Fuse 14 Alarm		Alarme Protec Distrib DC 14		AL Dist DC 14
48	32			15			Fuse 15 Alarm				Fuse 15 Alarm		Alarme Protec Distrib DC 15		AL Dist DC 15
49	32			15			Fuse 16 Alarm				Fuse 16 Alarm		Alarme Protec Distrib DC 16		AL Dist DC 16
50	32			15			HW Test Alarm			HW Test Alarm		Alarme Test Etat HW			AL Test Etat HW
51	32			15			SMDU 7					SMDU 7			SMDU 7			SMDU 7
52	32			15			Battery Fuse 1 Voltage			BattFuse 1 Volt		Tension Protect Bat 1			V Protect Bat 1
53	32			15			Battery Fuse 2 Voltage			BattFuse 2 Volt		Tension Protect Bat 2			V Protect Bat 2
54	32			15			Battery Fuse 3 Voltage			BattFuse 3 Volt		Tension Protect Bat 3			V Protect Bat 3
55	32			15			Battery Fuse 4 Voltage			BattFuse 4 Volt		Tension Protect Bat 4			V Protect Bat 4
56	32			15			Battery Fuse 1 Status			Batt Fuse 1			Etat Protect Bat 1			Etat Prot Bat 1
57	32			15			Battery Fuse 2 Status			Batt Fuse 2		Etat Protect Bat 2			Etat Prot Bat 2
58	32			15			Battery Fuse 3 Status			Batt Fuse 3		Etat Protect Bat 3			Etat Prot Bat 3
59	32			15			Battery Fuse 4 Status			Batt Fuse 4		Etat Protect Bat 4			Etat Prot Bat 4
60	32			15			On				On			Ferme					Ferme
61	32			15			Off				Off			Ouvert					Ouvert
62	32			15			Battery Fuse 1 Alarm			BattFuse 1 Alm		Alarme Prot Bat 1			Al Prot Bat 1
63	32			15			Battery Fuse 2 Alarm			BattFuse 2 Alm		Alarme Prot Bat 2			Al Prot Bat 2
64	32			15			Battery Fuse 3 Alarm			BattFuse 3 Alm		Alarme Prot Bat 3			Al Prot Bat 3
65	32			15			Battery Fuse 4 Alarm			BattFuse 4 Alm		Alarme Prot Bat 4			Al Prot Bat 4
66	32			15			All Load Current			All Load Curr		Courant Total				Courant Total
67	32			15			Over Current Point(Load)		Over Curr Point			Seuil SurCharge Courant			Seuil Sur I
68	32			15			Over Current(Load)			Over Current		SurCharge Courant			Sur Courant
69	32			15			LVD 1					LVD 1		LVD 1					LVD 1
70	32			15			LVD 1 Mode			LVD 1 Mode		Commande LVD1				Commande LVD1
71	32			15			LVD 1 Reconnect Delay		LVD1 ReconDelay	Tempo de fermeture LVD1			Tempo ferm.LVD1
72	32			15			LVD 2					LVD 2		LVD 2					LVD 2 
73	32			15			LVD 2 Mode			LVD2 Mode		Mode Commande LVD2			Commande LVD2
74	32			15			LVD 2 Reconnect Delay		LVD2 ReconDelay			Tempo de fermeture LVD2			Tempo ferm.LVD2
75	32			15			LVD 1 Status			LVD 1 Status		Etat LVD1				Etat LVD1
76	32			15			LVD 2 Status			LVD 2 Status		Etat LVD2				Etat LVD2
77	32			15			Disabled			Disabled		DéActivé				DéActivé
78	32			15			Enabled				Enabled			Activé					Activé
79	32			15			Voltage					Voltage			En Tension				En Tension
80	32			15			Time					Time		En Duree				En Duree
81	32			15			Bus Bar Voltage Alarm			Bus Bar Alarm			Alarme tension Vs			Al tension Vs
82	32			15			Normal				Normal			Normal					Normal
83	32			15			Low				Low			Basse					Basse
84	32			15			High				High			Haute					Haute
85	32			15			Under Voltage				Under Voltage		Tension Basse				Tension Basse
86	32			15			Over Voltage				Over Voltage		Tension Haute				Tension Haute
87	32			15			Shunt 1 Current Alarm			Shunt 1 Alarm		Alarme Courant Shunt1			Al I Shunt1
88	32			15			Shunt 2 Current Alarm			Shunt 2 Alarm		Alarme Courant Shunt2			Al I Shunt2
89	32			15			Shunt 3 Current Alarm			Shunt 3 Alarm		Alarme Courant Shunt3			Al I Shunt3
90	32			15			Shunt 4 Current Alarm			Shunt 4 Alarm		Alarme Courant Shunt4			Al I Shunt4
91	32			15			Shunt 1 Over Current			Shunt 1 OverCur		Sur-Courant Shunt1			Sur-I Shunt1
92	32			15			Shunt 2 Over Current			Shunt 2 OverCur		Sur-Courant Shunt2			Sur-I Shunt2
93	32			15			Shunt 3 Over Current			Shunt 3 OverCur		Sur-Courant Shunt3			Sur-I Shunt3
94	32			15			Shunt 4 Over Current			Shunt 4 OverCur		Sur-Courant Shunt4			Sur-I Shunt4
95	32			15			Times of Communication Fail		Times Comm Fail		Duree du defaut de communication		DureeDefCOM
96	32			15			Existent			Existent		Present					Present
97	32			15			Not Existent				Not Existent		Absent					Absent
98	32			15			Very Low			Very Low		Tres Basse				Tres Basse
99	32			15			Very High			Very High		Tres Haute				Tres Haute
100	32			15			Switch				Switch			Switch					Switch
101	32			15			LVD1 Failure			LVD 1 Failure		Defaut LVD1				Defaut LVD1
102	32			15			LVD2 Failure			LVD 2 Failure		Defaut LVD2				Defaut LVD2
103	32			15			High Temperature Disconnect 1	HTD 1		HTD1				HTD1  
104	32			15			High Temperature Disconnect 2	HTD 2		HTD2				HTD2
105	32			15			Battery LVD			Battery LVD		Contacteur Batteries			Contact. Bat.
106	32			15			No Battery			No Battery		Aucune Batteries			Aucune Bat.
107	32			15			LVD 1				LVD 1			LVD 1					LVD 1
108	32			15			LVD 2				LVD 2			LVD 2					LVD 2
109	32			15			Battery Always On			Batt Always On		Contacteurs Toujours Ferme		Contact. Tj Fer
110	32			15			Barcode				Barcode			Barcode					Barcode
111	32			15			DC Over Voltage				DC Over Volt	Sur Tension DC				Sur U DC
112	32			15			DC Under Voltage			DC Under Volt		Sous Tension DC				Sous U DC
113	32			15			Over Current 1				Over Curr 1		Sur Courant 1				Sur I 1
114	32			15			Over Current 2				Over Curr 2		Sur Courant 2				Sur I 2
115	32			15			Over Current 3				Over Curr 3		Sur Courant 3				Sur I 3
116	32			15			Over Current 4				Over Curr 4		Sur Courant 4				Sur I 4
117	32			15			Existence State			Existence State		Existence State				Existence State
118	32			15			Communication Fail			Comm Fail		Defaut de comunication				Defaut de COM
119	32			15			Bus Voltage Status			Bus Volt Status		Bus Status				Bus Status
120	32			15			Comm OK					Comm OK			Comm OK					Comm OK
121	32			15			All Batteries Comm Fail			AllBattCommFail		Defaut Comm batteries		Def COM Batt
122	32			15			Communication Fail			Comm Fail		Defaut de comunication				Defaut de COM
123	32			15			Rated Capacity				Rated Capacity		Estimation Capacitée			Estimation Cap
124	32			15			Load 5					Load 5			Courant Utilisation 5			I Util. 5

125	32			15			Shunt 1 Voltage			Shunt 1 Voltage		Tension shunt 1			V shunt 1	
126	32			15			Shunt 1 Current			Shunt 1 Current			Courant Shunt 1			I shunt 1
127	32			15			Shunt 2 Voltage			Shunt 2 Voltage		Tension shunt 2			V shunt 2
128	32			15			Shunt 2 Current			Shunt 2 Current			Courant Shunt 2			I shunt 2
129	32			15			Shunt 3 Voltage			Shunt 3 Voltage		Tension shunt 3			V shunt 3
130	32			15			Shunt 3 Current			Shunt 3 Current			Courant Shunt 3			I shunt 3
131	32			15			Shunt 4 Voltage			Shunt 4 Voltage		Tension shunt 4			V shunt 4
132	32			15			Shunt 4 Current			Shunt 4 Current			Courant Shunt 4			I shunt 4
133	32			15			Shunt 5 Voltage			Shunt 5 Voltage		Tension shunt 5			V shunt 5
134	32			15			Shunt 5 Current			Shunt 5 Current			Courant Shunt 5			I shunt 5

170	32			15			Current1 High 1 Current		Curr1 Hi1		Alta1 Corriente 1		Alta1 Corr1
171	32			15			Current1 High 2 Current		Curr1 Hi2		Alta2 Corriente 1		Alta2 Corr1
172	32			15			Current2 High 1 Current			Curr2 Hi1		Alta1 Corriente 2		Alta1 Corr2
173	32			15			Current2 High 2 Current			Curr2 Hi2		Alta2 Corriente 2		Alta2 Corr2
174	32			15			Current3 High 1 Current			Curr3 Hi1		Alta1 Corriente 3		Alta1 Corr3
175	32			15			Current3 High 2 Current			Curr3 Hi2		Alta2 Corriente 3		Alta2 Corr3
176	32			15			Current4 High 1 Current			Curr4 Hi1		Alta1 Corriente 4		Alta1 Corr4
177	32			15			Current4 High 2 Current			Curr4 Hi2		Alta2 Corriente 4		Alta2 Corr4
178	32			15			Current5 High 1 Current			Curr5 Hi1		Alta1 Corriente 5		Alta1 Corr5
179	32			15			Current5 High 2 Current			Curr5 Hi2		Alta2 Corriente 5		Alta2 Corr5

220	32			15			Current1 High 1 Current Limit		Curr1 Hi1 Lmt	Courant1 High 1 Limite Courant1		Cour1 Hi1 Lmt	
221	32			15			Current1 High 2 Current Limit		Curr1 Hi2 Lmt	Courant1 High 2 Limite Courant1		Cour1 Hi2 Lmt	
222	32			15			Current2 High 1 Current Limit		Curr2 Hi1 Lmt	Courant2 High 1 Limite Courant1		Cour2 Hi1 Lmt	
223	32			15			Current2 High 2 Current Limit		Curr2 Hi2 Lmt	Courant2 High 2 Limite Courant1		Cour2 Hi2 Lmt	
224	32			15			Current3 High 1 Current Limit		Curr3 Hi1 Lmt	Courant3 High 1 Limite Courant1		Cour3 Hi1 Lmt	
225	32			15			Current3 High 2 Current Limit		Curr3 Hi2 Lmt	Courant3 High 2 Limite Courant1		Cour3 Hi2 Lmt	
226	32			15			Current4 High 1 Current Limit		Curr4 Hi1 Lmt	Courant4 High 1 Limite Courant1		Cour4 Hi1 Lmt	
227	32			15			Current4 High 2 Current Limit		Curr4 Hi2 Lmt	Courant4 High 2 Limite Courant1		Cour4 Hi2 Lmt	
228	32			15			Current5 High 1 Current Limit		Curr5 Hi1 Lmt	Courant5 High 1 Limite Courant1		Cour5 Hi1 Lmt	
229	32			15			Current5 High 2 Current Limit		Curr5 Hi2 Lmt	Courant5 High 2 Limite Courant1		Cour5 Hi2 Lmt	

270	32			15			Current1 Break Value			Curr 1 Brk Val		Calibre Protection Bat 1	Cal.Prot.Bat.1
271	32			15			Current2 Break Value			Curr 2 Brk Val		Calibre Protection Bat 2	Cal.Prot.Bat.2
272	32			15			Current3 Break Value			Curr 3 Brk Val		Calibre Protection Bat 3	Cal.Prot.Bat.3
273	32			15			Current4 Break Value			Curr 4 Brk Val		Calibre Protection Bat 4	Cal.Prot.Bat.4
274	32			15			Current5 Break Value			Curr 5 Brk Val		Calibre Protection Bat 5	Cal.Prot.Bat.5

281	32			15			Set Shunt Coefficient			Set Shunt Coeff			Config. Param Shunt par Switch		Conf.Sh Switch
#282	32			15			Shunt 5 Coefficient Display		Shunt5 Display		Config. Param Shunt par Switch		Conf.Sh Switch

283	32			15			Shunt1 Coefficient Conflict		Shunt1 Conflict			Conflit Config Shunt 1		Conf.Config.Sh1
284	32			15			Shunt2 Coefficient Conflict		Shunt2 Conflict			Conflit Config Shunt 2		Conf.Config.Sh2
285	32			15			Shunt3 Coefficient Conflict		Shunt3 Conflict			Conflit Config Shunt 3		Conf.Config.Sh3
286	32			15			Shunt4 Coefficient Conflict		Shunt4 Conflict			Conflit Config Shunt 4		Conf.Config.Sh4
287	32			15			Shunt5 Coefficient Conflict		Shunt5 Conflict			Conflit Config Shunt 5		Conf.Config.Sh5

290	32			15			By Software					By Software			Par Software			Par Software
291	32			15			By Dip-Switch				By Dip-Switch			Par Switch			Par Switch
292	32			15			Not Supported				Not Supported		Non pris en charge				Non pris charge
293	32			15			Not Used				Not Used				Non Utilisée				Non Utilisée
294	32			15			General				General				General	General
295	32			15			Load						Load						Charge	Charge
296	32			15			Battery					Battery					Batterie					Batterie	
297	32			15			Shunt1 Set As			Shunt1SetAs			Config shunt 1	Conf SH1 
298	32			15			Shunt2 Set As			Shunt2SetAs			Config shunt 2	Conf SH2 
299	32			15			Shunt3 Set As			Shunt3SetAs			Config shunt 3	Conf SH3 
300	32			15			Shunt4 Set As			Shunt4SetAs			Config shunt 4	Conf SH4 
301	32			15			Shunt5 Set As			Shunt5SetAs			Config shunt 5	Conf SH5 
302	32			15			Shunt 1		Shunt 1		Lecture shunt1	Lecture SH1
303	32			15			Shunt 2		Shunt 2		Lecture shunt2	Lecture SH2
304	32			15			Shunt 3		Shunt 3		Lecture shunt3	Lecture SH3
305	32			15			Shunt 4		Shunt 4		Lecture shunt4	Lecture SH4
306	32			15			Shunt 5		Shunt 5		Lecture shunt5	Lecture SH5
307	32			15			Source 1		Source 1		Source 1		Source 1
308	32			15			Source 2		Source 2		Source 2		Source 2
309	32			15			Source 3		Source 3		Source 3		Source 3
310	32			15			Source 4		Source 4		Source 4		Source 4
311	32			15			Source 5		Source 5		Source 5		Source 5
500	32			15			Current1 High 1 Curr			Curr1 Hi1Cur		Alta1 Corriente 1		Alta1 Corr1	
501	32			15			Current1 High 2 Curr			Curr1 Hi2Cur		Alta2 Corriente 1		Alta2 Corr1	
502	32			15			Current2 High 1 Curr			Curr2 Hi1Cur		Alta1 Corriente 2		Alta1 Corr2	
503	32			15			Current2 High 2 Curr			Curr2 Hi2Cur		Alta2 Corriente 2		Alta2 Corr2	
504	32			15			Current3 High 1 Curr			Curr3 Hi1Cur		Alta1 Corriente 3		Alta1 Corr3	
505	32			15			Current3 High 2 Curr			Curr3 Hi2Cur		Alta2 Corriente 3		Alta2 Corr3	
506	32			15			Current4 High 1 Curr			Curr4 Hi1Cur		Alta1 Corriente 4		Alta1 Corr4	
507	32			15			Current4 High 2 Curr			Curr4 Hi2Cur		Alta2 Corriente 4		Alta2 Corr4	
508	32			15			Current5 High 1 Curr			Curr5 Hi1Cur		Alta1 Corriente 5		Alta1 Corr5	
509	32			15			Current5 High 2 Curr			Curr5 Hi2Cur		Alta2 Corriente 5		Alta2 Corr5	

550	32			15			Source		Source		Source			Source

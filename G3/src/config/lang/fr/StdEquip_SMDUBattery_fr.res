﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Battery Current				Batt Current		Courant batterie		Courant bat
2	32			15			Battery Rating(Ah)			Batt Rating(Ah)		Evaluation Batterie(Ah)		Evalua Batt(Ah)
3	32			15			Exceed Current Limit			Exceed Curr Lmt		Depacement limit.Courant	Depace I limit
4	32			15			Battery					Battery			Batterie			Batterie
5	32			15			Over Battery Current			Over Current		Depacement sur-courant		Depace sur-I
6	32			15			Battery Capacity (%)			Batt Cap (%)		Capacite batterie(%)		Capacite bat(%)
7	32			15			Battery Voltage				Batt Voltage		Tension batterie		Tension bat
8	32			15			Low Capacity				Low Capacity		Capacite basse			Capacite basse
9	32			15			On					On			Correct				Correct
10	32			15			Off					Off			Defaut				Defaut
11	32			15			Battery Fuse Voltage			Fuse Voltage		Seuil Detect V Def Protect Bat	V Def Prot Bat
12	32			15			Battery Fuse Status			Fuse Status		Etat Protection Bat		Etat Prot Bat
13	32			15			Fuse Alarm				Fuse Alarm		Alarm Protection Bat		AL Prot Bat
14	32			15			SMDU Battery				SMDU Battery		SMDUBatteries			SMDUBatteries
15	32			15			State					State			Etat				Etat
16	32			15			Off					Off			Arrêt				Arrêt
17	32			15			On					On			Marche				Marche
18	32			15			Switch					Switch			Commutateur			Commutateur
19	32			15			Battery Over Current			Batt Over Curr		Sur Courant Batterie		Sur I Batterie
20	32			15			Battery Management			Batt Management			Carte active dans Process	CI acti Process
21	32			15			Yes					Yes			Oui				Oui
22	32			15			No					No			Non				Non
23	32			15			Over Voltage Limit			Over Volt Limit		Tension haute Niveau 1		V haute Niv.1
24	32			15			Low Voltage Limit			Low Volt Limit		Tension basse niveau 1		V basse Niv.1
25	32			15			Battery Over Voltage			Over Voltage		Tension haute Niveau 2		V haute Niv.2
26	32			15			Battery Under Voltage			Under Voltage		Tension basse niveau 2		V basse Niv.2
27	32			15			Over Current				Over Current		Sur Courant Batterie		Sur I Batterie
28	32			15			Communication Fail			Comm Fail		Perte Communication		Perte Comm.
29	32			15			Times of Communication Fail			Times Comm Fail		Duree du defaut de communication		DureeDefCOM

44	32			15			Battery Temp Number			Batt Temp Num	N° de sonde batterie		N°sonde bat.

87	32			15			No					No		Aucune				Aucune

91	32			15			Temperature 1			Temperature 1		Température 1			Température 1
92	32			15			Temperature 2			Temperature 2		Température 2			Température 2
93	32			15			Temperature 3			Temperature 3		Température 3			Température 3
94	32			15			Temperature 4			Temperature 4		Température 4			Température 4
95	32			15			Temperature 5			Temperature 5		Température 5			Température 5
96	32			15			Rated Capacity				Rated Capacity		Capacité Estimée		Capa Estimée
97	32			15			Battery Temperature			Battery Temp		Température Batterie		Temp Bat
98	32			15			Battery Temperature Sensor		BattTemp Sensor		Capteur Température Batterie	Capt Temp Bat
99	32			15			None					None			Aucune				Aucune
100	32			15			Temperature 1			Temperature 1		Température 1			Température 1
101	32			15			Temperature 2			Temperature 2		Température 2			Température 2
102	32			15			Temperature 3			Temperature 3		Température 3			Température 3
103	32			15			Temperature 4			Temperature 4		Température 4			Température 4
104	32			15			Temperature 5			Temperature 5		Température 5			Température 5
105	32			15			Temperature 6			Temperature 6		Température 6			Température 6
106	32			15			Temperature 7			Temperature 7		Température 7			Température 7
107	32			15			Temperature 8			Temperature 8		Température 8			Température 8
108	32			15			Temperature 9			Temperature 9		Température 9			Température 9
109	32			15			Temperature 10			Temperature 10		Température 10			Température 10
110	32			15			SMDU1 Battery1			SMDU1 Battery1		SMDU1Batteries1		SMDU1Batteries1
111	32			15			SMDU1 Battery2			SMDU1 Battery2		SMDU1Batteries2		SMDU1Batteries2
112	32			15			SMDU1 Battery3			SMDU1 Battery3		SMDU1Batteries3		SMDU1Batteries3
113	32			15			SMDU1 Battery4			SMDU1 Battery4		SMDU1Batteries4		SMDU1Batteries4
114	32			15			SMDU1 Battery5			SMDU1 Battery5		SMDU1Batteries5		SMDU1Batteries5
115	32			15			SMDU2 Battery1			SMDU2 Battery1		SMDU2Batteries1		SMDU2Batteries1
116	32			15			SMDU2 Battery2			SMDU2 Battery2		SMDU2Batteries2		SMDU2Batteries2
117	32			15			SMDU2 Battery3			SMDU2 Battery3		SMDU2Batteries3		SMDU2Batteries3
118	32			15			SMDU2 Battery4			SMDU2 Battery4		SMDU2Batteries4		SMDU2Batteries4
119	32			15			SMDU2 Battery5			SMDU2 Battery5		SMDU2Batteries5		SMDU2Batteries5
120	32			15			SMDU3 Battery1			SMDU3 Battery1		SMDU3Batteries1		SMDU3Batteries1
121	32			15			SMDU3 Battery2			SMDU3 Battery2		SMDU3Batteries2		SMDU3Batteries2
122	32			15			SMDU3 Battery3			SMDU3 Battery3		SMDU3Batteries3		SMDU3Batteries3
123	32			15			SMDU3 Battery4			SMDU3 Battery4		SMDU3Batteries4		SMDU3Batteries4
124	32			15			SMDU3 Battery5			SMDU3 Battery5		SMDU3Batteries5		SMDU3Batteries5
125	32			15			SMDU4 Battery1			SMDU4 Battery1		SMDU4Batteries1		SMDU4Batteries1
126	32			15			SMDU4 Battery2			SMDU4 Battery2		SMDU4Batteries2		SMDU4Batteries2
127	32			15			SMDU4 Battery3			SMDU4 Battery3		SMDU4Batteries3		SMDU4Batteries3
128	32			15			SMDU4 Battery4			SMDU4 Battery4		SMDU4Batteries4		SMDU4Batteries4
129	32			15			SMDU4 Battery5			SMDU4 Battery5		SMDU4Batteries5		SMDU4Batteries5
130	32			15			SMDU5 Battery1			SMDU5 Battery1		SMDU5Batteries1		SMDU5Batteries1
131	32			15			SMDU5 Battery2			SMDU5 Battery2		SMDU5Batteries2		SMDU5Batteries2
132	32			15			SMDU5 Battery3			SMDU5 Battery3		SMDU5Batteries3		SMDU5Batteries3
133	32			15			SMDU5 Battery4			SMDU5 Battery4		SMDU5Batteries4		SMDU5Batteries4
134	32			15			SMDU5 Battery5			SMDU5 Battery5		SMDU5Batteries5		SMDU5Batteries5
135	32			15			SMDU6 Battery1			SMDU6 Battery1		SMDU6Batteries1		SMDU6Batteries1
136	32			15			SMDU6 Battery2			SMDU6 Battery2		SMDU6Batteries2		SMDU6Batteries2
137	32			15			SMDU6 Battery3			SMDU6 Battery3		SMDU6Batteries3		SMDU6Batteries3
138	32			15			SMDU6 Battery4			SMDU6 Battery4		SMDU6Batteries4		SMDU6Batteries4
139	32			15			SMDU6 Battery5			SMDU6 Battery5		SMDU6Batteries5		SMDU6Batteries5
140	32			15			SMDU7 Battery1			SMDU7 Battery1		SMDU7Batteries1		SMDU7Batteries1
141	32			15			SMDU7 Battery2			SMDU7 Battery2		SMDU7Batteries2		SMDU7Batteries2
142	32			15			SMDU7 Battery3			SMDU7 Battery3		SMDU7Batteries3		SMDU7Batteries3
143	32			15			SMDU7 Battery4			SMDU7 Battery4		SMDU7Batteries4		SMDU7Batteries4
144	32			15			SMDU7 Battery5			SMDU7 Battery5		SMDU7Batteries5		SMDU7Batteries5
145	32			15			SMDU8 Battery1			SMDU8 Battery1		SMDU8Batteries1		SMDU8Batteries1
146	32			15			SMDU8 Battery2			SMDU8 Battery2		SMDU8Batteries2		SMDU8Batteries2
147	32			15			SMDU8 Battery3			SMDU8 Battery3		SMDU8Batteries3		SMDU8Batteries3
148	32			15			SMDU8 Battery4			SMDU8 Battery4		SMDU8Batteries4		SMDU8Batteries4
149	32			15			SMDU8 Battery5			SMDU8 Battery5		SMDU8Batteries5		SMDU8Batteries5

151	32			15			Battery 1			Batt 1			Batterie 1			Batt 1
152	32			15			Battery 2			Batt 2			Batterie 2			Batt 2
153	32			15			Battery 3			Batt 3			Batterie 3			Batt 3
154	32			15			Battery 4			Batt 4			Batterie 4			Batt 4
155	32			15			Battery 5			Batt 5			Batterie 5			Batt 5


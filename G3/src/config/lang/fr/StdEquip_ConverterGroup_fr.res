﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE			
1	32			15			Converter Group				Converter Grp		Groupe Convertisseurs			Groupe Convert.
2	32			15			Total Current				Tot Conv Curr		Courant Total				Courant Conv
3	32			15			Voltage					Conv Voltage		Tension Moyenne				Tension Conv
4	32			15			Converter Capacity Used			Conv Cap Used		Pourcentage de Charge			Pourcent Charge
5	32			15			Maximum Capacity Used			Max Cap Used		Pourcentage Charge Max			Pourc.Chg Max
6	32			15			Minimum Capacity Used			Min Cap Used		Pourcentage Charge Min			Pourc.Chg Min
7	32			15			Total Converters Communicating		Num Convs Comm		Nombre Convert. en comm.	Nb Conv Com.
8	32			15			Valid Converters			Valid Convs		Convertisseur Valide			Conv.Valide
9	32			15			Number of Converters			Conv Num	Numéro du Convertisseur			Num ro Conv
10	32			15			Converter AC Fail State		Conv AC Fail		Etat Défaut AC convertisseurs		Déf AC Conv
11	32			15			Multi-Converters Fail Status		Multi-Conv Fail		Défaut Multiple Convertisseur		Déf +1 Convert
12	32			15			Converter Current Limit			Conv Curr Limit			Limit Courrant Conv.	Limit I Convert
13	32			15			Converter Trim				Converter Trim			Limitation Convertisseurs		Limit Convert
14	32			15			DC On/Off Control			DC On/Off Ctrl		Controle DC				Controle DC
15	32			15			AC On/Off Control			AC On/Off Ctrl		Controle AC				Controle AC
16	32			15			Converters LED Control			Conv LED Ctrl		Controle LEDs				Controle LEDs
17	32			15			Fan Speed Control			Fan Speed Ctrl		Controle ventilateurs			Controle Ventl
18	32			15			Rated Voltage				Rated Voltage		Tension Convertisseurs			V Convert 
19	32			15			Rated Current				Rated Current		Courant Convertisseurs			I Convert
20	32			15			High Voltage Shutdown Limit		HVSD Limit		Limite Tension Haute			Limite V Haute
21	32			15			Low Voltage Limit			Low Volt Limit			Limite Tension Basse			Limite V Basse
22	32			15			High Temperature Limit			High Temp Limit		Limite Température Haute		Limit Temp Haut
24	32			15			HVSD Restart Time			HVSD Restart T		Tempo Démarrage Surtension		Tempo Dém Sur U
25	32			15			Walk-In Time				Walk-In Time		Temporisation Démarrage lent		Tempo Dém lent 
26	32			15			Walk-In					Walk-In			Active Démarrage Lent			Act Dém Lent
27	32			15			Minimum Redundancy			Min Redundancy		Redondance Minimum			Redond Min
28	32			15			Maximum Redundancy			Max Redundancy		Redondance Maximum			Redond Max
29	32			15			Turn Off Delay				Turn Off Delay		Retard à l'arrêt			Retardàl'arrêt
30	32			15			Cycle Period				Cycle Period		Périodicitée du Cycle			Périod Cycle
31	32			15			Cycle Activation Time			Cyc Active Time		Durée du Cycle				Durée du Cycle
32	32			15			Converter AC Fail			Conv AC Fail		Défaut AC Convertisseur			Déf AC Conv.
33	32			15			Multiple Converters Fail		Multi-Conv Fail			Défaut Multi Convertisseur			Défaut +1 Conv.
36	32			15			Normal					Normal			Normal					Normal
37	32			15			Fail					Fail			Défaut					Défaut	
38	32			15			Switch Off All				Switch Off All		Tous redresseurs à l'Arrêt		Tous red Arrêt
39	32			15			Switch On All				Switch On All		Tous redresseurs en Marche		Tous red Marche
42	32			15			All Flashing				All Flashing		Clignotement				Clignotement
43	32			15			Stop Flashing				Stop Flashing		Fixe					Fixe
44	32			15			Full Speed				Full Speed		Vitesse maximum				Vitesse max	
45	32			15			Automatic Speed				Auto Speed		Vitesse Automatique			Vitesse Auto
46	32			32			Current Limit Control			Curr Limit Ctrl			Controle limite de courant		Control limit I
47	32			32			Full Capability Control			Full Cap Ctrl		Puissance maximum			Puissance max
54	32			15			Disabled				Disabled		Dés actif				Dés actif
55	32			15			Enabled					Enabled			Actif					Actif
68	32			15			System ECO				System ECO		Mis en attente				Mis en attente
72	32			15			Turn On when AC Over Voltage		Turn On AC Ov V			Marche Forcée Surtension AC		Mar.ForcSurV.AC
73	32			15			No					No			Non					Non
74	32			15			Yes					Yes			Oui					Oui
77	32			15			Pre-CurrLimit on Turn-On Enabled	Pre-Curr Limit		Pre Limitation				Pre Limitation
78	32			15			Converter Power Type			Conv Power Type	Type Convertisseur				Type Conv.
79	32			15			Double Supply				Double Supply			Double Entrée				Double Entrée
80	32			15			Single Supply				Single Supply		Simple Entrée				Simple Entrée
81	32			15			Last Converters Quantity		Last Convs Qty		Dernière Quantité Convertisseur		Der.Quant Conv.	
82	32			15			Converter Lost				Converter Lost		Perte Convertisseur			Perte Convert
83	32			15			Converter Lost				Converter Lost		Perte Convertisseur			Perte Convert
84	32			15			Clear Converter Lost Alarm		Clear Conv Lost		Reset Perte Convertisseur		ResetPerteConv
85	32			15			Clear					Clear			Effacement				Effacement
86	32			15			Confirm Converter ID			Confirm ID		Confirmation ID Convertisseur	Confirm ID Conv
87	32			15			Confirm					Confirm			Confirmation				Confirmation
88	32			15			Best Operating Point			Best Oper Pt		Meilleur Point				Meilleur Point
89	32			15			Converter Redundancy			Conv Redundancy			Redondance Convertisseur		Redond.Convert.
90	32			15			Load Fluctuation Range			Fluct Range		Pourcentage Equilibrage			% Equilibrage
91	32			15			System Energy Saving Point		Energy Save Pt			Meilleur Point de fonctionnement	Meil P.Fonct
92	32			15			E-Stop Function				E-Stop Function	Fonction E-Stop				Fonction E-Stop
93	32			15			AC Phases				AC Phases		Nb Phases				Nb Phases
94	32			15			Single Phase				Single Phase		Entrée Monophasé			Entrée 1 Phase
95	32			15			Three Phases				Three Phases		Entrée Tri-Phasee			Entrée 3 Phases
96	32			15			Input Current Limit			Input Curr Lmt		Limit Courant AC			Limit I AC
97	32			15			Double Supply				Double Supply		Double Entrée AC			2 Entrées AC
98	32			15			Single Supply				Single Supply		Simple Entrée AC			1 Entrée AC
99	32			15			Small Supply				Small Supply		Petite Puissance			Petite Puissanc
100	32			15			Restart on HVSD				Restart on HVSD		Redémarrage Redres. sur SurV DC		Dem Red Surt DC
101	32			15			Sequence Start Interval			Start Interval		Demarrage Sequentiel			Dem. Sequentiel
102	32			15			Rated Voltage				Rated Voltage		Tension de repli			Tension repli
103	32			15			Rated Current				Rated Current		Courant max de repli			Imax repli
104	32			15			All Converters Comm Fail		AllConvCommFail		Pas de Réponse Convertisseur		Pas de Rép.Con.
105	32			15			Inactive				Inactive		Désactive				Désactive
106	32			15			Active					Active			Active					Active
107	32			15			Converter Redundancy Active		Redund Active			Activation ECO Mode			Activ ECO Mode
108	32			15			Existence State				Existence State		Détection				Détection
109	32			15			Existent				Existent		Présent					Présent	
110	32			15			Not Existent				Not Existent		Non Présent				Non Présent
111	32			15			Average Current				Average Current		Courant Moyen				Courant Moyen
112	32			15			Default Current Limit			Current Limit		Limitation Courant			Limit.Courant
113	32			15			Output Voltage				Output Voltage		Tension de sortie		U sortie
114	32			15			Under Voltage				Under Voltage		Sous Tension				Sous Tension
115	32			15			Over Voltage				Over Voltage		Sur Tension				Sur Tension
116	32			15			Over Current				Over Current		Sur Courant				Sur Courant
117	32			15			Average Voltage				Average Voltage		Tension Moyenne				U Moyenne
118	32			15			HVSD Limit				HVSD Limit		Limite HVSD				Limite HVSD
119	32			15			HVSD Limit				HVSD Limit	Limite HVSD				Limite HVSD	
120	32			15			Clear Converter Comm Fail		ClrConvCommFail		Reset Défaut Communication		Reset Déf.Comm.

121	32			15			HVSD					HVSD			HVSD					HVSD
135	32			15			Current Limit Point			Curr Limit Pt		Limitation Courant Point		Limit.CourantPt
136	32			15			Current Limit				Current Limit		Limitation Courant			Limit.Courant
137	32			15			Maximum Current Limit Value		Max Curr Limit		Limitation Courant Max			Limit.Cour.Max
138	32			15			Default Current Limit Point		Def Curr Lmt Pt		Défaut Limitation Courant		Def.Limit.Cour.
139	32			15			Minimize Current Limit Value		Min Curr Limit				Limitation Courant Min			Limit.Cour.Min

290	32			15			Over Current				Over Current		Sur Courant				Sur Courant
291	32			15			Over Voltage				Over Voltage		Sur Tension				Sur Tension
292	32			15			Under Voltage				Under Voltage		Tension basse				Tension basse
293	32			15			Clear All Converters Comm Fail		ClrAllConvCommF		Reset Tous Défaut Communication		Reset Déf.Comm.

294	32			15			All Conv Comm Status			All Conv Status			Etat com. tous Conv.		Etat com.Conv.
295	32			15			Converter Trim(24V)			Conv Trim(24V)		Conv Trim(24V)				Conv Trim(24V)
296	32			15			Last Conv Qty(Internal Use)		LastConvQty(P)		Dernier nombre convertissseur		Dernier Nb Conv
297	32			15			Def Currt Lmt Pt(Inter Use)		DefCurrLmtPt(P)		Defaut limitation courant		Def Lim Courant
298	32			15			Converter Protect			Conv Protect		Protection convertisseur		Prot Conv
299	32			15			Input Rated Voltage			Input RatedVolt		Tension d'entrée nominale		TensioEntreeNom	
300	32			15			Total Rated Current			Total RatedCurr		Courant d'entrée nominale		CouranEntreeNom	
301	32			15			Converter Type				Conv Type		Type de convertisseur	Type de CV	
302	32			15			24-48V Conv				24-48V Conv		Convertisseur 24-48V		Convert 24-48V
303	32			15			48-24V Conv				48-24V Conv		Convertisseur 48-24V		Convert 48-24V
304	32			15			400-48V Conv				400-48V Conv		Convertisseur 400-48V		Convert 400-48V	
305	32			15			Total Output Power			Output Power		Puissance de sortie totale			W - TotalSortie
306	32			15			Reset Converter IDs			Reset Conv IDs		Reset Convertisseur IDs	Reset Conv IDs

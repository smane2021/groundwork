﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			Fuel Tank Group				Fuel Tank Grp		Grupo Depósito Combustible	Grupo Dp Gasoil
#2		32			15			Standby				Standby		Standby				Standby
#3		32			15			Refresh				Refresh			Refrescar			Refrescar
#4		32			15			Setting Refresh			Setting Refresh		Ajuste Refresco			Ajuste Refresco
6		32			15			No				No			No				No
7		32			15			Yes				Yes			Sí				Sí
10		32			15			Fuel Surveillance Failure	Fuel Surve Fail		Fallo vigilante Combustible	Fallo Vig Comb
20		32			15			Fuel Group Communication Failure	Fuel Comm Fail		Fallo Comunicación G Combust	Fallo COM Comb
21		32			15			Fuel Number			Fuel Number		Número de Combustible		Núm Combustible
22		32			15			0				0			0				0
23		32			15			1				1			1				1
24		32			15			2				2			2				2
25		32			15			3				3			3				3
26		32			15			4				4			4				4
27		32			15			5				5			5				5
28		32			15			OB Fuel Number			OB Fuel Number		Nombre de reservoir externe		Nb ReservoirExt







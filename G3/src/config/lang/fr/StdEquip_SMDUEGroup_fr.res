﻿#
#  Locale language support:French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			SMDUE Group		SMDUE Group		Groupe SMDUE		Groupe SMDUE
2		32			15			Number of SMDUEs	Num of SMDUEs		Nom SMDUE		Nom SMDUE
3		32			15			Communication Fail	Comm Fail		Échec Communication		Échec Com
4		32			15			Existence State		Existence State		Etat Existence		Etat Exist
5		32			15			Comm OK			Comm OK			Comm OK		Comm OK
6		32			15			Communication Fail	Comm Fail		Échec Communication		Échec Com
7		32			15			Existent		Existent		Existant			Existant
8		32			15			Not Existent		Not Existent		Non existant			Non existant

9		32			15			SMDUE Changed(LCD)		SMDUE Chg(LCD)		SMDUE Changé(LCD)			SMDUE Chg(LCD)
10		32			15			SMDUE Changed(WEB)		SMDUE Chg(WEB)		SMDUE Changé(WEB)			SMDUE Chg(WEB)
11		32			15			Total SMDUELoad Current		Total Load Curr		SMDUE CourChargeTotal	CourChargeTotal
12		32			15			Not Changed		Not Changed		Inchangé			Inchangé
13		32			15			Changed			Changed			Changé				Changé

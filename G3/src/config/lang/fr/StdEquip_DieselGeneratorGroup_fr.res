﻿#
#  Locale language support:french
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN			FULL_IN_LOCALE			ABBR_IN_LOCALE			
1		32			15		Last Diesel Generator Test Date		Last Test Date				Date du dernier Test		Dernier Test				
2		32			15		Diesel Generator Test Running		Test Running			Test en cours			Test en cours				
3		32			15		Diesel Generator Test Results		Test Results			Résultat du Test		Résultat Test				
4		32			15		Start Diesel Generator Test		Start Test			Démarrage du Test		Démarrage Test				
5		32			15		Diesel Generator Max Test Time		Max Test Time			Temps maximum du Test		Temps max Test
6		32			15		Planned Diesel Gen Test			Planned Test			Test électrogène planifié actif		Tst plan actif
7		32			15		Diesel Generator Control Inhibit	Control Inhibit			Inhibition diesel		Inhi. diesel				
8		32			15		Diesel Generator Test Running		Test Running		Test en cours			Test en cours				
9		32			15		Diesel Generator Test Failure		Test Failure			Défaut Test			Défaut Test				
10		32			15		No					No				Non				Non
11		32			15		Yes					Yes			Oui				Oui
12		32			15		Reset Diesel Gen Test Error		Reset Test Err			RAZ défaut Test			RAZ défaut Test				
13		32			15		New Diesel Generator Test Delay		New Test Delay			Délais nouveau Test		Délais nou.Test			
14		32			15		Num of Scheduled Tests per Year		Yr Test Times			Numero Test			No Test					
15		32			15		Test 1 Date				Test 1 Date			Date Test 1			Date Test 1				
16		32			15		Test 2 Date				Test 2 Date			Date Test 2			Date Test 2				
17		32			15		Test 3 Date				Test 3 Date			Date Test 3			Date Test 3				
18		32			15		Test 4 Date				Test 4 Date			Date Test 4			Date Test 4				
19		32			15		Test 5 Date				Test 5 Date			Date Test 5			Date Test 5				
20		32			15		Test 6 Date				Test 6 Date			Date Test 6			Date Test 6				
21		32			15		Test 7 Date				Test 7 Date			Date Test 7			Date Test 7				
22		32			15		Test 8 Date				Test 8 Date			Date Test 8			Date Test 8				
23		32			15		Test 9 Date				Test 9 Date			Date Test 9			Date Test 9				
24		32			15		Test 10 Date				Test 10 Date			Date Test 10			Date Test 10				
25		32			15		Test 11 Date				Test 11 Date			Date Test 11			Date Test 11				
26		32			15		Test 12 Date				Test 12 Date			Date Test 12			Date Test 12				
27		32			15		Normal					Normal				Normal				Normal					
28		32			15		End by Manual				End by Manual			Arrêt manuel			Arrêt manuel				
29		32			15		Time is Up				Time is Up			Dépacement de temps		Dépace. temps
30		32			15		In Manual State				In Manual State			Etat manuel			Etat manuel				
31		32			15		Low Battery Voltage			Low Batt Volt			Tension basse batterie		V bas batt			
32		32			15		High Water Temperature			High Water Temp			Température eau haute		Temp. eau haute				
33		32			15		Low Oil Pressure			Low Oil Press			Pression d huile basse		Pres. huile bas				
34		32			15		Low Fuel Level				Low Fuel Level			Niveau bas fuel			Niv bas fuel				
35		32			15		Diesel Generator Failure		Diesel Gen Fail				Défaut GrpElec			Défaut GrpElec				
36		32			15		Diesel Generator Group			Dsl Gen Group			Groupe électrogén		Grp électrogén				
37		32			15		State					State				Etat				Etat
38		32			15		Existence State				Existence State			Module présent			Module présent	
39		32			15		Existent				Existent			Présent				Présent
40		32			15		Not Existent				Not Existent			Non Présent			Non Présent
41		32			15			Total Input Current			Input Current			Courant d'entrée total			CouranEntrTotal	






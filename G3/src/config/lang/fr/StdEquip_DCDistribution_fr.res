﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			DC Distribution			DC Distr			Distribution DC		Distrib DC
2		32			15			DC Voltage			DC Voltage		Tension DC		Tension DC
3		32			15			Load Current			LoadCurr		Courant de charge	I charge
4		32			15			Load Shunt			Load Shunt		Activation shunt Util	Activ SH Util
5		32			15			Disabled			Disabled		Desactive		Desactive
6		32			15			Enabled				Enabled			Active			Active
7		32			15			Shunt Current			Shunt Current		Courant Shunt		Courant Shunt
8		32			15			Shunt Voltage			Shunt Voltage		Tension Shunt		Tension Shunt
9		32			15			Load Shunt Exist		LoadShuntExist		Shunt Présent		Shunt Présent
10		32			15			Yes				Yes			Oui			Oui
11		32			15			No				No			Non			Non
12		32			15			Over Voltage 1			Over Voltage 1		Tension DC haute		V DC haut
13		32			15			Over Voltage 2			Over Voltage 2	Tension DC Tres haute		V DC Tres haut
14		32			15			Under Voltage 1			Under Voltage 1		Tension DC Basse		V DC Basse
15		32			15			Under Voltage 2			Under Voltage 2	Tension DC Tres Basse		V DC Tres Basse
16		32			15			Over Voltage 1 (24V)		24V Over Volt1		Tension DC haute(24V)		V DC haut(24V)
17		32			15			Over Voltage 2 (24V)		24V Over Volt2			Tension DC Tres haute(24V)	V DC Tres haut
18		32			15			Under Voltage 1 (24V)		24V Under Volt1		Tension DC Basse(24V)		V DC Basse(24V)
19		32			15			Under Voltage 2 (24V)		24V Under Volt2		Tension DC Tres Basse(24V)	V DC Tres Basse
20		32				15			Total Load Current			TotalLoadCurr		Corriente total carga		Total carga
500	32			15			Current Break Size				Curr1 Brk Size		Pause Taille Courant		PauseTail.Cour1																														
501	32			15			Current High 1 Current Limit			Curr1 Hi1 Limit		Courant High 1 Limite Courant	Cour1 Hi1 Lmt																															
502	32			15			Current High 2 Current Limit		Curr1 Hi2 Lmt		Courant High 2 Limite Courant	Cour1 Hi2 Lmt																																
503	32			15			Current High 1 Curr		Curr Hi1Cur		Courant High 1 Courant			Courant Hi 1																																															
504	32			15			Current High 2 Curr		Curr Hi2Cur		Courant High 2 Courant			Courant Hi 2
﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			DC Meter Group			DCMeter Group		Groupe Mesure DC	Gr.Mesure DC
2		32			15			DC Meter Number			DCMeter Num		Nombre de Mesure DC	Nb.Mesure DC
3		32			15			Communication Fail	Comm Fail		Défaut Communication	Déf.Com.
4		32			15			Existence State			Existence		Présence		Présence
5		32			15			Existent			Existent		Présent			Présent
6		32			15			Not Existent			Not Existent		Innexistant		Innexistant

11		32			15			DC Meter Lost		DCMeter Lost		Perte Module Mesure	Perte ModMesure
12		32			15			DC Meter Num Last Time	LastDCMeter Num	Nombre de Modules	Nombre Modules
13		32			15			Clear DC Meter Lost Alarm	ClrDCMeterLost		Reset Perte Module	Reset Perte
14		32			15			Clear				Clear			Effacement		Effacement
15		32			15			Total Energy Consumption	TotalEnergy		Puissance Totale	P.Totale

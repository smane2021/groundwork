﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name 
    
[LOCALE_LANGUAGE]
fr



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			AC Distribution Number		AC Distri Num		Numéro Distribution AC		Num Distr AC
2		32			15			LargeDU AC Distribution Group	LargeDUACDistGr			Groupe Distribution AC		Grpe Distrib AC
3		32			15			Over Voltage Limit		Over Volt Limit		Niveau Sous Tension		Sous Tension
4		32			15			Under Voltage Limit		Under Volt Lmt	Niveau Sur Tension		Sur Tension
5		32			15			Phase Failure Voltage		Phase Fail Volt	Défaut Phase			Défaut Phase
6		32			15			Over Frequency Limit		Over Freq Limit			Limite Fréquence Haute		Lim Fréqu Haute
7		32			15			Under Frequency Limit		Under Freq Lmt	Limite Fréquence Basse		Lim Fréqu Basse
8		32			15			Mains Failure			Mains Failure		Défaut Secteur			Défaut Secteur
9		32			15			Noraml				Normal			Normal				Normal
10		32			15			Alarm				Alarm			Alarme				Alarme
11		32			15			Mains Failure			Mains Failure		Défaut Secteur			Défaut Secteur
12		32			15			Existence State			Existence State		Détection			Détection
13		32			15			Existent			Existent		Présent				Présent
14		32			15			Not Existent			Not Existent		Non Présent			Non Présent

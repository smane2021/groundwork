﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr



[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE			
1	32			15		Fuse 1			Fuse 1			Utilisation 1		Utilisation 1
2	32			15		Fuse 2			Fuse 2			Utilisation 2		Utilisation 2
3	32			15		Fuse 3			Fuse 3			Utilisation 3		Utilisation 3
4	32			15		Fuse 4			Fuse 4			Utilisation 4		Utilisation 4
5	32			15		Fuse 5			Fuse 5			Utilisation 5		Utilisation 5
6	32			15		Fuse 6			Fuse 6			Utilisation 6		Utilisation 6
7	32			15		Fuse 7			Fuse 7			Utilisation 7		Utilisation 7
8	32			15		Fuse 8			Fuse 8			Utilisation 8		Utilisation 8
9	32			15		Fuse 9			Fuse 9			Utilisation 9		Utilisation 9
10	32			15		Fuse 10			Fuse 10			Utilisation 10		Utilisation 10
11	32			15		Fuse 11			Fuse 11			Utilisation 11		Utilisation 11
12	32			15		Fuse 12			Fuse 12			Utilisation 12		Utilisation 12
13	32			15		Fuse 13			Fuse 13			Utilisation 13		Utilisation 13
14	32			15		Fuse 14			Fuse 14			Utilisation 14		Utilisation 14
15	32			15		Fuse 15			Fuse 15			Utilisation 15		Utilisation 15
16	32			15		Fuse 16			Fuse 16			Utilisation 16		Utilisation 16
17	32			15		SMDU DC Fuse		SMDU DC Fuse		SMDU DC Fuse				SMDU DC Fuse
18	32			15			State				State			Etat					Etat
19	32			15			Off				Off			Off					Off
20	32			15			On				On			On					On
21	32			15		Fuse 1 Alarm		DC Fuse 1 Alm		Alarme Protec DC 1			Al Protec DC 1
22	32			15		Fuse 2 Alarm		DC Fuse 2 Alm		Alarme Protec DC 2			Al Protec DC 2
23	32			15		Fuse 3 Alarm		DC Fuse 3 Alm		Alarme Protec DC 3			Al Protec DC 3
24	32			15		Fuse 4 Alarm		DC Fuse 4 Alm		Alarme Protec DC 4			Al Protec DC 4
25	32			15		Fuse 5 Alarm		DC Fuse 5 Alm		Alarme Protec DC 5			Al Protec DC 5
26	32			15		Fuse 6 Alarm		DC Fuse 6 Alm		Alarme Protec DC 6			Al Protec DC 6
27	32			15		Fuse 7 Alarm		DC Fuse 7 Alm		Alarme Protec DC 7			Al Protec DC 7
28	32			15		Fuse 8 Alarm		DC Fuse 8 Alm		Alarme Protec DC 8			Al Protec DC 8
29	32			15		Fuse 9 Alarm		DC Fuse 9 Alm		Alarme Protec DC 9			Al Protec DC 9
30	32			15		Fuse 10 Alarm		DC Fuse 10 Alm		Alarme Protec DC 10			Al Protec DC 10
31	32			15		Fuse 11 Alarm		DC Fuse 11 Alm		Alarme Protec DC 11			Al Protec DC 11
32	32			15		Fuse 12 Alarm		DC Fuse 12 Alm		Alarme Protec DC 12			Al Protec DC 12
33	32			15		Fuse 13 Alarm		DC Fuse 13 Alm		Alarme Protec DC 13			Al Protec DC 13
34	32			15		Fuse 14 Alarm		DC Fuse 14 Alm		Alarme Protec DC 14			Al Protec DC 14
35	32			15		Fuse 15 Alarm		DC Fuse 15 Alm		Alarme Protec DC 15			Al Protec DC 15
36	32			15		Fuse 16 Alarm		DC Fuse 16 Alm		Alarme Protec DC 16			Al Protec DC 16
37	32			15			Times of Communication Fail		Times Comm Fail			Duree du defaut de communication		DureeDefCOM
38	32			15			Communication Fail	Comm Fail		Defaut de comunication		Defaut de COM
39	32			15			Load 1 Current			Load 1 Current		Courant Utilisation 1			I Util 1
40	32			15			Load 2 Current			Load 2 Current		Courant Utilisation 2			I Util 2
41	32			15			Load 3 Current			Load 3 Current		Courant Utilisation 3			I Util 3
42	32			15			Load 4 Current			Load 4 Current		Courant Utilisation 4			I Util 4
43	32			15			Load 5 Current			Load 5 Current		Courant Utilisation 5			I Util 5

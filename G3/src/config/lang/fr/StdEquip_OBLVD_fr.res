﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			LVD Unit			LVD Unit		Unitée Contacteur			Unitée LVD
11		32			15			Connected			Connected		Connecté				Connecté
12		32			15			Disconnected			Disconnected		Deconnecté				Deconnecté
13		32			15			No				No			Non					Non
14		32			15			Yes				Yes			Oui					Oui
21		32			15			LVD 1 Status			LVD 1 Status	Etat LVD 1				Etat LVD 1
22		32			15			LVD 2 Status			LVD 2 Status	Etat LVD 2				Etat LVD 2
23		32			15			LVD 1 Fail			LVD 1 Fail		Defaut LVD 1				Defaut LVD 1
24		32			15			LVD 2 Fail			LVD 2 Fail		Defaut LVD 2				Defaut LVD 2
25		32			15			Communication Fail		Comm Fail		Defaut communication			Defaut Comm.
26		32			15			State				State			Etat					Etat
27		32			15			LVD 1 Control			LVD 1 Control		Control LVD 1				Control LVD 1
28		32			15			LVD 2 Control			LVD 2 Control		Control LVD 2				Control LVD 2
31		32			15			LVD 1				LVD 1		LVD 1				LVD 1
32		32			15			LVD 1 Mode			LVD 1 Mode		Mode Commande Contacteur 1		Mode Cmd LVD 1
33		32			15			LVD 1 Voltage			LVD 1 Voltage		Tension Deconnexion Contacteur 1	Tens Decon LVD1
34		32			15			LVD 1 Reconnect Voltage		LVD1 Recon Volt		Tension Reconnexion Contacteur 1	Tens Recon LVD1
35		32			15			LVD 1 Reconnect Delay		LVD1 ReconDelay		Retard Reconnexion Contacteur 1		Retard RecnLVD1
36		32			15			LVD 1 Time			LVD 1 Time		CMD Contacteur 1 Sur Duree		CMD LVD1 Duree
37		32			15			LVD 1 Dependency			LVD1 Dependency		Dependance Contacteur 1			Depend. LVD1
41		32			15			LVD 2				LVD 2		Activation Contacteur 2			Activation LVD2
42		32			15			LVD 2 Mode			LVD 2 Mode		Mode Commande Contacteur 2		Mode Cmd LVD2
43		32			15			LVD 2 Voltage			LVD 2 Voltage		Tension Deconnexion Contacteur 2	Tens Decon LVD2
44		32			15			LVD 2 Reconnect Voltage		LVD2 Recon Volt		Tension Reconnexion Contacteur 2	Tens Recon LVD2
45		32			15			LVD 2 Reconnect Delay		LVD2 ReconDelay		Retard Reconnexion Contacteur 2		Retard RecnLVD2
46		32			15			LVD 2 Time			LVD 2 Time		CMD Contacteur 2 Sur Duree		CMD LVD2 Duree
47		32			15			LVD 2 Dependency			LVD2 Dependency		Dependance Contacteur 2			Depend. LVD2
51		32			15			Disabled			Disabled		Desactiver				Desactiver
52		32			15			Enabled				Enabled			Activer					Activer
53		32			15			Voltage			Voltage		Tension					Tension	
54		32			15			Time				Time			Duree					Duree
55		32			15			None				None			Aucun					Aucun
56		32			15			LVD 1				LVD 1			LVD 1			LVD 1
57		32			15			LVD 2				LVD 2			LVD 2			LVD 2
103		32			15			High Temp Disconnect 1		HTD 1				Ouverture LVD1 Sur Temp Haute		HTD1
104		32			15			High Temp Disconnect 2		HTD 2				Ouverture LVD2 Sur Temp Haute		HTD2
105		32			15			Battery LVD			Battery LVD			Contacteur Batterie			LVD Batterie
106		32			15			No Battery			No Battery			Pas de Batterie				Pas de Batterie
107		32			15			LVD 1				LVD 1			LVD 1			LVD 1
108		32			15			LVD 2				LVD 2			LVD 2			LVD 2
109		32			15			Battery Always On		Batt Always On		Contacteur Batt Toujours Ferme		LVDBat.TJ Ferme
110		32			15			LVD Contactor Type		LVD Type		Type Contacteur			Type Contacteur
111		32			15			Bistable			Bistable		Bistable				Bistable
112		32			15			Mono-Stable			Mono-Stable		Mono-stable				Mono-stable
113		32			15			Mono w/Sample			Mono w/Sample		Mono-stable sans detect			Mono sansdetect
116		32			15			LVD 1 Disconnect		LVD1 Disconnect	Contacteur 1 Ouvert			LVD 1 Ouvert
117		32			15			LVD 2 Disconnect		LVD2 Disconnect		Contacteur 2 Ouvert			LVD 2 Ouvert
118		32			15			LVD 1 Mono w/Sample		LVD1 Mono Sampl	LVD1 Mono-stable sans detect		LVD1 Monodetect
119		32			15			LVD 2 Mono w/Sample		LVD2 Mono Sampl	LVD2 Mono-stable sans detect		LVD2 Monodetect
125		32			15			State				State			Etat					Etat
126		32			15			LVD 1 Voltage (24V)		LVD 1 Voltage		Tension Deconnexion Contacteur 1	V DecoLVD1
127		32			15			LVD 1 Reconnect Voltage (24V)	LVD1 Recon Volt			Tension Reconnexion Contacteur 1	Tens Recon LVD1
128		32			15			LVD 2 Voltage (24V)		LVD 2 Voltage		V Deconnexion Contacteur 2(24V)		V DecoLVD2(24V)
129		32			15			LVD 2 Reconnect Voltage (24V)	LVD2 Recon Volt		V Reconnexion Contacteur 2(24V)		V RecoLVD2(24V)
130		32			15			LVD 3				LVD 3			LVD 3			LVD 3



﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			SM Temp Group			SMTemp Group		Groupe SMTemp			Groupe SMTemp
2		32			15			SM Temp Number			SMTemp Num		Numéro de SMTemp		Num. SMTemp
3		32			15			Communication Fail	Comm Fail		Defaut de communication			Defaut COM
4		32			15			Existence State			Existence State		Détection			Détection
5		32			15			Existent			Existent		Présence			Présence
6		32			15			Not Existent		Not Existent		Absent				Absent
11		32			15			SM Temp Lost			SMTemp Lost		Perte SMTemp perdido		Perte SMTemp
12		32			15			SM Temp Num Last Time	SMTemp Num Last		Dernier numéro de SMTemp	Dernier N SMTem
13		32			15			Clear SM Temp Lost Alarm	Clr SMTemp Lost		Effacement Alarme Perte SMTemp	Eff Al Pert SMT
14		32			15			Clear				Clear			Effacement			Effacement

﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
fr



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			LVD1				LVD1			LVD1				LVD1
2		32			15			LVD2				LVD2			LVD2				LVD2
3		32			15			LVD3				LVD3			LVD3				LVD3
4		32			15			DC Distribution Number		DC Distr Number		Numérode Distribution		Num Distrib
5		32			15			LargeDU DC Distribution Group	DC Distr Group		Groupe GDistribution		Groupe GDist
6		32			15			Temp1 High Temperature Limit	Temp1 High Lmt		Limite Température 1 Haute	Lmt Temp1 Haute
7		32			15			Temp2 High Temperature Limit	Temp2 High Lmt		Limite Température 2 Haute	Lmt Temp2 Haute
8		32			15			Temp3 High Temperature Limit	Temp3 High Lmt		Limite Température 3 Haute	Lmt Temp3 Haute
9		32			15			Temp1 Low Temperature Limit	Temp1 Low Lmt		Limite Température 1 Basse	Lmt Temp1 Basse
10		32			15			Temp2 Low Temperature Limit	Temp2 Low Lmt		Limite Température 2 Basse	Lmt Temp2 Basse
11		32			15			Temp3 Low Temperature Limit	Temp3 Low Lmt		Limite Température 3 Basse	Lmt Temp3 Basse
12		32			15			LVD1 Voltage			LVD1 Voltage		Tension LVD1			Tension LVD1
13		32			15			LVD2 Voltage			LVD2 Voltage		Tension LVD2			Tension LVD2
14		32			15			LVD3 Voltage			LVD3 Voltage		Tension LVD3			Tension LVD3
15		32			15			Over Voltage			Over Voltage		Sur Tension			Sur Tension
16		32			15			Under Voltage			Under Voltage		Sous Tension			Sous Tension
17		32			15			On				On			Connecté			Connecté
18		32			15			Off				Off			Ouvert				Ouvert
19		32			15			On				On			Connecté			Connecté
20		32			15			Off				Off			Ouvert				Ouvert
21		32			15			On				On			Connecté			Connecté
22		32			15			Off				Off			Ouvert				Ouvert
23		32			15			Total Load Current		Total Load Curr			Courant Total de Charge		I Total Charge	
24		32			15			Total DC Distribution Current	TotalDCDistCurr		Courant Total de Distribution	I Total Distrib
25		32			15			DC Distribution Average Voltage	DCDistrAveVolt	Tension Moyenne			Tension Moyenne
26		32			15			LVD1			LVD1		LVD1			LVD1 
27		32			15			LVD1 Mode			LVD1 Mode		Mode LVD1			Mode LVD1
28	32			15		LVD1 Time		LVD1 Time		Temporisation LVD1		Tempo LVD1
29		32			15			LVD1 Reconnect Voltage		LVD1 Recon Volt		Tension reconnexion LVD1	Tens rec LVD1
30		32			15			LVD1 Reconnect Delay		LVD1 ReconDelay		Retard reconexion LVD1		Retard rec LVD1
31		32			15			LVD1 Dependency			LVD1 Dependency	Dépendence LVD1			Dépendence LVD1
32		32			15			LVD2			LVD2		Ouverture LVD2			Ouverture LVD2
33		32			15			LVD2 Mode			LVD2 Mode		Mode LVD2			Mode LVD2
34		32			15			LVD2 Time		LVD2 Time		Temporisation LVD2		Tempo LVD2
35		32			15			LVD2 Reconnect Voltage		LVD2 Recon Volt			Tension reconnexion LVD2	Tens rec LVD2
36		32			15			LVD2 Reconnect Delay		LVD2 ReconDelay		Retard reconexion LVD2		Retard rec LVD2
37		32			15			LVD2 Dependency			LVD2 Dependency	Dépendence LVD2			Dépendence LVD2
38		32			15			LVD3			LVD3		LVD3			LVD3
39		32			15			LVD3 Mode			LVD3 Mode		Mode LVD3			Mode LVD3
40		32			15			LVD3 Time		LVD3 Time		Temporisation LVD3		Tempo LVD3
41		32			15			LVD3 Reconnect Voltage		LVD3 Recon Volt	Tension reconnexion LVD3	Tens rec LVD3
42		32			15			LVD3 Reconnect Delay		LVD3 ReconDelay		Retard reconnexion LVD3		Retard rec LVD3
43		32			15			LVD3 Dependency			LVD3 Dependency		Dépendence LVD3			Dépendence LVD3
44		32			15			Disabled			Disabled		Deshabilitada			Deshabilitada
45		32			15			Enabled				Enabled			Habilitada			Habilitada
46		32			15			Voltage				Voltage			Tension				Tension
47		32			15		Time			Time			Tiempo				Tiempo
48		32			15			None				None			Aucun				Aucun
49		32			15			LVD1				LVD1			LVD1				LVD1
50		32			15			LVD2				LVD2			LVD2				LVD2
51		32			15			LVD3				LVD3			LVD3				LVD3
52		32			15			Number of LVD			Num of LVD		Numéro de LVDs			Num LVDs
53		32			15			0				0			0				0
54		32			15			1				1			1				1
55		32			15			2				2			2				2
56		32			15			3				3			3				3
57		32			15			Existence State			Existence State		Détection			Détection
58		32			15			Existent			Existent		Présent				Présent
59		32			15			Not Existent			Not Existent		Non Présent			Non Présent
60		32			15			Battery Over Voltage		Batt Over Volt		Sur Tension Batterie		Sur U Batterie
61		32			15			Battery Under Voltage		Batt Under Volt		Sous Tension Batterie		Sous U Batterie

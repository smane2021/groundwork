﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
51	32			15			SMBRC Unit				SMBRC Unit		Unite SMBRC		Unite SMBRC
95	32			15			Times of Communication Fail		Times Comm Fail		Duree du defaut de communication		DureeDefCOM
96	32			15			Existent				Existent		Existant		Existant
97	32			15			Not Existent				Not Existent		Inexistant			Inexistant
117	32			15			Existence State				Existence State		Etat existant		Etat existant
118	32			15			Communication Fail			Comm Fail		Defaut de comunication		Defaut de COM
120	32			15			Communication OK			Comm OK			Communication OK		COMM OK
121	32			15			All Communication Fail			All Comm Fail		Defaut de communication		Def COM
122	32			15			Communication Fail			Comm Fail		Defaut de comunication		Defaut de COM
123	32			15			Rated Capacity				Rated Capacity		Capacite nominale		Capacite nomin	
150	32			15			Digital Input Number			Dig Input Num		DI Numero		DI Numero
151	32			15			Digital Input 1				Digital Input 1		DI 1		DI 1
152	32			15			Low					Low			Bas		Bas
153	32			15			High					High			Haut			Haut
154	32			15			Digital Input 2				Digital Input 2		DI 2		DI 2
155	32			15			Digital Input 3				Digital Input 3		DI 3		DI 3
156	32			15			Digital Input 4				Digital Input 4		DI 4		DI 4
157	32			15			Num of Digital Out			Num of DO	Num de DO		Num DO	
158	32			15			Digital Output 1			Digital Out 1		DO 1			DO 1
159	32			15			Digital Output 2			Digital Out 2		DO 2			DO 2
160	32			15			Digital Output 3			Digital Out 3		DO 3			DO 3
161	32			15			Digital Output 4			Digital Out 4		DO 4			DO 4
162	32			15			Operation Status			Operation State		État opération		État opération
163	32			15			Normal					Normal			Normal					Normal
164	32			15			Test					Test			Test					Test
165	32			15			Discharge				Discharge		Décharge				Décharge
166	32			15			Calibration				Calibration		Calibration				Calibration
167	32			15			Diagnostic				Diagnostic		Diagnostique				Diagnostique
168	32			15			Maintenance				Maintenance		Maintenance				Maintenance
169	32			15			Internal Resistance TestInterval	InterR Test Int			Intervale Test Resistance		Inter.Test Res.
170	32			15			Ambient Temperature Value		Amb Temp Value		Température Ambiante			Temp. Ambiante
171	32			15			Ambient High Temperature		Amb Temp High		Température Ambiante Haute		Temp. Amb Haute
172	32			15			Battery String Config Number		String Cfg Num		Numéro de configuration			Num Config
173	32			15			Exist Batt Num				Exist Batt Num		Numéro de Batterie			Num Batterie
174	32			15			Unit Seq Num				Unit Seq Num		Numéro de séquence Module		Num séq Module
175	32			15			Start Battery Sequence			Start Batt Seq		Démarrage Bat Séquence			Dém. Bat.Séq
176	32			15			Ambient Low Temperature			Amb Temp Low		Température ambiante Basse		Temp Amb Basse
177	32			15			Ambt Temp Not Used			Amb Temp No Use		Défaut Capteur Temp Ambiante		DéfCapTempAmb

﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
fr



[RES_INFO]
#R_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE		
1		32		15			Bridge Card			Bridge Card		Bridge Card			Bridge Card
2		32		15			Total Batt Curr			Total Batt Curr		Total Batt Curr			Total Batt Curr
3		32		15			Average Batt Volt		Ave Batt Volt	Average Batt Volt		Ave Batt Volt
4		32		15			Average Batt Temp		Ave Batt Temp	Average Batt Temp		Ave Batt Temp
5		32		15			Batt Lost			Batt Lost		Batt Lost			Batt Lost
6		32		15			Number of Installed		Num of Install		Number of Installed		NumberOf Inst
7		32		15			Number of Disconncted		Num of Discon		Number of Disconncted		NumberOf Discon
8		32		15			Number of No Reply		Num of No Reply		Number of No Reply		NumOf NoReply
														
9		32		15			Inventory Updating		Invent Update		Inventory Updating		Invent Update
10		32		15			BridgeCard Firmware Version	BdgCard FirmVer	BridgeCard Firmware Version	BdgCard FirmVer
11		32		15			Bridge Card Barcode 1		BdgCardBarcode1	Bridge Card Barcode 1		BdgCardBarcode1
12		32		15			Bridge Card Barcode 2		BdgCardBarcode2	Bridge Card Barcode 2		BdgCardBarcode2
13		32		15			Bridge Card Barcode 3		BdgCardBarcode3	Bridge Card Barcode 3		BdgCardBarcode3
14		32		15			Bridge Card Barcode 4		BdgCardBarcode4	Bridge Card Barcode 4		BdgCardBarcode4
														
														
50		32		15			All Batteries Communication Fail	All Comm Fail		All Batteries Communication Fail	All Comm Fail
51		32		15			Multi-Batt Comm Fail		Multi CommFail	Multi-Batt Comm Fail		Multil CommFail
52		32		15			Batt Lost			Batt Lost		Batt Lost			Batt Lost
														
														
99		32		15			Communication Fail		Comm Fail		Communication Fail		Comm Fail
100		32		15			Exist State			Exist State		Exist State			Exist State
101		32		15			Normal				Normal			Normal				Normal
102		32		15			Alarm				Alarm			Alarm				Alarm
103		32		15			Exist				Exist			Exist				Exist
104		32		15			No Exist			No Exist		No Exist			No Exist

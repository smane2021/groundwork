﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name 
    
[LOCALE_LANGUAGE]
fr

[RES_INFO]
#R_ID	MAX_LEN_FULL	MAX_LEN_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32		16		Power System			Power System		Système				Système	
2	32		15		System Voltage			Sys Volt		Tension système			Tension sys.
3	32		15		System Load			System Load		Charge système			Charge sys.
4	32		15		System Power			System Power			Puissance système		Puis W sys.
5	32		15		Total Power Consumption		Pwr Consumption			Puissance consommée		Puis VA sys.
6	32		15		Power Peak in 24 Hours		Power Peak		Pique Puis. des dernières 24h	Pique W
7	32		15		Average Power in 24 Hours	Ave Power		PU moyen. des dernières 24h	Moy.puissance
8	32		15		Hardware Write-protect Switch	Hardware Switch		Protection écriture		Protect. écrit.
9	32		15		Ambient Temperature		Amb Temp		Température ambiante		Temp ambiante
18	32		15		DO 1-DO 1			DO 1-DO 1		DO 1-DO 1			DO 1-DO 1
19	32		15		DO 2-DO 2			DO 2-DO 2		DO 2-DO 2			DO 2-DO 2
20	32		15		DO 3-DO 3			DO 3-DO 3		DO 3-DO 3			DO 3-DO 3
21	32		15		DO 4-DO 4			DO 4-DO 4		DO 4-DO 4			DO 4-DO 4
22	32		15		DO 5-DO 5			DO 5-DO 5		DO 5-DO 5			DO 5-DO 5
23	32		15		DO 6-DO 6			DO 6-DO 6		DO 6-DO 6			DO 6-DO 6
24	32		15		DO 7-DO 7			DO 7-DO 7		DO 7-DO 7			DO 7-DO 7
25	32		15		DO 8-DO 8			DO 8-DO 8		DO 8-DO 8			DO 8-DO 8
27	32		15		Under Voltage 1 Level		Under Voltage 1		Tension DC basse		V DC bas
28	32		15		Under Voltage 2 Level		Under Voltage 2		Tension DC très basse		V DC très bas
29	32		15		Over Voltage 1 Level		Over Voltage 1	Tension DC haute		V DC haut
31	32		15		Temperature 1 High 1		Temp 1 High 1	Limite Température 1 haute	Lim Temp1 haute
32	32		15		Temperature 1 Low 1		Temp 1 Low 1		Limite Température 1 basse	Lim Temp1 basse
33	32		15		Auto/Manual State		Auto/Man State		Etat Aut/Man			Etat Aut/Man
34	32		15		Outgoing Alarms Blocked	Alarm Blocked			Blocage report des alarmes	Bloc Report alm
35	32		15		Supervision Unit Fail		SupV Unit Fail		Défaut Contrôleur		Déf Contrôleur
36	32		15		CAN Communication Failure	CAN Comm Fail			Défaut Communication CAN	Défaut com CAN
37	32		15		Mains Failure			Mains Failure			Absence secteur			Abs secteur
38	32		15		Under Voltage 1			Under Voltage 1		Tension DC basse		V DC bas
39	32		15		Under voltage 2			Under Voltage 2		Tension DC très basse		V DC très bas
40	32		15		Over Voltage			Over Voltage		Tension DC haute		V DC haut
41	32		15		High Temperature 1	High Temp 1		Température 1 haute		Temp 1 haute
42	32		15		Low Temperature 1		Low Temp 1		Température 1 basse		Temp 1 basse
43	32		15		Temperature 1 Sensor Fail	T1 Sensor Fail			Défaut capteur temp.		Défaut CapTemp
44	32		15		Outgoing Alarms Blocked		Alarm Blocked			Blocage report des alarmes	Bloc Report alm
45	32		15		Maintenance Time Limit Alarm	Mtnc Time Alarm		Alarme maintenance		AlrMaintenance
46	32		15		Unprotected			Unprotected		Pas de protection		Pas de protect.
47	32		15		Protected			Protected		Protection			Protection
48	32		15		Normal				Normal			Normal				Normal
49	32		15		Fail				Fail			Défaut				Défaut
50	32		15		Off				Off			Inactive			Inactive
51	32		15		On				On			Active				Active
52	32		15		Off				Off			Inactive			Inactive
53	32		15		On				On			Active				Active
54	32		15		Off				Off			Inactive			Inactive
55	32		15		On				On			Active				Active
56	32		15		Off				Off			Inactive			Inactive
57	32		15		On				On			Active				Active
58	32		15		Off				Off			Inactive			Inactive
59	32		15		On				On			Active				Active
60	32		15		Off				Off			Inactive			Inactive
61	32		15		On				On			Active				Active
62	32		15		Off				Off			Inactive			Inactive
63	32		15		On				On			Active				Active
64	32		15		Open				Open			Ouvert				Ouvert	
65	32		15		Closed				Closed			Fermé				Fermé
66	32		15		Open				Open			Ouvert				Ouvert
67	32		15		Closed				Closed			Fermé				Fermé
78	32		15		Off				Off			Inactive			Inactive
79	32		15		On				On			Active				Active
80	32		15		Auto				Auto			Auto				Auto
81	32		15		Manual				Manual			Manuel				Manuel
82	32		15		Normal				Normal			Normal				Normal
83	32		15		Blocked				Blocked			Bloqué				Bloqué
85	32		15		Set to Auto Mode		To Auto Mode		Mode Automatique		Mode AUTO
86	32		15		Set to Manual Mode		To Manual Mode		Mode Manuel			Mode Manuel
87	32		15		Power Rate Level		PowerRate Level		Limite de Puissance		Limit de Puis.
88	32		15		Power Peak Limit		Power Limit		Limite de courrant		Limit I
89	32		15		Peak				Peak			Pique				Pique
90	32		15		High				High			Haut				Haut
91	32		15		Flat				Flat			Constant			Constant
92	32		15		Low				Low			Bas				Bas
93	32		15		Lower Consumption		Lwr Consumption	Conso. basse activée		Conso basse
94	32		15		Power Peak Savings		P Peak Savings			Limitation de puissance active	Limit P active
95	32		15		Disabled				Disabled			Inactif				Inactif
96	32		15		Enabled				Enabled			Actif				Actif
97	32		15		Disabled				Disabled			Inactif				Inactif
98	32		15		Enabled				Enabled			Actif				Actif
99	32		15		Over Maximum Power		Over Power		Sur puissance			Sur puissance
100	32		15		Normal				Normal			Normal				Normal
101	32		15		Alarm				Alarm			Alarme				Alarme
102	32		15		Over Maximum Power Alarm	Over Power		Sur puissance			Sur Puis.
104	32		15		System Alarm Status		Alarm Status		Etat système			Etat système
105	32		15		No Alarm			No Alm		Aucune alarme			0 alarme
106	32		15		Observation Alarm		Observation		Observation			Observation
107	32		15		Major Alarm			Major			Mineur				Mineur
108	32		15		Critical Alarm			Critical		Majeur				Majeur
109	32		15		Maintenance Run Time		Mtnc Run Time		Tps fct depuis dernière maint.	Maintenance Fct
110	32		15		Maintenance Cycle Time		Mtnc Cycle Time		Limite prochaine maintenance	Limite maint
111	32		15		Maintenance Time Delay			Mtnc Time Delay			Prochaine maintenance		Prochaine maint
112	32		15		Maintenance Time Out		Mtnc Time Out		Mtn Temps Out			Mtn Temps Out
113	32		15		No				No			Non				Non
114	32		15		Yes				Yes			Oui				Oui
115	32		15		Power Split Mode		Power Split		Mode puissance			Mode puissance
116	32		15		Slave Current Limit Value	Slave Curr Lmt		Limitation de courant		Limit. courant
117	32		15		Delta Voltage			Delta Volt		Esclave: Ecart de tension	Escl: Ecart U
118	32		15		Proportional Coefficient	Proportion Coef			Esclave:Proportionnel P		Escl:Proport P
119	32		15		Integral Time			Integral Time		Esclave:Durée Intégrale		Escl:Durée Inté
120	32		15		Master Mode			Master			Mode maître			Mode maître
121	32		15		Slave Mode			Slave			Mode esclave			Mode esclave
122	32		15		MPCL Control Step		MPCL Ctrl Step		MPCL commande			MPCL commande
123	32		15		MPCL Pwr Range			MPCL Pwr Range		MPCL seuil			MPCL seuil
124	32		15		MPCL Battery Discharge On	MPCL BatDsch On			MPCL décharge actif		MPCL décharge
125	32		15		MPCL Diesel Control On		MPCL DslCtrl On		MPCL diesel actif		MPCL diesel
126	32		15		Disabled			Disabled		Inactif				Inactif
127	32		15		Enabled				Enabled			Actif				Actif
128	32		15		Disabled			Disabled		Inactif				Inactif
129	32		15		Enabled				Enabled			Actif				Actif
130	32		15		System Time			System Time		Heure système			Heure système
131	32		15		LCD Language			LCD Language		LCD langue			LCD langue
#changed by Frank Wu,1/1,20140221 for TR196 which text “Alarm Voice” should change to “Audible alarm”
#132	32		15		LCD Alarm Voice			LCD Alarm Voice  LCD Alarm Voice			LCD Alarm Voice
132	32		15		Audible Alarm			Audible Alarm		LCD buzzer			LCD¸buzzer
133	32		15		English				English			Anglais				Anglais
134	32		15		French				French			Français			Français
135	32		15		On				On			Marche				Marche
136	32		15		Off				Off			Arrêt				Arrêt
137	32		15		3 min				3 min			3 Min				3 Min
138	32		15		10 min				10 min				10 Min				10 Min
139	32		15		1 hour				1 h			1 Heure				1 Heure
140	32		15		4 hours				4 h			4 Heure				4 Heure
141	32		15		Clear Maintenance Run Time	Clr MtncRunTime		RAZ date			RAZ date
142	32		15		No				No			Non				Non
143	32		15		Yes				Yes			Oui				Oui
144	32		15		Alarm				Alarm			Alarme				Alarme
145	32		15		HW Status			HW Status		Etat ACU+			Etat ACU+
146	32		15		Normal				Normal			Normale				Normale
147	32		15		Config Error			Config Error		Erreur configuration		Err. Conf.
148	32		15		Flash Fail			Flash Fail		Défaut flash			Défaut flash
150	32		15		LCD Time Zone			Time Zone		Decalage horaire		Decal. Horaire
151	32		15		Time Sync Main Time Server	Time Sync Svr		Synchro. horaire		Syn. horaire
152	32		15		Time Sync Backup Time Server	Backup Sync Svr		Synchro Horaire sur Server	Synch H Server
153	32		15		Time Sync Interval		Sync Interval		Synchro interval		Synchro inter.
155	32		15		Reset SCU			Reset SCU		SCU Reset			SCU Reset
156	32		15		Running Config Type		Config Type		Type configuration		Config type
157	32		15		Normal Config			Normal Config	Config. normale			Config. norm.
158	32		15		Backup Config			Backup Config	Restauration config.		Restor Config
159	32		15		Default Config			Default Config			Défaut Config.			Défaut Config
160	32		15		Config Error(Backup Config)	Config Error 1			Err config 1			Err config 1
161	32		15		Config Error(Default Config)	Config Error 2		Err config 2			Err config 2
162	32		15		Control System Alarm LED	Ctrl Sys Alarm		Système alarme led		Alarme led
163	32		15		Imbalance System Current	Imbalance Curr		Anormal courant util		Err. I util
164	32		15		Normal				Normal			Normal				Normal
165	32		15		Abnormal			Abnormal		Anormal				Anormal
167	32		15		MainSwitch Block Condition	MnSw Block Cond			Switch bloqué			Switch bloqué
168	32		15		No				No			Non				Non
169	32		15		Yes				Yes			Oui				Oui
170	32		15		Total Run Time			Total Run Time		Temps total de fonc.		Temps total fct
171	32		15		Total Alarm Number		Total Alm Num			Total alarme			Total alarme
172	32		15		Total OA Number			Total OA Num		Total OA nbre			Total OA
173	32		15		Total MA Number			Total MA Num		Total MA			Total MA
174	32		15		Total CA Number			Total CA Num		Total CA			Total CA
175	32		15		SPD Fail			SPD Fail		Défaut Protection Départ DC	Déf Prot DC
176	32		15		Over Voltage 2 Level		Over Voltage 2		Tension DC très haute		V DC très haute
177	32		15			Very Over Voltage		Very Over Volt		Tension DC très haute		V DC très haute
178	32		15			Regulation Voltage		Regulate Volt		Inibition Regulation Tension	Inib Reg Vs
179	32		15			Regulation Voltage Range	Regu Volt Range		Niveau Regulation Tension	Niv Reg. Vs
180	32		15		Keypad Voice			Keypad Voice		Bip Clavier			Bip Clavier
181	32		15		On				On			Marche				Marche
182	32		15		Off				Off			Arrêt				Arrêt
183	32		15		Load Shunt Full Current		Load Shunt Curr		Courant Shunt Util		I Shunt Util
184	32		15		Load Shunt Full Voltage		Load Shunt Volt		Tension Shunt Util		V Shunt Util
185	32		15		Bat 1 Shunt Full Current	Bat1 Shunt Curr		Courant Shunt Bat 1		I Shunt Bat 1
186	32		15		Bat 1 Shunt Full Voltage	Bat1 Shunt Volt		Tension Shunt Bat 1		V Shunt Bat 1
187	32		15		Bat 2 Shunt Full Current	Bat2 Shunt Curr		Courant Shunt Bat 2		I Shunt Bat 2
188	32		15		Bat 2 Shunt Full Voltage	Bat2 Shunt Volt		Tension Shunt Bat 2		V Shunt Bat 2
219	32		15			Relay 6			Relay 6		Relai 6		Relai 6	
220	32		15			Relay 7			Relay 7		Relai 7		Relai 7	
221	32		15			Relay 8			Relay 8		Relai 8		Relai 8	
222	32		15		RS232 Baud Rate			RS232 Baud Rate			RS232 Baudrate			RS232 Baudrate
223	32		15		Local Address			Local Address		Local Address			Local Addr
224	32		15		Callback Number			Callback Number		Callback Num			Callback Num
225	32		15		Interval Time of Callback	Interval		Interval Time of Callback	Interval
#226	32		15		Communication Password		Comm Password		Communication Password		Comm Password
227	32		15		DC Data Flag (On-Off)		DC Data Flag 1		DCDataFlage(on-off)		DCDataFlage1
228	32		15		DC Data Flag (Alarm)		DC Data Flag 2		DCDataFlage(Alarm)		DCDataFlage2
229	32		15		Relay Reporting			Relay Reporting			Type Repport Alarme		Type Rep. Alar.
230	32		15		Fixed				Fixed			Standard			Standard
231	32		15		User Defined			User Defined		Configuration utilisateur	Config util
232	32		15		Rect Data Flag (Alarm)		RectDataFlag 2		RectDataFlage(Alarm)		RectDataFlage2
233	32		15		Master Controlled		Master Ctrl		Maitre				Maitre
234	32		15		Slave Controlled		Slave Ctrl		Esclave				Esclave
236	32		15		Over Load Alarm Point		Over Load Point			Niveau de surcharge		Niveau surcharg
237	32		15		Over Load			Over Load		Surcharge			Surcharge
238	32		15		System Data Flag (On-Off)	Sys Data Flag 1				SystemDataFlage(on-off)		SysDataFlage1
239	32		15		System Data Flag (Alarm)	Sys Data Flag 2				SystemDataFlage(Alarm)		SysDataFlage2
240	32		15		French Language			French			Langue Française		Français
241	32		15		Imbalance Protection		Imb Protect		Protection Déséquilibrage actif		Prot Désé actif
242	32		15		Enabled				Enabled			Marche				Marche
243	32		15		Disabled				Disabled			Arrêt				Arrêt
244	32		15		Buzzer Control			Buzzer Control		Alarme sonore			Alarme sonore
245	32		15		Normal				Normal			Normal				Normal
246	32		15		Disabled				Disabled			Arrêt				Arrêt
247	32		15		Ambient Temp Alarm		Temp Alarm		Alarme température ambiante		Alarm Temp 
248	32		15		Normal				Normal			Normal				Normal
249	32		15		Low				Low			Bas				Bas
250	32		15		High				High			Haut				Haut
251	32		15		Reallocate Rect Address		Reallocate Addr		Réatribution adresse redresseurs		Réatrib adresse 
252	32		15		Normal				Normal			Normal				Normal
253	32		15		Reallocate Address		Reallocate Addr			Réatribution adresse		Réatrib adresse 
254	32		15		Test State Set			Test State Set		Test State Set			Test State Set
255	32		15		Normal				Normal			Normal				Normal
256	32		15		Test State			Test State		Test State			Test State
257	32		15		Minification for Test		Minification		Minification for Test		Minification
258	32		15		Digital Input 1			DI 1			Entree Digital 1		DI 1
259	32		15		Digital Input 2			DI 2			Digital Input 2			DI 2
260	32		15		Digital Input 3			DI 3			Digital Input 3			DI 3
261	32		15		Digital Input 4			DI 4			Digital Input 4			DI 4
262	32		15		System Type Value		Sys Type Value		System Type Value		Sys Type Value
263	32		15		AC/DC Board Type		AC/DC BoardType			Type module AC/DC		Type mod AC/DC
264	32		15		B14C3U1				B14C3U1			B14C3U1				B14C3U1
265	32		15		Large DU			Large DU		Large DU			Large DU
266	32		15		SPD				SPD			SPD				SPD
267	32		15		Temp1				Temp1			Température 1			Temp1
268	32		15		Temp2				Temp2			Température 2			Temp2
269	32		15		Temp3				Temp3			Température 3			Temp3
270	32		15		Temp4				Temp4			Température 4			Temp4
271	32		15		Temp5				Temp5			Température 5			Temp5
272	32		15		Auto Mode			Auto Mode		Mode Auto			Mode Auto
273	32		15		EMEA				EMEA			EMEA				EMEA
274	32		15		Normal				Normal			Normal				Normal
275	32		15		Nominal Voltage			Nom Voltage	Tension floating		V floating
276	32		15		EStop/EShutdown			EStop/EShutdown		EStop/EShutdown			EStop/EShutdown
277	32		15		Shunt 1 Full Current		Shunt 1 Current		Courant Shunt 1			I Shunt 1
278	32		15		Shunt 2 Full Current		Shunt 2 Current		Courant Shunt 2			I Shunt 2
279	32		15		Shunt 3 Full Current		Shunt 3 Current		Courant Shunt 3			I Shunt 3
280	32		15		Shunt 1 Full Voltage		Shunt 1 Voltage		Tension Shunt 1			V Shunt 1
281	32		15		Shunt 2 Full Voltage		Shunt 2 Voltage		Tension Shunt 2			V Shunt 2
282	32		15		Shunt 3 Full Voltage		Shunt 3 Voltage		Tension Shunt 3			V Shunt 3
283	32		15		Temperature 1			Temp 1			Sonde Temp 1			Sonde Temp1
284	32		15		Temperature 2			Temp 2			Sonde Temp 2			Sonde Temp2
285	32		15		Temperature 3			Temp 3			Sonde Temp 3			Sonde Temp3
286	32		15		Temperature 4			Temp 4			Sonde Temp 4			Sonde Temp4
287	32		15		Temperature 5			Temp 5			Sonde Temp 5			Sonde Temp5
288	32		15		Very High Temperature 1 Limit	Very Hi Temp 1				Temp 1 Très haute		Temp 1 T.Haute 
289	32		15		Very High Temperature 2 Limit	Very Hi Temp 2				Temp 2 Très haute		Temp 2 T.Haute 
290	32		15		Very High Temperature 3 Limit	Very Hi Temp 3				Temp 3 Très haute		Temp 3 T.Haute 
291	32		15		Very High Temperature 4 Limit	Very Hi Temp 4				Temp 4 Très haute		Temp 4 T.Haute 
292	32		15		Very High Temperature5 Limit	Very Hi Temp 5				Temp 5 Très haute		Temp 5 T.Haute 
293	32		15		High Temperature 2 Limit	High Temp 2		Température 2 Haute		Temp 2 Haute 
294	32		15		High Temperature 3 Limit	High Temp 3		Température 3 Haute		Temp 3 Haute 
295	32		15		High Temperature 4 Limit	High Temp 4		Température 4 Haute		Temp 4 Haute 
296	32		15		High Temperature 5 Limit	High Temp 5		Température 5 Haute		Temp 5 Haute 
297	32		15		Low Temperature 2 Limit		Low Temp 2		Température2 Basse		Temp2 Basse
298	32		15		Low Temperature 3 Limit		Low Temp 3		Température3 Basse		Temp3 Basse
299	32		15		Low Temperature 4 Limit		Low Temp 4		Température4 Basse		Temp4 Basse
300	32		15		Low Temperature 5 Limit		Low Temp 5		Température5 Basse		Temp5 Basse
301	32		15		Temperature 1 Not Used		Temp 1 Not Used		Sonde Tempér.1 non utilisée	Temp1 non util.
302	32		15		Temperature 2 Not Used		Temp 2 Not Used		Sonde Tempér.2 non utilisée	Temp2 non util.
303	32		15		Temperature 3 Not Used		Temp 3 Not Used		Sonde Tempér.3 non utilisée	Temp3 non util.
304	32		15		Temperature 4 Not Used		Temp 4 Not Used		Sonde Tempér.4 non utilisée	Temp4 non util.
305	32		15		Temperature 5 Not Used		Temp 5 Not Used		Sonde Tempér.5 non utilisée	Temp5 non util.
306	32		15		High Temperature 2		High Temp 2		Temp 2 Haute			Temp 2 Haute 
307	32		15		Low Temperature 2		Low Temp 2		Température2 Basse		Temp2 Basse
308	32		15		Temperature 2 Sensor Fail	T2 Sensor Fail	Défaut capteur Température2	Déf capt Temp2
309	32		15		High Temperature 3	High Temp 3		Température 3 Haute		Temp 3 Haute 
310	32		15		Low Temperature 3		Low Temp 3		Température3 Basse		Temp3 Basse
311	32		15		Temperature 3 Sensor Fail	T3 Sensor Fail		Défaut capteur Température3	Déf capt Temp3
312	32		15		High Temperature 4	High Temp 4		Température 4 Haute		Temp 4 Haute 
313	32		15		Low Temperature 4		Low Temp 4		Température4 Basse		Temp4 Basse
314	32		15		Temperature 4 Sensor Fail	T4 Sensor Fail		Défaut capteur Température4	Déf capt Temp4
315	32		15		High Temperature 5	High Temp 5		Température 5 Haute		Temp 5 Haute 
316	32		15		Low Temperature 5		Low Temp 5		Température5 Basse		Temp5 Basse
317	32		15		Temperature 5 Sensor Fail	T5 Sensor Fail		Défaut capteur Température5	Déf capt Temp5
318	32		15		Very High Temperature 1		Very Hi Temp 1		Température 1 Très haute	Temp 1 T.Haute 
319	32		15		Very High Temperature 2		Very Hi Temp 2		Température 2 Très haute	Temp 2 T.Haute 
320	32		15		Very High Temperature 3		Very Hi Temp 3		Température 3 Très haute	Temp 3 T.Haute 
321	32		15		Very High Temperature 4		Very Hi Temp 4		Température 4 Très haute	Temp 4 T.Haute 
322	32		15		Very High Temperature 5		Very Hi Temp 5		Température 5 Très haute	Temp 5 T.Haute 
323	32		15		ECO Mode Status			ECO Mode Status		Etat MODE ECO			Etat MODE ECO
324	32		15		ECO Mode		ECO Mode	MODE ECO actif			MODE ECO actif	
325	32		15		Internal Use (I2C)		Internal Use		Internal Use(I2C)		Internal Use
326	32		15		Disabled			Disabled			Arrêt				Arrêt
327	32		15		EStop				EStop			Arrêt d'urgence			Arrêt d'urgence
328	32		15		EShutdown			EShutdown		Arrêt d'urgence			Arrêt d'urgence
329	32		15		Ambient				Ambient			Environment			Environment
330	32		15		Battery				Battery			Batterie			Batterie
331	32		15		Temperature 6			Temperature 6			Température6			Temp6
332	32		15		Temperature 7			Temperature 7			Température7			Temp7
333	32		15		Temperature 6			Temp 6			Sonde Temp 6			Sonde Temp6
334	32		15		Temperature 7			Temp 7			Sonde Temp 7			Sonde Temp7
335	32		15		Very High Temperature 6 Limit	Very Hi Temp 6		Température 6 Très haute	Temp 6 T.Haute 
336	32		15		Very High Temperature 7 Limit	Very Hi Temp 7		Température 7 Très haute	Temp 7 T.Haute 
337	32		15		High Temperature6 Limit	High Temp 6		Température 6  haute		Temp 6 Haute 
338	32		15		High Temperature7 Limit	High Temp 7		Température 7  haute		Temp 7 Haute 
339	32		15		Low Temperature6 Limit		Low Temp 6		Température6 Basse		Temp6 Basse
340	32		15		Low Temperature7 Limit		Low Temp 7		Température7 Basse		Temp7 Basse
341	32		15		EIB Temp1 Not Used		EIB T1 Not Used			EIB Sonde Tempér.1 non utilisée		EIB T1 non util
342	32		15		EIB Temp2 Not Used		EIB T2 Not Used			EIB Sonde Tempér.2 non utilisée		EIB T2 non util
343	32		15		High Temperature 6		High Temp 6		Température 6  haute		Temp 6 Haute 
344	32		15		Low Temperature 6		Low Temp 6		Température6 Basse		Temp6 Basse
345	32		15		Temperature6 Sensor Fail	T6 Sensor Fail		Défaut capteur Température6	Déf capt Temp6
346	32		15		High Temperature 7		High Temp 7		Température 7  haute		Temp 7 Haute 
347	32		15		Low Temperature 7		Low Temp 7		Température7 Basse		Temp7 Basse
348	32		15		Temperature 7 Sensor Fail	T7 Sensor Fail		Défaut capteur Température7	Déf capt Temp7
349	32		15		Very High Temperature 6		Very Hi Temp 6		Température 6 Très haute	Temp 6 T.Haute 
350	32		15		Very High Temperature 7		Very Hi Temp 7		Température 7 Très haute	Temp 7 T.Haute 

1111	32		15		System Temp 1			System Temp1		Système Température 1			Sys Temp1
1131	32		15		System Temp 1			System Temp1		Système Température 1			Sys Temp1
1132	32		15		Battery Temperature 1 High 2	Batt Temp1 Hi2		Température 1 très haute	Temp1 Très haut
1133	32		15		Battery Temperature 1 High 1	Batt Temp1 Hi1		Température 1 haute		Temp 1 haute
1134	32		15		Battery Temperature 1 Low	Batt Temp1 Low		Température 1 Basse		Temp 1 Basse
1141	32		15		System Temp 1 Not Used		Sys T1 Not Used			Tempér.1 système non utilisée		T1 Sys non util
1142	32		15		System Temp 1 Sensor Fail	SysT1SensorFail		Défaut capteur Système Temp1		Déf capt Temp1
1143	32		15		Battery Temperature 1 High 2	Batt Temp1 Hi2		Température 1 Très haute	Temp 1 T.Haute
1144	32		15		Battery Temperature 1 High 1	Batt Temp1 Hi1		Température 1 haute		Temp 1 Haute
1145	32		15		Battery Temperature 1 Low	Batt Temp1 Low		Température 1 Basse		Temp1 Basse

1211	32		15		System Temp 2			System Temp2			Système Température 2			Sys Temp2
1231	32		15		System Temp 2			System Temp2			Système Température 2			Sys Temp2
1232	32		15		Battery Temperature 2 High 2	Batt Temp2 Hi2		Température 2 très haute	Temp2 Très haut
1233	32		15		Battery Temperature 2 High 1	Batt Temp2 Hi1	Température 2 haute		Temp 2 haute
1234	32		15		Battery Temperature 2 Low	Batt Temp2 Low		Température 2 Basse		Temp 2 Basse
1241	32		15		System Temp 2 Not Used		Sys T2 Not Used			Tempér.2 système non utilisée		T2 Sys non util
1242	32		15		System Temp 2 Sensor Fail	SysT2SensorFail		Défaut capteur Système Temp3		Déf capt Temp3
1243	32		15		Battery Temperature 2 High 2	Batt Temp2 Hi2		Température 2 très haute	Temp2 Très haut
1244	32		15		Battery Temperature 2 High 1	Batt Temp2 Hi1		Température 2 haute		Temp 2 haute
1245	32		15		Battery Temperature 2 Low	Batt Temp2 Low		Température 2 Basse		Temp 2 Basse

1311	32		15		System Temp 3			System Temp3			Système Température 3			Sys Temp3
1331	32		15		System Temp 3			System Temp3			Système Température 3			Sys Temp3
1332	32		15		Battery Temperature 3 High 2	Batt Temp3 Hi2		Température 3 très haute	Temp3 Très haut
1333	32		15		Battery Temperature 3 High 1	Batt Temp3 Hi1		Température 3 haute		Temp 3 haute
1334	32		15		Battery Temperature 3 Low	Batt Temp3 Low		Température 3 Basse		Temp 3 Basse
1341	32		15		System Temp 3 Not Used		Sys T3 Not Used		Tempér.3 système non utilisée		T3 Sys non util
1342	32		15		System Temp 3 Sensor Fail	SysT3SensorFail			Défaut capteur Système Temp3		Déf capt Temp3
1343	32		15		Battery Temperature 3 High 2	Batt Temp3 Hi2		Température 3 très haute	Temp3 Très haut
1344	32		15		Battery Temperature 3 High 1	Batt Temp3 Hi1		Température 3 haute		Temp 3 haute
1345	32		15		Battery Temperature 3 Low	Batt Temp3 Low		Température 3 Basse		Temp 3 Basse

1411	32		15		IB2-1 Temp 1			IB2-1 Temp1			IB2 Température 1			IB2 Temp1
1431	32		15		IB2-1 Temp 1			IB2-1 Temp1			Activation IB2 Température 1		Act IB2 Temp1
1432	32		15		Battery Temperature 4 High 2	Batt Temp4 Hi2			Température 4 très haute	Temp4 Très haut
1433	32		15		Battery Temperature 4 High 1	Batt Temp4 Hi1			Température 4 haute		Temp 4 haute
1434	32		15		Battery Temperature 4 Low	Batt Temp4 Low			Température 4 Basse		Temp 4 Basse
1441	32		15		IB2-1 Temp 1 Not Used		IB2-1T1NotUsed		Tempér.1 IB2 non utilisée		T1 IB2 non util
1442	32		15		IB2-1 Temp 1 Sensor Fail	IB2-1T1SensFail		Défaut capteur IB2 Temp1		Déf capt IB2 T1
1443	32		15		Battery Temperature 4 High 2	Batt Temp4 Hi2			Température 4 très haute	Temp4 Très haut
1444	32		15		Battery Temperature 4 High 1	Batt Temp4 Hi1			Température 4 haute		Temp 4 haute
1445	32		15		Battery Temperature 4 Low	Batt Temp4 Low			Température 4 Basse		Temp 4 Basse
1511	32		15		IB2-1 Temp 2			IB2-1 Temp2				IB2 Température 2			IB2 Temp2
1531	32		15		IB2-1 Temp 2			IB2-1 Temp2			Activation IB2 Température 2		Act IB2 Temp2
1532	32		15		Battery Temperature 5 High 2	Batt Temp5 Hi2			Température 5 très haute	Temp5 Très haut
1533	32		15		Battery Temperature 5 High 1	Batt Temp5 Hi1			Température 5 haute		Temp 5 haute
1534	32		15		Battery Temperature 5 Low	Batt Temp5 Low			Température 5 Basse		Temp 5 Basse
1541	32		15		IB2-1 Temp 2 Not Used		IB2-1T2NotUsed		Tempér.2 IB2 non utilisée		T2 IB2 non util
1542	32		15		IB2-1 Temp 2 Sensor Fail	IB2-1T2SensFail			Défaut capteur IB2 Temp2		Déf capt IB2 T2
1543	32		15		Battery Temperature 5 High 2	Batt Temp5 Hi2			Température 5 très haute	Temp5 Très haut
1544	32		15		Battery Temperature 5 High 1	Batt Temp5 Hi1				Température 5 haute		Temp 5 haute
1545	32		15		Battery Temperature 5 Low	Batt Temp5 Low			Température 5 Basse		Temp 5 Basse
1611	32		15		EIB-1 Temp 1			EIB-1 Temp1				EIB Température 1			EIB Temp1
1631	32		15		EIB-1 Temp 1			EIB-1 Temp1			Activation EIB Température 1		Act EIB Temp1
1632	32		15		Battery Temperature 6 High 2	Batt Temp6 Hi2			Température 6 très haute	Temp6 Très haut
1633	32		15		Battery Temperature 6 High 1	Batt Temp6 Hi1			Température 6 haute		Temp 6 haute
1634	32		15		Battery Temperature 6 Low	Batt Temp6 Low			Température 6 Basse		Temp 6 Basse
1641	32		15		EIB-1 Temp 1 Not Used		EIB-1T1NotUsed		Tempér.1 EIB non utilisée		T1 EIB non util
1642	32		15		EIB-1 Temp 1 Sensor Fail	EIB-1T1SensFail			Défaut capteur EIB Temp1		Déf capt EIB T1
1643	32		15		Battery Temperature 6 High 2	Batt Temp6 Hi2			Température 6 très haute	Temp6 Très haut
1644	32		15		Battery Temperature 6 High 1	Batt Temp6 Hi1			Température 6 haute		Temp 6 haute
1645	32		15		Battery Temperature 6 Low	Batt Temp6 Low			Température 6 Basse		Temp 6 Basse
1711	32		15		EIB-1 Temp 2			EIB-1 Temp2				EIB Température 2			EIB Temp2
1731	32		15		EIB-1 Temp 2			EIB-1 Temp2				Activation EIB Température 2		Act EIB Temp2
1732	32		15		Battery Temperature 7 High 2	Batt Temp7 Hi2			Température 7 très haute	Temp7 Très haut
1733	32		15		Battery Temperature 7 High 1	Batt Temp7 Hi1			Température 8 haute		Temp 8 haute
1734	32		15		Battery Temperature 7 Low	Batt Temp7 Low			Température 7 Basse		Temp 7 Basse
1741	32		15		EIB-1 Temp 2 Not Used		EIB-1T2NotUsed		Tempér.2 EIB non utilisée		T2 EIB non util
1742	32		15		EIB-1 Temp 2 Sensor Fail	EIB-1T2SensFail		Défaut capteur IB2 Temp2		Déf capt IB2 T2
1743	32		15		Battery Temperature 7 High 2	Batt Temp7 Hi2		Température 7 très haute	Temp7 Très haut
1744	32		15		Battery Temperature 7 High 1	Batt Temp7 Hi1		Température 8 haute		Temp 8 haute
1745	32		15		Battery Temperature 7 Low	Batt Temp7 Low		Température 7 Basse		Temp 7 Basse
1746	32		15		DHCP Failure			DHCP Failure		DHCP Failure			DHCP Failure

1747	32		15		Internal Use (CAN)		Internal Use		Internal Use(CAN)		Internal Use
1748	32		15		Internal Use (485)		Internal Use		Internal Use(485)		Internal Use
1749	32		15		Address				Address		Adresse second système		Address(Slave)
1750	32		15		201				201			201				201
1751	32		15		202				202			202				202
1752	32		15		203				203			203				203
1753	32		15		Master/Slave Mode		Mast/Slave Mode			Mode Maitre/Esclave		Mode Mait/Escl
1754	32		15		Master				Master			Maitre				Maitre
1755	32		15		Slave				Slave			Esclave				Esclave
1756	32		15		Alone				Alone			Mode seul			Mode seul
1757	32		15		SM Batt Temp High Limit		SM Bat Temp Hi	Température SMBAT haute		Temp SMBAT haut
1758	32		15		SM Batt Temp Low Limit		SM Bat Temp Low		Température SMBAT Basse		Temp SMBAT Bass
1759	32		15		PLC Config Error		PLC Config Err		Erreur Configuration PLC	Erreur Cfg PLC
1760	32		15		Rectifier Expansion		Rect Expansion		Mode de travail			Mode de travail
1761	32		15		Inactive			Inactive		Autonome			Autonome
1762	32		15		Primary				Primary			Maitre				Maitre
1763	32		15		Secondary			Secondary		Esclave			Esclave
1764	128		64		Mode Changed. Auto Configuration Will Start.	Mode Changed. Auto Configuration Will Start.		Le mode à Changé! L'auto configuration va démarrer!		L'auto configuration va démarrer!
1765	32		15		Previous Language		Previous Lang		Langue Précédente		Langue Précéd.
1766	32		15		Current Language		Current Lang		Langue Actuelle			Langue Act.
1767	32		15		Eng				Eng		Anglais				Anglais	
1768	32		15		French				French			Français			Français
1769	32		15		485 Communication Failure	485 Comm Fail	Défaut COM RS-485		Déf COM 485
1770	64		32		SMDU Sampler Mode		SMDU Samp Mode		Mode Mesure SMDU		Mesure SMDU
1771	32		15		CAN				CAN			CAN				CAN
1772	32		15		RS485				RS485			RS485				RS485
1773	32		15		Manual Mode Time Limit		Manual Mode Lmt		Retard Automatique		Retard Auto
1774	32		15		Observation Summary		OA Summary		Total Observation		Total Obser.
1775	32		15		Major Summary			MA Summary		Total Mineure			Total Mineure
1776	32		15		Critical Summary		CA Summary		Total Majeure			Total Majeure
1777	32		15		All Rectifiers Current		All Rects Curr		Courant Total Redresseur	I Total Red.
1778	32		15		Rectifier Group Lost Status	Rect Grp Lost		Perte Groupe Redresseur		Perte Gr.Red.
1779	32		15		Reset Rectifier Group Lost Alarm	Rst RectGrpLost		Reset Perte Redresseur		Reset Perte Red
1780	32		15		Rect Group Num on Last Power-On	RectGrp On Num	Dernier Groupe Redresseur	Dernier Gr.Red.
1781	32		15		Rectifier Group Lost		Rect Group Lost		Perte Groupe Redresseur		Perte Gr.Red.
1782	32		15		ETemperature 1 High 1		ETemp 1 High 1		Limite Temp Ambiante1 Haute	Temp1 Amb Haute
1783	32		15		ETemperature 1 Low		ETemp 1 Low				Limite Temp Ambiante1 Basse	Temp1 Amb Basse
1784	32		15		ETemperature 2 High 1		ETemp 2 High 1		Limite Temp Ambiante2 Haute	Temp2 Amb Haute
1785	32		15		ETemperature 2 Low		ETemp 2 Low				Limite Temp Ambiante2 Basse	Temp2 Amb Basse
1786	32		15		ETemperature 3 High 1		ETemp 3 High 1		Limite Temp Ambiante3 Haute	Temp3 Amb Haute
1787	32		15		ETemperature 3 Low		ETemp 3 Low				Limite Temp Ambiante3 Basse	Temp3 Amb Basse
1788	32		15		ETemperature 4 High 1		ETemp 4 High 1		Limite Temp Ambiante4 Haute	Temp4 Amb Haute
1789	32		15		ETemperature 4 Low		ETemp 4 Low				Limite Temp Ambiante4 Basse	Temp4 Amb Basse
1790	32		15		ETemperature 5 High 1		ETemp 5 High 1		Limite Temp Ambiante5 Haute	Temp5 Amb Haute
1791	32		15		ETemperature 5 Low		ETemp 5 Low				Limite Temp Ambiante5 Basse	Temp5 Amb Basse
1792	32		15		ETemperature 6 High 1		ETemp 6 High 1		Limite Temp Ambiante6 Haute	Temp6 Amb Haute
1793	32		15		ETemperature 6 Low		ETemp 6 Low				Limite Temp Ambiante6 Basse	Temp6 Amb Basse
1794	32		15		ETemperature 7 High 1		ETemp 7 High 1		Limite Temp Ambiante7 Haute	Temp7 Amb Haute
1795	32		15		ETemperature 7 Low		ETemp 7 Low				Limite Temp Ambiante7 Basse	Temp7 Amb Basse
1796	32		15		ETemperature 1 High 1		ETemp 1 High 1		Temp Ambiante1 Haute		Temp Amb1 Haute
1797	32		15		ETemperature 1 Low		ETemp 1 Low				Temp Ambiante1 Basse		Temp Amb1 Basse
1798	32		15		ETemperature 2 High 1		ETemp 2 High 1		Temp Ambiante2 Haute		Temp Amb2 Haute
1799	32		15		ETemperature 2 Low		ETemp 2 Low				Temp Ambiante2 Basse		Temp Amb2 Basse
1800	32		15		ETemperature 3 High 1		ETemp 3 High 1		Temp Ambiante3 Haute		Temp Amb3 Haute
1801	32		15		ETemperature 3 Low		ETemp 3 Low				Temp Ambiante3 Basse		Temp Amb3 Basse
1802	32		15		ETemperature 4 High 1		ETemp 4 High 1		Temp Ambiante4 Haute		Temp Amb4 Haute
1803	32		15		ETemperature 4 Low		ETemp 4 Low				Temp Ambiante4 Basse		Temp Amb4 Basse
1804	32		15		ETemperature 5 High 1		ETemp 5 High 1		Temp Ambiante5 Haute		Temp Amb5 Haute
1805	32		15		ETemperature 5 Low		ETemp 5 Low				Temp Ambiante5 Basse		Temp Amb5 Basse
1806	32		15		ETemperature 6 High 1		ETemp 6 High 1		Temp Ambiante6 Haute		Temp Amb6 Haute
1807	32		15		ETemperature 6 Low		ETemp 6 Low				Temp Ambiante6 Basse		Temp Amb6 Basse
1808	32		15		ETemperature 7 High 1		ETemp 7 High 1		Temp Ambiante7 Haute		Temp Amb7 Haute
1809	32		15		ETemperature 7 Low		ETemp 7 Low				Temp Ambiante7 Basse		Temp Amb7 Basse
1810	32		15		Fast Sampler Flag		Fast Sampl Flag	Fast Sampler Flag		Fast SamplFlag
1811	32		15		LVD Quantity			LVD Quantity		Nombre de Contacteur		Nombre de LVDs
1812	32		15		LCD Rotation			LCD Rotation		Rotation Ecran			Rotation LCD
1813	32		15		0 deg				0 deg			0 Degré				0 Degré
1814	32		15		90 deg				90 deg			90 Degrés			90 Degrés
1815	32		15		180 deg				180 deg			180 Degrés			180 Degrés
1816	32		15		270 deg				270 deg			270 Degrés			270 Degrés
1817	32		15		Over Voltage 1			Over Voltage 1		Sur-Tension 1			Sur-Tension 1
1818	32		15		Over Voltage 2			Over Voltage 2			Sur-Tension 2			Sur-Tension 2
1819	32		15		Under Voltage 1			Under Voltage 1			Sous-Tension 1			Sous-Tension 1
1820	32		15		Under Voltage 2			Under Voltage 2	Sous-Tension 2			Sous-Tension 2
1821	32		15		Over Voltage 1 (24V)		24V Over Volt1		Sur-Tension 1			Sur-Tension 1
1822	32		15		Over Voltage 2 (24V)		24V Over Volt2		Sur-Tension 2			Sur-Tension 2 
1823	32		15		Under Voltage 1 (24V)		24V Under Volt1		Sous-Tension 1			Sous-Tension 1
1824	32		15		Under Voltage 2 (24V)		24V Under Volt2		Sous-Tension 2			Sous-Tension 2
1825	32		15		Fail Safe Mode			Fail Safe		Défaut Mode Sécurité		Déf.Mode Sécu.
1826	32		15		Disabled				Disabled			Arrêt				Arrêt
1827	32		15		Enabled				Enabled			Marche				Marche
1828	32		15		Hybrid Mode			Hybrid Mode		Mode Hybride			Mode Hybride½	
1829	32		15		Disabled				Disabled			Arrêt				Arrêt
1830	32		15		Capacity			Capacity		Capacité			Capacité
1831	32		15		Fixed Daily			Fixed Daily			Cycle				Cycle		
1832	32		15		DG Run at High Temp		DG Run Overtemp			Fonction. GE à Température Haute	Fonct.GE Temp.H
1833	32		15		DG Used for Hybrid		DG Used			Utilisation GE pour Hybride	Util.GE Hybrid.
1834	32		15		DG1				DG1			Générateur 1			Générateur 1
1835	32		15		DG2				DG2			Générateur 2			Générateur 2
1836	32		15		Both				Both			Ensemble			Ensemble			
1837	32		15		DI for Grid			DI for Grid		Entrée Pour Secteur		Entrée Secteur						
1838	32		15		IB2-1 DI1				IB2-1 DI1			IB2-1 Entrée 1			IB2-1 Entrée1
1839	32		15		IB2-1 DI2				IB2-1 DI2			IB2-1 Entrée 2			IB2-1 Entrée2
1840	32		15		IB2-1 DI3				IB2-1 DI3			IB2-1 Entrée 3			IB2-1 Entrée3
1841	32		15		IB2-1 DI4				IB2-1 DI4			IB2-1 Entrée 4			IB2-1 Entrée4
1842	32		15		IB2-1 DI5				IB2-1 DI5			IB2-1 Entrée 5			IB2-1 Entrée5
1843	32		15		IB2-1 DI6				IB2-1 DI6			IB2-1 Entrée 6			IB2-1 Entrée6
1844	32		15		IB2-1 DI7				IB2-1 DI7			IB2-1 Entrée 7			IB2-1 Entrée7
1845	32		15		IB2-1 DI8				IB2-1 DI8			IB2-1 Entrée 8			IB2-1 Entrée8
1846	32		15		DOD				DOD		DOD				DOD
1847	32		15		Discharge Duration		Dsch Duration		Durée Décharge			Durée Décharge
1848	32		15	Start Discharge Time		Start Dsch Time			Heure démarrage décharge	Heure Dém.Décha		
1849	32		15		Diesel Run Over Temp		DG Run OverTemp	Fonction. GE à Température Haute	Fonct.GE Temp.H
1850	32		15		DG1 is Running			DG1 is Running		GE 1 En Marche			GE 1 En Marche
1851	32		15		DG2 is Running			DG2 is Running		GE 2 En Marche			GE 2 En Marche
1852	32		15		Hybrid High Load Setting	High Load Set		Charge Max Hybride		Charge Max Hybr
1853	32		15		Hybrid is High Load		High Load		Charge Haute Hybride		Char.Haut.Hybri
1854	32		15		DG Run Time at High Temp	DG Run Time		Fonction. GE à Température Haute	Fonct.GE Temp.H
1855	32		15		Equalizing Start Time		Equal StartTime		Heure démarrage Egalisation		Heure Dém.Egali	
1856	32		15		DG1 Failure			DG1 Failure		Défaut GE1			Défaut GE1
1857	32		15		DG2 Failure			DG2 Failure		Défaut GE2			Défaut GE2
1858	32		15		Diesel Alarm Delay		DG Alarm Delay		Délais Alarme GE		Délais Alarm GE
1859	32		15		Grid is on			Grid is on		Secteur ON			Secteur ON
1860	32		15		PowerSplit Contactor Mode	Contactor Mode		Mode contacteur PowerSplit	Mode LVD PSplit	
1861	32		15		Ambient Temp		Amb Temp		Température Ambiante		Temp.Amb
1862	32		15		Ambient Temp Sensor		Amb Temp Sensor	Capteur Temp.Ambiante		Capt. Temp.Amb.
1863	32		15		None				None			Aucun				Aucun
1864	32		15		Temperature 1			Temperature 1			Capteur Temp.1			Capteur Temp. 1
1865	32		15		Temperature 2			Temperature 2			Capteur Temp.2			Capteur Temp. 2
1866	32		15		Temperature 3			Temperature 3			Capteur Temp.3			Capteur Temp. 3
1867	32		15		Temperature 4			Temperature 4			Capteur Temp.4			Capteur Temp. 4
1868	32		15		Temperature 5			Temperature 5			Capteur Temp.5			Capteur Temp. 5
1869	32		15		Temperature 6			Temperature 6			Capteur Temp.6			Capteur Temp. 6
1870	32		15		Temperature 7			Temperature 7			Capteur Temp.7			Capteur Temp.7
1871	32		15		Temperature 8			Temperature 8			Capteur Temp.8			Capteur Temp.8
1872	32		15		Temperature 9			Temperature 9			Capteur Temp.9			Capteur Temp.9
1873	32		15		Temperature 10			Temperature 10		Capteur Temp.10			Capteur Temp.10
1874	32		15		Ambient Temp High1		Amb Temp Hi1		Température Haute		Temp. Haute
1875	32		15		Ambient Temp Low		Amb Temp Low		Température Basse		Temp. Basse
1876	32		15		Ambient Temp High1		Amb Temp Hi1	Température Haute		Temp. Haute
1877	32		15		Ambient Temp Low		Amb Temp Low	Température Basse		Temp. Basse
1878	32		15		Ambient Sensor Fail		AmbSensor Fail			Défaut Capteur Température		Déf.Cap.Temp.
1879	32		15		Digital Input 1			DI1		Entrée 1			Entrée 1
1880	32		15		Digital Input 2			DI2		Entrée 2			Entrée 2
1881	32		15		Digital Input 3			DI3		Entrée 3			Entrée 3
1882	32		15		Digital Input 4			DI4		Entrée 4			Entrée 4
1883	32		15		Digital Input 5			DI5		Entrée 5			Entrée 5
1884	32		15		Digital Input 6			DI6		Entrée 6			Entrée 6
1885	32		15		Digital Input 7			DI7		Entrée 7			Entrée 7
1886	32		15		Digital Input 8			DI8		Entrée 8			Entrée 8
1887	32		15		Off				Off		Arrêt				Arrêt
1888	32		15		On				On		Marche				Marche
1889	32		15		DO1-Relay Output		DO1-RelayOutput		Sortie DO1 relais	Sortie DO1 Rel
1890	32		15		DO2-Relay Output		DO2-RelayOutput		Sortie DO2 relais	Sortie DO2 Rel
1891	32		15		DO3-Relay Output		DO3-RelayOutput		Sortie DO3 relais	Sortie DO3 Rel
1892	32		15		DO4-Relay Output		DO4-RelayOutput		Sortie DO4 relais	Sortie DO4 Rel
1893	32		15		DO5-Relay Output		DO5-RelayOutput		Sortie DO5 relais	Sortie DO5 Rel
1894	32		15		DO6-Relay Output		DO6-RelayOutput		Sortie DO6 relais	Sortie DO6 Rel
1895	32		15		DO7-Relay Output		DO7-RelayOutput		Sortie DO7 relais	Sortie DO7 Rel
1896	32		15		DO8-Relay Output		DO8-RelayOutput		Sortie DO8 relais	Sortie DO8 Rel
1897	32		15		DI1 Alarm State			DI1 Alm State		Etat Alarme Entrée 1		Etat Al.Entrée1
1898	32		15		DI2 Alarm State			DI2 Alm State		Etat Alarme Entrée 2		Etat Al.Entrée2
1899	32		15		DI3 Alarm State			DI3 Alm State		Etat Alarme Entrée 3		Etat Al.Entrée3
1900	32		15		DI4 Alarm State			DI4 Alm State		Etat Alarme Entrée 4		Etat Al.Entrée4
1901	32		15		DI5 Alarm State			DI5 Alm State		Etat Alarme Entrée 5		Etat Al.Entrée5
1902	32		15		DI6 Alarm State			DI6 Alm State		Etat Alarme Entrée 6		Etat Al.Entrée6
1903	32		15		DI7 Alarm State			DI7 Alm State		Etat Alarme Entrée 7		Etat Al.Entrée7
1904	32		15		DI8 Alarm State			DI8 Alm State		Etat Alarme Entrée 8		Etat Al.Entrée8

1905	32		15		DI1 Alarm			DI1 Alarm		Alarme Entrée 1			Alarme Entrée 1
1906	32		15		DI2 Alarm			DI2 Alarm		Alarme Entrée 2			Alarme Entrée 2
1907	32		15		DI3 Alarm			DI3 Alarm		Alarme Entrée 3			Alarme Entrée 3
1908	32		15		DI4 Alarm			DI4 Alarm		Alarme Entrée 4			Alarme Entrée 4
1909	32		15		DI5 Alarm			DI5 Alarm		Alarme Entrée 5			Alarme Entrée 5
1910	32		15		DI6 Alarm			DI6 Alarm		Alarme Entrée 6			Alarme Entrée 6
1911	32		15		DI7 Alarm			DI7 Alarm		Alarme Entrée 7			Alarme Entrée 7
1912	32		15		DI8 Alarm			DI8 Alarm		Alarme Entrée 8			Alarme Entrée 8

1913	32		15		On				On			Marche				Marche
1914	32		15		Off				Off			Arrêt				Arrêt
1915	32		15		High				High			Niveau Haut			Niveau Haut
1916	32		15		Low				Low			Niveau Bas			Niveau Bas
1917	32		15		IB Num				IB Num		Nombre carte IB			Nbre carte IB	
1918	32		15		IB Type				IB Type			Type carte IB			Type carte IB
1919	32		15		CSU_Undervoltage 1		Undervoltage 1		CSU Sous-Tension 1		CSU Sous U 1
1920	32		15		CSU_Undervoltage 2		Undervoltage 2		CSU Sous-Tension 2		CSU Sous U 2
1921	32		15		CSU_Overvoltage			Overvoltage			CSU Sur-Tension 2		CSU Sur U 2
1922	32		15		CSU_Communication Fail		Comm Fail		CSU Défaut Communication	CSU Def.Comm.
1923	32		15		CSU_External Alarm 1		Exter_Alarm1	CSU Alarme Externe 1		CSU Al.Ext.1
1924	32		15		CSU_External Alarm 2		Exter_Alarm2		CSU Alarme Externe 2		CSU Al.Ext.2
1925	32		15		CSU_External Alarm 3		Exter_Alarm3	CSU Alarme Externe 3		CSU Al.Ext.3
1926	32		15		CSU_External Alarm 4		Exter_Alarm4		CSU Alarme Externe 4		CSU Al.Ext.4
1927	32		15		CSU_External Alarm 5		Exter_Alarm5		CSU Alarme Externe 5		CSU Al.Ext.5
1928	32		15		CSU_External Alarm 6		Exter_Alarm6		CSU Alarme Externe 6		CSU Al.Ext.6
1929	32		15		CSU_External Alarm 7		Exter_Alarm7		CSU Alarme Externe 7		CSU Al.Ext.7
1930	32		15		CSU_External Alarm 8		Exter_Alarm8	CSU Alarme Externe 8		CSU Al.Ext.8
1931	32		15		CSU_External Comm Fail		Ext_Comm Fail		CSU External Défaut Comm		CSU Ext.Déf.Com
1932	32		15		CSU_Bat_Curr Limit Alarm	Bat_Curr Limit		CSU Batt Alarme Lim.Courant		CSUBat AlmLim.I
1933	32		15		DC LC Number			DC LC Num		Nombre DCLC			Nombre DCLC
1934	32		15		Bat LC Number			Bat LC Num	Nombre BatLC			Nombre BatLC
1935	32		15		Ambient Temp High2		Amb Temp Hi2		Très haute Température		Très haute Temp
1936	32		15		System Type			System Type		System Type			System Type
1937	32		15		Normal				Normal			Normal				Normal
1938	32		15		Test				Test			Test				Test
1939	32		15		Auto Mode			Auto Mode		Mode Automatique		Mode Auto.
1940	32		15		EMEA				EMEA			EMEA				EMEA
1941	32		15		Normal				Normal			Normal				Normal

1942	32		15		Bus Run Mode			Bus Run Mode		Bus Run Mode			Bus Run Mode
1943	32		15		NO				NO			NO		NO
1944	32		15		NC				NC			NC		NC
1945	32		15		Fail Safe Mode(Hybrid)		Fail Safe		Fail Safe Mode(Hybrid)		Fail Safe	
1946	32		15		IB Communication Fail		IB Comm Fail		IB Défaut communication		IB Défaut Com	

1947	32		15		Relay Test			Relay Test		Test relais				Test relais
1948	32		15		NCU DO1 Test			NCU DO1 Test			Test relais 1				Test relais 1
1949	32		15		NCU DO2 Test			NCU DO2 Test			Test relais 2				Test relais 2
1950	32		15		NCU DO3 Test			NCU DO3 Test			Test relais 3				Test relais 3
1951	32		15		NCU DO4 Test			NCU DO4 Test			Test relais 4				Test relais 4
1952	32		15		Relay 5 Test			Relay 5 Test			Test relais 5				Test relais 5
1953	32		15		Relay 6 Test			Relay 6 Test			Test relais 6				Test relais 6
1954	32		15		Relay 7 Test			Relay 7 Test			Test relais 7				Test relais 7
1955	32		15		Relay 8 Test			Relay 8 Test			Test relais 8				Test relais 8
1956	32		15		Relay Test			Relay Test			Test relais				Test relais
1957	32		15		Relay Test Time			Relay Test Time			Heure de test relais			Heure test rel.
1958	32		15		Manual			Manual			Manuel			Manuel
1959	32		15		Automatic			Automatic			Automatique				Automatique

1960	32		15		System Temp1		System T1	Temp1 (ACU)		Temp1 (ACU)
1961	32		15		System Temp2		System T2	Temp2 (ACU)		Temp2 (ACU)
1962	32		15		System Temp3		System T3	Temp3 (OB)		Temp3 (OB)
1963	32		15		IB2-1 Temp1		IB2-1 T1	IB2-1-Temp1		IB2-1-Temp1
1964	32		15		IB2-1 Temp2		IB2-1 T2	IB2-1-Temp2		IB2-1-Temp2
1965	32		15		EIB-1 Temp1		EIB-1 T1	EIB-1-Temp1		EIB-1-Temp1
1966	32		15		EIB-1 Temp2		EIB-1 T2	EIB-1-Temp2		EIB-1-Temp2
1967	32		15		SMTemp1 Temp1		SMTemp1 T1	SMTemp1 Temp1		SMTemp1 Temp1
1968	32		15		SMTemp1 Temp2		SMTemp1 T2	SMTemp1 Temp2		SMTemp1 Temp2
1969	32		15		SMTemp1 Temp3		SMTemp1 T3	SMTemp1 Temp3		SMTemp1 Temp3
1970	32		15		SMTemp1 Temp4		SMTemp1 T4	SMTemp1 Temp4		SMTemp1 Temp4
1971	32		15		SMTemp1 Temp5		SMTemp1 T5	SMTemp1 Temp5		SMTemp1 Temp5
1972	32		15		SMTemp1 Temp6		SMTemp1 T6	SMTemp1 Temp6		SMTemp1 Temp6
1973	32		15		SMTemp1 Temp7		SMTemp1 T7	SMTemp1 Temp7		SMTemp1 Temp7
1974	32		15		SMTemp1 Temp8		SMTemp1 T8	SMTemp1 Temp8		SMTemp1 Temp8
1975	32		15		SMTemp2 Temp1		SMTemp2 T1	SMTemp2 Temp1		SMTemp2 Temp1
1976	32		15		SMTemp2 Temp2		SMTemp2 T2	SMTemp2 Temp2		SMTemp2 Temp2
1977	32		15		SMTemp2 Temp3		SMTemp2 T3	SMTemp2 Temp3		SMTemp2 Temp3
1978	32		15		SMTemp2 Temp4		SMTemp2 T4	SMTemp2 Temp4		SMTemp2 Temp4
1979	32		15		SMTemp2 Temp5		SMTemp2 T5	SMTemp2 Temp5		SMTemp2 Temp5
1980	32		15		SMTemp2 Temp6		SMTemp2 T6	SMTemp2 Temp6		SMTemp2 Temp6
1981	32		15		SMTemp2 Temp7		SMTemp2 T7	SMTemp2 Temp7		SMTemp2 Temp7
1982	32		15		SMTemp2 Temp8		SMTemp2 T8	SMTemp2 Temp8		SMTemp2 Temp8
1983	32		15		SMTemp3 Temp1		SMTemp3 T1	SMTemp3 Temp1		SMTemp3 Temp1
1984	32		15		SMTemp3 Temp2		SMTemp3 T2	SMTemp3 Temp2		SMTemp3 Temp2
1985	32		15		SMTemp3 Temp3		SMTemp3 T3	SMTemp3 Temp3		SMTemp3 Temp3
1986	32		15		SMTemp3 Temp4		SMTemp3 T4	SMTemp3 Temp4		SMTemp3 Temp4
1987	32		15		SMTemp3 Temp5		SMTemp3 T5	SMTemp3 Temp5		SMTemp3 Temp5
1988	32		15		SMTemp3 Temp6		SMTemp3 T6	SMTemp3 Temp6		SMTemp3 Temp6
1989	32		15		SMTemp3 Temp7		SMTemp3 T7	SMTemp3 Temp7		SMTemp3 Temp7
1990	32		15		SMTemp3 Temp8		SMTemp3 T8	SMTemp3 Temp8		SMTemp3 Temp8
1991	32		15		SMTemp4 Temp1		SMTemp4 T1	SMTemp4 Temp1		SMTemp4 Temp1
1992	32		15		SMTemp4 Temp2		SMTemp4 T2	SMTemp4 Temp2		SMTemp4 Temp2
1993	32		15		SMTemp4 Temp3		SMTemp4 T3	SMTemp4 Temp3		SMTemp4 Temp3
1994	32		15		SMTemp4 Temp4		SMTemp4 T4	SMTemp4 Temp4		SMTemp4 Temp4
1995	32		15		SMTemp4 Temp5		SMTemp4 T5	SMTemp4 Temp5		SMTemp4 Temp5
1996	32		15		SMTemp4 Temp6		SMTemp4 T6	SMTemp4 Temp6		SMTemp4 Temp6
1997	32		15		SMTemp4 Temp7		SMTemp4 T7	SMTemp4 Temp7		SMTemp4 Temp7
1998	32		15		SMTemp4 Temp8		SMTemp4 T8	SMTemp4 Temp8		SMTemp4 Temp8
1999	32		15		SMTemp5 Temp1		SMTemp5 T1	SMTemp5 Temp1		SMTemp5 Temp1
2000	32		15		SMTemp5 Temp2		SMTemp5 T2	SMTemp5 Temp2		SMTemp5 Temp2
2001	32		15		SMTemp5 Temp3		SMTemp5 T3	SMTemp5 Temp3		SMTemp5 Temp3
2002	32		15		SMTemp5 Temp4		SMTemp5 T4	SMTemp5 Temp4		SMTemp5 Temp4
2003	32		15		SMTemp5 Temp5		SMTemp5 T5	SMTemp5 Temp5		SMTemp5 Temp5
2004	32		15		SMTemp5 Temp6		SMTemp5 T6	SMTemp5 Temp6		SMTemp5 Temp6
2005	32		15		SMTemp5 Temp7		SMTemp5 T7	SMTemp5 Temp7		SMTemp5 Temp7
2006	32		15		SMTemp5 Temp8		SMTemp5 T8	SMTemp5 Temp8		SMTemp5 Temp8
2007	32		15		SMTemp6 Temp1		SMTemp6 T1	SMTemp6 Temp1		SMTemp6 Temp1
2008	32		15		SMTemp6 Temp2		SMTemp6 T2	SMTemp6 Temp2		SMTemp6 Temp2
2009	32		15		SMTemp6 Temp3		SMTemp6 T3	SMTemp6 Temp3		SMTemp6 Temp3
2010	32		15		SMTemp6 Temp4		SMTemp6 T4	SMTemp6 Temp4		SMTemp6 Temp4
2011	32		15		SMTemp6 Temp5		SMTemp6 T5	SMTemp6 Temp5		SMTemp6 Temp5
2012	32		15		SMTemp6 Temp6		SMTemp6 T6	SMTemp6 Temp6		SMTemp6 Temp6
2013	32		15		SMTemp6 Temp7		SMTemp6 T7	SMTemp6 Temp7		SMTemp6 Temp7
2014	32		15		SMTemp6 Temp8		SMTemp6 T8	SMTemp6 Temp8		SMTemp6 Temp8
2015	32		15		SMTemp7 Temp1		SMTemp7 T1	SMTemp7 Temp1		SMTemp7 Temp1
2016	32		15		SMTemp7 Temp2		SMTemp7 T2	SMTemp7 Temp2		SMTemp7 Temp2
2017	32		15		SMTemp7 Temp3		SMTemp7 T3	SMTemp7 Temp3		SMTemp7 Temp3
2018	32		15		SMTemp7 Temp4		SMTemp7 T4	SMTemp7 Temp4		SMTemp7 Temp4
2019	32		15		SMTemp7 Temp5		SMTemp7 T5	SMTemp7 Temp5		SMTemp7 Temp5
2020	32		15		SMTemp7 Temp6		SMTemp7 T6	SMTemp7 Temp6		SMTemp7 Temp6
2021	32		15		SMTemp7 Temp7		SMTemp7 T7	SMTemp7 Temp7		SMTemp7 Temp7
2022	32		15		SMTemp7 Temp8		SMTemp7 T8	SMTemp7 Temp8		SMTemp7 Temp8
2023	32		15		SMTemp8 Temp1		SMTemp8 T1	SMTemp8 Temp1		SMTemp8 Temp1
2024	32		15		SMTemp8 Temp2		SMTemp8 T2	SMTemp8 Temp2		SMTemp8 Temp2
2025	32		15		SMTemp8 Temp3		SMTemp8 T3	SMTemp8 Temp3		SMTemp8 Temp3
2026	32		15		SMTemp8 Temp4		SMTemp8 T4	SMTemp8 Temp4		SMTemp8 Temp4
2027	32		15		SMTemp8 Temp5		SMTemp8 T5	SMTemp8 Temp5		SMTemp8 Temp5
2028	32		15		SMTemp8 Temp6		SMTemp8 T6	SMTemp8 Temp6		SMTemp8 Temp6
2029	32		15		SMTemp8 Temp7		SMTemp8 T7	SMTemp8 Temp7		SMTemp8 Temp7
2030	32		15		SMTemp8 Temp8		SMTemp8 T8	SMTemp8 Temp8		SMTemp8 Temp8

2031		32		15		System Temp1 High 2	System T1 Hi2	Température 1 Système Très haute		T1.Sys Très hau
2032		32		15		System Temp1 High 1	System T1 Hi1	Température 1 Système Haute			T1.Sys Haute
2033		32		15		System Temp1 Low	System T1 Low	Température 1 Système Basse			T1.Sys Basse
2034		32		15		System Temp2 High 2	System T2 Hi2	Température 2 Système Très haute		T2.Sys Très hau
2035		32		15		System Temp2 High 1	System T2 Hi1	Température 2 Système Haute			T2.Sys Haute
2036		32		15		System Temp2 Low	System T2 Low	Température 2 Système Basse			T2.Sys Basse
2037		32		15		System Temp3 High 2	System T3 Hi2	Température 3 Système Très haute		T3.Sys Très hau
2038		32		15		System Temp3 High 1	System T3 Hi1	Température 3 Système Haute			T3.Sys Haute
2039		32		15		System Temp3 Low	System T3 Low	Température 3 Système Basse			T3.Sys Basse
2040		32		15		IB2-1 Temp1 High 2	IB2-1 T1 Hi2	Température 1 IB2-1 Très haute		T1.IB2-1 Haute2
2041		32		15		IB2-1 Temp1 High 1	IB2-1 T1 Hi1	Température 1 IB2-1 Haute			T1.IB2-1 Haute2
2042		32		15		IB2-1 Temp1 Low		IB2-1 T1 Low	Température 1 IB2-1 Basse			T1.IB2-1 Basse
2043		32		15		IB2-1 Temp2 High 2	IB2-1 T2 Hi2	Température 2 IB2-1 Très haute		T2.IB2-1 Haute2
2044		32		15		IB2-1 Temp2 High 1	IB2-1 T2 Hi1	Température 2 IB2-1 Haute			T2.IB2-1  Haute
2045		32		15		IB2-1 Temp2 Low		IB2-1 T2 Low	Température 2 IB2-1 Basse			T2.IB2-1 Basse
2046		32		15		EIB-1 Temp1 High 2	EIB-1 T1 Hi2	Température 1 EIB-1 Très haute		T1.EIB-1 Haute2
2047		32		15		EIB-1 Temp1 High 1	EIB-1 T1 Hi1	Température 1 EIB-1 Haute			T1.EIB-1  Haute
2048		32		15		EIB-1 Temp1 Low		EIB-1 T1 Low	Température 1 EIB-1 Basse			T1.EIB-1 Basse
2049		32		15		EIB-1 Temp2 High 2	EIB-1 T2 Hi2	Température 2 EIB-1 Très haute		T1.EIB-1 Haute2
2050		32		15		EIB-1 Temp2 High 1	EIB-1 T2 Hi1	Température 2 EIB-1 Haute			T2.EIB-1  Haute
2051		32		15		EIB-1 Temp2 Low		EIB-1 T2 Low	Température 2 EIB-1 Basse			T2.EIB-1 Basse
2052		32		15		SMTemp1 Temp1 High 2	SMTemp1 T1 Hi2		SMTemp1 Temp 1 Très haute		SMTemp1 T1 THau
2053		32		15		SMTemp1 Temp1 High 1	SMTemp1 T1 Hi1		SMTemp1 Temp 1 Haute			SMTemp1 T1 HauT
2054		32		15		SMTemp1 Temp1 Low	SMTemp1 T1 Low		SMTemp1 Temp 1 Basse			SMTemp1 T1 Bass
2055		32		15		SMTemp1 Temp2 High 2	SMTemp1 T2 Hi2		SMTemp1 Temp 2 Très haute		SMTemp1 T2 THau
2056		32		15		SMTemp1 Temp2 High 1	SMTemp1 T2 Hi1		SMTemp1 Temp 2 Haute			SMTemp1 T2 HauT
2057		32		15		SMTemp1 Temp2 Low	SMTemp1 T2 Low		SMTemp1 Temp 2 Basse			SMTemp1 T2 Bass
2058		32		15		SMTemp1 Temp3 High 2	SMTemp1 T3 Hi2		SMTemp1 Temp 3 Très haute		SMTemp1 T3 THau
2059		32		15		SMTemp1 Temp3 High 1	SMTemp1 T3 Hi1		SMTemp1 Temp 3 Haute			SMTemp1 T3 HauT
2060		32		15		SMTemp1 Temp3 Low	SMTemp1 T3 Low		SMTemp1 Temp 3 Basse			SMTemp1 T3 Bass
2061		32		15		SMTemp1 Temp4 High 2	SMTemp1 T4 Hi2		SMTemp1 Temp 4 Très haute		SMTemp1 T4 THau
2062		32		15		SMTemp1 Temp4 High 1	SMTemp1 T4 Hi1		SMTemp1 Temp 4 Haute			SMTemp1 T4 HauT
2063		32		15		SMTemp1 Temp4 Low	SMTemp1 T4 Low		SMTemp1 Temp 4 Basse			SMTemp1 T4 Bass
2064		32		15		SMTemp1 Temp5 High 2	SMTemp1 T5 Hi2		SMTemp1 Temp 5 Très haute		SMTemp1 T5 THau
2065		32		15		SMTemp1 Temp5 High 1	SMTemp1 T5 Hi1		SMTemp1 Temp 5 Haute			SMTemp1 T5 HauT
2066		32		15		SMTemp1 Temp5 Low	SMTemp1 T5 Low		SMTemp1 Temp 5 Basse			SMTemp1 T5 Bass
2067		32		15		SMTemp1 Temp6 High 2	SMTemp1 T6 Hi2		SMTemp1 Temp 6 Très haute		SMTemp1 T6 THau
2068		32		15		SMTemp1 Temp6 High 1	SMTemp1 T6 Hi1		SMTemp1 Temp 6 Haute			SMTemp1 T6 HauT
2069		32		15		SMTemp1 Temp6 Low	SMTemp1 T6 Low		SMTemp1 Temp 6 Basse			SMTemp1 T6 Bass
2070		32		15		SMTemp1 Temp7 High 2	SMTemp1 T7 Hi2		SMTemp1 Temp 7 Très haute		SMTemp1 T7 THau
2071		32		15		SMTemp1 Temp7 High 1	SMTemp1 T7 Hi1		SMTemp1 Temp 7 Haute			SMTemp1 T7 HauT
2072		32		15		SMTemp1 Temp7 Low	SMTemp1 T7 Low		SMTemp1 Temp 7 Basse			SMTemp1 T7 Bass
2073		32		15		SMTemp1 Temp8 High 2	SMTemp1 T8 Hi2		SMTemp1 Temp 8 Très haute		SMTemp1 T8 THau
2074		32		15		SMTemp1 Temp8 High 1	SMTemp1 T8 Hi1		SMTemp1 Temp 8 Haute			SMTemp1 T8 HauT
2075		32		15		SMTemp1 Temp8 Low	SMTemp1 T8 Low		SMTemp1 Temp 8 Basse			SMTemp1 T8 Bass
2076		32		15		SMTemp2 Temp1 High 2	SMTemp2 T1 Hi2		SMTemp2 Temp 1 Très haute		SMTemp2 T1 THau
2077		32		15		SMTemp2 Temp1 High 1	SMTemp2 T1 Hi1		SMTemp2 Temp 1 Haute			SMTemp2 T1 HauT
2078		32		15		SMTemp2 Temp1 Low	SMTemp2 T1 Low		SMTemp2 Temp 1 Basse			SMTemp2 T1 Bass
2079		32		15		SMTemp2 Temp2 High 2	SMTemp2 T2 Hi2		SMTemp2 Temp 2 Très haute		SMTemp2 T2 THau
2080		32		15		SMTemp2 Temp2 High 1	SMTemp2 T2 Hi1		SMTemp2 Temp 2 Haute			SMTemp2 T2 HauT
2081		32		15		SMTemp2 Temp2 Low	SMTemp2 T2 Low		SMTemp2 Temp 2 Basse			SMTemp2 T2 Bass
2082		32		15		SMTemp2 Temp3 High 2	SMTemp2 T3 Hi2		SMTemp2 Temp 3 Très haute		SMTemp2 T3 THau
2083		32		15		SMTemp2 Temp3 High 1	SMTemp2 T3 Hi1		SMTemp2 Temp 3 Haute			SMTemp2 T3 HauT
2084		32		15		SMTemp2 Temp3 Low	SMTemp2 T3 Low		SMTemp2 Temp 3 Basse			SMTemp2 T3 Bass
2085		32		15		SMTemp2 Temp4 High 2	SMTemp2 T4 Hi2		SMTemp2 Temp 4 Très haute		SMTemp2 T4 THau
2086		32		15		SMTemp2 Temp4 High 1	SMTemp2 T4 Hi1		SMTemp2 Temp 4 Haute			SMTemp2 T4 HauT
2087		32		15		SMTemp2 Temp4 Low	SMTemp2 T4 Low		SMTemp2 Temp 4 Basse			SMTemp2 T4 Bass
2088		32		15		SMTemp2 Temp5 High 2	SMTemp2 T5 Hi2		SMTemp2 Temp 5 Très haute		SMTemp2 T5 THau
2089		32		15		SMTemp2 Temp5 High 1	SMTemp2 T5 Hi1		SMTemp2 Temp 5 Haute			SMTemp2 T5 HauT
2090		32		15		SMTemp2 Temp5 Low	SMTemp2 T5 Low		SMTemp2 Temp 5 Basse			SMTemp2 T5 Bass
2091		32		15		SMTemp2 Temp6 High 2	SMTemp2 T6 Hi2		SMTemp2 Temp 6 Très haute		SMTemp2 T6 THau
2092		32		15		SMTemp2 Temp6 High 1	SMTemp2 T6 Hi1		SMTemp2 Temp 6 Haute			SMTemp2 T6 HauT
2093		32		15		SMTemp2 Temp6 Low	SMTemp2 T6 Low		SMTemp2 Temp 6 Basse			SMTemp2 T6 Bass
2094		32		15		SMTemp2 Temp7 High 2	SMTemp2 T7 Hi2		SMTemp2 Temp 7 Très haute		SMTemp2 T7 THau
2095		32		15		SMTemp2 Temp7 High 1	SMTemp2 T7 Hi1		SMTemp2 Temp 7 Haute			SMTemp2 T7 HauT
2096		32		15		SMTemp2 Temp7 Low	SMTemp2 T7 Low		SMTemp2 Temp 7 Basse			SMTemp2 T7 Bass
2097		32		15		SMTemp2 Temp8 High 2	SMTemp2 T8 Hi2		SMTemp2 Temp 8 Très haute		SMTemp2 T8 THau
2098		32		15		SMTemp2 Temp8 High 1	SMTemp2 T8 Hi1		SMTemp2 Temp 8 Haute			SMTemp2 T8 HauT
2099		32		15		SMTemp2 Temp8 Low	SMTemp2 T8 Low		SMTemp2 Temp 8 Basse			SMTemp2 T8 Bass
2100		32		15		SMTemp3 Temp1 High 2	SMTemp3 T1 Hi2		SMTemp3 Temp 1 Très haute		SMTemp3 T1 THau
2101		32		15		SMTemp3 Temp1 High 1	SMTemp3 T1 Hi1		SMTemp3 Temp 1 Haute			SMTemp3 T1 HauT
2102		32		15		SMTemp3 Temp1 Low	SMTemp3 T1 Low		SMTemp3 Temp 1 Basse			SMTemp3 T1 Bass
2103		32		15		SMTemp3 Temp2 High 2	SMTemp3 T2 Hi2		SMTemp3 Temp 2 Très haute		SMTemp3 T2 THau
2104		32		15		SMTemp3 Temp2 High 1	SMTemp3 T2 Hi1		SMTemp3 Temp 2 Haute			SMTemp3 T2 HauT
2105		32		15		SMTemp3 Temp2 Low	SMTemp3 T2 Low		SMTemp3 Temp 2 Basse			SMTemp3 T2 Bass
2106		32		15		SMTemp3 Temp3 High 2	SMTemp3 T3 Hi2		SMTemp3 Temp 3 Très haute		SMTemp3 T3 THau
2107		32		15		SMTemp3 Temp3 High 1	SMTemp3 T3 Hi1		SMTemp3 Temp 3 Haute			SMTemp3 T3 HauT
2108		32		15		SMTemp3 Temp3 Low	SMTemp3 T3 Low		SMTemp3 Temp 3 Basse			SMTemp3 T3 Bass
2109		32		15		SMTemp3 Temp4 High 2	SMTemp3 T4 Hi2		SMTemp3 Temp 4 Très haute		SMTemp3 T4 THau
2110		32		15		SMTemp3 Temp4 High 1	SMTemp3 T4 Hi1		SMTemp3 Temp 4 Haute			SMTemp3 T4 HauT
2111		32		15		SMTemp3 Temp4 Low	SMTemp3 T4 Low		SMTemp3 Temp 4 Basse			SMTemp3 T4 Bass
2112		32		15		SMTemp3 Temp5 High 2	SMTemp3 T5 Hi2		SMTemp3 Temp 5 Très haute		SMTemp3 T5 THau
2113		32		15		SMTemp3 Temp5 High 1	SMTemp3 T5 Hi1		SMTemp3 Temp 5 Haute			SMTemp3 T5 HauT
2114		32		15		SMTemp3 Temp5 Low	SMTemp3 T5 Low		SMTemp3 Temp 5 Basse			SMTemp3 T5 Bass
2115		32		15		SMTemp3 Temp6 High 2	SMTemp3 T6 Hi2		SMTemp3 Temp 6 Très haute		SMTemp3 T6 THau
2116		32		15		SMTemp3 Temp6 High 1	SMTemp3 T6 Hi1		SMTemp3 Temp 6 Haute			SMTemp3 T6 HauT
2117		32		15		SMTemp3 Temp6 Low	SMTemp3 T6 Low		SMTemp3 Temp 6 Basse			SMTemp3 T6 Bass
2118		32		15		SMTemp3 Temp7 High 2	SMTemp3 T7 Hi2		SMTemp3 Temp 7 Très haute		SMTemp3 T7 THau
2119		32		15		SMTemp3 Temp7 High 1	SMTemp3 T7 Hi1		SMTemp3 Temp 7 Haute			SMTemp3 T7 HauT
2120		32		15		SMTemp3 Temp7 Low	SMTemp3 T7 Low		SMTemp3 Temp 7 Basse			SMTemp3 T7 Bass
2121		32		15		SMTemp3 Temp8 High 2	SMTemp3 T8 Hi2		SMTemp3 Temp 8 Très haute		SMTemp3 T8 THau
2122		32		15		SMTemp3 Temp8 High 1	SMTemp3 T8 Hi1		SMTemp3 Temp 8 Haute			SMTemp3 T8 HauT
2123		32		15		SMTemp3 Temp8 Low	SMTemp3 T8 Low		SMTemp3 Temp 8 Basse			SMTemp3 T8 Bass
2124		32		15		SMTemp4 Temp1 High 2	SMTemp4 T1 Hi2		SMTemp4 Temp 1 Très haute		SMTemp4 T1 THau
2125		32		15		SMTemp4 Temp1 High 1	SMTemp4 T1 Hi1		SMTemp4 Temp 1 Haute			SMTemp4 T1 HauT
2126		32		15		SMTemp4 Temp1 Low	SMTemp4 T1 Low		SMTemp4 Temp 1 Basse			SMTemp4 T1 Bass
2127		32		15		SMTemp4 Temp2 High 2	SMTemp4 T2 Hi2		SMTemp4 Temp 2 Très haute		SMTemp4 T2 THau
2128		32		15		SMTemp4 Temp2 High 1	SMTemp4 T2 Hi1		SMTemp4 Temp 2 Haute			SMTemp4 T2 HauT
2129		32		15		SMTemp4 Temp2 Low	SMTemp4 T2 Low		SMTemp4 Temp 2 Basse			SMTemp4 T2 Bass
2130		32		15		SMTemp4 Temp3 High 2	SMTemp4 T3 Hi2		SMTemp4 Temp 3 Très haute		SMTemp4 T3 THau
2131		32		15		SMTemp4 Temp3 High 1	SMTemp4 T3 Hi1		SMTemp4 Temp 3 Haute			SMTemp4 T3 HauT
2132		32		15		SMTemp4 Temp3 Low	SMTemp4 T3 Low		SMTemp4 Temp 3 Basse			SMTemp4 T3 Bass
2133		32		15		SMTemp4 Temp4 High 2	SMTemp4 T4 Hi2		SMTemp4 Temp 4 Très haute		SMTemp4 T4 THau
2134		32		15		SMTemp4 Temp4 High 1	SMTemp4 T4 Hi1		SMTemp4 Temp 4 Haute			SMTemp4 T4 HauT
2135		32		15		SMTemp4 Temp4 Low	SMTemp4 T4 Low		SMTemp4 Temp 4 Basse			SMTemp4 T4 Bass
2136		32		15		SMTemp4 Temp5 High 2	SMTemp4 T5 Hi2		SMTemp4 Temp 5 Très haute		SMTemp4 T5 THau
2137		32		15		SMTemp4 Temp5 High 1	SMTemp4 T5 Hi1		SMTemp4 Temp 5 Haute			SMTemp4 T5 HauT
2138		32		15		SMTemp4 Temp5 Low	SMTemp4 T5 Low		SMTemp4 Temp 5 Basse			SMTemp4 T5 Bass
2139		32		15		SMTemp4 Temp6 High 2	SMTemp4 T6 Hi2		SMTemp4 Temp 6 Très haute		SMTemp4 T6 THau
2140		32		15		SMTemp4 Temp6 High 1	SMTemp4 T6 Hi1		SMTemp4 Temp 6 Haute			SMTemp4 T6 HauT
2141		32		15		SMTemp4 Temp6 Low	SMTemp4 T6 Low		SMTemp4 Temp 6 Basse			SMTemp4 T6 Bass
2142		32		15		SMTemp4 Temp7 High 2	SMTemp4 T7 Hi2		SMTemp4 Temp 7 Très haute		SMTemp4 T7 THau
2143		32		15		SMTemp4 Temp7 High 1	SMTemp4 T7 Hi1		SMTemp4 Temp 7 Haute			SMTemp4 T7 HauT
2144		32		15		SMTemp4 Temp7 Low	SMTemp4 T7 Low		SMTemp4 Temp 7 Basse			SMTemp4 T7 Bass
2145		32		15		SMTemp4 Temp8 High 2	SMTemp4 T8 Hi2		SMTemp4 Temp 8 Très haute		SMTemp4 T8 THau
2146		32		15		SMTemp4 Temp8 Hi1	SMTemp4 T8 Hi1		SMTemp4 Temp 8 Haute			SMTemp4 T8 HauT
2147		32		15		SMTemp4 Temp8 Low	SMTemp4 T8 Low		SMTemp4 Temp 8 Basse			SMTemp4 T8 Bass
2148		32		15		SMTemp5 Temp1 High 2	SMTemp5 T1 Hi2		SMTemp5 Temp 1 Très haute		SMTemp5 T1 THau
2149		32		15		SMTemp5 Temp1 High 1	SMTemp5 T1 Hi1		SMTemp5 Temp 1 Haute			SMTemp5 T1 HauT
2150		32		15		SMTemp5 Temp1 Low	SMTemp5 T1 Low		SMTemp5 Temp 1 Basse			SMTemp5 T1 Bass
2151		32		15		SMTemp5 Temp2 High 2	SMTemp5 T2 Hi2		SMTemp5 Temp 2 Très haute		SMTemp5 T2 THau
2152		32		15		SMTemp5 Temp2 High 1	SMTemp5 T2 Hi1		SMTemp5 Temp 2 Haute			SMTemp5 T2 HauT
2153		32		15		SMTemp5 Temp2 Low	SMTemp5 T2 Low		SMTemp5 Temp 2 Basse			SMTemp5 T2 Bass
2154		32		15		SMTemp5 Temp3 High 2	SMTemp5 T3 Hi2		SMTemp5 Temp 3 Très haute		SMTemp5 T3 THau
2155		32		15		SMTemp5 Temp3 High 1	SMTemp5 T3 Hi1		SMTemp5 Temp 3 Haute			SMTemp5 T3 HauT
2156		32		15		SMTemp5 Temp3 Low	SMTemp5 T3 Low		SMTemp5 Temp 3 Basse			SMTemp5 T3 Bass
2157		32		15		SMTemp5 Temp4 High 2	SMTemp5 T4 Hi2		SMTemp5 Temp 4 Très haute		SMTemp5 T4 THau
2158		32		15		SMTemp5 Temp4 High 1	SMTemp5 T4 Hi1		SMTemp5 Temp 4 Haute			SMTemp5 T4 HauT
2159		32		15		SMTemp5 Temp4 Low	SMTemp5 T4 Low		SMTemp5 Temp 4 Basse			SMTemp5 T4 Bass
2160		32		15		SMTemp5 Temp5 High 2	SMTemp5 T5 Hi2		SMTemp5 Temp 5 Très haute		SMTemp5 T5 THau
2161		32		15		SMTemp5 Temp5 High 1	SMTemp5 T5 Hi1		SMTemp5 Temp 5 Haute			SMTemp5 T5 HauT
2162		32		15		SMTemp5 Temp5 Low	SMTemp5 T5 Low		SMTemp5 Temp 5 Basse			SMTemp5 T5 Bass
2163		32		15		SMTemp5 Temp6 High 2	SMTemp5 T6 Hi2		SMTemp5 Temp 6 Très haute		SMTemp5 T6 THau
2164		32		15		SMTemp5 Temp6 High 1	SMTemp5 T6 Hi1		SMTemp5 Temp 6 Haute			SMTemp5 T6 HauT
2165		32		15		SMTemp5 Temp6 Low	SMTemp5 T6 Low		SMTemp5 Temp 6 Basse			SMTemp5 T6 Bass
2166		32		15		SMTemp5 Temp7 High 2	SMTemp5 T7 Hi2		SMTemp5 Temp 7 Très haute		SMTemp5 T7 THau
2167		32		15		SMTemp5 Temp7 High 1	SMTemp5 T7 Hi1		SMTemp5 Temp 7 Haute			SMTemp5 T7 HauT
2168		32		15		SMTemp5 Temp7 Low	SMTemp5 T7 Low		SMTemp5 Temp 7 Basse			SMTemp5 T7 Bass
2169		32		15		SMTemp5 Temp8 High 2	SMTemp5 T8 Hi2		SMTemp5 Temp 8 Très haute		SMTemp5 T8 THau
2170		32		15		SMTemp5 Temp8 High 1	SMTemp5 T8 Hi1		SMTemp5 Temp 8 Haute			SMTemp5 T8 HauT
2171		32		15		SMTemp5 Temp8 Low	SMTemp5 T8 Low		SMTemp5 Temp 8 Basse			SMTemp5 T8 Bass
2172		32		15		SMTemp6 Temp1 High 2	SMTemp6 T1 Hi2		SMTemp6 Temp 1 Très haute		SMTemp6 T1 THau
2173		32		15		SMTemp6 Temp1 High 1	SMTemp6 T1 Hi1		SMTemp6 Temp 1 Haute			SMTemp6 T1 HauT
2174		32		15		SMTemp6 Temp1 Low	SMTemp6 T1 Low		SMTemp6 Temp 1 Basse			SMTemp6 T1 Bass
2175		32		15		SMTemp6 Temp2 High 2	SMTemp6 T2 Hi2		SMTemp6 Temp 2 Très haute		SMTemp6 T2 THau
2176		32		15		SMTemp6 Temp2 High 1	SMTemp6 T2 Hi1		SMTemp6 Temp 2 Haute			SMTemp6 T2 HauT
2177		32		15		SMTemp6 Temp2 Low	SMTemp6 T2 Low		SMTemp6 Temp 2 Basse			SMTemp6 T2 Bass
2178		32		15		SMTemp6 Temp3 High 2	SMTemp6 T3 Hi2		SMTemp6 Temp 3 Très haute		SMTemp6 T3 THau
2179		32		15		SMTemp6 Temp3 High 1	SMTemp6 T3 Hi1		SMTemp6 Temp 3 Haute			SMTemp6 T3 HauT
2180		32		15		SMTemp6 Temp3 Low	SMTemp6 T3 Low		SMTemp6 Temp 3 Basse			SMTemp6 T3 Bass
2181		32		15		SMTemp6 Temp4 High 2	SMTemp6 T4 Hi2		SMTemp6 Temp 4 Très haute		SMTemp6 T4 THau
2182		32		15		SMTemp6 Temp4 High 1	SMTemp6 T4 Hi1		SMTemp6 Temp 4 Haute			SMTemp6 T4 HauT
2183		32		15		SMTemp6 Temp4 Low	SMTemp6 T4 Low		SMTemp6 Temp 4 Basse			SMTemp6 T4 Bass
2184		32		15		SMTemp6 Temp5 High 2	SMTemp6 T5 Hi2		SMTemp6 Temp 5 Très haute		SMTemp6 T5 THau
2185		32		15		SMTemp6 Temp5 High 1	SMTemp6 T5 Hi1		SMTemp6 Temp 5 Haute			SMTemp6 T5 HauT
2186		32		15		SMTemp6 Temp5 Low	SMTemp6 T5 Low		SMTemp6 Temp 5 Basse			SMTemp6 T5 Bass
2187		32		15		SMTemp6 Temp6 High 2	SMTemp6 T6 Hi2		SMTemp6 Temp 6 Très haute		SMTemp6 T6 THau
2188		32		15		SMTemp6 Temp6 High 1	SMTemp6 T6 Hi1		SMTemp6 Temp 6 Haute			SMTemp6 T6 HauT
2189		32		15		SMTemp6 Temp6 Low	SMTemp6 T6 Low		SMTemp6 Temp 6 Basse			SMTemp6 T6 Bass
2190		32		15		SMTemp6 Temp7 High 2	SMTemp6 T7 Hi2		SMTemp6 Temp 7 Très haute		SMTemp6 T7 THau
2191		32		15		SMTemp6 Temp7 High 1	SMTemp6 T7 Hi1		SMTemp6 Temp 7 Haute			SMTemp6 T7 HauT
2192		32		15		SMTemp6 Temp7 Low	SMTemp6 T7 Low		SMTemp6 Temp 7 Basse			SMTemp6 T7 Bass
2193		32		15		SMTemp6 Temp8 High 2	SMTemp6 T8 Hi2		SMTemp6 Temp 8 Très haute		SMTemp6 T8 THau
2194		32		15		SMTemp6 Temp8 High 1	SMTemp6 T8 Hi1		SMTemp6 Temp 8 Haute			SMTemp6 T8 HauT
2195		32		15		SMTemp6 Temp8 Low	SMTemp6 T8 Low		SMTemp6 Temp 8 Basse			SMTemp6 T8 Bass
2196		32		15		SMTemp7 Temp1 High 2	SMTemp7 T1 Hi2		SMTemp7 Temp 1 Très haute		SMTemp7 T1 THau
2197		32		15		SMTemp7 Temp1 High 1	SMTemp7 T1 Hi1		SMTemp7 Temp 1 Haute			SMTemp7 T1 HauT
2198		32		15		SMTemp7 Temp1 Low	SMTemp7 T1 Low		SMTemp7 Temp 1 Basse			SMTemp7 T1 Bass
2199		32		15		SMTemp7 Temp2 High 2	SMTemp7 T2 Hi2		SMTemp7 Temp 2 Très haute		SMTemp7 T2 THau
2200		32		15		SMTemp7 Temp2 High 1	SMTemp7 T2 Hi1		SMTemp7 Temp 2 Haute			SMTemp7 T2 HauT
2201		32		15		SMTemp7 Temp2 Low	SMTemp7 T2 Low		SMTemp7 Temp 2 Basse			SMTemp7 T2 Bass
2202		32		15		SMTemp7 Temp3 High 2	SMTemp7 T3 Hi2		SMTemp7 Temp 3 Très haute		SMTemp7 T3 THau
2203		32		15		SMTemp7 Temp3 High 1	SMTemp7 T3 Hi1		SMTemp7 Temp 3 Haute			SMTemp7 T3 HauT
2204		32		15		SMTemp7 Temp3 Low	SMTemp7 T3 Low		SMTemp7 Temp 3 Basse			SMTemp7 T3 Bass
2205		32		15		SMTemp7 Temp4 High 2	SMTemp7 T4 Hi2		SMTemp7 Temp 4 Très haute		SMTemp7 T4 THau
2206		32		15		SMTemp7 Temp4 High 1	SMTemp7 T4 Hi1		SMTemp7 Temp 4 Haute			SMTemp7 T4 HauT
2207		32		15		SMTemp7 Temp4 Low	SMTemp7 T4 Low		SMTemp7 Temp 4 Basse			SMTemp7 T4 Bass
2208		32		15		SMTemp7 Temp5 High 2	SMTemp7 T5 Hi2		SMTemp7 Temp 5 Très haute		SMTemp7 T5 THau
2209		32		15		SMTemp7 Temp5 High 1	SMTemp7 T5 Hi1		SMTemp7 Temp 5 Haute			SMTemp7 T5 HauT
2210		32		15		SMTemp7 Temp5 Low	SMTemp7 T5 Low		SMTemp7 Temp 5 Basse			SMTemp7 T5 Bass
2211		32		15		SMTemp7 Temp6 High 2	SMTemp7 T6 Hi2		SMTemp7 Temp 6 Très haute		SMTemp7 T6 THau
2212		32		15		SMTemp7 Temp6 High 1	SMTemp7 T6 Hi1		SMTemp7 Temp 6 Haute			SMTemp7 T6 HauT
2213		32		15		SMTemp7 Temp6 Low	SMTemp7 T6 Low		SMTemp7 Temp 6 Basse			SMTemp7 T6 Bass
2214		32		15		SMTemp7 Temp7 High 2	SMTemp7 T7 Hi2		SMTemp7 Temp 7 Très haute		SMTemp7 T7 THau
2215		32		15		SMTemp7 Temp7 High 1	SMTemp7 T7 Hi1		SMTemp7 Temp 7 Haute			SMTemp7 T7 HauT
2216		32		15		SMTemp7 Temp7 Low	SMTemp7 T7 Low		SMTemp7 Temp 7 Basse			SMTemp7 T7 Bass
2217		32		15		SMTemp7 Temp8 High 2	SMTemp7 T8 Hi2		SMTemp7 Temp 8 Très haute		SMTemp7 T8 THau
2218		32		15		SMTemp7 Temp8 High 1	SMTemp7 T8 Hi1		SMTemp7 Temp 8 Haute			SMTemp7 T8 HauT
2219		32		15		SMTemp7 Temp8 Low	SMTemp7 T8 Low		SMTemp7 Temp 8 Basse			SMTemp7 T8 Bass
2220		32		15		SMTemp8 Temp1 High 2	SMTemp8 T1 Hi2		SMTemp8 Temp 1 Très haute		SMTemp8 T1 THau
2221		32		15		SMTemp8 Temp1 High 1	SMTemp8 T1 Hi1		SMTemp8 Temp 1 Haute			SMTemp8 T1 HauT
2222		32		15		SMTemp8 Temp1 Low	SMTemp8 T1 Low		SMTemp8 Temp 1 Basse			SMTemp8 T1 Bass
2223		32		15		SMTemp8 Temp2 High 2	SMTemp8 T2 Hi2		SMTemp8 Temp 2 Très haute		SMTemp8 T2 THau
2224		32		15		SMTemp8 Temp2 High 1	SMTemp8 T2 Hi1		SMTemp8 Temp 2 Haute			SMTemp8 T2 HauT
2225		32		15		SMTemp8 Temp2 Low	SMTemp8 T2 Low		SMTemp8 Temp 2 Basse			SMTemp8 T2 Bass
2226		32		15		SMTemp8 Temp3 High 2	SMTemp8 T3 Hi2		SMTemp8 Temp 3 Très haute		SMTemp8 T3 THau
2227		32		15		SMTemp8 Temp3 High 1	SMTemp8 T3 Hi1		SMTemp8 Temp 3 Haute			SMTemp8 T3 HauT
2228		32		15		SMTemp8 Temp3 Low	SMTemp8 T3 Low		SMTemp8 Temp 3 Basse			SMTemp8 T3 Bass
2229		32		15		SMTemp8 Temp4 High 2	SMTemp8 T4 Hi2		SMTemp8 Temp 4 Très haute		SMTemp8 T4 THau
2230		32		15		SMTemp8 Temp4 High 1	SMTemp8 T4 Hi1		SMTemp8 Temp 4 Haute			SMTemp8 T4 HauT
2231		32		15		SMTemp8 Temp4 Low	SMTemp8 T4 Low		SMTemp8 Temp 4 Basse			SMTemp8 T4 Bass
2232		32		15		SMTemp8 Temp5 High 2	SMTemp8 T5 Hi2		SMTemp8 Temp 5 Très haute		SMTemp8 T5 THau
2233		32		15		SMTemp8 Temp5 High 1	SMTemp8 T5 Hi1		SMTemp8 Temp 5 Haute			SMTemp8 T5 HauT
2234		32		15		SMTemp8 Temp5 Low	SMTemp8 T5 Low		SMTemp8 Temp 5 Basse			SMTemp8 T5 Bass
2235		32		15		SMTemp8 Temp6 High 2	SMTemp8 T6 Hi2		SMTemp8 Temp 6 Très haute		SMTemp8 T6 THau
2236		32		15		SMTemp8 Temp6 High 1	SMTemp8 T6 Hi1		SMTemp8 Temp 6 Haute			SMTemp8 T6 HauT
2237		32		15		SMTemp8 Temp6 Low	SMTemp8 T6 Low		SMTemp8 Temp 6 Basse			SMTemp8 T6 Bass
2238		32		15		SMTemp8 Temp7 High 2	SMTemp8 T7 Hi2		SMTemp8 Temp 7 Très haute		SMTemp8 T7 THau
2239		32		15		SMTemp8 Temp7 High 1	SMTemp8 T7 Hi1		SMTemp8 Temp 7 Haute			SMTemp8 T7 HauT
2240		32		15		SMTemp8 Temp7 Low	SMTemp8 T7 Low		SMTemp8 Temp 7 Basse			SMTemp8 T7 Bass
2241		32		15		SMTemp8 Temp8 High 2	SMTemp8 T8 Hi2		SMTemp8 Temp 8 Très haute		SMTemp8 T8 THau
2242		32		15		SMTemp8 Temp8 High 1	SMTemp8 T8 Hi1		SMTemp8 Temp 8 Haute			SMTemp8 T8 HauT
2243		32		15		SMTemp8 Temp8 Low	SMTemp8 T8 Low		SMTemp8 Temp 8 Basse			SMTemp8 T8 Bass

2244		32		15		None			None			Aucun					Aucun
2245		32		15		High Load Level1	HighLoadLevel1		Niveau 1 Surcharge			Niveau 1 Surch
2246		32		15		High Load Level2	HighLoadLevel2		Niveau 2 Surcharge			Niveau 2 Surch

2247		32		15		Maximum			Maximum			Maximum					Maximum
2248		32		15		Average			Average			Moyenne					Moyenne

2249		32		15		Solar Mode		Solar Mode		Mode Solaire			Mode Solaire
2250		32		15		Running Way(For Solar)	Running Way	Mode de Fonctionnement		Mode Fonct.
2251		32		15		Disabled			Disabled		Désactivé			Désactivé
2252		32		15		RECT-SOLAR		RECT-SOLAR		REDRESSEUR+SOLAIRE		RED+SOLAIRE
2253		32		15		SOLAR			SOLAR			SOLAIRE				SOLAIRE
2254		32		15		RECT First		RECT First		REDRESSEUR Prioritaire		RED.Prio.
2255		32		15		Solar First		Solar First		SOLAIRE Prioritaire		SOLAIRE Prio.
2256		32		15		Please reboot after changing			Please reboot			SVP Rebooter après Modifications		SVP Rebooter
2257		32		15		CSU Failure		CSU Failure		Défaut Controleur		Déf.Controleur	
2258		32		15		SSL and SNMPV3		SSL and SNMPV3		SSL et SNMPV3		SSL et SNMPV3	
2259		32		15		Adjust Bus Input Voltage	Adjust In-Volt		Reglage tension d'entree	Regl Tension En	
2260		32		15		Confirm Voltage Supplied	Confirm Voltage		Confirmation tension		Confirm Tension	
2261		32		15			Converter Only				Converter Only		Convertisseur seul			Convertiss seul
2262		32		15		Net-Port Reset Interval		Reset Interval		Interval initial.port reseau		IntervInitIPort
2263		32		15		DC1 Load			DC1 Load		Charge DC1	Charge DC1

2264	32		15		NCU DI1				NCU DI1				NCU ETOR1			NCU ETOR1
2265	32		15		NCU DI2				NCU DI2				NCU ETOR2			NCU ETOR2
2266	32		15		NCU DI3				NCU DI3				NCU ETOR3			NCU ETOR3
2267	32		15		NCU DI4				NCU DI4				NCU ETOR4			NCU ETOR4
2268	32			15		Digital Input 1		DI 1	Entree TOR 1		Entree TOR 1
2269	32			15		Digital Input 2		DI 2	Entree TOR 2		Entree TOR 2
2270	32			15		Digital Input 3		DI 3	Entree TOR 3		Entree TOR 3
2271	32			15		Digital Input 4		DI 4	Entree TOR 4		Entree TOR 4
2272	32			15		DI1 Alarm State		DI1 Alm State		Etat alarme ETOR1		Etat Al ETOR1
2273	32			15		DI2 Alarm State		DI2 Alm State		Etat alarme ETOR2		Etat Al ETOR2
2274	32			15		DI3 Alarm State		DI3 Alm State		Etat alarme ETOR3		Etat Al ETOR3
2275	32			15		DI4 Alarm State		DI4 Alm State		Etat alarme ETOR4		Etat Al ETOR4
2276	32			15		DI1 Alarm			DI1 Alarm			Alarme ETOR1			Alarme ETOR1
2277	32			15		DI2 Alarm			DI2 Alarm			Alarme ETOR2			Alarme ETOR2
2278	32			15		DI3 Alarm			DI3 Alarm			Alarme ETOR3			Alarme ETOR3
2279	32			15		DI4 Alarm			DI4 Alarm			Alarme ETOR4			Alarme ETOR4
2280	32			15		None			None		Aucun		Aucun
2281	32			15		Exist			Exist		Existe			Existe
2282	32			15		IB01 State			IB01 State		Etat IB01			Etat IB01
2283	32		15		DO1-Relay Output			DO1-RelayOutput		Sortie DO1 relais	Sortie DO1 Rel
2284	32		15		DO2-Relay Output			DO2-RelayOutput		Sortie DO2 relais	Sortie DO2 Rel
2285	32		15		DO3-Relay Output			DO3-RelayOutput		Sortie DO3 relais	Sortie DO3 Rel
2286	32		15		DO4-Relay Output			DO4-RelayOutput		Sortie DO4 relais	Sortie DO4 Rel
2287	32		15		Time Display Format		Time Format		Format affichage heure			FormatAffHeur  
2288	32		15		DD/MM/YYYY			DD/MM/YYYY		DD/MM/YYYY			DD/MM/YYYY
2289	32		15		MM/DD/YYYY			MM/DD/YYYY		MM/DD/YYYY			MM/DD/YYYY
2290	32		15		YYYY/MM/DD			YYYY/MM/DD		YYYY/MM/DD			YYYY/MM/DD
2291	32		15		Help				Help		Aide				Aide  
2292	32		15		Current Protocol Type		Protocol	Type de protocole valide			TypProtocValide 
2293	32		15		EEM				EEM		EEM				EEM  
2294	32		15		YDN23				YDN23		YDN23				YDN23
2295	32		15		Modbus				ModBus		ModBus				ModBus
2296	32		15		SMS Alarm Level			SMS Alarm Level		Niveau alarme SMS				Niv Al SMS
2297	32		15		Disabled			Disabled			Invalide			Invalide
2298	32		15		Observation		Observation		Observation		Observation
2299	32		15		Major			Major			Majeure		Majeure
2300	32		15		Critical		Critical		Critique		Critique 
2301	64		64		SPD is not connected to system or SPD is broken.	SPD is not connected to system or SPD is broken.	Parafoudre HS	Parafoudre HS
2302	32		15		Big Screen		Big Screen		Ecran large		Ecran large
2303	32		15		EMAIL Alarm Level			EMAIL Alm Level		Niveau alarme EMAIL				Niv Al EMAIL
2304	32		15		HLMS Protocol Type	Protocol Type		HLMS protocol		HLMS protocol
2305	32		15		EEM			EEM			EEM			EEM
2306	32		15		YDN23			YDN23			YDN23			YDN23
2307	32		15		Modbus			Modbus			Modbus			Modbus
2308		32		15		24V Display State		24V Display		Etat Affichage 24V		Etat Affich 24V
2309		32		15		Syst Volt Level				Syst Volt Level				Tension système				Tension système 
2310		32		15		48 V System				48 V System				Système 48Vdc					Système 48Vdc
2311		32		15		24 V System				24 V System					Système 24Vdc					Système 24Vdc
2312		32		15		Power Input Type			Input Type			Puissance d'entree				Puiss Entree
2313		32		15		AC Input				AC Input				Entree AC				Entree AC
2314		32		15		Diesel Input				Diesel Input				Entree GE				Entree GE
2315		32		15		2nd IB2 Temp1		2nd IB2 Temp1	Temp1 2eme IB2		Temp1 2eme IB2
2316		32		15		2nd IB2 Temp2		2nd IB2 Temp2	Temp2 2eme IB2		Temp2 2eme IB2
2317		32		15		EIB2 Temp1		EIB2 Temp1	Temp1 EIB2		Temp1 EIB2
2318		32		15		EIB2 Temp2		EIB2 Temp2	Temp2 EIB2		Temp2 EIB2
2319		32		15		2nd IB2 Temp1 High 2	2nd IB2 T1 Hi2	2eme IB2 Seuil haut 2 Temp 1	2eme IB2 Ht2 T1
2320		32		15		2nd IB2 Temp1 High 1	2nd IB2 T1 Hi1	2eme IB2 Seuil haut 1 Temp 1	2eme IB2 Ht1 T1
2321		32		15		2nd IB2 Temp1 Low	2nd IB2 T1 Low	2eme IB2 Seuil bas Temp 1		2eme IB2 Bas T1
2322		32		15		2nd IB2 Temp2 High 2	2nd IB2 T2 Hi2	2eme IB2 Seuil haut 2 Temp 2	2eme IB2 Ht2 T2
2323		32		15		2nd IB2 Temp2 High 1	2nd IB2 T2 Hi1	2eme IB2 Seuil haut 1 Temp 2	2eme IB2 Ht1 T2
2324		32		15		2nd IB2 Temp2 Low	2nd IB2 T2 Low	2eme IB2 Seuil bas Temp 2		2eme IB2 Bas T2
2325		32		15		EIB2 Temp1 High 2	EIB2 T1 Hi2	EIB2 Seuil haut 2 Temp 1		EIB2 Haut2 T1
2326		32		15		EIB2 Temp1 High 1	EIB2 T1 Hi1	EIB2 Seuil haut 1 Temp 1		EIB2 Haut1 T1
2327		32		15		EIB2 Temp1 Low		EIB2 T1 Low	EIB2 Seuil bas Temp 1	EIB2 Bas Temp1
2328		32		15		EIB2 Temp2 High 2	EIB2 T2 Hi2	EIB2 Seuil haut 2 Temp 2		EIB2 Haut2 T2
2329		32		15		EIB2 Temp2 High 1	EIB2 T2 Hi1	EIB2 Seuil haut 1 Temp 2		EIB2 Haut1 T2
2330		32		15		EIB2 Temp2 Low		EIB2 T2 Low	EIB2 Seuil bas Temp 2	EIB2 Bas Temp2
2331	32		15		NCU DO1 Test			NCU DO1 Test		Test DO1		Test DO1
2332	32		15		NCU DO2 Test			NCU DO2 Test		Test DO2		Test DO2
2333	32		15		NCU DO3 Test			NCU DO3 Test		Test DO3		Test DO3
2334	32		15		NCU DO4 Test			NCU DO4 Test		Test DO4		Test DO4
2335	32		15		Total Output Rated			Total Output		% charge totale	% charge totale 
2336	32		15		Load current capacity			Load capacity		Taux de charge	Taux de charge
2337	32		15		EES System Mode			EES System Mode		Système en mode EES	Syst en modeEES
2338	32		15		SMS Modem Fail			SMS Modem Fail		Defaut Modem SMS	Def Modem SMS
2339		32		15		SMDU-EIB Mode		SMDU-EIB Mode		Mode SMDU-EIB	Mode SMDU-EIB
2340		32		15		Load Switch				Load Switch				Commutateur de charge	CommutDe charge
2341		32		15		Manual State			Manual State			Mode Manuel	Mode Manuel
2342		32		15		Converter Voltage Level		Volt Level			Niv Tension Convertisseur		Niv Tension
2343		32		15		CB Threshold  Value			Threshold Value		Val seuil disj				Val seuil 
2344		32		15		CB Overload  Value			Overload Value			Val seuil disj				Val surcharge
2345		32		15		SNMP Config Error	SNMP Config Err		SNMP Config Err	SNMP Config Err
2346		32		15		Fiamm Battery		Fiamm Battery		Batteries FIAM	Batteries FIAM
2347		32		15		TL1			TL1			TL1		TL1
2348		32		15		TL1 Port Activation		Port Activation		TL1 Activation port		Activation port	
2349		32		15		TL1 Protocol Media	Protocol Media		TL1 Protocole Media		Protocol Media
2350		32		15		TL1 Access Port		Access Port		TL1 Access Port		Access Port
2351		32		15		TL1 Port Keep Alive	Keep Alive		Port TL1 en veille	En veille
2352		32		15		TL1 Session Timeout	Session Timeout		Session Timeout	Session Timeout
2353		32		15		IPV4			IPV4			IPV4		IPV4
2354		32		15		IPV6			IPV6			IPV6		IPV6
2355		32		15		Ambient Temp Summary Alarm	AmbTempSummAlm		Alarme synthese temp ambiante	AmbTempSummAlm
2356		32		15		DHCP Enable		DHCP Enable		DHCP Activé		DHCP Activé

2357		32		15		Modbus Config Error	ModbusConfigErr		Erreur de configuration Modbus		Erreu de Modbus
2358		32		15		TL1 AID Delimeter	AID Delimeter		Délimètre AID TL1	Délimètre AID

2400	32		15		Not Active			Not Active		Arrêt				Arrêt
2401	32		15		Active				Active			Marche				Marche
2402		32		15		IB4 Communication Status		IB4 Comm Status		IB4 Etat Communication		IB4 Etat Com	
2403		32		15		IB4 Communication Fail			IB4 Comm Fail		IB4 Communication Défaut	IB4 Comm Défaut	
2406	32		15		IB2-2 Temp1		IB2-2 T1		IB2-2Temp1		IB2-2T1
2407	32		15		IB2-2 Temp2		IB2-2 T2		IB2-2Temp2		IB2-2T2
2408	32		15		EIB-2 Temp1		EIB-2 T1		EIB-2Temp1		EIB-2T1
2409	32		15		EIB-2 Temp2		EIB-2 T2		EIB-2Temp2		EIB-2T2

2410	32		15		IB2-2 Temp1 High 2	IB2-2 T1 Hi2	IB2-2 Seuil haut 2 Temp 1		IB2-2 Haut2 T1
2411	32		15		IB2-2 Temp1 High 1	IB2-2 T1 Hi1	IB2-2 Seuil haut 1 Temp 1		IB2-2 Haut1 T1
2412	32		15		IB2-2 Temp1 Low		IB2-2 T1 Low	IB2-2 Seuil bas Temp 1			IB2-2 Bas T1
2413	32		15		IB2-2 Temp2 High 2	IB2-2 T2 Hi2	IB2-2 Seuil haut 2 Temp 2		IB2-2 Haut2 T2
2414	32		15		IB2-2 Temp2 High 1	IB2-2 T2 Hi1	IB2-2 Seuil haut 1 Temp 2		IB2-2 Haut1 T2
2415	32		15		IB2-2 Temp2 Low		IB2-2 T2 Low	IB2-2 Seuil bas Temp 2			IB2-2 Bas T2
2416	32		15		EIB-2 Temp1 High 2	EIB-2 T1 Hi2	EIB-2 Seuil haut 2 Temp 1		EIB2-2 Haut2 T1
2417	32		15		EIB-2 Temp1 High 1	EIB-2 T1 Hi1	EIB-2 Seuil haut 1 Temp 1		EIB2-2 Haut1 T1
2418	32		15		EIB-2 Temp1 Low		EIB-2 T1 Low	EIB-2 Seuil bas Temp 1			EIB2-2 Bas T1
2419	32		15		EIB-2 Temp2 High 2	EIB-2 T2 Hi2	EIB-2 Seuil haut 2 Temp 2		EIB2-2 Haut2 T2
2420	32		15		EIB-2 Temp2 High 1	EIB-2 T2 Hi1	EIB-2 Seuil haut 1 Temp 2		EIB2-2 Haut1 T2
2421	32		15		EIB-2 Temp2 Low		EIB-2 T2 Low	EIB-2 Seuil bas Temp 2			EIB2-2 Bas T2

2422	32		15		IB2-2 Temp 1			IB2-2 Temp1		IB2-2 Température 1		IB2-2 Temp1
2423	32		15		IB2-2 Temp 1			IB2-2 Temp1		Activation IB2-2 Température 1	Act IB2-2 Temp1
2427	32		15		IB2-2 Temp 1 Not Used		IB2-2T1NotUsed		Tempér.1 IB2-2 non utilisée		T1IB2-2NonUtil
2428	32		15		IB2-2 Temp 1 Sensor Fail	IB2-2T1SensFail		Défaut capteur IB2-2 Temp1		DéfCaptIB2-2T1
2432	32		15		IB2-2 Temp 2			IB2-2 Temp2		IB2-2 Température 2		IB2-2 Temp2
2433	32		15		IB2-2 Temp 2			IB2-2 Temp2		Activation IB2-2 Température 2	Act IB2-2 Temp2
2437	32		15		IB2-2 Temp 2 Not Used		IB2-2T2NotUsed		Tempér.2 IB2-2 non utilisée		T2IB2-2NonUtil
2438	32		15		IB2-2 Temp 2 Sensor Fail	IB2-2T2SensFail		Défaut capteur IB2-2 Temp2		DéfCaptIB2-2T2
2442	32		15		EIB-2 Temp 1			EIB-2 Temp1		EIB-2 Température 1		EIB-2 Temp1
2443	32		15		EIB-2 Temp 1			EIB-2 Temp1		Activation EIB-2 Température 1	Act EIB-2 Temp1
2447	32		15		EIB-2 Temp 1 Not Used		EIB-2T1NotUsed		Tempér.1 EIB-2 non utilisée		T1EIB-2NonUtil
2448	32		15		EIB-2 Temp 1 Sensor Fail	EIB-2T1SensFail		Défaut capteur EIB-2 Temp1		DéfCaptEIB-2T1
2452	32		15		EIB-2 Temp 2			EIB-2 Temp2		EIB-2 Température 2		EIB-2 Temp2
2453	32		15		EIB-2 Temp 2			EIB-2 Temp2		Activation EIB-2 Température 2	Act EIB-2 Temp2
2457	32		15		EIB-2 Temp 2 Not Used		EIB-2T2NotUsed		Tempér.2 EIB-2 non utilisée		T2EIB-2NonUtil
2458	32		15		EIB-2 Temp 2 Sensor Fail	EIB-2T2SensFail		Défaut capteur EIB-2 Temp2		DéfCaptEIB-2T2

2480	32		15		Temperature Format		Temp Format		Format Température		FormatTemp
2481	32		15		Celsius				Celsius			Celsius				Celsius
2482	32		15		Fahrenheit			Fahrenheit		Fahrenheit			Fahrenheit
2483	32		15		System Alarm Function		Sys Alarm Func		System Fonction d'alarme		Sys Fonc Alm
2484	32		15		CR Only				CR Only			seulement CR			seulement CR
2485	32		15		CR and MJ			CR and MJ		CR et MJ			CR et MJ
2486	32		15		CR, MJ, and OB			CR, MJ, and OB		CR,MJ et OB			CR,MJ et OB
2487	32		15		Verizon Display			Verizon Display		Verizon Afficher			VerizonAfficher	
2488	32		15		Normal Mode			Normal Mode		Mode normal			Mode normal	
2489	32		15		Verizon Mode			Verizon Mode		Mode Verizon			Mode Verizon		
	
2500	32		15		Source Current		Source Current		Source actuelle			Source Actu
2501	32		15		Source Current Number	SourceCurrNum		Source Numéro actuel		SourceNumActuel
2502	32		15		Source Rated Current	SourceRatedCurr		Courant nominal de la source	CouNomSource
2503	32		15		NTP Function Enable	NTPFuncEnable		Fonction NTP Activée		FoncNTPActivée

2510	32		15		Alarm Sound Activation	Alm Sound Act	告警音激活		告警音激活
2511	32		15		Close Alarm Sound	Close Alm Sound		关告警音		关告警音
2512	32		15		Close				Close		关闭			关闭
2513	32		15		Open				Open		开启			开启
2521	32		15		SMDUE1 Temp1		SMDUE1 T1	SMDUE1 Temp1		SMDUE1 Temp1
2522	32		15		SMDUE1 Temp2		SMDUE1 T2	SMDUE1 Temp2		SMDUE1 Temp2
2523	32		15		SMDUE1 Temp3		SMDUE1 T3	SMDUE1 Temp3		SMDUE1 Temp3
2524	32		15		SMDUE1 Temp4		SMDUE1 T4	SMDUE1 Temp4		SMDUE1 Temp4
2525	32		15		SMDUE1 Temp5		SMDUE1 T5	SMDUE1 Temp5		SMDUE1 Temp5
2526	32		15		SMDUE1 Temp6		SMDUE1 T6	SMDUE1 Temp6		SMDUE1 Temp6
2527	32		15		SMDUE1 Temp7		SMDUE1 T7	SMDUE1 Temp7		SMDUE1 Temp7
2528	32		15		SMDUE1 Temp8		SMDUE1 T8	SMDUE1 Temp8		SMDUE1 Temp8
2527	32		15		SMDUE1 Temp7		SMDUE1 T7	SMDUE1 Temp7		SMDUE1 Temp7
2528	32		15		SMDUE1 Temp8		SMDUE1 T8	SMDUE1 Temp8		SMDUE1 Temp8
2529	32		15		SMDUE1 Temp9		SMDUE1 T7	SMDUE1 Temp9		SMDUE1 Temp9
2530	32		15		SMDUE1 Temp10		SMDUE1 T8	SMDUE1 Temp10		SMDUE1 Temp10
2531	32		15		SMDUE2 Temp1		SMDUE2 T1	SMDUE2 Temp1		SMDUE2 Temp1
2532	32		15		SMDUE2 Temp2		SMDUE2 T2	SMDUE2 Temp2		SMDUE2 Temp2
2533	32		15		SMDUE2 Temp3		SMDUE2 T3	SMDUE2 Temp3		SMDUE2 Temp3
2534	32		15		SMDUE2 Temp4		SMDUE2 T4	SMDUE2 Temp4		SMDUE2 Temp4
2535	32		15		SMDUE2 Temp5		SMDUE2 T5	SMDUE2 Temp5		SMDUE2 Temp5
2536	32		15		SMDUE2 Temp6		SMDUE2 T6	SMDUE2 Temp6		SMDUE2 Temp6
2537	32		15		SMDUE2 Temp7		SMDUE2 T7	SMDUE2 Temp7		SMDUE2 Temp7
2538	32		15		SMDUE2 Temp8		SMDUE2 T8	SMDUE2 Temp8		SMDUE2 Temp8
2537	32		15		SMDUE2 Temp7		SMDUE2 T7	SMDUE2 Temp7		SMDUE2 Temp7
2538	32		15		SMDUE2 Temp8		SMDUE2 T8	SMDUE2 Temp8		SMDUE2 Temp8
2539	32		15		SMDUE2 Temp9		SMDUE2 T7	SMDUE2 Temp9		SMDUE2 Temp9
2540	32		15		SMDUE2 Temp10		SMDUE2 T8	SMDUE2 Temp10		SMDUE2 Temp10
2541		32		15		SMDUE1 Temp1 High 2	SMDUE1 T1 Hi2		SMDUE1 Temp 1 Très haute		SMDUE1 T1 THau
2542		32		15		SMDUE1 Temp1 High 1	SMDUE1 T1 Hi1		SMDUE1 Temp 1 Haute			SMDUE1 T1 HauT
2543		32		15		SMDUE1 Temp1 Low		SMDUE1 T1 Low		SMDUE1 Temp 1 Basse			SMDUE1 T1 Bass
2544		32		15		SMDUE1 Temp2 High 2	SMDUE1 T2 Hi2		SMDUE1 Temp 2 Très haute		SMDUE1 T2 THau
2545		32		15		SMDUE1 Temp2 High 1	SMDUE1 T2 Hi1		SMDUE1 Temp 2 Haute			SMDUE1 T2 HauT
2546		32		15		SMDUE1 Temp2 Low		SMDUE1 T2 Low		SMDUE1 Temp 2 Basse			SMDUE1 T2 Bass
2547		32		15		SMDUE1 Temp3 High 2	SMDUE1 T3 Hi2		SMDUE1 Temp 3 Très haute		SMDUE1 T3 THau
2548		32		15		SMDUE1 Temp3 High 1	SMDUE1 T3 Hi1		SMDUE1 Temp 3 Haute			SMDUE1 T3 HauT
2549		32		15		SMDUE1 Temp3 Low		SMDUE1 T3 Low		SMDUE1 Temp 3 Basse			SMDUE1 T3 Bass
2550		32		15		SMDUE1 Temp4 High 2	SMDUE1 T4 Hi2		SMDUE1 Temp 4 Très haute		SMDUE1 T4 THau
2551		32		15		SMDUE1 Temp4 High 1	SMDUE1 T4 Hi1		SMDUE1 Temp 4 Haute			SMDUE1 T4 HauT
2552		32		15		SMDUE1 Temp4 Low		SMDUE1 T4 Low		SMDUE1 Temp 4 Basse			SMDUE1 T4 Bass
2553		32		15		SMDUE1 Temp5 High 2	SMDUE1 T5 Hi2		SMDUE1 Temp 5 Très haute		SMDUE1 T5 THau
2554		32		15		SMDUE1 Temp5 High 1	SMDUE1 T5 Hi1		SMDUE1 Temp 5 Haute			SMDUE1 T5 HauT
2555		32		15		SMDUE1 Temp5 Low		SMDUE1 T5 Low		SMDUE1 Temp 5 Basse			SMDUE1 T5 Bass
2556		32		15		SMDUE1 Temp6 High 2	SMDUE1 T6 Hi2		SMDUE1 Temp 6 Très haute		SMDUE1 T6 THau
2557		32		15		SMDUE1 Temp6 High 1	SMDUE1 T6 Hi1		SMDUE1 Temp 6 Haute			SMDUE1 T6 HauT
2558		32		15		SMDUE1 Temp6 Low		SMDUE1 T6 Low		SMDUE1 Temp 6 Basse			SMDUE1 T6 Bass
2559		32		15		SMDUE1 Temp7 High 2	SMDUE1 T7 Hi2		SMDUE1 Temp 7 Très haute		SMDUE1 T7 THau
2560		32		15		SMDUE1 Temp7 High 1	SMDUE1 T7 Hi1		SMDUE1 Temp 7 Haute			SMDUE1 T7 HauT
2561		32		15		SMDUE1 Temp7 Low		SMDUE1 T7 Low		SMDUE1 Temp 7 Basse			SMDUE1 T7 Bass
2562		32		15		SMDUE1 Temp8 High 2	SMDUE1 T8 Hi2		SMDUE1 Temp 8 Très haute		SMDUE1 T8 THau
2563		32		15		SMDUE1 Temp8 High 1	SMDUE1 T8 Hi1		SMDUE1 Temp 8 Haute			SMDUE1 T8 HauT
2564		32		15		SMDUE1 Temp8 Low		SMDUE1 T8 Low		SMDUE1 Temp 8 Basse			SMDUE1 T8 Bass
2565		32		15		SMDUE1 Temp9 High 2	SMDUE1 T9 Hi2		SMDUE1 Temp 9 Très haute		SMDUE1 T9 THau
2566		32		15		SMDUE1 Temp9 High 1	SMDUE1 T9 Hi1		SMDUE1 Temp 9 Haute			SMDUE1 T9 HauT
2567		32		15		SMDUE1 Temp9 Low		SMDUE1 T9 Low		SMDUE1 Temp 9 Basse			SMDUE1 T9 Bass
2568		32		15		SMDUE1 Temp10 High 2	SMDUE1 T10 Hi2		SMDUE1 Temp 10 Très haute		SMDUE1 T10 THau
2569		32		15		SMDUE1 Temp10 High 1	SMDUE1 T10 Hi1		SMDUE1 Temp 10 Haute			SMDUE1 T10 HauT
2570		32		15		SMDUE1 Temp10 Low	SMDUE1 T10 Low		SMDUE1 Temp 10 Basse			SMDUE1 T10 Bass
2571		32		15		SMDUE2 Temp1 High 2	SMDUE2 T1 Hi2		SMDUE2 Temp 1 Très haute		SMDUE2 T1 THau
2572		32		15		SMDUE2 Temp1 High 1	SMDUE2 T1 Hi1		SMDUE2 Temp 1 Haute			SMDUE2 T1 HauT
2573		32		15		SMDUE2 Temp1 Low		SMDUE2 T1 Low		SMDUE2 Temp 1 Basse			SMDUE2 T1 Bass
2574		32		15		SMDUE2 Temp2 High 2	SMDUE2 T2 Hi2		SMDUE2 Temp 2 Très haute		SMDUE2 T2 THau
2575		32		15		SMDUE2 Temp2 High 1	SMDUE2 T2 Hi1		SMDUE2 Temp 2 Haute			SMDUE2 T2 HauT
2576		32		15		SMDUE2 Temp2 Low		SMDUE2 T2 Low		SMDUE2 Temp 2 Basse			SMDUE2 T2 Bass
2577		32		15		SMDUE2 Temp3 High 2	SMDUE2 T3 Hi2		SMDUE2 Temp 3 Très haute		SMDUE2 T3 THau
2578		32		15		SMDUE2 Temp3 High 1	SMDUE2 T3 Hi1		SMDUE2 Temp 3 Haute			SMDUE2 T3 HauT
2579		32		15		SMDUE2 Temp3 Low		SMDUE2 T3 Low		SMDUE2 Temp 3 Basse			SMDUE2 T3 Bass
2580		32		15		SMDUE2 Temp4 High 2	SMDUE2 T4 Hi2		SMDUE2 Temp 4 Très haute		SMDUE2 T4 THau
2581		32		15		SMDUE2 Temp4 High 1	SMDUE2 T4 Hi1		SMDUE2 Temp 4 Haute			SMDUE2 T4 HauT
2582		32		15		SMDUE2 Temp4 Low		SMDUE2 T4 Low		SMDUE2 Temp 4 Basse			SMDUE2 T4 Bass
2583		32		15		SMDUE2 Temp5 High 2	SMDUE2 T5 Hi2		SMDUE2 Temp 5 Très haute		SMDUE2 T5 THau
2584		32		15		SMDUE2 Temp5 High 1	SMDUE2 T5 Hi1		SMDUE2 Temp 5 Haute			SMDUE2 T5 HauT
2585		32		15		SMDUE2 Temp5 Low		SMDUE2 T5 Low		SMDUE2 Temp 5 Basse			SMDUE2 T5 Bass
2586		32		15		SMDUE2 Temp6 High 2	SMDUE2 T6 Hi2		SMDUE2 Temp 6 Très haute		SMDUE2 T6 THau
2587		32		15		SMDUE2 Temp6 High 1	SMDUE2 T6 Hi1		SMDUE2 Temp 6 Haute			SMDUE2 T6 HauT
2588		32		15		SMDUE2 Temp6 Low		SMDUE2 T6 Low		SMDUE2 Temp 6 Basse			SMDUE2 T6 Bass
2589		32		15		SMDUE2 Temp7 High 2	SMDUE2 T7 Hi2		SMDUE2 Temp 7 Très haute		SMDUE2 T7 THau
2590		32		15		SMDUE2 Temp7 High 1	SMDUE2 T7 Hi1		SMDUE2 Temp 7 Haute			SMDUE2 T7 HauT
2591		32		15		SMDUE2 Temp7 Low		SMDUE2 T7 Low		SMDUE2 Temp 7 Basse			SMDUE2 T7 Bass
2592		32		15		SMDUE2 Temp8 High 2	SMDUE2 T8 Hi2		SMDUE2 Temp 8 Très haute		SMDUE2 T8 THau
2593		32		15		SMDUE2 Temp8 High 1	SMDUE2 T8 Hi1		SMDUE2 Temp 8 Haute			SMDUE2 T8 HauT
2594		32		15		SMDUE2 Temp8 Low		SMDUE2 T8 Low		SMDUE2 Temp 8 Basse			SMDUE2 T8 Bass
2595		32		15		SMDUE2 Temp9 High 2	SMDUE2 T9 Hi2		SMDUE2 Temp 9 Très haute		SMDUE2 T9 THau
2596		32		15		SMDUE2 Temp9 High 1	SMDUE2 T9 Hi1		SMDUE2 Temp 9 Haute			SMDUE2 T9 HauT
2597		32		15		SMDUE2 Temp9 Low		SMDUE2 T9 Low		SMDUE2 Temp 9 Basse			SMDUE2 T9 Bass
2598		32		15		SMDUE2 Temp10 High 2	SMDUE2 T10 Hi2		SMDUE2 Temp 10 Très haute		SMDUE2 T10 THau
2599		32		15		SMDUE2 Temp10 High 1	SMDUE2 T10 Hi1		SMDUE2 Temp 10 Haute			SMDUE2 T10 HauT
2600		32		15		SMDUE2 Temp10 Low	SMDUE2 T10 Low		SMDUE2 Temp 10 Basse			SMDUE2 T10 Bass

2900		32		15		Virtual SMDUP Control	Vir SMDUP Ctl		Contrôle virtuel SMDUP		Ctl vir SMDUP
2901		32		15		Yes						Yes					Oui					Oui

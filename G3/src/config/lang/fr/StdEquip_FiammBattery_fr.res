﻿#
#  Locale language support:fr
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15			Battery Current				Batt Current		Courant Batterie		Courant Batt			
2	32			15			Battery Rating (Ah)		Batt Rating(Ah)		Batterie Indice (Ah)		Batt Indice(Ah)			
3	32			15			Bus Voltage				Bus Voltage		Tension Bus		Tension Bus		
5	32			15			Battery Over Current			Batt Over Curr		Batterie Over Current		Batt Over Curr		
6	32			15			Battery Capacity (%)			Batt Cap (%)		Capacité Batterie(%)		Cap Batt(%)		
7	32			15			Battery Voltage				Batt Voltage		Batterie Tension		Batt Tension		
18	32			15			SoNick Battery				SoNick Batt		SoNick Batterie		SoNick Batt		
29	32			15			Yes					Yes			Oui			Oui
30	32			15			No					No			Non.			Non.
31	32			15			On					On			Dix			Dix
32	32			15			Off					Off			De			De
33	32			15			State					State			Etat			Etat
87	32			15			No					No			Non.			Non.	
96	32			15			Rated Capacity				Rated Capacity		Capacité Nominale	Cap Nominale	
99	32			15			Battery Temperature(AVE)		Batt Temp(AVE)		Température Batterie	Temp Batt
100	32			15			Board Temperature			Board Temp		Conseil Température	Conseil Temp
101	32			15			Tc Center Temperature			Tc Center Temp		Tc Centre Température		Tc Centre Temp
102	32			15			Tc Left Temperature			Tc Left Temp		Tc Température gauche		Tc Temp Gauche
103	32			15			Tc Right Temperature			Tc Right Temp		Tc Droite Température		Tc Droite Temp
114	32			15			Battery Communication Fail		Batt Comm Fail		Batterie Communication Fail	Batt Comm Fail
129	32			15			Low Ambient Temperature			Low Amb Temp		Basse température ambiante		Basse Temp Amb
130	32			15			High Ambient Temperature Warning	High Amb Temp W		Haute Temp ambiante Avertis.		Haute Temp Amb	
131	32			15			High Ambient Temperature		High Amb Temp		Haute Température ambiante		Haute Temp Amb	
132	32			15			Low Battery Internal Temperature	Low Int Temp		Basse Temp Batterie interne		BasseTempBatInt
133	32			15			High Batt Internal Temp Warning		Hi Int Temp W		Haute temp Avertis Batt interne		HauteTempBatInt	
134	32			15			High Batt Internal Temperature		Hi Bat Int Temp		Haute temp Avertis Batt interne		HauteTempBatInt	
135	32			15			Bus Voltage Below 40V			Bus V Below 40V		V bus Voici 40V		V Bus Voici 40V	
136	32			15			Bus Voltage Below 39V			Bus V Below 39V		V bus Voici 39V		V Bus Voici 39V	
137	32			15			Bus Voltage Above 60V			Bus V Above 60V		V Bus Dessus60V		V Bus Dessus60V	
138	32			15			Bus Voltage Above 65V			Bus V Above 65V		V Bus Dessus65V		V Bus Dessus65V	
139	32			15			High Discharge Current Warning		Hi Disch Curr W		Haute décharge Avertis Courant		Haute Déch Cour
140	32			15			High Discharge Current			High Disch Curr		Haute décharge Courant		Haute Déch Cour
141	32			15			Main Switch Error			Main Switch Err		Interrupteur principal Erreur		InterPrinc Err
142	32			15			Fuse Blown				Fuse Blown		Blown Fuse			Blown Fuse
143	32			15			Heaters Failure				Heaters Failure		Heaters Échec		Heaters Échec
144	32			15			Thermocouple Failure			Thermocple Fail		Thermocouple Échec		ThermocpleÉchec
145	32			15			Voltage Measurement Circuit Fail	V M Cir Fail		Mesure V Circuit Échec		M V Cir Échec
146	32			15			Current Measurement Circuit Fail	C M Cir Fail		Mesure C Circuit Échec		M C Cir Échec
147	32			15			BMS Hardware Failure			BMS Hdw Failure		BMS Échec Hardware			BMS Échec
148	32			15			Hardware Protection Sys Active		HdW Protect Sys		Protection Hardwarel Sys actif		HdW Protect Sys
149	32			15			High Heatsink Temperature		Hi Heatsink Tmp		Haute Température Dissipateur		HauteTempDissip
150	32			15			Battery Voltage Below 39V		Bat V Below 39V		V Batt Voici39V		V Batt Voici39V	
151	32			15			Battery Voltage Below 38V		Bat V Below 38V		V Batt Voici38V		V Batt Voici38V	
152	32			15			Battery Voltage Above 53.5V		Bat V Abv 53.5V		V Batt Dessus53.5V	VBatDessus53.5V
153	32			15			Battery Voltage Above 53.6V		Bat V Abv 53.6V		V Batt Dessus53.6V	VBatDessus53.6V
154	32			15			High Charge Current Warning		Hi Chrge Curr W		Avertis Haut Courant Charge		HautCourCharge
155	32			15			High Charge Current			Hi Charge Curr		Haut Courant Charge		HautCourCharge
156	32			15			High Discharge Current Warning		Hi Disch Curr W		Avertis Haut Courant Décharge		HautCourDéch
157	32			15			High Discharge Current			Hi Disch Curr		Haut Courant Décharge		HautCourDéch
158	32			15			Voltage Unbalance Warning		V unbalance W		Déséquilibre V Avertiss		Déséquilibre V
159	32			15			Voltages Unbalance			Volt Unbalance		Déséquilibre V Avertiss		Déséquilibre V
160	32			15			Dc Bus Pwr Too Low for Charging		DC Low for Chrg		DC Bus pour charge trop bas		DC Charge Bas
161	32			15			Charge Regulation Failure		Charge Reg Fail		Règlement Charge échec		RègCharge Échec
162	32			15			Capacity Below 12.5%			Cap Below 12.5%		Voici Capacité 12.5%		Voici Cap12.5%
163	32			15			Thermocouples Mismatch			Tcples Mismatch		Thermocouples Mismatch			Tcples Mismatch
164	32			15			Heater Fuse Blown			Heater FA		Heater Fuse Blown			Heater FA

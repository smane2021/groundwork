﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			LVD Unit			LVD Unit		Unitée Contacteur			Unitée LVD
2		32			15			LargeDU LVD			LargeDU LVD		LargDU LVD				LargDU LVD
11		32			15			Connected			Connected		Connecté				Connecté
12		32			15			Disconnected			Disconnected		Deconnecté				Deconnecté
13		32			15			No				No			Non					Non
14		32			15			Yes				Yes			Oui					Oui
21		32			15			LVD1 Status			LVD1 Status		Etat LVD 1				Etat LVD 1
22		32			15			LVD2 Status			LVD2 Status		Etat LVD 2				Etat LVD 2
23		32			15			LVD1 Disconnected		LVD1 Disconnect			Defaut LVD 1				Defaut LVD 1
24		32			15			LVD2 Disconnected		LVD2 Disconnect			Defaut LVD 2				Defaut LVD 2
25		32			15			Communication Failure		Comm Failure		Defaut communication			Defaut Comm.
26		32			15			State				State			Etat					Etat
27		32			15			LVD1 Control			LVD1 Control		Control LVD 1				Control LVD 1
28		32			15			LVD2 Control			LVD2 Control		Control LVD 2				Control LVD 2
31		32			15			LVD1			LVD1	Activation Contacteur 1			Activation LVD1
32		32			15			LVD1 Mode			LVD1 Mode		Mode Commande Contacteur 1		Mode Cmd LVD 1
33		32			15			LVD1 Voltage			LVD1 Voltage		Tension Deconnexion Contacteur 1	Tens Decon LVD1
34		32			15			LVD1 Reconnect Voltage		LVD1 Recon Volt		Tension Reconnexion Contacteur 1	Tens Recon LVD1
35		32			15			LVD1 Reconnect Delay		LVD1 ReconDelay		Retard Reconnexion Contacteur 1		RetardRecnLVD1
36		32			15			LVD1 Time			LVD1 Time		CMD Contacteur 1 Sur Duree		CMD LVD1 Duree
37		32			15			LVD1 Dependency			LVD1 Dependency		Dependance Contacteur 1			Depend. LVD1
41		32			15			LVD2			LVD2		Activation Contacteur 2			Activation LVD2
42		32			15			LVD2 Mode			LVD2 Mode		Mode Commande Contacteur 2		Mode Cmd LVD2
43		32			15			LVD2 Voltage			LVD2 Voltage		Tension Deconnexion Contacteur 2	Tens Decon LVD2
44		32			15			LVD2 Reconnect Voltage		LVD2 Recon Volt		Tension Reconnexion Contacteur 2	Tens Recon LVD2
45		32			15			LVD2 Reconnect Delay		LVD2 ReconDelay		Retard Reconnexion Contacteur 2		RetardRecnLVD2
46		32			15			LVD2 Time			LVD2 Time		CMD Contacteur 2 Sur Duree		CMD LVD2 Duree
47		32			15			LVD2 Dependency			LVD2 Dependency		Dependance Contacteur 2			Depend. LVD2
51		32			15			Disabled			Disabled		Desactiver				Desactiver
52		32			15			Enabled				Enabled			Activer					Activer
53		32			15			Voltage				Voltage		Tension					Tension	
54		32			15			Time				Time			Duree					Duree
55		32			15			None				None			Aucun					Aucun
56		32			15			LVD1				LVD1			LVD1					LVD1
57		32			15			LVD2				LVD2			LVD2					LVD2
103		32			15			High Temp Disconnect 1	HTD1		Activation HTD1				Activation HTD1
104		32			15		High Temp Disconnect 2	HTD2		Activation HTD 2			Activation HTD2
105		32			15			Battery LVD			Battery LVD		Contacteur Batterie			LVD Batterie
110		32			15			Communication Fail		Comm Fail		Defaut de communication		Defaut de COM
111		32			15			Times of Communication Fail	Times Comm Fail			Duree du defaut de communication			DureeDefCOM
116		32			15			LVD1 Contactor Failure		LVD1 Failure		Défaut Contacteur LVD1			Déf.Cont.LVD1
117		32			15			LVD2 Contactor Failure		LVD2 Failure		Défaut Contacteur LVD2			Déf.Cont.LVD2
118		32			15			DC Distribution Number		DC Distr Num		Numéro DCD				Numéro DCD

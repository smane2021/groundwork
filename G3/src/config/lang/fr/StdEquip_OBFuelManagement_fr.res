﻿#
#  Locale language support:french
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE	
1		32			16			OBFuel Tank				OBFuel Tank		OB Reservoir gasoil		OB ReservoirGas
2		32			15			Remaining Fuel Height			Remained Height		Niveau restant de gasoil		NiveauGasoil
3		32			15			Remaining Fuel Volume			Remained Volume		Volume restant de gasoil		VolumeGasoil
4		32			15			Remaining Fuel Percent			RemainedPercent		Pourcentage de fuel restant		%Gasoil restant
5		32			15			Fuel Theft Alarm			Fuel Theft Alm		Alarme vol gasoil		Al Vol Gasoil
6		32			15			No					No			Non			Non
7		32			15			Yes					Yes			Oui		Oui
8		32			15			Multi-Shape Height Error Status		Multi Hgt Err		Err.Multi-Pics consommation		Err_MulPicConso
9		32			15			Setting Configuration Done		Set Config Done		Configuration effectuee		Config Faite
10		32			15			Reset Theft Alarm			Reset Theft Alm		RAZ alarme Vol		RAZ Alarm Vol
11		32			15			Fuel Tank Type				Fuel Tank Type		Type de reservoir gasoil		TypReserGasoil
12		32			15			Square Tank Length			Square Tank L		Longueur reservoir rectangulaire		LongReserRectan
13		32			15			Square Tank Width			Square Tank W		Largeur reservoir rectangulaire		LargReserRectan
14		32			15			Square Tank Height			Square Tank H		Hauteur reservoir rectangulaire		HautReserRectan
15		32			15			Vertical Cylinder Tank Diameter	Vertical Tank D		Diam.reserv.cylind.vertical		DiamResrvCylind
16		32			15			Vertical Cylinder Tank Height		Vertical Tank H		Haut.reserv.cylind.vertical		HautResrvCylind
17		32			15			Horiz Cylinder Tank Diameter		Horiz Tank D		Diam.reserv.cylind.horizontal	DiamResrvCylind
18		32			15			Horiz Cylinder Tank Length		Horiz Tank L		Haut.reserv.cylind.horizontal		HautResrvCylind
#for multi sharp 20 calibration points
20		32			15			Number of Calibration Points		Num Cal Points		Nombre points calibration		NbPtsCalibratio
21		32			15			Height 1 of Calibration Point		Height 1 Point		Hauteur 1 point calibration		Point Haut 1
22		32			15			Volume 1 of Calibration Point		Volume 1 Point		Volume 1 point calibration		NbPtsCalibratio
23		32			15			Height 2 of Calibration Point		Height 2 Point		Hauteur 2 point calibration		Point Haut 2
24		32			15			Volume 2 of Calibration Point		Volume 2 Point		Volume 2 point calibration		NbPtsCalibratio
25		32			15			Height 3 of Calibration Point		Height 3 Point		Hauteur 3 point calibration		Point Haut 3
26		32			15			Volume 3 of Calibration Point		Volume 3 Point		Volume 3 point calibration		NbPtsCalibratio
27		32			15			Height 4 of Calibration Point		Height 4 Point		Hauteur4 point calibration		Point Haut 4
28		32			15			Volume 4 of Calibration Point		Volume 4 Point		Volume 4 point calibration		NbPtsCalibratio
29		32			15			Height 5 of Calibration Point		Height 5 Point		Hauteur 5 point calibration		Point Haut 5
30		32			15			Volume 5 of Calibration Point		Volume 5 Point		Volume 5 point calibration		NbPtsCalibratio
31		32			15			Height 6 of Calibration Point		Height 6 Point		Hauteur 6 point calibration		Point Haut 6
32		32			15			Volume 6 of Calibration Point		Volume 6 Point		Volume 6 point calibration		NbPtsCalibratio
33		32			15			Height 7 of Calibration Point		Height 7 Point		Hauteur 7 point calibration		Point Haut 7
34		32			15			Volume 7 of Calibration Point		Volume 7 Point		Volume 7 point calibration		NbPtsCalibratio
35		32			15			Height 8 of Calibration Point		Height 8 Point		Hauteur 8 point calibration		Point Haut 8
36		32			15			Volume 8 of Calibration Point		Volume 8 Point		Volume 8 point calibration		NbPtsCalibratio
37		32			15			Height 9 of Calibration Point		Height 9 Point		Hauteur 9 point calibration		Point Haut 9
38		32			15			Volume 9 of Calibration Point		Volume 9 Point		Volume 9 point calibration		NbPtsCalibratio
39		32			15			Height 10 of Calibration Point		Height 10 Point		Hauteur 10 point calibration		Point Haut 10
40		32			15			Volume 10 of Calibration Point		Volume 10 Point		Volume 10 point calibration		NbPtsCalibratio
41		32			15			Height 11 of Calibration Point		Height 11 Point		Hauteur 11 point calibration		Point Haut 11
42		32			15			Volume 11 of Calibration Point		Volume 11 Point		Volume 11 point calibration		NbPtsCalibratio
43		32			15			Height 12 of Calibration Point		Height 12 Point		Hauteur 12 point calibration		Point Haut 12
44		32			15			Volume 12 of Calibration Point		Volume 12 Point		Volume 12 point calibration		NbPtsCalibratio
45		32			15			Height 13 of Calibration Point		Height 13 Point		Hauteur 13 point calibration		Point Haut 13
46		32			15			Volume 13 of Calibration Point		Volume 13 Point		Volume 13 point calibration		NbPtsCalibratio
47		32			15			Height 14 of Calibration Point		Height 14 Point		Hauteur 14 point calibration		Point Haut 14
48		32			15			Volume 14 of Calibration Point		Volume 14 Point		Volume 14 point calibration		NbPtsCalibratio
49		32			15			Height 15 of Calibration Point		Height 15 Point		Hauteur 15 point calibration		Point Haut 15
50		32			15			Volume 15 of Calibration Point		Volume 15 Point		Volume 15 point calibration		NbPtsCalibratio
51		32			15			Height 16 of Calibration Point		Height 16 Point		Hauteur 16 point calibration		Point Haut 16
52		32			15			Volume 16 of Calibration Point		Volume 16 Point		Volume 16 point calibration		NbPtsCalibratio
53		32			15			Height 17 of Calibration Point		Height 17 Point		Hauteur 17 point calibration		Point Haut 17
54		32			15			Volume 17 of Calibration Point		Volume 17 Point		Volume 17 point calibration		NbPtsCalibratio
55		32			15			Height 18 of Calibration Point		Height 18 Point		Hauteur 18 point calibration		Point Haut 18
56		32			15			Volume 18 of Calibration Point		Volume 18 Point		Volume 18 point calibration		NbPtsCalibratio
57		32			15			Height 19 of Calibration Point		Height 19 Point		Hauteur 19 point calibration		Point Haut 19
58		32			15			Volume 19 of Calibration Point		Volume 19 Point		Volume 19 point calibration		NbPtsCalibratio
59		32			15			Height 20 of Calibration Point		Height 20 Point		Hauteur 20 point calibration		Point Haut 20
60		32			15			Volume 20 of Calibration Point		Volume 20 Point		Volume 20 point calibration		NbPtsCalibratio
#61		32			15			None					None			Aucun		Aucun
62		32			15			Square Tank				Square Tank		Reservoir rectangulaire	ReservRectang
63		32			15			Vertical Cylinder Tank			Vert Cyl Tank		Reservoir cylindrique vertical		ResrvCylindVert
64		32			15			Horizontal Cylinder Tank		Horiz Cyl Tank		Reservoir cylindrique horizontal		ResrvCylindHori
65		32			15			Multi Shape Tank			MultiShapeTank		Reservoir  Multi-Sharp		ReservMultSharp
66		32			15			Low Fuel Level Limit			Low Level Limit		Niveau bas reservoir	NivBasReserv
67		32			15			High Fuel Level Limit			Hi Level Limit		Niveau haut reservoir	NivHautReserv
68		32			15			Maximum Consumption Speed		Max Flow Speed		Consommation instantanée maximum		ConsomInstaMaxi
#for alarm of the volume of remained fuel	
71		32			15			High Fuel Level Alarm			Hi Level Alarm		Alarme Niveau haut reservoir		AlNivHautReserv
72		32			15			Low Fuel Level Alarm			Low Level Alarm		Alarme Niveau bas reservoir		Al NivBasReserv
73		32			15			Fuel Theft Alarm			Fuel Theft Alm		Alarme vol gasoil		Al Vol Gasoil
74		32			15			Square Tank Height Error		Sq Tank Hgt Err		Err.haut.reservoir rectangulaire		ErrHautResRect
75		32			15			Vert Cylinder Tank Height Error		Vt Tank Hgt Err		Err.haut.reservoir vertical		ErrHautResVert
76		32			15			Horiz Cylinder Tank Height Error	Hr Tank Hgt Err		Err.haut.reservoir horizontal	ErrHautResHoriz
77		32			15			Tank Height Error			Tank Height Err		Erreur hauteur reservoir		ErrHautReservoi	
78		32			15			Fuel Tank Config Error			Fuel Config Err		Erreur configuration reservoir	ErrConfReservoi
80		32			15			Fuel Tank Config Error Status		Config Err		Err.statut config.reservoir		ErrStaConfReser
81		32			15			X1		X1		X1	X1
82		32			15			Y1		Y1		Y1	Y1
83		32			15			X2		X2		X2	X2
84		32			15			Y2		Y2		Y2	Y2
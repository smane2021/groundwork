﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			Diesel Generator Battery Voltage	Diesel Bat Volt			Tension Batterie Groupe		Tens Bat Groupe
2		32			15			Diesel Generator Running		Diesel Running		Groupe en marche		Groupe marche
3		32			15			Relay 2 Status				Relay 2 Status		Etat Relais 2			Etat Relais 2
4		32			15			Relay 3 Status				Relay 3 Status		Etat Relais 3			Etat Relais 3
5		32			15			Relay 4 Status				Relay 4 Status		Etat Relais 4			Etat Relais 4
6		32			15			Diesel Generator Failure Status		Diesel Fail	Défaut Groupe Electrogène	Défaut Groupe
7		32			15			DG Connected Status		DG Connect			Groupe E. connecté		Groupe connecté
8		32			15			Low Fuel Level Status			Low Fuel Level		Niveau bas Gasoil		Niveau bas Gas.
9		32			15			High Water Temperature Status		High Water Temp	Température haute Eau		Temp. haute Eau
10		32			15			Low Oil Pressure Status			Low Oil Press		Pression Huile basse		PressHuileBasse
11		32			15			Start Diesel Generator				Start Diesel		Démarrage Groupe		Démarrage Grp
12		32			15			Relay 2 On/Off				Relay 2 On/Off		Relais 2 connecté		Rel2 connecté
13		32			15			Relay 3 On/Off				Relay 3 On/Off		Relais 3 connecté		Rel3 connecté
14		32			15			Relay 4 On/Off				Relay 4 On/Off		Relais 4 connecté		Rel4 connecté
15		32			15			Battery Voltage Limit			Batt Volt Limit		Límite tensión Batería		Lim Tens Bat
16		32			15			Relay 1 Pulse Time			Relay1 Pulse T		Temporisation relais 1		Tempo relais 1
17		32			15			Relay 2 Pulse Time			Relay2 Pulse T		Temporisation relais 2		Tempo relais 2
18		32			15			Relay 1 Pulse Time			Relay1 Pulse T	Temporisation relais 1		Tempo relais 1
19		32			15			Relay 2 Pulse Time			Relay2 Pulse T		Temporisation relais 2		Tempo relais 2
20		32			15			Low DC Voltage				Low DC Voltage		Tension batterie basse		V Bat basse
21		32			15			DG Supervision Failure			SupervisionFail	Défaut suppervision G.E.	Déf Supper G.E
22		32			15			Diesel Generator Failure		Diesel Fail		Défaut Groupe Electrogène	Défaut GE
23		32			15			Diesel Generator Connected		Diesel Connect		Groupe Electrogène connecté	GE connecté
24		32			15			Not Running				Not Running		Arrêt				Arrêt
25		32			15			Running					Running			Marche				Marche	
26		32			15			Off					Off			Off				Off
27		32			15			On					On			On				On
28		32			15			Off					Off			Off				Off
29		32			15			On					On			On				On
30		32			15			Off					Off			Off				Off
31		32			15			On					On			On				On
32		32			15			No					No			Non				Non
33		32			15			Yes					Yes			Oui				Oui
34		32			15			No					No			Non				Non
35		32			15			Yes					Yes			Oui				Oui
36		32			15			Off					Off			Off				Off
37		32			15			On					On			On				On
38		32			15			Off					Off			Off				Off
39		32			15			On					On			On				On
40		32			15			Off					Off			Off				Off
41		32			15			On					On			On				On
42		32			15			Diesel Generator			Dsl Generator		Groupe electrogène		Groupe electro
43		32			15			Mains Connected				Mains Connected		Secteur connecté		SecteurConnecté
44		32			15			Diesel Generator Shutdown		Diesel Shutdown		Groupe E. déconnecté		GE déconnecté
45		32			15			Low Fuel Level				Low Fuel Level		Niveau bas Gasoil		Niveau bas Gas.
46		32			15			High Water Temperature			High Water Temp		Température haute Eau		Temp. haute Eau
47		32			15			Low Oil Pressure			Low Oil Press		Pression Huile basse		PressHuileBasse
48		32			15			Communication Failure			Comm Fail		Défaut suppervision		Défaut supp.
49		32			15			Low Oil Pressure Alarm Status		Low Oil Press		Etat Pression Huile		Press.Huile
50		32			15			Clear Low Oil Pressure Alarm		Clr Oil Press		Alarme Pression Huile		Al.Press.Huile
51		32			15			State					State			Etat				Etat
52		32			15			Existence State				Existence State		Présent				Présent
53		32			15			Existent				Existent		Présent				Présent
54		32			15			Not Existent				Not Existent		Non Présent			Non Présent
55		32			15			Total Run Time				Total Run Time			Temps de fonctionnement total		TempsFonctTotal
56		32			15			Maintenance Time Limit			Mtn Time Limit			Duree maintenance périodique			DureeMaintPerio
57		32			15			Clear Total Run Time			Clr Run Time			Initial.du temps focntionnement		Init TempsFonct
58		32			15			Periodical Maintenance Required		Mtn Required			Demande Maintenance périodique			DemandMaintPeri
59		32			15			Maintenance Countdown Timer		Mtn DownTimer		Decompte Periode Maintenance			Dec_MaintPeriod

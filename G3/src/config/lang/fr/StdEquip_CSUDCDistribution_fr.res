﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Distribution Fuse Tripped		Dist Fuse Trip	Défaut protection Distribution		Déf Prot Distri
2		32			15			Contactor 1 Failure		Contactor1 Fail		Défaut contacteur 1			Déf contact 1
3		32			15			Distribution Fuse 2 Tripped		Dist Fuse2 Trip		Défaut protection Distribution2		Déf Prot Dist 2
4		32			15			Contactor 2 Failure		Contactor2 Fail		Défaut contacteur  2			Déf contact 2
5		32			15			Distribution Voltage		Distr Voltage	Tension Distribution			Tension Distrib
6		32			15			Distribution Current		Distr Current	Courant Distribution			Courant Distrib
7		32			15			Distribution Temperature		Distr Temp		Température Distribution		Temp Distrib
8		32			15			Distribution 2 Current		Distr 2 Current		Courant Distribution 2			Courant Dist 2
9		32			15			Distribution 2 Voltage			Distr 2 Voltage		Tension Distribution 2			Tension Dist 2
10		32			15			CSU DC Distribution			CSU DC Distr		Distribution DC CSU			CSU Distr DC
11		32			15			CSU DC Distribution Failure		CSU DCDist Fail	Défaut protection DC CSU		Déf Prot DC CSU
12		32			15			No								No						Non						Non
13		32			15			Yes					Yes			Oui					Oui
14		32			15			Existent				Existent		Présent					Présent
15		32			15			Not Existent				Not Existent		Non Présent				Non Présent

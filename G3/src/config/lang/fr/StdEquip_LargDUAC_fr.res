﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#ResId	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Manual					Manual			Manuel				Manuel
2	32			15			Auto					Auto			Auto				Auto
3	32			15			Off					Off			Non				Non
4	32			15			On					On			Oui				Oui
5	32			15			No Input				No Input		Pas d'entrée			Pas d'entrée
6	32			15			Input 1					Input 1			Entrée 1			Entrée 1
7	32			15			Input 2					Input 2			Entrée 2			Entrée 2
8	32			15			Input 3					Input 3			Entrée 3			Entrée 3
9	32			15			No Input				No Input		Pas d'entrée			Pas d'entrée
10	32			15			Input					Input			Entrée				Entrée
11	32			15			Closed					Closed			Fermé				Fermé	
12	32			15			Open					Open			Ouvert				Ouvert
13	32			15			Closed					Closed			Fermé				Fermé
14	32			15			Open					Open			Ouvert				Ouvert
15	32			15			Closed					Closed			Fermé				Fermé
16	32			15			Open					Open			Ouvert				Ouvert
17	32			15			Closed					Closed			Fermé				Fermé
18	32			15			Open					Open			Ouvert				Ouvert
19	32			15			Closed					Closed			Fermé				Fermé
20	32			15			Open					Open			Ouvert				Ouvert
21	32			15			Closed					Closed			Fermé				Fermé
22	32			15			Open					Open			Ouvert				Ouvert
23	32			15			Closed					Closed			Fermé				Fermé
24	32			15			Open					Open			Ouvert				Ouvert
25	32			15			Closed					Closed			Fermé				Fermé
26	32			15			Open					Open			Ouvert				Ouvert
27	32			15			1-Phase					1-Phase			Mono Phasé			Mono Phasé
28	32			15			3-Phase					3-Phase			Tri Phasé			Tri Phasé
29	32			15			No Measurement				No Measurement		Pas de mesure			Pas de mesure
30	32			15			1-Phase					1-Phase			Mono Phasé			Mono Phasé
31	32			15			3-Phase					3-Phase			Tri Phasé			Tri Phasé
32	32			15			Communication OK		Comm OK			Communication OK				COMM OK
33	32			15			Communication Fail		Comm Fail		Defaut de communication			Defaut COM
34	32			15			AC Distribution			AC Distribution		Distribution AC			Distribution AC
35	32			15			Mains 1 Uab/Ua			Mains 1 Uab/Ua		Secteur 1 V phase 1		AC 1 V phase 1
36	32			15			Mains 1 Ubc/Ub			Mains 1 Ubc/Ub		Secteur 1 V phase 2		AC 1 V phase 2
37	32			15			Mains 1 Uca/Uc			Mains 1 Uca/Uc		Secteur 1 V phase 3		AC 1 V phase 3
38	32			15			Mains 2 Uab/Ua			Mains 2 Uab/Ua		Secteur 2 V phase 1		AC 2 V phase 1
39	32			15			Mains 2 Ubc/Ub			Mains 2 Ubc/Ub		Secteur 2 V phase 2		AC 2 V phase 2
40	32			15			Mains 2 Uca/Uc			Mains 2 Uca/Uc		Secteur 2 V phase 3		AC 2 V phase 3
41	32			15			Mains 3 Uab/Ua			Mains 3 Uab/Ua		Secteur 3 V phase 1		AC 3 V phase 1
42	32			15			Mains 3 Ubc/Ub			Mains 3 Ubc/Ub		Secteur 3 V phase 2		AC 3 V phase 2
43	32			15			Mains 3 Uca/Uc			Mains 3 Uca/Uc		Secteur 3 V phase 3		AC 3 V phase 3
53	32			15			Working Phase A Current		Phase A Curr		Courant Phase 1			Courant Phase 1
54	32			15			Working Phase B Current		Phase B Curr		Courant Phase 2			Courant Phase 2
55	32			15			Working Phase C Current		Phase C Curr		Courant Phase 3			Courant Phase 3
56	32			15			AC Input Frequency		AC Input Freq		Fréquence Entrée AC		Fréq. Entr CA
57	32			15			AC Input Switch Mode		AC Switch Mode		Mode commutateur entrée AC	Mode Commut. AC
58	32			15			Fault Lighting Status		Fault Lighting		Défaut parafoudre		Déf parafoudre
59	32			15			Mains 1 Input Status		1 Input Status		Etat entrée AC 1		Etat entrée AC1
60	32			15			Mains 2 Input Status		2 Input Status		Etat entrée AC 2		Etat entrée AC2
61	32			15			Mains 3 Input Status		3 Input Status		Etat entrée AC 3		Etat entrée AC3
62	32			15			AC Output 1 Status		Output 1 Status		Etat Sortie AC 1		Etat Sortie AC1
63	32			15			AC Output 2 Status		Output 2 Status		Etat Sortie AC 2		Etat Sortie AC2
64	32			15			AC Output 3 Status		Output 3 Status		Etat Sortie AC 3		Etat Sortie AC3
65	32			15			AC Output 4 Status		Output 4 Status		Etat Sortie AC 4		Etat Sortie AC4
66	32			15			AC Output 5 Status		Output 5 Status		Etat Sortie AC 5		Etat Sortie AC5
67	32			15			AC Output 6 Status		Output 6 Status		Etat Sortie AC 6		Etat Sortie AC6
68	32			15			AC Output 7 Status		Output 7 Status		Etat Sortie AC 7		Etat Sortie AC7
69	32			15			AC Output 8 Status		Output 8 Status		Etat Sortie AC 8		Etat Sortie AC8
70	32			15			AC Input Frequency High		Frequency High		Fréquence AC Haute		Fréq. AC Haute
71	32			15			AC Input Frequency Low		Frequency Low		Fréquence AC Basse		Fréq. AC Basse
72	32			15			AC Input MCCB Trip		Input MCCB Trip		Entrée coupure disjoncteur	Ent.coupure DJ
73	32			15			SPD Trip				SPD Trip		Défaut parafoudre		Déf Parafoudre
74	32			15			AC Output MCCB Trip		OutputMCCB Trip		Défaut Protect.Parafoudre	Déf.Prot.Para
75	32			15			AC Input 1 Failure		Input 1 Failure		Défaut entrée AC 1		Déf entrée AC1
76	32			15			AC Input 2 Failure		Input 2 Failure		Défaut entrée AC 2		Déf entrée AC2
77	32			15			AC Input 3 Failure		Input 3 Failure		Défaut entrée AC 3		Déf entrée AC3
78	32			15			Mains 1 Uab/Ua Under Voltage	M1Uab/Ua UnderV		V Bas  AC1 Ph1			V Bas  AC1 Ph1
79	32			15			Mains 1 Ubc/Ub Under Voltage	M1Ubc/Ub UnderV		V Bas  AC1 Ph2			V Bas  AC1 Ph2
80	32			15			Mains 1 Uca/Uc Under Voltage	M1Uca/Uc UnderV		V Bas  AC1 Ph3			V Bas  AC1 Ph3
81	32			15			Mains 2 Uab/Ua Under Voltage	M2Uab/Ua UnderV		V Bas  AC2 Ph1			V Bas  AC2 Ph1
82	32			15			Mains 2 Ubc/Ub Under Voltage	M2Ubc/Ub UnderV		V Bas  AC2 Ph2			V Bas  AC2 Ph2
83	32			15			Mains 2 Uca/Uc Under Voltage	M2Uca/Uc UnderV		V Bas  AC2 Ph3			V Bas  AC2 Ph3
84	32			15			Mains 3 Uab/Ua Under Voltage	M3Uab/Ua UnderV		V Bas  AC3 Ph1			V Bas  AC3 Ph1
85	32			15			Mains 3 Ubc/Ub Under Voltage	M3Ubc/Ub UnderV		V Bas  AC3 Ph1			V Bas  AC3 Ph1
86	32			15			Mains 3 Uca/Uc Under Voltage	M3Uca/Uc UnderV		V Bas  AC3 Ph2			V Bas  AC3 Ph2
87	32			15			Mains 1 Uab/Ua Over Voltage	M1 Uab/Ua OverV		V Haut  AC1 Ph1			V Haut AC1 Ph1
88	32			15			Mains 1 Ubc/Ub Over Voltage	M1 Ubc/Ub OverV		V Haut  AC1 Ph2			V Haut AC1 Ph2
89	32			15			Mains 1 Uca/Uc Over Voltage	M1 Uca/Uc OverV		V Haut  AC1 Ph3			V Haut AC1 Ph3
90	32			15			Mains 2 Uab/Ua Over Voltage	M2 Uab/Ua OverV		V Haut  AC2 Ph1			V Haut AC2 Ph1
91	32			15			Mains 2 Ubc/Ub Over Voltage	M2 Ubc/Ub OverV		V Haut  AC2 Ph2			V Haut AC2 Ph2
92	32			15			Mains 2 Uca/Uc Over Voltage	M2 Uca/Uc OverV		V Haut  AC2 Ph3			V Haut AC2 Ph3
93	32			15			Mains 3 Uab/Ua Over Voltage	M3 Uab/Ua OverV		V Haut  AC3 Ph1			V Haut AC3 Ph1
94	32			15			Mains 3 Ubc/Ub Over Voltage	M3 Ubc/Ub OverV		V Haut  AC3 Ph2			V Haut AC3 Ph2
95	32			15			Mains 3 Uca/Uc Over Voltage	M3 Uca/Uc OverV		V Haut  AC3 Ph3			V Haut AC3 Ph3
96	32			15			Mains 1 Uab/Ua Failure		M1 Uab/Ua Fail		Défaut AC1 Ph1			Défaut AC1 Ph1
97	32			15			Mains 1 Ubc/Ub Failure		M1 Ubc/Ub Fail		Défaut AC1 Ph2			Défaut AC1 Ph2
98	32			15			Mains 1 Uca/Uc Failure		M1 Uca/Uc Fail		Défaut AC1 Ph3			Défaut AC1 Ph3
99	32			15			Mains 2 Uab/Ua Failure		M2 Uab/Ua Fail		Défaut AC2 Ph1			Défaut AC2 Ph1
100	32			15			Mains 2 Ubc/Ub Failure		M2 Ubc/Ub Fail		Défaut AC2 Ph2			Défaut AC2 Ph2
101	32			15			Mains 2 Uca/Uc Failure		M2 Uca/Uc Fail		Défaut AC2 Ph3			Défaut AC2 Ph3
102	32			15			Mains 3 Uab/Ua Failure		M3 Uab/Ua Fail		Défaut AC3 Ph1			Défaut AC3 Ph1
103	32			15			Mains 3 Ubc/Ub Failure		M3 Ubc/Ub Fail		Défaut AC3 Ph2			Défaut AC3 Ph2
104	32			15			Mains 3 Uca/Uc Failure		M3 Uca/Uc Fail		Défaut AC3 Ph3			Défaut AC3 Ph3
105	32			15			Communication Fail		Comm Fail		Defaut de communication			Defaut COM
106	32			15			Over Voltage Limit		Over Volt Limit			Niveau surtension		Niveau sur U
107	32			15			Under Voltage Limit		Under Volt Lmt		Niveau soustension		Niveau sous U
108	32			15			Phase Failure Voltage		Phase Fail Volt	Tension de Défaut		Tension Défaut
109	32			15			Over Frequency Limit		Over Freq Limit		Fréquence Maximum		Fréquence Max
110	32			15			Under Frequency Limit		Under Freq Lmt		Fréquence Mimimum		Fréquence Mim
111	32			15			Current Transformer Coef	Curr Trans Coef	Coefficient Transformateur	Coeff Transfo
112	32			15			Input Type			Input Type	Type entrée			Type entrée
113	32			15			Input Number			Input Number	Numéro Entrée			Numéro Entrée
114	32			15			Current Measurement		Cur Measurement		Mesure courant			Mesure courant
115	32			15			Output Number			Output Number	Numéro de Sortie		Numéro Sortie
116	32			15			Distribution Address		Distri Address	Adresse Distribution		Adresse Distrib
117	32			15			Mains 1 Failure				Mains 1 Fail		Défaut AC1			Défaut AC1
118	32			15			Mains 2 Failure				Mains 2 Fail		Défaut AC2			Défaut AC2
119	32			15			Mains 3 Failure				Mains 3 Fail		Défaut AC3			Défaut AC3
120	32			15			Mains 1 Uab/Ua Failure		M1 Uab/Ua Fail		Défaut AC1 Ph1			Défaut AC1 Ph1
121	32			15			Mains 1 Ubc/Ub Failure		M1 Ubc/Ub Fail		Défaut AC1 Ph2			Défaut AC1 Ph2
122	32			15			Mains 1 Uca/Uc Failure		M1 Uca/Uc Fail		Défaut AC1 Ph3			Défaut AC1 Ph3
123	32			15			Mains 2 Uab/Ua Failure		M2 Uab/Ua Fail		Défaut AC2 Ph1			Défaut AC2 Ph1
124	32			15			Mains 2 Ubc/Ub Failure		M2 Ubc/Ub Fail		Défaut AC2 Ph2			Défaut AC2 Ph2
125	32			15			Mains 2 Uca/Uc Failure		M2 Uca/Uc Fail		Défaut AC2 Ph3			Défaut AC2 Ph3
126	32			15			Mains 3 Uab/Ua Failure		M3 Uab/Ua Fail		Défaut AC3 Ph1			Défaut AC3 Ph1
127	32			15			Mains 3 Ubc/Ub Failure		M3 Ubc/Ub Fail		Défaut AC3 Ph2			Défaut AC3 Ph2
128	32			15			Mains 3 Uca/Uc Failure		M3 Uca/Uc Fail		Défaut AC3 Ph3			Défaut AC3 Ph3
129	32			15			Over Frequency			Over Frequency		Fréquence AC Haute		Fréq. AC Haute
130	32			15			Under Frequency			Under Frequency		Fréquence AC Basse		Fréq. AC Basse
131	32			15			Mains 1 Uab/Ua Under Voltage	M1Uab/Ua UnderV	Sous U AC1 Ph 1			Sous U AC1 Ph1
132	32			15			Mains 1 Ubc/Ub Under Voltage	M1Ubc/Ub UnderV	Sous U AC1 Ph 2			Sous U AC1 Ph2
133	32			15			Mains 1 Uca/Uc Under Voltage	M1Uca/Uc UnderV	Sous U AC1 Ph 3			Sous U AC1 Ph3
134	32			15			Mains 2 Uab/Ua Under Voltage	M2Uab/Ua UnderV	Sous U AC2 Ph 1			Sous U AC2 Ph1
135	32			15			Mains 2 Ubc/Ub Under Voltage	M2Ubc/Ub UnderV	Sous U AC2 Ph 2			Sous U AC2 Ph2
136	32			15			Mains 2 Uca/Uc Under Voltage	M2Uca/Uc UnderV	Sous U AC2 Ph 3			Sous U AC2 Ph3
137	32			15			Mains 3 Uab/Ua Under Voltage	M3Uab/Ua UnderV	Sous U AC3 Ph 1			Sous U AC3 Ph1
138	32			15			Mains 3 Ubc/Ub Under Voltage	M3Ubc/Ub UnderV	Sous U AC3 Ph 2			Sous U AC3 Ph2
139	32			15			Mains 3 Uca/Uc Under Voltage	M3Uca/Uc UnderV	Sous U AC3 Ph 3			Sous U AC3 Ph3
140	32			15			Mains 1 Uab/Ua Over Voltage	M1 Uab/Ua OverV		Sur U AC1 Ph 1			Sur U AC1 Ph1
141	32			15			Mains 1 Ubc/Ub Over Voltage	M1 Ubc/Ub OverV		Sur U AC1 Ph 2			Sur U AC1 Ph2
142	32			15			Mains 1 Uca/Uc Over Voltage	M1 Uca/Uc OverV		Sur U AC1 Ph 3			Sur U AC1 Ph3
143	32			15			Mains 2 Uab/Ua Over Voltage	M2 Uab/Ua OverV		Sur U AC2 Ph 1			Sur U AC2 Ph1
144	32			15			Mains 2 Ubc/Ub Over Voltage	M2 Ubc/Ub OverV		Sur U AC2 Ph 2			Sur U AC2 Ph2
145	32			15			Mains 2 Uca/Uc Over Voltage	M2 Uca/Uc OverV		Sur U AC2 Ph 3			Sur U AC2 Ph3
146	32			15			Mains 3 Uab/Ua Over Voltage	M3 Uab/Ua OverV		Sur U AC3 Ph 1			Sur U AC3 Ph1
147	32			15			Mains 3 Ubc/Ub Over Voltage	M3 Ubc/Ub OverV		Sur U AC3 Ph 2			Sur U AC3 Ph2
148	32			15			Mains 3 Uca/Uc Over Voltage	M3 Uca/Uc OverV		Sur U AC3 Ph 3			Sur U AC3 Ph3
149	32			15			AC Input MCCB Trip		Input MCCB Trip		Entrée Défaut disjoncteur	Ent.Déf. DJ
150	32			15			AC Output MCCB Trip		OutputMCCB Trip		Sortie défaut disjoncteur	Sort.Def DJ
151	32			15			SPD Trip			SPD Trip	Défaut Parafoudre		Déf Parafoudre
169	32			15			Communication Fail		Comm Fail		Defaut de communication			Defaut COM
170	32			15			Mains Failure				Mains Failure		Défaut secteur			Défaut secteur
171	32			15			LargeDU AC Distribution		AC Distribution			Grande DU Distribution AC		GranDU DistriAC
172	32			15			No Alarm				No Alarm		Aucune Alarme			Aucune Alarme
173	32			15			Over Voltage			Over Volt		Sous Tension			Sous Tension
174	32			15			Under Voltage			Under Volt		Sur Tension			Sur Tension
175	32			15			AC Phase Failure			AC Phase Fail		Défaut Phase AC			Déf. Phase AC
176	32			15			No Alarm				No Alarm		Aucune Alarme			Aucune Alarme
177	32			15			Over Frequency			Over Frequency		Fréquence Haute			Fréquence Haute
178	32			15			Under Frequency			Under Frequency		Fréquence Basse			Fréquence Basse
179	32			15			No Alarm				No Alarm		Aucune Alarme			Aucune Alarme
180	32			15			AC Over Voltage			AC Over Volt		Sous Tension			Sous Tension
181	32			15			AC Under Voltage		AC Under Volt		Sur Tension			Sur Tension
182	32			15			AC Phase Failure			AC Phase Fail		Défaut Phase AC			Déf. Phase AC
183	32			15			No Alarm				No Alarm		Aucune Alarme			Aucune Alarme
184	32			15			AC Over Voltage			AC Over Volt	Sous Tension			Sous Tension
185	32			15			AC Under Voltage		AC Under Volt		Sur Tension			Sur Tension
186	32			15			AC Phase Failure			AC Phase Fail		Défaut Phase AC			Déf. Phase AC
187	32			15			Mains 1 Uab/Ua Alarm		M1 Uab/Ua Alarm		Alarme U AC1 Ph 1		Al U AC1 Ph1
188	32			15			Mains 1 Ubc/Ub Alarm		M1 Ubc/Ub Alarm		Alarme U AC1 Ph 2		Al U AC1 Ph2
189	32			15			Mains 1 Uca/Uc Alarm		M1 Uca/Uc Alarm		Alarme U AC1 Ph 3		Al U AC1 Ph3
190	32			15			Frequency Alarm			Frequency Alarm		Alarme Fréquence		Al. Fréquence
191	32			15			Communication Fail		Comm Fail		Defaut de communication			Defaut COM
192	32			15			Normal					Normal			Normal				Normal
193	32			15			Failure					Failure			Défaut				Défaut
194	32			15			Communication Fail		Comm Fail		Defaut de communication			Defaut COM
195	32			15			Mains Input Number		Mains Input Num		Nombre entrée AC		Nbre entrée AC
196	32			15			Number 1			Number 1			1 entrée			1 entrée
197	32			15			Number 2			Number 2			2 entrées			2 entrées
198	32			15			Number 3			Number 3			3 entrées			3 entrées
199	32			15			None					None			Aucune				Aucune
200	32			15			Emergency Light				Emergency Light		Arrêt d'urgence			Arrêt Urgence
201	32			15			Closed					Closed			Non				Non
202	32			15			Open					Open			Oui				Oui
203	32			15			Mains 2 Uab/Ua Alarm		M2 Uab/Ua Alarm		Alarme U AC2 Ph 1		Al U AC2 Ph1
204	32			15			Mains 2 Ubc/Ub Alarm		M2 Ubc/Ub Alarm		Alarme U AC2 Ph 2		Al U AC2 Ph2
205	32			15			Mains 2 Uca/Uc Alarm		M2 Uca/Uc Alarm		Alarme U AC2 Ph 3		Al U AC2 Ph3
206	32			15			Mains 3 Uab/Ua Alarm		M3 Uab/Ua Alarm		Alarme U AC3 Ph 1		Al U AC3 Ph1
207	32			15			Mains 3 Ubc/Ub Alarm		M3 Ubc/Ub Alarm		Alarme U AC3 Ph 2		Al U AC3 Ph2
208	32			15			Mains 3 Uca/Uc Alarm		M3 Uca/Uc Alarm		Alarme U AC3 Ph 3		Al U AC3 Ph3
209	32			15			Normal					Normal			Normal				Normal
210	32			15			Alarm					Alarm			Alarme				Alarme
211	32			15			AC Fuse Number			AC Fuse Num		Nombre départs AC		Nombre dép.AC
212	32			15			Existence State				Existence State		Détection			Détection
213	32			15			Existent				Existent		Présent				Présent	
214	32			15			Not Existent			Not Existent		Non Présent			Non Présent

﻿#
#  Locale language support:french
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Battery Current				Batt Current		Courant batterie		I batterie
2	32			15			Battery Rating(Ah)			Batt Rating(Ah)		Capacité batterie(Ah)		Capacité bat.
3	32			15			Exceed Current Limit			Exceed Curr Lmt		Limite de courant		Lim.de courant
4	32			15			Battery					Battery			Batterie			Batterie
5	32			15			Over Battery Current			Over Current		Sur Courant Batterie		Sur I Batt.
6	32			15			Battery Capacity (%)			Batt Cap (%)		Capacité batterie (%)		Cap bat(%)
7	32			15			Battery Voltage				Batt Voltage		Tension batterie		V batterie
8	32			15			Low Capacity				Low Capacity		Capacité Basse			Capacité Basse
9	32			15			On					On			On				On
10	32			15			Off					Off			Off				Off
11	32			15			Battery Fuse Voltage			Batt Fuse Volt		Tension fusible batterie		Tension Fus Bat
12	32			15			Battery Fuse Status			Batt Fuse State		Etat Fusible			Etat Fusible
13	32			15			Fuse Alarm				Fuse Alarm		Alarme Fusible			Alarme Fusible
14	32			15			SMBRC Battery				SMBRC Battery		SMBRC Bat			SMBRC Bat
15	32			15			State					State			Etat				Etat
16	32			15			Off					Off			Off				Off
17	32			15			On					On			On				On
18	32			15			Switch					Switch			Switch				Switch
19	32			15			Battery Over Current			Batt Over Curr			Sur Courant Batterie		Sur I Batt.
20	32			15			Battery Management			Batt Management		Gestion de Batterie		Gestion Bat
21	32			15			Yes					Yes			Oui				Oui
22	32			15			No					No			Non				Non
23	32			15			Over Voltage Setpoint			Over Volt Limit		Sur Tension			Sur Tension
24	32			15			Low Voltage Setpoint			Low Volt Limit		Sous Tension			Sous Tension
25	32			15			Battery Over Voltage			Batt Over Volt	Sur Tension Batterie		Sur Tension
26	32			15			Battery Under Voltage			Batt Under Volt		Sous Tension Batterie		Sous Tension
27	32			15			Over Current				Over Current		Sur Courant Batterie		Sur I Batt.
28	32			15			Communication Fail			Comm Fail		Défaut Communication		Défaut COM
29	32			15			Times of Communication Fail				Times Comm Fail		Défaut Communication		Défaut COM
44	32			15			Temp Num of Battery			Batt Temp Num	Capteur Température		Capteur Temp.
87	32			15			No					No			Aucun				Aucun
91	32			15			Battery Temp 1				Batt Temp 1		Capteur Température 1		Capteur Temp.1
92	32			15			Battery Temp 2				Batt Temp 2	Capteur Température 2		Capteur Temp.2
93	32			15			Battery Temp 3				Batt Temp 3		Capteur Température 3		Capteur Temp.3
94	32			15			Battery Temp 4				Batt Temp 4		Capteur Température 4		Capteur Temp.4
95	32			15			Battery Temp 5				Batt Temp 5	Capteur Température 5		Capteur Temp.5
96	32			15			Rated Capacity				Rated Capacity		Estimation Capacité		Estimation Cap
97	32			15			Low					Low			Basse				Basse
98	32			15			High					High			Haute				Haute
#99
100	32			15			Overall Voltage				Overall Volt		Tension totale			Tension totale
101	32			15			String Current				String Curr		Courant Branche Batterie	I Branche Batt
102	32			15			Float Current				Float Curr		Courant Floating Batterie	I Float Batt
103	32			15			Ripple Current				Ripple Curr		Pic Courant Batterie		Pic I Batt
104	32			15			Cell Number				Cell Number		Numéro de Batterie		Num.Batterie
105	32			15			Cell 1 Voltage				Cell 1 Voltage		Tension cellule 1			V cellule 1
106	32			15			Cell 2 Voltage				Cell 2 Voltage		Tension cellule 2			V cellule 2
107	32			15			Cell 3 Voltage				Cell 3 Voltage		Tension cellule 3			V cellule 3
108	32			15			Cell 4 Voltage				Cell 4 Voltage		Tension cellule 4			V cellule 4
109	32			15			Cell 5 Voltage				Cell 5 Voltage		Tension cellule 5			V cellule 5
110	32			15			Cell 6 Voltage				Cell 6 Voltage		Tension cellule 6			V cellule 6
111	32			15			Cell 7 Voltage				Cell 7 Voltage		Tension cellule 7			V cellule 7
112	32			15			Cell 8 Voltage				Cell 8 Voltage		Tension cellule 8			V cellule 8
113	32			15			Cell 9 Voltage				Cell 9 Voltage		Tension cellule 9			V cellule 9
114	32			15			Cell 10 Voltage				Cell 10 Voltage		Tension cellule 10			V cellule 10
115	32			15			Cell 11 Voltage				Cell 11 Voltage		Tension cellule 11			V cellule 11
116	32			15			Cell 12 Voltage				Cell 12 Voltage		Tension cellule 12			V cellule 12
117	32			15			Cell 13 Voltage				Cell 13 Voltage		Tension cellule 13			V cellule 13
118	32			15			Cell 14 Voltage				Cell 14 Voltage		Tension cellule 14			V cellule 14
119	32			15			Cell 15 Voltage				Cell 15 Voltage		Tension cellule 15			V cellule 15
120	32			15			Cell 16 Voltage				Cell 16 Voltage		Tension cellule 16			V cellule 16
121	32			15			Cell 17 Voltage				Cell 17 Voltage		Tension cellule 17			V cellule 17
122	32			15			Cell 18 Voltage				Cell 18 Voltage		Tension cellule 18			V cellule 18
123	32			15			Cell 19 Voltage				Cell 19 Voltage		Tension cellule 19			V cellule 19
124	32			15			Cell 20 Voltage				Cell 20 Voltage		Tension cellule 20			V cellule 20
125	32			15			Cell 21 Voltage				Cell 21 Voltage		Tension cellule 21			V cellule 21
126	32			15			Cell 22 Voltage				Cell 22 Voltage		Tension cellule 22			V cellule 22
127	32			15			Cell 23 Voltage				Cell 23 Voltage		Tension cellule 23			V cellule 23
128	32			15			Cell 24 Voltage				Cell 24 Voltage		Tension cellule 24			V cellule 24
129	32			15			Cell 1 Temperature			Cell 1 Temp		Température cellule 1		Temp cellule 1
130	32			15			Cell 2 Temperature			Cell 2 Temp		Température cellule 2		Temp cellule 2
131	32			15			Cell 3 Temperature			Cell 3 Temp		Température cellule 3		Temp cellule 3
132	32			15			Cell 4 Temperature			Cell 4 Temp		Température cellule 4		Temp cellule 4
133	32			15			Cell 5 Temperature			Cell 5 Temp		Température cellule 5		Temp cellule 5
134	32			15			Cell 6 Temperature			Cell 6 Temp		Température cellule 6		Temp cellule 6
135	32			15			Cell 7 Temperature			Cell 7 Temp		Température cellule 7		Temp cellule 7
136	32			15			Cell 8 Temperature			Cell 8 Temp		Température cellule 8		Temp cellule 8
137	32			15			Cell 9 Temperature			Cell 9 Temp		Température cellule 9		Temp cellule 9
138	32			15			Cell 10 Temperature			Cell 10 Temp		Température cellule 10		Temp cellule 10
139	32			15			Cell 11 Temperature			Cell 11 Temp		Température cellule 11		Temp cellule 11
140	32			15			Cell 12 Temperature			Cell 12 Temp		Température cellule 12		Temp cellule 12
141	32			15			Cell 13 Temperature			Cell 13 Temp		Température cellule 13		Temp cellule 13
142	32			15			Cell 14 Temperature			Cell 14 Temp		Température cellule 14		Temp cellule 14
143	32			15			Cell 15 Temperature			Cell 15 Temp		Température cellule 15		Temp cellule 15
144	32			15			Cell 16 Temperature			Cell 16 Temp		Température cellule 16		Temp cellule 16
145	32			15			Cell 17 Temperature			Cell 17 Temp		Température cellule 17		Temp cellule 17
146	32			15			Cell 18 Temperature			Cell 18 Temp		Température cellule 18		Temp cellule 18
147	32			15			Cell 19 Temperature			Cell 19 Temp		Température cellule 19		Temp cellule 19
148	32			15			Cell 20 Temperature			Cell 20 Temp		Température cellule 20		Temp cellule 20
149	32			15			Cell 21 Temperature			Cell 21 Temp		Température cellule 21		Temp cellule 21
150	32			15			Cell 22 Temperature			Cell 22 Temp		Température cellule 22		Temp cellule 22
151	32			15			Cell 23 Temperature			Cell 23 Temp		Température cellule 23		Temp cellule 23
152	32			15			Cell 24 Temperature			Cell 24 Temp		Température cellule 24		Temp cellule 24
153	32			15			Overall Volt Alarm			Overall VoltAlm		Alarme tension total		Alarme Tension
154	32			15			String Current Alarm			String Curr Alm		Alarme Courant Branche Bat	Al I BrancheBat
155	32			15			Float Current Alarm			Float Curr Alm		Alarme Courant Floating Bat	AL I Float Batt
156	32			15			Ripple Current Alarm			Ripple Curr Alm		Alarme Pic Courant Bat.		AL Pic I Batt
157	32			15			Cell Ambient Alarm			Cell Amb Alm		Alarme Température Cellule	AL Temp Cellule
158	32			15			Cell 1 Voltage Alarm			Cell 1 Volt Alm		Tension de Cellule 1		Tens Cellule 1
159	32			15			Cell 2 Voltage Alarm			Cell 2 Volt Alm		Tension de Cellule 2		Tens Cellule 2
160	32			15			Cell 3 Voltage Alarm			Cell 3 Volt Alm		Tension de Cellule 3		Tens Cellule 3
161	32			15			Cell 4 Voltage Alarm			Cell 4 Volt Alm		Tension de Cellule 4		Tens Cellule 4
162	32			15			Cell 5 Voltage Alarm			Cell 5 Volt Alm		Tension de Cellule 5		Tens Cellule 5
163	32			15			Cell 6 Voltage Alarm			Cell 6 Volt Alm		Tension de Cellule 6		Tens Cellule 6
164	32			15			Cell 7 Voltage Alarm			Cell 7 Volt Alm		Tension de Cellule 7		Tens Cellule 7
165	32			15			Cell 8 Voltage Alarm			Cell 8 Volt Alm		Tension de Cellule 8		Tens Cellule 8
166	32			15			Cell 9 Voltage Alarm			Cell 9 Volt Alm		Tension de Cellule 9		Tens Cellule 9
167	32			15			Cell 10 Voltage Alarm			Cell10 Volt Alm		Tension de Cellule 10		Tens Cellule 10
168	32			15			Cell 11 Voltage Alarm			Cell11 Volt Alm		Tension de Cellule 11		Tens Cellule 11
169	32			15			Cell 12 Voltage Alarm			Cell12 Volt Alm		Tension de Cellule 12		Tens Cellule 12
170	32			15			Cell 13 Voltage Alarm			Cell13 Volt Alm		Tension de Cellule 13		Tens Cellule 13
171	32			15			Cell 14 Voltage Alarm			Cell14 Volt Alm		Tension de Cellule 14		Tens Cellule 14
172	32			15			Cell 15 Voltage Alarm			Cell15 Volt Alm		Tension de Cellule 15		Tens Cellule 15
173	32			15			Cell 16 Voltage Alarm			Cell16 Volt Alm		Tension de Cellule 16		Tens Cellule 16
174	32			15			Cell 17 Voltage Alarm			Cell17 Volt Alm		Tension de Cellule 17		Tens Cellule 17
175	32			15			Cell 18 Voltage Alarm			Cell18 Volt Alm		Tension de Cellule 18		Tens Cellule 18
176	32			15			Cell 19 Voltage Alarm			Cell19 Volt Alm		Tension de Cellule 19		Tens Cellule 19
177	32			15			Cell 20 Voltage Alarm			Cell20 Volt Alm		Tension de Cellule 20		Tens Cellule 20
178	32			15			Cell 21 Voltage Alarm			Cell21 Volt Alm		Tension de Cellule 21		Tens Cellule 21
179	32			15			Cell 22 Voltage Alarm			Cell22 Volt Alm		Tension de Cellule 22		Tens Cellule 22
180	32			15			Cell 23 Volt Alarm			Cell23 Volt Alm		Tension de Cellule 23		Tens Cellule 23
181	32			15			Cell 24 Volt Alarm			Cell24 Volt Alm		Tension de Cellule 24		Tens Cellule 24
182	32			15			Cell 1 Temperature Alarm		Cell 1 Temp Alm		Alarme Temp Cellule 1		Temp Cellule 1
183	32			15			Cell 2 Temperature Alarm		Cell 2 Temp Alm		Alarme Temp Cellule 2		Temp Cellule 2
184	32			15			Cell 3 Temperature Alarm		Cell 3 Temp Alm		Alarme Temp Cellule 3		Temp Cellule 3
185	32			15			Cell 4 Temperature Alarm		Cell 4 Temp Alm		Alarme Temp Cellule 4		Temp Cellule 4
186	32			15			Cell 5 Temperature Alarm		Cell 5 Temp Alm		Alarme Temp Cellule 5		Temp Cellule 5
187	32			15			Cell 6 Temperature Alarm		Cell 6 Temp Alm		Alarme Temp Cellule 6		Temp Cellule 6
188	32			15			Cell 7 Temperature Alarm		Cell 7 Temp Alm		Alarme Temp Cellule 7		Temp Cellule 7
189	32			15			Cell 8 Temperature Alarm		Cell 8 Temp Alm		Alarme Temp Cellule 8		Temp Cellule 8
190	32			15			Cell 9 Temperature Alarm		Cell 9 Temp Alm		Alarme Temp Cellule 9		Temp Cellule 9
191	32			15			Cell 10 Temperature Alarm		Cell10 Temp Alm		Alarme Temp Cellule 10		Temp Cellule 10
192	32			15			Cell 11 Temperature Alarm		Cell11 Temp Alm		Alarme Temp Cellule 11		Temp Cellule 11
193	32			15			Cell 12 Temperature Alarm		Cell12 Temp Alm		Alarme Temp Cellule 12		Temp Cellule 12
194	32			15			Cell 13 Temperature Alarm		Cell13 Temp Alm		Alarme Temp Cellule 13		Temp Cellule 13
195	32			15			Cell 14 Temperature Alarm		Cell14 Temp Alm		Alarme Temp Cellule 14		Temp Cellule 14
196	32			15			Cell 15 Temperature Alarm		Cell15 Temp Alm		Alarme Temp Cellule 15		Temp Cellule 15
197	32			15			Cell 16 Temperature Alarm		Cell16 Temp Alm		Alarme Temp Cellule 16		Temp Cellule 16
198	32			15			Cell 17 Temperature Alarm		Cell17 Temp Alm		Alarme Temp Cellule 17		Temp Cellule 17
199	32			15			Cell 18 Temperature Alarm		Cell18 Temp Alm		Alarme Temp Cellule 18		Temp Cellule 18
200	32			15			Cell 19 Temperature Alarm		Cell19 Temp Alm		Alarme Temp Cellule 19		Temp Cellule 19
201	32			15			Cell 20 Temperature Alarm		Cell20 Temp Alm		Alarme Temp Cellule 20		Temp Cellule 20
202	32			15			Cell 21 Temperature Alarm		Cell21 Temp Alm		Alarme Temp Cellule 21		Temp Cellule 21
203	32			15			Cell 22 Temperature Alarm		Cell22 Temp Alm		Alarme Temp Cellule 22		Temp Cellule 22
204	32			15			Cell 23 Temperature Alarm		Cell23 Temp Alm		Alarme Temp Cellule 23		Temp Cellule 23
205	32			15			Cell 24 Temperature Alarm		Cell24 Temp Alm		Alarme Temp Cellule 24		Temp Cellule 24
206	32			15			Cell 1 Resistance Alarm			Cell 1  Res Alm	Défaut Résistance Cellule 1	Res Cellule 1
207	32			15			Cell 2 Resistance Alarm			Cell 2  Res Alm	Défaut Résistance Cellule 2	Res Cellule 2
208	32			15			Cell 3 Resistance Alarm			Cell 3  Res Alm	Défaut Résistance Cellule 3	Res Cellule 3
209	32			15			Cell 4 Resistance Alarm			Cell 4  Res Alm	Défaut Résistance Cellule 4	Res Cellule 4
210	32			15			Cell 5 Resistance Alarm			Cell 5  Res Alm	Défaut Résistance Cellule 5	Res Cellule 5
211	32			15			Cell 6 Resistance Alarm			Cell 6  Res Alm	Défaut Résistance Cellule 6	Res Cellule 6
212	32			15			Cell 7 Resistance Alarm			Cell 7  Res Alm	Défaut Résistance Cellule 7	Res Cellule 7
213	32			15			Cell 8 Resistance Alarm			Cell 8  Res Alm		Défaut Résistance Cellule 8	Res Cellule 8
214	32			15			Cell 9 Resistance Alarm			Cell 9  Res Alm		Défaut Résistance Cellule 9	Res Cellule 9
215	32			15			Cell 10 Resistance Alarm		Cell 10 Res Alm		Défaut Résistance Cellule 10	Res Cellule 10
216	32			15			Cell 11 Resistance Alarm		Cell 11 Res Alm		Défaut Résistance Cellule 11	Res Cellule 11
217	32			15			Cell 12 Resistance Alarm		Cell 12 Res Alm		Défaut Résistance Cellule 12	Res Cellule 12
218	32			15			Cell 13 Resistance Alarm		Cell 13 Res Alm		Défaut Résistance Cellule 13	Res Cellule 13
219	32			15			Cell 14 Resistance Alarm		Cell 14 Res Alm		Défaut Résistance Cellule 14	Res Cellule 14
220	32			15			Cell 15 Resistance Alarm		Cell 15 Res Alm		Défaut Résistance Cellule 15	Res Cellule 15
221	32			15			Cell 16 Resistance Alarm		Cell 16 Res Alm		Défaut Résistance Cellule 16	Res Cellule 16
222	32			15			Cell 17 Resistance Alarm		Cell 17 Res Alm		Défaut Résistance Cellule 17	Res Cellule 17
223	32			15			Cell 18 Resistance Alarm		Cell 18 Res Alm		Défaut Résistance Cellule 18	Res Cellule 18
224	32			15			Cell 19 Resistance Alarm		Cell 19 Res Alm		Défaut Résistance Cellule 19	Res Cellule 19
225	32			15			Cell 20 Resistance Alarm		Cell 20 Res Alm		Défaut Résistance Cellule 20	Res Cellule 20
226	32			15			Cell 21 Resistance Alarm		Cell 21 Res Alm		Défaut Résistance Cellule 21	Res Cellule 21
227	32			15			Cell 22 Resistance Alarm		Cell 22 Res Alm		Défaut Résistance Cellule 22	Res Cellule 22
228	32			15			Cell 23 Resistance Alarm		Cell 23 Res Alm		Défaut Résistance Cellule 23	Res Cellule 23
229	32			15			Cell 24 Resistance Alarm		Cell 24 Res Alm		Défaut Résistance Cellule 24	Res Cellule 24
230	32			15			Cell 1 Intercell Alarm			Cell 1 InterAlm	Défaut Résistance Cellule 1		DéfautCellule1
231	32			15			Cell 2 Intercell Alarm			Cell 2 InterAlm		Défaut Résistance Cellule 2		DéfautCellule2
232	32			15			Cell 3 Intercell Alarm			Cell 3 InterAlm		Défaut Résistance Cellule 3		DéfautCellule3
233	32			15			Cell 4 Intercell Alarm			Cell 4 InterAlm		Défaut Résistance Cellule 4		DéfautCellule4
234	32			15			Cell 5 Intercell Alarm			Cell 5 InterAlm		Défaut Résistance Cellule 5		DéfautCellule5
235	32			15			Cell 6 Intercell Alarm			Cell 6 InterAlm		Défaut Résistance Cellule 6		DéfautCellule6
236	32			15			Cell 7 Intercell Alarm			Cell 7 InterAlm		Défaut Résistance Cellule 7		DéfautCellule7
237	32			15			Cell 8 Intercell Alarm			Cell 8 InterAlm		Défaut Résistance Cellule 8		DéfautCellule8
238	32			15			Cell 9 Intercell Alarm			Cell 9 InterAlm		Défaut Résistance Cellule 9		DéfautCellule9
239	32			15			Cell 10 Intercell Alarm			Cell10 InterAlm		Défaut Résistance Cellule 10		DéfautCellule10
240	32			15			Cell 11 Intercell Alarm			Cell11 InterAlm		Défaut Résistance Cellule 11		DéfautCellule11
241	32			15			Cell 12 Intercell Alarm			Cell12 InterAlm		Défaut Résistance Cellule 12		DéfautCellule12
242	32			15			Cell 13 Intercell Alarm			Cell13 InterAlm		Défaut Résistance Cellule 13		DéfautCellule13
243	32			15			Cell 14 Intercell Alarm			Cell14 InterAlm		Défaut Résistance Cellule 14		DéfautCellule14
244	32			15			Cell 15 Intercell Alarm			Cell15 InterAlm		Défaut Résistance Cellule 15		DéfautCellule15
245	32			15			Cell 16 Intercell Alarm			Cell16 InterAlm		Défaut Résistance Cellule 16		DéfautCellule16
246	32			15			Cell 17 Intercell Alarm			Cell17 InterAlm		Défaut Résistance Cellule 17		DéfautCellule17
247	32			15			Cell 18 Intercell Alarm			Cell18 InterAlm		Défaut Résistance Cellule 18		DéfautCellule18
248	32			15			Cell 19 Intercell Alarm			Cell19 InterAlm		Défaut Résistance Cellule 19		DéfautCellule19
249	32			15			Cell 20 Intercell Alarm			Cell20 InterAlm		Défaut Résistance Cellule 20		DéfautCellule20
250	32			15			Cell 21 Intercell Alarm			Cell21 InterAlm		Défaut Résistance Cellule 21		DéfautCellule21
251	32			15			Cell 22 Intercell Alarm			Cell22 InterAlm		Défaut Résistance Cellule 22		DéfautCellule22
252	32			15			Cell 23 Intercell Alarm			Cell23 InterAlm		Défaut Résistance Cellule 23		DéfautCellule23
253	32			15			Cell 24 Intercell Alarm			Cell24 InterAlm		Défaut Résistance Cellule 24		DéfautCellule24
254	32			15			Cell1 Ambient Alm		Cell1 Amb Alm			Alarme Cellule 1		Amb Cellule 1
255	32			15			Cell2 Ambient Alm		Cell2 Amb Alm			Alarme Cellule 2		Amb Cellule 2
256	32			15			Cell3 Ambient Alm		Cell3 Amb Alm			Alarme Cellule 3		Amb Cellule 3
257	32			15			Cell4 Ambient Alm		Cell4 Amb Alm			Alarme Cellule 4		Amb Cellule 4
258	32			15			Cell5 Ambient Alm		Cell5 Amb Alm			Alarme Cellule 5		Amb Cellule 5
259	32			15			Cell6 Ambient Alm			Cell6 Amb Alm			Alarme Cellule 6		Amb Cellule 6
260	32			15			Cell7 Ambient Alm			Cell7 Amb Alm			Alarme Cellule 7		Amb Cellule 7
261	32			15			Cell8 Ambient Alm		Cell8 Amb Alm			Alarme Cellule 8		Amb Cellule 8
262	32			15			Cell9 Ambient Alm			Cell9 Amb Alm	Alarme Cellule 9		Amb Cellule 9
263	32			15			Cell10 Ambient Alm			Cell10 Amb Alm		Alarme Cellule 10	Amb Cellule 10
264	32			15			Cell11 Ambient Alm			Cell11 Amb Alm		Alarme Cellule 11	Amb Cellule 11
265	32			15			Cell12 Ambient Alm			Cell12 Amb Alm		Alarme Cellule 12	Amb Cellule 12
266	32			15			Cell13 Ambient Alm			Cell13 Amb Alm		Alarme Cellule 13	Amb Cellule 13
267	32			15			Cell14 Ambient Alm			Cell14 Amb Alm		Alarme Cellule 14	Amb Cellule 14
268	32			15			Cell15 Ambient Alm			Cell15 Amb Alm		Alarme Cellule 15	Amb Cellule 15
269	32			15			Cell16 Ambient Alm			Cell16 Amb Alm		Alarme Cellule 16	Amb Cellule 16
270	32			15			Cell17 Ambient Alm			Cell17 Amb Alm		Alarme Cellule 17	Amb Cellule 17
271	32			15			Cell18 Ambient Alm		Cell18 Amb Alm		Alarme Cellule 18	Amb Cellule 18
272	32			15			Cell19 Ambient Alm			Cell19 Amb Alm		Alarme Cellule 19	Amb Cellule 19
273	32			15			Cell20 Ambient Alm			Cell20 Amb Alm		Alarme Cellule 20	Amb Cellule 20
274	32			15			Cell21 Ambient Alm			Cell21 Amb Alm		Alarme Cellule 21	Amb Cellule 21
275	32			15			Cell22 Ambient Alm			Cell22 Amb Alm		Alarme Cellule 22	Amb Cellule 22
276	32			15			Cell23 Ambient Alm			Cell23 Amb Alm		Alarme Cellule 23	Amb Cellule 23
277	32			15			Cell24 Ambient Alm			Cell24 Amb Alm		Alarme Cellule 24	Amb Cellule 24
278	32			15			String Addr Value			String Addr Val		Numéro de branche		Numéro branche
279	32			15			String Seq Num				String Seq Num		Numéro de branche		Numéro branche
280	32			15			Cell Volt Low Alarm				Volt Low Alm		Tension de Cellule Basse		V Cellule Bas
281	32			15			Cell Temp Low Alarm			Temp Low Alm		Température de Cellule basse		Temp CelluleBas
282	32			15			Cell Resist Low Alarm			Resist Low Alm		Résistance de Cellule Basse		Res Cel Basse
283	32			15			Cell Inter Low Alarm				Inter Low Alm		Résist.Jonction Cellule Basse	R Jonc Cel Bas
284	32			15			Cell Ambient Low Alarm			Amb Low Alm		Température Ambiante Basse		Temp Amb Basse
285	32			15			Ambient Temperature Value		Amb Temp Value		Mesure Température Ambiante		Temp. Ambiante
290	32			15			None					None			Non				Non
291	32			15			Alarm					Alarm			Oui				Oui
292	32			15			Overall Voltage High			Overall Volt Hi		Tension Haute total			V Haute Total
293	32			15			Overall Voltage Low			Overall Volt Lo		Tension Basse total			V Basse Total
294	32			15			String Current High				String Curr Hi		Sur Courant Branche			Sur I Branche
295	32			15			String Current Low			String Curr Lo			Sous Courant Branche			Sous I Branche
296	32			15			Float Current High			Float Curr Hi			Courant Haut Floating			I Haut Float
297	32			15			Float Current Low				Float Curr Lo			Courant Bas Floating			I Bas Float
298	32			15			Ripple Current High			Ripple Curr Hi			Courant Ondulation Haut		I Ondul. Haut
299	32			15			Ripple Current Low			Ripple Curr Lo			Courant Ondulation Bas			I Ondul. Bas
#Test Resistances
300	32			15			Test Resistance 1				Test Resist 1				Test de Résistance Bloc 1	Test R Bloc 1
301	32			15			Test Resistance 2				Test Resist 2				Test de Résistance Bloc 2	Test R Bloc 2
302	32			15			Test Resistance 3				Test Resist 3				Test de Résistance Bloc 3	Test R Bloc 3
303	32			15			Test Resistance 4				Test Resist 4				Test de Résistance Bloc 4	Test R Bloc 4
304	32			15			Test Resistance 5				Test Resist 5				Test de Résistance Bloc 5	Test R Bloc 5
305	32			15			Test Resistance 6				Test Resist 6				Test de Résistance Bloc 6	Test R Bloc 6
307	32			15			Test Resistance 7				Test Resist 7				Test de Résistance Bloc 7	Test R Bloc 7
308	32			15			Test Resistance 8				Test Resist 8				Test de Résistance Bloc 8	Test R Bloc 8
309	32			15			Test Resistance 9				Test Resist 9				Test de Résistance Bloc 9	Test R Bloc 9
310	32			15			Test Resistance 10				Test Resist 10					Test de Résistance Bloc 10	Test R Bloc 10
311	32			15			Test Resistance 11				Test Resist 11					Test de Résistance Bloc 11	Test R Bloc 11
312	32			15			Test Resistance 12				Test Resist 12					Test de Résistance Bloc 12	Test R Bloc 12
313	32			15			Test Resistance 13				Test Resist 13					Test de Résistance Bloc 13	Test R Bloc 13
314	32			15			Test Resistance 14				Test Resist 14					Test de Résistance Bloc 14	Test R Bloc 14
315	32			15			Test Resistance 15				Test Resist 15					Test de Résistance Bloc 15	Test R Bloc 15
316	32			15			Test Resistance 16				Test Resist 16					Test de Résistance Bloc 16	Test R Bloc 16
317	32			15			Test Resistance 17				Test Resist 17					Test de Résistance Bloc 17	Test R Bloc 17
318	32			15			Test Resistance 18				Test Resist 18					Test de Résistance Bloc 18	Test R Bloc 18
319	32			15			Test Resistance 19				Test Resist 19					Test de Résistance Bloc 19	Test R Bloc 19
320	32			15			Test Resistance 20				Test Resist 20					Test de Résistance Bloc 20	Test R Bloc 20
321	32			15			Test Resistance 21				Test Resist 21					Test de Résistance Bloc 21	Test R Bloc 21
322	32			15			Test Resistance 22				Test Resist 22					Test de Résistance Bloc 22	Test R Bloc 22
323	32			15			Test Resistance 23				Test Resist 23					Test de Résistance Bloc 23	Test R Bloc 23
324	32			15			Test Resistance 24				Test Resist 24					Test de Résistance Bloc 24	Test R Bloc 24
325	32			15			Test Intercell 1				Test Intercel1		Resis1 Inter Cellule		Res1 InterCel
326	32			15			Test Intercell 2				Test Intercel2		Resis2 Inter Cellule		Res2 InterCel
327	32			15			Test Intercell 3				Test Intercel3		Resis3 Inter Cellule		Res3 InterCel
328	32			15			Test Intercell 4				Test Intercel4		Resis4 Inter Cellule		Res4 InterCel
329	32			15			Test Intercell 5				Test Intercel5		Resis5 Inter Cellule		Res5 InterCel
330	32			15			Test Intercell 6				Test Intercel6		Resis6 Inter Cellule		Res6 InterCel
331	32			15			Test Intercell 7				Test Intercel7		Resis7 Inter Cellule		Res7 InterCel
332	32			15			Test Intercell 8				Test Intercel8		Resis8 Inter Cellule		Res8 InterCel
333	32			15			Test Intercell 9				Test Intercel9		Resis9 Inter Cellule		Res9 InterCel
334	32			15			Test Intercell 10				Test Intercel10		Resis10 Inter Cellule		Res10 InterCel
335	32			15			Test Intercell 11				Test Intercel11		Resis11 Inter Cellule		Res11 InterCel
336	32			15			Test Intercell 12				Test Intercel12		Resis12 Inter Cellule		Res12 InterCel
337	32			15			Test Intercell 13				Test Intercel13		Resis13 Inter Cellule		Res13 InterCel
338	32			15			Test Intercell 14				Test Intercel14		Resis14 Inter Cellule		Res14 InterCel
339	32			15			Test Intercell 15				Test Intercel15		Resis15 Inter Cellule		Res15 InterCel
340	32			15			Test Intercell 16				Test Intercel16		Resis16 Inter Cellule		Res16 InterCel
341	32			15			Test Intercell 17				Test Intercel17		Resis17 Inter Cellule		Res17 InterCel
342	32			15			Test Intercell 18				Test Intercel18		Resis18 Inter Cellule		Res18 InterCel
343	32			15			Test Intercell 19				Test Intercel19		Resis19 Inter Cellule		Res19 InterCel
344	32			15			Test Intercell 20				Test Intercel20		Resis20 Inter Cellule		Res20 InterCel
345	32			15			Test Intercell 21				Test Intercel21		Resis21 Inter Cellule		Res21 InterCel
346	32			15			Test Intercell 22				Test Intercel22		Resis22 Inter Cellule		Res22 InterCel
347	32			15			Test Intercell 23				Test Intercel23		Resis23 Inter Cellule		Res23 InterCel
348	32			15			Test Intercell 24				Test Intercel24		Resis24 Inter Cellule		Res24 InterCel
349	32			15			Cell High Voltage Alarm				Cell HiVolt Alm			Tension de Cellule Haute		V Cellule Haute
350	32			15			Cell High Cell Temperature Alarm		Cell HiTemp Alm			Température de Cellule Haute		Temp Cel Haute
351	32			15			Cell High Resistance Alarm			Cell HiRes Alm			Résistance de Cellule Haute		R Cellule Haute
352	32			15			Cell High Intercell Resist Alarm		Inter HiRes Alm					R Jonction Inter Cellule Haute		R Jonct CelHaut
353	32			15			Cell High Ambient Temp Alarm			Cell HiAmb Alm					Alarme Température Haute Cellule		Al Temp HautCel

354	32			15			Temperature 1 Not Used				Temp1 Not Used			Sonde Tempér. bloc 1 non util.			Bloc1TempNoUtil
355	32			15			Temperature 2 Not Used				Temp2 Not Used			Sonde Tempér. bloc 2 non util.			Bloc2TempNoUtil
356	32			15			Temperature 3 Not Used				Temp3 Not Used			Sonde Tempér. bloc 3 non util.			Bloc3TempNoUtil
357	32			15			Temperature 4 Not Used				Temp4 Not Used			Sonde Tempér. bloc 4 non util.			Bloc4TempNoUtil
358	32			15			Temperature 5 Not Used				Temp5 Not Used			Sonde Tempér. bloc 5 non util.			Bloc5TempNoUtil
359	32			15			Temperature 6 Not Used				Temp6 Not Used			Sonde Tempér. bloc 6 non util.			Bloc6TempNoUtil
360	32			15			Temperature 7 Not Used				Temp7 Not Used			Sonde Tempér. bloc 7 non util.			Bloc7TempNoUtil
361	32			15			Temperature 8 Not Used				Temp8 Not Used			Sonde Tempér. bloc 8 non util.			Bloc8TempNoUtil
362	32			15			Temperature 9 Not Used				Temp9 Not Used		Sonde Tempér. bloc 9 non util.			Bloc9TempNoUtil
363	32			15			Temperature 10 Not Used				Temp10 Not Used		Sonde Tempér. bloc 10 non util.			Blo10TempNoUtil
364	32			15			Temperature 11 Not Used				Temp11 Not Used		Sonde Tempér. bloc 11 non util.			Blo11TempNoUtil
365	32			15			Temperature 12 Not Used				Temp12 Not Used		Sonde Tempér. bloc 12 non util.			Blo12TempNoUtil
366	32			15			Temperature 13 Not Used				Temp13 Not Used		Sonde Tempér. bloc 13 non util.			Blo13TempNoUtil
367	32			15			Temperature 14 Not Used				Temp14 Not Used		Sonde Tempér. bloc 14 non util.			Blo14TempNoUtil
368	32			15			Temperature 15 Not Used				Temp15 Not Used		Sonde Tempér. bloc 15 non util.			Blo15TempNoUtil
369	32			15			Temperature 16 Not Used				Temp16 Not Used		Sonde Tempér. bloc 16 non util.			Blo16TempNoUtil
370	32			15			Temperature 17 Not Used				Temp17 Not Used		Sonde Tempér. bloc 17 non util.			Blo17TempNoUtil
371	32			15			Temperature 18 Not Used				Temp18 Not Used		Sonde Tempér. bloc 18 non util.			Blo18TempNoUtil
372	32			15			Temperature 19 Not Used				Temp19 Not Used		Sonde Tempér. bloc 19 non util.			Blo19TempNoUtil
373	32			15			Temperature 20 Not Used				Temp20 Not Used		Sonde Tempér. bloc 20 non util.			Blo20TempNoUtil
374	32			15			Temperature 21 Not Used				Temp21 Not Used		Sonde Tempér. bloc 21 non util.			Blo21TempNoUtil
375	32			15			Temperature 22 Not Used				Temp22 Not Used		Sonde Tempér. bloc 22 non util.			Blo22TempNoUtil
376	32			15			Temperature 23 Not Used				Temp23 Not Used		Sonde Tempér. bloc 23 non util.			Blo23TempNoUtil
377	32			15			Temperature 24 Not Used				Temp24 Not Used		Sonde Tempér. bloc 24 non util.			Blo24TempNoUtil

﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Phase A Voltage				R			Tension phase A			R
2	32			15			Phase B Voltage		S			Tension phase B			S
3	32			15			Phase C Voltage		T			Tension phase C			T
4	32			15			Line AB Voltage		R			Tension AB			R-S
5	32			15			Line BC Voltage		S			Tension BC			S-T
6	32			15			Line CA Voltage		T			Tension CA			R-T
7	32			15			Phase A Current		Phase A Current		Courant phase A			Courant phase A	
8	32			15			Phase B Current		Phase B Current		Courant phase B			Courant phase B	
9	32			15			Phase C Current		Phase C Current			Courant phase C			Courant phase C	
10	32			15			AC Frequency		AC Frequency		Frequence			Frequence
11	32			15		Total Real Power		Total Real Pwr		Puissance réelle totale		P. Réelle total
12	32			15			Phase A Real Power		PH-A Real Power		Puissance réelle phase A	P. Réelle ph.A
13	32			15			Phase B Real Power		PH-B Real Power		Puissance réelle phase B	P. Réelle ph.B
14	32			15			Phase C Real Power		PH-C Real Power		Puissance réelle phase C	P. Réelle ph.C
15	32			15			Total Reactive Power		Total React Pwr			Puissance réactive totale	P. Réact totale
16	32			15			Phase A Reactive Power		PH-A React Pwr			Puissance réactive phase A	P. Réact ph A
17	32			15			Phase B Reactive Power		PH-B React Pwr			Puissance réactive phase B	P. Réact ph B
18	32			15			Phase C Reactive Power		PH-C React Pwr		Puissance réactive phase C	P. Réact ph C
19	32			15			Total Apparent Power		Total Appar Pwr		Puissance apparante totale	P. App totale
20	32			15			Phase A Apparent Power		PH-A Appar Pwr		Puissance apparante phase A	P. App phase A
21	32			15			Phase B Apparent Power		PH-B Appar Pwr		Puissance apparante phase B	P. App phase B
22	32			15			Phase C Apparent Power		PH-C Appar Pwr		Puissance apparante phase C	P. App phase C
23	32			15			Power Factor		Power Factor		Facteur de puissance		Facteur de P
24	32			15			Phase A Power Factor		PH-A Pwr Factor		Facteur de puissance phase A	Facteur P phA
25	32			15			Phase B Power Factor		PH-B Pwr Factor		Facteur de puissance phase B	Facteur P phB
26	32			15		Phase C Power Factor		PH-C Pwr Factor		Facteur de puissance phase C	Facteur P phC
27	32			15			Phase A Current Crest Factor		Ia Crest Factor		Facteur de crête phase A	Facteur crêteIa
28	32			15			Phase B Current Crest Factor		Ib Crest Factor		Facteur de crête phase B	Facteur crêteIb
29	32			15			Phase C Current Crest Factor		Ic Crest Factor		Facteur de crête phase C	Facteur crêteIc
30	32			15			Phase A Current THD		PH-A Curr THD		Courant THD phase A		Courant THD A
31	32			15			Phase B Current THD		PH-B Curr THD		Courant THD phase B		Courant THD B
32	32			15			Phase C Current THD		PH-C Curr THD		Courant THD phase C		Courant THD C
33	32			15			Phase A Voltage THD		PH-A Volt THD		Tension THD phase A		Tension THD A
34	32			15			Phase B Voltage THD		PH-B Volt THD		Tension THD phase B		Tension THD B
35	32			15			Phase C Voltage THD		PH-C Volt THD		Tension THD phase C		Tension THD C
36	32			15			Total Real Energy		TotalRealEnergy		Energie réelle totale		E réelle totale
37	32			15			Total Reactive Energy		TotalReacEnergy		Energie réactive totale		E react. totale
38	32			15			Total Apparent Energy		TotalAppaEnergy		Energie apparante totale	E app. totale
39	32			15			Ambient Temperature		Ambient Temp		Température ambiante		Temp. ambiante
40	32			15			Nominal Line Voltage			NominalLineVolt		Tension de ligne nominale	V ligne nominal
41	32			15			Nominal Phase Voltage			Nominal PH-Volt		Tension secteur nominale	VAC nominal
42	32			15			Nominal Frequency			Nominal Freq		Frequence nominale		F. nominale
43	32			15			Mains Failure Alarm Limit 1		Mains Fail Alm1		Seuil 1 alarme def. AC		Seuil 1 Alrm AC
44	32			15			Mains Failure Alarm Limit 2		Mains Fail Alm2		Seuil 2 alarme def. AC		Seuil 2 Alrm AC
45	32			15		Voltage Alarm Limit 1			Volt Alarm Lmt1		Seuil 1 alarme tension		Seuil 1 Al V
46	32			15			Voltage Alarm Limit 2			Volt Alarm Lmt2		Seuil 2 alarme tension		Seuil 2 Al V
47	32			15		Frequency Alarm Limit			Freq Alarm Lmt		Seuil alarme frequence		Seuil Al F
48	32			15			High Temperature Limit			High Temp Limit		Limite haute température	Limite Haute T
49	32			15			Low Temperature Limit			Low Temp Limit		Limite basse température	Limite Basse T
50	32			15			Supervision Fail			SupervisionFail	Défaut supervision		Déf supervision
51	32			15			Line AB Over Voltage 1			L-AB Over Volt1		Haute Tension ligne AB		Haute V AB
52	32			15			Line AB Over Voltage 2			L-AB Over Volt2		Très haute Tension ligne AB	Très haute V AB
53	32			15			Line AB Under Voltage 1			L-AB UnderVolt1		Basse Tension ligne AB		Basse V AB
54	32			15			Line AB Under Voltage 2			L-AB UnderVolt2		Très Basse Tension ligne AB	Très Basse V AB
55	32			15			Line BC Over Voltage 1			L-BC Over Volt1		Haute Tension ligne BC		Haute V BC
56	32			15			Line BC Over Voltage 2			L-BC Over Volt2		Très haute Tension ligne BC	Très haute V BC
57	32			15			Line BC Under Voltage 1			L-BC UnderVolt1		Basse Tension ligne BC		Basse V BC
58	32			15			Line BC Under Voltage 2			L-BC UnderVolt2		Très Basse Tension ligne BC	Très Basse V BC
59	32			15			Line CA Over Voltage 1			L-CA Over Volt1		Haute Tension ligne CA		Haute V CA
60	32			15			Line CA Over Voltage 2			L-CA Over Volt2			Très haute Tension ligne CA	Très haute V CA
61	32			15			Line CA Under Voltage 1			L-CA UnderVolt1			Basse Tension ligne CA		Basse V CA
62	32			15			Line CA Under Voltage 2			L-CA UnderVolt2		Très Basse Tension ligne CA	Très Basse V CA
63	32			15			Phase A Over Voltage 1			PH-A Over Volt1		Haute tension phase A		Haute V phA
64	32			15			Phase A Over Voltage 2			PH-A Over Volt2			Très haute tension phase A	Très haut V phA
65	32			15			Phase A Under Voltage 1			PH-A UnderVolt1			Basse tension phase A		Basse V phA
66	32			15			Phase A Under Voltage 2			PH-A UnderVolt2		Très basse tension phase A	Très bas V phA
67	32			15			Phase B Over Voltage 1			PH-B Over Volt1		Haute tension phase B		Haute V phB
68	32			15			Phase B Over Voltage 2			PH-B Over Volt2			Très haute tension phase B	Très haut V phB
69	32			15			Phase B Under Voltage 1			PH-B UnderVolt1		Basse tension phase B		Basse V phB
70	32			15			Phase B Under Voltage 2			PH-B UnderVolt2		Très basse tension phase B	Très bas V phB
71	32			15			Phase C Over Voltage 1			PH-C Over Volt1			Haute tension phase C		Haute V phC
72	32			15			Phase C Over Voltage 2			PH-C Over Volt2		Très haute tension phase C	Très haut V phC
73	32			15			Phase C Under Voltage 1			PH-C UnderVolt1			Basse tension phase C		Basse V phC
74	32			15			Phase C Under Voltage 2			PH-C UnderVolt2		Très basse tension phase C	Très bas V phC
75	32			15			Mains Failure				Mains Failure		Défaut secteur			Défaut secteur
76	32			15			Severe Mains Failure			SevereMainsFail	Défaut secteur sévère		Déf sévère sect
77	32			15			High Frequency				High Frequency		Fréquence haute			F haute
78	32			15			Low Frequency				Low Frequency		Fréquence basse			F basse
79	32			15			High Temperature			High Temp		Température haute		T haute
80	32			15			Low Temperature				Low Temperature		Température basse		T basse
81	32			15			Rectifier AC				Rectifier AC			Entrée AC redresseur		AC redresseur
82	32			15			Supervision Fail			SupervisionFail		Défaut supervision		Déf supervision
83	32			15			No					No			Non				Non
84	32			15			Yes					Yes			Oui				Oui
85	32			15			Phase A Mains Failure Counter		PH-A Fail Count		Cpt de defauts secteur phase A	Cpt def sec phA
86	32			15			Phase B Mains Failure Counter		PH-B Fail Count			Cpt de defauts secteur phase B	Cpt def sec phB
87	32			15			Phase C Mains Failure Counter		PH-C Fail Count		Cpt de defauts secteur phase C	Cpt def sec phC
88	32			15			Frequency Failure Counter		FreqFailCounter		Compteur de defauts frequence	Cpt defauts F
89	32			15			Reset Phase A Mains Fail Counter	RstPH-AFailCnt		Reset compteur def secteur phA	Rst def sec phA
90	32			15			Reset Phase B Mains Fail Counter	RstPH-BFailCnt		Reset compteur def secteur phB	Rst def sec phB
91	32			15			Reset Phase C Mains Fail Counter	RstPH-CFailCnt		Reset compteur def secteur phC	Rst def sec phC
92	32			15			Reset Frequency Fail Counter		RstFreqFailCnt		Reset compteur defaut frequence	Rst cpt Def F
93	32			15			Current Alarm Limit			Curr Alm Limit	Seuil alarme courant		Seuil Def I
94	32			15			Phase A High Current			PH-A High Curr		Surcourant phase A		Surcourant phA
95	32			15			Phase B High Current			PH-B High Curr		Surcourant phase B		Surcourant phB
96	32			15			Phase C High Current			PH-C High Curr		Surcourant phase C		Surcourant phC
97	32			15			Minimum Phase Voltage			Min Phase Volt		Tension Minimum Phase		V Min Phase
98	32			15			Maximum Phase Voltage			Max Phase Volt		Tension Maximum Phase		V Max Phase
99	32			15			Raw Data 1				Raw Data 1		Donnees 1			Donnees 1
100	32			15			Raw Data 2				Raw Data 2		Donnees 2			Donnees 2
101	32			15			Raw Data 3				Raw Data 3		Donnees 3			Donnees 3
102	32			15			Reference Voltage			Reference Volt		Tension de reference		V Ref
103	32			15			State					State			Etat				Etat
104	32			15			Off					Off			Arrêt				Arrêt
105	32			15			On					On			Marche				Marche
106	32			15			High Phase Voltage			High Ph-Volt		Tension Phase Haute		V PH Haute
107	32			15			Very High Phase Voltage		VHigh Ph-Volt		Tension Phase tres Haute	V PH tres Haute
108	32			15			Low Phase Voltage			Low Ph-Volt		Tension Phase Basse		V PH Basse
109	32			15			Very Low Phase Voltage			VLow Ph-Volt		Tension Phase tres Basse	V PH tres Basse
110	32			15			All Rectifiers Comm Fail		AllRectCommFail		Aucune Reponse Redresseur	Reponse Red
120		32			32			Input PhaseA Current		Input PhaseA Current			Phase d'entréeA Courant		Phase d'entréeA Courant
121		32			32			Input PhaseB Current		Input PhaseB Current			Phase d'entréeB Courant		Phase d'entréeB Courant
122		32			32			Input PhaseC Current		Input PhaseC Current			Phase d'entréeC Courant		Phase d'entréeC Courant

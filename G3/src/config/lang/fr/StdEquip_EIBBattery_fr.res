ï»?
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			EIB Battery			EIB Battery		EIB Batterie			EIB Batterie
2		32			15			Battery Current				Battery Current		Courant Batterie		I Batterie
3		32			15			Battery Voltage			Battery Voltage			Tension Batterie		V Batterie
4		32		15			Battery Rating(Ah)			Batt Rating(Ah)		évaluation Batterie (AH)	éval Bat (AH)
5		32			15			Battery Capacity (%)		Batt Cap (%)	Capacite Batterie (%)		Capa Bat (%)
6		32			15			Battery Current Limit Exceeded		Ov Bat Curr Lmt		Depassement Courant Limitation		Depass.I Limit.
7		32			15			Battery Over Current			Bat Over Curr		Sur Courant Batterie		Sur I Batt.
8		32			15			Battery Low Capacity			Low Batt Cap		Capacitee Basse		Capa. Basse
9		32			15			Yes				Yes			Oui				Oui
10		32			15			No				No			Non				Non
26		32			15			State				State			Etat				Etat
27		32			15			Battery Block 1 Voltage			Bat Block1 Volt		Tension Block 1			V Block 1
28		32			15			Battery Block 2 Voltage			Bat Block2 Volt		Tension Block 2			V Block 2
29		32			15			Battery Block 3 Voltage			Bat Block3 Volt		Tension Block 3			V Block 3
30		32			15			Battery Block 4 Voltage			Bat Block4 Volt		Tension Block 4			V Block 4
31		32			15			Battery Block 5 Voltage			Bat Block5 Volt		Tension Block 5			V Block 5
32		32			15			Battery Block 6 Voltage			Bat Block6 Volt		Tension Block 6			V Block 6
33		32			15			Battery Block 7 Voltage			Bat Block7 Volt		Tension Block 7			V Block 7
34		32			15			Battery Block 8 Voltage			Bat Block8 Volt		Tension Block 8			V Block 8
35		32			15			Battery Management		Batt Management		Gestion Batterie		Gest. Bat
36		32			15			Enabled				Enabled			Valide				Valide
37		32			15			Disabled				Disabled			Invalide				Invalide
38		32			15			Communication Fail				Comm Fail		Defaut de communication			Defaut COM
39		32			15			Shunt Full Current		Shunt Full Curr		Courant Shunt			I Shunt
40		32			15			Shunt Full Voltage		Shunt Full Volt		Tension Shunt			V Shunt
41		32			15			On				On			Marche				Marche
42		32			15			Off				Off			Off				Off	
43		32			15			Communication Fail				Comm Fail		Defaut de communication			Defaut COM
44		32			15			Battery Temperature Probe Number	BattTempPrbNum		Temp batterie Nombre Probe		TempBatNomProbe
87		32			15			No				No			Non				Non
91		32			15			Temperature 1				Temperature 1		Temperature 1		Temperature 1
92		32			15			Temperature 2				Temperature 2		Temperature 2		Temperature 2
93		32			15			Temperature 3				Temperature 3		Temperature 3		Temperature 3
94		32			15			Temperature 4				Temperature 4		Temperature 4		Temperature 4
95		32			15			Temperature 5				Temperature 5		Temperature 5		Temperature 5
96		32			15			Rated Capacity			Rated Capacity		Capacitenominal C10		CapaciteC10
97		32			15			Battery Temperature		Battery Temp		Temperature batterie		Temp. bat.
98		32			15			Battery Tempurature Sensor		Bat Temp Sensor		Capteur Temperature batterie	Capt.Temp. bat.
99		32			15			None				None			Aucun				Aucun
100		32			15			Temperature 1				Temperature 1		Temperature 1		Temperature 1
101		32			15			Temperature 2				Temperature 2		Temperature 2		Temperature 2
102		32			15			Temperature 3				Temperature 3		Temperature 3		Temperature 3
103		32			15			Temperature 4				Temperature 4		Temperature 4		Temperature 4
104		32			15			Temperature 5				Temperature 5		Temperature 5		Temperature 5
105		32			15			Temperature 6				Temperature 6		Temperature 6		Temperature 6
106		32			15			Temperature 7				Temperature 7		Temperature 7		Temperature 7
107		32			15			Temperature 8				Temperature 8		Temperature 8		Temperature 8
108		32			15			Temperature 9				Temperature 9		Temperature 9		Temperature 9
109		32			15			Temperature 10				Temperature 10		Temperature 10		Temperature 10
110		32			15			EIB1 Battery1				EIB1 Battery1		EIB1 Batterie1		EIB1 Batterie1		
111		32			15			EIB1 Battery2				EIB1 Battery2		EIB1 Batterie2		EIB1 Batterie2		
112		32			15			EIB1 Battery3				EIB1 Battery3		EIB1 Batterie3		EIB1 Batterie3		
113		32			15			EIB2 Battery1				EIB2 Battery1		EIB2 Batterie1		EIB2 Batterie1		
114		32			15			EIB2 Battery2				EIB2 Battery2		EIB2 Batterie2		EIB2 Batterie2		
115		32			15			EIB2 Battery3				EIB2 Battery3		EIB2 Batterie3		EIB2 Batterie3		
116		32			15			EIB3 Battery1				EIB3 Battery1		EIB3 Batterie1		EIB3 Batterie1		
117		32			15			EIB3 Battery2				EIB3 Battery2		EIB3 Batterie2		EIB3 Batterie2		
118		32			15			EIB3 Battery3				EIB3 Battery3		EIB3 Batterie3		EIB3 Batterie3		
119		32			15			EIB4 Battery1				EIB4 Battery1		EIB4 Batterie1		EIB4 Batterie1		
120		32			15			EIB4 Battery2				EIB4 Battery2		EIB4 Batterie2		EIB4 Batterie2		
121		32			15			EIB4 Battery3				EIB4 Battery3		EIB4 Batterie3		EIB4 Batterie3		

150		32			15			Battery 1				Batt 1			Batterie 1			Batt 1		
151		32			15			Battery 2				Batt 2			Batterie 2			Batt 2		
152		32			15			Battery 3				Batt 3			Batterie 3			Batt 3		



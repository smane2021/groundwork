#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Bus Bar Voltage				Bus Bar Voltage		Tension Systeme		Tension Systeme
2	32			15			Shunt 1					Shunt 1		Shunt 1					Shunt 1
3	32			15			Shunt 2					Shunt 2		Shunt 2					Shunt 2
4	32			15			Shunt 3					Shunt 3		Shunt 3					Shunt 3
5	32			15			Shunt 4					Shunt 4		Shunt 4					Shunt 4
6	32			15			Fuse 1					Fuse 1			Fusible 1			Fusible 1
7	32			15			Fuse 2					Fuse 2			Fusible 2			Fusible 2
8	32			15			Fuse 3					Fuse 3			Fusible 3			Fusible 3
9	32			15			Fuse 4					Fuse 4			Fusible 4			Fusible 4
10	32			15			Fuse 5					Fuse 5			Fusible 5			Fusible 5
11	32			15			Fuse 6					Fuse 6			Fusible 6			Fusible 6
12	32			15			Fuse 7					Fuse 7			Fusible 7			Fusible 7
13	32			15			Fuse 8					Fuse 8			Fusible 8			Fusible 8
14	32			15			Fuse 9					Fuse 9			Fusible 9			Fusible 9
15	32			15			Fuse 10					Fuse 10			Fusible 10			Fusible 10
16	32			15			Fuse 11					Fuse 11			Fusible 11			Fusible 11
17	32			15			Fuse 12					Fuse 12			Fusible 12			Fusible 12
18	32			15			Fuse 13					Fuse 13			Fusible 13			Fusible 13
19	32			15			Fuse 14					Fuse 14			Fusible 14			Fusible 14
20	32			15			Fuse 15					Fuse 15			Fusible 15			Fusible 15
21	32			15			Fuse 16					Fuse 16			Fusible 16			Fusible 16
22	32			15			Run Time				Run Time		TTemps de Fonctionnement			Temps Fonct.
23	32			15			LV Disconnect 1 Control			LVD 1 Control		Control LVD1			Control LVD1
24	32			15			LV Disconnect 2 Control			LVD 2 Control		Control LVD2			Control LVD2
25	32			15			LV Disconnect 1 Voltage			LVD 1 Voltage		Tension LVD1			Tension LVD1
26	32			15			LV Resconnect 1 Voltage			LVR 1 Voltage		Tension LVR1			Tension LVR1
27	32			15			LV Disconnect 2 Voltage			LVD 2 Voltage		Tension LVD2			Tension LVD2
28	32			15			LV Resconnect 2 Voltage			LVR 2 Voltage		Tension LVR2			Tension LVR2
29	32			15			On					On			Conectado			Conectado
30	32			15			Off					Off			Desconectado			Desconectado
31	32			15			Normal					Normal			Normal				Normal
32	32			15			Error					Error			Error				Error
33	32			15			On					On			Conectado			Conectado
34	32			15			Fuse 1 Alarm				Fuse 1 Alarm	Alarma Fusible 1		Alarma fus1
35	32			15			Fuse 2 Alarm				Fuse 2 Alarm	Alarma Fusible 2		Alarma fus2
36	32			15			Fuse 3 Alarm				Fuse 3 Alarm	Alarma Fusible 3		Alarma fus3
37	32			15			Fuse 4 Alarm				Fuse 4 Alarm	Alarma Fusible 4		Alarma fus4
38	32			15			Fuse 5 Alarm				Fuse 5 Alarm	Alarma Fusible 5		Alarma fus5
39	32			15			Fuse 6 Alarm				Fuse 6 Alarm	Alarma Fusible 6		Alarma fus6
40	32			15			Fuse 7 Alarm				Fuse 7 Alarm	Alarma Fusible 7		Alarma fus7
41	32			15			Fuse 8 Alarm				Fuse 8 Alarm	Alarma Fusible 8		Alarma fus8
42	32			15			Fuse 9 Alarm				Fuse 9 Alarm	Alarma Fusible 9		Alarma fus9
43	32			15			Fuse 10 Alarm				Fuse 10 Alarm		Alarma Fusible 10		Alarma fus10
44	32			15			Fuse 11 Alarm				Fuse 11 Alarm		Alarma Fusible 11		Alarma fus11
45	32			15			Fuse 12 Alarm				Fuse 12 Alarm		Alarma Fusible 12		Alarma fus12
46	32			15			Fuse 13 Alarm				Fuse 13 Alarm		Alarma Fusible 13		Alarma fus13
47	32			15			Fuse 14 Alarm				Fuse 14 Alarm		Alarma Fusible 14		Alarma fus14
48	32			15			Fuse 15 Alarm				Fuse 15 Alarm		Alarma Fusible 15		Alarma fus15
49	32			15			Fuse 16 Alarm				Fuse 16 Alarm		Alarma Fusible 16		Alarma fus16
50	32			15			HW Test Alarm				HW Test Alarm		Alarma prueba HW		Alarma test HW
51	32			15			SMDUP 15					SMDUP 15			SMDUP 15			SMDUP 15
52	32			15			Battery Fuse 1 Voltage			BattFuse 1 Volt			Tension fusible Batterie 1	Tens fus bat1
53	32			15			Battery Fuse 2 Voltage			BattFuse 2 Volt			Tension fusible Batterie 2	Tens fus bat2
54	32			15			Battery Fuse 3 Voltage			BattFuse 3 Volt			Tension fusible Batterie 3	Tens fus bat3
55	32			15			Battery Fuse 4 Voltage			BattFuse 4 Volt			Tension fusible Batterie 4	Tens fus bat4
56	32			15			Battery Fuse 1 Status			Batt Fuse 1				Estado fusible Batterie 1	Estado fus bat1
57	32			15			Battery Fuse 2 Status			Batt Fuse 2				Estado fusible Batterie 2	Estado fus bat2
58	32			15			Battery Fuse 3 Status			Batt Fuse 3				Estado fusible Batterie 3	Estado fus bat3
59	32			15			Battery Fuse 4 Status			Batt Fuse 4				Estado fusible Batterie 4	Estado fus bat4
60	32			15			On					On			Conectado			Conectado
61	32			15			Off					Off			Desconectado			Desconectado
62	32			15			Battery Fuse 1 Alarm			Batt Fuse 1 Alm	Alarma fusible Batterie 1	Alarma fus1 bat
63	32			15			Battery Fuse 2 Alarm			Batt Fuse 2 Alm	Alarma fusible Batterie 2	Alarma fus2 bat
64	32			15			Battery Fuse 3 Alarm			Batt Fuse 3 Alm	Alarma fusible Batterie 3	Alarma fus3 bat
65	32			15			Battery Fuse 4 Alarm			Batt Fuse 4 Alm	Alarma fusible Batterie 4	Alarma fus4 bat
66	32			15			All Load Current			All Load Curr		Corriente total carga		Total carga
67	32			15			Over Current Limit (Load)		Over Curr Limit		Valor de sobrecorriente		Sobrecorriente
68	32			15			Over Current (Load)			Over Current		Sobrecorriente			Sobrecorriente
69	32			15			LVD 1				LVD 1		LVD1			LVD1 
70	32			15			LVD 1 Mode				LVD 1 Mode		Modo LVD1			Modo LVD1
71	32			15			LVD 1 Reconnect Delay			LVD1 ReconDelay		Retardo recon LVD1		RetarRecon LVD1
72	32			15			LVD 2				LVD 2		LVD2			LVD2 
73	32			15			LVD 2 Mode				LVD 2 Mode		Modo LVD2			Modo LVD2
74	32			15			LVD 2 Reconnect Delay			LVD2 ReconDelay			Retardo recon LVD2		RetarRecon LVD2
75	32			15			LVD 1 Status				LVD 1 Status		Estado LVD1			Estado LVD1
76	32			15			LVD 2 Status				LVD 2 Status		Estado LVD2			Estado LVD2
77	32			15			Disabled				Disabled		Deshabilitado			Deshabilitado
78	32			15			Enabled					Enabled			Habilitado			Habilitado
79	32			15			Voltage				Voltage			Tension			Tension
80	32			15			Time					Time			tiempo			tiempo
81	32			15			Bus Bar Volt Alarm			Bus Bar Alarm		Alarma Barra Dist	Alarma bus Dist
82	32			15			Normal					Normal			Normal				Normal
83	32			15			Low					Low			Bajo				Bajo
84	32			15			High					High			Alto				Alto
85	32			15			Under Voltage				Under Voltage			Baja Tension			Baja Tension
86	32			15			Over Voltage				Over Voltage		Alta Tension			Alta Tension
87	32			15			Shunt 1 Current Alarm			Shunt 1 Alarm		Alarma corriente shunt1		Alarma shunt1
88	32			15			Shunt 2 Current Alarm			Shunt 2 Alarm		Alarma corriente shunt2		Alarma shunt2
89	32			15			Shunt 3 Current Alarm			Shunt 3 Alarm		Alarma corriente shunt3		Alarma shunt3
90	32			15			Shunt 4 Current Alarm			Shunt 4 Alarm		Alarma corriente shunt4		Alarma shunt4
91	32			15			Shunt 1 Over Current			Shunt 1 OverCur		Sobrecorriente en shunt1	Sobrecor shunt1
92	32			15			Shunt 2 Over Current			Shunt 2 OverCur		Sobrecorriente en shunt2	Sobrecor shunt2
93	32			15			Shunt 3 Over Current			Shunt 3 OverCur		Sobrecorriente en shunt3	Sobrecor shunt3
94	32			15			Shunt 4 Over Current			Shunt 4 OverCur		Sobrecorriente en shunt4	Sobrecor shunt4
95	32			15			Times of Communication Fail		Times Comm Fail	Interrupciones			Interrupciones
96	32			15			Existent				Existent		Existente			Existente
97	32			15			Not Existent				Not Existent		Inexistente			Inexistente
98	32			15			Very Low				Very Low		Muy bajo			Muy bajo
99	32			15			Very High				Very High		Muy alto			Muy alto
100	32			15			Switch					Switch			Interruptor			Interruptor
101	32			15			LVD 1 Fail				LVD 1 Fail		Fallo LVD1			Fallo LVD1
102	32			15			LVD 2 Fail				LVD 2 Fail		Fallo LVD2			Fallo LVD2
103	32			15			High Temperature Disconnect 1	HTD 1		Habilitar HTD1			Habilitar HTD1
104	32			15			High Temperature Disconnect 2	HTD 2		Habilitar HTD2			Habilitar HTD2
105	32			15			Battery LVD				Batt LVD		LVD de Batterie			LVD de Batterie
106	32			15			No Battery				No Battery		Sin Batterie			Sin Batterie
107	32			15			LVD 1					LVD 1			LVD1				LVD1
108	32			15			LVD 2					LVD 2			LVD2				LVD2
109	32			15			Batt Always On				Batt Always On		Batterie siempre conectada	Siempre con Bat
110	32			15			Barcode					Barcode			Barcode				Barcode
111	32			15			DC Over Voltage				DC Over Volt		SobreTension CC			SobreTension CC
112	32			15			DC Under Voltage			DC Under Volt		SubTension CC			SubTension CC
113	32			15			Over Current 1				Over Current 1		Sobrecorriente 1		Sobrecorrien 1
114	32			15			Over Current 2				Over Current 2		Sobrecorriente 2		Sobrecorrien 2
115	32			15			Over Current 3				Over Current 3		Sobrecorriente 3		Sobrecorrien 3
116	32			15			Over Current 4				Over Current 4		Sobrecorriente 4		Sobrecorrien 4
117	32			15			Existence State				Existence State		Existence State				Existence State
118	32			15			Communication Fail			Comm Fail	Defaut de comunication				Defaut de COM
119	32			15			Bus Voltage Status			Bus Status		Estado Bus Tension		Estado Bus V
120	32			15			Comm OK					Comm OK			Comm OK					Comm OK
121	32			15			All Batteries Comm Fail			All Comm Fail		Defaut Comm batteries		Def COM Batt
122	32			15			Communication Fail			Comm Fail	Defaut de comunication				Defaut de COM
123	32			15			Rated Capacity				Rated Capacity		Capacidad Estimada		Capacidad Est
124	32			15			Shunt 5				Shunt 5		Shunt 5				Shunt 5
125	32			15			Shunt 6				Shunt 6		Shunt 6				Shunt 6
126	32			15			Shunt 7				Shunt 7		Shunt 7				Shunt 7
127	32			15			Shunt 8				Shunt 8		Shunt 8				Shunt 8
128	32			15			Shunt 9				Shunt 9		Shunt 9				Shunt 9
129	32			15			Shunt 10			Shunt 10		Shunt 10			Shunt 10
130	32			15			Shunt 11			Shunt 11		Shunt 11			Shunt 11
131	32			15			Shunt 12			Shunt 12		Shunt 12			Shunt 12
132	32			15			Shunt 13			Shunt 13		Shunt 13			Shunt 13
133	32			15			Shunt 14			Shunt 14		Shunt 14			Shunt 14
134	32			15			Shunt 15			Shunt 15		Shunt 15			Shunt 15
135	32			15			Shunt 16			Shunt 16		Shunt 16			Shunt 16
136	32			15			Shunt 17			Shunt 17		Shunt 17			Shunt 17
137	32			15			Shunt 18			Shunt 18		Shunt 18			Shunt 18
138	32			15			Shunt 19			Shunt 19		Shunt 19			Shunt 19
139	32			15			Shunt 20			Shunt 20		Shunt 20			Shunt 20
140	32			15			Shunt 21			Shunt 21		Shunt 21			Shunt 21
141	32			15			Shunt 22			Shunt 22		Shunt 22			Shunt 22
142	32			15			Shunt 23			Shunt 23		Shunt 23			Shunt 23
143	32			15			Shunt 24			Shunt 24		Shunt 24			Shunt 24
144	32			15			Shunt 25			Shunt 25		Shunt 25			Shunt 25
145	32			15			Voltage 1				Voltage 1		Tension 1			Tension 1
146	32			15			Voltage 2				Voltage 2		Tension 2			Tension 2
147	32			15			Voltage 3				Voltage 3		Tension 3			Tension 3
148	32			15			Voltage 4				Voltage 4		Tension 4			Tension 4
149	32			15			Voltage 5				Voltage 5		Tension 5			Tension 5
150	32			15			Voltage 6				Voltage 6		Tension 6			Tension 6
151	32			15			Voltage 7				Voltage 7		Tension 7			Tension 7
152	32			15			Voltage 8				Voltage 8		Tension 8			Tension 8
153	32			15			Voltage 9				Voltage 9		Tension 9			Tension 9
154	32			15			Voltage 10				Voltage 10		Tension 10			Tension 10
155	32			15			Voltage 11				Voltage 11		Tension 11			Tension 11
156	32			15			Voltage 12				Voltage 12		Tension 12			Tension 12
157	32			15			Voltage 13				Voltage 13		Tension 13			Tension 13
158	32			15			Voltage 14				Voltage 14		Tension 14			Tension 14
159	32			15			Voltage 15				Voltage 15		Tension 15			Tension 15
160	32			15			Voltage 16				Voltage 16		Tension 16			Tension 16
161	32			15			Voltage 17				Voltage 17		Tension 17			Tension 17
162	32			15			Voltage 18				Voltage 18		Tension 18			Tension 18
163	32			15			Voltage 19				Voltage 19		Tension 19			Tension 19
164	32			15			Voltage 20				Voltage 20		Tension 20			Tension 20
165	32			15			Voltage 21				Voltage 21		Tension 21			Tension 21
166	32			15			Voltage 22				Voltage 22		Tension 22			Tension 22
167	32			15			Voltage 23				Voltage 23		Tension 23			Tension 23
168	32			15			Voltage 24				Voltage 24		Tension 24			Tension 24
169	32			15			Voltage 25				Voltage 25		Tension 25			Tension 25

170	32			15			Current1 High 1 Current		Curr 1 Hi 1		Alta1 Corriente 1		Alta1 Corr1
171	32			15			Current1 High 2 Current		Curr 1 Hi 2		Alta2 Corriente 1		Alta2 Corr1
172	32			15			Current2 High 1 Current			Curr 2 Hi 1		Alta1 Corriente 2		Alta1 Corr2
173	32			15			Current2 High 2 Current			Curr 2 Hi 2		Alta2 Corriente 2		Alta2 Corr2
174	32			15			Current3 High 1 Current			Curr 3 Hi 1		Alta1 Corriente 3		Alta1 Corr3
175	32			15			Current3 High 2 Current			Curr 3 Hi 2		Alta2 Corriente 3		Alta2 Corr3
176	32			15			Current4 High 1 Current			Curr 4 Hi 1		Alta1 Corriente 4		Alta1 Corr4
177	32			15			Current4 High 2 Current			Curr 4 Hi 2		Alta2 Corriente 4		Alta2 Corr4
178	32			15			Current5 High 1 Current			Curr 5 Hi 1		Alta1 Corriente 5		Alta1 Corr5
179	32			15			Current5 High 2 Current			Curr 5 Hi 2		Alta2 Corriente 5		Alta2 Corr5
180	32			15			Current6 High 1 Current			Curr 6 Hi 1		Alta1 Corriente 6		Alta1 Corr6
181	32			15			Current6 High 2 Current			Curr 6 Hi 2		Alta2 Corriente 6		Alta2 Corr6
182	32			15			Current7 High 1 Current			Curr 7 Hi 1		Alta1 Corriente 7		Alta1 Corr7
183	32			15			Current7 High 2 Current			Curr 7 Hi 2		Alta2 Corriente 7		Alta2 Corr7
184	32			15			Current8 High 1 Current			Curr 8 Hi 1		Alta1 Corriente 8		Alta1 Corr8
185	32			15			Current8 High 2 Current			Curr 8 Hi 2		Alta2 Corriente 8		Alta2 Corr8
186	32			15			Current9 High 1 Current			Curr 9 Hi 1		Alta1 Corriente 9		Alta1 Corr9
187	32			15			Current9 High 2 Current			Curr 9 Hi 2		Alta2 Corriente 9		Alta2 Corr9
188	32			15			Current10 High 1 Current		Curr 10 Hi 1	Alta1 Corriente 10		Alta1 Corr10	
189	32			15			Current10 High 2 Current		Curr 10 Hi 2	Alta2 Corriente 10		Alta2 Corr10	
190	32			15			Current11 High 1 Current		Curr 11 Hi 1		Alta1 Corriente 11		Alta1 Corr11		
191	32			15			Current11 High 2 Current		Curr 11 Hi 2		Alta2 Corriente 11		Alta2 Corr11		
192	32			15			Current12 High 1 Current		Curr 12 Hi 1		Alta1 Corriente 12		Alta1 Corr12		
193	32			15			Current12 High 2 Current		Curr 12 Hi 2		Alta2 Corriente 12		Alta2 Corr12		
194	32			15			Current13 High 1 Current		Curr 13 Hi 1		Alta1 Corriente 13		Alta1 Corr13		
195	32			15			Current13 High 2 Current		Curr 13 Hi 2		Alta2 Corriente 13		Alta2 Corr13		
196	32			15			Current14 High 1 Current		Curr 14 Hi 1		Alta1 Corriente 14		Alta1 Corr14		
197	32			15			Current14 High 2 Current		Curr 14 Hi 2		Alta2 Corriente 14		Alta2 Corr14		
198	32			15			Current15 High 1 Current		Curr 15 Hi 1		Alta1 Corriente 15		Alta1 Corr15		
199	32			15			Current15 High 2 Current		Curr 15 Hi 2		Alta2 Corriente 15		Alta2 Corr15		
200	32			15			Current16 High 1 Current		Curr 16 Hi 1		Alta1 Corriente 16		Alta1 Corr16		
201	32			15			Current16 High 2 Current		Curr 16 Hi 2		Alta2 Corriente 16		Alta2 Corr16		
202	32			15			Current17 High 1 Current		Curr 17 Hi 1		Alta1 Corriente 17		Alta1 Corr17		
203	32			15			Current17 High 2 Current		Curr 17 Hi 2		Alta2 Corriente 17		Alta2 Corr17		
204	32			15			Current18 High 1 Current		Curr 18 Hi 1		Alta1 Corriente 18		Alta1 Corr18		
205	32			15			Current18 High 2 Current		Curr 18 Hi 2		Alta2 Corriente 18		Alta2 Corr18		
206	32			15			Current19 High 1 Current		Curr 19 Hi 1		Alta1 Corriente 19		Alta1 Corr19		
207	32			15			Current19 High 2 Current		Curr 19 Hi 2		Alta2 Corriente 19		Alta2 Corr19		
208	32			15			Current20 High 1 Current		Curr 20 Hi 1		Alta1 Corriente 20		Alta1 Corr20		
209	32			15			Current20 High 2 Current		Curr 20 Hi 2		Alta2 Corriente 20		Alta2 Corr20		
210	32			15			Current21 High 1 Current		Curr 21 Hi 1		Alta1 Corriente 21		Alta1 Corr21		
211	32			15			Current21 High 2 Current		Curr 21 Hi 2		Alta2 Corriente 21		Alta2 Corr21		
212	32			15			Current22 High 1 Current		Curr 22 Hi 1		Alta1 Corriente 22		Alta1 Corr22		
213	32			15			Current22 High 2 Current		Curr 22 Hi 2		Alta2 Corriente 22		Alta2 Corr22		
214	32			15			Current23 High 1 Current		Curr 23 Hi 1		Alta1 Corriente 23		Alta1 Corr23		
215	32			15			Current23 High 2 Current		Curr 23 Hi 2		Alta2 Corriente 23		Alta2 Corr23		
216	32			15			Current24 High 1 Current		Curr 24 Hi 1		Alta1 Corriente 24		Alta1 Corr24		
217	32			15			Current24 High 2 Current		Curr 24 Hi 2		Alta2 Corriente 24		Alta2 Corr24		
218	32			15			Current25 High 1 Current		Curr 25 Hi 1		Alta1 Corriente 25		Alta1 Corr25		
219	32			15			Current25 High 2 Current		Curr 25 Hi 2		Alta2 Corriente 25		Alta2 Corr25		

220	32			15			Current1 High 1 Current Limit		Curr1 Hi 1 Lmt	Courant1 High 1 Limite Courant1		Cour1 Hi1 Lmt	
221	32			15			Current1 High 2 Current Limit		Curr1 Hi 2 Lmt	Courant1 High 2 Limite Courant1		Cour1 Hi2 Lmt	
222	32			15			Current2 High 1 Current Limit		Curr2 Hi 1 Lmt	Courant2 High 1 Limite Courant1		Cour2 Hi1 Lmt	
223	32			15			Current2 High 2 Current Limit		Curr2 Hi 2 Lmt	Courant2 High 2 Limite Courant1		Cour2 Hi2 Lmt	
224	32			15			Current3 High 1 Current Limit		Curr3 Hi 1 Lmt	Courant3 High 1 Limite Courant1		Cour3 Hi1 Lmt	
225	32			15			Current3 High 2 Current Limit		Curr3 Hi 2 Lmt	Courant3 High 2 Limite Courant1		Cour3 Hi2 Lmt	
226	32			15			Current4 High 1 Current Limit		Curr4 Hi 1 Lmt	Courant4 High 1 Limite Courant1		Cour4 Hi1 Lmt	
227	32			15			Current4 High 2 Current Limit		Curr4 Hi 2 Lmt	Courant4 High 2 Limite Courant1		Cour4 Hi2 Lmt	
228	32			15			Current5 High 1 Current Limit		Curr5 Hi 1 Lmt	Courant5 High 1 Limite Courant1		Cour5 Hi1 Lmt	
229	32			15			Current5 High 2 Current Limit		Curr5 Hi 2 Lmt	Courant5 High 2 Limite Courant1		Cour5 Hi2 Lmt	
230	32			15			Current6 High 1 Current Limit		Curr6 Hi 1 Lmt	Courant6 High 1 Limite Courant1		Cour6 Hi1 Lmt	
231	32			15			Current6 High 2 Current Limit		Curr6 Hi 2 Lmt	Courant6 High 2 Limite Courant1		Cour6 Hi2 Lmt	
232	32			15			Current7 High 1 Current Limit		Curr7 Hi 1 Lmt	Courant7 High 1 Limite Courant1		Cour7 Hi1 Lmt	
233	32			15			Current7 High 2 Current Limit		Curr7 Hi 2 Lmt	Courant7 High 2 Limite Courant1		Cour7 Hi2 Lmt	
234	32			15			Current8 High 1 Current Limit		Curr8 Hi 1 Lmt	Courant8 High 1 Limite Courant1		Cour8 Hi1 Lmt	
235	32			15			Current8 High 2 Current Limit		Curr8 Hi 2 Lmt	Courant8 High 2 Limite Courant1		Cour8 Hi2 Lmt	
236	32			15			Current9 High 1 Current Limit		Curr9 Hi 1 Lmt	Courant9 High 1 Limite Courant1		Cour9 Hi1 Lmt	
237	32			15			Current9 High 2 Current Limit		Curr9 Hi 2 Lmt	Courant9 High 2 Limite Courant1		Cour9 Hi2 Lmt	
238	32			15			Current10 High 1 Current Limit		Curr10 Hi 1 Lmt		Courant10 High 1 Limite Courant1		Cour10 Hi1 Lmt	
239	32			15			Current10 High 2 Current Limit		Curr10 Hi 2 Lmt		Courant10 High 2 Limite Courant1		Cour10 Hi2 Lmt
240	32			15			Current11 High 1 Current Limit		Curr11 Hi 1 Lmt		Courant11 High 1 Limite Courant1		Cour11 Hi1 Lmt		
241	32			15			Current11 High 2 Current Limit		Curr11 Hi 2 Lmt		Courant11 High 2 Limite Courant1		Cour11 Hi2 Lmt
242	32			15			Current12 High 1 Current Limit		Curr12 Hi 1 Lmt		Courant12 High 1 Limite Courant1		Cour12 Hi1 Lmt
243	32			15			Current12 High 2 Current Limit		Curr12 Hi 2 Lmt		Courant12 High 2 Limite Courant1		Cour12 Hi2 Lmt
244	32			15			Current13 High 1 Current Limit		Curr13 Hi 1 Lmt		Courant13 High 1 Limite Courant1		Cour13 Hi1 Lmt
245	32			15			Current13 High 2 Current Limit		Curr13 Hi 2 Lmt		Courant13 High 2 Limite Courant1		Cour13 Hi2 Lmt
246	32			15			Current14 High 1 Current Limit		Curr14 Hi 1 Lmt		Courant14 High 1 Limite Courant1		Cour14 Hi1 Lmt
247	32			15			Current14 High 2 Current Limit		Curr14 Hi 2 Lmt		Courant14 High 2 Limite Courant1		Cour14 Hi2 Lmt
248	32			15			Current15 High 1 Current Limit		Curr15 Hi 1 Lmt		Courant15 High 1 Limite Courant1		Cour15 Hi1 Lmt
249	32			15			Current15 High 2 Current Limit		Curr15 Hi 2 Lmt		Courant15 High 2 Limite Courant1		Cour15 Hi2 Lmt
250	32			15			Current16 High 1 Current Limit		Curr16 Hi 1 Lmt		Courant16 High 1 Limite Courant1		Cour16 Hi1 Lmt
251	32			15			Current16 High 2 Current Limit		Curr16 Hi 2 Lmt		Courant16 High 2 Limite Courant1		Cour16 Hi2 Lmt
252	32			15			Current17 High 1 Current Limit		Curr17 Hi 1 Lmt		Courant17 High 1 Limite Courant1		Cour17 Hi1 Lmt
253	32			15			Current17 High 2 Current Limit		Curr17 Hi 2 Lmt		Courant17 High 2 Limite Courant1		Cour17 Hi2 Lmt
254	32			15			Current18 High 1 Current Limit		Curr18 Hi 1 Lmt		Courant18 High 1 Limite Courant1		Cour18 Hi1 Lmt
255	32			15			Current18 High 2 Current Limit		Curr18 Hi 2 Lmt		Courant18 High 2 Limite Courant1		Cour18 Hi2 Lmt
256	32			15			Current19 High 1 Current Limit		Curr19 Hi 1 Lmt		Courant19 High 1 Limite Courant1		Cour19 Hi1 Lmt
257	32			15			Current19 High 2 Current Limit		Curr19 Hi 2 Lmt		Courant19 High 2 Limite Courant1		Cour19 Hi2 Lmt
258	32			15			Current20 High 1 Current Limit		Curr20 Hi 1 Lmt		Courant20 High 1 Limite Courant1		Cour20 Hi1 Lmt		
259	32			15			Current20 High 2 Current Limit		Curr20 Hi 2 Lmt		Courant20 High 2 Limite Courant1		Cour20 Hi2 Lmt		
260	32			15			Current21 High 1 Current Limit		Curr21 Hi 1 Lmt		Courant21 High 1 Limite Courant1		Cour21 Hi1 Lmt		
261	32			15			Current21 High 2 Current Limit		Curr21 Hi 2 Lmt		Courant21 High 2 Limite Courant1		Cour21 Hi2 Lmt		
262	32			15			Current22 High 1 Current Limit		Curr22 Hi 1 Lmt		Courant22 High 1 Limite Courant1		Cour22 Hi1 Lmt		
263	32			15			Current22 High 2 Current Limit		Curr22 Hi 2 Lmt		Courant22 High 2 Limite Courant1		Cour22 Hi2 Lmt		
264	32			15			Current23 High 1 Current Limit		Curr23 Hi 1 Lmt		Courant23 High 1 Limite Courant1		Cour23 Hi1 Lmt		
265	32			15			Current23 High 2 Current Limit		Curr23 Hi 2 Lmt		Courant23 High 2 Limite Courant1		Cour23 Hi2 Lmt		
266	32			15			Current24 High 1 Current Limit		Curr24 Hi 1 Lmt		Courant24 High 1 Limite Courant1		Cour24 Hi1 Lmt		
267	32			15			Current24 High 2 Current Limit		Curr24 Hi 2 Lmt		Courant24 High 2 Limite Courant1		Cour24 Hi2 Lmt		
268	32			15			Current25 High 1 Current Limit		Curr25 Hi 1 Lmt		Courant25 High 1 Limite Courant1		Cour25 Hi1 Lmt		
269	32			15			Current25 High 2 Current Limit		Curr25 Hi 2 Lmt		Courant25 High 2 Limite Courant1		Cour25 Hi2 Lmt		

270	32			15			Current1 Break Value				Curr1 Brk Val		Calibre Protection Bat 1	Cal.Prot.Bat.1
271	32			15			Current2 Break Value				Curr2 Brk Val		Calibre Protection Bat 2	Cal.Prot.Bat.2
272	32			15			Current3 Break Value				Curr3 Brk Val		Calibre Protection Bat 3	Cal.Prot.Bat.3
273	32			15			Current4 Break Value				Curr4 Brk Val		Calibre Protection Bat 4	Cal.Prot.Bat.4
274	32			15			Current5 Break Value				Curr5 Brk Val		Calibre Protection Bat 5	Cal.Prot.Bat.5
275	32			15			Current6 Break Value				Curr6 Brk Val		Calibre Protection Bat 6	Cal.Prot.Bat.6
276	32			15			Current7 Break Value				Curr7 Brk Val		Calibre Protection Bat 7	Cal.Prot.Bat.7
277	32			15			Current8 Break Value				Curr8 Brk Val		Calibre Protection Bat 8	Cal.Prot.Bat.8
278	32			15			Current9 Break Value				Curr9 Brk Val		Calibre Protection Bat 9	Cal.Prot.Bat.9
279	32			15			Current10 Break Value				Curr10 Brk Val		Calibre Protection Bat 10	Cal.Prot.Bat.10
280	32			15			Current11 Break Value				Curr11 Brk Val		Calibre Protection Bat 11	Cal.Prot.Bat.11
281	32			15			Current12 Break Value				Curr12 Brk Val		Calibre Protection Bat 12	Cal.Prot.Bat.12
282	32			15			Current13 Break Value				Curr13 Brk Val		Calibre Protection Bat 13	Cal.Prot.Bat.13
283	32			15			Current14 Break Value				Curr14 Brk Val		Calibre Protection Bat 14	Cal.Prot.Bat.14
284	32			15			Current15 Break Value				Curr15 Brk Val		Calibre Protection Bat 15	Cal.Prot.Bat.15
285	32			15			Current16 Break Value				Curr16 Brk Val		Calibre Protection Bat 16	Cal.Prot.Bat.16
286	32			15			Current17 Break Value				Curr17 Brk Val		Calibre Protection Bat 17	Cal.Prot.Bat.17
287	32			15			Current18 Break Value				Curr18 Brk Val		Calibre Protection Bat 18	Cal.Prot.Bat.18
288	32			15			Current19 Break Value				Curr19 Brk Val		Calibre Protection Bat 19	Cal.Prot.Bat.19
289	32			15			Current20 Break Value				Curr20 Brk Val		Calibre Protection Bat 20	Cal.Prot.Bat.20
290	32			15			Current21 Break Value				Curr21 Brk Val		Calibre Protection Bat 21	Cal.Prot.Bat.21
291	32			15			Current22 Break Value				Curr22 Brk Val		Calibre Protection Bat 22	Cal.Prot.Bat.22
292	32			15			Current23 Break Value				Curr23 Brk Val		Calibre Protection Bat 23	Cal.Prot.Bat.23
293	32			15			Current24 Break Value				Curr24 Brk Val		Calibre Protection Bat 24	Cal.Prot.Bat.24
294	32			15			Current25 Break Value				Curr25 Brk Val		Calibre Protection Bat 25	Cal.Prot.Bat.25

295	32			15			Shunt 1 Voltage					Shunt1 Voltage			Tension Shunt 1		V Shunt 1
296	32			15			Shunt 1 Current					Shunt1 Current			Courant Shunt 1		I Shunt 1
297	32			15			Shunt 2 Voltage					Shunt2 Voltage			Tension Shunt 2		V Shunt 2
298	32			15			Shunt 2 Current					Shunt2 Current			Courant Shunt 2		I Shunt 2
299	32			15			Shunt 3 Voltage					Shunt3 Voltage			Tension Shunt 3		V Shunt 3
300	32			15			Shunt 3 Current					Shunt3 Current			Courant Shunt 3		I Shunt 3
301	32			15			Shunt 4 Voltage					Shunt4 Voltage			Tension Shunt 4		V Shunt 4
302	32			15			Shunt 4 Current					Shunt4 Current			Courant Shunt 4		I Shunt 4
303	32			15			Shunt 5 Voltage					Shunt5 Voltage			Tension Shunt 5		V Shunt 5
304	32			15			Shunt 5 Current					Shunt5 Current			Courant Shunt 5		I Shunt 5
305	32			15			Shunt 6 Voltage					Shunt6 Voltage			Tension Shunt 6		V Shunt 6
306	32			15			Shunt 6 Current					Shunt6 Current			Courant Shunt 6		I Shunt 6
307	32			15			Shunt 7 Voltage					Shunt7 Voltage			Tension Shunt 7		V Shunt 7
308	32			15			Shunt 7 Current					Shunt7 Current			Courant Shunt 7		I Shunt 7
309	32			15			Shunt 8 Voltage					Shunt8 Voltage			Tension Shunt 8		V Shunt 8
310	32			15			Shunt 8 Current					Shunt8 Current			Courant Shunt 8		I Shunt 8
311	32			15			Shunt 9 Voltage					Shunt9 Voltage			Tension Shunt 9		V Shunt 9
312	32			15			Shunt 9 Current					Shunt9 Current			Courant Shunt 9		I Shunt 9
313	32			15			Shunt 10 Voltage				Shunt10 Voltage			Tension Shunt 10	V Shunt 10
314	32			15			Shunt 10 Current				Shunt10 Current			Courant Shunt 10	I Shunt 10
315	32			15			Shunt 11 Voltage				Shunt11 Voltage			Tension Shunt 11	V Shunt 11
316	32			15			Shunt 11 Current				Shunt11 Current			Courant Shunt 11	I Shunt 11
317	32			15			Shunt 12 Voltage				Shunt12 Voltage			Tension Shunt 12	V Shunt 12
318	32			15			Shunt 12 Current				Shunt12 Current			Courant Shunt 12	I Shunt 12
319	32			15			Shunt 13 Voltage				Shunt13 Voltage			Tension Shunt 13	V Shunt 13
320	32			15			Shunt 13 Current				Shunt13 Current			Courant Shunt 13	I Shunt 13
321	32			15			Shunt 14 Voltage				Shunt14 Voltage			Tension Shunt 14	V Shunt 14
322	32			15			Shunt 14 Current				Shunt14 Current			Courant Shunt 14	I Shunt 14
323	32			15			Shunt 15 Voltage				Shunt15 Voltage			Tension Shunt 15	V Shunt 15
324	32			15			Shunt 15 Current				Shunt15 Current			Courant Shunt 15	I Shunt 15
325	32			15			Shunt 16 Voltage				Shunt16 Voltage			Tension Shunt 16	V Shunt 16
326	32			15			Shunt 16 Current				Shunt16 Current			Courant Shunt 16	I Shunt 16
327	32			15			Shunt 17 Voltage				Shunt17 Voltage			Tension Shunt 17	V Shunt 17
328	32			15			Shunt 17 Current				Shunt17 Current			Courant Shunt 17	I Shunt 17
329	32			15			Shunt 18 Voltage				Shunt18 Voltage			Tension Shunt 18	V Shunt 18
330	32			15			Shunt 18 Current				Shunt18 Current			Courant Shunt 18	I Shunt 18
331	32			15			Shunt 19 Voltage				Shunt19 Voltage			Tension Shunt 19	V Shunt 19
332	32			15			Shunt 19 Current				Shunt19 Current			Courant Shunt 19	I Shunt 19
333	32			15			Shunt 20 Voltage				Shunt20 Voltage			Tension Shunt 20	V Shunt 20
334	32			15			Shunt 20 Current				Shunt20 Current			Courant Shunt 20	I Shunt 20
335	32			15			Shunt 21 Voltage				Shunt21 Voltage			Tension Shunt 21	V Shunt 21
336	32			15			Shunt 21 Current				Shunt21 Current			Courant Shunt 21	I Shunt 21
337	32			15			Shunt 22 Voltage				Shunt22 Voltage			Tension Shunt 22	V Shunt 22
338	32			15			Shunt 22 Current				Shunt22 Current			Courant Shunt 22	I Shunt 22
339	32			15			Shunt 23 Voltage				Shunt23 Voltage			Tension Shunt 23	V Shunt 23
340	32			15			Shunt 23 Current				Shunt23 Current			Courant Shunt 23	I Shunt 23
341	32			15			Shunt 24 Voltage				Shunt24 Voltage			Tension Shunt 24	V Shunt 24
342	32			15			Shunt 24 Current				Shunt24 Current			Courant Shunt 24	I Shunt 24
343	32			15			Shunt 25 Voltage				Shunt25 Voltage			Tension Shunt 25	V Shunt 25
344	32			15			Shunt 25 Current				Shunt25 Current			Courant Shunt 25	I Shunt 25

345	32			15			Shunt Size Settable				Shunt Settable			Configuration Shunt	Config. Shunt
346	32			15			By Software					By Software			Par Software		Par Software
347	32			15			By Dip-Switch					By Dip-Switch			Par Dip switch		Par Dip switch
348	32			15			Not Supported					Not Supported			Pas Supporte		Pas Supporte
349	32			15			Shunt Coefficient Conflict			Shunt Conflict		Conflit Param Shunt		Conflit Shunt
350	32			15			Disabled					Disabled			Deshabilitado			Deshabilitado				
351	32			15			Enable						Enable				Habilitado			Habilitado				
352	32			15			Shunt 1						Shunt 1					Shunt 1						Shunt 1	
353	32			15			Shunt 2						Shunt 2					Shunt 2						Shunt 2	
354	32			15			Shunt 3						Shunt 3					Shunt 3						Shunt 3	
355	32			15			Shunt 4						Shunt 4					Shunt 4						Shunt 4	
356	32			15			Shunt 5						Shunt 5					Shunt 5						Shunt 5	
357	32			15			Shunt 6						Shunt 6					Shunt 6						Shunt 6	
358	32			15			Shunt 7						Shunt 7					Shunt 7						Shunt 7	
359	32			15			Shunt 8						Shunt 8					Shunt 8						Shunt 8	
360	32			15			Shunt 9						Shunt 9					Shunt 9						Shunt 9	
361	32			15			Shunt 10					Shunt 10				Shunt 10					Shunt 10
362	32			15			Shunt 11					Shunt 11				Shunt 11					Shunt 11
363	32			15			Shunt 12					Shunt 12				Shunt 12					Shunt 12
364	32			15			Shunt 13					Shunt 13				Shunt 13					Shunt 13
365	32			15			Shunt 14					Shunt 14				Shunt 14					Shunt 14
366	32			15			Shunt 15					Shunt 15				Shunt 15					Shunt 15
367	32			15			Shunt 16					Shunt 16				Shunt 16					Shunt 16
368	32			15			Shunt 17					Shunt 17				Shunt 17					Shunt 17
369	32			15			Shunt 18					Shunt 18				Shunt 18					Shunt 18
370	32			15			Shunt 19					Shunt 19				Shunt 19					Shunt 19
371	32			15			Shunt 20					Shunt 20				Shunt 20					Shunt 20
372	32			15			Shunt 21					Shunt 21				Shunt 21					Shunt 21
373	32			15			Shunt 22					Shunt 22				Shunt 22					Shunt 22
374	32			15			Shunt 23					Shunt 23				Shunt 23					Shunt 23
375	32			15			Shunt 24					Shunt 24				Shunt 24					Shunt 24
376	32			15			Shunt 25					Shunt 25				Shunt 25					Shunt 25
377	32			15			Not Used				Not Used			non utilis��				non utilis��
378	32			15			General					General				g��n��ral				g��n��ral
379	32			15			Load					Load				charge				charge
380	32			15			Load 1					Load 1		charge 1			charge 1
381	32			15			Load 2					Load 2		charge 2			charge 2
382	32			15			Load 3					Load 3		charge 3			charge 3
383	32			15			Load 4					Load 4		charge 4			charge 4
384	32			15			Load 5				Load 5		charge 5			charge 5
385	32			15			Load 6				Load 6		charge 6			charge 6
386	32			15			Load 7				Load 7		charge 7			charge 7
387	32			15			Load 8				Load 8		charge 8			charge 8
388	32			15			Load 9				Load 9		charge 9			charge 9
389	32			15			Load 10			Load 10		charge 10			charge 10
390	32			15			Load 11			Load 11		charge 11			charge 11
391	32			15			Load 12			Load 12		charge 12			charge 12
392	32			15			Load 13			Load 13		charge 13			charge 13
393	32			15			Load 14			Load 14		charge 14			charge 14
394	32			15			Load 15			Load 15		charge 15			charge 15
395	32			15			Load 16			Load 16		charge 16			charge 16
396	32			15			Load 17			Load 17		charge 17			charge 17
397	32			15			Load 18			Load 18		charge 18			charge 18
398	32			15			Load 19			Load 19		charge 19			charge 19
399	32			15			Load 20			Load 20		charge 20			charge 20
400	32			15			Load 21			Load 21		charge 21			charge 21
401	32			15			Load 22			Load 22		charge 22			charge 22
402	32			15			Load 23			Load 23		charge 23			charge 23
403	32			15			Load 24			Load 24		charge 24			charge 24
404	32			15			Load 25			Load 25		charge 25			charge 25


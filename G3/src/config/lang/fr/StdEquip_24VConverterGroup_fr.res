﻿#
#  Locale language support:French

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			24V Converter Group		24V Conv Group		Groupe convertisseurs 24V	Group CV 24V
19		32			15			Shunt 3 Rated Current		Shunt 3 Current		Calibre shunt (I)		Cal Shunt (I)
21		32			15			Shunt 3 Rated Voltage		Shunt 3 Voltage		Calibre de shunt (U)		Cal Shunt (U)
26		32			15			Closed				Closed			Fermé			Fermé
27		32			15			Open				Open			Ouvert			Ouvert
29		32			15			No				No			Non			Non
30		32			15			Yes				Yes			Oui			Oui
3002		32		15				Converter Installed		Conv Installed		Convertisseur installe		Conver Installe
3003		32		15				No				No			Non			Non
3004		32		15				Yes				Yes			Oui			Oui
3005		32		15				Under Voltage			Under Volt		Sous Tension			Sous Tension
3006		32		15				Over Voltage			Over Volt		Sur Tension			Sur Tension
3007		32		15				Over Current			Over Current		Sur Courant			Sur Courant
3008		32		15				Under Voltage			Under Volt		Sous Tension			Sous Tension
3009		32		15				Over Voltage			Over Volt		Sur Tension			Sur Tension
3010		32		15				Over Current			Over Current		Sur Courant			Sur Courant
3011		32		15				Voltage				Voltage		Tension			Tension
3012		32		15				Total Current			Total Current		Courant total			Courant total
3013		32		15				Input Current			Input Current		Courant d'entrée		Courant entrée
3014		32		15				Efficiency			Efficiency		Rendement			Rendement


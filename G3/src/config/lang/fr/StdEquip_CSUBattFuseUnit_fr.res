﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			Battery Fuse Tripped			Batt Fuse Trip		Défaut protection batterie	Déf Prot Bat
2		32			15			Battery 2 Fuse Tripped		Bat 2 Fuse Trip		Défaut protection batterie 2	Déf Prot Bat2
3		32			15			Battery 1 Voltage			Batt 1 Voltage		Tension Batterie 1		Tension Bat1
4		32			15			Battery 1 Current			Batt 1 Current		Courant Batterie 1		Courant Bat1
5		32			15			Battery 1 Temperature		Battery 1 Temp		Température Batterie 1		Temp. Bat1
6		32			15			Battery 2 Voltage			Batt 2 Voltage		Tension Batterie 2		Tension Bat2
7		32			15			Battery 2 Current			Batt 2 Current		Courant Batterie 2		Courant Bat2
8		32			15			Battery 2 Temperature		Battery 2 Temp		Température Batterie 2		Temp. Bat2
9		32			15			CSU Battery				CSU Battery		Batterie CSU			Batterie CSU
10		32			15			CSU Battery Failure		CSU BatteryFail			Défaut batterie CSU		Déf Bat CSU
11		32			15			No					No			Non				Non
12		32			15			Yes					Yes			Oui				Oui
13		32			15			Battery 2 Connected		Bat 2 Connected			Batterie 2 connectée		Bat 2 connectée
14		32			15			Battery 1 Connected		Bat 1 Connected		Batterie 1 connectée		Bat 1 connectée
15		32			15			Existent				Existent		Présent				Présent
16		32			15			Not Existent			Not Existent		Non Présent			Non Présent

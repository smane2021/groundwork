﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			LVD Group				LVD Group		Groupe contacteur		Grp contacteur
2		32			15			Battery LVD				Battery LVD		Contacteur Batterie		Contacteur Bat
3		32			15			None					None			Aucun				Aucun
4		32			15			LVD 1					LVD 1			LVD 1				LVD 1
5		32			15			LVD 2					LVD 2			LVD 2				LVD 2
6		32			15			LVD 3					LVD 3			LVD 3				LVD 3
7		32			15			AC Fail Required			ACFail Required		Abs.AC Cmd LVD		Abs.AC Cmd LVD
8		32			15			Disabled				Disabled			Desactive			Desactive
9		32			15			Enabled					Enabled			Active				Active
10		32			15			Number of LVD				Number of LVD		Nombre LVD			Nombre LVD
11		32			15			0					0			0				0
12		32			15			1					1			1				1
13		32			15			2					2			2				2
14		32			15			3					3			3				3
15		32			15			HTD Point				HTD Point		Temp ouverture contacteur	Temp.ouvert.LVD
16		32			15			HTD Reconnect Point			HTD Recon Point			Temp fermeture contacteur	Temp.fermet.LVD
17		32			15			HTD Temperature Sensor			HTD Temp Sensor		Référence temp.			Référence temp.
18		32			15			Ambient Temperature			Ambient Temp		Température Ambiante		Temp Ambiante
19		32			15			Battery Temperature			Battery Temp		Température Batteries		Temp Batteries
20		32			15			Temperature 1				Temperature 1			Sonde Température 1		Sonde Temp.1
21		32			15			Temperature 2				Temperature 2		Sonde Température 2		Sonde Temp.2
22		32			15			Temperature 3				Temperature 3			Sonde Température 3		Sonde Temp.3
23		32			15			Temperature 4				Temperature 4			Sonde Température 4		Sonde Temp.4
24		32			15			Temperature 5				Temperature 5			Sonde Température 5		Sonde Temp.5
25		32			15			Temperature 6				Temperature 6			Sonde Température 6		Sonde Temp.6
26		32			15			Temperature 7				Temperature 7			Sonde Température 7		Sonde Temp.7
27		32			15			Existence State				Existence State	Module Présent			Module Présent
28		32			15			Existent				Existent		Présent				Présent
29		32			15			Not Existent				Not Existent		Absent				Absent
31	32			15		LVD 1					LVD 1			LVD1	LVD1 
32	32			15		LVD 1 Mode				LVD 1 Mode				Mode LVD1		Mode LVD1
33		32			15			LVD 1 Voltage				LVD 1 Voltage		Tension LVD1			Tension LVD1
34		32			15			LVD 1 Reconnect Voltage			LVD1 Recon Volt	Tension Reconnexion LVD1	Reconnex.LVD1
35		32			15			LVD 1 Reconnect Delay			LVD1 ReconDelay		Retard Reconnexion LVD1		Retard LVD1
36		32			15			LVD 1 Time				LVD 1 Time		Tempo LVD1			Tempo LVD1
37		32			15			LVD 1 Dependency			LVD1 Dependency			Dépendance LVD1			Dépend LVD1
41	32			15		LVD 2					LVD 2		LVD2	LVD2 
42		32			15			LVD 2 Mode				LVD 2 Mode		Mode LVD2		Mode LVD2
43		32			15			LVD 2 Voltage				LVD 2 Voltage		Tension LVD2			Tension LVD2
44		32			15			LVD 2 Reconnect Voltage			LVD2 Recon Volt		Tension Reconnexion LVD2	Reconnex.LVD2
45	32			15		LVD 2 Reconnect Delay			LVD2 ReconDelay	Retard Reconnexion LVD2		Retard LVD2
46		32			15			LVD 2 Time				LVD 2 Time		Tempo LVD2			Tempo LVD2
47		32			15			LVD 2 Dependency			LVD2 Dependency			Dépendance LVD2			Dépend LVD2
51		32			15			Disabled				Disabled		Non				Non
52		32			15			Enabled					Enabled			Oui				Oui
53		32			15			Voltage					Voltage			Tension				Tension
54		32			15			Time					Time			Temporisation			Temporisation
55		32			15			None					None			Non				Non
56		32			15			LVD 1					LVD 1			LVD1			LVD1
57		32			15			LVD 2					LVD 2			LVD2			LVD2
103		32			15			High Temp Disconnect 1			HTD 1			Ouverture sur Temp.Haute1	HTD 1
104	32			15		High Temp Disconnect 2			HTD 2	Ouverture sur Temp.Haute2	HTD 2
105	32			15		Battery LVD				Battery LVD		LVD Batterie			LVD Batterie
106	32			15		No Battery				No Battery		Aucune Batterie			Aucune Bat.
107	32			15		LVD 1					LVD 1			LVD 1				LVD 1
108	32			15		LVD 2					LVD 2			LVD 2				LVD 2
109		32			15			Battery Always On			Batt Always On		Batterie Toujours Connectée		BatTJ Connectée
110		32			15			LVD Contactor Type			LVD Type		Type contacteur LVD		Type Contac LVD
111		32			15			Bistable				Bistable		Bi-stable			Bi-stable
112		32			15			Mono-Stable				Mono-Stable	Mono-stable			Mono-stable
113		32			15			Mono w/Sample				Mono w/Sample		Mono-stable sans detect		Mono sansdetect
116		32			15			LVD 1 Disconnect			LVD1 Disconnect	LVD1 desconectado		LVD1 Abierto
117		32			15			LVD 2 Disconnect			LVD2 Disconnect	LVD2 desconectado		LVD2 Abierto
118		32			15			LVD 1 Mono w/Sample			LVD1 Mono Sampl		Mono-stable sans detect	LVD1 sansdetect
119		32			15			LVD 2 Mono w/Sample			LVD2 Mono Sampl		Mono-stable sans detect	LVD2 sansdetect
125		32			15			State					State			Etat				Etat
126		32			15			LVD 1 Voltage (24V)			LVD 1 Voltage		Tension LVD1			Tension LVD1
127		32			15			LVD 1 Reconnect Voltage (24V)		LVD1 Recon Volt			Tension Reconex LVD1		V Reconex LVD1
128		32			15			LVD 2 Voltage (24V)			LVD 2 Voltage		Tension LVD2 (24V)		Tensión LVD2
129		32			15			LVD 2 Reconnect Voltage (24V)		LVD2 Recon Volt			Tension Reconex LVD2 (24V)	V Reconex LVD2
130		32			15			LVD 1 Control				LVD 1			Control LVD1		LVD1  
131		32			15			LVD 2 Control				LVD 2			Control LVD2		LVD2  
132		32			15			LVD 1 Reconnect				LVD 1 Reconnect			Reconnection LVD1		Reconnect LVD1
133		32			15			LVD 2 Reconnect				LVD 2 Reconnect		Reconnection LVD2		Reconnect LVD2
134		32			15			HTD Point				HTD Point		Temp ouverture contacteur	Temp.ouvert.LVD
135		32			15			HTD Reconnect Point			HTD Recon Point		Temp fermeture contacteur	Temp.fermet.LVD
136		32			15			HTD Temperature Sensor			HTD Temp Sensor		Reference temp.			Reference temp.
137		32			15			Ambient Temperature			Ambient Temp			Ambiante			Ambiante
138		32			15			Battery Temperature			Battery Temp		Batterie			Batterie
139		32			15			LVD Synchronize				LVD Synchronize		Synchroniser LVD		Synch LVD
140		32			15			Relay for LVD3			Relay for LVD3			Relai pour LVD3			Relai pour LVD3
141		32			15			Relay Output 1			Relay Output 1			Sortie relai 1			Sortie relai 1	
142		32			15			Relay Output 2			Relay Output 2			Sortie relai 2			Sortie relai 2	
143		32			15			Relay Output 3			Relay Output 3			Sortie relai 3			Sortie relai 3	
144		32			15			Relay Output 4			Relay Output 4			Sortie relai 4			Sortie relai 4	
145		32			15			Relay Output 5			Relay Output 5			Sortie relai 5			Sortie relai 5	
146		32			15			Relay Output 6			Relay Output 6			Sortie relai 6			Sortie relai 6	
147		32			15			Relay Output 7			Relay Output 7			Sortie relai 7			Sortie relai 7	
148		32			15			Relay Output 8			Relay Output 8			Sortie relai 8			Sortie relai 8	
149		32			15			Relay Output 14			Relay Output 14			Sortie relai 14			Sortie relai 14	
150		32			15			Relay Output 15			Relay Output 15			Sortie relai 15			Sortie relai 15
151		32			15			Relay Output 16			Relay Output 16			Sortie relai 16			Sortie relai 16
152		32			15			Relay Output 17			Relay Output 17			Sortie relai 17			Sortie relai 17
153		32			15			LVD 3 Enable			LVD 3 Enable			LVD3 Active			LVD3 Active

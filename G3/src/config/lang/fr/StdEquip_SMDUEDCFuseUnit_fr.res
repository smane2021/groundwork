﻿#
#  Locale language support:French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Fuse 1						Fuse 1				Fusible 1			Fusible 1
2	32			15			Fuse 2						Fuse 2				Fusible 2			Fusible 2
3	32			15			Fuse 3						Fuse 3				Fusible 3			Fusible 3
4	32			15			Fuse 4						Fuse 4				Fusible 4			Fusible 4
5	32			15			Fuse 5						Fuse 5				Fusible 5			Fusible 5
6	32			15			Fuse 6						Fuse 6				Fusible 6			Fusible 6
7	32			15			Fuse 7						Fuse 7				Fusible 7			Fusible 7
8	32			15			Fuse 8						Fuse 8				Fusible 8			Fusible 8
9	32			15			Fuse 9						Fuse 9				Fusible 9			Fusible 9
10	32			15			Fuse 10						Fuse 10				Fusible 10			Fusible 10
11	32			15			Off						Off				De			De
12	32			15			On						On				Sur			Sur
13	32			15			Communication Fail				Comm Fail			Échec Communication		Échec Com
14	32			15			Existence State					Existence State		Etat Existence		Etat Exist
15	32			15			Comm OK						Comm OK				Comm OK		Comm OK
16	32			15			Communication Fail				Comm Fail			Échec Communication		Échec Com
17	32			15			Existent					Existent			Existant			Existant
18	32			15			Not Existent					Not Existent		Non existant			Non existant
19	32			15			DC Fuse 1 Alarm					Fuse 1 Alm			DC Fusible 1 Alarme		Fusible 1 Alm
20	32			15			DC Fuse 2 Alarm					Fuse 2 Alm			DC Fusible 2 Alarme		Fusible 2 Alm
21	32			15			DC Fuse 3 Alarm					Fuse 3 Alm			DC Fusible 3 Alarme		Fusible 3 Alm
22	32			15			DC Fuse 4 Alarm					Fuse 4 Alm			DC Fusible 4 Alarme		Fusible 4 Alm
23	32			15			DC Fuse 5 Alarm					Fuse 5 Alm			DC Fusible 5 Alarme		Fusible 5 Alm
24	32			15			DC Fuse 6 Alarm					Fuse 6 Alm			DC Fusible 6 Alarme		Fusible 6 Alm
25	32			15			DC Fuse 7 Alarm					Fuse 7 Alm			DC Fusible 7 Alarme		Fusible 7 Alm
26	32			15			DC Fuse 8 Alarm					Fuse 8 Alm			DC Fusible 8 Alarme		Fusible 8 Alm
27	32			15			DC Fuse 9 Alarm					Fuse 9 Alm			DC Fusible 9 Alarme		Fusible 9 Alm
28	32			15			DC Fuse 10 Alarm				Fuse 10 Alm			DC Fusible 10 Alarme		Fusible 10 Alm
29	32			15			SMDUEDCFuseUnit1				DCFuseUnit1			SMDUEDC Unité Fusible1		DC Unité Fusi1
30	32			15			SMDUEDCFuseUnit2				DCFuseUnit2			SMDUEDC Unité Fusible2		DC Unité Fusi2
31	32			15			SMDUEDCFuseUnit3				DCFuseUnit3			SMDUEDC Unité Fusible3		DC Unité Fusi3
32	32			15			SMDUEDCFuseUnit4				DCFuseUnit4			SMDUEDC Unité Fusible4		DC Unité Fusi4
33	32			15			SMDUEDCFuseUnit5				DCFuseUnit5			SMDUEDC Unité Fusible5		DC Unité Fusi5
34	32			15			SMDUEDCFuseUnit6				DCFuseUnit6			SMDUEDC Unité Fusible6		DC Unité Fusi6
35	32			15			SMDUEDCFuseUnit7				DCFuseUnit7			SMDUEDC Unité Fusible7		DC Unité Fusi7
36	32			15			SMDUEDCFuseUnit8				DCFuseUnit8			SMDUEDC Unité Fusible8		DC Unité Fusi8






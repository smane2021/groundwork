﻿#
#  Locale language support:french
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15		Phase A Voltage				Phase A Volt		Tension phase A			Tension phase A
2		32			15			Phase B Voltage		Phase B Volt	Tension phase B			Tension phase B
3		32			15			Phase C Voltage		Phase C Volt	Tension phase C			Tension phase C
4		32			15			AB Line Voltage		AB Line Volt	Tension AB			Tension  AB
5		32			15			BC Line Voltage		BC Line Volt	Tension BC			Tension  BC
6		32			15			CA Line Voltage		CA Line Volt	Tension CA			Tension  CA
7		32			15			Phase A Current		Phase A Curr	Courant phase A			Courant phase A
8		32			15			Phase B Current		Phase B Curr	Courant phase B			Courant phase B
9		32			15			Phase C Current		Phase C Curr	Courant phase C			Courant phase C
10		32			15			AC Frequency		AC Frequency	Fréquence			Fréquence
11		32			15			Total Real Power		Tot Real Power	Puissance réelle totale		P. Réelle total
12	32			15		Phase A Real Power		PH-A Real Power			Puissance réelle phase A		P. Réelle ph.A
13		32			15			Phase B Real Power		PH-B Real Power		Puissance réelle phase B		P. Réelle ph.B
14		32			15			Phase C Real Power		PH-C Real Power		Puissance réelle phase C		P. Réelle ph.C
15		32			15			Total Reactive Power		Tot React Power		Puissance réactive totale	P. Réact totale
16		32			15			Phase A Reactive Power		PH-A React Pwr		Puissance réactive phase A	P. Réact ph A
17		32			15			Phase B Reactive Power		PH-B React Pwr		Puissance réactive phase B	P. Réact ph B
18		32			15			Phase C Reactive Power		PH-C React Pwr		Puissance réactive phase C	P. Réact ph C
19		32			15			Total Apparent Power		Total App Power		Puissance apparante totale	P. App totale
20		32			15			Phase A Apparent Power		PH-A App Power			Puissance apparante phase A	P. App phase A
21		32			15			Phase B Apparent Power		PH-B App Power			Puissance apparante phase B	P. App phase B
22		32			15			Phase C Apparent Power		PH-C App Power			Puissance apparante phase C	P. App phase C
23		32			15			Power Factor		Power Factor				Facteur de puissance		Facteur de P
24		32			15			Phase A Power Factor		PH-A Pwr Fact	Facteur de puissance phase A	Facteur P phA
25		32			15			Phase B Power Factor		PH-B Pwr Fact	Facteur de puissance phase B	Facteur P phB
26		32			15			Phase C Power Factor		PH-C Pwr Fact	Facteur de puissance phase C	Facteur P phC
27		32			15			Phase A Current Crest Factor		Ia Crest Factor		Facteur de crête phase A	Facteur crêteIa
28		32			15			Phase B Current Crest Factor		Ib Crest Factor		Facteur de crête phase B	Facteur crêteIb
29		32			15			Phase C Current Crest Factor		Ic Crest Factor		Facteur de crête phase C	Facteur crêteIc
30		32			15			Phase A Current THD		PH-A Curr THD		Courant THD phase A		Courant THD A
31		32			15			Phase B Current THD		PH-B Curr THD		Courant THD phase B		Courant THD B
32		32			15			Phase C Current THD		PH-C Curr THD		Courant THD phase C		Courant THD C
33		32			15			Phase A Voltage THD		PH-A Volt THD		Tension THD phase A		Tension THD A
34		32			15			Phase B Voltage THD		PH-B Volt THD		Tension THD phase B		Tension THD B
35		32			15			Phase C Voltage THD		PH-C Volt THD		Tension THD phase C		Tension THD C
36		32			15			Total Real Energy		Tot Real Energy		Energie réelle totale		E réelle totale
37		32			15			Total Reactive Energy		Tot ReactEnergy		Energie réactive totale		E réact. totale
38		32			15			Total Apparent Energy		Tot App Energy		Energie apparante totale	E app. totale
39		32			15			Ambient Temperature		Ambient Temp	Température ambiante		Temp. ambiante
40		32			15			Nominal Line Voltage			Nom Line Volt			Tension de ligne nominale	V ligne nominal
41		32			15			Nominal Phase Voltage			Nom Phase Volt			Tension secteur nominale	VAC nominal
42		32			15			Nominal Frequency			Nom Frequency			Fréquence nominale		F. nominale
43		32			15			Mains Failure Alarm Threshold 1		MFA Threshold 1		Seuil 1 alarme déf. AC		Seuil 1 Alrm AC
44		32			15			Mains Failure Alarm Threshold 2	MFA Threshold 2		Seuil 2 alarme déf. AC		Seuil 2 Alrm AC
45		32			15			Voltage Alarm Threshold 1		Volt Alm Trld 1		Seuil 1 alarme tension		Seuil 1 Al V
46		32			15			Voltage Alarm Threshold 2		Volt Alm Trld 2		Seuil 2 alarme tension		Seuil 2 Al V
47		32			15			Frequency Alarm Threshold		Freq Alarm Trld		Seuil alarme fréquence		Seuil Al F
48		32			15			High Temperature Limit			High Temp Limit		Limite haute température	Limite Haute T
49		32			15			Low Temperature Limit			Low Temp Limit		Limite basse température	Limite Basse T
50		32			15			Supervision Fail			Supervise Fail		Défaut supervision		Déf supervision
51		32			15			High Line Voltage AB			Hi LineVolt AB		Haute Tension ligne AB		Haute V AB
52		32			15			Very High Line Voltage AB		VHi LineVolt AB	Très haute Tension ligne AB		Très haute V AB
53		32			15			Low Line Voltage AB			Lo LineVolt AB		Basse Tension ligne AB		Basse V BC
54		32			15			Very Low Line Voltage AB		VLo LineVolt AB		Très Basse Tension ligne AB	Très Basse V AB
55		32			15			High Line Voltage BC			Hi LineVolt BC	Haute Tension ligne BC		Haute V BC
56		32			15			Very High Line Voltage BC		VHi LineVolt BC		Très haute Tension ligne BC	Très haute V BC
57		32			15			Low Line Voltage BC			Lo LineVolt BC		Basse Tension ligne BC		Basse V BC
58		32			15			Very Low Line Voltage BC		VLo LineVolt BC		Très Basse Tension ligne BC	Très Basse V BC
59		32			15			High Line Voltage CA			Hi LineVolt CA	Haute Tension ligne CA		Haute V CA
60		32			15			Very High Line Voltage CA		VHi LineVolt CA		Très haute Tension ligne CA	Très haute V CA
61		32			15			Low Line Voltage CA			Lo LineVolt CA		Basse Tension ligne CA		Basse V CA
62		32			15			Very Low Line Voltage CA		VLo LineVolt CA		Très Basse Tension ligne CA	Très Basse V CA
63		32			15			High Phase Voltage A			Hi PhaseVolt A	Haute tension phase A		Haute V phA
64		32			15			Very High Phase Voltage A		VHi PhaseVolt A		Très haute tension phase A	Très haut V phA
65		32			15			Low Phase Voltage A			Lo PhaseVolt A		Basse tension phase A		Basse V phA
66		32			15			Very Low Phase Voltage A		VLo PhaseVolt A		Très basse tension phase A	Très bas V phA
67		32			15			High Phase Voltage B			Hi PhaseVolt B	Haute tension phase B		Haute V phB
68		32			15			Very High Phase Voltage B		VHi PhaseVolt B		Très haute tension phase B	Très haut V phB
69		32			15			Low Phase Voltage B			Lo PhaseVolt B		Basse tension phase B		Basse V phB
70		32			15			Very Low Phase Voltage B		VLo PhaseVolt B		Très basse tension phase B	Très bas V phB
71		32			15			High Phase Voltage C			Hi PhaseVolt C	Haute tension phase C		Haute V phC
72		32			15			Very High Phase Voltage C		VHi PhaseVolt C		Très haute tension phase C	Très haut V phC
73		32			15			Low Phase Voltage C			Lo PhaseVolt C		Basse tension phase C		Basse V phC
74		32			15			Very Low Phase Voltage C		VLo PhaseVolt C	Très basse tension phase C	Très bas V phC
75		32			15			Mains Failure				Mains Failure		Défaut secteur			Défaut secteur
76		32			15			Severe Mains Failure		SevereMainsFail		Défaut secteur sévère		Déf sévère sect
77		32			15			High Frequency				High Frequency		Fréquence haute			F haute
78		32			15			Low Frequency				Low Frequency		Fréquence basse			F basse
79		32			15			High Temperature			High Temp		Température haute		T haute
80		32			15			Low Temperature				Low Temperature		Température basse		T basse
81		32			15			SMAC					SMAC		Info secteur			Info secteur
82		32			15			Supervision Fail			SMAC Fail		Défaut supervision		Déf supervision
83		32			15			No					No			Non				Non
84		32			15			Yes					Yes			Oui				Oui
85		32			15			Phase A Mains Failure Counter		PH-A ACFail Cnt		Cpt de défauts secteur phase A	Cpt déf sec phA
86		32			15			Phase B Mains Failure Counter		PH-B ACFail Cnt		Cpt de défauts secteur phase B	Cpt déf sec phB
87		32			15			Phase C Mains Failure Counter		PH-C ACFail Cnt		Cpt de défauts secteur phase C	Cpt déf sec phC
88		32			15			Frequency Failure Counter		Freq Fail Cnt		Compteur de défauts fréquence	Cpt défauts F
89		32			15			Reset Phase A Mains Fail Counter	Reset A FailCnt		Reset compteur déf secteur phA	Rst déf sec phA
90		32			15			Reset Phase B Mains Fail Counter	Reset B FailCnt		Reset compteur déf secteur phB	Rst déf sec phB
91		32			15			Reset Phase C Mains Fail Counter	Reset C FailCnt		Reset compteur déf secteur phC	Rst déf sec phC
92		32			15			Reset Frequency Counter			Rst Frq FailCnt	Reset compteur défaut fréquence	Rst cpt Déf F
93		32			15			Current Alarm Threshold			Curr Alm Limit	Seuil  alarme courant		Seuil Déf I
94		32			15			Phase A  High Current			PH-A Hi Current	Surcourant phase A		Surcourant phA
95		32			15			Phase B  High Current			PH-B Hi Current	Surcourant phase B		Surcourant phB
96		32			15			Phase C  High Current			PH-C Hi Current	Surcourant phase C		Surcourant phC
97		32			15			State					State			Etat				Etat
98		32			15			Off					Off			Arrêt				Arrêt
99		32			15			On					On			Marche				Marche
100	32			15			System Power				System Power		Puissance Système		Puis.Système
101	32			15			Total System Power Consumption		Pwr Consumption			Consommation Totale		Consom.Totale
102		32			15			Existence State				Existence State		Détection			Détection
103		32			15			Existent				Existent		Présent				Présent	
104		32			15			Not Existent			Not Existent		Non Présent			Non Présent	

﻿#
#  Locale language support:French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Bus Bar Voltage				Bus Bar Voltage		Tension du bus		Tension du bus
2	32			15			Shunt 1					Shunt 1			Shunt de 1		Shunt 1	
3	32			15			Shunt 2					Shunt 2			Shunt de 2		Shunt 2
4	32			15			Shunt 3					Shunt 3			Shunt de 3		Shunt 3	
5	32			15			Shunt 4					Shunt 4			Shunt de 4		Shunt 4
6	32			15			Shunt 5					Shunt 5			Shunt de 5		Shunt 5	
7	32			15			Shunt 6					Shunt 6			Shunt de 6		Shunt 6	
8	32			15			Shunt 7					Shunt 7			Shunt de 7		Shunt 7	
9	32			15			Shunt 8					Shunt 8			Shunt de 8		Shunt 8	
10	32			15			Shunt 9					Shunt 9			Shunt de 9		Shunt 9	
11	32			15			Shunt 10				Shunt 10		Shunt de 10		Shunt 10	

12	32			15			Tran1(0-20mA)			Tran1(0-20mA)		Tran1(0-20mA)			Tran1(0-20mA)
13	32			15			Tran2(0-20mA)			Tran2(0-20mA)		Tran2(0-20mA)			Tran2(0-20mA)
14	32			15			Tran3(0-20mA)			Tran3(0-20mA)		Tran3(0-20mA)			Tran3(0-20mA)
15	32			15			Tran4(0-20mA)			Tran4(0-20mA)		Tran4(0-20mA)			Tran4(0-20mA)
16	32			15			Tran5(0-20mA)			Tran5(0-20mA)		Tran5(0-20mA)			Tran5(0-20mA)
17	32			15			Tran6(0-20mA)			Tran6(0-20mA)		Tran6(0-20mA)			Tran6(0-20mA)
18	32			15			Tran7(0-20mA)			Tran7(0-20mA)		Tran7(0-20mA)			Tran7(0-20mA)
19	32			15			Tran8(0-20mA)			Tran8(0-20mA)		Tran8(0-20mA)			Tran8(0-20mA)
20	32			15			Tran9(0-20mA)			Tran9(0-20mA)		Tran9(0-20mA)			Tran9(0-20mA)
21	32			15			Tran10(0-20mA)			Tran10(0-20mA)		Tran10(0-20mA)			Tran10(0-20mA))
22	32			15			Tran1(0-10V)				Tran1(0-10V)		Tran1(0-10V)				Tran1(0-10V)
23	32			15			Tran2(0-10V)				Tran2(0-10V)		Tran2(0-10V)				Tran2(0-10V)
24	32			15			Tran3(0-10V)				Tran3(0-10V)		Tran3(0-10V)				Tran3(0-10V)
25	32			15			Tran4(0-10V)				Tran4(0-10V)		Tran4(0-10V)				Tran4(0-10V)
26	32			15			Tran5(0-10V)				Tran5(0-10V)		Tran5(0-10V)				Tran5(0-10V)
27	32			15			Tran6(0-10V)				Tran6(0-10V)		Tran6(0-10V)				Tran6(0-10V)
28	32			15			Tran7(0-10V)				Tran7(0-10V)		Tran7(0-10V)				Tran7(0-10V)
29	32			15			Tran8(0-10V)				Tran8(0-10V)		Tran8(0-10V)				Tran8(0-10V)
30	32			15			Tran9(0-10V)				Tran9(0-10V)		Tran9(0-10V)				Tran9(0-10V)
31	32			15			Tran10(0-10V)				Tran10(0-10V)		Tran10(0-10V)				Tran10(0-10V)

32	32			15			Temperature 1				Temperature 1		Temperatura 1			Temperatura 1
33	32			15			Temperature 2				Temperature 2		Temperatura 1			Temperatura 1
34	32			15			Temperature 3				Temperature 3		Temperatura 1			Temperatura 1
35	32			15			Temperature 4				Temperature 4		Temperatura 1			Temperatura 1
36	32			15			Temperature 5				Temperature 5		Temperatura 1			Temperatura 1
37	32			15			Temperature 6				Temperature 6		Temperatura 1			Temperatura 1
38	32			15			Temperature 7				Temperature 7		Temperatura 1			Temperatura 1
39	32			15			Temperature 8				Temperature 8		Temperatura 1			Temperatura 1
40	32			15			Temperature 9				Temperature 9		Temperatura 1			Temperatura 1
41	32			15			Temperature 10				Temperature 10		Temperatura 1			Temperatura 1

42	32			15			Voltage1			Voltage1		Tension1		Tension1
43	32			15			Voltage2			Voltage2		Tension2		Tension2
44	32			15			Voltage3			Voltage3		Tension3		Tension3
45	32			15			Voltage4			Voltage4		Tension4		Tension4
46	32			15			Voltage5			Voltage5		Tension5		Tension5
47	32			15			Voltage6			Voltage6		Tension6		Tension6
48	32			15			Voltage7			Voltage7		Tension7		Tension7
49	32			15			Voltage8			Voltage8		Tension8		Tension8
50	32			15			Voltage9			Voltage9		Tension9		Tension9
51	32			15			Voltage10			Voltage10		Tension10		Tension10

52	32			15			Bus Bar Volt				Bus Bar Volt		Tension du bus		Tension du bus
53	32			15			Version No.				Version No.		Num Version			Num Version	
54	32			15			Serial No. High				Serial No. High		Num série Haut		Num série Haut
55	32			15			Serial No. Low				Serial No. Low		Num série Faible	Num série Faibl
56	32			15			Bar Code 1				Bar Code 1		Code Barre 1			Code Barre 1
57	32			15			Bar Code 2				Bar Code 2		Code Barre 2			Code Barre 2
58	32			15			Bar Code 3				Bar Code 3		Code Barre 3			Code Barre 3
59	32			15			Bar Code 4				Bar Code 4		Code Barre 4			Code Barre 4
60	32			15			Comm OK					Comm OK			Comm OK		Comm OK
61	32			15			Communication Fail			Comm Fail		Échec communication		Échec Comm
62	32			15			Existent				Existent		Existant			Existant
63	32			15			Not Existent				Not Existent		Non existant		Non Existant
64	32			15			Shunt 1 Voltage					Shunt1 Voltage			Tension shunt1		Tensin shunt1
65	32			15			Shunt 1 Current					Shunt1 Current			Shunt1 Courant		Shunt1 Courant
66	32			15			Shunt 2 Voltage					Shunt2 Voltage			Tension shunt2		Tensin shunt2
67	32			15			Shunt 2 Current					Shunt2 Current			Shunt2 Courant		Shunt2 Courant
68	32			15			Shunt 3 Voltage					Shunt3 Voltage			Tension shunt3		Tensin shunt3
69	32			15			Shunt 3 Current					Shunt3 Current			Shunt3 Courant		Shunt3 Courant
70	32			15			Shunt 4 Voltage					Shunt4 Voltage			Tension shunt4		Tensin shunt4
71	32			15			Shunt 4 Current					Shunt4 Current			Shunt4 Courant		Shunt4 Courant
72	32			15			Shunt 5 Voltage					Shunt5 Voltage			Tension shunt5		Tensin shunt5
73	32			15			Shunt 5 Current					Shunt5 Current			Shunt5 Courant		Shunt5 Courant
74	32			15			Shunt 6 Voltage					Shunt6 Voltage			Tension shunt6		Tensin shunt6
75	32			15			Shunt 6 Current					Shunt6 Current			Shunt6 Courant		Shunt6 Courant
76	32			15			Shunt 7 Voltage					Shunt7 Voltage			Tension shunt7		Tensin shunt7
77	32			15			Shunt 7 Current					Shunt7 Current			Shunt7 Courant		Shunt7 Courant
78	32			15			Shunt 8 Voltage					Shunt8 Voltage			Tension shunt8		Tensin shunt8
79	32			15			Shunt 8 Current					Shunt8 Current			Shunt8 Courant		Shunt8 Courant
80	32			15			Shunt 9 Voltage					Shunt9 Voltage			Tension shunt9		Tensin shunt9
81	32			15			Shunt 9 Current					Shunt9 Current			Shunt9 Courant		Shunt9 Courant
82	32			15			Shunt 10 Voltage				Shunt10 Voltage			Tension shunt10		Tensin shunt10
83	32			15			Shunt 10 Current				Shunt10 Current			Shunt10 Courant		Shunt10 Courant
84	32			15			Shunt 1						Shunt 1				Shunt 1						Shunt 1	
85	32			15			Shunt 2						Shunt 2				Shunt 2						Shunt 2	
86	32			15			Shunt 3						Shunt 3				Shunt 3						Shunt 3	
87	32			15			Shunt 4						Shunt 4				Shunt 4						Shunt 4	
88	32			15			Shunt 5						Shunt 5				Shunt 5						Shunt 5	
89	32			15			Shunt 6						Shunt 6				Shunt 6						Shunt 6	
90	32			15			Shunt 7						Shunt 7				Shunt 7						Shunt 7	
91	32			15			Shunt 8						Shunt 8				Shunt 8						Shunt 8	
92	32			15			Shunt 9						Shunt 9				Shunt 9						Shunt 9	
93	32			15			Shunt 10					Shunt 10			Shunt 10					Shunt 10
94	32			15			Disabled					Disabled			Désactivé			Désactivé																														
95	32			15			Enabled					Enabled				Activé			Activé																														
96	32			15			Communication Fail				Comm Fail			Échec Com		Échec Com
97	32			15			Existence State					Existence State		Etat d'existence	Etat existence
98	32			15			SMDUE1						SMDUE1				SMDUE1			SMDUE1
99	32			15			SMDUE2						SMDUE2				SMDUE2			SMDUE2
100	32			15			SMDUE3						SMDUE3				SMDUE3			SMDUE3
101	32			15			SMDUE4						SMDUE4				SMDUE4			SMDUE4
102	32			15			SMDUE5						SMDUE5				SMDUE5			SMDUE5
103	32			15			SMDUE6						SMDUE6				SMDUE6			SMDUE6
104	32			15			SMDUE7						SMDUE7				SMDUE7			SMDUE7
105	32			15			SMDUE8						SMDUE8				SMDUE8			SMDUE8
106	32			15			All SMDUE Comm Fail				AllSMCommFail			Tous SMDUE Comm Fail		TousSMCommFail
107	32			15			ESNA Fuse Alarm Mode		ESNA Fuse Alm		ESNA Alarme Fusible			ESNA Alm Fusib
108	32			15			Normal Mode					Normal Mode			Mode normal					Mode normal
109	32			15		Fuse1 Alarm Mode		Fuse1 Mode		Mode Alarme Fusible1			Mode Fusible1	
110	32			15		Fuse2 Alarm Mode		Fuse2 Mode		Mode Alarme Fusible2			Mode Fusible2	
111	32			15		Fuse3 Alarm Mode		Fuse3 Mode		Mode Alarme Fusible3			Mode Fusible3	
112	32			15		Fuse4 Alarm Mode		Fuse4 Mode		Mode Alarme Fusible4			Mode Fusible4
113	32			15		Fuse5 Alarm Mode		Fuse5 Mode		Mode Alarme Fusible5			Mode Fusible5	
114	32			15		Fuse6 Alarm Mode		Fuse6 Mode		Mode Alarme Fusible6			Mode Fusible6	
115	32			15		Fuse7 Alarm Mode		Fuse7 Mode		Mode Alarme Fusible7			Mode Fusible7	
116	32			15		Fuse8 Alarm Mode		Fuse8 Mode		Mode Alarme Fusible8			Mode Fusible8	
117	32			15		Fuse9 Alarm Mode		Fuse9 Mode		Mode Alarme Fusible9			Mode Fusible9	
118	32			15		Fuse10 Alarm Mode		Fuse10 Mode		Mode Alarme Fusible10			Mode Fusible10	

120	32			15			Current1 Break Value				Curr1 Brk Val		Cour1 Pause Valeur		Cour1 Pause Val																														
121	32			15			Current2 Break Value				Curr2 Brk Val		Cour2 Pause Valeur		Cour2 Pause Val																														
122	32			15			Current3 Break Value				Curr3 Brk Val		Cour3 Pause Valeur		Cour3 Pause Val																													
123	32			15			Current4 Break Value				Curr4 Brk Val		Cour4 Pause Valeur		Cour4 Pause Val																														
124	32			15			Current5 Break Value				Curr5 Brk Val		Cour5 Pause Valeur		Cour5 Pause Val																													
125	32			15			Current6 Break Value				Curr6 Brk Val		Cour6 Pause Valeur		Cour6 Pause Val																													
126	32			15			Current7 Break Value				Curr7 Brk Val		Cour7 Pause Valeur		Cour7 Pause Val																														
127	32			15			Current8 Break Value				Curr8 Brk Val		Cour8 Pause Valeur		Cour8 Pause Val																														
128	32			15			Current9 Break Value				Curr9 Brk Val		Cour9 Pause Valeur		Cour9 Pause Val																														
129	32			15			Current10 Break Value				Curr10 Brk Val		Cour10 Pause Valeur		Cour10 PauseVal																													

130	32			15			Current1 High 1 Current Limit		Curr1 Hi 1 Lmt		Courant1 Haut 1 Limite courant		Cour1 Haut1 Lmt																													
131	32			15			Current1 High 2 Current Limit		Curr1 Hi 2 Lmt		Courant1 Haut 2 Limite courant		Cour1 Haut2 Lmt																															
132	32			15			Current2 High 1 Current Limit		Curr2 Hi 1 Lmt		Courant2 Haut 1 Limite courant		Cour2 Haut1 Lmt																													
133	32			15			Current2 High 2 Current Limit		Curr2 Hi 2 Lmt		Courant2 Haut 2 Limite courant		Cour2 Haut2 Lmt																															
134	32			15			Current3 High 1 Current Limit		Curr3 Hi 1 Lmt		Courant3 Haut 1 Limite courant		Cour3 Haut1 Lmt																												
135	32			15			Current3 High 2 Current Limit		Curr3 Hi 2 Lmt		Courant3 Haut 2 Limite courant		Cour3 Haut2 Lmt																														
136	32			15			Current4 High 1 Current Limit		Curr4 Hi 1 Lmt		Courant4 Haut 1 Limite courant		Cour4 Haut1 Lmt																												
137	32			15			Current4 High 2 Current Limit		Curr4 Hi 2 Lmt		Courant4 Haut 2 Limite courant		Cour4 Haut2 Lmt																														
138	32			15			Current5 High 1 Current Limit		Curr5 Hi 1 Lmt		Courant5 Haut 1 Limite courant		Cour5 Haut1 Lmt																													
139	32			15			Current5 High 2 Current Limit		Curr5 Hi 2 Lmt		Courant5 Haut 2 Limite courant		Cour5 Haut2 Lmt																															
140	32			15			Current6 High 1 Current Limit		Curr6 Hi 1 Lmt		Courant6 Haut 1 Limite courant		Cour6 Haut1 Lmt																													
141	32			15			Current6 High 2 Current Limit		Curr6 Hi 2 Lmt		Courant6 Haut 2 Limite courant		Cour6 Haut2 Lmt																															
142	32			15			Current7 High 1 Current Limit		Curr7 Hi 1 Lmt		Courant7 Haut 1 Limite courant		Cour7 Haut1 Lmt																												
143	32			15			Current7 High 2 Current Limit		Curr7 Hi 2 Lmt		Courant7 Haut 2 Limite courant		Cour7 Haut2 Lmt																														
144	32			15			Current8 High 1 Current Limit		Curr8 Hi 1 Lmt		Courant8 Haut 1 Limite courant		Cour8 Haut1 Lmt																												
145	32			15			Current8 High 2 Current Limit		Curr8 Hi 2 Lmt		Courant8 Haut 2 Limite courant		Cour8 Haut2 Lmt																														
146	32			15			Current9 High 1 Current Limit		Curr9 Hi 1 Lmt		Courant9 Haut 1 Limite courant		Cour9 Haut1 Lmt																												
147	32			15			Current9 High 2 Current Limit		Curr9 Hi 2 Lmt		Courant9 Haut 2 Limite courant		Cour9 Haut2 Lmt																														
148	32			15			Current10 High 1 Current Limit		Curr10 Hi 1 Lmt		Courant10 Haut 1 Limite courant		Cour10 Haut1Lmt																														
149	32			15			Current10 High 2 Current Limit		Curr10 Hi 2 Lmt		Courant10 Haut 2 Limite courant		Cour10 Haut2Lmt																														

150	32			15			Current1 High 1 Current			Curr 1 Hi 1		Courant1 Haut 1 courant			Cour1 Haut1																																														
151	32			15			Current1 High 2 Current			Curr 1 Hi 2		Courant1 Haut 2 courant			Cour1 Haut1																																															
152	32			15			Current2 High 1 Current			Curr 2 Hi 1		Courant2 Haut 1 courant			Cour2 Haut1																																															
153	32			15			Current2 High 2 Current			Curr 2 Hi 2		Courant2 Haut 2 courant			Cour2 Haut1																																															
154	32			15			Current3 High 1 Current			Curr 3 Hi 1		Courant3 Haut 1 courant			Cour3 Haut1																																															
155	32			15			Current3 High 2 Current			Curr 3 Hi 2		Courant3 Haut 2 courant			Cour3 Haut1																																																
156	32			15			Current4 High 1 Current			Curr 4 Hi 1		Courant4 Haut 1 courant			Cour4 Haut1																																															
157	32			15			Current4 High 2 Current			Curr 4 Hi 2		Courant4 Haut 2 courant			Cour4 Haut1																																																
158	32			15			Current5 High 1 Current			Curr 5 Hi 1		Courant5 Haut 1 courant			Cour5 Haut1																																															
159	32			15			Current5 High 2 Current			Curr 5 Hi 2		Courant5 Haut 2 courant			Cour5 Haut1																																																
160	32			15			Current6 High 1 Current			Curr 6 Hi 1		Courant6 Haut 1 courant			Cour6 Haut1																																															
161	32			15			Current6 High 2 Current			Curr 6 Hi 2		Courant6 Haut 2 courant			Cour6 Haut1																																															
162	32			15			Current7 High 1 Current			Curr 7 Hi 1		Courant7 Haut 1 courant			Cour7 Haut1																																															
163	32			15			Current7 High 2 Current			Curr 7 Hi 2		Courant7 Haut 2 courant			Cour7 Haut1																																																
164	32			15			Current8 High 1 Current			Curr 8 Hi 1		Courant8 Haut 1 courant			Cour8 Haut1																																															
165	32			15			Current8 High 2 Current			Curr 8 Hi 2		Courant8 Haut 2 courant			Cour8 Haut1																																																
166	32			15			Current9 High 1 Current			Curr 9 Hi 1		Courant9 Haut 1 courant			Cour9 Haut1																																															
167	32			15			Current9 High 2 Current			Curr 9 Hi 2		Courant9 Haut 2 courant			Cour9 Haut1																																																
168	32			15			Current10 High 1 Current		Curr 10 Hi 1		Courant10 Haut 1 courant			Cour10 Haut1																																														
169	32			15			Current10 High 2 Current		Curr 10 Hi 2		Courant10 Haut 2 courant			Cour10 Haut1																																																

214	32			15			Not Used				Not Used				Non Utilisée			Non Utilisée
215	32			15			General					General					General					General
216	32			15			Load					Load					Charge					Charge
217	32			15			Battery					Battery					Batterie				Batterie	

221	32			15			SMDUE1 Temp 1 Assign Equip		S1-T1AssiEquip		SMDUE1 Temp 1 Assign Equip		S1-T1AssiEquip	
222	32			15			SMDUE1 Temp 2 Assign Equip		S1-T2AssiEquip		SMDUE1 Temp 2 Assign Equip		S1-T2AssiEquip	
223	32			15			SMDUE1 Temp 3 Assign Equip		S1-T3AssiEquip		SMDUE1 Temp 3 Assign Equip		S1-T3AssiEquip	
224	32			15			SMDUE1 Temp 4 Assign Equip		S1-T4AssiEquip		SMDUE1 Temp 4 Assign Equip		S1-T4AssiEquip	
225	32			15			SMDUE1 Temp 5 Assign Equip		S1-T5AssiEquip		SMDUE1 Temp 5 Assign Equip		S1-T5AssiEquip	
226	32			15			SMDUE1 Temp 6 Assign Equip		S1-T6AssiEquip		SMDUE1 Temp 6 Assign Equip		S1-T6AssiEquip	
227	32			15			SMDUE1 Temp 7 Assign Equip		S1-T7AssiEquip		SMDUE1 Temp 7 Assign Equip		S1-T7AssiEquip	
228	32			15			SMDUE1 Temp 8 Assign Equip		S1-T8AssiEquip		SMDUE1 Temp 8 Assign Equip		S1-T8AssiEquip	
229	32			15			SMDUE1 Temp 9 Assign Equip		S1-T9AssiEquip		SMDUE1 Temp 9 Assign Equip		S1-T9AssiEquip	
230	32			15			SMDUE1 Temp 10 Assign Equip		S1-T10AssiEquip		SMDUE1 Temp 10 Assign Equip		S1-T10AssiEquip
231	32			15			None					None			Aucune					Aucune
232	32			15			Ambient					Ambient			Ambiance				Ambiance
233	32			15			Battery					Battery			Batterie				Batterie
234	32			15			BTRM					BTRM			BTRM			BTRM

241	32			15			Load 1					Load 1			Charge1		Charge1	
242	32			15			Load 2					Load 2			Charge2		Charge2	
243	32			15			Load 3					Load 3			Charge3		Charge3	
244	32			15			Load 4					Load 4			Charge4		Charge4	
245	32			15			Load 5					Load 5			Charge5		Charge5	
246	32			15			Load 6					Load 6			Charge6		Charge6	
247	32			15			Load 7					Load 7			Charge7		Charge7	
248	32			15			Load 8					Load 8			Charge8		Charge8	
249	32			15			Load 9					Load 9			Charge9		Charge9	
250	32			15			Load 10					Load 10			Charge10	Charge10

251	32			15			SMDUE2 Temp 1 Assign Equip		S2-T1AssiEquip		SMDUE2 Temp 1 Assign Equip		S2-T1AssiEquip	
252	32			15			SMDUE2 Temp 2 Assign Equip		S2-T2AssiEquip		SMDUE2 Temp 2 Assign Equip		S2-T2AssiEquip	
253	32			15			SMDUE2 Temp 3 Assign Equip		S2-T3AssiEquip		SMDUE2 Temp 3 Assign Equip		S2-T3AssiEquip	
254	32			15			SMDUE2 Temp 4 Assign Equip		S2-T4AssiEquip		SMDUE2 Temp 4 Assign Equip		S2-T4AssiEquip	
255	32			15			SMDUE2 Temp 5 Assign Equip		S2-T5AssiEquip		SMDUE2 Temp 5 Assign Equip		S2-T5AssiEquip	
256	32			15			SMDUE2 Temp 6 Assign Equip		S2-T6AssiEquip		SMDUE2 Temp 6 Assign Equip		S2-T6AssiEquip	
257	32			15			SMDUE2 Temp 7 Assign Equip		S2-T7AssiEquip		SMDUE2 Temp 7 Assign Equip		S2-T7AssiEquip	
258	32			15			SMDUE2 Temp 8 Assign Equip		S2-T8AssiEquip		SMDUE2 Temp 8 Assign Equip		S2-T8AssiEquip	
259	32			15			SMDUE2 Temp 9 Assign Equip		S2-T9AssiEquip		SMDUE2 Temp 9 Assign Equip		S2-T9AssiEquip	
260	32			15			SMDUE2 Temp 10 Assign Equip		S2-T10AssiEquip		SMDUE2 Temp 10 Assign Equip		S2-T10AssiEquip
261		32		15		SMDUE1 Temp1 High 2	SMDUE1 T1 Hi2		SMDUE1 Temp 1 Très haute		SMDUE1 T1 THau
262		32		15		SMDUE1 Temp1 High 1	SMDUE1 T1 Hi1		SMDUE1 Temp 1 Haute			SMDUE1 T1 HauT
263		32		15		SMDUE1 Temp1 Low		SMDUE1 T1 Low		SMDUE1 Temp 1 Basse			SMDUE1 T1 Bass
264		32		15		SMDUE1 Temp2 High 2	SMDUE1 T2 Hi2		SMDUE1 Temp 2 Très haute		SMDUE1 T2 THau
265		32		15		SMDUE1 Temp2 High 1	SMDUE1 T2 Hi1		SMDUE1 Temp 2 Haute			SMDUE1 T2 HauT
266		32		15		SMDUE1 Temp2 Low		SMDUE1 T2 Low		SMDUE1 Temp 2 Basse			SMDUE1 T2 Bass
267		32		15		SMDUE1 Temp3 High 2	SMDUE1 T3 Hi2		SMDUE1 Temp 3 Très haute		SMDUE1 T3 THau
268		32		15		SMDUE1 Temp3 High 1	SMDUE1 T3 Hi1		SMDUE1 Temp 3 Haute			SMDUE1 T3 HauT
269		32		15		SMDUE1 Temp3 Low		SMDUE1 T3 Low		SMDUE1 Temp 3 Basse			SMDUE1 T3 Bass
270		32		15		SMDUE1 Temp4 High 2	SMDUE1 T4 Hi2		SMDUE1 Temp 4 Très haute		SMDUE1 T4 THau
271		32		15		SMDUE1 Temp4 High 1	SMDUE1 T4 Hi1		SMDUE1 Temp 4 Haute			SMDUE1 T4 HauT
272		32		15		SMDUE1 Temp4 Low		SMDUE1 T4 Low		SMDUE1 Temp 4 Basse			SMDUE1 T4 Bass
273		32		15		SMDUE1 Temp5 High 2	SMDUE1 T5 Hi2		SMDUE1 Temp 5 Très haute		SMDUE1 T5 THau
274		32		15		SMDUE1 Temp5 High 1	SMDUE1 T5 Hi1		SMDUE1 Temp 5 Haute			SMDUE1 T5 HauT
275		32		15		SMDUE1 Temp5 Low		SMDUE1 T5 Low		SMDUE1 Temp 5 Basse			SMDUE1 T5 Bass
276		32		15		SMDUE1 Temp6 High 2	SMDUE1 T6 Hi2		SMDUE1 Temp 6 Très haute		SMDUE1 T6 THau
277		32		15		SMDUE1 Temp6 High 1	SMDUE1 T6 Hi1		SMDUE1 Temp 6 Haute			SMDUE1 T6 HauT
278		32		15		SMDUE1 Temp6 Low		SMDUE1 T6 Low		SMDUE1 Temp 6 Basse			SMDUE1 T6 Bass
279		32		15		SMDUE1 Temp7 High 2	SMDUE1 T7 Hi2		SMDUE1 Temp 7 Très haute		SMDUE1 T7 THau
280		32		15		SMDUE1 Temp7 High 1	SMDUE1 T7 Hi1		SMDUE1 Temp 7 Haute			SMDUE1 T7 HauT
281		32		15		SMDUE1 Temp7 Low		SMDUE1 T7 Low		SMDUE1 Temp 7 Basse			SMDUE1 T7 Bass
282		32		15		SMDUE1 Temp8 High 2	SMDUE1 T8 Hi2		SMDUE1 Temp 8 Très haute		SMDUE1 T8 THau
283		32		15		SMDUE1 Temp8 High 1	SMDUE1 T8 Hi1		SMDUE1 Temp 8 Haute			SMDUE1 T8 HauT
284		32		15		SMDUE1 Temp8 Low		SMDUE1 T8 Low		SMDUE1 Temp 8 Basse			SMDUE1 T8 Bass
285		32		15		SMDUE1 Temp9 High 2	SMDUE1 T9 Hi2		SMDUE1 Temp 9 Très haute		SMDUE1 T9 THau
286		32		15		SMDUE1 Temp9 High 1	SMDUE1 T9 Hi1		SMDUE1 Temp 9 Haute			SMDUE1 T9 HauT
287		32		15		SMDUE1 Temp9 Low		SMDUE1 T9 Low		SMDUE1 Temp 9 Basse			SMDUE1 T9 Bass
288		32		15		SMDUE1 Temp10 High 2	SMDUE1 T10 Hi2		SMDUE1 Temp 10 Très haute		SMDUE1 T10 THau
289		32		15		SMDUE1 Temp10 High 1	SMDUE1 T10 Hi1		SMDUE1 Temp 10 Haute			SMDUE1 T10 HauT
290		32		15		SMDUE1 Temp10 Low	SMDUE1 T10 Low		SMDUE1 Temp 10 Basse			SMDUE1 T10 Bass
291		32		15		SMDUE2 Temp1 High 2	SMDUE2 T1 Hi2		SMDUE2 Temp 1 Très haute		SMDUE2 T1 THau
292		32		15		SMDUE2 Temp1 High 1	SMDUE2 T1 Hi1		SMDUE2 Temp 1 Haute			SMDUE2 T1 HauT
293		32		15		SMDUE2 Temp1 Low		SMDUE2 T1 Low		SMDUE2 Temp 1 Basse			SMDUE2 T1 Bass
294		32		15		SMDUE2 Temp2 High 2	SMDUE2 T2 Hi2		SMDUE2 Temp 2 Très haute		SMDUE2 T2 THau
295		32		15		SMDUE2 Temp2 High 1	SMDUE2 T2 Hi1		SMDUE2 Temp 2 Haute			SMDUE2 T2 HauT
296		32		15		SMDUE2 Temp2 Low		SMDUE2 T2 Low		SMDUE2 Temp 2 Basse			SMDUE2 T2 Bass
297		32		15		SMDUE2 Temp3 High 2	SMDUE2 T3 Hi2		SMDUE2 Temp 3 Très haute		SMDUE2 T3 THau
298		32		15		SMDUE2 Temp3 High 1	SMDUE2 T3 Hi1		SMDUE2 Temp 3 Haute			SMDUE2 T3 HauT
299		32		15		SMDUE2 Temp3 Low		SMDUE2 T3 Low		SMDUE2 Temp 3 Basse			SMDUE2 T3 Bass
300		32		15		SMDUE2 Temp4 High 2	SMDUE2 T4 Hi2		SMDUE2 Temp 4 Très haute		SMDUE2 T4 THau
301		32		15		SMDUE2 Temp4 High 1	SMDUE2 T4 Hi1		SMDUE2 Temp 4 Haute			SMDUE2 T4 HauT
302		32		15		SMDUE2 Temp4 Low		SMDUE2 T4 Low		SMDUE2 Temp 4 Basse			SMDUE2 T4 Bass
303		32		15		SMDUE2 Temp5 High 2	SMDUE2 T5 Hi2		SMDUE2 Temp 5 Très haute		SMDUE2 T5 THau
304		32		15		SMDUE2 Temp5 High 1	SMDUE2 T5 Hi1		SMDUE2 Temp 5 Haute			SMDUE2 T5 HauT
305		32		15		SMDUE2 Temp5 Low		SMDUE2 T5 Low		SMDUE2 Temp 5 Basse			SMDUE2 T5 Bass
306		32		15		SMDUE2 Temp6 High 2	SMDUE2 T6 Hi2		SMDUE2 Temp 6 Très haute		SMDUE2 T6 THau
307		32		15		SMDUE2 Temp6 High 1	SMDUE2 T6 Hi1		SMDUE2 Temp 6 Haute			SMDUE2 T6 HauT
308		32		15		SMDUE2 Temp6 Low		SMDUE2 T6 Low		SMDUE2 Temp 6 Basse			SMDUE2 T6 Bass
309		32		15		SMDUE2 Temp7 High 2	SMDUE2 T7 Hi2		SMDUE2 Temp 7 Très haute		SMDUE2 T7 THau
310		32		15		SMDUE2 Temp7 High 1	SMDUE2 T7 Hi1		SMDUE2 Temp 7 Haute			SMDUE2 T7 HauT
311		32		15		SMDUE2 Temp7 Low		SMDUE2 T7 Low		SMDUE2 Temp 7 Basse			SMDUE2 T7 Bass
312		32		15		SMDUE2 Temp8 High 2	SMDUE2 T8 Hi2		SMDUE2 Temp 8 Très haute		SMDUE2 T8 THau
313		32		15		SMDUE2 Temp8 High 1	SMDUE2 T8 Hi1		SMDUE2 Temp 8 Haute			SMDUE2 T8 HauT
314		32		15		SMDUE2 Temp8 Low		SMDUE2 T8 Low		SMDUE2 Temp 8 Basse			SMDUE2 T8 Bass
315		32		15		SMDUE2 Temp9 High 2	SMDUE2 T9 Hi2		SMDUE2 Temp 9 Très haute		SMDUE2 T9 THau
316		32		15		SMDUE2 Temp9 High 1	SMDUE2 T9 Hi1		SMDUE2 Temp 9 Haute			SMDUE2 T9 HauT
317		32		15		SMDUE2 Temp9 Low		SMDUE2 T9 Low		SMDUE2 Temp 9 Basse			SMDUE2 T9 Bass
318		32		15		SMDUE2 Temp10 High 2	SMDUE2 T10 Hi2		SMDUE2 Temp 10 Très haute		SMDUE2 T10 THau
319		32		15		SMDUE2 Temp10 High 1	SMDUE2 T10 Hi1		SMDUE2 Temp 10 Haute			SMDUE2 T10 HauT
320		32		15		SMDUE2 Temp10 Low	SMDUE2 T10 Low		SMDUE2 Temp 10 Basse			SMDUE2 T10 Bass
321	32			32			SMDUE1 Temp 1 Not Used		S1-T1 Not Used		SMDUE1-T1 non utilisée		S1-T1 non util
322	32			32			SMDUE1 Temp 2 Not Used		S1-T2 Not Used		SMDUE1-T2 non utilisée		S1-T2 non util
323	32			32			SMDUE1 Temp 3 Not Used		S1-T3 Not Used		SMDUE1-T3 non utilisée		S1-T3 non util
324	32			32			SMDUE1 Temp 4 Not Used		S1-T4 Not Used		SMDUE1-T4 non utilisée		S1-T4 non util
325	32			32			SMDUE1 Temp 5 Not Used		S1-T5 Not Used		SMDUE1-T5 non utilisée		S1-T5 non util
326	32			32			SMDUE1 Temp 6 Not Used		S1-T6 Not Used		SMDUE1-T6 non utilisée		S1-T6 non util
327	32			32			SMDUE1 Temp 7 Not Used		S1-T7 Not Used		SMDUE1-T7 non utilisée		S1-T7 non util
328	32			32			SMDUE1 Temp 8 Not Used		S1-T8 Not Used		SMDUE1-T8 non utilisée		S1-T8 non util
329	32			32			SMDUE1 Temp 9 Not Used		S1-T9 Not Used		SMDUE1-T9 non utilisée		S1-T9 non util
330	32			32			SMDUE1 Temp 10 Not Used		S1-T10 Not Used		SMDUE1-T10 non utilisée		S1-T10 non util
331	32			32			SMDUE1 Temp 1 Sensor Fail	S1-T1 SensFail			Défaut capteur SMDUE1-T1		Déf captS1-T1 
332	32			32			SMDUE1 Temp 2 Sensor Fail	S1-T2 SensFail			Défaut capteur SMDUE1-T2		Déf captS1-T2 
333	32			32			SMDUE1 Temp 3 Sensor Fail	S1-T3 SensFail			Défaut capteur SMDUE1-T3		Déf captS1-T3 
334	32			32			SMDUE1 Temp 4 Sensor Fail	S1-T4 SensFail			Défaut capteur SMDUE1-T4		Déf captS1-T4 
335	32			32			SMDUE1 Temp 5 Sensor Fail	S1-T5 SensFail			Défaut capteur SMDUE1-T5		Déf captS1-T5 
336	32			32			SMDUE1 Temp 6 Sensor Fail	S1-T6 SensFail			Défaut capteur SMDUE1-T6		Déf captS1-T6 
337	32			32			SMDUE1 Temp 7 Sensor Fail	S1-T7 SensFail			Défaut capteur SMDUE1-T7		Déf captS1-T7 
338	32			32			SMDUE1 Temp 8 Sensor Fail	S1-T8 SensFail			Défaut capteur SMDUE1-T8		Déf captS1-T8 
339	32			32			SMDUE1 Temp 9 Sensor Fail	S1-T9 SensFail			Défaut capteur SMDUE1-T9		Déf captS1-T9 
340	32			32			SMDUE1 Temp 10 Sensor Fail	S1-T10SensFail			Défaut capteur SMDUE1-T10		Déf captS1-T10

341	32			32			SMDUE2 Temp 1 Not Used		S2-T1 Not Used		SMDUE2-T1 non utilisée		S2-T1 non util
342	32			32			SMDUE2 Temp 2 Not Used		S2-T2 Not Used		SMDUE2-T2 non utilisée		S2-T2 non util
343	32			32			SMDUE2 Temp 3 Not Used		S2-T3 Not Used		SMDUE2-T3 non utilisée		S2-T3 non util
344	32			32			SMDUE2 Temp 4 Not Used		S2-T4 Not Used		SMDUE2-T4 non utilisée		S2-T4 non util
345	32			32			SMDUE2 Temp 5 Not Used		S2-T5 Not Used		SMDUE2-T5 non utilisée		S2-T5 non util
346	32			32			SMDUE2 Temp 6 Not Used		S2-T6 Not Used		SMDUE2-T6 non utilisée		S2-T6 non util
347	32			32			SMDUE2 Temp 7 Not Used		S2-T7 Not Used		SMDUE2-T7 non utilisée		S2-T7 non util
348	32			32			SMDUE2 Temp 8 Not Used		S2-T8 Not Used		SMDUE2-T8 non utilisée		S2-T8 non util
349	32			32			SMDUE2 Temp 9 Not Used		S2-T9 Not Used		SMDUE2-T9 non utilisée		S2-T9 non util
350	32			32			SMDUE2 Temp 10 Not Used		S2-T10 Not Used		SMDUE2-T10 non utilisée		S2-T10 non util
351	32			32			SMDUE2 Temp 1 Sensor Fail	S2-T1 SensFail			Défaut capteur SMDUE2-T1		Déf capt S2-T1 
352	32			32			SMDUE2 Temp 2 Sensor Fail	S2-T2 SensFail			Défaut capteur SMDUE2-T2		Déf capt S2-T2 
353	32			32			SMDUE2 Temp 3 Sensor Fail	S2-T3 SensFail			Défaut capteur SMDUE2-T3		Déf capt S2-T3 
354	32			32			SMDUE2 Temp 4 Sensor Fail	S2-T4 SensFail			Défaut capteur SMDUE2-T4		Déf capt S2-T4 
355	32			32			SMDUE2 Temp 5 Sensor Fail	S2-T5 SensFail			Défaut capteur SMDUE2-T5		Déf capt S2-T5 
356	32			32			SMDUE2 Temp 6 Sensor Fail	S2-T6 SensFail			Défaut capteur SMDUE2-T6		Déf capt S2-T6 
357	32			32			SMDUE2 Temp 7 Sensor Fail	S2-T7 SensFail			Défaut capteur SMDUE2-T7		Déf capt S2-T7 
358	32			32			SMDUE2 Temp 8 Sensor Fail	S2-T8 SensFail			Défaut capteur SMDUE2-T8		Déf capt S2-T8 
359	32			32			SMDUE2 Temp 9 Sensor Fail	S2-T9 SensFail			Défaut capteur SMDUE2-T9		Déf capt S2-T9 
360	32			32			SMDUE2 Temp 10 Sensor Fail	S2-T10 SensFail			Défaut capteur SMDUE2-T10		Déf capt S2-T10

361		32			15			AI Ch1 Type 	AI Ch1 Type		AI Ch1 Type 	AI Ch1 Type	
362		32			15			AI Ch2 Type 	AI Ch2 Type		AI Ch2 Type 	AI Ch2 Type	
363		32			15			AI Ch3 Type 	AI Ch3 Type		AI Ch3 Type 	AI Ch3 Type	
364		32			15			AI Ch4 Type 	AI Ch4 Type		AI Ch4 Type 	AI Ch4 Type	
365		32			15			AI Ch5 Type 	AI Ch5 Type		AI Ch5 Type 	AI Ch5 Type	
366		32			15			AI Ch6 Type 	AI Ch6 Type		AI Ch6 Type 	AI Ch6 Type	
367		32			15			AI Ch7 Type 	AI Ch7 Type		AI Ch7 Type 	AI Ch7 Type	
368		32			15			AI Ch8 Type 	AI Ch8 Type		AI Ch8 Type 	AI Ch8 Type	
369		32			15			AI Ch9 Type 	AI Ch9 Type		AI Ch9 Type 	AI Ch9 Type	
370		32			15			AI Ch10 Type	AI Ch10 Type	AI Ch10 Type	AI Ch10 Type

371		32			15			Use For Shunt 		Use For Shunt		Utiliser pour shunt 	UtiliPourShunt 
372		32			15			Use For Current 	Use For Current		Utiliser pour courant 	UtiliPourCour
373		32			15			Use For Volt 		Use For Volt		Utilisez pour Tension 	UtiliPourTens 
374		32			15			Use For Temp		Use For Temp		Utiliser pour Temp		UtiliPourTemp

400		32			15			Cabinet1 Volt Enable		Volt1 Enable		Cabinet1 Volt activé		Volt1 activé
401		32			15			Cabinet2 Volt Enable		Volt2 Enable		Cabinet2 Volt activé		Volt2 activé
402		32			15			Cabinet3 Volt Enable		Volt3 Enable		Cabinet3 Volt activé		Volt3 activé
403		32			15			Cabinet4 Volt Enable		Volt4 Enable		Cabinet4 Volt activé		Volt4 activé
404		32			15			Cabinet5 Volt Enable		Volt5 Enable		Cabinet5 Volt activé		Volt5 activé
405		32			15			Cabinet6 Volt Enable		Volt6 Enable		Cabinet6 Volt activé		Volt6 activé
406		32			15			Cabinet7 Volt Enable		Volt7 Enable		Cabinet7 Volt activé		Volt7 activé
407		32			15			Cabinet8 Volt Enable		Volt8 Enable		Cabinet8 Volt activé		Volt8 activé
408		32			15			Cabinet9 Volt Enable		Volt9 Enable		Cabinet9 Volt activé		Volt9 activé
409		32			15			Cabinet10 Volt Enable		Volt10 Enable		Cabinet10 Volt activé		Volt10 activé

410		32			15			Fuse1 Alarm Enable			Fuse1 AlmEnable		Fusible1 alarme Activé		Fusi1AlmActivé
411		32			15			Fuse2 Alarm Enable			Fuse2 AlmEnable		Fusible2 alarme Activé		Fusi2AlmActivé
412		32			15			Fuse3 Alarm Enable			Fuse3 AlmEnable		Fusible3 alarme Activé		Fusi3AlmActivé
413		32			15			Fuse4 Alarm Enable			Fuse4 AlmEnable		Fusible4 alarme Activé		Fusi4AlmActivé
414		32			15			Fuse5 Alarm Enable			Fuse5 AlmEnable		Fusible5 alarme Activé		Fusi5AlmActivé
415		32			15			Fuse6 Alarm Enable			Fuse6 AlmEnable		Fusible6 alarme Activé		Fusi6AlmActivé
416		32			15			Fuse7 Alarm Enable			Fuse7 AlmEnable		Fusible7 alarme Activé		Fusi7AlmActivé
417		32			15			Fuse8 Alarm Enable			Fuse8 AlmEnable		Fusible8 alarme Activé		Fusi8AlmActivé
418		32			15			Fuse9 Alarm Enable			Fuse9 AlmEnable		Fusible9 alarme Activé		Fusi9AlmActivé
419		32			15			Fuse10 Alarm Enable			Fuse10 AlmEnable	Fusible10 alarme Activé		Fusi10AlmActivé

420		32			15			Analog1 Input Type			Ana1 InputType		Analog1 Type Entrée		Ana1TypeEntrée
421		32			15			Analog2 Input Type			Ana2 InputType		Analog2 Type Entrée		Ana2TypeEntrée
422		32			15			Analog3 Input Type			Ana3 InputType		Analog3 Type Entrée		Ana3TypeEntrée
423		32			15			Analog4 Input Type			Ana4 InputType		Analog4 Type Entrée		Ana4TypeEntrée
424		32			15			Analog5 Input Type			Ana5 InputType		Analog5 Type Entrée		Ana5TypeEntrée
425		32			15			Analog6 Input Type			Ana6 InputType		Analog6 Type Entrée		Ana6TypeEntrée
426		32			15			Analog7 Input Type			Ana7 InputType		Analog7 Type Entrée		Ana7TypeEntrée
427		32			15			Analog8 Input Type			Ana8 InputType		Analog8 Type Entrée		Ana8TypeEntrée
428		32			15			Analog9 Input Type			Ana9 InputType		Analog9 Type Entrée		Ana9TypeEntrée
429		32			15			Analog10 Input Type			Ana10 InputType		Analog10 Type Entrée	Ana10TypeEntrée
430		32			15			Disabled					Disabled				désactivé				désactivé
431		32			15			Enabled						Enabled					Activé					Activé
432		32			15			Shunt						Shunt					Shunter					Shunter
433		32			15			Temperature					Temperature				Température				Température
434		32			15			Transducer (0-10V)			Tsr (0-10V)				Transducteur(0-10V)		Trans(0-10V)	
435		32			15			Transducer (0-20mA)			Tsr (0-20mA)			Transducteur(0-20mA)	Trans(0-20mA)

441		32			15			Input Block 1		Input Block 1	Input Block 1		Input Block 1
442		32			15			Input Block 2		Input Block 2	Input Block 2		Input Block 2
443		32			15			Input Block 3		Input Block 3	Input Block 3		Input Block 3
444		32			15			Input Block 4		Input Block 4	Input Block 4		Input Block 4
445		32			15			Input Block 5		Input Block 5	Input Block 5		Input Block 5
446		32			15			Input Block 6		Input Block 6	Input Block 6		Input Block 6
447		32			15			Input Block 7		Input Block 7	Input Block 7		Input Block 7
448		32			15			Input Block 8		Input Block 8	Input Block 8		Input Block 8
449		32			15			Input Block 9		Input Block 9	Input Block 9		Input Block 9
450		32			15			Input Block 10		Input Block 10	Input Block 10		Input Block 10

451		32			15			Current Transducer1		C Tran 1		Transducteur Courant1		C Tran 1
452		32			15			Current Transducer2		C Tran 2		Transducteur Courant2		C Tran 2
453		32			15			Current Transducer3		C Tran 3		Transducteur Courant3		C Tran 3
454		32			15			Current Transducer4		C Tran 4		Transducteur Courant4		C Tran 4
455		32			15			Current Transducer5		C Tran 5		Transducteur Courant5		C Tran 5
456		32			15			Current Transducer6		C Tran 6		Transducteur Courant6		C Tran 6
457		32			15			Current Transducer7		C Tran 7		Transducteur Courant7		C Tran 7
458		32			15			Current Transducer8		C Tran 8		Transducteur Courant8		C Tran 8
459		32			15			Current Transducer9		C Tran 9		Transducteur Courant9		C Tran 9
460		32			15			Current Transducer10	C Tran 10		Transducteur Courant10		C Tran 10

461		32			15		Voltage Transducer1			V Tran 1	Tension Transducer1			V Tran 1
462		32			15		Voltage Transducer2			V Tran 2	Tension Transducer2			V Tran 2
463		32			15		Voltage Transducer3			V Tran 3	Tension Transducer3			V Tran 3
464		32			15		Voltage Transducer4			V Tran 4	Tension Transducer4			V Tran 4
465		32			15		Voltage Transducer5			V Tran 5	Tension Transducer5			V Tran 5
466		32			15		Voltage Transducer6			V Tran 6	Tension Transducer6			V Tran 6
467		32			15		Voltage Transducer7			V Tran 7	Tension Transducer7			V Tran 7
468		32			15		Voltage Transducer8			V Tran 8	Tension Transducer8			V Tran 8
469		32			15		Voltage Transducer9			V Tran 9	Tension Transducer9			V Tran 9
470		32			15		Voltage Transducer10		V Tran 10	Tension Transducer10		V Tran 10

471		32			15			X1					X1					X1					X1	
472		32			15			Y1(Value at X1)		Y1(Value at X1)		Y1(Valeur à X1)		Y1(Valeur à X1)	
473		32			15			X2					X2					X2					X2
474		32			15			Y2(Value at X2)		Y2(Value at X2)		Y2(Valeur à X2)		Y2(Valeur à X2)	


600		32			15			Voltage1 High2 Volt Limit	Vol1 Hi2 Vol		Limite Tension1 Haute2 Tens		Tens1 H2 Tens
601		32			15			Voltage1 High1 Volt Limit	Vol1 Hi1 Vol		Limite Tension1 Haute1 Tens		Tens1 H1 Tens	
602		32			15			Voltage1 Low1 Volt Limit	Vol1 Lw1 Vol		Limite Tension1 Bas1 Tens		Tens1 Bas1 Tens
603		32			15			Voltage1 Low2 Volt Limit	Vol1 Lw2 Vol		Limite Tension1 Bas2 Tens		Tens1 Bas2 Tens
604		32			15			Voltage2 High2 Volt Limit	Vol2 Hi2 Vol		Limite Tension2 Haute2 Tens		Tens2 H2 Tens
605		32			15			Voltage2 High1 Volt Limit	Vol2 Hi1 Vol		Limite Tension2 Haute1 Tens		Tens2 H1 Tens	
606		32			15			Voltage2 Low1 Volt Limit	Vol2 Lw1 Vol		Limite Tension2 Bas1 Tens		Tens2 Bas1 Tens
607		32			15			Voltage2 Low2 Volt Limit	Vol2 Lw2 Vol		Limite Tension2 Bas2 Tens		Tens2 Bas2 Tens
608		32			15			Voltage3 High2 Volt Limit	Vol3 Hi2 Vol		Limite Tension3 Haute2 Tens		Tens3 H2 Tens	
609		32			15			Voltage3 High1 Volt Limit	Vol3 Hi1 Vol		Limite Tension3 Haute1 Tens		Tens3 H1 Tens	
610		32			15			Voltage3 Low1 Volt Limit	Vol3 Lw1 Vol		Limite Tension3 Bas1 Tens		Tens3 Bas1 Tens
611		32			15			Voltage3 Low2 Volt Limit	Vol3 Lw2 Vol		Limite Tension3 Bas2 Tens		Tens3 Bas2 Tens	
612		32			15			Voltage4 High2 Volt Limit	Vol4 Hi2 Vol		Limite Tension4 Haute2 Tens		Tens4 H2 Tens	
613		32			15			Voltage4 High1 Volt Limit	Vol4 Hi1 Vol		Limite Tension4 Haute1 Tens		Tens4 H1 Tens	
614		32			15			Voltage4 Low1 Volt Limit	Vol4 Lw1 Vol		Limite Tension4 Bas1 Tens		Tens4 Bas1 Tens
615		32			15			Voltage4 Low2 Volt Limit	Vol4 Lw2 Vol		Limite Tension4 Bas2 Tens		Tens4 Bas2 Tens	
616		32			15			Voltage5 High2 Volt Limit	Vol5 Hi2 Vol		Limite Tension5 Haute2 Tens		Tens5 H2 Tens	
617		32			15			Voltage5 High1 Volt Limit	Vol5 Hi1 Vol		Limite Tension5 Haute1 Tens		Tens5 H1 Tens	
618		32			15			Voltage5 Low1 Volt Limit	Vol5 Lw1 Vol		Limite Tension5 Bas1 Tens		Tens5 Bas1 Tens
619		32			15			Voltage5 Low2 Volt Limit	Vol5 Lw2 Vol		Limite Tension5 Bas2 Tens		Tens5 Bas2 Tens
620		32			15			Voltage6 High2 Volt Limit	Vol6 Hi2 Vol		Limite Tension6 Haute2 Tens		Tens6 H2 Tens	
621		32			15			Voltage6 High1 Volt Limit	Vol6 Hi1 Vol		Limite Tension6 Haute1 Tens		Tens6 H1 Tens	
622		32			15			Voltage6 Low1 Volt Limit	Vol6 Lw1 Vol		Limite Tension6 Bas1 Tens		Tens6 Bas1 Tens
623		32			15			Voltage6 Low2 Volt Limit	Vol6 Lw2 Vol		Limite Tension6 Bas2 Tens		Tens6 Bas2 Tens
624		32			15			Voltage7 High2 Volt Limit	Vol7 Hi2 Vol		Limite Tension7 Haute2 Tens		Tens7 H2 Tens	
625		32			15			Voltage7 High1 Volt Limit	Vol7 Hi1 Vol		Limite Tension7 Haute1 Tens		Tens7 H1 Tens	
626		32			15			Voltage7 Low1 Volt Limit	Vol7 Lw1 Vol		Limite Tension7 Bas1 Tens		Tens7 Bas1 Tens
627		32			15			Voltage7 Low2 Volt Limit	Vol7 Lw2 Vol		Limite Tension7 Bas2 Tens		Tens7 Bas2 Tens
628		32			15			Voltage8 High2 Volt Limit	Vol8 Hi2 Vol		Limite Tension8 Haute2 Tens		Tens8 H2 Tens	
629		32			15			Voltage8 High1 Volt Limit	Vol8 Hi1 Vol		Limite Tension8 Haute1 Tens		Tens8 H1 Tens	
630		32			15			Voltage8 Low1 Volt Limit	Vol8 Lw1 Vol		Limite Tension8 Bas1 Tens		Tens8 Bas1 Tens
631		32			15			Voltage8 Low2 Volt Limit	Vol8 Lw2 Vol		Limite Tension8 Bas2 Tens		Tens8 Bas2 Tens
632		32			15			Voltage9 High2 Volt Limit	Vol9 Hi2 Vol		Limite Tension9 Haute2 Tens		Tens9 H2 Tens	
633		32			15			Voltage9 High1 Volt Limit	Vol9 Hi1 Vol		Limite Tension9 Haute1 Tens		Tens9 H1 Tens	
634		32			15			Voltage9 Low1 Volt Limit	Vol9 Lw1 Vol		Limite Tension9 Bas1 Tens		Tens9 Bas1 Tens
635		32			15			Voltage9 Low2 Volt Limit	Vol9 Lw2 Vol		Limite Tension9 Bas2 Tens		Tens9 Bas2 Tens
636		32			15			Voltage10 High2 Volt Limit	Vol10 Hi2 Vol		Limite Tension10 Haute2 Tens	Tens10 H2 Tens	
637		32			15			Voltage10 High1 Volt Limit	Vol10 Hi1 Vol		Limite Tension10 Haute1 Tens	Tens10 H1 Tens	
638		32			15			Voltage10 Low1 Volt Limit	Vol10 Lw1 Vol		Limite Tension10 Bas1 Tens		Tens10 Bas1 Tens
639		32			15			Voltage10 Low2 Volt Limit	Vol10 Lw2 Vol		Limite Tension10 Bas2 Tens		Tens10 Bas2 Tens

640		32			15			Voltage1 Low1 Volt Alarm			Vol1 Lw1 Vol Alm	Tension1 Bas1 Tens Alm	Tens1 Bas1 Tens	
641		32			15			Voltage1 Low2 Volt Alarm			Vol1 Lw2 Vol Alm	Tension1 Bas2 Tens Alm	Tens1 Bas2 Tens		
642		32			15			Voltage1 High1 Volt Alarm			Vol1 Hi1 Vol Alm	Tension1 Haute1 Tens Alm	Tens1 H1 Tens	
643		32			15			Voltage1 High2 Volt Alarm			Vol1 Hi2 Vol Alm	Tension1 Haute2 Tens Alm	Tens1 H2 Tens	
644		32			15			Voltage2 Low1 Volt Alarm			Vol2 Lw1 Vol Alm	Tension2 Bas1 Tens Alm		Tens2 Bas1 Tens		
645		32			15			Voltage2 Low2 Volt Alarm			Vol2 Lw2 Vol Alm	Tension2 Bas2 Tens Alm		Tens2 Bas2 Tens		
646		32			15			Voltage2 High1 Volt Alarm			Vol2 Hi1 Vol Alm	Tension2 Haute1 Tens Alm	Tens2 H1 Tens	
647		32			15			Voltage2 High2 Volt Alarm			Vol2 Hi2 Vol Alm	Tension2 Haute2 Tens Alm	Tens2 H2 Tens	
648		32			15			Voltage3 Low1 Volt Alarm			Vol3 Lw1 Vol Alm	Tension3 Bas1 Tens Alm		Tens3 Bas1 Tens		
649		32			15			Voltage3 Low2 Volt Alarm			Vol3 Lw2 Vol Alm	Tension3 Bas2 Tens Alm		Tens3 Bas2 Tens			
650		32			15			Voltage3 High1 Volt Alarm			Vol3 Hi1 Vol Alm	Tension3 Haute1 Tens Alm	Tens3 H1 Tens		
651		32			15			Voltage3 High2 Volt Alarm			Vol3 Hi2 Vol Alm	Tension3 Haute2 Tens Alm	Tens3 H2 Tens		
652		32			15			Voltage4 Low1 Volt Alarm			Vol4 Lw1 Vol Alm	Tension4 Bas1 Tens Alm		Tens4 Bas1 Tens		
653		32			15			Voltage4 Low2 Volt Alarm			Vol4 Lw2 Vol Alm	Tension4 Bas2 Tens Alm		Tens4 Bas2 Tens		
654		32			15			Voltage4 High1 Volt Alarm			Vol4 Hi1 Vol Alm	Tension4 Haute1 Tens Alm	Tens4 H1 Tens		
655		32			15			Voltage4 High2 Volt Alarm			Vol4 Hi2 Vol Alm	Tension4 Haute2 Tens Alm	Tens4 H2 Tens		
656		32			15			Voltage5 Low1 Volt Alarm			Vol5 Lw1 Vol Alm	Tension5 Bas1 Tens Alm	Tens5 Bas1 Tens		
657		32			15			Voltage5 Low2 Volt Alarm			Vol5 Lw2 Vol Alm	Tension5 Bas2 Tens Alm	Tens5 Bas2 Tens			
658		32			15			Voltage5 High1 Volt Alarm			Vol5 Hi1 Vol Alm	Tension5 Haute1 Tens Alm	Tens5 H1 Tens		
659		32			15			Voltage5 High2 Volt Alarm			Vol5 Hi2 Vol Alm	Tension5 Haute2 Tens Alm	Tens5 H2 Tens		
660		32			15			Voltage6 Low1 Volt Alarm			Vol6 Lw1 Vol Alm	Tension6 Bas1 Tens Alm		Tens6 Bas1 Tens			
661		32			15			Voltage6 Low2 Volt Alarm			Vol6 Lw2 Vol Alm	Tension6 Bas2 Tens Alm		Tens6 Bas2 Tens		
662		32			15			Voltage6 High1 Volt Alarm			Vol6 Hi1 Vol Alm	Tension6 Haute1 Tens Alm	Tens6 H1 Tens		
663		32			15			Voltage6 High2 Volt Alarm			Vol6 Hi2 Vol Alm	Tension6 Haute2 Tens Alm	Tens6 H2 Tens		
664		32			15			Voltage7 Low1 Volt Alarm			Vol7 Lw1 Vol Alm	Tension7 Bas1 Tens Alm		Tens7 Bas1 Tens			
665		32			15			Voltage7 Low2 Volt Alarm			Vol7 Lw2 Vol Alm	Tension7 Bas2 Tens Alm		Tens7 Bas2 Tens		
666		32			15			Voltage7 High1 Volt Alarm			Vol7 Hi1 Vol Alm	Tension7 Haute1 Tens Alm	Tens7 H1 Tens		
667		32			15			Voltage7 High2 Volt Alarm			Vol7 Hi2 Vol Alm	Tension7 Haute2 Tens Alm	Tens7 H2 Tens		
668		32			15			Voltage8 Low1 Volt Alarm			Vol8 Lw1 Vol Alm	Tension8 Bas1 Tens Alm		Tens8 Bas1 Tens			
669		32			15			Voltage8 Low2 Volt Alarm			Vol8 Lw2 Vol Alm	Tension8 Bas2 Tens Alm		Tens8 Bas2 Tens		
670		32			15			Voltage8 High1 Volt Alarm			Vol8 Hi1 Vol Alm	Tension8 Haute1 Tens Alm	Tens8 H1 Tens		
671		32			15			Voltage8 High2 Volt Alarm			Vol8 Hi2 Vol Alm	Tension8 Haute2 Tens Alm	Tens8 H2 Tens		
672		32			15			Voltage9 Low1 Volt Alarm			Vol9 Lw1 Vol Alm	Tension9 Bas1 Tens Alm		Tens9 Bas1 Tens		
673		32			15			Voltage9 Low2 Volt Alarm			Vol9 Lw2 Vol Alm	Tension9 Bas2 Tens Alm		Tens9 Bas2 Tens		
674		32			15			Voltage9 High1 Volt Alarm			Vol9 Hi1 Vol Alm	Tension9 Haute1 Tens Alm	Tens9 H1 Tens		
675		32			15			Voltage9 High2 Volt Alarm			Vol9 Hi2 Vol Alm	Tension9 Haute2 Tens Alm	Tens9 H2 Tens		
676		32			15			Voltage10 Low1 Volt Alarm			Vol10 Lw1 Vol Alm	Tension10 Bas1 Tens Alm		Tens10 Bas1Tens		
677		32			15			Voltage10 Low2 Volt Alarm			Vol10 Lw2 Vol Alm	Tension10 Bas2 Tens Alm		Tens10 Bas2Tens		
678		32			15			Voltage10 High1 Volt Alarm			Vol10 Hi1 Vol Alm	Tension10 Haute1 Tens Alm	Tens10 H1 Tens		
679		32			15			Voltage10 High2 Volt Alarm			Vol10 Hi2 Vol Alm	Tension10 Haute2 Tens Alm	Tens10 H2 Tens		

680		32			15			Transducer1 High2 Volt Limit		Vol1 Hi2 Vol		Transducer1 Tens Limite Haut 2		Tension1 H2Tens	
681		32			15			Transducer1 High1 Volt Limit		Vol1 Hi1 Vol		Transducer1 Tens Limite Haut 1		Tension1 H1Tens	
682		32			15			Transducer1 Low1 Volt Limit			Vol1 Lw1 Vol		Transducer1 Tens Limite Bas1		Tens1 Bas1Tens	
683		32			15			Transducer1 Low2 Volt Limit			Vol1 Lw2 Vol		Transducer1 Tens Limite Bas2		Tens1 Bas2Tens	
684		32			15			Transducer2 High2 Volt Limit		Vol2 Hi2 Vol		Transducer2 Tens Limite Haut 2		Tension2 H2Tens		
685		32			15			Transducer2 High1 Volt Limit		Vol2 Hi1 Vol		Transducer2 Tens Limite Haut 1		Tension2 H1Tens	
686		32			15			Transducer2 Low1 Volt Limit			Vol2 Lw1 Vol		Transducer2 Tens Limite Bas1		Tens2 Bas1Tens	
687		32			15			Transducer2 Low2 Volt Limit			Vol2 Lw2 Vol		Transducer2 Tens Limite Bas2		Tens2 Bas2Tens		
688		32			15			Transducer3 High2 Volt Limit		Vol3 Hi2 Vol		Transducer3 Tens Limite Haut 2		Tension3 H2Tens	
689		32			15			Transducer3 High1 Volt Limit		Vol3 Hi1 Vol		Transducer3 Tens Limite Haut 1		Tension3 H1Tens	
690		32			15			Transducer3 Low1 Volt Limit			Vol3 Lw1 Vol		Transducer3 Tens Limite Bas1		Tens3 Bas1Tens	
691		32			15			Transducer3 Low2 Volt Limit			Vol3 Lw2 Vol		Transducer3 Tens Limite Bas2		Tens3 Bas2Tens	
692		32			15			Transducer4 High2 Volt Limit		Vol4 Hi2 Vol		Transducer4 Tens Limite Haut 2		Tension4 H2Tens	
693		32			15			Transducer4 High1 Volt Limit		Vol4 Hi1 Vol		Transducer4 Tens Limite Haut 1		Tension4 H1Tens	
694		32			15			Transducer4 Low1 Volt Limit			Vol4 Lw1 Vol		Transducer4 Tens Limite Bas1		Tens4 Bas1Tens	
695		32			15			Transducer4 Low2 Volt Limit			Vol4 Lw2 Vol		Transducer4 Tens Limite Bas2		Tens4 Bas2Tens	
696		32			15			Transducer5 High2 Volt Limit		Vol5 Hi2 Vol		Transducer5 Tens Limite Haut 2		Tension5 H2Tens	
697		32			15			Transducer5 High1 Volt Limit		Vol5 Hi1 Vol		Transducer5 Tens Limite Haut 1		Tension5 H1Tens	
698		32			15			Transducer5 Low1 Volt Limit			Vol5 Lw1 Vol		Transducer5 Tens Limite Bas1		Tens5 Bas1Tens	
699		32			15			Transducer5 Low2 Volt Limit			Vol5 Lw2 Vol		Transducer5 Tens Limite Bas2		Tens5 Bas2Tens	
700		32			15			Transducer6 High2 Volt Limit		Vol6 Hi2 Vol		Transducer6 Tens Limite Haut 2		Tension6 H2Tens	
701		32			15			Transducer6 High1 Volt Limit		Vol6 Hi1 Vol		Transducer6 Tens Limite Haut 1		Tension6 H1Tens	
702		32			15			Transducer6 Low1 Volt Limit			Vol6 Lw1 Vol		Transducer6 Tens Limite Bas1		Tens6 Bas1Tens	
703		32			15			Transducer6 Low2 Volt Limit			Vol6 Lw2 Vol		Transducer6 Tens Limite Bas2		Tens6 Bas2Tens	
704		32			15			Transducer7 High2 Volt Limit		Vol7 Hi2 Vol		Transducer7 Tens Limite Haut 2		Tension7 H2Tens	
705		32			15			Transducer7 High1 Volt Limit		Vol7 Hi1 Vol		Transducer7 Tens Limite Haut 1		Tension7 H1Tens	
706		32			15			Transducer7 Low1 Volt Limit			Vol7 Lw1 Vol		Transducer7 Tens Limite Bas1		Tens7 Bas1Tens	
707		32			15			Transducer7 Low2 Volt Limit			Vol7 Lw2 Vol		Transducer7 Tens Limite Bas2		Tens7 Bas2Tens	
708		32			15			Transducer8 High2 Volt Limit		Vol8 Hi2 Vol		Transducer8 Tens Limite Haut 2		Tension8 H2Tens	
709		32			15			Transducer8 High1 Volt Limit		Vol8 Hi1 Vol		Transducer8 Tens Limite Haut 1		Tension8 H1Tens	
710		32			15			Transducer8 Low1 Volt Limit			Vol8 Lw1 Vol		Transducer8 Tens Limite Bas1		Tens8 Bas1Tens	
711		32			15			Transducer8 Low2 Volt Limit			Vol8 Lw2 Vol		Transducer8 Tens Limite Bas2		Tens8 Bas2Tens	
712		32			15			Transducer9 High2 Volt Limit		Vol9 Hi2 Vol		Transducer9 Tens Limite Haut 2		Tension9 H2Tens	
713		32			15			Transducer9 High1 Volt Limit		Vol9 Hi1 Vol		Transducer9 Tens Limite Haut 1		Tension9 H1Tens	
714		32			15			Transducer9 Low1 Volt Limit			Vol9 Lw1 Vol		Transducer9 Tens Limite Bas1		Tens9 Bas1Tens	
715		32			15			Transducer9 Low2 Volt Limit			Vol9 Lw2 Vol		Transducer9 Tens Limite Bas2		Tens9 Bas2Tens	
716		32			15			Transducer10 High2 Volt Limit		Vol10 Hi2 Vol		Transducer10 Tens Limite Haut 2		Tension10 H2Tens	
717		32			15			Transducer10 High1 Volt Limit		Vol10 Hi1 Vol		Transducer10 Tens Limite Haut 1		Tension10 H1Tens	
718		32			15			Transducer10 Low1 Volt Limit		Vol10 Lw1 Vol		Transducer10 Tens Limite Bas1		Tens10 Bas1Tens	
719		32			15			Transducer10 Low2 Volt Limit		Vol10 Lw2 Vol		Transducer10 Tens Limite Bas2		Tens10 Bas2Tens	

720		32			15			TranVolt1 Low1 Volt Alarm			TranVol1 Lw1 Alm		Alm Trans Tens Bas1 Tension1		TransBas1Tens1
721		32			15			TranVolt1 Low2 Volt Alarm			TranVol1 Lw2 Alm		Alm Trans Tens Bas2 Tension1		TransBas2Tens1		
722		32			15			TranVolt1 High1 Volt Alarm			TranVol1 Hi1 Alm		Alm Trans Tens Haut1 Tension1		TransHaut1Tens1
723		32			15			TranVolt1 High2 Volt Alarm			TranVol1 Hi2 Alm		Alm Trans Tens Haut2 Tension1		TransHaut2Tens1		
724		32			15			TranVolt2 Low1 Volt Alarm			TranVol2 Lw1 Alm		Alm Trans Tens Bas1 Tension2		TransBas1Tens2	
725		32			15			TranVolt2 Low2 Volt Alarm			TranVol2 Lw2 Alm		Alm Trans Tens Bas2 Tension2		TransBas2Tens2		
726		32			15			TranVolt2 High1 Volt Alarm			TranVol2 Hi1 Alm		Alm Trans Tens Haut1 Tension2		TransHaut1Tens2
727		32			15			TranVolt2 High2 Volt Alarm			TranVol2 Hi2 Alm		Alm Trans Tens Haut2 Tension2		TransHaut2Tens2		
728		32			15			TranVolt3 Low1 Volt Alarm			TranVol3 Lw1 Alm		Alm Trans Tens Bas1 Tension3		TransBas1Tens3	
729		32			15			TranVolt3 Low2 Volt Alarm			TranVol3 Lw2 Alm		Alm Trans Tens Bas2 Tension3		TransBas2Tens3		
730		32			15			TranVolt3 High1 Volt Alarm			TranVol3 Hi1 Alm		Alm Trans Tens Haut1 Tension3		TransHaut1Tens3
731		32			15			TranVolt3 High2 Volt Alarm			TranVol3 Hi2 Alm		Alm Trans Tens Haut2 Tension3		TransHaut2Tens3
732		32			15			TranVolt4 Low1 Volt Alarm			TranVol4 Lw1 Alm		Alm Trans Tens Bas1 Tension4		TransBas1Tens4
733		32			15			TranVolt4 Low2 Volt Alarm			TranVol4 Lw2 Alm		Alm Trans Tens Bas2 Tension4		TransBas2Tens4		
734		32			15			TranVolt4 High1 Volt Alarm			TranVol4 Hi1 Alm		Alm Trans Tens Haut1 Tension4		TransHaut1Tens4
735		32			15			TranVolt4 High2 Volt Alarm			TranVol4 Hi2 Alm		Alm Trans Tens Haut2 Tension4		TransHaut2Tens4		
736		32			15			TranVolt5 Low1 Volt Alarm			TranVol5 Lw1 Alm		Alm Trans Tens Bas1 Tension5		TransBas1Tens5	
737		32			15			TranVolt5 Low2 Volt Alarm			TranVol5 Lw2 Alm		Alm Trans Tens Bas2 Tension5		TransBas2Tens5		
738		32			15			TranVolt5 High1 Volt Alarm			TranVol5 Hi1 Alm		Alm Trans Tens Haut1 Tension5		TransHaut1Tens5
739		32			15			TranVolt5 High2 Volt Alarm			TranVol5 Hi2 Alm		Alm Trans Tens Haut2 Tension5		TransHaut2Tens5		
740		32			15			TranVolt6 Low1 Volt Alarm			TranVol6 Lw1 Alm		Alm Trans Tens Bas1 Tension6		TransBas1Tens6		
741		32			15			TranVolt6 Low2 Volt Alarm			TranVol6 Lw2 Alm		Alm Trans Tens Bas2 Tension6		TransBas2Tens6		
742		32			15			TranVolt6 High1 Volt Alarm			TranVol6 Hi1 Alm		Alm Trans Tens Haut1 Tension6		TransHaut1Tens6
743		32			15			TranVolt6 High2 Volt Alarm			TranVol6 Hi2 Alm		Alm Trans Tens Haut2 Tension6		TransHaut2Tens6		
744		32			15			TranVolt7 Low1 Volt Alarm			TranVol7 Lw1 Alm		Alm Trans Tens Bas1 Tension7		TransBas1Tens7	
745		32			15			TranVolt7 Low2 Volt Alarm			TranVol7 Lw2 Alm		Alm Trans Tens Bas2 Tension7		TransBas2Tens7	
746		32			15			TranVolt7 High1 Volt Alarm			TranVol7 Hi1 Alm		Alm Trans Tens Haut1 Tension7		TransHaut1Tens7
747		32			15			TranVolt7 High2 Volt Alarm			TranVol7 Hi2 Alm		Alm Trans Tens Haut2 Tension7		TransHaut2Tens7	
748		32			15			TranVolt8 Low1 Volt Alarm			TranVol8 Lw1 Alm		Alm Trans Tens Bas1 Tension8		TransBas1Tens8
749		32			15			TranVolt8 Low2 Volt Alarm			TranVol8 Lw2 Alm		Alm Trans Tens Bas2 Tension8		TransBas2Tens8	
750		32			15			TranVolt8 High1 Volt Alarm			TranVol8 Hi1 Alm		Alm Trans Tens Haut1 Tension8		TransHaut1Tens8
751		32			15			TranVolt8 High2 Volt Alarm			TranVol8 Hi2 Alm		Alm Trans Tens Haut2 Tension8		TransHaut2Tens8		
752		32			15			TranVolt9 Low1 Volt Alarm			TranVol9 Lw1 Alm		Alm Trans Tens Bas1 Tension9		TransBas1Tens9	
753		32			15			TranVolt9 Low2 Volt Alarm			TranVol9 Lw2 Alm		Alm Trans Tens Bas2 Tension9		TransBas2Tens9	
754		32			15			TranVolt9 High1 Volt Alarm			TranVol9 Hi1 Alm		Alm Trans Tens Haut1 Tension9		TransHaut1Tens9
755		32			15			TranVolt9 High2 Volt Alarm			TranVol9 Hi2 Alm		Alm Trans Tens Haut2 Tension9		TransHaut2Tens9
756		32			15			TranVolt10 Low1 Volt Alarm			TranVol10 Lw1 Alm		Alm Trans Tens Bas1 Tension10		TransBas1Tens10	
757		32			15			TranVolt10 Low2 Volt Alarm			TranVol10 Lw2 Alm		Alm Trans Tens Bas2 Tension10		TransBas2Tens10	
758		32			15			TranVolt10 High1 Volt Alarm			TranVol10 Hi1 Alm		Alm Trans Tens Haut1 Tension10		TransH1Tens10
759		32			15			TranVolt10 High2 Volt Alarm			TranVol10 Hi2 Alm		Alm Trans Tens Haut2 Tension10		TransH2Tens10

760		32			15			Transducer1 High2 Curr Limit		Curr1 Hi2 Curr		Limite Courant Trans1 Haut 2	Cour1 H2Cour
761		32			15			Transducer1 High1 Curr Limit		Curr1 Hi1 Curr		Limite Courant Trans1 Haut 1	Cour1 H1Cour	
762		32			15			Transducer1 Low1 Curr Limit			Curr1 Lw1 Curr		Limite Courant Trans1 Bas1		Cour1 Bas1Cour
763		32			15			Transducer1 Low2 Curr Limit			Curr1 Lw2 Curr		Limite Courant Trans1 Bas2		Cour1 Bas2Cour
764		32			15			Transducer2 High2 Curr Limit		Curr2 Hi2 Curr		Limite Courant Trans2 Haut 2	Cour2 H2Cour
765		32			15			Transducer2 High1 Curr Limit		Curr2 Hi1 Curr		Limite Courant Trans2 Haut 1	Cour2 H1Cour
766		32			15			Transducer2 Low1 Curr Limit			Curr2 Lw1 Curr		Limite Courant Trans2 Bas1		Cour2 Bas1Cour
767		32			15			Transducer2 Low2 Curr Limit			Curr2 Lw2 Curr		Limite Courant Trans2 Bas2		Cour2 Bas2Cour
768		32			15			Transducer3 High2 Curr Limit		Curr3 Hi2 Curr		Limite Courant Trans3 Haut 2	Cour3 H2Cour
769		32			15			Transducer3 High1 Curr Limit		Curr3 Hi1 Curr		Limite Courant Trans3 Haut 1	Cour3 H1Cour
770		32			15			Transducer3 Low1 Curr Limit			Curr3 Lw1 Curr		Limite Courant Trans3 Bas1		Cour3 Bas1Cour
771		32			15			Transducer3 Low2 Curr Limit			Curr3 Lw2 Curr		Limite Courant Trans3 Bas2		Cour3 Bas2Cour
772		32			15			Transducer4 High2 Curr Limit		Curr4 Hi2 Curr		Limite Courant Trans4 Haut 2	Cour4 H2Cour
773		32			15			Transducer4 High1 Curr Limit		Curr4 Hi1 Curr		Limite Courant Trans4 Haut 1	Cour4 H1Cour
774		32			15			Transducer4 Low1 Curr Limit			Curr4 Lw1 Curr		Limite Courant Trans4 Bas1		Cour4 Bas1Cour
775		32			15			Transducer4 Low2 Curr Limit			Curr4 Lw2 Curr		Limite Courant Trans4 Bas2		Cour4 Bas2Cour
776		32			15			Transducer5 High2 Curr Limit		Curr5 Hi2 Curr		Limite Courant Trans5 Haut 2	Cour5 H2Cour
777		32			15			Transducer5 High1 Curr Limit		Curr5 Hi1 Curr		Limite Courant Trans5 Haut 1	Cour5 H1Cour
778		32			15			Transducer5 Low1 Curr Limit			Curr5 Lw1 Curr		Limite Courant Trans5 Bas1		Cour5 Bas1Cour
779		32			15			Transducer5 Low2 Curr Limit			Curr5 Lw2 Curr		Limite Courant Trans5 Bas2		Cour5 Bas2Cour
780		32			15			Transducer6 High2 Curr Limit		Curr6 Hi2 Curr		Limite Courant Trans6 Haut 2	Cour6 H2Cour
781		32			15			Transducer6 High1 Curr Limit		Curr6 Hi1 Curr		Limite Courant Trans6 Haut 1	Cour6 H1Cour
782		32			15			Transducer6 Low1 Curr Limit			Curr6 Lw1 Curr		Limite Courant Trans6 Bas1		Cour6 Bas1Cour
783		32			15			Transducer6 Low2 Curr Limit			Curr6 Lw2 Curr		Limite Courant Trans6 Bas2		Cour6 Bas2Cour
784		32			15			Transducer7 High2 Curr Limit		Curr7 Hi2 Curr		Limite Courant Trans7 Haut 2	Cour7 H2Cour
785		32			15			Transducer7 High1 Curr Limit		Curr7 Hi1 Curr		Limite Courant Trans7 Haut 1	Cour7 H1Cour
786		32			15			Transducer7 Low1 Curr Limit			Curr7 Lw1 Curr		Limite Courant Trans7 Bas1		Cour7 Bas1Cour
787		32			15			Transducer7 Low2 Curr Limit			Curr7 Lw2 Curr		Limite Courant Trans7 Bas2		Cour7 Bas2Cour
788		32			15			Transducer8 High2 Curr Limit		Curr8 Hi2 Curr		Limite Courant Trans8 Haut 2	Cour8 H2Cour
789		32			15			Transducer8 High1 Curr Limit		Curr8 Hi1 Curr		Limite Courant Trans8 Haut 1	Cour8 H1Cour
790		32			15			Transducer8 Low1 Curr Limit			Curr8 Lw1 Curr		Limite Courant Trans8 Bas1		Cour8 Bas1Cour
791		32			15			Transducer8 Low2 Curr Limit			Curr8 Lw2 Curr		Limite Courant Trans8 Bas2		Cour8 Bas2Cour
792		32			15			Transducer9 High2 Curr Limit		Curr9 Hi2 Curr		Limite Courant Trans9 Haut 2	Cour9 H2Cour
793		32			15			Transducer9 High1 Curr Limit		Curr9 Hi1 Curr		Limite Courant Trans9 Haut 1	Cour9 H1Cour
794		32			15			Transducer9 Low1 Curr Limit			Curr9 Lw1 Curr		Limite Courant Trans9 Bas1		Cour9 Bas1Cour
795		32			15			Transducer9 Low2 Curr Limit			Curr9 Lw2 Curr		Limite Courant Trans9 Bas2		Cour9 Bas2Cour
796		32			15			Transducer10 High2 Curr Limit		Curr10 Hi2 Curr		Limite Courant Trans10 Haut 2	Cour10 H2Cour
797		32			15			Transducer10 High1 Curr Limit		Curr10 Hi1 Curr		Limite Courant Trans10 Haut 1	Cour10 H1Cour
798		32			15			Transducer10 Low1 Curr Limit		Curr10 Lw1 Curr		Limite Courant Trans10 Bas1		Cour10 Bas1Cour
799		32			15			Transducer10 Low2 Curr Limit		Curr10 Lw2 Curr		Limite Courant Trans10 Bas2		Cour10 Bas2Cour

800		32			15			TranCurr1 Low1 Curr Alarm			TranCurr1 Lw1		TransAlmCourant1 Bas1		TranCour1Bas1	
801		32			15			TranCurr1 Low2 Curr Alarm			TranCurr1 Lw2		TransAlmCourant1 Bas2		TranCour1Bas2
802		32			15			TranCurr1 High1 Curr Alarm			TranCurr1 Hi1		TransAlmCourant1 Haut1		TranCour1Haut1
803		32			15			TranCurr1 High2 Curr Alarm			TranCurr1 Hi2		TransAlmCourant1 Haut2		TranCour1Haut2	
804		32			15			TranCurr2 Low1 Curr Alarm			TranCurr2 Lw1		TransAlmCourant2 Bas1		TranCour2Bas1	
805		32			15			TranCurr2 Low2 Curr Alarm			TranCurr2 Lw2		TransAlmCourant2 Bas2		TranCour2Bas2
806		32			15			TranCurr2 High1 Curr Alarm			TranCurr2 Hi1		TransAlmCourant2 Haut1		TranCour2Haut1
807		32			15			TranCurr2 High2 Curr Alarm			TranCurr2 Hi2		TransAlmCourant2 Haut2		TranCour2Haut2	
808		32			15			TranCurr3 Low1 Curr Alarm			TranCurr3 Lw1		TransAlmCourant3 Bas1		TranCour3Bas1		
809		32			15			TranCurr3 Low2 Curr Alarm			TranCurr3 Lw2		TransAlmCourant3 Bas2		TranCour3Bas2
810		32			15			TranCurr3 High1 Curr Alarm			TranCurr3 Hi1		TransAlmCourant3 Haut1		TranCour3Haut1
811		32			15			TranCurr3 High2 Curr Alarm			TranCurr3 Hi2		TransAlmCourant3 Haut2		TranCour3Haut2		
812		32			15			TranCurr4 Low1 Curr Alarm			TranCurr4 Lw1		TransAlmCourant4 Bas1		TranCour4Bas1		
813		32			15			TranCurr4 Low2 Curr Alarm			TranCurr4 Lw2		TransAlmCourant4 Bas2		TranCour4Bas2
814		32			15			TranCurr4 High1 Curr Alarm			TranCurr4 Hi1		TransAlmCourant4 Haut1		TranCour4Haut1
815		32			15			TranCurr4 High2 Curr Alarm			TranCurr4 Hi2		TransAlmCourant4 Haut2		TranCour4Haut2		
816		32			15			TranCurr5 Low1 Curr Alarm			TranCurr5 Lw1		TransAlmCourant5 Bas1		TranCour5Bas1
817		32			15			TranCurr5 Low2 Curr Alarm			TranCurr5 Lw2		TransAlmCourant5 Bas2		TranCour5Bas2
818		32			15			TranCurr5 High1 Curr Alarm			TranCurr5 Hi1		TransAlmCourant5 Haut1		TranCour5Haut1
819		32			15			TranCurr5 High2 Curr Alarm			TranCurr5 Hi2		TransAlmCourant5 Haut2		TranCour5Haut2
820		32			15			TranCurr6 Low1 Curr Alarm			TranCurr6 Lw1		TransAlmCourant6 Bas1		TranCour6Bas1
821		32			15			TranCurr6 Low2 Curr Alarm			TranCurr6 Lw2		TransAlmCourant6 Bas2		TranCour6Bas2
822		32			15			TranCurr6 High1 Curr Alarm			TranCurr6 Hi1		TransAlmCourant6 Haut1		TranCour6Haut1
823		32			15			TranCurr6 High2 Curr Alarm			TranCurr6 Hi2		TransAlmCourant6 Haut2		TranCour6Haut2
824		32			15			TranCurr7 Low1 Curr Alarm			TranCurr7 Lw1		TransAlmCourant7 Bas1		TranCour7Bas1
825		32			15			TranCurr7 Low2 Curr Alarm			TranCurr7 Lw2		TransAlmCourant7 Bas2		TranCour7Bas2
826		32			15			TranCurr7 High1 Curr Alarm			TranCurr7 Hi1		TransAlmCourant7 Haut1		TranCour7Haut1
827		32			15			TranCurr7 High2 Curr Alarm			TranCurr7 Hi2		TransAlmCourant7 Haut2		TranCour7Haut2
828		32			15			TranCurr8 Low1 Curr Alarm			TranCurr8 Lw1		TransAlmCourant8 Bas1		TranCour8Bas1
829		32			15			TranCurr8 Low2 Curr Alarm			TranCurr8 Lw2		TransAlmCourant8 Bas2		TranCour8Bas2
830		32			15			TranCurr8 High1 Curr Alarm			TranCurr8 Hi1		TransAlmCourant8 Haut1		TranCour8Haut1
831		32			15			TranCurr8 High2 Curr Alarm			TranCurr8 Hi2		TransAlmCourant8 Haut2		TranCour8Haut2
832		32			15			TranCurr9 Low1 Curr Alarm			TranCurr9 Lw1		TransAlmCourant9 Bas1		TranCour9Bas1	
833		32			15			TranCurr9 Low2 Curr Alarm			TranCurr9 Lw2		TransAlmCourant9 Bas2		TranCour9Bas2
834		32			15			TranCurr9 High1 Curr Alarm			TranCurr9 Hi1		TransAlmCourant9 Haut1		TranCour9Haut1
835		32			15			TranCurr9 High2 Curr Alarm			TranCurr9 Hi2		TransAlmCourant9 Haut2		TranCour9Haut2	
836		32			15			TranCurr10 Low1 Curr Alarm			TranCurr10 Lw1		TransAlmCourant10 Bas1		TranCour10Bas1
837		32			15			TranCurr10 Low2 Curr Alarm			TranCurr10 Lw2		TransAlmCourant10 Bas2		TranCour10Bas2
838		32			15			TranCurr10 High1 Curr Alarm			TranCurr10 Hi1		TransAlmCourant10 Haut1		TranCour10Haut1
839		32			15			TranCurr10 High2 Curr Alarm			TranCurr10 Hi2		TransAlmCourant10 Haut2		TranCour10Haut2

840		32			15			Transducer1 Units		Trans1 Units		Transducer1 Unités		Trans1 Unités	
841		32			15			Transducer2 Units		Trans2 Units		Transducer2 Unités		Trans2 Unités	
842		32			15			Transducer3 Units		Trans3 Units		Transducer3 Unités		Trans3 Unités	
843		32			15			Transducer4 Units		Trans4 Units		Transducer4 Unités		Trans4 Unités	
844		32			15			Transducer5 Units		Trans5 Units		Transducer5 Unités		Trans5 Unités	
845		32			15			Transducer6 Units		Trans6 Units		Transducer6 Unités		Trans6 Unités	
846		32			15			Transducer7 Units		Trans7 Units		Transducer7 Unités		Trans7 Unités	
847		32			15			Transducer8 Units		Trans8 Units		Transducer8 Unités		Trans8 Unités	
848		32			15			Transducer9 Units		Trans9 Units		Transducer9 Unités		Trans9 Unités	
849		32			15			Transducer10 Units		Trans10 Units		Transducer10 Unités		Trans10 Unités

850		32			15			None			None			Aucun			Aucun	
851		32			15			Volts DC		Volts DC		Tension DC		Tension DC	
852		32			15			Volts AC		Volts AC		Tension AC		Tension AC	
853		32			15			Amps DC			Amps DC			Amplis DC		Amplis DC	
854		32			15			Amps AC			Amps AC			Amplis AC		Amplis AC	
855		32			15			Watts			Watts			Watts			Watts	
856		32			15			KW				KW				KW				KW		
857		32			15			Gallons			Gallons			Gallons			Gallons	
858		32			15			Liters			Liters			Litres			Litres	
859		32			15			PSI				PSI				PSI			PSI	
860		32			15			CFM				CFM				CFM			CFM		
861		32			15			RPM				RPM				RPM			RPM	
862		32			15			BTU				BTU				BTU			BTU
863		32			15			HP				HP				HP			HP	
864		32			15			W/hr			W/hr			W/hr		W/hr	
865		32			15			KW/hr			KW/hr			KW/hr		KW/hr
866		32			15			None			None			None		None

﻿#
#  Locale language support: French
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15		Battery Current				Battery Current			Courant				Courant
2		32			15			Battery Rating(Ah)			Batt Rating(Ah)		Capacite batterie(Ah)		Cap. bat(Ah)
3		32			15			Battery Current Limit Exceeded		Ov Bat Cur Lmt		Depacement limit.Courant	Depace I limit
4		32			15			CSU Battery					CSU Battery		Batterie CSU			Batterie CSU
5		32			15			Over Battery Current			Over Batt Curr		Depacement sur-courant		Depace sur-I
6		32			15			Battery Capacity (%)			Batt Cap (%)		Capacite batterie(%)		Capacite bat(%)
7		32			15			Battery Voltage				Battery Voltage			Tension batterie		Tension bat
8		32			15			Battery Low Capacity			BattLowCapacity			Capacite basse			Capacite basse
9		32			15			CSU Battery Temperature				CSU Batt Temp		Température Batterire CSU	Temp Bat CSU
10		32			15			CSU Battery Failure				CSU Batt Fail		Défaut batterie CSU		Déf Bat CSU
11		32			15			Existent					Existent		Présent				Présent
12		32			15			Not Existent				Not Existent		Non Présent			Non Présent
28		32			15			Battery Management		Batt Management		Utilisé en Gestion Bat		Util Gest Bat
29		32			15			Yes						Yes			Oui				Oui
30		32			15			No						No			Non				Non
96		32			15			Rated Capacity					Rated Capacity		Capacitée nominal C10		Capacitée C10

﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
zh



[RES_INFO]
#R_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE		
1		32			15			Li-Ion Battery String		LiBattString		Li电池单元			Li电池单元
2		32			15			Batt Voltage			Batt Voltage		电池电压			电池电压
3		32			15			Battery Terminal Voltage	Batt Term Volt		终端电压			终端电压
4		32			15			Battery Current			Batt Current		电池电流			电池电流
5		32			15			Cell Temperature		Cell Temp		单体温度			单体温度
6		32			15			Switch Temperature		Switch Temp		节间温度			节间温度
7		32			15			Capacity of Charge		Cap of Charge	充电容量百分比			充电百分比
8		32			15			Battery LED Status		Batt LED Status		LED指示灯状态			LED灯状态
9		32			15			Battery Relay Status		BattRelayStatus		电池告警继电器告警		继电器告警
10		32			15			Charge Enabled			Charge Enabled		充电使能			充电使能
11		32			15			Battery Discharging		Batt Discharge		放电使能			放电使能
12		32			15			Battery Charging		Batt Charge		正在充电			正在充电
13		32			15			DisCharging			DisCharging		正在放电			正在放电
14		32			15			Charging(5A)			Charging(5A)		正在充电(5A)			正在充电(5A)
15		32			15			DisCharging(5A)			DisCharging(5A)		正在放电(5A)			正在放电(5A)
16		32			15			DisChar Enabled14		DisCharEnable14		放电使能14			放电使能14
17		32			15			Char Enabled15			Char Enabled15		充电使能15			充电使能15

18		32			15			Battery Temperature Fail	Batt Temp Fail		温度失效			温度失效
19		32			15			Battery Current Fail		Current Fail		电流告警			电流告警
20		32			15			Battery Hardware Failure	Hardware Fail		硬件告警			硬件告警
21		32			15			Battery Over-voltage		Over-volt		过压				过压
22		32			15			Battery Low-Voltage		Low-volt		欠压				欠压
23		32			15			Cell Volt Deviation		CellVoltDeviat		节电压偏离			节电压偏离
24		32			15			Low Cell Voltage		Lo Cell Volt		节电压低			节电压低
25		32			15			High Cell Voltage		Hi Cell Volt		节电压高			节电压高
26		32			15			High Cell Temperature		Hi Cell Temp		节温度高			节温度高
27		32			15			High Switch DisTemp		HiSwitchDisTemp		放电节间温度高			放电节间温度高
28		32			15			Charge Short Circuit		Char ShortCirc		充电短路			充电短路
29		32			15			DisChar Short Circuit		DisChar SC		放电短路			放电短路
30		32			15			High Switch CharTemp		Hi SW Char Temp		充电节间温度高			充电节间温度高
31		32			15			Hardware Fail 20		HardwareFail20		硬件失败20			硬件失败20
32		32			15			Hardware Fail 21		HardwareFail21		硬件失败21			硬件失败21
33		32			15			High Charge Current		Hi Charge Curr		充电电流高			充电电流高
34		32			15			High DisCharge Current		Hi Dischar Curr		放电电流高			放电电流高
35		32			15			Communication Fail		Comm Fail		通信中断			通信中断
36		32			15			Address Of Li-Ion Module	Module Address		Li电池地址			Li电池地址

37		32			15			Alarm				Alarm			告警				告警
38		32			15			Normal				Normal			正常				正常

39		32			15			Full On Green			Full on Green		绿色常亮			绿色常亮
40		32			15			Blinking Green			Blink Green		绿色闪烁			绿色闪烁
41		32			15			Full On Yellow			Full on Yellow		黄色常亮			黄色常亮
42		32			15			Blinking Yellow			Blink Yellow		黄色闪烁			黄色闪烁
43		32			15			Blinking Red			Blink Red		红色闪烁			红色闪烁
44		32			15			Full On Red			Full on Red		红色常亮			红色常亮
45		32			15			LED Off				LED Off			LED灯灭				LED灯灭
46		32			15			Batt Rating(Ah)			Batt Rating(Ah)		电池容量(Ah)			电池容量(Ah)

48		32			15			Battery Disconnected Status	Batt Disconnect		电池未连接			电池未连接
49		32			15			Battery Disconnected		Batt Disconnect		电池未连接			电池未连接

96		32			15			Rated Capacity			Rated Capacity		标称容量			标称容量
99		32			15			Communication Fail		Comm Fail		中断状态			中断状态
100		32			15			Exist State			Exist State		存在状态			存在状态
101		32			15			True				True			是				是
102		32			15			False				False			否				否


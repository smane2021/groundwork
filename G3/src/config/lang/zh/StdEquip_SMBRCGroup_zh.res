﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			SMBRC Group				SMBRC Group		SMBRC组			SMBRC组
2		32			15			Standby					Standby		待命			待命
3		32			15			Refresh					Refresh			刷新			刷新
4		32			15			Setting Refresh				Setting Refresh		设置信号刷新		设置信号刷新
5		32			15			E-Stop					E-Stop			E-Stop			E-Stop
6		32			15			Yes					Yes			是			是
7		32			15			Existence State				Existence State		是否存在		是否存在
8		32			15			Existent				Existent		存在			存在
9		32			15			Not Existent				Not Existent		不存在			不存在
10		32			15			Num of SMBRCs				Num of SMBRCs		SM-BRC数量		SM-BRC数量
11		32			15			SMBRC Config Changed			Cfg Changed		配置是否改变		配置是否改变
12		32			15			Not Changed				Not Changed		未改变			未改变
13		32			15			Changed					Changed			已改变			已改变
14		32			15			SMBRC Configuration			SMBRC Cfg		SM-BRC配置		SM-BRC配置
15		32			15			SMBRC Configuration Number		SMBRC Cfg Num		SMBRC配置值		SMBRC配置值
16		32			15			SMBRC Interval				SMBRC Interval		SMBRC阻抗间隔		SMBRC阻抗间隔
17		32			15			SMBRC Reset Test			SMBRC Rst Test		SMBRC阻抗测试		SMBRC阻抗测试
18		32			15			Start					Start			开始			开始
19		32			15			Stop					Stop			停止			停止
20		32			15			Cell Voltage High			Cell Volt High		节电压上限		节电压上限
21		32			15			Cell Voltage Low			Cell Volt Low		节电压下限		节电压下限
22		32			15			Cell Temperature High			Cell Temp High		节温度上限		节温度上限
23		32			15			Cell Temperature Low			Cell Temp Low		节温度下限		节温度下限
24		32			15			String Current High			String Curr Hi		串电流上限		串电流上限
25		32			15			Ripple Current High			Ripple Curr Hi		纹波电流上限		纹波电流上限
26		32			15			High Cell Resistance			Hi Cell Resist		阻抗上限		阻抗上限
27		32			15			Low Cell Resistance			Low Cell Resist		阻抗下限		阻抗下限
28		32			15			High Intercell Resistance		Hi InterResist		内阻上限		内阻上限
29		32			15			Low Intercell Resistance		Low InterResist		内阻下限		内阻下限
30		32			15			String Current Low			String Curr Low		串电流下限		串电流下限
31		32			15			Ripple Current Low			Ripple Curr Low		纹波电流下限		纹波电流下限



﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			LVD Unit			LVD Unit			LVD			LVD
2		32			15			LargeDU LVD			LargeDU LVD		LargDU LVD		LargDU LVD
11		32			15			Connected			Connected		连接			连接
12		32			15			Disconnected			Disconnected		断开			断开
13		32			15			No				No			否			否
14		32			15			Yes				Yes			是			是
21		32			15			LVD1 Status			LVD1 Status		LVD1状态		LVD1状态
22		32			15			LVD2 Status			LVD2 Status		LVD2状态		LVD2状态
23		32			15			LVD1 Disconnected		LVD1 Disconnect		LVD1下电		LVD1下电
24		32			15			LVD2 Disconnected		LVD2 Disconnect		LVD2下电		LVD2下电
25		32			15			Communication Failure		Comm Failure		通信中断		通信中断
26		32			15			State				State			工作状态		工作状态
27		32			15			LVD1 Control			LVD1 Control		LVD1控制		LVD1控制
28		32			15			LVD2 Control			LVD2 Control		LVD2控制		LVD2控制
31	32			15		LVD1			LVD1		LVD1允许		LVD1允许
32	32			15		LVD1 Mode			LVD1 Mode		LVD1方式		LVD1方式
33		32			15			LVD1 Voltage			LVD1 Voltage		LVD1电压		LVD1电压
34		32			15			LVD1 Reconnect Voltage		LVD1 Recon Volt		LVD1上电电压		LVD1上电电压
35	32			15		LVD1 Reconnect Delay		LVD1 ReconDelay		LVD1上电延迟		LVD1上电延迟
36		32			15			LVD1 Time			LVD1 Time		LVD1时间		LVD1时间
37		32			15			LVD1 Dependency			LVD1 Dependency		LVD1依赖关系		LVD1依赖关系
41	32			15		LVD2			LVD2		LVD2允许		LVD2允许
42	32			15		LVD2 Mode			LVD2 Mode		LVD2方式		LVD2方式
43		32			15			LVD2 Voltage			LVD2 Voltage		LVD2电压		LVD2电压
44		32			15			LVD2 Reconnect Voltage		LVD2 Recon Volt		LVD2上电电压		LVD2上电电压
45	32			15		LVD2 Reconnect Delay		LVD2 ReconDelay		LVD2上电延迟		LVD2上电延迟
46		32			15			LVD2 Time			LVD2 Time		LVD2时间		LVD2时间
47		32			15			LVD2 Dependency			LVD2 Dependency		LVD2依赖关系		LVD2依赖关系
51		32			15			Disabled			Disabled		禁止			禁止
52		32			15			Enabled				Enabled			允许			允许
53		32			15			Voltage				Voltage			电压方式		电压方式
54		32			15			Time				Time			时间方式		时间方式
55		32			15			None				None			无			无
56	32			15		LVD1				LVD1			LVD1			LVD1
57	32			15		LVD2				LVD2			LVD2			LVD2
103	32			15		High Temp Disconnect 1	HTD1		HTD1高温下电允许	HTD1下电允许
104	32			15		High Temp Disconnect 2	HTD2		HTD2高温下电允许	HTD2下电允许
105	32			15		Battery LVD			Battery LVD		电池下电		电池下电
110		32			15			Communication Fail		Comm Fail		通讯中断		通讯中断
111		32			15			Times of Communication Fail	Times Comm Fail		通信失败次数		通信失败次数
116		32			15			LVD1 Contactor Failure		LVD1 Failure		LVD1控制失败		LVD1接触器故障
117		32			15			LVD2 Contactor Failure		LVD2 Failure		LVD2控制失败		LVD2接触器故障
118		32			15			DC Distribution Number		DC Distr Num		直流屏号		直流屏号







﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			Voltage L1-N		Volt L1-N		电压L1-N		电压L1-N
2		32			15			Voltage L2-N		Volt L2-N		电压L2-N		电压L2-N
3		32			15			Voltage L3-N		Volt L3-N		电压L3-N		电压L3-N
4		32			15			Voltage L1-L2		Volt L1-L2		电压L1-L2		电压L1-L2
5		32			15			Voltage L2-L3		Volt L2-L3		电压L2-L3		电压L2-L3
6		32			15			Voltage L3-L1		Volt L3-L1		电压L3-L1		电压L3-L1
7		32			15			Current L1		Curr L1			电流L1			电流L1
8		32			15			Current L2		Curr L2			电流L2			电流L2
9		32			15			Current L3		Curr L3			电流L3			电流L3
10		32			15			Watt L1			Watt L1			瓦特L1			瓦特L1
11		32			15			Watt L2			Watt L2			瓦特L2			瓦特L2
12		32			15			Watt L3			Watt L3			瓦特L3			瓦特L3

13		32			15			VA L1			VA L1			伏安L1			伏安L1
14		32			15			VA L2			VA L2			伏安L2			伏安L2
15		32			15			VA L3			VA L3			伏安L3			伏安L3

16		32			15			VAR L1			VAR L1			有功功率L1		有功功率L1
17		32			15			VAR L2			VAR L2			有功功率L2		有功功率L2
18		32			15			VAR L3			VAR L3			有功功率L3		有功功率L3

19		32			15			AC Frequency		AC Frequency		交流频率		交流频率



20		32			15			Communication State	Comm State		中断状态		中断状态
21		32			15			Existence State		Existence State		是否存在		是否存在

22		32			15			AC Meter		AC Meter		交流电表		交流电表


23		32			15			On					On			on			on
24		32			15			Off					Off			off			off

25		32			15			Existent		Existent		存在			存在
26		32			15			Not Existent		Not Existent		不存在			不存在
27		32			15			Communication Fail			Comm Fail		通讯中断		通讯中断

28		32			15			V L-N ACC		V L-N ACC		V L-N ACC		V L-N ACC
29		32			15			V L-L ACC		V L-L ACC		V L-L ACC		V L-L ACC
30		32			15			Watt ACC		Watt ACC		Watt ACC		Watt ACC
31		32			15			VA ACC			VA ACC			VAR ACC			VAR ACC
32		32			15			VAR ACC			VAR ACC			VAR ACC			VAR ACC
33		32			15			DMD Watt ACC		DMD Watt ACC		DMD Watt ACC		DMD Watt ACC
34		32			15			DMD VA ACC		DMD VA ACC		DMD VA ACC		DMD VA ACC
35		32			15			PF L1			PF L1			PF L1			PF L1
36		32			15			PF L2			PF L2			PF L2			PF L2
37		32			15			PF L3			PF L3			PF L3			PF L3
38		32			15			PF ACC			PF ACC			PF ACC			PF ACC
39		32			15			Phase Sequence		Phase Sequence		Phase Sequence		Phase Sequence
40		32			15			L1-L2-L3		L1-L2-L3		L1-L2-L3		L1-L2-L3
41		32			15			L1-L3-L2		L1-L3-L2		L1-L3-L2		L1-L3-L2


42	32			15		Nominal Line Voltage			NominalLineVolt		标称线电压		标称线电压
43	32			15		Nominal Phase Voltage			Nominal PH-Volt		标称相电压		标称相电压
44	32			15		Nominal Frequency			Nominal Freq		标称频率		标称频率
45	32			15		Mains Failure Alarm Limit 1		Mains Fail Alm1		电压告警门限1		电压告警门限1
46	32			15		Mains Failure Alarm Limit 2		Mains Fail Alm2		电压告警门限2		电压告警门限2
47	32			15		Frequency Alarm Limit			Freq Alarm Lmt		频率告警门限		频率告警门限
48	32			15			Current Alarm Limit			Curr Alm Limit		AC过流告警点		AC过流告警点

51	32			15		Line AB Over Voltage 1			L-AB Over Volt1		线电压AB高压		线电压AB高压
52	32			15			Line AB Over Voltage 2			L-AB Over Volt2		线电压AB超高		线电压AB超高
53	32			15			Line AB Under Voltage 1			L-AB UnderVolt1		线电压AB低压		线电压AB低压
54	32			15			Line AB Under Voltage 2			L-AB UnderVolt2		线电压AB超低		线电压AB超低
55	32			15			Line BC Over Voltage 1			L-BC Over Volt1		线电压BC高压		线电压BC高压
56	32			15			Line BC Over Voltage 2			L-BC Over Volt2		线电压BC超高		线电压BC超高
57	32			15			Line BC Under Voltage 1			L-BC UnderVolt1		线电压BC低压		线电压BC低压
58	32			15			Line BC Under Voltage 2			L-BC UnderVolt2		线电压BC超低		线电压BC超低
59	32			15			Line CA Over Voltage 1			L-CA Over Volt1		线电压CA高压		线电压CA高压
60	32			15			Line CA Over Voltage 2			L-CA Over Volt2		线电压CA超高		线电压CA超高
61	32			15			Line CA Under Voltage 1			L-CA UnderVolt1		线电压CA低压		线电压CA低压
62	32			15			Line CA Under Voltage 2			L-CA UnderVolt2		线电压CA超低		线电压CA超低
63	32			15			Phase A Over Voltage 1			PH-A Over Volt1		A相电压高		A相电压高
64	32			15			Phase A Over Voltage 2			PH-A Over Volt2		A相电压超高		A相电压超高
65	32			15			Phase A Under Voltage 1			PH-A UnderVolt1		A相电压低		A相电压低
66	32			15			Phase A Under Voltage 2			PH-A UnderVolt2		A相电压超低		A相电压超低
67	32			15			Phase B Over Voltage 1			PH-B Over Volt1		B相电压高		B相电压高
68	32			15			Phase B Over Voltage 2			PH-B Over Volt2		B相电压超高		B相电压超高
69	32			15			Phase B Under Voltage 1			PH-B UnderVolt1		B相电压低		B相电压低
70	32			15			Phase B Under Voltage 2			PH-B UnderVolt2		B相电压超低		B相电压超低
71	32			15			Phase C Over Voltage 1			PH-C Over Volt1		C相电压高		C相电压高
72	32			15			Phase C Over Voltage 2			PH-C Over Volt2		C相电压超高		C相电压超高
73	32			15			Phase C Under Voltage 1			PH-C UnderVolt1		C相电压低		C相电压低
74	32			15			Phase C Under Voltage 2			PH-C UnderVolt2		C相电压超低		C相电压超低
75	32			15			Mains Failure				Mains Failure		交流停电		交流停电
76	32			15			Severe Mains Failure			SevereMainsFail		严重市电失败		严重市电失败
77	32			15			High Frequency				High Frequency		高频			高频
78	32			15			Low Frequency				Low Frequency		低频			低频
79	32			15			Phase A High Current			PH-A High Curr		A相过流			A相过流
80	32			15			Phase B High Current			PH-B High Curr		B相过流			B相过流
81	32			15			Phase C High Current			PH-C High Curr		C相过流			C相过流

82	32			15			DMD W MAX				DMD W MAX		DMD W MAX		DMD W MAX
83	32			15			DMD VA MAX				DMD VA MAX		DMD VA MAX		DMD VA MAX
84	32			15			DMD A MAX				DMD A MAX		DMD A MAX		DMD A MAX
85	32			15			KWH(+) TOT				KWH(+) TOT		KWH(+) TOT		KWH(+) TOT
86	32			15			KVARH(+) TOT				KVARH(+) TOT		KVARH(+) TOT		KVARH(+) TOT
87	32			15			KWH(+) PAR				KWH(+) PAR		KWH(+) PAR		KWH(+) PAR
88	32			15			KVARH(+) PAR				KVARH(+) PAR		KVARH(+) PAR		KVARH(+) PAR
89	32			15			KWH(+) L1				KWH(+) L1		KWH(+) L1		KWH(+) L1
90	32			15			KWH(+) L2				KWH(+) L2		KWH(+) L2		KWH(+) L2
91	32			15			KWH(+) L3				KWH(+) L3		KWH(+) L3		KWH(+) L3
92	32			15			KWH(+) T1				KWH(+) T1		KWH(+) T1		KWH(+) T1
93	32			15			KWH(+) T2				KWH(+) T2		KWH(+) T2		KWH(+) T2
94	32			15			KWH(+) T3				KWH(+) T3		KWH(+) T3		KWH(+) T3
95	32			15			KWH(+) T4				KWH(+) T4		KWH(+) T4		KWH(+) T4
96	32			15			KVARH(+) T1				KVARH(+) T1		KVARH(+) T1		KVARH(+) T1
97	32			15			KVARH(+) T2				KVARH(+) T2		KVARH(+) T2		KVARH(+) T2
98	32			15			KVARH(+) T3				KVARH(+) T3		KVARH(+) T3		KVARH(+) T3
99	32			15			KVARH(+) T4				KVARH(+) T4		KVARH(+) T4		KVARH(+) T4

100	32			15			KWH(-) TOT				KWH(-) TOT		KWH(-) TOT		KWH(-) TOT
101	32			15			KVARH(-) TOT				KVARH(-) TOT		KVARH(-) TOT		KVARH(-) TOT
102	32			15			HOUR					HOUR			HOUR			HOUR
103	32			15			COUNTER 1				COUNTER 1		COUNTER 1		COUNTER 1
104	32			15			COUNTER 2				COUNTER 2		COUNTER 2		COUNTER 2
105	32			15			COUNTER 3				COUNTER 3		COUNTER 3		COUNTER 3

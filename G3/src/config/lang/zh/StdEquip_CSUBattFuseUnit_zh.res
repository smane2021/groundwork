﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1		32			15			Battery Fuse Tripped		Batt Fuse Trip		Bat_fuse tripped	熔丝触发
2		32			15			Battery 2 Fuse Tripped		Bat 2 Fuse Trip		Battery 2 fuse tripped	Bat 2 fuse trip
3		32			15			Battery 1 Voltage		Batt 1 Voltage		Battery 1 voltage	Bat 1 voltage
4		32			15			Battery 1 Current		Batt 1 Current		Battery 1 current	Bat 1 current
5		32			15			Battery 1 Temperature		Battery 1 Temp		Battery 1 temp		Battery 1 temp
6		32			15			Battery 2 Voltage		Batt 2 Voltage		Battery 2 voltage	Bat 2 voltage 
7		32			15			Battery 2 Current		Batt 2 Current		Battery 2 current	Bat 2 current 
8		32			15			Battery 2 Temperature		Battery 2 Temp		Battery 2 temp		Battery 2 temp
9		32			15			CSU Battery			CSU Battery		CSU_Battery		CSU_Battery
10		32			15			CSU Battery Failure		CSU BatteryFail		CSU_Battery failure	CSU_Batteryfail
11		32			15			No				No			否			否
12		32			15			Yes				Yes			是			是
13		32			15			Battery 2 Connected		Bat 2 Connected		Battery 2 connected	Batt2connected
14		32			15			Battery 1 Connected		Bat 1 Connected		Battery 1 connected	Batt1connected
15		32			15			Existent			Existent		存在			存在
16		32			15			Not Existent			Not Existent		不存在			不存在









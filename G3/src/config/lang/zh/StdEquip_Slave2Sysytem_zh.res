﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN			FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15			System Voltage					System Voltage			系统电压		系统电压	
2	32			15			Rectifiers Number				Num of G2Rect			整流模块数		组2模块数	
3	32			15			Total Rectifier Current				Total Rect Curr			输出电流		输出电流
4	32			15			Rectifier Lost					Rectifier Lost			模块丢失		模块丢失
5	32			15			All Rectifiers Comm Fail			AllRectCommFail			所有模块通信中断	所有模块通信断
6	32			15			Comm Fail				Comm Fail		通信中断		通信中断
7	32			15			Existence State					Existence State			是否存在		是否存在
8	32			15			Existent					Existent			存在			存在
9	32			15			Not Existent					Not Existent			不存在			不存在
10	32			15			Normal						Normal				正常			正常	
11	32			15			Fail					Fail			故障			故障	
12	32			15			Rectifier Current Limit				Current Limit			模块限流		模块限流	
13	32			15			Rectifiers Trim					Rect Trim			模块调压		模块调压	
14	32			15			DC On/Off Control				DC On/Off Ctrl			模块直流开关机		模块直流开关机	
15	32			15			AC On/Off Control				AC On/Off Ctrl			模块交流开关机		模块交流开关机	
16	32			15			G2 Rectifiers LED Control			Rects LED Ctrl			模块LED灯控制		模块LED灯控制	
17	32			15			Switch Off All					Switch Off All			关所有模块		关所有模块	
18	32			15			Switch On All					Switch On All			开所有模块		开所有模块	
19	32			15			All						All			全部灯闪		全部灯闪	
20	32			15			Stop Flashing					Stop Flashing			全部灯不闪		全部灯不闪	
21	32			32			Current Limit Control				Curr Limit Ctrl			限流点控制		限流点控制	
22	32			32			Full Capability Control				Full Capab Ctrl			满容量运行		满容量运行	
23	32			15			G2 Clear Rectifier Lost Alarm		G2 ClrRectLost				清除模块丢失告警	清模块丢失告警	
24	32			15			G2 Reset Cycle Alarm			G2 ClrCycleAlm				复位节能振荡告警	复位振荡告警
25	32			15			Yes					Yes				清除			清除
26	32			15			Rectifier Group II				Rect Group II			模块组II			模块组II
27	32			15			E-Stop Function					E-Stop Function			E-Stop功能		E-Stop功能
36	32			15			Normal					Normal			正常				正常	
37	32			15			Failure					Failure			故障				故障	
38	32			15			Switch Off All					Switch Off All			关所有模块		关所有模块	
39	32			15			Switch On All					Switch On All			开所有模块		开所有模块           
83	32			15			No						No				否			否
84	32			15			Yes						Yes				是			是
96	32			15			Input Current Limit			Input Curr Lmt		输入电流限值		输入电流限值
97	32			15			Mains Failure				Mains Failure		交流停电		交流停电
98	32			15			G2 Clear Rect Comm Fail Alarm		G2 ClrCommFail		清模块通讯中断告警		清模块通讯中断
99	32			15			System Capacity Used			Sys Cap Used		模块组使用容量			模块组使用容量	
100	32			15			Maximum Used Capacity			Max Cap Used		最大使用容量			最大使用容量	
101	32			15			Minimum Used Capacity			Min Cap Used		最小使用容量			最小使用容量	
102	32			15			Total Rated Current		Total Rated Cur		工作模块总额定电流		工作模块总额定
103	32			15			Total Rectifiers Communicating		Num Rects Comm		通讯正常模块数			通讯正常模块数	
104	32			15			Rated Voltage				Rated Voltage		额定电压			额定电压
105	32			15			G2 Fan Speed Control			Fan Speed Ctrl		风扇运行速度			风扇运行速度	
106	32			15			Full Speed				Full Speed		全速				全速	
107	32			15			Automatic Speed				Auto Speed		自动调速			自动调速	
108	32			15			G2 Confirm Rectifier ID/Feed		Confirm ID/Feed		确认模块位置和相位		确认位置和相位	
109	32			15			Yes					Yes			是				是	
110	32			15			Multiple Rectifiers Fail	Multi-Rect Fail		多模块故障			多模块故障	
111	32			15			Total Output Power			OutputPower		总功率				总功率
112	32			15			G2 Reset Rectifier IDs			G2 ResetRectIDs		G2清除模块位置号		G2清除模块号

﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			SMDUH Group		SMDUH Group		SMDUH组			SMDUH组
2		32			15			Standby		Standby		待命			待命
3		32			15			Refresh			Refresh			刷新			刷新
4		32			15			Setting Refresh		Setting Refresh		设置信号刷新		设置信号刷新
5		32			15			E-Stop		E-Stop			E-Stop		E-Stop
6		32			15			Yes			Yes			是			是
7		32			15			Existence State		Existence State		是否存在		是否存在
8		32			15			Existent		Existent		存在			存在
9		32			15			Not Existent		Not Existent		不存在			不存在
10		32			15			Number of SMDUHs	Num of SMDUHs		SMDUH数量		SMDUH数量
11		32			15			SMDUH Config Changed	Cfg Changed		配置是否改变		配置是否改变
12		32			15			Not Changed		Not Changed		未改变			未改变
13		32			15			Changed			Changed			已改变			已改变


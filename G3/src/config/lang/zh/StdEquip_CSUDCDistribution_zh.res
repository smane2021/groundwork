﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE			
1		32			15			Distribution Fuse Tripped		Dist Fuse Trip		Distribution fuse tripped	Dist fuse trip	
2		32			15			Contactor 1 Failure		Contactor1 Fail		Contactor 1 failure		Contactor1 fail	
3		32			15			Distribution Fuse 2 Tripped		Dist Fuse2 Trip		Distribution fuse 2 tripped	Dist fuse2 trip	
4		32			15			Contactor 2 Failure		Contactor2 Fail		Contactor 2 failure		Contactor2 fail	
5		32			15			Distribution Voltage		Distr Voltage		Distribution voltage		Dist voltage	
6		32			15			Distribution Current		Distr Current		Distribution current		Dist current	
7		32			15			Distribution Temperature		Distr Temp		Distribution temperature	配电温度
8		32			15			Distribution 2 Current		Distr 2 Current		Distribution current 2		Dist current2	
9		32			15			Distribution 2 Voltage			Distr 2 Voltage		Distribution voltage fuse 2	Dist volt fuse2	
10		32			15			CSU DC Distribution			CSU DC Distr		CSU_DCDistribution		CSU_DCDistri
11		32			15			CSU DC Distribution Failure		CSU DCDist Fail		CSU_DCDistri failure		CSU_DCDistfail
12		32			15			No					No			否				否
13		32			15			Yes					Yes			是				是
14		32			15			Existent				Existent		存在				存在
15		32			15			Not Existent				Not Existent		不存在				不存在









﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
zh



[RES_INFO]
#R_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE		
1		32		15			Bridge Card			Bridge Card		Li电池桥卡			Li电池桥卡
2		32		15			Total Batt Curr			Total Batt Curr		电池总电流			电池总电流
3		32		15			Average Batt Volt		Ave Batt Volt	电池平均电压			电池平均电压
4		32		15			Average Batt Temp		Ave Batt Temp	电池平均温度			电池平均温度
5		32		15			Batt Lost			Batt Lost		电池丢失			电池丢失
6		32		15			Number of Installed		Num of Install		电池串数			电池串数
7		32		15			Number of Disconncted		Num of Discon		未连接电池串数			未连接电池数
8		32		15			Number of No Reply		Num of No Reply		未响应电池串数			未响应电池串数

9		32		15			Inventory Updating		Invent Update		地址重排中			地址重排中
10		32		15			BridgeCard Firmware Version	BdgCard FirmVer		桥卡固件号		桥卡固件号
11		32		15			Bridge Card Barcode 1		BdgCardBarcode1		桥卡条码1		桥卡条码1
12		32		15			Bridge Card Barcode 2		BdgCardBarcode2		桥卡条码2		桥卡条码2
13		32		15			Bridge Card Barcode 3		BdgCardBarcode3		桥卡条码3		桥卡条码3
14		32		15			Bridge Card Barcode 4		BdgCardBarcode4		桥卡条码4		桥卡条码4


50		32		15			All Batteries Communication Fail	All Comm Fail		所有电池无响应			所有电池无响应
51		32		15			Multi-Batt Comm Fail		Multi CommFail	多电池无响应			多电池无响应
52		32		15			Batt Lost			Batt Lost		电池丢失			电池丢失


99		32		15			Communication Fail		Comm Fail		中断状态			中断状态
100		32		15			Exist State			Exist State		存在状态			存在状态
101		32		15			Normal				Normal			正常				正常
102		32		15			Alarm				Alarm			告警				告警
103		32		15			Exist				Exist			存在				存在
104		32		15			No Exist			No Exist		不存在				不存在

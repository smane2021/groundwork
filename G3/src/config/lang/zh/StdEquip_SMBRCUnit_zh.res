﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
51	32			15			SMBRC Unit				SMBRC Unit		SMBRC单元		SMBRC单元
95	32			15			Times of Communication Fail		Times Comm Fail		通信失败次数		通信失败次数
96	32			15			Existent				Existent		存在			存在
97	32			15			Not Existent				Not Existent		不存在			不存在
117	32			15			Existence State				Existence State		是否存在		是否存在
118	32			15			Communication Fail			Comm Fail		通讯中断		通讯中断
120	32			15			Communication OK			Comm OK			通讯正常		通讯正常
121	32			15			All Communication Fail			All Comm Fail		都通讯中断		都通讯中断
122	32			15			Communication Fail			Comm Fail		通讯中断		通讯中断
123	32			15			Rated Capacity				Rated Capacity		标称容量		标称容量	
150	32			15			Digital Input Number			Dig Input Num		数字量输入数		数字输入数	
151	32			15			Digital Input 1				Digital Input 1		数字输入1		数字输入1
152	32			15			Low					Low			低			低
153	32			15			High					High			高			高
154	32			15			Digital Input 2				Digital Input 2		数字输入2		数字输入2
155	32			15			Digital Input 3				Digital Input 3		数字输入3		数字输入3
156	32			15			Digital Input 4				Digital Input 4		数字输入4		数字输入4
157	32			15			Num of Digital Out			Num of DO	数字输出数		数字输出数
158	32			15			Digital Output 1			Digital Out 1		数字输出1		数字输出1
159	32			15			Digital Output 2			Digital Out 2		数字输出2		数字输出2
160	32			15			Digital Output 3			Digital Out 3		数字输出3		数字输出3
161	32			15			Digital Output 4			Digital Out 4		数字输出4		数字输出4
162	32			15			Operation Status			Operation State		运行状态		运行状态
163	32			15			Normal					Normal			正常			正常
164	32			15			Test					Test			测试			测试
165	32			15			Discharge				Discharge		放电			放电
166	32			15			Calibration				Calibration		校准			校准
167	32			15			Diagnostic				Diagnostic		自检			自检
168	32			15			Maintenance				Maintenance		维护			维护
169	32			15			Internal Resistance TestInterval	InterR Test Int		测试间隔		测试间隔
170	32			15			Ambient Temperature Value		Amb Temp Value		环境温度		环境温度
171	32			15			Ambient High Temperature		Amb Temp High		环境温度高		环境温度高
172	32			15			Battery String Config Number		String Cfg Num		配置类型		配置类型
173	32			15			Exist Batt Num				Exist Batt Num		电池串数量		电池串数量
174	32			15			Unit Seq Num				Unit Seq Num		单元序列		单元序列
175	32			15			Start Battery Sequence			Start Batt Seq		开始电池序列		开始电池序列
176	32			15			Ambient Low Temperature			Amb Temp Low		环境温度低		环境温度低
177	32			15			Ambt Temp Not Used			Amb Temp No Use		环境温度未用		环境温度未用



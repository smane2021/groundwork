﻿#
#  Locale language support:China
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Fuse 1						Fuse 1				Fusible 1			Fusible 1
2	32			15			Fuse 2						Fuse 2				Fusible 2			Fusible 2
3	32			15			Fuse 3						Fuse 3				Fusible 3			Fusible 3
4	32			15			Fuse 4						Fuse 4				Fusible 4			Fusible 4
5	32			15			Fuse 5						Fuse 5				Fusible 5			Fusible 5
6	32			15			Fuse 6						Fuse 6				Fusible 6			Fusible 6
7	32			15			Fuse 7						Fuse 7				Fusible 7			Fusible 7
8	32			15			Fuse 8						Fuse 8				Fusible 8			Fusible 8
9	32			15			Fuse 9						Fuse 9				Fusible 9			Fusible 9
10	32			15			Fuse 10						Fuse 10				Fusible 10			Fusible 10
11	32			15			Off						Off				De			De
12	32			15			On						On				Sur			Sur
13	32			15			Communication Fail				Comm Fail			Échec Communication		Échec Com
14	32			15			Existence State					Existence State		Etat Existence		Etat Exist
15	32			15			Comm OK						Comm OK				Comm OK		Comm OK
16	32			15			Communication Fail				Comm Fail			Échec Communication		Échec Com
17	32			15			Existent					Existent			Existant			Existant
18	32			15			Not Existent					Not Existent		Non existant			Non existant
19	32			15			DC Fuse 1 Alarm					Fuse 1 Alm			DC熔丝1告警				熔丝1告警
20	32			15			DC Fuse 2 Alarm					Fuse 2 Alm			DC熔丝2告警				熔丝2告警
21	32			15			DC Fuse 3 Alarm					Fuse 3 Alm			DC熔丝3告警				熔丝3告警
22	32			15			DC Fuse 4 Alarm					Fuse 4 Alm			DC熔丝4告警				熔丝4告警
23	32			15			DC Fuse 5 Alarm					Fuse 5 Alm			DC熔丝5告警				熔丝5告警
24	32			15			DC Fuse 6 Alarm					Fuse 6 Alm			DC熔丝6告警				熔丝6告警
25	32			15			DC Fuse 7 Alarm					Fuse 7 Alm			DC熔丝7告警				熔丝7告警
26	32			15			DC Fuse 8 Alarm					Fuse 8 Alm			DC熔丝8告警				熔丝8告警
27	32			15			DC Fuse 9 Alarm					Fuse 9 Alm			DC熔丝9告警				熔丝9告警
28	32			15			DC Fuse 10 Alarm				Fuse 10 Alm			DC熔丝10告警			熔丝10告警
29	32			15			SMDUEDCFuseUnit1				DCFuseUnit1			SMDUEDC熔丝单元1		DC熔丝单元1
30	32			15			SMDUEDCFuseUnit2				DCFuseUnit2			SMDUEDC熔丝单元2		DC熔丝单元2
31	32			15			SMDUEDCFuseUnit3				DCFuseUnit3			SMDUEDC熔丝单元3		DC熔丝单元3
32	32			15			SMDUEDCFuseUnit4				DCFuseUnit4			SMDUEDC熔丝单元4		DC熔丝单元4
33	32			15			SMDUEDCFuseUnit5				DCFuseUnit5			SMDUEDC熔丝单元5		DC熔丝单元5
34	32			15			SMDUEDCFuseUnit6				DCFuseUnit6			SMDUEDC熔丝单元6		DC熔丝单元6
35	32			15			SMDUEDCFuseUnit7				DCFuseUnit7			SMDUEDC熔丝单元7		DC熔丝单元7
36	32			15			SMDUEDCFuseUnit8				DCFuseUnit8			SMDUEDC熔丝单元8		DC熔丝单元8








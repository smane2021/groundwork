﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			SM-IO Group						SM-IO Group		SM-IO组			SM-IO组
2	32			15			Existence State						Existence State			是否存在		是否存在
3	32			15			Existent						Existent			存在			存在
4	32			15			Not Existent						Not Existent			不存在			不存在








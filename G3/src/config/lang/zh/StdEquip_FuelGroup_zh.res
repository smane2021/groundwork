﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			Fuel Tank Group				Fuel Tank Grp		油箱组			油箱组
#2		32			15			Standby				Standby		待命			待命
#3		32			15			Refresh					Refresh			刷新			刷新
#4		32			15			Setting Refresh				Setting Refresh		设置信号刷新		设置信号刷新
6		32			15			No					No			否			否
7		32			15			Yes					Yes			是			是
10		32			15			Fuel Surveillance Failure		Fuel Surve Fail		油箱组监控失败		油箱监控失败
20		32			15			Fuel Group Communication Failure	Fuel Comm Fail		油箱组通讯失败		油箱组通讯失败
21		32			15			Fuel Number				Fuel Number		油箱数目		油箱数目
22		32			15			0					0			0			0
23		32			15			1					1			1			1
24		32			15			2					2			2			2
25		32			15			3					3			3			3
26		32			15			4					4			4			4
27		32			15			5					5			5			5
28		32			15			OB Fuel Number				OB Fuel Number		OB板油箱数目		OB板油箱数目








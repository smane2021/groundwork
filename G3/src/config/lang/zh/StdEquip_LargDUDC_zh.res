﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#


[LOCALE_LANGUAGE]
zh



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			Temperature 1			Temperature 1		温度1			温度1
2		32			15			Temperature 2			Temperature 2		温度2			温度2
3		32			15			Temperature 3			Temperature 3		温度3			温度3
4		32			15			DC Voltage			DC Voltage		直流电压		直流电压
5		32			15			Load Current			Load Current		负载电流		负载电流
6		32			15			Branch 1 Current		Branch 1 Curr		分路1电流		分路1电流
7		32			15			Branch 2 Current		Branch 2 Curr		分路2电流		分路2电流
8		32			15			Branch 3 Current		Branch 3 Curr		分路3电流		分路3电流
9		32			15			Branch 4 Current		Branch 4 Curr		分路4电流		分路4电流
10		32			15			Branch 5 Current		Branch 5 Curr		分路5电流		分路5电流
11		32			15			Branch 6 Current		Branch 6 Curr		分路6电流		分路6电流
12		32			15			DC Over Voltage			DC Over Volt		直流过压		直流过压
13		32			15			DC Under Voltage		DC Under Volt		直流欠压		直流欠压
14		32			15			Temperature 1 Over Temperature	T1 Over Temp		温度1过温		温度1过温
15		32			15			Temperature 2 Over Temperature	T2 Over Temp		温度2过温		温度2过温
16		32			15			Temperature 3 Over Temperature	T3 Over Temp		温度3过温		温度3过温
17		32			15			DC Output 1 Disconnected	Output 1 Discon		负载支路1断		负载支路1断
18		32			15			DC Output 2 Disconnected	Output 2 Discon		负载支路2断		负载支路2断
19		32			15			DC Output 3 Disconnected	Output 3 Discon		负载支路3断		负载支路3断
20		32			15			DC Output 4 Disconnected	Output 4 Discon		负载支路4断		负载支路4断
21		32			15			DC Output 5 Disconnected	Output 5 Discon		负载支路5断		负载支路5断
22		32			15			DC Output 6 Disconnected	Output 6 Discon		负载支路6断		负载支路6断
23		32			15			DC Output 7 Disconnected	Output 7 Discon		负载支路7断		负载支路7断
24		32			15			DC Output 8 Disconnected	Output 8 Discon		负载支路8断		负载支路8断
25		32			15			DC Output 9 Disconnected	Output 9 Discon		负载支路9断		负载支路9断
26		32			15			DC Output 10 Disconnected	Output10 Discon		负载支路10断		负载支路10断
27		32			15			DC Output 11 Disconnected	Output11 Discon		负载支路11断		负载支路11断
28		32			15			DC Output 12 Disconnected	Output12 Discon		负载支路12断		负载支路12断
29		32			15			DC Output 13 Disconnected	Output13 Discon		负载支路13断		负载支路13断
30		32			15			DC Output 14 Disconnected	Output14 Discon		负载支路14断		负载支路14断
31		32			15			DC Output 15 Disconnected	Output15 Discon		负载支路15断		负载支路15断
32		32			15			DC Output 16 Disconnected	Output16 Discon		负载支路16断		负载支路16断
33		32			15			DC Output 17 Disconnected	Output17 Discon		负载支路17断		负载支路17断
34		32			15			DC Output 18 Disconnected	Output18 Discon		负载支路18断		负载支路18断
35		32			15			DC Output 19 Disconnected	Output19 Discon		负载支路19断		负载支路19断
36		32			15			DC Output 20 Disconnected	Output20 Discon		负载支路20断		负载支路20断
37		32			15			DC Output 21 Disconnected	Output21 Discon		负载支路21断		负载支路21断
38		32			15			DC Output 22 Disconnected	Output22 Discon		负载支路22断		负载支路22断
39		32			15			DC Output 23 Disconnected	Output23 Discon		负载支路23断		负载支路23断
40		32			15			DC Output 24 Disconnected	Output24 Discon		负载支路24断		负载支路24断
41		32			15			DC Output 25 Disconnected	Output25 Discon		负载支路25断		负载支路25断
42		32			15			DC Output 26 Disconnected	Output26 Discon		负载支路26断		负载支路26断
43		32			15			DC Output 27 Disconnected	Output27 Discon		负载支路27断		负载支路27断
44		32			15			DC Output 28 Disconnected	Output28 Discon		负载支路28断		负载支路28断
45		32			15			DC Output 29 Disconnected	Output29 Discon		负载支路29断		负载支路29断
46		32			15			DC Output 30 Disconnected	Output30 Discon		负载支路30断		负载支路30断
47		32			15			DC Output 31 Disconnected	Output31 Discon		负载支路31断		负载支路31断
48		32			15			DC Output 32 Disconnected	Output32 Discon		负载支路32断		负载支路32断
49		32			15			DC Output 33 Disconnected	Output33 Discon		负载支路33断		负载支路33断
50		32			15			DC Output 34 Disconnected	Output34 Discon		负载支路34断		负载支路34断
51		32			15			DC Output 35 Disconnected	Output35 Discon		负载支路35断		负载支路35断
52		32			15			DC Output 36 Disconnected	Output36 Discon		负载支路36断		负载支路36断
53		32			15			DC Output 37 Disconnected	Output37 Discon		负载支路37断		负载支路37断
54		32			15			DC Output 38 Disconnected	Output38 Discon		负载支路38断		负载支路38断
55		32			15			DC Output 39 Disconnected	Output39 Discon		负载支路39断		负载支路39断
56		32			15			DC Output 40 Disconnected	Output40 Discon		负载支路40断		负载支路40断
57		32			15			DC Output 41 Disconnected	Output41 Discon		负载支路41断		负载支路41断
58		32			15			DC Output 42 Disconnected	Output42 Discon		负载支路42断		负载支路42断
59		32			15			DC Output 43 Disconnected	Output43 Discon		负载支路43断		负载支路43断
60		32			15			DC Output 44 Disconnected	Output44 Discon		负载支路44断		负载支路44断
61		32			15			DC Output 45 Disconnected	Output45 Discon		负载支路45断		负载支路45断
62		32			15			DC Output 46 Disconnected	Output46 Discon		负载支路46断		负载支路46断
63		32			15			DC Output 47 Disconnected	Output47 Discon		负载支路47断		负载支路47断
64		32			15			DC Output 48 Disconnected	Output48 Discon		负载支路48断		负载支路48断
65		32			15			DC Output 49 Disconnected	Output49 Discon		负载支路49断		负载支路49断
66		32			15			DC Output 50 Disconnected	Output50 Discon		负载支路50断		负载支路50断
67		32			15			DC Output 51 Disconnected	Output51 Discon		负载支路51断		负载支路51断
68		32			15			DC Output 52 Disconnected	Output52 Discon		负载支路52断		负载支路52断
69		32			15			DC Output 53 Disconnected	Output53 Discon		负载支路53断		负载支路53断
70		32			15			DC Output 54 Disconnected	Output54 Discon		负载支路54断		负载支路54断
71		32			15			DC Output 55 Disconnected	Output55 Discon		负载支路55断		负载支路55断
72		32			15			DC Output 56 Disconnected	Output56 Discon		负载支路56断		负载支路56断
73		32			15			DC Output 57 Disconnected	Output57 Discon		负载支路57断		负载支路57断
74		32			15			DC Output 58 Disconnected	Output58 Discon		负载支路58断		负载支路58断
75		32			15			DC Output 59 Disconnected	Output59 Discon		负载支路59断		负载支路59断
76		32			15			DC Output 60 Disconnected	Output60 Discon		负载支路60断		负载支路60断
77		32			15			DC Output 61 Disconnected	Output61 Discon		负载支路61断		负载支路61断
78		32			15			DC Output 62 Disconnected	Output62 Discon		负载支路62断		负载支路62断
79		32			15			DC Output 63 Disconnected	Output63 Discon		负载支路63断		负载支路63断
80		32			15			DC Output 64 Disconnected	Output64 Discon		负载支路64断		负载支路64断
81		32			15			LVD1 State			LVD1 State		下电接触器1状态		下电接触器1状态
82		32			15			LVD2 State			LVD2 State		下电接触器2状态		下电接触器2状态
83		32			15			LVD3 State			LVD3 State		下电接触器3状态		下电接触器3状态
84		32			15			Communication Fail			Comm Fail		直流屏通讯中断		直流屏通讯中断
85		32			15			LVD1				LVD1			LVD1			LVD1
86		32			15			LVD2				LVD2			LVD2			LVD2
87		32			15			LVD3				LVD3			LVD3			LVD3
88	32			15			Temperature 1 High Limit	Temp 1 Hi Limit		温度1过温点		温度1过温点
89		32			15			Temperature 2 High Limit	Temp 2 Hi Limit		温度2过温点		温度2过温点
90		32			15			Temperature 3 High Limit	Temp 3 Hi Limit		温度3过温点		温度3过温点
91		32			15			LVD1 Limit			LVD1 Limit		下电点1			下电点1
92		32			15			LVD2 Limit			LVD2 Limit		下电点2			下电点2
93		32			15			LVD3 Limit			LVD3 Limit		下电点3			下电点3
94		32			15			Battery Over Voltage Limit	BatOverVoltLmt		电池过压告警点		电池过压告警点
95		32			15			Battery Under Voltage Limit	BatUnderVoltLmt		电池欠压告警点		电池欠压告警点
96		32			15			Temperature Coefficient		Temp Coeff		温度系数		温度系数
97		32			15			Current Sensor Coefficient	Sensor Coeff		负载电流传感器系数	负载电流系数
98		32			15			Number of Battery		Num of Battery		电池串数		电池串数
99		32			15			Temperature Number		Temp Number		温度路数		温度路数
100		32			15			Branch Current Coefficient	BranchCurrCoeff		分路电流系数		分路电流系数
101		32			15			Distribution Address		Distri Address		直流屏地址		直流屏地址
102		32			15			Current Measurement Output Num	Curr Output Num	分路电流路数		分路电流路数
103		32			15			Number of Output		Num of Output		负载支路数		负载支路数
104		32			15			DC Over Voltage			DC Over Volt		输出过压		输出过压
105		32			15			DC Under Voltage		DC Under Volt		输出欠压		输出欠压
106		32			15			DC Output 1 Disconnected	Output1 Discon		输出支路1断		输出支路1断
107		32			15			DC Output 2 Disconnected	Output2 Discon		输出支路2断		输出支路2断
108		32			15			DC Output 3 Disconnected	Output3 Discon		输出支路3断		输出支路3断
109		32			15			DC Output 4 Disconnected	Output4 Discon		输出支路4断		输出支路4断
110		32			15			DC Output 5 Disconnected	Output5 Discon		输出支路5断		输出支路5断
111		32			15			DC Output 6 Disconnected	Output6 Discon		输出支路6断		输出支路6断
112		32			15			DC Output 7 Disconnected	Output7 Discon		输出支路7断		输出支路7断
113		32			15			DC Output 8 Disconnected	Output8 Discon		输出支路8断		输出支路8断
114		32			15			DC Output 9 Disconnected	Output9 Discon		输出支路9断		输出支路9断
115		32			15			DC Output 10 Disconnected	Output10 Discon		输出支路10断		输出支路10断
116		32			15			DC Output 11 Disconnected	Output11 Discon		输出支路11断		输出支路11断
117		32			15			DC Output 12 Disconnected	Output12 Discon		输出支路12断		输出支路12断
118		32			15			DC Output 13 Disconnected	Output13 Discon		输出支路13断		输出支路13断
119		32			15			DC Output 14 Disconnected	Output14 Discon		输出支路14断		输出支路14断
120		32			15			DC Output 15 Disconnected	Output15 Discon		输出支路15断		输出支路15断
121		32			15			DC Output 16 Disconnected	Output16 Discon		输出支路16断		输出支路16断
122		32			15			DC Output 17 Disconnected	Output17 Discon		输出支路17断		输出支路17断
123		32			15			DC Output 18 Disconnected	Output18 Discon		输出支路18断		输出支路18断
124		32			15			DC Output 19 Disconnected	Output19 Discon		输出支路19断		输出支路19断
125		32			15			DC Output 20 Disconnected	Output20 Discon		输出支路20断		输出支路20断
126		32			15			DC Output 21 Disconnected	Output21 Discon		输出支路21断		输出支路21断
127		32			15			DC Output 22 Disconnected	Output22 Discon		输出支路22断		输出支路22断
128		32			15			DC Output 23 Disconnected	Output23 Discon		输出支路23断		输出支路23断
129		32			15			DC Output 24 Disconnected	Output24 Discon		输出支路24断		输出支路24断
130		32			15			DC Output 25 Disconnected	Output25 Discon		输出支路25断		输出支路25断
131		32			15			DC Output 26 Disconnected	Output26 Discon		输出支路26断		输出支路26断
132		32			15			DC Output 27 Disconnected	Output27 Discon		输出支路27断		输出支路27断
133		32			15			DC Output 28 Disconnected	Output28 Discon		输出支路28断		输出支路28断
134		32			15			DC Output 29 Disconnected	Output29 Discon		输出支路29断		输出支路29断
135		32			15			DC Output 30 Disconnected	Output30 Discon		输出支路30断		输出支路30断
136		32			15			DC Output 31 Disconnected	Output31 Discon		输出支路31断		输出支路31断
137		32			15			DC Output 32 Disconnected	Output32 Discon		输出支路32断		输出支路32断
138		32			15			DC Output 33 Disconnected	Output33 Discon		输出支路33断		输出支路33断
139		32			15			DC Output 34 Disconnected	Output34 Discon		输出支路34断		输出支路34断
140		32			15			DC Output 35 Disconnected	Output35 Discon		输出支路35断		输出支路35断
141		32			15			DC Output 36 Disconnected	Output36 Discon		输出支路36断		输出支路36断
142		32			15			DC Output 37 Disconnected	Output37 Discon		输出支路37断		输出支路37断
143		32			15			DC Output 38 Disconnected	Output38 Discon		输出支路38断		输出支路38断
144		32			15			DC Output 39 Disconnected	Output39 Discon		输出支路39断		输出支路39断
145		32			15			DC Output 40 Disconnected	Output40 Discon		输出支路40断		输出支路40断
146		32			15			DC Output 41 Disconnected	Output41 Discon		输出支路41断		输出支路41断
147		32			15			DC Output 42 Disconnected	Output42 Discon		输出支路42断		输出支路42断
148		32			15			DC Output 43 Disconnected	Output43 Discon		输出支路43断		输出支路43断
149		32			15			DC Output 44 Disconnected	Output44 Discon		输出支路44断		输出支路44断
150		32			15			DC Output 45 Disconnected	Output45 Discon		输出支路45断		输出支路45断
151		32			15			DC Output 46 Disconnected	Output46 Discon		输出支路46断		输出支路46断
152		32			15			DC Output 47 Disconnected	Output47 Discon		输出支路47断		输出支路47断
153		32			15			DC Output 48 Disconnected	Output48 Discon		输出支路48断		输出支路48断
154		32			15			DC Output 49 Disconnected	Output49 Discon		输出支路49断		输出支路49断
155		32			15			DC Output 50 Disconnected	Output50 Discon		输出支路50断		输出支路50断
156		32			15			DC Output 51 Disconnected	Output51 Discon		输出支路51断		输出支路51断
157		32			15			DC Output 52 Disconnected	Output52 Discon		输出支路52断		输出支路52断
158		32			15			DC Output 53 Disconnected	Output53 Discon		输出支路53断		输出支路53断
159		32			15			DC Output 54 Disconnected	Output54 Discon		输出支路54断		输出支路54断
160		32			15			DC Output 55 Disconnected	Output55 Discon		输出支路55断		输出支路55断
161		32			15			DC Output 56 Disconnected	Output56 Discon		输出支路56断		输出支路56断
162		32			15			DC Output 57 Disconnected	Output57 Discon		输出支路57断		输出支路57断
163		32			15			DC Output 58 Disconnected	Output58 Discon		输出支路58断		输出支路58断
164		32			15			DC Output 59 Disconnected	Output59 Discon		输出支路59断		输出支路59断
165		32			15			DC Output 60 Disconnected	Output60 Discon		输出支路60断		输出支路60断
166		32			15			DC Output 61 Disconnected	Output61 Discon		输出支路61断		输出支路61断
167		32			15			DC Output 62 Disconnected	Output62 Discon		输出支路62断		输出支路62断
168		32			15			DC Output 63 Disconnected	Output63 Discon		输出支路63断		输出支路63断
169		32			15			DC Output 64 Disconnected	Output64 Discon		输出支路64断		输出支路64断
170		32			15			Communication Fail			Comm Fail		直流屏通讯中断		直流屏通讯中断
171		32			15			LVD1				LVD1			LVD1			LVD1
172		32			15			LVD2				LVD2			LVD2			LVD2
173		32			15			LVD3				LVD3			LVD3			LVD3
174		32			15			Temperature 1 Over Temperature	T1 Over Temp		温度1过温		温度1过温
175		32			15			Temperature 2 Over Temperature	T2 Over Temp		温度2过温		温度2过温
176		32			15			Temperature 3 Over Temperature	T3 Over Temp		温度3过温		温度3过温
177		32			15			LargeDU DC Distribution		DC Distribution		LargDU直流屏		直流屏
178		32			15			Temperature 1 Low Limit		Temp1 Low Limit		温度1下限		温度1下限
179		32			15			Temperature 2 Low Limit		Temp2 Low Limit		温度2下限		温度2下限
180		32			15			Temperature 3 Low Limit		Temp3 Low Limit		温度3下限		温度3下限
181		32			15			Temperature 1 Under Temperature	T1 Under Temp		温度1欠温		温度1欠温
182		32			15			Temperature 2 Under Temperature	T2 Under Temp		温度2欠温		温度2欠温
183		32			15			Temperature 3 Under Temperature	T3 Under Temp		温度3欠温		温度3欠温
184		32			15			Temperature 1 Alarm		Temp1 Alarm		温度1告警		温度1告警
185		32			15			Temperature 2 Alarm		Temp2 Alarm		温度2告警		温度2告警
186		32			15			Temperature 3 Alarm		Temp3 Alarm		温度3告警		温度3告警
187		32			15			Voltage Alarm			Voltage Alarm		电压告警		电压告警                                                                       
188		32			15			No Alarm			No Alarm		无告警			无告警                                                                           
189		32			15			Over Temperature		Over Temp		过温			过温
190		32			15			Under Temperature		Under Temp		欠温			欠温
191		32			15			No Alarm			No Alarm		无告警			无告警                                                  
192		32			15			Over Temperature		Over Temp		过温			过温
193		32			15			Under Temperature		Under Temp		欠温			欠温
194		32			15			No Alarm			No Alarm		无告警			无告警                                                              
195		32			15			Over Temperature		Over Temp		过温			过温
196		32			15			Under Temperature		Under Temp		欠温			欠温
197		32			15			No Alarm			No Alarm		无告警			无告警                                                                                           
198		32			15			Over Voltage			Over Voltage		过压			过压
199		32			15			Under Voltage			Under Voltage		欠压			欠压
200		32			15			Voltage Alarm			Voltage Alarm		电压告警		电压告警
201		32			15			DC Distr Communication Fail	DCD Comm Fail		直流屏通讯中断		直流屏通讯中断
202		32			15			Normal				Normal			正常			正常
203		32			15			Failure				Failure			中断			中断
204		32			15			DC Distr Communication Fail	DCD Comm Fail		直流屏通讯中断		直流屏通讯中断
205		32			15			On				On			上电			上电
206		32			15			Off				Off			下电			下电
207		32			15			On				On			上电			上电
208		32			15			Off				Off			下电			下电
209		32			15			On				On			上电			上电
210		32			15			Off				Off			下电			下电
211		32			15			Temperature 1 Sensor Failure	T1 Sensor Fail		温度传感器1故障		温度传感器1故障
212		32			15			Temperature 2 Sensor Failure	T2 Sensor Fail		温度传感器2故障		温度传感器2故障
213		32			15			Temperature 3 Sensor Failure	T3 Sensor Fail		温度传感器3故障		温度传感器3故障
214		32			15			Connected			Connected		连接			连接
215		32			15			Disconnected			Disconnected		断开			断开
216		32			15			Connected			Connected		连接			连接
217		32			15			Disconnected			Disconnected		断开			断开
218		32			15			Connected			Connected		连接			连接
219		32			15			Disconnected			Disconnected		断开			断开
220		32			15			Normal				Normal			正常			正常
221		32			15			DC Distr Communication Fail	DCD Comm Fail		断开			断开
222		32			15			Normal				Normal			正常			正常
223		32			15			Alarm				Alarm			告警			告警
224		32			15			Branch 7 Current		Branch 7 Curr		分路7电流		分路7电流
225		32			15			Branch 8 Current		Branch 8 Curr		分路8电流		分路8电流
226		32			15			Branch 9 Current		Branch 9 Curr		分路9电流		分路9电流
227		32			15			Existence State			Existence State		是否存在		是否存在
228		32			15			Existent			Existent		存在			存在
229		32			15			Not Existent			Not Existent		不存在			不存在
230	32			15		LVD Number			LVD Number		LVD级数			LVD级数
231		32			15			0				0			0			0
232		32			15			1				1			1			1
233		32			15			2				2			2			2
234		32			15			3				3			3			3
235		32			15			Battery Shutdown Number		Batt SD Num		电池分流器数		电池分流器数

236		32			15			Rated Capacity			Rated Capacity		标称容量		标称容量	






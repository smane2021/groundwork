﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN			FULL_IN_LOCALE			ABBR_IN_LOCALE			
1	32			15		Last Diesel Generator Test Date		Last Test Date			最近测试时间			最近测试时间
2	32			15		Diesel Generator Test Running		Test Running			油机测试			油机测试
3	32			15		Diesel Generator Test Results		Test Results			油机测试结果			油机测试结果
4	32			15		Start Diesel Generator Test		Start Test			开始油机测试			开始油机测试
5	32			15		Diesel Generator Max Test Time		Max Test Time			油机测试时间			油机测试时间
6	32			15		Planned Diesel Gen Test			Planned Test			计划测试允许			计划测试允许
7	32			15		Diesel Generator Control Inhibit	Control Inhibit			油机控制禁止			油机控制禁止
8	32			15		Diesel Generator Test Running		Test Running			油机测试				油机测试
9	32			15		Diesel Generator Test Failure		Test Failure			油机测试失败			油机测试失败
10	32			15		No					No				否				否
11	32			15		Yes					Yes				是				是
12		32			15			Reset Diesel Gen Test Error		Reset Test Err			清除测试失败			清除测试失败
13		32			15			New Diesel Generator Test Delay		New Test Delay			新测试延时			新测试延时
14		32			15			Num of Scheduled Tests per Year		Yr Test Times			每年油机测试次			每年油机测试次
15		32			15			Test 1 Date				Test 1 Date			油机测试时间1			油机测试时间1
16		32			15			Test 2 Date				Test 2 Date			油机测试时间2			油机测试时间2  
17		32			15			Test 3 Date				Test 3 Date			油机测试时间3			油机测试时间3  
18		32			15			Test 4 Date				Test 4 Date			油机测试时间4			油机测试时间4  
19		32			15			Test 5 Date				Test 5 Date			油机测试时间5			油机测试时间5  
20		32			15			Test 6 Date				Test 6 Date			油机测试时间6			油机测试时间6  
21		32			15			Test 7 Date				Test 7 Date			油机测试时间7			油机测试时间7  
22		32			15			Test 8 Date				Test 8 Date			油机测试时间8			油机测试时间8  
23		32			15			Test 9 Date				Test 9 Date			油机测试时间9			油机测试时间9  
24		32			15			Test 10 Date				Test 10 Date			油机测试时间10			油机测试时间10 
25		32			15			Test 11 Date				Test 11 Date			油机测试时间11			油机测试时间11 
26		32			15			Test 12 Date				Test 12 Date			油机测试时间12			油机测试时间12 
27		32			15			Normal					Normal				正常				正常
28		32			15			End by Manual				End by Manual			手动停止			手动停止
29		32			15			Time is Up				Time is Up			测试时间到			测试时间到
30		32			15			In Manual State				In Manual State			油机在手动状态			手动状态
31		32			15			Low Battery Voltage			Low Batt Volt			电池电压低告警			电池电压低告警
32		32			15			High Water Temperature			High Water Temp			高水温告警			高水温告警
33		32			15			Low Oil Pressure			Low Oil Press			低油压告警			低油压告警
34		32			15			Low Fuel Level				Low Fuel Level			低油位告警			低油位告警
35		32			15			Diesel Generator Failure		Diesel Gen Fail			油机故障			油机故障
36		32			15			Diesel Generator Group			Dsl Gen Group			油机组				油机组
37		32			15			State					State				State				State
38		32			15			Existence State				Existence State			是否存在			是否存在
39		32			15			Existent				Existent			存在				存在
40		32			15			Not Existent				Not Existent			不存在				不存在
41		32			15			Total Input Current			Input Current			总输入电流			总输入电流	


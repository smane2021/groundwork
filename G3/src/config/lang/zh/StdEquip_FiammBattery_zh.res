﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15			Battery Current				Batt Current		电池电流		电池电流			
2	32			15			Battery Rating (Ah)		Batt Rating(Ah)		电池容量(Ah)		电池容量(Ah)			
3	32			15			Bus Voltage				Bus Voltage		母排电压		母排电压		
5	32			15			Battery Over Current			Batt Over Curr		电池充电过流		电池充电过流		
6	32			15			Battery Capacity (%)			Batt Cap (%)		电池容量(%)		电池容量(%)		
7	32			15			Battery Voltage				Batt Voltage		电池电压		电池电压		
18	32			15			SoNick Battery				SoNick Batt		SoNick电池		SoNick电池		
29	32			15			Yes					Yes			是			是
30	32			15			No					No			否			否
31	32			15			On					On			开			开
32	32			15			Off					Off			关			关
33	32			15			State					State			State			State
87	32			15			No					No			否			否	
96	32			15			Rated Capacity				Rated Capacity		标称容量		标称容量	
99	32			15			Battery Temperature(AVE)		Batt Temp(AVE)		电池温度		电池温度
100	32			15			Board Temperature			Board Temp		主板温度		主板温度
101	32			15			Tc Center Temperature			Tc Center Temp		Tc中心温度		Tc中心温度
102	32			15			Tc Left Temperature			Tc Left Temp		Tc左边温度		Tc左边温度
103	32			15			Tc Right Temperature			Tc Right Temp		Tc右边温度		Tc右边温度
114	32			15			Battery Communication Fail		Batt Comm Fail		电池通信中断		电池通信中断
129	32			15			Low Ambient Temperature			Low Amb Temp		环境温度低		环境温度低
130	32			15			High Ambient Temperature Warning	High Amb Temp W	环境温度高		环境温度高	
131	32			15			High Ambient Temperature		High Amb Temp		环境温度过高		环境温度过高	
132	32			15			Low Battery Internal Temperature	Low Int Temp		电池温度低		电池温度低
133	32			15			High Batt Internal Temp Warning		Hi Int Temp W		电池温度高		电池温度高	
134	32			15			High Batt Internal Temperature		Hi Bat Int Temp		电池温度过高		电池温度过高	
135	32			15			Bus Voltage Below 40V			Bus V Below 40V		母排电压小于40V		母排电压小于40V	
136	32			15			Bus Voltage Below 39V			Bus V Below 39V		母排电压小于39V		母排电压小于39V	
137	32			15			Bus Voltage Above 60V			Bus V Above 60V		母排电压大于60V		母排电压大于60V	
138	32			15			Bus Voltage Above 65V			Bus V Above 65V		母排电压大于65V		母排电压大于65V	
139	32			15			High Discharge Current Warning		Hi Disch Curr W		放电电流高		放电电流高
140	32			15			High Discharge Current			High Disch Curr		放电电流过高		放电电流过高
141	32			15			Main Switch Error			Main Switch Err		MainSwitch故障		MainSwitch故障
142	32			15			Fuse Blown				Fuse Blown		熔丝断			熔丝断
143	32			15			Heaters Failure				Heaters Failure		加热器故障		加热器故障
144	32			15			Thermocouple Failure			Thermocple Fail		热电偶故障		热电偶故障
145	32			15			Voltage Measurement Circuit Fail	V M Cir Fail		电压测量故障		电压测量故障
146	32			15			Current Measurement Circuit Fail	C M Cir Fail		电流测量故障		电流测量故障
147	32			15			BMS Hardware Failure			BMS Hdw Failure		BMS故障			BMS故障
148	32			15			Hardware Protection Sys Active		HdW Protect Sys		硬件保护		硬件保护
149	32			15			High Heatsink Temperature		Hi Heatsink Tmp		散热片温度高		散热片温度高
150	32			15			Battery Voltage Below 39V		Bat V Below 39V		电池电压小于39V		电池电压小于39V
151	32			15			Battery Voltage Below 38V		Bat V Below 38V		电池电压小于38V		电池电压小于38V
152	32			15			Battery Voltage Above 53.5V		Bat V Abv 53.5V		电池电压大于53.5V	电池电压大于53.5V
153	32			15			Battery Voltage Above 53.6V		Bat V Abv 53.6V		电池电压大于53.6V	电池电压大于53.6V
154	32			15			High Charge Current Warning		Hi Chrge Curr W		充电电流高		充电电流高
155	32			15			High Charge Current			Hi Charge Curr		充电电流过高		充电电流过高
156	32			15			High Discharge Current Warning		Hi Disch Curr W		放电电流高		放电电流高
157	32			15			High Discharge Current			Hi Disch Curr		放电电流过高		放电电流过高
158	32			15			Voltage Unbalance Warning		V unbalance W		电压不平衡		电压不平衡
159	32			15			Voltages Unbalance			Volt Unbalance		电压太不平衡		电压太不平衡
160	32			15			Dc Bus Pwr Too Low for Charging		DC Low for Chrg		无法充电		无法充电
161	32			15			Charge Regulation Failure		Charge Reg Fail		充电限流故障		充电限流故障
162	32			15			Capacity Below 12.5%			Cap Below 12.5%		容量小于12.5%		容量小于12.5%
163	32			15			Thermocouples Mismatch			Tcples Mismatch		热电偶不匹配		热电偶不匹配
164	32			15			Heater Fuse Blown			Heater FA		加热器熔丝断		加热器熔丝断

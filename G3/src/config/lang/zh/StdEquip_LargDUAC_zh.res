﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#ResId	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			Manual				Manual			手动			手动
2		32			15			Auto				Auto			自动			自动
3		32			15			Off				Off			关				关
4		32			15			On				On			开				开
5		32			15			No Input			No Input		无输入			无输入
6		32			15			Input 1				Input 1			输入1			输入1
7		32			15			Input 2				Input 2			输入2			输入2
8		32			15			Input 3				Input 3			输入3			输入3
9		32			15			No Input			No Input		无输入			无输入
10		32			15			Input				Input			有输入			有输入
11		32			15			Closed				Closed			闭合			闭合
12		32			15			Open				Open			断开			断开
13		32			15			Closed				Closed			闭合			闭合
14		32			15			Open				Open			断开			断开
15		32			15			Closed				Closed			闭合			闭合
16		32			15			Open				Open			断开			断开
17		32			15			Closed				Closed			闭合			闭合
18		32			15			Open				Open			断开			断开
19		32			15			Closed				Closed			闭合			闭合
20		32			15			Open				Open			断开			断开
21		32			15			Closed				Closed			闭合			闭合
22		32			15			Open				Open			断开			断开
23		32			15			Closed				Closed			闭合			闭合
24		32			15			Open				Open			断开			断开
25		32			15			Closed				Closed			闭合			闭合
26		32			15			Open				Open			断开			断开
27		32			15			1-Phase				1-Phase			单相			单相
28		32			15			3-Phase				3-Phase			三相			三相
29		32			15			No Measurement			No Measurement		不测			不测
30		32			15			1-Phase				1-Phase			单相			单相
31		32			15			3-Phase				3-Phase			三相			三相
32		32			15			Communication OK		Comm OK			有响应			有响应
33		32			15			Communication Fail		Comm Fail		无响应			无响应
34		32			15			AC Distribution		AC Distribution		交流屏			交流屏
35		32			15			Mains 1 Uab/Ua			Mains 1 Uab/Ua		第一路Uab/Ua电压	第一路Uab/Ua
36		32			15			Mains 1 Ubc/Ub			Mains 1 Ubc/Ub		第一路Ubc/Ub电压	第一路Ubc/Ub
37		32			15			Mains 1 Uca/Uc			Mains 1 Uca/Uc		第一路Uca/Uc电压	第一路Uca/Uc
38		32			15			Mains 2 Uab/Ua			Mains 2 Uab/Ua		第二路Uab/Ua电压	第二路Uab/Ua
39		32			15			Mains 2 Ubc/Ub			Mains 2 Ubc/Ub		第二路Ubc/Ub电压	第二路Ubc/Ub
40		32			15			Mains 2 Uca/Uc			Mains 2 Uca/Uc		第二路Uca/Uc电压	第二路Uca/Uc
41		32			15			Mains 3 Uab/Ua			Mains 3 Uab/Ua		第三路Uab/Ua电压	第三路Uab/Ua
42		32			15			Mains 3 Ubc/Ub			Mains 3 Ubc/Ub		第三路Ubc/Ub电压	第三路Ubc/Ub
43		32			15			Mains 3 Uca/Uc			Mains 3 Uca/Uc		第三路Uca/Uc电压	第三路Uca/Uc
53		32			15			Working Phase A Current		Phase A Curr		工作路A相电流		工作路A相电流
54		32			15			Working Phase B Current		Phase B Curr		工作路B相电流		工作路B相电流
55		32			15			Working Phase C Current		Phase C Curr		工作路C相电流		工作路C相电流
56		32			15			AC Input Frequency		AC Input Freq		交流输入频率		交流输入频率
57		32			15			AC Input Switch Mode		AC Switch Mode		交流输入切换方式	输入切换方式
58		32			15			Fault Lighting Status		Fault Lighting		故障照明状态		故障照明状态
59		32			15			Mains 1 Input Status		1 Input Status		交流1路输入		交流1路输入
60		32			15			Mains 2 Input Status		2 Input Status		交流2路输入		交流2路输入
61		32			15			Mains 3 Input Status		3 Input Status		交流3路输入		交流3路输入
62		32			15			AC Output 1 Status		Output 1 Status		交流输出1路状态		输出1路状态
63		32			15			AC Output 2 Status		Output 2 Status		交流输出2路状态		输出2路状态
64		32			15			AC Output 3 Status		Output 3 Status		交流输出3路状态		输出3路状态
65		32			15			AC Output 4 Status		Output 4 Status		交流输出4路状态		输出4路状态
66		32			15			AC Output 5 Status		Output 5 Status		交流输出5路状态		输出5路状态
67		32			15			AC Output 6 Status		Output 6 Status		交流输出6路状态		输出6路状态
68		32			15			AC Output 7 Status		Output 7 Status		交流输出7路状态		输出7路状态
69		32			15			AC Output 8 Status		Output 8 Status		交流输出8路状态		输出8路状态
70		32			15			AC Input Frequency High		Frequency High		频率过高		频率过高
71		32			15			AC Input Frequency Low		Frequency Low		频率过低		频率过低
72		32			15			AC Input MCCB Trip		Input MCCB Trip		交流输入空开跳		输入空开跳
73		32			15			SPD Trip			SPD Trip		防雷器故障		防雷器故障
74		32			15			AC Output MCCB Trip		OutputMCCB Trip		交流输出空开跳		输出空开跳
75		32			15			AC Input 1 Failure		Input 1 Failure		交流输入1路停电		输入1路停电
76		32			15			AC Input 2 Failure		Input 2 Failure		交流输入2路停电		输入2路停电
77		32			15			AC Input 3 Failure		Input 3 Failure		交流输入3路停电		输入3路停电
78		32			15			Mains 1 Uab/Ua Under Voltage	M1Uab/Ua UnderV		交流输入1Uab/Ua欠压	1路Uab/Ua欠压       
79		32			15			Mains 1 Ubc/Ub Under Voltage	M1Ubc/Ub UnderV		交流输入1Ubc/Ub欠压	1路Ubc/Ub欠压
80		32			15			Mains 1 Uca/Uc Under Voltage	M1Uca/Uc UnderV		交流输入1Uca/Uc欠压	1路Uca/Uc欠压
81		32			15			Mains 2 Uab/Ua Under Voltage	M2Uab/Ua UnderV		交流输入2Uab/Ua欠压	2路Uab/Ua欠压
82		32			15			Mains 2 Ubc/Ub Under Voltage	M2Ubc/Ub UnderV		交流输入2Ubc/Ub欠压	2路Ubc/Ub欠压
83		32			15			Mains 2 Uca/Uc Under Voltage	M2Uca/Uc UnderV		交流输入2Uca/Uc欠压	2路Uca/Uc欠压
84		32			15			Mains 3 Uab/Ua Under Voltage	M3Uab/Ua UnderV		交流输入3Uab/Ua欠压	3路Uab/Ua欠压
85		32			15			Mains 3 Ubc/Ub Under Voltage	M3Ubc/Ub UnderV		交流输入3Ubc/Ub欠压	3路Ubc/Ub欠压
86		32			15			Mains 3 Uca/Uc Under Voltage	M3Uca/Uc UnderV		交流输入3Uca/Uc欠压	3路Uca/Uc欠压
87		32			15			Mains 1 Uab/Ua Over Voltage	M1 Uab/Ua OverV		交流输入1Uab/Ua过压	1路Uab/Ua过压
88		32			15			Mains 1 Ubc/Ub Over Voltage	M1 Ubc/Ub OverV		交流输入1Ubc/Ub过压	1路Ubc/Ub过压
89		32			15			Mains 1 Uca/Uc Over Voltage	M1 Uca/Uc OverV		交流输入1Uca/Uc过压	1路Uca/Uc过压
90		32			15			Mains 2 Uab/Ua Over Voltage	M2 Uab/Ua OverV		交流输入2Uab/Ua过压	2路Uab/Ua过压
91		32			15			Mains 2 Ubc/Ub Over Voltage	M2 Ubc/Ub OverV		交流输入2Ubc/Ub过压	2路Ubc/Ub过压
92		32			15			Mains 2 Uca/Uc Over Voltage	M2 Uca/Uc OverV		交流输入2Uca/Uc过压	2路Uca/Uc过压
93		32			15			Mains 3 Uab/Ua Over Voltage	M3 Uab/Ua OverV		交流输入3Uab/Ua过压	3路Uab/Ua过压
94		32			15			Mains 3 Ubc/Ub Over Voltage	M3 Ubc/Ub OverV		交流输入3Ubc/Ub过压	3路Ubc/Ub过压
95		32			15			Mains 3 Uca/Uc Over Voltage	M3 Uca/Uc OverV		交流输入3Uca/Uc过压	3路Uca/Uc过压
96		32			15			Mains 1 Uab/Ua Failure		M1 Uab/Ua Fail		交流输入1Uab/Ua停电	1路Uab/Ua停电
97		32			15			Mains 1 Ubc/Ub Failure		M1 Ubc/Ub Fail		交流输入1Ubc/Ub停电	1路Ubc/Ub停电
98		32			15			Mains 1 Uca/Uc Failure		M1 Uca/Uc Fail		交流输入1Uca/Uc停电	1路Uca/Uc停电
99		32			15			Mains 2 Uab/Ua Failure		M2 Uab/Ua Fail		交流输入2Uab/Ua停电	2路Uab/Ua停电
100		32			15			Mains 2 Ubc/Ub Failure		M2 Ubc/Ub Fail		交流输入2Ubc/Ub停电	2路Ubc/Ub停电
101		32			15			Mains 2 Uca/Uc Failure		M2 Uca/Uc Fail		交流输入2Uca/Uc停电	2路Uca/Uc停电
102		32			15			Mains 3 Uab/Ua Failure		M3 Uab/Ua Fail		交流输入3Uab/Ua停电	3路Uab/Ua停电
103		32			15			Mains 3 Ubc/Ub Failure		M3 Ubc/Ub Fail		交流输入3Ubc/Ub停电	3路Ubc/Ub停电
104		32			15			Mains 3 Uca/Uc Failure		M3 Uca/Uc Fail		交流输入3Uca/Uc停电	3路Uca/Uc停电
105		32			15			Communication Fail		Comm Fail		交流屏通讯中断		交流屏通讯中断
106		32			15			Over Voltage Limit		Over Volt Limit		输入过压告警点	输入过压点
107		32			15			Under Voltage Limit		Under Volt Lmt	输入欠压告警点	输入欠压点
108		32			15			Phase Failure Voltage		Phase Fail Volt	输入缺相告警点	输入缺相点
109		32			15			Over Frequency Limit		Over Freq Limit		频率过高告警点	频率过高点
110		32			15			Under Frequency Limit		Under Freq Lmt	频率过低告警点	频率过低点
111		32			15			Current Transformer Coef	Curr Trans Coef	交流互感器系数	交流互感系数
112		32			15			Input Type			Input Type	交流输入类型	交流输入类型
113		32			15			Input Number			Input Number	交流输入路数	交流输入路数
114		32			15			Current Measurement		Cur Measurement		交流电流测量方式	电流测量方式
115		32			15			Output Number			Output Number	交流输出路数	交流输出路数
116		32			15			Distribution Address		Distri Address	交流屏地址	交流屏地址
117		32			15			Mains 1 Failure			Mains 1 Fail		交流1路输入停电		交流1路输入停电                         
118		32			15			Mains 2 Failure			Mains 2 Fail		交流2路输入停电		交流2路输入停电 
119		32			15			Mains 3 Failure			Mains 3 Fail		交流3路输入停电		交流3路输入停电 
120		32			15			Mains 1 Uab/Ua Failure		M1 Uab/Ua Fail		交流1路Uab/Ua缺相	1路Uab/Ua缺相
121		32			15			Mains 1 Ubc/Ub Failure		M1 Ubc/Ub Fail		交流1路Ubc/Ub缺相	1路Ubc/Ub缺相
122		32			15			Mains 1 Uca/Uc Failure		M1 Uca/Uc Fail		交流1路Uca/Uc缺相	1路Uca/Uc缺相
123		32			15			Mains 2 Uab/Ua Failure		M2 Uab/Ua Fail		交流2路Uab/Ua缺相	2路Uab/Ua缺相
124		32			15			Mains 2 Ubc/Ub Failure		M2 Ubc/Ub Fail		交流2路Ubc/Ub缺相	2路Ubc/Ub缺相
125		32			15			Mains 2 Uca/Uc Failure		M2 Uca/Uc Fail		交流2路Uca/Uc缺相	2路Uca/Uc缺相
126		32			15			Mains 3 Uab/Ua Failure		M3 Uab/Ua Fail		交流3路Uab/Ua缺相	3路Uab/Ua缺相
127		32			15			Mains 3 Ubc/Ub Failure		M3 Ubc/Ub Fail		交流3路Ubc/Ub缺相	3路Ubc/Ub缺相
128		32			15			Mains 3 Uca/Uc Failure		M3 Uca/Uc Fail		交流3路Uca/Uc缺相	3路Uca/Uc缺相
129		32			15			Over Frequency			Over Frequency		交流频率过高		交流频率过高
130		32			15			Under Frequency			Under Frequency		交流频率过低		交流频率过低
131		32			15			Mains 1 Uab/Ua Under Voltage	M1Uab/Ua UnderV		交流输入1Uab/Ua欠压	1路Uab/Ua欠压
132		32			15			Mains 1 Ubc/Ub Under Voltage	M1Ubc/Ub UnderV		交流输入1Ubc/Ub欠压	1路Ubc/Ub欠压
133		32			15			Mains 1 Uca/Uc Under Voltage	M1Uca/Uc UnderV		交流输入1Uca/Uc欠压	1路Uca/Uc欠压
134		32			15			Mains 2 Uab/Ua Under Voltage	M2Uab/Ua UnderV		交流输入2Uab/Ua欠压	2路Uab/Ua欠压
135		32			15			Mains 2 Ubc/Ub Under Voltage	M2Ubc/Ub UnderV		交流输入2Ubc/Ub欠压	2路Ubc/Ub欠压
136		32			15			Mains 2 Uca/Uc Under Voltage	M2Uca/Uc UnderV		交流输入2Uca/Uc欠压	2路Uca/Uc欠压
137		32			15			Mains 3 Uab/Ua Under Voltage	M3Uab/Ua UnderV		交流输入3Uab/Ua欠压	3路Uab/Ua欠压
138		32			15			Mains 3 Ubc/Ub Under Voltage	M3Ubc/Ub UnderV		交流输入3Ubc/Ub欠压	3路Ubc/Ub欠压
139		32			15			Mains 3 Uca/Uc Under Voltage	M3Uca/Uc UnderV		交流输入3Uca/Uc欠压	3路Uca/Uc欠压
140		32			15			Mains 1 Uab/Ua Over Voltage	M1 Uab/Ua OverV		交流输入1Uab/Ua过压	1路Uab/Ua过压
141		32			15			Mains 1 Ubc/Ub Over Voltage	M1 Ubc/Ub OverV		交流输入1Ubc/Ub过压	1路Ubc/Ub过压
142		32			15			Mains 1 Uca/Uc Over Voltage	M1 Uca/Uc OverV		交流输入1Uca/Uc过压	1路Uca/Uc过压
143		32			15			Mains 2 Uab/Ua Over Voltage	M2 Uab/Ua OverV		交流输入2Uab/Ua过压	2路Uab/Ua过压
144		32			15			Mains 2 Ubc/Ub Over Voltage	M2 Ubc/Ub OverV		交流输入2Ubc/Ub过压	2路Ubc/Ub过压
145		32			15			Mains 2 Uca/Uc Over Voltage	M2 Uca/Uc OverV		交流输入2Uca/Uc过压	2路Uca/Uc过压
146		32			15			Mains 3 Uab/Ua Over Voltage	M3 Uab/Ua OverV		交流输入3Uab/Ua过压	3路Uab/Ua过压
147		32			15			Mains 3 Ubc/Ub Over Voltage	M3 Ubc/Ub OverV		交流输入3Ubc/Ub过压	3路Ubc/Ub过压
148		32			15			Mains 3 Uca/Uc Over Voltage	M3 Uca/Uc OverV		交流输入3Uca/Uc过压	3路Uca/Uc过压
149		32			15			AC Input MCCB Trip		Input MCCB Trip		输入空开跳		输入空开跳
150		32			15			AC Output MCCB Trip		OutputMCCB Trip		输出空开跳		输出空开跳
151		32			15			SPD Trip			SPD Trip		防雷器故障		防雷器故障
169		32			15			Communication Fail		Comm Fail		交流屏通讯中断		交流屏通讯中断
170		32			15			Mains Failure			Mains Failure		交流停电		交流停电
171		32			15			LargeDU AC Distribution		AC Distribution		LargeDU交流屏			交流屏
172		32			15			No Alarm			No Alarm		无告警			无告警
173		32			15			Over Voltage			Over Volt		过压			过压  
174		32			15			Under Voltage			Under Volt		欠压			欠压  
175		32			15			AC Phase Failure		AC Phase Fail		交流缺相		交流缺相
176		32			15			No Alarm			No Alarm		无告警			无告警
177		32			15			Over Frequency			Over Frequency		交流频率过高		交流频率过高  
178		32			15			Under Frequency			Under Frequency		交流频率过低		交流频率过低
179		32			15			No Alarm			No Alarm		无告警			无告警
180		32			15			AC Over Voltage			AC Over Volt		交流过压		交流过压
181		32			15			AC Under Voltage		AC Under Volt		交流欠压		交流欠压
182		32			15			AC Phase Failure		AC Phase Fail		交流缺相		交流缺相
183		32			15			No Alarm			No Alarm		无告警			无告警
184		32			15			AC Over Voltage			AC Over Volt		交流过压		交流过压  
185		32			15			AC Under Voltage		AC Under Volt		交流欠压		交流欠压  
186		32			15			AC Phase Failure		AC Phase Fail		交流缺相		交流缺相
187		32			15			Mains 1 Uab/Ua Alarm		M1 Uab/Ua Alarm		1路Uab/Ua电压告警	1路Uab/Ua告警
188		32			15			Mains 1 Ubc/Ub Alarm		M1 Ubc/Ub Alarm		1路Ubc/Ub电压告警	1路Ubc/Ub告警
189		32			15			Mains 1 Uca/Uc Alarm		M1 Uca/Uc Alarm		1路Uca/Uc电压告警	1路Uca/Uc告警
190		32			15			Frequency Alarm			Frequency Alarm		频率告警		频率告警
191		32			15			Communication Fail		Comm Fail		交流屏通讯中断		交流屏通讯中断
192		32			15			Normal				Normal			正常			正常
193		32			15			Failure				Failure			中断			中断
194		32			15			Communication Fail		Comm Fail		交流屏通讯中断		交流屏通讯中断
195		32			15			Mains Input Number		Mains Input Num		交流输入路号		交流输入路号
196		32			15			Number 1			Number 1		第一路			第一路
197		32			15			Number 2			Number 2		第二路			第二路
198		32			15			Number 3			Number 3		第三路			第三路
199		32			15			None				None			无			无
200		32			15			Emergency Light			Emergency Light		故障照明灯状态		故障照明灯状态
201		32			15			Closed				Closed			关闭			关闭
202		32			15			Open				Open			打开			打开
203		32			15			Mains 2 Uab/Ua Alarm		M2 Uab/Ua Alarm		2路Uab/Ua电压告警	2路Uab/Ua告警
204		32			15			Mains 2 Ubc/Ub Alarm		M2 Ubc/Ub Alarm		2路Ubc/Ub电压告警	2路Ubc/Ub告警
205		32			15			Mains 2 Uca/Uc Alarm		M2 Uca/Uc Alarm		2路Uca/Uc电压告警	2路Uca/Uc告警
206		32			15			Mains 3 Uab/Ua Alarm		M3 Uab/Ua Alarm		3路Uab/Ua电压告警	3路Uab/Ua告警
207		32			15			Mains 3 Ubc/Ub Alarm		M3 Ubc/Ub Alarm		3路Ubc/Ub电压告警	3路Ubc/Ub告警
208		32			15			Mains 3 Uca/Uc Alarm		M3 Uca/Uc Alarm		3路Uca/Uc电压告警	3路Uca/Uc告警
209		32			15			Normal				Normal			正常			正常
210		32			15			Alarm				Alarm			告警			告警
211		32			15			AC Fuse Number			AC Fuse Num		交流熔丝数量		交流熔丝数量
212		32			15			Existence State			Existence State		是否存在		是否存在
213		32			15			Existent			Existent		存在			存在
214		32			15			Not Existent			Not Existent		不存在			不存在




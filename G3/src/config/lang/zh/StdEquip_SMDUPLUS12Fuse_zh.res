﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh



[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15		Fuse 1			Fuse 1			支路1			支路1
2	32			15		Fuse 2			Fuse 2			支路2			支路2
3	32			15		Fuse 3			Fuse 3			支路3			支路3
4	32			15		Fuse 4			Fuse 4			支路4			支路4
5	32			15		Fuse 5			Fuse 5			支路5			支路5
6	32			15		Fuse 6			Fuse 6			支路6			支路6
7	32			15		Fuse 7			Fuse 7			支路7			支路7
8	32			15		Fuse 8			Fuse 8			支路8			支路8
9	32			15		Fuse 9			Fuse 9			支路9			支路9
10	32			15		Fuse 10			Fuse 10			支路10			支路10
11	32			15		Fuse 11			Fuse 11			支路11			支路11
12	32			15		Fuse 12			Fuse 12			支路12			支路12
13	32			15		Fuse 13			Fuse 13			支路13			支路13
14	32			15		Fuse 14			Fuse 14			支路14			支路14
15	32			15		Fuse 15			Fuse 15			支路15			支路15
16	32			15		Fuse 16			Fuse 16			支路16			支路16
17	32			15		SMDUP12 DC Fuse		SMDUP12 DC Fuse		SMDUP12负载支路		SMDUP12负载支路
18	32			15			State			State			State			State
19	32			15			Off			Off			断开			断开
20	32			15			On			On			连接			连接
21	32			15		DC Fuse 1 Alarm			Fuse 1 Alm		负载支路1告警		支路1告警		
22	32			15		DC Fuse 2 Alarm			Fuse 2 Alm		负载支路2告警		支路2告警		
23	32			15		DC Fuse 3 Alarm			Fuse 3 Alm		负载支路3告警		支路3告警		
24	32			15		DC Fuse 4 Alarm			Fuse 4 Alm		负载支路4告警		支路4告警		
25	32			15		DC Fuse 5 Alarm			Fuse 5 Alm		负载支路5告警		支路5告警		
26	32			15		DC Fuse 6 Alarm			Fuse 6 Alm		负载支路6告警		支路6告警		
27	32			15		DC Fuse 7 Alarm			Fuse 7 Alm		负载支路7告警		支路7告警		
28	32			15		DC Fuse 8 Alarm			Fuse 8 Alm		负载支路8告警		支路8告警		
29	32			15		DC Fuse 9 Alarm			Fuse 9 Alm		负载支路9告警		支路9告警		
30	32			15		DC Fuse 10 Alarm		Fuse 10 Alm		负载支路10告警		支路10告警		
31	32			15		DC Fuse 11 Alarm		Fuse 11 Alm		负载支路11告警		支路11告警		
32	32			15		DC Fuse 12 Alarm		Fuse 12 Alm		负载支路12告警		支路12告警		
33	32			15		DC Fuse 13 Alarm		Fuse 13 Alm		负载支路13告警		支路13告警		
34	32			15		DC Fuse 14 Alarm		Fuse 14 Alm		负载支路14告警		支路14告警		
35	32			15		DC Fuse 15 Alarm		Fuse 15 Alm		负载支路15告警		支路15告警		
36	32			15		DC Fuse 16 Alarm		Fuse 16 Alm		负载支路16告警		支路16告警		
37	32			15			Times of Communication Fail		Times Comm Fail		通信失败次数		通信失败次数
38	32			15			Communication Fail			Comm Fail		通讯中断		通讯中断
39	32			15		Fuse 17			Fuse 17			支路17			支路17
40	32			15		Fuse 18			Fuse 18			支路18			支路18
41	32			15		Fuse 19			Fuse 19			支路19			支路19
42	32			15		Fuse 20			Fuse 20			支路20			支路20
43	32			15		Fuse 21			Fuse 21			支路21			支路21
44	32			15		Fuse 22			Fuse 22			支路22			支路22
45	32			15		Fuse 23			Fuse 23			支路23			支路23
46	32			15		Fuse 24			Fuse 24			支路24			支路24
47	32			15		Fuse 25			Fuse 25			支路25			支路25
48	32			15		DC Fuse 17 Alarm		Fuse 17 Alm			负载支路17告警		支路17告警		
49	32			15		DC Fuse 18 Alarm		Fuse 18 Alm			负载支路18告警		支路18告警		
50	32			15		DC Fuse 19 Alarm		Fuse 19 Alm			负载支路19告警		支路19告警		
51	32			15		DC Fuse 20 Alarm		Fuse 20 Alm			负载支路20告警		支路20告警		
52	32			15		DC Fuse 21 Alarm		Fuse 21 Alm			负载支路21告警		支路21告警		
53	32			15		DC Fuse 22 Alarm		Fuse 22 Alm			负载支路22告警		支路22告警		
54	32			15		DC Fuse 23 Alarm		Fuse 23 Alm			负载支路23告警		支路23告警		
55	32			15		DC Fuse 24 Alarm		Fuse 24 Alm			负载支路24告警		支路24告警		
56	32			15		DC Fuse 25 Alarm		Fuse 25 Alm			负载支路25告警		支路25告警	






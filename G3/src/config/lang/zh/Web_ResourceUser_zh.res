﻿
#Language resources file

[LOCAL_LANGUAGE]
zh

[LOCAL_LANGUAGE_VERSION]
1.00

[NUM_OF_PAGES]
39

[login.html:Number]
50

[login.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIP1					64			You are requesting access		你需要授权登录
2		ID_TIP2					32			located at				位于
3		ID_TIP3					128			The user name and password for this device is set by the system administrator.		用户名和密码由管理员设定.
4		ID_LOGIN				32			Login				登录
5		ID_USER					32			User				用户名
6		ID_PASSWD				32			Password				密码
7		ID_LOCAL_LANGUAGE			32			简体中文				简体中文
8		ID_SITE_NAME				32			Site Name				站点名
9		ID_SYSTEM_NAME				32			System Name				系统名
10		ID_PRODUCT_MODEL			32			Product Model			产品型号
11		ID_SERIAL				32			Serial Number			序列号
12		ID_HW_VERSION				32			Hardware Version			硬件版本
13		ID_SW_VERSION				32			Software Version			软件版本
14		ID_CONFIG_VERSION			32			Config Version			配置版本
15		ID_FORGET_PASSWD			32			Forgot Your Password?			忘记密码?
16		ID_ERROR0				64			Unknown Error										未知错误
17		ID_ERROR1				64			Successful											登录成功
18		ID_ERROR2				64			Incorrect Password										密码错误
19		ID_ERROR3				64		        Incorrect User Name										用户名错误
20		ID_ERROR4				64			Failed to communicate with the application.							与应用程序通讯失败.
21		ID_ERROR5				64			Over 5 connections, please retry later.							超过5个连接, 请稍候再试.
22		ID_ERROR6				128			Controller is starting, please retry later.							监控正在启动, 请稍候再试.
23		ID_ERROR7				256			Automatic configuration in progress, please wait about 2-5 minutes.				正在自动配置, 请稍等2-5分钟.
24		ID_ERROR8				64			Controller in Secondary Mode.								监控处于从机模式.
25		ID_ERROR9				256			Monitoring has restarted and the password needs to be initialized.						监控已重启，密码需要初始化.
25		ID_LOGIN1				32			LOGIN				登录
26		ID_SELECT				32			Please select language				请选择语言
27		ID_TIP4					32			Loading...				加载中...
28		ID_TIP5					256			Sorry, your browser doesn't support cookies or cookies are disabled. Please enable cookie or change your browser and login again.				对不起, 您的浏览器不支持或禁用了cookie! 请开启cookie或更换浏览器后再尝试登录!
29		ID_TIP6					128			Data lost.<a href='login.html' style='margin:0 10px;'>Retry</a>				数据不完整!<a href='login.html' style='margin:0 10px;'>重试</a>
30		ID_TIP7					64			Controller is starting, please wait...							监控正在启动中, 请稍候...
31		ID_TIP8					32			Logging In ...				登录中...
32		ID_TIP9					32			Login				登录
49		ID_FORGET_PASSWD			32			Forgot Your Password?						忘记密码?
50		ID_TIP10				32			Loading, please wait...					加载中, 请稍候...
51		ID_TIP11				64			Controller is in Secondary Mode.				监控处于从机模式.
52		ID_LOGIN_TITLE				32			Login-Vertiv G3						登录-Vertiv G3



[index.html:Number]
147

[index.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	STRING				LOCAL
1		ID_ALL_ALARMS				32			All Alarms				所有告警
2		ID_OBSERVATION				32			Observation				一般告警
3		ID_MAJOR				32			Major				重要告警
4		ID_CRITICAL				32			Critical				紧急告警
5		ID_CONTROLLER_SPECIFICATIONS		32			Controller Specifications		监控配置
6		ID_SYSTEM_NAME				32			System Name				系统名
7		ID_RECTIFIERS_NUMBER			32			Rectifiers				整流模块
8		ID_CONVERTERS_NUMBER			32			Converters				DC/DC模块
9		ID_SYSTEM_SPECIFICATIONS		32			System Specifications		系统配置
10		ID_PRODUCT_MODEL			32			Product Model			产品型号
11		ID_SERIAL_NUMBER			32			Serial Number			序列号	
12		ID_HARDWARE_VERSION			32			Hardware Version			硬件版本
13		ID_SOFTWARE_VERSION			32			Software Version			软件版本
14		ID_CONFIGURATION_VERSION		32			Config Version			配置版本
15		ID_HOME					32			Home				主页
16		ID_SETTINGS				32			Settings				设置
17		ID_HISTORY_LOG				32			History Log				历史记录
18		ID_SYSTEM_INVENTORY			32			System Inventory			产品信息
19		ID_ADVANCED_SETTING			32			Advanced Settings			高级设置
20		ID_INDEX				32			Index				序号
21		ID_ALARM_LEVEL				32			Alarm Level				告警级别
22		ID_RELATIVE				32			Relative Device			相关设备
23		ID_SIGNAL_NAME				32			Signal Name				信号名
24		ID_SAMPLE_TIME				32			Sample Time				采集时间
25		ID_WELCOME				32			Welcome				欢迎
26		ID_LOGOUT				32			Logout				退出
27		ID_AUTO_POPUP				32			Auto Popup				自动弹出
28		ID_COPYRIGHT				64			2018 Vertiv Tech Co.,Ltd.All rights reserved.		2018 Vertiv Tech Co.,Ltd.All rights reserved.
29		ID_OA					32			Observation				一般告警
30		ID_MA					32			Major				重要告警
31		ID_CA					32			Critical				紧急告警
32		ID_HOME1				32			Home				主页
33		ID_ERROR0				32			Failed.				失败.
34		ID_ERROR1				32			Successful.				成功.
35		ID_ERROR2				32			Failed. Conflicting setting.	失败,不满足关联条件.
36		ID_ERROR3				32			Failed. No privileges.		失败,没有权限.
37		ID_ERROR4				32			No information to send.		没有控制信息发送
38		ID_ERROR5				64			Failed. Controller is hardware protected.		失败,监控处于硬件保护状态
39		ID_ERROR6				64			Time expired. Please login again.				超时. 请重新登录.
40		ID_HIS_ERROR0				32			Unknown error.				未知错误.			
41		ID_HIS_ERROR1				32			Successful.					成功			
42		ID_HIS_ERROR2				32			No data.					没有查询到任何数据.	
43		ID_HIS_ERROR3				32			Failed					失败.				
44		ID_HIS_ERROR4				32			Failed. No privileges.			失败,没有权限.			
45		ID_HIS_ERROR5				64			Failed to communicate with the controller.	与监控通讯失败.		
46		ID_HIS_ERROR6				64			Clear successful.				清除成功		
47		ID_HIS_ERROR7				64			Time expired. Please login again.		超时. 请重新登录.
48		ID_WIZARD				32			Install Wizard				安装向导
49		ID_SITE					16			Site					站点
50		ID_PEAK_CURR				32			Peak Current				峰值电流
51		ID_INDEX_TIPS1				32			Loading, please wait.			加载中, 请稍候...
52		ID_INDEX_TIPS2				128			Data format error, getting the data again...please wait.			数据格式错误,正在重新获取数据...请稍候!
53		ID_INDEX_TIPS3				128			Fail to load the data, please check the network and data file.		数据加载失败!请检查网络和数据文件!
54		ID_INDEX_TIPS4				128			Template webpage not exist or network error.			模板页面不存在或网络错误!
55		ID_INDEX_TIPS5				64			Data lost.			请求数据不完整!
56		ID_INDEX_TIPS6				64			Setting...please wait.			设置中...请稍候!
57		ID_INDEX_TIPS7				64			Confirm logout?			您确定要退出系统吗?
58		ID_INDEX_TIPS8				64			Loading data, please wait.			正在加载数据, 请稍候...
59		ID_INDEX_TIPS9				64			Please shut down the browser to return to the home page.		关闭并返回首页
60		ID_INDEX_TIPS10				16			OK				确定
61		ID_INDEX_TIPS11				16			Error:			错误:
62		ID_INDEX_TIPS12				16			Retry			重试
63		ID_INDEX_TIPS13				16			Loading...			加载中...
64		ID_INDEX_TIPS14				64			Time expired. Please login again.			登录超时, 需重新登录.
65		ID_INDEX_TIPS15				32			Re-loading			重新加载
66		ID_INDEX_TIPS16				128			File does not exist or unable to load the file due to a network error.			文件不存在或网络错误等，导致加载失败!
67		ID_INDEX_TIPS17				32			id is \"			id为\"
68		ID_INDEX_TIPS18				32			Request error.			请求出错!
69		ID_INDEX_TIPS19				32			Login again.			重新登录!
70		ID_INDEX_TIPS20				32			No Data			无数据
71		ID_INDEX_TIPS21				128			Can't be empty, please enter 0 or a positive number within range.		不能为空,必须为正整数或0,且不能超出设置范围!
72		ID_INDEX_TIPS22				128			Can't be empty, please enter 0 or an integer number or a float number within range.	不能为空,必须为整数,0或浮点数,且不能超出设置范围!
73		ID_INDEX_TIPS23				128			Can't be empty, please enter 0 or a positive number or a negative number within range.	不能为空,正整数或负整数或0,且不能超出设置范围!
74		ID_INDEX_TIPS24				128			No change to the current value.			所设值相同,无须再设置!
75		ID_INDEX_TIPS25				128			Please input an email address in the form name@domain.			请输入正确的邮箱格式!
76		ID_INDEX_TIPS26				64			Please enter an IP address in the form nnn.nnn.nnn.nnn.			请输入正确的IP格式!
77		ID_INDEX_TIME1				16			January			一月
78		ID_INDEX_TIME2				16			February			二月
79		ID_INDEX_TIME3				16			March			三月
80		ID_INDEX_TIME4				16			April			四月
81		ID_INDEX_TIME5				16			May				五月
82		ID_INDEX_TIME6				16			June			六月
83		ID_INDEX_TIME7				16			July			七月
84		ID_INDEX_TIME8				16			August			八月
85		ID_INDEX_TIME9				16			September			九月
86		ID_INDEX_TIME10				16			October			十月
87		ID_INDEX_TIME11				16			November			十一月
88		ID_INDEX_TIME12				16			December			十二月
89		ID_INDEX_TIME13				16			Jan			一月
90		ID_INDEX_TIME14				16			Feb			二月
91		ID_INDEX_TIME15				16			Mar			三月
92		ID_INDEX_TIME16				16			Apr			四月
93		ID_INDEX_TIME17				16			May				五月
94		ID_INDEX_TIME18				16			Jun			六月
95		ID_INDEX_TIME19				16			Jul			七月
96		ID_INDEX_TIME20				16			Aug			八月
97		ID_INDEX_TIME21				16			Sep			九月
98		ID_INDEX_TIME22				16			Oct			十月
99		ID_INDEX_TIME23				16			Nov			十一月
100		ID_INDEX_TIME24				16			Dec			十二月
101		ID_INDEX_TIME25				16			Sunday			星期日
102		ID_INDEX_TIME26				16			Monday			星期一
103		ID_INDEX_TIME27				16			Tuesday			星期二
104		ID_INDEX_TIME28				16			Wednesday			星期三
105		ID_INDEX_TIME29				16			Thursday			星期四
106		ID_INDEX_TIME30				16			Friday			星期五
107		ID_INDEX_TIME31				16			Saturday			星期六
108		ID_INDEX_TIME32				16			Sun			日
109		ID_INDEX_TIME33				16			Mon			一
110		ID_INDEX_TIME34				16			Tue			二
111		ID_INDEX_TIME35				16			Wed			三
112		ID_INDEX_TIME36				16			Thu			四
113		ID_INDEX_TIME37				16			Fri			五
114		ID_INDEX_TIME38				16			Sat			六
115		ID_INDEX_TIME39				16			Sun			日
116		ID_INDEX_TIME40				16			Mon			一
117		ID_INDEX_TIME41				16			Tue			二
118		ID_INDEX_TIME42				16			Wed			三
119		ID_INDEX_TIME43				16			Thu			四
120		ID_INDEX_TIME44				16			Fri			五
121		ID_INDEX_TIME45				16			Sat			六
122		ID_INDEX_TIME46				16			Current Time		当前时间
123		ID_INDEX_TIME47				16			Confirm			确定
124		ID_INDEX_TIME48				16			Time			时间
125		ID_INDEX_TIME49				16			Hour			时
126		ID_INDEX_TIME50				16			Minute			分
127		ID_INDEX_TIME51				16			Second			秒
128		ID_MPPTS				16			Solar Converters		太阳能模块
129		ID_INDEX_TIPS27				32			Refresh			刷新
130		ID_INDEX_TIPS28				32			There is no data to show.				无数据!
131		ID_INDEX_TIPS29				128			Fail to connect to controller, please login again.		与监控连接失败, 请重新登录!
132		ID_INDEX_TIPS30				128			Link attribute \"data\" data structure error, should be {} object format.			链接属性\"data\"数据结构错误, 应为{}对象格式!
133		ID_INDEX_TIPS31				256			The format of \"validate\" is incorrect.  It should be have {} object format.  Please check the template file.			当前行Input的\"validate\"格式错误, 应为{}对象格式, 请检查模板文件!
134		ID_INDEX_TIPS32				128			Data format error.			数据格式错误!
135		ID_INDEX_TIPS33				64			Setting date error.			日期设置错误!
136		ID_INDEX_TIPS34				128			There is an error in the input parameter.  It should be in the format ({}).		传入参数格式错误，必须为对象格式({})!
137		ID_INDEX_TIPS35				32			Too many clicks. Please wait.			点击过快, 请稍候再试.
138		ID_INDEX_TIPS36				32			Refresh webpage			刷新当前页
139		ID_INDEX_TIPS37				32			Refresh console			刷新控制台
140		ID_INDEX_TIPS38				128			Special characters are not allowed.			不允许包含特殊字符!
141		ID_INDEX_TIPS39				64			The value can not be empty.			不能为空值!
142		ID_INDEX_TIPS40				64			The value must be a positive integer or 0.			数值必须为正整数或0!
143		ID_INDEX_TIPS41				64			The value must be a floating point number.			数值必须为浮点数!
144		ID_INDEX_TIPS42				32			The value must be an integer.			数值必须为整数!
145		ID_INDEX_TIPS43				64			The value is out of range.			数值超出范围!
146		ID_INDEX_TIPS44				32			Communication fail.			通讯中断!
147		ID_INDEX_TIPS45				64			Confirm the change to the control value?			确定控制?
148		ID_INDEX_TIPS46				128			Time format error or time is beyond the range. The format should be: Year/Month/Day Hour/Minute/Second			时间格式错误, 或时间超出范围. 正确格式为: 年/月/日 时:分:秒.
149		ID_INDEX_TIPS47				32			Unknown error.				未知错误.			
150		ID_INDEX_TIPS48				64			(Data is beyond the normal range.)	(数据超出正常范围!)
151		ID_INDEX_TIPS49				12			Date:				日期:
152		ID_INDEX_TIPS50				32			Temperature Trend Diagram		温度趋势图
153		ID_INDEX_TIPS51				16			Tips					提示
154		ID_TIPS1				32			Unknown error.			未知错误
155		ID_TIPS2				16			Successful.							成功.
156		ID_TIPS3				128			Failed. Incorrect time setting.				失败,不正确的时间设置.
157		ID_TIPS4				128			Failed. Incomplete information.				失败,输入信息不完整.
158		ID_TIPS5				64			Failed. No privileges.					失败,没有权限.
159		ID_TIPS6				128			Cannot be modified. Controller is hardware protected.	监控处于硬件保护状态,不能修改
160		ID_TIPS7				256			Secondary Server IP Address is incorrect. \nThe format should be :'nnn.nnn.nnn.nnn'.	从服务器IP地址格式错误,格式应该如下所示:'nnn.nnn.nnn.nnn'.
161		ID_TIPS8				128			Format error! There is one blank between time and date.					格式错误! 时间和日期间只允许一个空格!
162		ID_TIPS9				128					Incorrect time interval. \nTime interval should be a positive integer.			时间间隔错误,应该为正整数.
163		ID_TIPS10				128			Date should be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.			日期应该在'1970/01/01 00:00:00' 和 '2038/01/01 00:00:00'之间.
164		ID_TIPS11				128					Please clear the IP before time setting.							请先清除IP再设置时间.
165		ID_TIPS12				64			Time expired, please login again.						登录超时, 请重新登录.
166		ID_TIPS13				16			Site						站点
167		ID_TIPS14				16			System Name						系统名
168		ID_TIPS15				32			Back to Battery List		返回电池列表
169		ID_TIPS16				64			Capacity Trend Diagram		容量趋势图
170		ID_INDEX_TIPS52				64			Set successfully. Controller is restarting, please wait	设置成功, 监控正在重启, 请等待
171		ID_INDEX_TIPS53				16			seconds.					秒.
172		ID_INDEX_TIPS54				64			Back to the login page, please wait.		返回到登录页面, 请等待.
173		ID_INDEX_TIPS55				32			Confirm to be set to				确定设置成
174		ID_INDEX_TIPS56				16			's value is					的值为
175		ID_INDEX_TIPS57				32			controller will restart.					监控将会重启.
176		ID_INDEX_TIPS58				64			Template error, please refresh the page.		模板解析错误, 请刷新页面.
177		ID_INDEX_TIPS59				64			[OK]Reconnect. [Cancel]Stop the page.		[确定]重连. [取消]停止当前页面.
178		ID_ENGINEER				32			Engineer Settings				工程师设置
179		ID_INDEX_TIPS60				128			Jump to the engineer web pages, please use IE browser.		跳转到工程师页面, 请使用IE浏览器.
180		ID_INDEX_TIPS61				64			Jumping, please wait.		跳转中, 请稍候.
181		ID_INDEX_TIPS62				64			Fail to jump.		跳转失败.
182		ID_INDEX_TIPS63				64			Please select new alarm level.		请选择新的告警级别.
183		ID_INDEX_TIPS64				64			Please select new relay number.		请选择新的继电器号.
184		ID_INDEX_TIPS65				64			After setting you need to wait		设置此功能需要等待
185		ID_OA1					16			OA				OA
186		ID_MA1					16			MA				MA
187		ID_CA1					16			CA				CA
188		ID_INDEX_TIPS66				128			After restarting, please clear the browser cache and login again.		重启后请清除浏览器缓存再次登录.
189		ID_DOWNLOAD_ERROR0			32			Unknown error.								未知错误.		
190		ID_DOWNLOAD_ERROR1			64			File downloaded successfully.						下载文件成功.		
191		ID_DOWNLOAD_ERROR2			64			Failed to download file.							下载文件失败.		
192		ID_DOWNLOAD_ERROR3			64			Failed to download, the file is too large.				文件太大, 下载文件失败.	
193		ID_DOWNLOAD_ERROR4			64			Failed. No privileges.							失败, 没有权限.		
194		ID_DOWNLOAD_ERROR5			64			Controller started successfully.						监控启动成功.	
195		ID_DOWNLOAD_ERROR6			64			File downloaded successfully.						下载文件成功.		
196		ID_DOWNLOAD_ERROR7			64			Failed to download file.							下载文件失败.		
197		ID_DOWNLOAD_ERROR8			64			Failed to upload file.							上传文件失败.		
198		ID_DOWNLOAD_ERROR9			64			File downloaded successfully.						下载文件成功.		
199		ID_DOWNLOAD_ERROR10			64			Failed to upload file. Hardware is protected.				下载文件失败, 硬件保护.
200		ID_SYSTEM_VOLTAGE			32			Output Voltage			输出电压
201		ID_SYSTEM_CURRENT			32			Output Current			输出电流
202		ID_SYS_STATUS				32			System Status			系统状态
203		ID_INDEX_TIPS67				64			There was an error on this page.		页面存在错误.
204		ID_INDEX_TIPS68				16			Error				错误
205		ID_INDEX_TIPS69				16			Line				行
206		ID_INDEX_TIPS70				64			Click OK to continue.		点击确定继续.
207		ID_INDEX_TIPS71				32			Request error, please retry.	请求错误, 请重试.
#//changed by Frank Wu,1/N/27,20140527, for power split
208		ID_INDEX_TIPS72				32				Please select						请选择
209		ID_INDEX_TIPS73				16				NA									NA
210		ID_INDEX_TIPS74				64				Please select equipment.			请选择的设备.
211		ID_INDEX_TIPS75				64				Please select signal type.			请选择信号类型.
212		ID_INDEX_TIPS76				64				Please select signal.				请选择信号.
213		ID_INDEX_TIPS77				64				Full name is too long				信号全名太长
#//changed by Frank Wu,1/N/N,20140716, for loading different language css file, eg. skin.en.css
#//Note:please don't translate the special ID_CSS_LAN
214		ID_CSS_LAN					16				en									zh
215		ID_CON_VOLT				32				Converter Voltage				DC/DC电压
216		ID_CON_CURR				32				Converter Current				DC/DC电流
217		ID_INDEX_TIPS78				128				The High 2 Curr Limit Alarm must be larger than the High 1 Curr Limit Alarm				过流2告警必须大于过流1告警
218		ID_INDEX_TIPS79				128			Time format error or time is beyond the range. The format should be: Day/Month/Year Hour/Minute/Second			时间格式错误, 或时间超出范围. 正确格式为: 日/月/年 时:分:秒.
219		ID_INDEX_TIPS80				128			Time format error or time is beyond the range. The format should be: Month/Day/Year Hour/Minute/Second			时间格式错误, 或时间超出范围. 正确格式为: 月/日/年 时:分:秒.
220		ID_INDEX_TIPS81					16				en					zh
[tmp.index.html:Number]
22

[tmp.index.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	STRING				LOCAL
1		ID_SYSTEM				32			Power System			电源系统
2		ID_HYBRID				32			Energy Sources			能源趋势
3		ID_SOLAR				32			Solar				太阳能
4		ID_SOLAR_RECT				32			Solar Converter			太阳能模块
5		ID_MAINS				32			Mains				市电
6		ID_RECTIFIER				32			Rectifier				整流模块
7		ID_DG					32			DG					油机
8		ID_CONVERTER				32			Converter				DC/DC模块
9		ID_DC					32			DC					直流
10		ID_BATTERY				32			Battery				电池
11		ID_SYSTEM_VOLTAGE			32			Output Voltage			输出电压
12		ID_SYSTEM_CURRENT			32			Output Current			输出电流
13		ID_LOAD_TREND				32			Load Trend				负载趋势
14		ID_DC1					32			DC					直流
15		ID_AMBIENT_TEMP				32			Ambient Temp			环境温度
16		ID_PEAK_CURRENT				32			Peak Current			峰值电流
17		ID_AVERAGE_CURRENT			32			Average Current			平均电流
18		ID_L1					32			R					R
19		ID_L2					32			S					S
20		ID_L3					32			T					T
21		ID_BATTERY1				32			Battery				电池
22		ID_TIPS					64			Data is beyond the normal range.	数据超出正常范围!
23		ID_CONVERTER1				32			Converter				DC/DC模块
24		ID_SMIO					16			SMIO				SMIO
25		ID_TIPS2				32			No Sensor				无传感器
26		ID_USER_DEF				32			User Define				用户定义
27		ID_CONSUM_MAP				32			Consumption Map			消耗分布图
28		ID_SAMPLE				32			Sample Signal			采集信号
29		ID_DISTRI_BAY				32			Distribution Bay			分配域
30		ID_SOURCE				32			Source			资源



[tmp.hybrid.html:Number]
9

[tmp.hybrid.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	STRING				LOCAL
1		ID_MAINS				32			Mains				市电
2		ID_SOLAR				32			Solar				太阳能
3		ID_DG					32			DG					油机
4		ID_WIND					32			Wind				风能
5		ID_1_WEEK				32			This Week				最新一周
6		ID_2_WEEK				32			Last Week				上一周
7		ID_3_WEEK				32			Two Weeks Ago			两周之前
8		ID_4_WEEK				32			Three Weeks Ago			三周之前
9		ID_NO_DATA				32			No Data				无数据
10		ID_TIMEFORMAT				32			Date:MM-DD				日期:MM-DD


[tmp.system_rectifier.html:Number]
16

[tmp.system_rectifier.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_RECTIFIER				32			Rectifier				整流模块
2		ID_CONVERTER				32			Converter				DC/DC模块
3		ID_SOLAR				32			Solar Converter			太阳能模块
4		ID_VOLTAGE				32			Average Voltage			平均电压
5		ID_CURRENT				32			Total Current			总电流
6		ID_CAPACITY_USED			32			System Capacity Used		系统功率消耗
7		ID_NUM_OF_RECT				32			Number of Rectifiers		模块数量
8		ID_TOTAL_COMM_RECT			32			Total Rectifiers Communicating	通讯模块数量
9		ID_MAX_CAPACITY				32			Max Used Capacity			最大功率消耗
10		ID_SIGNAL				32			Signal				信号
11		ID_VALUE				32			Value				值
12		ID_SOLAR1				32			Solar Converter			太阳能模块
13		ID_CURRENT1				32			Total Current			总电流
14		ID_RECTIFIER1				32			GI Rectifier			GI整流模块
15		ID_RECTIFIER2				32			GII Rectifier			GII整流模块
16		ID_RECTIFIER3				32			GIII Rectifier			GIII整流模块
17		ID_RECT_SET				32			Rectifier Settings			模块设置
18		ID_BACK					16			Back				返回


[tmp.system_rectifierS1.html:Number]
16

[tmp.system_rectifierS1.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_RECTIFIER				32			Rectifier				整流模块
2		ID_CONVERTER				32			Converter				DC/DC模块
3		ID_SOLAR				32			Solar Converter				太阳能模块
4		ID_VOLTAGE				32			Average Voltage			平均电压
5		ID_CURRENT				32			Total Current			总电流
6		ID_CAPACITY_USED			32			System Capacity Used		系统功率消耗
7		ID_NUM_OF_RECT				32			Number of Rectifiers		模块数量
8		ID_TOTAL_COMM_RECT			32			Number of Rects in communication	通讯模块数量
9		ID_MAX_CAPACITY				32			Max Used Capacity			最大功率消耗
10		ID_SIGNAL				32			Signal				信号
11		ID_VALUE				32			Value				值
12		ID_SOLAR1				32			Solar Converter				太阳能模块
13		ID_CURRENT1				32			Total Current			总电流
14		ID_RECTIFIER1				32			GI Rectifier			GI整流模块
15		ID_RECTIFIER2				32			GII Rectifier			GII整流模块
16		ID_RECTIFIER3				32			GIII Rectifier			GIII整流模块
#//changed by Frank Wu,1/1/30,20140527, for add single converter and single solar settings pages
17		ID_RECT_SET				32			Secondary Rectifier Settings			从机模块设置
18		ID_BACK					16			Back						返回


[tmp.system_battery.html:Number]
10

[tmp.system_battery.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_BATTERY				32			Battery				电池
2		ID_MANAGE_STATE				32			Battery Management State		电池管理状态
3		ID_BATT_CURR				32			Total Battery Current		电池总电流
4		ID_REMAIN_TIME				32			Estimated Remaining Time		预计剩余时间
5		ID_TEMP					32			Battery Temp			电池温度
6		ID_SIGNAL				32			Signal				信号
7		ID_VALUE				32			Value				值
8		ID_SIGNAL1				32			Signal				信号
9		ID_VALUE1				32			Value				值
10		ID_TIPS					32			Back to Battery List		返回电池列表
11		ID_SIGNAL2				32			Signal				信号
12		ID_VALUE2				32			Value				值
13		ID_TIP1					64			Capacity Trend Diagram		容量趋势图
14		ID_TIP2					64			Temperature Trend Diagram		温度趋势图
15		ID_TIP3					32			No Sensor		无传感器
16		ID_EIB					16			EIB		EIB



[tmp.system_dc.html:Number]
5

[tmp.system_dc.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_DC					32			DC					直流
2		ID_SIGNAL				32			Signal				信号
3		ID_VALUE				32			Value				值
4		ID_SIGNAL1				32			Signal				信号
5		ID_VALUE1				32			Value				值
6		ID_SMDUH				32			SMDUH				SMDUH
7		ID_EIB					32			EIB					EIB
8		ID_DC_METER				32			DC Meter				直流电表
9		ID_SMDU					32			SMDU				SMDU
10		ID_SMDUP				32			SMDUP				SMDUP
11		ID_NO_DATA				32			No Data				无数据
12		ID_SMDUP1				32			SMDUP1				SMDUP1
13		ID_CABINET				32			Cabinet Map				机柜配置
14		ID_NARADA_BMS				32			BMS				BMS
15		ID_SMDUE				32			SMDUE				SMDUE


[tmp.history_alarmlog.html:Number]
17

[tmp.history_alarmlog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_ALARM				32			Alarm History Log			历史告警记录
2		ID_BATT_TEST				32			Battery Test Log			电池测试记录
3		ID_EVENT				32			Event Log				历史事件记录
4		ID_DATA					32			Data History Log			历史数据记录
5		ID_DEVICE				32			Device				设备
6		ID_FROM					32			From				从
7		ID_TO					32			To					到
8		ID_QUERY				32			Query				查询
9		ID_UPLOAD				32			Upload				上载
10		ID_TIPS					64			Displays the last 500 entries		显示最多500条日志
11		ID_INDEX				32			Index				序号
12		ID_DEVICE1				32			Device Name				设备名
13		ID_SIGNAL				32			Signal Name				信号名
14		ID_LEVEL				32			Alarm Level				告警级别
15		ID_START				32			Start Time				开始时间
16		ID_END					32			End Time				结束时间
17		ID_ALL_DEVICE				32			All Devices				所有设备
#//changed by Frank Wu,1/N/14,20140527, for system log
18		ID_SYSTEMLOG			32				System Log				系统日志记录
19		ID_OA					32			OA				OA
20		ID_MA					32			MA				MA
21		ID_CA					32			CA				CA


[tmp.history_testlog.html:Number]
900

[tmp.history_testlog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS					64			Choose the last battery test	选择最新的电池测试次数
2		ID_SUMMARY_HEAD0			32			Start Time				开始时间
3		ID_SUMMARY_HEAD1			32			End Time				结束时间
4		ID_SUMMARY_HEAD2			32			Start Reason			开始原因
5		ID_SUMMARY_HEAD3			32			End Reason				结束原因
6		ID_SUMMARY_HEAD4			32			Test Result			测试结果
7		ID_QUERY				32			Query				查询
8		ID_UPLOAD				32			Upload				上载
9		ID_START_REASON0			64			Start Planned Test			计划测试	
10		ID_START_REASON1			64			Start Manual Test			手动测试	
11		ID_START_REASON2			64			Start AC Fail Test			交流停电测试	
12		ID_START_REASON3			64			Start Master Battery Test		跟随主机测试	
13		ID_START_REASON4			64			Start Test for Other Reasons			其他原因	
14		ID_END_REASON0				64			End Test Manually						手动结束测试			
15		ID_END_REASON1				64			End Test for Alarm						告警结束测试			
16		ID_END_REASON2				64			End Test for Test Time-Out					测试时间到结束测试		
17		ID_END_REASON3				64			End Test for Capacity Condition				容量条件到结束测试		
18		ID_END_REASON4				64			End Test for Voltage Condition				电压条件到结束测试	
19		ID_END_REASON5				64			End Test for AC Fail					交流停电结束测试			
20		ID_END_REASON6				64			End AC Fail Test for AC Restore				交流来电结束测试			
21		ID_END_REASON7				64			End AC Fail Test for Being Disabled				禁止交流停电测试			
22		ID_END_REASON8				64			End Master Battery Test					跟随主机结束测试			
23		ID_END_REASON9				64			End PowerSplit BT for Auto to Manual			自动转手动结束测试			
24		ID_END_REASON10				64			End PowerSplit Man-BT for Manual to Auto			手动转自动结束测试			
25		ID_END_REASON11				64			End Test for Other Reasons					其他原因结束测试			
26		ID_RESULT0				16			No Result.			没有结果
27		ID_RESULT1				16			Battery is OK			电池正常
28		ID_RESULT2				16			Battery is Bad			电池坏	
29		ID_RESULT3				32			It's PowerSplit Test		PowerSplit测试
30		ID_RESULT4				16			Other results.			其他结果
31		ID_SUMMARY0				32			Index				序号	
32		ID_SUMMARY1				32			Record Time				记录时间
33		ID_SUMMARY2				32			System Voltage			系统电压
34		ID_HEAD0				32			Battery1 Current			电池1电流	
35		ID_HEAD1				32			Battery1 Voltage			电池1电压	
36		ID_HEAD2				32			Battery1 Capacity			电池1容量	
37		ID_HEAD3				32			Battery2 Current			电池2电流	
38		ID_HEAD4				32			Battery2 Voltage			电池2电压	
39		ID_HEAD5				32			Battery2 Capacity			电池2容量	
40		ID_HEAD6				32			EIB1Battery1 Current		EIB1电池1电流	
41		ID_HEAD7				32			EIB1Battery1 Voltage		EIB1电池1电压	
42		ID_HEAD8				32			EIB1Battery1 Capacity		EIB1电池1容量	
43		ID_HEAD9				32			EIB1Battery2 Current		EIB1电池2电流	
44		ID_HEAD10				32			EIB1Battery2 Voltage		EIB1电池2电压	
45		ID_HEAD11				32			EIB1Battery2 Capacity		EIB1电池2容量	
46		ID_HEAD12				32			EIB2Battery1 Current		EIB2电池1电流	
47		ID_HEAD13				32			EIB2Battery1 Voltage		EIB2电池1电压	
48		ID_HEAD14				32			EIB2Battery1 Capacity		EIB2电池1容量	
49		ID_HEAD15				32			EIB2Battery2 Current		EIB2电池2电流	
50		ID_HEAD16				32			EIB2Battery2 Voltage		EIB2电池2电压	
51		ID_HEAD17				32			EIB2Battery2 Capacity		EIB2电池2容量	
52		ID_HEAD18				32			EIB3Battery1 Current		EIB3电池1电流	
53		ID_HEAD19				32			EIB3Battery1 Voltage		EIB3电池1电压	
54		ID_HEAD20				32			EIB3Battery1 Capacity		EIB3电池1容量	
55		ID_HEAD21				32			EIB3Battery2 Current		EIB3电池2电流	
56		ID_HEAD22				32			EIB3Battery2 Voltage		EIB3电池2电压	
57		ID_HEAD23				32			EIB3Battery2 Capacity		EIB3电池2容量	
58		ID_HEAD24				32			EIB4Battery1 Current		EIB4电池1电流	
59		ID_HEAD25				32			EIB4Battery1 Voltage		EIB4电池1电压	
60		ID_HEAD26				32			EIB4Battery1 Capacity		EIB4电池1容量	
61		ID_HEAD27				32			EIB4Battery2 Current		EIB4电池2电流	
62		ID_HEAD28				32			EIB4Battery2 Voltage		EIB4电池2电压	
63		ID_HEAD29				32			EIB4Battery2 Capacity		EIB4电池2容量	
64		ID_HEAD30				32			SMDU1Battery1 Current		SMDU1电池1电流		
65		ID_HEAD31				32			SMDU1Battery1 Voltage		SMDU1电池1电压		
66		ID_HEAD32				32			SMDU1Battery1 Capacity		SMDU1电池1容量		
67		ID_HEAD33				32			SMDU1Battery2 Current		SMDU1电池2电流		
68		ID_HEAD34				32			SMDU1Battery2 Voltage		SMDU1电池2电压		
69		ID_HEAD35				32			SMDU1Battery2 Capacity		SMDU1电池2容量		
70		ID_HEAD36				32			SMDU1Battery3 Current		SMDU1电池3电流		
71		ID_HEAD37				32			SMDU1Battery3 Voltage		SMDU1电池3电压		
72		ID_HEAD38				32			SMDU1Battery3 Capacity		SMDU1电池3容量		
73		ID_HEAD39				32			SMDU1Battery4 Current		SMDU1电池4电流		
74		ID_HEAD40				32			SMDU1Battery4 Voltage		SMDU1电池4电压		
75		ID_HEAD41				32			SMDU1Battery4 Capacity		SMDU1电池4容量		
76		ID_HEAD42				32			SMDU2Battery1 Current		SMDU2电池1电流		
77		ID_HEAD43				32			SMDU2Battery1 Voltage		SMDU2电池1电压		
78		ID_HEAD44				32			SMDU2Battery1 Capacity		SMDU2电池1容量		
79		ID_HEAD45				32			SMDU2Battery2 Current		SMDU2电池2电流		
80		ID_HEAD46				32			SMDU2Battery2 Voltage		SMDU2电池2电压		
81		ID_HEAD47				32			SMDU2Battery2 Capacity		SMDU2电池2容量		
82		ID_HEAD48				32			SMDU2Battery3 Current		SMDU2电池3电流		
83		ID_HEAD49				32			SMDU2Battery3 Voltage		SMDU2电池3电压		
84		ID_HEAD50				32			SMDU2Battery3 Capacity		SMDU2电池3容量		
85		ID_HEAD51				32			SMDU2Battery4 Current		SMDU2电池4电流		
86		ID_HEAD52				32			SMDU2Battery4 Voltage		SMDU2电池4电压		
87		ID_HEAD53				32			SMDU2Battery4 Capacity		SMDU2电池4容量		
88		ID_HEAD54				32			SMDU3Battery1 Current		SMDU3电池1电流		
89		ID_HEAD55				32			SMDU3Battery1 Voltage		SMDU3电池1电压		
90		ID_HEAD56				32			SMDU3Battery1 Capacity		SMDU3电池1容量		
91		ID_HEAD57				32			SMDU3Battery2 Current		SMDU3电池2电流		
92		ID_HEAD58				32			SMDU3Battery2 Voltage		SMDU3电池2电压		
93		ID_HEAD59				32			SMDU3Battery2 Capacity		SMDU3电池2容量		
94		ID_HEAD60				32			SMDU3Battery3 Current		SMDU3电池3电流		
95		ID_HEAD61				32			SMDU3Battery3 Voltage		SMDU3电池3电压		
96		ID_HEAD62				32			SMDU3Battery3 Capacity		SMDU3电池3容量		
97		ID_HEAD63				32			SMDU3Battery4 Current		SMDU3电池4电流		
98		ID_HEAD64				32			SMDU3Battery4 Voltage		SMDU3电池4电压		
99		ID_HEAD65				32			SMDU3Battery4 Capacity		SMDU3电池4容量		
100		ID_HEAD66				32			SMDU4Battery1 Current		SMDU4电池1电流		
101		ID_HEAD67				32			SMDU4Battery1 Voltage		SMDU4电池1电压		
102		ID_HEAD68				32			SMDU4Battery1 Capacity		SMDU4电池1容量		
103		ID_HEAD69				32			SMDU4Battery2 Current		SMDU4电池2电流		
104		ID_HEAD70				32			SMDU4Battery2 Voltage		SMDU4电池2电压		
105		ID_HEAD71				32			SMDU4Battery2 Capacity		SMDU4电池2容量		
106		ID_HEAD72				32			SMDU4Battery3 Current		SMDU4电池3电流		
107		ID_HEAD73				32			SMDU4Battery3 Voltage		SMDU4电池3电压		
108		ID_HEAD74				32			SMDU4Battery3 Capacity		SMDU4电池3容量		
109		ID_HEAD75				32			SMDU4Battery4 Current		SMDU4电池4电流		
110		ID_HEAD76				32			SMDU4Battery4 Voltage		SMDU4电池4电压		
111		ID_HEAD77				32			SMDU4Battery4 Capacity		SMDU4电池4容量		
112		ID_HEAD78				32			SMDU5Battery1 Current		SMDU5电池1电流		
113		ID_HEAD79				32			SMDU5Battery1 Voltage		SMDU5电池1电压		
114		ID_HEAD80				32			SMDU5Battery1 Capacity		SMDU5电池1容量		
115		ID_HEAD81				32			SMDU5Battery2 Current		SMDU5电池2电流		
116		ID_HEAD82				32			SMDU5Battery2 Voltage		SMDU5电池2电压		
117		ID_HEAD83				32			SMDU5Battery2 Capacity		SMDU5电池2容量		
118		ID_HEAD84				32			SMDU5Battery3 Current		SMDU5电池3电流		
119		ID_HEAD85				32			SMDU5Battery3 Voltage		SMDU5电池3电压		
120		ID_HEAD86				32			SMDU5Battery3 Capacity		SMDU5电池3容量		
121		ID_HEAD87				32			SMDU5Battery4 Current		SMDU5电池4电流		
122		ID_HEAD88				32			SMDU5Battery4 Voltage		SMDU5电池4电压		
123		ID_HEAD89				32			SMDU5Battery4 Capacity		SMDU5电池4容量		
124		ID_HEAD90				32			SMDU6Battery1 Current		SMDU6电池1电流		
125		ID_HEAD91				32			SMDU6Battery1 Voltage		SMDU6电池1电压		
126		ID_HEAD92				32			SMDU6Battery1 Capacity		SMDU6电池1容量		
127		ID_HEAD93				32			SMDU6Battery2 Current		SMDU6电池2电流		
128		ID_HEAD94				32			SMDU6Battery2 Voltage		SMDU6电池2电压		
129		ID_HEAD95				32			SMDU6Battery2 Capacity		SMDU6电池2容量		
130		ID_HEAD96				32			SMDU6Battery3 Current		SMDU6电池3电流		
131		ID_HEAD97				32			SMDU6Battery3 Voltage		SMDU6电池3电压		
132		ID_HEAD98				32			SMDU6Battery3 Capacity		SMDU6电池3容量		
133		ID_HEAD99				32			SMDU6Battery4 Current		SMDU6电池4电流		
134		ID_HEAD100				32			SMDU6Battery4 Voltage		SMDU6电池4电压		
135		ID_HEAD101				32			SMDU6Battery4 Capacity		SMDU6电池4容量		
136		ID_HEAD102				32			SMDU7Battery1 Current		SMDU7电池1电流		
137		ID_HEAD103				32			SMDU7Battery1 Voltage		SMDU7电池1电压		
138		ID_HEAD104				32			SMDU7Battery1 Capacity		SMDU7电池1容量		
139		ID_HEAD105				32			SMDU7Battery2 Current		SMDU7电池2电流		
140		ID_HEAD106				32			SMDU7Battery2 Voltage		SMDU7电池2电压		
141		ID_HEAD107				32			SMDU7Battery2 Capacity		SMDU7电池2容量		
142		ID_HEAD108				32			SMDU7Battery3 Current		SMDU7电池3电流		
143		ID_HEAD109				32			SMDU7Battery3 Voltage		SMDU7电池3电压		
144		ID_HEAD110				32			SMDU7Battery3 Capacity		SMDU7电池3容量		
145		ID_HEAD111				32			SMDU7Battery4 Current		SMDU7电池4电流		
146		ID_HEAD112				32			SMDU7Battery4 Voltage		SMDU7电池4电压		
147		ID_HEAD113				32			SMDU7Battery4 Capacity		SMDU7电池4容量		
148		ID_HEAD114				32			SMDU8Battery1 Current		SMDU8电池1电流		
149		ID_HEAD115				32			SMDU8Battery1 Voltage		SMDU8电池1电压		
150		ID_HEAD116				32			SMDU8Battery1 Capacity		SMDU8电池1容量		
151		ID_HEAD117				32			SMDU8Battery2 Current		SMDU8电池2电流		
152		ID_HEAD118				32			SMDU8Battery2 Voltage		SMDU8电池2电压		
153		ID_HEAD119				32			SMDU8Battery2 Capacity		SMDU8电池2容量		
154		ID_HEAD120				32			SMDU8Battery3 Current		SMDU8电池3电流		
155		ID_HEAD121				32			SMDU8Battery3 Voltage		SMDU8电池3电压		
156		ID_HEAD122				32			SMDU8Battery3 Capacity		SMDU8电池3容量		
157		ID_HEAD123				32			SMDU8Battery4 Current		SMDU8电池4电流		
158		ID_HEAD124				32			SMDU8Battery4 Voltage		SMDU8电池4电压		
159		ID_HEAD125				32			SMDU8Battery4 Capacity		SMDU8电池4容量		
160		ID_HEAD126				32			EIB1Battery3 Current		EIB1电池3电流		
161		ID_HEAD127				32			EIB1Battery3 Voltage		EIB1电池3电压		
162		ID_HEAD128				32			EIB1Battery3 Capacity		EIB1电池3容量		
163		ID_HEAD129				32			EIB2Battery3 Current		EIB2电池3电流		
164		ID_HEAD130				32			EIB2Battery3 Voltage		EIB2电池3电压		
165		ID_HEAD131				32			EIB2Battery3 Capacity		EIB2电池3容量		
166		ID_HEAD132				32			EIB3Battery3 Current		EIB3电池3电流		
167		ID_HEAD133				32			EIB3Battery3 Voltage		EIB3电池3电压		
168		ID_HEAD134				32			EIB3Battery3 Capacity		EIB3电池3容量		
169		ID_HEAD135				32			EIB4Battery3 Current		EIB4电池3电流		
170		ID_HEAD136				32			EIB4Battery3 Voltage		EIB4电池3电压		
171		ID_HEAD137				32			EIB4Battery3 Capacity		EIB4电池3容量		
172		ID_HEAD138				32			EIB1Block1Voltage			EIB1电池块电压1		
173		ID_HEAD139				32			EIB1Block2Voltage			EIB1电池块电压2		
174		ID_HEAD140				32			EIB1Block3Voltage			EIB1电池块电压3		
175		ID_HEAD141				32			EIB1Block4Voltage			EIB1电池块电压4		
176		ID_HEAD142				32			EIB1Block5Voltage			EIB1电池块电压5		
177		ID_HEAD143				32			EIB1Block6Voltage			EIB1电池块电压6		
178		ID_HEAD144				32			EIB1Block7Voltage			EIB1电池块电压7		
179		ID_HEAD145				32			EIB1Block8Voltage			EIB1电池块电压8		
180		ID_HEAD146				32			EIB2Block1Voltage			EIB2电池块电压1		
181		ID_HEAD147				32			EIB2Block2Voltage			EIB2电池块电压2		
182		ID_HEAD148				32			EIB2Block3Voltage			EIB2电池块电压3		
183		ID_HEAD149				32			EIB2Block4Voltage			EIB2电池块电压4		
184		ID_HEAD150				32			EIB2Block5Voltage			EIB2电池块电压5		
185		ID_HEAD151				32			EIB2Block6Voltage			EIB2电池块电压6		
186		ID_HEAD152				32			EIB2Block7Voltage			EIB2电池块电压7		
187		ID_HEAD153				32			EIB2Block8Voltage			EIB2电池块电压8		
188		ID_HEAD154				32			EIB3Block1Voltage			EIB3电池块电压1		
189		ID_HEAD155				32			EIB3Block2Voltage			EIB3电池块电压2		
190		ID_HEAD156				32			EIB3Block3Voltage			EIB3电池块电压3		
191		ID_HEAD157				32			EIB3Block4Voltage			EIB3电池块电压4		
192		ID_HEAD158				32			EIB3Block5Voltage			EIB3电池块电压5		
193		ID_HEAD159				32			EIB3Block6Voltage			EIB3电池块电压6		
194		ID_HEAD160				32			EIB3Block7Voltage			EIB3电池块电压7		
195		ID_HEAD161				32			EIB3Block8Voltage			EIB3电池块电压8		
196		ID_HEAD162				32			EIB4Block1Voltage			EIB4电池块电压1		
197		ID_HEAD163				32			EIB4Block2Voltage			EIB4电池块电压2		
198		ID_HEAD164				32			EIB4Block3Voltage			EIB4电池块电压3		
199		ID_HEAD165				32			EIB4Block4Voltage			EIB4电池块电压4		
200		ID_HEAD166				32			EIB4Block5Voltage			EIB4电池块电压5		
201		ID_HEAD167				32			EIB4Block6Voltage			EIB4电池块电压6		
202		ID_HEAD168				32			EIB4Block7Voltage			EIB4电池块电压7		
203		ID_HEAD169				32			EIB4Block8Voltage			EIB4电池块电压8		
204		ID_HEAD170				32			Temperature1			温度1			
205		ID_HEAD171				32			Temperature2			温度2			
206		ID_HEAD172				32			Temperature3			温度3			
207		ID_HEAD173				32			Temperature4			温度4			
208		ID_HEAD174				32			Temperature5			温度5			
209		ID_HEAD175				32			Temperature6			温度6			
210		ID_HEAD176				32			Temperature7			温度7			
211		ID_HEAD177				32			Battery1 Current			电池1电流		
212		ID_HEAD178				32			Battery1 Voltage			电池1电压		
213		ID_HEAD179				32			Battery1 Capacity			电池1容量		
214		ID_HEAD180				32			LargeDUBattery1 Current		LargeDU电池1电流	
215		ID_HEAD181				32			LargeDUBattery1 Voltage		LargeDU电池1电压	
216		ID_HEAD182				32			LargeDUBattery1 Capacity		LargeDU电池1容量	
217		ID_HEAD183				32			LargeDUBattery2 Current		LargeDU电池2电流	
218		ID_HEAD184				32			LargeDUBattery2 Voltage		LargeDU电池2电压	
219		ID_HEAD185				32			LargeDUBattery2 Capacity		LargeDU电池2容量	
220		ID_HEAD186				32			LargeDUBattery3 Current		LargeDU电池3电流	
221		ID_HEAD187				32			LargeDUBattery3 Voltage		LargeDU电池3电压	
222		ID_HEAD188				32			LargeDUBattery3 Capacity		LargeDU电池3容量	
223		ID_HEAD189				32			LargeDUBattery4 Current		LargeDU电池4电流	
224		ID_HEAD190				32			LargeDUBattery4 Voltage		LargeDU电池4电压	
225		ID_HEAD191				32			LargeDUBattery4 Capacity		LargeDU电池4容量	
226		ID_HEAD192				32			LargeDUBattery5 Current		LargeDU电池5电流	
227		ID_HEAD193				32			LargeDUBattery5 Voltage		LargeDU电池5电压	
228		ID_HEAD194				32			LargeDUBattery5 Capacity		LargeDU电池5容量	
229		ID_HEAD195				32			LargeDUBattery6 Current		LargeDU电池6电流	
230		ID_HEAD196				32			LargeDUBattery6 Voltage		LargeDU电池6电压	
231		ID_HEAD197				32			LargeDUBattery6 Capacity		LargeDU电池6容量	
232		ID_HEAD198				32			LargeDUBattery7 Current		LargeDU电池7电流	
233		ID_HEAD199				32			LargeDUBattery7 Voltage		LargeDU电池7电压	
234		ID_HEAD200				32			LargeDUBattery7 Capacity		LargeDU电池7容量	
235		ID_HEAD201				32			LargeDUBattery8 Current		LargeDU电池8电流	
236		ID_HEAD202				32			LargeDUBattery8 Voltage		LargeDU电池8电压	
237		ID_HEAD203				32			LargeDUBattery8 Capacity		LargeDU电池8容量	
238		ID_HEAD204				32			LargeDUBattery9 Current		LargeDU电池9电流	
239		ID_HEAD205				32			LargeDUBattery9 Voltage		LargeDU电池9电压	
240		ID_HEAD206				32			LargeDUBattery9 Capacity		LargeDU电池9容量	
241		ID_HEAD207				32			LargeDUBattery10 Current		LargeDU电池10电流	
242		ID_HEAD208				32			LargeDUBattery10 Voltage		LargeDU电池10电压	
243		ID_HEAD209				32			LargeDUBattery10 Capacity		LargeDU电池10容量	
244		ID_HEAD210				32			LargeDUBattery11 Current		LargeDU电池11电流	
245		ID_HEAD211				32			LargeDUBattery11 Voltage		LargeDU电池11电压	
246		ID_HEAD212				32			LargeDUBattery11 Capacity		LargeDU电池11容量	
247		ID_HEAD213				32			LargeDUBattery12 Current		LargeDU电池12电流	
248		ID_HEAD214				32			LargeDUBattery12 Voltage		LargeDU电池12电压	
249		ID_HEAD215				32			LargeDUBattery12 Capacity		LargeDU电池12容量	
250		ID_HEAD216				32			LargeDUBattery13 Current		LargeDU电池13电流	
251		ID_HEAD217				32			LargeDUBattery13 Voltage		LargeDU电池13电压	
252		ID_HEAD218				32			LargeDUBattery13 Capacity		LargeDU电池13容量	
253		ID_HEAD219				32			LargeDUBattery14 Current		LargeDU电池14电流	
254		ID_HEAD220				32			LargeDUBattery14 Voltage		LargeDU电池14电压	
255		ID_HEAD221				32			LargeDUBattery14 Capacity		LargeDU电池14容量	
256		ID_HEAD222				32			LargeDUBattery15 Current		LargeDU电池15电流	
257		ID_HEAD223				32			LargeDUBattery15 Voltage		LargeDU电池15电压	
258		ID_HEAD224				32			LargeDUBattery15 Capacity		LargeDU电池15容量	
259		ID_HEAD225				32			LargeDUBattery16 Current		LargeDU电池16电流	
260		ID_HEAD226				32			LargeDUBattery16 Voltage		LargeDU电池16电压	
261		ID_HEAD227				32			LargeDUBattery16 Capacity		LargeDU电池16容量	
262		ID_HEAD228				32			LargeDUBattery17 Current		LargeDU电池17电流	
263		ID_HEAD229				32			LargeDUBattery17 Voltage		LargeDU电池17电压	
264		ID_HEAD230				32			LargeDUBattery17 Capacity		LargeDU电池17容量	
265		ID_HEAD231				32			LargeDUBattery18 Current		LargeDU电池18电流	
266		ID_HEAD232				32			LargeDUBattery18 Voltage		LargeDU电池18电压	
267		ID_HEAD233				32			LargeDUBattery18 Capacity		LargeDU电池18容量	
268		ID_HEAD234				32			LargeDUBattery19 Current		LargeDU电池19电流	
269		ID_HEAD235				32			LargeDUBattery19 Voltage		LargeDU电池19电压	
270		ID_HEAD236				32			LargeDUBattery19 Capacity		LargeDU电池19容量	
271		ID_HEAD237				32			LargeDUBattery20 Current		LargeDU电池20电流	
272		ID_HEAD238				32			LargeDUBattery20 Voltage		LargeDU电池20电压	
273		ID_HEAD239				32			LargeDUBattery20 Capacity		LargeDU电池20容量	
274		ID_HEAD240				32			Temperature8			温度8			
275		ID_HEAD241				32			Temperature9			温度9			
276		ID_HEAD242				32			Temperature10			温度10			
277		ID_HEAD243				32			SMBattery1 Current			SMBattery1 电流		
278		ID_HEAD244				32			SMBattery1 Voltage			SMBattery1 电压		
279		ID_HEAD245				32			SMBattery1 Capacity			SMBattery1 容量		
280		ID_HEAD246				32			SMBattery2 Current			SMBattery2 电流		
281		ID_HEAD247				32			SMBattery2 Voltage			SMBattery2 电压		
282		ID_HEAD248				32			SMBattery2 Capacity			SMBattery2 容量		
283		ID_HEAD249				32			SMBattery3 Current			SMBattery3 电流		
284		ID_HEAD250				32			SMBattery3 Voltage			SMBattery3 电压		
285		ID_HEAD251				32			SMBattery3 Capacity			SMBattery3 容量		
286		ID_HEAD252				32			SMBattery4 Current			SMBattery4 电流		
287		ID_HEAD253				32			SMBattery4 Voltage			SMBattery4 电压		
288		ID_HEAD254				32			SMBattery4 Capacity			SMBattery4 容量		
289		ID_HEAD255				32			SMBattery5 Current			SMBattery5 电流		
290		ID_HEAD256				32			SMBattery5 Voltage			SMBattery5 电压		
291		ID_HEAD257				32			SMBattery5 Capacity			SMBattery5 容量		
292		ID_HEAD258				32			SMBattery6 Current			SMBattery6 电流		
293		ID_HEAD259				32			SMBattery6 Voltage			SMBattery6 电压		
294		ID_HEAD260				32			SMBattery6 Capacity			SMBattery6 容量		
295		ID_HEAD261				32			SMBattery7 Current			SMBattery7 电流		
296		ID_HEAD262				32			SMBattery7 Voltage			SMBattery7 电压		
297		ID_HEAD263				32			SMBattery7 Capacity			SMBattery7 容量		
298		ID_HEAD264				32			SMBattery8 Current			SMBattery8 电流		
299		ID_HEAD265				32			SMBattery8 Voltage			SMBattery8 电压		
300		ID_HEAD266				32			SMBattery8 Capacity			SMBattery8 容量		
301		ID_HEAD267				32			SMBattery9 Current			SMBattery9 电流		
302		ID_HEAD268				32			SMBattery9 Voltage			SMBattery9 电压		
303		ID_HEAD269				32			SMBattery9 Capacity			SMBattery9 容量		
304		ID_HEAD270				32			SMBattery10 Current			SMBattery10 电流	
305		ID_HEAD271				32			SMBattery10 Voltage			SMBattery10 电压	
306		ID_HEAD272				32			SMBattery10 Capacity		SMBattery10 容量	
307		ID_HEAD273				32			SMBattery11 Current			SMBattery11 电流	
308		ID_HEAD274				32			SMBattery11 Voltage			SMBattery11 电压	
309		ID_HEAD275				32			SMBattery11 Capacity		SMBattery11 容量	
310		ID_HEAD276				32			SMBattery12 Current			SMBattery12 电流	
311		ID_HEAD277				32			SMBattery12 Voltage			SMBattery12 电压	
312		ID_HEAD278				32			SMBattery12 Capacity		SMBattery12 容量	
313		ID_HEAD279				32			SMBattery13 Current			SMBattery13 电流	
314		ID_HEAD280				32			SMBattery13 Voltage			SMBattery13 电压	
315		ID_HEAD281				32			SMBattery13 Capacity		SMBattery13 容量	
316		ID_HEAD282				32			SMBattery14 Current			SMBattery14 电流	
317		ID_HEAD283				32			SMBattery14 Voltage			SMBattery14 电压	
318		ID_HEAD284				32			SMBattery14 Capacity		SMBattery14 容量	
319		ID_HEAD285				32			SMBattery15 Current			SMBattery15 电流	
320		ID_HEAD286				32			SMBattery15 Voltage			SMBattery15 电压	
321		ID_HEAD287				32			SMBattery15 Capacity		SMBattery15 容量	
322		ID_HEAD288				32			SMBattery16 Current			SMBattery16 电流	
323		ID_HEAD289				32			SMBattery16 Voltage			SMBattery16 电压	
324		ID_HEAD290				32			SMBattery16 Capacity		SMBattery16 容量	
325		ID_HEAD291				32			SMBattery17 Current			SMBattery17 电流	
326		ID_HEAD292				32			SMBattery17 Voltage			SMBattery17 电压	
327		ID_HEAD293				32			SMBattery17 Capacity		SMBattery17 容量	
328		ID_HEAD294				32			SMBattery18 Current			SMBattery18 电流	
329		ID_HEAD295				32			SMBattery18 Voltage			SMBattery18 电压	
330		ID_HEAD296				32			SMBattery18 Capacity		SMBattery18 容量	
331		ID_HEAD297				32			SMBattery19 Current			SMBattery19 电流	
332		ID_HEAD298				32			SMBattery19 Voltage			SMBattery19 电压	
333		ID_HEAD299				32			SMBattery19 Capacity		SMBattery19 容量	
334		ID_HEAD300				32			SMBattery20 Current			SMBattery20 电流	
335		ID_HEAD301				32			SMBattery20 Voltage			SMBattery20 电压	
336		ID_HEAD302				32			SMBattery20 Capacity		SMBattery20 容量	
337		ID_HEAD303				32			SMDU1Battery5 Current		SMDU1Battery5 电流	
338		ID_HEAD304				32			SMDU1Battery5 Voltage		SMDU1Battery5 电压	
339		ID_HEAD305				32			SMDU1Battery5 Capacity		SMDU1Battery5 容量	
340		ID_HEAD306				32			SMDU2Battery5 Current		SMDU2Battery5 电流	
341		ID_HEAD307				32			SMDU2Battery5 Voltage		SMDU2Battery5 电压	
342		ID_HEAD308				32			SMDU2Battery5 Capacity		SMDU2Battery5 容量	
343		ID_HEAD309				32			SMDU3Battery5 Current		SMDU3Battery5 电流	
344		ID_HEAD310				32			SMDU3Battery5 Voltage		SMDU3Battery5 电压	
345		ID_HEAD311				32			SMDU3Battery5 Capacity		SMDU3Battery5 容量	
346		ID_HEAD312				32			SMDU4Battery5 Current		SMDU4Battery5 电流	
347		ID_HEAD313				32			SMDU4Battery5 Voltage		SMDU4Battery5 电压	
348		ID_HEAD314				32			SMDU4Battery5 Capacity		SMDU4Battery5 容量	
349		ID_HEAD315				32			SMDU5Battery5 Current		SMDU5Battery5 电流	
350		ID_HEAD316				32			SMDU5Battery5 Voltage		SMDU5Battery5 电压	
351		ID_HEAD317				32			SMDU5Battery5 Capacity		SMDU5Battery5 容量	
352		ID_HEAD318				32			SMDU6Battery5 Current		SMDU6Battery5 电流	
353		ID_HEAD319				32			SMDU6Battery5 Voltage		SMDU6Battery5 电压	
354		ID_HEAD320				32			SMDU6Battery5 Capacity		SMDU6Battery5 容量	
355		ID_HEAD321				32			SMDU7Battery5 Current		SMDU7Battery5 电流	
356		ID_HEAD322				32			SMDU7Battery5 Voltage		SMDU7Battery5 电压	
357		ID_HEAD323				32			SMDU7Battery5 Capacity		SMDU7Battery5 容量	
358		ID_HEAD324				32			SMDU8Battery5 Current		SMDU8Battery5 电流	
359		ID_HEAD325				32			SMDU8Battery5 Voltage		SMDU8Battery5 电压	
360		ID_HEAD326				32			SMDU8Battery5 Capacity		SMDU8Battery5 容量	
361		ID_HEAD327				32			SMBRCBattery1 Current		SMBRCBattery1 电流	
362		ID_HEAD328				32			SMBRCBattery1 Voltage		SMBRCBattery1 电压	
363		ID_HEAD329				32			SMBRCBattery1 Capacity		SMBRCBattery1 容量	
364		ID_HEAD330				32			SMBRCBattery2 Current		SMBRCBattery2 电流	
365		ID_HEAD331				32			SMBRCBattery2 Voltage		SMBRCBattery2 电压	
366		ID_HEAD332				32			SMBRCBattery2 Capacity		SMBRCBattery2 容量	
367		ID_HEAD333				32			SMBRCBattery3 Current		SMBRCBattery3 电流	
368		ID_HEAD334				32			SMBRCBattery3 Voltage		SMBRCBattery3 电压	
369		ID_HEAD335				32			SMBRCBattery3 Capacity		SMBRCBattery3 容量	
370		ID_HEAD336				32			SMBRCBattery4 Current		SMBRCBattery4 电流	
371		ID_HEAD337				32			SMBRCBattery4 Voltage		SMBRCBattery4 电压	
372		ID_HEAD338				32			SMBRCBattery4 Capacity		SMBRCBattery4 容量	
373		ID_HEAD339				32			SMBRCBattery5 Current		SMBRCBattery5 电流	
374		ID_HEAD340				32			SMBRCBattery5 Voltage		SMBRCBattery5 电压	
375		ID_HEAD341				32			SMBRCBattery5 Capacity		SMBRCBattery5 容量	
376		ID_HEAD342				32			SMBRCBattery6 Current		SMBRCBattery6 电流	
377		ID_HEAD343				32			SMBRCBattery6 Voltage		SMBRCBattery6 电压	
378		ID_HEAD344				32			SMBRCBattery6 Capacity		SMBRCBattery6 容量	
379		ID_HEAD345				32			SMBRCBattery7 Current		SMBRCBattery7 电流	
380		ID_HEAD346				32			SMBRCBattery7 Voltage		SMBRCBattery7 电压	
381		ID_HEAD347				32			SMBRCBattery7 Capacity		SMBRCBattery7 容量	
382		ID_HEAD348				32			SMBRCBattery8 Current		SMBRCBattery8 电流	
383		ID_HEAD349				32			SMBRCBattery8 Voltage		SMBRCBattery8 电压	
384		ID_HEAD350				32			SMBRCBattery8 Capacity		SMBRCBattery8 容量	
385		ID_HEAD351				32			SMBRCBattery9 Current		SMBRCBattery9 电流	
386		ID_HEAD352				32			SMBRCBattery9 Voltage		SMBRCBattery9 电压	
387		ID_HEAD353				32			SMBRCBattery9 Capacity		SMBRCBattery9 容量	
388		ID_HEAD354				32			SMBRCBattery10 Current		SMBRCBattery10 电流	
389		ID_HEAD355				32			SMBRCBattery10 Voltage		SMBRCBattery10 电压	
390		ID_HEAD356				32			SMBRCBattery10 Capacity		SMBRCBattery10 容量	
391		ID_HEAD357				32			SMBRCBattery11 Current		SMBRCBattery11 电流	
392		ID_HEAD358				32			SMBRCBattery11 Voltage		SMBRCBattery11 电压	
393		ID_HEAD359				32			SMBRCBattery11 Capacity		SMBRCBattery11 容量	
394		ID_HEAD360				32			SMBRCBattery12 Current		SMBRCBattery12 电流	
395		ID_HEAD361				32			SMBRCBattery12 Voltage		SMBRCBattery12 电压	
396		ID_HEAD362				32			SMBRCBattery12 Capacity		SMBRCBattery12 容量	
397		ID_HEAD363				32			SMBRCBattery13 Current		SMBRCBattery13 电流	
398		ID_HEAD364				32			SMBRCBattery13 Voltage		SMBRCBattery13 电压	
399		ID_HEAD365				32			SMBRCBattery13 Capacity		SMBRCBattery13 容量	
400		ID_HEAD366				32			SMBRCBattery14 Current		SMBRCBattery14 电流	
401		ID_HEAD367				32			SMBRCBattery14 Voltage		SMBRCBattery14 电压	
402		ID_HEAD368				32			SMBRCBattery14 Capacity		SMBRCBattery14 容量	
403		ID_HEAD369				32			SMBRCBattery15 Current		SMBRCBattery15 电流	
404		ID_HEAD370				32			SMBRCBattery15 Voltage		SMBRCBattery15 电压	
405		ID_HEAD371				32			SMBRCBattery15 Capacity		SMBRCBattery15 容量	
406		ID_HEAD372				32			SMBRCBattery16 Current		SMBRCBattery16 电流	
407		ID_HEAD373				32			SMBRCBattery16 Voltage		SMBRCBattery16 电压	
408		ID_HEAD374				32			SMBRCBattery16 Capacity		SMBRCBattery16 容量	
409		ID_HEAD375				32			SMBRCBattery17 Current		SMBRCBattery17 电流	
410		ID_HEAD376				32			SMBRCBattery17 Voltage		SMBRCBattery17 电压	
411		ID_HEAD377				32			SMBRCBattery17 Capacity		SMBRCBattery17 容量	
412		ID_HEAD378				32			SMBRCBattery18 Current		SMBRCBattery18 电流	
413		ID_HEAD379				32			SMBRCBattery18 Voltage		SMBRCBattery18 电压	
414		ID_HEAD380				32			SMBRCBattery18 Capacity		SMBRCBattery18 容量	
415		ID_HEAD381				32			SMBRCBattery19 Current		SMBRCBattery19 电流	
416		ID_HEAD382				32			SMBRCBattery19 Voltage		SMBRCBattery19 电压	
417		ID_HEAD383				32			SMBRCBattery19 Capacity		SMBRCBattery19 容量	
418		ID_HEAD384				32			SMBRCBattery20 Current		SMBRCBattery20 电流	
419		ID_HEAD385				32			SMBRCBattery20 Voltage		SMBRCBattery20 电压	
420		ID_HEAD386				32			SMBRCBattery20 Capacity		SMBRCBattery20 容量	
421		ID_HEAD387				32			SMBAT/BRC1 BLOCK1  Voltage		SMBAT/BRC1块电压1	
422		ID_HEAD388				32			SMBAT/BRC1 BLOCK2  Voltage		SMBAT/BRC1块电压2	
423		ID_HEAD389				32			SMBAT/BRC1 BLOCK3  Voltage		SMBAT/BRC1块电压3	
424		ID_HEAD390				32			SMBAT/BRC1 BLOCK4  Voltage		SMBAT/BRC1块电压4	
425		ID_HEAD391				32			SMBAT/BRC1 BLOCK5  Voltage		SMBAT/BRC1块电压5	
426		ID_HEAD392				32			SMBAT/BRC1 BLOCK6  Voltage		SMBAT/BRC1块电压6	
427		ID_HEAD393				32			SMBAT/BRC1 BLOCK7  Voltage		SMBAT/BRC1块电压7	
428		ID_HEAD394				32			SMBAT/BRC1 BLOCK8  Voltage		SMBAT/BRC1块电压8	
429		ID_HEAD395				32			SMBAT/BRC1 BLOCK9  Voltage		SMBAT/BRC1块电压9	
430		ID_HEAD396				32			SMBAT/BRC1 BLOCK10 Voltage		SMBAT/BRC1块电压10	
431		ID_HEAD397				32			SMBAT/BRC1 BLOCK11 Voltage		SMBAT/BRC1块电压11	
432		ID_HEAD398				32			SMBAT/BRC1 BLOCK12 Voltage		SMBAT/BRC1块电压12	
433		ID_HEAD399				32			SMBAT/BRC1 BLOCK13 Voltage		SMBAT/BRC1块电压13	
434		ID_HEAD400				32			SMBAT/BRC1 BLOCK14 Voltage		SMBAT/BRC1块电压14	
435		ID_HEAD401				32			SMBAT/BRC1 BLOCK15 Voltage		SMBAT/BRC1块电压15	
436		ID_HEAD402				32			SMBAT/BRC1 BLOCK16 Voltage		SMBAT/BRC1块电压16	
437		ID_HEAD403				32			SMBAT/BRC1 BLOCK17 Voltage		SMBAT/BRC1块电压17	
438		ID_HEAD404				32			SMBAT/BRC1 BLOCK18 Voltage		SMBAT/BRC1块电压18	
439		ID_HEAD405				32			SMBAT/BRC1 BLOCK19 Voltage		SMBAT/BRC1块电压19	
440		ID_HEAD406				32			SMBAT/BRC1 BLOCK20 Voltage		SMBAT/BRC1块电压20	
441		ID_HEAD407				32			SMBAT/BRC1 BLOCK21 Voltage		SMBAT/BRC1块电压21	
442		ID_HEAD408				32			SMBAT/BRC1 BLOCK22 Voltage		SMBAT/BRC1块电压22	
443		ID_HEAD409				32			SMBAT/BRC1 BLOCK23 Voltage		SMBAT/BRC1块电压23	
444		ID_HEAD410				32			SMBAT/BRC1 BLOCK24 Voltage		SMBAT/BRC1块电压24	
445		ID_HEAD411				32			SMBAT/BRC2 BLOCK1  Voltage		SMBAT/BRC2块电压1	
446		ID_HEAD412				32			SMBAT/BRC2 BLOCK2  Voltage		SMBAT/BRC2块电压2	
447		ID_HEAD413				32			SMBAT/BRC2 BLOCK3  Voltage		SMBAT/BRC2块电压3	
448		ID_HEAD414				32			SMBAT/BRC2 BLOCK4  Voltage		SMBAT/BRC2块电压4	
449		ID_HEAD415				32			SMBAT/BRC2 BLOCK5  Voltage		SMBAT/BRC2块电压5	
450		ID_HEAD416				32			SMBAT/BRC2 BLOCK6  Voltage		SMBAT/BRC2块电压6	
451		ID_HEAD417				32			SMBAT/BRC2 BLOCK7  Voltage		SMBAT/BRC2块电压7	
452		ID_HEAD418				32			SMBAT/BRC2 BLOCK8  Voltage		SMBAT/BRC2块电压8	
453		ID_HEAD419				32			SMBAT/BRC2 BLOCK9  Voltage		SMBAT/BRC2块电压9	
454		ID_HEAD420				32			SMBAT/BRC2 BLOCK10 Voltage		SMBAT/BRC2块电压10	
455		ID_HEAD421				32			SMBAT/BRC2 BLOCK11 Voltage		SMBAT/BRC2块电压11	
456		ID_HEAD422				32			SMBAT/BRC2 BLOCK12 Voltage		SMBAT/BRC2块电压12	
457		ID_HEAD423				32			SMBAT/BRC2 BLOCK13 Voltage		SMBAT/BRC2块电压13	
458		ID_HEAD424				32			SMBAT/BRC2 BLOCK14 Voltage		SMBAT/BRC2块电压14	
459		ID_HEAD425				32			SMBAT/BRC2 BLOCK15 Voltage		SMBAT/BRC2块电压15	
460		ID_HEAD426				32			SMBAT/BRC2 BLOCK16 Voltage		SMBAT/BRC2块电压16	
461		ID_HEAD427				32			SMBAT/BRC2 BLOCK17 Voltage		SMBAT/BRC2块电压17	
462		ID_HEAD428				32			SMBAT/BRC2 BLOCK18 Voltage		SMBAT/BRC2块电压18	
463		ID_HEAD429				32			SMBAT/BRC2 BLOCK19 Voltage		SMBAT/BRC2块电压19	
464		ID_HEAD430				32			SMBAT/BRC2 BLOCK20 Voltage		SMBAT/BRC2块电压20	
465		ID_HEAD431				32			SMBAT/BRC2 BLOCK21 Voltage		SMBAT/BRC2块电压21	
466		ID_HEAD432				32			SMBAT/BRC2 BLOCK22 Voltage		SMBAT/BRC2块电压22	
467		ID_HEAD433				32			SMBAT/BRC2 BLOCK23 Voltage		SMBAT/BRC2块电压23	
468		ID_HEAD434				32			SMBAT/BRC2 BLOCK24 Voltage		SMBAT/BRC2块电压24	
469		ID_HEAD435				32			SMBAT/BRC3 BLOCK1  Voltage		SMBAT/BRC3块电压1	
470		ID_HEAD436				32			SMBAT/BRC3 BLOCK2  Voltage		SMBAT/BRC3块电压2	
471		ID_HEAD437				32			SMBAT/BRC3 BLOCK3  Voltage		SMBAT/BRC3块电压3	
472		ID_HEAD438				32			SMBAT/BRC3 BLOCK4  Voltage		SMBAT/BRC3块电压4	
473		ID_HEAD439				32			SMBAT/BRC3 BLOCK5  Voltage		SMBAT/BRC3块电压5	
474		ID_HEAD440				32			SMBAT/BRC3 BLOCK6  Voltage		SMBAT/BRC3块电压6	
475		ID_HEAD441				32			SMBAT/BRC3 BLOCK7  Voltage		SMBAT/BRC3块电压7	
476		ID_HEAD442				32			SMBAT/BRC3 BLOCK8  Voltage		SMBAT/BRC3块电压8	
477		ID_HEAD443				32			SMBAT/BRC3 BLOCK9  Voltage		SMBAT/BRC3块电压9	
478		ID_HEAD444				32			SMBAT/BRC3 BLOCK10 Voltage		SMBAT/BRC3块电压10	
479		ID_HEAD445				32			SMBAT/BRC3 BLOCK11 Voltage		SMBAT/BRC3块电压11	
480		ID_HEAD446				32			SMBAT/BRC3 BLOCK12 Voltage		SMBAT/BRC3块电压12	
481		ID_HEAD447				32			SMBAT/BRC3 BLOCK13 Voltage		SMBAT/BRC3块电压13	
482		ID_HEAD448				32			SMBAT/BRC3 BLOCK14 Voltage		SMBAT/BRC3块电压14	
483		ID_HEAD449				32			SMBAT/BRC3 BLOCK15 Voltage		SMBAT/BRC3块电压15	
484		ID_HEAD450				32			SMBAT/BRC3 BLOCK16 Voltage		SMBAT/BRC3块电压16	
485		ID_HEAD451				32			SMBAT/BRC3 BLOCK17 Voltage		SMBAT/BRC3块电压17	
486		ID_HEAD452				32			SMBAT/BRC3 BLOCK18 Voltage		SMBAT/BRC3块电压18	
487		ID_HEAD453				32			SMBAT/BRC3 BLOCK19 Voltage		SMBAT/BRC3块电压19	
488		ID_HEAD454				32			SMBAT/BRC3 BLOCK20 Voltage		SMBAT/BRC3块电压20	
489		ID_HEAD455				32			SMBAT/BRC3 BLOCK21 Voltage		SMBAT/BRC3块电压21	
490		ID_HEAD456				32			SMBAT/BRC3 BLOCK22 Voltage		SMBAT/BRC3块电压22	
491		ID_HEAD457				32			SMBAT/BRC3 BLOCK23 Voltage		SMBAT/BRC3块电压23	
492		ID_HEAD458				32			SMBAT/BRC3 BLOCK24 Voltage		SMBAT/BRC3块电压24	
493		ID_HEAD459				32			SMBAT/BRC4 BLOCK1  Voltage		SMBAT/BRC4块电压1	
494		ID_HEAD460				32			SMBAT/BRC4 BLOCK2  Voltage		SMBAT/BRC4块电压2	
495		ID_HEAD461				32			SMBAT/BRC4 BLOCK3  Voltage		SMBAT/BRC4块电压3	
496		ID_HEAD462				32			SMBAT/BRC4 BLOCK4  Voltage		SMBAT/BRC4块电压4	
497		ID_HEAD463				32			SMBAT/BRC4 BLOCK5  Voltage		SMBAT/BRC4块电压5	
498		ID_HEAD464				32			SMBAT/BRC4 BLOCK6  Voltage		SMBAT/BRC4块电压6	
499		ID_HEAD465				32			SMBAT/BRC4 BLOCK7  Voltage		SMBAT/BRC4块电压7	
500		ID_HEAD466				32			SMBAT/BRC4 BLOCK8  Voltage		SMBAT/BRC4块电压8	
501		ID_HEAD467				32			SMBAT/BRC4 BLOCK9  Voltage		SMBAT/BRC4块电压9	
502		ID_HEAD468				32			SMBAT/BRC4 BLOCK10 Voltage		SMBAT/BRC4块电压10	
503		ID_HEAD469				32			SMBAT/BRC4 BLOCK11 Voltage		SMBAT/BRC4块电压11	
504		ID_HEAD470				32			SMBAT/BRC4 BLOCK12 Voltage		SMBAT/BRC4块电压12	
505		ID_HEAD471				32			SMBAT/BRC4 BLOCK13 Voltage		SMBAT/BRC4块电压13	
506		ID_HEAD472				32			SMBAT/BRC4 BLOCK14 Voltage		SMBAT/BRC4块电压14	
507		ID_HEAD473				32			SMBAT/BRC4 BLOCK15 Voltage		SMBAT/BRC4块电压15	
508		ID_HEAD474				32			SMBAT/BRC4 BLOCK16 Voltage		SMBAT/BRC4块电压16	
509		ID_HEAD475				32			SMBAT/BRC4 BLOCK17 Voltage		SMBAT/BRC4块电压17	
510		ID_HEAD476				32			SMBAT/BRC4 BLOCK18 Voltage		SMBAT/BRC4块电压18	
511		ID_HEAD477				32			SMBAT/BRC4 BLOCK19 Voltage		SMBAT/BRC4块电压19	
512		ID_HEAD478				32			SMBAT/BRC4 BLOCK20 Voltage		SMBAT/BRC4块电压20	
513		ID_HEAD479				32			SMBAT/BRC4 BLOCK21 Voltage		SMBAT/BRC4块电压21	
514		ID_HEAD480				32			SMBAT/BRC4 BLOCK22 Voltage		SMBAT/BRC4块电压22	
515		ID_HEAD481				32			SMBAT/BRC4 BLOCK23 Voltage		SMBAT/BRC4块电压23	
516		ID_HEAD482				32			SMBAT/BRC4 BLOCK24 Voltage		SMBAT/BRC4块电压24	
517		ID_HEAD483				32			SMBAT/BRC5 BLOCK1  Voltage		SMBAT/BRC5块电压1	
518		ID_HEAD484				32			SMBAT/BRC5 BLOCK2  Voltage		SMBAT/BRC5块电压2	
519		ID_HEAD485				32			SMBAT/BRC5 BLOCK3  Voltage		SMBAT/BRC5块电压3	
520		ID_HEAD486				32			SMBAT/BRC5 BLOCK4  Voltage		SMBAT/BRC5块电压4	
521		ID_HEAD487				32			SMBAT/BRC5 BLOCK5  Voltage		SMBAT/BRC5块电压5	
522		ID_HEAD488				32			SMBAT/BRC5 BLOCK6  Voltage		SMBAT/BRC5块电压6	
523		ID_HEAD489				32			SMBAT/BRC5 BLOCK7  Voltage		SMBAT/BRC5块电压7	
524		ID_HEAD490				32			SMBAT/BRC5 BLOCK8  Voltage		SMBAT/BRC5块电压8	
525		ID_HEAD491				32			SMBAT/BRC5 BLOCK9  Voltage		SMBAT/BRC5块电压9	
526		ID_HEAD492				32			SMBAT/BRC5 BLOCK10 Voltage		SMBAT/BRC5块电压10	
527		ID_HEAD493				32			SMBAT/BRC5 BLOCK11 Voltage		SMBAT/BRC5块电压11	
528		ID_HEAD494				32			SMBAT/BRC5 BLOCK12 Voltage		SMBAT/BRC5块电压12	
529		ID_HEAD495				32			SMBAT/BRC5 BLOCK13 Voltage		SMBAT/BRC5块电压13	
530		ID_HEAD496				32			SMBAT/BRC5 BLOCK14 Voltage		SMBAT/BRC5块电压14	
531		ID_HEAD497				32			SMBAT/BRC5 BLOCK15 Voltage		SMBAT/BRC5块电压15	
532		ID_HEAD498				32			SMBAT/BRC5 BLOCK16 Voltage		SMBAT/BRC5块电压16	
533		ID_HEAD499				32			SMBAT/BRC5 BLOCK17 Voltage		SMBAT/BRC5块电压17	
534		ID_HEAD500				32			SMBAT/BRC5 BLOCK18 Voltage		SMBAT/BRC5块电压18	
535		ID_HEAD501				32			SMBAT/BRC5 BLOCK19 Voltage		SMBAT/BRC5块电压19	
536		ID_HEAD502				32			SMBAT/BRC5 BLOCK20 Voltage		SMBAT/BRC5块电压20	
537		ID_HEAD503				32			SMBAT/BRC5 BLOCK21 Voltage		SMBAT/BRC5块电压21	
538		ID_HEAD504				32			SMBAT/BRC5 BLOCK22 Voltage		SMBAT/BRC5块电压22	
539		ID_HEAD505				32			SMBAT/BRC5 BLOCK23 Voltage		SMBAT/BRC5块电压23	
540		ID_HEAD506				32			SMBAT/BRC5 BLOCK24 Voltage		SMBAT/BRC5块电压24	
541		ID_HEAD507				32			SMBAT/BRC6 BLOCK1  Voltage		SMBAT/BRC6块电压1	
542		ID_HEAD508				32			SMBAT/BRC6 BLOCK2  Voltage		SMBAT/BRC6块电压2	
543		ID_HEAD509				32			SMBAT/BRC6 BLOCK3  Voltage		SMBAT/BRC6块电压3	
544		ID_HEAD510				32			SMBAT/BRC6 BLOCK4  Voltage		SMBAT/BRC6块电压4	
545		ID_HEAD511				32			SMBAT/BRC6 BLOCK5  Voltage		SMBAT/BRC6块电压5	
546		ID_HEAD512				32			SMBAT/BRC6 BLOCK6  Voltage		SMBAT/BRC6块电压6	
547		ID_HEAD513				32			SMBAT/BRC6 BLOCK7  Voltage		SMBAT/BRC6块电压7	
548		ID_HEAD514				32			SMBAT/BRC6 BLOCK8  Voltage		SMBAT/BRC6块电压8	
549		ID_HEAD515				32			SMBAT/BRC6 BLOCK9  Voltage		SMBAT/BRC6块电压9	
550		ID_HEAD516				32			SMBAT/BRC6 BLOCK10 Voltage		SMBAT/BRC6块电压10	
551		ID_HEAD517				32			SMBAT/BRC6 BLOCK11 Voltage		SMBAT/BRC6块电压11	
552		ID_HEAD518				32			SMBAT/BRC6 BLOCK12 Voltage		SMBAT/BRC6块电压12	
553		ID_HEAD519				32			SMBAT/BRC6 BLOCK13 Voltage		SMBAT/BRC6块电压13	
554		ID_HEAD520				32			SMBAT/BRC6 BLOCK14 Voltage		SMBAT/BRC6块电压14	
555		ID_HEAD521				32			SMBAT/BRC6 BLOCK15 Voltage		SMBAT/BRC6块电压15	
556		ID_HEAD522				32			SMBAT/BRC6 BLOCK16 Voltage		SMBAT/BRC6块电压16	
557		ID_HEAD523				32			SMBAT/BRC6 BLOCK17 Voltage		SMBAT/BRC6块电压17	
558		ID_HEAD524				32			SMBAT/BRC6 BLOCK18 Voltage		SMBAT/BRC6块电压18	
559		ID_HEAD525				32			SMBAT/BRC6 BLOCK19 Voltage		SMBAT/BRC6块电压19	
560		ID_HEAD526				32			SMBAT/BRC6 BLOCK20 Voltage		SMBAT/BRC6块电压20	
561		ID_HEAD527				32			SMBAT/BRC6 BLOCK21 Voltage		SMBAT/BRC6块电压21	
562		ID_HEAD528				32			SMBAT/BRC6 BLOCK22 Voltage		SMBAT/BRC6块电压22	
563		ID_HEAD529				32			SMBAT/BRC6 BLOCK23 Voltage		SMBAT/BRC6块电压23	
564		ID_HEAD530				32			SMBAT/BRC6 BLOCK24 Voltage		SMBAT/BRC6块电压24	
565		ID_HEAD531				32			SMBAT/BRC7 BLOCK1  Voltage		SMBAT/BRC7块电压1	
566		ID_HEAD532				32			SMBAT/BRC7 BLOCK2  Voltage		SMBAT/BRC7块电压2	
567		ID_HEAD533				32			SMBAT/BRC7 BLOCK3  Voltage		SMBAT/BRC7块电压3	
568		ID_HEAD534				32			SMBAT/BRC7 BLOCK4  Voltage		SMBAT/BRC7块电压4	
569		ID_HEAD535				32			SMBAT/BRC7 BLOCK5  Voltage		SMBAT/BRC7块电压5	
570		ID_HEAD536				32			SMBAT/BRC7 BLOCK6  Voltage		SMBAT/BRC7块电压6	
571		ID_HEAD537				32			SMBAT/BRC7 BLOCK7  Voltage		SMBAT/BRC7块电压7	
572		ID_HEAD538				32			SMBAT/BRC7 BLOCK8  Voltage		SMBAT/BRC7块电压8	
573		ID_HEAD539				32			SMBAT/BRC7 BLOCK9  Voltage		SMBAT/BRC7块电压9	
574		ID_HEAD540				32			SMBAT/BRC7 BLOCK10 Voltage		SMBAT/BRC7块电压10	
575		ID_HEAD541				32			SMBAT/BRC7 BLOCK11 Voltage		SMBAT/BRC7块电压11	
576		ID_HEAD542				32			SMBAT/BRC7 BLOCK12 Voltage		SMBAT/BRC7块电压12	
577		ID_HEAD543				32			SMBAT/BRC7 BLOCK13 Voltage		SMBAT/BRC7块电压13	
578		ID_HEAD544				32			SMBAT/BRC7 BLOCK14 Voltage		SMBAT/BRC7块电压14	
579		ID_HEAD545				32			SMBAT/BRC7 BLOCK15 Voltage		SMBAT/BRC7块电压15	
580		ID_HEAD546				32			SMBAT/BRC7 BLOCK16 Voltage		SMBAT/BRC7块电压16	
581		ID_HEAD547				32			SMBAT/BRC7 BLOCK17 Voltage		SMBAT/BRC7块电压17	
582		ID_HEAD548				32			SMBAT/BRC7 BLOCK18 Voltage		SMBAT/BRC7块电压18	
583		ID_HEAD549				32			SMBAT/BRC7 BLOCK19 Voltage		SMBAT/BRC7块电压19	
584		ID_HEAD550				32			SMBAT/BRC7 BLOCK20 Voltage		SMBAT/BRC7块电压20	
585		ID_HEAD551				32			SMBAT/BRC7 BLOCK21 Voltage		SMBAT/BRC7块电压21	
586		ID_HEAD552				32			SMBAT/BRC7 BLOCK22 Voltage		SMBAT/BRC7块电压22	
587		ID_HEAD553				32			SMBAT/BRC7 BLOCK23 Voltage		SMBAT/BRC7块电压23	
588		ID_HEAD554				32			SMBAT/BRC7 BLOCK24 Voltage		SMBAT/BRC7块电压24	
589		ID_HEAD555				32			SMBAT/BRC8 BLOCK1  Voltage		SMBAT/BRC8块电压1	
590		ID_HEAD556				32			SMBAT/BRC8 BLOCK2  Voltage		SMBAT/BRC8块电压2	
591		ID_HEAD557				32			SMBAT/BRC8 BLOCK3  Voltage		SMBAT/BRC8块电压3	
592		ID_HEAD558				32			SMBAT/BRC8 BLOCK4  Voltage		SMBAT/BRC8块电压4	
593		ID_HEAD559				32			SMBAT/BRC8 BLOCK5  Voltage		SMBAT/BRC8块电压5	
594		ID_HEAD560				32			SMBAT/BRC8 BLOCK6  Voltage		SMBAT/BRC8块电压6	
595		ID_HEAD561				32			SMBAT/BRC8 BLOCK7  Voltage		SMBAT/BRC8块电压7	
596		ID_HEAD562				32			SMBAT/BRC8 BLOCK8  Voltage		SMBAT/BRC8块电压8	
597		ID_HEAD563				32			SMBAT/BRC8 BLOCK9  Voltage		SMBAT/BRC8块电压9	
598		ID_HEAD564				32			SMBAT/BRC8 BLOCK10 Voltage		SMBAT/BRC8块电压10	
599		ID_HEAD565				32			SMBAT/BRC8 BLOCK11 Voltage		SMBAT/BRC8块电压11	
600		ID_HEAD566				32			SMBAT/BRC8 BLOCK12 Voltage		SMBAT/BRC8块电压12	
601		ID_HEAD567				32			SMBAT/BRC8 BLOCK13 Voltage		SMBAT/BRC8块电压13	
602		ID_HEAD568				32			SMBAT/BRC8 BLOCK14 Voltage		SMBAT/BRC8块电压14	
603		ID_HEAD569				32			SMBAT/BRC8 BLOCK15 Voltage		SMBAT/BRC8块电压15	
604		ID_HEAD570				32			SMBAT/BRC8 BLOCK16 Voltage		SMBAT/BRC8块电压16	
605		ID_HEAD571				32			SMBAT/BRC8 BLOCK17 Voltage		SMBAT/BRC8块电压17	
606		ID_HEAD572				32			SMBAT/BRC8 BLOCK18 Voltage		SMBAT/BRC8块电压18	
607		ID_HEAD573				32			SMBAT/BRC8 BLOCK19 Voltage		SMBAT/BRC8块电压19	
608		ID_HEAD574				32			SMBAT/BRC8 BLOCK20 Voltage		SMBAT/BRC8块电压20	
609		ID_HEAD575				32			SMBAT/BRC8 BLOCK21 Voltage		SMBAT/BRC8块电压21	
610		ID_HEAD576				32			SMBAT/BRC8 BLOCK22 Voltage		SMBAT/BRC8块电压22	
611		ID_HEAD577				32			SMBAT/BRC8 BLOCK23 Voltage		SMBAT/BRC8块电压23	
612		ID_HEAD578				32			SMBAT/BRC8 BLOCK24 Voltage		SMBAT/BRC8块电压24	
613		ID_HEAD579				32			SMBAT/BRC9 BLOCK1  Voltage		SMBAT/BRC9块电压1	
614		ID_HEAD580				32			SMBAT/BRC9 BLOCK2  Voltage		SMBAT/BRC9块电压2	
615		ID_HEAD581				32			SMBAT/BRC9 BLOCK3  Voltage		SMBAT/BRC9块电压3	
616		ID_HEAD582				32			SMBAT/BRC9 BLOCK4  Voltage		SMBAT/BRC9块电压4	
617		ID_HEAD583				32			SMBAT/BRC9 BLOCK5  Voltage		SMBAT/BRC9块电压5	
618		ID_HEAD584				32			SMBAT/BRC9 BLOCK6  Voltage		SMBAT/BRC9块电压6	
619		ID_HEAD585				32			SMBAT/BRC9 BLOCK7  Voltage		SMBAT/BRC9块电压7	
620		ID_HEAD586				32			SMBAT/BRC9 BLOCK8  Voltage		SMBAT/BRC9块电压8	
621		ID_HEAD587				32			SMBAT/BRC9 BLOCK9  Voltage		SMBAT/BRC9块电压9	
622		ID_HEAD588				32			SMBAT/BRC9 BLOCK10 Voltage		SMBAT/BRC9块电压10	
623		ID_HEAD589				32			SMBAT/BRC9 BLOCK11 Voltage		SMBAT/BRC9块电压11	
624		ID_HEAD590				32			SMBAT/BRC9 BLOCK12 Voltage		SMBAT/BRC9块电压12	
625		ID_HEAD591				32			SMBAT/BRC9 BLOCK13 Voltage		SMBAT/BRC9块电压13	
626		ID_HEAD592				32			SMBAT/BRC9 BLOCK14 Voltage		SMBAT/BRC9块电压14	
627		ID_HEAD593				32			SMBAT/BRC9 BLOCK15 Voltage		SMBAT/BRC9块电压15	
628		ID_HEAD594				32			SMBAT/BRC9 BLOCK16 Voltage		SMBAT/BRC9块电压16	
629		ID_HEAD595				32			SMBAT/BRC9 BLOCK17 Voltage		SMBAT/BRC9块电压17	
630		ID_HEAD596				32			SMBAT/BRC9 BLOCK18 Voltage		SMBAT/BRC9块电压18	
631		ID_HEAD597				32			SMBAT/BRC9 BLOCK19 Voltage		SMBAT/BRC9块电压19	
632		ID_HEAD598				32			SMBAT/BRC9 BLOCK20 Voltage		SMBAT/BRC9块电压20	
633		ID_HEAD599				32			SMBAT/BRC9 BLOCK21 Voltage		SMBAT/BRC9块电压21	
634		ID_HEAD600				32			SMBAT/BRC9 BLOCK22 Voltage		SMBAT/BRC9块电压22	
635		ID_HEAD601				32			SMBAT/BRC9 BLOCK23 Voltage		SMBAT/BRC9块电压23	
636		ID_HEAD602				32			SMBAT/BRC9 BLOCK24 Voltage		SMBAT/BRC9块电压24	
637		ID_HEAD603				32			SMBAT/BRC10 BLOCK1  Voltage		SMBAT/BRC10块电压1	
638		ID_HEAD604				32			SMBAT/BRC10 BLOCK2  Voltage		SMBAT/BRC10块电压2	
639		ID_HEAD605				32			SMBAT/BRC10 BLOCK3  Voltage		SMBAT/BRC10块电压3	
640		ID_HEAD606				32			SMBAT/BRC10 BLOCK4  Voltage		SMBAT/BRC10块电压4	
641		ID_HEAD607				32			SMBAT/BRC10 BLOCK5  Voltage		SMBAT/BRC10块电压5	
642		ID_HEAD608				32			SMBAT/BRC10 BLOCK6  Voltage		SMBAT/BRC10块电压6	
643		ID_HEAD609				32			SMBAT/BRC10 BLOCK7  Voltage		SMBAT/BRC10块电压7	
644		ID_HEAD610				32			SMBAT/BRC10 BLOCK8  Voltage		SMBAT/BRC10块电压8	
645		ID_HEAD611				32			SMBAT/BRC10 BLOCK9  Voltage		SMBAT/BRC10块电压9	
646		ID_HEAD612				32			SMBAT/BRC10 BLOCK10 Voltage		SMBAT/BRC10块电压10	
647		ID_HEAD613				32			SMBAT/BRC10 BLOCK11 Voltage		SMBAT/BRC10块电压11	
648		ID_HEAD614				32			SMBAT/BRC10 BLOCK12 Voltage		SMBAT/BRC10块电压12	
649		ID_HEAD615				32			SMBAT/BRC10 BLOCK13 Voltage		SMBAT/BRC10块电压13	
650		ID_HEAD616				32			SMBAT/BRC10 BLOCK14 Voltage		SMBAT/BRC10块电压14	
651		ID_HEAD617				32			SMBAT/BRC10 BLOCK15 Voltage		SMBAT/BRC10块电压15	
652		ID_HEAD618				32			SMBAT/BRC10 BLOCK16 Voltage		SMBAT/BRC10块电压16	
653		ID_HEAD619				32			SMBAT/BRC10 BLOCK17 Voltage		SMBAT/BRC10块电压17	
654		ID_HEAD620				32			SMBAT/BRC10 BLOCK18 Voltage		SMBAT/BRC10块电压18	
655		ID_HEAD621				32			SMBAT/BRC10 BLOCK19 Voltage		SMBAT/BRC10块电压19	
656		ID_HEAD622				32			SMBAT/BRC10 BLOCK20 Voltage		SMBAT/BRC10块电压20	
657		ID_HEAD623				32			SMBAT/BRC10 BLOCK21 Voltage		SMBAT/BRC10块电压21	
658		ID_HEAD624				32			SMBAT/BRC10 BLOCK22 Voltage		SMBAT/BRC10块电压22	
659		ID_HEAD625				32			SMBAT/BRC10 BLOCK23 Voltage		SMBAT/BRC10块电压23	
660		ID_HEAD626				32			SMBAT/BRC10 BLOCK24 Voltage		SMBAT/BRC10块电压24	
661		ID_HEAD627				32			SMBAT/BRC11 BLOCK1  Voltage		SMBAT/BRC11块电压1	
662		ID_HEAD628				32			SMBAT/BRC11 BLOCK2  Voltage		SMBAT/BRC11块电压2	
663		ID_HEAD629				32			SMBAT/BRC11 BLOCK3  Voltage		SMBAT/BRC11块电压3	
664		ID_HEAD630				32			SMBAT/BRC11 BLOCK4  Voltage		SMBAT/BRC11块电压4	
665		ID_HEAD631				32			SMBAT/BRC11 BLOCK5  Voltage		SMBAT/BRC11块电压5	
666		ID_HEAD632				32			SMBAT/BRC11 BLOCK6  Voltage		SMBAT/BRC11块电压6	
667		ID_HEAD633				32			SMBAT/BRC11 BLOCK7  Voltage		SMBAT/BRC11块电压7	
668		ID_HEAD634				32			SMBAT/BRC11 BLOCK8  Voltage		SMBAT/BRC11块电压8	
669		ID_HEAD635				32			SMBAT/BRC11 BLOCK9  Voltage		SMBAT/BRC11块电压9	
670		ID_HEAD636				32			SMBAT/BRC11 BLOCK10 Voltage		SMBAT/BRC11块电压10	
671		ID_HEAD637				32			SMBAT/BRC11 BLOCK11 Voltage		SMBAT/BRC11块电压11	
672		ID_HEAD638				32			SMBAT/BRC11 BLOCK12 Voltage		SMBAT/BRC11块电压12	
673		ID_HEAD639				32			SMBAT/BRC11 BLOCK13 Voltage		SMBAT/BRC11块电压13	
674		ID_HEAD640				32			SMBAT/BRC11 BLOCK14 Voltage		SMBAT/BRC11块电压14	
675		ID_HEAD641				32			SMBAT/BRC11 BLOCK15 Voltage		SMBAT/BRC11块电压15	
676		ID_HEAD642				32			SMBAT/BRC11 BLOCK16 Voltage		SMBAT/BRC11块电压16	
677		ID_HEAD643				32			SMBAT/BRC11 BLOCK17 Voltage		SMBAT/BRC11块电压17	
678		ID_HEAD644				32			SMBAT/BRC11 BLOCK18 Voltage		SMBAT/BRC11块电压18	
679		ID_HEAD645				32			SMBAT/BRC11 BLOCK19 Voltage		SMBAT/BRC11块电压19	
680		ID_HEAD646				32			SMBAT/BRC11 BLOCK20 Voltage		SMBAT/BRC11块电压20	
681		ID_HEAD647				32			SMBAT/BRC11 BLOCK21 Voltage		SMBAT/BRC11块电压21	
682		ID_HEAD648				32			SMBAT/BRC11 BLOCK22 Voltage		SMBAT/BRC11块电压22	
683		ID_HEAD649				32			SMBAT/BRC11 BLOCK23 Voltage		SMBAT/BRC11块电压23	
684		ID_HEAD650				32			SMBAT/BRC11 BLOCK24 Voltage		SMBAT/BRC11块电压24	
685		ID_HEAD651				32			SMBAT/BRC12 BLOCK1  Voltage		SMBAT/BRC12块电压1	
686		ID_HEAD652				32			SMBAT/BRC12 BLOCK2  Voltage		SMBAT/BRC12块电压2	
687		ID_HEAD653				32			SMBAT/BRC12 BLOCK3  Voltage		SMBAT/BRC12块电压3	
688		ID_HEAD654				32			SMBAT/BRC12 BLOCK4  Voltage		SMBAT/BRC12块电压4	
689		ID_HEAD655				32			SMBAT/BRC12 BLOCK5  Voltage		SMBAT/BRC12块电压5	
690		ID_HEAD656				32			SMBAT/BRC12 BLOCK6  Voltage		SMBAT/BRC12块电压6	
691		ID_HEAD657				32			SMBAT/BRC12 BLOCK7  Voltage		SMBAT/BRC12块电压7	
692		ID_HEAD658				32			SMBAT/BRC12 BLOCK8  Voltage		SMBAT/BRC12块电压8	
693		ID_HEAD659				32			SMBAT/BRC12 BLOCK9  Voltage		SMBAT/BRC12块电压9	
694		ID_HEAD660				32			SMBAT/BRC12 BLOCK10 Voltage		SMBAT/BRC12块电压10	
695		ID_HEAD661				32			SMBAT/BRC12 BLOCK11 Voltage		SMBAT/BRC12块电压11	
696		ID_HEAD662				32			SMBAT/BRC12 BLOCK12 Voltage		SMBAT/BRC12块电压12	
697		ID_HEAD663				32			SMBAT/BRC12 BLOCK13 Voltage		SMBAT/BRC12块电压13	
698		ID_HEAD664				32			SMBAT/BRC12 BLOCK14 Voltage		SMBAT/BRC12块电压14	
699		ID_HEAD665				32			SMBAT/BRC12 BLOCK15 Voltage		SMBAT/BRC12块电压15	
700		ID_HEAD666				32			SMBAT/BRC12 BLOCK16 Voltage		SMBAT/BRC12块电压16	
701		ID_HEAD667				32			SMBAT/BRC12 BLOCK17 Voltage		SMBAT/BRC12块电压17	
702		ID_HEAD668				32			SMBAT/BRC12 BLOCK18 Voltage		SMBAT/BRC12块电压18	
703		ID_HEAD669				32			SMBAT/BRC12 BLOCK19 Voltage		SMBAT/BRC12块电压19	
704		ID_HEAD670				32			SMBAT/BRC12 BLOCK20 Voltage		SMBAT/BRC12块电压20	
705		ID_HEAD671				32			SMBAT/BRC12 BLOCK21 Voltage		SMBAT/BRC12块电压21	
706		ID_HEAD672				32			SMBAT/BRC12 BLOCK22 Voltage		SMBAT/BRC12块电压22	
707		ID_HEAD673				32			SMBAT/BRC12 BLOCK23 Voltage		SMBAT/BRC12块电压23	
708		ID_HEAD674				32			SMBAT/BRC12 BLOCK24 Voltage		SMBAT/BRC12块电压24	
709		ID_HEAD675				32			SMBAT/BRC13 BLOCK1  Voltage		SMBAT/BRC13块电压1	
710		ID_HEAD676				32			SMBAT/BRC13 BLOCK2  Voltage		SMBAT/BRC13块电压2	
711		ID_HEAD677				32			SMBAT/BRC13 BLOCK3  Voltage		SMBAT/BRC13块电压3	
712		ID_HEAD678				32			SMBAT/BRC13 BLOCK4  Voltage		SMBAT/BRC13块电压4	
713		ID_HEAD679				32			SMBAT/BRC13 BLOCK5  Voltage		SMBAT/BRC13块电压5	
714		ID_HEAD680				32			SMBAT/BRC13 BLOCK6  Voltage		SMBAT/BRC13块电压6	
715		ID_HEAD681				32			SMBAT/BRC13 BLOCK7  Voltage		SMBAT/BRC13块电压7	
716		ID_HEAD682				32			SMBAT/BRC13 BLOCK8  Voltage		SMBAT/BRC13块电压8	
717		ID_HEAD683				32			SMBAT/BRC13 BLOCK9  Voltage		SMBAT/BRC13块电压9	
718		ID_HEAD684				32			SMBAT/BRC13 BLOCK10 Voltage		SMBAT/BRC13块电压10	
719		ID_HEAD685				32			SMBAT/BRC13 BLOCK11 Voltage		SMBAT/BRC13块电压11	
720		ID_HEAD686				32			SMBAT/BRC13 BLOCK12 Voltage		SMBAT/BRC13块电压12	
721		ID_HEAD687				32			SMBAT/BRC13 BLOCK13 Voltage		SMBAT/BRC13块电压13	
722		ID_HEAD688				32			SMBAT/BRC13 BLOCK14 Voltage		SMBAT/BRC13块电压14	
723		ID_HEAD689				32			SMBAT/BRC13 BLOCK15 Voltage		SMBAT/BRC13块电压15	
724		ID_HEAD690				32			SMBAT/BRC13 BLOCK16 Voltage		SMBAT/BRC13块电压16	
725		ID_HEAD691				32			SMBAT/BRC13 BLOCK17 Voltage		SMBAT/BRC13块电压17	
726		ID_HEAD692				32			SMBAT/BRC13 BLOCK18 Voltage		SMBAT/BRC13块电压18	
727		ID_HEAD693				32			SMBAT/BRC13 BLOCK19 Voltage		SMBAT/BRC13块电压19	
728		ID_HEAD694				32			SMBAT/BRC13 BLOCK20 Voltage		SMBAT/BRC13块电压20	
729		ID_HEAD695				32			SMBAT/BRC13 BLOCK21 Voltage		SMBAT/BRC13块电压21	
730		ID_HEAD696				32			SMBAT/BRC13 BLOCK22 Voltage		SMBAT/BRC13块电压22	
731		ID_HEAD697				32			SMBAT/BRC13 BLOCK23 Voltage		SMBAT/BRC13块电压23	
732		ID_HEAD698				32			SMBAT/BRC13 BLOCK24 Voltage		SMBAT/BRC13块电压24	
733		ID_HEAD699				32			SMBAT/BRC14 BLOCK1  Voltage		SMBAT/BRC14块电压1	
734		ID_HEAD700				32			SMBAT/BRC14 BLOCK2  Voltage		SMBAT/BRC14块电压2	
735		ID_HEAD701				32			SMBAT/BRC14 BLOCK3  Voltage		SMBAT/BRC14块电压3	
736		ID_HEAD702				32			SMBAT/BRC14 BLOCK4  Voltage		SMBAT/BRC14块电压4	
737		ID_HEAD703				32			SMBAT/BRC14 BLOCK5  Voltage		SMBAT/BRC14块电压5	
738		ID_HEAD704				32			SMBAT/BRC14 BLOCK6  Voltage		SMBAT/BRC14块电压6	
739		ID_HEAD705				32			SMBAT/BRC14 BLOCK7  Voltage		SMBAT/BRC14块电压7	
740		ID_HEAD706				32			SMBAT/BRC14 BLOCK8  Voltage		SMBAT/BRC14块电压8	
741		ID_HEAD707				32			SMBAT/BRC14 BLOCK9  Voltage		SMBAT/BRC14块电压9	
742		ID_HEAD708				32			SMBAT/BRC14 BLOCK10 Voltage		SMBAT/BRC14块电压10	
743		ID_HEAD709				32			SMBAT/BRC14 BLOCK11 Voltage		SMBAT/BRC14块电压11	
744		ID_HEAD710				32			SMBAT/BRC14 BLOCK12 Voltage		SMBAT/BRC14块电压12	
745		ID_HEAD711				32			SMBAT/BRC14 BLOCK13 Voltage		SMBAT/BRC14块电压13	
746		ID_HEAD712				32			SMBAT/BRC14 BLOCK14 Voltage		SMBAT/BRC14块电压14	
747		ID_HEAD713				32			SMBAT/BRC14 BLOCK15 Voltage		SMBAT/BRC14块电压15	
748		ID_HEAD714				32			SMBAT/BRC14 BLOCK16 Voltage		SMBAT/BRC14块电压16	
749		ID_HEAD715				32			SMBAT/BRC14 BLOCK17 Voltage		SMBAT/BRC14块电压17	
750		ID_HEAD716				32			SMBAT/BRC14 BLOCK18 Voltage		SMBAT/BRC14块电压18	
751		ID_HEAD717				32			SMBAT/BRC14 BLOCK19 Voltage		SMBAT/BRC14块电压19	
752		ID_HEAD718				32			SMBAT/BRC14 BLOCK20 Voltage		SMBAT/BRC14块电压20	
753		ID_HEAD719				32			SMBAT/BRC14 BLOCK21 Voltage		SMBAT/BRC14块电压21	
754		ID_HEAD720				32			SMBAT/BRC14 BLOCK22 Voltage		SMBAT/BRC14块电压22	
755		ID_HEAD721				32			SMBAT/BRC14 BLOCK23 Voltage		SMBAT/BRC14块电压23	
756		ID_HEAD722				32			SMBAT/BRC14 BLOCK24 Voltage		SMBAT/BRC14块电压24	
757		ID_HEAD723				32			SMBAT/BRC15 BLOCK1  Voltage		SMBAT/BRC15块电压1	
758		ID_HEAD724				32			SMBAT/BRC15 BLOCK2  Voltage		SMBAT/BRC15块电压2	
759		ID_HEAD725				32			SMBAT/BRC15 BLOCK3  Voltage		SMBAT/BRC15块电压3	
760		ID_HEAD726				32			SMBAT/BRC15 BLOCK4  Voltage		SMBAT/BRC15块电压4	
761		ID_HEAD727				32			SMBAT/BRC15 BLOCK5  Voltage		SMBAT/BRC15块电压5	
762		ID_HEAD728				32			SMBAT/BRC15 BLOCK6  Voltage		SMBAT/BRC15块电压6	
763		ID_HEAD729				32			SMBAT/BRC15 BLOCK7  Voltage		SMBAT/BRC15块电压7	
764		ID_HEAD730				32			SMBAT/BRC15 BLOCK8  Voltage		SMBAT/BRC15块电压8	
765		ID_HEAD731				32			SMBAT/BRC15 BLOCK9  Voltage		SMBAT/BRC15块电压9	
766		ID_HEAD732				32			SMBAT/BRC15 BLOCK10 Voltage		SMBAT/BRC15块电压10	
767		ID_HEAD733				32			SMBAT/BRC15 BLOCK11 Voltage		SMBAT/BRC15块电压11	
768		ID_HEAD734				32			SMBAT/BRC15 BLOCK12 Voltage		SMBAT/BRC15块电压12	
769		ID_HEAD735				32			SMBAT/BRC15 BLOCK13 Voltage		SMBAT/BRC15块电压13	
770		ID_HEAD736				32			SMBAT/BRC15 BLOCK14 Voltage		SMBAT/BRC15块电压14	
771		ID_HEAD737				32			SMBAT/BRC15 BLOCK15 Voltage		SMBAT/BRC15块电压15	
772		ID_HEAD738				32			SMBAT/BRC15 BLOCK16 Voltage		SMBAT/BRC15块电压16	
773		ID_HEAD739				32			SMBAT/BRC15 BLOCK17 Voltage		SMBAT/BRC15块电压17	
774		ID_HEAD740				32			SMBAT/BRC15 BLOCK18 Voltage		SMBAT/BRC15块电压18	
775		ID_HEAD741				32			SMBAT/BRC15 BLOCK19 Voltage		SMBAT/BRC15块电压19	
776		ID_HEAD742				32			SMBAT/BRC15 BLOCK20 Voltage		SMBAT/BRC15块电压20	
777		ID_HEAD743				32			SMBAT/BRC15 BLOCK21 Voltage		SMBAT/BRC15块电压21	
778		ID_HEAD744				32			SMBAT/BRC15 BLOCK22 Voltage		SMBAT/BRC15块电压22	
779		ID_HEAD745				32			SMBAT/BRC15 BLOCK23 Voltage		SMBAT/BRC15块电压23	
780		ID_HEAD746				32			SMBAT/BRC15 BLOCK24 Voltage		SMBAT/BRC15块电压24	
781		ID_HEAD747				32			SMBAT/BRC16 BLOCK1  Voltage		SMBAT/BRC16块电压1	
782		ID_HEAD748				32			SMBAT/BRC16 BLOCK2  Voltage		SMBAT/BRC16块电压2	
783		ID_HEAD749				32			SMBAT/BRC16 BLOCK3  Voltage		SMBAT/BRC16块电压3	
784		ID_HEAD750				32			SMBAT/BRC16 BLOCK4  Voltage		SMBAT/BRC16块电压4	
785		ID_HEAD751				32			SMBAT/BRC16 BLOCK5  Voltage		SMBAT/BRC16块电压5	
786		ID_HEAD752				32			SMBAT/BRC16 BLOCK6  Voltage		SMBAT/BRC16块电压6	
787		ID_HEAD753				32			SMBAT/BRC16 BLOCK7  Voltage		SMBAT/BRC16块电压7	
788		ID_HEAD754				32			SMBAT/BRC16 BLOCK8  Voltage		SMBAT/BRC16块电压8	
789		ID_HEAD755				32			SMBAT/BRC16 BLOCK9  Voltage		SMBAT/BRC16块电压9	
790		ID_HEAD756				32			SMBAT/BRC16 BLOCK10 Voltage		SMBAT/BRC16块电压10	
791		ID_HEAD757				32			SMBAT/BRC16 BLOCK11 Voltage		SMBAT/BRC16块电压11	
792		ID_HEAD758				32			SMBAT/BRC16 BLOCK12 Voltage		SMBAT/BRC16块电压12	
793		ID_HEAD759				32			SMBAT/BRC16 BLOCK13 Voltage		SMBAT/BRC16块电压13	
794		ID_HEAD760				32			SMBAT/BRC16 BLOCK14 Voltage		SMBAT/BRC16块电压14	
795		ID_HEAD761				32			SMBAT/BRC16 BLOCK15 Voltage		SMBAT/BRC16块电压15	
796		ID_HEAD762				32			SMBAT/BRC16 BLOCK16 Voltage		SMBAT/BRC16块电压16	
797		ID_HEAD763				32			SMBAT/BRC16 BLOCK17 Voltage		SMBAT/BRC16块电压17	
798		ID_HEAD764				32			SMBAT/BRC16 BLOCK18 Voltage		SMBAT/BRC16块电压18	
799		ID_HEAD765				32			SMBAT/BRC16 BLOCK19 Voltage		SMBAT/BRC16块电压19	
800		ID_HEAD766				32			SMBAT/BRC16 BLOCK20 Voltage		SMBAT/BRC16块电压20	
801		ID_HEAD767				32			SMBAT/BRC16 BLOCK21 Voltage		SMBAT/BRC16块电压21	
802		ID_HEAD768				32			SMBAT/BRC16 BLOCK22 Voltage		SMBAT/BRC16块电压22	
803		ID_HEAD769				32			SMBAT/BRC16 BLOCK23 Voltage		SMBAT/BRC16块电压23	
804		ID_HEAD770				32			SMBAT/BRC16 BLOCK24 Voltage		SMBAT/BRC16块电压24	
805		ID_HEAD771				32			SMBAT/BRC17 BLOCK1  Voltage		SMBAT/BRC17块电压1	
806		ID_HEAD772				32			SMBAT/BRC17 BLOCK2  Voltage		SMBAT/BRC17块电压2	
807		ID_HEAD773				32			SMBAT/BRC17 BLOCK3  Voltage		SMBAT/BRC17块电压3	
808		ID_HEAD774				32			SMBAT/BRC17 BLOCK4  Voltage		SMBAT/BRC17块电压4	
809		ID_HEAD775				32			SMBAT/BRC17 BLOCK5  Voltage		SMBAT/BRC17块电压5	
810		ID_HEAD776				32			SMBAT/BRC17 BLOCK6  Voltage		SMBAT/BRC17块电压6	
811		ID_HEAD777				32			SMBAT/BRC17 BLOCK7  Voltage		SMBAT/BRC17块电压7	
812		ID_HEAD778				32			SMBAT/BRC17 BLOCK8  Voltage		SMBAT/BRC17块电压8	
813		ID_HEAD779				32			SMBAT/BRC17 BLOCK9  Voltage		SMBAT/BRC17块电压9	
814		ID_HEAD780				32			SMBAT/BRC17 BLOCK10 Voltage		SMBAT/BRC17块电压10	
815		ID_HEAD781				32			SMBAT/BRC17 BLOCK11 Voltage		SMBAT/BRC17块电压11	
816		ID_HEAD782				32			SMBAT/BRC17 BLOCK12 Voltage		SMBAT/BRC17块电压12	
817		ID_HEAD783				32			SMBAT/BRC17 BLOCK13 Voltage		SMBAT/BRC17块电压13	
818		ID_HEAD784				32			SMBAT/BRC17 BLOCK14 Voltage		SMBAT/BRC17块电压14	
819		ID_HEAD785				32			SMBAT/BRC17 BLOCK15 Voltage		SMBAT/BRC17块电压15	
820		ID_HEAD786				32			SMBAT/BRC17 BLOCK16 Voltage		SMBAT/BRC17块电压16	
821		ID_HEAD787				32			SMBAT/BRC17 BLOCK17 Voltage		SMBAT/BRC17块电压17	
822		ID_HEAD788				32			SMBAT/BRC17 BLOCK18 Voltage		SMBAT/BRC17块电压18	
823		ID_HEAD789				32			SMBAT/BRC17 BLOCK19 Voltage		SMBAT/BRC17块电压19	
824		ID_HEAD790				32			SMBAT/BRC17 BLOCK20 Voltage		SMBAT/BRC17块电压20	
825		ID_HEAD791				32			SMBAT/BRC17 BLOCK21 Voltage		SMBAT/BRC17块电压21	
826		ID_HEAD792				32			SMBAT/BRC17 BLOCK22 Voltage		SMBAT/BRC17块电压22	
827		ID_HEAD793				32			SMBAT/BRC17 BLOCK23 Voltage		SMBAT/BRC17块电压23	
828		ID_HEAD794				32			SMBAT/BRC17 BLOCK24 Voltage		SMBAT/BRC17块电压24	
829		ID_HEAD795				32			SMBAT/BRC18 BLOCK1  Voltage		SMBAT/BRC18块电压1	
830		ID_HEAD796				32			SMBAT/BRC18 BLOCK2  Voltage		SMBAT/BRC18块电压2	
831		ID_HEAD797				32			SMBAT/BRC18 BLOCK3  Voltage		SMBAT/BRC18块电压3	
832		ID_HEAD798				32			SMBAT/BRC18 BLOCK4  Voltage		SMBAT/BRC18块电压4	
833		ID_HEAD799				32			SMBAT/BRC18 BLOCK5  Voltage		SMBAT/BRC18块电压5	
834		ID_HEAD800				32			SMBAT/BRC18 BLOCK6  Voltage		SMBAT/BRC18块电压6	
835		ID_HEAD801				32			SMBAT/BRC18 BLOCK7  Voltage		SMBAT/BRC18块电压7	
836		ID_HEAD802				32			SMBAT/BRC18 BLOCK8  Voltage		SMBAT/BRC18块电压8	
837		ID_HEAD803				32			SMBAT/BRC18 BLOCK9  Voltage		SMBAT/BRC18块电压9	
838		ID_HEAD804				32			SMBAT/BRC18 BLOCK10 Voltage		SMBAT/BRC18块电压10	
839		ID_HEAD805				32			SMBAT/BRC18 BLOCK11 Voltage		SMBAT/BRC18块电压11	
840		ID_HEAD806				32			SMBAT/BRC18 BLOCK12 Voltage		SMBAT/BRC18块电压12	
841		ID_HEAD807				32			SMBAT/BRC18 BLOCK13 Voltage		SMBAT/BRC18块电压13	
842		ID_HEAD808				32			SMBAT/BRC18 BLOCK14 Voltage		SMBAT/BRC18块电压14	
843		ID_HEAD809				32			SMBAT/BRC18 BLOCK15 Voltage		SMBAT/BRC18块电压15	
844		ID_HEAD810				32			SMBAT/BRC18 BLOCK16 Voltage		SMBAT/BRC18块电压16	
845		ID_HEAD811				32			SMBAT/BRC18 BLOCK17 Voltage		SMBAT/BRC18块电压17	
846		ID_HEAD812				32			SMBAT/BRC18 BLOCK18 Voltage		SMBAT/BRC18块电压18	
847		ID_HEAD813				32			SMBAT/BRC18 BLOCK19 Voltage		SMBAT/BRC18块电压19	
848		ID_HEAD814				32			SMBAT/BRC18 BLOCK20 Voltage		SMBAT/BRC18块电压20	
849		ID_HEAD815				32			SMBAT/BRC18 BLOCK21 Voltage		SMBAT/BRC18块电压21	
850		ID_HEAD816				32			SMBAT/BRC18 BLOCK22 Voltage		SMBAT/BRC18块电压22	
851		ID_HEAD817				32			SMBAT/BRC18 BLOCK23 Voltage		SMBAT/BRC18块电压23	
852		ID_HEAD818				32			SMBAT/BRC18 BLOCK24 Voltage		SMBAT/BRC18块电压24	
853		ID_HEAD819				32			SMBAT/BRC19 BLOCK1  Voltage		SMBAT/BRC19块电压1	
854		ID_HEAD820				32			SMBAT/BRC19 BLOCK2  Voltage		SMBAT/BRC19块电压2	
855		ID_HEAD821				32			SMBAT/BRC19 BLOCK3  Voltage		SMBAT/BRC19块电压3	
856		ID_HEAD822				32			SMBAT/BRC19 BLOCK4  Voltage		SMBAT/BRC19块电压4	
857		ID_HEAD823				32			SMBAT/BRC19 BLOCK5  Voltage		SMBAT/BRC19块电压5	
858		ID_HEAD824				32			SMBAT/BRC19 BLOCK6  Voltage		SMBAT/BRC19块电压6	
859		ID_HEAD825				32			SMBAT/BRC19 BLOCK7  Voltage		SMBAT/BRC19块电压7	
860		ID_HEAD826				32			SMBAT/BRC19 BLOCK8  Voltage		SMBAT/BRC19块电压8	
861		ID_HEAD827				32			SMBAT/BRC19 BLOCK9  Voltage		SMBAT/BRC19块电压9	
862		ID_HEAD828				32			SMBAT/BRC19 BLOCK10 Voltage		SMBAT/BRC19块电压10	
863		ID_HEAD829				32			SMBAT/BRC19 BLOCK11 Voltage		SMBAT/BRC19块电压11	
864		ID_HEAD830				32			SMBAT/BRC19 BLOCK12 Voltage		SMBAT/BRC19块电压12	
865		ID_HEAD831				32			SMBAT/BRC19 BLOCK13 Voltage		SMBAT/BRC19块电压13	
866		ID_HEAD832				32			SMBAT/BRC19 BLOCK14 Voltage		SMBAT/BRC19块电压14	
867		ID_HEAD833				32			SMBAT/BRC19 BLOCK15 Voltage		SMBAT/BRC19块电压15	
868		ID_HEAD834				32			SMBAT/BRC19 BLOCK16 Voltage		SMBAT/BRC19块电压16	
869		ID_HEAD835				32			SMBAT/BRC19 BLOCK17 Voltage		SMBAT/BRC19块电压17	
870		ID_HEAD836				32			SMBAT/BRC19 BLOCK18 Voltage		SMBAT/BRC19块电压18	
871		ID_HEAD837				32			SMBAT/BRC19 BLOCK19 Voltage		SMBAT/BRC19块电压19	
872		ID_HEAD838				32			SMBAT/BRC19 BLOCK20 Voltage		SMBAT/BRC19块电压20	
873		ID_HEAD839				32			SMBAT/BRC19 BLOCK21 Voltage		SMBAT/BRC19块电压21	
874		ID_HEAD840				32			SMBAT/BRC19 BLOCK22 Voltage		SMBAT/BRC19块电压22	
875		ID_HEAD841				32			SMBAT/BRC19 BLOCK23 Voltage		SMBAT/BRC19块电压23	
876		ID_HEAD842				32			SMBAT/BRC19 BLOCK24 Voltage		SMBAT/BRC19块电压24	
877		ID_HEAD843				32			SMBAT/BRC20 BLOCK1  Voltage		SMBAT/BRC20块电压1	
878		ID_HEAD844				32			SMBAT/BRC20 BLOCK2  Voltage		SMBAT/BRC20块电压2	
879		ID_HEAD845				32			SMBAT/BRC20 BLOCK3  Voltage		SMBAT/BRC20块电压3	
880		ID_HEAD846				32			SMBAT/BRC20 BLOCK4  Voltage		SMBAT/BRC20块电压4	
881		ID_HEAD847				32			SMBAT/BRC20 BLOCK5  Voltage		SMBAT/BRC20块电压5	
882		ID_HEAD848				32			SMBAT/BRC20 BLOCK6  Voltage		SMBAT/BRC20块电压6	
883		ID_HEAD849				32			SMBAT/BRC20 BLOCK7  Voltage		SMBAT/BRC20块电压7	
884		ID_HEAD850				32			SMBAT/BRC20 BLOCK8  Voltage		SMBAT/BRC20块电压8	
885		ID_HEAD851				32			SMBAT/BRC20 BLOCK9  Voltage		SMBAT/BRC20块电压9	
886		ID_HEAD852				32			SMBAT/BRC20 BLOCK10 Voltage		SMBAT/BRC20块电压10	
887		ID_HEAD853				32			SMBAT/BRC20 BLOCK11 Voltage		SMBAT/BRC20块电压11	
888		ID_HEAD854				32			SMBAT/BRC20 BLOCK12 Voltage		SMBAT/BRC20块电压12	
889		ID_HEAD855				32			SMBAT/BRC20 BLOCK13 Voltage		SMBAT/BRC20块电压13	
890		ID_HEAD856				32			SMBAT/BRC20 BLOCK14 Voltage		SMBAT/BRC20块电压14	
891		ID_HEAD857				32			SMBAT/BRC20 BLOCK15 Voltage		SMBAT/BRC20块电压15	
892		ID_HEAD858				32			SMBAT/BRC20 BLOCK16 Voltage		SMBAT/BRC20块电压16	
893		ID_HEAD859				32			SMBAT/BRC20 BLOCK17 Voltage		SMBAT/BRC20块电压17	
894		ID_HEAD860				32			SMBAT/BRC20 BLOCK18 Voltage		SMBAT/BRC20块电压18	
895		ID_HEAD861				32			SMBAT/BRC20 BLOCK19 Voltage		SMBAT/BRC20块电压19	
896		ID_HEAD862				32			SMBAT/BRC20 BLOCK20 Voltage		SMBAT/BRC20块电压20	
897		ID_HEAD863				32			SMBAT/BRC20 BLOCK21 Voltage		SMBAT/BRC20块电压21	
898		ID_HEAD864				32			SMBAT/BRC20 BLOCK22 Voltage		SMBAT/BRC20块电压22	
899		ID_HEAD865				32			SMBAT/BRC20 BLOCK23 Voltage		SMBAT/BRC20块电压23	
900		ID_HEAD866				32			SMBAT/BRC20 BLOCK24 Voltage		SMBAT/BRC20块电压24	
901		ID_TIPS1				32			Search for data			搜索数据
902		ID_TIPS2				32			Please select row			请选择行
903		ID_TIPS3				32			Please select column		请选择列



[tmp.history_eventlog.html:Number]
23

[tmp.history_eventlog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_INDEX				32			Index				序号
2		ID_EQUIP				32			Device Name				设备名
3		ID_SIGNAL				32			Signal Name				信号名
4		ID_CONTROL_VALUE			32			Value				值
5		ID_UNIT					32			Unit				单位
6		ID_TIME					32			Time				时间
7		ID_SENDER_NAME				32			Sender Name				发送者名
8		ID_FROM					32			From				从
9		ID_TO					32			To					到
10		ID_QUERY				32			Query				查询
11		ID_UPLOAD				32			Upload				上载
12		ID_TIPS					64			Displays the last 500 entries		显示最多500条日志
13		ID_QUERY_TYPE				16			Query Type				查询类型
14		ID_EVENT_LOG				16			Event Log				事件日志
15		ID_SENDER_TYPE				16			Sender Type				发送者类型
16		ID_CTL_RESULT0				64			Successful				成功		
17		ID_CTL_RESULT1				64			No Memory				无内存		
18		ID_CTL_RESULT2				64			Time Expired				超时		
19		ID_CTL_RESULT3				64			Failed				失败		
20		ID_CTL_RESULT4				64			Communication Busy			通讯忙		
21		ID_CTL_RESULT5				64			Control was suppressed.		控制被屏蔽	
22		ID_CTL_RESULT6				64			Control was disabled.		不能控制	
23		ID_CTL_RESULT7				64			Control was canceled.		控制取消	


[tmp.history_datalog.html:Number]
13

[tmp.history_datalog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_DEVICE				32			Device				设备
2		ID_FROM					32			From				从
3		ID_TO					32			To					到
4		ID_QUERY				32			Query				查询
5		ID_UPLOAD				32			Upload				上载
6		ID_TIPS					64			Displays the last 500 entries		显示最多500条日志
7		ID_INDEX				32			Index				序号
8		ID_DEVICE1				32			Device Name				设备名
9		ID_SIGNAL				32			Signal Name				信号名
10		ID_VALUE				32			Value				值
11		ID_UNIT					32			Unit				单位
12		ID_TIME					32			Time				时间
13		ID_ALL_DEVICE				32			All Devices				所有设备


[tmp.setting_charge.html:Number]
20

[tmp.setting_charge.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
6		ID_SIGNAL				32			Signal				信号
7		ID_VALUE				32			Value				值
8		ID_TIME					32			Time Last Set			上次设置时间
9		ID_SET_VALUE				32			Set Value				设置值
10		ID_SET					32			Set					设置
11		ID_SUCCESS				32			Setting Success			设置成功
12		ID_OVER_TIME				32			Login Time Expired			超时
13		ID_FAIL					32			Setting Fail			设置失败
14		ID_NO_AUTHORITY				32			No privilege			没有权限
15		ID_UNKNOWN_ERROR			32			Unknown Error			未知错误
16		ID_PROTECT				32			Write Protected			写保护
17		ID_SET1					32			Set					设置
18		ID_CHARGE1				32			Battery Charge			电池充电
23		ID_NO_DATA				16			No data.				无数据!




[tmp.system_converter.html:Number]
16

[tmp.system_converter.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_RECTIFIER				32			Rectifier				整流模块
2		ID_CONVERTER				32			Converter				DC/DC模块
3		ID_SOLAR				32			Solar Converter				太阳能模块
4		ID_VOLTAGE				32			Average Voltage			平均电压
5		ID_CURRENT				32			Total Current			总电流
6		ID_CAPACITY_USED			32			System Capacity Used		系统功率消耗
7		ID_NUM_OF_RECT				32			Number of Converters		模块数量
8		ID_TOTAL_COMM_RECT			32			Total Converters Communicating	通讯模块数量
9		ID_MAX_CAPACITY				32			Max Used Capacity			最大功率消耗
10		ID_SIGNAL				32			Signal				信号
11		ID_VALUE				32			Value				值
12		ID_SOLAR1				32			Solar Converter				太阳能模块
13		ID_CURRENT1				32			Total Current			总电流
14		ID_RECTIFIER1				32			GI Rectifier			GI整流模块
15		ID_RECTIFIER2				32			GII Rectifier			GII整流模块
16		ID_RECTIFIER3				32			GIII Rectifier			GIII整流模块
#//changed by Frank Wu,2/2/30,20140527, for add single converter and single solar settings pages
17		ID_RECT_SET				32			Converter Settings			DC/DC模块设置
18		ID_BACK					16			Back						返回


[tmp.system_solar.html:Number]
16

[tmp.system_solar.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_RECTIFIER				32			Rectifier				整流模块
2		ID_CONVERTER				32			Converter				DC/DC模块
3		ID_SOLAR				32			Solar Converter				太阳能模块
4		ID_VOLTAGE				32			Average Voltage			平均电压
5		ID_CURRENT				32			Total Current			总电流
6		ID_CAPACITY_USED			32			System Capacity Used		系统功率消耗
7		ID_NUM_OF_RECT				32			Number of Solar Converters		模块数量
8		ID_TOTAL_COMM_RECT			64			Total Solar Converters Communicating	通讯模块数量
9		ID_MAX_CAPACITY				32			Max Used Capacity			最大功率消耗
10		ID_SIGNAL				32			Signal				信号
11		ID_VALUE				32			Value				值
12		ID_SOLAR1				32			Solar Converter				太阳能模块
13		ID_CURRENT1				32			Total Current			总电流
14		ID_RECTIFIER1				32			GI Rectifier			GI整流模块
15		ID_RECTIFIER2				32			GII Rectifier			GII整流模块
16		ID_RECTIFIER3				32			GIII Rectifier			GIII整流模块
#//changed by Frank Wu,3/3/30,20140527, for add single converter and single solar settings pages
17		ID_RECT_SET				32			Solar Converter Settings		太阳能模块设置
18		ID_BACK					16			Back							返回

[tmp.setting_eco.html:Number]
7

[tmp.setting_eco.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				信号
2		ID_VALUE				32			Value				值
3		ID_TIME					32			Time Last Set			上次设置时间
4		ID_SET_VALUE				32			Set Value				设置值
5		ID_SET					32			Set					设置
6		ID_SET1					32			Set					设置
7		ID_ECO					16			ECO					ECO
8		ID_NO_DATA				16			No data.				无数据!


[tmp.setting_lvd.html:Number]
6

[tmp.setting_lvd.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				信号
2		ID_VALUE				32			Value				值
3		ID_TIME					32			Time Last Set			上次设置时间
4		ID_SET_VALUE				32			Set Value				设置值
5		ID_SET					32			Set					设置
6		ID_SET1					32			Set					设置
7		ID_NO_DATA				16			No data.				无数据!


[tmp.setting_rectifiers.html:Number]
6

[tmp.setting_rectifiers.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				信号
2		ID_VALUE				32			Value				值
3		ID_TIME					32			Time Last Set			上次设置时间
4		ID_SET_VALUE				32			Set Value				设置值
5		ID_SET					32			Set					设置
6		ID_SET1					32			Set					设置
7		ID_RECT					32			Rectifiers				模块
8		ID_NO_DATA				16			No data.				无数据!


[tmp.setting_batteryTest.html:Number]
7

[tmp.setting_batteryTest.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				信号
2		ID_VALUE				32			Value				值
3		ID_TIME					32			Time Last Set			上次设置时间
4		ID_SET_VALUE				32			Set Value				设置值
5		ID_SET					32			Set					设置
6		ID_SET1					32			Set					设置
7		ID_BATT_TEST				32			Battery Test			电池测试
8		ID_NO_DATA				16			No data.				无数据!

[tmp.setting_temp.html:Number]
7

[tmp.setting_temp.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				信号
2		ID_VALUE				32			Value				值
3		ID_TIME					32			Time Last Set			上次设置时间
4		ID_SET_VALUE				32			Set Value				设置值
5		ID_SET					32			Set					设置
6		ID_SET1					32			Set					设置
7		ID_TEMP					32			Temperature				温度
8		ID_NO_DATA				16			No data.				无数据!

[tmp.setting_hybrid.html:Number]
7

[tmp.setting_hybrid.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				信号
2		ID_VALUE				32			Value				值
3		ID_TIME					32			Time Last Set			上次设置时间
4		ID_SET_VALUE				32			Set Value				设置值
5		ID_SET					32			Set					设置
6		ID_SET1					32			Set					设置
7		ID_HYBRID				32			Generator				油机
8		ID_NO_DATA				16			No data.				无数据!


[tmp.install_wizard.html:Number]
30

[tmp.install_wizard.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIME_SET				32			Time Settings			时间设置
2		ID_GET_TIME				64			Get Local Time from Connected PC			从电脑获取本地时间
3		ID_SITE_SET				32			Site Settings			局站设置
4		ID_SIGNAL				32			Signal				信号
5		ID_VALUE				32			Value				值
6		ID_SET_VALUE				32			Set Value				设置值
7		ID_SET					32			Set					设置
8		ID_SIGNAL1				32			Signal				信号
9		ID_VALUE1				32			Value				值
10		ID_TIME1				32			Time Last Set			上次设置时间
11		ID_SET_VALUE1				32			Set Value				设置值
12		ID_SET1					32			Set					设置
13		ID_SITE					32			Site Settings			局站设置
14		ID_WIZARD				32			Install Wizard			安装向导
15		ID_SET2					32			Set					设置
16		ID_SIGNAL_SET				32			Signal Settings			信号设置
17		ID_SET3					32			Set					设置
18		ID_SET4					32			Set					设置
19		ID_CHARGE				32			Battery Charge			电池充电
20		ID_ECO					32			ECO					ECO
21		ID_LVD					32			LVD					LVD
22		ID_QUICK_SET				32			Quick Settings			快速设置
23		ID_TEMP					32			Temperature				温度
24		ID_RECT					32			Rectifiers				整流模块
25		ID_CONVERTER				32			DC/DC Converters				DC/DC模块
26		ID_BATT_TEST				32			Battery Test			电池测试
27		ID_TIME_CFG				32			Time Settings				时间设置
28		ID_TIPS13				32			Site Name				局站名
29		ID_TIPS14				32			Site Location			局站地点
30		ID_TIPS15				32			System Name				系统名
31		ID_DEVICE				32			Device Name				设备名
32		ID_SIGNAL				32			Signal Name				信号名
33		ID_VALUE				32			Value				值
34		ID_SETTING_VALUE			32			Set Value				设置值
35		ID_SITE1				32			Site				局站
36		ID_POWER_SYS				32			System				系统
37		ID_USER_SET1				32			User Config1			用户配置1
38		ID_USER_SET2				32			User Config2			用户配置2
39		ID_USER_SET3				32			User Config3			用户配置3
#//changed by Frank Wu,1/1/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
40		ID_MPPT					32			Solar				太阳能
41		ID_TIPS1				32			Unknown error.			未知错误
42		ID_TIPS2				16			Successful.							成功.
43		ID_TIPS3				128			Failed. Incorrect time setting.				失败,不正确的时间设置.
44		ID_TIPS4				128			Failed. Incomplete information.				失败,输入信息不完整.
45		ID_TIPS5				64			Failed. No privileges.					失败,没有权限.
46		ID_TIPS6				128			Cannot be modified. Controller is hardware protected.	监控处于硬件保护状态,不能修改
47		ID_TIPS7				256			Secondary Server IP Address is incorrect. \nThe format should be :'nnn.nnn.nnn.nnn'.	从服务器IP地址格式错误,格式应该如下所示:'nnn.nnn.nnn.nnn'.
48		ID_TIPS8				128			Format error! There is one blank between time and date.					格式错误! 时间和日期间只允许一个空格!
49		ID_TIPS9				128					Incorrect time interval. \nTime interval should be a positive integer.			时间间隔错误,应该为正整数.
50		ID_TIPS10				128			Date should be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.			日期应该在'1970/01/01 00:00:00' 和 '2038/01/01 00:00:00'之间.
51		ID_TIPS11				128					Please clear the IP before time setting.							请先清除IP再设置时间.
52		ID_TIPS12				64			Time expired, please login again.						登录超时, 请重新登录.



[tmp.setting_user.html:Number]

[tmp.setting_user.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_USER					32			User Info				用户信息
2		ID_IPV4					32			Ethernet				Ethernet
3		ID_IPV6					32			IPV6				IPV6
4		ID_HLMS_CONF				32			Monitor Protocol			监控协议
5		ID_SITE_INFO				32			Site Info				局站信息
6		ID_TIME_CFG				32			Time Sync				时间同步
7		ID_AUTO_CONFIG				32			Auto Config				自动配置
8		ID_OTHER				32			Other Settings			其他设置
9		ID_WEB_HEAD				32			User Information			用户信息
10		ID_USER_NAME				32			User Name				用户名
11		ID_USER_AUTHORITY			32			Privilege				权限
12		ID_USER_DELETE				32			Delete				删除
13		ID_MODIFY_ADD				32			Add or Modify User			增加修改用户
14		ID_USER_NAME1				32			User Name				用户名
15		ID_USER_AUTHORITY1			32			Privilege					权限
16		ID_PASSWORD				32			Password				密码
17		ID_CONFIRM				32			Confirm				确认
18		ID_USER_ADD				32			Add					增加
19		ID_USER_MODIFY				32			Modify				修改
20		ID_ERROR0				32			Unknown Error						未知错误				
21		ID_ERROR1				32			Successful							成功				
22		ID_ERROR2				64			Failed. Incomplete information.				失败, 信息不完整.				
23		ID_ERROR3				64			Failed. The user name already exists.			失败, 用户名已经存在.		
24		ID_ERROR4				64			Failed. No privilege.					失败, 没有权限.			
25		ID_ERROR5				64			Failed. Controller is hardware protected.			失败, 监控处于硬件保护状态.	
26		ID_ERROR6				64			Failed. You can only change your password.			失败, 只能修改自己的密码.		
27		ID_ERROR7				64			Failed. Deleting 'admin' is not allowed.			失败, 不允许删除用户admin.		
28		ID_ERROR8				64			Failed. Deleting a logged in user is not allowed.		失败, 不允许删除正在登录的用户.	
29		ID_ERROR9				128			Failed. The user already exists.				失败, 该用户已经存在.		
30		ID_ERROR10				128			Failed. Too many users.					失败, 用户太多.			
31		ID_ERROR11				128			Failed. The user does not exist.				失败, 用户不存在.			
32		ID_AUTHORITY_LEVEL0			32			Browser				浏览
33		ID_AUTHORITY_LEVEL1			32			Operator				操作
34		ID_AUTHORITY_LEVEL2			32			Engineer				专业
35		ID_AUTHORITY_LEVEL3			32			Administrator			管理
36		ID_TIPS1				32			Please enter an user name.						请输入用户名.			
37		ID_TIPS2				128			The user name cannot be started or ended with spaces.		用户名不能以空格开始或结尾.			
38		ID_TIPS3				128			Passwords do not match.						密码和密码确认不匹配.		
39		ID_TIPS4				128			Please remember the password entered.				请牢记密码.					
40		ID_TIPS5				128			Please enter password.						请输入密码.			
41		ID_TIPS6				128			Please remember the password entered.				请牢记密码.					
42		ID_TIPS7				128			Already exists. Please try again.					已经存在, 请重试.			
43		ID_TIPS8				128			The user does not exist.						用户不存在.	
44		ID_TIPS9				128			Please select a user.						请选择用户.				
45		ID_TIPS10				128			The follow characters must not be included in user name, please try again.		以下字符不能在名字中出现, 请重新输入.
46		ID_TIPS11				32			Please enter password.						请输入密码.
47		ID_TIPS12				32			Please confirm password.						请确认密码.
48		ID_RESET				16			Reset								重置
49		ID_TIPS13				128			Only modifying password can be valid for account 'admin'.		对于admin账号只能修改密码!
50		ID_TIPS14				128			User name or password can only be letter or number or '_'.		用户名或密码只允许字母, 数字, 下划线!
51		ID_TIPS15				32			Are you sure to modify		确认要修改
52		ID_TIPS16				32			's information?		信息?
53		ID_TIPS17				64			The password must contain at least six characters.		密码长度至少为6位!
54		ID_TIPS18				64			OK to delete?		你确认要删除
55		ID_ERROR12				64			Failed. Deleting 'engineer' is not allowed.			失败, 不允许删除用户engineer.		
56		ID_TIPS19				128			Only modifying password can be valid for account 'engineer'.		对于engineer账号只能修改密码!
#//changed by Frank Wu,1/N/15,20140527, for e-mail
57		ID_USER_CONTACT			16				E-Mail								电子邮箱
58		ID_USER_CONTACT1		16				E-Mail								电子邮箱
59		ID_TIPS20				128				Valid E-Mail should contain the char '@', eg. name@vertiv.com			有效电子邮件地址必须包含字符'@',例如:name@vertiv.com


[tmp.setting_ipv4.html:Number]
25

[tmp.setting_ipv4.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_IPV4					32			Ethernet				Ethernet
2		ID_SCUP_IP				32			IP Address				IP地址
3		ID_SCUP_MASK				32			Subnet Mask				子网掩码
4		ID_SCUP_GATEWAY				32			Default Gateway			默认网关
5		ID_ERROR0				32			Setting Failed.			设置失败.		
6		ID_ERROR1				32			Successful.				成功.	
7		ID_ERROR2				64			Failed. Incorrect input.				失败,不正确的输入.	
8		ID_ERROR3				64			Failed. Incomplete information.			失败,信息不完整.		
9		ID_ERROR4				64			Failed. No privilege.				失败,没有权限.		
10		ID_ERROR5				128			Failed. Controller is hardware protected.		失败,监控处于硬件保护状态.
11		ID_ERROR6				32			Failed. DHCP is ON.					失败,监控处于DHCP状态.
12		ID_TIPS0				32			Set Network Parameter														设置网络参数											
13		ID_TIPS1				128			Units IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171.					IP地址输入错误。\n正确格式为'nnn.nnn.nnn.nnn'，如10.75.14.171					
14		ID_TIPS2				128			Mask IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 255.255.0.0.						子网掩码输入错误。\n正确格式为'nnn.nnn.nnn.nnn'，如255.255.0.0				
15		ID_TIPS3				64			Units IP Address and Mask mismatch.													IP地址或子网掩码错误。请重新输入。							
16		ID_TIPS4				128			Gateway IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171. Enter 0.0.0.0 for no gateway	网关IP地址输入错误。\n正确格式为'nnn.nnn.nnn.nnn'，如10.75.14.254，0.0.0.0表示无网关.	
17		ID_TIPS5				128			Units IP Address, Gateway, Mask mismatch. Enter Address again.									IP地址、缺省网关和子网掩码不匹配.请重新输入.						
18		ID_TIPS6				64			Please wait. Controller is rebooting.												请等待,监控正在启动										
19		ID_TIPS7				128			Parameters have been modified.  Controller is rebooting...										参数被修改,监控正在启动									
20		ID_TIPS8				64			Controller homepage will be refreshed.												监控主页将被刷新										
21		ID_TIPS9				128			Confirm the change to the IP address?				确认修改IP地址?
22		ID_SAVE					16			Save						保存
23		ID_TIPS10				32			IP Address Error				IP地址错误
24		ID_TIPS11				32			Subnet Mask Error				子网掩码错误
25		ID_TIPS12				32			Default Gateway Error			默认网关错误
26		ID_USER					32			Users				账户
27		ID_IPV4_1					32			IPV4				IPV4
28		ID_IPV6					32			IPV6				IPV6
29		ID_HLMS_CONF				32			Monitor Protocol				监控协议
30		ID_SITE_INFO				32			Site Info				局站信息
31		ID_AUTO_CONFIG				32			Auto Config				自动配置
32		ID_OTHER				32			Alarm Report			告警上报
33		ID_NMS					32			SNMP				SNMP
34		ID_ALARM_SET				32			Alarms		告警
35		ID_CLEAR_DATA				16			Clear Data			清除数据
36		ID_RESTORE_DEFAULT			32			Restore Defaults		恢复默认配置
37		ID_DOWNLOAD_UPLOAD			32			SW Maintenance			软件维护
38		ID_HYBRID				32			Generator				油机
39		ID_DHCP					16			IPV4 DHCP				IPV4 DHCP
40		ID_IP1					32			Server IP				服务器IP
41		ID_TIPS13				16			Loading				加载
42		ID_TIPS14				16			Loading				加载
43		ID_TIPS15				32			failed, data format error!				失败, 数据格式错误!
44		ID_TIPS16				32			failed, please refresh the page!			失败, 请刷新页面!
45		ID_LANG					16			Language			语言
#//changed by Frank Wu,2/2/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
46		ID_SHUNT_SET			32				Shunt				分流器
#//changed by Frank Wu,1/N/35,20140527, for adding the the web setting tab page 'DI'
47		ID_DI_ALARM_SET			32				DI Alarms			DI告警
#//changed by Frank Wu,1/1/N,00000000, for add PowerSplit
48		ID_POWER_SPLIT_SET		32				Power Split			Power Split
49		ID_IPV6_1			16				IPV6				IPV6
50		ID_LOCAL_IP			32				Link-Local Address		本地链接地址
51		ID_GLOBAL_IP			32				IPV6 Address			IPV6地址
52		ID_SCUP_PREV			16				Subnet Prefix				子网前缀
53		ID_SCUP_GATEWAY1		16				Default Gateway				默认网关
54		ID_SAVE1			16				Save				保存
55		ID_DHCP1			16				IPV6 DHCP			IPV6 DHCP
56		ID_IP2				32				Server IP			服务器IP
57		ID_TIPS17			64				Please fill in the correct IPV6 address.	请填写正确的IPV6地址.
58		ID_TIPS18			128				Prefix can not be empty or beyond the range.	前缀不能为空或超出范围.
59		ID_TIPS19			64				Please fill in the correct IPV6 gateway.	请填写正确的IPV6网关.


[tmp.setting_time_sync.html:Number]
29

[tmp.setting_time_sync.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIME_HEAD				32			Time Synchronization		时间同步
2		ID_ZONE					64			Local Zone(for synchronization with time servers)				本地时区(用于与时间服务器同步)	
3		ID_GET_ZONE				32			Get Local Zone			获取本地时区
4		ID_SELECT1				64			Get time automatically from the following time servers.	从以下服务器自动获取时间.	
5		ID_PRIMARY_SERVER			32			Primary Server IP						主服务器IP			
6		ID_SECONDARY_SERVER			32			Secondary Server IP						从服务器IP			
7		ID_TIMER_INTERVAL			64			Interval to Adjust Time					校时间隔			
8		ID_SPECIFY_TIME				32			Specify Time						使用指定时间校时		
9		ID_GET_TIME				64			Get Local Time from Connected PC				从电脑获取本地时间			
10		ID_DATE_TIME				32			Date & Time							日期 & 时间				
11		ID_SUBMIT				16			Set								设置				
12		ID_ERROR0				32			Unknown error.						未知错误.			
13		ID_ERROR1				16			Successful.							成功.				
14		ID_ERROR2				128			Failed. Incorrect time setting.				失败,不正确的时间设置.		
15		ID_ERROR3				128			Failed. Incomplete information.				失败,输入信息不完整.		
16		ID_ERROR4				64			Failed. No privilege.					失败,没有权限.			
17		ID_ERROR5				128			Cannot be modified. Controller is hardware protected.	监控处于硬件保护状态,不能修改	
18		ID_MINUTE				16			min								分钟
19		ID_TIPS0				256			Primary Server IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'.	主服务器IP地址格式错误,格式应该如下所示:'nnn.nnn.nnn.nnn'.		
20		ID_TIPS1				256			Secondary Server IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'.	从服务器IP地址格式错误,格式应该如下所示:'nnn.nnn.nnn.nnn'.		
21		ID_TIPS2				128			Incorrect time interval. \nTime interval should be a positive integer.			时间间隔错误,应该为正整数.					
22		ID_TIPS3				128			Synchronizing time, please wait.								正在校时,请等待...						
23		ID_TIPS4				128			Incorrect format.  \nPlease enter the date as yyyy/mm/dd,e.g., 2000/09/30		日期格式错误,格式应该如下所示:2000/09/30/			
24		ID_TIPS5				128			Incorrect format.  \nPlease enter the time as  'hh:mm:ss', e.g., 8:23:08		时间格式错误,格式应该如下所示:'hh:mm:ss', 如 8:23:08		
25		ID_TIPS6				128			Date must be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.			日期应该在'1970/01/01 00:00:00' 和 '2038/01/01 00:00:00'之间.	
26		ID_TIPS7				128			The time server has been set. Time will be set by time server.				已经设置校时服务器, 此时间将会在校时时间到时被修改.
27		ID_TIPS8				128			Incorrect date and time format.								不正确的日期时间格式。
28		ID_TIPS9				128			Please clear the IP address before setting the time.						请先清除IP再设置时间.
29		ID_TIPS10				64			Time expired, please login again.						登录超时, 请重新登录.

[tmp.system_inventory.html:Number]
6

[tmp.system_inventory.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_INVENTORY				32			System Inventory			产品信息
2		ID_EQUIP				32			Equipment				设备
3		ID_MODEL				32			Product Model			产品型号
4		ID_REVISION				32			Hardware Revision			硬件版本
5		ID_SERIAL				32			Serial Number			序列号
6		ID_SOFT_REVISION			32			Software Revision			软件版本

[forgot_password.html:Number]
12

[forgot_password.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_FORGET_PASSWD			32			Forgot Password			忘记密码
2		ID_FIND_PASSWD				32			Find Password			找回密码
3		ID_INPUT_USER				32			Input User Name:			请输入用户名:
4		ID_FIND_PASSWD1				32			Find Password			找回密码
5		ID_RETURN				32			Back to Login Page			返回登录页面
6		ID_ERROR0				64			Unknown error.			位置错误!
7		ID_ERROR1				128			Password has been sent to appointed mailbox.	密码已经发送到指定邮箱!
8		ID_ERROR2				64			No such user.			无此用户!
9		ID_ERROR3				64			No email address.			未设邮箱地址!
10		ID_ERROR4				32			Input User Name			请输入用户名
11		ID_ERROR5				32			Fail.				失败!
12		ID_ERROR6				32			Error data format.		错误的数据格式!
13		ID_USERNAME				32			    User Name			用户名

[tmp.setting_other.html:Number]
61

[tmp.setting_other.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SMTP					16			SMTP				SMTP
2		ID_EMAIL				32			Email To				电子邮件发送到
3		ID_IP					32			Server IP				服务器IP
4		ID_PORT					32			Server Port				服务器端口
5		ID_AUTHORIY				32			Privilege					权限
6		ID_ENABLE				32			Enabled				使能
7		ID_DISABLE				32			Disabled				不使能
8		ID_ACCOUNT				32			SMTP Account			SMTP账号
9		ID_PASSWORD				32			SMTP Password			SMTP密码
10		ID_ALARM_REPORT				32			Alarm Report Level			告警上报级别
11		ID_OA					32			All Alarms				所有告警
12		ID_MA					32			Major and Critical Alarm				重要和紧急告警
13		ID_CA					32			Critical Alarm			紧急告警
14		ID_NONE					32			None				无
15		ID_SET					32			Set					设置
18		ID_LOAD					64			Loading data, please wait.		正在加载数据, 请稍候...
19		ID_VPN					32			Open VPN				是否开启VPN
20		ID_ENABLE1				32			Enabled				使能
21		ID_DISABLE1				32			Disabled			不使能
22		ID_VPN_IP				32			VPN IP				VPN IP
23		ID_VPN_PORT				32			VPN Port				VPN端口
24		ID_VPN					16			VPN					VPN
25		ID_LOAD1				64			Loading data, please wait.	正在加载数据, 请稍候...
26		ID_SET1					32			Set					设置
27		ID_HANDPHONE1				32			Cell Phone Number 1			手机号码1
28		ID_HANDPHONE2				32			Cell Phone Number 2			手机号码2
29		ID_HANDPHONE3				32			Cell Phone Number 3			手机号码3
30		ID_ALARMLEVEL				32			Alarm Report Level			告警上报级别
31		ID_OA1					64			All Alarms				所有告警
32		ID_MA1					64			Major and Critical Alarm				重要和紧急告警
33		ID_CA1					32			Critical Alarm			紧急告警
34		ID_NONE1				32			None				无
35		ID_SMS					16			SMS					SMS
36		ID_LOAD2				64			Loading data, please wait.	正在加载数据, 请稍候...
37		ID_SET2					32			Set					设置
38		ID_ERROR0				64			Unknown error.			位置错误
39		ID_ERROR1				32			Success				成功
40		ID_ERROR2				128			Failure, administrator privilege required.	失败,需要管理员权限!
41		ID_ERROR3				128			Failure, the user name already exists.	失败,用户名已存在!
42		ID_ERROR4				128			Failure, incomplete information.		失败,不完整信息!
43		ID_ERROR5				128			Failure, NSC is hardware protected.		失败,ACU处于硬件保护!
44		ID_ERROR6				64			Failure, incorrect password.	失败,密码错误!
45		ID_ERROR7				128			Failure, deleting Admin is not allowed.	失败,不允许删除管理员!
46		ID_ERROR8				64			Failure, not enough space.			失败,空间不足!
47		ID_ERROR9				128			Failure, user already existed.		失败,用户已存在!
48		ID_ERROR10				64			Failure, too many users.			失败,用户数超出上限!
49		ID_ERROR11				64			Failuer, user is not exist.			失败,用户不存在!
50		ID_TIPS1				64			Please select the user.			请选择需要修改的用户!
51		ID_TIPS2				64			Please fill in user name.			请填写用户名!
52		ID_TIPS3				64			Please fill in password.			请填写密码!
53		ID_TIPS4				64			Please confirm password.			请确认密码!
54		ID_TIPS5				64			Password not consistent.			密码不一致!
55		ID_TIPS6				64			Please input an email address in the form name@domain.			请输入正确的邮箱!
56		ID_TIPS7				64			Please input the right IP.			请输入正确的IP地址!
57		ID_TIPS8				16			Loading					加载
58		ID_TIPS9				64			Failure, please refresh the page.		失败, 请刷新页面!
59		ID_LOAD3				64			Loading data, please wait...		正在加载数据, 请稍候...
60		ID_LOAD4				64			Loading data, please wait...		正在加载数据, 请稍候...
61		ID_EMAIL1				32			Email From				电子邮件来自于
65		ID_TIPS14				64			Only numbers are permitted!			只能输入数字!
66		ID_TIPS10				16			Loading				装载
67		ID_TIPS12				64			failed, data format error!		失败, 数据格式错误!
68		ID_TIPS13				64			failed, please refresh the page!	失败, 请刷新页面!



[tmp.setting_hlms_configuration.html:Number]
64

[tmp.setting_hlms_configuration.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_ESRTIPS1				64			Range 0-255				范围 0-255
2		ID_ESRTIPS2				64			Range 0-600				范围 0-600
3		ID_ESRTIPS3				64			Main Report Phone Number		主上报电话号码
4		ID_ESRTIPS4				64			Second Report Phone Number		从上报电话号码
5		ID_ESRTIPS5				64			Callback Phone Number		回叫电话号码
6		ID_ESRTIPS6				64			Maximum Alarm Report Attempts	最大告警上报次数
7		ID_ESRTIPS7				64			Call Elapse Time			呼叫持续时间
8		ID_YDN23TIPS1				64			Range 0-5				范围 0-5
9		ID_YDN23TIPS2				64			Range 0-300				范围 0-300
10		ID_YDN23TIPS3				64			First Report Phone Number		第一上报电话号码
11		ID_YDN23TIPS4				64			Second Report Phone Number		第二上报电话号码
12		ID_YDN23TIPS5				64			Third Report Phone Number		第三上报电话号码
13		ID_YDN23TIPS6				64			Times of Dialing Attempt		回叫次数
14		ID_YDN23TIPS7				64			Interval between Two Dialings	回叫间隔时间
15		ID_HLMSERRORS1				32			Successful.				成功.
16		ID_HLMSERRORS2				32			Failed.				失败.
17		ID_HLMSERRORS3				64			Failed. ESR Service was exited.	失败,服务已经退出.
18		ID_HLMSERRORS4				64			Failed. Invalid parameter.		失败,无效的参数.
19		ID_HLMSERRORS5				64			Failed. Invalid data.		失败,无效的数据.
20		ID_HLMSERRORS6				128			Cannot be modified. Controller is hardware protected.		监控处于硬件保护状态,不能修改.
21		ID_HLMSERRORS7				128			Service is busy. Cannot change configuration at this time.		服务正在忙,目前不能修改配置.
22		ID_HLMSERRORS8				128			Non-shared port already occupied.		串口被占用,修改失败.
23		ID_HLMSERRORS9				64			Failed. No privilege.			失败,没有权限.
24		ID_EEM					16			EEM						EEM
25		ID_YDN23				16			YDN23					YDN23
26		ID_MODBUS				16			Modbus					Modbus
27		ID_RESET				64			Valid after Restart				重启后生效
28		ID_TYPE					32			Protocol Type				协议类型
29		ID_EEM1					16			EEM						EEM
30		ID_RSOC					16			RSOC					RSOC
31		ID_SOCTPE				16			SOC/TPE					SOC/TPE
32		ID_YDN231				16			YDN23					YDN23
33		ID_MEDIA				32			Protocol Media				协议媒质
34		ID_232					16			RS-232					RS-232
35		ID_MODEM				16			Modem					Modem
36		ID_ETHERNET				16			IPV4					IPV4
37		ID_ADRESS				32			Self Address				本机地址
38		ID_CALLBACKEN				32			Callback Enabled				回叫使能
39		ID_REPORTEN				32			Report Enabled				上报使能
40		ID_ALARMREP				32			Alarm Reporting				告警上报
41		ID_RANGE				32			Range 1-255					范围 1-255
42		ID_SOCID				32			SOCID					SOCID
43		ID_RANGE1				32			Range 1-20479				范围 1-20479
44		ID_RANGE2				32			Range 0-255					范围 0-255
45		ID_RANGE3				32			Range 0-600s				范围 0-600s
46		ID_IP1					32			Main Report IP				主上报IP
47		ID_IP2					32			Second Report IP				从上报IP
48		ID_SECURITYIP				32			Security Connection IP 1			安全连接IP1
49		ID_SECURITYIP2				32			Security Connection IP 2			安全连接IP2
50		ID_LEVEL				32			Safety Level				安全级别
51		ID_TIPS1				64			All commands are available.			所有命令均可用.
52		ID_TIPS2				128			Only read commands are available.		只有读取命令可用.
53		ID_TIPS3				128			Only the Call Back command is available.	只有回叫命令可用.
54		ID_TIPS4				64			Confirm to change the protocol to		确定切换协议为
55		ID_SAVE					32			Save					保存
56		ID_TIPS5				32			Switch Fail					设置失败
57		ID_CCID					16			CCID					CCID
58		ID_TYPE1				32			Protocol					协议
59		ID_TIPS6				32			Port Parameter				端口参数
60		ID_TIPS7				128			Port Parameters & Phone Number	端口参数&电话号码
61		ID_TIPS8				32			TCP/IP Port Number				TCP/IP端口号
62		ID_TIPS9				128			Set successfully, controller is restarting, please wait		设置成功, 监控重启中, 请等待
63		ID_TIPS10				64			Main Report Phone Number Error.			主上报电话号码错误!
64		ID_TIPS11				64			Second Report Phone Number Error.			从上报电话号码错误!
65		ID_ERROR10				32			Unknown error.			未知错误
66		ID_ERROR11				32			Successful.				成功.
67		ID_ERROR12				32			Failed.				失败.
68		ID_ERROR13				64			Insufficient privileges for this function.		失败,没有权限.
69		ID_ERROR14				32			No information to send.		没有控制信息发送
70		ID_ERROR15				64			Failed. Controller is hardware protected.		失败,监控处于硬件保护状态
71		ID_ERROR16				64			Time expired. Please login again.				超时.请重新登录.
72		ID_TIPS12				64			Network Error		网络错误
73		ID_TIPS13				64			CCID not recognized. Please enter a number.		CCID错误, 请输入数字.
74		ID_TIPS14				64			CCID					CCID
75		ID_TIPS15				64			Input Error					输入错误
76		ID_TIPS16				64			SOCID not recognized. Please enter a number.		SOCID错误, 请输入数字.
77		ID_TIPS17				64			SOCID					SOCID
78		ID_TIPS18				64			Cannot be zero. Please enter a different number.		不能为0.
79		ID_TIPS19				64			SOCID			SOCID
80		ID_TIPS20				64			Input error.		输入错误.
81		ID_TIPS21				64			Unable to recognize maximum number of alarms.		最大告警上报次数错误.
82		ID_TIPS22				64			Unable to recognize maximum number of alarms.		最大告警上报次数错误.
83		ID_TIPS23				64			Maximum call elapse time is error.		最大呼叫持续时间错误.
84		ID_TIPS24				64			Maximum call elapse time is input error.		最大呼叫持续时间错误.
85		ID_TIPS25				64			Report IP not recognized.		上报IP错误.
86		ID_TIPS26				64			Report IP not recognized.		上报IP错误.
87		ID_TIPS27				64			Security IP not recognized.	安全IP错误.
88		ID_TIPS28				64			Security IP not recognized.	安全IP错误.
89		ID_TIPS29				64			Port input error.		端口输入错误.
90		ID_TIPS30				64			Port input error.		端口输入错误.
91		ID_IPV6					16			IPV6			IPV6
92		ID_TIPS31				32			[IPV6 Addr]:Port		[IPV6 地址]:端口
93		ID_TIPS32				32			[IPV6 Addr]:Port		[IPV6 地址]:端口
94		ID_TIPS33				32			[IPV6 Addr]:Port		[IPV6 地址]:端口
95		ID_TIPS34				32			[IPV6 Addr]:Port		[IPV6 地址]:端口
96		ID_TIPS35				32			IPV4 Addr:Port		IPV4 地址:端口
97		ID_TIPS36				32			IPV4 Addr:Port		IPV4 地址:端口
98		ID_TIPS37				32			IPV4 Addr:Port		IPV4 地址:端口
99		ID_TIPS38				32			IPV4 Addr:Port		IPV4 地址:端口
100		ID_TIPS39				64			Callback Phone Number Error.		回叫电话号码错误.
101		ID_TL1					16			TL1					TL1
102		ID_PORT_ACT				32			Port Activation			端口激活
103		ID_DISABLE				16			Disabled				禁止
104		ID_ENABLE				16			Enabled				使能
105		ID_ETHERNET1				16			IPV4				IPV4
106		ID_IPV6_1				16			IPV6			IPV6
107		ID_TIPS40				16			Access Port				端口参数
108		ID_RANGE4				32			Range 1024-65534					范围 1024-65534
109		ID_TIPS41				32			Port Keep-alive			保持端口激活
110		ID_DISABLE1				16			Disabled				禁止
111		ID_ENABLE1				16			Enabled				使能
112		ID_TIPS42				32			Session Timeout			连接超时
113		ID_RANGE5				32			Range 0-1440 minutes		范围0-1440分钟
114		ID_TIPS43				32			Auto Login User			自动登录用户
115		ID_TIPS44				32			System Identifier			系统标识码
116		ID_MEDIA1				32			Protocol Media			协议媒质
117		ID_NA					16			None				无
118		ID_TIPS45				32			Access Port Error			端口参数错误
119		ID_TIPS46				32			Session Timeout Error		连接超时错误
120		ID_TIPS47				32			System Identifier Error		系统标识码错误
121		ID_TL1_ERRORS1				32			Successful.				成功.
122		ID_TL1_ERRORS2				32			Configuration fail.			配置失败
123		ID_TL1_ERRORS3				64			Failed. Service was exited.		失败,服务已经退出.
124		ID_TL1_ERRORS4				32			Invalid parameters.			不合法的参数.
125		ID_TL1_ERRORS5				32			Invalid parameters.			不合法的参数.
126		ID_TL1_ERRORS6				64			Fail to write to Flash.		写Flash失败.
127		ID_TL1_ERRORS7				32			Service is busy.			服务正忙.
128		ID_TL1_ERRORS8				64			Port has already been occupied.	端口已被占用.
129		ID_TL1_ERRORS9				64			Port has already been occupied.	端口已被占用.
130		ID_TL1_ERRORS10				64			Port has already been occupied.	端口已被占用.
131		ID_TL1_ERRORS11				64			Port has already been occupied.	端口已被占用.
132		ID_TL1_ERRORS12				64			The TL1 feature is not available.	TL1不可用.
136		ID_NA2					16			None				无


[tmp.setting_nms.html:Number]
64

[tmp.setting_nms.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TRAP_LEVEL1				16			Not Used				未使用
2		ID_TRAP_LEVEL2				16			All Alarms				所有告警
3		ID_TRAP_LEVEL3				16			Major Alarms			重要告警
4		ID_TRAP_LEVEL4				16			Critical Alarms			紧急告警
5		ID_NMS_TRAP				32			Accepted Trap Level			Trap级别
6		ID_SET_TRAP				16			Set					设置
7		ID_NMSV2_CONF				32			NMSV2 Configuration			NMSV2配置
8		ID_NMS_IP				32			NMS IP				NMS IP
9		ID_NMS_PUBLIC				32			Public Community			公有通讯字
10		ID_NMS_PRIVATE				32			Private Community			私有通讯字
11		ID_TRAP_ENABLE				16			Trap Enabled				Trap使能
12		ID_DELETE				16			Delete				删除
13		ID_NMS_IP1				32			NMS IP				NMS IP
14		ID_NMS_PUBLIC1				32			Public Community			公有通讯字
15		ID_NMS_PRIVATE1				32			Private Community			私有通讯字
16		ID_TRAP_ENABLE1				16			Trap Enabled				Trap使能
17		ID_DISABLE				16			Disabled				禁止
18		ID_ENABLE				16			Enabled				使能
19		ID_ADD					16			Add					增加
20		ID_MODIFY				16			Modify				修改
21		ID_RESET				16			Reset				重置
22		ID_NMSV3_CONF				32			NMSV3 Configuration			NMSV3配置
23		ID_NMS_TRAP_LEVEL0			32			NoAuthNoPriv			无认证无加密
24		ID_NMS_TRAP_LEVEL1			32			AuthNoPriv				有认证无加密
25		ID_NMS_TRAP_LEVEL2			32			AuthPriv				有认证有加密
26		ID_NMS_USERNAME				32			User Name				用户名
27		ID_NMS_DES				32			Priv Password DES			私钥密码DES
28		ID_NMS_MD5				32			Auth Password MD5			认证密码MD5
29		ID_V3TRAP_ENABLE			16			Trap Enabled				Trap使能
30		ID_NMS_TRAP_IP				16			Trap IP				Trap IP
31		ID_NMS_V3TRAP_LEVE			32			Trap Security Level			Trap安全等级
32		ID_V3DELETE				16			Delete				删除
33		ID_NMS_USERNAME1			32			User Name				用户名
34		ID_NMS_DES1				32			Priv Password DES			私钥密码DES
35		ID_NMS_MD51				32			Auth Password MD5			认证密码MD5
36		ID_V3TRAP_ENABLE1			16			Trap Enabled				Trap使能
37		ID_NMS_TRAP_IP1				16			Trap IP				Trap IP
38		ID_NMS_V3TRAP_LEVE1			32			Trap Security Level			Trap安全等级
39		ID_DISABLE1				16			Disabled				禁止
40		ID_ENABLE1				16			Enabled				使能
41		ID_ADD1					16			Add					增加
42		ID_MODIFY1				16			Modify				修改
43		ID_RESET1				16			Reset				重置
44		ID_ERROR0				32			Unknown error.			未知错误.
45		ID_ERROR1				32			Successful.				成功.
46		ID_ERROR2				64			Failed. Controller is hardware protected.	监控处于硬件保护状态, 不能修改.
47		ID_ERROR3				64			SNMPV3 functions are not enabled.			无SNMPV3功能.
48		ID_ERROR4				64			Insufficient privileges for this function.		失败, 权限不足.
49		ID_ERROR5				128			Failed. Maximum number (16 for SNMPV2 accounts, 5 for SNMPV3 accounts) exceeded.	失败, 超出最大数量(SNMPV2账户为16个, SNMPV3账户为5个).
50		ID_TIPS1				64			Please select an NMS before continuing.		在此操作前请先选择一个账户!
51		ID_TIPS2				64			IP address is not valid.		请输入正确的IP.
52		ID_TIPS3				128			Only numbers, letters and '_' are permitted, can't be over 16 characters or blanks included.		只允许数字, 字母和下划线, 不超过16个字符, 且不能为空.
53		ID_TIPS4				128			Only numbers, letters and '_' are permitted, the length of characters is between 8 and 16.	只允许数字, 字母和下划线, 至少8个字符, 且不能超过16字符.
54		ID_NMS_PUBLIC2				32			Public Community			公有通讯字
55		ID_NMS_PRIVATE2				32			Private Community			私有通讯字
56		ID_NMS_USERNAME2			32			User Name				用户名
57		ID_NMS_DES2				32			Priv Password DES			私钥密码DES
58		ID_NMS_MD52				32			Auth Password MD5			认证密码MD5
59		ID_LOAD					16			Loading				加载
60		ID_LOAD1				16			Loading				加载
61		ID_TIPS5				64			Failed, data format error.		失败, 数据格式错误!
62		ID_TIPS6				64			Failed. Please refresh the page.	失败, 请刷新页面!
63		ID_DISABLE2				16			Disabled				禁止
64		ID_ENABLE2				16			Enabled				使能
65		ID_DISABLE3				16			Disabled				禁止
66		ID_ENABLE3				16			Enabled				使能
67		ID_IPV6					64			IPV6 address is not valid.		IPV6地址不合法.
68		ID_IPV6_ADDR				64			IPV6				IPV6
69		ID_IPV6_ADDR1				64			IPV6				IPV6


[tmp.setting_system_rect.html:Number]
64

[tmp.setting_system_rect.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				信号
2		ID_VALUE				32			Value				值
3		ID_TIME					32			Time Last Set			上次设置时间
4		ID_SET_VALUE				32			Set Value				设置值
5		ID_SET					32			Set					设置
6		ID_SET1					32			Set					设置

[tmp.system_dg.html:Number]
64

[tmp.system_dg.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				信号
2		ID_VALUE				32			Value				值
3		ID_DG					32			DG					油机
4		ID_SIGNAL1				32			Signal				信号
5		ID_VALUE1				32			Value				值
6		ID_NO_DATA				32			No Data				无数据


[tmp.system_ac.html:Number]
64

[tmp.system_ac.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				信号
2		ID_VALUE				32			Value				值
3		ID_SIGNAL1				32			Signal				信号
4		ID_VALUE1				32			Value				值
5		ID_SMAC					32			SMAC				SMAC
6		ID_AC_METER				32			AC Meter				交流表
7		ID_NO_DATA				32			No Data				无数据
8		ID_AC					32			AC					交流

[tmp.system_smio.html:Number]
64

[tmp.system_smio.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				信号
2		ID_VALUE				32			Value				值
3		ID_SIGNAL1				32			Signal				信号
4		ID_VALUE1				32			Value				值
5		ID_NO_DATA				32			No Data				无数据

[tmp.setting_alarm_equipment.html:Number]
1

[tmp.setting_alarm_equipment.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS					128			Please select equipment type	请选择设备类型

[tmp.setting_alarm_content.html:Number]
20

[tmp.setting_alarm_content.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS1					32		New Alarm Level			新告警级别
2		ID_TIPS2					32		Please select			请选择
3		ID_NA						16		NA					无告警
4		ID_OA						16		OA					一般告警
5		ID_MA						16		MA					重要告警
6		ID_CA						16		CA					紧急告警
7		ID_TIPS3					32		New Relay Number			新继电器号
8		ID_TIPS4					32		Please select			请选择
9		ID_SET						16		Set					设置
10		ID_INDEX					16		Index				序号
11		ID_NAME						16		Name				名称
12		ID_LEVEL					32		Alarm Level				告警级别
13		ID_ALARMREG					32		Relay Number			继电器号
14		ID_MODIFY					32		Modify				修改
15		ID_NA1						16		NA					无告警
16		ID_OA1						16		OA					一般告警
17		ID_MA1						16		MA					重要告警
18		ID_CA1						16		CA					紧急告警
19		ID_MODIFY1					32		Modify				修改
20		ID_TIPS5					32		No Data				无数据
21		ID_NA2						16		None				无
22		ID_NA3						16		None				无
23		ID_NA4						16		NA					NA

[tmp.setting_converter.html:Number]
6

[tmp.setting_converter.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				信号
2		ID_VALUE				32			Value				值
3		ID_TIME					32			Time Last Set			上次设置时间
4		ID_SET_VALUE				32			Set Value				设置值
5		ID_SET					32			Set					设置
6		ID_SET1					32			Set					设置
7		ID_CONVERTER				32			Converter				DC/DC模块
8		ID_NO_DATA				16			No data.				无数据!

[tmp.setting_clear_data.html:Number]
14

[tmp.setting_clear_data.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_CLEAR				16			Clear Data				清除数据
2		ID_HISTORY_ALARM			64			Alarm History			历史告警
3		ID_HISTORY_DATA				64			Data History			历史数据
4		ID_HISTORY_CONTROL			64			Event Log				历史事件
5		ID_HISTORY_BATTERY			64			Battery Test Log			电池测试记录
6		ID_HISTORY_DISEL_TEST			64			Diesel Test Log			油机测试记录
7		ID_CLEAR1				16			Clear				清除
8		ID_ERROR0				64			Failed to clear data.				清除数据失败.			
9		ID_ERROR1				64			Cleared.						成功被清除.			
10		ID_ERROR2				64			Unknown error.					未知错误.			
11		ID_ERROR3				128			Failed. No privilege.				失败, 没有权限.			
12		ID_ERROR4				64			Failed to communicate with the controller.		与监控通讯失败.			
13		ID_ERROR5				64			Failed. Controller is hardware protected.		失败, 监控处于硬件保护状态.	
14		ID_TIPS1				64			Clearing...please wait.				清除中...请稍候.

[tmp.setting_upload_download.html:Number]
14

[tmp.setting_upload_download.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_HEAD					128			Restore Factory Defaults						恢复默认配置					
2		ID_TIPS0				128			Restore default configuration? The system will reboot.		恢复默认配置, 系统将重启.				
3		ID_RESTORE_DEFAULT			64			Restore Defaults							恢复默认配置					
4		ID_TIPS1				128			Restore default config will cause system to reboot, are you sure?	恢复默认配置, 系统将重启. 您确信恢复默认配置?	
5		ID_TIPS2				128			Cannot be restored. Controller is hardware protected.		监控处于硬件保护状态, 不能恢复默认配置.			
6		ID_TIPS3				128			Are you sure you want to reboot the controller?			您确信重启监控?						
7		ID_TIPS4				128			Failed. No privilege.						失败, 没有权限.						
8		ID_START_SCUP				32			Reboot controller							重启监控					
9		ID_TIPS5				64			Restoring defaults...please wait.					正在恢复默认配置, 请等待.						
10		ID_TIPS6				64			Rebooting controller...please wait.					正在重启监控, 请等待.						
11		ID_HEAD1				32			Upload/Download							上传/下载
12		ID_TIPS7				128			Upload/Download needs to stop the Controller. Do you want to stop the Controller?		上传下载需要关闭监控, 关闭监控吗?
13		ID_CLOSE_SCUP				16			Stop Controller				关闭监控
14		ID_HEAD2				32			Upload/Download File			上传/下载文件
15		ID_TIPS8				256			Caution: Only the file SettingParam.run or files with extension .tar or .tar.gz can be downloaded. If the downloaded file is NOT correct, the Controller will run abnormally. You must hit the START CONTROLLER button before leaving this screen.		注意: 只有SettingParam.run文件, 或者扩展名为tar或tar.gz的文件才能下载. 如果下载了不正确的文件, 监控将运行不正常. 下载后请手动重启监控.
16		ID_FILE1				32			Select File					选择文件
17		ID_TIPS9				32			Browse...					浏览...
18		ID_DOWNLOAD				64			Download to Controller			下载到监控
19		ID_CONFIG_TAR				32			Configuration Package			配置包
20		ID_LANG_TAR				32			Language Package				语言包
21		ID_UPLOAD				64			Upload to Computer				上传到电脑
22		ID_STARTSCUP				64			Start Controller				启动监控
23		ID_STARTSCUP1				64			Start Controller				启动监控
24		ID_TIPS10				64			Stop controller...please wait.		监控停止中...请等待.
25		ID_TIPS11				64			A file name is required.		文件名不能为空.
26		ID_TIPS12				64			Are you sure you want to download?		确信下载文件?
27		ID_TIPS13				64			Downloading...please wait.			下载中...请稍候.
28		ID_TIPS14				128			Incorrect file type or file name contains invalid characters.			错误的文件格式或文件名包含非法字符.
29		ID_TIPS15				32			please wait...				请等待...
30		ID_TIPS16				128			Are you sure you want to start the controller?		确信重启监控?
31		ID_TIPS17				64			Controller is rebooting...			监控正在启动...
32		ID_FILE0				32			File in controller			监控文件
33		ID_CLOSE_ACU				32			Auto Config												自动配置								
34		ID_ERROR0				32			Unknown error.												未知错误.									
35		ID_ERROR1				128			Auto configuration started. Please wait.									自动配置开始，请等待.							
36		ID_ERROR2				64			Failed to get.												获取失败.									
37		ID_ERROR3				64			Insufficient privileges to stop the controller.								权限不够.								
38		ID_ERROR4				64			Failed to communicate with the controller.									与监控通讯失败.								
39		ID_AUTO_CONFIG1				256			The controller will auto configure then restart. Please wait 2-5 minutes.	监控将要进入自动配置状态, 监控将会重启, 请稍等(2-5分钟)后重新登录.	
40		ID_TIPS					128			This function will automatically configure SM units and modbus devices that have been connected to RS485 bus.	此功能将自动配置接到485总线上的SM单元和modbus设备.									
41		ID_HEAD3				32			Auto Config											自动配置
42		ID_TIPS18				32			Confirm auto configuration?										确定要自动配置吗?
50		ID_TIPS3				128			Set successfully, controller is restarting, please wait		设置成功, 监控重启中, 请等待
51		ID_TIPS4				16			seconds.				秒!
52		ID_TIPS5				64			Returning to login page. Please wait.		返回到登录页面, 请等待...
59		ID_ERROR5				32			Unknown error.												未知错误.											
60		ID_ERROR6				128			Controller was stopped successfully. You can upload/download the file.					监控成功关闭, 现在可以上传下载文件了.							
61		ID_ERROR7				64			Failed to stop the controller.										关闭监控失败											
62		ID_ERROR8				64			Insufficient privileges to stop the controller.								权限不够.										
63		ID_ERROR9				64			Failed to communicate with the controller.									与监控通讯失败				
64		ID_GET_PARAM				32			Retrieve SettingParam.tar											获取设置参数
65		ID_TIPS19				128			Retrieve the current settings of the controller's adjustable parameters.					从监控获取设置信号当前值.
66		ID_RETRIEVE				32			Retrieve File												获取
67		ID_ERROR10				32			Unknown error.												未知错误.	
68		ID_ERROR11				128			Retrieval successful.											获取成功.	
69		ID_ERROR12				64			Failed to get.												获取失败.	
70		ID_ERROR13				64			Insufficient privileges to stop the controller.									权限不够.	
71		ID_ERROR14				64			Failed to communicate with the controller.									与监控通讯失败.
72		ID_TIPS20				32			Please wait...					请稍候...
73		ID_DOWNLOAD_ERROR0			32			Unknown error.								未知错误.		
74		ID_DOWNLOAD_ERROR1			64			File downloaded successfully.						下载文件成功.		
75		ID_DOWNLOAD_ERROR2			64			Failed to download file.							下载文件失败.		
76		ID_DOWNLOAD_ERROR3			64			Failed to download, the file is too large.				文件太大, 下载文件失败.	
77		ID_DOWNLOAD_ERROR4			64			Failed. No privileges.							失败, 没有权限.		
78		ID_DOWNLOAD_ERROR5			64			Controller started successfully.						监控启动成功.	
79		ID_DOWNLOAD_ERROR6			64			File downloaded successfully.						下载文件成功.		
80		ID_DOWNLOAD_ERROR7			64			Failed to download file.							下载文件失败.		
81		ID_DOWNLOAD_ERROR8			64			Failed to upload file.							上传文件失败.		
82		ID_DOWNLOAD_ERROR9			64			File downloaded successfully.						下载文件成功.		
83		ID_DOWNLOAD_ERROR10			64			Failed to upload file. Hardware is protected.				下载文件失败, 硬件保护.

[tmp.setting_power_system.html:Number]
14

[tmp.setting_power_system.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				信号
2		ID_VALUE				32			Value				值
3		ID_TIME					32			Time Last Set			上次设置时间
4		ID_SET_VALUE				32			Set Value				设置值
5		ID_SET					32			Set					设置
6		ID_SET1					32			Set					设置
7		ID_NO_DATA				16			No data.				无数据!
#8		ID_EIB					32			EIB Equipment				EIB设备
#9		ID_SMDU					32			SMDU Equipment				SMDU设备
#10		ID_LVD_GROUP				32			LVD Group				LVD组
#changed by Frank Wu,1/1/32,20140312, for adding new web pages to the tab of "power system"
11		ID_SYSTEM_GROUP				32				Power System			电源系统
12		ID_AC_GROUP					32				AC Group				AC组
13		ID_AC_UNIT					32				AC Equipment			AC设备
14		ID_ACMETER_GROUP			32				ACMeter Group			ACMeter组
15		ID_ACMETER_UNIT				32				ACMeter Equipment		ACMeter设备
16		ID_DC_UNIT					32				DC Equipment			DC设备
17		ID_DCMETER_GROUP			32				DCMeter Group			DCMeter组
18		ID_DCMETER_UNIT				32				DCMeter Equipment		DCMeter设备
19		ID_LVD_GROUP				32				LVD Group				LVD组
20		ID_DIESEL_GROUP				32				Diesel Group			Diesel组
21		ID_DIESEL_UNIT				32				Diesel Equipment		Diesel设备
22		ID_FUEL_GROUP				32				Fuel Group				Fuel组
23		ID_FUEL_UNIT				32				Fuel Equipment			Fuel设备
24		ID_IB_GROUP					32				IB Group				IB组
25		ID_IB_UNIT					32				IB Equipment			IB设备
26		ID_EIB_GROUP				32				EIB Group				EIB组
27		ID_EIB_UNIT					32				EIB Equipment			EIB设备
28		ID_OBAC_UNIT				32				OBAC Equipment			OBAC设备
29		ID_OBLVD_UNIT				32				OBLVD Equipment			OBLVD设备
30		ID_OBFUEL_UNIT				32				OBFuel Equipment		OBFuel设备
31		ID_SMDU_GROUP				32				SMDU Group				SMDU组
32		ID_SMDU_UNIT				32				SMDU Equipment			SMDU设备
33		ID_SMDUP_GROUP				32				SMDUP Group				SMDUP组
34		ID_SMDUP_UNIT				32				SMDUP Equipment			SMDUP设备
35		ID_SMDUH_GROUP				32				SMDUH Group				SMDUH组
36		ID_SMDUH_UNIT				32				SMDUH Equipment			SMDUH设备
37		ID_SMBRC_GROUP				32				SMBRC Group				SMBRC组
38		ID_SMBRC_UNIT				32				SMBRC Equipment			SMBRC设备
39		ID_SMIO_GROUP				32				SMIO Group				SMIO组
40		ID_SMIO_UNIT				32				SMIO Equipment			SMIO设备
41		ID_SMTEMP_GROUP				32				SMTemp Group			SMTemp组
42		ID_SMTEMP_UNIT				32				SMTemp Equipment		SMTemp设备
43		ID_SMAC_UNIT				32				SMAC Equipment			SMAC设备
44		ID_SMLVD_UNIT				32				SMDU-LVD Equipment			SMDU-LVD设备
45		ID_LVD3_UNIT				32				LVD3 Equipment			LVD3设备
46		ID_SELECT_TYPE				48				Please select equipment type	请选择设备类型
47		ID_EXPAND					32				Expand					展开
48		ID_COLLAPSE					32				Collapse				收缩
49		ID_OBBATTFUSE_UNIT			32				OBBattFuse Equipment	OBBattFuse设备
50		ID_NARADA_BMS_UNIT			32				BMS		BMS
51		ID_NARADA_BMS_GROUP			32				BMS Group		BMS组


[tmp.setting_language.html:Number]
14

[tmp.setting_language.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS1				16			Local Language			本地语言
2		ID_TIPS2				64			Please select language		请选择语言
3		ID_GERMANY_TIP				64			Kontroller wird mit deutscher Sprache neustarten.					Kontroller wird mit deutscher Sprache neustarten.
4		ID_GERMANY_TIP1				128			Ausgewählte Sprache identisch mit lokaler Sprache, keine Änderung notwendig.	Ausgewählte Sprache identisch mit lokaler Sprache, keine Änderung notwendig.
5		ID_SPAISH_TIP				64			La controladora se va a reiniciar en Español.					La controladora se va a reiniciar en Español.
6		ID_SPAISH_TIP1				64			El idioma seleccionado es el mismo. No se necesita cambiar.				El idioma seleccionado es el mismo. No se necesita cambiar.
7		ID_FRANCE_TIP				64			Le controleur  redemarrera en Français.						Le controleur  redemarrera en Français.
8		ID_FRANCE_TIP1				128			La langue selectionnée est la langue locale, pas besoin de changer.			La langue selectionnée est la langue locale, pas besoin de changer.
9		ID_ITALIAN_TIP				64			Il controllore ripartirà in Italiano.						Il controllore ripartirà in Italiano.
10		ID_ITALIAN_TIP1				128			La Lingua selezionata è lo stessa della lingua locale, non serve cambiare.		La Lingua selezionata è lo stessa della lingua locale, non serve cambiare.
11		ID_RUSSIAN_TIP				128			Контроллер будет перезагружен на русском языке.					Контроллер будет перезагружен на русском языке.
12		ID_RUSSIAN_TIP1				128			Выбранный язык такой же, как и локальный, без необходимости изменения.		Выбранный язык такой же, как и локальный, без необходимости изменения.
13		ID_TURKEY_TIP				64			Controller will restart in Turkish.							Controller will restart in Turkish.
14		ID_TURKEY_TIP1				64			Selected language is same as local language, no need to change.			Selected language is same as local language, no need to change.
15		ID_TW_TIP				64			监控将会重启, 更改成\"繁体中文\".				监控将会重启, 更改成\"繁体中文\".
16		ID_TW_TIP1				64			选择的语言与本地语言一致, 无需更改.				选择的语言与本地语言一致, 无需更改.
17		ID_ZH_TIP				64			监控将会重启, 更改成\"简体中文\".				监控将会重启, 更改成\"简体中文\".
18		ID_ZH_TIP1				64			选择的语言与本地语言一致, 无需更改.				选择的语言与本地语言一致, 无需更改.
19		ID_SET					32			Set					设置

[tmp.system_udef.html:Number]
14

[tmp.system_udef.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_NO_DATA				16			No data				无数据
2		ID_USER_DEF				32			User Define				用户定义
3		ID_SIGNAL				32			Signal				信号
4		ID_VALUE				32			Value				值
5		ID_SIGNAL1				32			Signal				信号
6		ID_VALUE1				32			Value				值

[tmp.udef_setting.html:Number]
14

[tmp.udef_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_USER_SET				16			User Config				用户配置
2		ID_SIGNAL				32			Signal				信号
3		ID_VALUE				32			Value				值
4		ID_TIME					32			Time Last Set			上次设置时间
5		ID_SET_VALUE				32			Set Value				设置值
6		ID_SET					32			Set					设置
7		ID_SET1					32			Set					设置
8		ID_NO_DATA				16			No data				无数据

[tmp.udef_setting_2.html:Number]
14

[tmp.udef_setting_2.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_USER_SET				16			User Config2			用户配置2
2		ID_SIGNAL				32			Signal				信号
3		ID_VALUE				32			Value				值
4		ID_TIME					32			Time Last Set			上次设置时间
5		ID_SET_VALUE				32			Set Value				设置值
6		ID_SET					32			Set					设置
7		ID_SET1					32			Set					设置
8		ID_NO_DATA				16			No data				无数据

[tmp.udef_setting_3.html:Number]
14

[tmp.udef_setting_3.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_USER_SET				16			User Config3			用户配置3
2		ID_SIGNAL				32			Signal				信号
3		ID_VALUE				32			Value				值
4		ID_TIME					32			Time Last Set			上次设置时间
5		ID_SET_VALUE				32			Set Value				设置值
6		ID_SET					32			Set					设置
7		ID_SET1					32			Set					设置
8		ID_NO_DATA				16			No data				无数据


#//changed by Frank Wu,3/3/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
[tmp.setting_mppt.html:Number]
14

[tmp.setting_mppt.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL						32					Signal				信号
2		ID_VALUE						32					Value				值
3		ID_TIME							32					Time Last Set		上次设置时间
4		ID_SET_VALUE					32					Set Value			设置值
5		ID_SET							32					Set					设置
6		ID_SET1							32					Set					设置
7		ID_NO_DATA						32					No Data				无数据
8		ID_MPPT							32					Solar				太阳能

[tmp.shunt_setting.html:Number]
14

[tmp.shunt_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL						32					Signal				信号
2		ID_VALUE						32					Value				值
3		ID_TIME							32					Time Last Set		上次设置时间
4		ID_SET_VALUE					32					Set Value			设置值
5		ID_SET							32					Set					设置
6		ID_SET1							32					Set					设置
7		ID_NO_DATA						32					No Data				无数据
8		ID_SHUNT_SET					32					Shunt				分流器
9		ID_EQUIP						32					Equipment			设备名


#//changed by Frank Wu,4/4/30,20140527, for add single converter and single solar settings pages
[tmp.single_converter_setting.html:Number]
6

[tmp.single_converter_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL					32						Signal				信号
2		ID_VALUE					32						Value				值
3		ID_TIME						32						Time Last Set		上次设置时间
4		ID_SET_VALUE				32						Set Value			设置值
5		ID_SET						32						Set					设置
6		ID_SET1						32						Set					设置
7		ID_RECT						32						Converter			DC/DC模块
8		ID_NO_DATA					16						No data.			无数据!

[tmp.single_solar_setting.html:Number]
6

[tmp.single_solar_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL					32						Signal				信号
2		ID_VALUE					32						Value				值
3		ID_TIME						32						Time Last Set		上次设置时间
4		ID_SET_VALUE				32						Set Value			设置值
5		ID_SET						32						Set					设置
6		ID_SET1						32						Set					设置
7		ID_RECT						32						Solar Conv			太阳能模块
8		ID_NO_DATA					16						No data.			无数据!

[tmp.cabinet_branch.html:Number]
6

[tmp.cabinet_branch.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS1				32			Please select cabinet		请选择cabinet
2		ID_TIPS2				32			Please select DU			请选择DU

[tmp.cabinet_map.html:Number]
6

[tmp.cabinet_map.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_MAJOR				16			Major				严重
2		ID_OBS					16			Observation				一般
3		ID_NORMAL				16			Normal				正常
4		ID_NO_DATA				16			No Data				无数据
5		ID_SELECT				32			Please select			请选择
6		ID_NO_DATA1				16			No Data				无数据


[tmp.cabinet_set.html:Number]
6

[tmp.cabinet_set.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SET					16			Set					设置
2		ID_BRANCH				16			Branches				支路
3		ID_TIPS					16			is selected				被选中
4		ID_RESET				32			Reset				重置
5		ID_TIPS1				32			Show Designation			显示分配
6		ID_TIPS2				32			Close Detail			关闭显示
9		ID_TIPS3				32			Success to select.			选择成功.
10		ID_TIPS4				32			Click to delete			点击删除
11		ID_TIPS5				128			Please select a cabinet to set.	请选择一个要设置的cabinet.
12		ID_TIPS6				64			This Cabinet has been set		此Cabinet已经设置了
13		ID_TIPS7				64			branches, confirm to cancel?	分支, 确定取消设置?
14		ID_TIPS8				32			Please select			请选择
15		ID_TIPS9				32			no allocation			未分配
16		ID_TIPS10				128			Please add more branches for the selected cabinet.		请对选中的Cabinet增加更多的支路.
17		ID_TIPS11				64			Success to deselect.		成功取消选择.
18		ID_TIPS12				64			Reset cabinet...please wait.	重置Cabinet, 请等待.
19		ID_TIPS13				64			Reset Successful, please wait.	重置成功, 请等待.
20		ID_ERROR1				16			Failed.				失败.
21		ID_ERROR2				16			Successful.				成功.
22		ID_ERROR3				32			Insufficient privileges.		权限不够.
23		ID_ERROR4				64			Failed. Controller is hardware protected.		失败,监控处于硬件保护状态
24		ID_TIPS14				32			Exceed 20 branches.			超过20路支路.
25		ID_TIPS15				128			More than 16 characters, or you have input invalid characters.		超过16个字符, 或输入了非法字符.
26		ID_TIPS16				256			Alarm level value can only be integer or just a decimal, and Alarm Level2 must be larger than or equal to Alarm Level1.	告警门限值只能是整数或带一位小数, 且告警门限2必须大于等于告警门限1.
27		ID_TIPS17				32			Show Parameter			显示参数
28		ID_SET1					16			Designate				分配
29		ID_SET2					16			Set					设置
30		ID_NAME					16			Name				名字
31		ID_LEVEL1				32			Alarm Level1			告警门限1
32		ID_LEVEL2				32			Alarm Level2			告警门限2
33		ID_RATING				32			Rating Current			额定电流
34		ID_TIPS18				128			Rating current must be from 0 to 10000, and can only have one decimal at most.	额定电流必须从0到10000, 并且最多只能有一位小数.
35		ID_SET3					16			Set					设置
36		ID_TIPS19				64			Confirm to reset the current cabinet?	确认重置当前Cabinet吗?

#//changed by Frank Wu,2/N/35,20140527, for adding the the web setting tab page 'DI'
[tmp.setting_di_content.html:Number]
6

[tmp.setting_di_content.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS1					32			Signal Full Name				信号名全称
2		ID_TIPS2					32			Signal Abbr Name			信号名简称
3		ID_TIPS3					32			New Alarm Level					新告警级别
4		ID_TIPS4					32			Please select					请选择
5		ID_TIPS5					32			New Relay Number				新继电器号
6		ID_TIPS6					32			Please select					请选择
7		ID_TIPS7					32			New Alarm State					新告警条件
8		ID_TIPS8					32			Please select					请选择
9		ID_NA						16			NA								无告警
10		ID_OA						16			OA								一般告警
11		ID_MA						16			MA								重要告警
12		ID_CA						16			CA								紧急告警
13		ID_NA2						16			None							无
14		ID_LOW						16			Low								低电平
15		ID_HIGH						16			High							高电平
16		ID_SET						16			Set								设置
17		ID_TITLE					16			DI Alarms						DI告警
18		ID_INDEX					16			Index							序号
19		ID_EQUIPMENT				32			Equipment Name					设备名
20		ID_SIGNAL_NAME				32			Signal Name						信号名
21		ID_ALARM_LEVEL				32			Alarm Level						告警级别
22		ID_ALARM_STATE				32			Alarm State						告警条件
23		ID_REPLAY_NUMBER			32			Alarm Relay						继电器号
24		ID_MODIFY					32			Modify							修改
25		ID_NA1						16			NA								无告警
26		ID_OA1						16			OA								一般告警
27		ID_MA1						16			MA								重要告警
28		ID_CA1						16			CA								紧急告警
29		ID_LOW1						16			Low								低电平
30		ID_HIGH1					16			High							高电平
31		ID_NA3						16			None							无
32		ID_MODIFY1					32			Modify							修改
33		ID_NO_DATA					32			No Data							无数据
34		ID_NA4						16			NA							无继电器
35		ID_POWER_SYSTEM						16			Power System							电源系统
36		ID_DO_NCU				32			NCU				NCU







#//changed by Frank Wu,2/N/14,20140527, for system log
[tmp.history_systemlog.html:Number]
23

[tmp.history_systemlog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_INDEX				32			Index					序号
2		ID_TASK_NAME			32			Task Name				任务名
3		ID_INFO_LEVEL			32			Info Level				等级
4		ID_LOG_TIME				32			Time					时间
5		ID_INFORMATION			32			Information				内容
8		ID_FROM					32			From				从
9		ID_TO					32			To					到
10		ID_QUERY				32			Query				查询
11		ID_UPLOAD				32			Upload				上载
12		ID_TIPS					64			Displays the last 500 entries		显示最多500条日志
13		ID_QUERY_TYPE				16			Query Type				查询类型
14		ID_SYSTEM_LOG				16			System Log				系统日志
16		ID_CTL_RESULT0				64			Successful				成功		
17		ID_CTL_RESULT1				64			No Memory				无内存		
18		ID_CTL_RESULT2				64			Time Expired				超时		
19		ID_CTL_RESULT3				64			Failed				失败		
20		ID_CTL_RESULT4				64			Communication Busy			通讯忙		
21		ID_CTL_RESULT5				64			Control was suppressed.		控制被屏蔽	
22		ID_CTL_RESULT6				64			Control was disabled.		不能控制	
23		ID_CTL_RESULT7				64			Control was canceled.		控制取消	


#//changed by Frank Wu,2/N/27,20140527, for power split
[tmp.setting_power_split.html:Number]
6

[tmp.setting_power_split.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_LVD1							32			LVD1							LVD1
2		ID_LVD2							32			LVD2							LVD2
3		ID_LVD3							32			LVD3							LVD3
4		ID_BATTERY_TEST					32			BATTERY_TEST					BATTERY_TEST
5		ID_EQUALIZE_CHARGE				32			EQUALIZE_CHARGE					EQUALIZE_CHARGE
6		ID_SAMP_TYPE					16			Sample							采样
7		ID_CTRL_TYPE					16			Control							控制
8		ID_SET_TYPE						16			Setting							设置
9		ID_ALARM_TYPE					16			Alarm							告警
10		ID_SLAVE						16			SLAVE							从模式
11		ID_MASTER						16			MASTER							主模式
12		ID_SELECT						32			Please select					请选择
13		ID_NA							16			NA								NA
14		ID_EQUIP						16			Equipment						设备名
15		ID_SIG_TYPE						16			Signal Type						信号类型
16		ID_SIG							16			Signal							信号名
17		ID_MODE							32			Power Split Mode				Power Split模式
18		ID_SET							16			Set								设置
19		ID_SET1							16			Set								设置
20		ID_TITLE						32			Power Split						Power Split
21		ID_INDEX						16			Index							序号
22		ID_SIG_NAME						32			Signal							信号名
23		ID_EQUIP_NAME					32			Equipment						设备名
24		ID_SIG_TYPE						32			Signal Type						信号类型
25		ID_SIG_NAME1					32			Signal Name						信号名
26		ID_MODIFY						32			Modify							修改
27		ID_MODIFY1						32			Modify							修改
28		ID_NO_DATA						32			No Data							无数据

[tmp.batt_setting.html:Number]
7

[tmp.batt_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				信号
2		ID_VALUE				32			Value				值
3		ID_TIME					32			Time Last Set			上次设置时间
4		ID_SET_VALUE				32			Set Value				设置值
5		ID_SET					32			Set					设置
6		ID_SET1					32			Set					设置
7		ID_BACK					16			Back				返回
8		ID_NO_DATA				32			No Data				无数据


[tmp.system_lvd_fuse.html:Number]
7

[tmp.system_lvd_fuse.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				信号
2		ID_VALUE				32			Value				值
3		ID_TIME					32			Time Last Set			上次设置时间
4		ID_SET_VALUE				32			Set Value				设置值
5		ID_SET					32			Set					设置
6		ID_SET1					32			Set					设置
7		ID_BACK					16			Back				返回
8		ID_NO_DATA				32			No Data				无数据
9		ID_SIGNAL1				32			Signal				信号
10		ID_VALUE1				32			Value				值

[tmp.system_battery_tabs.html:Number]
7

[tmp.system_battery_tabs.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_BATTERY				32			Battery				电池
2		ID_BATT_FUSE_GROUP			32			Battery Fuse Group			电池熔丝组
3		ID_BATT_FUSE				32			Battery Fuse			电池熔丝
4		ID_LVD_GROUP				32			LVD Group				LVD组
5		ID_LVD_UNIT				32			LVD Unit				LVD单元
6		ID_SMDU_BATT_FUSE			32			SMDU Battery Fuse			SMDU电池熔丝
7		ID_SMDU_LVD				32			SMDU LVD				SMDU LVD
8		ID_LVD3_UNIT				32			LVD3 Unit				LVD3单元

#//changed by Frank Wu,N/N/N,20140613, for two tabs
[tmp.setting_tabs.html:Number]
7

[tmp.setting_tabs.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_CHARGE				32			Battery Charge			电池充电
2		ID_ECO					32			ECO						ECO
3		ID_LVD					32			LVD						LVD
4		ID_QUICK_SET			32			Quick Settings			快速设置
5		ID_TEMP					32			Temperature				温度
6		ID_RECT					32			Rectifiers				整流模块
7		ID_CONVERTER			32			DC/DC Converters		DC/DC模块
8		ID_BATT_TEST			32			Battery Test			电池测试
9		ID_TIME_CFG				32			Time Settings			时间设置
10		ID_USER_DEF_SET_1		32			User Config1			用户配置1
11		ID_USER_SET2			32			User Config2			用户配置2
12		ID_USER_SET3			32			User Config3			用户配置3
13		ID_MPPT					32				Solar					太阳能
14		ID_POWER_SYS			32			System					系统
15		ID_CABINET_SET			32			Set Cabinet					设置机柜
16		ID_USER_SET4			32			User Config4			用户配置4


[tmp.system_tabs_a.html:Number]
7

[tmp.system_tabs_a.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_RECTIFIER				32			Rectifier				整流模块
2		ID_RECTIFIER1				32			GI Rectifier			GI整流模块
3		ID_RECTIFIER2				32			GII Rectifier			GII整流模块
4		ID_RECTIFIER3				32			GIII Rectifier			GIII整流模块
5		ID_CONVERTER				32			Converter				DC/DC模块
6		ID_SOLAR					32			Solar Converter			太阳能模块

[tmp.consumption_map.html:Number]
7

[tmp.consumption_map.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS1				32			branch number			支路数量
2		ID_TIPS2				32			total current			总电流
3		ID_TIPS3				32			total power				总功率
4		ID_TIPS4				32			total energy			总能量
5		ID_TIPS5				32			peak power last 24h			24小时内最大功率
6		ID_TIPS6				32			peak power last week		一周内最大功率
7		ID_TIPS7				32			peak power last month		一个月内最大功率

[tmp.system_dc_smdup.html:Number]
7

[tmp.system_dc_smdup.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				信号
2		ID_VALUE				32			Value				值
3		ID_SIGNAL1				32			Signal				信号
4		ID_VALUE1				32			Value				值
5		ID_NO_DATA				32			No Data				无数据

[tmp.adv_setting_tabs.html:Number]
25

[tmp.adv_setting_tabs.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_IPV4					32			Ethernet				Ethernet
2		ID_SCUP_IP				32			IP Address				IP地址
3		ID_SCUP_MASK				32			Subnet Mask				子网掩码
4		ID_SCUP_GATEWAY				32			Default Gateway			默认网关
5		ID_ERROR0				32			Setting Failed.			设置失败.		
6		ID_ERROR1				32			Successful.				成功.	
7		ID_ERROR2				64			Failed. Incorrect input.				失败,不正确的输入.	
8		ID_ERROR3				64			Failed. Incomplete information.			失败,信息不完整.		
9		ID_ERROR4				64			Failed. No privilege.				失败,没有权限.		
10		ID_ERROR5				128			Failed. Controller is hardware protected.		失败,监控处于硬件保护状态.
11		ID_ERROR6				32			Failed. DHCP is ON.					失败,监控处于DHCP状态.
12		ID_TIPS0				32			Set Network Parameter														设置网络参数											
13		ID_TIPS1				128			Units IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171.					IP地址输入错误。\n正确格式为'nnn.nnn.nnn.nnn'，如10.75.14.171					
14		ID_TIPS2				128			Mask IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 255.255.0.0.						子网掩码输入错误。\n正确格式为'nnn.nnn.nnn.nnn'，如255.255.0.0				
15		ID_TIPS3				64			Units IP Address and Mask mismatch.													IP地址或子网掩码错误。请重新输入。							
16		ID_TIPS4				128			Gateway IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171. Enter 0.0.0.0 for no gateway	网关IP地址输入错误。\n正确格式为'nnn.nnn.nnn.nnn'，如10.75.14.254，0.0.0.0表示无网关.	
17		ID_TIPS5				128			Units IP Address, Gateway, Mask mismatch. Enter Address again.									IP地址、缺省网关和子网掩码不匹配.请重新输入.						
18		ID_TIPS6				64			Please wait. Controller is rebooting.												请等待,监控正在启动										
19		ID_TIPS7				128			Parameters have been modified.  Controller is rebooting...										参数被修改,监控正在启动									
20		ID_TIPS8				64			Controller homepage will be refreshed.												监控主页将被刷新										
21		ID_TIPS9				128			Confirm the change to the IP address?				确认修改IP地址?
22		ID_SAVE					16			Save						保存
23		ID_TIPS10				32			IP Address Error				IP地址错误
24		ID_TIPS11				32			Subnet Mask Error				子网掩码错误
25		ID_TIPS12				32			Default Gateway Error			默认网关错误
26		ID_USER					32			Users				账户
27		ID_IPV4_1					32			IPV4				IPV4
28		ID_IPV6					32			IPV6				IPV6
29		ID_HLMS_CONF				32			Monitor Protocol				监控协议
30		ID_SITE_INFO				32			Site Info				局站信息
31		ID_AUTO_CONFIG				32			Auto Config				自动配置
32		ID_OTHER				32			Alarm Report			告警上报
33		ID_NMS					32			SNMP				SNMP
34		ID_ALARM_SET				32			Alarms		告警
35		ID_CLEAR_DATA				16			Clear Data			清除数据
36		ID_RESTORE_DEFAULT			32			Restore Defaults		恢复默认配置
37		ID_DOWNLOAD_UPLOAD			32			SW Maintenance			软件维护
38		ID_HYBRID				32			Generator				油机
39		ID_DHCP					16			IPV4 DHCP				IPV4 DHCP
40		ID_IP1					32			Server IP				服务器IP
41		ID_TIPS13				16			Loading				加载
42		ID_TIPS14				16			Loading				加载
43		ID_TIPS15				32			failed, data format error!				失败, 数据格式错误!
44		ID_TIPS16				32			failed, please refresh the page!			失败, 请刷新页面!
45		ID_LANG					16			Language			语言
#//changed by Frank Wu,2/2/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
46		ID_SHUNT_SET			32				Shunts				分流器
#//changed by Frank Wu,1/N/35,20140527, for adding the the web setting tab page 'DI'
47		ID_DI_ALARM_SET			32				DI Alarms			DI告警
#//changed by Frank Wu,1/1/N,00000000, for add PowerSplit
48		ID_POWER_SPLIT_SET		32				Power Split			Power Split
49		ID_IPV6_1			16				IPV6				IPV6
50		ID_LOCAL_IP			32				Link-Local Address		本地链接地址
51		ID_GLOBAL_IP			32				IPV6 Address			IPV6地址
52		ID_SCUP_PREV			16				Subnet Prefix				子网前缀
53		ID_SCUP_GATEWAY1		16				Default Gateway				默认网关
54		ID_SAVE1			16				Save				保存
55		ID_DHCP1			16				IPV6 DHCP			IPV6 DHCP
56		ID_IP2				32				Server IP			服务器IP
57		ID_TIPS17			64				Please fill in the correct IPV6 address.	请填写正确的IPV6地址.
58		ID_TIPS18			128				Prefix can not be empty or beyond the range.	前缀不能为空或超出范围.
59		ID_TIPS19			64				Please fill in the correct IPV6 gateway.	请填写正确的IPV6网关.
60		ID_TL1_GROUP			16				TL1 AID Group					TL1 AID组
61		ID_TL1_SIGNAL			16				TL1 AID Signal					TL1 AID信号
62		ID_DO_RELAY				32			DO(relay)						DO(继电器)
63		ID_SHUNTS_SET				32			Shunts						分流器
64		ID_CR_SET				32			Fuse						熔丝
65		ID_CUSTOM_INPUT				32			Custom Inputs						自定义输入
66		ID_ANALOG_SET				32			Analogs						模拟量
[tmp.setting_TL1_group.html:Number]
25

[tmp.setting_TL1_group.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_AID_GROUP				16			AID Group				AID组
2		ID_AID_NAME				16			AID Name				AID名字
3		ID_AID_PREFIX				32			Sub AID Name Prefix			子AID名字前缀
4		ID_AID_TYPE				16			AID Type				AID类型
5		ID_EQPT					16			EQPT				EQPT
6		ID_ENV					16			ENV					ENV
7		ID_AID_INDEX				16			index				索引
8		ID_SUBMIT				16			Submit				提交
12		ID_ERROR3				64			Insufficient privileges for this function.		失败, 权限不足.
13		ID_ERROR4				32			Invalid character(s).			非法字符.
14		ID_ERROR5				32			Can't be empty.			不能为空.
15		ID_AID_NAME1				16			AID Name				AID名字
16		ID_AID_PREFIX1				32			Sub AID Name Prefix			子AID名字前缀
17		ID_AID_TYPE1				16			AID Type				AID类型
18		ID_TL1_ERRORS1				32			Successful.				成功.
19		ID_TL1_ERRORS2				32			Configuration fail.			配置失败
20		ID_TL1_ERRORS3				64			Failed. Service was exited.		失败,服务已经退出.
21		ID_TL1_ERRORS4				32			Invalid parameters.			不合法的参数.
22		ID_TL1_ERRORS5				32			Invalid parameters.			不合法的参数.
23		ID_TL1_ERRORS6				64			Fail to write to Flash.		写Flash失败.
24		ID_TL1_ERRORS7				32			Service is busy.			服务正忙.
25		ID_TL1_ERRORS8				64			Port has already been occupied.	端口已被占用.
26		ID_TL1_ERRORS9				64			Port has already been occupied.	端口已被占用.
27		ID_TL1_ERRORS10				64			Port has already been occupied.	端口已被占用.
28		ID_TL1_ERRORS11				64			Port has already been occupied.	端口已被占用.
29		ID_TL1_ERRORS12				64			The TL1 feature is not available.	TL1不可用.
30		ID_ERROR6				128			The length sum of AID name and Sub AID name prefix exceed range.	AID名字和子AID名字前缀长度之和超出范围.


[tmp.setting_TL1_signal.html:Number]
25

[tmp.setting_TL1_signal.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_AID_GROUP				16			AID Group				AID组
2		ID_AID_PREFIX				32			Sub AID Name Prefix			子AID名字前缀
3		ID_AID_INDEX				16			index				索引
4		ID_AID_NAME				16			AID Name				AID名字
8		ID_ERROR3				64			Insufficient privileges for this function.		失败, 权限不足.
9		ID_ERROR4				32			Invalid character(s).			非法字符
10		ID_ERROR5				32			Can't be empty.			不能为空.
11		ID_ACTIVE				16			Signal Active			信号激活
12		ID_SAMPLE				16			Sample Signal			采集信号
13		ID_ALARM				16			Alarm Signal			告警信号
14		ID_SETTING				16			Setting Signal			设置信号
15		ID_CON_TYPE				32			Condition Type			条件类型
16		ID_CON_DESC				32			Condition Description		条件描述
17		ID_NOTI_CODE				32			Notification Code			通知代码
18		ID_SERV_CODE				32			Service Affect Code			服务影响代码
19		ID_MONI_FORT				32			Monitor Format			监控格式
20		ID_MONI_TYPE				32			Monitor Type			监控类型
21		ID_SUBMIT				16			Submit				提交
22		ID_TIPS1				64			Please Select AID Group and Equip		请选择AID组和设备
23		ID_TIPS2				32			Please Select Signal		请选择信号
24		ID_TIPS3				16			Please Select			请选择
25		ID_EQUIP_LIST				16			Equip List				设备列表
26		ID_TIPS4				16			Please Select			请选择
27		ID_TIPS5				64			First select AID Group, then select Equip.		先选择AID组, 然后选择设备.
28		ID_TL1_ERRORS1				32			Successful.				成功.
29		ID_TL1_ERRORS2				32			Configuration fail.			配置失败
30		ID_TL1_ERRORS3				64			Failed. Service was exited.		失败,服务已经退出.
31		ID_TL1_ERRORS4				32			Invalid parameters.			不合法的参数.
32		ID_TL1_ERRORS5				32			Invalid parameters.			不合法的参数.
33		ID_TL1_ERRORS6				64			Fail to write to Flash.		写Flash失败.
34		ID_TL1_ERRORS7				32			Service is busy.			服务正忙.
35		ID_TL1_ERRORS8				64			Port has already been occupied.	端口已被占用.
36		ID_TL1_ERRORS9				64			Port has already been occupied.	端口已被占用.
37		ID_TL1_ERRORS10				64			Port has already been occupied.	端口已被占用.
38		ID_TL1_ERRORS11				64			Port has already been occupied.	端口已被占用.
39		ID_TL1_ERRORS12				64			Error, TL1 is an option and is not active.	TL1不可用.
[tmp.setting_relay_content.html:Number]
12

[tmp.setting_relay_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_DO_RELAY				32			DO(Relay)				DO(relay)
2		ID_SIGNAL_NAME				32			Signal Name				信号名
3		ID_VALUE				32			Value						值
4		ID_SET_VALUE			32			Set Value					设置值
5		ID_SET					32			Set							设置
6		ID_SET1					32			Set							设置
7		ID_SET2					32			Set							设置
8		ID_SET3					32			Set							设置
9		ID_MODIFY				32			Modify							修改
10		ID_MODIFY1				32			Modify							修改
11		ID_NO_DATA				32			No Data							无数据
12		ID_TIPS1				32			Signal Full Name				信号名全称
13		ID_INPUTTIP				32			Max Characters:20				最大字符:20
14		ID_INPUTTIP2				32			Max Characters:32				最大字符:32
15		ID_INPUTTIP3				32			Max Characters:16				最大字符:16
16		ID_DO_NCU				32			NCU				NCU
17		ID_DO_POEWRSYSTEM				32			Power System				电源系统
18		ID_INPUTTIP4				32			Max Characters:20				最大字符:10

[tmp.setting_shunts_content.html:Number]
11

[tmp.setting_shunts_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_REFERENCE				32			Reference				参考
2		ID_SIGNAL_SHUNTNAME				32			Shunt Name				信号名
3		ID_MODIFY				32			Modify						修改
4		ID_VIEW				32			View					视图
5		ID_FULLSC				32			Full Scale Current					满量程电流
6		ID_FULLSV			32			Full Scale Voltage					满量程电压
7		ID_BREAK_VALUE				32			Break Value					阈值
8		ID_CURRENT_SETTING				32			Current Setting					当前设置
9		ID_NEW_SETTING				32			New Setting					新设置
10		ID_RANGE				32			Range					范围
11		ID_SETAS				32			Set As					设置
12		ID_SIGNAL_FULL_NAME				32			Signal Full Name					信号名全称
13		ID_SIGNAL_ABBR_NAME				32			Signal Abbr Name					信号名缩写
14		ID_SHUNT1				32			Shunt 1					分流器1
15		ID_SHUNT2				32			Shunt 2					分流器2
16		ID_SHUNT3				32			Shunt 3					分流器3
17		ID_SHUNT4				32			Shunt 4					分流器4
18		ID_SHUNT5				32			Shunt 5					分流器5
19		ID_SHUNT6				32			Shunt 6					分流器6
20		ID_SHUNT7				32			Shunt 7					分流器7
21		ID_SHUNT8				32			Shunt 8					分流器8
22		ID_SHUNT9				32			Shunt 9					分流器9
23		ID_SHUNT10				32			Shunt 10					分流器10
24		ID_SHUNT11				32			Shunt 11					分流器11
25		ID_SHUNT12				32			Shunt 12					分流器12
26		ID_SHUNT13				32			Shunt 13					分流器13
27		ID_SHUNT14				32			Shunt 14					分流器14
28		ID_SHUNT15				32			Shunt 15					分流器15
29		ID_SHUNT16				32			Shunt 16					分流器16
30		ID_SHUNT17				32			Shunt 17					分流器17
31		ID_SHUNT18				32			Shunt 18					分流器18
32		ID_SHUNT19				32			Shunt 19					分流器19
33		ID_SHUNT20				32			Shunt 20					分流器20
34		ID_SHUNT21				32			Shunt 21					分流器21
35		ID_SHUNT22				32			Shunt 22					分流器22
36		ID_SHUNT23				32			Shunt 23					分流器23
37		ID_SHUNT24				32			Shunt 24					分流器24
38		ID_SHUNT25				32			Shunt 25					分流器25
39		ID_LOADSHUNT				32			Load Shunt					直流
40		ID_BATTERYSHUNT				32			Battery Shunt					电池串
41		ID_TIPS1					32			Please select					请选择
42		ID_TIPS2					32			Please select					请选择
43		ID_NA						16			NA								无告警
44		ID_OA						16			OA								一般告警
45		ID_MA						16			MA								重要告警
46		ID_CA						16			CA								紧急告警
47		ID_NA2						16			None							无
48		ID_NOTUSEd						16			Not Used							不使用
49		ID_GENERL						16			General							通用
50		ID_LOAD						16			Load							负载
51		ID_BATTERY						16			Battery							电池
52		ID_NA3						16			NA								无告警
53		ID_OA2						16			OA								一般告警
54		ID_MA2						16			MA								重要告警
55		ID_CA2						16			CA								紧急告警
56		ID_NA4						16			None							无
57		ID_MODIFY				32			Modify						修改
58		ID_VIEW				32			View					视图
59		ID_SET						16			set							设置
60		ID_NA1						16			None								无
61		ID_Severity1				32			Alarm Relay				告警继电器
62		ID_Relay1				32			Alarm Severity				告警级别
63		ID_Severity2				32			Alarm Relay				告警继电器
64		ID_Relay2				32			Alarm Severity				告警级别
65		ID_Alarm				32			Alarm				告警
66		ID_Alarm2				32			Alarm				告警
67		ID_INPUTTIP				32			Max Characters:20				最大字符:20
68		ID_INPUTTIP2				32			Max Characters:20				最大字符:20
69		ID_INPUTTIP3				32			Max Characters:8				最大字符:8
70		ID_FULLSV				32			Full Scale Voltage				满量程电压
71		ID_FULLSC				32			Full Scale Current				满量程电流
72		ID_BREAK_VALUE2				32			Break Value					告警阈值
73		ID_High1CLA				32			High 1 Curr Limit Alarm				过流1告警
74		ID_High1CAS				32			High 1 Curr Alarm Severity			过流1告警级别
75		ID_High1CAR				32			High 1 Curr Alarm Relay				过流1告警继电器
76		ID_High2CLA				32			High 2 Curr Limit Alarm				过流2告警
77		ID_High2CAS				32			High 2 Curr Alarm Severity			过流2告警级别
78		ID_High2CAR				32			High 2 Curr Alarm Relay				过流2告警继电器
79		ID_HI1CUR				32			Hi1Cur				电流过流
80		ID_HI2CUR				32			Hi2Cur				电流过过流
81		ID_HI1CURRENT				32			High 1 Curr				电流过流
82		ID_HI2CURRENT				32			High 2 Curr				电流过过流
83		ID_NA4						16			NA								无继电器


[tmp.setting_fuse_content.html:Number]
12

[tmp.setting_fuse_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL_NAME				32			Signal Name				信号名
2		ID_MODIFY				32			Modify				修改
3		ID_SET				32			Set				设置
4		ID_TIPS				32			Signal Full Name				信号名全称
5		ID_MODIFY1				32			Modify				修改
6		ID_TIPS2				32			Signal Abbr Name				信号名简称
7		ID_INPUTTIP				32			Max Characters:20				最大字符:20
8		ID_INPUTTIP2				32			Max Characters:32				最大字符:32
9		ID_INPUTTIP3				32			Max Characters:15				最大字符:15

[tmp.shunts_data_content.html:Number]
11
[tmp.shunts_data_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_NA5						16			NA								无继电器
2		ID_REFERENCE2				32			Reference				参考
3		ID_SIGNAL_SHUNTNAME2				32			Shunt Name				信号名
4		ID_MODIFY2				32			Modify						修改
5		ID_VIEW2				32			View					视图
6		ID_MODIFY3				32			Modify						修改
7		ID_VIEW3				32			View					视图
8		ID_BATTERYSHUNT2				32			Battery Shunt					电池串
9		ID_LOADSHUNT2				32			Load Shunt					直流
10		ID_BATTERYSHUNT2				32			Battery Shunt					电池串

[tmp.system_source_smdu.html:Number]
1
[tmp.system_source_smdu.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				信号
2		ID_VALUE				32			Value				值
3		ID_SIGNAL1				32			Signal				信号
4		ID_VALUE1				32			Value				值
5		ID_NO_DATA				32			No Data				无数据

[tmp.system_source.html:Number]
1
[tmp.system_source.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				信号
2		ID_VALUE				32			Value				值
3		ID_SIGNAL1				32			Signal				信号
4		ID_VALUE1				32			Value				值
5		ID_NO_DATA				32			No Data				无数据


[tmp.setting_custominputs.html:Number]
1
[tmp.setting_custominputs.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_REFERENCE				32			Reference				参数
2		ID_VOLTAGE_INPUT				32			Voltage Input				电压输入
3		ID_FUSE_ALARM_INPUT				32			Fuse Alarm Input				熔丝告警输入
4		ID_ANALOG_INPUT				32			Analog Input				模拟量输入
5		ID_MODIFY				32			Modify				修改
6		ID_MODIFY2				32			Modify				修改
7		ID_ADVANCEDSETTING_ANALOGS				32			See Advanced Settings > Analogs				查看 高级设置> 模拟量
8		ID_ADVANCEDSETTING_ANALOGS2				32			See Advanced Settings > Analogs				查看 高级设置> 模拟量
9		ID_ADVANCEDSETTING_ANALOGS3				32			See Advanced Settings > Analogs				查看 高级设置> 模拟量
10		ID_SETTINGS_FUSES				32			See Advanced Settings > Fuses				查看 高级设置> 熔丝
11		ID_ADVANCESSETTING_SHUNTS				64			See Advanced Settings > Shunts				查看 高级设置> 分流器
12		ID_SETTINGS_TEMPERATURE				32			See Settings > Temp Probes				查看 设置> 温度探测器
13		ID_FUNCTION				32			Function				功能
14		ID_CURRENT_SETTING				32			Current Setting				当前设置
15		ID_NEW_SETTING				32			New Setting				新设置
16		ID_VOLTAGE_INPUT2				32			Voltage Input				电压输入
17		ID_FUSE_ALARM_INPUT2				32			Fuse Alarm Input				熔丝告警输入
18		ID_ANALOG_INPUT2				32			Analog Input				模拟量输入
19		ID_SET				32			Set				Conjunto

[tmp.setting_analog.html:Number]
1
[tmp.setting_analog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_REFERENCE					32			Reference				参数
2		ID_SIGNALFULLNAME				32			Signal Full Name			信号全名
3		ID_MODIFY						32			Modify					修改
4		ID_SIGNAL					32			Signal				信号
5		ID_CURRENT_SETTING				32			Current Setting			当前设置
6		ID_NEW_CURRENT						32			New Current					新设置
7		ID_RANGE						32			Range					范围
8		ID_SIGNAL_FULL_NAME				32			Signal Full Name					信号全名
9		ID_SIGNAL_ABBR_NAME				32			Signal Abbr Name					信号短名
10		ID_LOW1_ALARM_LIMIT				32			Low 1 Alarm Limit					低告警阈值
11		ID_LOW1_ALARM_SEVERITY				32			Low 1 Alarm Severity					低告警级别
12		ID_LOW1_ALARM_RELAY				32			Low 1 Alarm Relay					低继电器号
13		ID_LOW2_ALARM_LIMIT				32			Low 2 Alarm Limit					低低告警阈值
14		ID_LOW2_ALARM_SEVERITY				32			Low 2 Alarm Severity					低低告警级别
15		ID_LOW2_ALARM_RELAY				32			Low 2 Alarm Relay					低低继电器告警
16		ID_HIGH1_ALARM_LIMIT				32			High 1 Alarm Limit					高告警阈值
17		ID_HIGH1_ALARM_SEVERITY				32			High 1 Alarm Severity						高告警级别
18		ID_HIGH1_ALARM_RELAY				32			High 1 Alarm Relay					高继电器号
19		ID_HIGH2_ALARM_LIMIT				32			High 2 Alarm Limit					高高告警阈值
20		ID_HIGH2_ALARM_SEVERITY				32			High 2 Alarm Severity						高高告警级别
21		ID_HIGH2_ALARM_RELAY				32			High 2 Alarm Relay					高高继电器告警
22		ID_SET							32			Set							设置
23		ID_NA							32			NA							NA
24		ID_OA							32			OA							OA
25		ID_MA							32			MA							MA
26		ID_CA							32			CA							CA
27		ID_NA2							32			NA							NA
28		ID_OA2							32			OA							OA
29		ID_MA2							32			MA							MA
30		ID_CA2							32			CA							CA
31		ID_NA3							32			NA							NA
32		ID_OA3							32			OA							OA
33		ID_MA3							32			MA							MA
34		ID_CA3							32			CA							CA
35		ID_NA4							32			NA							NA
36		ID_OA4							32			OA							OA
37		ID_MA4							32			MA							MA
38		ID_CA4							32			CA							CA
39		ID_MODIFY2						32			Modify					修改
40		ID_INPUTTIP						32			Max Characters:25				最大字符:12
41		ID_INPUTTIP2						32			Max Characters:11				最大字符:5
42		ID_NA5							32			None							无
43		ID_NA6							32			None							无
44		ID_NA7							32			None							无
45		ID_NA8							32			None							无
46		ID_TIPS1							32			Please select					请选择
47		ID_TIPS2							32			Please select					请选择
48		ID_TIPS3							32			Please select					请选择
49		ID_TIPS4							32			Please select					请选择
50		ID_LOW							32			Low					低
51		ID_HIGH							32			High					高
52		ID_LOW2							32			Lo					低
53		ID_HIGH2							32			Hi					高
54		ID_SIGNAL_UNITS							32			Signal Units					信号单位
55		ID_INPUTTIP3							32			Max Characters:8					最大字符:8
56		ID_SIGNAL_X1							32			X1					X1
57		ID_SIGNAL_Y1							32			Y1(value at X1)					Y1 (X1 的值)
58		ID_SIGNAL_X2							32			X2					X2
59		ID_SIGNAL_Y2							32			Y2(value at X2)					Y2 (X2 的值)
60		ID_NO_DATA				64			No SMDUE inputs being used at this time				目前没有使用SMDUE输入

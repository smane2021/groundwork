﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Bus Bar Voltage				Bus Bar Voltage		母排电压		母排电压
2	32			15			Shunt 1					Shunt 1			电流1			电流1
3	32			15			Shunt 2					Shunt 2			电流2			电流2
4	32			15			Shunt 3					Shunt 3			电流3			电流3
5	32			15			Shunt 4					Shunt 4			电流4			电流4
6	32			15			Fuse 1					Fuse 1			熔丝1			熔丝1
7	32			15			Fuse 2					Fuse 2			熔丝2			熔丝2
8	32			15			Fuse 3					Fuse 3			熔丝3			熔丝3
9	32			15			Fuse 4					Fuse 4			熔丝4			熔丝4
10	32			15			Fuse 5					Fuse 5			熔丝5			熔丝5
11	32			15			Fuse 6					Fuse 6			熔丝6			熔丝6
12	32			15			Fuse 7					Fuse 7			熔丝7			熔丝7
13	32			15			Fuse 8					Fuse 8			熔丝8			熔丝8
14	32			15			Fuse 9					Fuse 9			熔丝9			熔丝9
15	32			15			Fuse 10					Fuse 10			熔丝10			熔丝10
16	32			15			Fuse 11					Fuse 11			熔丝11			熔丝11
17	32			15			Fuse 12					Fuse 12			熔丝12			熔丝12
18	32			15			Fuse 13					Fuse 13			熔丝13			熔丝13
19	32			15			Fuse 14					Fuse 14			熔丝14			熔丝14
20	32			15			Fuse 15					Fuse 15			熔丝15			熔丝15
21	32			15			Fuse 16					Fuse 16			熔丝16			熔丝16
22	32			15			Run Time				Run Time		运行时间		运行时间
23	32			15			LV Disconnect 1 Control			LVD 1 Control		LVD1控制		LVD1控制
24	32			15			LV Disconnect 2 Control			LVD 2 Control		LVD2控制		LVD2控制
25	32			15			LV Disconnect 1 Voltage			LVD 1 Voltage		LVD1电压		LVD1电压
26	32			15			LV Resconnect 1 Voltage			LVR 1 Voltage		LVR1电压		LVR1电压
27	32			15			LV Disconnect 2 Voltage			LVD 2 Voltage		LVD2电压		LVD2电压
28	32			15			LV Resconnect 2 Voltage			LVR 2 Voltage		LVR2电压		LVR2电压
29	32			15			On					On			正常			正常
30	32			15			Off					Off			断开			断开
31	32			15			Normal					Normal			正常			正常
32	32			15			Error					Error			错误			错误
33	32			15			On					On			闭合			闭合
34	32			15			Fuse 1 Alarm				Fuse 1 Alarm		负载熔丝1告警		熔丝1告警
35	32			15			Fuse 2 Alarm				Fuse 2 Alarm		负载熔丝2告警		熔丝2告警
36	32			15			Fuse 3 Alarm				Fuse 3 Alarm		负载熔丝3告警		熔丝3告警
37	32			15			Fuse 4 Alarm				Fuse 4 Alarm		负载熔丝4告警		熔丝4告警
38	32			15			Fuse 5 Alarm				Fuse 5 Alarm		负载熔丝5告警		熔丝5告警
39	32			15			Fuse 6 Alarm				Fuse 6 Alarm		负载熔丝6告警		熔丝6告警
40	32			15			Fuse 7 Alarm				Fuse 7 Alarm		负载熔丝7告警		熔丝7告警
41	32			15			Fuse 8 Alarm				Fuse 8 Alarm		负载熔丝8告警		熔丝8告警
42	32			15			Fuse 9 Alarm				Fuse 9 Alarm		负载熔丝9告警		熔丝9告警
43	32			15			Fuse 10 Alarm				Fuse 10 Alarm		负载熔丝10告警		熔丝10告警
44	32			15			Fuse 11 Alarm				Fuse 11 Alarm		负载熔丝11告警		熔丝11告警
45	32			15			Fuse 12 Alarm				Fuse 12 Alarm		负载熔丝12告警		熔丝12告警
46	32			15			Fuse 13 Alarm				Fuse 13 Alarm		负载熔丝13告警		熔丝13告警
47	32			15			Fuse 14 Alarm				Fuse 14 Alarm		负载熔丝14告警		熔丝14告警
48	32			15			Fuse 15 Alarm				Fuse 15 Alarm		负载熔丝15告警		熔丝15告警
49	32			15			Fuse 16 Alarm				Fuse 16 Alarm		负载熔丝16告警		熔丝16告警
50	32			15			HW Test Alarm				HW Test Alarm		硬件自检告警		硬件自检告警                                                                                                                                     
51	32			15			SMDUP 8					SMDUP 8			SMDUP 8			SMDUP 8
52	32			15			Battery Fuse 1 Voltage			BattFuse 1 Volt		电池熔丝1电压		电池熔丝1电压
53	32			15			Battery Fuse 2 Voltage			BattFuse 2 Volt		电池熔丝2电压		电池熔丝2电压
54	32			15			Battery Fuse 3 Voltage			BattFuse 3 Volt		电池熔丝3电压		电池熔丝3电压
55	32			15			Battery Fuse 4 Voltage			BattFuse 4 Volt		电池熔丝4电压		电池熔丝4电压
56	32			15			Battery Fuse 1 Status			Batt Fuse 1		电池熔丝1状态		电池熔丝1状态
57	32			15			Battery Fuse 2 Status			Batt Fuse 2		电池熔丝2状态		电池熔丝2状态
58	32			15			Battery Fuse 3 Status			Batt Fuse 3		电池熔丝3状态		电池熔丝3状态
59	32			15			Battery Fuse 4 Status			Batt Fuse 4		电池熔丝4状态		电池熔丝4状态
60	32			15			On					On			正常			正常
61	32			15			Off					Off			断开			断开
62	32			15			Battery Fuse 1 Alarm			Batt Fuse 1 Alm		电池熔丝1告警		电池熔丝1告警
63	32			15			Battery Fuse 2 Alarm			Batt Fuse 2 Alm		电池熔丝2告警		电池熔丝2告警
64	32			15			Battery Fuse 3 Alarm			Batt Fuse 3 Alm		电池熔丝3告警		电池熔丝3告警
65	32			15			Battery Fuse 4 Alarm			Batt Fuse 4 Alm		电池熔丝4告警		电池熔丝4告警
66	32			15			All Load Current			All Load Curr		负载总电流		负载总电流
67	32			15			Over Current Limit (Load)		Over Curr Limit		负载总电流过流点	过流点
68	32			15			Over Current (Load)			Over Current		负载总电流过流		过流
69	32			15		LVD 1					LVD 1			LVD1允许		LVD1允许
70	32			15		LVD 1 Mode				LVD 1 Mode		LVD1方式		LVD1方式
71	32			15		LVD 1 Reconnect Delay			LVD1 ReconDelay		LVD1上电延迟		LVD1上电延迟
72	32			15		LVD 2					LVD 2			LVD2允许		LVD2允许
73	32			15		LVD 2 Mode				LVD 2 Mode		LVD2方式		LVD2方式
74	32			15		LVD 2 Reconnect Delay			LVD2 ReconDelay		LVD2上电延迟		LVD2上电延迟
75	32			15			LVD 1 Status				LVD 1 Status		LVD1状态		LVD1状态
76	32			15			LVD 2 Status				LVD 2 Status		LVD2状态		LVD2状态
77	32			15			Disabled				Disabled		禁止			禁止
78	32			15			Enabled					Enabled			允许			允许
79	32			15			Voltage				Voltage			电压方式		电压方式
80	32			15			Time					Time			时间方式		时间方式
81	32			15			Bus Bar Volt Alarm			Bus Bar Alarm		母排电压告警		母排电压告警
82	32			15			Normal					Normal			正常			正常
83	32			15			Low					Low			低于下限		低于下限
84	32			15			High					High			高于上限		高于上限
85	32			15			Under Voltage				Under Voltage		欠压			欠压
86	32			15			Over Voltage				Over Voltage		过压			过压
87	32			15			Shunt 1 Current Alarm			Shunt 1 Alarm		分流器1告警		分流器1告警
88	32			15			Shunt 2 Current Alarm			Shunt 2 Alarm		分流器2告警		分流器2告警
89	32			15			Shunt 3 Current Alarm			Shunt 3 Alarm		分流器3告警		分流器3告警
90	32			15			Shunt 4 Current Alarm			Shunt 4 Alarm		分流器4告警		分流器4告警
91	32			15			Shunt 1 Over Current			Shunt 1 OverCur		分流器1过流		分流器1过流
92	32			15			Shunt 2 Over Current			Shunt 2 OverCur		分流器2过流		分流器2过流
93	32			15			Shunt 3 Over Current			Shunt 3 OverCur		分流器3过流		分流器3过流
94	32			15			Shunt 4 Over Current			Shunt 4 OverCur		分流器4过流		分流器4过流
95	32			15			Times of Communication Fail		Times Comm Fail		通信失败次数		通信失败次数
96	32			15			Existent				Existent		存在			存在
97	32			15			Not Existent				Not Existent		不存在			不存在
98	32			15			Very Low				Very Low		低于下下限		低于下下限
99	32			15			Very High				Very High		高于上上限		高于上上限
100	32			15			Switch					Switch			Swich			Swich
101	32			15			LVD 1 Fail			LVD 1 Fail	LVD1控制失败		LVD1控制失败
102	32			15			LVD 2 Fail			LVD 2 Fail	LVD2控制失败		LVD2控制失败
103	32			15			High Temperature Disconnect 1	HTD 1		HTD1高温下电允许	HTD1下电允许
104	32			15			High Temperature Disconnect 2	HTD 2		HTD2高温下电允许	HTD2下电允许
105	32			15			Battery LVD				Batt LVD		电池下电		电池下电
106	32			15			No Battery				No Battery			无电池			无电池
107	32			15			LVD 1					LVD 1			LVD1			LVD1
108	32			15			LVD 2					LVD 2			LVD2			LVD2
109	32			15			Batt Always On				Batt Always On		有电池但不下电		有电池但不下电
110	32			15			Barcode					Barcode			Barcode			Barcode
111	32			15			DC Over Voltage				DC Over Volt		直流过压点		直流过压点
112	32			15			DC Under Voltage			DC Under Volt		直流欠压点		直流欠压点
113	32			15			Over Current 1				Over Current 1		过流点1			过流点1
114	32			15			Over Current 2				Over Current 2		过流点2			过流点2
115	32			15			Over Current 3				Over Current 3		过流点3			过流点3
116	32			15			Over Current 4				Over Current 4		过流点4			过流点4
117	32			15			Existence State				Existence State		是否存在		是否存在
118	32			15			Communication Fail			Comm Fail		通讯中断		通讯中断
119	32			15			Bus Voltage Status			Bus Status		电压状态		电压状态
120	32			15			Comm OK					Comm OK			通讯正常		通讯正常
121	32			15			All Batteries Comm Fail			All Comm Fail		都通讯中断		都通讯中断
122	32			15			Communication Fail			Comm Fail		通讯中断		通讯中断
123	32			15			Rated Capacity				Rated Capacity		标称容量		标称容量	
124	32			15			Shunt 5					Shunt 5			电流5			电流5
125	32			15			Shunt 6					Shunt 6			电流6			电流6
126	32			15			Shunt 7					Shunt 7			电流7			电流7
127	32			15			Shunt 8					Shunt 8			电流8			电流8
128	32			15			Shunt 9					Shunt 9			电流9			电流9
129	32			15			Shunt 10				Shunt 10		电流10			电流10
130	32			15			Shunt 11				Shunt 11		电流11			电流11
131	32			15			Shunt 12				Shunt 12		电流12			电流12
132	32			15			Shunt 13				Shunt 13		电流13			电流13
133	32			15			Shunt 14				Shunt 14		电流14			电流14
134	32			15			Shunt 15				Shunt 15		电流15			电流15
135	32			15			Shunt 16				Shunt 16		电流16			电流16
136	32			15			Shunt 17				Shunt 17		电流17			电流17
137	32			15			Shunt 18				Shunt 18		电流18			电流18
138	32			15			Shunt 19				Shunt 19		电流19			电流19
139	32			15			Shunt 20				Shunt 20		电流20			电流20
140	32			15			Shunt 21				Shunt 21		电流21			电流21
141	32			15			Shunt 22				Shunt 22		电流22			电流22
142	32			15			Shunt 23				Shunt 23		电流23			电流23
143	32			15			Shunt 24				Shunt 24		电流24			电流24
144	32			15			Shunt 25				Shunt 25		电流25			电流25
145	32			15			Voltage 1				Voltage 1		电压1			电压1
146	32			15			Voltage 2				Voltage 2		电压2			电压2
147	32			15			Voltage 3				Voltage 3		电压3			电压3
148	32			15			Voltage 4				Voltage 4		电压4			电压4
149	32			15			Voltage 5				Voltage 5		电压5			电压5
150	32			15			Voltage 6				Voltage 6		电压6			电压6
151	32			15			Voltage 7				Voltage 7		电压7			电压7
152	32			15			Voltage 8				Voltage 8		电压8			电压8
153	32			15			Voltage 9				Voltage 9		电压9			电压9
154	32			15			Voltage 10				Voltage 10		电压10			电压10
155	32			15			Voltage 11				Voltage 11		电压11			电压11
156	32			15			Voltage 12				Voltage 12		电压12			电压12
157	32			15			Voltage 13				Voltage 13		电压13			电压13
158	32			15			Voltage 14				Voltage 14		电压14			电压14
159	32			15			Voltage 15				Voltage 15		电压15			电压15
160	32			15			Voltage 16				Voltage 16		电压16			电压16
161	32			15			Voltage 17				Voltage 17		电压17			电压17
162	32			15			Voltage 18				Voltage 18		电压18			电压18
163	32			15			Voltage 19				Voltage 19		电压19			电压19
164	32			15			Voltage 20				Voltage 20		电压20			电压20
165	32			15			Voltage 21				Voltage 21		电压21			电压21
166	32			15			Voltage 22				Voltage 22		电压22			电压22
167	32			15			Voltage 23				Voltage 23		电压23			电压23
168	32			15			Voltage 24				Voltage 24		电压24			电压24
169	32			15			Voltage 25				Voltage 25		电压25			电压25

170	32			15			Current1 High 1 Current		Curr 1 Hi 1		电流1过流			电流1过流																																															
171	32			15			Current1 High 2 Current		Curr 1 Hi 2		电流1过过流			电流1过过流																																															
172	32			15			Current2 High 1 Current			Curr 2 Hi 1		电流2过流			电流2过流																																															
173	32			15			Current2 High 2 Current			Curr 2 Hi 2		电流2过过流			电流2过过流																																															
174	32			15			Current3 High 1 Current			Curr 3 Hi 1		电流3过流			电流3过流																																															
175	32			15			Current3 High 2 Current			Curr 3 Hi 2		电流3过过流			电流3过过流																																															
176	32			15			Current4 High 1 Current			Curr 4 Hi 1		电流4过流			电流4过流																																															
177	32			15			Current4 High 2 Current			Curr 4 Hi 2		电流4过过流			电流4过过流																																															
178	32			15			Current5 High 1 Current			Curr 5 Hi 1		电流5过流			电流5过流																																															
179	32			15			Current5 High 2 Current			Curr 5 Hi 2		电流5过过流			电流5过过流																																															
180	32			15			Current6 High 1 Current			Curr 6 Hi 1		电流6过流			电流6过流																																															
181	32			15			Current6 High 2 Current			Curr 6 Hi 2		电流6过过流			电流6过过流																																															
182	32			15			Current7 High 1 Current			Curr 7 Hi 1		电流7过流			电流7过流																																															
183	32			15			Current7 High 2 Current			Curr 7 Hi 2		电流7过过流			电流7过过流																																															
184	32			15			Current8 High 1 Current			Curr 8 Hi 1		电流8过流			电流8过流																																															
185	32			15			Current8 High 2 Current			Curr 8 Hi 2		电流8过过流			电流8过过流																																															
186	32			15			Current9 High 1 Current			Curr 9 Hi 1		电流9过流			电流9过流																																															
187	32			15			Current9 High 2 Current			Curr 9 Hi 2		电流9过过流			电流9过过流																																															
188	32			15			Current10 High 1 Current		Curr 10 Hi 1		电流10过流			电流10过流																																															
189	32			15			Current10 High 2 Current		Curr 10 Hi 2		电流10过过流			电流10过过流																																															
190	32			15			Current11 High 1 Current		Curr 11 Hi 1		电流11过流			电流11过流																																															
191	32			15			Current11 High 2 Current		Curr 11 Hi 2		电流11过过流			电流11过过流																																															
192	32			15			Current12 High 1 Current		Curr 12 Hi 1		电流12过流			电流12过流																																															
193	32			15			Current12 High 2 Current		Curr 12 Hi 2		电流12过过流			电流12过过流																																															
194	32			15			Current13 High 1 Current		Curr 13 Hi 1		电流13过流			电流13过流																																															
195	32			15			Current13 High 2 Current		Curr 13 Hi 2		电流13过过流			电流13过过流																																															
196	32			15			Current14 High 1 Current		Curr 14 Hi 1		电流14过流			电流14过流																																															
197	32			15			Current14 High 2 Current		Curr 14 Hi 2		电流14过过流			电流14过过流																																															
198	32			15			Current15 High 1 Current		Curr 15 Hi 1		电流15过流			电流15过流																																															
199	32			15			Current15 High 2 Current		Curr 15 Hi 2		电流15过过流			电流15过过流																																															
200	32			15			Current16 High 1 Current		Curr 16 Hi 1		电流16过流			电流16过流																																															
201	32			15			Current16 High 2 Current		Curr 16 Hi 2		电流16过过流			电流16过过流																																															
202	32			15			Current17 High 1 Current		Curr 17 Hi 1		电流17过流			电流17过流																																															
203	32			15			Current17 High 2 Current		Curr 17 Hi 2		电流17过过流			电流17过过流																																															
204	32			15			Current18 High 1 Current		Curr 18 Hi 1		电流18过流			电流18过流																																															
205	32			15			Current18 High 2 Current		Curr 18 Hi 2		电流18过过流			电流18过过流																																															
206	32			15			Current19 High 1 Current		Curr 19 Hi 1		电流19过流			电流19过流																																															
207	32			15			Current19 High 2 Current		Curr 19 Hi 2		电流19过过流			电流19过过流																																															
208	32			15			Current20 High 1 Current		Curr 20 Hi 1		电流20过流			电流20过流																																															
209	32			15			Current20 High 2 Current		Curr 20 Hi 2		电流20过过流			电流20过过流																																															
210	32			15			Current21 High 1 Current		Curr 21 Hi 1		电流21过流			电流21过流																																															
211	32			15			Current21 High 2 Current		Curr 21 Hi 2		电流21过过流			电流21过过流																																															
212	32			15			Current22 High 1 Current		Curr 22 Hi 1		电流22过流			电流22过流																																															
213	32			15			Current22 High 2 Current		Curr 22 Hi 2		电流22过过流			电流22过过流																																															
214	32			15			Current23 High 1 Current		Curr 23 Hi 1		电流23过流			电流23过流																																															
215	32			15			Current23 High 2 Current		Curr 23 Hi 2		电流23过过流			电流23过过流																																															
216	32			15			Current24 High 1 Current		Curr 24 Hi 1		电流24过流			电流24过流																																															
217	32			15			Current24 High 2 Current		Curr 24 Hi 2		电流24过过流			电流24过过流																																															
218	32			15			Current25 High 1 Current		Curr 25 Hi 1		电流25过流			电流25过流																																															
219	32			15			Current25 High 2 Current		Curr 25 Hi 2		电流25过过流			电流25过过流																																															

220	32			15			Current1 High 1 Current Limit		Curr1 Hi 1 Lmt		电流1过流点		电流1过电流点																															
221	32			15			Current1 High 2 Current Limit		Curr1 Hi 2 Lmt		电流1过过电流点		电流1过过流点																															
222	32			15			Current2 High 1 Current Limit		Curr2 Hi 1 Lmt		电流2过流点		电流2过流点																															
223	32			15			Current2 High 2 Current Limit		Curr2 Hi 2 Lmt		电流2过过流点		电流2过过流点																															
224	32			15			Current3 High 1 Current Limit		Curr3 Hi 1 Lmt		电流3过流点		电流3过流点																															
225	32			15			Current3 High 2 Current Limit		Curr3 Hi 2 Lmt		电流3过过流点		电流3过过流点																															
226	32			15			Current4 High 1 Current Limit		Curr4 Hi 1 Lmt		电流4过流点		电流4过流点																															
227	32			15			Current4 High 2 Current Limit		Curr4 Hi 2 Lmt		电流4过过流点		电流4过过流点																															
228	32			15			Current5 High 1 Current Limit		Curr5 Hi 1 Lmt		电流5过流点		电流5过流点																															
229	32			15			Current5 High 2 Current Limit		Curr5 Hi 2 Lmt		电流5过过流点		电流5过过流点																															
230	32			15			Current6 High 1 Current Limit		Curr6 Hi 1 Lmt		电流6过流点		电流6过流点																															
231	32			15			Current6 High 2 Current Limit		Curr6 Hi 2 Lmt		电流6过过流点		电流6过过流点																															
232	32			15			Current7 High 1 Current Limit		Curr7 Hi 1 Lmt		电流7过流点		电流7过流点																															
233	32			15			Current7 High 2 Current Limit		Curr7 Hi 2 Lmt		电流7过过流点		电流7过过流点																															
234	32			15			Current8 High 1 Current Limit		Curr8 Hi 1 Lmt		电流8过流点		电流8过流点																															
235	32			15			Current8 High 2 Current Limit		Curr8 Hi 2 Lmt		电流8过过流点		电流8过过流点																															
236	32			15			Current9 High 1 Current Limit		Curr9 Hi 1 Lmt		电流9过流点		电流9过流点																															
237	32			15			Current9 High 2 Current Limit		Curr9 Hi 2 Lmt		电流9过过流点		电流9过过流点																															
238	32			15			Current10 High 1 Current Limit		Curr10 Hi 1 Lmt		电流10过流点		电流10过流点																															
239	32			15			Current10 High 2 Current Limit		Curr10 Hi 2 Lmt		电流10过过流点		电流10过过流点																															
240	32			15			Current11 High 1 Current Limit		Curr11 Hi 1 Lmt		电流11过流点		电流11过流点																															
241	32			15			Current11 High 2 Current Limit		Curr11 Hi 2 Lmt		电流11过过流点		电流11过过流点																															
242	32			15			Current12 High 1 Current Limit		Curr12 Hi 1 Lmt		电流12过流点		电流12过流点																															
243	32			15			Current12 High 2 Current Limit		Curr12 Hi 2 Lmt		电流12过过流点		电流12过过流点																															
244	32			15			Current13 High 1 Current Limit		Curr13 Hi 1 Lmt		电流13过流点		电流13过流点																															
245	32			15			Current13 High 2 Current Limit		Curr13 Hi 2 Lmt		电流13过过流点		电流13过过流点																															
246	32			15			Current14 High 1 Current Limit		Curr14 Hi 1 Lmt		电流14过流点		电流14过流点																															
247	32			15			Current14 High 2 Current Limit		Curr14 Hi 2 Lmt		电流14过过流点		电流14过过流点																															
248	32			15			Current15 High 1 Current Limit		Curr15 Hi 1 Lmt		电流15过流点		电流15过流点																															
249	32			15			Current15 High 2 Current Limit		Curr15 Hi 2 Lmt		电流15过过流点		电流15过过流点																															
250	32			15			Current16 High 1 Current Limit		Curr16 Hi 1 Lmt		电流16过流点		电流16过流点																															
251	32			15			Current16 High 2 Current Limit		Curr16 Hi 2 Lmt		电流16过过流点		电流16过过流点																															
252	32			15			Current17 High 1 Current Limit		Curr17 Hi 1 Lmt		电流17过流点		电流17过流点																															
253	32			15			Current17 High 2 Current Limit		Curr17 Hi 2 Lmt		电流17过过流点		电流17过过流点																															
254	32			15			Current18 High 1 Current Limit		Curr18 Hi 1 Lmt		电流18过流点		电流18过流点																															
255	32			15			Current18 High 2 Current Limit		Curr18 Hi 2 Lmt		电流18过过流点		电流18过过流点																															
256	32			15			Current19 High 1 Current Limit		Curr19 Hi 1 Lmt		电流19过流点		电流19过流点																															
257	32			15			Current19 High 2 Current Limit		Curr19 Hi 2 Lmt		电流19过过流点		电流19过过流点																															
258	32			15			Current20 High 1 Current Limit		Curr20 Hi 1 Lmt		电流20过流点		电流20过流点																															
259	32			15			Current20 High 2 Current Limit		Curr20 Hi 2 Lmt		电流20过过流点		电流20过过流点																															
260	32			15			Current21 High 1 Current Limit		Curr21 Hi 1 Lmt		电流21过流点		电流21过流点																															
261	32			15			Current21 High 2 Current Limit		Curr21 Hi 2 Lmt		电流21过过流点		电流21过过流点																															
262	32			15			Current22 High 1 Current Limit		Curr22 Hi 1 Lmt		电流22过流点		电流22过流点																															
263	32			15			Current22 High 2 Current Limit		Curr22 Hi 2 Lmt		电流22过过流点		电流22过过流点																															
264	32			15			Current23 High 1 Current Limit		Curr23 Hi 1 Lmt		电流23过流点		电流23过流点																															
265	32			15			Current23 High 2 Current Limit		Curr23 Hi 2 Lmt		电流23过过流点		电流23过过流点																															
266	32			15			Current24 High 1 Current Limit		Curr24 Hi 1 Lmt		电流24过流点		电流24过流点																															
267	32			15			Current24 High 2 Current Limit		Curr24 Hi 2 Lmt		电流24过过流点		电流24过过流点																															
268	32			15			Current25 High 1 Current Limit		Curr25 Hi 1 Lmt		电流25过流点		电流25过流点																															
269	32			15			Current25 High 2 Current Limit		Curr25 Hi 2 Lmt		电流25过过流点		电流25过过流点																																

270	32			15			Current1 Break Value				Curr1 Brk Val		电流1电流告警阈值		电流1告警阈值																														
271	32			15			Current2 Break Value				Curr2 Brk Val		电流2电流告警阈值		电流2告警阈值																														
272	32			15			Current3 Break Value				Curr3 Brk Val		电流3电流告警阈值		电流3告警阈值																														
273	32			15			Current4 Break Value				Curr4 Brk Val		电流4电流告警阈值		电流4告警阈值																														
274	32			15			Current5 Break Value				Curr5 Brk Val		电流5电流告警阈值		电流5告警阈值																														
275	32			15			Current6 Break Value				Curr6 Brk Val		电流6电流告警阈值		电流6告警阈值																														
276	32			15			Current7 Break Value				Curr7 Brk Val		电流7电流告警阈值		电流7告警阈值																														
277	32			15			Current8 Break Value				Curr8 Brk Val		电流8电流告警阈值		电流8告警阈值																														
278	32			15			Current9 Break Value				Curr9 Brk Val		电流9电流告警阈值		电流9告警阈值																														
279	32			15			Current10 Break Value				Curr10 Brk Val		电流10电流告警阈值		电流10告警阈值																														
280	32			15			Current11 Break Value				Curr11 Brk Val		电流11电流告警阈值		电流11告警阈值																														
281	32			15			Current12 Break Value				Curr12 Brk Val		电流12电流告警阈值		电流12告警阈值																														
282	32			15			Current13 Break Value				Curr13 Brk Val		电流13电流告警阈值		电流13告警阈值																														
283	32			15			Current14 Break Value				Curr14 Brk Val		电流14电流告警阈值		电流14告警阈值																														
284	32			15			Current15 Break Value				Curr15 Brk Val		电流15电流告警阈值		电流15告警阈值																														
285	32			15			Current16 Break Value				Curr16 Brk Val		电流16电流告警阈值		电流16告警阈值																														
286	32			15			Current17 Break Value				Curr17 Brk Val		电流17电流告警阈值		电流17告警阈值																														
287	32			15			Current18 Break Value				Curr18 Brk Val		电流18电流告警阈值		电流18告警阈值																														
288	32			15			Current19 Break Value				Curr19 Brk Val		电流19电流告警阈值		电流19告警阈值																														
289	32			15			Current20 Break Value				Curr20 Brk Val		电流20电流告警阈值		电流20告警阈值																														
290	32			15			Current21 Break Value				Curr21 Brk Val		电流21电流告警阈值		电流21告警阈值																														
291	32			15			Current22 Break Value				Curr22 Brk Val		电流22电流告警阈值		电流22告警阈值																														
292	32			15			Current23 Break Value				Curr23 Brk Val		电流23电流告警阈值		电流23告警阈值																														
293	32			15			Current24 Break Value				Curr24 Brk Val		电流24电流告警阈值		电流24告警阈值																														
294	32			15			Current25 Break Value				Curr25 Brk Val		电流25电流告警阈值		电流25告警阈值																														

295	32			15			Shunt 1 Voltage					Shunt1 Voltage			分流器1电压		分流器1电压
296	32			15			Shunt 1 Current					Shunt1 Current			分流器1电流		分流器1电流
297	32			15			Shunt 2 Voltage					Shunt2 Voltage			分流器2电压		分流器2电压
298	32			15			Shunt 2 Current					Shunt2 Current			分流器2电流		分流器2电流
299	32			15			Shunt 3 Voltage					Shunt3 Voltage			分流器3电压		分流器3电压
300	32			15			Shunt 3 Current					Shunt3 Current			分流器3电流		分流器3电流
301	32			15			Shunt 4 Voltage					Shunt4 Voltage			分流器4电压		分流器4电压
302	32			15			Shunt 4 Current					Shunt4 Current			分流器4电流		分流器4电流
303	32			15			Shunt 5 Voltage					Shunt5 Voltage			分流器5电压		分流器5电压
304	32			15			Shunt 5 Current					Shunt5 Current			分流器5电流		分流器5电流
305	32			15			Shunt 6 Voltage					Shunt6 Voltage			分流器6电压		分流器6电压
306	32			15			Shunt 6 Current					Shunt6 Current			分流器6电流		分流器6电流
307	32			15			Shunt 7 Voltage					Shunt7 Voltage			分流器7电压		分流器7电压
308	32			15			Shunt 7 Current					Shunt7 Current			分流器7电流		分流器7电流
309	32			15			Shunt 8 Voltage					Shunt8 Voltage			分流器8电压		分流器8电压
310	32			15			Shunt 8 Current					Shunt8 Current			分流器8电流		分流器8电流
311	32			15			Shunt 9 Voltage					Shunt9 Voltage			分流器9电压		分流器9电压
312	32			15			Shunt 9 Current					Shunt9 Current			分流器9电流		分流器9电流
313	32			15			Shunt 10 Voltage				Shunt10 Voltage			分流器10电压		分流器10电压
314	32			15			Shunt 10 Current				Shunt10 Current			分流器10电流		分流器10电流
315	32			15			Shunt 11 Voltage				Shunt11 Voltage			分流器11电压		分流器11电压
316	32			15			Shunt 11 Current				Shunt11 Current			分流器11电流		分流器11电流
317	32			15			Shunt 12 Voltage				Shunt12 Voltage			分流器12电压		分流器12电压
318	32			15			Shunt 12 Current				Shunt12 Current			分流器12电流		分流器12电流
319	32			15			Shunt 13 Voltage				Shunt13 Voltage			分流器13电压		分流器13电压
320	32			15			Shunt 13 Current				Shunt13 Current			分流器13电流		分流器13电流
321	32			15			Shunt 14 Voltage				Shunt14 Voltage			分流器14电压		分流器14电压
322	32			15			Shunt 14 Current				Shunt14 Current			分流器14电流		分流器14电流
323	32			15			Shunt 15 Voltage				Shunt15 Voltage			分流器15电压		分流器15电压
324	32			15			Shunt 15 Current				Shunt15 Current			分流器15电流		分流器15电流
325	32			15			Shunt 16 Voltage				Shunt16 Voltage			分流器16电压		分流器16电压
326	32			15			Shunt 16 Current				Shunt16 Current			分流器16电流		分流器16电流
327	32			15			Shunt 17 Voltage				Shunt17 Voltage			分流器17电压		分流器17电压
328	32			15			Shunt 17 Current				Shunt17 Current			分流器17电流		分流器17电流
329	32			15			Shunt 18 Voltage				Shunt18 Voltage			分流器18电压		分流器18电压
330	32			15			Shunt 18 Current				Shunt18 Current			分流器18电流		分流器18电流
331	32			15			Shunt 19 Voltage				Shunt19 Voltage			分流器19电压		分流器19电压
332	32			15			Shunt 19 Current				Shunt19 Current			分流器19电流		分流器19电流
333	32			15			Shunt 20 Voltage				Shunt20 Voltage			分流器20电压		分流器20电压
334	32			15			Shunt 20 Current				Shunt20 Current			分流器20电流		分流器20电流
335	32			15			Shunt 21 Voltage				Shunt21 Voltage			分流器21电压		分流器21电压
336	32			15			Shunt 21 Current				Shunt21 Current			分流器21电流		分流器21电流
337	32			15			Shunt 22 Voltage				Shunt22 Voltage			分流器22电压		分流器22电压
338	32			15			Shunt 22 Current				Shunt22 Current			分流器22电流		分流器22电流
339	32			15			Shunt 23 Voltage				Shunt23 Voltage			分流器23电压		分流器23电压
340	32			15			Shunt 23 Current				Shunt23 Current			分流器23电流		分流器23电流
341	32			15			Shunt 24 Voltage				Shunt24 Voltage			分流器24电压		分流器24电压
342	32			15			Shunt 24 Current				Shunt24 Current			分流器24电流		分流器24电流
343	32			15			Shunt 25 Voltage				Shunt25 Voltage			分流器25电压		分流器25电压
344	32			15			Shunt 25 Current				Shunt25 Current			分流器25电流		分流器25电流

345	32			15			Shunt Size Settable				Shunt Settable			分流器系数可设		分流器系数可设
346	32			15			By Software					By Software			软件设置		软件设置																														
347	32			15			By Dip-Switch					By Dip-Switch			拨码设置		拨码设置																														
348	32			15			Not Supported					Not Supported			不支持			不支持
349	32			15			Shunt Coefficient Conflict			Shunt Conflict			分流器系数改变		分流器系数改变																															
350	32			15			Disabled					Disabled			禁止		禁止																														
351	32			15			Enable						Enable				使能		使能																														
352	32			15			Shunt 1						Shunt 1					分流器1			分流器1	
353	32			15			Shunt 2						Shunt 2					分流器2			分流器2	
354	32			15			Shunt 3						Shunt 3					分流器3			分流器3	
355	32			15			Shunt 4						Shunt 4					分流器4			分流器4	
356	32			15			Shunt 5						Shunt 5					分流器5			分流器5	
357	32			15			Shunt 6						Shunt 6					分流器6			分流器6	
358	32			15			Shunt 7						Shunt 7					分流器7			分流器7	
359	32			15			Shunt 8						Shunt 8					分流器8			分流器8	
360	32			15			Shunt 9						Shunt 9					分流器9			分流器9	
361	32			15			Shunt 10					Shunt 10				分流器10			分流器10
362	32			15			Shunt 11					Shunt 11				分流器11			分流器11
363	32			15			Shunt 12					Shunt 12				分流器12			分流器12
364	32			15			Shunt 13					Shunt 13				分流器13			分流器13
365	32			15			Shunt 14					Shunt 14				分流器14			分流器14
366	32			15			Shunt 15					Shunt 15				分流器15			分流器15
367	32			15			Shunt 16					Shunt 16				分流器16			分流器16
368	32			15			Shunt 17					Shunt 17				分流器17			分流器17
369	32			15			Shunt 18					Shunt 18				分流器18			分流器18
370	32			15			Shunt 19					Shunt 19				分流器19			分流器19
371	32			15			Shunt 20					Shunt 20				分流器20			分流器20
372	32			15			Shunt 21					Shunt 21				分流器21			分流器21
373	32			15			Shunt 22					Shunt 22				分流器22			分流器22
374	32			15			Shunt 23					Shunt 23				分流器23			分流器23
375	32			15			Shunt 24					Shunt 24				分流器24			分流器24
376	32			15			Shunt 25					Shunt 25				分流器25			分流器25
377	32			15			Not Used				Not Used			未使用				未使用
378	32			15			General					General				常规				常规
379	32			15			Load					Load				负载				负载
380	32			15			Load 1					Load 1		负载电流1			负载电流1
381	32			15			Load 2					Load 2		负载电流2			负载电流2
382	32			15			Load 3					Load 3		负载电流3			负载电流3
383	32			15			Load 4					Load 4		负载电流4			负载电流4
384	32			15			Load 5				Load 5		负载电流5			负载电流5
385	32			15			Load 6				Load 6		负载电流6			负载电流6
386	32			15			Load 7				Load 7		负载电流7			负载电流7
387	32			15			Load 8				Load 8		负载电流8			负载电流8
388	32			15			Load 9				Load 9		负载电流9			负载电流9
389	32			15			Load 10			Load 10		负载电流10			负载电流10
390	32			15			Load 11			Load 11		负载电流11			负载电流11
391	32			15			Load 12			Load 12		负载电流12			负载电流12
392	32			15			Load 13			Load 13		负载电流13			负载电流13
393	32			15			Load 14			Load 14		负载电流14			负载电流14
394	32			15			Load 15			Load 15		负载电流15			负载电流15
395	32			15			Load 16			Load 16		负载电流16			负载电流16
396	32			15			Load 17			Load 17		负载电流17			负载电流17
397	32			15			Load 18			Load 18		负载电流18			负载电流18
398	32			15			Load 19			Load 19		负载电流19			负载电流19
399	32			15			Load 20			Load 20		负载电流20			负载电流20
400	32			15			Load 21			Load 21		负载电流21			负载电流21
401	32			15			Load 22			Load 22		负载电流22			负载电流22
402	32			15			Load 23			Load 23		负载电流23			负载电流23
403	32			15			Load 24			Load 24		负载电流24			负载电流24
404	32			15			Load 25			Load 25		负载电流25			负载电流25

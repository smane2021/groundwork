﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# ABBR_IN_EN: Abbreviated English name
# ABBR_IN_LOCALE: Abbreviated locale name
# ITEM_DESCRIPTION: The description of the resource item
#
[LOCALE_LANGUAGE]
zh

#1. Define the number of the self define multi language display items
[SELF_DEFINE_LANGUAGE_ITEM_NUMBER]
58

[SELF_DEFINE_LANGUAGE_ITEM_INFO]
#Sequence ID	#RES_ID		MAX_LEN_OF_BYTE_ABBR	ABBR_IN_EN		ABBR_IN_LOCALE		ITEM_DESCRIPTION
1		1		32			Main Menu		主菜单			Main Menu
2		2		32			Status			运行信息		Running Info
3		3		32			Manual			控制输出		Maintain
4		4		32			Settings		参数设置		Parameter Set
5		5		32			ECO Mode		节能管理		Energy Saving Parameter Set
6		6		32			Quick Settings		快速设置		Quick Settings Menu
7		7		32			Quick Settings		快速设置		Quick Settings Menu
8		8		32			Test Menu 1		自测试菜单1		Menu for self test
9		9		32			Test Menu 2		自测试菜单2		Menu for self test
10		10		32			Man/Auto Set		手自动设置		Man/Auto Set in Maintain SubMenu
#
11		11		32			Select User		选择用户		Select user in password input screen
12		12		32			Enter Password		输入密码		Enter password in password input screen
#
13		13		32			Slave Settings		从机参数设置		Slave Parameter Set
#
21		21		32			Active Alarms		当前告警		Active Alarms
22		22		32			Alarm History		历史告警		Alarm History
23		23		32			No Active Alarm		无实时告警		No Active Alarm
24		24		32			No Alarm History	无历史告警		No Alarm History
#
31		31		32			Acknowledge Info	确认信息		Acknowledge Info
32		32		32			ENT Confirm		ENT键继续		ENT to run
33		33		32			ESC Cancel		ESC键放弃		ESC Quit
34		34		32			Prompt Info		提示信息		Prompt Info
35		35		32			Password Error		密码错误		Password Error!
36		36		32			ESC or ENT Ret		ESC或ENT键返回		ESC or ENT Ret
37		37		32			No Privilege		无此权限		No Privilege
38		38		32			No Item Info		无子项信息		No Item Info
39		39		32			Switch to Next		切换到下一个		Switch To Next equip
40		40		32			Switch to Prev		切换到前一个		Switch To Previous equip
41		41		32			Disabled Set		此信号不可设		Disabled Set
42		42		32			Disabled Ctrl		此信号不可控		Disabled Ctrl
43		43		32			Conflict Setting	不满足关联条件		Conflict setting of signal relationship
44		44		32			Failed to Set		控制或设置失败		Failed to Control or set
45		45		32			HW Protect		硬件保护		Hardware Protect status
46		46		32			Reboot System		系统重新启动		Reboot System
47		47		32			App is Auto		应用程序正在		App is Auto configing
48		48		32			Configuring		自动配置过程中		App is Auto configing
49		49		32			Copying File		拷贝配置文件		Copy config file
50		50		32			Please wait...		请稍候．．．		Please Wait...
51		51		32			Switch to Set		设置：			Switch to Set Alarm Level or Grade
52		52		32			DHCP is Open		DHCP已经开启
53		53		32			Cannot set.		不能设置！
54		54		32			Download Entire	下载完成		Download Config File completely
55		55		32			Reboot Validate	重启后生效		Validate after reboot
#
#
#以下为Barcode信号，其ID不得大于256
61		61		32			Sys Inventory		产品信息		Product info of Devices
62		62		32			Device Name		设备名称		Device Name
63		63		32			Part Number		产品编号		Part Number
64		64		32			Product Ver		产品版本		HW Version
65		65		32			SW Version		软件版本		SW Version
66		66		32			Serial Number		产品序列号		Serial Number
#
#
80		80		32			Chinese			简体中文		Chinese
81		81		32			Reboot Validate		重启后生效		Reboot Validate
#
82		82		32			Alm Severity		告警级别		Alarm Grade
83		83		32			None			不告警			No Alarm
84		84		32			Observation		一般告警		Observation alarm
85		85		32			Major			重要告警		Major alarm
86		86		32			Critical		紧急告警		Critical alarm
#
87		87		32			Alarm Relay		告警继电器		Alarm Relay Settings
88		88		32			None			无			No Relay Output
89		89		32			Relay 1			继电器1			Relay Output 1 of IB
90		90		32			Relay 2			继电器2			Relay Output 2 of IB
91		91		32			Relay 3			继电器3			Relay Output 3 of IB
92		92		32			Relay 4			继电器4			Relay Output 4 of IB
93		93		32			Relay 5			继电器5			Relay Output 5 of IB
94		94		32			Relay 6			继电器6			Relay Output 6 of IB
95		95		32			Relay 7			继电器7			Relay Output 7 of IB
96		96		32			Relay 8			继电器8			Relay Output 8 of IB
97		97		32			Relay 9			继电器9			Relay Output 1 of EIB
98		98		32			Relay 10		继电器10		Relay Output 2 of EIB
99		99		32			Relay 11		继电器11		Relay Output 3 of EIB
100		100		32			Relay 12		继电器12		Relay Output 4 of EIB
101		101		32			Relay 13		继电器13		Relay Output 5 of EIB
#
102		102		32			Alarm Param		告警控制		Alarm Param
103		103		32			Alarm Voice		告警音长		Alarm Voice
104		104		32			Block Alarm		阻塞当前告警		Block Alarm
105		105		32			Clr Alm Hist		清历史告警		Clear History alarm
106		106		32			Yes			是			Yes
107		107		32			No			否			No
#
108		108		32			Alarm Voltage		告警电平		Alarm Voltage Level of IB
#
#
121		121		32			Sys Settings		系统参数		System Param
122		122		32			Language		操作语言		Current language displayed in LCD screen
123		123		32			Time Zone		显示时区		Time Zone
124		124		32			Date			系统日期		Set ACU+ Date, according to time zone
125		125		32			Time			系统时间		Set Time, accoring to time zone
126		126		32			Reload Config		恢复默认配置		Reload Default Configuration
127		127		32			Keypad Voice		按键声音		Keypad Voice
128		128		32			Download Config		下载配置文件		Download config file
129		129		32			Auto Config		自动配置		Auto config
#
#
141		141		32			Communication		通讯参数		Communication Parameter
#
142		142		32			DHCP			DHCP功能		DHCP Function
143		143		32			IP Address		IP地址			IP Address of ACU+
144		144		32			Subnet Mask		子网掩码		Subnet Mask of ACU+
145		145		32			Default Gateway		默认网关		Default Gateway of ACU+
#
146		146		32			Self Address		本机地址		Self Addr
147		147		32			Port Type		端口类型		Connection Mode
148		148		32			Port Param		端口参数		Port Parameter
149		149		32			Alarm Report 		是否告警回叫		Alarm Report 
150		150		32			Dial Times 		回叫次数		Dialing Attempt Times
151		151		32			Dial Interval		回叫间隔时间		Dialing Interval
152		152		32			1st Phone Num		回叫号码1		First Call Back Phone  Num
153		153		32			2nd Phone  Num		回叫号码2		Second Call Back Phone  Num
154		154		32			3rd Phone  Num		回叫号码3		Third Call Back Phone  Num
#
161		161		32			Enabled			开启			Enable DHCP
162		162		32			Disabled		关闭			Disable DHCP
163		163		32			Error			错误			DHCP function error
164		164		32			RS-232			RS-232			YDN23 Connection Mode RS-232
165		165		32			Modem			Modem			YDN23 Connection Mode MODEM
166		166		32			Ethernet		Ethernet		YDN23 Connection Mode Ethernet
167		167		32			5050			5050			Ethernet Port Number
168		168		32			2400,n,8,1		2400,n,8,1		Serial Port Parameter
169		169		32			4800,n,8,1		4800,n,8,1		Serial Port Parameter
170		170		32			9600,n,8,1		9600,n,8,1		Serial Port Parameter
171		171		32			19200,n,8,1		19200,n,8,1		Serial Port Parameter
172		172		32			38400,n,8,1		38400,n,8,1		Serial Port Parameter
#
# The next level of Battery Group
201		201		32			Basic			基本参数		Sub Menu Resouce of BattGroup Para Setting
202		202		32			Charge			充电参数		Sub Menu Resouce of BattGroup Para Setting
203		203		32			Test			电池测试		Sub Menu Resouce of BattGroup Para Setting
204		204		32			Temp Comp		温度补偿		Sub Menu Resouce of BattGroup Para Setting
205		205		32			Capacity		容量计算		Sub Menu Resouce of BattGroup Para Setting
# The next level of Power System
206		206		32			General			基本参数		Sub Menu Resouce of PowerSystem Para Setting
207		207		32			Power Split		并机参数		Sub Menu Resouce of PowerSystem Para Setting
208		208		32			Temp Probe(s)		温度参数		Sub Menu Resouce of PowerSystem Para Setting
# ENERGY SAVING
209		209		32			ECO Mode		节能设置		Sub Menu Resouce of ENERGY SAVING
#
#以下用于：在默认屏按ESC，显示设备及配置信息
301		301		32			Serial Num		产品序列号		Serial Number
302		302		32			HW Ver			硬件版本		Hardware Version
303		303		32			SW Ver			软件版本		Software Version
304		304		32			MAC Addr		MAC地址			MAC Addr
305		305		32			File Sys		文件系统		File System Revision
306		306		32			Device Name		产品型号		Product Model
307		307		32			Config			配置			Solution Config File Version
#
#
501		501		32			LCD Size		屏幕大小		Set the LCD Height
502		502		32			128x64			128x64			Set the LCD Height to 128 X 64
503		503		32			128x128			128x128			Set the LCD Height to 128 X 128
504		504		32			LCD Rotation		屏幕角度		Set the LCD Rotation
505		505		32			0 deg			0度			Set the LCD Rotation to 0 degree
506		506		32			90 deg			90度			Set the LCD Rotation to 90 degree
507		507		32			180 deg			180度			Set the LCD Rotation to 180 degree
508		508		32			270 deg			270度			Set the LCD Rotation to 270 degree
#
#
601		601		32			All Rect Ctrl		所有模块控制
602		602		32			All Rect Set		所有模块设置
#
621		621		32			Rectifier		整流模块
622		622		32			Battery			电池
623		623		32			LVD			LVD
624		624		32			Rect AC			模块交流
625		625		32			Converter		变流模块
626		626		32			SMIO			SMIO
627		627		32			Diesel			油机
628		628		32			Rect Group 2		模块组2
629		629		32			Rect Group 3		模块组3
630		630		32			Rect Group 4		模块组4
631		631		32			All Conv Ctrl		所有模块控制
632		632		32			All Conv Set		所有模块设置
633		633		32			SMDU			SMDU
#
1001		1001		32			Auto/Manual		手自动切换
1002		1002		32			ECO Mode Set		节能设置
1003		1003		32			FLT/EQ Voltage		均浮充电压
1004		1004		32			FLT/EQ Change		均浮充转换
1005		1005		32			Temp Comp		温补设置
1006		1006		32			Work Mode Set		工作模式设置
1007		1007		32			Maintenance		控制输出
1008		1008		32			Energy Saving		节能参数
1009		1009		32			Alarm Settings		告警参数
1010		1010		32			Rect Settings		模块参数
1011		1011		32			Batt Settings		电池参数
1012		1012		32			Batt1 Settings		电池串1参数
1013		1013		32			Batt2 Settings		电池串2参数
1014		1014		32			LVD Settings		LVD参数
1015		1015		32			AC Settings		交流参数
1016		1016		32			Template 1		模块1
1017		1017		32			Template 2		模块2
1018		1018		32			Template N		模块N
#
1101		1101		32			Batt1			电池1
1102		1102		32			Batt2			电池2
1103		1103		32			Comp			温补
1104		1104		32			Amb			环境
1105		1105		32			Remain			剩余
1106		1106		32			RectNum			模块数


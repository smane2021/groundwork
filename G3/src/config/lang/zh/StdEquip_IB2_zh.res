﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15		IB2-2				IB2-2			IB2-2			IB2-2
8		32			15			DI1			DI1		数字量输入1		数字输入1
9		32			15			DI2			DI2		数字量输入2		数字输入2
10		32			15			DI3			DI3		数字量输入3		数字输入3
11		32			15			DI4			DI4		数字量输入4		数字输入4
12		32			15			DI5			DI5		数字量输入5		数字输入5
13		32			15			DI6			DI6		数字量输入6		数字输入6
14		32			15			DI7			DI7		数字量输入7		数字输入7
15		32			15			DI8			DI8		数字量输入8		数字输入8
16		32			15			Not Active			Not Active		断开			断开                      
17		32			15			Active				Active			闭合			闭合
18		32			15			DO1-Relay Output		DO1-RelayOutput		继电器1			继电器1                 
19		32			15			DO2-Relay Output		DO2-RelayOutput		继电器2			继电器2                 
20		32			15			DO3-Relay Output		DO3-RelayOutput		继电器3			继电器3                 
21		32			15			DO4-Relay Output		DO4-RelayOutput		继电器4			继电器4                 
22		32			15			DO5-Relay Output		DO5-RelayOutput		继电器5			继电器5                 
23		32			15			DO6-Relay Output		DO6-RelayOutput		继电器6			继电器6                 
24		32			15			DO7-Relay Output		DO7-RelayOutput		继电器7			继电器7                 
25		32			15			DO8-Relay Output		DO8-RelayOutput		继电器8			继电器8                 
26		32			15			DI1 Alarm State	DI1 Alarm State		数字量输入1告警条件	输入1告警条件
27		32			15			DI2 Alarm State	DI2 Alarm State		数字量输入2告警条件	输入2告警条件
28		32			15			DI3 Alarm State	DI3 Alarm State		数字量输入3告警条件	输入3告警条件
29		32			15			DI4 Alarm State	DI4 Alarm State		数字量输入4告警条件	输入4告警条件
30		32			15			DI5 Alarm State	DI5 Alarm State		数字量输入5告警条件	输入5告警条件
31		32			15			DI6 Alarm State	DI6 Alarm State		数字量输入6告警条件	输入6告警条件
32		32			15			DI7 Alarm State	DI7 Alarm State		数字量输入7告警条件	输入7告警条件
33		32			15			DI8 Alarm State	DI8 Alarm State		数字量输入8告警条件	输入8告警条件
34		32			15			State				State			State			State
35		32			15			Communication Fail		Comm Fail		IB通信中断		IB通信中断
36		32			15			Barcode				Barcode			Barcode			Barcode
37		32			15			On				On			开			开
38		32			15			Off				Off			关			关
39		32			15			High				High			高电平			高电平
40		32			15			Low				Low			低电平			低电平
41		32			15			DI1 Alarm		DI1 Alarm		数字量输入1告警		输入1告警
42		32			15			DI2 Alarm		DI2 Alarm		数字量输入2告警		输入2告警
43		32			15			DI3 Alarm		DI3 Alarm		数字量输入3告警		输入3告警
44		32			15			DI4 Alarm		DI4 Alarm		数字量输入4告警		输入4告警
45		32			15			DI5 Alarm		DI5 Alarm		数字量输入5告警		输入5告警
46		32			15			DI6 Alarm		DI6 Alarm		数字量输入6告警		输入6告警
47		32			15			DI7 Alarm		DI7 Alarm		数字量输入7告警		输入7告警
48		32			15			DI8 Alarm		DI8 Alarm		数字量输入8告警		输入8告警
78		32			15			IB2-2 DO1 Test			IB2-2 DO1 Test		测试继电器1		测试继电器1
79		32			15			IB2-2 DO2 Test			IB2-2 DO2 Test		测试继电器2		测试继电器2
80		32			15			IB2-2 DO3 Test			IB2-2 DO3 Test		测试继电器3		测试继电器3
81		32			15			IB2-2 DO4 Test			IB2-2 DO4 Test		测试继电器4		测试继电器4
82		32			15			IB2-2 DO5 Test			IB2-2 DO5 Test		测试继电器5		测试继电器5
83		32			15			IB2-2 DO6 Test			IB2-2 DO6 Test		测试继电器6		测试继电器6
84		32			15			IB2-2 DO7 Test			IB2-2 DO7 Test		测试继电器7		测试继电器7
85		32			15			IB2-2 DO8 Test			IB2-2 DO8 Test		测试继电器8		测试继电器8
86		32			15			Temp1				Temp1			温度1			温度1
87		32			15			Temp2				Temp2			温度2			温度2







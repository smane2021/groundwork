﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support           
# FULL_IN_LOCALE2: Full name in locale2 language 
# ABBR_IN_LOCALE2: Abbreviated locale2 name      
#
[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID	MAX_LEN_FULL	MAX_LEN_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32		15		Battery Voltage				Battery Voltage		电池电压			电池电压
2	32		15		Total Battery Current			Total Batt Curr		电池总电流			电池总电流    
3	32		15		Battery Temperature			Battery Temp		电池温度			电池温度      
4	32		15		Short BOD Total Time			Short BOD Time		浅放电总时间			浅放电总时间  
5	32		15		Long BOD Total Time			Long BOD Time		深放电总时间			深放电总时间  
6	32		15		Full BOD Total Time			Full BOD Time		过放电总时间			过放电总时间  
7	32		15		Short BOD Counter			ShortBODCounter		浅放电总次数			浅放电总次数  
8	32		15		Long BOD Counter			LongBODCounter		深放电总次数			深放电总次数  
9	32		15		Full BOD Counter			FullBODCounter		过放电总次数			过放电总次数  
14	32		15		End Test on Voltage			End Test Volt		电压测试结束			电压测试结束  
15	32		15		Discharge Current Imbalance		Dsch Curr Imb		放电电流不平衡			放电电流不平衡
19	32		15		Abnormal Battery Current		AbnormalBatCurr		电池电流异常			电池电流异常  
21	32		15		System Current Limit Active		SystemCurrLimit	系统限流			系统限流      
23	32		15		Equalize/Float Charge Control		EQ/FLT Control		均浮充控制			均浮充控制    
25	32		15		Battery Test Control			BattTestControl		电池测试启停			电池测试启停  
30	32		15		Number of Battery Blocks		Num Batt Blocks		每组电池的块数			每组电池的块数
31	32		15		End Test Time				End Test Time		电池测试时间			电池测试时间  
32	32		15		End Test Voltage			End Test Volt		测试结束电压			测试结束电压  
33	32		15		End Test Capacity			EndTestCapacity		测试结束容量			测试结束容量  
34	32		15		Constant Current Test			Const Curr Test		恒流测试允许			恒流测试允许
35	32		15		Constant Current Test Current		ConstCurrT Curr		恒流测试电流			恒流测试电流	
37	32		15		AC Fail Test				AC Fail Test		停电测试允许			停电测试允许	
38	32		15		Short Test				Short Test		短测试允许			短测试允许	
39	32		15		Short Test Cycle			ShortTest Cycle		短测试启动条件			短测试启动条件	
40	32		15		Short Test Max Difference Curr		Max Diff Curr		短测试最大流差			短测试最大流差	
41	32		15		Short Test Time				Short Test Time		短测试时间			短测试时间	
42	32		15		Float Charge Voltage			Float Voltage		浮充电压			浮充电压	
43	32		15		Equalize Charge Voltage			EQ Voltage		均充电压			均充电压	
44	32		15		Maximum Equalize Charge Time		Maximum EQ Time		均充保护时间			均充保护时间	
45	32		15		Equalize Stop Current			EQ Stop Curr		稳流均充电流			稳流均充电流	
46	32		15		Equalize Stop Delay Time		EQ Stop Delay		稳流均充时间			稳流均充时间	
47	32		15		Automatic Equalize			Auto EQ			自动均充允许			自动均充允许	
48	32		15		Equalize Start Current			EQ Start Curr		转均充电流			转均充电流	
49	32		15		Equalize Start Capacity			EQ Start Cap		转均充容量			转均充容量	
50	32		15		Cyclic Equalize				Cyc EQ			定时均充允许			定时均充允许	
51	32		15		Cyclic Equalize Interval		Cyc EQ Interval		定时均充周期			定时均充周期	
52	32		15		Cyclic Equalize Duration	Cyc EQ Duration		均充持续时间			均充持续时间	
53	32		20		Temperature Compensation Center		TempComp Center		温补中心点			温补中心点	
54	32		15		Temp Comp Coefficient (slope)		Temp Comp Coeff		温补系数			温补系数	
55	32		15		Battery Current Limit			Batt Curr Limit		电池限流点			电池限流点	
56	32		15		Battery Type Number			Batt Type Num		电池类型			电池类型	
57	32		15		Rated Capacity				Rated Capacity		标称容量			标称容量	
58	32		15		Charging Efficiency			Charging Eff		充电效率			充电效率	
59	32		15		Time to 0.1C10 Discharge Current	Time 0.1C10		0.1C10放电时间			0.1C10放电时间	
60	32		15		Time to 0.2C10 Discharge Current	Time 0.2C10		0.2C10放电时间			0.2C10放电时间	
61	32		15		Time to 0.3C10 Discharge Current	Time 0.3C10		0.3C10放电时间			0.3C10放电时间	
62	32		15		Time to 0.4C10 Discharge Current	Time 0.4C10		0.4C10放电时间			0.4C10放电时间	
63	32		15		Time to 0.5C10 Discharge Current	Time 0.5C10		0.5C10放电时间			0.5C10放电时间	
64	32		15		Time to 0.6C10 Discharge Current	Time 0.6C10		0.6C10放电时间			0.6C10放电时间	
65	32		15		Time to 0.7C10 Discharge Current	Time 0.7C10		0.7C10放电时间			0.7C10放电时间	
66	32		15		Time to 0.8C10 Discharge Current	Time 0.8C10		0.8C10放电时间			0.8C10放电时间	
67	32		15		Time to 0.9C10 Discharge Current	Time 0.9C10		0.9C10放电时间			0.9C10放电时间	
68	32		15		Time to 1.0C10 Discharge Current	Time 1.0C10		1.0C10放电时间			1.0C10放电时间	
70	32		15		Temperature Sensor Failure		TempSensorFail		温度传感器故障			温度传感器故障	
71	32		15		High Temperature 1			High Temp 1		高温				高温	
72	32		15		High Temperature 2			High Temp 2		超高温				超高温	
73	32		15		Low Temperature				Low Temperature	低温				低温	
74	32		15		Planned Battery Test Running		PlanBattTestRun		计划电池测试			计划电池测试	
77	32		15		Short Battery Test Running		ShortBatTestRun		短测试				短测试	
81	32		15		Automatic Equalize			Auto EQ			自动均充			自动均充	
83	32		15		Abnormal Battery Current		Abnl Batt Curr		电池电流异常			电池电流异常	
84	32		15		Temperature Compensation Active		TempComp Active		温补激活			温补激活	
85	32		15		Battery Current Limit Active		Batt Curr Limit	电池限流			电池限流	
86	32		15		Battery Charge Prohibited Alarm		BattChgProhiAlm		禁止电池充电			禁止电池充电	
87	32		15		No					No			否				否	
88	32		15		Yes					Yes			是				是	
90	32		15		None					None			无				无	
91	32		15		Temperature 1				Temperature 1		温度1				温度1
92	32		15		Temperature 2				Temperature 2		温度2				温度2
93	32		15		Temperature 3				Temperature 3		温度3				温度3
94	32		15		Temperature 4				Temperature 4		温度4				温度4
95	32		15		Temperature 5				Temperature 5		温度5				温度5
96	32		15		Temperature 6				Temperature 6		温度6				温度6
97	32		15		Temperature 7				Temperature 7		温度7				温度7
98	32		15		0					0			0				0	
99	32		15		1					1			1				1	
100	32		15		2					2			2				2	
113	32		15		Float Charge				Float Charge		浮充				浮充	
114	32		15		Equalize Charge				EQ Charge		均充				均充	
121	32		15		Disabled					Disabled			否				否	
122	32		15		Enabled					Enabled			是				是	
136	32		15		Record Threshold			RecordThreshold		测试记录阈值			测试记录阈值	
137	32		15		Remaining Time				Remaining		预计放电时间			剩余	
138	32		15		Battery Management State		Batt Management		电池管理状态			电池管理状态	
139	32		15		Float Charge				Float Charge		浮充				浮充	
140	32		15		Short Test				Short Test		短测试				短测试	
141	32		15		Equalize for Test			EQ for Test		预均充				预均充	
142	32		15		Manual Test				Manual Test		手动测试			手动测试	
143	32		15		Planned Test				Planned Test		计划测试			计划测试	
144	32		15		AC Fail Test				AC Fail Test		交流停电测试			交流停电测试	
145	32		15		AC Fail					AC Fail			交流停电			交流停电	
146	32		15		Manual Equalize				Manual EQ		手动均充			手动均充	
147	32		15		Auto Equalize				Auto EQ			自动均充			自动均充	
148	32		15		Cyclic Equalize		Cyclic EQ		定时均充			定时均充	
152	32		15		Over Current Limit			Over Curr Lmt		充电过流点			充电过流点	
153	32		15		Stop Battery Test			Stop Batt Test		停止电池测试			电池测试结束	
154	32		15		Battery Group				Battery Group		电池串组			电池串组
157	32		15		Master Battery Test			Master Bat Test	跟随主机测试			跟随主机测试	
158	32		15		Master Equalize			Master EQ		跟随主机均充			跟随主机均充	
165	32		15		Test Voltage Level			Test Volt Level		电池测试电压			电池测试电压	
166	32		15		Bad Battery				Bad Battery		坏电池				坏电池	
168	32		15		Clear Bad Battery Alarm		Clr Bad Bat Alm		清除坏电池告警			清除坏电池告警	
170	32		15		Cyclic Equalize Start Time		Cyc EQ Time				电池均充开始时间	开始时间
171	32		15		Valid EQ Enable				Valid EQ			电池均充使能		均充使能
172	32		15		Start Battery Test			Start Batt Test		启动电池测试			开始电池测试	
173	32		15		Stop					Stop			停止				停止	
174	32		15		Start					Start			开始				开始	
175	32		15		Number of Planned Tests per Year	Planned Tests		年电池测试次数			年电池测试次数	
176	32		15		Planned Test 1 (MM-DD Hr)		Test1 (M-D Hr)		电池测试时间1(MM-DD HH)		时间1(M-D H)
177	32		15		Planned Test 2 (MM-DD Hr)		Test2 (M-D Hr)		电池测试时间2(MM-DD HH)		时间2(M-D H)
178	32		15		Planned Test 3 (MM-DD Hr)		Test3 (M-D Hr)		电池测试时间3(MM-DD HH)		时间3(M-D H)
179	32		15		Planned Test 4 (MM-DD Hr)		Test4 (M-D Hr)		电池测试时间4(MM-DD HH)		时间4(M-D H)
180	32		15		Planned Test 5 (MM-DD Hr)		Test5 (M-D Hr)		电池测试时间5(MM-DD HH)		时间5(M-D H)
181	32		15		Planned Test 6 (MM-DD Hr)		Test6 (M-D Hr)		电池测试时间6(MM-DD HH)		时间6(M-D H)
182	32		15		Planned Test 7 (MM-DD Hr)		Test7 (M-D Hr)		电池测试时间7(MM-DD HH)		时间7(M-D H)
183	32		15		Planned Test 8 (MM-DD Hr)		Test8 (M-D Hr)		电池测试时间8(MM-DD HH)		时间8(M-D H)
184	32		15		Planned Test 9 (MM-DD Hr)		Test9 (M-D Hr)		电池测试时间9(MM-DD HH)		时间9(M-D H)
185	32		15		Planned Test 10 (MM-DD Hr)		Test10 (M-D Hr)		电池测试时间10(MM-DD HH)	时间10(M-D H)
186	32		15		Planned Test 11 (MM-DD Hr)		Test11 (M-D Hr)		电池测试时间11(MM-DD HH)	时间11(M-D H)
187	32		15		Planned Test 12 (MM-DD Hr)		Test12 (M-D Hr)		电池测试时间12(MM-DD HH)	时间12(M-D H)
188	32		15		Reset Battery Capacity			Reset Batt Cap		复位电池容量			复位电池容量	
191	32		15		Clear Abnormal Bat Current Alarm	Clr AbnlCur Alm		清除电流异常告警		清电流异常告警	
192	32		15		Clear Discharge Curr Imbalance		Clr Cur Imb Alm		清除放电不平衡告警		清放电不平衡	
193	32		15		Expected Current Limit			ExpectedCurrLmt		当前限流点			当前限流点	
194	32		15		Battery Test Running			BatTestRunning		电池测试进行中			电池测试进行中	
195	32		15		Low Capacity Point			Low Capacity Pt		低容量告警点			低容量告警点
196	32		15		Battery Discharge			Battery Disch		电池放电			电池放电
197	32		15		Over Voltage				Over Voltage		电池过压点			电池过压点
198	32		15		Low Voltage				Low Voltage		电池欠压点			电池欠压点	
200	32		15		Number of Battery Shunts		Num Batt Shunts		电池分流器个数			电池分流器个数
201	32		15		Imbalance Protection			Imb Protection		不平衡保护			不平衡保护
202	32		15		Temp Compensation Probe Number		TempComp Sensor		温补用温度			温补用温度
203	32		15		EIB Battery Number			EIB Battery Num		EIB电池数			EIB电池数
204	32		15		Normal					Normal			正常				正常
205	32		15		Special for NA				Special for NA		特殊(北美)			特殊
206	32		15		Battery Voltage Type			Batt Volt Type		电池电压类型			电池电压类型
207	32		15		Voltage on Very High Batt Temp		VoltVHiBatTemp		超高温电压			超高温电压
#208	32		15		Current Limited				Current Limited		限流使能			限流使能
209	32		15		Battery Temperature Probe Number	BatTempProbeNum		电池用温度路数			电池用温度路数
212	32		15		Action on Very High Battery Temp	Action on VHBT		电池高高温动作			电池高高温动作
213	32		15		Disabled				Disabled		禁止				禁止
214	32		15		Lowering Voltage			Lowering Volt		调低输出电压			调低输出电压
215	32		15		Disconnection				Disconnection		电池下电			电池下电
216	32		15		Reconnection Temperature		ReconnectTemp		电池上电温度			电池上电温度
217	32		15		Very Hi Temp Volt Setpoint (24V)	VHigh Temp Volt		超高温电压(24V)			超高温电压
218	32		15		Float Charge Voltage (24V)		Float Voltage		浮充电压(24V)			浮充电压	
219	32		15		Equalize Charge Voltage (24V)		EQ Voltage		均充电压(24V)			均充电压	
220	32		15		Test Voltage Limit (24V)		Test Volt Lmt		电池测试电压(24V)		电池测试电压	
221	32		15		Test End Voltage (24V)			Test End Volt		测试结束电压(24V)		测试结束电压  
222	32		15		Current Limited				Current Limited		自动限流允许			自动限流允许
223	32		15		Batt Voltage for North America		Batt Volt NA		北美电压测量模式		北美模式
224	32		15		Battery Changed				Battery Changed		电池改变			电池改变
225	32		15		Battery Test Lowest Capacity		BattTestLowCap		电池测试最低容量		测试需最低容量
226	32		15		Temperature 8				Temperature 8		温度8				温度8
227	32		15		Temperature 9				Temperature 9		温度9				温度9
228	32		15		Temperature 10				Temperature 10		温度10				温度10
229	32		15		LargeDU Rated Capacity			Rated Capacity		LargeDU标称容量			标称容量	
230	32		15		Clear Battery Test Fail Alarm		ClrBatTestFail		清除电池测试失败告警		清电池测试失败	
231	32		15		Battery Test Fail			BatteryTestFail		电池测试失败			电池测试失败	
232	32		15		Maximum					Maximum			最大温度			最大温度
233	32		15		Average					Average			平均温度			平均温度
234	32		15		Average SMBRC				Average SMBRC		SMBRC平均温度			SMBRC平均温度
235	32		15		Compensation Temperature		Comp Temp		温补温度			温补温度
236	32		15		Comp Temp High1			Comp Temp Hi1		温补温度高温点			温补高温点
237	32		15		Comp Temp Low				Comp Temp Low		温补温度低温点			温补低温点
238	32		15		Comp Temp High1			Comp Temp Hi1		温补温度高			温补温度高
239	32		15		Comp Temp Low				Comp Temp Low		温补温度低			温补温度低
240	32		15		Comp Temp High2			Comp Temp Hi2		温补温度过温点			温补过温点
241	32		15		Comp Temp High2			Comp Temp Hi2		温补过温			温补过温
242	32		15		Compensation Sensor Fault		CompTempFail		温补温度故障			温补温度故障
243	32		15		Calculate Battery Current		Calc Batt Curr		计算电池电流			计算电池电流
244	32		15		Start Equalize Charge (for EEM)	Star EQ(EEM)		均充开始(EEM)			均充开始(EEM)    
245	32		15		Start Float Charge (for EEM)	Star FLT(EEM)		浮充开始(EEM)			浮充开始(EEM)    
246	32		15		Start Resistance Test(for EEM)		StartTest(EEM)	开始电阻测试(EEM)		开始电阻测试(EEM)  
247	32		15		Stop Resistance Test(for EEM)		Stop Test(EEM)	停止电阻测试(EEM)		停止电阻测试(EEM)  
248	32		15		Reset Batt Cap(Internal Use)	ResetCap(Int)	复位电池容量(内部使用)		复位容量(内部用)
249	32		15		Reset Battery Capacity(for EEM)		ResetCap(EEM)		复位电池容量(EEM)		复位容量(EEM)
250	32		15		Clear Bad Battery Alarm(for EEM)	ClrAlm(EEM)	清除坏电池告警(EEM)		清除坏电池告警(EEM)	

251	32		15		System Temp1		System T1	系统温度1		系统温度1
252	32		15		System Temp2		System T2	系统温度2		系统温度2
253	32		15		System Temp3		System T3	系统温度3		系统温度3
254	32		15		IB2-1 Temp1		IB2-1 T1		IB2-1温度1		IB2-1温度1
255	32		15		IB2-1 Temp2		IB2-1 T2		IB2-1温度2		IB2-1温度2
256	32		15		EIB-1 Temp1		EIB-1 T1		EIB-1温度1		EIB-1温度1
257	32		15		EIB-1 Temp2		EIB-1 T2		EIB-1温度2		EIB-1温度2
258	32		15		SMTemp1 Temp1		SMTemp1 T1	SMTemp1温度1		SMTemp1温度1
259	32		15		SMTemp1 Temp2		SMTemp1 T2	SMTemp1温度2		SMTemp1温度2
260	32		15		SMTemp1 Temp3		SMTemp1 T3	SMTemp1温度3		SMTemp1温度3
261	32		15		SMTemp1 Temp4		SMTemp1 T4	SMTemp1温度4		SMTemp1温度4
262	32		15		SMTemp1 Temp5		SMTemp1 T5	SMTemp1温度5		SMTemp1温度5
263	32		15		SMTemp1 Temp6		SMTemp1 T6	SMTemp1温度6		SMTemp1温度6
264	32		15		SMTemp1 Temp7		SMTemp1 T7	SMTemp1温度7		SMTemp1温度7
265	32		15		SMTemp1 Temp8		SMTemp1 T8	SMTemp1温度8		SMTemp1温度8
266	32		15		SMTemp2 Temp1		SMTemp2 T1	SMTemp2温度1		SMTemp2温度1
267	32		15		SMTemp2 Temp2		SMTemp2 T2	SMTemp2温度2		SMTemp2温度2
268	32		15		SMTemp2 Temp3		SMTemp2 T3	SMTemp2温度3		SMTemp2温度3
269	32		15		SMTemp2 Temp4		SMTemp2 T4	SMTemp2温度4		SMTemp2温度4
270	32		15		SMTemp2 Temp5		SMTemp2 T5	SMTemp2温度5		SMTemp2温度5
271	32		15		SMTemp2 Temp6		SMTemp2 T6	SMTemp2温度6		SMTemp2温度6
272	32		15		SMTemp2 Temp7		SMTemp2 T7	SMTemp2温度7		SMTemp2温度7
273	32		15		SMTemp2 Temp8		SMTemp2 T8	SMTemp2温度8		SMTemp2温度8
274	32		15		SMTemp3 Temp1		SMTemp3 T1	SMTemp3温度1		SMTemp3温度1
275	32		15		SMTemp3 Temp2		SMTemp3 T2	SMTemp3温度2		SMTemp3温度2
276	32		15		SMTemp3 Temp3		SMTemp3 T3	SMTemp3温度3		SMTemp3温度3
277	32		15		SMTemp3 Temp4		SMTemp3 T4	SMTemp3温度4		SMTemp3温度4
278	32		15		SMTemp3 Temp5		SMTemp3 T5	SMTemp3温度5		SMTemp3温度5
279	32		15		SMTemp3 Temp6		SMTemp3 T6	SMTemp3温度6		SMTemp3温度6
280	32		15		SMTemp3 Temp7		SMTemp3 T7	SMTemp3温度7		SMTemp3温度7
281	32		15		SMTemp3 Temp8		SMTemp3 T8	SMTemp3温度8		SMTemp3温度8
282	32		15		SMTemp4 Temp1		SMTemp4 T1	SMTemp4温度1		SMTemp4温度1
283	32		15		SMTemp4 Temp2		SMTemp4 T2	SMTemp4温度2		SMTemp4温度2
284	32		15		SMTemp4 Temp3		SMTemp4 T3	SMTemp4温度3		SMTemp4温度3
285	32		15		SMTemp4 Temp4		SMTemp4 T4	SMTemp4温度4		SMTemp4温度4
286	32		15		SMTemp4 Temp5		SMTemp4 T5	SMTemp4温度5		SMTemp4温度5
287	32		15		SMTemp4 Temp6		SMTemp4 T6	SMTemp4温度6		SMTemp4温度6
288	32		15		SMTemp4 Temp7		SMTemp4 T7	SMTemp4温度7		SMTemp4温度7
289	32		15		SMTemp4 Temp8		SMTemp4 T8	SMTemp4温度8		SMTemp4温度8
290	32		15		SMTemp5 Temp1		SMTemp5 T1	SMTemp5温度1		SMTemp5温度1
291	32		15		SMTemp5 Temp2		SMTemp5 T2	SMTemp5温度2		SMTemp5温度2
292	32		15		SMTemp5 Temp3		SMTemp5 T3	SMTemp5温度3		SMTemp5温度3
293	32		15		SMTemp5 Temp4		SMTemp5 T4	SMTemp5温度4		SMTemp5温度4
294	32		15		SMTemp5 Temp5		SMTemp5 T5	SMTemp5温度5		SMTemp5温度5
295	32		15		SMTemp5 Temp6		SMTemp5 T6	SMTemp5温度6		SMTemp5温度6
296	32		15		SMTemp5 Temp7		SMTemp5 T7	SMTemp5温度7		SMTemp5温度7
297	32		15		SMTemp5 Temp8		SMTemp5 T8	SMTemp5温度8		SMTemp5温度8
298	32		15		SMTemp6 Temp1		SMTemp6 T1	SMTemp6温度1		SMTemp6温度1
299	32		15		SMTemp6 Temp2		SMTemp6 T2	SMTemp6温度2		SMTemp6温度2
300	32		15		SMTemp6 Temp3		SMTemp6 T3	SMTemp6温度3		SMTemp6温度3
301	32		15		SMTemp6 Temp4		SMTemp6 T4	SMTemp6温度4		SMTemp6温度4
302	32		15		SMTemp6 Temp5		SMTemp6 T5	SMTemp6温度5		SMTemp6温度5
303	32		15		SMTemp6 Temp6		SMTemp6 T6	SMTemp6温度6		SMTemp6温度6
304	32		15		SMTemp6 Temp7		SMTemp6 T7	SMTemp6温度7		SMTemp6温度7
305	32		15		SMTemp6 Temp8		SMTemp6 T8	SMTemp6温度8		SMTemp6温度8
306	32		15		SMTemp7 Temp1		SMTemp7 T1	SMTemp7温度1		SMTemp7温度1
307	32		15		SMTemp7 Temp2		SMTemp7 T2	SMTemp7温度2		SMTemp7温度2
308	32		15		SMTemp7 Temp3		SMTemp7 T3	SMTemp7温度3		SMTemp7温度3
309	32		15		SMTemp7 Temp4		SMTemp7 T4	SMTemp7温度4		SMTemp7温度4
310	32		15		SMTemp7 Temp5		SMTemp7 T5	SMTemp7温度5		SMTemp7温度5
311	32		15		SMTemp7 Temp6		SMTemp7 T6	SMTemp7温度6		SMTemp7温度6
312	32		15		SMTemp7 Temp7		SMTemp7 T7	SMTemp7温度7		SMTemp7温度7
313	32		15		SMTemp7 Temp8		SMTemp7 T8	SMTemp7温度8		SMTemp7温度8
314	32		15		SMTemp8 Temp1		SMTemp8 T1	SMTemp8温度1		SMTemp8温度1
315	32		15		SMTemp8 Temp2		SMTemp8 T2	SMTemp8温度2		SMTemp8温度2
316	32		15		SMTemp8 Temp3		SMTemp8 T3	SMTemp8温度3		SMTemp8温度3
317	32		15		SMTemp8 Temp4		SMTemp8 T4	SMTemp8温度4		SMTemp8温度4
318	32		15		SMTemp8 Temp5		SMTemp8 T5	SMTemp8温度5		SMTemp8温度5
319	32		15		SMTemp8 Temp6		SMTemp8 T6	SMTemp8温度6		SMTemp8温度6
320	32		15		SMTemp8 Temp7		SMTemp8 T7	SMTemp8温度7		SMTemp8温度7
321	32		15		SMTemp8 Temp8		SMTemp8 T8	SMTemp8温度8		SMTemp8温度8
322	32		15		Minimum			Minimum		最小值			最小值

351	32		15		System Temp1 High 2	System T1 Hi2	系统温度1过过温		系统温度1过过温
352	32		15		System Temp1 High 1	System T1 Hi1	系统温度1过温		系统温度1过温
353	32		15		System Temp1 Low	System T1 Low	系统温度1低温		系统温度1低温
354	32		15		System Temp2 High 2	System T2 Hi2	系统温度2过过温		系统温度2过过温
355	32		15		System Temp2 High 1	System T2 Hi1	系统温度2过温		系统温度2过温
356	32		15		System Temp2 Low	System T2 Low	系统温度2低温		系统温度2低温
357	32		15		System Temp3 High 2	System T3 Hi2	系统温度3过过温		系统温度3过过温
358	32		15		System Temp3 High 1	System T3 Hi1	系统温度3过温		系统温度3过温
359	32		15		System Temp3 Low	System T3 Low	系统温度3低温		系统温度3低温
360	32		15		IB2-1 Temp1 High 2	IB2-1 T1 Hi2	IB2-1温度1过过温		IB2-1温度1过温2
361	32		15		IB2-1 Temp1 High 1	IB2-1 T1 Hi1	IB2-1温度1过温		IB2-1温度1过温
362	32		15		IB2-1 Temp1 Low		IB2-1 T1 Low	IB2-1温度1低温		IB2-1温度1低温
363	32		15		IB2-1 Temp2 High 2	IB2-1 T2 Hi2	IB2-1温度2过过温		IB2-1温度2过温2
364	32		15		IB2-1 Temp2 High 1	IB2-1 T2 Hi1	IB2-1温度2过温		IB2-1温度2过温
365	32		15		IB2-1 Temp2 Low		IB2-1 T2 Low	IB2-1温度2低温		IB2-1温度2低温
366	32		15		EIB-1 Temp1 High 2	EIB-1 T1 Hi2	EIB-1温度1过过温		EIB-1温度1过温2
367	32		15		EIB-1 Temp1 High 1	EIB-1 T1 Hi1	EIB-1温度1过温		EIB-1温度1过温
368	32		15		EIB-1 Temp1 Low		EIB-1 T1 Low	EIB-1温度1低温		EIB-1温度1低温
369	32		15		EIB-1 Temp2 High 2	EIB-1 T2 Hi2	EIB-1温度2过过温		EIB-1温度2过温2
370	32		15		EIB-1 Temp2 High 1	EIB-1 T2 Hi1	EIB-1温度2过温		EIB-1温度2过温
371	32		15		EIB-1 Temp2 Low		EIB-1 T2 Low	EIB-1温度2低温		EIB-1温度2低温
372	32		15		SMTemp1 Temp1 High 2	SMTemp1 T1 Hi2	SMTemp1 T1过过温	SMTemp1T1过过温
373	32		15		SMTemp1 Temp1 High 1	SMTemp1 T1 Hi1	SMTemp1 T1过温		SMTemp1T1过温
374	32		15		SMTemp1 Temp1 Low	SMTemp1 T1 Low	SMTemp1 T1低温		SMTemp1T1低温
375	32		15		SMTemp1 Temp2 High 2	SMTemp1 T2 Hi2	SMTemp1 T2过过温	SMTemp1T2过过温
376	32		15		SMTemp1 Temp2 High 1	SMTemp1 T2 Hi1	SMTemp1 T2过温		SMTemp1T2过温
377	32		15		SMTemp1 Temp2 Low	SMTemp1 T2 Low	SMTemp1 T2低温		SMTemp1T2低温
378	32		15		SMTemp1 Temp3 High 2	SMTemp1 T3 Hi2	SMTemp1 T3过过温	SMTemp1T3过过温
379	32		15		SMTemp1 Temp3 High 1	SMTemp1 T3 Hi1	SMTemp1 T3过温		SMTemp1T3过温
380	32		15		SMTemp1 Temp3 Low	SMTemp1 T3 Low	SMTemp1 T3低温		SMTemp1T3低温
381	32		15		SMTemp1 Temp4 High 2	SMTemp1 T4 Hi2	SMTemp1 T4过过温	SMTemp1T4过过温
382	32		15		SMTemp1 Temp4 High 1	SMTemp1 T4 Hi1	SMTemp1 T4过温		SMTemp1T4过温
383	32		15		SMTemp1 Temp4 Low	SMTemp1 T4 Low	SMTemp1 T4低温		SMTemp1T4低温
384	32		15		SMTemp1 Temp5 High 2	SMTemp1 T5 Hi2	SMTemp1 T5过过温	SMTemp1T5过过温
385	32		15		SMTemp1 Temp5 High 1	SMTemp1 T5 Hi1	SMTemp1 T5过温		SMTemp1T5过温
386	32		15		SMTemp1 Temp5 Low	SMTemp1 T5 Low	SMTemp1 T5低温		SMTemp1T5低温
387	32		15		SMTemp1 Temp6 High 2	SMTemp1 T6 Hi2	SMTemp1 T6过过温	SMTemp1T6过过温
388	32		15		SMTemp1 Temp6 High 1	SMTemp1 T6 Hi1	SMTemp1 T6过温		SMTemp1T6过温
389	32		15		SMTemp1 Temp6 Low	SMTemp1 T6 Low	SMTemp1 T6低温		SMTemp1T6低温
390	32		15		SMTemp1 Temp7 High 2	SMTemp1 T7 Hi2	SMTemp1 T7过过温	SMTemp1T7过过温
391	32		15		SMTemp1 Temp7 High 1	SMTemp1 T7 Hi1	SMTemp1 T7过温		SMTemp1T7过温
392	32		15		SMTemp1 Temp7 Low	SMTemp1 T7 Low	SMTemp1 T7低温		SMTemp1T7低温
393	32		15		SMTemp1 Temp8 High 2	SMTemp1 T8 Hi2	SMTemp1 T8过过温	SMTemp1T8过过温
394	32		15		SMTemp1 Temp8 High 1	SMTemp1 T8 Hi1	SMTemp1 T8过温		SMTemp1T8过温
395	32		15		SMTemp1 Temp8 Low	SMTemp1 T8 Low	SMTemp1 T8低温		SMTemp1T8低温
396	32		15		SMTemp2 Temp1 High 2	SMTemp2 T1 Hi2	SMTemp2 T1过过温	SMTemp2T1过过温
397	32		15		SMTemp2 Temp1 High 1	SMTemp2 T1 Hi1	SMTemp2 T1过温		SMTemp2T1过温
398	32		15		SMTemp2 Temp1 Low	SMTemp2 T1 Low	SMTemp2 T1低温		SMTemp2T1低温
399	32		15		SMTemp2 Temp2 High 2	SMTemp2 T2 Hi2	SMTemp2 T2过过温	SMTemp2T2过过温
400	32		15		SMTemp2 Temp2 High 1	SMTemp2 T2 Hi1	SMTemp2 T2过温		SMTemp2T2过温
401	32		15		SMTemp2 Temp2 Low	SMTemp2 T2 Low	SMTemp2 T2低温		SMTemp2T2低温
402	32		15		SMTemp2 Temp3 High 2	SMTemp2 T3 Hi2	SMTemp2 T3过过温	SMTemp2T3过过温
403	32		15		SMTemp2 Temp3 High 1	SMTemp2 T3 Hi1	SMTemp2 T3过温		SMTemp2T3过温
404	32		15		SMTemp2 Temp3 Low	SMTemp2 T3 Low	SMTemp2 T3低温		SMTemp2T3低温
405	32		15		SMTemp2 Temp4 High 2	SMTemp2 T4 Hi2	SMTemp2 T4过过温	SMTemp2T4过过温
406	32		15		SMTemp2 Temp4 High 1	SMTemp2 T4 Hi1	SMTemp2 T4过温		SMTemp2T4过温
407	32		15		SMTemp2 Temp4 Low	SMTemp2 T4 Low	SMTemp2 T4低温		SMTemp2T4低温
408	32		15		SMTemp2 Temp5 High 2	SMTemp2 T5 Hi2	SMTemp2 T5过过温	SMTemp2T5过过温
409	32		15		SMTemp2 Temp5 High 1	SMTemp2 T5 Hi1	SMTemp2 T5过温		SMTemp2T5过温
410	32		15		SMTemp2 Temp5 Low	SMTemp2 T5 Low	SMTemp2 T5低温		SMTemp2T5低温
411	32		15		SMTemp2 Temp6 High 2	SMTemp2 T6 Hi2	SMTemp2 T6过过温	SMTemp2T6过过温
412	32		15		SMTemp2 Temp6 High 1	SMTemp2 T6 Hi1	SMTemp2 T6过温		SMTemp2T6过温
413	32		15		SMTemp2 Temp6 Low	SMTemp2 T6 Low	SMTemp2 T6低温		SMTemp2T6低温
414	32		15		SMTemp2 Temp7 High 2	SMTemp2 T7 Hi2	SMTemp2 T7过过温	SMTemp2T7过过温
415	32		15		SMTemp2 Temp7 High 1	SMTemp2 T7 Hi1	SMTemp2 T7过温		SMTemp2T7过温
416	32		15		SMTemp2 Temp7 Low	SMTemp2 T7 Low	SMTemp2 T7低温		SMTemp2T7低温
417	32		15		SMTemp2 Temp8 High 2	SMTemp2 T8 Hi2	SMTemp2 T8过过温	SMTemp2T8过过温
418	32		15		SMTemp2 Temp8 High 1	SMTemp2 T8 Hi1	SMTemp2 T8过温		SMTemp2T8过温
419	32		15		SMTemp2 Temp8 Low	SMTemp2 T8 Low	SMTemp2 T8低温		SMTemp2T8低温
420	32		15		SMTemp3 Temp1 High 2	SMTemp3 T1 Hi2	SMTemp3 T1过过温	SMTemp3T1过过温
421	32		15		SMTemp3 Temp1 High 1	SMTemp3 T1 Hi1	SMTemp3 T1过温		SMTemp3T1过温
422	32		15		SMTemp3 Temp1 Low	SMTemp3 T1 Low	SMTemp3 T1低温		SMTemp3T1低温
423	32		15		SMTemp3 Temp2 High 2	SMTemp3 T2 Hi2	SMTemp3 T2过过温	SMTemp3T2过过温
424	32		15		SMTemp3 Temp2 High 1	SMTemp3 T2 Hi1	SMTemp3 T2过温		SMTemp3T2过温
425	32		15		SMTemp3 Temp2 Low	SMTemp3 T2 Low	SMTemp3 T2低温		SMTemp3T2低温
426	32		15		SMTemp3 Temp3 High 2	SMTemp3 T3 Hi2	SMTemp3 T3过过温	SMTemp3T3过过温
427	32		15		SMTemp3 Temp3 High 1	SMTemp3 T3 Hi1	SMTemp3 T3过温		SMTemp3T3过温
428	32		15		SMTemp3 Temp3 Low	SMTemp3 T3 Low	SMTemp3 T3低温		SMTemp3T3低温
429	32		15		SMTemp3 Temp4 High 2	SMTemp3 T4 Hi2	SMTemp3 T4过过温	SMTemp3T4过过温
430	32		15		SMTemp3 Temp4 High 1	SMTemp3 T4 Hi1	SMTemp3 T4过温		SMTemp3T4过温
431	32		15		SMTemp3 Temp4 Low	SMTemp3 T4 Low	SMTemp3 T4低温		SMTemp3T4低温
432	32		15		SMTemp3 Temp5 High 2	SMTemp3 T5 Hi2	SMTemp3 T5过过温	SMTemp3T5过过温
433	32		15		SMTemp3 Temp5 High 1	SMTemp3 T5 Hi1	SMTemp3 T5过温		SMTemp3T5过温
434	32		15		SMTemp3 Temp5 Low	SMTemp3 T5 Low	SMTemp3 T5低温		SMTemp3T5低温
435	32		15		SMTemp3 Temp6 High 2	SMTemp3 T6 Hi2	SMTemp3 T6过过温	SMTemp3T6过过温
436	32		15		SMTemp3 Temp6 High 1	SMTemp3 T6 Hi1	SMTemp3 T6过温		SMTemp3T6过温
437	32		15		SMTemp3 Temp6 Low	SMTemp3 T6 Low	SMTemp3 T6低温		SMTemp3T6低温
438	32		15		SMTemp3 Temp7 High 2	SMTemp3 T7 Hi2	SMTemp3 T7过过温	SMTemp3T7过过温
439	32		15		SMTemp3 Temp7 High 1	SMTemp3 T7 Hi1	SMTemp3 T7过温		SMTemp3T7过温
440	32		15		SMTemp3 Temp7 Low	SMTemp3 T7 Low	SMTemp3 T7低温		SMTemp3T7低温
441	32		15		SMTemp3 Temp8 High 2	SMTemp3 T8 Hi2	SMTemp3 T8过过温	SMTemp3T8过过温
442	32		15		SMTemp3 Temp8 High 1	SMTemp3 T8 Hi1	SMTemp3 T8过温		SMTemp3T8过温
443	32		15		SMTemp3 Temp8 Low	SMTemp3 T8 Low	SMTemp3 T8低温		SMTemp3T8低温
444	32		15		SMTemp4 Temp1 High 2	SMTemp4 T1 Hi2	SMTemp4 T1过过温	SMTemp4T1过过温
445	32		15		SMTemp4 Temp1 High 1	SMTemp4 T1 Hi1	SMTemp4 T1过温		SMTemp4T1过温
446	32		15		SMTemp4 Temp1 Low	SMTemp4 T1 Low	SMTemp4 T1低温		SMTemp4T1低温
447	32		15		SMTemp4 Temp2 High 2	SMTemp4 T2 Hi2	SMTemp4 T2过过温	SMTemp4T2过过温
448	32		15		SMTemp4 Temp2 High 1	SMTemp4 T2 Hi1	SMTemp4 T2过温		SMTemp4T2过温
449	32		15		SMTemp4 Temp2 Low	SMTemp4 T2 Low	SMTemp4 T2低温		SMTemp4T2低温
450	32		15		SMTemp4 Temp3 High 2	SMTemp4 T3 Hi2	SMTemp4 T3过过温	SMTemp4T3过过温
451	32		15		SMTemp4 Temp3 High 1	SMTemp4 T3 Hi1	SMTemp4 T3过温		SMTemp4T3过温
452	32		15		SMTemp4 Temp3 Low	SMTemp4 T3 Low	SMTemp4 T3低温		SMTemp4T3低温
453	32		15		SMTemp4 Temp4 High 2	SMTemp4 T4 Hi2	SMTemp4 T4过过温	SMTemp4T4过过温
454	32		15		SMTemp4 Temp4 High 1	SMTemp4 T4 Hi1	SMTemp4 T4过温		SMTemp4T4过温
455	32		15		SMTemp4 Temp4 Low	SMTemp4 T4 Low	SMTemp4 T4低温		SMTemp4T4低温
456	32		15		SMTemp4 Temp5 High 2	SMTemp4 T5 Hi2	SMTemp4 T5过过温	SMTemp4T5过过温
457	32		15		SMTemp4 Temp5 High 1	SMTemp4 T5 Hi1	SMTemp4 T5过温		SMTemp4T5过温
458	32		15		SMTemp4 Temp5 Low	SMTemp4 T5 Low	SMTemp4 T5低温		SMTemp4T5低温
459	32		15		SMTemp4 Temp6 High 2	SMTemp4 T6 Hi2	SMTemp4 T6过过温	SMTemp4T6过过温
460	32		15		SMTemp4 Temp6 High 1	SMTemp4 T6 Hi1	SMTemp4 T6过温		SMTemp4T6过温
461	32		15		SMTemp4 Temp6 Low	SMTemp4 T6 Low	SMTemp4 T6低温		SMTemp4T6低温
462	32		15		SMTemp4 Temp7 High 2	SMTemp4 T7 Hi2	SMTemp4 T7过过温	SMTemp4T7过过温
463	32		15		SMTemp4 Temp7 High 1	SMTemp4 T7 Hi1	SMTemp4 T7过温		SMTemp4T7过温
464	32		15		SMTemp4 Temp7 Low	SMTemp4 T7 Low	SMTemp4 T7低温		SMTemp4T7低温
465	32		15		SMTemp4 Temp8 High 2	SMTemp4 T8 Hi2	SMTemp4 T8过过温	SMTemp4T8过过温
466	32		15		SMTemp4 Temp8 Hi1	SMTemp4 T8 Hi1	SMTemp4 T8过温		SMTemp4T8过温
467	32		15		SMTemp4 Temp8 Low	SMTemp4 T8 Low	SMTemp4 T8低温		SMTemp4T8低温
468	32		15		SMTemp5 Temp1 High 2	SMTemp5 T1 Hi2	SMTemp5 T1过过温	SMTemp5T1过过温
469	32		15		SMTemp5 Temp1 High 1	SMTemp5 T1 Hi1	SMTemp5 T1过温		SMTemp5T1过温
470	32		15		SMTemp5 Temp1 Low	SMTemp5 T1 Low	SMTemp5 T1低温		SMTemp5T1低温
471	32		15		SMTemp5 Temp2 High 2	SMTemp5 T2 Hi2	SMTemp5 T2过过温	SMTemp5T2过过温
472	32		15		SMTemp5 Temp2 High 1	SMTemp5 T2 Hi1	SMTemp5 T2过温		SMTemp5T2过温
473	32		15		SMTemp5 Temp2 Low	SMTemp5 T2 Low	SMTemp5 T2低温		SMTemp5T2低温
474	32		15		SMTemp5 Temp3 High 2	SMTemp5 T3 Hi2	SMTemp5 T3过过温	SMTemp5T3过过温
475	32		15		SMTemp5 Temp3 High 1	SMTemp5 T3 Hi1	SMTemp5 T3过温		SMTemp5T3过温
476	32		15		SMTemp5 Temp3 Low	SMTemp5 T3 Low	SMTemp5 T3低温		SMTemp5T3低温
477	32		15		SMTemp5 Temp4 High 2	SMTemp5 T4 Hi2	SMTemp5 T4过过温	SMTemp5T4过过温
478	32		15		SMTemp5 Temp4 High 1	SMTemp5 T4 Hi1	SMTemp5 T4过温		SMTemp5T4过温
479	32		15		SMTemp5 Temp4 Low	SMTemp5 T4 Low	SMTemp5 T4低温		SMTemp5T4低温
480	32		15		SMTemp5 Temp5 High 2	SMTemp5 T5 Hi2	SMTemp5 T5过过温	SMTemp5T5过过温
481	32		15		SMTemp5 Temp5 High 1	SMTemp5 T5 Hi1	SMTemp5 T5过温		SMTemp5T5过温
482	32		15		SMTemp5 Temp5 Low	SMTemp5 T5 Low	SMTemp5 T5低温		SMTemp5T5低温
483	32		15		SMTemp5 Temp6 High 2	SMTemp5 T6 Hi2	SMTemp5 T6过过温	SMTemp5T6过过温
484	32		15		SMTemp5 Temp6 High 1	SMTemp5 T6 Hi1	SMTemp5 T6过温		SMTemp5T6过温
485	32		15		SMTemp5 Temp6 Low	SMTemp5 T6 Low	SMTemp5 T6低温		SMTemp5T6低温
486	32		15		SMTemp5 Temp7 High 2	SMTemp5 T7 Hi2	SMTemp5 T7过过温	SMTemp5T7过过温
487	32		15		SMTemp5 Temp7 High 1	SMTemp5 T7 Hi1	SMTemp5 T7过温		SMTemp5T7过温
488	32		15		SMTemp5 Temp7 Low	SMTemp5 T7 Low	SMTemp5 T7低温		SMTemp5T7低温
489	32		15		SMTemp5 Temp8 High 2	SMTemp5 T8 Hi2	SMTemp5 T8过过温	SMTemp5T8过过温
490	32		15		SMTemp5 Temp8 High 1	SMTemp5 T8 Hi1	SMTemp5 T8过温		SMTemp5T8过温
491	32		15		SMTemp5 Temp8 Low	SMTemp5 T8 Low	SMTemp5 T8低温		SMTemp5T8低温
492	32		15		SMTemp6 Temp1 High 2	SMTemp6 T1 Hi2	SMTemp6 T1过过温	SMTemp6T1过过温
493	32		15		SMTemp6 Temp1 High 1	SMTemp6 T1 Hi1	SMTemp6 T1过温		SMTemp6T1过温
494	32		15		SMTemp6 Temp1 Low	SMTemp6 T1 Low	SMTemp6 T1低温		SMTemp6T1低温
495	32		15		SMTemp6 Temp2 High 2	SMTemp6 T2 Hi2	SMTemp6 T2过过温	SMTemp6T2过过温
496	32		15		SMTemp6 Temp2 High 1	SMTemp6 T2 Hi1	SMTemp6 T2过温		SMTemp6T2过温
497	32		15		SMTemp6 Temp2 Low	SMTemp6 T2 Low	SMTemp6 T2低温		SMTemp6T2低温
498	32		15		SMTemp6 Temp3 High 2	SMTemp6 T3 Hi2	SMTemp6 T3过过温	SMTemp6T3过过温
499	32		15		SMTemp6 Temp3 High 1	SMTemp6 T3 Hi1	SMTemp6 T3过温		SMTemp6T3过温
500	32		15		SMTemp6 Temp3 Low	SMTemp6 T3 Low	SMTemp6 T3低温		SMTemp6T3低温
501	32		15		SMTemp6 Temp4 High 2	SMTemp6 T4 Hi2	SMTemp6 T4过过温	SMTemp6T4过过温
502	32		15		SMTemp6 Temp4 High 1	SMTemp6 T4 Hi1	SMTemp6 T4过温		SMTemp6T4过温
503	32		15		SMTemp6 Temp4 Low	SMTemp6 T4 Low	SMTemp6 T4低温		SMTemp6T4低温
504	32		15		SMTemp6 Temp5 High 2	SMTemp6 T5 Hi2	SMTemp6 T5过过温	SMTemp6T5过过温
505	32		15		SMTemp6 Temp5 High 1	SMTemp6 T5 Hi1	SMTemp6 T5过温		SMTemp6T5过温
506	32		15		SMTemp6 Temp5 Low	SMTemp6 T5 Low	SMTemp6 T5低温		SMTemp6T5低温
507	32		15		SMTemp6 Temp6 High 2	SMTemp6 T6 Hi2	SMTemp6 T6过过温	SMTemp6T6过过温
508	32		15		SMTemp6 Temp6 High 1	SMTemp6 T6 Hi1	SMTemp6 T6过温		SMTemp6T6过温
509	32		15		SMTemp6 Temp6 Low	SMTemp6 T6 Low	SMTemp6 T6低温		SMTemp6T6低温
510	32		15		SMTemp6 Temp7 High 2	SMTemp6 T7 Hi2	SMTemp6 T7过过温	SMTemp6T7过过温
511	32		15		SMTemp6 Temp7 High 1	SMTemp6 T7 Hi1	SMTemp6 T7过温		SMTemp6T7过温
512	32		15		SMTemp6 Temp7 Low	SMTemp6 T7 Low	SMTemp6 T7低温		SMTemp6T7低温
513	32		15		SMTemp6 Temp8 High 2	SMTemp6 T8 Hi2	SMTemp6 T8过过温	SMTemp6T8过过温
514	32		15		SMTemp6 Temp8 High 1	SMTemp6 T8 Hi1	SMTemp6 T8过温		SMTemp6T8过温
515	32		15		SMTemp6 Temp8 Low	SMTemp6 T8 Low	SMTemp6 T8低温		SMTemp6T8低温
516	32		15		SMTemp7 Temp1 High 2	SMTemp7 T1 Hi2	SMTemp7 T1过过温	SMTemp7T1过过温
517	32		15		SMTemp7 Temp1 High 1	SMTemp7 T1 Hi1	SMTemp7 T1过温		SMTemp7T1过温
518	32		15		SMTemp7 Temp1 Low	SMTemp7 T1 Low	SMTemp7 T1低温		SMTemp7T1低温
519	32		15		SMTemp7 Temp2 High 2	SMTemp7 T2 Hi2	SMTemp7 T2过过温	SMTemp7T2过过温
520	32		15		SMTemp7 Temp2 High 1	SMTemp7 T2 Hi1	SMTemp7 T2过温		SMTemp7T2过温
521	32		15		SMTemp7 Temp2 Low	SMTemp7 T2 Low	SMTemp7 T2低温		SMTemp7T2低温
522	32		15		SMTemp7 Temp3 High 2	SMTemp7 T3 Hi2	SMTemp7 T3过过温	SMTemp7T3过过温
523	32		15		SMTemp7 Temp3 High 1	SMTemp7 T3 Hi1	SMTemp7 T3过温		SMTemp7T3过温
524	32		15		SMTemp7 Temp3 Low	SMTemp7 T3 Low	SMTemp7 T3低温		SMTemp7T3低温
525	32		15		SMTemp7 Temp4 High 2	SMTemp7 T4 Hi2	SMTemp7 T4过过温	SMTemp7T4过过温
526	32		15		SMTemp7 Temp4 High 1	SMTemp7 T4 Hi1	SMTemp7 T4过温		SMTemp7T4过温
527	32		15		SMTemp7 Temp4 Low	SMTemp7 T4 Low	SMTemp7 T4低温		SMTemp7T4低温
528	32		15		SMTemp7 Temp5 High 2	SMTemp7 T5 Hi2	SMTemp7 T5过过温	SMTemp7T5过过温
529	32		15		SMTemp7 Temp5 High 1	SMTemp7 T5 Hi1	SMTemp7 T5过温		SMTemp7T5过温
530	32		15		SMTemp7 Temp5 Low	SMTemp7 T5 Low	SMTemp7 T5低温		SMTemp7T5低温
531	32		15		SMTemp7 Temp6 High 2	SMTemp7 T6 Hi2	SMTemp7 T6过过温	SMTemp7T6过过温
532	32		15		SMTemp7 Temp6 High 1	SMTemp7 T6 Hi1	SMTemp7 T6过温		SMTemp7T6过温
533	32		15		SMTemp7 Temp6 Low	SMTemp7 T6 Low	SMTemp7 T6低温		SMTemp7T6低温
534	32		15		SMTemp7 Temp7 High 2	SMTemp7 T7 Hi2	SMTemp7 T7过过温	SMTemp7T7过过温
535	32		15		SMTemp7 Temp7 High 1	SMTemp7 T7 Hi1	SMTemp7 T7过温		SMTemp7T7过温
536	32		15		SMTemp7 Temp7 Low	SMTemp7 T7 Low	SMTemp7 T7低温		SMTemp7T7低温
537	32		15		SMTemp7 Temp8 High 2	SMTemp7 T8 Hi2	SMTemp7 T8过过温	SMTemp7T8过过温
538	32		15		SMTemp7 Temp8 High 1	SMTemp7 T8 Hi1	SMTemp7 T8过温		SMTemp7T8过温
539	32		15		SMTemp7 Temp8 Low	SMTemp7 T8 Low	SMTemp7 T8低温		SMTemp7T8低温
540	32		15		SMTemp8 Temp1 High 2	SMTemp8 T1 Hi2	SMTemp8 T1过过温	SMTemp8T1过过温
541	32		15		SMTemp8 Temp1 High 1	SMTemp8 T1 Hi1	SMTemp8 T1过温		SMTemp8T1过温
542	32		15		SMTemp8 Temp1 Low	SMTemp8 T1 Low	SMTemp8 T1低温		SMTemp8T1低温
543	32		15		SMTemp8 Temp2 High 2	SMTemp8 T2 Hi2	SMTemp8 T2过过温	SMTemp8T2过过温
544	32		15		SMTemp8 Temp2 High 1	SMTemp8 T2 Hi1	SMTemp8 T2过温		SMTemp8T2过温
545	32		15		SMTemp8 Temp2 Low	SMTemp8 T2 Low	SMTemp8 T2低温		SMTemp8T2低温
546	32		15		SMTemp8 Temp3 High 2	SMTemp8 T3 Hi2	SMTemp8 T3过过温	SMTemp8T3过过温
547	32		15		SMTemp8 Temp3 High 1	SMTemp8 T3 Hi1	SMTemp8 T3过温		SMTemp8T3过温
548	32		15		SMTemp8 Temp3 Low	SMTemp8 T3 Low	SMTemp8 T3低温		SMTemp8T3低温
549	32		15		SMTemp8 Temp4 High 2	SMTemp8 T4 Hi2	SMTemp8 T4过过温	SMTemp8T4过过温
550	32		15		SMTemp8 Temp4 High 1	SMTemp8 T4 Hi1	SMTemp8 T4过温		SMTemp8T4过温
551	32		15		SMTemp8 Temp4 Low	SMTemp8 T4 Low	SMTemp8 T4低温		SMTemp8T4低温
552	32		15		SMTemp8 Temp5 High 2	SMTemp8 T5 Hi2	SMTemp8 T5过过温	SMTemp8T5过过温
553	32		15		SMTemp8 Temp5 High 1	SMTemp8 T5 Hi1	SMTemp8 T5过温		SMTemp8T5过温
554	32		15		SMTemp8 Temp5 Low	SMTemp8 T5 Low	SMTemp8 T5低温		SMTemp8T5低温
555	32		15		SMTemp8 Temp6 High 2	SMTemp8 T6 Hi2	SMTemp8 T6过过温	SMTemp8T6过过温
556	32		15		SMTemp8 Temp6 High 1	SMTemp8 T6 Hi1	SMTemp8 T6过温		SMTemp8T6过温
557	32		15		SMTemp8 Temp6 Low	SMTemp8 T6 Low	SMTemp8 T6低温		SMTemp8T6低温
558	32		15		SMTemp8 Temp7 High 2	SMTemp8 T7 Hi2	SMTemp8 T7过过温	SMTemp8T7过过温
559	32		15		SMTemp8 Temp7 High 1	SMTemp8 T7 Hi1	SMTemp8 T7过温		SMTemp8T7过温
560	32		15		SMTemp8 Temp7 Low	SMTemp8 T7 Low	SMTemp8 T7低温		SMTemp8T7低温
561	32		15		SMTemp8 Temp8 High 2	SMTemp8 T8 Hi2	SMTemp8 T8过过温	SMTemp8T8过过温
562	32		15		SMTemp8 Temp8 High 1	SMTemp8 T8 Hi1	SMTemp8 T8过温		SMTemp8T8过温
563	32		15		SMTemp8 Temp8 Low	SMTemp8 T8 Low	SMTemp8 T8低温		SMTemp8T8低温

564	32		15		Temp Comp Voltage Clamp		Temp Comp Clamp		北美温补模式		北美温补模式
565	32		15		Temp Comp Max Voltage		Temp Comp Max V		北美温补高压限		北美温补高压限
566	32		15		Temp Comp Min Voltage		Temp Comp Min V		北美温补低压限		北美温补低压限

567	32		15		BTRM Temperature		BTRM Temp		BTRM温度传感器		BTRM温度
568	32		15		BTRM Temp High 2		BTRM Temp High2		BTRM温度过过温		BTRM过过温
569	32		15		BTRM Temp High 1		BTRM Temp High1		BTRM温度过温		BTRM过温

570	32		15		Temp Comp Max Voltage (24V)	Temp Comp Max V		北美温补高压限(24V)		北美温补高压限
571	32		15		Temp Comp Min Voltage (24V)	Temp Comp Min V		北美温补低压限(24V)		北美温补低压限

572	32		15		BTRM Temp Sensor		BTRM TempSensor		BTRM温度传感器		BTRM温度
573	32		15		BTRM Sensor Fail		BTRM TempFail		BTRM传感器故障		BTRM故障

574	32		15		Li-Ion Group Avg Temperature	LiBatt AvgTemp		Li电池组平均温度	Li电池平均温度
575	32		15		Number of Installed Batteries	Num Installed		已安装的电池数目	已安装电池数
576	32		15		Number of Disconnected Batteries	NumDisconnected		未连接的电池数目	未连接电池数
577	32		15		Inventory Update In Process	InventUpdating		锂电池模块地址重排	电池地址重排
578	32		15		Number of Comm Fail Batt	Num Comm Fail		通信失败电池数目	通信失败电池数
579	32		15		Li-Ion Battery Lost		LiBatt Lost		Li电池丢失		Li电池丢失
580	32		15		System Battery Type		Sys Batt Type		系统电池类型		系统电池类型

581	32		15		1 Li-Ion Battery Disconnect	1 LiBattDiscon		1个Li电池未连接		1Li电池未连接
582	32		15		2+Li-Ion Battery Disconnect	2+LiBattDiscon		2个以上Li电池未连接	多Li电池未连接
583	32		15		1 Li-Ion Battery No Reply	1 LiBattNoReply		1个Li电池通信失败	1Li电池无响应
584	32		15		2+Li-Ion Battery No Reply	2+LiBattNoReply		2个以上Li电池通信失败	多Li电池无响应
585	32		15		Clear Li-Ion Battery Lost	Clr LiBatt Lost		清除Li电池丢失		清除Li电池丢失
586	32		15		Clear				Clear			清除			清除	
587	32		15		Float Charge Voltage(Solar)	Float Volt(S)		浮充电压(太阳能)		浮充电压(S)	
588	32		15		Equalize Charge Voltage(Solar)	EQ Voltage(S)		均充电压(太阳能)		均充电压(S)	
589	32		15		Float Charge Voltage(RECT)	Float Volt(R)		浮充电压(RECT)		浮充电压(R)	
590	32		15		Equalize Charge Voltage(RECT)	EQ Voltage(R)		均充电压(RECT)		均充电压(R)	

591	32		15		Active Battery Current Limit	ABCL Point		活动Li电池限流点	ABCL点
592	32		15		Clear Li Battery CommInterrupt	ClrLiBatComFail		清除Li电池通信中断告警	清Li电池通信断
593	32		15		ABCL is active			ABCL Active		ABCL模式开启		ABCL模式开启
594	32		15		Last SMBAT Battery Number	Last SMBAT Num		尾SMBAT电池数		尾SMBAT电池数
595	32		15		Voltage Adjust Gain		VoltAdjustGain		调压系数		调压系数
596	32		15		Curr Limited Mode		Curr Limit Mode		限流方式		限流方式
597	32		15		Current				Current			电流方式		电流方式
598	32		15		Voltage				Voltage			电压方式		电压方式
599	32		15		Battery Charge Prohibited Status		Charge Prohibit		禁止电池充电		禁止电池充电
600	32		15		Battery Charge Prohibited Alarm			Charge Prohibit		禁止电池充电		禁止电池充电
601	32		15		Battery Lower Capacity		Lower Capacity		最小当前容量			最小当前容量
602	32		15		Charge Current			Charge Current		充电电流			充电电流
603	32		15		Upper Limit			Upper Lmt		电池电流上限			电池电流上限
604	32		15		Stable Range Upper Limit	Stable Up Lmt		电流范围上限			电流范围上限
605	32		15		Stable Range Lower Limit	Stable Low Lmt		电流范围下限			电流范围下限
606	32		15		Speed Set Point			Speed Set Point		调速点				调速点
607	32		15		Slow Speed Coefficient		Slow Coeff		慢速系数			慢速系数
608	32		15		Fast Speed Coefficient		Fast Coeff		快速系数			快速系数
609	32		15		Min Amplitude			Min Amplitude		最小幅值			最小幅值
610	32		15		Max Amplitude			Max Amplitude		最大幅值			最大幅值
611	32		15		Cycle Number			Cycle Num		周期个数			周期个数
612	32		15		Battery Temp Summary Alarm	BattTempSummAlm		电池温度告警			电池温度告警
613	32		15		EQTemp Comp Coefficient		EQCompCoeff		均充温度补偿系数		均充温补系数																		

620	32		15		IB2-2 Temp1		IB2-2 T1		IB2-2温度1		IB2-2温度1
621	32		15		IB2-2 Temp2		IB2-2 T2		IB2-2温度2		IB2-2温度2
622	32		15		EIB-2 Temp1		EIB-2 T1		EIB-2温度1		EIB-2温度1
623	32		15		EIB-2 Temp2		EIB-2 T2		EIB-2温度2		EIB-2温度2

624	32		15		IB2-2 Temp1 High 2	IB2-2 T1 Hi2	IB2-2温度1过过温	IB2-2温度1过温2
625	32		15		IB2-2 Temp1 High 1	IB2-2 T1 Hi1	IB2-2温度1过温		IB2-2温度1过温
626	32		15		IB2-2 Temp1 Low		IB2-2 T1 Low	IB2-2温度1低温		IB2-2温度1低温
627	32		15		IB2-2 Temp2 High 2	IB2-2 T2 Hi2	IB2-2温度2过过温	IB2-2温度2过温2
628	32		15		IB2-2 Temp2 High 1	IB2-2 T2 Hi1	IB2-2温度2过温		IB2-2温度2过温
629	32		15		IB2-2 Temp2 Low		IB2-2 T2 Low	IB2-2温度2低温		IB2-2温度2低温
630	32		15		EIB-2 Temp1 High 2	EIB-2 T1 Hi2	EIB-2温度1过过温	EIB-2温度1过温2
631	32		15		EIB-2 Temp1 High 1	EIB-2 T1 Hi1	EIB-2温度1过温		EIB-2温度1过温
632	32		15		EIB-2 Temp1 Low		EIB-2 T1 Low	EIB-2温度1低温		EIB-2温度1低温
633	32		15		EIB-2 Temp2 High 2	EIB-2 T2 Hi2	EIB-2温度2过过温	EIB-2温度2过温2
634	32		15		EIB-2 Temp2 High 1	EIB-2 T2 Hi1	EIB-2温度2过温		EIB-2温度2过温
635	32		15		EIB-2 Temp2 Low		EIB-2 T2 Low	EIB-2温度2低温		EIB-2温度2低温

685	32		15		Temp Comp Off When		TC Off When		温补关闭当		温补关闭当
686	32		15		All Probes Fail			All Probes Fail		所有探测器失败		所有探测器失败
687	32		15		Any Probe Fails			Any Probe Fails		任何一个探测器失败	任何一个失败
688	32		15		Temp Compensation Enabled	Temp Comp		温补使能		温补使能
689	32		15		Temp Comp Threshold Enabled	TC Threshold		温补阈值功能使能	阈值使能
690	32		15		Temp for Temp Comp Off		Off Temp		温补关闭温度		温补关闭温度
691	32		15		Temp for Temp Comp On		On Temp			温补打开温度		温补打开温度


701	32		15		SMDUE1 Temp1		SMDUE1 T1		SMDUE1温度1		SMDUE1温度1
702	32		15		SMDUE1 Temp2		SMDUE1 T2		SMDUE1温度2		SMDUE1温度2
703	32		15		SMDUE1 Temp3		SMDUE1 T3		SMDUE1温度3		SMDUE1温度3
704	32		15		SMDUE1 Temp4		SMDUE1 T4		SMDUE1温度4		SMDUE1温度4
705	32		15		SMDUE1 Temp5		SMDUE1 T5		SMDUE1温度5		SMDUE1温度5
706	32		15		SMDUE1 Temp6		SMDUE1 T6		SMDUE1温度6		SMDUE1温度6
707	32		15		SMDUE1 Temp7		SMDUE1 T7		SMDUE1温度7		SMDUE1温度7
708	32		15		SMDUE1 Temp8		SMDUE1 T8		SMDUE1温度8		SMDUE1温度8
709	32		15		SMDUE1 Temp9		SMDUE1 T9		SMDUE1温度9		SMDUE1温度9
710	32		15		SMDUE1 Temp10		SMDUE1 T10		SMDUE1温度10	SMDUE1温度10
711	32		15		SMDUE2 Temp1		SMDUE2 T1		SMDUE2温度1		SMDUE2温度1
712	32		15		SMDUE2 Temp2		SMDUE2 T2		SMDUE2温度2		SMDUE2温度2
713	32		15		SMDUE2 Temp3		SMDUE2 T3		SMDUE2温度3		SMDUE2温度3
714	32		15		SMDUE2 Temp4		SMDUE2 T4		SMDUE2温度4		SMDUE2温度4
715	32		15		SMDUE2 Temp5		SMDUE2 T5		SMDUE2温度5		SMDUE2温度5
716	32		15		SMDUE2 Temp6		SMDUE2 T6		SMDUE2温度6		SMDUE2温度6
717	32		15		SMDUE2 Temp7		SMDUE2 T7		SMDUE2温度7		SMDUE2温度7
718	32		15		SMDUE2 Temp8		SMDUE2 T8		SMDUE2温度8		SMDUE2温度8
719	32		15		SMDUE2 Temp9		SMDUE2 T9		SMDUE2温度9		SMDUE2温度9
720	32		15		SMDUE2 Temp10		SMDUE2 T10	SMDUE2温度10		SMDUE2温度10
721		32		15		SMDUE1 Temp1 High 2		SMDUE1 T1 Hi2		SMDUE1 T1过过温		SMDUE1T1过过温
722		32		15		SMDUE1 Temp1 High 1		SMDUE1 T1 Hi1		SMDUE1 T1过温		SMDUE1T1过温
723		32		15		SMDUE1 Temp1 Low			SMDUE1 T1 Low		SMDUE1 T1低温		SMDUE1T1低温
724		32		15		SMDUE1 Temp2 High 2	SMDUE1 T2 Hi2			SMDUE1 T2过过温		SMDUE1T2过过温
725		32		15		SMDUE1 Temp2 High 1	SMDUE1 T2 Hi1			SMDUE1 T2过温		SMDUE1T2过温
726		32		15		SMDUE1 Temp2 Low		SMDUE1 T2 Low			SMDUE1 T2低温		SMDUE1T2低温
727		32		15		SMDUE1 Temp3 High 2	SMDUE1 T3 Hi2			SMDUE1 T3过过温		SMDUE1T3过过温
728		32		15		SMDUE1 Temp3 High 1	SMDUE1 T3 Hi1			SMDUE1 T3过温		SMDUE1T3过温
729		32		15		SMDUE1 Temp3 Low		SMDUE1 T3 Low		SMDUE1 T3低温		SMDUE1T3低温
730		32		15		SMDUE1 Temp4 High 2	SMDUE1 T4 Hi2		SMDUE1 T4过过温		SMDUE1T4过过温
732		32		15		SMDUE1 Temp4 High 1	SMDUE1 T4 Hi1		SMDUE1 T4过温		SMDUE1T4过温
732		32		15		SMDUE1 Temp4 Low		SMDUE1 T4 Low		SMDUE1 T4低温		SMDUE1T4低温
733		32		15		SMDUE1 Temp5 High 2	SMDUE1 T5 Hi2		SMDUE1 T5过过温		SMDUE1T5过过温
734		32		15		SMDUE1 Temp5 High 1	SMDUE1 T5 Hi1		SMDUE1 T5过温		SMDUE1T5过温
735		32		15		SMDUE1 Temp5 Low		SMDUE1 T5 Low		SMDUE1 T5低温		SMDUE1T5低温
736		32		15		SMDUE1 Temp6 High 2	SMDUE1 T6 Hi2		SMDUE1 T6过过温		SMDUE1T6过过温
737		32		15		SMDUE1 Temp6 High 1	SMDUE1 T6 Hi1		SMDUE1 T6过温		SMDUE1T6过温
738		32		15		SMDUE1 Temp6 Low		SMDUE1 T6 Low		SMDUE1 T6低温		SMDUE1T6低温
739		32		15		SMDUE1 Temp7 High 2	SMDUE1 T7 Hi2		SMDUE1 T7过过温		SMDUE1T7过过温
740		32		15		SMDUE1 Temp7 High 1	SMDUE1 T7 Hi1		SMDUE1 T7过温		SMDUE1T7过温
741		32		15		SMDUE1 Temp7 Low		SMDUE1 T7 Low		SMDUE1 T7低温		SMDUE1T7低温
742		32		15		SMDUE1 Temp8 High 2	SMDUE1 T8 Hi2		SMDUE1 T8过过温		SMDUE1T8过过温
743		32		15		SMDUE1 Temp8 High 1	SMDUE1 T8 Hi1		SMDUE1 T8过温		SMDUE1T8过温
744		32		15		SMDUE1 Temp8 Low		SMDUE1 T8 Low		SMDUE1 T8低温		SMDUE1T8低温
745		32		15		SMDUE1 Temp9 High 2	SMDUE1 T9 Hi2		SMDUE1 T9过过温		SMDUE1T9过过温
746		32		15		SMDUE1 Temp9 High 1	SMDUE1 T9 Hi1		SMDUE1 T9过温		SMDUE1T9过温
747		32		15		SMDUE1 Temp9 Low		SMDUE1 T9 Low		SMDUE1 T9低温		SMDUE1T9低温
748		32		15		SMDUE1 Temp10 High 2	SMDUE1 T10 Hi2		SMDUE1 T10过过温		SMDUE1T10过过温
749		32		15		SMDUE1 Temp10 High 1	SMDUE1 T10 Hi1		SMDUE1 T10过温		SMDUE1T10过温
750		32		15		SMDUE1 Temp10 Low	SMDUE1 T10 Low		SMDUE1 T10低温		SMDUE1T10低温
751		32		15		SMDUE2 Temp1 High 2		SMDUE2 T1 Hi2		SMDUE2 T1过过温		SMDUE2T1过过温
752		32		15		SMDUE2 Temp1 High 1		SMDUE2 T1 Hi1		SMDUE2 T1过温		SMDUE2T1过温
753		32		15		SMDUE2 Temp1 Low			SMDUE2 T1 Low		SMDUE2 T1低温		SMDUE2T1低温
754		32		15		SMDUE2 Temp2 High 2	SMDUE2 T2 Hi2			SMDUE2 T2过过温		SMDUE2T2过过温
755		32		15		SMDUE2 Temp2 High 1	SMDUE2 T2 Hi1			SMDUE2 T2过温		SMDUE2T2过温
756		32		15		SMDUE2 Temp2 Low		SMDUE2 T2 Low			SMDUE2 T2低温		SMDUE2T2低温
757		32		15		SMDUE2 Temp3 High 2	SMDUE2 T3 Hi2			SMDUE2 T3过过温		SMDUE2T3过过温
758		32		15		SMDUE2 Temp3 High 1	SMDUE2 T3 Hi1			SMDUE2 T3过温		SMDUE2T3过温
759		32		15		SMDUE2 Temp3 Low		SMDUE2 T3 Low		SMDUE2 T3低温		SMDUE2T3低温
760		32		15		SMDUE2 Temp4 High 2	SMDUE2 T4 Hi2		SMDUE2 T4过过温		SMDUE2T4过过温
761		32		15		SMDUE2 Temp4 High 1	SMDUE2 T4 Hi1		SMDUE2 T4过温		SMDUE2T4过温
762		32		15		SMDUE2 Temp4 Low		SMDUE2 T4 Low		SMDUE2 T4低温		SMDUE2T4低温
763		32		15		SMDUE2 Temp5 High 2	SMDUE2 T5 Hi2		SMDUE2 T5过过温		SMDUE2T5过过温
764		32		15		SMDUE2 Temp5 High 1	SMDUE2 T5 Hi1		SMDUE2 T5过温		SMDUE2T5过温
765		32		15		SMDUE2 Temp5 Low		SMDUE2 T5 Low		SMDUE2 T5低温		SMDUE2T5低温
766		32		15		SMDUE2 Temp6 High 2	SMDUE2 T6 Hi2		SMDUE2 T6过过温		SMDUE2T6过过温
767		32		15		SMDUE2 Temp6 High 1	SMDUE2 T6 Hi1		SMDUE2 T6过温		SMDUE2T6过温
768		32		15		SMDUE2 Temp6 Low		SMDUE2 T6 Low		SMDUE2 T6低温		SMDUE2T6低温
769		32		15		SMDUE2 Temp7 High 2	SMDUE2 T7 Hi2		SMDUE2 T7过过温		SMDUE2T7过过温
770		32		15		SMDUE2 Temp7 High 1	SMDUE2 T7 Hi1		SMDUE2 T7过温		SMDUE2T7过温
771		32		15		SMDUE2 Temp7 Low		SMDUE2 T7 Low		SMDUE2 T7低温		SMDUE2T7低温
772		32		15		SMDUE2 Temp8 High 2	SMDUE2 T8 Hi2		SMDUE2 T8过过温		SMDUE2T8过过温
773		32		15		SMDUE2 Temp8 High 1	SMDUE2 T8 Hi1		SMDUE2 T8过温		SMDUE2T8过温
774		32		15		SMDUE2 Temp8 Low		SMDUE2 T8 Low		SMDUE2 T8低温		SMDUE2T8低温
775		32		15		SMDUE2 Temp9 High 2	SMDUE2 T9 Hi2		SMDUE2 T9过过温		SMDUE2T9过过温
776		32		15		SMDUE2 Temp9 High 1	SMDUE2 T9 Hi1		SMDUE2 T9过温		SMDUE2T9过温
777		32		15		SMDUE2 Temp9 Low		SMDUE2 T9 Low		SMDUE2 T9低温		SMDUE2T9低温
778		32		15		SMDUE2 Temp10 High 2	SMDUE2 T10 Hi2		SMDUE2 T10过过温		SMDUE2T10过过温
779		32		15		SMDUE2 Temp10 High 1	SMDUE2 T10 Hi1		SMDUE2 T10过温		SMDUE2T10过温
780		32		15		SMDUE2 Temp10 Low	SMDUE2 T10 Low		SMDUE2 T10低温		SMDUE2T10低温





﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE	
1		32			16			OBFuel Tank				OBFuel Tank		OB油箱			OB油箱
2		32			15			Remaining Fuel Height			Remained Height		油位高度		油位高度
3		32			15			Remaining Fuel Volume			Remained Volume		剩余油量		剩余油量
4		32			15			Remaining Fuel Percent			RemainedPercent		剩余油量百分比		剩余油量百分比
5		32			15			Fuel Theft Alarm			Fuel Theft Alm		偷盗告警状态		偷盗告警状态
6		32			15			No					No			否			否
7		32			15			Yes					Yes			是			是
8		32			15			Multi-Shape Height Error Status		Multi Hgt Err		多边形高度错误状态	多边形高度错误
9		32			15			Setting Configuration Done		Set Config Done		设置配置完成		设置配置完成
10		32			15			Reset Theft Alarm			Reset Theft Alm		偷盗告警清零		偷盗告警清零
11		32			15			Fuel Tank Type				Fuel Tank Type		油罐类型		油罐类型
12		32			15			Square Tank Length			Square Tank L		方体油罐长		方体油罐长
13		32			15			Square Tank Width			Square Tank W		方体油罐宽		方体油罐宽
14		32			15			Square Tank Height			Square Tank H		方体油罐高		方体油罐高
15		32			15			Vertical Cylinder Tank Diameter	Vertical Tank D		立式圆柱油罐直径	立式油罐直径
16		32			15			Vertical Cylinder Tank Height		Vertical Tank H		立式圆柱油罐高		立式油罐高
17		32			15			Horiz Cylinder Tank Diameter		Horiz Tank D		卧式圆柱油罐直径	卧式油罐直径
18		32			15			Horiz Cylinder Tank Length		Horiz Tank L		卧式圆柱油罐长		卧式油罐长
#for multi sharp 20 calibration points
20		32			15			Number of Calibration Points		Num Cal Points		多边形刻度数		多边形刻度数
21		32			15			Height 1 of Calibration Point		Height 1 Point		多边形高度刻度1		高度刻度1
22		32			15			Volume 1 of Calibration Point		Volume 1 Point		多边形容积刻度1		容积刻度1
23		32			15			Height 2 of Calibration Point		Height 2 Point		多边形高度刻度2		高度刻度2
24		32			15			Volume 2 of Calibration Point		Volume 2 Point		多边形容积刻度2		容积刻度2
25		32			15			Height 3 of Calibration Point		Height 3 Point		多边形高度刻度3		高度刻度3
26		32			15			Volume 3 of Calibration Point		Volume 3 Point		多边形容积刻度3		容积刻度3
27		32			15			Height 4 of Calibration Point		Height 4 Point		多边形高度刻度4		高度刻度4
28		32			15			Volume 4 of Calibration Point		Volume 4 Point		多边形容积刻度4		容积刻度4
29		32			15			Height 5 of Calibration Point		Height 5 Point		多边形高度刻度5		高度刻度5
30		32			15			Volume 5 of Calibration Point		Volume 5 Point		多边形容积刻度5		容积刻度5
31		32			15			Height 6 of Calibration Point		Height 6 Point		多边形高度刻度6		高度刻度6
32		32			15			Volume 6 of Calibration Point		Volume 6 Point		多边形容积刻度6		容积刻度6
33		32			15			Height 7 of Calibration Point		Height 7 Point		多边形高度刻度7		高度刻度7
34		32			15			Volume 7 of Calibration Point		Volume 7 Point		多边形容积刻度7		容积刻度7
35		32			15			Height 8 of Calibration Point		Height 8 Point		多边形高度刻度8		高度刻度8
36		32			15			Volume 8 of Calibration Point		Volume 8 Point		多边形容积刻度8		容积刻度8
37		32			15			Height 9 of Calibration Point		Height 9 Point		多边形高度刻度9		高度刻度9
38		32			15			Volume 9 of Calibration Point		Volume 9 Point		多边形容积刻度9		容积刻度9
39		32			15			Height 10 of Calibration Point		Height 10 Point		多边形高度刻度10	高度刻度10
40		32			15			Volume 10 of Calibration Point		Volume 10 Point		多边形容积刻度10	容积刻度10
41		32			15			Height 11 of Calibration Point		Height 11 Point		多边形高度刻度11	高度刻度11
42		32			15			Volume 11 of Calibration Point		Volume 11 Point		多边形容积刻度11	容积刻度11
43		32			15			Height 12 of Calibration Point		Height 12 Point		多边形高度刻度12	高度刻度12
44		32			15			Volume 12 of Calibration Point		Volume 12 Point		多边形容积刻度12	容积刻度12
45		32			15			Height 13 of Calibration Point		Height 13 Point		多边形高度刻度13	高度刻度13
46		32			15			Volume 13 of Calibration Point		Volume 13 Point		多边形容积刻度13	容积刻度13
47		32			15			Height 14 of Calibration Point		Height 14 Point		多边形高度刻度14	高度刻度14
48		32			15			Volume 14 of Calibration Point		Volume 14 Point		多边形容积刻度14	容积刻度14
49		32			15			Height 15 of Calibration Point		Height 15 Point		多边形高度刻度15	高度刻度15
50		32			15			Volume 15 of Calibration Point		Volume 15 Point		多边形容积刻度15	容积刻度15
51		32			15			Height 16 of Calibration Point		Height 16 Point		多边形高度刻度16	高度刻度16
52		32			15			Volume 16 of Calibration Point		Volume 16 Point		多边形容积刻度16	容积刻度16
53		32			15			Height 17 of Calibration Point		Height 17 Point		多边形高度刻度17	高度刻度17
54		32			15			Volume 17 of Calibration Point		Volume 17 Point		多边形容积刻度17	容积刻度17
55		32			15			Height 18 of Calibration Point		Height 18 Point		多边形高度刻度18	高度刻度18
56		32			15			Volume 18 of Calibration Point		Volume 18 Point		多边形容积刻度18	容积刻度18
57		32			15			Height 19 of Calibration Point		Height 19 Point		多边形高度刻度19	高度刻度19
58		32			15			Volume 19 of Calibration Point		Volume 19 Point		多边形容积刻度19	容积刻度19
59		32			15			Height 20 of Calibration Point		Height 20 Point		多边形高度刻度20	高度刻度20
60		32			15			Volume 20 of Calibration Point		Volume 20 Point		多边形容积刻度20	容积刻度20
#61		32			15			None					None			无			无
62		32			15			Square Tank				Square Tank		长方形油罐		长方形油罐
63		32			15			Vertical Cylinder Tank			Vert Cyl Tank		立式圆柱体油罐		立式圆柱体油罐
64		32			15			Horizontal Cylinder Tank		Horiz Cyl Tank		卧式圆柱体油罐		卧式圆柱体油罐
65		32			15			Multi Shape Tank			MultiShapeTank		多边形油罐		多边形油罐
66		32			15			Low Fuel Level Limit			Low Level Limit		油罐最小油量限制	最小油量限制
67		32			15			High Fuel Level Limit			Hi Level Limit		油罐最大油量限制	最大油量限制
68		32			15			Maximum Consumption Speed		Max Flow Speed		最大流量限制		最大流量限制
#for alarm of the volume of remained fuel	
71		32			15			High Fuel Level Alarm			Hi Level Alarm		油罐油量过高		油量过高
72		32			15			Low Fuel Level Alarm			Low Level Alarm		油罐油量过低		油量过低
73		32			15			Fuel Theft Alarm			Fuel Theft Alm		偷盗告警		偷盗告警
74		32			15			Square Tank Height Error		Sq Tank Hgt Err		方形油罐高度错		方形油罐高度错
75		32			15			Vert Cylinder Tank Height Error		Vt Tank Hgt Err		立式圆柱油罐高度错误	立式油罐高度错
76		32			15			Horiz Cylinder Tank Height Error	Hr Tank Hgt Err		卧式圆柱油罐高度错误	卧式油罐高度错
77		32			15			Tank Height Error			Tank Height Err		高度错误		高度错误	
78		32			15			Fuel Tank Config Error			Fuel Config Err		油罐参数配置错误	油罐配置错误
80		32			15			Fuel Tank Config Error Status		Config Err		油罐参数配置错误状态	配置错误状态
81		32			15			X1		X1		X1	X1
82		32			15			Y1		Y1		Y1	Y1
83		32			15			X2		X2		X2	X2
84		32			15			Y2		Y2		Y2	Y2
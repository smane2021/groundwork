﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Battery Current				Batt Current		电池电流			电池电流
2	32			15			Battery Rating(Ah)			Batt Rating(Ah)		电池容量(Ah)		电池容量(Ah)		
3	32			15			Exceed Current Limit			Exceed Curr Lmt		超过电池限流点		超过电池限流点
4	32			15			Battery					Battery			电池			电池
5	32			15			Over Battery Current			Over Current		电池充电过流		电池充电过流
6	32			15			Battery Capacity (%)			Batt Cap (%)		电池容量(%)		电池容量(%)
7	32			15			Battery Voltage				Batt Voltage		电池电压			电池电压
8	32			15			Low Capacity				Low Capacity		容量低			容量低
9	32			15			On					On			正常			正常
10	32			15			Off					Off			断开			断开
11	32			15			Battery Fuse Voltage			Fuse Voltage		电池熔丝电压		电池熔丝电压
12	32			15			Battery Fuse Status			Fuse Status		电池熔丝状态		电池熔丝状态
13	32			15			Fuse Alarm				Fuse Alarm		熔丝告警			熔丝告警
14	32			15			SMDUE Battery				SMDUE Battery		SMDUE电池			SMDUE电池
15	32			15			State					State			状态			状态
16	32			15			Off					Off			断开			断开
17	32			15			On					On			闭合			闭合
18	32			15			Switch					Switch			Swich			Swich
19	32			15			Battery Over Current			Batt Over Curr		电池过流			电池过流
20	32			15			Battery Management			Batt Management		参与电池管理		参与电池管理
21	32			15			Yes					Yes			是			是
22	32			15			No					No			否			否
23	32			15			Over Voltage Limit			Over Volt Limit		电池过压点		电池过压点
24	32			15			Low Voltage Limit			Low Volt Limit		电池欠压点		电池欠压点
25	32			15			Battery Over Voltage			Over Voltage		过压			过压
26	32			15			Battery Under Voltage			Under Voltage		欠压			欠压
27	32			15			Over Current				Over Current		过流点			过流点
28	32			15			Communication Fail			Comm Fail		通讯中断			通讯中断
29	32			15			Times of Communication Fail		Times Comm Fail		通信失败次数		通信失败次数
44	32			15			Battery Temp Number			Batt Temp Num		电池用温度路数		电池用温度路数
87	32			15			No					No			否			否	
91	32			15			Temperature 1				Temperature 1		温度1			温度1
92	32			15			Temperature 2				Temperature 2		温度2			温度2
93	32			15			Temperature 3				Temperature 3		温度3			温度3
94	32			15			Temperature 4				Temperature 4		温度4			温度4
95	32			15			Temperature 5				Temperature 5		温度5			温度5
96	32			15			Rated Capacity				Rated Capacity		标称容量			标称容量	
97	32			15			Battery Temperature			Battery Temp		电池温度			电池温度
98	32			15			Battery Temperature Sensor		BattTemp Sensor		电池温度传感器		电池温度传感器
99	32			15			None				None			无			无	
100	32			15			Temperature 1			Temperature 1		温度1			温度1
101	32			15			Temperature 2			Temperature 2		温度2			温度2
102	32			15			Temperature 3			Temperature 3		温度3			温度3
103	32			15			Temperature 4			Temperature 4		温度4			温度4
104	32			15			Temperature 5			Temperature 5		温度5			温度5
105	32			15			Temperature 6			Temperature 6		温度6			温度6
106	32			15			Temperature 7			Temperature 7		温度7			温度7
107	32			15			Temperature 8			Temperature 8		温度8			温度8
108	32			15			Temperature 9			Temperature 9		温度9			温度9
109	32			15			Temperature 10			Temperature 10		温度10			温度10

110	32			15			SMDUE1 Battery1			SMDUE1 Battery1			SMDUE1电池1		SMDUE1电池1	
111	32			15			SMDUE1 Battery2			SMDUE1 Battery2			SMDUE1电池2		SMDUE1电池2	
112	32			15			SMDUE1 Battery3			SMDUE1 Battery3			SMDUE1电池3		SMDUE1电池3	
113	32			15			SMDUE1 Battery4			SMDUE1 Battery4			SMDUE1电池4		SMDUE1电池4	
114	32			15			SMDUE1 Battery5			SMDUE1 Battery5			SMDUE1电池5		SMDUE1电池5	
115	32			15			SMDUE1 Battery6			SMDUE1 Battery6			SMDUE1电池6		SMDUE1电池6	
116	32			15			SMDUE1 Battery7			SMDUE1 Battery7			SMDUE1电池7		SMDUE1电池7	
117	32			15			SMDUE1 Battery8			SMDUE1 Battery8			SMDUE1电池8		SMDUE1电池8	
118	32			15			SMDUE1 Battery9			SMDUE1 Battery9			SMDUE1电池9		SMDUE1电池9	
119	32			15			SMDUE1 Battery10		SMDUE1 Battery10		SMDUE1电池10	SMDUE1电池10
120	32			15			SMDUE2 Battery1			SMDUE2 Battery1			SMDUE2电池1		SMDUE2电池1	
121	32			15			SMDUE2 Battery2			SMDUE2 Battery2			SMDUE2电池2		SMDUE2电池2	
122	32			15			SMDUE2 Battery3			SMDUE2 Battery3			SMDUE2电池3		SMDUE2电池3	
123	32			15			SMDUE2 Battery4			SMDUE2 Battery4			SMDUE2电池4		SMDUE2电池4	
124	32			15			SMDUE2 Battery5			SMDUE2 Battery5			SMDUE2电池5		SMDUE2电池5	
125	32			15			SMDUE2 Battery6			SMDUE2 Battery6			SMDUE2电池6		SMDUE2电池6	
126	32			15			SMDUE2 Battery7			SMDUE2 Battery7			SMDUE2电池7		SMDUE2电池7	
127	32			15			SMDUE2 Battery8			SMDUE2 Battery8			SMDUE2电池8		SMDUE2电池8	
128	32			15			SMDUE2 Battery9			SMDUE2 Battery9			SMDUE2电池9		SMDUE2电池9	
129	32			15			SMDUE2 Battery10		SMDUE2 Battery10		SMDUE2电池10	SMDUE2电池10


130	32			15			Battery 1			Batt 1			电池1			电池1
131	32			15			Battery 2			Batt 2			电池2			电池2
132	32			15			Battery 3			Batt 3			电池3			电池3
133	32			15			Battery 4			Batt 4			电池4			电池4
134	32			15			Battery 5			Batt 5			电池5			电池5
135	32			15			Battery 6			Batt 6			电池6			电池6
136	32			15			Battery 7			Batt 7			电池7			电池7
137	32			15			Battery 8			Batt 8			电池8			电池8
138	32			15			Battery 9			Batt 9			电池9			电池9
139	32			15			Battery 10			Batt 10			电池10			电池10


﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15		Battery Current				Battery Current		电池电流		电池电流
2	32			15		Battery Rating(Ah)			Batt Rating(Ah)		电池容量(Ah)		电池容量(Ah)		
3	32			15		Battery Current Limit Exceeded		Ov Bat Cur Lmt		超过电池限流点		超过电池限流点
4	32			15		CSU Battery				CSU Battery		CSU电池			CSU电池
5		32			15			Over Battery Current			Over Batt Curr		电池充电过流		电池充电过流
6		32			15			Battery Capacity (%)			Batt Cap (%)		电池容量(%)		电池容量(%)
7	32			15			Battery Voltage				Battery Voltage		电池电压		电池电压
8	32			15			Battery Low Capacity			BattLowCapacity		容量低			容量低
9		32			15			CSU Battery Temperature			CSU Batt Temp		CSU_Bat temp		CSU_Bat temp
10		32			15			CSU Battery Failure			CSU Batt Fail		CSU_Battery failure	CSU_Batteryfail
11		32			15			Existent				Existent		存在			存在
12		32			15			Not Existent				Not Existent		不存在			不存在
28		32			15			Battery Management		Batt Management		参与电池管理		参与电池管理
29		32			15			Yes					Yes			是			是
30		32			15			No					No			否			否
96		32			15			Rated Capacity				Rated Capacity		标称容量		标称容量	

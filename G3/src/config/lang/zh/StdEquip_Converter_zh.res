﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
zh



[RES_INFO]
#R_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE		
1		32		15			Converter			Converter		变流模块			变流模块
2		32		15			Output Voltage			Output Voltage		输出电压			输出电压
3		32		15			Output Current			Output Current		模块电流			模块电流
4		32		15			Temperature			Temperature		模块温度			模块温度
5		32		15			Converter High Serial Number	Conv High SN		模块HighSN			模块HighSN
6		32		15			Converter SN			Converter SN		序列号				序列号
7		32		15			Total Running Time		Running Time		模块总运行时间			模块总运行时间
8		32		15			Converter ID Overlap		Conv ID Overlap		模块ID重复			模块ID重复
9		32		15			Converter Identification Status	Conv ID Status		模块重排			模块重排
10		32		15			Fan Full Speed Status		Fan Full Speed		模块风扇全速			模块风扇全速
11		32		15			EEPROM Fail Status		EEPROM Fail		EEPROM故障			EEPROM故障
12		32		15			Thermal Shutdown Status		Thermal SD		高温关机			高温关机
13		32		15			Input Low Voltage Status	Input Low Volt		输入欠压			输入欠压
14		32		15			High Ambient Temperature Status	High Amb Temp		环境温度高			环境温度高
15		32		15			Walk-In				Walk-In			Walk-in使能			Walk-in使能
16		32		15			On/Off Status			On/Off Status		开关机状态			开关机状态
17		32		15			Stopped Status			Stopped Status		Stopped Status			Stopped Status
18		32		15			Power Limit			Power Limit		模块温度限功率			模块温度限功率
19		32		15			Over Voltage Status (DC)	Over Volt (DC)		直流电压告警上限		电压告警上限
20		32		15			Fan Fail Status			Fan Fail		风扇故障			风扇故障
21		32		15			Converter Fail Status		Converter Fail		模块故障			模块故障
22		32		15			Barcode 1			Barcode 1		Barcode1			Barcode1
23		32		15			Barcode 2			Barcode 2		Barcode2			Barcode2
24		32		15			Barcode 3			Barcode 3		Barcode3			Barcode3
25		32		15			Barcode 4			Barcode 4		Barcode4			Barcode4
26		32		15			EStop/EShutdown Status		EStop/EShutdown		EStop/EShutdown状态		EStop/EShutdown
27		32		15			Communication Status		Comm Status		模块通信状态			模块通信状态
28		32		15			Existence State			Existence State		模块存在			模块存在
29		32		15			DC On/Off Control		DC On/Off Ctrl		模块直流开关机			模块直流开关机
30		32		15			Converter Reset			Converter Reset		过压复位			过压复位
31		32		15			LED Control			LED Control		模块灯控制			模块灯控制
32		32		15			Converter Reset			Converter Reset		模块复位			模块复位
33		32		15			AC Input Fail			AC Input Fail		模块交流停电			模块交流停电
34		32		15			Over Voltage			Over Voltage		直流输出过压			直流输出过压
37		32		15			Current Limit			Current Limit		模块限流			模块限流
39		32		15			Normal				Normal			否				否
40		32		15			Limited				Limited			是				是
45		32		15			Normal				Normal			正常				正常
46		32		15			Full				Full			全速				全速
47		32		15			Disabled			Disabled		无效				无效
48		32		15			Enabled				Enabled			有效				有效
49		32		15			On				On			开				开
50		32		15			Off				Off			关				关
51		32		15			Normal				Normal			正常				正常
52		32		15			Fail				Fail			故障				故障
53		32		15			Normal				Normal			正常				正常
54		32		15			Over Temperature		Over Temp		过温				过温
55		32		15			Normal				Normal			正常				正常
56		32		15			Fail				Fail			故障				故障
57		32		15			Normal				Normal			正常				正常
58		32		15			Protected			Protected		保护				保护
59		32		15			Normal				Normal			正常				正常
60		32		15			Fail				Fail			故障				故障
61		32		15			Normal				Normal			正常				正常
62		32		15			Alarm				Alarm			告警				告警
63		32		15			Normal				Normal			正常				正常
64		32		15			Fail				Fail			故障				故障
65		32		15			Off				Off			关				关
66		32		15			On				On			开				开
67		32		15			Reset				Reset			复位				复位
68		32		15			Normal				Normal			正常				正常
69		32		15			Flash				Flash			灯闪				灯闪
70		32		15			Stop Flashing			Stop Flashing		不闪				不闪
71		32		15			Off				Off			关				关
72		32		15			Reset				Reset			复位				复位
73		32		15			Converter On			Converter On		开				开
74		32		15			Converter Off			Converter Off		关				关
75		32		15			Off				Off			关				关
76		32		15			LED Control			LED Control		闪灯				闪灯
77		32		15			Converter Reset			Converter Reset		模块复位			模块复位
80		32		15			Communication Fail		Comm Fail		DC/DC模块通讯中断		DC/DC通讯中断
84		32		15			Converter High Serial Number	Conv High SN		模块高序列号			模块高序列号
85		32		15			Converter Version		Conv Version		模块版本			模块版本
86		32		15			Converter Part Number		Conv Part Num		模块产品号			模块产品号
87		32		15			Current Share State		Current Share		模块均流状态			模块均流状态
88		32		15			Current Share Alarm		Curr Share Alm		模块不均流			模块不均流
89		32		15			HVSD Alarm			HVSD Alarm		模块过压脱离			模块过压脱离
90		32		15			Normal				Normal			正常				正常
91		32		15			Over Voltage			Over Voltage		过压				过压
92		32		15			Line AB Voltage			Line AB Volt		线电压 AB			线电压 AB
93		32		15			Line BC Voltage			Line BC Volt		线电压 BC			线电压 BC
94		32		15			Line CA Voltage			Line CA Volt		线电压 CA			线电压 CA
95		32		15			Low Voltage			Low Voltage		欠压				欠压
96		32		15			AC Under Voltage Protection	U-Volt Protect		模块交流欠压保护		交流欠压保护
97		32		15			Converter ID			Converter ID		模块位置号			模块位置号
98		32		15			DC Output Shut Off		DC Output Off		模块直流输出关			直流输出关
99		32		15			Converter Phase			Converter Phase		模块相位			模块相位
100		32		15			A				A			A				A
101		32		15			B				B			B				B
102		32		15			C				C			C				C
103		32		15			Severe Sharing Current Alarm	SevereCurrShare		严重模块不均流			严重模块不均流
104		32		15			Barcode 1			Barcode 1		Bar Code1			Bar Code1
105		32		15			Barcode 2			Barcode 2		Bar Code2			Bar Code2
106		32		15			Barcode 3			Barcode 3		Bar Code3			Bar Code3
107		32		15			Barcode 4			Barcode 4		Bar Code4			Bar Code4
108		32		15			Converter Communication Fail	Conv Comm Fail		通信中断			通信中断
109		32		15			No				No			否				否
110		32		15			Yes				Yes			是				是
111		32		15			Existence State			Existence State		设备是否存在			设备是否存在
112		32		15			Converter Fail			Converter Fail		模块故障			模块故障
113		32		15			Communication OK		Comm OK			模块通讯正常			模块通讯正常
114		32		15			All Converters Comm Fail	AllConvCommFail		模块都通讯中断			都通讯中断
115		32		15			Communication Fail		Comm Fail		模块通讯中断			模块通讯中断
116		32		15			Valid Rated Current		Rated Current		有效额定电流			有效额定电流
117		32		15			Efficiency			Efficiency		模块效率			模块效率
118		32		15			Input Rated Voltage		Input RatedVolt		额定输入电压			额定输入电压
119		32		15			Output Rated Voltage		OutputRatedVolt		额定输出电压			额定输出电压
120		32		15			LT 93				LT 93			小于93				小于93
121		32		15			GT 93				GT 93			大于93				大于93
122		32		15			GT 95				GT 95			大于95				大于95
123		32		15			GT 96				GT 96			大于96				大于96
124		32		15			GT 97				GT 97			大于97				大于97
125		32		15			GT 98				GT 98			大于98				大于98
126		32		15			GT 99				GT 99			大于99				大于99

276		32		15			EStop/EShutdown			EStop/EShutdown		EStop/EShutdown			EStop/EShutdown
277		32		15			Fan Fail			Fan Fail		风扇故障			风扇故障
278		32		15			Input Low Voltage		Input Low Volt		输入欠压			输入欠压
279		32		15			Converter ID			Converter ID		模块位置号			模块位置号

280		32		15			EEPROM Fail			EEPROM Fail		EEPROM 告警			EEPROM 告警
281		32		15			Thermal Shutdown		Thermal SD		热关机				热关机
282		32		15			High Temperature		High Temp		环境温度高			环境温度高
283		32		15			Thermal Power Limit		Therm Power Lmt		温度限功率			温度限功率
284		32		15			Fan Fail			Fan Fail		风扇故障			风扇故障
285		32		15			Converter Fail			Converter Fail		模块故障			模块故障
286		32		15			Mod ID Overlap			Mod ID Overlap		ID 重复				ID 重复
287		32		15			Low Input Volt			Low Input Volt		输入电压低			输入电压低
288		32		15			Under Voltage			Under Voltage		欠压				欠压
289		32		15			Over Voltage			Over Voltage		过压				过压
290		32		15			Over Current			Over Current		过流				过流
291		32		15			GT 94				GT 94			大于94				大于94
292		32		15			Under Voltage(24V)		Under Volt(24V)		欠压				欠压(24V)
293		32		15			Over Voltage(24V)		Over Volt(24V)		过压				过压(24V)
294		32			15			Converter Summary Alarm			ConvSummaryAlm		Conv告警		Conv告警


﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Bus Bar Voltage				Bus Bar Voltage		母排电压		母排电压
2	32			15			Channel 1 Current			Channel1 Curr		通道1电流		通道1电流
3	32			15			Channel 2 Current			Channel2 Curr		通道2电流		通道2电流
4	32			15			Channel 3 Current			Channel3 Curr		通道3电流		通道3电流
5	32			15			Channel 4 Current			Channel4 Curr		通道4电流		通道4电流
6	32			15			Channel 1 Energy Consumption		Channel1 Energy		通道1电量		通道1电量
7	32			15			Channel 2 Energy Consumption		Channel2 Energy		通道2电量		通道2电量
8	32			15			Channel 3 Energy Consumption		Channel3 Energy		通道3电量		通道3电量
9	32			15			Channel 4 Energy Consumption		Channel4 Energy		通道4电量		通道4电量

10	32			15			Channel 1			Channel1		通道1使能		通道1使能
11	32			15			Channel 2			Channel2		通道2使能		通道2使能
12	32			15			Channel 3			Channel3		通道3使能		通道3使能
13	32			15			Channel 4			Channel4		通道4使能		通道4使能

14	32			15			Clear Channel 1 Energy			ClrChan1Energy		清除通道1电量		清除通道1电量
15	32			15			Clear Channel 2 Energy			ClrChan2Energy		清除通道2电量		清除通道2电量
16	32			15			Clear Channel 3 Energy			ClrChan3Energy		清除通道3电量		清除通道3电量
17	32			15			Clear Channel 4 Energy			ClrChan4Energy		清除通道4电量		清除通道4电量

18	32			15			Shunt 1 Voltage				Shunt1 Volt		分流器1电压		分流器1电压
19	32			15			Shunt 1 Current				Shunt1 Curr		分流器1电流		分流器1电流
20	32			15			Shunt 2 Voltage				Shunt2 Volt		分流器2电压		分流器2电压
21	32			15			Shunt 2 Current				Shunt2 Curr		分流器2电流		分流器2电流
22	32			15			Shunt 3 Voltage				Shunt3 Volt		分流器3电压		分流器3电压
23	32			15			Shunt 3 Current				Shunt3 Curr		分流器3电流		分流器3电流
24	32			15			Shunt 4 Voltage				Shunt4 Volt		分流器4电压		分流器4电压
25	32			15			Shunt 4 Current				Shunt4 Curr		分流器4电流		分流器4电流

26	32			15			Enabled					Enabled			启用			启用
27	32			15			Disabled				Disabled		禁用			禁用

28	32			15			Existence State				Exist State		存在状态		存在状态
29	32			15			Communication Fail			Comm Fail		通信中断		通信中断

30	32			15			DC Meter				DC Meter		直流电表		直流电表

31	32			15			Clear					Clear			清除			清除

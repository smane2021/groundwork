﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh



[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15		Fuse 1			Fuse 1			支路1			支路1
2	32			15		Fuse 2			Fuse 2			支路2			支路2
3	32			15		Fuse 3			Fuse 3			支路3			支路3
4	32			15		Fuse 4			Fuse 4			支路4			支路4
5	32			15		Fuse 5			Fuse 5			支路5			支路5
6	32			15		Fuse 6			Fuse 6			支路6			支路6
7	32			15		Fuse 7			Fuse 7			支路7			支路7
8	32			15		Fuse 8			Fuse 8			支路8			支路8
9	32			15		Fuse 9			Fuse 9			支路9			支路9
10	32			15		Fuse 10			Fuse 10			支路10			支路10
11	32			15		Fuse 11			Fuse 11			支路11			支路11
12	32			15		Fuse 12			Fuse 12			支路12			支路12
13	32			15		Fuse 13			Fuse 13			支路13			支路13
14	32			15		Fuse 14			Fuse 14			支路14			支路14
15	32			15		Fuse 15			Fuse 15			支路15			支路15
16	32			15		Fuse 16			Fuse 16			支路16			支路16
17	32			15		SMDU5 DC Fuse		SMDU5 DC Fuse		SMDU5负载支路		SMDU5负载支路
18	32			15			State			State			State			State
19	32			15			Off			Off			断开			断开
20	32			15			On			On			连接			连接
21	32			15		Fuse 1 Alarm		DC Fuse 1 Alm		支路1告警		负载支路1告警
22	32			15		Fuse 2 Alarm		DC Fuse 2 Alm		支路2告警		负载支路2告警
23	32			15		Fuse 3 Alarm		DC Fuse 3 Alm		支路3告警		负载支路3告警
24	32			15		Fuse 4 Alarm		DC Fuse 4 Alm		支路4告警		负载支路4告警
25	32			15		Fuse 5 Alarm		DC Fuse 5 Alm		支路5告警		负载支路5告警
26	32			15		Fuse 6 Alarm		DC Fuse 6 Alm		支路6告警		负载支路6告警
27	32			15		Fuse 7 Alarm		DC Fuse 7 Alm		支路7告警		负载支路7告警
28	32			15		Fuse 8 Alarm		DC Fuse 8 Alm		支路8告警		负载支路8告警
29	32			15		Fuse 9 Alarm		DC Fuse 9 Alm		支路9告警		负载支路9告警
30	32			15		Fuse 10 Alarm		DC Fuse 10 Alm		支路10告警		负载支路10告警
31	32			15		Fuse 11 Alarm		DC Fuse 11 Alm		支路11告警		负载支路11告警
32	32			15		Fuse 12 Alarm		DC Fuse 12 Alm		支路12告警		负载支路12告警
33	32			15		Fuse 13 Alarm		DC Fuse 13 Alm		支路13告警		负载支路13告警
34	32			15		Fuse 14 Alarm		DC Fuse 14 Alm		支路14告警		负载支路14告警
35	32			15		Fuse 15 Alarm		DC Fuse 15 Alm		支路15告警		负载支路15告警
36	32			15		Fuse 16 Alarm		DC Fuse 16 Alm		支路16告警		负载支路16告警
37	32			15			Times of Communication Fail		Times Comm Fail		通信失败次数		通信失败次数
38	32			15			Communication Fail	Comm Fail		通讯中断		通讯中断
39	32			15			Load 1 Current				Load 1 Current		负载电流1		负载电流1
40	32			15			Load 2 Current				Load 2 Current		负载电流2		负载电流2
41	32			15			Load 3 Current				Load 3 Current		负载电流3		负载电流3
42	32			15			Load 4 Current				Load 4 Current		负载电流4		负载电流4
43	32			15			Load 5 Current				Load 5 Current		负载电流5		负载电流5


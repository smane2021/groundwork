﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15		Battery Current				Battery Current		电池电流		电池电流			
2	32			15		Battery Rating(Ah)			Batt Rating(Ah)		电池容量(Ah)		电池容量(Ah)			
3	32			15		Battery Current Limit Exceeded		Ov Batt Cur Lmt		超过电池限流点		超过电池限流点		
4	32			15		Battery					Battery			电池			电池		
5		32			15			Battery Over Current			Batt Over Curr		电池充电过流		电池充电过流		
6		32			15			Battery Capacity (%)			Batt Cap(%)		电池容量(%)		电池容量(%)		
7	32			15			Battery Voltage				Battery Voltage		电池电压		电池电压		
8	32			15			Battery Low Capacity			Batt Low Cap		容量低			容量低		
9		32			15			Battery Fuse Failure			Batt Fuse Fail		电池支路断		电池支路断		
10		32			15			DC Distribution Sequence Number		DC Dist Seq Num		直流屏序号		直流屏序号		
11		32			15			Battery Over Voltage			Batt Over Volt		电池过压		电池过压			
12		32			15			Battery Under Voltage			Batt Under Volt		电池欠压		电池欠压		
13		32			15			Battery Over Current			Batt Over Curr		电池充电过流		电池充电过流		
14		32			15			Battery Fuse Failure			Batt Fuse Fail		电池熔丝断		熔丝断		
15		32			15			Battery Over Voltage			Batt Over Volt		电池过压		电池过压		
16		32			15			Battery Under Voltage			Batt Under Volt		电池欠压		电池欠压		
17		32			15			Battery Over Current			Batt Over Curr		电池过流		电池过流		
18		32			15			LargeDU Battery				LargeDU Batt		LargeDU电池串		LargeDU电池串
19		32			15			Battery Shunt Coefficient		Bat Shunt Coeff		电池电流系数		电流系数
20		32			15			Over Voltage Limit			Over Volt Limit		电池过压点		电池过压点
21		32			15			Low Voltage Limit			Low Volt Limit		电池欠压点		电池欠压点
22		32			15			Battery Communication Fail		Batt Comm Fail		电池通信中断		电池通信中断
23		32			15			Communication OK			Comm OK			通信正常		通信正常
24		32			15			Communication Fail			Comm Fail		电池通信中断		电池通信中断
25		32			15			Communication Fail			Comm Fail		电池通信中断		电池通信中断
26		32			15			Existence State				Existence State		是否存在		是否存在
27		32			15			Existent				Existent		存在			存在
28		32			15			Not Existent				Not Existent		不存在			不存在
29		32			15			Rated Capacity				Rated Capacity		标称容量		标称容量	
30		32			15			Battery Management		Batt Management		用于电池管理		用于电池管理
31		32			15			Enabled					Enabled			是			是
32		32			15			Disabled				Disabled		否			否
97		32			15			Battery Temperature			Battery Temp		电池温度		电池温度
98		32			15			Battery Temperature Sensor		Bat Temp Sensor		电池温度传感器		电池温度传感器
99		32			15			None					None			无			无	
100		32			15			Temperature 1				Temperature 1		温度1			温度1
101		32			15			Temperature 2				Temperature 2		温度2			温度2
102		32			15			Temperature 3				Temperature 3		温度3			温度3
103		32			15			Temperature 4				Temperature 4		温度4			温度4
104		32			15			Temperature 5				Temperature 5		温度5			温度5
105		32			15			Temperature 6				Temperature 6		温度6			温度6
106		32			15			Temperature 7				Temperature 7		温度7			温度7
107		32			15			Temperature 8				Temperature 8		温度8			温度8
108		32			15			Temperature 9				Temperature 9		温度9			温度9
109		32			15			Temperature 10				Temperature 10		温度10			温度10







﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN			FULL_IN_LOCALE			ABBR_IN_LOCALE			
1		32			15			Diesel Generator Battery Voltage	Diesel Bat Volt			油机电池电压			油机电池电压	
2		32			15			Diesel Generator Running		Diesel Running			油机运行状态			油机运行状态	
3		32			15			Relay 2 Status				Relay 2 Status			继电器2状态			继电器2状态	
4		32			15			Relay 3 Status				Relay 3 Status			继电器3状态			继电器3状态	
5		32			15			Relay 4 Status				Relay 4 Status			继电器4状态			继电器4状态	
6		32			15			Diesel Generator Failure Status		Diesel Fail			油机故障状态			油机故障状态	
7		32			15			DG Connected Status		DG Connect			油机连接状态			油机连接状态	
8		32			15			Low Fuel Level Status			Low Fuel Level			低油位				低油位	
9		32			15			High Water Temperature Status		High Water Temp			高水温				高水温		
10		32			15			Low Oil Pressure Status			Low Oil Press			低油压				低油压		
11		32			15			Start Diesel Generator			Start Diesel			启动油机			启动油机	
12		32			15			Relay 2 On/Off				Relay 2 On/Off			继电器2 开/关			继电器2 开/关	
13		32			15			Relay 3 On/Off				Relay 3 On/Off			继电器3 开/关			继电器3 开/关	
14		32			15			Relay 4 On/Off				Relay 4 On/Off			继电器4 开/关			继电器4 开/关	
15		32			15			Battery Voltage Limit			Batt Volt Limit			电池电压限点			电池电压限点	
16		32			15			Relay 1 Pulse Time			Relay1 Pulse T			继电器1脉冲时间			继电器1脉冲时间	
17		32			15			Relay 2 Pulse Time			Relay2 Pulse T			继电器2脉冲时间			继电器2脉冲时间	
18		32			15			Relay 1 Pulse Time			Relay1 Pulse T			继电器1脉冲时间			继电器1脉冲时间	
19		32			15			Relay 2 Pulse Time			Relay2 Pulse T			继电器2脉冲时间			继电器2脉冲时间	
20		32			15			Low DC Voltage				Low DC Voltage			低直流电压			低直流电压	
21		32			15			DG Supervision Failure			SupervisionFail			监控故障			监控故障	
22		32			15			Diesel Generator Failure		Diesel Fail			油机故障			油机故障	
23		32			15			Diesel Generator Connected		Diesel Connect			油机连接			油机连接	
24		32			15			Not Running				Not Running			停机				停机	
25		32			15			Running					Running				运行				运行	
26		32			15			Off					Off				关				关	
27		32			15			On					On				开				开	
28		32			15			Off					Off				关				关	
29		32			15			On					On				开				开	
30		32			15			Off					Off				关				关	
31		32			15			On					On				开				开	
32		32			15			No					No				否				否	
33		32			15			Yes					Yes				是				是	
34		32			15			No					No				否				否	
35		32			15			Yes					Yes				是				是	
36		32			15			Off					Off				关				关	
37		32			15			On					On				开				开	
38		32			15			Off					Off				关				关	
39		32			15			On					On				开				开	
40		32			15			Off					Off				关				关	
41		32			15			On					On				开				开	
42		32			15			Diesel Generator			Dsl Generator			柴油发电机			柴油发电机
43		32			15			Mains Connected				Mains Connected			市电供电			市电供电
44		32			15			Diesel Generator Shutdown		Diesel Shutdown			关油机				关油机
45		32			15			Low Fuel Level				Low Fuel Level			低油位告警			低油位告警
46		32			15			High Water Temperature			High Water Temp			高水温告警			高水温告警
47		32			15			Low Oil Pressure			Low Oil Press			低油压告警			低油压告警
48		32			15			Communication Failure			Comm Fail			SMAC通讯失败			SMAC通讯失败
49		32			15			Low Oil Pressure Alarm Status		Low Oil Press			低油压告警			低油压告警
50		32			15			Clear Low Oil Pressure Alarm		Clr Oil Press			清除油压低告警			清除油压低告警
51		32			15			State					State				State				State
52		32			15			Existence State				Existence State			是否存在			是否存在
53		32			15			Existent				Existent			存在				存在
54		32			15			Not Existent				Not Existent			不存在				不存在
55		32			15			Total Run Time				Total Run Time			未维护已运行时间		未维护已运行
56		32			15			Maintenance Time Limit			Mtn Time Limit			维护周期			维护周期
57		32			15			Clear Total Run Time			Clr Run Time			清除未维护已运行时间		清除已运行时间
58		32			15			Periodical Maintenance Required		Mtn Required			维护时间到			维护时间到
59		32			15			Maintenance Countdown Timer		Mtn DownTimer			维护倒计时			维护倒计时





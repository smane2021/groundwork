﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			SMDUE Group		SMDUE Group		SMDUE组			SMDUE组
2		32			15			Number of SMDUEs	Num of SMDUEs		SM-DUE数量		SM-DUE数量
3		32			15			Communication Fail	Comm Fail		通讯中断		通讯中断
4		32			15			Existence State		Existence State		是否存在		是否存在
5		32			15			Comm OK			Comm OK			通讯正常		通讯正常
6		32			15			Communication Fail	Comm Fail		通讯中断		通讯中断
7		32			15			Existent		Existent		存在			存在
8		32			15			Not Existent		Not Existent		不存在			不存在

9		32			15			SMDUE Changed(LCD)		SMDUE Chg(LCD)		SMDUE改变(LCD)			SMDUE改变(LCD)
10		32			15			SMDUE Changed(WEB)		SMDUE Chg(WEB)		SMDUE改变(WEB)			SMDUE改变(WEB)
11		32			15			Total SMDUELoad Current		Total Load Curr		SMDUE总负载电流			总负载电流
12		32			15			Not Changed		Not Changed		没有改变			没有改变
13		32			15			Changed			Changed			改变				改变

﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Battery Current				Batt Current		电池电流			电池电流
2	32			15			Battery Rating(Ah)			Batt Rating(Ah)		电池容量(Ah)		电池容量(Ah)		
3	32			15			Exceed Current Limit			Exceed Curr Lmt		超过电池限流点		超过电池限流点
4	32			15			Battery					Battery			电池			电池
5	32			15			Over Battery Current			Over Current		电池充电过流		电池充电过流
6	32			15			Battery Capacity (%)			Batt Cap (%)		电池容量(%)		电池容量(%)
7	32			15			Battery Voltage				Batt Voltage		电池电压			电池电压
8	32			15			Low Capacity				Low Capacity		容量低			容量低
9	32			15			On					On			正常			正常
10	32			15			Off					Off			断开			断开
11	32			15			Battery Fuse Voltage			Fuse Voltage		电池熔丝电压		电池熔丝电压
12	32			15			Battery Fuse Status			Fuse Status		电池熔丝状态		电池熔丝状态
13	32			15			Fuse Alarm				Fuse Alarm		熔丝告警			熔丝告警
14	32			15			SMDU Battery				SMDU Battery		SMDU电池			SMDU电池
15	32			15			State					State			状态			状态
16	32			15			Off					Off			断开			断开
17	32			15			On					On			闭合			闭合
18	32			15			Switch					Switch			Swich			Swich
19	32			15			Battery Over Current			Batt Over Curr		电池过流			电池过流
20	32			15			Battery Management			Batt Management		参与电池管理		参与电池管理
21	32			15			Yes					Yes			是			是
22	32			15			No					No			否			否
23	32			15			Over Voltage Limit			Over Volt Limit		电池过压点		电池过压点
24	32			15			Low Voltage Limit			Low Volt Limit		电池欠压点		电池欠压点
25	32			15			Battery Over Voltage			Over Voltage		过压			过压
26	32			15			Battery Under Voltage			Under Voltage		欠压			欠压
27	32			15			Over Current				Over Current		过流点			过流点
28	32			15			Communication Fail			Comm Fail		通讯中断			通讯中断
29	32			15			Times of Communication Fail		Times Comm Fail		通信失败次数		通信失败次数
44	32			15			Battery Temp Number			Batt Temp Num		电池用温度路数		电池用温度路数
87	32			15			No					No			否			否	
91	32			15			Temperature 1				Temperature 1		温度1			温度1
92	32			15			Temperature 2				Temperature 2		温度2			温度2
93	32			15			Temperature 3				Temperature 3		温度3			温度3
94	32			15			Temperature 4				Temperature 4		温度4			温度4
95	32			15			Temperature 5				Temperature 5		温度5			温度5
96	32			15			Rated Capacity				Rated Capacity		标称容量			标称容量	
97	32			15			Battery Temperature			Battery Temp		电池温度			电池温度
98	32			15			Battery Temperature Sensor		BattTemp Sensor		电池温度传感器		电池温度传感器
99	32			15			None				None			无			无	
100	32			15			Temperature 1			Temperature 1		温度1			温度1
101	32			15			Temperature 2			Temperature 2		温度2			温度2
102	32			15			Temperature 3			Temperature 3		温度3			温度3
103	32			15			Temperature 4			Temperature 4		温度4			温度4
104	32			15			Temperature 5			Temperature 5		温度5			温度5
105	32			15			Temperature 6			Temperature 6		温度6			温度6
106	32			15			Temperature 7			Temperature 7		温度7			温度7
107	32			15			Temperature 8			Temperature 8		温度8			温度8
108	32			15			Temperature 9			Temperature 9		温度9			温度9
109	32			15			Temperature 10			Temperature 10		温度10			温度10
110	32			15			SMDU1 Battery1			SMDU1 Battery1		SMDU1电池1		SMDU1电池1
111	32			15			SMDU1 Battery2			SMDU1 Battery2		SMDU1电池2		SMDU1电池2
112	32			15			SMDU1 Battery3			SMDU1 Battery3		SMDU1电池3		SMDU1电池3
113	32			15			SMDU1 Battery4			SMDU1 Battery4		SMDU1电池4		SMDU1电池4
114	32			15			SMDU1 Battery5			SMDU1 Battery5		SMDU1电池5		SMDU1电池5
115	32			15			SMDU2 Battery1			SMDU2 Battery1		SMDU2电池1		SMDU2电池1
116	32			15			SMDU2 Battery2			SMDU2 Battery2		SMDU2电池2		SMDU2电池2
117	32			15			SMDU2 Battery3			SMDU2 Battery3		SMDU2电池3		SMDU2电池3
118	32			15			SMDU2 Battery4			SMDU2 Battery4		SMDU2电池4		SMDU2电池4
119	32			15			SMDU2 Battery5			SMDU2 Battery5		SMDU2电池5		SMDU2电池5
120	32			15			SMDU3 Battery1			SMDU3 Battery1		SMDU3电池1		SMDU3电池1
121	32			15			SMDU3 Battery2			SMDU3 Battery2		SMDU3电池2		SMDU3电池2
122	32			15			SMDU3 Battery3			SMDU3 Battery3		SMDU3电池3		SMDU3电池3
123	32			15			SMDU3 Battery4			SMDU3 Battery4		SMDU3电池4		SMDU3电池4
124	32			15			SMDU3 Battery5			SMDU3 Battery5		SMDU3电池5		SMDU3电池5
125	32			15			SMDU4 Battery1			SMDU4 Battery1		SMDU4电池1		SMDU4电池1
126	32			15			SMDU4 Battery2			SMDU4 Battery2		SMDU4电池2		SMDU4电池2
127	32			15			SMDU4 Battery3			SMDU4 Battery3		SMDU4电池3		SMDU4电池3
128	32			15			SMDU4 Battery4			SMDU4 Battery4		SMDU4电池4		SMDU4电池4
129	32			15			SMDU4 Battery5			SMDU4 Battery5		SMDU4电池5		SMDU4电池5
130	32			15			SMDU5 Battery1			SMDU5 Battery1		SMDU5电池1		SMDU5电池1
131	32			15			SMDU5 Battery2			SMDU5 Battery2		SMDU5电池2		SMDU5电池2
132	32			15			SMDU5 Battery3			SMDU5 Battery3		SMDU5电池3		SMDU5电池3
133	32			15			SMDU5 Battery4			SMDU5 Battery4		SMDU5电池4		SMDU5电池4
134	32			15			SMDU5 Battery5			SMDU5 Battery5		SMDU5电池5		SMDU5电池5
135	32			15			SMDU6 Battery1			SMDU6 Battery1		SMDU6电池1		SMDU6电池1
136	32			15			SMDU6 Battery2			SMDU6 Battery2		SMDU6电池2		SMDU6电池2
137	32			15			SMDU6 Battery3			SMDU6 Battery3		SMDU6电池3		SMDU6电池3
138	32			15			SMDU6 Battery4			SMDU6 Battery4		SMDU6电池4		SMDU6电池4
139	32			15			SMDU6 Battery5			SMDU6 Battery5		SMDU6电池5		SMDU6电池5
140	32			15			SMDU7 Battery1			SMDU7 Battery1		SMDU7电池1		SMDU7电池1
141	32			15			SMDU7 Battery2			SMDU7 Battery2		SMDU7电池2		SMDU7电池2
142	32			15			SMDU7 Battery3			SMDU7 Battery3		SMDU7电池3		SMDU7电池3
143	32			15			SMDU7 Battery4			SMDU7 Battery4		SMDU7电池4		SMDU7电池4
144	32			15			SMDU7 Battery5			SMDU7 Battery5		SMDU7电池5		SMDU7电池5
145	32			15			SMDU8 Battery1			SMDU8 Battery1		SMDU8电池1		SMDU8电池1
146	32			15			SMDU8 Battery2			SMDU8 Battery2		SMDU8电池2		SMDU8电池2
147	32			15			SMDU8 Battery3			SMDU8 Battery3		SMDU8电池3		SMDU8电池3
148	32			15			SMDU8 Battery4			SMDU8 Battery4		SMDU8电池4		SMDU8电池4
149	32			15			SMDU8 Battery5			SMDU8 Battery5		SMDU8电池5		SMDU8电池5

151	32			15			Battery 1			Batt 1			电池1			电池1
152	32			15			Battery 2			Batt 2			电池2			电池2
153	32			15			Battery 3			Batt 3			电池3			电池3
154	32			15			Battery 4			Batt 4			电池4			电池4
155	32			15			Battery 5			Batt 5			电池5			电池5


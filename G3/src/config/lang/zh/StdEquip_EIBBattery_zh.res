﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15		EIB Battery				EIB Battery		EIB电池			EIB电池		
2	32			15		Battery Current				Battery Current		电池电流			电池电流			
3	32			15		Battery Voltage				Battery Voltage		电池电压			电池电压		
4	32			15		Battery Rating(Ah)			Batt Rating(Ah)		电池容量(Ah)		电池容量(Ah)			
5	32			15		Battery Capacity (%)			Batt Cap (%)		电池容量(%)		电池容量(%)		
6	32			15		Battery Current Limit Exceeded		Ov Bat Curr Lmt		超过电池限流点		超过电池限流点		
7	32			15		Battery Over Current			Bat Over Curr		电池充电过流		电池充电过流		
8	32			15		Battery Low Capacity			Low Batt Cap		容量低			容量低		
9		32			15			Yes					Yes			是			是
10		32			15			No					No			否			否
26		32			15			State					State			状态			状态
27		32			15			Battery Block 1 Voltage			Bat Block1 Volt		电池块1电压		电池块1电压
28		32			15			Battery Block 2 Voltage			Bat Block2 Volt		电池块2电压		电池块2电压
29		32			15			Battery Block 3 Voltage			Bat Block3 Volt		电池块3电压		电池块3电压
30		32			15			Battery Block 4 Voltage			Bat Block4 Volt		电池块4电压		电池块4电压
31		32			15			Battery Block 5 Voltage			Bat Block5 Volt		电池块5电压		电池块5电压
32		32			15			Battery Block 6 Voltage			Bat Block6 Volt		电池块6电压		电池块6电压
33		32			15			Battery Block 7 Voltage			Bat Block7 Volt		电池块7电压		电池块7电压
34		32			15			Battery Block 8 Voltage			Bat Block8 Volt		电池块8电压		电池块8电压
35		32			15			Battery Management			Batt Management		参与电池管理		参与电池管理
36		32			15			Enabled					Enabled			允许			允许
37		32			15			Disabled				Disabled		不允许			不允许
38		32			15			Communication Fail			Comm Fail		通信中断		通信中断
39		32			15			Shunt Full Current			Shunt Full Curr		分流器额定电流		分流器额定电流
40		32			15			Shunt Full Voltage			Shunt Full Volt		分流器额定电压		分流器额定电压
41		32			15			On					On			开			开
42		32			15			Off					Off			关			关
43		32			15			Communication Fail			Comm Fail		通信中断		通信中断
44		32			15			Battery Temperature Probe Number	BattTempPrbNum		电池用温度路数		电池用温度路数
87		32			15			No					No			否			否	
91		32			15			Temperature 1				Temperature 1		温度1			温度1
92		32			15			Temperature 2				Temperature 2		温度2			温度2
93		32			15			Temperature 3				Temperature 3		温度3			温度3
94		32			15			Temperature 4				Temperature 4		温度4			温度4
95		32			15			Temperature 5				Temperature 5		温度5			温度5
96		32			15			Rated Capacity				Rated Capacity		标称容量		标称容量	
97		32			15			Battery Temperature			Battery Temp		电池温度		电池温度
98		32			15			Battery Tempurature Sensor		Bat Temp Sensor		电池温度传感器		电池温度传感器
99		32			15			None					None			无			无	
100		32			15			Temperature 1				Temperature 1		温度1			温度1
101		32			15			Temperature 2				Temperature 2		温度2			温度2
102		32			15			Temperature 3				Temperature 3		温度3			温度3
103		32			15			Temperature 4				Temperature 4		温度4			温度4
104		32			15			Temperature 5				Temperature 5		温度5			温度5
105		32			15			Temperature 6				Temperature 6		温度6			温度6
106		32			15			Temperature 7				Temperature 7		温度7			温度7
107		32			15			Temperature 8				Temperature 8		温度8			温度8
108		32			15			Temperature 9				Temperature 9		温度9			温度9
109		32			15			Temperature 10				Temperature 10		温度10			温度10
110		32			15			EIB1 Battery1				EIB1 Battery1		EIB1电池1		EIB1电池1		
111		32			15			EIB1 Battery2				EIB1 Battery2		EIB1电池2		EIB1电池2		
112		32			15			EIB1 Battery3				EIB1 Battery3		EIB1电池3		EIB1电池3		
113		32			15			EIB2 Battery1				EIB2 Battery1		EIB2电池1		EIB2电池1		
114		32			15			EIB2 Battery2				EIB2 Battery2		EIB2电池2		EIB2电池2		
115		32			15			EIB2 Battery3				EIB2 Battery3		EIB2电池3		EIB2电池3		
116		32			15			EIB3 Battery1				EIB3 Battery1		EIB3电池1		EIB3电池1		
117		32			15			EIB3 Battery2				EIB3 Battery2		EIB3电池2		EIB3电池2		
118		32			15			EIB3 Battery3				EIB3 Battery3		EIB3电池3		EIB3电池3		
119		32			15			EIB4 Battery1				EIB4 Battery1		EIB4电池1		EIB4电池1		
120		32			15			EIB4 Battery2				EIB4 Battery2		EIB4电池2		EIB4电池2		
121		32			15			EIB4 Battery3				EIB4 Battery3		EIB4电池3		EIB4电池3		

150		32			15		Battery 1				Batt 1		电池1			电池1			
151		32			15		Battery 2				Batt 2		电池2			电池2			
152		32			15		Battery 3				Batt 3		电池3			电池3			


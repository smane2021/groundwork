﻿# 
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15		Fuse 1 Voltage		Fuse 1 Voltage		支路1电压		支路1电压	
2	32			15		Fuse 2 Voltage		Fuse 2 Voltage		支路2电压		支路2电压	
3	32			15		Fuse 3 Voltage		Fuse 3 Voltage		支路3电压		支路3电压	
4	32			15		Fuse 4 Voltage		Fuse 4 Voltage		支路4电压		支路4电压
5	32			15		Batt Fuse 1 Alarm	Batt Fuse 1 Alm		电池支路1告警		电池支路1告警	
6	32			15		Batt Fuse 2 Alarm	Batt Fuse 2 Alm		电池支路2告警		电池支路2告警	
7	32			15		Batt Fuse 3 Alarm	Batt Fuse 3 Alm		电池支路3告警		电池支路3告警	
8	32			15		Batt Fuse 4 Alarm	Batt Fuse 4 Alm		电池支路4告警		电池支路4告警	
9	32			15		SMDU Battery Fuse Unit	SMDU Bat Fuse		SMDU电池支路单元	SMDU电池支路
10		32			15			On			On			正常			正常
11		32			15			Off			Off			断开			断开
12	32			15		Fuse 1 Status		Fuse 1 Status		支路1状态		支路1状态
13	32			15		Fuse 2 Status		Fuse 2 Status		支路2状态		支路2状态	
14	32			15		Fuse 3 Status		Fuse 3 Status		支路3状态		支路3状态	
15	32			15		Fuse 4 Status		Fuse 4 Status		支路4状态		支路4状态	
16		32			15			State			State			State			State
17		32			15			Normal			Normal			正常			正常
18		32			15			Low			Low			低于下限		低于下限
19		32			15			High			High			高于上限		高于上限
20		32			15			Very Low		Very Low		低于下下限		低于下下限
21		32			15			Very High		Very High		高于上上限		高于上上限
22		32			15			On			On			开			开
23		32			15			Off			Off			关			关
24		32			15			Communication Fail	Comm Fail		通讯中断		通讯中断
25		32			15			Times of Communication Fail	Times Comm Fail		通信失败次数		通信失败次数
26		32			15			Fuse 5 Status		Fuse 5 Status		支路5状态		支路5状态	
27		32			15			Fuse 6 Status		Fuse 6 Status		支路6状态		支路6状态	
28		32			15			Batt Fuse 5 Alarm	Batt Fuse 5 Alm		电池支路5告警		电池支路5告警	
29		32			15			Batt Fuse 6 Alarm	Batt Fuse 6 Alm		电池支路6告警		电池支路6告警	




﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15		IB Group		IB Group		IB组			IB组
2	32			15		Load Current		Load Current		负载电流		负载电流
3	32			15		DC Distribution		DC Distribution		直流配电		直流配电
4		32			15			Load Shunt		Load Shunt		负载分流器使能		负载分流器使能
5		32			15			Disabled		Disabled		禁止			禁止
6		32			15			Enabled			Enabled			使能			使能
7		32			15			Existence State		Existence State		是否存在		是否存在
8		32			15			Existent		Existent		存在			存在
9		32			15			Not Existent		Not Existent		不存在			不存在
10		32			15			IB1			IB1			IB1			IB1
11		32			15			IB2			IB2			IB2			IB2
12		32			15			NA			NA			NA			NA

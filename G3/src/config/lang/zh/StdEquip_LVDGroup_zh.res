﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15		LVD Group				LVD Group		LVD组			LVD组
2	32			15		Battery LVD				Battery LVD		电池下电		电池下电
3	32			15		None					None			无			无
4	32			15		LVD 1					LVD 1			LVD1			LVD1
5	32			15		LVD 2					LVD 2			LVD2			LVD2
6	32			15		LVD 3					LVD 3			LVD3			LVD3
7		32			15			AC Fail Required			ACFail Required		交流停电才下电		交流停电才下电
8		32			15			Disabled				Disabled		否			否
9		32			15			Enabled					Enabled			是			是
10		32			15			Number of LVD				Number of LVD		LVD数量			LVD数量
11		32			15			0					0			0			0
12		32			15			1					1			1			1
13		32			15			2					2			2			2
14		32			15			3					3			3			3
15		32			15			HTD Point				HTD Point		高温下电点		高温下电点
16		32			15			HTD Reconnect Point			HTD Recon Point		高温上电点		高温上电点
17		32			15			HTD Temperature Sensor			HTD Temp Sensor		温度采集点		温度采集点
18		32			15			Ambient Temperature			Ambient Temp		环境温度		环境温度
19		32			15			Battery Temperature			Battery Temp		电池温度		电池温度
20		32			15			Temperature 1				Temperature 1		温度1			温度1
21		32			15			Temperature 2				Temperature 2		温度2			温度2
22		32			15			Temperature 3				Temperature 3		温度3			温度3
23		32			15			Temperature 4				Temperature 4		温度4			温度4
24		32			15			Temperature 5				Temperature 5		温度5			温度5
25		32			15			Temperature 6				Temperature 6		温度6			温度6
26		32			15			Temperature 7				Temperature 7		温度7			温度7
27		32			15			Existence State				Existence State		是否存在		是否存在
28		32			15			Existent				Existent		存在			存在
29		32			15			Not Existent				Not Existent		不存在			不存在
31		32			15			LVD 1					LVD 1			LVD1允许		LVD1允许
32		32			15			LVD 1 Mode				LVD 1 Mode		LVD1方式		LVD1方式
33		32			15			LVD 1 Voltage				LVD 1 Voltage		LVD1电压		LVD1电压
34		32			15			LVD 1 Reconnect Voltage			LVD1 Recon Volt		LVD1上电电压		LVD1上电电压
35		32			15			LVD 1 Reconnect Delay			LVD1 ReconDelay		LVD1上电延迟		LVD1上电延迟
36		32			15			LVD 1 Time				LVD 1 Time		LVD1时间		LVD1时间
37		32			15			LVD 1 Dependency			LVD1 Dependency		LVD1依赖关系		LVD1依赖关系
41		32			15			LVD 2					LVD 2			LVD2允许		LVD2允许
42		32			15			LVD 2 Mode				LVD 2 Mode		LVD2方式		LVD2方式
43		32			15			LVD 2 Voltage				LVD 2 Voltage		LVD2电压		LVD2电压
44		32			15			LVD 2 Reconnect Voltage			LVD2 Recon Volt		LVD2上电电压		LVD2上电电压
45		32			15			LVD 2 Reconnect Delay			LVD2 ReconDelay		LVD2上电延迟		LVD2上电延迟
46		32			15			LVD 2 Time				LVD 2 Time		LVD2时间		LVD2时间
47		32			15			LVD 2 Dependency			LVD2 Dependency		LVD2依赖关系		LVD2依赖关系
51		32			15			Disabled				Disabled		禁止			禁止
52		32			15			Enabled					Enabled			允许			允许
53		32			15			Voltage					Voltage			电压方式		电压方式
54		32			15			Time					Time			时间方式		时间方式
55		32			15			None					None			无			无
56		32			15			LVD 1					LVD 1			LVD1			LVD1
57		32			15			LVD 2					LVD 2			LVD2			LVD2
103		32			15			High Temp Disconnect 1			HTD 1			HTD1高温下电允许	HTD1下电允许
104		32			15			High Temp Disconnect 2			HTD 2			HTD2高温下电允许	HTD2下电允许
105		32			15			Battery LVD				Battery LVD		电池下电		电池下电
106		32			15			No Battery				No Battery		无电池			无电池
107		32			15			LVD 1					LVD 1			LVD1			LVD1
108		32			15			LVD 2					LVD 2			LVD2			LVD2
109		32			15			Battery Always On			Batt Always On		有电池但不下电		有电池但不下电
110		32			15			LVD Contactor Type			LVD Type		下电接触器类型		接触器类型
111		32			15			Bistable				Bistable		双稳			双稳
112		32			15			Mono-Stable				Mono-Stable		单稳			单稳
113		32			15			Mono w/Sample				Mono w/Sample		单稳带回采		单稳带回采
116		32			15			LVD 1 Disconnect			LVD1 Disconnect		LVD1下电		LVD1下电
117		32			15			LVD 2 Disconnect			LVD2 Disconnect		LVD2下电		LVD2下电
118		32			15			LVD 1 Mono w/Sample			LVD1 Mono Sampl		LVD1单稳带回采		LVD1单稳带回采
119		32			15			LVD 2 Mono w/Sample			LVD2 Mono Sampl		LVD2单稳带回采		LVD2单稳带回采
125		32			15			State					State			状态			状态
126		32			15			LVD 1 Voltage (24V)			LVD 1 Voltage		LVD1电压(24V)		LVD1电压
127		32			15			LVD 1 Reconnect Voltage (24V)		LVD1 Recon Volt		LVD1上电电压(24V)	LVD1上电电压
128		32			15			LVD 2 Voltage (24V)			LVD 2 Voltage		LVD2电压(24V)		LVD2电压
129		32			15			LVD 2 Reconnect Voltage (24V)		LVD2 Recon Volt		LVD2上电电压(24V)	LVD2上电电压
130		32			15			LVD 1 Control				LVD 1			LVD1控制		LVD1控制
131		32			15			LVD 2 Control				LVD 2			LVD2控制		LVD2控制
132		32			15			LVD 1 Reconnect				LVD 1 Reconnect		LVD1上电		LVD1上电
133		32			15			LVD 2 Reconnect				LVD 2 Reconnect		LVD2上电		LVD2上电
134		32			15			HTD Point				HTD Point		高温下电点		高温下电点
135		32			15			HTD Reconnect Point			HTD Recon Point		高温上电点		高温上电点
136		32			15			HTD Temperature Sensor			HTD Temp Sensor		温度采集点		温度采集点
137		32			15			Ambient Temperature			Ambient Temp		环境温度		环境温度
138		32			15			Battery Temperature			Battery Temp		电池温度		电池温度
139		32			15			LVD Synchronize				LVD Synchronize		同步LVD板		同步LVD板
140		32			15			Relay for LVD3			Relay for LVD3			LVD3继电器		LVD3继电器
141		32			15			Relay Output 1			Relay Output 1			继电器1			继电器1	
142		32			15			Relay Output 2			Relay Output 2			继电器2			继电器2	
143		32			15			Relay Output 3			Relay Output 3			继电器3			继电器3	
144		32			15			Relay Output 4			Relay Output 4			继电器4			继电器4	
145		32			15			Relay Output 5			Relay Output 5			继电器5			继电器5	
146		32			15			Relay Output 6			Relay Output 6			继电器6			继电器6	
147		32			15			Relay Output 7			Relay Output 7			继电器7			继电器7	
148		32			15			Relay Output 8			Relay Output 8			继电器8			继电器8	
149		32			15			Relay Output 14			Relay Output 14			继电器14		继电器14	
150		32			15			Relay Output 15			Relay Output 15			继电器15		继电器15
151		32			15			Relay Output 16			Relay Output 16			继电器16		继电器16
152		32			15			Relay Output 17			Relay Output 17			继电器17		继电器17
153		32			15			LVD 3 Enable			LVD 3 Enable			LVD3使能		LVD3使能

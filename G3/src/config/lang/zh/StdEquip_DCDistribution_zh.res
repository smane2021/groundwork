﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			DC Distribution			DC Distr		直流配电		直流配电
2		32			15			DC Voltage			DC Voltage		直流电压		直流电压
3		32			15			Load Current			LoadCurr		负载电流		负载电流
4		32			15			Load Shunt			Load Shunt		负载分流器使能		负载分流器使能
5		32			15			Disabled			Disabled		禁止			禁止
6		32			15			Enabled				Enabled			使能			使能
7		32			15			Shunt Current			Shunt Current		分流器额定电流		分流器额定电流
8		32			15			Shunt Voltage			Shunt Voltage		分流器额定电压		分流器额定电压
9		32			15			Load Shunt Exist		LoadShuntExist		测量负载电流		测量负载电流
10		32			15			Yes				Yes			是			是
11		32			15			No				No			否			否
12		32			15			Over Voltage 1			Over Voltage 1		直流电压高		直流电压高
13		32			15			Over Voltage 2			Over Voltage 2		直流过压		直流过压
14		32			15			Under Voltage 1			Under Voltage 1		直流电压低		直流电压低
15		32			15			Under Voltage 2			Under Voltage 2		直流欠压		直流欠压
16		32			15			Over Voltage 1 (24V)		24V Over Volt1		直流电压高(24V)		直流电压高(24V)
17		32			15			Over Voltage 2 (24V)		24V Over Volt2		直流过压(24V)		直流过压(24V)
18		32			15			Under Voltage 1 (24V)		24V Under Volt1	直流电压低(24V)		直流电压低(24V)
19		32			15			Under Voltage 2 (24V)		24V Under Volt2		直流欠压(24V)		直流欠压(24V)
20		32			15			Total Load Current		TotalLoadCurr		总负载电流		总负载电流
500		32			15			Current Break Size		Curr1 Brk Size		电流1电流告警阈值		电流1告警阈值																														
501		32			15			Current High 1 Current Limit	Curr1 Hi1 Limit		电流1过流点		电流1过电流点																															
502		32			15			Current High 2 Current Limit	Curr1 Hi2 Lmt		电流1过过电流点		电流1过过流点																															
503		32			15			Current High 1 Curr		Curr Hi1Cur		电流1过流		电流1过流																																															
504		32			15			Current High 2 Curr		Curr Hi2Cur		电流1过过流		电流1过过流			
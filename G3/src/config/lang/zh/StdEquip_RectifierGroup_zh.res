﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE			
1	32			15			Rectifier Group				Rect Group		整流模块组			整流模块组
2	32			15			Total Current				Tot Rect Curr		模块总电流			整流总电流	
3	32			15			Average Voltage				Rect Voltage		输出电压平均值			整流电压	
4	32			15			Rectifier Capacity Used			Sys Cap Used		模块组使用容量			系统使用容量	
5	32			15			Maximum Capacity Used			Max Cap Used		最大使用容量			最大使用容量	
6	32			15			Minimum Capacity Used			Min Cap Used		最小使用容量			最小使用容量	
7	32			15			Total Rectifiers Communicating		Num Rects Comm		通讯正常模块数			通讯正常模块数	
8	32			15			Valid Rectifiers			Valid Rects	有效模块数			有效模块数	
9	32			15			Number of Rectifiers			Num of Rects		整流模块数			整流模块数	
10	32			15			Rectifier AC Fail State			Rect AC Fail		交流失电状态			交流失电状态	
11	32			15			Rectifier(s) Fail Status		Rect(s) Fail		多模块故障			多模块故障	
12	32			15			Rectifier Current Limit			Rect Curr Limit		模块限流			模块限流	
13	32			15			Rectifier Trim				Rectifier Trim		模块调压			模块调压	
14	32			15			DC On/Off Control			DC On/Off Ctrl		模块直流开关机			模块直流开关机	
15	32			15			AC On/Off Control			AC On/Off Ctrl		模块交流开关机			模块交流开关机	
16	32			15			Rectifiers LED Control			Rect LED Ctrl		模块LED灯控制			模块LED灯控制	
17	32			15			Fan Speed Control			Fan Speed Ctrl		风扇运行速度			风扇运行速度	
18	32			15			Rated Voltage				Rated Voltage		额定输出电压			额定输出电压	
19	32			15			Rated Current				Rated Current		额定输出电流			额定输出电流	
20	32			15			HVSD Limit				HVSD Limit		直流输出过压点			直流输出过压点	
21	32			15			Low Voltage Limit			Low Volt Limit		直流输出欠压点			直流输出欠压点	
22	32			15			High Temperature Limit			High Temp Limit		模块过温点			模块过温点	
24	32			15			HVSD Restart Time			HVSD Restart T		过压重启时间			过压重启时间	
25	32			15			Walk-In Time				Walk-In Time		带载软启动时间			带载软启动时间	
26	32			15			Walk-In					Walk-In		带载软启动允许			带载软启动允许	
27	32			15			Minimum Redundancy			Min Redundancy		最小冗余			最小冗余	
28	32			15			Maximum Redundancy			Max Redundancy		最大冗余			最大冗余	
29	32			15			Turn Off Delay				Turn Off Delay		关机延时			关机延时	
30	32			15			Cycle Period				Cycle Period		循环开关机周期			循环开关机周期	
31	32			15			Cycle Activation Time			Cyc Active Time		循环开关机执行时刻		开关机执行时刻	
32	32			15			Rectifier AC Fail			Rect AC Fail		模块交流停电			模块交流停电	
33	32			15			Multiple Rectifiers Fail		Multi-Rect Fail		多模块故障			多模块故障	
36	32			15			Normal					Normal			正常				正常	
37	32			15			Fail					Fail			故障				故障	
38	32			15			Switch Off All				Switch Off All		关所有模块			关所有模块	
39	32			15			Switch On All				Switch On All		开所有模块			开所有模块	
42	32			15			All Flashing				All Flashing		全部灯闪			全部灯闪	
43	32			15			Stop Flashing				Stop Flashing		全部灯不闪			全部灯不闪	
44	32			15			Full Speed				Full Speed		全速				全速	
45	32			15			Automatic Speed				Auto Speed		自动调速			自动调速	
46	32			32			Current Limit Control			Curr Limit Ctrl		限流点控制			限流点控制	
47	32			32			Full Capability Control			Full Cap Ctrl		满容量运行			满容量运行	
54	32			15			Disabled				Disabled		否				否	
55	32			15			Enabled					Enabled			是				是	
68	32			15			ECO Mode				ECO Mode		ECO模式使能			ECO模式使能	
72	32			15			Turn On when AC Over Voltage		Turn On ACOverV		交流过压开机			交流过压开机	
73	32			15			No					No			否				否	
74	32			15			Yes					Yes			是				是	
77	32			15			Pre-CurrLimit Turn-On			Pre-Curr Limit		开模块预限流			开模块预限流	
78	32			15			Rectifier Power Type			Rect Power Type		模块供电类型			模块供电类型	
79	32			15			Double Supply				Double Supply		双供电				双供电	
80	32			15			Single Supply				Single Supply		单供电				单供电	
81	32			15			Last Rectifiers Quantity		Last Rects Qty		原模块数			原模块数	
82	32			15			Rectifier Lost				Rectifier Lost		模块丢失			模块丢失	
83	32			15			Rectifier Lost				Rectifier Lost		模块丢失			模块丢失	
84	32			15			Clear Rectifier Lost Alarm		ClrRectLost		清除模块丢失告警		清模块丢失告警	
85	32			15			Clear					Yes			清除				是
86	32			15			Confirm Rectifier ID/Phase		Confirm ID/PH		确认模块位置和相位		确认位置和相位	
87	32			15			Confirm					Confirm			确认位置			确认位置                    
88	32			15			Best Operating Point			Best Oper Point		最佳工作点			最佳工作点
89	32			15			Rectifier Redundancy			Rect Redundancy		节能运行			节能运行
90	32			15			Load Fluctuation Range			Fluct Range		负载波动率			负载波动率
91	32			15			System Energy Saving Point		Energy Save Pt		系统节能点			系统节能点
92	32			15			E-Stop Function				E-Stop Function		E-Stop功能			E-Stop功能
93	32			15			AC Phases				AC Phases		交流相数			交流相数
94	32			15			Single Phase				Single Phase		单相				单相
95	32			15			Three Phases				Three Phases		三相				三相
96	32			15			Input Current Limit			Input Curr Lmt		输入电流限值			输入电流限值
97	32			15			Double Supply				Double Supply		双供电				双供电
98	32			15			Single Supply				Single Supply		单供电				单供电
99	32			15			Small Supply				Small Supply		Small模块供电方式		Small供电方式
100	32			15			Restart on HVSD				Restart on HVSD		直流过压复位			直流过压复位
101	32			15			Sequence Start Interval			Start Interval		顺序开机间隔			顺序开机间隔
102	32			15			Rated Voltage				Rated Voltage		额定输出电压			额定输出电压		
103	32			15			Rated Current				Rated Current		额定输出电流			额定输出电流		
104	32			15			All Rectifiers Comm Fail		AllRectCommFail		所有模块通信中断		所有模块通信断
105	32			15			Inactive				Inactive		否				否		
106	32			15			Active					Active			是				是		
107	32			15			ECO Active		ECO Active		节能运行			节能运行
108	32			15			ECO Cycle Alarm				ECO Cycle Alarm		节能功能振荡			节能功能振荡
109	32			15			Reset Cycle Alarm			ClrCycleAlm		清除节能振荡告警		清节能振荡告警
110	32			15			HVSD Limit (24V)			HVSD Limit		直流输出过压点(24V)		直流输出过压点		
111	32			15			Rectifier Trim (24V)			Rectifier Trim		模块调压(24V)			模块调压		
112	32			15			Rated Voltage (Internal Use)		Rated Voltage		额定输出电压(Internal Use)	额定输出电压		
113	32			15			Rect Info Change (Internal)	RectInfo Change		模块信息发生改变		模块信息已改变
114	32			15			MixHE Power				MixHE Power		高功率模块是否降额		高功率模块降额
115	32			15			Derated					Derated			降额				降额		
116	32			15			Non-Derated				Non-Derated		不降				不降		
117	32			15			All Rects ON Time			Rects ON Time		模块烘干时间			模块烘干时间
118	32			15			All Rectifiers are On			All Rects On		模块正在烘干			模块正在烘干
119	32			15			Clear Rect Comm Fail Alarm		ClrRectCommFail		清模块通讯中断告警		清模块通讯中断
120	32			15			HVSD					HVSD			HVSD使能			HVSD使能
121	32			15			HVSD Voltage Difference			HVSD Volt Diff		HVSD压差			HVSD压差
122	32			15			Total Rated Current		Total Rated Cur		工作模块总额定电流		工作模块总额定
123	32			15			Diesel Generator Power Limit		DG Pwr Lmt		油机限功率使能			油机限功率使能
124	32			15			Diesel Generator Digital Input		Diesel DI Input		油机干接点输入			油机干接点输入
125	32			15			Diesel Gen Power Limit Point Set	DG Pwr Lmt Pt		油机限功率点			油机限功率点
126	32			15			None					None			无				无
127	32			15			Digital Input 1				Digital Input 1		开关量 1			开关量 1
128	32			15			Digital Input 2				Digital Input 2		开关量 2			开关量 2
129	32			15			Digital Input 3				Digital Input 3		开关量 3			开关量 3
130	32			15			Digital Input 4				Digital Input 4		开关量 4			开关量 4
131	32			15			Digital Input 5				Digital Input 5		开关量 5			开关量 5
132	32			15			Digital Input 6				Digital Input 6		开关量 6			开关量 6
133	32			15			Digital Input 7				Digital Input 7		开关量 7			开关量 7
134	32			15			Digital Input 8				Digital Input 8		开关量 8			开关量 8
135	32			15			Current Limit Point			Curr Limit Pt		最大输出限流点			最大输出限流点
136	32			15			Current Limit				Current Limit		最大输出电流使能		输出电流使能
137	32			15			Maximum Current Limit Value		Max Curr Limit		最大输出电流			最大输出电流
138	32			15			Default Current Limit Point		Def Curr Lmt Pt		默认输出电流			默认输出电流
139	32			15			Minimize Current Limit Value		Min Curr Limit		最小输出电流			最小输出电流
140	32			15			AC Power Limit Mode			AC Power Lmt		交流限功率模式			交流限功率模式
141	32			15			A					A			A				A
142	32			15			B					B			B				B
143	32			15			Existence State				Existence State		是否存在		是否存在
144	32			15			Existent				Existent		存在			存在
145	32			15			Not Existent				Not Existent		不存在			不存在
146	32			15			Total Output Power			Output Power		总输出功率			总输出功率
147	32			15			Total Slave Rated Current		Total RatedCurr		总额定电流			总额定电流
148	32			15			Reset Rectifier IDs		Reset Rect IDs		清除模块位置号		清除模块位置号
149	32			15			HVSD Voltage Difference (24V)			HVSD Volt Diff		HVSD压差(24V)			HVSD压差
#changed by Frank Wu,28/30,20140217, for upgrading software of rectifier---start---
150	32			15			Normal Update				Normal Update		普通升级				普通升级
151	32			15			Force Update				Force Update		强制升级				强制升级
152	32			15			Update OK Number			Update OK Num		升级成功数				升级成功数
153	32			15			Update State				Update State		升级状态				升级状态
154	32			15			Updating					Updating			升级中					升级中
155	32			15			Normal Successful			Normal Success		普通升级成功			普通升级成功
156	32			15			Normal Failed				Normal Fail			普通升级失败			普通升级失败
157	32			15			Force Successful			Force Success		强制升级成功			强制升级成功
158	32			15			Force Failed				Force Fail			强制升级失败			强制升级失败
159	32			15			Communication Time-Out		Comm Time-Out		通讯超时				通讯超时
160	32			15			Open File Failed			Open File Fail		打开文件失败			打开文件失败
#changed by Frank Wu,28/30,20140217, for upgrading software of rectifier---end---
#161	32			15			Total current Level1			Current Level1		总电流告警门限1			告警门限1
#162	32			15			Total current Level2			Current Level2		总电流告警门限2			告警门限2
	
163	32			15			Old Firmware Rec On Can1		Old FW On Can1		在Can1总线上存在老模块			Can1老模块
164	32			15			Old Firmware Rec On Can2		Old FW On Can2		在Can2总线上存在老模块			Can2老模块
165	32			15			Comm Rectifier On CAN1			Com Rec On CAN1		在Can1总线上实际通信的模块		CAN1上通信的模块
166	32			15			Comm Rectifier On CAN2			Com Rec On CAN2		在Can2总线上实际通信的模块		CAN2上通信的模块
167	32			15			Rectifiers Limited(CAN1)		Rect Lmt(CAN1)		模块数量限制(CAN1)			限制(CAN1)
168	32			15			Rectifiers Limited(CAN2)		Rect Lmt(CAN2)		模块数量限制(CAN2)			限制(CAN2)
169	32			15			Load-share Problem(CAN1)		Load-share Prob		均流问题(CAN1)				均流问题(CAN1)
170	32			15			Load-share Problem(CAN2)		Load-share Prob		均流问题(CAN2)				均流问题(CAN2)
171	64			64		The limited number of Rectifier in the system is 60 on CAN1 Bus.	The limited number of Rectifier in the system is 60 on CAN1 Bus.	在本系统中的CAN1总线上模块数量被限制为最多60个		在本系统中的CAN1总线上模块数量被限制为最多60个
172	64			64		The limited number of Rectifier in the system is 60 on CAN2 Bus.	The limited number of Rectifier in the system is 60 on CAN2 Bus.	在本系统中的CAN2总线上模块数量被限制为最多60个		在本系统中的CAN2总线上模块数量被限制为最多60个
173	64			64		Load-share problem due to rectifiers with old firmware on CAN1.		Load-share problem due to rectifiers with old firmware on CAN1.		在CAN1总线上存在老软件的模块导致均流问题	在CAN1总线上存在老软件的模块导致均流问题	
174	64			64		Load-share problem due to rectifiers with old firmware on CAN2.		Load-share problem due to rectifiers with old firmware on CAN2.		在CAN2总线上存在老软件的模块导致均流问题	在CAN2总线上存在老软件的模块导致均流问题	

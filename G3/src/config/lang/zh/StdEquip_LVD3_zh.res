﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			LVD 3 Unit			LVD 3 Unit		LVD 3			LVD 3
11		32			15			Connected			Connected		连接			连接
12		32			15			Disconnected			Disconnected		断开			断开
13		32			15			No				No			否			否
14		32			15			Yes				Yes			是			是
21		32			15			LVD 3 Status			LVD 3 Status		LVD3状态		LVD1状态
22		32			15			LVD 2 Status			LVD 2 Status		LVD2状态		LVD2状态
23		32			15			LVD 3 Fail			LVD 3 Fail		LVD3控制失败		LVD1控制失败
24		32			15			LVD 2 Fail			LVD 2 Fail		LVD2控制失败		LVD2控制失败
25		32			15			Communication Fail		Comm Fail		通信中断		通信中断
26		32			15			State				State			工作状态		工作状态
27		32			15			LVD 3 Control			LVD 3 Control		LVD3控制		LVD3控制
28		32			15			LVD 2 Control			LVD 2 Control		LVD2控制		LVD2控制
31		32			15			LVD 3				LVD 3			LVD3允许		LVD3允许
32		32			15			LVD 3 Mode			LVD 3 Mode		LVD3方式		LVD3方式
33		32			15			LVD 3 Voltage			LVD 3 Voltage		LVD3电压		LVD3电压
34		32			15			LVD 3 Reconnect Voltage		LVD3 Recon Volt		LVD3上电电压		LVD3上电电压
35		32			15			LVD 3 Reconnect Delay		LVD3 ReconDelay		LVD3上电延迟		LVD3上电延迟
36		32			15			LVD 3 Time			LVD 3 Time		LVD3时间		LVD3时间
37		32			15			LVD 3 Dependency		LVD3 Dependency		LVD3依赖关系		LVD3依赖关系
41		32			15			LVD 2				LVD 2			LVD2允许		LVD2允许
42		32			15			LVD 2 Mode			LVD 2 Mode		LVD2方式		LVD2方式
43		32			15			LVD 2 Voltage			LVD 2 Voltage		LVD2电压		LVD2电压
44		32			15			LVD 2 Reconnect Voltage		LVD2 Recon Volt		LVD2上电电压		LVD2上电电压
45		32			15			LVD 2 Reconnect Delay		LVD2 ReconDelay		LVD2上电延迟		LVD2上电延迟
46		32			15			LVD 2 Time			LVD 2 Time		LVD2时间		LVD2时间
47		32			15			LVD 2 Dependency		LVD2 Dependency		LVD2依赖关系		LVD2依赖关系
51		32			15			Disabled			Disabled		禁止			禁止
52		32			15			Enabled				Enabled			允许			允许
53		32			15			Voltage				Voltage			电压方式		电压方式
54		32			15			Time				Time			时间方式		时间方式
55		32			15			None				None			无			无
56		32			15			LVD 1				LVD 1			LVD 1			LVD 1
57		32			15			LVD 2				LVD 2			LVD 2			LVD 2
103		32			15			High Temp Disconnect 3		HTD 3			HTD3高温下电允许	HTD3下电允许
104		32			15			High Temp Disconnect 2		HTD 2			HTD2高温下电允许	HTD2下电允许
105		32			15			Battery LVD			Battery LVD		电池下电		电池下电
106		32			15			No Battery			No Battery		无电池			无电池
107		32			15			LVD 3				LVD 3			LVD 3			LVD 3
108		32			15			LVD 2				LVD 2			LVD 2			LVD 2
109		32			15			Battery Always On		Batt Always On		有电池但不下电		有电池但不下电
110		32			15			LVD Contactor Type		LVD Type		下电接触器类型		接触器类型
111		32			15			Bistable			Bistable		双稳			双稳
112		32			15			Mono-Stable			Mono-Stable		单稳			单稳
113		32			15			Mono w/Sample			Mono w/Sample		单稳带回采		单稳带回采
116		32			15			LVD 3 Disconnect		LVD3 Disconnect		LVD3下电		LVD3下电
117		32			15			LVD 2 Disconnect		LVD2 Disconnect		LVD2下电		LVD2下电
118		32			15			LVD 3 Mono w/Sample		LVD3 Mono Sampl		LVD3单稳带回采		LVD3单稳带回采
119		32			15			LVD 2 Mono w/Sample		LVD2 Mono Sampl		LVD2单稳带回采		LVD2单稳带回采
125		32			15			State				State			State			State
126		32			15			LVD 3 Voltage (24V)		LVD 3 Voltage		LVD3电压(24V)		LVD3电压
127		32			15			LVD 3 Reconnect Voltage (24V)	LVD3 Recon Volt		LVD3上电电压(24V)	LVD3上电电压
128		32			15			LVD 2 Voltage (24V)		LVD 2 Voltage		LVD2电压(24V)		LVD2电压
129		32			15			LVD 2 Reconnect Voltage (24V)	LVD2 Recon Volt		LVD2上电电压(24V)	LVD2上电电压
130		32			15			LVD 3				LVD 3			LVD 3			LVD 3


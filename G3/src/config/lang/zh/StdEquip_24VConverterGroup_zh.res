﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			24V Converter Group		24V Conv Group		24V Converter Group	24V Conv Group
19		32			15			Shunt 3 Rated Current		Shunt 3 Current		分流器3额定电流		分流器3额定电流
21		32			15			Shunt 3 Rated Voltage		Shunt 3 Voltage		分流器3额定电压		分流器3额定电压
26		32			15			Closed				Closed			闭合			闭合
27		32			15			Open				Open			断开			断开                      
29		32			15			No				No			否			否
30		32			15			Yes				Yes			是			是
3002		32			15			Converter Installed		Conv Installed		是否安装		是否安装
3003		32			15			No				No			否			否
3004		32			15			Yes				Yes			是			是
3005		32			15			Under Voltage			Under Volt		欠压			欠压
3006		32			15			Over Voltage			Over Volt		过压			过压
3007		32			15			Over Current			Over Current		过流			过流
3008		32			15			Under Voltage			Under Volt		欠压			欠压
3009		32			15			Over Voltage			Over Volt		过压			过压
3010		32			15			Over Current			Over Current		过流			过流
3011		32			15			Voltage				Voltage			电压			电压
3012		32			15			Total Current			Total Current		总电流			总电流
3013		32			15			Input Current			Input Current		输入电流		输入电流
3014		32			15			Efficiency			Efficiency		效率			效率


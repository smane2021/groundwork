﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
zh



[RES_INFO]
#R_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE		
1		32		15			Solar Converter			Solar Conv			太阳能模块			太阳能模块		
2		32		15			DC Status			DC Status		直流开关状态			直流开关状态	
3		32		15			DC Output Voltage		DC Voltage		输出电压			输出电压	
4		32		15			Origin Current			Origin Current		输出电流原始值			输出电流原始值
5		32		15			Temperature			Temperature		温度				温度	
6		32		15			Used Capacity			Used Capacity		使用容量			使用容量	
7		32		15			Input Voltage			Input Voltage		输入电压			输入电压	
8		32		15			DC Output Current		DC Current		直流输出电流			直流输出电流	
9		32		15			SN				SN			序列号				序列号	
10		32		15			Total Running Time		Running Time		总运行时间			总运行时间	
11		32		15			Communication Fail Count	Comm Fail Count		通讯中断次数			通讯中断次数	
13		32		15			Derated by Temp			Derated by Temp		温度限功率			温度限功率	
14		32		15			Derated				Derated			模块限功率			模块限功率	
15		32		15			Full Fan Speed			Full Fan Speed		风扇全速			风扇全速	
16		32		15			Walk-In			Walk-In		Walk-in功能			Walk-in功能	
18		32		15			Current Limit			Current Limit		模块限流点			模块限流点	
19		32		15			Voltage High Limit		Volt Hi-limit		直流电压告警上限		电压告警上限	
20		32		15			Solar Status			Solar Status		太阳能状态			太阳能状态	
21		32		15			Solar Converter Temperature High	SolarConvTempHi		过温				过温		
22		32		15			Solar Converter Fail			SolarConv Fail		故障				故障		
23		32		15			Solar Converter Protected	SolarConv Prot		保护				保护	
24		32		15			Fan Fail			Fan Fail		风扇故障			风扇故障	
25		32		15			Current Limit State		Curr Lmt State		限流状态			限流状态	
26		32		15			EEPROM Fail			EEPROM Fail		EEPROM故障			EEPROM故障	
27		32		15			DC On/Off Control		DC On/Off Ctrl		直流开关机			直流开关机
29		32		15			LED Control			LED Control		Led灯控制			Led灯控制	
30		32		15			Solar Converter Reset		SolarConvReset		复位				复位	
31		32		15			Input Fail			Input Fail		输入故障			输入故障	
34		32		15			Over Voltage			Over Voltage		直流输出过压			直流输出过压	
37		32		15			Current Limit			Current Limit		限流				限流	
39		32		15			Normal				Normal			否				否
40		32		15			Limited				Limited			是				是
45		32		15			Normal				Normal			正常				正常	
46		32		15			Full				Full			全速				全速	
47		32		15			Disabled			Disabled		无效				无效	
48		32		15			Enabled				Enabled			有效				有效	
49		32		15			On				On			开				开
50		32		15			Off				Off			关				关
51		32		15			Day				Day			白天				白天	
52		32		15			Fail				Fail			故障				故障	
53		32		15			Normal				Normal			正常				正常	
54		32		15			Over Temperature		Over Temp		过温				过温度	
55		32		15			Normal				Normal			正常				正常
56		32		15			Fail				Fail			故障				故障
57		32		15			Normal				Normal			正常				正常
58		32		15			Protected			Protected		保护				保护
59		32		15			Normal				Normal			正常				正常
60		32		15			Fail				Fail			故障				故障
61		32		15			Normal				Normal			正常				正常
62		32		15			Alarm				Alarm			告警				告警
63		32		15			Normal				Normal			正常				正常
64		32		15			Fail				Fail			故障				故障
65		32		15			Off				Off			关				关
66		32		15			On				On			开				开
67		32		15			Off				Off			关				关
68		32		15			On				On			开				开
69		32		15			LED Control			LED Control		灯闪				灯闪	
70		32		15			Cancel				Cancel			不闪				不闪	
71		32		15			Off				Off			关				关	
72		32		15			Reset				Reset			复位				复位
73		32		15			Solar Converter On		Solar Conv On		开				开
74		32		15			Solar Converter Off		Solar Conv Off		关				关
75		32		15			Off				Off			关				关
76		32		15			LED Control			LED Control			闪灯				闪灯
77		32		15			Solar Converter Reset			SolarConv Reset		模块复位			模块复位
80		34		15			Solar Converter Comm Fail	SolConvCommFail		模块通讯中断			模块通讯中断
84		32		15			Solar Converter High SN			SolConvHighSN		模块高序列号			模块高序列号
85		32		15			Solar Converter Version			SolarConv Ver		模块版本			模块版本
86		32		15			Solar Converter Part Number		SolConv PartNum		模块产品号			模块产品号
87		32		15			Current Share State		Current Share		模块均流状态			模块均流状态
88		32		15			Current Share Alarm		Curr Share Alm		模块不均流			模块不均流
89		32		15			Over Voltage			Over Voltage		模块过压			模块过压
90		32		15			Normal				Normal			正常				正常
91		32		15			Over Voltage			Over Voltage		过压				过压
95		32		15			Low Voltage			Low Voltage		欠压				欠压
96		32		15			Input Under Voltage Protection	Low Input	模块输入欠压保护		输入欠压保护
97		32		15			Solar Converter ID		Solar Conv ID		模块位置号			模块位置号
98		32		15			DC Output Shut Off		DC Output Off		模块直流输出关			直流输出关
99		32		15			Solar Converter Phase		SolarConvPhase		模块相位			模块相位
103		32		15			Severe Current Share Alarm	SevereCurrShare		严重模块不均流			严重模块不均流
104		32		15			Barcode 1			Barcode 1		Bar Code1			Bar Code1
105		32		15			Barcode 2			Barcode 2		Bar Code2			Bar Code2
106		32		15			Barcode 3			Barcode 3		Bar Code3			Bar Code3
107		32		15			Barcode 4			Barcode 4		Bar Code4			Bar Code4
108		34		15			Solar Converter Comm Fail	SolConvComFail		通信中断			通信中断
109		32		15			No				No			否				否
110		32		15			Yes				Yes			是				是
111		32		15			Existence State			Existence State		设备是否存在			设备是否存在
113		32		15			Comm OK				Comm OK			模块通讯正常			模块通讯正常
114		32		15			All Solar Converters Comm Fail	All Comm Fail		模块都通讯中断			都通讯中断
115		32		15			Communication Fail		Comm Fail		模块通讯中断			模块通讯中断
116		32		15			Valid Rated Current		Rated Current		有效额定电流			有效额定电流
117		32		15			Efficiency			Efficiency		模块效率			模块效率
118		32		15			LT 93				LT 93			小于93				小于93
119		32		15			GT 93				GT 93			大于93				大于93
120		32		15			GT 95				GT 95			大于95				大于95
121		32		15			GT 96				GT 96			大于96				大于96
122		32		15			GT 97				GT 97			大于97				大于97
123		32		15			GT 98				GT 98			大于98				大于98
124		32		15			GT 99				GT 99			大于99				大于99
125		32		15			Solar Converter HVSD Status	HVSD Status		模块HVSD状态			模块HVSD状态
126		32		15			Solar Converter Reset		SolConvResetEEM		模块复位			模块复位	
127		32		15			Night				Night			黑夜				黑夜
128		32		15			Input Current			Input Current		输入电流			输入电流	
129		32		15			Input Power			Input Power		输入功率			输入功率
130		32		15			Input Not DC			Input Not DC		非直流输入			非直流输入
131		32		15			Output Power			Output Power		输出功率			输出功率

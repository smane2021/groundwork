﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN						ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15			Battery Current						Batt Current		电池电流		电池电流
2	32			15			Battery Capacity					Batt Capacity		电池容量		电池容量
3	32			15			Battery Voltage						Batt Voltage		电池电压		电池电压
4	32			15			Ambient Temperature					Ambient Temp		环境温度		环境温度
5	32			15			Acid Temperature					Acid Temp		电池温度		电池温度
6	32			15			Total Time Batt Temp GT 30 deg.C			ToT GT 30 deg.C		电池高温总时间		电池高温总时间
7	32			15			Total Time Batt Temp LT 10 deg.C			ToT LT 10 deg.C		电池低温总时间		电池低温总时间
8	32			15			Battery Block 1 Voltage					Block 1 Volt		Block1电压		Block1电压
9	32			15			Battery Block 2 Voltage					Block 2 Volt		Block2电压		Block2电压
10	32			15			Battery Block 3 Voltage					Block 3 Volt		Block3电压		Block3电压
11	32			15			Battery Block 4 Voltage					Block 4 Volt		Block4电压		Block4电压
12	32			15			Battery Block 5 Voltage					Block 5 Volt		Block5电压		Block5电压
13	32			15			Battery Block 6 Voltage					Block 6 Volt		Block6电压		Block6电压
14	32			15			Battery Block 7 Voltage					Block 7 Volt		Block7电压		Block7电压
15	32			15			Battery Block 8 Voltage					Block 8 Volt		Block8电压		Block8电压
16	32			15			Battery Block 9 Voltage					Block 9 Volt		Block9电压		Block9电压
17	32			15			Battery Block 10 Voltage				Block 10 Volt		Block10电压		Block10电压
18	32			15			Battery Block 11 Voltage				Block 11 Volt		Block11电压		Block11电压
19	32			15			Battery Block 12 Voltage				Block 12 Volt		Block12电压		Block12电压
20	32			15			Battery Block 13 Voltage				Block 13 Volt		Block13电压		Block13电压
21	32			15			Battery Block 14 Voltage				Block 14 Volt		Block14电压		Block14电压
22	32			15			Battery Block 15 Voltage				Block 15 Volt		Block15电压		Block15电压
23	32			15			Battery Block 16 Voltage				Block 16 Volt		Block16电压		Block16电压
24	32			15			Battery Block 17 Voltage				Block 17 Volt		Block17电压		Block17电压
25	32			15			Battery Block 18 Voltage				Block 18 Volt		Block18电压		Block18电压
26	32			15			Battery Block 19 Voltage				Block 19 Volt		Block19电压		Block19电压
27	32			15			Battery Block 20 Voltage				Block 20 Volt		Block20电压		Block20电压
28	32			15			Battery Block 21 Voltage				Block 21 Volt		Block21电压		Block21电压
29	32			15			Battery Block 22 Voltage				Block 22 Volt		Block22电压		Block22电压
30	32			15			Battery Block 23 Voltage				Block 23 Volt		Block23电压		Block23电压
31	32			15			Battery Block 24 Voltage				Block 24 Volt		Block24电压		Block24电压
32	32			15			Battery Block 25 Voltage				Block 25 Volt		Block25电压		Block25电压
33	32			15			Shunt Voltage						Shunt Voltage		分流器电压		分流器电压
34	32			15			Battery Leakage						Batt Leakage		电池泄漏		电池泄漏
35	32			15			Low Acid Level						Low Acid Level		浅酸			浅酸
36	32			15			Battery Disconnected					Batt Disconnec		电池断开		电池断开
37	32			15			Battery High Temperature				Batt High Temp		电池过温		电池过温
38	32			15			Battery Low Temperature					Batt Low Temp		电池低温		电池低温
39	32			15			Block Voltage Difference				Block Volt Diff		块电压差异		块电压差异
40	32			15			Battery Shunt Size					Batt Shunt Size		电池分流器系数		电池分流器系数
41	32			15			Block Voltage Difference				Block Volt Diff		Block电压差		Block电压差
42	32			15			Battery High Temp Limit					High Temp Limit		电池温度上限		电池温度上限
43	32			15			Batt High Temp Limit Hysteresis				Batt HiTemp Hys		过温回差		过温回差
44	32			15			Battery Low Temp Limit				Low Temp Limit		电池温度下限		电池温度下限
45	32			15			Batt Low Temp Limit Hysteresis				Batt LoTemp Hys		低温回差		低温回差
46	32			15			Exceed Batt Current Limit				Over Curr Limit		电池过限流点		电池过限流点
47	32			15			Battery Leakage						Battery Leakage		电池泄漏		电池泄漏
48	32			15			Low Acid Level						Low Acid Level		浅酸			浅酸
49	32			15			Battery Disconnected					Batt Disconnec		电池断开		电池断开
50	32			15			Battery High Temperature				Batt High Temp		电池过温		电池过温
51	32			15			Battery Low Temperature					Batt Low Temp		电池低温		电池低温
52		32			15			Cell Voltage Difference					Cell Volt Diff		单元电压差异		单元电压差异
53		32			15			SM Unit Fail						SM Unit Fail		SM BAT通讯失败		SM BAT通讯失败
54		32			15			Battery Disconnected					Batt Disconnec		电池断开		电池断开
55		32			15			No							No			否			否
56		32			15			Yes							Yes			是			是
57		32			15			No							No			否			否
58		32			15			Yes							Yes			是			是
59		32			15			No							No			否			否
60		32			15			Yes							Yes			是			是
61		32			15			No							No			否			否
62		32			15			Yes							Yes			是			是
63		32			15			No							No			否			否
64		32			15			Yes							Yes			是			是
65		32			15			No							No			否			否
66		32			15			Yes							Yes			是			是
67		32			15			No							No			否			否
68		32			15			Yes							Yes			是			是
69		32			15			SM Battery						SM Battery		SM电池			SM电池
70		32			15			Over Battery Current					Over Batt Curr		电池充电过流		电池充电过流
71		32			15			Battery Capacity (%)					Batt Cap (%)		电池容量(%)		电池容量(%)
72		32			15			SMBAT Fail						SMBAT Fail		SM BAT通讯失败		SM BAT通讯失败
73		32			15			No							No			否			否
74		32			15			Yes							Yes			是			是
75		32			15			AI 4							AI 4			AI 4			AI 4
76		32			15			AI 7							AI 7			AI 7			AI 7
77		32			15			DI 4							DI 4			DI 4			DI 4
78		32			15			DI 5							DI 5			DI 5			DI 5
79		32			15			DI 6							DI 6			DI 6			DI 6
80		32			15			DI 7							DI 7			DI 7			DI 7
81		32			15			DI 8							DI 8			DI 8			DI 8
82		32			15			Relay 1 Status						Relay 1 Status		继电器1状态		继电器1状态
83		32			15			Relay 2 Status						Relay 2 Status		继电器2状态		继电器2状态
84		32			15			No							No			否			否
85		32			15			Yes							Yes			是			是
86		32			15			No							No			否			否
87		32			15			Yes							Yes			是			是
88		32			15			No							No			否			否
89		32			15			Yes							Yes			是			是
90		32			15			No							No			否			否
91		32			15			Yes							Yes			是			是
92		32			15			No							No			否			否
93		32			15			Yes							Yes			是			是
94		32			15			Off							Off			关闭			关闭
95		32			15			On							On			打开			打开
96		32			15			Off							Off			关闭			关闭
97		32			15			On							On			打开			打开
98		32			15		Relay 1 On/Off					Relay 1 On/Off		继电器1开/关		继电器1开/关
99	32			15		Relay 2 On/Off					Relay 2 On/Off		继电器2开/关		继电器2开/关
100		32			15			AI 2							AI 2			AI 2			AI 2
101		32			15			Battery Temperature Sensor Fail			T Sensor Fail		电池温度传感器故障	温度传感器故障
102	32			15			Low Capacity						Low Capacity		容量低			容量低
103		32			15			Existence State						Existence State		是否存在		是否存在
104		32			15			Existent						Existent		存在			存在
105		32			15			Not Existent						Not Existent		不存在			不存在
106		32			15			Battery Communication Fail				Batt Comm Fail		电池通信中断		电池通信中断
107		32			15			Communication OK					Comm OK			通信正常		通信正常
108		32			15			Communication Fail					Comm Fail		电池通信中断		电池通信中断
109		32			15			Rated Capacity						Rated Capacity		标称容量		标称容量	
110		32			15			Battery Management					Batt Management		参与电池管理		参与电池管理
111		32			15			SM Batt Temp High Limit					SMBat TempHiLmt		SM电池过温点		SM电池过温点
112		32			15			SM Batt Temp Low Limit					SMBat TempLoLmt		SM电池欠温点		SM电池欠温点
113		32			15			SM Batt Temp						SM Bat Temp		SM电池温度使能		SM电池温度使能
114		32			15			Battery Communication Fail				Batt Comm Fail		SM电池通信中断		SM电池通信中断
115		32			15			Battery Temp not Used					Bat Temp No Use		电池温度未用		电池温度未用





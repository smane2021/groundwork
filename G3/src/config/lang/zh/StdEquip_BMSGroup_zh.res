﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			BMS Group			BMS Group		BMS组			BMS组
2		32			15			Number of BMS		NumOfBMS		BMS数量		BMS数量
3		32			15			Communication Fail		Comm Fail		中断状态		中断状态
4		32			15			Existence State			Existence State		是否存在		是否存在
5		32			15			Existent			Existent		存在			存在
6		32			15			Not Existent			Not Existent		不存在			不存在
7		32			15			BMS Existence State	BMSState		BMS状态		BMS状态
8		32			15			Charge Current			Charge Current		最大充电电流		最大充电电流



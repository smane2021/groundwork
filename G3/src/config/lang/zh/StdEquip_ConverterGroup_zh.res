﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15			Converter Group				Converter Grp		Converter组		变流模块组
2	32			15			Total Current				Tot Conv Curr		模块总电流		变流总电流	
3	32			15			Voltage					Conv Voltage		输出电压		变流电压	
4	32			15			Converter Capacity Used			Conv Cap Used		模块组使用容量		模块组使用容量	
5	32			15			Maximum Capacity Used			Max Cap Used		最大使用容量		最大使用容量	
6	32			15			Minimum Capacity Used			Min Cap Used		最小使用容量		最小使用容量	
7	32			15			Total Converters Communicating		Num Convs Comm		通讯正常模块数		通讯正常模块数	
8	32			15			Valid Converters			Valid Convs	有效模块数		有效模块数	
9	32			15			Number of Converters			Conv Num		变流模块数		变流模块数	
10	32			15			Converter AC Fail State		Conv AC Fail		交流失电状态		交流失电状态	
11	32			15			Multi-Converters Fail Status		Multi-Conv Fail		多模块故障		多模块故障	
12	32			15			Converter Current Limit			Conv Curr Limit		模块限流		模块限流	
13	32			15			Converter Trim				Converter Trim		模块调压		模块调压	
14	32			15			DC On/Off Control			DC On/Off Ctrl		模块直流开关机		模块直流开关机	
15	32			15			AC On/Off Control			AC On/Off Ctrl		模块交流开关机		模块交流开关机	
16	32			15			Converters LED Control			Conv LED Ctrl		模块LED灯控制		模块LED灯控制	
17	32			15			Fan Speed Control			Fan Speed Ctrl		风扇运行速度		风扇运行速度	
18	32			15			Rated Voltage				Rated Voltage		额定输出电压		额定输出电压	
19	32			15			Rated Current				Rated Current		额定输出电流		额定输出电流	
20	32			15			High Voltage Shutdown Limit		HVSD Limit		直流输出过压点		直流输出过压点	
21	32			15			Low Voltage Limit			Low Volt Limit		直流输出欠压点		直流输出欠压点	
22	32			15			High Temperature Limit			High Temp Limit		模块过温点		模块过温点	
24	32			15			HVSD Restart Time			HVSD Restart T		过压重启时间		过压重启时间	
25	32			15			Walk-In Time				Walk-In Time		带载软启动时间		带载软启动时间	
26	32			15			Walk-In					Walk-In			带载软启动允许		带载软启动允许	
27	32			15			Minimum Redundancy			Min Redundancy		最小冗余		最小冗余	
28	32			15			Maximum Redundancy			Max Redundancy		最大冗余		最大冗余	
29	32			15			Turn Off Delay				Turn Off Delay		关机延时		关机延时	
30	32			15			Cycle Period				Cycle Period		循环开关机周期		循环开关机周期	
31	32			15			Cycle Activation Time			Cyc Active Time		循环开关机执行时刻	开关机执行时刻	
32	32			15			Converter AC Fail			Conv AC Fail		模块交流停电		模块交流停电	
33	32			15			Multiple Converters Fail		Multi-Conv Fail		多模块故障		多模块故障	
36	32			15			Normal					Normal			正常			正常	
37	32			15			Fail					Fail			故障			故障	
38	32			15			Switch Off All				Switch Off All		关所有模块		关所有模块	
39	32			15			Switch On All				Switch On All		开所有模块		开所有模块	
42	32			15			All Flashing				All Flashing		全部灯闪		全部灯闪	
43	32			15			Stop Flashing				Stop Flashing		全部灯不闪		全部灯不闪	
44	32			15			Full Speed				Full Speed		全速			全速	
45	32			15			Automatic Speed				Auto Speed		自动调速		自动调速	
46	32			32			Current Limit Control			Curr Limit Ctrl		限流点控制		限流点控制	
47	32			32			Full Capability Control			Full Cap Ctrl		满容量运行		满容量运行	
54	32			15			Disabled				Disabled		否			否	
55	32			15			Enabled					Enabled			是			是	
68	32			15			System ECO				System ECO		系统节能允许		系统节能允许	
72	32			15			Turn On when AC Over Voltage		Turn On AC Ov V		交流过压开机		交流过压开机	
73	32			15			No					No			否			否	
74	32			15			Yes					Yes			是			是	
77	32			15			Pre-CurrLimit on Turn-On Enabled	Pre-Curr Limit		开模块预限流		开模块预限流	
78	32			15			Converter Power Type			Conv Power Type		模块供电类型		模块供电类型	
79	32			15			Double Supply				Double Supply		双供电			双供电	
80	32			15			Single Supply				Single Supply		单供电			单供电	
81	32			15			Last Converters Quantity		Last Convs Qty		原模块数		原模块数	
82	32			15			Converter Lost				Converter Lost		模块丢失		模块丢失	
83	32			15			Converter Lost				Converter Lost		模块丢失		模块丢失	
84	32			15			Clear Converter Lost Alarm		Clear Conv Lost		清除Conv丢失告警	清Conv丢失告警	
85	32			15			Clear					Clear			清除			清除
86	32			15			Confirm Converter ID			Confirm ID		确认位置		确认位置	
87	32			15			Confirm					Confirm			确认位置		确认位置                    
88	32			15			Best Operating Point			Best Oper Pt		最佳工作点		最佳工作点
89	32			15			Converter Redundancy			Conv Redundancy		节能运行		节能运行
90	32			15			Load Fluctuation Range			Fluct Range		负载波动率		负载波动率
91	32			15			System Energy Saving Point		Energy Save Pt		系统节能点		系统节能点
92	32			15			E-Stop Function				E-Stop Function		E-Stop功能		E-Stop功能
93	32			15			AC Phases				AC Phases		交流相数		交流相数
94	32			15			Single Phase				Single Phase		单相			单相
95	32			15			Three Phases				Three Phases		三相			三相
96	32			15			Input Current Limit			Input Curr Lmt		输入电流限值		输入电流限值
97	32			15			Double Supply				Double Supply		双供电			双供电
98	32			15			Single Supply				Single Supply		单供电			单供电
99	32			15			Small Supply				Small Supply		Small模块供电方式	Small供电方式
100	32			15			Restart on HVSD				Restart on HVSD		直流过压复位		直流过压复位
101	32			15			Sequence Start Interval			Start Interval		顺序开机间隔		顺序开机间隔
102	32			15			Rated Voltage				Rated Voltage		额定输出电压		额定输出电压	
103	32			15			Rated Current				Rated Current		额定输出电流		额定输出电流	
104	32			15			All Converters Comm Fail		AllConvCommFail		所有模块通信中断	所有模块通信断
105	32			15			Inactive				Inactive		否			否	
106	32			15			Active					Active			是			是	
107	32			15			Converter Redundancy Active		Redund Active		节能运行		节能运行
108	32			15			Existence State				Existence State		是否存在		是否存在
109	32			15			Existent				Existent		存在			存在
110	32			15			Not Existent				Not Existent		不存在			不存在
111	32			15			Average Current				Average Current		平均电流		平均电流
112	32			15			Default Current Limit			Current Limit		默认电流		默认电流
113	32			15			Output Voltage				Output Voltage		默认电压		默认电压
114	32			15			Under Voltage				Under Voltage		模块欠压点		模块欠压点
115	32			15			Over Voltage				Over Voltage		模块过压点		模块过压点
116	32			15			Over Current				Over Current		模块过流点		模块过流点
117	32			15			Average Voltage				Average Voltage		平均电压		平均电压
118	32			15			HVSD Limit				HVSD Limit		HVSD电压		HVSD电压
119	32			15			HVSD Limit				HVSD Limit		HVSD电压		HVSD电压
120	32			15			Clear Converter Comm Fail		ClrConvCommFail		清Conv通讯中断		清Conv通讯中断
121	32			15			HVSD					HVSD			HVSD使能		HVSD使能
135	32			15			Current Limit Point			Curr Limit Pt		最大输出限流点		最大输出限流点
136	32			15			Current Limit				Current Limit		最大输出限流		最大输出限流
137	32			15			Maximum Current Limit Value		Max Curr Limit		最大限流点			最大限流点
138	32			15			Default Current Limit Point		Def Curr Lmt Pt		默认输出电流			默认输出电流
139	32			15			Minimize Current Limit Value		Min Curr Limit		最小输出电流			最小输出电流
290	32			15			Over Current				Over Current		过流			过流
291	32			15			Over Voltage				Over Voltage		过压			过压
292	32			15			Under Voltage				Under Voltage		欠压			欠压
293	32			15			Clear All Converters Comm Fail		ClrAllConvCommF		清所有Conv通信中断			清所有Conv中断
294	32			15			All Conv Comm Status			All Conv Status		所有Conv通信状态			所有Conv通信状态
295	32			15			Converter Trim(24V)			Conv Trim(24V)		模块调压(24V)		模块调压(24V)	
296	32			15			Last Conv Qty(Internal Use)		LastConvQty(P)		原模块数(内部使用)		原模块数(P)	
297	32			15			Def Currt Lmt Pt(Inter Use)		DefCurrLmtPt(P)		默认输出电流(内部使用)			默认输出(P)
298	32			15			Converter Protect			Conv Protect		模块保护			模块保护
299	32			15			Input Rated Voltage			Input RatedVolt		额定输入电压		额定输入电压	
300	32			15			Total Rated Current			Total RatedCurr		总额定输出电流		总额定输出电流	
301	32			15			Converter Type				Conv Type		Converter类型		Converter类型	
302	32			15			24-48V Conv				24-48V Conv		24-48V Conv		24-48V Conv	
303	32			15			48-24V Conv				48-24V Conv		48-24V Conv		48-24V Conv	
304	32			15			400-48V Conv				400-48V Conv		400-48V Conv		400-48V Conv	
305	32			15			Total Output Power			Output Power		总输出功率		总输出功率
306	32			15			Reset Converter IDs			Reset Conv IDs		清除模块位置号		清除模块位置号

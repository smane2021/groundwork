﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			SMTemp6 Temperature 1			SMTemp6 Temp1		温度1			温度1
2	32			15			SMTemp6 Temperature 2			SMTemp6 Temp2		温度2			温度2
3	32			15			SMTemp6 Temperature 3			SMTemp6 Temp3		温度3			温度3
4	32			15			SMTemp6 Temperature 4			SMTemp6 Temp4		温度4			温度4
5	32			15			SMTemp6 Temperature 5			SMTemp6 Temp5		温度5			温度5
6	32			15			SMTemp6 Temperature 6			SMTemp6 Temp6		温度6			温度6
7	32			15			SMTemp6 Temperature 7			SMTemp6 Temp7		温度7			温度7
8	32			15			SMTemp6 Temperature 8			SMTemp6 Temp8		温度8			温度8
9	32			15			Temperature Probe 1 Status		Probe1 Status		温度探头1状态		探头1状态
10	32			15			Temperature Probe 2 Status		Probe2 Status		温度探头2状态		探头2状态
11	32			15			Temperature Probe 3 Status		Probe3 Status		温度探头3状态		探头3状态
12	32			15			Temperature Probe 4 Status		Probe4 Status		温度探头4状态		探头4状态
13	32			15			Temperature Probe 5 Status		Probe5 Status		温度探头5状态		探头5状态
14	32			15			Temperature Probe 6 Status		Probe6 Status		温度探头6状态		探头6状态
15	32			15			Temperature Probe 7 Status		Probe7 Status		温度探头7状态		探头7状态
16	32			15			Temperature Probe 8 Status		Probe8 Status		温度探头8状态		探头8状态

17	32			15			Normal					Normal			正常			正常
18	32			15			Shorted					Shorted			短路			短路
19	32			15			Open					Open			断开			断开
20	32			15			Not Installed				Not Installed		未安装			未安装

21	32			15			Module ID Overlap			ID Overlap		模块ID重复		模块ID重复
22	32			15			AD Converter Failure			AD Conv Fail		模块A/D转换器故障	A/D转换器故障
23	32			15			SMTemp EEPROM Failure			EEPROM Fail		SMTemp EEPROM故障	EEPROM故障

24	32			15			Communication Fail			Comm Fail		中断状态		中断状态
25	32			15			Existence State				Exist State		存在状态		存在状态
26	32			15			Communication Fail			Comm Fail		通信中断		通信中断

27	32			15			Temperature Probe 1 Shorted		Probe1 Short		温度探头1短路		探头1短路
28	32			15			Temperature Probe 2 Shorted		Probe2 Short		温度探头2短路		探头2短路
29	32			15			Temperature Probe 3 Shorted		Probe3 Short		温度探头3短路		探头3短路
30	32			15			Temperature Probe 4 Shorted		Probe4 Short		温度探头4短路		探头4短路
31	32			15			Temperature Probe 5 Shorted		Probe5 Short		温度探头5短路		探头5短路
32	32			15			Temperature Probe 6 Shorted		Probe6 Short		温度探头6短路		探头6短路
33	32			15			Temperature Probe 7 Shorted		Probe7 Short		温度探头7短路		探头7短路
34	32			15			Temperature Probe 8 Shorted		Probe8 Short		温度探头8短路		探头8短路

35	32			15			Temperature Probe 1 Open		Probe1 Open		温度探头1断开		探头1断开
36	32			15			Temperature Probe 2 Open		Probe2 Open		温度探头2断开		探头2断开
37	32			15			Temperature Probe 3 Open		Probe3 Open		温度探头3断开		探头3断开
38	32			15			Temperature Probe 4 Open		Probe4 Open		温度探头4断开		探头4断开
39	32			15			Temperature Probe 5 Open		Probe5 Open		温度探头5断开		探头5断开
40	32			15			Temperature Probe 6 Open		Probe6 Open		温度探头6断开		探头6断开
41	32			15			Temperature Probe 7 Open		Probe7 Open		温度探头7断开		探头7断开
42	32			15			Temperature Probe 8 Open		Probe8 Open		温度探头8断开		探头8断开

43	32			15			SMTemp 6				SMTemp 6		SMTemp 6		SMTemp 6
44	32			15			Abnormal				Abnormal		异常			异常

45	32			15			Clear					Clear			清除			清除
46	32			15			Clear Probe Alarm			Clr Probe Alm		清除探头告警		清探头告警

51	32			15			Temperature 1 Assign Equipment		T1 Assign Equip		探头1分配设备		探头1分配设备
54	32			15			Temperature 2 Assign Equipment		T2 Assign Equip		探头2分配设备		探头2分配设备
57	32			15			Temperature 3 Assign Equipment		T3 Assign Equip		探头3分配设备		探头3分配设备
60	32			15			Temperature 4 Assign Equipment		T4 Assign Equip		探头4分配设备		探头4分配设备
63	32			15			Temperature 5 Assign Equipment		T5 Assign Equip		探头5分配设备		探头5分配设备
66	32			15			Temperature 6 Assign Equipment		T6 Assign Equip		探头6分配设备		探头6分配设备
69	32			15			Temperature 7 Assign Equipment		T7 Assign Equip		探头7分配设备		探头7分配设备
72	32			15			Temperature 8 Assign Equipment		T8 Assign Equip		探头8分配设备		探头8分配设备

150	32			15			None					None			未定义			未定义
151	32			15			Ambient					Ambient			环境			环境
152	32			15			Battery					Battery			电池			电池

﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			BMS				BMS		BMS			BMS
2	32			15			Rated Capacity				Rated Capacity		标称容量		标称容量
3	32			15			Communication Fail			Comm Fail		通讯中断		通讯中断
4	32			15			Existence State				Existence State		是否存在		是否存在
5	32			15			Existent				Existent		存在			存在
6	32			15			Not Existent				Not Existent		不存在			不存在
7	32			15			Battery Voltage				Batt Voltage		电池电压		电池电压
8	32			15			Battery Current				Batt Current		电池电流		电池电流
9	32			15			Battery Rating (Ah)			Batt Rating(Ah)		电池容量(Ah)		电池容量(Ah)
10	32			15			Battery Capacity (%)			Batt Cap (%)		电池容量(%)		电池容量(%)
11	32			15			Bus Voltage				Bus Voltage		母排电压		母排电压
12	32			15			Battery Center Temperature(AVE)		Batt Temp(AVE)		电芯平均温度		电芯平均温度
13	32			15			Ambient Temperature			Ambient Temp		环境温度		环境温度
29	32			15			Yes					Yes			是			是
30	32			15			No					No			否			否
31	32			15			Single Over Voltage Alarm		SingleOverVolt		单体过压告警		单体过压告警
32	32			15			Single Under Voltage Alarm		SingleUnderVolt		单体欠压告警		单体欠压告警
33	32			15			Whole Over Voltage Alarm		WholeOverVolt		整体过压告警		整体过压告警
34	32			15			Whole Under Voltage Alarm		WholeUnderVolt		整体欠压告警		整体欠压告警
35	32			15			Charge Over Current Alarm		ChargeOverCurr		充电过流告警		充电过流告警
36	32			15			Discharge Over Current Alarm		DisCharOverCurr		放电过流告警		放电过流告警
37	32			15			High Battery Temperature Alarm		HighBattTemp		电池高温告警		电池高温告警
38	32			15			Low Battery Temperature Alarm		LowBattTemp		电池低温告警		电池低温告警
39	32			15			High Ambient Temperature Alarm		HighAmbientTemp		环境高温告警		环境高温告警
40	32			15			Low Ambient Temperature Alarm		LowAmbientTemp		环境低温告警		环境低温告警
41	32			15			High PCB Temperature Alarm		HighPCBTemp		PCB高温告警		PCB高温告警
42	32			15			Low Battery Capacity Alarm		LowBattCapacity		容量过低告警		容量过低告警
43	32			15			High Voltage Difference Alarm		HighVoltDiff		压差过大告警		压差过大告警
44	32			15			Single Over Voltage Protect		SingOverVProt		单体过压保护		单体过压保护
45	32			15			Single Under Voltage Protect		SingUnderVProt		单体欠压保护		单体欠压保护
46	32			15			Whole Over Voltage Protect		WholeOverVProt		整体过压保护		整体过压保护
47	32			15			Whole Under Voltage Protect		WholeUnderVProt		整体欠压保护		整体欠压保护
48	32			15			Short Circuit Protect			ShortCircProt		短路保护		短路保护
49	32			15			Over Current Protect			OverCurrProt		过流保护		过流保护
50	32			15			Charge High Temperature Protect		CharHiTempProt		充电高温保护		充电高温保护
51	32			15			Charge Low Temperature Protect		CharLoTempProt		充电低温保护		充电低温保护
52	32			15			Discharge High Temp Protect		DischHiTempProt		放电高温保护		放电高温保护
53	32			15			Discharge Low Temp Protect		DischLoTempProt		放电低温保护		放电低温保护
54	32			15			Front End Sample Comm Fault		SampCommFault		前端采样通讯故障	采样通讯故障
55	32			15			Temp Sensor Disconnect Fault		TempSensDiscFlt		温度传感器断线		温度传感断线
56	32			15			Charging				Charging		充电中			充电中
57	32			15			Discharging				Discharging		放电中			放电中
58	32			15			Charging MOS Breakover			CharMOSBrkover		充电MOS管导通		充电MOS通
59	32			15			Dischanging MOS Breakover		DischMOSBrkover		放电MOS管导通		放电MOS通
60	32			15			Communication Address			CommAddress		通讯地址		通讯地址
61	32			15			Current Limit Activation		CurrLmtAct		限流激活		限流激活




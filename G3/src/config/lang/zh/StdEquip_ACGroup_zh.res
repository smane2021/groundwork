﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			AC Group			AC Group		交流组			交流组
2		32			15			Total Phase A Current		Phase A Current		A相电流			A相电流	
3		32			15			Total Phase B Current		Phase B Current		B相电流			B相电流	
4		32			15			Total Phase C Current		Phase C Current		C相电流			C相电流	
5		32			15			Total Phase A Power		Phase A Power		A相有功功率		A相有功功率
6		32			15			Total Phase B Power		Phase B Power		B相有功功率		B相有功功率
7		32			15			Total Phase C Power		Phase C Power		C相有功功率		C相有功功率
8		32			15			AC Unit Type			AC Unit Type		AC单元类型		AC单元类型
9		32			15			SCU AC Board			SCU AC Board		SCU ACBoard		SCU ACBoard
10		32			15			SCU No AC Board			SCU No AC Board		SCU NoACBoard		SCU NoACBoard
11		32			15			SMAC				SMAC			SMAC			SMAC
12		32			15			Mains Failure			Mains Failure		市电停电		市电停电
13		32			15			Existence State			Existence State		是否存在		是否存在
14		32			15			Existent			Existent		存在			存在
15		32			15			Not Existent			Not Existent		不存在			不存在
16		32			15			Total Input Current		Input Current		总输入电流		总输入电流	








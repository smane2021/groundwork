﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15		LVD1				LVD1			LVD1			LVD1
2	32			15		LVD2				LVD2			LVD2			LVD2
3	32			15		LVD3				LVD3			LVD3			LVD3
4		32			15			DC Distribution Number		DC Distr Number		直流屏数		直流屏数
5		32			15			LargeDU DC Distribution Group	DC Distr Group		LargDU直流屏组		直流屏组
6		32			15			Temp1 High Temperature Limit	Temp1 High Lmt		温度1过温点		温度1过温点
7		32			15			Temp2 High Temperature Limit	Temp2 High Lmt		温度2过温点		温度2过温点
8		32			15			Temp3 High Temperature Limit	Temp3 High Lmt		温度3过温点		温度3过温点
9		32			15			Temp1 Low Temperature Limit	Temp1 Low Lmt		温度1欠温点		温度1欠温点
10		32			15			Temp2 Low Temperature Limit	Temp2 Low Lmt		温度2欠温点		温度2欠温点
11		32			15			Temp3 Low Temperature Limit	Temp3 Low Lmt		温度3欠温点		温度3欠温点
12		32			15			LVD1 Voltage			LVD1 Voltage		LVD1下电电压		LVD1下电电压
13		32			15			LVD2 Voltage			LVD2 Voltage		LVD2下电电压		LVD2下电电压
14		32			15			LVD3 Voltage			LVD3 Voltage		LVD3下电电压		LVD3下电电压
15		32			15			Over Voltage			Over Voltage		直流过压告警点		过压告警点
16		32			15			Under Voltage			Under Voltage		直流欠压告警点		欠压告警点
17		32			15			On				On			上电			上电
18		32			15			Off				Off			下电			下电
19		32			15			On				On			上电			上电
20		32			15			Off				Off			下电			下电
21		32			15			On				On			上电			上电
22		32			15			Off				Off			下电			下电
23		32			15			Total Load Current		Total Load Curr		负载总电流		负载总电流
24		32			15			Total DC Distribution Current	TotalDCDistCurr		直流屏总电流		直流屏总电流
25		32			15			DC Distribution Average Voltage	DCDistrAveVolt		直流屏平均电压		直流屏平均电压
26	32			15		LVD1			LVD1		LVD1允许		LVD1允许
27	32			15		LVD1 Mode			LVD1 Mode		LVD1下电方式		LVD1下电方式
28	32			15		LVD1 Time		LVD1 Time		LVD1下电时间		LVD1下电时间
29	32			15		LVD1 Reconnect Voltage		LVD1 Recon Volt		LVD1上电电压		LVD1上电电压
30	32			15		LVD1 Reconnect Delay		LVD1 ReconDelay		LVD1上电延迟		LVD1上电延迟
31	32			15		LVD1 Dependency			LVD1 Dependency		LVD1依赖性		LVD1依赖性
32	32			15		LVD2			LVD2		LVD2允许		LVD2允许
33	32			15		LVD2 Mode			LVD2 Mode		LVD2下电方式		LVD2下电方式
34	32			15		LVD2 Time		LVD2 Time		LVD2下电时间		LVD2下电时间
35	32			15		LVD2 Reconnect Voltage		LVD2 Recon Volt		LVD2上电电压		LVD2上电电压
36	32			15		LVD2 Reconnect Delay		LVD2 ReconDelay		LVD2上电延迟		LVD2上电延迟
37	32			15		LVD2 Dependency			LVD2 Dependency		LVD2依赖性		LVD2依赖性
38	32			15		LVD3			LVD3		LVD3允许		LVD3允许
39	32			15		LVD3 Mode			LVD3 Mode		LVD3下电方式		LVD3下电方式
40	32			15		LVD3 Time		LVD3 Time		LVD3下电时间		LVD3下电时间
41	32			15		LVD3 Reconnect Voltage		LVD3 Recon Volt		LVD3上电电压		LVD3上电电压
42	32			15		LVD3 Reconnect Delay		LVD3 ReconDelay		LVD3上电延迟		LVD3上电延迟
43	32			15		LVD3 Dependency			LVD3 Dependency		LVD3依赖性		LVD3依赖性
44		32			15			Disabled			Disabled		禁止			禁止
45		32			15			Enabled				Enabled			允许			允许
46		32			15			Voltage			Voltage		电压方式		电压方式
47		32			15		Time			Time			时间方式		时间方式
48	32			15		None				None			无			无
49	32			15		LVD1				LVD1			LVD1			LVD1
50	32			15		LVD2				LVD2			LVD2			LVD2
51	32			15		LVD3				LVD3			LVD3			LVD3
52	32			15		Number of LVD			Num of LVD		LVD级数			LVD级数
53		32			15			0				0			0			0
54		32			15			1				1			1			1
55		32			15			2				2			2			2
56		32			15			3				3			3			3
57		32			15			Existence State			Existence State		是否存在		是否存在
58		32			15			Existent			Existent		存在			存在
59		32			15			Not Existent			Not Existent		不存在			不存在
60		32			15			Battery Over Voltage		Batt Over Volt		电池过压告警点		电池过压告警点
61		32			15			Battery Under Voltage		Batt Under Volt		电池欠压告警点		电池欠压告警点






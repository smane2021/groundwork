﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15		Fuse 1 Voltage		Fuse 1 Voltage		支路1电压		支路1电压	
2	32			15		Fuse 2 Voltage		Fuse 2 Voltage		支路2电压		支路2电压	
3	32			15		Fuse 3 Voltage		Fuse 3 Voltage		支路3电压		支路3电压	
4	32			15		Fuse 4 Voltage		Fuse 4 Voltage		支路4电压		支路4电压
5	32			15		Fuse 1 Alarm		Fuse 1 Alarm		支路1告警		支路1告警	
6	32			15		Fuse 2 Alarm		Fuse 2 Alarm		支路2告警		支路2告警	
7	32			15		Fuse 3 Alarm		Fuse 3 Alarm		支路3告警		支路3告警	
8	32			15		Fuse 4 Alarm		Fuse 4 Alarm		支路4告警		支路4告警	
9	32			15	Battery Fuse		Batt Fuse		OB电池支路		OB电池支路
10		32			15			On			On			正常			正常
11		32			15			Off			Off			断开			断开
12	32			15		Fuse 1 Status		Fuse 1 Status		支路1状态		支路1状态
13	32			15		Fuse 2 Status		Fuse 2 Status		支路2状态		支路2状态	
14	32			15		Fuse 3 Status		Fuse 3 Status		支路3状态		支路3状态	
15	32			15		Fuse 4 Status		Fuse 4 Status		支路4状态		支路4状态	
16		32			15			State			State			State			State
17		32			15			Communication Fail			Comm Fail			通信中断		通信中断
18		32			15			No			No			否			否
19		32			15			Yes			Yes			是			是
20		32			15			Number of Battery Fuses	Num of BatFuses	电池熔丝路数		电池熔丝路数
21	32			15		0			0			0			0	
22	32			15		1			1			1			1	
23	32			15		2			2			2			2	
24	32			15		3			3			3			3	
25	32			15		4			4			4			4	
26	32			15		5			5			5			5	
27	32			15		6			6			6			6	

28	32			15		Fuse 5 Voltage		Fuse 5 Voltage		支路5电压		支路5电压	
29	32			15		Fuse 6 Voltage		Fuse 6 Voltage		支路6电压		支路6电压
30	32			15		Fuse 5 Alarm		Fuse 5 Alarm		支路5告警		支路5告警	
31	32			15		Fuse 6 Alarm		Fuse 6 Alarm		支路6告警		支路6告警	
32	32			15		Fuse 5 Status		Fuse 5 Status		支路5状态		支路5状态
33	32			15		Fuse 6 Status		Fuse 6 Status		支路6状态		支路6状态
34	32			15		7			7			7			7	
35	32			15		8			8			8			8	
36	32			15		Fuse 7 Voltage		Fuse 7 Voltage		支路7电压		支路7电压	
37	32			15		Fuse 8 Voltage		Fuse 8 Voltage		支路8电压		支路8电压
38	32			15		Fuse 7 Alarm		Fuse 7 Alarm		支路7告警		支路7告警	
39	32			15		Fuse 8 Alarm		Fuse 8 Alarm		支路8告警		支路8告警	
40	32			15		Fuse 7 Status		Fuse 7 Status		支路7状态		支路7状态
41	32			15		Fuse 8 Status		Fuse 8 Status		支路8状态		支路8状态




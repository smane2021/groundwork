﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15		Phase A Voltage				R			A相电压			R
2	32			15		Phase B Voltage		S			B相电压			S
3	32			15		Phase C Voltage		T			C相电压			T
4	32			15		Line AB Voltage		R		AB线电压		R
5	32			15		Line BC Voltage		S		BC线电压		S
6	32			15		Line CA Voltage		T		CA线电压		T
7	32			15		Phase A Current		Phase A Current		A相电流			A相电流	
8	32			15		Phase B Current		Phase B Current		B相电流			B相电流	
9	32			15		Phase C Current		Phase C Current		C相电流			C相电流	
10	32			15		AC Frequency		AC Frequency		交流频率		交流频率
11	32			15		Total Real Power		Total Real Pwr		总有功功率		总有功功率
12	32			15		Phase A Real Power		PH-A Real Power		A相有功功率		A相有功功率
13	32			15		Phase B Real Power		PH-B Real Power		B相有功功率		B相有功功率
14	32			15		Phase C Real Power		PH-C Real Power		C相有功功率		C相有功功率
15	32			15		Total Reactive Power		Total React Pwr		总无功功率		总无功功率
16	32			15		Phase A Reactive Power		PH-A React Pwr		A相无功功率		A相无功功率
17	32			15		Phase B Reactive Power		PH-B React Pwr		B相无功功率		B相无功功率
18	32			15		Phase C Reactive Power		PH-C React Pwr		C相无功功率		C相无功功率
19	32			15		Total Apparent Power		Total Appar Pwr		总视在功率		总视在功率
20	32			15		Phase A Apparent Power		PH-A Appar Pwr		A相视在功率		A相视在功率
21	32			15		Phase B Apparent Power		PH-B Appar Pwr		B相视在功率		B相视在功率
22	32			15		Phase C Apparent Power		PH-C Appar Pwr		C相视在功率		C相视在功率
23	32			15		Power Factor		Power Factor		功率因数		功率因数
24	32			15		Phase A Power Factor		PH-A Pwr Factor		A相功率因数		A相功率因数
25	32			15		Phase B Power Factor		PH-B Pwr Factor		B相功率因数		B相功率因数
26	32			15		Phase C Power Factor		PH-C Pwr Factor		C相功率因数		C相功率因数
27	32			15		Phase A Current Crest Factor		Ia Crest Factor		Ia波峰因数		Ia波峰因数
28	32			15		Phase B Current Crest Factor		Ib Crest Factor		Ib波峰因数		Ib波峰因数
29	32			15		Phase C Current Crest Factor		Ic Crest Factor		Ic波峰因数		Ic波峰因数
30	32			15		Phase A Current THD		PH-A Curr THD		A相电流THD		A相电流THD
31	32			15		Phase B Current THD		PH-B Curr THD		B相电流THD		B相电流THD
32	32			15		Phase C Current THD		PH-C Curr THD		C相电流THD		C相电流THD
33	32			15		Phase A Voltage THD		PH-A Volt THD		A相电压THD		A相电压THD
34	32			15		Phase B Voltage THD		PH-B Volt THD		B相电压THD		B相电压THD
35	32			15		Phase C Voltage THD		PH-C Volt THD		C相电压THD		C相电压THD
36	32			15		Total Real Energy		TotalRealEnergy		总有功能量		总有功能量
37	32			15		Total Reactive Energy		TotalReacEnergy		总无功能量		总无功能量
38	32			15		Total Apparent Energy		TotalAppaEnergy		总视在能量		总视在能量
39	32			15		Ambient Temperature		Ambient Temp		环境温度		环境温度
40	32			15		Nominal Line Voltage			NominalLineVolt		标称线电压		标称线电压
41	32			15		Nominal Phase Voltage			Nominal PH-Volt		标称相电压		标称相电压
42	32			15		Nominal Frequency			Nominal Freq		标称频率		标称频率
43	32			15		Mains Failure Alarm Limit 1		Mains Fail Alm1		电压告警门限1		电压告警门限1
44	32			15		Mains Failure Alarm Limit 2		Mains Fail Alm2		电压告警门限2		电压告警门限2
45	32			15		Voltage Alarm Limit 1			Volt Alarm Lmt1		电压告警门限1		电压告警门限1
46	32			15		Voltage Alarm Limit 2			Volt Alarm Lmt2		电压告警门限2		电压告警门限2
47	32			15		Frequency Alarm Limit			Freq Alarm Lmt		频率告警门限		频率告警门限
48	32			15		High Temperature Limit			High Temp Limit		高温点			高温点
49	32			15		Low Temperature Limit			Low Temp Limit		低温点			低温点
50	32			15		Supervision Fail			SupervisionFail		监控失败		监控失败
51	32			15		Line AB Over Voltage 1			L-AB Over Volt1		线电压AB高压		线电压AB高压
52	32			15			Line AB Over Voltage 2			L-AB Over Volt2		线电压AB超高		线电压AB超高
53	32			15			Line AB Under Voltage 1			L-AB UnderVolt1		线电压AB低压		线电压AB低压
54	32			15			Line AB Under Voltage 2			L-AB UnderVolt2		线电压AB超低		线电压AB超低
55	32			15			Line BC Over Voltage 1			L-BC Over Volt1		线电压BC高压		线电压BC高压
56	32			15			Line BC Over Voltage 2			L-BC Over Volt2		线电压BC超高		线电压BC超高
57	32			15			Line BC Under Voltage 1			L-BC UnderVolt1		线电压BC低压		线电压BC低压
58	32			15			Line BC Under Voltage 2			L-BC UnderVolt2		线电压BC超低		线电压BC超低
59	32			15			Line CA Over Voltage 1			L-CA Over Volt1		线电压CA高压		线电压CA高压
60	32			15			Line CA Over Voltage 2			L-CA Over Volt2		线电压CA超高		线电压CA超高
61	32			15			Line CA Under Voltage 1			L-CA UnderVolt1		线电压CA低压		线电压CA低压
62	32			15			Line CA Under Voltage 2			L-CA UnderVolt2		线电压CA超低		线电压CA超低
63	32			15			Phase A Over Voltage 1			PH-A Over Volt1		A相电压高		A相电压高
64	32			15			Phase A Over Voltage 2			PH-A Over Volt2		A相电压超高		A相电压超高
65	32			15			Phase A Under Voltage 1			PH-A UnderVolt1		A相电压低		A相电压低
66	32			15			Phase A Under Voltage 2			PH-A UnderVolt2		A相电压超低		A相电压超低
67	32			15			Phase B Over Voltage 1			PH-B Over Volt1		B相电压高		B相电压高
68	32			15			Phase B Over Voltage 2			PH-B Over Volt2		B相电压超高		B相电压超高
69	32			15			Phase B Under Voltage 1			PH-B UnderVolt1		B相电压低		B相电压低
70	32			15			Phase B Under Voltage 2			PH-B UnderVolt2		B相电压超低		B相电压超低
71	32			15			Phase C Over Voltage 1			PH-C Over Volt1		C相电压高		C相电压高
72	32			15			Phase C Over Voltage 2			PH-C Over Volt2		C相电压超高		C相电压超高
73	32			15			Phase C Under Voltage 1			PH-C UnderVolt1		C相电压低		C相电压低
74	32			15			Phase C Under Voltage 2			PH-C UnderVolt2		C相电压超低		C相电压超低
75	32			15			Mains Failure				Mains Failure		交流停电		交流停电
76	32			15			Severe Mains Failure			SevereMainsFail		严重市电失败		严重市电失败
77	32			15			High Frequency				High Frequency		高频			高频
78	32			15			Low Frequency				Low Frequency		低频			低频
79	32			15			High Temperature			High Temp		高温			高温
80	32			15			Low Temperature				Low Temperature		低温			低温
81	32			15			Rectifier AC				Rectifier AC		模块交流		模块交流
82	32			15			Supervision Fail			SupervisionFail		监控失败		监控失败
83	32			15			No					No			否			否
84	32			15			Yes					Yes			是			是
85	32			15			Phase A Mains Failure Counter		PH-A Fail Count		A相故障计数		A相故障计数
86	32			15			Phase B Mains Failure Counter		PH-B Fail Count		B相故障计数		B相故障计数
87	32			15			Phase C Mains Failure Counter		PH-C Fail Count		C相故障计数		C相故障计数
88	32			15			Frequency Failure Counter		FreqFailCounter		频率故障计数		频率故障计数
89	32			15			Reset Phase A Mains Fail Counter	RstPH-AFailCnt		A相故障复位		A相故障复位
90	32			15			Reset Phase B Mains Fail Counter	RstPH-BFailCnt		B相故障复位		B相故障复位
91	32			15			Reset Phase C Mains Fail Counter	RstPH-CFailCnt		C相故障复位		C相故障复位
92	32			15			Reset Frequency Fail Counter		RstFreqFailCnt		频率故障复位		频率故障复位
93	32			15			Current Alarm Limit			Curr Alm Limit		AC过流告警点		AC过流告警点
94	32			15			Phase A High Current			PH-A High Curr		A相过流			A相过流
95	32			15			Phase B High Current			PH-B High Curr		B相过流			B相过流
96	32			15			Phase C High Current			PH-C High Curr		C相过流			C相过流
97	32			15		Minimum Phase Voltage			Min Phase Volt		相电压最小值		相电压最小值
98	32			15		Maximum Phase Voltage			Max Phase Volt		相电压最大值		相电压最大值
99	32			15		Raw Data 1				Raw Data 1		原始数据1		原始数据1
100	32			15		Raw Data 2				Raw Data 2		原始数据2		原始数据2
101	32			15		Raw Data 3				Raw Data 3		原始数据3		原始数据3
102	32			15		Reference Voltage			Reference Volt		基准电压		基准电压
103	32			15			State					State			State			State
104	32			15			Off					Off			off			off
105	32			15			On					On			on			on
106	32			15			High Phase Voltage			High Ph-Volt		相电压高		相电压高
107	32			15			Very High Phase Voltage		VHigh Ph-Volt		相电压超高		相电压超高
108	32			15			Low Phase Voltage			Low Ph-Volt		相电压低		相电压低
109	32			15			Very Low Phase Voltage			VLow Ph-Volt		相电压超低		相电压超低
110	32			15			All Rectifiers Comm Fail		AllRectCommFail		所有模块通信中断	所有模块通信断







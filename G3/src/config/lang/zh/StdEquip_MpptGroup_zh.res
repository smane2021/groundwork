﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE			
1	32			15			Solar Converter Group			Solar ConvGrp		太阳能模块组			太阳能模块组
2	32			15			Total Current				Total Current		模块总电流			模块总电流	
3	32			15			Average Voltage				Average Voltage		输出电压平均值			输出电压平均值	
4	32			15			System Capacity Used			Sys Cap Used		模块组使用容量			模块组使用容量	
5	32			15			Maximum Used Capacity			Max Cap Used		最大使用容量			最大使用容量	
6	32			15			Minimum Used Capacity			Min Cap Used		最小使用容量			最小使用容量	
7	36			15			Total Solar Converters Communicating	Num Solar Convs		通讯正常模块数			通讯正常模块数	
8	32			15			Valid Solar Converter			Valid SolarConv		有效模块数			有效模块数	
9	32			15			Number of Solar Converters		Num Solar Convs		整流模块数			太阳能模块数	
11	34			15			Solar Converter(s) Failure Status	SolarConv Fail		多模块故障			多模块故障	
12	32			15			Solar Converter Current Limit		SolConvCurrLmt		模块限流			模块限流	
13	32			15			Solar Converter Trim			Solar Conv Trim		模块调压			模块调压	
14	32			15			DC On/Off Control			DC On/Off Ctrl		模块直流开关机			模块直流开关机	
16	32			15			Solar Converter LED Control		SolConv LEDCtrl		模块LED灯控制			模块LED灯控制	
17	32			15			Fan Speed Control			Fan Speed Ctrl		风扇运行速度			风扇运行速度	
18	32			15			Rated Voltage				Rated Voltage		额定输出电压			额定输出电压	
19	32			15			Rated Current				Rated Current		额定输出电流			额定输出电流	
20	32			15			High Voltage Shutdown Limit		HVSD Limit		直流输出过压点			直流输出过压点	
21	32			15			Low Voltage Limit			Low Volt Limit		直流输出欠压点			直流输出欠压点	
22	32			15			High Temperature Limit			High Temp Limit		模块过温点			模块过温点	
24	32			15			HVSD Restart Time			HVSD Restart T		过压重启时间			过压重启时间	
25	32			15			Walk-In Time				Walk-In Time		带载软启动时间			带载软启动时间	
26	32			15			Walk-In					Walk-In			带载软启动允许			带载软启动允许	
27	32			15			Minimum Redundancy			Min Redundancy		最小冗余			最小冗余	
28	32			15			Maximum Redundancy			Max Redundancy		最大冗余			最大冗余	
29	32			15			Turn Off Delay				Turn Off Delay		关机延时			关机延时	
30	32			15			Cycle Period				Cycle Period		循环开关机周期			循环开关机周期	
31	32			15			Cycle Activation Time			Cyc Active Time		循环开关机执行时刻		开关机执行时刻	
33	32			15			Multiple Solar Converter Failure	MultSolConvFail		多模块故障			多模块故障	
36	32			15			Normal					Normal			正常				正常	
37	32			15			Failure					Failure			故障				故障	
38	32			15			Switch Off All				Switch Off All		关所有模块			关所有模块	
39	32			15			Switch On All				Switch On All		开所有模块			开所有模块	
42	32			15			All Flashing				All Flashing		全部灯闪			全部灯闪	
43	32			15			Stop Flashing				Stop Flashing		全部灯不闪			全部灯不闪	
44	32			15			Full Speed				Full Speed		全速				全速	
45	32			15			Automatic Speed				Auto Speed		自动调速			自动调速	
46	32			32			Current Limit Control			Curr Limit Ctrl		限流点控制			限流点控制	
47	32			32			Full Capability Control			Full Cap Ctrl		满容量运行			满容量运行	
54	32			15			Disabled				Disabled		否				否	
55	32			15			Enabled					Enabled			是				是	
68	32			15			ECO Mode				ECO Mode		ECO模式使能			ECO模式使能	
73	32			15			No					No			否				否	
74	32			15			Yes					Yes			是				是	
77	32			15			Pre-CurrLimit Turn-On			Pre-Curr Limit		开模块预限流			开模块预限流	
78	32			15			Solar Converter Power Type		SolConv PwrType		模块供电类型			模块供电类型	
79	32			15			Double Supply				Double Supply		双供电				双供电	
80	32			15			Single Supply				Single Supply		单供电				单供电	
81	32			15			Last Solar Converter Quantity		LastSolConvQty		原模块数			原模块数	
83	32			15			Solar Converter Lost			Solar Conv Lost		太阳能模块丢失			太阳能模块丢失	
84	32			15			Clear Solar Converter Lost Alarm	ClearSolarLost		清太阳能模块丢失告警		清Solar模块丢失	
85	32			15			Clear					Clear			清除				清除
86	32			15			Confirm Solar Converter ID		Confirm ID		确认模块位置			确认位置	
87	32			15			Confirm					Confirm			确认位置			确认位置                    
88	32			15			Best Operating Point			Best Oper Point		最佳工作点			最佳工作点
89	32			15			Solar Converter Redundancy		SolConv Redund		节能运行			节能运行
90	32			15			Load Fluctuation Range			Fluct Range		负载波动率			负载波动率
91	32			15			System Energy Saving Point		Energy Save Pt		系统节能点			系统节能点
92	32			15			E-Stop Function				E-Stop Function		E-Stop功能			E-Stop功能
94	32			15			Single Phase				Single Phase		单相				单相
95	32			15			Three Phases				Three Phases		三相				三相
96	32			15			Input Current Limit			Input Curr Lmt		输入电流限值			输入电流限值
97	32			15			Double Supply				Double Supply		双供电				双供电
98	32			15			Single Supply				Single Supply		单供电				单供电
99	32			15			Small Supply				Small Supply		Small模块供电方式		Small供电方式
100	32			15			Restart on HVSD				Restart on HVSD		直流过压复位			直流过压复位
101	32			15			Sequence Start Interval			Start Interval		顺序开机间隔			顺序开机间隔
102	32			15			Rated Voltage				Rated Voltage		额定输出电压			额定输出电压		
103	32			15			Rated Current				Rated Current		额定输出电流			额定输出电流		
104	32			15			All Solar Converters Comm Fail		SolarsComFail		所有模块通信中断		所有模块通信断
105	32			15			Inactive				Inactive		否				否		
106	32			15			Active					Active			是				是		
112	32			15			Rated Voltage (Internal Use)		Rated Voltage		额定输出电压(Internal Use)	额定输出电压		
113	32			15			SolarConv Info Change(Internal)	InfoChange		模块信息发生改变		模块信息已改变
114	32			15			MixHE Power				MixHE Power		高功率模块是否降额		高功率模块降额
115	32			15			Derated					Derated			降额				降额		
116	32			15			Non-Derated				Non-Derated		不降				不降		
117	32			15			All Solar Converter ON Time		SolarConvONTime		模块烘干时间			模块烘干时间
118	32			15			All Solar Converter are On		AllSolarConvOn		模块正在烘干			模块正在烘干
119	39			15			Clear Solar Converter Comm Fail Alarm	ClrSolCommFail		清模块通讯中断告警		清模块通讯中断
120	32			15			HVSD					HVSD			HVSD使能			HVSD使能
121	32			15			HVSD Voltage Difference			HVSD Volt Diff		HVSD压差			HVSD压差
122	32			15			Total Rated Current			Total Rated Cur		工作模块总额定电流		工作模块总额定
123	32			15			Diesel Generator Power Limit		DG Pwr Lmt		油机限功率使能			油机限功率使能
124	32			15			Diesel Generator Digital Input		Diesel DI Input		油机干接点输入			油机干接点输入
125	32			15			Diesel Gen Power Limit Point Set	DG Pwr Lmt Pt		油机限功率点			油机限功率点
126	32			15			None					None			无				无
140	32			15			Solar Converter Delta Voltage		SolConvDeltaVol		MPPT状态电压差			MPPT电压差
141	32			15			Solar Status				Solar Status		光照状态			光照状态
142	32			15			Day					Day			白天				白天
143	32			15			Night					Night			黑夜				黑夜
144	32			15			Existence State				Existence State		是否存在		是否存在
145	32			15			Existent				Existent		存在			存在
146	32			15			Not Existent				Not Existent		不存在			不存在
147	32			15			Total Input Current			Input Current		总输入电流			总输入电流	
148	32			15			Total Input Power			Input Power		输入总功率			输入总功率	
149	32			15			Total Rated Current			Total Rated Cur		工作模块总额定电流		工作模块总额定
150	32			15			Total Output Power			Output Power		总输出功率			总输出功率
151	32			15			Reset Solar Converter IDs		Reset Solar IDs		清除模块位置号		清除模块位置号
152	32			15			Clear Solar Converter Comm Fail		ClrSolCommFail		清太阳能模块通讯中断		清Solar通讯中断
153	32			15			Solar Failure Min Time			SolFailMinTime		失败时长		失败时长

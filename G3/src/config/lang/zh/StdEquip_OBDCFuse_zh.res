﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15		Fuse 1				Fuse 1			支路1			支路1
2	32			15		Fuse 2				Fuse 2			支路2			支路2
3	32			15		Fuse 3				Fuse 3			支路3			支路3
4	32			15		Fuse 4				Fuse 4			支路4			支路4
5	32			15		Fuse 5				Fuse 5			支路5			支路5
6	32			15		Fuse 6				Fuse 6			支路6			支路6
7	32			15		Fuse 7				Fuse 7			支路7			支路7
8	32			15		Fuse 8				Fuse 8			支路8			支路8
9	32			15		Fuse 9				Fuse 9			支路9			支路9
10	32			15		Auxiliary Load Fuse		Aux Load Fuse		辅助支路		辅助支路
11	32			15		Fuse 1 Alarm			Fuse 1 Alarm		支路1告警		支路1告警
12	32			15		Fuse 2 Alarm			Fuse 2 Alarm		支路2告警		支路2告警
13	32			15		Fuse 3 Alarm			Fuse 3 Alarm		支路3告警		支路3告警
14	32			15		Fuse 4 Alarm			Fuse 4 Alarm		支路4告警		支路4告警
15	32			15		Fuse 5 Alarm			Fuse 5 Alarm		支路5告警		支路5告警
16	32			15		Fuse 6 Alarm			Fuse 6 Alarm		支路6告警		支路6告警
17	32			15		Fuse 7 Alarm			Fuse 7 Alarm		支路7告警		支路7告警
18	32			15		Fuse 8 Alarm			Fuse 8 Alarm		支路8告警		支路8告警
19	32			15		Fuse 9 Alarm			Fuse 9 Alarm		支路9告警		支路9告警
20	32			15		Auxiliary Load Fuse Alarm	AuxLoadFuseAlm		辅助支路告警		辅助支路告警
21	32			15		On				On			连接			连接
22	32			15		Off				Off			断开			断开
23	32			15		DC Fuse Unit			DC Fuse Unit		直流负载支路		直流负载支路
24	32			15		Fuse 1 Voltage			Fuse 1 Voltage		支路1			支路1
25	32			15		Fuse 2 Voltage			Fuse 2 Voltage		支路2			支路2
26	32			15		Fuse 3 Voltage			Fuse 3 Voltage		支路3			支路3
27	32			15		Fuse 4 Voltage			Fuse 4 Voltage		支路4			支路4
28	32			15		Fuse 5 Voltage			Fuse 5 Voltage		支路5			支路5
29	32			15		Fuse 6 Voltage			Fuse 6 Voltage		支路6			支路6
30	32			15		Fuse 7 Voltage			Fuse 7 Voltage		支路7			支路7
31	32			15		Fuse 8 Voltage			Fuse 8 Voltage		支路8			支路8
32	32			15		Fuse 9 Voltage			Fuse 9 Voltage		支路9			支路9
33	32			15		Fuse 10 Voltage			Fuse 10 Voltage		支路10			支路10
34	32			15		Fuse 10				Fuse 10			支路10			支路10
35	32			15		Fuse 10 Alarm			Fuse 10 Alarm		支路10告警		支路10告警
36		32			15			State				State			工作状态		工作状态
37		32			15			Communication Fail		Comm Fail		通信中断		通信中断
38		32			15			No				No			否			否
39		32			15			Yes				Yes			是			是
40	32			15		Fuse 11				Fuse 11			支路11			支路11
41	32			15		Fuse 12				Fuse 12			支路12			支路12
42	32			15		Fuse 11 Alarm			Fuse 11 Alarm		支路11告警		支路11告警
43	32			15		Fuse 12 Alarm			Fuse 12 Alarm		支路12告警		支路12告警

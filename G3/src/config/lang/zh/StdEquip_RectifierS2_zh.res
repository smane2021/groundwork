﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
zh



[RES_INFO]
#R_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE		
1		32		15			GroupII Rectifier		GroupII Rect		组II整流模块			组II整流模块		
2		32		15			DC Status			DC Status		直流开关状态			直流开关状态	
3		32		15			DC Output Voltage		DC Voltage		输出电压			模块电压	
4		32		15			Origin Current			Origin Current		模块电流原始值			模块电流原始值
5		32		15			Temperature			Temperature		模块温度			模块温度	
6		32		15			Used Capacity			Used Capacity		使用容量			使用容量	
7		32		15			AC Input Voltage		AC Voltage		交流输入电压			交流输入电压	
8		32		15			DC Output Current		DC Current		直流输出电流			直流输出电流	
9		32		15			SN				SN			序列号				序列号	
10		32		15			Total Running Time		Running Time		模块总运行时间			模块总运行时间	
11		32		15			Communication Fail Count		Comm Fail Count		通讯中断次数			通讯中断次数	
12		32		15			Derated by AC			Derated by AC		交流限功率			交流限功率	
13		32		15			Derated by Temp			Derated by Temp		温度限功率			温度限功率	
14		32		15			Derated				Derated			模块限功率			模块限功率	
15		32		15			Full Fan Speed			Full Fan Speed		风扇全速			风扇全速	
16		32		15			Walk-In		Walk-In		Walk-In功能			Walk-In功能	
17		32		15			AC On/Off			AC On/Off		交流开关状态			交流开关状态	
18		32		15			Current Limit			Curr Limit		模块限流点			模块限流点	
19		32		15			Voltage High Limit		Volt Hi-limit		直流电压告警上限		电压告警上限	
20		32		15			AC Input Status			AC Status		交流状态			交流状态	
21		32		15			Rect Temperature High		Rect Temp High		模块过温			模块过温		
22		32		15			Rectifier Fail			Rect Fail		模块故障			模块故障		
23		32		15			Rectifier Protected		Rect Protected		模块保护			模块保护	
24		32		15			Fan Fail		Fan Fail	模块风扇故障			模块风扇故障	
25		32		15			Current Limit State		CurrLimit State		模块限流状态			模块限流状态	
26		32		15			EEPROM Fail		EEPROM Fail	EEPROM故障			EEPROM故障	
27		32		15			DC On/Off Control		DC On/Off Ctrl		模块直流开关机			模块直流开关机
28		32		15			AC On/Off Control		AC On/Off Ctrl		模块交流开关机			模块交流开关机
29		32		15			LED Control			LED Control		模块灯控制			模块灯控制	
30		32		15			Rectifier Reset			Rectifier Reset		模块复位			模块复位	
31		32		15			AC Input Fail	AC Fail	模块交流停电			模块交流停电	
34		32		15			Over Voltage			Over Voltage		直流输出过压			直流输出过压	
37		32		15			Current Limit			Current Limit		模块限流			模块限流	
39		32		15			Normal				Normal			否				否
40		32		15			Limited				Limited			是				是
45		32		15			Normal				Normal			正常				正常	
46		32		15			Full				Full			全速				全速	
47		32		15			Disabled			Disabled		无效				无效	
48		32		15			Enabled				Enabled			有效				有效	
49		32		15			On				On			开				开
50		32		15			Off				Off			关				关
51		32		15			Normal				Normal			正常				正常	
52		32		15			Fail			Fail		故障				故障	
53		32		15			Normal				Normal			正常				正常	
54		32		15			Over Temperature		Over Temp		过温				过温度	
55		32		15			Normal				Normal			正常				正常
56		32		15			Fail				Fail			故障				故障
57		32		15			Normal				Normal			正常				正常
58		32		15			Protected			Protected		保护				保护
59		32		15			Normal				Normal			正常				正常
60		32		15			Fail			Fail		故障				故障
61		32		15			Normal				Normal			正常				正常
62		32		15			Alarm				Alarm			告警				告警
63		32		15			Normal				Normal			正常				正常
64		32		15			Fail			Fail		故障				故障
65		32		15			Off				Off			关				关
66		32		15			On				On			开				开
67		32		15			Off				Off			关				关
68		32		15			On				On			开				开
69		32		15			LED Control			LED Control			灯闪				灯闪	
70		32		15			Cancel				Cancel			不闪				不闪	
71		32		15			Off				Off			关				关	
72		32		15			Reset				Reset			复位				复位
73		32		15			Open Rectifier			Rectifier On			开				开
74		32		15			Close Rectifier			Rectifier Off			关				关
75		32		15			Off				Off			关				关
76		32		15			LED Control			LED Control			闪灯				闪灯
77		32		15			Rectifier Reset			Rectifier Reset		模块复位			模块复位
80		32		15			Rectifier Communication Fail		Rect Comm Fail		模块通讯中断			模块通讯中断
84		32		15			Rectifier High SN		Rect High SN		模块高序列号			模块高序列号
85		32		15			Rectifier Version		Rect Version		模块版本			模块版本
86		32		15			Rectifier Part Number		Rect Part Num		模块产品号			模块产品号
87		32		15			Current Share State		CurrShare State		模块均流状态			模块均流状态
88		32		15			Current Share Alarm		CurrShare Alarm		模块不均流			模块不均流
89		32		15			Over Voltage			Over Voltage		模块过压			模块过压
90		32		15			Normal				Normal			正常				正常
91		32		15			Over Voltage			Over Voltage		过压				过压
92		32		15			Line AB Voltage			Line AB Volt		线电压 AB			线电压 AB
93		32		15			Line BC Voltage			Line BC Volt		线电压 BC			线电压 BC
94		32		15			Line CA Voltage			Line CA Volt		线电压 CA			线电压 CA
95		32		15			Low Voltage			Low Voltage		欠压				欠压
96		32		15			Low AC Voltage Protection	Low AC Protect		模块交流欠压保护		交流欠压保护
97		32		15			Rectifier ID		Rectifier ID		模块位置号			模块位置号
98		32		15			DC Output Turned Off		DC Output Off		模块直流输出关			直流输出关
99		32		15			Rectifier Phase			Rect Phase		模块相位			模块相位
100		32		15			A				A			A				A
101		32		15			B				B			B				B
102		32		15			C				C			C				C
103		32		15			Severe Sharing CurrAlarm	SevereCurrShare		严重模块不均流			严重模块不均流
104		32		15			Barcode 1			Barcode 1		Bar Code1			Bar Code1
105		32		15			Barcode 2			Barcode 2		Bar Code2			Bar Code2
106		32		15			Barcode 3			Barcode 3		Bar Code3			Bar Code3
107		32		15			Barcode 4			Barcode 4		Bar Code4			Bar Code4
108		32		15			Rectifier Communication Fail		Rect Comm Fail		通信中断			通信中断
109		32		15			No				No			否				否
110		32		15			Yes				Yes			是				是
111		32		15			Existence State			Existence State		设备是否存在			设备是否存在
113		32		15			Comm OK				Comm OK			模块通讯正常			模块通讯正常
114		32		15			All Rectifiers Comm Fail			AllRectCommFail		模块都通讯中断			都通讯中断
115		32		15			Communication Fail			Comm Fail		模块通讯中断			模块通讯中断
116		32		15			Rated Current			Rated Current		额定电流			额定电流
117		32		15			Efficiency			Efficiency		额定效率			额定效率
118		32		15			LT 93				LT 93			小于93				小于93
119		32		15			GT 93				GT 93			大于93				大于93
120		32		15			GT 95				GT 95			大于95				大于95
121		32		15			GT 96				GT 96			大于96				大于96
122		32		15			GT 97				GT 97			大于97				大于97
123		32		15			GT 98				GT 98			大于98				大于98
124		32		15			GT 99				GT 99			大于99				大于99
125		32		15			Redundancy Related Alarm	Redundancy Alm		冗余相关告警			冗余相关告警
126		32		15			Rect HVSD Status		HVSD Status		模块HVSD状态			模块HVSD状态
276		32		15			EStop/EShutdown			EStop/EShutdown		EStop/EShutdown			EStop/EShutdown



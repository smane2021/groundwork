﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE			
1	32			15		Analog Input 1				Analog Input 1		模拟量输入1			模拟量输入1
2	32			15		Analog Input 2				Analog Input 2		模拟量输入2			模拟量输入2
3	32			15		Analog Input 3				Analog Input 3		模拟量输入3			模拟量输入3
4	32			15		Analog Input 4				Analog Input 4		模拟量输入4			模拟量输入4
5	32			15		Analog Input 5				Analog Input 5		模拟量输入5			模拟量输入5
6	32			15		Frequency Input			Frequency Input		频率输入			频率输入
7	32			15		Digital Input 1			Digital Input 1		数字量输入1			数字量输入1
8	32			15		Digital Input 2			Digital Input 2		数字量输入2			数字量输入2
9	32			15		Digital Input 3			Digital Input 3		数字量输入3			数字量输入3
10	32			15		Digital Input 4			Digital Input 4		数字量输入4			数字量输入4
11	32			15		Digital Input 5			Digital Input 5		数字量输入5			数字量输入5
12	32			15		Digital Input 6			Digital Input 6		数字量输入6			数字量输入6
13	32			15		Digital Input 7			Digital Input 7		数字量输入7			数字量输入7
14	32			15		Relay 1 Status			Relay 1 Status		继电器1状态			继电器1状态
15	32			15		Relay 2 Status			Relay 2 Status		继电器2状态			继电器2状态
16	32			15		Relay 3 Status			Relay 3 Status		继电器3状态			继电器3状态
17	32			15		Relay 1 On/Off			Relay 1 On/Off		继电器1开关			继电器1开关
18	32			15		Relay 2 On/Off			Relay 2 On/Off		继电器2开关			继电器2开关
19	32			15		Relay 3 On/Off			Relay 3 On/Off		继电器3开关			继电器3开关
23	32			15		High Analog Input 1 Limit		Hi AI 1 Limit		高模拟量输入1限点			高模量输入1限点
24	32			15		Low Analog Input 1 Limit		Low AI 1 Limit		低模拟量输入1限点			低模量输入1限点
25	32			15		High Analog Input 2 Limit		Hi AI 2 Limit		高模拟量输入2限点			高模量输入2限点
26	32			15		Low Analog Input 2 Limit		Low AI 2 Limit		低模拟量输入2限点			低模量输入2限点
27	32			15		High Analog Input 3 Limit		Hi AI 3 Limit		高模拟量输入3限点			高模量输入3限点
28	32			15		Low Analog Input 3 Limit		Low AI 3 Limit		低模拟量输入3限点			低模量输入3限点
29	32			15		High Analog Input 4 Limit		Hi AI 4 Limit		高模拟量输入4限点			高模量输入4限点
30	32			15		Low Analog Input 4 Limit		Low AI 4 Limit		低模拟量输入4限点			低模量输入4限点
31	32			15		High Analog Input 5 Limit		Hi AI 5 Limit		高模拟量输入5限点			高模量输入5限点
32	32			15		Low Analog Input 5 Limit		Low AI 5 Limit		低模拟量输入5限点			低模量输入5限点
33	32			15		High Frequency Limit		High Freq Limit		高频点					高频点
34	32			15		Low Frequency Limit		Low Freq Limit		低频点					低频点
35	32			15		High Analog Input 1 Alarm		Hi AI 1 Alarm		高模拟量输入1告警			高模量输入1告警
36	32			15		Low Analog Input 1 Alarm		Low AI 1 Alarm		低模拟量输入1告警			低模量输入1告警
37	32			15		High Analog Input 2 Alarm		Hi AI 2 Alarm		高模拟量输入2告警			高模量输入2告警
38	32			15		Low Analog Input 2 Alarm		Low AI 2 Alarm		低模拟量输入2告警			低模量输入2告警
39	32			15		High Analog Input 3 Alarm		Hi AI 3 Alarm		高模拟量输入3告警			高模量输入3告警
40	32			15		Low Analog Input 3 Alarm		Low AI 3 Alarm		低模拟量输入3告警			低模量输入3告警
41	32			15		High Analog Input 4 Alarm		Hi AI 4 Alarm		高模拟量输入4告警			高模量输入4告警
42	32			15		Low Analog Input 4 Alarm		Low AI 4 Alarm		低模拟量输入4告警			低模量输入4告警
43		32			15			High Analog Input 5 Alarm		Hi AI 5 Alarm		高模拟量输入5告警			高模量输入5告警
44		32			15			Low Analog Input 5 Alarm		Low AI 5 Alarm		低模拟量输入5告警			低模量输入5告警
45		32			15			High Frequency Input Alarm		Hi Freq In Alm		高频输入告警				高频输入告警
46		32			15			Low Frequency Input Alarm		Low Freq In Alm		低频输入告警				低频输入告警
47	32			15		Off					Off		关			关
48	32			15		On					On		开			开
49	32			15		Off					Off		关			关
50	32			15		On					On		开			开
51	32			15		Off					Off		关			关
52		32			15			On					On		开			开
53		32			15			Off					Off		关			关
54		32			15			On					On		开			开
55		32			15			Off					Off		关			关
56		32			15			On					On		开			开
57		32			15			Off					Off		关			关
58		32			15			On					On		开			开
59		32			15			Off					Off		关			关
60		32			15			On					On		开			开
61		32			15			Off					Off		关			关
62		32			15			On					On		开			开
63		32			15			Off					Off		关			关
64		32			15			On					On		开			开
65		32			15			Off					Off		关			关
66		32			15			On					On		开			开
67		32			15			Off					Off		关			关
68		32			15			On					On		开			开
69		32			15			Off					Off		关			关
70		32			15			On					On		开			开
71		32			15			Off					Off		关			关
72		32			15			On					On		开			开
73		32			15			SMIO Generic Unit 7			SMIO Unit 7		SMIO 7					SMIO 7
74		32			15			SMIO Failure				SMIO Fail		SMIO通讯失败				SMIO通讯失败
75		32			15			SMIO Failure				SMIO Fail		SMIO通讯失败				SMIO通讯失败
76		32			15			No					No			否					否
77		32			15			Yes					Yes			是					是
78		32			15			Testing Relay 1			Testing Relay 1		测试继电器1		测试继电器1
79		32			15			Testing Relay 2			Testing Relay 2		测试继电器2		测试继电器2
80		32			15			Testing Relay 3			Testing Relay 3		测试继电器3		测试继电器3

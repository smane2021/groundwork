﻿# 
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15		Phase A Voltage				Phase A Volt		A相电压			A相电压
2	32			15		Phase B Voltage		Phase B Volt		B相电压			B相电压
3	32			15		Phase C Voltage		Phase C Volt		C相电压			C相电压
4	32			15		AB Line Voltage		AB Line Volt		AB线电压		AB线电压
5	32			15		BC Line Voltage		BC Line Volt		BC线电压		BC线电压
6	32			15		CA Line Voltage		CA Line Volt		CA线电压		CA线电压
7	32			15		Phase A Current		Phase A Curr		A相电流			A相电流	
8	32			15		Phase B Current		Phase B Curr		B相电流			B相电流	
9	32			15		Phase C Current		Phase C Curr		C相电流			C相电流	
10	32			15		AC Frequency		AC Frequency		交流频率		交流频率
11	32			15		Total Real Power		Tot Real Power		总有功功率		总有功功率
12	32			15		Phase A Real Power		PH-A Real Power		A相有功功率		A相有功功率
13	32			15		Phase B Real Power		PH-B Real Power		B相有功功率		B相有功功率
14	32			15		Phase C Real Power		PH-C Real Power		C相有功功率		C相有功功率
15	32			15		Total Reactive Power		Tot React Power		总无功功率		总无功功率
16	32			15		Phase A Reactive Power		PH-A React Pwr		A相无功功率		A相无功功率
17	32			15		Phase B Reactive Power		PH-B React Pwr		B相无功功率		B相无功功率
18	32			15		Phase C Reactive Power		PH-C React Pwr		C相无功功率		C相无功功率
19	32			15		Total Apparent Power		Total App Power		总视在功率		总视在功率
20	32			15		Phase A Apparent Power		PH-A App Power		A相视在功率		A相视在功率
21	32			15		Phase B Apparent Power		PH-B App Power		B相视在功率		B相视在功率
22	32			15		Phase C Apparent Power		PH-C App Power		C相视在功率		C相视在功率
23	32			15		Power Factor		Power Factor		功率因数		功率因数
24	32			15		Phase A Power Factor		PH-A Pwr Fact		A相功率因数		A相功率因数
25	32			15		Phase B Power Factor		PH-B Pwr Fact		B相功率因数		B相功率因数
26	32			15		Phase C Power Factor		PH-C Pwr Fact		C相功率因数		C相功率因数
27	32			15		Phase A Current Crest Factor		Ia Crest Factor		Ia波峰因数		Ia波峰因数
28	32			15		Phase B Current Crest Factor		Ib Crest Factor		Ib波峰因数		Ib波峰因数
29	32			15		Phase C Current Crest Factor		Ic Crest Factor		Ic波峰因数		Ic波峰因数
30	32			15		Phase A Current THD		PH-A Curr THD		A相电流THD		A相电流THD
31	32			15		Phase B Current THD		PH-B Curr THD		B相电流THD		B相电流THD
32	32			15		Phase C Current THD		PH-C Curr THD		C相电流THD		C相电流THD
33	32			15		Phase A Voltage THD		PH-A Volt THD		A相电压THD		A相电压THD
34	32			15		Phase B Voltage THD		PH-B Volt THD		B相电压THD		B相电压THD
35	32			15		Phase C Voltage THD		PH-C Volt THD		C相电压THD		C相电压THD
36	32			15		Total Real Energy		Tot Real Energy		总有功能量		总有功能量
37	32			15		Total Reactive Energy		Tot ReactEnergy		总无功能量		总无功能量
38	32			15		Total Apparent Energy		Tot App Energy		总视在能量		总视在能量
39	32			15		Ambient Temperature		Ambient Temp		环境温度		环境温度
40	32			15		Nominal Line Voltage			Nom Line Volt		标称线电压		标称线电压
41	32			15		Nominal Phase Voltage			Nom Phase Volt		标称相电压		标称相电压
42	32			15		Nominal Frequency			Nom Frequency		标称频率		标称频率
43	32			15		Mains Failure Alarm Threshold 1		MFA Threshold 1		电压告警门限1	电压告警门限1
44	32			15		Mains Failure Alarm Threshold 2	MFA Threshold 2		电压告警门限2	电压告警门限2
45	32			15		Voltage Alarm Threshold 1		Volt Alm Trld 1		电压告警门限1		电压告警门限1
46	32			15		Voltage Alarm Threshold 2		Volt Alm Trld 2		电压告警门限2		电压告警门限2
47	32			15		Frequency Alarm Threshold		Freq Alarm Trld		频率告警门限		频率告警门限
48	32			15		High Temperature Limit			High Temp Limit		高温点			高温点
49	32			15		Low Temperature Limit			Low Temp Limit		低温点			低温点
50	32			15		Supervision Fail			Supervise Fail		监控失败		监控失败
51	32			15		High Line Voltage AB			Hi LineVolt AB		高线电压AB		高线电压AB
52		32			15			Very High Line Voltage AB		VHi LineVolt AB		超高线电压AB		超高线电压AB
53		32			15			Low Line Voltage AB			Lo LineVolt AB		低线电压AB		低线电压AB
54		32			15			Very Low Line Voltage AB		VLo LineVolt AB		超低线电压AB		超低线电压AB
55		32			15			High Line Voltage BC			Hi LineVolt BC		高线电压BC		高线电压BC
56		32			15			Very High Line Voltage BC		VHi LineVolt BC		超高线电压BC		超高线电压BC
57		32			15			Low Line Voltage BC			Lo LineVolt BC		低线电压BC		低线电压BC
58		32			15			Very Low Line Voltage BC		VLo LineVolt BC		超低线电压BC		超低线电压BC
59		32			15			High Line Voltage CA			Hi LineVolt CA		高线电压CA		高线电压CA
60		32			15			Very High Line Voltage CA		VHi LineVolt CA		超高线电压CA		超高线电压CA
61		32			15			Low Line Voltage CA			Lo LineVolt CA		低线电压CA		低线电压CA
62		32			15			Very Low Line Voltage CA		VLo LineVolt CA		超低线电压CA		超低线电压CA
63		32			15			High Phase Voltage A			Hi PhaseVolt A		高相电压A			高相电压A
64		32			15			Very High Phase Voltage A		VHi PhaseVolt A		超高相电压A		超高相电压A
65		32			15			Low Phase Voltage A			Lo PhaseVolt A		低相电压A			低相电压A
66		32			15			Very Low Phase Voltage A		VLo PhaseVolt A		超低相电压A		超低相电压A
67		32			15			High Phase Voltage B			Hi PhaseVolt B		高相电压B			高相电压B
68		32			15			Very High Phase Voltage B		VHi PhaseVolt B		超高相电压B		超高相电压B
69		32			15			Low Phase Voltage B			Lo PhaseVolt B		低相电压B			低相电压B
70		32			15			Very Low Phase Voltage B		VLo PhaseVolt B		超低相电压B		超低相电压B
71		32			15			High Phase Voltage C			Hi PhaseVolt C		高相电压C			高相电压C
72		32			15			Very High Phase Voltage C		VHi PhaseVolt C		超高相电压C		超高相电压C
73		32			15			Low Phase Voltage C			Lo PhaseVolt C		低相电压C			低相电压C
74		32			15			Very Low Phase Voltage C		VLo PhaseVolt C		超低相电压C		超低相电压C
75		32			15			Mains Failure				Mains Failure		市电失败			市电失败
76		32			15			Severe Mains Failure			SevereMainsFail		严重市电失败		严重市电失败
77		32			15			High Frequency				High Frequency		高频			高频
78		32			15			Low Frequency				Low Frequency		低频			低频
79		32			15			High Temperature			High Temp		高温			高温
80		32			15			Low Temperature				Low Temperature		低温			低温
81		32			15			SMAC					SMAC			SMAC			SMAC
82		32			15			Supervision Fail			SMAC Fail		SMAC通讯失败		SMAC通讯失败
83		32			15			No					No			否			否
84		32			15			Yes					Yes			是			是
85		32			15			Phase A Mains Failure Counter		PH-A ACFail Cnt	A相故障计数		A相故障计数
86		32			15			Phase B Mains Failure Counter		PH-B ACFail Cnt	B相故障计数		B相故障计数
87		32			15			Phase C Mains Failure Counter		PH-C ACFail Cnt	C相故障计数		C相故障计数
88		32			15			Frequency Failure Counter		Freq Fail Cnt		频率故障计数		频率故障计数
89		32			15			Reset Phase A Mains Fail Counter	Reset A FailCnt		A相故障复位		A相故障复位
90		32			15			Reset Phase B Mains Fail Counter	Reset B FailCnt		B相故障复位		B相故障复位
91		32			15			Reset Phase C Mains Fail Counter	Reset C FailCnt		C相故障复位		C相故障复位
92		32			15			Reset Frequency Counter			Rst Frq FailCnt		频率故障复位		频率故障复位
93		32			15			Current Alarm Threshold			Curr Alm Limit	AC过流告警点		AC过流告警点
94		32			15			Phase A  High Current			PH-A Hi Current		A相过流			A相过流
95		32			15			Phase B  High Current			PH-B Hi Current		B相过流			B相过流
96		32			15			Phase C  High Current			PH-C Hi Current		C相过流			C相过流
97		32			15			State					State			State			State
98		32			15			Off					Off			off			off
99		32			15			On					On			on			on
100	32			15			System Power				System Power		系统功率		系统功率
101	32			15			Total System Power Consumption		Pwr Consumption		功率消耗		功率消耗
102		32			15			Existence State			Existence State		是否存在		是否存在
103		32			15			Existent			Existent		存在			存在
104		32			15			Not Existent			Not Existent		不存在			不存在





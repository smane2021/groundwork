﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			DC Meter Group		DCMeter Group		直流电表组		直流电表组
2		32			15			DC Meter Number		DCMeter Num		直流电表个数		直流电表个数
3		32			15			Communication Fail	Comm Fail		中断状态		中断状态
4		32			15			Existence State		Existence		是否存在		是否存在
5		32			15			Existent		Existent		存在			存在
6		32			15			Not Existent		Not Existent		不存在			不存在

11		32			15			DC Meter Lost		DCMeter Lost		直流电表丢失		直流电表丢失
12		32			15			DC Meter Num Last Time	LastDCMeter Num	上次上电直流电表数量	上次电表数量
13		32			15			Clear DC Meter Lost Alarm	ClrDCMeterLost		清除直流电表丢失告警	清直流电表丢失
14		32			15			Clear			Clear			清除			清除
15		32			15			Total Energy Consumption	TotalEnergy		总电量		总电量

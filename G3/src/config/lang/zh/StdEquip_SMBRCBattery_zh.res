﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Battery Current				Batt Current		电池电流		电池电流
2	32			15			Battery Rating(Ah)			Batt Rating(Ah)		电池容量(Ah)		电池容量(Ah)		
3	32			15			Exceed Current Limit			Exceed Curr Lmt		超过电池限流点		超过电池限流点
4	32			15			Battery					Battery			电池			电池
5	32			15			Over Battery Current			Over Current		电池充电过流		电池充电过流
6	32			15			Battery Capacity (%)			Batt Cap (%)		电池容量(%)		电池容量(%)
7	32			15			Battery Voltage				Batt Voltage		电池电压		电池电压
8	32			15			Low Capacity				Low Capacity		容量低			容量低
9	32			15			On					On			正常			正常
10	32			15			Off					Off			断开			断开
11	32			15			Battery Fuse Voltage			Batt Fuse Volt		电池熔丝电压		电池熔丝电压
12	32			15			Battery Fuse Status			Batt Fuse State		电池熔丝状态		电池熔丝状态
13	32			15			Fuse Alarm				Fuse Alarm		熔丝告警		熔丝告警
14	32			15			SMBRC Battery				SMBRC Battery		SMBRC电池		SMBRC电池
15	32			15			State					State			状态			状态
16	32			15			Off					Off			断开			断开
17	32			15			On					On			闭合			闭合
18	32			15			Switch					Switch			Swich			Swich
19	32			15			Battery Over Current			Batt Over Curr		电池过流		电池过流
20	32			15			Battery Management			Batt Management		参与电池管理		参与电池管理
21	32			15			Yes					Yes			是			是
22	32			15			No					No			否			否
23	32			15			Over Voltage Setpoint			Over Volt Limit		电池过压点		电池过压点
24	32			15			Low Voltage Setpoint			Low Volt Limit		电池欠压点		电池欠压点
25	32			15			Battery Over Voltage			Batt Over Volt		过压			过压
26	32			15			Battery Under Voltage			Batt Under Volt		欠压			欠压
27	32			15			Over Current				Over Current		过流点			过流点
28	32			15			Communication Fail			Comm Fail		通讯中断		通讯中断
29	32			15			Times of Communication Fail				Times Comm Fail		通信失败次数		通信失败次数
44	32			15			Temp Num of Battery			Batt Temp Num	电池用温度路数		电池用温度路数
87	32			15			No					No			否			否	
91	32			15			Battery Temp 1				Batt Temp 1			温度1			温度1
92	32			15			Battery Temp 2				Batt Temp 2			温度2			温度2
93	32			15			Battery Temp 3				Batt Temp 3			温度3			温度3
94	32			15			Battery Temp 4				Batt Temp 4			温度4			温度4
95	32			15			Battery Temp 5				Batt Temp 5			温度5			温度5
96	32			15			Rated Capacity				Rated Capacity		标称容量		标称容量	

97	32			15			Low					Low			低			低
98	32			15			High					High			高			高

100	32			15			Overall Voltage				Overall Volt		总电压			总电压
101	32			15			String Current				String Curr		串电流			串电流
102	32			15			Float Current				Float Curr		浮充电流		浮充电流
103	32			15			Ripple Current				Ripple Curr		纹波电流		纹波电流
104	32			15			Cell Number				Cell Number		电池节数		电池节数
105	32			15			Cell 1 Voltage				Cell 1 Voltage		节1 电压		节1 电压
106	32			15			Cell 2 Voltage				Cell 2 Voltage		节2 电压		节2 电压
107	32			15			Cell 3 Voltage				Cell 3 Voltage		节3 电压		节3 电压
108	32			15			Cell 4 Voltage				Cell 4 Voltage		节4 电压		节4 电压
109	32			15			Cell 5 Voltage				Cell 5 Voltage		节5 电压		节5 电压
110	32			15			Cell 6 Voltage				Cell 6 Voltage		节6 电压		节6 电压
111	32			15			Cell 7 Voltage				Cell 7 Voltage		节7 电压		节7 电压
112	32			15			Cell 8 Voltage				Cell 8 Voltage		节8 电压		节8 电压
113	32			15			Cell 9 Voltage				Cell 9 Voltage		节9 电压		节9 电压
114	32			15			Cell 10 Voltage				Cell 10 Voltage		节10电压		节10电压
115	32			15			Cell 11 Voltage				Cell 11 Voltage		节11电压		节11电压
116	32			15			Cell 12 Voltage				Cell 12 Voltage		节12电压		节12电压
117	32			15			Cell 13 Voltage				Cell 13 Voltage		节13电压		节13电压
118	32			15			Cell 14 Voltage				Cell 14 Voltage		节14电压		节14电压
119	32			15			Cell 15 Voltage				Cell 15 Voltage		节15电压		节15电压
120	32			15			Cell 16 Voltage				Cell 16 Voltage		节16电压		节16电压
121	32			15			Cell 17 Voltage				Cell 17 Voltage		节17电压		节17电压
122	32			15			Cell 18 Voltage				Cell 18 Voltage		节18电压		节18电压
123	32			15			Cell 19 Voltage				Cell 19 Voltage		节19电压		节19电压
124	32			15			Cell 20 Voltage				Cell 20 Voltage		节20电压		节20电压
125	32			15			Cell 21 Voltage				Cell 21 Voltage		节21电压		节21电压
126	32			15			Cell 22 Voltage				Cell 22 Voltage		节22电压		节22电压
127	32			15			Cell 23 Voltage				Cell 23 Voltage		节23电压		节23电压
128	32			15			Cell 24 Voltage				Cell 24 Voltage		节24电压		节24电压
129	32			15			Cell 1 Temperature			Cell 1 Temp		节1 温度		节1 温度
130	32			15			Cell 2 Temperature			Cell 2 Temp		节2 温度		节2 温度
131	32			15			Cell 3 Temperature			Cell 3 Temp		节3 温度		节3 温度
132	32			15			Cell 4 Temperature			Cell 4 Temp		节4 温度		节4 温度
133	32			15			Cell 5 Temperature			Cell 5 Temp		节5 温度		节5 温度
134	32			15			Cell 6 Temperature			Cell 6 Temp		节6 温度		节6 温度
135	32			15			Cell 7 Temperature			Cell 7 Temp		节7 温度		节7 温度
136	32			15			Cell 8 Temperature			Cell 8 Temp		节8 温度		节8 温度
137	32			15			Cell 9 Temperature			Cell 9 Temp		节9 温度		节9 温度
138	32			15			Cell 10 Temperature			Cell 10 Temp		节10温度		节10温度
139	32			15			Cell 11 Temperature			Cell 11 Temp		节11温度		节11温度
140	32			15			Cell 12 Temperature			Cell 12 Temp		节12温度		节12温度
141	32			15			Cell 13 Temperature			Cell 13 Temp		节13温度		节13温度
142	32			15			Cell 14 Temperature			Cell 14 Temp		节14温度		节14温度
143	32			15			Cell 15 Temperature			Cell 15 Temp		节15温度		节15温度
144	32			15			Cell 16 Temperature			Cell 16 Temp		节16温度		节16温度
145	32			15			Cell 17 Temperature			Cell 17 Temp		节17温度		节17温度
146	32			15			Cell 18 Temperature			Cell 18 Temp		节18温度		节18温度
147	32			15			Cell 19 Temperature			Cell 19 Temp		节19温度		节19温度
148	32			15			Cell 20 Temperature			Cell 20 Temp		节20温度		节20温度
149	32			15			Cell 21 Temperature			Cell 21 Temp		节21温度		节21温度
150	32			15			Cell 22 Temperature			Cell 22 Temp		节22温度		节22温度
151	32			15			Cell 23 Temperature			Cell 23 Temp		节23温度		节23温度
152	32			15			Cell 24 Temperature			Cell 24 Temp		节24温度		节24温度

153	32			15			Overall Volt Alarm			Overall VoltAlm	整体电压告警		整体电压告警
154	32			15			String Current Alarm			String Curr Alm		串电流			串电流
155	32			15			Float Current Alarm			Float Curr Alm	浮充电流		浮充电流
156	32			15			Ripple Current Alarm			Ripple Curr Alm	纹波电流		纹波电流
157	32			15			Cell Ambient Alarm			Cell Amb Alm	节环境温度		节环境温度
	
158	32			15			Cell 1 Voltage Alarm			Cell 1 Volt Alm		节1 电压告警			节1 电压告警
159	32			15			Cell 2 Voltage Alarm			Cell 2 Volt Alm		节2 电压告警			节2 电压告警
160	32			15			Cell 3 Voltage Alarm			Cell 3 Volt Alm		节3 电压告警			节3 电压告警
161	32			15			Cell 4 Voltage Alarm			Cell 4 Volt Alm		节4 电压告警			节4 电压告警
162	32			15			Cell 5 Voltage Alarm			Cell 5 Volt Alm		节5 电压告警			节5 电压告警
163	32			15			Cell 6 Voltage Alarm			Cell 6 Volt Alm		节6 电压告警			节6 电压告警
164	32			15			Cell 7 Voltage Alarm			Cell 7 Volt Alm		节7 电压告警			节7 电压告警
165	32			15			Cell 8 Voltage Alarm			Cell 8 Volt Alm		节8 电压告警			节8 电压告警
166	32			15			Cell 9 Voltage Alarm			Cell 9 Volt Alm		节9 电压告警			节9 电压告警
167	32			15			Cell 10 Voltage Alarm			Cell10 Volt Alm		节10电压告警			节10电压告警
168	32			15			Cell 11 Voltage Alarm			Cell11 Volt Alm		节11电压告警			节11电压告警
169	32			15			Cell 12 Voltage Alarm			Cell12 Volt Alm		节12电压告警			节12电压告警
170	32			15			Cell 13 Voltage Alarm			Cell13 Volt Alm		节13电压告警			节13电压告警
171	32			15			Cell 14 Voltage Alarm			Cell14 Volt Alm		节14电压告警			节14电压告警
172	32			15			Cell 15 Voltage Alarm			Cell15 Volt Alm		节15电压告警			节15电压告警
173	32			15			Cell 16 Voltage Alarm			Cell16 Volt Alm		节16电压告警			节16电压告警
174	32			15			Cell 17 Voltage Alarm			Cell17 Volt Alm		节17电压告警			节17电压告警
175	32			15			Cell 18 Voltage Alarm			Cell18 Volt Alm		节18电压告警			节18电压告警
176	32			15			Cell 19 Voltage Alarm			Cell19 Volt Alm		节19电压告警			节19电压告警
177	32			15			Cell 20 Voltage Alarm			Cell20 Volt Alm		节20电压告警			节20电压告警
178	32			15			Cell 21 Voltage Alarm			Cell21 Volt Alm		节21电压告警			节21电压告警
179	32			15			Cell 22 Voltage Alarm			Cell22 Volt Alm		节22电压告警			节22电压告警
180	32			15			Cell 23 Volt Alarm			Cell23 Volt Alm		节23电压告警			节23电压告警
181	32			15			Cell 24 Volt Alarm			Cell24 Volt Alm		节24电压告警			节24电压告警																				
182	32			15			Cell 1 Temperature Alarm		Cell 1 Temp Alm		节1 温度告警			节1 温度告警
183	32			15			Cell 2 Temperature Alarm		Cell 2 Temp Alm		节2 温度告警			节2 温度告警
184	32			15			Cell 3 Temperature Alarm		Cell 3 Temp Alm		节3 温度告警			节3 温度告警
185	32			15			Cell 4 Temperature Alarm		Cell 4 Temp Alm		节4 温度告警			节4 温度告警
186	32			15			Cell 5 Temperature Alarm		Cell 5 Temp Alm		节5 温度告警			节5 温度告警
187	32			15			Cell 6 Temperature Alarm		Cell 6 Temp Alm		节6 温度告警			节6 温度告警
188	32			15			Cell 7 Temperature Alarm		Cell 7 Temp Alm		节7 温度告警			节7 温度告警
189	32			15			Cell 8 Temperature Alarm		Cell 8 Temp Alm		节8 温度告警			节8 温度告警
190	32			15			Cell 9 Temperature Alarm		Cell 9 Temp Alm		节9 温度告警			节9 温度告警
191	32			15			Cell 10 Temperature Alarm		Cell10 Temp Alm		节10温度告警			节10温度告警
192	32			15			Cell 11 Temperature Alarm		Cell11 Temp Alm		节11温度告警			节11温度告警
193	32			15			Cell 12 Temperature Alarm		Cell12 Temp Alm		节12温度告警			节12温度告警
194	32			15			Cell 13 Temperature Alarm		Cell13 Temp Alm		节13温度告警			节13温度告警
195	32			15			Cell 14 Temperature Alarm		Cell14 Temp Alm		节14温度告警			节14温度告警
196	32			15			Cell 15 Temperature Alarm		Cell15 Temp Alm		节15温度告警			节15温度告警
197	32			15			Cell 16 Temperature Alarm		Cell16 Temp Alm		节16温度告警			节16温度告警
198	32			15			Cell 17 Temperature Alarm		Cell17 Temp Alm		节17温度告警			节17温度告警
199	32			15			Cell 18 Temperature Alarm		Cell18 Temp Alm		节18温度告警			节18温度告警
200	32			15			Cell 19 Temperature Alarm		Cell19 Temp Alm		节19温度告警			节19温度告警
201	32			15			Cell 20 Temperature Alarm		Cell20 Temp Alm		节20温度告警			节20温度告警
202	32			15			Cell 21 Temperature Alarm		Cell21 Temp Alm		节21温度告警			节21温度告警
203	32			15			Cell 22 Temperature Alarm		Cell22 Temp Alm		节22温度告警			节22温度告警
204	32			15			Cell 23 Temperature Alarm		Cell23 Temp Alm		节23温度告警			节23温度告警
205	32			15			Cell 24 Temperature Alarm		Cell24 Temp Alm		节24温度告警			节24温度告警

206	32			15			Cell 1 Resistance Alarm			Cell 1  Res Alm		节1 阻抗告警			节1 阻抗告警
207	32			15			Cell 2 Resistance Alarm			Cell 2  Res Alm		节2 阻抗告警			节2 阻抗告警
208	32			15			Cell 3 Resistance Alarm			Cell 3  Res Alm		节3 阻抗告警			节3 阻抗告警
209	32			15			Cell 4 Resistance Alarm			Cell 4  Res Alm		节4 阻抗告警			节4 阻抗告警
210	32			15			Cell 5 Resistance Alarm			Cell 5  Res Alm		节5 阻抗告警			节5 阻抗告警
211	32			15			Cell 6 Resistance Alarm			Cell 6  Res Alm		节6 阻抗告警			节6 阻抗告警
212	32			15			Cell 7 Resistance Alarm			Cell 7  Res Alm		节7 阻抗告警			节7 阻抗告警
213	32			15			Cell 8 Resistance Alarm			Cell 8  Res Alm		节8 阻抗告警			节8 阻抗告警
214	32			15			Cell 9 Resistance Alarm			Cell 9  Res Alm		节9 阻抗告警			节9 阻抗告警
215	32			15			Cell 10 Resistance Alarm		Cell 10 Res Alm		节10阻抗告警			节10阻抗告警
216	32			15			Cell 11 Resistance Alarm		Cell 11 Res Alm		节11阻抗告警			节11阻抗告警
217	32			15			Cell 12 Resistance Alarm		Cell 12 Res Alm		节12阻抗告警			节12阻抗告警
218	32			15			Cell 13 Resistance Alarm		Cell 13 Res Alm		节13阻抗告警			节13阻抗告警
219	32			15			Cell 14 Resistance Alarm		Cell 14 Res Alm		节14阻抗告警			节14阻抗告警
220	32			15			Cell 15 Resistance Alarm		Cell 15 Res Alm		节15阻抗告警			节15阻抗告警
221	32			15			Cell 16 Resistance Alarm		Cell 16 Res Alm		节16阻抗告警			节16阻抗告警
222	32			15			Cell 17 Resistance Alarm		Cell 17 Res Alm		节17阻抗告警			节17阻抗告警
223	32			15			Cell 18 Resistance Alarm		Cell 18 Res Alm		节18阻抗告警			节18阻抗告警
224	32			15			Cell 19 Resistance Alarm		Cell 19 Res Alm		节19阻抗告警			节19阻抗告警
225	32			15			Cell 20 Resistance Alarm		Cell 20 Res Alm		节20阻抗告警			节20阻抗告警
226	32			15			Cell 21 Resistance Alarm		Cell 21 Res Alm		节21阻抗告警			节21阻抗告警
227	32			15			Cell 22 Resistance Alarm		Cell 22 Res Alm		节22阻抗告警			节22阻抗告警
228	32			15			Cell 23 Resistance Alarm		Cell 23 Res Alm		节23阻抗告警			节23阻抗告警
229	32			15			Cell 24 Resistance Alarm		Cell 24 Res Alm		节24阻抗告警			节24阻抗告警


230	32			15			Cell 1 Intercell Alarm			Cell 1 InterAlm		节1 内阻告警			节1 内阻告警
231	32			15			Cell 2 Intercell Alarm			Cell 2 InterAlm		节2 内阻告警			节2 内阻告警
232	32			15			Cell 3 Intercell Alarm			Cell 3 InterAlm		节3 内阻告警			节3 内阻告警
233	32			15			Cell 4 Intercell Alarm			Cell 4 InterAlm		节4 内阻告警			节4 内阻告警
234	32			15			Cell 5 Intercell Alarm			Cell 5 InterAlm		节5 内阻告警			节5 内阻告警
235	32			15			Cell 6 Intercell Alarm			Cell 6 InterAlm		节6 内阻告警			节6 内阻告警
236	32			15			Cell 7 Intercell Alarm			Cell 7 InterAlm		节7 内阻告警			节7 内阻告警
237	32			15			Cell 8 Intercell Alarm			Cell 8 InterAlm		节8 内阻告警			节8 内阻告警
238	32			15			Cell 9 Intercell Alarm			Cell 9 InterAlm		节9 内阻告警			节9 内阻告警
239	32			15			Cell 10 Intercell Alarm			Cell10 InterAlm		节10内阻告警			节10内阻告警
240	32			15			Cell 11 Intercell Alarm			Cell11 InterAlm		节11内阻告警			节11内阻告警
241	32			15			Cell 12 Intercell Alarm			Cell12 InterAlm		节12内阻告警			节12内阻告警
242	32			15			Cell 13 Intercell Alarm			Cell13 InterAlm		节13内阻告警			节13内阻告警
243	32			15			Cell 14 Intercell Alarm			Cell14 InterAlm		节14内阻告警			节14内阻告警
244	32			15			Cell 15 Intercell Alarm			Cell15 InterAlm		节15内阻告警			节15内阻告警
245	32			15			Cell 16 Intercell Alarm			Cell16 InterAlm		节16内阻告警			节16内阻告警
246	32			15			Cell 17 Intercell Alarm			Cell17 InterAlm		节17内阻告警			节17内阻告警
247	32			15			Cell 18 Intercell Alarm			Cell18 InterAlm		节18内阻告警			节18内阻告警
248	32			15			Cell 19 Intercell Alarm			Cell19 InterAlm		节19内阻告警			节19内阻告警
249	32			15			Cell 20 Intercell Alarm			Cell20 InterAlm		节20内阻告警			节20内阻告警
250	32			15			Cell 21 Intercell Alarm			Cell21 InterAlm		节21内阻告警			节21内阻告警
251	32			15			Cell 22 Intercell Alarm			Cell22 InterAlm		节22内阻告警			节22内阻告警
252	32			15			Cell 23 Intercell Alarm			Cell23 InterAlm		节23内阻告警			节23内阻告警
253	32			15			Cell 24 Intercell Alarm			Cell24 InterAlm		节24内阻告警			节24内阻告警



254	32			15			Cell1 Ambient Alm		Cell1 Amb Alm			节1 内环告警			节1 内环告警
255	32			15			Cell2 Ambient Alm		Cell2 Amb Alm			节2 内环告警			节2 内环告警
256	32			15			Cell3 Ambient Alm		Cell3 Amb Alm			节3 内环告警			节3 内环告警
257	32			15			Cell4 Ambient Alm		Cell4 Amb Alm			节4 内环告警			节4 内环告警
258	32			15			Cell5 Ambient Alm		Cell5 Amb Alm			节5 内环告警			节5 内环告警
259	32			15			Cell6 Ambient Alm			Cell6 Amb Alm			节6 内环告警			节6 内环告警
260	32			15			Cell7 Ambient Alm			Cell7 Amb Alm			节7 内环告警			节7 内环告警
261	32			15			Cell8 Ambient Alm		Cell8 Amb Alm			节8 内环告警			节8 内环告警
262	32			15			Cell9 Ambient Alm			Cell9 Amb Alm			节9 内环告警			节9 内环告警
263	32			15			Cell10 Ambient Alm			Cell10 Amb Alm		节10内环告警			节10内环告警
264	32			15			Cell11 Ambient Alm			Cell11 Amb Alm	节11内环告警			节11内环告警
265	32			15			Cell12 Ambient Alm			Cell12 Amb Alm	节12内环告警			节12内环告警
266	32			15			Cell13 Ambient Alm			Cell13 Amb Alm		节13内环告警			节13内环告警
267	32			15			Cell14 Ambient Alm			Cell14 Amb Alm	节14内环告警			节14内环告警
268	32			15			Cell15 Ambient Alm			Cell15 Amb Alm		节15内环告警			节15内环告警
269	32			15			Cell16 Ambient Alm			Cell16 Amb Alm		节16内环告警			节16内环告警
270	32			15			Cell17 Ambient Alm			Cell17 Amb Alm	节17内环告警			节17内环告警
271	32			15			Cell18 Ambient Alm		Cell18 Amb Alm		节18内环告警			节18内环告警
272	32			15			Cell19 Ambient Alm			Cell19 Amb Alm		节19内环告警			节19内环告警
273	32			15			Cell20 Ambient Alm			Cell20 Amb Alm		节20内环告警			节20内环告警
274	32			15			Cell21 Ambient Alm			Cell21 Amb Alm		节21内环告警			节21内环告警
275	32			15			Cell22 Ambient Alm			Cell22 Amb Alm		节22内环告警			节22内环告警
276	32			15			Cell23 Ambient Alm			Cell23 Amb Alm		节23内环告警			节23内环告警
277	32			15			Cell24 Ambient Alm			Cell24 Amb Alm		节24内环告警			节24内环告警


278	32			15			String Addr Value			String Addr Val		本串地址			本串地址
279	32			15			String Seq Num				String Seq Num			本串序列			本串序列

280	32			15			Cell Volt Low Alarm				Volt Low Alm		节电压低告警			节电压低告警
281	32			15			Cell Temp Low Alarm			Temp Low Alm		节温度低告警			节温度低告警
282	32			15			Cell Resist Low Alarm			Resist Low Alm		节阻抗低告警			节阻抗低告警
283	32			15			Cell Inter Low Alarm				Inter Low Alm		节内阻低告警			节内阻低告警
284	32			15			Cell Ambient Low Alarm			Amb Low Alm		节环温低告警			节环温低告警
285	32			15			Ambient Temperature Value		Amb Temp Value		环境温度		环境温度

290	32			15			None						None				无				无
291	32			15			Alarm						Alarm				告警				告警

292	32			15			Overall Voltage High				Overall Volt Hi		总电压高			总电压高
293	32			15			Overall Voltage Low			Overall Volt Lo		总电压低			总电压低
294	32			15			String Current High				String Curr Hi		串电流大			串电流大
295	32			15			String Current Low			String Curr Lo			串电流小			串电流小
296	32			15			Float Current High				Float Curr Hi			浮充电流大			浮充电流大
297	32			15			Float Current Low				Float Curr Lo			浮充电流小			浮充电流小
298	32			15			Ripple Current High			Ripple Curr Hi			纹波电流大			纹波电流大
299	32			15			Ripple Current Low			Ripple Curr Lo			纹波电流小			纹波电流小


#Test Resist
300	32			15			Test Resistance 1				Test Resist 1			测试节阻抗1			测试节阻抗1
301	32			15			Test Resistance 2				Test Resist 2			测试节阻抗2			测试节阻抗2
302	32			15			Test Resistance 3				Test Resist 3			测试节阻抗3			测试节阻抗3
303	32			15			Test Resistance 4				Test Resist 4			测试节阻抗4			测试节阻抗4
304	32			15			Test Resistance 5				Test Resist 5			测试节阻抗5			测试节阻抗5
305	32			15			Test Resistance 6				Test Resist 6			测试节阻抗6			测试节阻抗6
307	32			15			Test Resistance 7				Test Resist 7			测试节阻抗7			测试节阻抗7
308	32			15			Test Resistance 8				Test Resist 8			测试节阻抗8			测试节阻抗8
309	32			15			Test Resistance 9				Test Resist 9			测试节阻抗9			测试节阻抗9
310	32			15			Test Resistance 10				Test Resist 10			测试节阻抗10			测试节阻抗10
311	32			15			Test Resistance 11				Test Resist 11			测试节阻抗11			测试节阻抗11
312	32			15			Test Resistance 12				Test Resist 12			测试节阻抗12			测试节阻抗12
313	32			15			Test Resistance 13				Test Resist 13			测试节阻抗13			测试节阻抗13
314	32			15			Test Resistance 14				Test Resist 14			测试节阻抗14			测试节阻抗14
315	32			15			Test Resistance 15				Test Resist 15			测试节阻抗15			测试节阻抗15
316	32			15			Test Resistance 16				Test Resist 16			测试节阻抗16			测试节阻抗16
317	32			15			Test Resistance 17				Test Resist 17			测试节阻抗17			测试节阻抗17
318	32			15			Test Resistance 18				Test Resist 18			测试节阻抗18			测试节阻抗18
319	32			15			Test Resistance 19				Test Resist 19			测试节阻抗19			测试节阻抗19
320	32			15			Test Resistance 20				Test Resist 20			测试节阻抗20			测试节阻抗20
321	32			15			Test Resistance 21				Test Resist 21			测试节阻抗21			测试节阻抗21
322	32			15			Test Resistance 22				Test Resist 22			测试节阻抗22			测试节阻抗22
323	32			15			Test Resistance 23				Test Resist 23			测试节阻抗23			测试节阻抗23
324	32			15			Test Resistance 24				Test Resist 24			测试节阻抗24			测试节阻抗24

325	32			15			Test Intercell 1				Test Intercel1			测试节内阻1			测试节内阻1
326	32			15			Test Intercell 2				Test Intercel2			测试节内阻2			测试节内阻2
327	32			15			Test Intercell 3				Test Intercel3			测试节内阻3			测试节内阻3
328	32			15			Test Intercell 4				Test Intercel4			测试节内阻4			测试节内阻4
329	32			15			Test Intercell 5				Test Intercel5			测试节内阻5			测试节内阻5
330	32			15			Test Intercell 6				Test Intercel6			测试节内阻6			测试节内阻6
331	32			15			Test Intercell 7				Test Intercel7			测试节内阻7			测试节内阻7
332	32			15			Test Intercell 8				Test Intercel8			测试节内阻8			测试节内阻8
333	32			15			Test Intercell 9				Test Intercel9			测试节内阻9			测试节内阻9
334	32			15			Test Intercell 10				Test Intercel10			测试节内阻10			测试节内阻10
335	32			15			Test Intercell 11				Test Intercel11			测试节内阻11			测试节内阻11
336	32			15			Test Intercell 12				Test Intercel12			测试节内阻12			测试节内阻12
337	32			15			Test Intercell 13				Test Intercel13			测试节内阻13			测试节内阻13
338	32			15			Test Intercell 14				Test Intercel14			测试节内阻14			测试节内阻14
339	32			15			Test Intercell 15				Test Intercel15			测试节内阻15			测试节内阻15
340	32			15			Test Intercell 16				Test Intercel16			测试节内阻16			测试节内阻16
341	32			15			Test Intercell 17				Test Intercel17			测试节内阻17			测试节内阻17
342	32			15			Test Intercell 18				Test Intercel18			测试节内阻18			测试节内阻18
343	32			15			Test Intercell 19				Test Intercel19			测试节内阻19			测试节内阻19
344	32			15			Test Intercell 20				Test Intercel20			测试节内阻20			测试节内阻20
345	32			15			Test Intercell 21				Test Intercel21			测试节内阻21			测试节内阻21
346	32			15			Test Intercell 22				Test Intercel22			测试节内阻22			测试节内阻22
347	32			15			Test Intercell 23				Test Intercel23			测试节内阻23			测试节内阻23
348	32			15			Test Intercell 24				Test Intercel24			测试节内阻24			测试节内阻24
##//2011/03/02杨工 要求将告警区分高低
349	32			15			Cell High Voltage Alarm				Cell HiVolt Alm			节电压高告警			节电压高告警
350	32			15			Cell High Cell Temperature Alarm		Cell HiTemp Alm			节温度高告警			节温度高告警
351	32			15			Cell High Resistance Alarm			Cell HiRes Alm			节阻抗高告警			节阻抗高告警
352	32			15			Cell High Intercell Resist Alarm		Inter HiRes Alm			节内阻高告警			节内阻高告警
353	32			15			Cell High Ambient Temp Alarm			Cell HiAmb Alm			节环温高告警			节环温高告警
354	32			15			Temperature 1 Not Used				Temp1 Not Used			节温度 1 未用			节温度 1 未用
355	32			15			Temperature 2 Not Used				Temp2 Not Used			节温度 2 未用			节温度 2 未用
356	32			15			Temperature 3 Not Used				Temp3 Not Used			节温度 3 未用			节温度 3 未用
357	32			15			Temperature 4 Not Used				Temp4 Not Used			节温度 4 未用			节温度 4 未用
358	32			15			Temperature 5 Not Used				Temp5 Not Used			节温度 5 未用			节温度 5 未用
359	32			15			Temperature 6 Not Used				Temp6 Not Used			节温度 6 未用			节温度 6 未用
360	32			15			Temperature 7 Not Used				Temp7 Not Used			节温度 7 未用			节温度 7 未用
361	32			15			Temperature 8 Not Used				Temp8 Not Used			节温度 8 未用			节温度 8 未用
362	32			15			Temperature 9 Not Used				Temp9 Not Used			节温度 9 未用			节温度 9 未用
363	32			15			Temperature 10 Not Used				Temp10 Not Used		节温度 10 未用			节温度 10 未用
364	32			15			Temperature 11 Not Used				Temp11 Not Used		节温度 11 未用			节温度 11 未用
365	32			15			Temperature 12 Not Used				Temp12 Not Used		节温度 12 未用			节温度 12 未用
366	32			15			Temperature 13 Not Used				Temp13 Not Used		节温度 13 未用			节温度 13 未用
367	32			15			Temperature 14 Not Used				Temp14 Not Used		节温度 14 未用			节温度 14 未用
368	32			15			Temperature 15 Not Used				Temp15 Not Used		节温度 15 未用			节温度 15 未用
369	32			15			Temperature 16 Not Used				Temp16 Not Used		节温度 16 未用			节温度 16 未用
370	32			15			Temperature 17 Not Used				Temp17 Not Used		节温度 17 未用			节温度 17 未用
371	32			15			Temperature 18 Not Used				Temp18 Not Used		节温度 18 未用			节温度 18 未用
372	32			15			Temperature 19 Not Used				Temp19 Not Used		节温度 19 未用			节温度 19 未用
373	32			15			Temperature 20 Not Used				Temp20 Not Used		节温度 20 未用			节温度 20 未用
374	32			15			Temperature 21 Not Used				Temp21 Not Used		节温度 21 未用			节温度 21 未用
375	32			15			Temperature 22 Not Used				Temp22 Not Used		节温度 22 未用			节温度 22 未用
376	32			15			Temperature 23 Not Used				Temp23 Not Used		节温度 23 未用			节温度 23 未用
377	32			15			Temperature 24 Not Used				Temp24 Not Used		节温度 24 未用			节温度 24 未用


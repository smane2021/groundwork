﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name 
    
[LOCALE_LANGUAGE]
zh



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15		AC Distribution Number		AC Distri Num		交流屏数		交流屏数
2		32			15			LargeDU AC Distribution Group	LargeDUACDistGr		LargDU交流屏组		交流屏组
3		32			15			Over Voltage Limit		Over Volt Limit		输入过压告警点	输入过压点
4		32			15			Under Voltage Limit		Under Volt Lmt		输入欠压告警点	输入欠压点
5		32			15			Phase Failure Voltage		Phase Fail Volt	输入缺相告警点	输入缺相点
6		32			15			Over Frequency Limit		Over Freq Limit		频率过高告警点	频率过高点
7		32			15			Under Frequency Limit		Under Freq Lmt		频率过低告警点	频率过低点
8		32			15			Mains Failure			Mains Failure		交流停电		交流停电         
9		32			15			Noraml				Normal			正常			正常
10		32			15			Alarm				Alarm			告警			告警
11		32			15			Mains Failure			Mains Failure		交流停电		交流停电
12		32			15			Existence State			Existence State		是否存在		是否存在
13		32			15			Existent			Existent		存在			存在
14		32			15			Not Existent			Not Existent		不存在			不存在



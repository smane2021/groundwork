﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Bus Bar Voltage				Bus Bar Voltage		母排电压		母排电压
2	32			15			Shunt 1					Shunt 1			分流器电流1		分流器电流1	
3	32			15			Shunt 2					Shunt 2			分流器电流2		分流器电流2	
4	32			15			Shunt 3					Shunt 3			分流器电流3		分流器电流3	
5	32			15			Shunt 4					Shunt 4			分流器电流4		分流器电流4	
6	32			15			Shunt 5					Shunt 5			分流器电流5		分流器电流5	
7	32			15			Shunt 6					Shunt 6			分流器电流6		分流器电流6	
8	32			15			Shunt 7					Shunt 7			分流器电流7		分流器电流7	
9	32			15			Shunt 8					Shunt 8			分流器电流8		分流器电流8	
10	32			15			Shunt 9					Shunt 9			分流器电流9		分流器电流9	
11	32			15			Shunt 10				Shunt 10		分流器电流10		分流器电流10

12	32			15			Tran1(0-20mA)			Tran1(0-20mA)		传感器1(0-20mA)		传感器1(0-20mA)
13	32			15			Tran2(0-20mA)			Tran2(0-20mA)		传感器2(0-20mA)		传感器2(0-20mA)
14	32			15			Tran3(0-20mA)			Tran3(0-20mA)		传感器3(0-20mA)		传感器3(0-20mA)
15	32			15			Tran4(0-20mA)			Tran4(0-20mA)		传感器4(0-20mA)		传感器4(0-20mA)
16	32			15			Tran5(0-20mA)			Tran5(0-20mA)		传感器5(0-20mA)		传感器5(0-20mA)
17	32			15			Tran6(0-20mA)			Tran6(0-20mA)		传感器6(0-20mA)		传感器6(0-20mA)
18	32			15			Tran7(0-20mA)			Tran7(0-20mA)		传感器7(0-20mA)		传感器7(0-20mA)
19	32			15			Tran8(0-20mA)			Tran8(0-20mA)		传感器8(0-20mA)		传感器8(0-20mA)
20	32			15			Tran9(0-20mA)			Tran9(0-20mA)		传感器9(0-20mA)		传感器9(0-20mA)
21	32			15			Tran10(0-20mA)			Tran10(0-20mA)		传感器10(0-20mA)	传感器10(0-20mA)
22	32			15			Tran1(0-10V)				Tran1(0-10V)		传感器1(0-10V)		传感器1(0-10V)
23	32			15			Tran2(0-10V)				Tran2(0-10V)		传感器2(0-10V)		传感器2(0-10V)
24	32			15			Tran3(0-10V)				Tran3(0-10V)		传感器3(0-10V)		传感器3(0-10V)
25	32			15			Tran4(0-10V)				Tran4(0-10V)		传感器4(0-10V)		传感器4(0-10V)
26	32			15			Tran5(0-10V)				Tran5(0-10V)		传感器5(0-10V)		传感器5(0-10V)
27	32			15			Tran6(0-10V)				Tran6(0-10V)		传感器6(0-10V)		传感器6(0-10V)
28	32			15			Tran7(0-10V)				Tran7(0-10V)		传感器7(0-10V)		传感器7(0-10V)
29	32			15			Tran8(0-10V)				Tran8(0-10V)		传感器8(0-10V)		传感器8(0-10V)
30	32			15			Tran9(0-10V)				Tran9(0-10V)		传感器9(0-10V)		传感器9(0-10V)
31	32			15			Tran10(0-10V)				Tran10(0-10V)		传感器10(0-10V)		传感器10(0-10V)

32	32			15			Temperature 1				Temperature 1		温度1			温度1
33	32			15			Temperature 2				Temperature 2		温度2			温度2
34	32			15			Temperature 3				Temperature 3		温度3			温度3
35	32			15			Temperature 4				Temperature 4		温度4			温度4
36	32			15			Temperature 5				Temperature 5		温度5			温度5
37	32			15			Temperature 6				Temperature 6		温度6			温度6
38	32			15			Temperature 7				Temperature 7		温度7			温度7
39	32			15			Temperature 8				Temperature 8		温度8			温度8
40	32			15			Temperature 9				Temperature 9		温度9			温度9
41	32			15			Temperature 10				Temperature 10		温度10			温度10

42	32			15			Voltage1			Voltage1	电压1		电压1
43	32			15			Voltage2			Voltage2	电压2		电压2
44	32			15			Voltage3			Voltage3	电压3		电压3
45	32			15			Voltage4			Voltage4	电压4		电压4
46	32			15			Voltage5			Voltage5	电压5		电压5
47	32			15			Voltage6			Voltage6	电压6		电压6
48	32			15			Voltage7			Voltage7	电压7		电压7
49	32			15			Voltage8			Voltage8	电压8		电压8
50	32			15			Voltage9			Voltage9	电压9		电压9
51	32			15			Voltage10			Voltage10		电压10		电压10

52	32			15			Bus Bar Volt				Bus Bar Volt		母排电压		母排电压
53	32			15			Version No.				Version No.		版本号			版本号
54	32			15			Serial No. High				Serial No. High		序列号高		序列号高
55	32			15			Serial No. Low				Serial No. Low		序列号低		序列号低
56	32			15			Bar Code 1				Bar Code 1		条码1			条码1
57	32			15			Bar Code 2				Bar Code 2		条码2			条码2
58	32			15			Bar Code 3				Bar Code 3		条码3			条码3
59	32			15			Bar Code 4				Bar Code 4		条码4			条码4
60	32			15			Comm OK					Comm OK			通讯正常		通讯正常
61	32			15			Communication Fail			Comm Fail		通讯中断		通讯中断
62	32			15			Existent				Existent		存在			存在
63	32			15			Not Existent				Not Existent		不存在			不存在
64	32			15			Shunt 1 Voltage					Shunt1 Voltage			分流器1电压		分流器1电压
65	32			15			Shunt 1 Current					Shunt1 Current			分流器1电流		分流器1电流
66	32			15			Shunt 2 Voltage					Shunt2 Voltage			分流器2电压		分流器2电压
67	32			15			Shunt 2 Current					Shunt2 Current			分流器2电流		分流器2电流
68	32			15			Shunt 3 Voltage					Shunt3 Voltage			分流器3电压		分流器3电压
69	32			15			Shunt 3 Current					Shunt3 Current			分流器3电流		分流器3电流
70	32			15			Shunt 4 Voltage					Shunt4 Voltage			分流器4电压		分流器4电压
71	32			15			Shunt 4 Current					Shunt4 Current			分流器4电流		分流器4电流
72	32			15			Shunt 5 Voltage					Shunt5 Voltage			分流器5电压		分流器5电压
73	32			15			Shunt 5 Current					Shunt5 Current			分流器5电流		分流器5电流
74	32			15			Shunt 6 Voltage					Shunt6 Voltage			分流器6电压		分流器6电压
75	32			15			Shunt 6 Current					Shunt6 Current			分流器6电流		分流器6电流
76	32			15			Shunt 7 Voltage					Shunt7 Voltage			分流器7电压		分流器7电压
77	32			15			Shunt 7 Current					Shunt7 Current			分流器7电流		分流器7电流
78	32			15			Shunt 8 Voltage					Shunt8 Voltage			分流器8电压		分流器8电压
79	32			15			Shunt 8 Current					Shunt8 Current			分流器8电流		分流器8电流
80	32			15			Shunt 9 Voltage					Shunt9 Voltage			分流器9电压		分流器9电压
81	32			15			Shunt 9 Current					Shunt9 Current			分流器9电流		分流器9电流
82	32			15			Shunt 10 Voltage				Shunt10 Voltage			分流器10电压		分流器10电压
83	32			15			Shunt 10 Current				Shunt10 Current			分流器10电流		分流器10电流
84	32			15			Shunt 1						Shunt 1				分流器1			分流器1	
85	32			15			Shunt 2						Shunt 2				分流器2			分流器2	
86	32			15			Shunt 3						Shunt 3				分流器3			分流器3	
87	32			15			Shunt 4						Shunt 4				分流器4			分流器4	
88	32			15			Shunt 5						Shunt 5				分流器5			分流器5	
89	32			15			Shunt 6						Shunt 6				分流器6			分流器6	
90	32			15			Shunt 7						Shunt 7				分流器7			分流器7	
91	32			15			Shunt 8						Shunt 8				分流器8			分流器8	
92	32			15			Shunt 9						Shunt 9				分流器9			分流器9	
93	32			15			Shunt 10					Shunt 10			分流器10		分流器10
94	32			15			Disabled					Disabled			禁止			禁止																														
95	32			15			Enabled						Enabled				使能			使能																														
96	32			15			Communication Fail				Comm Fail			通讯中断		通讯中断
97	32			15			Existence State					Existence State			是否存在		是否存在
98	32			15			SMDUE1						SMDUE1				SMDUE1			SMDUE1
99	32			15			SMDUE2						SMDUE2				SMDUE2			SMDUE2
100	32			15			SMDUE3						SMDUE3				SMDUE3			SMDUE3
101	32			15			SMDUE4						SMDUE4				SMDUE4			SMDUE4
102	32			15			SMDUE5						SMDUE5				SMDUE5			SMDUE5
103	32			15			SMDUE6						SMDUE6				SMDUE6			SMDUE6
104	32			15			SMDUE7						SMDUE7				SMDUE7			SMDUE7
105	32			15			SMDUE8						SMDUE8				SMDUE8			SMDUE8
106	32			15			All SMDUE Comm Fail				AllSMCommFail			都通讯中断		都通讯中断
107	32			15			ESNA Fuse Alarm Mode				ESNA Fuse Alarm Mode			ESNA 熔丝告警模式			ESNA 熔丝告警模式
108	32			15			Normal Mode				Normal Mode			正常模式			正常模式
109	32			15		Fuse1 Alarm Mode		Fuse1 Mode		熔丝1告警模式			熔丝1告警模式	
110	32			15		Fuse2 Alarm Mode		Fuse2 Mode		熔丝2告警模式			熔丝2告警模式	
111	32			15		Fuse3 Alarm Mode		Fuse3 Mode		熔丝3告警模式			熔丝3告警模式	
112	32			15		Fuse4 Alarm Mode		Fuse4 Mode		熔丝4告警模式			熔丝4告警模式	
113	32			15		Fuse5 Alarm Mode		Fuse5 Mode		熔丝5告警模式			熔丝5告警模式	
114	32			15		Fuse6 Alarm Mode		Fuse6 Mode		熔丝6告警模式			熔丝6告警模式	
115	32			15		Fuse7 Alarm Mode		Fuse7 Mode		熔丝7告警模式			熔丝7告警模式	
116	32			15		Fuse8 Alarm Mode		Fuse8 Mode		熔丝8告警模式			熔丝8告警模式	
117	32			15		Fuse9 Alarm Mode		Fuse9 Mode		熔丝9告警模式			熔丝9告警模式	
118	32			15		Fuse10 Alarm Mode		Fuse10 Mode		熔丝10告警模式			熔丝10告警模式	

120	32			15			Current1 Break Value				Curr1 Brk Val		电流1电流告警阈值		电流1告警阈值																														
121	32			15			Current2 Break Value				Curr2 Brk Val		电流2电流告警阈值		电流2告警阈值																														
122	32			15			Current3 Break Value				Curr3 Brk Val		电流3电流告警阈值		电流3告警阈值																														
123	32			15			Current4 Break Value				Curr4 Brk Val		电流4电流告警阈值		电流4告警阈值																														
124	32			15			Current5 Break Value				Curr5 Brk Val		电流5电流告警阈值		电流5告警阈值																														
125	32			15			Current6 Break Value				Curr6 Brk Val		电流6电流告警阈值		电流6告警阈值																														
126	32			15			Current7 Break Value				Curr7 Brk Val		电流7电流告警阈值		电流7告警阈值																														
127	32			15			Current8 Break Value				Curr8 Brk Val		电流8电流告警阈值		电流8告警阈值																														
128	32			15			Current9 Break Value				Curr9 Brk Val		电流9电流告警阈值		电流9告警阈值																														
129	32			15			Current10 Break Value				Curr10 Brk Val		电流10电流告警阈值		电流10告警阈值																														

130	32			15			Current1 High 1 Current Limit		Curr1 Hi 1 Lmt		电流1过流点		电流1过电流点																															
131	32			15			Current1 High 2 Current Limit		Curr1 Hi 2 Lmt		电流1过过电流点		电流1过过流点																															
132	32			15			Current2 High 1 Current Limit		Curr2 Hi 1 Lmt		电流2过流点		电流2过流点																															
133	32			15			Current2 High 2 Current Limit		Curr2 Hi 2 Lmt		电流2过过流点		电流2过过流点																															
134	32			15			Current3 High 1 Current Limit		Curr3 Hi 1 Lmt		电流3过流点		电流3过流点																															
135	32			15			Current3 High 2 Current Limit		Curr3 Hi 2 Lmt		电流3过过流点		电流3过过流点																															
136	32			15			Current4 High 1 Current Limit		Curr4 Hi 1 Lmt		电流4过流点		电流4过流点																															
137	32			15			Current4 High 2 Current Limit		Curr4 Hi 2 Lmt		电流4过过流点		电流4过过流点																															
138	32			15			Current5 High 1 Current Limit		Curr5 Hi 1 Lmt		电流5过流点		电流5过流点																															
139	32			15			Current5 High 2 Current Limit		Curr5 Hi 2 Lmt		电流5过过流点		电流5过过流点																															
140	32			15			Current6 High 1 Current Limit		Curr6 Hi 1 Lmt		电流6过流点		电流6过流点																															
141	32			15			Current6 High 2 Current Limit		Curr6 Hi 2 Lmt		电流6过过流点		电流6过过流点																															
142	32			15			Current7 High 1 Current Limit		Curr7 Hi 1 Lmt		电流7过流点		电流7过流点																															
143	32			15			Current7 High 2 Current Limit		Curr7 Hi 2 Lmt		电流7过过流点		电流7过过流点																															
144	32			15			Current8 High 1 Current Limit		Curr8 Hi 1 Lmt		电流8过流点		电流8过流点																															
145	32			15			Current8 High 2 Current Limit		Curr8 Hi 2 Lmt		电流8过过流点		电流8过过流点																															
146	32			15			Current9 High 1 Current Limit		Curr9 Hi 1 Lmt		电流9过流点		电流9过流点																															
147	32			15			Current9 High 2 Current Limit		Curr9 Hi 2 Lmt		电流9过过流点		电流9过过流点																															
148	32			15			Current10 High 1 Current Limit		Curr10 Hi 1 Lmt		电流10过流点		电流10过流点																															
149	32			15			Current10 High 2 Current Limit		Curr10 Hi 2 Lmt		电流10过过流点		电流10过过流点																															

150	32			15			Current1 High 1 Current			Curr 1 Hi 1		电流1过流			电流1过流																																															
151	32			15			Current1 High 2 Current			Curr 1 Hi 2		电流1过过流			电流1过过流																																															
152	32			15			Current2 High 1 Current			Curr 2 Hi 1		电流2过流			电流2过流																																															
153	32			15			Current2 High 2 Current			Curr 2 Hi 2		电流2过过流			电流2过过流																																															
154	32			15			Current3 High 1 Current			Curr 3 Hi 1		电流3过流			电流3过流																																															
155	32			15			Current3 High 2 Current			Curr 3 Hi 2		电流3过过流			电流3过过流																																															
156	32			15			Current4 High 1 Current			Curr 4 Hi 1		电流4过流			电流4过流																																															
157	32			15			Current4 High 2 Current			Curr 4 Hi 2		电流4过过流			电流4过过流																																															
158	32			15			Current5 High 1 Current			Curr 5 Hi 1		电流5过流			电流5过流																																															
159	32			15			Current5 High 2 Current			Curr 5 Hi 2		电流5过过流			电流5过过流																																															
160	32			15			Current6 High 1 Current			Curr 6 Hi 1		电流6过流			电流6过流																																															
161	32			15			Current6 High 2 Current			Curr 6 Hi 2		电流6过过流			电流6过过流																																															
162	32			15			Current7 High 1 Current			Curr 7 Hi 1		电流7过流			电流7过流																																															
163	32			15			Current7 High 2 Current			Curr 7 Hi 2		电流7过过流			电流7过过流																																															
164	32			15			Current8 High 1 Current			Curr 8 Hi 1		电流8过流			电流8过流																																															
165	32			15			Current8 High 2 Current			Curr 8 Hi 2		电流8过过流			电流8过过流																																															
166	32			15			Current9 High 1 Current			Curr 9 Hi 1		电流9过流			电流9过流																																															
167	32			15			Current9 High 2 Current			Curr 9 Hi 2		电流9过过流			电流9过过流																																															
168	32			15			Current10 High 1 Current		Curr 10 Hi 1		电流10过流			电流10过流																																															
169	32			15			Current10 High 2 Current		Curr 10 Hi 2		电流10过过流			电流10过过流																																															

214	32			15			Not Used				Not Used				不使用					不使用
215	32			15			General					General					通用					通用
216	32			15			Load					Load					负载					负载
217	32			15			Battery					Battery					电池					电池

221	32			32			SMDUE1 Temp 1 Assign Equip		S1-T1AssiEquip			SMDUE1探头1分配设备		SMDUE1探头1分配设备
222	32			32			SMDUE1 Temp 2 Assign Equip		S1-T2AssiEquip			SMDUE1探头2分配设备		SMDUE1探头2分配设备
223	32			32			SMDUE1 Temp 3 Assign Equip		S1-T3AssiEquip			SMDUE1探头3分配设备		SMDUE1探头3分配设备
224	32			32			SMDUE1 Temp 4 Assign Equip		S1-T4AssiEquip			SMDUE1探头4分配设备		SMDUE1探头4分配设备
225	32			32			SMDUE1 Temp 5 Assign Equip		S1-T5AssiEquip			SMDUE1探头5分配设备		SMDUE1探头5分配设备
226	32			32			SMDUE1 Temp 6 Assign Equip		S1-T6AssiEquip			SMDUE1探头6分配设备		SMDUE1探头6分配设备
227	32			32			SMDUE1 Temp 7 Assign Equip		S1-T7AssiEquip			SMDUE1探头7分配设备		SMDUE1探头7分配设备
228	32			32			SMDUE1 Temp 8 Assign Equip		S1-T8AssiEquip			SMDUE1探头8分配设备		SMDUE1探头8分配设备
229	32			32			SMDUE1 Temp 9 Assign Equip		S1-T9AssiEquip			SMDUE1探头9分配设备		SMDUE1探头9分配设备
230	32			32			SMDUE1 Temp 10 Assign Equip		S1-T10AssiEquip			SMDUE1探头10分配设备	SMDUE1探头10分配设备
231	32			15			None					None			未定义			未定义
232	32			15			Ambient					Ambient			环境			环境
233	32			15			Battery					Battery			电池			电池
234	32			15			BTRM					BTRM			BTRM			BTRM

241	32			15			Load 1					Load 1			Load 1					Load 1		
242	32			15			Load 2					Load 2			Load 2					Load 2		
243	32			15			Load 3					Load 3			Load 3					Load 3		
244	32			15			Load 4					Load 4			Load 4					Load 4		
245	32			15			Load 5					Load 5			Load 5					Load 5		
246	32			15			Load 6					Load 6			Load 6					Load 6		
247	32			15			Load 7					Load 7			Load 7					Load 7		
248	32			15			Load 8					Load 8			Load 8					Load 8		
249	32			15			Load 9					Load 9			Load 9					Load 9		
250	32			15			Load 10					Load 10			Load 10					Load 10		

251	32			32			SMDUE2 Temp 1 Assign Equip		T1 Assign Equip		SMDUE2探头1分配设备		SMDUE2探头1分配设备
252	32			32			SMDUE2 Temp 2 Assign Equip		T2 Assign Equip		SMDUE2探头2分配设备		SMDUE2探头2分配设备
253	32			32			SMDUE2 Temp 3 Assign Equip		T3 Assign Equip		SMDUE2探头3分配设备		SMDUE2探头3分配设备
254	32			32			SMDUE2 Temp 4 Assign Equip		T4 Assign Equip		SMDUE2探头4分配设备		SMDUE2探头4分配设备
255	32			32			SMDUE2 Temp 5 Assign Equip		T5 Assign Equip		SMDUE2探头5分配设备		SMDUE2探头5分配设备
256	32			32			SMDUE2 Temp 6 Assign Equip		T6 Assign Equip		SMDUE2探头6分配设备		SMDUE2探头6分配设备
257	32			32			SMDUE2 Temp 7 Assign Equip		T7 Assign Equip		SMDUE2探头7分配设备		SMDUE2探头7分配设备
258	32			32			SMDUE2 Temp 8 Assign Equip		T8 Assign Equip		SMDUE2探头8分配设备		SMDUE2探头8分配设备
259	32			32			SMDUE2 Temp 9 Assign Equip		T9 Assign Equip		SMDUE2探头9分配设备		SMDUE2探头9分配设备
260	32			32			SMDUE2 Temp 10 Assign Equip		T10 Assign Equip	SMDUE2探头10分配设备	SMDUE2探头10分配设备

261		32		15		SMDUE1 Temp1 High 2		SMDUE1 T1 Hi2		SMDUE1 T1过过温		SMDUE1T1过过温
262		32		15		SMDUE1 Temp1 High 1		SMDUE1 T1 Hi1		SMDUE1 T1过温		SMDUE1T1过温
263		32		15		SMDUE1 Temp1 Low			SMDUE1 T1 Low		SMDUE1 T1低温		SMDUE1T1低温
264		32		15		SMDUE1 Temp2 High 2	SMDUE1 T2 Hi2			SMDUE1 T2过过温		SMDUE1T2过过温
265		32		15		SMDUE1 Temp2 High 1	SMDUE1 T2 Hi1			SMDUE1 T2过温		SMDUE1T2过温
266		32		15		SMDUE1 Temp2 Low		SMDUE1 T2 Low			SMDUE1 T2低温		SMDUE1T2低温
267		32		15		SMDUE1 Temp3 High 2	SMDUE1 T3 Hi2			SMDUE1 T3过过温		SMDUE1T3过过温
268		32		15		SMDUE1 Temp3 High 1	SMDUE1 T3 Hi1			SMDUE1 T3过温		SMDUE1T3过温
269		32		15		SMDUE1 Temp3 Low		SMDUE1 T3 Low		SMDUE1 T3低温		SMDUE1T3低温
270		32		15		SMDUE1 Temp4 High 2	SMDUE1 T4 Hi2		SMDUE1 T4过过温		SMDUE1T4过过温
271		32		15		SMDUE1 Temp4 High 1	SMDUE1 T4 Hi1		SMDUE1 T4过温		SMDUE1T4过温
272		32		15		SMDUE1 Temp4 Low		SMDUE1 T4 Low		SMDUE1 T4低温		SMDUE1T4低温
273		32		15		SMDUE1 Temp5 High 2	SMDUE1 T5 Hi2		SMDUE1 T5过过温		SMDUE1T5过过温
274		32		15		SMDUE1 Temp5 High 1	SMDUE1 T5 Hi1		SMDUE1 T5过温		SMDUE1T5过温
275		32		15		SMDUE1 Temp5 Low		SMDUE1 T5 Low		SMDUE1 T5低温		SMDUE1T5低温
276		32		15		SMDUE1 Temp6 High 2	SMDUE1 T6 Hi2		SMDUE1 T6过过温		SMDUE1T6过过温
277		32		15		SMDUE1 Temp6 High 1	SMDUE1 T6 Hi1		SMDUE1 T6过温		SMDUE1T6过温
278		32		15		SMDUE1 Temp6 Low		SMDUE1 T6 Low		SMDUE1 T6低温		SMDUE1T6低温
279		32		15		SMDUE1 Temp7 High 2	SMDUE1 T7 Hi2		SMDUE1 T7过过温		SMDUE1T7过过温
280		32		15		SMDUE1 Temp7 High 1	SMDUE1 T7 Hi1		SMDUE1 T7过温		SMDUE1T7过温
281		32		15		SMDUE1 Temp7 Low		SMDUE1 T7 Low		SMDUE1 T7低温		SMDUE1T7低温
282		32		15		SMDUE1 Temp8 High 2	SMDUE1 T8 Hi2		SMDUE1 T8过过温		SMDUE1T8过过温
283		32		15		SMDUE1 Temp8 High 1	SMDUE1 T8 Hi1		SMDUE1 T8过温		SMDUE1T8过温
284		32		15		SMDUE1 Temp8 Low		SMDUE1 T8 Low		SMDUE1 T8低温		SMDUE1T8低温
285		32		15		SMDUE1 Temp9 High 2	SMDUE1 T9 Hi2		SMDUE1 T9过过温		SMDUE1T9过过温
286		32		15		SMDUE1 Temp9 High 1	SMDUE1 T9 Hi1		SMDUE1 T9过温		SMDUE1T9过温
287		32		15		SMDUE1 Temp9 Low		SMDUE1 T9 Low		SMDUE1 T9低温		SMDUE1T9低温
288		32		15		SMDUE1 Temp10 High 2	SMDUE1 T10 Hi2		SMDUE1 T10过过温		SMDUE1T10过过温
289		32		15		SMDUE1 Temp10 High 1	SMDUE1 T10 Hi1		SMDUE1 T10过温		SMDUE1T10过温
290		32		15		SMDUE1 Temp10 Low	SMDUE1 T10 Low		SMDUE1 T10低温		SMDUE1T10低温
291		32		15		SMDUE2 Temp1 High 2		SMDUE2 T1 Hi2		SMDUE2 T1过过温		SMDUE2T1过过温
292		32		15		SMDUE2 Temp1 High 1		SMDUE2 T1 Hi1		SMDUE2 T1过温		SMDUE2T1过温
293		32		15		SMDUE2 Temp1 Low			SMDUE2 T1 Low		SMDUE2 T1低温		SMDUE2T1低温
294		32		15		SMDUE2 Temp2 High 2	SMDUE2 T2 Hi2			SMDUE2 T2过过温		SMDUE2T2过过温
295		32		15		SMDUE2 Temp2 High 1	SMDUE2 T2 Hi1			SMDUE2 T2过温		SMDUE2T2过温
296		32		15		SMDUE2 Temp2 Low		SMDUE2 T2 Low			SMDUE2 T2低温		SMDUE2T2低温
297		32		15		SMDUE2 Temp3 High 2	SMDUE2 T3 Hi2			SMDUE2 T3过过温		SMDUE2T3过过温
298		32		15		SMDUE2 Temp3 High 1	SMDUE2 T3 Hi1			SMDUE2 T3过温		SMDUE2T3过温
299		32		15		SMDUE2 Temp3 Low		SMDUE2 T3 Low		SMDUE2 T3低温		SMDUE2T3低温
300		32		15		SMDUE2 Temp4 High 2	SMDUE2 T4 Hi2		SMDUE2 T4过过温		SMDUE2T4过过温
301		32		15		SMDUE2 Temp4 High 1	SMDUE2 T4 Hi1		SMDUE2 T4过温		SMDUE2T4过温
302		32		15		SMDUE2 Temp4 Low		SMDUE2 T4 Low		SMDUE2 T4低温		SMDUE2T4低温
303		32		15		SMDUE2 Temp5 High 2	SMDUE2 T5 Hi2		SMDUE2 T5过过温		SMDUE2T5过过温
304		32		15		SMDUE2 Temp5 High 1	SMDUE2 T5 Hi1		SMDUE2 T5过温		SMDUE2T5过温
305		32		15		SMDUE2 Temp5 Low		SMDUE2 T5 Low		SMDUE2 T5低温		SMDUE2T5低温
306		32		15		SMDUE2 Temp6 High 2	SMDUE2 T6 Hi2		SMDUE2 T6过过温		SMDUE2T6过过温
307		32		15		SMDUE2 Temp6 High 1	SMDUE2 T6 Hi1		SMDUE2 T6过温		SMDUE2T6过温
308		32		15		SMDUE2 Temp6 Low		SMDUE2 T6 Low		SMDUE2 T6低温		SMDUE2T6低温
309		32		15		SMDUE2 Temp7 High 2	SMDUE2 T7 Hi2		SMDUE2 T7过过温		SMDUE2T7过过温
310		32		15		SMDUE2 Temp7 High 1	SMDUE2 T7 Hi1		SMDUE2 T7过温		SMDUE2T7过温
311		32		15		SMDUE2 Temp7 Low		SMDUE2 T7 Low		SMDUE2 T7低温		SMDUE2T7低温
312		32		15		SMDUE2 Temp8 High 2	SMDUE2 T8 Hi2		SMDUE2 T8过过温		SMDUE2T8过过温
313		32		15		SMDUE2 Temp8 High 1	SMDUE2 T8 Hi1		SMDUE2 T8过温		SMDUE2T8过温
314		32		15		SMDUE2 Temp8 Low		SMDUE2 T8 Low		SMDUE2 T8低温		SMDUE2T8低温
315		32		15		SMDUE2 Temp9 High 2	SMDUE2 T9 Hi2		SMDUE2 T9过过温		SMDUE2T9过过温
316		32		15		SMDUE2 Temp9 High 1	SMDUE2 T9 Hi1		SMDUE2 T9过温		SMDUE2T9过温
317		32		15		SMDUE2 Temp9 Low		SMDUE2 T9 Low		SMDUE2 T9低温		SMDUE2T9低温
318		32		15		SMDUE2 Temp10 High 2	SMDUE2 T10 Hi2		SMDUE2 T10过过温		SMDUE2T10过过温
319		32		15		SMDUE2 Temp10 High 1	SMDUE2 T10 Hi1		SMDUE2 T10过温		SMDUE2T10过温
320		32		15		SMDUE2 Temp10 Low	SMDUE2 T10 Low		SMDUE2 T10低温		SMDUE2T10低温
321	32			32			SMDUE1 Temp 1 Not Used		S1-T1 Not Used			SMDUE1探头1未用		SMDUE1探头1未用
322	32			32			SMDUE1 Temp 2 Not Used		S1-T2 Not Used			SMDUE1探头2未用		SMDUE1探头2未用
323	32			32			SMDUE1 Temp 3 Not Used		S1-T3 Not Used			SMDUE1探头3未用		SMDUE1探头3未用
324	32			32			SMDUE1 Temp 4 Not Used		S1-T4 Not Used			SMDUE1探头4未用		SMDUE1探头4未用
325	32			32			SMDUE1 Temp 5 Not Used		S1-T5 Not Used			SMDUE1探头5未用		SMDUE1探头5未用
326	32			32			SMDUE1 Temp 6 Not Used		S1-T6 Not Used			SMDUE1探头6未用		SMDUE1探头6未用
327	32			32			SMDUE1 Temp 7 Not Used		S1-T7 Not Used			SMDUE1探头7未用		SMDUE1探头7未用
328	32			32			SMDUE1 Temp 8 Not Used		S1-T8 Not Used			SMDUE1探头8未用		SMDUE1探头8未用
329	32			32			SMDUE1 Temp 9 Not Used		S1-T9 Not Used			SMDUE1探头9未用		SMDUE1探头9未用
330	32			32			SMDUE1 Temp 10 Not Used		S1-T10 Not Used			SMDUE1探头10未用	SMDUE1探头10未用
331	32			32			SMDUE1 Temp 1 Sensor Fail	S1-T1 SensFail		SMDUE1探头1故障		SMDUE1探头1故障
332	32			32			SMDUE1 Temp 2 Sensor Fail	S1-T2 SensFail		SMDUE1探头2故障		SMDUE1探头2故障
333	32			32			SMDUE1 Temp 3 Sensor Fail	S1-T3 SensFail		SMDUE1探头3故障		SMDUE1探头3故障
334	32			32			SMDUE1 Temp 4 Sensor Fail	S1-T4 SensFail		SMDUE1探头4故障		SMDUE1探头4故障
335	32			32			SMDUE1 Temp 5 Sensor Fail	S1-T5 SensFail		SMDUE1探头5故障		SMDUE1探头5故障
336	32			32			SMDUE1 Temp 6 Sensor Fail	S1-T6 SensFail		SMDUE1探头6故障		SMDUE1探头6故障
337	32			32			SMDUE1 Temp 7 Sensor Fail	S1-T7 SensFail		SMDUE1探头7故障		SMDUE1探头7故障
338	32			32			SMDUE1 Temp 8 Sensor Fail	S1-T8 SensFail		SMDUE1探头8故障		SMDUE1探头8故障
339	32			32			SMDUE1 Temp 9 Sensor Fail	S1-T9 SensFail		SMDUE1探头9故障		SMDUE1探头9故障
340	32			32			SMDUE1 Temp 10 Sensor Fail	S1-T10SensFail		SMDUE1探头10故障	SMDUE1探头10故障

341	32			32			SMDUE2 Temp 1 Not Used		S2-T1 Not Used			SMDUE2探头1未用		SMDUE2探头1未用
342	32			32			SMDUE2 Temp 2 Not Used		S2-T2 Not Used			SMDUE2探头2未用		SMDUE2探头2未用
343	32			32			SMDUE2 Temp 3 Not Used		S2-T3 Not Used			SMDUE2探头3未用		SMDUE2探头3未用
344	32			32			SMDUE2 Temp 4 Not Used		S2-T4 Not Used			SMDUE2探头4未用		SMDUE2探头4未用
345	32			32			SMDUE2 Temp 5 Not Used		S2-T5 Not Used			SMDUE2探头5未用		SMDUE2探头5未用
346	32			32			SMDUE2 Temp 6 Not Used		S2-T6 Not Used			SMDUE2探头6未用		SMDUE2探头6未用
347	32			32			SMDUE2 Temp 7 Not Used		S2-T7 Not Used			SMDUE2探头7未用		SMDUE2探头7未用
348	32			32			SMDUE2 Temp 8 Not Used		S2-T8 Not Used			SMDUE2探头8未用		SMDUE2探头8未用
349	32			32			SMDUE2 Temp 9 Not Used		S2-T9 Not Used			SMDUE2探头9未用		SMDUE2探头9未用
350	32			32			SMDUE2 Temp 10 Not Used		S2-T10 Not Used			SMDUE2探头10未用	SMDUE2探头10未用
351	32			32			SMDUE2 Temp 1 Sensor Fail	S2-T1 SensFail		SMDUE2探头1故障		SMDUE2探头1故障
352	32			32			SMDUE2 Temp 2 Sensor Fail	S2-T2 SensFail		SMDUE2探头2故障		SMDUE2探头2故障
353	32			32			SMDUE2 Temp 3 Sensor Fail	S2-T3 SensFail		SMDUE2探头3故障		SMDUE2探头3故障
354	32			32			SMDUE2 Temp 4 Sensor Fail	S2-T4 SensFail		SMDUE2探头4故障		SMDUE2探头4故障
355	32			32			SMDUE2 Temp 5 Sensor Fail	S2-T5 SensFail		SMDUE2探头5故障		SMDUE2探头5故障
356	32			32			SMDUE2 Temp 6 Sensor Fail	S2-T6 SensFail		SMDUE2探头6故障		SMDUE2探头6故障
357	32			32			SMDUE2 Temp 7 Sensor Fail	S2-T7 SensFail		SMDUE2探头7故障		SMDUE2探头7故障
358	32			32			SMDUE2 Temp 8 Sensor Fail	S2-T8 SensFail		SMDUE2探头8故障		SMDUE2探头8故障
359	32			32			SMDUE2 Temp 9 Sensor Fail	S2-T9 SensFail		SMDUE2探头9故障		SMDUE2探头9故障
360	32			32			SMDUE2 Temp 10 Sensor Fail	S2-T10 SensFail		SMDUE2探头10故障	SMDUE2探头10故障

361		32			15			AI Ch1 Type 	AI Ch1 Type		AI 通道1 类型 	AI 通道1 类型 
362		32			15			AI Ch2 Type 	AI Ch2 Type		AI 通道2 类型 	AI 通道2 类型 
363		32			15			AI Ch3 Type 	AI Ch3 Type		AI 通道3 类型 	AI 通道3 类型 
364		32			15			AI Ch4 Type 	AI Ch4 Type		AI 通道4 类型 	AI 通道4 类型 
365		32			15			AI Ch5 Type 	AI Ch5 Type		AI 通道5 类型 	AI 通道5 类型 
366		32			15			AI Ch6 Type 	AI Ch6 Type		AI 通道6 类型 	AI 通道6 类型 
367		32			15			AI Ch7 Type 	AI Ch7 Type		AI 通道7 类型 	AI 通道7 类型 
368		32			15			AI Ch8 Type 	AI Ch8 Type		AI 通道8 类型 	AI 通道8 类型 
369		32			15			AI Ch9 Type 	AI Ch9 Type		AI 通道9 类型 	AI 通道9 类型 
370		32			15			AI Ch10 Type	AI Ch10 Type	AI 通道10 类型	AI 通道10 类型

371		32			15			Use For Shunt 		Use For Shunt		用于Shunt 	用于Shunt 
372		32			15			Use For Current 	Use For Current		用于Current 	用于Current 
373		32			15			Use For Volt 		Use For Volt		用于Volt 	用于Volt 
374		32			15			Use For Temp		Use For Temp		用于Temp 	用于Temp 

400		32			15			Cabinet1 Volt Enable		Volt1 Enable		机柜1电压使能		电压1使能
401		32			15			Cabinet2 Volt Enable		Volt2 Enable		机柜2电压使能		电压2使能
402		32			15			Cabinet3 Volt Enable		Volt3 Enable		机柜3电压使能		电压3使能
403		32			15			Cabinet4 Volt Enable		Volt4 Enable		机柜4电压使能		电压4使能
404		32			15			Cabinet5 Volt Enable		Volt5 Enable		机柜5电压使能		电压5使能
405		32			15			Cabinet6 Volt Enable		Volt6 Enable		机柜6电压使能		电压6使能
406		32			15			Cabinet7 Volt Enable		Volt7 Enable		机柜7电压使能		电压7使能
407		32			15			Cabinet8 Volt Enable		Volt8 Enable		机柜8电压使能		电压8使能
408		32			15			Cabinet9 Volt Enable		Volt9 Enable		机柜9电压使能		电压9使能
409		32			15			Cabinet10 Volt Enable		Volt10 Enable		机柜10电压使能		电压10使能
410		32			15			Fuse1 Alarm Enable			Fuse1 Alm Enable		熔丝1告警使能		熔丝1告警使能
411		32			15			Fuse2 Alarm Enable			Fuse2 Alm Enable		熔丝2告警使能		熔丝2告警使能
412		32			15			Fuse3 Alarm Enable			Fuse3 Alm Enable		熔丝3告警使能		熔丝3告警使能
413		32			15			Fuse4 Alarm Enable			Fuse4 Alm Enable		熔丝4告警使能		熔丝4告警使能
414		32			15			Fuse5 Alarm Enable			Fuse5 Alm Enable		熔丝5告警使能		熔丝5告警使能
415		32			15			Fuse6 Alarm Enable			Fuse6 Alm Enable		熔丝6告警使能		熔丝6告警使能
416		32			15			Fuse7 Alarm Enable			Fuse7 Alm Enable		熔丝7告警使能		熔丝7告警使能
417		32			15			Fuse8 Alarm Enable			Fuse8 Alm Enable		熔丝8告警使能		熔丝8告警使能
418		32			15			Fuse9 Alarm Enable			Fuse9 Alm Enable		熔丝9告警使能		熔丝9告警使能
419		32			15			Fuse10 Alarm Enable			Fuse10 Alm Enable		熔丝10告警使能		熔丝10告警使能
420		32			15			Analog1 Input Type			Ana1 InputType		熔丝1告警使能		熔丝1告警使能
421		32			15			Analog2 Input Type			Ana2 InputType		熔丝2告警使能		熔丝2告警使能
422		32			15			Analog3 Input Type			Ana3 InputType		熔丝3告警使能		熔丝3告警使能
423		32			15			Analog4 Input Type			Ana4 InputType		熔丝4告警使能		熔丝4告警使能
424		32			15			Analog5 Input Type			Ana5 InputType		熔丝5告警使能		熔丝5告警使能
425		32			15			Analog6 Input Type			Ana6 InputType		熔丝6告警使能		熔丝6告警使能
426		32			15			Analog7 Input Type			Ana7 InputType		熔丝7告警使能		熔丝7告警使能
427		32			15			Analog8 Input Type			Ana8 InputType		熔丝8告警使能		熔丝8告警使能
428		32			15			Analog9 Input Type			Ana9 InputType		熔丝9告警使能		熔丝9告警使能
429		32			15			Analog10 Input Type			Ana10 InputType		熔丝10告警使能		熔丝10告警使能
430		32			15			Disabled					Disabled					禁止				禁止
431		32			15			Enabled						Enabled					使能				使能
432		32			15			Shunt						Shunt					分流器				分流器
433		32			15			Temperature					Temperature				温度				温度
434		32			15			Transducer (0-10V)			Tsr (0-10V)				0到10伏传感器		10伏传感器
435		32			15			Transducer (0-20mA)			Tsr (0-20mA)			0到20毫安传感器		20毫安传感器

441		32			15			Input Block 1		Input Block 1	Input Block 1		Input Block 1
442		32			15			Input Block 2		Input Block 2	Input Block 2		Input Block 2
443		32			15			Input Block 3		Input Block 3	Input Block 3		Input Block 3
444		32			15			Input Block 4		Input Block 4	Input Block 4		Input Block 4
445		32			15			Input Block 5		Input Block 5	Input Block 5		Input Block 5
446		32			15			Input Block 6		Input Block 6	Input Block 6		Input Block 6
447		32			15			Input Block 7		Input Block 7	Input Block 7		Input Block 7
448		32			15			Input Block 8		Input Block 8	Input Block 8		Input Block 8
449		32			15			Input Block 9		Input Block 9	Input Block 9		Input Block 9
450		32			15			Input Block 10		Input Block 10	Input Block 10		Input Block 10

451		32			15			Current Transducer1		C Tran 1		电流传感器1			电流传感器1	
452		32			15			Current Transducer2		C Tran 2		电流传感器2			电流传感器2	
453		32			15			Current Transducer3		C Tran 3		电流传感器3			电流传感器3	
454		32			15			Current Transducer4		C Tran 4		电流传感器4			电流传感器4	
455		32			15			Current Transducer5		C Tran 5		电流传感器5			电流传感器5	
456		32			15			Current Transducer6		C Tran 6		电流传感器6			电流传感器6	
457		32			15			Current Transducer7		C Tran 7		电流传感器7			电流传感器7	
458		32			15			Current Transducer8		C Tran 8		电流传感器8			电流传感器8	
459		32			15			Current Transducer9		C Tran 9		电流传感器9			电流传感器9	
460		32			15			Current Transducer10	C Tran 10		电流传感器10		电流传感器10

461		32			15		Voltage Transducer1			V Tran 1	电压传感器1			电压传感器1	
462		32			15		Voltage Transducer2			V Tran 2	电压传感器2			电压传感器2	
463		32			15		Voltage Transducer3			V Tran 3	电压传感器3			电压传感器3	
464		32			15		Voltage Transducer4			V Tran 4	电压传感器4			电压传感器4	
465		32			15		Voltage Transducer5			V Tran 5	电压传感器5			电压传感器5	
466		32			15		Voltage Transducer6			V Tran 6	电压传感器6			电压传感器6	
467		32			15		Voltage Transducer7			V Tran 7	电压传感器7			电压传感器7	
468		32			15		Voltage Transducer8			V Tran 8	电压传感器8			电压传感器8	
469		32			15		Voltage Transducer9			V Tran 9	电压传感器9			电压传感器9	
470		32			15		Voltage Transducer10		V Tran 10	电压传感器10		电压传感器10

471		32			15			X1					X1					X1					X1	
472		32			15			Y1(Value at X1)		Y1(Value at X1)		Y1(基于X1的值)		Y1(基于X1的值)	
473		32			15			X2					X2					X2					X2
474		32			15			Y2(Value at X2)		Y2(Value at X2)		Y2(基于X2的值)		Y2(基于X2的值)	

600		32			15			Voltage1 High2 Volt Limit	Vol1 Hi2 Vol		电压1过过压		电压1过过压	
601		32			15			Voltage1 High1 Volt Limit	Vol1 Hi1 Vol		电压1过压		电压1过压	
602		32			15			Voltage1 Low1 Volt Limit	Vol1 Lw1 Vol		电压1欠压		电压1欠压	
603		32			15			Voltage1 Low2 Volt Limit	Vol1 Lw2 Vol		电压1欠欠压		电压1欠欠压	
604		32			15			Voltage2 High2 Volt Limit	Vol2 Hi2 Vol		电压2过过压		电压2过过压	
605		32			15			Voltage2 High1 Volt Limit	Vol2 Hi1 Vol		电压2过压		电压2过压	
606		32			15			Voltage2 Low1 Volt Limit	Vol2 Lw1 Vol		电压2欠压		电压2欠压	
607		32			15			Voltage2 Low2 Volt Limit	Vol2 Lw2 Vol		电压2欠欠压		电压2欠欠压	
608		32			15			Voltage3 High2 Volt Limit	Vol3 Hi2 Vol		电压3过过压		电压3过过压	
609		32			15			Voltage3 High1 Volt Limit	Vol3 Hi1 Vol		电压3过压		电压3过压	
610		32			15			Voltage3 Low1 Volt Limit	Vol3 Lw1 Vol		电压3欠压		电压3欠压	
611		32			15			Voltage3 Low2 Volt Limit	Vol3 Lw2 Vol		电压3欠欠压		电压3欠欠压	
612		32			15			Voltage4 High2 Volt Limit	Vol4 Hi2 Vol		电压4过过压		电压4过过压	
613		32			15			Voltage4 High1 Volt Limit	Vol4 Hi1 Vol		电压4过压		电压4过压	
614		32			15			Voltage4 Low1 Volt Limit	Vol4 Lw1 Vol		电压4欠压		电压4欠压	
615		32			15			Voltage4 Low2 Volt Limit	Vol4 Lw2 Vol		电压4欠欠压		电压4欠欠压	
616		32			15			Voltage5 High2 Volt Limit	Vol5 Hi2 Vol		电压5过过压		电压5过过压	
617		32			15			Voltage5 High1 Volt Limit	Vol5 Hi1 Vol		电压5过压		电压5过压	
618		32			15			Voltage5 Low1 Volt Limit	Vol5 Lw1 Vol		电压5欠压		电压5欠压	
619		32			15			Voltage5 Low2 Volt Limit	Vol5 Lw2 Vol		电压5欠欠压		电压5欠欠压	
620		32			15			Voltage6 High2 Volt Limit	Vol6 Hi2 Vol		电压6过过压		电压6过过压	
621		32			15			Voltage6 High1 Volt Limit	Vol6 Hi1 Vol		电压6过压		电压6过压	
622		32			15			Voltage6 Low1 Volt Limit	Vol6 Lw1 Vol		电压6欠压		电压6欠压	
623		32			15			Voltage6 Low2 Volt Limit	Vol6 Lw2 Vol		电压6欠欠压		电压6欠欠压	
624		32			15			Voltage7 High2 Volt Limit	Vol7 Hi2 Vol		电压7过过压		电压7过过压	
625		32			15			Voltage7 High1 Volt Limit	Vol7 Hi1 Vol		电压7过压		电压7过压	
626		32			15			Voltage7 Low1 Volt Limit	Vol7 Lw1 Vol		电压7欠压		电压7欠压	
627		32			15			Voltage7 Low2 Volt Limit	Vol7 Lw2 Vol		电压7欠欠压		电压7欠欠压	
628		32			15			Voltage8 High2 Volt Limit	Vol8 Hi2 Vol		电压8过过压		电压8过过压	
629		32			15			Voltage8 High1 Volt Limit	Vol8 Hi1 Vol		电压8过压		电压8过压	
630		32			15			Voltage8 Low1 Volt Limit	Vol8 Lw1 Vol		电压8欠压		电压8欠压	
631		32			15			Voltage8 Low2 Volt Limit	Vol8 Lw2 Vol		电压8欠欠压		电压8欠欠压	
632		32			15			Voltage9 High2 Volt Limit	Vol9 Hi2 Vol		电压9过过压		电压9过过压	
633		32			15			Voltage9 High1 Volt Limit	Vol9 Hi1 Vol		电压9过压		电压9过压	
634		32			15			Voltage9 Low1 Volt Limit	Vol9 Lw1 Vol		电压9欠压		电压9欠压	
635		32			15			Voltage9 Low2 Volt Limit	Vol9 Lw2 Vol		电压9欠欠压		电压9欠欠压	
636		32			15			Voltage10 High2 Volt Limit	Vol10 Hi2 Vol		电压10过过压	电压10过过压
637		32			15			Voltage10 High1 Volt Limit	Vol10 Hi1 Vol		电压10过压		电压10过压	
638		32			15			Voltage10 Low1 Volt Limit	Vol10 Lw1 Vol		电压10欠压		电压10欠压	
639		32			15			Voltage10 Low2 Volt Limit	Vol10 Lw2 Vol		电压10欠欠压	电压10欠欠压

640		32			15			Voltage1 Low1 Volt Alarm			Vol1 Lw1 Vol Alm		电压1欠压		电压1欠压	
641		32			15			Voltage1 Low2 Volt Alarm			Vol1 Lw2 Vol Alm		电压1欠欠压		电压1欠欠压		
642		32			15			Voltage1 High1 Volt Alarm			Vol1 Hi1 Vol Alm		电压1过压		电压1过压	
643		32			15			Voltage1 High2 Volt Alarm			Vol1 Hi2 Vol Alm		电压1过过压		电压1过过压		
644		32			15			Voltage2 Low1 Volt Alarm			Vol2 Lw1 Vol Alm		电压2欠压		电压2欠压		
645		32			15			Voltage2 Low2 Volt Alarm			Vol2 Lw2 Vol Alm		电压2欠欠压		电压2欠欠压	
646		32			15			Voltage2 High1 Volt Alarm			Vol2 Hi1 Vol Alm		电压2过压		电压2过压	
647		32			15			Voltage2 High2 Volt Alarm			Vol2 Hi2 Vol Alm		电压2过过压		电压2过过压		
648		32			15			Voltage3 Low1 Volt Alarm			Vol3 Lw1 Vol Alm		电压3欠压		电压3欠压	
649		32			15			Voltage3 Low2 Volt Alarm			Vol3 Lw2 Vol Alm		电压3欠欠压		电压3欠欠压	
650		32			15			Voltage3 High1 Volt Alarm			Vol3 Hi1 Vol Alm		电压3过压		电压3过压	
651		32			15			Voltage3 High2 Volt Alarm			Vol3 Hi2 Vol Alm		电压3过过压		电压3过过压	
652		32			15			Voltage4 Low1 Volt Alarm			Vol4 Lw1 Vol Alm		电压4欠压		电压4欠压	
653		32			15			Voltage4 Low2 Volt Alarm			Vol4 Lw2 Vol Alm		电压4欠欠压		电压4欠欠压	
654		32			15			Voltage4 High1 Volt Alarm			Vol4 Hi1 Vol Alm		电压4过压		电压4过压	
655		32			15			Voltage4 High2 Volt Alarm			Vol4 Hi2 Vol Alm		电压4过过压		电压4过过压	
656		32			15			Voltage5 Low1 Volt Alarm			Vol5 Lw1 Vol Alm		电压5欠压		电压5欠压	
657		32			15			Voltage5 Low2 Volt Alarm			Vol5 Lw2 Vol Alm		电压5欠欠压		电压5欠欠压	
658		32			15			Voltage5 High1 Volt Alarm			Vol5 Hi1 Vol Alm		电压5过压		电压5过压	
659		32			15			Voltage5 High2 Volt Alarm			Vol5 Hi2 Vol Alm		电压5过过压		电压5过过压	
660		32			15			Voltage6 Low1 Volt Alarm			Vol6 Lw1 Vol Alm		电压6欠压		电压6欠压		
661		32			15			Voltage6 Low2 Volt Alarm			Vol6 Lw2 Vol Alm		电压6欠欠压		电压6欠欠压	
662		32			15			Voltage6 High1 Volt Alarm			Vol6 Hi1 Vol Alm		电压6过压		电压6过压	
663		32			15			Voltage6 High2 Volt Alarm			Vol6 Hi2 Vol Alm		电压6过过压		电压6过过压		
664		32			15			Voltage7 Low1 Volt Alarm			Vol7 Lw1 Vol Alm		电压7欠压		电压7欠压		
665		32			15			Voltage7 Low2 Volt Alarm			Vol7 Lw2 Vol Alm		电压7欠欠压		电压7欠欠压	
666		32			15			Voltage7 High1 Volt Alarm			Vol7 Hi1 Vol Alm		电压7过压		电压7过压	
667		32			15			Voltage7 High2 Volt Alarm			Vol7 Hi2 Vol Alm		电压7过过压		电压7过过压		
668		32			15			Voltage8 Low1 Volt Alarm			Vol8 Lw1 Vol Alm		电压8欠压		电压8欠压		
669		32			15			Voltage8 Low2 Volt Alarm			Vol8 Lw2 Vol Alm		电压8欠欠压		电压8欠欠压	
670		32			15			Voltage8 High1 Volt Alarm			Vol8 Hi1 Vol Alm		电压8过压		电压8过压	
671		32			15			Voltage8 High2 Volt Alarm			Vol8 Hi2 Vol Alm		电压8过过压		电压8过过压		
672		32			15			Voltage9 Low1 Volt Alarm			Vol9 Lw1 Vol Alm		电压9欠压		电压9欠压	
673		32			15			Voltage9 Low2 Volt Alarm			Vol9 Lw2 Vol Alm		电压9欠欠压		电压9欠欠压	
674		32			15			Voltage9 High1 Volt Alarm			Vol9 Hi1 Vol Alm		电压9过压		电压9过压	
675		32			15			Voltage9 High2 Volt Alarm			Vol9 Hi2 Vol Alm		电压9过过压		电压9过过压	
676		32			15			Voltage10 Low1 Volt Alarm			Vol10 Lw1 Vol Alm		电压10欠压		电压10欠压	
677		32			15			Voltage10 Low2 Volt Alarm			Vol10 Lw2 Vol Alm		电压10欠欠压	电压10欠欠压
678		32			15			Voltage10 High1 Volt Alarm			Vol10 Hi1 Vol Alm		电压10过压		电压10过压	
679		32			15			Voltage10 High2 Volt Alarm			Vol10 Hi2 Vol Alm		电压10过过压	电压10过过压
																											
680		32			15			Transducer1 High2 Volt Limit		Vol1 Hi2 Vol		电压1过过压		电压1过过压	
681		32			15			Transducer1 High1 Volt Limit		Vol1 Hi1 Vol		电压1过压		电压1过压	
682		32			15			Transducer1 Low1 Volt Limit			Vol1 Lw1 Vol		电压1欠压		电压1欠压	
683		32			15			Transducer1 Low2 Volt Limit			Vol1 Lw2 Vol		电压1欠欠压		电压1欠欠压	
684		32			15			Transducer2 High2 Volt Limit		Vol2 Hi2 Vol		电压2过过压		电压2过过压	
685		32			15			Transducer2 High1 Volt Limit		Vol2 Hi1 Vol		电压2过压		电压2过压	
686		32			15			Transducer2 Low1 Volt Limit			Vol2 Lw1 Vol		电压2欠压		电压2欠压	
687		32			15			Transducer2 Low2 Volt Limit			Vol2 Lw2 Vol		电压2欠欠压		电压2欠欠压	
688		32			15			Transducer3 High2 Volt Limit		Vol3 Hi2 Vol		电压3过过压		电压3过过压	
689		32			15			Transducer3 High1 Volt Limit		Vol3 Hi1 Vol		电压3过压		电压3过压	
690		32			15			Transducer3 Low1 Volt Limit			Vol3 Lw1 Vol		电压3欠压		电压3欠压	
691		32			15			Transducer3 Low2 Volt Limit			Vol3 Lw2 Vol		电压3欠欠压		电压3欠欠压	
692		32			15			Transducer4 High2 Volt Limit		Vol4 Hi2 Vol		电压4过过压		电压4过过压	
693		32			15			Transducer4 High1 Volt Limit		Vol4 Hi1 Vol		电压4过压		电压4过压	
694		32			15			Transducer4 Low1 Volt Limit			Vol4 Lw1 Vol		电压4欠压		电压4欠压	
695		32			15			Transducer4 Low2 Volt Limit			Vol4 Lw2 Vol		电压4欠欠压		电压4欠欠压	
696		32			15			Transducer5 High2 Volt Limit		Vol5 Hi2 Vol		电压5过过压		电压5过过压	
697		32			15			Transducer5 High1 Volt Limit		Vol5 Hi1 Vol		电压5过压		电压5过压	
698		32			15			Transducer5 Low1 Volt Limit			Vol5 Lw1 Vol		电压5欠压		电压5欠压	
699		32			15			Transducer5 Low2 Volt Limit			Vol5 Lw2 Vol		电压5欠欠压		电压5欠欠压	
700		32			15			Transducer6 High2 Volt Limit		Vol6 Hi2 Vol		电压6过过压		电压6过过压	
701		32			15			Transducer6 High1 Volt Limit		Vol6 Hi1 Vol		电压6过压		电压6过压	
702		32			15			Transducer6 Low1 Volt Limit			Vol6 Lw1 Vol		电压6欠压		电压6欠压	
703		32			15			Transducer6 Low2 Volt Limit			Vol6 Lw2 Vol		电压6欠欠压		电压6欠欠压	
704		32			15			Transducer7 High2 Volt Limit		Vol7 Hi2 Vol		电压7过过压		电压7过过压	
705		32			15			Transducer7 High1 Volt Limit		Vol7 Hi1 Vol		电压7过压		电压7过压	
706		32			15			Transducer7 Low1 Volt Limit			Vol7 Lw1 Vol		电压7欠压		电压7欠压	
707		32			15			Transducer7 Low2 Volt Limit			Vol7 Lw2 Vol		电压7欠欠压		电压7欠欠压	
708		32			15			Transducer8 High2 Volt Limit		Vol8 Hi2 Vol		电压8过过压		电压8过过压	
709		32			15			Transducer8 High1 Volt Limit		Vol8 Hi1 Vol		电压8过压		电压8过压	
710		32			15			Transducer8 Low1 Volt Limit			Vol8 Lw1 Vol		电压8欠压		电压8欠压	
711		32			15			Transducer8 Low2 Volt Limit			Vol8 Lw2 Vol		电压8欠欠压		电压8欠欠压	
712		32			15			Transducer9 High2 Volt Limit		Vol9 Hi2 Vol		电压9过过压		电压9过过压	
713		32			15			Transducer9 High1 Volt Limit		Vol9 Hi1 Vol		电压9过压		电压9过压	
714		32			15			Transducer9 Low1 Volt Limit			Vol9 Lw1 Vol		电压9欠压		电压9欠压	
715		32			15			Transducer9 Low2 Volt Limit			Vol9 Lw2 Vol		电压9欠欠压		电压9欠欠压	
716		32			15			Transducer10 High2 Volt Limit		Vol10 Hi2 Vol		电压10过过压	电压10过过压
717		32			15			Transducer10 High1 Volt Limit		Vol10 Hi1 Vol		电压10过压		电压10过压	
718		32			15			Transducer10 Low1 Volt Limit		Vol10 Lw1 Vol		电压10欠压		电压10欠压	
719		32			15			Transducer10 Low2 Volt Limit		Vol10 Lw2 Vol		电压10欠欠压	电压10欠欠压

720		32			15			TranVolt1 Low1 Volt Alarm			TranVol1 Lw1 Alm		传感器电压1欠压			传感器1欠压	
721		32			15			TranVolt1 Low2 Volt Alarm			TranVol1 Lw2 Alm		传感器电压1欠欠压		传感器1欠欠压		
722		32			15			TranVolt1 High1 Volt Alarm			TranVol1 Hi1 Alm		传感器电压1过压			传感器1过压	
723		32			15			TranVolt1 High2 Volt Alarm			TranVol1 Hi2 Alm		传感器电压1过过压		传感器1过过压		
724		32			15			TranVolt2 Low1 Volt Alarm			TranVol2 Lw1 Alm		传感器电压2欠压			传感器2欠压		
725		32			15			TranVolt2 Low2 Volt Alarm			TranVol2 Lw2 Alm		传感器电压2欠欠压		传感器2欠欠压	
726		32			15			TranVolt2 High1 Volt Alarm			TranVol2 Hi1 Alm		传感器电压2过压			传感器2过压	
727		32			15			TranVolt2 High2 Volt Alarm			TranVol2 Hi2 Alm		传感器电压2过过压		传感器2过过压		
728		32			15			TranVolt3 Low1 Volt Alarm			TranVol3 Lw1 Alm		传感器电压3欠压			传感器3欠压	
729		32			15			TranVolt3 Low2 Volt Alarm			TranVol3 Lw2 Alm		传感器电压3欠欠压		传感器3欠欠压	
730		32			15			TranVolt3 High1 Volt Alarm			TranVol3 Hi1 Alm		传感器电压3过压			传感器3过压	
731		32			15			TranVolt3 High2 Volt Alarm			TranVol3 Hi2 Alm		传感器电压3过过压		传感器3过过压	
732		32			15			TranVolt4 Low1 Volt Alarm			TranVol4 Lw1 Alm		传感器电压4欠压			传感器4欠压	
733		32			15			TranVolt4 Low2 Volt Alarm			TranVol4 Lw2 Alm		传感器电压4欠欠压		传感器4欠欠压	
734		32			15			TranVolt4 High1 Volt Alarm			TranVol4 Hi1 Alm		传感器电压4过压			传感器4过压	
735		32			15			TranVolt4 High2 Volt Alarm			TranVol4 Hi2 Alm		传感器电压4过过压		传感器4过过压	
736		32			15			TranVolt5 Low1 Volt Alarm			TranVol5 Lw1 Alm		传感器电压5欠压			传感器5欠压	
737		32			15			TranVolt5 Low2 Volt Alarm			TranVol5 Lw2 Alm		传感器电压5欠欠压		传感器5欠欠压	
738		32			15			TranVolt5 High1 Volt Alarm			TranVol5 Hi1 Alm		传感器电压5过压			传感器5过压	
739		32			15			TranVolt5 High2 Volt Alarm			TranVol5 Hi2 Alm		传感器电压5过过压		传感器5过过压	
740		32			15			TranVolt6 Low1 Volt Alarm			TranVol6 Lw1 Alm		传感器电压6欠压			传感器6欠压		
741		32			15			TranVolt6 Low2 Volt Alarm			TranVol6 Lw2 Alm		传感器电压6欠欠压		传感器6欠欠压	
742		32			15			TranVolt6 High1 Volt Alarm			TranVol6 Hi1 Alm		传感器电压6过压			传感器6过压	
743		32			15			TranVolt6 High2 Volt Alarm			TranVol6 Hi2 Alm		传感器电压6过过压		传感器6过过压		
744		32			15			TranVolt7 Low1 Volt Alarm			TranVol7 Lw1 Alm		传感器电压7欠压			传感器7欠压		
745		32			15			TranVolt7 Low2 Volt Alarm			TranVol7 Lw2 Alm		传感器电压7欠欠压		传感器7欠欠压	
746		32			15			TranVolt7 High1 Volt Alarm			TranVol7 Hi1 Alm		传感器电压7过压			传感器7过压	
747		32			15			TranVolt7 High2 Volt Alarm			TranVol7 Hi2 Alm		传感器电压7过过压		传感器7过过压		
748		32			15			TranVolt8 Low1 Volt Alarm			TranVol8 Lw1 Alm		传感器电压8欠压			传感器8欠压		
749		32			15			TranVolt8 Low2 Volt Alarm			TranVol8 Lw2 Alm		传感器电压8欠欠压		传感器8欠欠压	
750		32			15			TranVolt8 High1 Volt Alarm			TranVol8 Hi1 Alm		传感器电压8过压			传感器8过压	
751		32			15			TranVolt8 High2 Volt Alarm			TranVol8 Hi2 Alm		传感器电压8过过压		传感器8过过压		
752		32			15			TranVolt9 Low1 Volt Alarm			TranVol9 Lw1 Alm		传感器电压9欠压			传感器9欠压	
753		32			15			TranVolt9 Low2 Volt Alarm			TranVol9 Lw2 Alm		传感器电压9欠欠压		传感器9欠欠压	
754		32			15			TranVolt9 High1 Volt Alarm			TranVol9 Hi1 Alm		传感器电压9过压			传感器9过压	
755		32			15			TranVolt9 High2 Volt Alarm			TranVol9 Hi2 Alm		传感器电压9过过压		传感器9过过压	
756		32			15			TranVolt10 Low1 Volt Alarm			TranVol10 Lw1 Alm		传感器电压10欠压		传感器10欠压	
757		32			15			TranVolt10 Low2 Volt Alarm			TranVol10 Lw2 Alm		传感器电压10欠欠压		传感器10欠欠压
758		32			15			TranVolt10 High1 Volt Alarm			TranVol10 Hi1 Alm		传感器电压10过压		传感器10过压	
759		32			15			TranVolt10 High2 Volt Alarm			TranVol10 Hi2 Alm		传感器电压10过过压		传感器10过过压

760		32			15			Transducer1 High2 Curr Limit		Curr1 Hi2 Curr		电流1过过流		电流1过过流	
761		32			15			Transducer1 High1 Curr Limit		Curr1 Hi1 Curr		电流1过流		电流1过流	
762		32			15			Transducer1 Low1 Curr Limit			Curr1 Lw1 Curr		电流1欠流		电流1欠流	
763		32			15			Transducer1 Low2 Curr Limit			Curr1 Lw2 Curr		电流1欠欠流		电流1欠欠流	
764		32			15			Transducer2 High2 Curr Limit		Curr2 Hi2 Curr		电流2过过流		电流2过过流	
765		32			15			Transducer2 High1 Curr Limit		Curr2 Hi1 Curr		电流2过流		电流2过流	
766		32			15			Transducer2 Low1 Curr Limit			Curr2 Lw1 Curr		电流2欠流		电流2欠流	
767		32			15			Transducer2 Low2 Curr Limit			Curr2 Lw2 Curr		电流2欠欠流		电流2欠欠流	
768		32			15			Transducer3 High2 Curr Limit		Curr3 Hi2 Curr		电流3过过流		电流3过过流	
769		32			15			Transducer3 High1 Curr Limit		Curr3 Hi1 Curr		电流3过流		电流3过流	
770		32			15			Transducer3 Low1 Curr Limit			Curr3 Lw1 Curr		电流3欠流		电流3欠流	
771		32			15			Transducer3 Low2 Curr Limit			Curr3 Lw2 Curr		电流3欠欠流		电流3欠欠流	
772		32			15			Transducer4 High2 Curr Limit		Curr4 Hi2 Curr		电流4过过流		电流4过过流	
773		32			15			Transducer4 High1 Curr Limit		Curr4 Hi1 Curr		电流4过流		电流4过流	
774		32			15			Transducer4 Low1 Curr Limit			Curr4 Lw1 Curr		电流4欠流		电流4欠流	
775		32			15			Transducer4 Low2 Curr Limit			Curr4 Lw2 Curr		电流4欠欠流		电流4欠欠流	
776		32			15			Transducer5 High2 Curr Limit		Curr5 Hi2 Curr		电流5过过流		电流5过过流	
777		32			15			Transducer5 High1 Curr Limit		Curr5 Hi1 Curr		电流5过流		电流5过流	
778		32			15			Transducer5 Low1 Curr Limit			Curr5 Lw1 Curr		电流5欠流		电流5欠流	
779		32			15			Transducer5 Low2 Curr Limit			Curr5 Lw2 Curr		电流5欠欠流		电流5欠欠流	
780		32			15			Transducer6 High2 Curr Limit		Curr6 Hi2 Curr		电流6过过流		电流6过过流	
781		32			15			Transducer6 High1 Curr Limit		Curr6 Hi1 Curr		电流6过流		电流6过流	
782		32			15			Transducer6 Low1 Curr Limit			Curr6 Lw1 Curr		电流6欠流		电流6欠流	
783		32			15			Transducer6 Low2 Curr Limit			Curr6 Lw2 Curr		电流6欠欠流		电流6欠欠流	
784		32			15			Transducer7 High2 Curr Limit		Curr7 Hi2 Curr		电流7过过流		电流7过过流	
785		32			15			Transducer7 High1 Curr Limit		Curr7 Hi1 Curr		电流7过流		电流7过流	
786		32			15			Transducer7 Low1 Curr Limit			Curr7 Lw1 Curr		电流7欠流		电流7欠流	
787		32			15			Transducer7 Low2 Curr Limit			Curr7 Lw2 Curr		电流7欠欠流		电流7欠欠流	
788		32			15			Transducer8 High2 Curr Limit		Curr8 Hi2 Curr		电流8过过流		电流8过过流	
789		32			15			Transducer8 High1 Curr Limit		Curr8 Hi1 Curr		电流8过流		电流8过流	
790		32			15			Transducer8 Low1 Curr Limit			Curr8 Lw1 Curr		电流8欠流		电流8欠流	
791		32			15			Transducer8 Low2 Curr Limit			Curr8 Lw2 Curr		电流8欠欠流		电流8欠欠流	
792		32			15			Transducer9 High2 Curr Limit		Curr9 Hi2 Curr		电流9过过流		电流9过过流	
793		32			15			Transducer9 High1 Curr Limit		Curr9 Hi1 Curr		电流9过流		电流9过流	
794		32			15			Transducer9 Low1 Curr Limit			Curr9 Lw1 Curr		电流9欠流		电流9欠流	
795		32			15			Transducer9 Low2 Curr Limit			Curr9 Lw2 Curr		电流9欠欠流		电流9欠欠流	
796		32			15			Transducer10 High2 Curr Limit		Curr10 Hi2 Curr		电流10过过流	电流10过过流
797		32			15			Transducer10 High1 Curr Limit		Curr10 Hi1 Curr		电流10过流		电流10过流	
798		32			15			Transducer10 Low1 Curr Limit		Curr10 Lw1 Curr		电流10欠流		电流10欠流	
799		32			15			Transducer10 Low2 Curr Limit		Curr10 Lw2 Curr		电流10欠欠流	电流10欠欠流

800		32			15			TranCurr1 Low1 Curr Alarm			TranCurr1 Lw1 	传感器电流1过过流		传感器1过过流	
801		32			15			TranCurr1 Low2 Curr Alarm			TranCurr1 Lw2 	传感器电流1过流			传感器1过流	
802		32			15			TranCurr1 High1 Curr Alarm			TranCurr1 Hi1 	传感器电流1欠流			传感器1欠流	
803		32			15			TranCurr1 High2 Curr Alarm			TranCurr1 Hi2 	传感器电流1欠欠流		传感器1欠欠流	
804		32			15			TranCurr2 Low1 Curr Alarm			TranCurr2 Lw1 	传感器电流2过过流		传感器2过过流	
805		32			15			TranCurr2 Low2 Curr Alarm			TranCurr2 Lw2 	传感器电流2过流			传感器2过流	
806		32			15			TranCurr2 High1 Curr Alarm			TranCurr2 Hi1 	传感器电流2欠流			传感器2欠流	
807		32			15			TranCurr2 High2 Curr Alarm			TranCurr2 Hi2 	传感器电流2欠欠流		传感器2欠欠流	
808		32			15			TranCurr3 Low1 Curr Alarm			TranCurr3 Lw1 	传感器电流3过过流		传感器3过过流	
809		32			15			TranCurr3 Low2 Curr Alarm			TranCurr3 Lw2 	传感器电流3过流			传感器3过流	
810		32			15			TranCurr3 High1 Curr Alarm			TranCurr3 Hi1 	传感器电流3欠流			传感器3欠流	
811		32			15			TranCurr3 High2 Curr Alarm			TranCurr3 Hi2 	传感器电流3欠欠流		传感器3欠欠流	
812		32			15			TranCurr4 Low1 Curr Alarm			TranCurr4 Lw1 	传感器电流4过过流		传感器4过过流	
813		32			15			TranCurr4 Low2 Curr Alarm			TranCurr4 Lw2 	传感器电流4过流			传感器4过流	
814		32			15			TranCurr4 High1 Curr Alarm			TranCurr4 Hi1 	传感器电流4欠流			传感器4欠流	
815		32			15			TranCurr4 High2 Curr Alarm			TranCurr4 Hi2 	传感器电流4欠欠流		传感器4欠欠流	
816		32			15			TranCurr5 Low1 Curr Alarm			TranCurr5 Lw1 	传感器电流5过过流		传感器5过过流	
817		32			15			TranCurr5 Low2 Curr Alarm			TranCurr5 Lw2 	传感器电流5过流			传感器5过流	
818		32			15			TranCurr5 High1 Curr Alarm			TranCurr5 Hi1 	传感器电流5欠流			传感器5欠流	
819		32			15			TranCurr5 High2 Curr Alarm			TranCurr5 Hi2 	传感器电流5欠欠流		传感器5欠欠流	
820		32			15			TranCurr6 Low1 Curr Alarm			TranCurr6 Lw1 	传感器电流6过过流		传感器6过过流	
821		32			15			TranCurr6 Low2 Curr Alarm			TranCurr6 Lw2 	传感器电流6过流			传感器6过流	
822		32			15			TranCurr6 High1 Curr Alarm			TranCurr6 Hi1 	传感器电流6欠流			传感器6欠流	
823		32			15			TranCurr6 High2 Curr Alarm			TranCurr6 Hi2 	传感器电流6欠欠流		传感器6欠欠流	
824		32			15			TranCurr7 Low1 Curr Alarm			TranCurr7 Lw1 	传感器电流7过过流		传感器7过过流	
825		32			15			TranCurr7 Low2 Curr Alarm			TranCurr7 Lw2 	传感器电流7过流			传感器7过流	
826		32			15			TranCurr7 High1 Curr Alarm			TranCurr7 Hi1 	传感器电流7欠流			传感器7欠流	
827		32			15			TranCurr7 High2 Curr Alarm			TranCurr7 Hi2 	传感器电流7欠欠流		传感器7欠欠流	
828		32			15			TranCurr8 Low1 Curr Alarm			TranCurr8 Lw1 	传感器电流8过过流		传感器8过过流	
829		32			15			TranCurr8 Low2 Curr Alarm			TranCurr8 Lw2 	传感器电流8过流			传感器8过流	
830		32			15			TranCurr8 High1 Curr Alarm			TranCurr8 Hi1 	传感器电流8欠流			传感器8欠流	
831		32			15			TranCurr8 High2 Curr Alarm			TranCurr8 Hi2 	传感器电流8欠欠流		传感器8欠欠流	
832		32			15			TranCurr9 Low1 Curr Alarm			TranCurr9 Lw1 	传感器电流9过过流		传感器9过过流	
833		32			15			TranCurr9 Low2 Curr Alarm			TranCurr9 Lw2 	传感器电流9过流			传感器9过流	
834		32			15			TranCurr9 High1 Curr Alarm			TranCurr9 Hi1 	传感器电流9欠流			传感器9欠流	
835		32			15			TranCurr9 High2 Curr Alarm			TranCurr9 Hi2 	传感器电流9欠欠流		传感器9欠欠流	
836		32			15			TranCurr10 Low1 Curr Alarm			TranCurr10 Lw1 	传感器电流10过过流		传感器10过过流
837		32			15			TranCurr10 Low2 Curr Alarm			TranCurr10 Lw2 	传感器电流10过流		传感器10过流	
838		32			15			TranCurr10 High1 Curr Alarm			TranCurr10 Hi1 	传感器电流10欠流		传感器10欠流	
839		32			15			TranCurr10 High2 Curr Alarm			TranCurr10 Hi2 	传感器电流10欠欠流		传感器10欠欠流

840		32			15			Transducer1 Units		Trans1 Units		传感器1单位			传感器1单位	
841		32			15			Transducer2 Units		Trans2 Units		传感器2单位			传感器2单位	
842		32			15			Transducer3 Units		Trans3 Units		传感器3单位			传感器3单位	
843		32			15			Transducer4 Units		Trans4 Units		传感器4单位			传感器4单位	
844		32			15			Transducer5 Units		Trans5 Units		传感器5单位			传感器5单位	
845		32			15			Transducer6 Units		Trans6 Units		传感器6单位			传感器6单位	
846		32			15			Transducer7 Units		Trans7 Units		传感器7单位			传感器7单位	
847		32			15			Transducer8 Units		Trans8 Units		传感器8单位			传感器8单位	
848		32			15			Transducer9 Units		Trans9 Units		传感器9单位			传感器9单位	
849		32			15			Transducer10 Units		Trans10 Units		传感器10单位		传感器10单位

850		32			15			None			None			无			无	
851		32			15			Volts DC		Volts DC		伏特DC		伏特DC	
852		32			15			Volts AC		Volts AC		伏特AC		伏特AC	
853		32			15			Amps DC			Amps DC			安培DC		安培DC	
854		32			15			Amps AC			Amps AC			安培AC		安培AC	
855		32			15			Watts			Watts			瓦特		瓦特	
856		32			15			KW				KW				千瓦 		千瓦	
857		32			15			Gallons			Gallons			加仑		加仑	
858		32			15			Liters			Liters			升			升	
859		32			15			PSI				PSI				PSI			PSI	
860		32			15			CFM				CFM				CFM			CFM		
861		32			15			RPM				RPM				RPM			RPM	
862		32			15			BTU				BTU				BTU			BTU
863		32			15			HP				HP				HP			HP	
864		32			15			W/hr			W/hr			W/hr		W/hr	
865		32			15			KW/hr			KW/hr			KW/hr		KW/hr
866		32			15			None			None			无			无

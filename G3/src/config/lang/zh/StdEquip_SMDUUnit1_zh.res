﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Bus Bar Voltage				Bus Bar Voltage		母排电压		母排电压
2	32			15			Load 1					Load 1			负载电流1		负载电流1
3	32			15			Load 2					Load 2			负载电流2		负载电流2
4	32			15			Load 3					Load 3			负载电流3		负载电流3
5	32			15			Load 4					Load 4			负载电流4		负载电流4
6	32			15			Load Fuse 1				Load Fuse 1		负载熔丝1		负载熔丝1
7	32			15			Load Fuse 2				Load Fuse 2		负载熔丝2		负载熔丝2
8	32			15			Load Fuse 3				Load Fuse 3		负载熔丝3		负载熔丝3
9	32			15			Load Fuse 4				Load Fuse 4		负载熔丝4		负载熔丝4
10	32			15			Load Fuse 5				Load Fuse 5		负载熔丝5		负载熔丝5
11	32			15			Load Fuse 6				Load Fuse 6		负载熔丝6		负载熔丝6
12	32			15			Load Fuse 7				Load Fuse 7		负载熔丝7		负载熔丝7
13	32			15			Load Fuse 8				Load Fuse 8		负载熔丝8		负载熔丝8
14	32			15			Load Fuse 9				Load Fuse9		负载熔丝9		负载熔丝9
15	32			15			Load Fuse 10				Load Fuse 10		负载熔丝10		负载熔丝10
16	32			15			Load Fuse 11				Load Fuse 11		负载熔丝11		负载熔丝11
17	32			15			Load Fuse 12				Load Fuse 12		负载熔丝12		负载熔丝12
18	32			15			Load Fuse 13				Load Fuse 13		负载熔丝13		负载熔丝13
19	32			15			Load Fuse 14				Load Fuse 14		负载熔丝14		负载熔丝14
20	32			15			Load Fuse 15				Load Fuse 15		负载熔丝15		负载熔丝15
21	32			15			Load Fuse 16				Load Fuse 16		负载熔丝16		负载熔丝16
22	32			15			Run Time				Run Time		运行时间		运行时间
23	32			15			LV Disconnect 1 Control			LVD 1 Control		LVD1控制		LVD1控制
24	32			15			LV Disconnect 2 Control			LVD 2 Control		LVD2控制		LVD2控制
25	32			15			LV Disconnect 1 Voltage			LVD 1 Voltage		LVD1电压		LVD1电压
26	32			15			LV Resconnect 1 Voltage			LVR 1 Voltage		LVR1电压		LVR1电压
27	32			15			LV Disconnect 2 Voltage			LVD 2 Voltage		LVD2电压		LVD2电压
28	32			15			LV Resconnect 2 Voltage			LVR 2 Voltage		LVR2电压		LVR2电压
29	32			15			On					On			正常			正常
30	32			15			Off					Off			断开			断开
31	32			15			Normal					Normal			正常			正常
32	32			15			Error					Error			错误			错误
33	32			15			On					On			闭合			闭合
34	32			15			Fuse 1 Alarm				Fuse 1 Alarm		负载熔丝1告警		熔丝1告警
35	32			15			Fuse 2 Alarm				Fuse 2 Alarm		负载熔丝2告警		熔丝2告警
36	32			15			Fuse 3 Alarm				Fuse 3 Alarm		负载熔丝3告警		熔丝3告警
37	32			15			Fuse 4 Alarm				Fuse 4 Alarm		负载熔丝4告警		熔丝4告警
38	32			15			Fuse 5 Alarm				Fuse 5 Alarm		负载熔丝5告警		熔丝5告警
39	32			15			Fuse 6 Alarm				Fuse 6 Alarm		负载熔丝6告警		熔丝6告警
40	32			15			Fuse 7 Alarm				Fuse 7 Alarm		负载熔丝7告警		熔丝7告警
41	32			15			Fuse 8 Alarm				Fuse 8 Alarm		负载熔丝8告警		熔丝8告警
42	32			15			Fuse 9 Alarm				Fuse 9 Alarm		负载熔丝9告警		熔丝9告警
43	32			15			Fuse 10 Alarm				Fuse 10 Alarm		负载熔丝10告警		熔丝10告警
44	32			15			Fuse 11 Alarm				Fuse 11 Alarm		负载熔丝11告警		熔丝11告警
45	32			15			Fuse 12 Alarm				Fuse 12 Alarm		负载熔丝12告警		熔丝12告警
46	32			15			Fuse 13 Alarm				Fuse 13 Alarm		负载熔丝13告警		熔丝13告警
47	32			15			Fuse 14 Alarm				Fuse 14 Alarm		负载熔丝14告警		熔丝14告警
48	32			15			Fuse 15 Alarm				Fuse 15 Alarm		负载熔丝15告警		熔丝15告警
49	32			15			Fuse 16 Alarm				Fuse 16 Alarm		负载熔丝16告警		熔丝16告警
50	32			15			HW Test Alarm				HW Test Alarm		硬件自检告警		硬件自检告警                                                                                                                                     
51	32			15			SMDU 1					SMDU 1			SMDU 1			SMDU 1
52	32			15			Battery Fuse 1 Voltage			BattFuse 1 Volt		电池熔丝1电压		电池熔丝1电压
53	32			15			Battery Fuse 2 Voltage			BattFuse 2 Volt		电池熔丝2电压		电池熔丝2电压
54	32			15			Battery Fuse 3 Voltage			BattFuse 3 Volt		电池熔丝3电压		电池熔丝3电压
55	32			15			Battery Fuse 4 Voltage			BattFuse 4 Volt		电池熔丝4电压		电池熔丝4电压
56	32			15			Battery Fuse 1 Status			Batt Fuse 1		电池熔丝1状态		电池熔丝1状态
57	32			15			Battery Fuse 2 Status			Batt Fuse 2		电池熔丝2状态		电池熔丝2状态
58	32			15			Battery Fuse 3 Status			Batt Fuse 3		电池熔丝3状态		电池熔丝3状态
59	32			15			Battery Fuse 4 Status			Batt Fuse 4		电池熔丝4状态		电池熔丝4状态
60	32			15			On					On			正常			正常
61	32			15			Off					Off			断开			断开
62	32			15			Battery Fuse 1 Alarm			BattFuse 1 Alm		电池熔丝1告警		电池熔丝1告警
63	32			15			Battery Fuse 2 Alarm			BattFuse 2 Alm		电池熔丝2告警		电池熔丝2告警
64	32			15			Battery Fuse 3 Alarm			BattFuse 3 Alm		电池熔丝3告警		电池熔丝3告警
65	32			15			Battery Fuse 4 Alarm			BattFuse 4 Alm		电池熔丝4告警		电池熔丝4告警
66	32			15			All Load Current			All Load Curr		负载总电流		负载总电流
67	32			15			Over Current Point(Load)		Over Curr Point		负载总电流过流点	过流点
68	32			15			Over Current(Load)			Over Current		负载总电流过流		过流
69	32			15		LVD 1					LVD 1			LVD1允许		LVD1允许
70	32			15		LVD 1 Mode				LVD 1 Mode		LVD1方式		LVD1方式
71	32			15		LVD 1 Reconnect Delay			LVD1 ReconDelay		LVD1上电延迟		LVD1上电延迟
72	32			15		LVD 2					LVD 2			LVD2允许		LVD2允许
73	32			15		LVD 2 Mode				LVD2 Mode		LVD2方式		LVD2方式
74	32			15		LVD 2 Reconnect Delay			LVD2 ReconDelay		LVD2上电延迟		LVD2上电延迟
75	32			15			LVD 1 Status				LVD 1 Status		LVD1状态		LVD1状态
76	32			15			LVD 2 Status				LVD 2 Status		LVD2状态		LVD2状态
77	32			15			Disabled				Disabled		禁止			禁止
78	32			15			Enabled					Enabled			允许			允许
79	32			15			Voltage					Voltage			电压方式		电压方式
80	32			15			Time					Time			时间方式		时间方式
81	32			15			Bus Bar Voltage Alarm			Bus Bar Alarm		母排电压告警		母排电压告警
82	32			15			Normal					Normal			正常			正常
83	32			15			Low					Low			低于下限		低于下限
84	32			15			High					High			高于上限		高于上限
85	32			15			Under Voltage				Under Voltage		欠压			欠压
86	32			15			Over Voltage				Over Voltage		过压			过压
87	32			15			Shunt 1 Current Alarm			Shunt 1 Alarm		分流器1告警		分流器1告警
88	32			15			Shunt 2 Current Alarm			Shunt 2 Alarm		分流器2告警		分流器2告警
89	32			15			Shunt 3 Current Alarm			Shunt 3 Alarm		分流器3告警		分流器3告警
90	32			15			Shunt 4 Current Alarm			Shunt 4 Alarm		分流器4告警		分流器4告警
91	32			15			Shunt 1 Over Current			Shunt 1 OverCur		分流器1过流		分流器1过流
92	32			15			Shunt 2 Over Current			Shunt 2 OverCur		分流器2过流		分流器2过流
93	32			15			Shunt 3 Over Current			Shunt 3 OverCur		分流器3过流		分流器3过流
94	32			15			Shunt 4 Over Current			Shunt 4 OverCur		分流器4过流		分流器4过流
95	32			15			Times of Communication Fail		Times Comm Fail		通信失败次数		通信失败次数
96	32			15			Existent				Existent		存在			存在
97	32			15			Not Existent				Not Existent		不存在			不存在
98	32			15			Very Low				Very Low		低于下下限		低于下下限
99	32			15			Very High				Very High		高于上上限		高于上上限
100	32			15			Switch					Switch			Swich			Swich
101	32			15			LVD1 Failure				LVD 1 Failure		LVD1控制失败		LVD1控制失败
102	32			15			LVD2 Failure				LVD 2 Failure		LVD2控制失败		LVD2控制失败
103	32			15			High Temperature Disconnect 1	HTD 1		HTD1高温下电允许	HTD1下电允许
104	32			15			High Temperature Disconnect 2	HTD 2		HTD2高温下电允许	HTD2下电允许
105	32			15			Battery LVD				Battery LVD		电池下电		电池下电
106	32			15			No Battery				No Battery		无电池			无电池
107	32			15			LVD 1					LVD 1			LVD1			LVD1
108	32			15			LVD 2					LVD 2			LVD2			LVD2
109	32			15			Battery Always On			Batt Always On		有电池但不下电		有电池但不下电
110	32			15			Barcode					Barcode			Barcode			Barcode
111	32			15			DC Over Voltage				DC Over Volt		直流过压点		直流过压点
112	32			15			DC Under Voltage			DC Under Volt		直流欠压点		直流欠压点
113	32			15			Over Current 1				Over Curr 1		过流点1			过流点1
114	32			15			Over Current 2				Over Curr 2		过流点2			过流点2
115	32			15			Over Current 3				Over Curr 3		过流点3			过流点3
116	32			15			Over Current 4				Over Curr 4		过流点4			过流点4
117	32			15			Existence State				Existence State		是否存在		是否存在
118	32			15			Communication Fail			Comm Fail		通讯中断		通讯中断
119	32			15			Bus Voltage Status			Bus Volt Status		电压状态		电压状态
120	32			15			Comm OK					Comm OK			通讯正常		通讯正常
121	32			15			All Batteries Comm Fail			AllBattCommFail		都通讯中断		都通讯中断
122	32			15			Communication Fail			Comm Fail		通讯中断		通讯中断
123	32			15			Rated Capacity				Rated Capacity		标称容量		标称容量	
124	32			15			Load 5					Load 5			负载电流5		负载电流5
125	32			15			Shunt 1 Voltage				Shunt 1 Voltage		分流器1电压		分流器1电压
126	32			15			Shunt 1 Current				Shunt 1 Current			分流器1电流		分流器1电流
127	32			15			Shunt 2 Voltage				Shunt 2 Voltage		分流器2电压		分流器2电压
128	32			15			Shunt 2 Current				Shunt 2 Current			分流器2电流		分流器2电流
129	32			15			Shunt 3 Voltage				Shunt 3 Voltage		分流器3电压		分流器3电压
130	32			15			Shunt 3 Current				Shunt 3 Current			分流器3电流		分流器3电流
131	32			15			Shunt 4 Voltage				Shunt 4 Voltage		分流器4电压		分流器4电压
132	32			15			Shunt 4 Current				Shunt 4 Current			分流器4电流		分流器4电流
133	32			15			Shunt 5 Voltage				Shunt 5 Voltage		分流器5电压		分流器5电压
134	32			15			Shunt 5 Current				Shunt 5 Current			分流器5电流		分流器5电流
170	32			15			Current1 High 1 Current		Curr1 Hi1		电流1过流		电流1过流																																															
171	32			15			Current1 High 2 Current		Curr1 Hi2		电流1过过流		电流1过过流																																															
172	32			15			Current2 High 1 Current			Curr2 Hi1		电流2过流		电流2过流																																															
173	32			15			Current2 High 2 Current			Curr2 Hi2		电流2过过流		电流2过过流																																														
174	32			15			Current3 High 1 Current			Curr3 Hi1		电流3过流		电流3过流																																															
175	32			15			Current3 High 2 Current			Curr3 Hi2		电流3过过流		电流3过过流																																															
176	32			15			Current4 High 1 Current			Curr4 Hi1		电流4过流		电流4过流																																															
177	32			15			Current4 High 2 Current			Curr4 Hi2		电流4过过流		电流4过过流																																															
178	32			15			Current5 High 1 Current			Curr5 Hi1		电流5过流		电流5过流																																															
179	32			15			Current5 High 2 Current			Curr5 Hi2		电流5过过流		电流5过过流																																															
220	32			15			Current1 High 1 Current Limit		Curr1 Hi1 Lmt		电流1过流点		电流1过流点																															
221	32			15			Current1 High 2 Current Limit		Curr1 Hi2 Lmt		电流1过过流点		电流1过过流点																															
222	32			15			Current2 High 1 Current Limit		Curr2 Hi1 Lmt		电流2过流点		电流2过流点																															
223	32			15			Current2 High 2 Current Limit		Curr2 Hi2 Lmt		电流2过过流点		电流2过过流点																															
224	32			15			Current3 High 1 Current Limit		Curr3 Hi1 Lmt		电流3过流点		电流3过流点																															
225	32			15			Current3 High 2 Current Limit		Curr3 Hi2 Lmt		电流3过过流点		电流3过过流点																															
226	32			15			Current4 High 1 Current Limit		Curr4 Hi1 Lmt		电流4过流点		电流4过流点																															
227	32			15			Current4 High 2 Current Limit		Curr4 Hi2 Lmt		电流4过过流点		电流4过过流点																															
228	32			15			Current5 High 1 Current Limit		Curr5 Hi1 Lmt		电流5过流点		电流5过流点																															
229	32			15			Current5 High 2 Current Limit		Curr5 Hi2 Lmt		电流5过过流点		电流5过过流点																														
270	32			15			Current1 Break Value			Curr 1 Brk Val			电流1电流告警阈值	电流1电流告警阈值																														
271	32			15			Current2 Break Value			Curr 2 Brk Val			电流2电流告警阈值	电流2电流告警阈值																														
272	32			15			Current3 Break Value			Curr 3 Brk Val			电流3电流告警阈值	电流3电流告警阈值																														
273	32			15			Current4 Break Value			Curr 4 Brk Val			电流4电流告警阈值	电流4电流告警阈值																														
274	32			15			Current5 Break Value			Curr 5 Brk Val			电流5电流告警阈值	电流5电流告警阈值																														
281	32			15			Set Shunt Coefficient			Set Shunt Coeff			分流器系数设置开关		分流器系数开关																														
#282	32			15			Shunt 5 Coefficient Display		Shunt5 Display		分流器系数5是否显示		系数5是否显示																														
283	32			15			Shunt1 Coefficient Conflict		Shunt1 Conflict		分流器系数1改变		分流器系数1改变																															
284	32			15			Shunt2 Coefficient Conflict		Shunt2 Conflict		分流器系数2改变		分流器系数2改变																														
285	32			15			Shunt3 Coefficient Conflict		Shunt3 Conflict		分流器系数3改变		分流器系数3改变																															
286	32			15			Shunt4 Coefficient Conflict		Shunt4 Conflict		分流器系数4改变		分流器系数4改变																															
287	32			15			Shunt5 Coefficient Conflict		Shunt5 Conflict		分流器系数5改变		分流器系数5改变																															
290	32			15			By Software				By Software				软件设置			软件设置																														
291	32			15			By Dip-Switch				By Dip-Switch				拨码设置			拨码设置																														
292	32			15			Not Supported				Not Supported				不支持				不支持
293	32			15			Not Used				Not Used				不使用				不使用
294	32			15			General				General				通用					通用
295	32			15			Load						Load						负载					负载
296	32			15			Battery					Battery					电池					电池
297	32			15			Shunt1 Set As			Shunt1SetAs			分流器1设置为			分流器1设置为
298	32			15			Shunt2 Set As			Shunt2SetAs			分流器2设置为			分流器2设置为
299	32			15			Shunt3 Set As			Shunt3SetAs			分流器3设置为			分流器3设置为
300	32			15			Shunt4 Set As			Shunt4SetAs			分流器4设置为			分流器4设置为
301	32			15			Shunt5 Set As			Shunt5SetAs			分流器5设置为			分流器5设置为
302	32			15			Shunt 1		Shunt 1		分流器1读数				分流器1读数
303	32			15			Shunt 2		Shunt 2		分流器2读数				分流器2读数
304	32			15			Shunt 3		Shunt 3		分流器3读数				分流器3读数
305	32			15			Shunt 4		Shunt 4		分流器4读数				分流器4读数
306	32			15			Shunt 5		Shunt 5		分流器5读数				分流器5读数
307	32			15			Source 1		Source 1		来源1读数				来源1读数
308	32			15			Source 2		Source 2		来源2读数				来源2读数
309	32			15			Source 3		Source 3		来源3读数				来源3读数
310	32			15			Source 4		Source 4		来源4读数				来源4读数
311	32			15			Source 5		Source 5		来源5读数				来源5读数

500	32			15			Current1 High 1 Curr			Curr1 Hi1Cur			电流1过流		电流1过流																																															
501	32			15			Current1 High 2 Curr			Curr1 Hi2Cur		电流1过过流		电流1过过流																																															
502	32			15			Current2 High 1 Curr			Curr2 Hi1Cur			电流2过流		电流2过流																																															
503	32			15			Current2 High 2 Curr			Curr2 Hi2Cur		电流2过过流		电流2过过流																																														
504	32			15			Current3 High 1 Curr			Curr3 Hi1Cur			电流3过流		电流3过流																																															
505	32			15			Current3 High 2 Curr			Curr3 Hi2Cur		电流3过过流		电流3过过流																																															
506	32			15			Current4 High 1 Curr			Curr4 Hi1Cur			电流4过流		电流4过流																																															
507	32			15			Current4 High 2 Curr			Curr4 Hi2Cur		电流4过过流		电流4过过流																																															
508	32			15			Current5 High 1 Curr			Curr5 Hi1Cur			电流5过流		电流5过流																																															
509	32			15			Current5 High 2 Curr			Curr5 Hi2Cur		电流5过过流		电流5过过流			
550	32			15			Source					Source					来源					来源

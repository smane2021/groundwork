﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#

    
[LOCALE_LANGUAGE]
zh

[RES_INFO]
#R_ID	MAX_LEN_FULL	MAX_LEN_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32		16		Power System			Power System		电源系统			电源系统
2	32		15		System Voltage			Sys Volt		系统电压			系统电压
3	32		15		System Load			System Load		负载电流			负载电流
4	32		15		System Power			System Power		系统功率			系统功率
5	32		15		Total Power Consumption		Pwr Consumption		功率消耗			功率消耗
6	32		15		Power Peak in 24 Hours		Power Peak		24小时功率峰值			24小时功率峰值          
7	32		15		Average Power in 24 Hours	Ave Power		24小时功率均值			24小时功率均值          
8	32		15		Hardware Write-protect Switch	Hardware Switch		硬件写保护开关			硬件开关状态            
9	32		15		Ambient Temperature		Amb Temp			环境温度			环境温度                
18	32		15		DO 1-DO 1			DO 1-DO 1		继电器1				继电器1                 
19	32		15		DO 2-DO 2			DO 2-DO 2		继电器2				继电器2                 
20	32		15		DO 3-DO 3			DO 3-DO 3		继电器3				继电器3                 
21	32		15		DO 4-DO 4			DO 4-DO 4		继电器4				继电器4                 
22	32		15		DO 5-DO 5			DO 5-DO 5		继电器5				继电器5                 
23	32		15		DO 6-DO 6			DO 6-DO 6		继电器6				继电器6                 
24	32		15		DO 7-DO 7			DO 7-DO 7		继电器7				继电器7                 
25	32		15		DO 8-DO 8			DO 8-DO 8		继电器8				继电器8                
27	32		15		Under Voltage 1 Level		Under Voltage 1		直流电压低			直流电压低              
28	32		15		Under Voltage 2 Level		Under Voltage 2		欠压				欠压                    
29	32		15		Over Voltage 1 Level		Over Voltage 1		过压				过压                    
31	32		15		Temperature 1 High 1		Temp 1 High 1		温度1高温点			温度1高温点          
32	32		15		Temperature 1 Low 1		Temp 1 Low 1		温度1低温点			温度1低温点          
33	32		15		Auto/Manual State		Auto/Man State		管理方式			管理方式              
34	32		15		Outgoing Alarms Blocked	Alarm Blocked		外出告警阻塞			外出告警阻塞            
35	32		15		Supervision Unit Fail		SupV Unit Fail		监控故障			监控故障                 
36	32		15		CAN Communication Failure	CAN Comm Fail		CAN通讯故障			CAN通讯故障             
37	32		15		Mains Failure			Mains Failure		市电停电			市电停电                
38	32		15		Under Voltage 1			Under Voltage 1		直流电压低告警			直流电压低告警          
39	32		15		Under voltage 2			Under Voltage 2		直流欠压告警			直流欠压告警            
40	32		15		Over Voltage			Over Voltage		直流过压告警			直流过压告警            
41	32		15		High Temperature 1	High Temp 1		温度1高温告警			温度1高温告警            
42	32		15		Low Temperature 1		Low Temp 1		温度1低温告警			温度1低温告警
43	32		15		Temperature 1 Sensor Fail	T1 Sensor Fail		温度1传感器故障			温度1传感器故障
44	32		15		Outgoing Alarms Blocked		Alarm Blocked		外出告警阻塞			外出告警阻塞
45	32		15		Maintenance Time Limit Alarm	Mtnc Time Alarm		维护时间到			维护时间到
46	32		15		Unprotected			Unprotected		不保护				不保护
47	32		15		Protected			Protected		保护				保护                    
48	32		15		Normal				Normal			正常				正常                    
49	32		15		Fail				Fail			故障				故障                    
50	32		15		Off				Off			关				关                      
51	32		15		On				On			开				开                      
52	32		15		Off				Off			关				关                      
53	32		15		On				On			开				开                      
54	32		15		Off				Off			关				关                      
55	32		15		On				On			开				开                      
56	32		15		Off				Off			关				关                      
57	32		15		On				On			开				开                      
58	32		15		Off				Off			关				关                      
59	32		15		On				On			开				开                      
60	32		15		Off				Off			关				关                      
61	32		15		On				On			开				开                      
62	32		15		Off				Off			关				关                      
63	32		15		On				On			开				开                      
64	32		15		Open				Open			断开				断开                      
65	32		15		Closed				Closed			闭合				闭合                      
66	32		15		Open				Open			断开				断开                      
67	32		15		Closed				Closed			闭合				闭合
78	32		15		Off				Off			关				关
79	32		15		On				On			开				开
80	32		15		Auto				Auto			自动				自动
81	32		15		Manual				Manual			手动				手动                    
82	32		15		Normal				Normal			正常				正常                    
83	32		15		Blocked				Blocked			阻塞				阻塞
85	32		15		Set to Auto Mode		To Auto Mode		切换至自动状态			切换至自动态
86	32		15		Set to Manual Mode		To Manual Mode		切换至手动状态			切换至手动态            
87	32		15		Power Rate Level		PowerRate Level		电价费率			电价费率                
88	32		15		Power Peak Limit		Power Limit		功率限值			功率限值                
89	32		15		Peak				Peak			最高				最高                    
90	32		15		High				High			高				高                      
91	32		15		Flat				Flat			一般				一般                    
92	32		15		Low				Low			低				低                      
93	32		15		Lower Consumption		Lwr Consumption		高费限电使能			高费限电使能            
94	32		15		Power Peak Savings		P Peak Savings		最大限功率使能			限功率使能              
95	32		15		Disabled			Disabled		不使能				不使能                  
96	32		15		Enabled				Enabled			使能				使能                    
97	32		15		Disabled			Disabled		不使能				不使能                  
98	32		15		Enabled				Enabled			使能				使能                    
99	32		15		Over Maximum Power		Over Power		功率越限			功率越限                
100	32		15		Normal				Normal			正常				正常                    
101	32		15		Alarm				Alarm			告警				告警                    
102	32		15		Over Maximum Power Alarm	Over Power		功率越限			功率越限                
104	32		15		System Alarm Status		Alarm Status		系统告警状态			系统告警状态                
105	32		15		No Alarm			No Alm		正常				正常                    
106	32		15		Observation Alarm		Observation		一般告警			一般告警                
107	32		15		Major Alarm			Major			重要告警			重要告警                
108	32		15		Critical Alarm			Critical		紧急告警			紧急告警                
109	32		15		Maintenance Run Time		Mtnc Run Time		未维护已运行时间		未维护已运行            
110	32		15		Maintenance Cycle Time		Mtnc Cycle Time		维护周期限			维护周期限		
111	32		15		Maintenance Time Delay		Mtnc Time Delay		需维护告警延时			维护告警延时                    
112	32		15		Maintenance Time Out		Mtnc Time Out		系统维护时间到			维护时间到              
113	32		15		No				No			否				否                      
114	32		15		Yes				Yes			是				是                      
115	32		15		Power Split Mode		Power Split		Power Split模式			Power Split模式                
116	32		15		Slave Current Limit Value	Slave Curr Lmt		Power Split正常限流点		正常限流点	
117	32		15		Delta Voltage			Delta Volt		Power Split浮充电压偏移		浮充电压偏移	
118	32		15		Proportional Coefficient	Proportion Coef		Power Split比例系数		比例系数	
119	32		15		Integral Time			Integral Time		Power Split积分时间		积分时间                
120	32		15		Master Mode			Master			主机模式			主机模式                
121	32		15		Slave Mode			Slave			从机模式			从机模式                
122	32		15		MPCL Control Step		MPCL Ctrl Step		MPCL控制步长			MPCL控制步长            
123	32		15		MPCL Pwr Range			MPCL Pwr Range		MPCL控制回差			MPCL控制回差            
124	32		15		MPCL Battery Discharge On	MPCL BatDsch On		MPCL电池放电允许		MPCL放电允许            
125	32		15		MPCL Diesel Control On		MPCL DslCtrl On		MPCL油机控制使能		MPCL油机控使能          
126	32		15		Disabled			Disabled		禁止				禁止                    
127	32		15		Enabled				Enabled			允许				允许                    
128	32		15		Disabled			Disabled		不使能				不使能                  
129	32		15		Enabled				Enabled			使能				使能                    
130	32		15		System Time			System Time		系统时间			系统时间                
131	32		15		LCD Language			LCD Language		LCD当前语言			LCD当前语言             
#changed by Frank Wu,1/1,20140221 for TR196 which text “Alarm Voice” should change to “Audible alarm”
#132	32		15		LCD Alarm Voice			LCD Alarm Voice		蜂鸣器告警音长			告警音长             
132	32		15		Audible Alarm			Audible Alarm		蜂鸣器告警音长			告警音长             
133	32		15		English				English			英语				英语                    
134	32		15		Simple Chinese			Chinese			中文				中文                    
135	32		15		On				On			常开				常开                    
136	32		15		Off				Off			常闭				常闭                    
137	32		15		3 min				3 min			3分钟				3分钟                   
138	32		15		10 min				10 min			10分钟				10分钟                  
139	32		15		1 hour				1 h			1小时				1小时                   
140	32		15		4 hours				4 h			4小时				4小时                   
141	32		15		Clear Maintenance Run Time	Clr MtncRunTime		清除未维护已运行时间		清除已运行时间          
142	32		15		No				No			否				否                      
143	32		15		Yes				Yes			是				是                      
144	32		15		Alarm				Alarm			告警				告警                    
145	32		15		HW Status			HW Status		硬件状态			硬件状态             
146	32		15		Normal				Normal			正常				正常                    
147	32		15		Config Error			Config Error		配置错误			配置错误                
148	32		15		Flash Fail			Flash Fail		写Flash错误			写Flash错误             
150	32		15		LCD Time Zone			Time Zone		显示时区			显示时区                
151	32		15		Time Sync Main Time Server	Time Sync Svr		时间同步主服务器		时间同步主IP            
152	32		15		Time Sync Backup Time Server	Backup Sync Svr		时间同步备份服务器		时间同步备份IP          
153	32		15		Time Sync Interval		Sync Interval		时间同步间隔			时间同步间隔            
155	32		15		Reset SCU			Reset SCU		重启SCU				重启SCU                 
156	32		15		Running Config Type		Config Type		当前运行配置			当前运行配置            
157	32		15		Normal Config			Normal Config		当前配置			当前配置                
158	32		15		Backup Config			Backup Config		备份配置			备份配置                
159	32		15		Default Config			Default Config		缺省配置			缺省配置                
160	32		15		Config Error(Backup Config)	Config Error 1		配置错误			配置错误               
161	32		15		Config Error(Default Config)	Config Error 2		备份配置错误			备份配置错误               
162	32		15		Control System Alarm LED	Ctrl Sys Alarm		控制系统告警灯			控制系统告警灯	
163	32		15		Imbalance System Current	Imbalance Curr		系统电流不平衡			系统电流不平衡	
164	32		15		Normal				Normal			正常				正常                    
165	32		15		Abnormal			Abnormal		异常				异常                    
167	32		15		MainSwitch Block Condition	MnSw Block Cond		MainSwitch阻塞条件		MS阻塞条件              
168	32		15		No				No			否				否                      
169	32		15		Yes				Yes			是				是                      
170	32		15		Total Run Time			Total Run Time		总运行时间			总运行时间              
171	32		15		Total Alarm Number		Total Alm Num		当前告警总数			当前告警总数	
172	32		15		Total OA Number			Total OA Num		一般告警数			一般告警数	
173	32		15		Total MA Number			Total MA Num		重要告警数			重要告警数	
174	32		15		Total CA Number			Total CA Num		紧急告警数			紧急告警数              
175	32		15		SPD Fail			SPD Fail		防雷器故障			防雷器故障              
176	32		15		Over Voltage 2 Level		Over Voltage 2		过压2				过压2                   
177	32		15		Very Over Voltage		Very Over Volt		直流过过压告警			直流过过压告警          
178	32		15		Regulation Voltage		Regulate Volt		校压使能			校压使能                
179	32		15		Regulation Voltage Range	Regu Volt Range		校压范围			校压范围                
180	32		15		Keypad Voice			Keypad Voice		按键声音			按键声音                
181	32		15		On				On			开				开                      
182	32		15		Off				Off			关				关                      
183	32		15		Load Shunt Full Current		Load Shunt Curr		负载分流器电流			负载分流器电流          
184	32		15		Load Shunt Full Voltage		Load Shunt Volt		负载分流器电压			负载分流器电压          
185	32		15		Bat 1 Shunt Full Current	Bat1 Shunt Curr		Bat1分流器电流			Bat1分流器电流          
186	32		15		Bat 1 Shunt Full Voltage	Bat1 Shunt Volt		Bat1分流器电压			Bat1分流器电压          
187	32		15		Bat 2 Shunt Full Current	Bat2 Shunt Curr		Bat2分流器电流			Bat2分流器电流          
188	32		15		Bat 2 Shunt Full Voltage	Bat2 Shunt Volt		Bat2分流器电压			Bat2分流器电压          
219	32		15		Relay 6			Relay 6			继电器6				继电器6          
220	32		15		Relay 7			Relay 7			继电器7				继电器7             
221	32		15		Relay 8			Relay 8			继电器8				继电器8       
222	32		15		RS232 Baud Rate			RS232 Baud Rate		后台串口波特率			后台串口波特率         
223	32		15		Local Address			Local Address		本机地址			本机地址           
224	32		15		Callback Number			Callback Number		回叫次数			回叫次数           
225	32		15		Interval Time of Callback	Interval		回叫间隔			回叫间隔       
#226	32		15		Communication Password		Comm Password		密码校验			密码校验       
227	32		15		DC Data Flag (On-Off)		DC Data Flag 1		直流屏标志1			直流屏标志1
228	32		15		DC Data Flag (Alarm)		DC Data Flag 2		直流屏标志2			直流屏标志2
229	32		15		Relay Reporting			Relay Reporting		继电器告警方式			继电器告警方式
230	32		15		Fixed				Fixed			固定				固定
231	32		15		User Defined			User Defined		用户自定义			用户自定义
232	32		15		Rect Data Flag (Alarm)		RectDataFlag 2		模块标志位2			模块标志位2
233	32		15		Master Controlled		Master Ctrl		主机控制			主机控制
234	32		15		Slave Controlled		Slave Ctrl		从机控制			从机控制
236	32		15		Over Load Alarm Point		Over Load Point		负载过重告警点			负载过重告警点
237	32		15		Over Load			Over Load		负载过重			负载过重
238	32		15		System Data Flag (On-Off)	Sys Data Flag 1		系统标志1			系统标志1
239	32		15		System Data Flag (Alarm)	Sys Data Flag 2		系统标志2			系统标志2
240	32		15		Chinese Language		Chinese			中文			中文
241	32		15		Imbalance Protection		Imb Protect		不平衡保护允许			不平衡保护允许      
242	32		15		Enabled				Enabled			允许				允许
243	32		15		Disabled			Disabled		禁止				禁止
244	32		15		Buzzer Control			Buzzer Control		蜂鸣器控制			蜂鸣器控制
245	32		15		Normal				Normal			正常				正常
246	32		15		Disabled			Disabled		消音				消音
247	32		15		Ambient Temp Alarm		Temp Alarm		环境温度告警			环境温度告警
248	32		15		Normal				Normal			正常				正常
249	32		15		Low				Low			低				低
250	32		15		High				High			高				高
251	32		15		Reallocate Rect Address		Reallocate Addr		重排模块地址			重排模块地址
252	32		15		Normal				Normal			正常				正常
253	32		15		Reallocate Address		Reallocate Addr		重排地址			重排地址
254	32		15		Test State Set			Test State Set		测试状态设置			测试状态设置
255	32		15		Normal				Normal			正常状态			正常状态
256	32		15		Test State			Test State		测试状态			测试状态
257	32		15		Minification for Test		Minification		测试时间缩小倍数		测试缩小倍数
258	32		15		Digital Input 1			DI 1			数字输入1			数字输入1
259	32		15		Digital Input 2			DI 2			数字输入2			数字输入2
260	32		15		Digital Input 3			DI 3			数字输入3			数字输入3
261	32		15		Digital Input 4			DI 4			数字输入4			数字输入4
262	32		15		System Type Value		Sys Type Value		系统类型特征值			系统类型特征值
263	32		15		AC/DC Board Type		AC/DC BoardType		交直流板类型			交直流板类型          
264	32		15		B14C3U1				B14C3U1			B14C3U1				B14C3U1
265	32		15		Large DU			Large DU		Large DU			Large DU
266	32		15		SPD				SPD			SPD				SPD
267	32		15		Temp1				Temp1			温度1				温度1
268	32		15		Temp2				Temp2			温度2				温度2
269	32		15		Temp3				Temp3			温度3				温度3
270	32		15		Temp4				Temp4			温度4				温度4
271	32		15		Temp5				Temp5			温度5				温度5
272	32		15		Auto Mode			Auto Mode		自动模式			自动模式
273	32		15		EMEA				EMEA			欧洲				欧洲
274	32		15		Normal				Normal			普通				普通
275	32		15		Nominal Voltage			Nom Voltage		系统电压			系统电压
276	32		15		EStop/EShutdown			EStop/EShutdown		EStop/EShutdown			EStop/EShutdown
277	32		15		Shunt 1 Full Current		Shunt 1 Current		分流器1额定电流			分流器1额定电流
278	32		15		Shunt 2 Full Current		Shunt 2 Current		分流器2额定电流			分流器2额定电流
279	32		15		Shunt 3 Full Current		Shunt 3 Current		分流器3额定电流			分流器3额定电流
280	32		15		Shunt 1 Full Voltage		Shunt 1 Voltage		分流器1额定电压			分流器1额定电压
281	32		15		Shunt 2 Full Voltage		Shunt 2 Voltage		分流器2额定电压			分流器2额定电压
282	32		15		Shunt 3 Full Voltage		Shunt 3 Voltage		分流器3额定电压			分流器3额定电压
283	32		15		Temperature 1			Temp 1			温度1使能			温度1使能
284	32		15		Temperature 2			Temp 2			温度2使能			温度2使能
285	32		15		Temperature 3			Temp 3			温度3使能			温度3使能
286	32		15		Temperature 4			Temp 4			温度4使能			温度4使能
287	32		15		Temperature 5			Temp 5			温度5使能			温度5使能
288	32		15		Very High Temperature 1 Limit	Very Hi Temp 1		温度1过温点			温度1过温点
289	32		15		Very High Temperature 2 Limit	Very Hi Temp 2		温度2过温点			温度2过温点
290	32		15		Very High Temperature 3 Limit	Very Hi Temp 3		温度3过温点			温度3过温点
291	32		15		Very High Temperature 4 Limit	Very Hi Temp 4		温度4过温点			温度4过温点
292	32		15		Very High Temperature5 Limit	Very Hi Temp 5		温度5过温点			温度5过温点
293	32		15		High Temperature 2 Limit	High Temp 2		温度2高温点			温度2高温点          
294	32		15		High Temperature 3 Limit	High Temp 3		温度3高温点			温度3高温点          
295	32		15		High Temperature 4 Limit	High Temp 4		温度4高温点			温度4高温点          
296	32		15		High Temperature 5 Limit	High Temp 5		温度5高温点			温度5高温点          
297	32		15		Low Temperature 2 Limit		Low Temp 2		温度2低温点			温度2低温点          
298	32		15		Low Temperature 3 Limit		Low Temp 3		温度3低温点			温度3低温点          
299	32		15		Low Temperature 4 Limit		Low Temp 4		温度4低温点			温度4低温点          
300	32		15		Low Temperature 5 Limit		Low Temp 5		温度5低温点			温度5低温点          
301	32		15		Temperature 1 Not Used		Temp 1 Not Used		温度1未用			温度1未用
302	32		15		Temperature 2 Not Used		Temp 2 Not Used		温度2未用			温度2未用
303	32		15		Temperature 3 Not Used		Temp 3 Not Used		温度3未用			温度3未用
304	32		15		Temperature 4 Not Used		Temp 4 Not Used		温度4未用			温度4未用
305	32		15		Temperature 5 Not Used		Temp 5 Not Used		温度5未用			温度5未用
306	32		15		High Temperature 2		High Temp 2		温度2高温告警			温度2高温告警            
307	32		15		Low Temperature 2		Low Temp 2		温度2低温告警			温度2低温告警
308	32		15		Temperature 2 Sensor Fail	T2 Sensor Fail		温度2传感器故障			温度2传感器故障
309	32		15		High Temperature 3	High Temp 3		温度3高温告警			温度3高温告警            
310	32		15		Low Temperature 3		Low Temp 3		温度3低温告警			温度3低温告警
311	32		15		Temperature 3 Sensor Fail	T3 Sensor Fail		温度3传感器故障			温度3传感器故障
312	32		15		High Temperature 4	High Temp 4		温度4高温告警			温度4高温告警            
313	32		15		Low Temperature 4		Low Temp 4		温度4低温告警			温度4低温告警
314	32		15		Temperature 4 Sensor Fail	T4 Sensor Fail		温度4传感器故障			温度4传感器故障
315	32		15		High Temperature 5	High Temp 5		温度5高温告警			温度5高温告警            
316	32		15		Low Temperature 5		Low Temp 5		温度5低温告警			温度5低温告警
317	32		15		Temperature 5 Sensor Fail	T5 Sensor Fail		温度5传感器故障			温度5传感器故障
318	32		15		Very High Temperature 1		Very Hi Temp 1		温度1过温			温度1过过温
319	32		15		Very High Temperature 2		Very Hi Temp 2		温度2过温			温度2过过温
320	32		15		Very High Temperature 3		Very Hi Temp 3		温度3过温			温度3过过温
321	32		15		Very High Temperature 4		Very Hi Temp 4		温度4过温			温度4过过温
322	32		15		Very High Temperature 5		Very Hi Temp 5		温度5过温			温度5过过温
323	32		15		ECO Mode Status			ECO Mode Status		能量管理状态			能量管理状态
324	32		15		ECO Mode		ECO Mode	能量管理使能			能量管理使能
325	32		15		Internal Use (I2C)		Internal Use		内部使用(I2C)			内部使用
326	32		15		Disabled			Disabled		禁止				禁止
327	32		15		EStop				EStop			EStop				EStop
328	32		15		EShutdown			EShutdown		EShutdown			EShutdown
329	32		15		Ambient				Ambient			环境				环境
330	32		15		Battery				Battery			电池				电池
331	32		15		Temperature 6			Temperature 6		温度6				温度6
332	32		15		Temperature 7			Temperature 7		温度7				温度7
333	32		15		Temperature 6			Temp 6			温度6使能			温度6使能
334	32		15		Temperature 7			Temp 7			温度7使能			温度7使能
335	32		15		Very High Temperature 6 Limit	Very Hi Temp 6		温度6过过温点			温度6过过温点
336	32		15		Very High Temperature 7 Limit	Very Hi Temp 7		温度7过过温点			温度7过过温点
337	32		15		High Temperature6 Limit		High Temp 6		温度6高温点			温度6高温点          
338	32		15		High Temperature7 Limit		High Temp 7		温度7高温点			温度7高温点          
339	32		15		Low Temperature6 Limit		Low Temp 6		温度6低温点			温度6低温点          
340	32		15		Low Temperature7 Limit		Low Temp 7		温度7低温点			温度7低温点          
341	32		15		EIB Temp1 Not Used		EIB T1 Not Used		温度6未用			温度6未用
342	32		15		EIB Temp2 Not Used		EIB T2 Not Used		温度7未用			温度7未用
343	32		15		High Temperature 6		High Temp 6		温度6高温告警			温度6高温告警            
344	32		15		Low Temperature 6		Low Temp 6		温度6低温告警			温度6低温告警
345	32		15		Temperature6 Sensor Fail	T6 Sensor Fail		温度6传感器故障			温度6传感器故障
346	32		15		High Temperature 7		High Temp 7		温度7高温告警			温度7高温告警            
347	32		15		Low Temperature 7		Low Temp 7		温度7低温告警			温度7低温告警
348	32		15		Temperature 7 Sensor Fail	T7 Sensor Fail		温度7传感器故障			温度7传感器故障
349	32		15		Very High Temperature 6		Very Hi Temp 6		温度6过过温			温度6过过温
350	32		15		Very High Temperature 7		Very Hi Temp 7		温度7过过温			温度7过过温
1111	32		15		System Temp 1			System Temp1		系统温度1			系统温度1
1131	32		15		System Temp 1			System Temp1		系统温度1使能			系统温度1使能
1132	32		15		Battery Temperature 1 High 2	Batt Temp1 Hi2		温度1过温点Bat			温度1过温点Bat
1133	32		15		Battery Temperature 1 High 1	Batt Temp1 Hi1		温度1高温点Bat			温度1高温点Bat         
1134	32		15		Battery Temperature 1 Low	Batt Temp1 Low		温度1低温点Bat			温度1低温点Bat          
1141	32		15		System Temp 1 Not Used		Sys T1 Not Used		系统温度1未用			系统温度1未用
1142	32		15		System Temp 1 Sensor Fail	SysT1SensorFail		系统温度1故障			系统温度1故障
1143	32		15		Battery Temperature 1 High 2	Batt Temp1 Hi2		温度1过温Bat			温度1过温Bat
1144	32		15		Battery Temperature 1 High 1	Batt Temp1 Hi1		温度1高温Bat			温度1高温Bat          
1145	32		15		Battery Temperature 1 Low	Batt Temp1 Low		温度1低温Bat			温度1低温Bat
1211	32		15		System Temp 2			System Temp2		系统温度2			系统温度2
1231	32		15		System Temp 2			System Temp2		系统温度2使能			系统温度2使能
1232	32		15		Battery Temperature 2 High 2	Batt Temp2 Hi2		温度2过温点Bat			温度2过温点Bat
1233	32		15		Battery Temperature 2 High 1	Batt Temp2 Hi1		温度2高温点Bat			温度2高温点Bat         
1234	32		15		Battery Temperature 2 Low	Batt Temp2 Low		温度2低温点Bat			温度2低温点Bat          
1241	32		15		System Temp 2 Not Used		Sys T2 Not Used		系统温度2未用			系统温度2未用
1242	32		15		System Temp 2 Sensor Fail	SysT2SensorFail		系统温度2故障			系统温度2故障
1243	32		15		Battery Temperature 2 High 2	Batt Temp2 Hi2		温度2过温Bat			温度2过温Bat
1244	32		15		Battery Temperature 2 High 1	Batt Temp2 Hi1		温度2高温Bat			温度2高温Bat            
1245	32		15		Battery Temperature 2 Low	Batt Temp2 Low		温度2低温Bat			温度2低温Bat
1311	32		15		System Temp 3			System Temp3		系统温度3			系统温度3
1331	32		15		System Temp 3			System Temp3		系统温度3使能			系统温度3使能
1332	32		15		Battery Temperature 3 High 2	Batt Temp3 Hi2		温度3过温点Bat			温度3过温点Bat
1333	32		15		Battery Temperature 3 High 1	Batt Temp3 Hi1		温度3高温点Bat			温度3高温点Bat          
1334	32		15		Battery Temperature 3 Low	Batt Temp3 Low		温度3低温点Bat			温度3低温点Bat          
1341	32		15		System Temp 3 Not Used		Sys T3 Not Used		系统温度3未用			系统温度3未用
1342	32		15		System Temp 3 Sensor Fail	SysT3SensorFail		系统温度3故障			系统温度3故障
1343	32		15		Battery Temperature 3 High 2	Batt Temp3 Hi2		温度3过温Bat			温度3过温Bat
1344	32		15		Battery Temperature 3 High 1	Batt Temp3 Hi1		温度3高温Bat			温度3高温Bat            
1345	32		15		Battery Temperature 3 Low	Batt Temp3 Low		温度3低温Bat			温度3低温Bat
1411	32		15		IB2-1 Temp 1			IB2-1 Temp1		IB2-1温度1			IB2-1温度1
1431	32		15		IB2-1 Temp 1			IB2-1 Temp1		IB2-1温度1使能			IB2-1温度1使能
1432	32		15		Battery Temperature 4 High 2	Batt Temp4 Hi2		温度4过温点Bat			温度4过温点Bat
1433	32		15		Battery Temperature 4 High 1	Batt Temp4 Hi1		温度4高温点Bat			温度4高温点Bat          
1434	32		15		Battery Temperature 4 Low	Batt Temp4 Low		温度4低温点Bat			温度4低温点Bat          
1441	32		15		IB2-1 Temp 1 Not Used		IB2-1T1NotUsed		IB2温度1未用			IB2温度1未用
1442	32		15		IB2-1 Temp 1 Sensor Fail	IB2-1T1SensFail		IB2温度1故障			IB2温度1故障
1443	32		15		Battery Temperature 4 High 2	Batt Temp4 Hi2		温度4过温Bat			温度4过温Bat
1444	32		15		Battery Temperature 4 High 1	Batt Temp4 Hi1		温度4高温Bat			温度4高温Bat            
1445	32		15		Battery Temperature 4 Low	Batt Temp4 Low		温度4低温Bat			温度4低温Bat
1511	32		15		IB2-1 Temp 2			IB2-1 Temp2		IB2温度2			IB2温度2
1531	32		15		IB2-1 Temp 2			IB2-1 Temp2		IB2温度2使能			IB2温度2使能
1532	32		15		Battery Temperature 5 High 2	Batt Temp5 Hi2		温度5过温点Bat			温度5过温点Bat
1533	32		15		Battery Temperature 5 High 1	Batt Temp5 Hi1		温度5高温点Bat			温度5高温点Bat         
1534	32		15		Battery Temperature 5 Low	Batt Temp5 Low		温度5低温点Bat			温度5低温点Bat          
1541	32		15		IB2-1 Temp 2 Not Used		IB2-1T2NotUsed		IB2温度2未用			IB2温度2未用
1542	32		15		IB2-1 Temp 2 Sensor Fail	IB2-1T2SensFail		IB2温度2故障			IB2温度2故障
1543	32		15		Battery Temperature 5 High 2	Batt Temp5 Hi2		温度5过温Bat			温度5过温Bat
1544	32		15		Battery Temperature 5 High 1	Batt Temp5 Hi1		温度5高温Bat			温度5高温Bat            
1545	32		15		Battery Temperature 5 Low	Batt Temp5 Low		温度5低温Bat			温度5低温Bat
1611	32		15		EIB-1 Temp 1			EIB-1 Temp1		EIB温度1			EIB温度1
1631	32		15		EIB-1 Temp 1			EIB-1 Temp1		EIB温度1使能			EIB温度1使能
1632	32		15		Battery Temperature 6 High 2	Batt Temp6 Hi2		温度6过温点Bat			温度6过温点Bat
1633	32		15		Battery Temperature 6 High 1	Batt Temp6 Hi1		温度6高温点Bat			温度6高温点Bat          
1634	32		15		Battery Temperature 6 Low	Batt Temp6 Low		温度6低温点Bat			温度6低温点Bat         
1641	32		15		EIB-1 Temp 1 Not Used		EIB-1T1NotUsed		EIB温度1未用			EIB温度1未用
1642	32		15		EIB-1 Temp 1 Sensor Fail	EIB-1T1SensFail		EIB温度1故障			EIB温度1故障
1643	32		15		Battery Temperature 6 High 2	Batt Temp6 Hi2		温度6过温Bat			温度6过温Bat
1644	32		15		Battery Temperature 6 High 1	Batt Temp6 Hi1		温度6高温Bat			温度6高温Bat            
1645	32		15		Battery Temperature 6 Low	Batt Temp6 Low		温度6低温Bat			温度6低温Bat
1711	32		15		EIB-1 Temp 2			EIB-1 Temp2		EIB温度2			EIB温度2
1731	32		15		EIB-1 Temp 2			EIB-1 Temp2		EIB温度2使能			EIB温度2使能
1732	32		15		Battery Temperature 7 High 2	Batt Temp7 Hi2		温度7过温点Bat			温度7过温点Bat
1733	32		15		Battery Temperature 7 High 1	Batt Temp7 Hi1		温度7高温点Bat			温度7高温点Bat          
1734	32		15		Battery Temperature 7 Low	Batt Temp7 Low		温度7低温点Bat			温度7低温点Bat          
1741	32		15		EIB-1 Temp 2 Not Used		EIB-1T2NotUsed		EIB温度2未用			EIB温度2未用
1742	32		15		EIB-1 Temp 2 Sensor Fail	EIB-1T2SensFail		EIB温度2故障			EIB温度2故障
1743	32		15		Battery Temperature 7 High 2	Batt Temp7 Hi2		温度7过温Bat			温度7过温Bat
1744	32		15		Battery Temperature 7 High 1	Batt Temp7 Hi1		温度7高温Bat			温度7高温Bat            
1745	32		15		Battery Temperature 7 Low	Batt Temp7 Low		温度7低温Bat			温度7低温Bat
1746	32		15		DHCP Failure			DHCP Failure		DHCP失效			DHCP失效
1747	32		15		Internal Use (CAN)		Internal Use		内部使用(CAN)			内部使用
1748	32		15		Internal Use (485)		Internal Use		内部使用(485)			内部使用
1749	32		15		Address				Address			地址(从机)			地址(从机)
1750	32		15		201				201			201				201
1751	32		15		202				202			202				202
1752	32		15		203				203			203				203
1753	32		15		Master/Slave Mode		Mast/Slave Mode		Master/Slave状态		Master/Slave状态
1754	32		15		Master				Master			Master				Master
1755	32		15		Slave				Slave			Slave				Slave
1756	32		15		Alone				Alone			Alone				Alone
1757	32		15		SM Batt Temp High Limit		SM Bat Temp Hi		SM电池过温点			SM电池过温点
1758	32		15		SM Batt Temp Low Limit		SM Bat Temp Low		SM电池欠温点			SM电池欠温点
1759	32		15		PLC Config Error		PLC Config Err		PLC配置错误			PLC配置错误
1760	32		15		Rectifier Expansion		Rect Expansion		工作模式			工作模式
1761	32		15		Inactive			Inactive		独立模式			独立模式
1762	32		15		Primary				Primary			主机模式			主机模式
1763	32		15		Secondary			Secondary		从机模式			从机模式
1764	128		64		Mode Changed. Auto Configuration Will Start.	Mode Changed. Auto Configuration Will Start.		模式改变，自动配置将会启动！		即将自动配置！
1765	32		15		Previous Language		Previous Lang		前一次语言		前一次语言
1766	32		15		Current Language		Current Lang		当前语言		当前语言
1767	32		15		Eng				Eng			eng			eng
1768	32		15		Loc				Loc			loc			loc
1769	32		15		485 Communication Failure	485 Comm Fail		485通信失败			485通信失败
1770	64		32		SMDU Sampler Mode		SMDU Samp Mode		SMDU采集模式		SMDU采集模式
1771	32		15		CAN				CAN			CAN				CAN
1772	32		15		RS485				RS485			RS485				RS485
1773	32		15		Manual Mode Time Limit		Manual Mode Lmt		转自动延时			转自动延时
1774	32		15		Observation Summary		OA Summary		系统有一般告警			有一般告警
1775	32		15		Major Summary			MA Summary		系统有重要告警			有重要告警
1776	32		15		Critical Summary		CA Summary		系统有紧急告警			有紧急告警
1777	32		15		All Rectifiers Current		All Rects Curr		全部模块总电流			全部模块总电流
1778	32		15		Rectifier Group Lost Status	Rect Grp Lost		模块组丢失状态			模块组丢失状态	
1779	32		15		Reset Rectifier Group Lost Alarm	Rst RectGrpLost	清除模块组丢失告警		清除模块组丢失
1780	32		15		Rect Group Num on Last Power-On	RectGrp On Num		模块组数目		模块组数目
1781	32		15		Rectifier Group Lost		Rect Group Lost		模块组丢失			模块组丢失
1782	32		15		ETemperature 1 High 1		ETemp 1 High 1		温度1高温点Ev			温度1高温点Ev            
1783	32		15		ETemperature 1 Low		ETemp 1 Low		温度1低温点Ev			温度1低温点Ev
1784	32		15		ETemperature 2 High 1		ETemp 2 High 1		温度2高温点Ev			温度2高温点Ev            
1785	32		15		ETemperature 2 Low		ETemp 2 Low		温度2低温点Ev			温度2低温点Ev
1786	32		15		ETemperature 3 High 1		ETemp 3 High 1		温度3高温点Ev			温度3高温点Ev            
1787	32		15		ETemperature 3 Low		ETemp 3 Low		温度3低温点Ev			温度3低温点Ev
1788	32		15		ETemperature 4 High 1		ETemp 4 High 1		温度4高温点Ev			温度4高温点Ev            
1789	32		15		ETemperature 4 Low		ETemp 4 Low		温度4低温点Ev			温度4低温点Ev
1790	32		15		ETemperature 5 High 1		ETemp 5 High 1		温度5高温点Ev			温度5高温点Ev            
1791	32		15		ETemperature 5 Low		ETemp 5 Low		温度5低温点Ev			温度5低温点Ev
1792	32		15		ETemperature 6 High 1		ETemp 6 High 1		温度6高温点Ev			温度6高温点Ev            
1793	32		15		ETemperature 6 Low		ETemp 6 Low		温度6低温点Ev			温度6低温点Ev
1794	32		15		ETemperature 7 High 1		ETemp 7 High 1		温度7高温点Ev			温度7高温点Ev            
1795	32		15		ETemperature 7 Low		ETemp 7 Low		温度7低温点Ev			温度7低温点Ev
1796	32		15		ETemperature 1 High 1		ETemp 1 High 1		温度1高温Ev			温度1高温Ev            
1797	32		15		ETemperature 1 Low		ETemp 1 Low		温度1低温Ev			温度1低温Ev
1798	32		15		ETemperature 2 High 1		ETemp 2 High 1		温度2高温Ev			温度2高温Ev            
1799	32		15		ETemperature 2 Low		ETemp 2 Low		温度2低温Ev			温度2低温Ev
1800	32		15		ETemperature 3 High 1		ETemp 3 High 1		温度3高温Ev			温度3高温Ev            
1801	32		15		ETemperature 3 Low		ETemp 3 Low		温度3低温Ev			温度3低温Ev
1802	32		15		ETemperature 4 High 1		ETemp 4 High 1		温度4高温Ev			温度4高温Ev            
1803	32		15		ETemperature 4 Low		ETemp 4 Low		温度4低温Ev			温度4低温Ev
1804	32		15		ETemperature 5 High 1		ETemp 5 High 1		温度5高温Ev			温度5高温Ev            
1805	32		15		ETemperature 5 Low		ETemp 5 Low		温度5低温Ev			温度5低温Ev
1806	32		15		ETemperature 6 High 1		ETemp 6 High 1		温度6高温Ev			温度6高温Ev            
1807	32		15		ETemperature 6 Low		ETemp 6 Low		温度6低温Ev			温度6低温Ev
1808	32		15		ETemperature 7 High 1		ETemp 7 High 1		温度7高温Ev			温度7高温Ev            
1809	32		15		ETemperature 7 Low		ETemp 7 Low		温度7低温Ev			温度7低温Ev
1810	32		15		Fast Sampler Flag		Fast Sampl Flag		快采状态			快采状态
1811	32		15		LVD Quantity			LVD Quantity		LVD数量				LVD数量
1812	32		15		LCD Rotation			LCD Rotation		LCD旋转				LCD旋转
1813	32		15		0 deg				0 deg			0度				0度
1814	32		15		90 deg				90 deg			90度				90度
1815	32		15		180 deg				180 deg			180度				180度
1816	32		15		270 deg				270 deg			270度				270度
1817	32		15		Over Voltage 1			Over Voltage 1		直流电压高		直流电压高
1818	32		15		Over Voltage 2			Over Voltage 2		直流过压		直流过压
1819	32		15		Under Voltage 1			Under Voltage 1		直流电压低		直流电压低
1820	32		15		Under Voltage 2			Under Voltage 2		直流欠压		直流欠压
1821	32		15		Over Voltage 1 (24V)		24V Over Volt1		直流电压高(24V)		直流电压高(24V)
1822	32		15		Over Voltage 2 (24V)		24V Over Volt2		直流过压(24V)		直流过压(24V)
1823	32		15		Under Voltage 1 (24V)		24V Under Volt1		直流电压低(24V)		直流电压低(24V)
1824	32		15		Under Voltage 2 (24V)		24V Under Volt2		直流欠压(24V)		直流欠压(24V)
1825	32		15		Fail Safe Mode			Fail Safe		失效保护模式		失效保护模式	
1826	32		15		Disabled			Disabled			禁止			禁止
1827	32		15		Enabled				Enabled			使能			使能
1828	32		15		Hybrid Mode			Hybrid Mode		Hybrid模式		Hybrid模式	
1829	32		15		Disabled			Disabled			禁止			禁止
1830	32		15		Capacity			Capacity		容量模式		容量模式
1831	32		15		Fixed Daily			Fixed Daily		固定时间模式		固定时间模式		
1832	32		15		DG Run at High Temp		DG Run Overtemp		过温开油机使能		过温开油机使能
1833	32		15		DG Used for Hybrid		DG Used			油机选择		油机选择
1834	32		15		DG1				DG1			油机1			油机1
1835	32		15		DG2				DG2			油机2			油机2
1836	32		15		Both				Both			油机1,2			油机1,2				
1837	32		15		DI for Grid			DI for Grid		市电输入		市电输入						
1838	32		15		IB2-1 DI1				IB2-1 DI1			IB2-1 数字输入1		IB2-1数字输入1
1839	32		15		IB2-1 DI2				IB2-1 DI2			IB2-1 数字输入2		IB2-1数字输入2
1840	32		15		IB2-1 DI3				IB2-1 DI3			IB2-1 数字输入3		IB2-1数字输入3
1841	32		15		IB2-1 DI4				IB2-1 DI4			IB2-1 数字输入4		IB2-1数字输入4
1842	32		15		IB2-1 DI5				IB2-1 DI5			IB2-1 数字输入5		IB2-1数字输入5
1843	32		15		IB2-1 DI6				IB2-1 DI6			IB2-1 数字输入6		IB2-1数字输入6
1844	32		15		IB2-1 DI7				IB2-1 DI7			IB2-1 数字输入7		IB2-1数字输入7
1845	32		15		IB2-1 DI8				IB2-1 DI8			IB2-1 数字输入8		IB2-1数字输入8
1846	32		15		DOD				DOD			结束容量		结束容量
1847	32		15		Discharge Duration		Dsch Duration		放电持续时间		放电持续时间	
1848	32		15		Start Discharge Time		Start Dsch Time		开始放电时间		开始放电时间			
1849	32		15		Diesel Run Over Temp		DG Run OverTemp		油机过温运行		油机过温运行
1850	32		15		DG1 is Running			DG1 is Running		油机1运行		油机1运行
1851	32		15		DG2 is Running			DG2 is Running		油机2运行		油机2运行
1852	32		15		Hybrid High Load Setting	High Load Set		过载设定		过载设定
1853	32		15		Hybrid is High Load		High Load		过载			过载
1854	32		15		DG Run Time at High Temp	DG Run Time		过温运行时间		过温运行时间
1855	32		15		Equalizing Start Time		Equal StartTime		均冲开始时刻		均冲开始时刻
1856	32		15		DG1 Failure			DG1 Failure		油机1故障		油机1故障
1857	32		15		DG2 Failure			DG2 Failure		油机2故障		油机2故障
1858	32		15		Diesel Alarm Delay		DG Alarm Delay		油机告警延迟		油机告警延迟
1859	32		15		Grid is on			Grid is on		市电运行		市电运行
1860	32		15		PowerSplit Contactor Mode	Contactor Mode		PowerSplit接触器模式	PS接触器模式	
1861	32		15			Ambient Temp		Amb Temp		环境温度		环境温度
1862	32		15		Ambient Temp Sensor		Amb Temp Sensor		环境温度传感器		环境温度传感器
1863	32		15		None				None			无			无	
1864	32		15		Temperature 1			Temperature 1		温度1			温度1
1865	32		15		Temperature 2			Temperature 2		温度2			温度2
1866	32		15		Temperature 3			Temperature 3		温度3			温度3
1867	32		15		Temperature 4			Temperature 4		温度4			温度4
1868	32		15		Temperature 5			Temperature 5		温度5			温度5
1869	32		15		Temperature 6			Temperature 6		温度6			温度6
1870	32		15		Temperature 7			Temperature 7		温度7			温度7
1871	32		15		Temperature 8			Temperature 8		温度8			温度8
1872	32		15		Temperature 9			Temperature 9		温度9			温度9
1873	32		15		Temperature 10			Temperature 10		温度10			温度10
1874	32		15		Ambient Temp High1		Amb Temp Hi1		环境温度高温点		环境温度高
1875	32		15		Ambient Temp Low		Amb Temp Low		环境温度低温点		环境温度低
1876	32		15		Ambient Temp High1		Amb Temp Hi1		环境温度高		环境温度高
1877	32		15		Ambient Temp Low		Amb Temp Low		环境温度低		环境温度低
1878	32		15		Ambient Sensor Fail		AmbSensor Fail		环境温度故障		环境温度故障
1879	32		15		Digital Input 1			DI1		数字量输入1		数字输入1
1880	32		15		Digital Input 2			DI2		数字量输入2		数字输入2
1881	32		15		Digital Input 3			DI3		数字量输入3		数字输入3
1882	32		15		Digital Input 4			DI4		数字量输入4		数字输入4
1883	32		15		Digital Input 5			DI5		数字量输入5		数字输入5
1884	32		15		Digital Input 6			DI6		数字量输入6		数字输入6
1885	32		15		Digital Input 7			DI7		数字量输入7		数字输入7
1886	32		15		Digital Input 8			DI8		数字量输入8		数字输入8
1887	32		15		Off				Off		关			关
1888	32		15		On				On		开			开
1889	32		15		DO1-Relay Output		DO1-RelayOutput		继电器1			继电器1                 
1890	32		15		DO2-Relay Output		DO2-RelayOutput		继电器2			继电器2                 
1891	32		15		DO3-Relay Output		DO3-RelayOutput		继电器3			继电器3                 
1892	32		15		DO4-Relay Output		DO4-RelayOutput		继电器4			继电器4                 
1893	32		15		DO5-Relay Output		DO5-RelayOutput		继电器5			继电器5                 
1894	32		15		DO6-Relay Output		DO6-RelayOutput		继电器6			继电器6                 
1895	32		15		DO7-Relay Output		DO7-RelayOutput		继电器7			继电器7                 
1896	32		15		DO8-Relay Output		DO8-RelayOutput		继电器8			继电器8                 
1897	32		15		DI1 Alarm State			DI1 Alm State		数字量输入1告警条件	输入1告警条件
1898	32		15		DI2 Alarm State			DI2 Alm State		数字量输入2告警条件	输入2告警条件
1899	32		15		DI3 Alarm State			DI3 Alm State		数字量输入3告警条件	输入3告警条件
1900	32		15		DI4 Alarm State			DI4 Alm State		数字量输入4告警条件	输入4告警条件
1901	32		15		DI5 Alarm State			DI5 Alm State		数字量输入5告警条件	输入5告警条件
1902	32		15		DI6 Alarm State			DI6 Alm State		数字量输入6告警条件	输入6告警条件
1903	32		15		DI7 Alarm State			DI7 Alm State		数字量输入7告警条件	输入7告警条件
1904	32		15		DI8 Alarm State			DI8 Alm State		数字量输入8告警条件	输入8告警条件
1905	32		15		DI1 Alarm			DI1 Alarm		数字量输入1告警		输入1告警
1906	32		15		DI2 Alarm			DI2 Alarm		数字量输入2告警		输入2告警
1907	32		15		DI3 Alarm			DI3 Alarm		数字量输入3告警		输入3告警
1908	32		15		DI4 Alarm			DI4 Alarm		数字量输入4告警		输入4告警
1909	32		15		DI5 Alarm			DI5 Alarm		数字量输入5告警		输入5告警
1910	32		15		DI6 Alarm			DI6 Alarm		数字量输入6告警		输入6告警
1911	32		15		DI7 Alarm			DI7 Alarm		数字量输入7告警		输入7告警
1912	32		15		DI8 Alarm			DI8 Alarm		数字量输入8告警		输入8告警
1913	32		15		On				On			开			开
1914	32		15		Off				Off			关			关
1915	32		15		High				High			高电平			高电平
1916	32		15		Low				Low			低电平			低电平
1917	32		15		IB Num				IB Num			IB Num			IB Num
1918	32		15		IB Type				IB Type			IB Type			IB Type
1919	32		15		CSU_Undervoltage 1		Undervoltage 1		Undervoltage 1		Undervoltage 1
1920	32		15		CSU_Undervoltage 2		Undervoltage 2		Undervoltage 2		Undervoltage 2
1921	32		15		CSU_Overvoltage			Overvoltage		Overvoltage		Overvoltage
1922	32		15		CSU_Communication Fail		Comm Fail		Communication Fail	Comm Fail
1923	32		15		CSU_External Alarm 1		Exter_Alarm1		External alarm 1	Exter_alarm1
1924	32		15		CSU_External Alarm 2		Exter_Alarm2		External alarm 2	Exter_alarm2
1925	32		15		CSU_External Alarm 3		Exter_Alarm3		External alarm 3	Exter_alarm3
1926	32		15		CSU_External Alarm 4		Exter_Alarm4		External alarm 4	Exter_alarm4
1927	32		15		CSU_External Alarm 5		Exter_Alarm5		External alarm 5	Exter_alarm5
1928	32		15		CSU_External Alarm 6		Exter_Alarm6		External alarm 6	Exter_alarm6
1929	32		15		CSU_External Alarm 7		Exter_Alarm7		External alarm 7	Exter_alarm7
1930	32		15		CSU_External Alarm 8		Exter_Alarm8		External alarm 8	Exter_alarm8
1931	32		15		CSU_External Comm Fail		Ext_Comm Fail		External comm Fail	Ext_comm Fail
1932	32		15		CSU_Bat_Curr Limit Alarm	Bat_Curr Limit		Bat_curr limit alarm	Bat_curr limit
1933	32		15		DC LC Number			DC LC Num		DCLCNumber		DCLCNumber
1934	32		15		Bat LC Number			Bat LC Num		BatLCNumber	BatLCNumber
1935	32		15		Ambient Temp High2		Amb Temp Hi2		环境温度超高		环境温度超高
1936	32		15		System Type			System Type		System Type		System Type
1937	32		15		Normal				Normal			Normal			Normal
1938	32		15		Test				Test			Test			Test
1939	32		15		Auto Mode			Auto Mode		自动模式		自动模式
1940	32		15		EMEA				EMEA			欧洲			欧洲
1941	32		15		Normal				Normal			普通			普通
1942	32		15		Bus Run Mode			Bus Run Mode		总线运行模式		总线模式
1943	32		15		NO				NO			常开			常开
1944	32		15		NC				NC			常闭		常闭
1945	32		15		Fail Safe Mode(Hybrid)		Fail Safe		失效保护模式(Hybrid)	失效保护模式	
1946	32		15		IB Communication Fail		IB Comm Fail		IB通信失败		IB通信失败	
1947	32		15		Relay Test			Relay Test	继电器测试		继电器测试	
1948	32		15		NCU DO1 Test			NCU DO1 Test			测试继电器1		测试继电器1
1949	32		15		NCU DO2 Test			NCU DO2 Test		测试继电器2		测试继电器2
1950	32		15		NCU DO3 Test			NCU DO3 Test			测试继电器3		测试继电器3
1951	32		15		NCU DO4 Test			NCU DO4 Test			测试继电器4		测试继电器4
1952	32		15		Relay 5 Test			Relay 5 Test		测试继电器5		测试继电器5
1953	32		15		Relay 6 Test			Relay 6 Test		测试继电器6		测试继电器6
1954	32		15		Relay 7 Test			Relay 7 Test		测试继电器7		测试继电器7
1955	32		15		Relay 8 Test			Relay 8 Test		测试继电器8		测试继电器8
1956	32		15		Relay Test			Relay Test		继电器测试		继电器测试
1957	32		15		Relay Test Time			Relay Test Time		继电器测试时间		继电器测试时间
1958	32		15		Manual			Manual		手动模式		手动模式
1959	32		15		Automatic			Automatic		自动模式		自动模式

1960	32		15		System Temp1		System T1	系统温度1		系统温度1
1961	32		15		System Temp2		System T2	系统温度2		系统温度2
1962	32		15		System Temp3		System T3	系统温度3		系统温度3
1963	32		15		IB2-1 Temp1		IB2-1 T1		IB2-1温度1		IB2-1温度1
1964	32		15		IB2-1 Temp2		IB2-1 T2		IB2-1温度2		IB2-1温度2
1965	32		15		EIB-1 Temp1		EIB-1 T1		EIB-1温度1		EIB-1温度1
1966	32		15		EIB-1 Temp2		EIB-1 T2		EIB-1温度2		EIB-1温度2
1967	32		15		SMTemp1 Temp1		SMTemp1 T1	SMTemp1温度1		SMTemp1温度1
1968	32		15		SMTemp1 Temp2		SMTemp1 T2	SMTemp1温度2		SMTemp1温度2
1969	32		15		SMTemp1 Temp3		SMTemp1 T3	SMTemp1温度3		SMTemp1温度3
1970	32		15		SMTemp1 Temp4		SMTemp1 T4	SMTemp1温度4		SMTemp1温度4
1971	32		15		SMTemp1 Temp5		SMTemp1 T5	SMTemp1温度5		SMTemp1温度5
1972	32		15		SMTemp1 Temp6		SMTemp1 T6	SMTemp1温度6		SMTemp1温度6
1973	32		15		SMTemp1 Temp7		SMTemp1 T7	SMTemp1温度7		SMTemp1温度7
1974	32		15		SMTemp1 Temp8		SMTemp1 T8	SMTemp1温度8		SMTemp1温度8
1975	32		15		SMTemp2 Temp1		SMTemp2 T1	SMTemp2温度1		SMTemp2温度1
1976	32		15		SMTemp2 Temp2		SMTemp2 T2	SMTemp2温度2		SMTemp2温度2
1977	32		15		SMTemp2 Temp3		SMTemp2 T3	SMTemp2温度3		SMTemp2温度3
1978	32		15		SMTemp2 Temp4		SMTemp2 T4	SMTemp2温度4		SMTemp2温度4
1979	32		15		SMTemp2 Temp5		SMTemp2 T5	SMTemp2温度5		SMTemp2温度5
1980	32		15		SMTemp2 Temp6		SMTemp2 T6	SMTemp2温度6		SMTemp2温度6
1981	32		15		SMTemp2 Temp7		SMTemp2 T7	SMTemp2温度7		SMTemp2温度7
1982	32		15		SMTemp2 Temp8		SMTemp2 T8	SMTemp2温度8		SMTemp2温度8
1983	32		15		SMTemp3 Temp1		SMTemp3 T1	SMTemp3温度1		SMTemp3温度1
1984	32		15		SMTemp3 Temp2		SMTemp3 T2	SMTemp3温度2		SMTemp3温度2
1985	32		15		SMTemp3 Temp3		SMTemp3 T3	SMTemp3温度3		SMTemp3温度3
1986	32		15		SMTemp3 Temp4		SMTemp3 T4	SMTemp3温度4		SMTemp3温度4
1987	32		15		SMTemp3 Temp5		SMTemp3 T5	SMTemp3温度5		SMTemp3温度5
1988	32		15		SMTemp3 Temp6		SMTemp3 T6	SMTemp3温度6		SMTemp3温度6
1989	32		15		SMTemp3 Temp7		SMTemp3 T7	SMTemp3温度7		SMTemp3温度7
1990	32		15		SMTemp3 Temp8		SMTemp3 T8	SMTemp3温度8		SMTemp3温度8
1991	32		15		SMTemp4 Temp1		SMTemp4 T1	SMTemp4温度1		SMTemp4温度1
1992	32		15		SMTemp4 Temp2		SMTemp4 T2	SMTemp4温度2		SMTemp4温度2
1993	32		15		SMTemp4 Temp3		SMTemp4 T3	SMTemp4温度3		SMTemp4温度3
1994	32		15		SMTemp4 Temp4		SMTemp4 T4	SMTemp4温度4		SMTemp4温度4
1995	32		15		SMTemp4 Temp5		SMTemp4 T5	SMTemp4温度5		SMTemp4温度5
1996	32		15		SMTemp4 Temp6		SMTemp4 T6	SMTemp4温度6		SMTemp4温度6
1997	32		15		SMTemp4 Temp7		SMTemp4 T7	SMTemp4温度7		SMTemp4温度7
1998	32		15		SMTemp4 Temp8		SMTemp4 T8	SMTemp4温度8		SMTemp4温度8
1999	32		15		SMTemp5 Temp1		SMTemp5 T1	SMTemp5温度1		SMTemp5温度1
2000	32		15		SMTemp5 Temp2		SMTemp5 T2	SMTemp5温度2		SMTemp5温度2
2001	32		15		SMTemp5 Temp3		SMTemp5 T3	SMTemp5温度3		SMTemp5温度3
2002	32		15		SMTemp5 Temp4		SMTemp5 T4	SMTemp5温度4		SMTemp5温度4
2003	32		15		SMTemp5 Temp5		SMTemp5 T5	SMTemp5温度5		SMTemp5温度5
2004	32		15		SMTemp5 Temp6		SMTemp5 T6	SMTemp5温度6		SMTemp5温度6
2005	32		15		SMTemp5 Temp7		SMTemp5 T7	SMTemp5温度7		SMTemp5温度7
2006	32		15		SMTemp5 Temp8		SMTemp5 T8	SMTemp5温度8		SMTemp5温度8
2007	32		15		SMTemp6 Temp1		SMTemp6 T1	SMTemp6温度1		SMTemp6温度1
2008	32		15		SMTemp6 Temp2		SMTemp6 T2	SMTemp6温度2		SMTemp6温度2
2009	32		15		SMTemp6 Temp3		SMTemp6 T3	SMTemp6温度3		SMTemp6温度3
2010	32		15		SMTemp6 Temp4		SMTemp6 T4	SMTemp6温度4		SMTemp6温度4
2011	32		15		SMTemp6 Temp5		SMTemp6 T5	SMTemp6温度5		SMTemp6温度5
2012	32		15		SMTemp6 Temp6		SMTemp6 T6	SMTemp6温度6		SMTemp6温度6
2013	32		15		SMTemp6 Temp7		SMTemp6 T7	SMTemp6温度7		SMTemp6温度7
2014	32		15		SMTemp6 Temp8		SMTemp6 T8	SMTemp6温度8		SMTemp6温度8
2015	32		15		SMTemp7 Temp1		SMTemp7 T1	SMTemp7温度1		SMTemp7温度1
2016	32		15		SMTemp7 Temp2		SMTemp7 T2	SMTemp7温度2		SMTemp7温度2
2017	32		15		SMTemp7 Temp3		SMTemp7 T3	SMTemp7温度3		SMTemp7温度3
2018	32		15		SMTemp7 Temp4		SMTemp7 T4	SMTemp7温度4		SMTemp7温度4
2019	32		15		SMTemp7 Temp5		SMTemp7 T5	SMTemp7温度5		SMTemp7温度5
2020	32		15		SMTemp7 Temp6		SMTemp7 T6	SMTemp7温度6		SMTemp7温度6
2021	32		15		SMTemp7 Temp7		SMTemp7 T7	SMTemp7温度7		SMTemp7温度7
2022	32		15		SMTemp7 Temp8		SMTemp7 T8	SMTemp7温度8		SMTemp7温度8
2023	32		15		SMTemp8 Temp1		SMTemp8 T1	SMTemp8温度1		SMTemp8温度1
2024	32		15		SMTemp8 Temp2		SMTemp8 T2	SMTemp8温度2		SMTemp8温度2
2025	32		15		SMTemp8 Temp3		SMTemp8 T3	SMTemp8温度3		SMTemp8温度3
2026	32		15		SMTemp8 Temp4		SMTemp8 T4	SMTemp8温度4		SMTemp8温度4
2027	32		15		SMTemp8 Temp5		SMTemp8 T5	SMTemp8温度5		SMTemp8温度5
2028	32		15		SMTemp8 Temp6		SMTemp8 T6	SMTemp8温度6		SMTemp8温度6
2029	32		15		SMTemp8 Temp7		SMTemp8 T7	SMTemp8温度7		SMTemp8温度7
2030	32		15		SMTemp8 Temp8		SMTemp8 T8	SMTemp8温度8		SMTemp8温度8

2031		32		15		System Temp1 High 2	System T1 Hi2	系统温度1过过温		系统温度1过过温
2032		32		15		System Temp1 High 1	System T1 Hi1	系统温度1过温		系统温度1过温
2033		32		15		System Temp1 Low	System T1 Low	系统温度1低温		系统温度1低温
2034		32		15		System Temp2 High 2	System T2 Hi2	系统温度2过过温		系统温度2过过温
2035		32		15		System Temp2 High 1	System T2 Hi1	系统温度2过温		系统温度2过温
2036		32		15		System Temp2 Low	System T2 Low	系统温度2低温		系统温度2低温
2037		32		15		System Temp3 High 2	System T3 Hi2	系统温度3过过温		系统温度3过过温
2038		32		15		System Temp3 High 1	System T3 Hi1	系统温度3过温		系统温度3过温
2039		32		15		System Temp3 Low	System T3 Low	系统温度3低温		系统温度3低温
2040		32		15		IB2-1 Temp1 High 2	IB2-1 T1 Hi2	IB2-1温度1过过温		IB2-1温度1过过温
2041		32		15		IB2-1 Temp1 High 1	IB2-1 T1 Hi1	IB2-1温度1过温		IB2-1温度1过温
2042		32		15		IB2-1 Temp1 Low		IB2-1 T1 Low	IB2-1温度1低温		IB2-1温度1低温
2043		32		15		IB2-1 Temp2 High 2	IB2-1 T2 Hi2	IB2-1温度2过过温		IB2-1温度2过过温
2044		32		15		IB2-1 Temp2 High 1	IB2-1 T2 Hi1	IB2-1温度2过温		IB2-1温度2过温
2045		32		15		IB2-1 Temp2 Low		IB2-1 T2 Low	IB2-1温度2低温		IB2-1温度2低温
2046		32		15		EIB-1 Temp1 High 2	EIB-1 T1 Hi2	EIB-1温度1过过温		EIB-1温度1过过温
2047		32		15		EIB-1 Temp1 High 1	EIB-1 T1 Hi1	EIB-1温度1过温		EIB-1温度1过温
2048		32		15		EIB-1 Temp1 Low		EIB-1 T1 Low	EIB-1温度1低温		EIB-1温度1低温
2049		32		15		EIB-1 Temp2 High 2	EIB-1 T2 Hi2	EIB-1温度2过过温		EIB-1温度2过过温
2050		32		15		EIB-1 Temp2 High 1	EIB-1 T2 Hi1	EIB-1温度2过温		EIB-1温度2过温
2051		32		15		EIB-1 Temp2 Low		EIB-1 T2 Low	EIB-1温度2低温		EIB-1温度2低温
2052		32		15		SMTemp1 Temp1 High 2	SMTemp1 T1 Hi2	SMTemp1 T1过过温	SMTemp1T1过过温
2053		32		15		SMTemp1 Temp1 High 1	SMTemp1 T1 Hi1	SMTemp1 T1过温		SMTemp1T1过温
2054		32		15		SMTemp1 Temp1 Low	SMTemp1 T1 Low	SMTemp1 T1低温		SMTemp1T1低温
2055		32		15		SMTemp1 Temp2 High 2	SMTemp1 T2 Hi2	SMTemp1 T2过过温	SMTemp1T2过过温
2056		32		15		SMTemp1 Temp2 High 1	SMTemp1 T2 Hi1	SMTemp1 T2过温		SMTemp1T2过温
2057		32		15		SMTemp1 Temp2 Low	SMTemp1 T2 Low	SMTemp1 T2低温		SMTemp1T2低温
2058		32		15		SMTemp1 Temp3 High 2	SMTemp1 T3 Hi2	SMTemp1 T3过过温	SMTemp1T3过过温
2059		32		15		SMTemp1 Temp3 High 1	SMTemp1 T3 Hi1	SMTemp1 T3过温		SMTemp1T3过温
2060		32		15		SMTemp1 Temp3 Low	SMTemp1 T3 Low	SMTemp1 T3低温		SMTemp1T3低温
2061		32		15		SMTemp1 Temp4 High 2	SMTemp1 T4 Hi2	SMTemp1 T4过过温	SMTemp1T4过过温
2062		32		15		SMTemp1 Temp4 High 1	SMTemp1 T4 Hi1	SMTemp1 T4过温		SMTemp1T4过温
2063		32		15		SMTemp1 Temp4 Low	SMTemp1 T4 Low	SMTemp1 T4低温		SMTemp1T4低温
2064		32		15		SMTemp1 Temp5 High 2	SMTemp1 T5 Hi2	SMTemp1 T5过过温	SMTemp1T5过过温
2065		32		15		SMTemp1 Temp5 High 1	SMTemp1 T5 Hi1	SMTemp1 T5过温		SMTemp1T5过温
2066		32		15		SMTemp1 Temp5 Low	SMTemp1 T5 Low	SMTemp1 T5低温		SMTemp1T5低温
2067		32		15		SMTemp1 Temp6 High 2	SMTemp1 T6 Hi2	SMTemp1 T6过过温	SMTemp1T6过过温
2068		32		15		SMTemp1 Temp6 High 1	SMTemp1 T6 Hi1	SMTemp1 T6过温		SMTemp1T6过温
2069		32		15		SMTemp1 Temp6 Low	SMTemp1 T6 Low	SMTemp1 T6低温		SMTemp1T6低温
2070		32		15		SMTemp1 Temp7 High 2	SMTemp1 T7 Hi2	SMTemp1 T7过过温	SMTemp1T7过过温
2071		32		15		SMTemp1 Temp7 High 1	SMTemp1 T7 Hi1	SMTemp1 T7过温		SMTemp1T7过温
2072		32		15		SMTemp1 Temp7 Low	SMTemp1 T7 Low	SMTemp1 T7低温		SMTemp1T7低温
2073		32		15		SMTemp1 Temp8 High 2	SMTemp1 T8 Hi2	SMTemp1 T8过过温	SMTemp1T8过过温
2074		32		15		SMTemp1 Temp8 High 1	SMTemp1 T8 Hi1	SMTemp1 T8过温		SMTemp1T8过温
2075		32		15		SMTemp1 Temp8 Low	SMTemp1 T8 Low	SMTemp1 T8低温		SMTemp1T8低温
2076		32		15		SMTemp2 Temp1 High 2	SMTemp2 T1 Hi2	SMTemp2 T1过过温	SMTemp2T1过过温
2077		32		15		SMTemp2 Temp1 High 1	SMTemp2 T1 Hi1	SMTemp2 T1过温		SMTemp2T1过温
2078		32		15		SMTemp2 Temp1 Low	SMTemp2 T1 Low	SMTemp2 T1低温		SMTemp2T1低温
2079		32		15		SMTemp2 Temp2 High 2	SMTemp2 T2 Hi2	SMTemp2 T2过过温	SMTemp2T2过过温
2080		32		15		SMTemp2 Temp2 High 1	SMTemp2 T2 Hi1	SMTemp2 T2过温		SMTemp2T2过温
2081		32		15		SMTemp2 Temp2 Low	SMTemp2 T2 Low	SMTemp2 T2低温		SMTemp2T2低温
2082		32		15		SMTemp2 Temp3 High 2	SMTemp2 T3 Hi2	SMTemp2 T3过过温	SMTemp2T3过过温
2083		32		15		SMTemp2 Temp3 High 1	SMTemp2 T3 Hi1	SMTemp2 T3过温		SMTemp2T3过温
2084		32		15		SMTemp2 Temp3 Low	SMTemp2 T3 Low	SMTemp2 T3低温		SMTemp2T3低温
2085		32		15		SMTemp2 Temp4 High 2	SMTemp2 T4 Hi2	SMTemp2 T4过过温	SMTemp2T4过过温
2086		32		15		SMTemp2 Temp4 High 1	SMTemp2 T4 Hi1	SMTemp2 T4过温		SMTemp2T4过温
2087		32		15		SMTemp2 Temp4 Low	SMTemp2 T4 Low	SMTemp2 T4低温		SMTemp2T4低温
2088		32		15		SMTemp2 Temp5 High 2	SMTemp2 T5 Hi2	SMTemp2 T5过过温	SMTemp2T5过过温
2089		32		15		SMTemp2 Temp5 High 1	SMTemp2 T5 Hi1	SMTemp2 T5过温		SMTemp2T5过温
2090		32		15		SMTemp2 Temp5 Low	SMTemp2 T5 Low	SMTemp2 T5低温		SMTemp2T5低温
2091		32		15		SMTemp2 Temp6 High 2	SMTemp2 T6 Hi2	SMTemp2 T6过过温	SMTemp2T6过过温
2092		32		15		SMTemp2 Temp6 High 1	SMTemp2 T6 Hi1	SMTemp2 T6过温		SMTemp2T6过温
2093		32		15		SMTemp2 Temp6 Low	SMTemp2 T6 Low	SMTemp2 T6低温		SMTemp2T6低温
2094		32		15		SMTemp2 Temp7 High 2	SMTemp2 T7 Hi2	SMTemp2 T7过过温	SMTemp2T7过过温
2095		32		15		SMTemp2 Temp7 High 1	SMTemp2 T7 Hi1	SMTemp2 T7过温		SMTemp2T7过温
2096		32		15		SMTemp2 Temp7 Low	SMTemp2 T7 Low	SMTemp2 T7低温		SMTemp2T7低温
2097		32		15		SMTemp2 Temp8 High 2	SMTemp2 T8 Hi2	SMTemp2 T8过过温	SMTemp2T8过过温
2098		32		15		SMTemp2 Temp8 High 1	SMTemp2 T8 Hi1	SMTemp2 T8过温		SMTemp2T8过温
2099		32		15		SMTemp2 Temp8 Low	SMTemp2 T8 Low	SMTemp2 T8低温		SMTemp2T8低温
2100		32		15		SMTemp3 Temp1 High 2	SMTemp3 T1 Hi2	SMTemp3 T1过过温	SMTemp3T1过过温
2101		32		15		SMTemp3 Temp1 High 1	SMTemp3 T1 Hi1	SMTemp3 T1过温		SMTemp3T1过温
2102		32		15		SMTemp3 Temp1 Low	SMTemp3 T1 Low	SMTemp3 T1低温		SMTemp3T1低温
2103		32		15		SMTemp3 Temp2 High 2	SMTemp3 T2 Hi2	SMTemp3 T2过过温	SMTemp3T2过过温
2104		32		15		SMTemp3 Temp2 High 1	SMTemp3 T2 Hi1	SMTemp3 T2过温		SMTemp3T2过温
2105		32		15		SMTemp3 Temp2 Low	SMTemp3 T2 Low	SMTemp3 T2低温		SMTemp3T2低温
2106		32		15		SMTemp3 Temp3 High 2	SMTemp3 T3 Hi2	SMTemp3 T3过过温	SMTemp3T3过过温
2107		32		15		SMTemp3 Temp3 High 1	SMTemp3 T3 Hi1	SMTemp3 T3过温		SMTemp3T3过温
2108		32		15		SMTemp3 Temp3 Low	SMTemp3 T3 Low	SMTemp3 T3低温		SMTemp3T3低温
2109		32		15		SMTemp3 Temp4 High 2	SMTemp3 T4 Hi2	SMTemp3 T4过过温	SMTemp3T4过过温
2110		32		15		SMTemp3 Temp4 High 1	SMTemp3 T4 Hi1	SMTemp3 T4过温		SMTemp3T4过温
2111		32		15		SMTemp3 Temp4 Low	SMTemp3 T4 Low	SMTemp3 T4低温		SMTemp3T4低温
2112		32		15		SMTemp3 Temp5 High 2	SMTemp3 T5 Hi2	SMTemp3 T5过过温	SMTemp3T5过过温
2113		32		15		SMTemp3 Temp5 High 1	SMTemp3 T5 Hi1	SMTemp3 T5过温		SMTemp3T5过温
2114		32		15		SMTemp3 Temp5 Low	SMTemp3 T5 Low	SMTemp3 T5低温		SMTemp3T5低温
2115		32		15		SMTemp3 Temp6 High 2	SMTemp3 T6 Hi2	SMTemp3 T6过过温	SMTemp3T6过过温
2116		32		15		SMTemp3 Temp6 High 1	SMTemp3 T6 Hi1	SMTemp3 T6过温		SMTemp3T6过温
2117		32		15		SMTemp3 Temp6 Low	SMTemp3 T6 Low	SMTemp3 T6低温		SMTemp3T6低温
2118		32		15		SMTemp3 Temp7 High 2	SMTemp3 T7 Hi2	SMTemp3 T7过过温	SMTemp3T7过过温
2119		32		15		SMTemp3 Temp7 High 1	SMTemp3 T7 Hi1	SMTemp3 T7过温		SMTemp3T7过温
2120		32		15		SMTemp3 Temp7 Low	SMTemp3 T7 Low	SMTemp3 T7低温		SMTemp3T7低温
2121		32		15		SMTemp3 Temp8 High 2	SMTemp3 T8 Hi2	SMTemp3 T8过过温	SMTemp3T8过过温
2122		32		15		SMTemp3 Temp8 High 1	SMTemp3 T8 Hi1	SMTemp3 T8过温		SMTemp3T8过温
2123		32		15		SMTemp3 Temp8 Low	SMTemp3 T8 Low	SMTemp3 T8低温		SMTemp3T8低温
2124		32		15		SMTemp4 Temp1 High 2	SMTemp4 T1 Hi2	SMTemp4 T1过过温	SMTemp4T1过过温
2125		32		15		SMTemp4 Temp1 High 1	SMTemp4 T1 Hi1	SMTemp4 T1过温		SMTemp4T1过温
2126		32		15		SMTemp4 Temp1 Low	SMTemp4 T1 Low	SMTemp4 T1低温		SMTemp4T1低温
2127		32		15		SMTemp4 Temp2 High 2	SMTemp4 T2 Hi2	SMTemp4 T2过过温	SMTemp4T2过过温
2128		32		15		SMTemp4 Temp2 High 1	SMTemp4 T2 Hi1	SMTemp4 T2过温		SMTemp4T2过温
2129		32		15		SMTemp4 Temp2 Low	SMTemp4 T2 Low	SMTemp4 T2低温		SMTemp4T2低温
2130		32		15		SMTemp4 Temp3 High 2	SMTemp4 T3 Hi2	SMTemp4 T3过过温	SMTemp4T3过过温
2131		32		15		SMTemp4 Temp3 High 1	SMTemp4 T3 Hi1	SMTemp4 T3过温		SMTemp4T3过温
2132		32		15		SMTemp4 Temp3 Low	SMTemp4 T3 Low	SMTemp4 T3低温		SMTemp4T3低温
2133		32		15		SMTemp4 Temp4 High 2	SMTemp4 T4 Hi2	SMTemp4 T4过过温	SMTemp4T4过过温
2134		32		15		SMTemp4 Temp4 High 1	SMTemp4 T4 Hi1	SMTemp4 T4过温		SMTemp4T4过温
2135		32		15		SMTemp4 Temp4 Low	SMTemp4 T4 Low	SMTemp4 T4低温		SMTemp4T4低温
2136		32		15		SMTemp4 Temp5 High 2	SMTemp4 T5 Hi2	SMTemp4 T5过过温	SMTemp4T5过过温
2137		32		15		SMTemp4 Temp5 High 1	SMTemp4 T5 Hi1	SMTemp4 T5过温		SMTemp4T5过温
2138		32		15		SMTemp4 Temp5 Low	SMTemp4 T5 Low	SMTemp4 T5低温		SMTemp4T5低温
2139		32		15		SMTemp4 Temp6 High 2	SMTemp4 T6 Hi2	SMTemp4 T6过过温	SMTemp4T6过过温
2140		32		15		SMTemp4 Temp6 High 1	SMTemp4 T6 Hi1	SMTemp4 T6过温		SMTemp4T6过温
2141		32		15		SMTemp4 Temp6 Low	SMTemp4 T6 Low	SMTemp4 T6低温		SMTemp4T6低温
2142		32		15		SMTemp4 Temp7 High 2	SMTemp4 T7 Hi2	SMTemp4 T7过过温	SMTemp4T7过过温
2143		32		15		SMTemp4 Temp7 High 1	SMTemp4 T7 Hi1	SMTemp4 T7过温		SMTemp4T7过温
2144		32		15		SMTemp4 Temp7 Low	SMTemp4 T7 Low	SMTemp4 T7低温		SMTemp4T7低温
2145		32		15		SMTemp4 Temp8 High 2	SMTemp4 T8 Hi2	SMTemp4 T8过过温	SMTemp4T8过过温
2146		32		15		SMTemp4 Temp8 Hi1	SMTemp4 T8 Hi1	SMTemp4 T8过温		SMTemp4T8过温
2147		32		15		SMTemp4 Temp8 Low	SMTemp4 T8 Low	SMTemp4 T8低温		SMTemp4T8低温
2148		32		15		SMTemp5 Temp1 High 2	SMTemp5 T1 Hi2	SMTemp5 T1过过温	SMTemp5T1过过温
2149		32		15		SMTemp5 Temp1 High 1	SMTemp5 T1 Hi1	SMTemp5 T1过温		SMTemp5T1过温
2150		32		15		SMTemp5 Temp1 Low	SMTemp5 T1 Low	SMTemp5 T1低温		SMTemp5T1低温
2151		32		15		SMTemp5 Temp2 High 2	SMTemp5 T2 Hi2	SMTemp5 T2过过温	SMTemp5T2过过温
2152		32		15		SMTemp5 Temp2 High 1	SMTemp5 T2 Hi1	SMTemp5 T2过温		SMTemp5T2过温
2153		32		15		SMTemp5 Temp2 Low	SMTemp5 T2 Low	SMTemp5 T2低温		SMTemp5T2低温
2154		32		15		SMTemp5 Temp3 High 2	SMTemp5 T3 Hi2	SMTemp5 T3过过温	SMTemp5T3过过温
2155		32		15		SMTemp5 Temp3 High 1	SMTemp5 T3 Hi1	SMTemp5 T3过温		SMTemp5T3过温
2156		32		15		SMTemp5 Temp3 Low	SMTemp5 T3 Low	SMTemp5 T3低温		SMTemp5T3低温
2157		32		15		SMTemp5 Temp4 High 2	SMTemp5 T4 Hi2	SMTemp5 T4过过温	SMTemp5T4过过温
2158		32		15		SMTemp5 Temp4 High 1	SMTemp5 T4 Hi1	SMTemp5 T4过温		SMTemp5T4过温
2159		32		15		SMTemp5 Temp4 Low	SMTemp5 T4 Low	SMTemp5 T4低温		SMTemp5T4低温
2160		32		15		SMTemp5 Temp5 High 2	SMTemp5 T5 Hi2	SMTemp5 T5过过温	SMTemp5T5过过温
2161		32		15		SMTemp5 Temp5 High 1	SMTemp5 T5 Hi1	SMTemp5 T5过温		SMTemp5T5过温
2162		32		15		SMTemp5 Temp5 Low	SMTemp5 T5 Low	SMTemp5 T5低温		SMTemp5T5低温
2163		32		15		SMTemp5 Temp6 High 2	SMTemp5 T6 Hi2	SMTemp5 T6过过温	SMTemp5T6过过温
2164		32		15		SMTemp5 Temp6 High 1	SMTemp5 T6 Hi1	SMTemp5 T6过温		SMTemp5T6过温
2165		32		15		SMTemp5 Temp6 Low	SMTemp5 T6 Low	SMTemp5 T6低温		SMTemp5T6低温
2166		32		15		SMTemp5 Temp7 High 2	SMTemp5 T7 Hi2	SMTemp5 T7过过温	SMTemp5T7过过温
2167		32		15		SMTemp5 Temp7 High 1	SMTemp5 T7 Hi1	SMTemp5 T7过温		SMTemp5T7过温
2168		32		15		SMTemp5 Temp7 Low	SMTemp5 T7 Low	SMTemp5 T7低温		SMTemp5T7低温
2169		32		15		SMTemp5 Temp8 High 2	SMTemp5 T8 Hi2	SMTemp5 T8过过温	SMTemp5T8过过温
2170		32		15		SMTemp5 Temp8 High 1	SMTemp5 T8 Hi1	SMTemp5 T8过温		SMTemp5T8过温
2171		32		15		SMTemp5 Temp8 Low	SMTemp5 T8 Low	SMTemp5 T8低温		SMTemp5T8低温
2172		32		15		SMTemp6 Temp1 High 2	SMTemp6 T1 Hi2	SMTemp6 T1过过温	SMTemp6T1过过温
2173		32		15		SMTemp6 Temp1 High 1	SMTemp6 T1 Hi1	SMTemp6 T1过温		SMTemp6T1过温
2174		32		15		SMTemp6 Temp1 Low	SMTemp6 T1 Low	SMTemp6 T1低温		SMTemp6T1低温
2175		32		15		SMTemp6 Temp2 High 2	SMTemp6 T2 Hi2	SMTemp6 T2过过温	SMTemp6T2过过温
2176		32		15		SMTemp6 Temp2 High 1	SMTemp6 T2 Hi1	SMTemp6 T2过温		SMTemp6T2过温
2177		32		15		SMTemp6 Temp2 Low	SMTemp6 T2 Low	SMTemp6 T2低温		SMTemp6T2低温
2178		32		15		SMTemp6 Temp3 High 2	SMTemp6 T3 Hi2	SMTemp6 T3过过温	SMTemp6T3过过温
2179		32		15		SMTemp6 Temp3 High 1	SMTemp6 T3 Hi1	SMTemp6 T3过温		SMTemp6T3过温
2180		32		15		SMTemp6 Temp3 Low	SMTemp6 T3 Low	SMTemp6 T3低温		SMTemp6T3低温
2181		32		15		SMTemp6 Temp4 High 2	SMTemp6 T4 Hi2	SMTemp6 T4过过温	SMTemp6T4过过温
2182		32		15		SMTemp6 Temp4 High 1	SMTemp6 T4 Hi1	SMTemp6 T4过温		SMTemp6T4过温
2183		32		15		SMTemp6 Temp4 Low	SMTemp6 T4 Low	SMTemp6 T4低温		SMTemp6T4低温
2184		32		15		SMTemp6 Temp5 High 2	SMTemp6 T5 Hi2	SMTemp6 T5过过温	SMTemp6T5过过温
2185		32		15		SMTemp6 Temp5 High 1	SMTemp6 T5 Hi1	SMTemp6 T5过温		SMTemp6T5过温
2186		32		15		SMTemp6 Temp5 Low	SMTemp6 T5 Low	SMTemp6 T5低温		SMTemp6T5低温
2187		32		15		SMTemp6 Temp6 High 2	SMTemp6 T6 Hi2	SMTemp6 T6过过温	SMTemp6T6过过温
2188		32		15		SMTemp6 Temp6 High 1	SMTemp6 T6 Hi1	SMTemp6 T6过温		SMTemp6T6过温
2189		32		15		SMTemp6 Temp6 Low	SMTemp6 T6 Low	SMTemp6 T6低温		SMTemp6T6低温
2190		32		15		SMTemp6 Temp7 High 2	SMTemp6 T7 Hi2	SMTemp6 T7过过温	SMTemp6T7过过温
2191		32		15		SMTemp6 Temp7 High 1	SMTemp6 T7 Hi1	SMTemp6 T7过温		SMTemp6T7过温
2192		32		15		SMTemp6 Temp7 Low	SMTemp6 T7 Low	SMTemp6 T7低温		SMTemp6T7低温
2193		32		15		SMTemp6 Temp8 High 2	SMTemp6 T8 Hi2	SMTemp6 T8过过温	SMTemp6T8过过温
2194		32		15		SMTemp6 Temp8 High 1	SMTemp6 T8 Hi1	SMTemp6 T8过温		SMTemp6T8过温
2195		32		15		SMTemp6 Temp8 Low	SMTemp6 T8 Low	SMTemp6 T8低温		SMTemp6T8低温
2196		32		15		SMTemp7 Temp1 High 2	SMTemp7 T1 Hi2	SMTemp7 T1过过温	SMTemp7T1过过温
2197		32		15		SMTemp7 Temp1 High 1	SMTemp7 T1 Hi1	SMTemp7 T1过温		SMTemp7T1过温
2198		32		15		SMTemp7 Temp1 Low	SMTemp7 T1 Low	SMTemp7 T1低温		SMTemp7T1低温
2199		32		15		SMTemp7 Temp2 High 2	SMTemp7 T2 Hi2	SMTemp7 T2过过温	SMTemp7T2过过温
2200		32		15		SMTemp7 Temp2 High 1	SMTemp7 T2 Hi1	SMTemp7 T2过温		SMTemp7T2过温
2201		32		15		SMTemp7 Temp2 Low	SMTemp7 T2 Low	SMTemp7 T2低温		SMTemp7T2低温
2202		32		15		SMTemp7 Temp3 High 2	SMTemp7 T3 Hi2	SMTemp7 T3过过温	SMTemp7T3过过温
2203		32		15		SMTemp7 Temp3 High 1	SMTemp7 T3 Hi1	SMTemp7 T3过温		SMTemp7T3过温
2204		32		15		SMTemp7 Temp3 Low	SMTemp7 T3 Low	SMTemp7 T3低温		SMTemp7T3低温
2205		32		15		SMTemp7 Temp4 High 2	SMTemp7 T4 Hi2	SMTemp7 T4过过温	SMTemp7T4过过温
2206		32		15		SMTemp7 Temp4 High 1	SMTemp7 T4 Hi1	SMTemp7 T4过温		SMTemp7T4过温
2207		32		15		SMTemp7 Temp4 Low	SMTemp7 T4 Low	SMTemp7 T4低温		SMTemp7T4低温
2208		32		15		SMTemp7 Temp5 High 2	SMTemp7 T5 Hi2	SMTemp7 T5过过温	SMTemp7T5过过温
2209		32		15		SMTemp7 Temp5 High 1	SMTemp7 T5 Hi1	SMTemp7 T5过温		SMTemp7T5过温
2210		32		15		SMTemp7 Temp5 Low	SMTemp7 T5 Low	SMTemp7 T5低温		SMTemp7T5低温
2211		32		15		SMTemp7 Temp6 High 2	SMTemp7 T6 Hi2	SMTemp7 T6过过温	SMTemp7T6过过温
2212		32		15		SMTemp7 Temp6 High 1	SMTemp7 T6 Hi1	SMTemp7 T6过温		SMTemp7T6过温
2213		32		15		SMTemp7 Temp6 Low	SMTemp7 T6 Low	SMTemp7 T6低温		SMTemp7T6低温
2214		32		15		SMTemp7 Temp7 High 2	SMTemp7 T7 Hi2	SMTemp7 T7过过温	SMTemp7T7过过温
2215		32		15		SMTemp7 Temp7 High 1	SMTemp7 T7 Hi1	SMTemp7 T7过温		SMTemp7T7过温
2216		32		15		SMTemp7 Temp7 Low	SMTemp7 T7 Low	SMTemp7 T7低温		SMTemp7T7低温
2217		32		15		SMTemp7 Temp8 High 2	SMTemp7 T8 Hi2	SMTemp7 T8过过温	SMTemp7T8过过温
2218		32		15		SMTemp7 Temp8 High 1	SMTemp7 T8 Hi1	SMTemp7 T8过温		SMTemp7T8过温
2219		32		15		SMTemp7 Temp8 Low	SMTemp7 T8 Low	SMTemp7 T8低温		SMTemp7T8低温
2220		32		15		SMTemp8 Temp1 High 2	SMTemp8 T1 Hi2	SMTemp8 T1过过温	SMTemp8T1过过温
2221		32		15		SMTemp8 Temp1 High 1	SMTemp8 T1 Hi1	SMTemp8 T1过温		SMTemp8T1过温
2222		32		15		SMTemp8 Temp1 Low	SMTemp8 T1 Low	SMTemp8 T1低温		SMTemp8T1低温
2223		32		15		SMTemp8 Temp2 High 2	SMTemp8 T2 Hi2	SMTemp8 T2过过温	SMTemp8T2过过温
2224		32		15		SMTemp8 Temp2 High 1	SMTemp8 T2 Hi1	SMTemp8 T2过温		SMTemp8T2过温
2225		32		15		SMTemp8 Temp2 Low	SMTemp8 T2 Low	SMTemp8 T2低温		SMTemp8T2低温
2226		32		15		SMTemp8 Temp3 High 2	SMTemp8 T3 Hi2	SMTemp8 T3过过温	SMTemp8T3过过温
2227		32		15		SMTemp8 Temp3 High 1	SMTemp8 T3 Hi1	SMTemp8 T3过温		SMTemp8T3过温
2228		32		15		SMTemp8 Temp3 Low	SMTemp8 T3 Low	SMTemp8 T3低温		SMTemp8T3低温
2229		32		15		SMTemp8 Temp4 High 2	SMTemp8 T4 Hi2	SMTemp8 T4过过温	SMTemp8T4过过温
2230		32		15		SMTemp8 Temp4 High 1	SMTemp8 T4 Hi1	SMTemp8 T4过温		SMTemp8T4过温
2231		32		15		SMTemp8 Temp4 Low	SMTemp8 T4 Low	SMTemp8 T4低温		SMTemp8T4低温
2232		32		15		SMTemp8 Temp5 High 2	SMTemp8 T5 Hi2	SMTemp8 T5过过温	SMTemp8T5过过温
2233		32		15		SMTemp8 Temp5 High 1	SMTemp8 T5 Hi1	SMTemp8 T5过温		SMTemp8T5过温
2234		32		15		SMTemp8 Temp5 Low	SMTemp8 T5 Low	SMTemp8 T5低温		SMTemp8T5低温
2235		32		15		SMTemp8 Temp6 High 2	SMTemp8 T6 Hi2	SMTemp8 T6过过温	SMTemp8T6过过温
2236		32		15		SMTemp8 Temp6 High 1	SMTemp8 T6 Hi1	SMTemp8 T6过温		SMTemp8T6过温
2237		32		15		SMTemp8 Temp6 Low	SMTemp8 T6 Low	SMTemp8 T6低温		SMTemp8T6低温
2238		32		15		SMTemp8 Temp7 High 2	SMTemp8 T7 Hi2	SMTemp8 T7过过温	SMTemp8T7过过温
2239		32		15		SMTemp8 Temp7 High 1	SMTemp8 T7 Hi1	SMTemp8 T7过温		SMTemp8T7过温
2240		32		15		SMTemp8 Temp7 Low	SMTemp8 T7 Low	SMTemp8 T7低温		SMTemp8T7低温
2241		32		15		SMTemp8 Temp8 High 2	SMTemp8 T8 Hi2	SMTemp8 T8过过温	SMTemp8T8过过温
2242		32		15		SMTemp8 Temp8 High 1	SMTemp8 T8 Hi1	SMTemp8 T8过温		SMTemp8T8过温
2243		32		15		SMTemp8 Temp8 Low	SMTemp8 T8 Low	SMTemp8 T8低温		SMTemp8T8低温

2244		32		15		None			None		未定义			未定义
2245		32		15		High Load Level1	HighLoadLevel1	重载1			重载1
2246		32		15		High Load Level2	HighLoadLevel2	重载2			重载2

2247		32		15		Maximum			Maximum			最大温度		最大温度
2248		32		15		Average			Average			平均温度		平均温度

2249		32		15		Solar Mode		Solar Mode		太阳能运行		太阳能运行
2250		32		15		Running Way(For Solar)	Running Way		工作方式		工作方式
2251		32		15		Disabled			Disabled			禁止			禁止
2252		32		15		RECT-SOLAR		RECT-SOLAR		模块太阳能混合		模块太阳能混合
2253		32		15		SOLAR			SOLAR			纯太阳能		纯太阳能
2254		32		15		RECT First		RECT First		模块优先		模块优先
2255		32		15		Solar First		Solar First		太阳能优先		太阳能优先
2256		32		15		Please reboot after changing			Please reboot		重启使参数生效		请重启
2257		32		15		CSU Failure			CSU Failure		CSU Failure		CSU Failure
2258		32		15		SSL and SNMPV3			SSL and SNMPV3		SSL和SNMPV3		SSL and SNMPV3
2259		32		15		Adjust Bus Input Voltage	Adjust In-Volt		母排校准输入电压	母排校准电压
2260		32		15		Confirm Voltage Supplied	Confirm Voltage		确认已输入电压		确认已输入电压
2261		32		15		Converter Only			Converter Only		DC/DC独立工作		DC/DC独立工作
2262		32		15		Net-Port Reset Interval		Reset Interval		网口重启间隔		网口重启间隔
2263		32		15		DC1 Load			DC1 Load		直流1负载		直流1负载

2264	32		15		NCU DI1				NCU DI1				NCU数字输入1			NCU数字输入1
2265	32		15		NCU DI2				NCU DI2				NCU数字输入2			NCU数字输入2
2266	32		15		NCU DI3				NCU DI3				NCU数字输入3			NCU数字输入3
2267	32		15		NCU DI4				NCU DI4				NCU数字输入4			NCU数字输入4
2268	32			15		Digital Input 1		DI 1		数字量输入1		数字输入1
2269	32			15		Digital Input 2		DI 2		数字量输入2		数字输入2
2270	32			15		Digital Input 3		DI 3		数字量输入3		数字输入3
2271	32			15		Digital Input 4		DI 4		数字量输入4		数字输入4
2272	32			15		DI1 Alarm State		DI1 Alm State		数字量输入1告警条件		输入1告警条件
2273	32			15		DI2 Alarm State		DI2 Alm State		数字量输入2告警条件		输入2告警条件
2274	32			15		DI3 Alarm State		DI3 Alm State		数字量输入3告警条件		输入3告警条件
2275	32			15		DI4 Alarm State		DI4 Alm State		数字量输入4告警条件		输入4告警条件
2276	32			15		DI1 Alarm			DI1 Alarm			数字量输入1告警		输入1告警
2277	32			15		DI2 Alarm			DI2 Alarm			数字量输入2告警		输入2告警
2278	32			15		DI3 Alarm			DI3 Alarm			数字量输入3告警		输入3告警
2279	32			15		DI4 Alarm			DI4 Alarm			数字量输入4告警		输入4告警
2280	32			15		None			None		不存在			不存在
2281	32			15		Exist			Exist		存在			存在
2282	32			15		IB01 State			IB01 State		IB01存在状态			IB01存在状态
2283	32		15		DO1-Relay Output			DO1-RelayOutput		继电器1			继电器1   
2284	32		15		DO2-Relay Output			DO2-RelayOutput		继电器2			继电器2  
2285	32		15		DO3-Relay Output			DO3-RelayOutput		继电器3			继电器3  
2286	32		15		DO4-Relay Output			DO4-RelayOutput		继电器4			继电器4  
2287	32		15		Time Display Format		Time Format		时间格式			时间格式  
2288	32		15		DD/MM/YYYY			DD/MM/YYYY		DD/MM/YYYY			DD/MM/YYYY
2289	32		15		MM/DD/YYYY			MM/DD/YYYY		MM/DD/YYYY			MM/DD/YYYY
2290	32		15		YYYY/MM/DD			YYYY/MM/DD		YYYY/MM/DD			YYYY/MM/DD
2291	32		15		Help				Help		帮助				帮助  
2292	32		15		Current Protocol Type		Protocol	当前协议类型			当前协议类型  
2293	32		15		EEM				EEM		EEM				EEM  
2294	32		15		YDN23				YDN23		YDN23				YDN23
2295	32		15		Modbus				ModBus		ModBus				ModBus
2296	32		15		SMS Alarm Level			SMS Alarm Level		短讯告警级别				短讯告警级别
2297	32		15		Disabled			Disabled			禁止			禁止
2298	32		15		Observation		Observation		一般告警		一般告警
2299	32		15		Major			Major			重要告警		重要告警
2300	32		15		Critical		Critical		紧急告警		紧急告警  
2301	64		64		SPD is not connected to system or SPD is broken.	SPD is not connected to system or SPD is broken.	SPD未连接到系统上或已经损坏!	SPD未连接到系统上或已经损坏!
2302	32		15		Big Screen		Big Screen		大屏			大屏
2303	32		15		EMAIL Alarm Level			EMAIL Alm Level		邮件告警级别				邮件告警级别
2304	32		15		HLMS Protocol Type	Protocol Type		后台协议类型		后台协议类型
2305	32		15		EEM			EEM			EEM			EEM
2306	32		15		YDN23			YDN23			YDN23			YDN23
2307	32		15		Modbus			Modbus			Modbus			Modbus
2308		32		15		24V Display State		24V Display		24V参数显示		24V参数显示
2309		32		15		Syst Volt Level				Syst Volt Level				系统电压等级				系统电压等级
2310		32		15		48 V System				48 V System				48V系统					48V系统
2311		32		15		24 V System				24 V System				24V系统					24V系统
2312		32		15		Power Input Type			Input Type			供电类型				供电类型
2313		32		15		AC Input				AC Input				交流输入				交流输入
2314		32		15		Diesel Input				Diesel Input				油机输入				油机输入
2315		32		15		2nd IB2 Temp1		2nd IB2 Temp1	2nd IB2 温度1		2nd IB2 温度1
2316		32		15		2nd IB2 Temp2		2nd IB2 Temp2	2nd IB2 温度2		2nd IB2 温度2
2317		32		15		EIB2 Temp1		EIB2 Temp1	EIB2 温度1		EIB2 温度1
2318		32		15		EIB2 Temp2		EIB2 Temp2	EIB2 温度2		EIB2 温度2
2319		32		15		2nd IB2 Temp1 High 2	2nd IB2 T1 Hi2	2nd IB2 T1过过温	2ndIB2 T1过过温
2320		32		15		2nd IB2 Temp1 High 1	2nd IB2 T1 Hi1	2nd IB2 T1过温		2ndIB2 T1过温
2321		32		15		2nd IB2 Temp1 Low	2nd IB2 T1 Low	2nd IB2 T1低温		2ndIB2 T1低温
2322		32		15		2nd IB2 Temp2 High 2	2nd IB2 T2 Hi2	2nd IB2 T2过过温	2ndIB2 T2过过温
2323		32		15		2nd IB2 Temp2 High 1	2nd IB2 T2 Hi1	2nd IB2 T2过温		2ndIB2 T2过温
2324		32		15		2nd IB2 Temp2 Low	2nd IB2 T2 Low	2nd IB2 T2低温		2ndIB2 T2低温
2325		32		15		EIB2 Temp1 High 2	EIB2 T1 Hi2	EIB2 T1过过温		EIB2 T1过过温
2326		32		15		EIB2 Temp1 High 1	EIB2 T1 Hi1	EIB2 T1过温		EIB2 T1过温
2327		32		15		EIB2 Temp1 Low		EIB2 T1 Low	EIB2 T1低温		EIB2 T1低温
2328		32		15		EIB2 Temp2 High 2	EIB2 T2 Hi2	EIB2 T2过过温		EIB2 T2过过温
2329		32		15		EIB2 Temp2 High 1	EIB2 T2 Hi1	EIB2 T2过温		EIB2 T2过温
2330		32		15		EIB2 Temp2 Low		EIB2 T2 Low	EIB2 T2低温		EIB2 T2低温
2331	32		15		NCU DO1 Test			NCU DO1 Test	测试继电器14		测试继电器14
2332	32		15		NCU DO2 Test			NCU DO2 Test	测试继电器15		测试继电器15
2333	32		15		NCU DO3 Test			NCU DO3 Test	测试继电器16		测试继电器16
2334	32		15		NCU DO4 Test			NCU DO4 Test		测试继电器17		测试继电器17
2335	32		15		Total Output Rated			Total Output		总输出能力		总输出能力
2336	32		15		Load current capacity			Load capacity		输出百分比		输出百分比
2337	32		15		EES System Mode			EES System Mode		EES模式		EES模式
2338	32		15		SMS Modem Fail			SMS Modem Fail		SMS模块故障		SMS模块故障
2339		32		15		SMDU-EIB Mode		SMDU-EIB Mode		SMDU-EIB模式		SMDU-EIB模式
2340		32		15		Load Switch				Load Switch				负载开关				负载开关
2341		32		15		Manual State			Manual State			手动状态				手动状态
2342		32		15		Converter Voltage Level			Volt Level			逆变模块等级				逆变模块等级
2343		32		15		CB Threshold  Value			Threshold Value		熔丝门限值	熔丝门限值
2344		32		15		CB Overload  Value			Overload Value			熔丝过载值	熔丝过载值
2345		32		15		SNMP Config Error		SNMP Config Err		SNMP配置错误			SNMP配置错误
2346		32		15		Fiamm Battery		Fiamm Battery		Fiamm电池	Fiamm电池
2347		32		15		TL1			TL1			TL1		TL1
2348		32		15		TL1 Port Activation	Port Activation		TL1端口激活	端口激活
2349		32		15		TL1 Protocol Media	Protocol Media		TL1协议媒质	协议媒质
2350		32		15		TL1 Access Port		Access Port		TL1端口参数	端口参数
2351		32		15		TL1 Port Keep Alive	Keep Alive		TL1保持端口激活	保持激活
2352		32		15		TL1 Session Timeout	Session Timeout		TL1连接超时	连接超时
2353		32		15		IPV4			IPV4			IPV4		IPV4
2354		32		15		IPV6			IPV6			IPV6		IPV6
2355		32		15		Ambient Temp Summary Alarm	AmbTempSummAlm		环境温度告警			环境温度告警
2356		32		15		DHCP Enable		DHCP Enable		DHCP使能	DHCP使能
2357		32		15		Modbus Config Error	ModbusConfigErr		Modbus配置错误		Modbus配置错误
2358		32		15		TL1 AID Delimeter	AID Delimeter		TL1 AID分离器	TL1 AID分离器

2400	32		15		Not Active			Not Active		关			关
2401	32		15		Active				Active			开			开

2402		32		15		IB4 Communication Status		IB4 Comm Status		IB4通信状态		IB4通信状态
2403		32		15		IB4 Communication Fail			IB4 Comm Fail		IB4通信失败		IB4通信失败

2406	32		15		IB2-2 Temp1		IB2-2 T1		IB2-2温度1		IB2-2温度1
2407	32		15		IB2-2 Temp2		IB2-2 T2		IB2-2温度2		IB2-2温度2
2408	32		15		EIB-2 Temp1		EIB-2 T1		EIB-2温度1		EIB-2温度1
2409	32		15		EIB-2 Temp2		EIB-2 T2		EIB-2温度2		EIB-2温度2

2410		32		15		IB2-2 Temp1 High 2	IB2-2 T1 Hi2	IB2-2温度1过过温	IB2-2温度1过过温
2411		32		15		IB2-2 Temp1 High 1	IB2-2 T1 Hi1	IB2-2温度1过温		IB2-2温度1过温
2412		32		15		IB2-2 Temp1 Low		IB2-2 T1 Low	IB2-2温度1低温		IB2-2温度1低温
2413		32		15		IB2-2 Temp2 High 2	IB2-2 T2 Hi2	IB2-2温度2过过温	IB2-2温度2过过温
2414		32		15		IB2-2 Temp2 High 1	IB2-2 T2 Hi1	IB2-2温度2过温		IB2-2温度2过温
2415		32		15		IB2-2 Temp2 Low		IB2-2 T2 Low	IB2-2温度2低温		IB2-2温度2低温
2416		32		15		EIB-2 Temp1 High 2	EIB-2 T1 Hi2	EIB-2温度1过过温	EIB-2温度1过过温
2417		32		15		EIB-2 Temp1 High 1	EIB-2 T1 Hi1	EIB-2温度1过温		EIB-2温度1过温
2418		32		15		EIB-2 Temp1 Low		EIB-2 T1 Low	EIB-2温度1低温		EIB-2温度1低温
2419		32		15		EIB-2 Temp2 High 2	EIB-2 T2 Hi2	EIB-2温度2过过温	EIB-2温度2过过温
2420		32		15		EIB-2 Temp2 High 1	EIB-2 T2 Hi1	EIB-2温度2过温		EIB-2温度2过温
2421		32		15		EIB-2 Temp2 Low		EIB-2 T2 Low	EIB-2温度2低温		EIB-2温度2低温

2422	32		15		IB2-2 Temp 1			IB2-2 Temp1		IB2-2温度1			IB2-2温度1
2423	32		15		IB2-2 Temp 1			IB2-2 Temp1		IB2-2温度1使能			IB2-2温度1使能
2427	32		15		IB2-2 Temp 1 Not Used		IB2-2T1NotUsed		IB2-2温度1未用			IB2-2温度1未用
2428	32		15		IB2-2 Temp 1 Sensor Fail	IB2-2T1SensFail		IB2-2温度1故障			IB2-2温度1故障
2432	32		15		IB2-2 Temp 2			IB2-2 Temp2		IB2-2温度2			IB2-2温度2
2433	32		15		IB2-2 Temp 2			IB2-2 Temp2		IB2-2温度2使能			IB2-2温度2使能
2437	32		15		IB2-2 Temp 2 Not Used		IB2-2T2NotUsed		IB2-2温度2未用			IB2-2温度2未用
2438	32		15		IB2-2 Temp 2 Sensor Fail	IB2-2T2SensFail		IB2-2温度2故障			IB2-2温度2故障
2442	32		15		EIB-2 Temp 1			EIB-2 Temp1		EIB-2温度1			EIB-2温度1
2443	32		15		EIB-2 Temp 1			EIB-2 Temp1		EIB-2温度1使能			EIB-2温度1使能
2447	32		15		EIB-2 Temp 1 Not Used		EIB-2T1NotUsed		EIB-2温度1未用			EIB-2温度1未用
2448	32		15		EIB-2 Temp 1 Sensor Fail	EIB-2T1SensFail		EIB-2温度1故障			EIB-2温度1故障
2452	32		15		EIB-2 Temp 2			EIB-2 Temp2		EIB-2温度2			EIB-2温度2
2453	32		15		EIB-2 Temp 2			EIB-2 Temp2		EIB-2温度2使能			EIB-2温度2使能
2457	32		15		EIB-2 Temp 2 Not Used		EIB-2T2NotUsed		EIB-2温度2未用			EIB-2温度2未用
2458	32		15		EIB-2 Temp 2 Sensor Fail	EIB-2T2SensFail		EIB-2温度2故障			EIB-2温度2故障

2480	32		15		Temperature Format		Temp Format		温度格式切换		温度格式切换
2481	32		15		Celsius				Celsius			摄氏度			摄氏度
2482	32		15		Fahrenheit			Fahrenheit		华氏度			华氏度

2483	32		15		System Alarm Function		Sys Alarm Func		告警功能		告警功能
2484	32		15		CR Only				CR Only			仅仅CR			仅仅CR
2485	32		15		CR and MJ			CR and MJ		CR和MJ			CR和MJ
2486	32		15		CR, MJ, and OB			CR, MJ, and OB		CR,MJ和OB		CR,MJ和OB
2487	32		15		Verizon Display			Verizon Display		Verizon显示			Verizon显示
2488	32		15		Normal Mode			Normal Mode		常规模式			常规模式
2489	32		15		Verizon Mode			Verizon Mode		Verizon模式			Verizon模式
				
2500	32		15		Source Current		Source Current		来源电流		来源电流
2501	32		15		Source Current Number	SourceCurrNum		来源电流个数		来源电流个数

2502	32		15		Source Rated Current	SourceRatedCurr		来源额定电流		来源额定电流
2503	32		15		NTP Function Enable	NTPFuncEnable		NTP功能开启		NTP功能开启

2510	32		15		Alarm Sound Activation	Alm Sound Act		告警音激活		告警音激活
2511	32		15		Close Alarm Sound	Close Alm Sound		关告警音		关告警音
2512	32		15		Close				Close		关闭			关闭
2513	32		15		Open				Open		开启			开启

2521	32		15		SMDUE1 Temp1		SMDUE1 T1		SMDUE1温度1		SMDUE1温度1
2522	32		15		SMDUE1 Temp2		SMDUE1 T2		SMDUE1温度2		SMDUE1温度2
2523	32		15		SMDUE1 Temp3		SMDUE1 T3		SMDUE1温度3		SMDUE1温度3
2524	32		15		SMDUE1 Temp4		SMDUE1 T4		SMDUE1温度4		SMDUE1温度4
2525	32		15		SMDUE1 Temp5		SMDUE1 T5		SMDUE1温度5		SMDUE1温度5
2526	32		15		SMDUE1 Temp6		SMDUE1 T6		SMDUE1温度6		SMDUE1温度6
2527	32		15		SMDUE1 Temp7		SMDUE1 T7		SMDUE1温度7		SMDUE1温度7
2528	32		15		SMDUE1 Temp8		SMDUE1 T8		SMDUE1温度8		SMDUE1温度8
2529	32		15		SMDUE1 Temp9		SMDUE1 T9		SMDUE1温度9		SMDUE1温度9
2530	32		15		SMDUE1 Temp10		SMDUE1 T10	SMDUE1温度10		SMDUE1温度10
2531	32		15		SMDUE2 Temp1		SMDUE2 T1		SMDUE2温度1		SMDUE2温度1
2532	32		15		SMDUE2 Temp2		SMDUE2 T2		SMDUE2温度2		SMDUE2温度2
2533	32		15		SMDUE2 Temp3		SMDUE2 T3		SMDUE2温度3		SMDUE2温度3
2534	32		15		SMDUE2 Temp4		SMDUE2 T4		SMDUE2温度4		SMDUE2温度4
2535	32		15		SMDUE2 Temp5		SMDUE2 T5		SMDUE2温度5		SMDUE2温度5
2536	32		15		SMDUE2 Temp6		SMDUE2 T6		SMDUE2温度6		SMDUE2温度6
2537	32		15		SMDUE2 Temp7		SMDUE2 T7		SMDUE2温度7		SMDUE2温度7
2538	32		15		SMDUE2 Temp8		SMDUE2 T8		SMDUE2温度8		SMDUE2温度8
2539	32		15		SMDUE2 Temp9		SMDUE2 T9		SMDUE2温度9		SMDUE2温度9
2540	32		15		SMDUE2 Temp10		SMDUE2 T10	SMDUE2温度10		SMDUE2温度10
2541		32		15		SMDUE1 Temp1 High 2		SMDUE1 T1 Hi2		SMDUE1 T1过过温		SMDUE1T1过过温
2542		32		15		SMDUE1 Temp1 High 1		SMDUE1 T1 Hi1		SMDUE1 T1过温		SMDUE1T1过温
2543		32		15		SMDUE1 Temp1 Low			SMDUE1 T1 Low		SMDUE1 T1低温		SMDUE1T1低温
2544		32		15		SMDUE1 Temp2 High 2	SMDUE1 T2 Hi2			SMDUE1 T2过过温		SMDUE1T2过过温
2545		32		15		SMDUE1 Temp2 High 1	SMDUE1 T2 Hi1			SMDUE1 T2过温		SMDUE1T2过温
2546		32		15		SMDUE1 Temp2 Low		SMDUE1 T2 Low			SMDUE1 T2低温		SMDUE1T2低温
2547		32		15		SMDUE1 Temp3 High 2	SMDUE1 T3 Hi2			SMDUE1 T3过过温		SMDUE1T3过过温
2548		32		15		SMDUE1 Temp3 High 1	SMDUE1 T3 Hi1			SMDUE1 T3过温		SMDUE1T3过温
2549		32		15		SMDUE1 Temp3 Low		SMDUE1 T3 Low		SMDUE1 T3低温		SMDUE1T3低温
2550		32		15		SMDUE1 Temp4 High 2	SMDUE1 T4 Hi2		SMDUE1 T4过过温		SMDUE1T4过过温
2551		32		15		SMDUE1 Temp4 High 1	SMDUE1 T4 Hi1		SMDUE1 T4过温		SMDUE1T4过温
2552		32		15		SMDUE1 Temp4 Low		SMDUE1 T4 Low		SMDUE1 T4低温		SMDUE1T4低温
2553		32		15		SMDUE1 Temp5 High 2	SMDUE1 T5 Hi2		SMDUE1 T5过过温		SMDUE1T5过过温
2554		32		15		SMDUE1 Temp5 High 1	SMDUE1 T5 Hi1		SMDUE1 T5过温		SMDUE1T5过温
2555		32		15		SMDUE1 Temp5 Low		SMDUE1 T5 Low		SMDUE1 T5低温		SMDUE1T5低温
2556		32		15		SMDUE1 Temp6 High 2	SMDUE1 T6 Hi2		SMDUE1 T6过过温		SMDUE1T6过过温
2557		32		15		SMDUE1 Temp6 High 1	SMDUE1 T6 Hi1		SMDUE1 T6过温		SMDUE1T6过温
2558		32		15		SMDUE1 Temp6 Low		SMDUE1 T6 Low		SMDUE1 T6低温		SMDUE1T6低温
2559		32		15		SMDUE1 Temp7 High 2	SMDUE1 T7 Hi2		SMDUE1 T7过过温		SMDUE1T7过过温
2560		32		15		SMDUE1 Temp7 High 1	SMDUE1 T7 Hi1		SMDUE1 T7过温		SMDUE1T7过温
2561		32		15		SMDUE1 Temp7 Low		SMDUE1 T7 Low		SMDUE1 T7低温		SMDUE1T7低温
2562		32		15		SMDUE1 Temp8 High 2	SMDUE1 T8 Hi2		SMDUE1 T8过过温		SMDUE1T8过过温
2563		32		15		SMDUE1 Temp8 High 1	SMDUE1 T8 Hi1		SMDUE1 T8过温		SMDUE1T8过温
2564		32		15		SMDUE1 Temp8 Low		SMDUE1 T8 Low		SMDUE1 T8低温		SMDUE1T8低温
2565		32		15		SMDUE1 Temp9 High 2	SMDUE1 T9 Hi2		SMDUE1 T9过过温		SMDUE1T9过过温
2566		32		15		SMDUE1 Temp9 High 1	SMDUE1 T9 Hi1		SMDUE1 T9过温		SMDUE1T9过温
2567		32		15		SMDUE1 Temp9 Low		SMDUE1 T9 Low		SMDUE1 T9低温		SMDUE1T9低温
2568		32		15		SMDUE1 Temp10 High 2	SMDUE1 T10 Hi2		SMDUE1 T10过过温		SMDUE1T10过过温
2569		32		15		SMDUE1 Temp10 High 1	SMDUE1 T10 Hi1		SMDUE1 T10过温		SMDUE1T10过温
2570		32		15		SMDUE1 Temp10 Low	SMDUE1 T10 Low		SMDUE1 T10低温		SMDUE1T10低温
2571		32		15		SMDUE2 Temp1 High 2		SMDUE2 T1 Hi2		SMDUE2 T1过过温		SMDUE2T1过过温
2572		32		15		SMDUE2 Temp1 High 1		SMDUE2 T1 Hi1		SMDUE2 T1过温		SMDUE2T1过温
2573		32		15		SMDUE2 Temp1 Low			SMDUE2 T1 Low		SMDUE2 T1低温		SMDUE2T1低温
2574		32		15		SMDUE2 Temp2 High 2	SMDUE2 T2 Hi2			SMDUE2 T2过过温		SMDUE2T2过过温
2575		32		15		SMDUE2 Temp2 High 1	SMDUE2 T2 Hi1			SMDUE2 T2过温		SMDUE2T2过温
2576		32		15		SMDUE2 Temp2 Low		SMDUE2 T2 Low			SMDUE2 T2低温		SMDUE2T2低温
2077		32		15		SMDUE2 Temp3 High 2	SMDUE2 T3 Hi2			SMDUE2 T3过过温		SMDUE2T3过过温
2078		32		15		SMDUE2 Temp3 High 1	SMDUE2 T3 Hi1			SMDUE2 T3过温		SMDUE2T3过温
2079		32		15		SMDUE2 Temp3 Low		SMDUE2 T3 Low		SMDUE2 T3低温		SMDUE2T3低温
2080		32		15		SMDUE2 Temp4 High 2	SMDUE2 T4 Hi2		SMDUE2 T4过过温		SMDUE2T4过过温
2081		32		15		SMDUE2 Temp4 High 1	SMDUE2 T4 Hi1		SMDUE2 T4过温		SMDUE2T4过温
2082		32		15		SMDUE2 Temp4 Low		SMDUE2 T4 Low		SMDUE2 T4低温		SMDUE2T4低温
2083		32		15		SMDUE2 Temp5 High 2	SMDUE2 T5 Hi2		SMDUE2 T5过过温		SMDUE2T5过过温
2084		32		15		SMDUE2 Temp5 High 1	SMDUE2 T5 Hi1		SMDUE2 T5过温		SMDUE2T5过温
2585		32		15		SMDUE2 Temp5 Low		SMDUE2 T5 Low		SMDUE2 T5低温		SMDUE2T5低温
2586		32		15		SMDUE2 Temp6 High 2	SMDUE2 T6 Hi2		SMDUE2 T6过过温		SMDUE2T6过过温
2587		32		15		SMDUE2 Temp6 High 1	SMDUE2 T6 Hi1		SMDUE2 T6过温		SMDUE2T6过温
2588		32		15		SMDUE2 Temp6 Low		SMDUE2 T6 Low		SMDUE2 T6低温		SMDUE2T6低温
2589		32		15		SMDUE2 Temp7 High 2	SMDUE2 T7 Hi2		SMDUE2 T7过过温		SMDUE2T7过过温
2590		32		15		SMDUE2 Temp7 High 1	SMDUE2 T7 Hi1		SMDUE2 T7过温		SMDUE2T7过温
2591		32		15		SMDUE2 Temp7 Low		SMDUE2 T7 Low		SMDUE2 T7低温		SMDUE2T7低温
2592		32		15		SMDUE2 Temp8 High 2	SMDUE2 T8 Hi2		SMDUE2 T8过过温		SMDUE2T8过过温
2593		32		15		SMDUE2 Temp8 High 1	SMDUE2 T8 Hi1		SMDUE2 T8过温		SMDUE2T8过温
2594		32		15		SMDUE2 Temp8 Low		SMDUE2 T8 Low		SMDUE2 T8低温		SMDUE2T8低温
2595		32		15		SMDUE2 Temp9 High 2	SMDUE2 T9 Hi2		SMDUE2 T9过过温		SMDUE2T9过过温
2596		32		15		SMDUE2 Temp9 High 1	SMDUE2 T9 Hi1		SMDUE2 T9过温		SMDUE2T9过温
2597		32		15		SMDUE2 Temp9 Low		SMDUE2 T9 Low		SMDUE2 T9低温		SMDUE2T9低温
2598		32		15		SMDUE2 Temp10 High 2	SMDUE2 T10 Hi2		SMDUE2 T10过过温		SMDUE2T10过过温
2599		32		15		SMDUE2 Temp10 High 1	SMDUE2 T10 Hi1		SMDUE2 T10过温		SMDUE2T10过温
2600		32		15		SMDUE2 Temp10 Low	SMDUE2 T10 Low		SMDUE2 T10低温		SMDUE2T10低温


2900		32		15		Virtual SMDUP Control	Vir SMDUP Ctl		虚拟SMDUP控制		虚拟SMDUP控制
2901		32		15		Yes						Yes					是					是






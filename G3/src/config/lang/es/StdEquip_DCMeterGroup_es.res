﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
##==============================================================123456789012345678901234567890--123456789012345---------123456789012345678901234567890--123456789012345
1		32			15			DC Meter Group			DCMeter Group		Grupo Contador CC		Grp Contador CC
2		32			15			DC Meter Number			DCMeter Num		Número de Contador CC		Núm Contador
3		32			15			Communication Fail	Comm Fail		Fallo de comunicación		Fallo COM
4		32			15			Existence State			Existence		Detección			Detección
5		32			15			Existent			Existent		Existente			Existente
6		32			15			Not Existent			Not Existent		No Existente			No Existente
11		32			15			DC Meter Lost			DCMeter Lost		Contador CC perdido		Cont CC perdido
12		32			15			DC Meter Num Last Time	LastDCMeter Num	DC Meter Num Last Time		DCMeter NumLast
13		32			15			Clear DC Meter Lost Alarm	ClrDCMeterLost		Cesar alarma Contador perdido	Cesar CCperdido
14		32			15			Clear				Clear			Sí				Sí
15		32			15			Total Energy Consumption	TotalEnergy		Consumo total de energía	Consumo total

﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Bus Bar Voltage				Bus Bar Voltage		Tensión barra distribución	Tensión barra D
2	32			15			Shunt 1					Shunt 1		Shunt 1					Shunt 1
3	32			15			Shunt 2					Shunt 2		Shunt 2					Shunt 2
4	32			15			Shunt 3					Shunt 3		Shunt 3					Shunt 3
5	32			15			Shunt 4					Shunt 4		Shunt 4					Shunt 4
6	32			15			Fuse 1					Fuse 1			Fusible 1			Fusible 1
7	32			15			Fuse 2					Fuse 2			Fusible 2			Fusible 2
8	32			15			Fuse 3					Fuse 3			Fusible 3			Fusible 3
9	32			15			Fuse 4					Fuse 4			Fusible 4			Fusible 4
10	32			15			Fuse 5					Fuse 5			Fusible 5			Fusible 5
11	32			15			Fuse 6					Fuse 6			Fusible 6			Fusible 6
12	32			15			Fuse 7					Fuse 7			Fusible 7			Fusible 7
13	32			15			Fuse 8					Fuse 8			Fusible 8			Fusible 8
14	32			15			Fuse 9					Fuse 9			Fusible 9			Fusible 9
15	32			15			Fuse 10					Fuse 10			Fusible 10			Fusible 10
16	32			15			Fuse 11					Fuse 11			Fusible 11			Fusible 11
17	32			15			Fuse 12					Fuse 12			Fusible 12			Fusible 12
18	32			15			Fuse 13					Fuse 13			Fusible 13			Fusible 13
19	32			15			Fuse 14					Fuse 14			Fusible 14			Fusible 14
20	32			15			Fuse 15					Fuse 15			Fusible 15			Fusible 15
21	32			15			Fuse 16					Fuse 16			Fusible 16			Fusible 16
22	32			15			Run Time				Run Time		Tiempo de operación		Tiempo operando
23	32			15			LV Disconnect 1 Control			LVD 1 Control		Control LVD1			Control LVD1
24	32			15			LV Disconnect 2 Control			LVD 2 Control		Control LVD2			Control LVD2
25	32			15			LV Disconnect 1 Voltage			LVD 1 Voltage		Tensión LVD1			Tensión LVD1
26	32			15			LV Resconnect 1 Voltage			LVR 1 Voltage		Tensión LVR1			Tensión LVR1
27	32			15			LV Disconnect 2 Voltage			LVD 2 Voltage		Tensión LVD2			Tensión LVD2
28	32			15			LV Resconnect 2 Voltage			LVR 2 Voltage		Tensión LVR2			Tensión LVR2
29	32			15			On					On			Conectado			Conectado
30	32			15			Off					Off			Desconectado			Desconectado
31	32			15			Normal					Normal			Normal				Normal
32	32			15			Error					Error			Error				Error
33	32			15			On					On			Conectado			Conectado
34	32			15			Fuse 1 Alarm				Fuse 1 Alarm	Alarma Fusible 1		Alarma fus1
35	32			15			Fuse 2 Alarm				Fuse 2 Alarm	Alarma Fusible 2		Alarma fus2
36	32			15			Fuse 3 Alarm				Fuse 3 Alarm	Alarma Fusible 3		Alarma fus3
37	32			15			Fuse 4 Alarm				Fuse 4 Alarm	Alarma Fusible 4		Alarma fus4
38	32			15			Fuse 5 Alarm				Fuse 5 Alarm	Alarma Fusible 5		Alarma fus5
39	32			15			Fuse 6 Alarm				Fuse 6 Alarm	Alarma Fusible 6		Alarma fus6
40	32			15			Fuse 7 Alarm				Fuse 7 Alarm	Alarma Fusible 7		Alarma fus7
41	32			15			Fuse 8 Alarm				Fuse 8 Alarm	Alarma Fusible 8		Alarma fus8
42	32			15			Fuse 9 Alarm				Fuse 9 Alarm	Alarma Fusible 9		Alarma fus9
43	32			15			Fuse 10 Alarm				Fuse 10 Alarm		Alarma Fusible 10		Alarma fus10
44	32			15			Fuse 11 Alarm				Fuse 11 Alarm		Alarma Fusible 11		Alarma fus11
45	32			15			Fuse 12 Alarm				Fuse 12 Alarm		Alarma Fusible 12		Alarma fus12
46	32			15			Fuse 13 Alarm				Fuse 13 Alarm		Alarma Fusible 13		Alarma fus13
47	32			15			Fuse 14 Alarm				Fuse 14 Alarm		Alarma Fusible 14		Alarma fus14
48	32			15			Fuse 15 Alarm				Fuse 15 Alarm		Alarma Fusible 15		Alarma fus15
49	32			15			Fuse 16 Alarm				Fuse 16 Alarm		Alarma Fusible 16		Alarma fus16
50	32			15			HW Test Alarm				HW Test Alarm		Alarma prueba HW		Alarma test HW
51	32			15			SMDUP 10					SMDUP 10			Unidad SMDUP10			Unidad SMDUP10
52	32			15			Battery Fuse 1 Voltage			BattFuse 1 Volt			Tensión fusible batería 1	Tens fus bat1
53	32			15			Battery Fuse 2 Voltage			BattFuse 2 Volt			Tensión fusible batería 2	Tens fus bat2
54	32			15			Battery Fuse 3 Voltage			BattFuse 3 Volt			Tensión fusible batería 3	Tens fus bat3
55	32			15			Battery Fuse 4 Voltage			BattFuse 4 Volt			Tensión fusible batería 4	Tens fus bat4
56	32			15			Battery Fuse 1 Status			Batt Fuse 1				Estado fusible batería 1	Estado fus bat1
57	32			15			Battery Fuse 2 Status			Batt Fuse 2				Estado fusible batería 2	Estado fus bat2
58	32			15			Battery Fuse 3 Status			Batt Fuse 3				Estado fusible batería 3	Estado fus bat3
59	32			15			Battery Fuse 4 Status			Batt Fuse 4				Estado fusible batería 4	Estado fus bat4
60	32			15			On					On			Conectado			Conectado
61	32			15			Off					Off			Desconectado			Desconectado
62	32			15			Battery Fuse 1 Alarm			Batt Fuse 1 Alm	Alarma fusible batería 1	Alarma fus1 bat
63	32			15			Battery Fuse 2 Alarm			Batt Fuse 2 Alm	Alarma fusible batería 2	Alarma fus2 bat
64	32			15			Battery Fuse 3 Alarm			Batt Fuse 3 Alm	Alarma fusible batería 3	Alarma fus3 bat
65	32			15			Battery Fuse 4 Alarm			Batt Fuse 4 Alm	Alarma fusible batería 4	Alarma fus4 bat
66	32			15			All Load Current			All Load Curr		Corriente total carga		Total carga
67	32			15			Over Current Limit (Load)		Over Curr Limit			Valor de sobrecorriente		Sobrecorriente
68	32			15			Over Current (Load)			Over Current		Sobrecorriente			Sobrecorriente
69	32			15		LVD 1					LVD 1			LVD1 habilitado			LVD1 habilitado
70	32			15			LVD 1 Mode				LVD 1 Mode		Modo LVD1			Modo LVD1
71	32			15			LVD 1 Reconnect Delay			LVD1 ReconDelay			Retardo reconexión LVD1		RetarRecon LVD1
72	32			15		LVD 2					LVD 2			LVD2 habilitado			LVD2 habilitado
73	32			15			LVD 2 Mode				LVD 2 Mode		Modo LVD2			Modo LVD2
74	32			15			LVD 2 Reconnect Delay			LVD2 ReconDelay			Retardo reconexión LVD2		RetarRecon LVD2
75	32			15			LVD 1 Status				LVD 1 Status		Estado LVD1			Estado LVD1
76	32			15			LVD 2 Status				LVD 2 Status		Estado LVD2			Estado LVD2
77	32			15			Disabled				Disabled		Deshabilitado			Deshabilitado
78	32			15			Enabled					Enabled			Habilitado			Habilitado
79	32			15			Voltage				Voltage			Por tensión			Por tensión
80	32			15			Time					Time			Por tiempo			Por tiempo
81	32			15			Bus Bar Volt Alarm			Bus Bar Alarm		Alarma Barra Distribución	Alarma bus Dist
82	32			15			Normal					Normal			Normal				Normal
83	32			15			Low					Low			Bajo				Bajo
84	32			15			High					High			Alto				Alto
85	32			15			Under Voltage				Under Voltage		Baja tensión			Baja tensión
86	32			15			Over Voltage				Over Voltage		Alta tensión			Alta tensión
87	32			15			Shunt 1 Current Alarm			Shunt 1 Alarm		Alarma corriente shunt1		Alarma shunt1
88	32			15			Shunt 2 Current Alarm			Shunt 2 Alarm		Alarma corriente shunt2		Alarma shunt2
89	32			15			Shunt 3 Current Alarm			Shunt 3 Alarm		Alarma corriente shunt3		Alarma shunt3
90	32			15			Shunt 4 Current Alarm			Shunt 4 Alarm		Alarma corriente shunt4		Alarma shunt4
91	32			15			Shunt 1 Over Current			Shunt 1 OverCur		Sobrecorriente en shunt1	Sobrecor shunt1
92	32			15			Shunt 2 Over Current			Shunt 2 OverCur		Sobrecorriente en shunt2	Sobrecor shunt2
93	32			15			Shunt 3 Over Current			Shunt 3 OverCur		Sobrecorriente en shunt3	Sobrecor shunt3
94	32			15			Shunt 4 Over Current			Shunt 4 OverCur		Sobrecorriente en shunt4	Sobrecor shunt4
95	32			15			Times of Communication Fail		Times Comm Fail		Fallos de Comunicación		Fallos COM
96	32			15			Existent				Existent		Existente			Existente
97	32			15			Not Existent				Not Existent		Inexistente			Inexistente
98	32			15			Very Low				Very Low		Muy bajo			Muy bajo
99	32			15			Very High				Very High		Muy alto			Muy alto
100	32			15			Switch					Switch			Interruptor			Interruptor
101	32			15			LVD 1 Fail			LVD 1 Fail		Fallo LVD1			Fallo LVD1
102	32			15			LVD 2 Fail			LVD 2 Fail		Fallo LVD2			Fallo LVD2
103	32			15			High Temperature Disconnect 1	HTD 1		Habilitar HTD1			Habilitar HTD1
104	32			15			High Temperature Disconnect 2	HTD 2		Habilitar HTD2			Habilitar HTD2
105	32			15			Battery LVD				Batt LVD	LVD de batería			LVD de batería
106	32			15			No Battery				No Battery		Sin batería			Sin batería
107	32			15			LVD 1					LVD 1			LVD1				LVD1
108	32			15			LVD 2					LVD 2			LVD2				LVD2
109	32			15			Batt Always On				Batt Always On		Batería siempre conectada	Siempre con Bat
110	32			15			Barcode					Barcode			Barcode				Barcode
111	32			15			DC Over Voltage				DC Over Volt	Sobretensión CC			Sobretensión CC
112	32			15			DC Under Voltage			DC Under Volt		Subtensión CC			Subtensión CC
113	32			15			Over Current 1				Over Current 1		Sobrecorriente 1		Sobrecorrien 1
114	32			15			Over Current 2				Over Current 2		Sobrecorriente 2		Sobrecorrien 2
115	32			15			Over Current 3				Over Current 3		Sobrecorriente 3		Sobrecorrien 3
116	32			15			Over Current 4				Over Current 4		Sobrecorriente 4		Sobrecorrien 4
117	32			15			Existence State				Existence State		Detección			Detección
118	32			15			Communication Fail			Comm Fail		Fallos de Comunicación		Fallos COM
119	32			15			Bus Voltage Status			Bus Status		Estado Bus Tensión		Estado Bus V
120	32			15			Comm OK					Comm OK		Comunicación OK		Comunicación OK
121	32			15			All Batteries Comm Fail			All Comm Fail		Todas las baterías en Fallo Com		FalloComTotBat
122	32			15			Communication Fail			Comm Fail		No responde			No responde
123	32			15			Rated Capacity				Rated Capacity		Capacidad Estimada		Capacidad Est
124	32			15			Shunt 5				Shunt 5		Shunt 5				Shunt 5
125	32			15			Shunt 6				Shunt 6		Shunt 6				Shunt 6
126	32			15			Shunt 7				Shunt 7		Shunt 7				Shunt 7
127	32			15			Shunt 8				Shunt 8		Shunt 8				Shunt 8
128	32			15			Shunt 9				Shunt 9		Shunt 9				Shunt 9
129	32			15			Shunt 10			Shunt 10		Shunt 10			Shunt 10
130	32			15			Shunt 11			Shunt 11		Shunt 11			Shunt 11
131	32			15			Shunt 12			Shunt 12		Shunt 12			Shunt 12
132	32			15			Shunt 13			Shunt 13		Shunt 13			Shunt 13
133	32			15			Shunt 14			Shunt 14		Shunt 14			Shunt 14
134	32			15			Shunt 15			Shunt 15		Shunt 15			Shunt 15
135	32			15			Shunt 16			Shunt 16		Shunt 16			Shunt 16
136	32			15			Shunt 17			Shunt 17		Shunt 17			Shunt 17
137	32			15			Shunt 18			Shunt 18		Shunt 18			Shunt 18
138	32			15			Shunt 19			Shunt 19		Shunt 19			Shunt 19
139	32			15			Shunt 20			Shunt 20		Shunt 20			Shunt 20
140	32			15			Shunt 21			Shunt 21		Shunt 21			Shunt 21
141	32			15			Shunt 22			Shunt 22		Shunt 22			Shunt 22
142	32			15			Shunt 23			Shunt 23		Shunt 23			Shunt 23
143	32			15			Shunt 24			Shunt 24		Shunt 24			Shunt 24
144	32			15			Shunt 25			Shunt 25		Shunt 25			Shunt 25
145	32			15			Voltage 1				Voltage 1		Tensión 1			Tensión 1
146	32			15			Voltage 2				Voltage 2		Tensión 2			Tensión 2
147	32			15			Voltage 3				Voltage 3		Tensión 3			Tensión 3
148	32			15			Voltage 4				Voltage 4		Tensión 4			Tensión 4
149	32			15			Voltage 5				Voltage 5		Tensión 5			Tensión 5
150	32			15			Voltage 6				Voltage 6		Tensión 6			Tensión 6
151	32			15			Voltage 7				Voltage 7		Tensión 7			Tensión 7
152	32			15			Voltage 8				Voltage 8		Tensión 8			Tensión 8
153	32			15			Voltage 9				Voltage 9		Tensión 9			Tensión 9
154	32			15			Voltage 10				Voltage 10		Tensión 10			Tensión 10
155	32			15			Voltage 11				Voltage 11		Tensión 11			Tensión 11
156	32			15			Voltage 12				Voltage 12		Tensión 12			Tensión 12
157	32			15			Voltage 13				Voltage 13		Tensión 13			Tensión 13
158	32			15			Voltage 14				Voltage 14		Tensión 14			Tensión 14
159	32			15			Voltage 15				Voltage 15		Tensión 15			Tensión 15
160	32			15			Voltage 16				Voltage 16		Tensión 16			Tensión 16
161	32			15			Voltage 17				Voltage 17		Tensión 17			Tensión 17
162	32			15			Voltage 18				Voltage 18		Tensión 18			Tensión 18
163	32			15			Voltage 19				Voltage 19		Tensión 19			Tensión 19
164	32			15			Voltage 20				Voltage 20		Tensión 20			Tensión 20
165	32			15			Voltage 21				Voltage 21		Tensión 21			Tensión 21
166	32			15			Voltage 22				Voltage 22		Tensión 22			Tensión 22
167	32			15			Voltage 23				Voltage 23		Tensión 23			Tensión 23
168	32			15			Voltage 24				Voltage 24		Tensión 24			Tensión 24
169	32			15			Voltage 25				Voltage 25		Tensión 25			Tensión 25
170	32			15			Current1 High 1 Current		Curr 1 Hi 1		Alta1 Corriente 1		Alta1 Corr1
171	32			15			Current1 High 2 Current		Curr 1 Hi 2		Alta2 Corriente 1		Alta2 Corr1
172	32			15			Current2 High 1 Current			Curr 2 Hi 1		Alta1 Corriente 2		Alta1 Corr2
173	32			15			Current2 High 2 Current			Curr 2 Hi 2		Alta2 Corriente 2		Alta2 Corr2
174	32			15			Current3 High 1 Current			Curr 3 Hi 1		Alta1 Corriente 3		Alta1 Corr3
175	32			15			Current3 High 2 Current			Curr 3 Hi 2		Alta2 Corriente 3		Alta2 Corr3
176	32			15			Current4 High 1 Current			Curr 4 Hi 1		Alta1 Corriente 4		Alta1 Corr4
177	32			15			Current4 High 2 Current			Curr 4 Hi 2		Alta2 Corriente 4		Alta2 Corr4
178	32			15			Current5 High 1 Current			Curr 5 Hi 1		Alta1 Corriente 5		Alta1 Corr5
179	32			15			Current5 High 2 Current			Curr 5 Hi 2		Alta2 Corriente 5		Alta2 Corr5
180	32			15			Current6 High 1 Current			Curr 6 Hi 1		Alta1 Corriente 6		Alta1 Corr6
181	32			15			Current6 High 2 Current			Curr 6 Hi 2		Alta2 Corriente 6		Alta2 Corr6
182	32			15			Current7 High 1 Current			Curr 7 Hi 1		Alta1 Corriente 7		Alta1 Corr7
183	32			15			Current7 High 2 Current			Curr 7 Hi 2		Alta2 Corriente 7		Alta2 Corr7
184	32			15			Current8 High 1 Current			Curr 8 Hi 1		Alta1 Corriente 8		Alta1 Corr8
185	32			15			Current8 High 2 Current			Curr 8 Hi 2		Alta2 Corriente 8		Alta2 Corr8
186	32			15			Current9 High 1 Current			Curr 9 Hi 1		Alta1 Corriente 9		Alta1 Corr9
187	32			15			Current9 High 2 Current			Curr 9 Hi 2		Alta2 Corriente 9		Alta2 Corr9
188	32			15			Current10 High 1 Current		Curr 10 Hi 1	Alta1 Corriente 10		Alta1 Corr10	
189	32			15			Current10 High 2 Current		Curr 10 Hi 2	Alta2 Corriente 10		Alta2 Corr10	
190	32			15			Current11 High 1 Current		Curr 11 Hi 1	Alta1 Corriente 11		Alta1 Corr11	
191	32			15			Current11 High 2 Current		Curr 11 Hi 2	Alta2 Corriente 11		Alta2 Corr11	
192	32			15			Current12 High 1 Current		Curr 12 Hi 1	Alta1 Corriente 12		Alta1 Corr12	
193	32			15			Current12 High 2 Current		Curr 12 Hi 2	Alta2 Corriente 12		Alta2 Corr12	
194	32			15			Current13 High 1 Current		Curr 13 Hi 1	Alta1 Corriente 13		Alta1 Corr13	
195	32			15			Current13 High 2 Current		Curr 13 Hi 2	Alta2 Corriente 13		Alta2 Corr13	
196	32			15			Current14 High 1 Current		Curr 14 Hi 1	Alta1 Corriente 14		Alta1 Corr14	
197	32			15			Current14 High 2 Current		Curr 14 Hi 2	Alta2 Corriente 14		Alta2 Corr14	
198	32			15			Current15 High 1 Current		Curr 15 Hi 1	Alta1 Corriente 15		Alta1 Corr15	
199	32			15			Current15 High 2 Current		Curr 15 Hi 2	Alta2 Corriente 15		Alta2 Corr15	
200	32			15			Current16 High 1 Current		Curr 16 Hi 1	Alta1 Corriente 16		Alta1 Corr16	
201	32			15			Current16 High 2 Current		Curr 16 Hi 2	Alta2 Corriente 16		Alta2 Corr16	
202	32			15			Current17 High 1 Current		Curr 17 Hi 1	Alta1 Corriente 17		Alta1 Corr17	
203	32			15			Current17 High 2 Current		Curr 17 Hi 2	Alta2 Corriente 17		Alta2 Corr17	
204	32			15			Current18 High 1 Current		Curr 18 Hi 1	Alta1 Corriente 18		Alta1 Corr18	
205	32			15			Current18 High 2 Current		Curr 18 Hi 2	Alta2 Corriente 18		Alta2 Corr18	
206	32			15			Current19 High 1 Current		Curr 19 Hi 1	Alta1 Corriente 19		Alta1 Corr19	
207	32			15			Current19 High 2 Current		Curr 19 Hi 2	Alta2 Corriente 19		Alta2 Corr19	
208	32			15			Current20 High 1 Current		Curr 20 Hi 1	Alta1 Corriente 20		Alta1 Corr20		
209	32			15			Current20 High 2 Current		Curr 20 Hi 2	Alta2 Corriente 20		Alta2 Corr20		
210	32			15			Current21 High 1 Current		Curr 21 Hi 1	Alta1 Corriente 21		Alta1 Corr21		
211	32			15			Current21 High 2 Current		Curr 21 Hi 2	Alta2 Corriente 21		Alta2 Corr21		
212	32			15			Current22 High 1 Current		Curr 22 Hi 1	Alta1 Corriente 22		Alta1 Corr22		
213	32			15			Current22 High 2 Current		Curr 22 Hi 2	Alta2 Corriente 22		Alta2 Corr22		
214	32			15			Current23 High 1 Current		Curr 23 Hi 1	Alta1 Corriente 23		Alta1 Corr23		
215	32			15			Current23 High 2 Current		Curr 23 Hi 2	Alta2 Corriente 23		Alta2 Corr23		
216	32			15			Current24 High 1 Current		Curr 24 Hi 1	Alta1 Corriente 24		Alta1 Corr24		
217	32			15			Current24 High 2 Current		Curr 24 Hi 2	Alta2 Corriente 24		Alta2 Corr24		
218	32			15			Current25 High 1 Current		Curr 25 Hi 1	Alta1 Corriente 25		Alta1 Corr25		
219	32			15			Current25 High 2 Current		Curr 25 Hi 2	Alta2 Corriente 25		Alta2 Corr25		
220	32			15			Current1 High 1 Current Limit		Curr1 Hi 1 Lmt		Alta1 corr.1 Límite de corr.		Alta1 Corr1Lmt	
221	32			15			Current1 High 2 Current Limit		Curr1 Hi 2 Lmt		Alta2 corr.1 Límite de corr.		Alta2 Corr1Lmt	
222	32			15			Current2 High 1 Current Limit		Curr2 Hi 1 Lmt		Alta1 corr.2 Límite de corr.		Alta1 Corr2Lmt	
223	32			15			Current2 High 2 Current Limit		Curr2 Hi 2 Lmt		Alta2 corr.2 Límite de corr.		Alta2 Corr2Lmt	
224	32			15			Current3 High 1 Current Limit		Curr3 Hi 1 Lmt		Alta1 corr.3 Límite de corr.		Alta1 Corr3Lmt	
225	32			15			Current3 High 2 Current Limit		Curr3 Hi 2 Lmt		Alta2 corr.3 Límite de corr.		Alta2 Corr3Lmt	
226	32			15			Current4 High 1 Current Limit		Curr4 Hi 1 Lmt		Alta1 corr.4 Límite de corr.		Alta1 Corr4Lmt	
227	32			15			Current4 High 2 Current Limit		Curr4 Hi 2 Lmt		Alta2 corr.4 Límite de corr.		Alta2 Corr4Lmt	
228	32			15			Current5 High 1 Current Limit		Curr5 Hi 1 Lmt		Alta1 corr.5 Límite de corr.		Alta1 Corr5Lmt	
229	32			15			Current5 High 2 Current Limit		Curr5 Hi 2 Lmt		Alta2 corr.5 Límite de corr.		Alta2 Corr5Lmt	
230	32			15			Current6 High 1 Current Limit		Curr6 Hi 1 Lmt		Alta1 corr.6 Límite de corr.		Alta1 Corr6Lmt	
231	32			15			Current6 High 2 Current Limit		Curr6 Hi 2 Lmt		Alta2 corr.6 Límite de corr.		Alta2 Corr6Lmt	
232	32			15			Current7 High 1 Current Limit		Curr7 Hi 1 Lmt		Alta1 corr.7 Límite de corr.		Alta1 Corr7Lmt	
233	32			15			Current7 High 2 Current Limit		Curr7 Hi 2 Lmt		Alta2 corr.7 Límite de corr.		Alta2 Corr7Lmt	
234	32			15			Current8 High 1 Current Limit		Curr8 Hi 1 Lmt		Alta1 corr.8 Límite de corr.		Alta1 Corr8Lmt	
235	32			15			Current8 High 2 Current Limit		Curr8 Hi 2 Lmt		Alta2 corr.8 Límite de corr.		Alta2 Corr8Lmt	
236	32			15			Current9 High 1 Current Limit		Curr9 Hi 1 Lmt		Alta1 corr.9 Límite de corr.		Alta1 Corr9Lmt	
237	32			15			Current9 High 2 Current Limit		Curr9 Hi 2 Lmt		Alta2 corr.9 Límite de corr.		Alta2 Corr9Lmt	
238	32			15			Current10 High 1 Current Limit		Curr10 Hi 1 Lmt		Alta1 corr.10 Límite de corr.		Alta1 Corr10Lmt	
239	32			15			Current10 High 2 Current Limit		Curr10 Hi 2 Lmt		Alta2 corr.10 Límite de corr.		Alta2 Corr10Lmt	
240	32			15			Current11 High 1 Current Limit		Curr11 Hi 1 Lmt		Alta1 corr.11 Límite de corr.		Alta1 Corr11Lmt		
241	32			15			Current11 High 2 Current Limit		Curr11 Hi 2 Lmt		Alta2 corr.11 Límite de corr.		Alta2 Corr11Lmt		
242	32			15			Current12 High 1 Current Limit		Curr12 Hi 1 Lmt		Alta1 corr.12 Límite de corr.		Alta1 Corr12Lmt		
243	32			15			Current12 High 2 Current Limit		Curr12 Hi 2 Lmt		Alta2 corr.12 Límite de corr.		Alta2 Corr12Lmt		
244	32			15			Current13 High 1 Current Limit		Curr13 Hi 1 Lmt		Alta1 corr.13 Límite de corr.		Alta1 Corr13Lmt		
245	32			15			Current13 High 2 Current Limit		Curr13 Hi 2 Lmt		Alta2 corr.13 Límite de corr.		Alta2 Corr13Lmt		
246	32			15			Current14 High 1 Current Limit		Curr14 Hi 1 Lmt		Alta1 corr.14 Límite de corr.		Alta1 Corr14Lmt		
247	32			15			Current14 High 2 Current Limit		Curr14 Hi 2 Lmt		Alta2 corr.14 Límite de corr.		Alta2 Corr14Lmt		
248	32			15			Current15 High 1 Current Limit		Curr15 Hi 1 Lmt		Alta1 corr.15 Límite de corr.		Alta1 Corr15Lmt		
249	32			15			Current15 High 2 Current Limit		Curr15 Hi 2 Lmt		Alta2 corr.15 Límite de corr.		Alta2 Corr15Lmt		
250	32			15			Current16 High 1 Current Limit		Curr16 Hi 1 Lmt		Alta1 corr.16 Límite de corr.		Alta1 Corr16Lmt		
251	32			15			Current16 High 2 Current Limit		Curr16 Hi 2 Lmt		Alta2 corr.16 Límite de corr.		Alta2 Corr16Lmt		
252	32			15			Current17 High 1 Current Limit		Curr17 Hi 1 Lmt		Alta1 corr.17 Límite de corr.		Alta1 Corr17Lmt		
253	32			15			Current17 High 2 Current Limit		Curr17 Hi 2 Lmt		Alta2 corr.17 Límite de corr.		Alta2 Corr17Lmt		
254	32			15			Current18 High 1 Current Limit		Curr18 Hi 1 Lmt		Alta1 corr.18 Límite de corr.		Alta1 Corr18Lmt		
255	32			15			Current18 High 2 Current Limit		Curr18 Hi 2 Lmt		Alta2 corr.18 Límite de corr.		Alta2 Corr18Lmt		
256	32			15			Current19 High 1 Current Limit		Curr19 Hi 1 Lmt		Alta1 corr.19 Límite de corr.		Alta1 Corr19Lmt		
257	32			15			Current19 High 2 Current Limit		Curr19 Hi 2 Lmt		Alta2 corr.19 Límite de corr.		Alta2 Corr19Lmt		
258	32			15			Current20 High 1 Current Limit		Curr20 Hi 1 Lmt		Alta1 corr.20 Límite de corr.		Alta1 Corr20Lmt		
259	32			15			Current20 High 2 Current Limit		Curr20 Hi 2 Lmt		Alta2 corr.20 Límite de corr.		Alta2 Corr20Lmt		
260	32			15			Current21 High 1 Current Limit		Curr21 Hi 1 Lmt		Alta1 corr.21 Límite de corr.		Alta1 Corr21Lmt		
261	32			15			Current21 High 2 Current Limit		Curr21 Hi 2 Lmt		Alta2 corr.21 Límite de corr.		Alta2 Corr21Lmt		
262	32			15			Current22 High 1 Current Limit		Curr22 Hi 1 Lmt		Alta1 corr.22 Límite de corr.		Alta1 Corr22Lmt		
263	32			15			Current22 High 2 Current Limit		Curr22 Hi 2 Lmt		Alta2 corr.22 Límite de corr.		Alta2 Corr22Lmt		
264	32			15			Current23 High 1 Current Limit		Curr23 Hi 1 Lmt		Alta1 corr.23 Límite de corr.		Alta1 Corr23Lmt		
265	32			15			Current23 High 2 Current Limit		Curr23 Hi 2 Lmt		Alta2 corr.23 Límite de corr.		Alta2 Corr23Lmt		
266	32			15			Current24 High 1 Current Limit		Curr24 Hi 1 Lmt		Alta1 corr.24 Límite de corr.		Alta1 Corr24Lmt		
267	32			15			Current24 High 2 Current Limit		Curr24 Hi 2 Lmt		Alta2 corr.24 Límite de corr.		Alta2 Corr24Lmt		
268	32			15			Current25 High 1 Current Limit		Curr25 Hi 1 Lmt		Alta1 corr.25 Límite de corr.		Alta1 Corr25Lmt		
269	32			15			Current25 High 2 Current Limit		Curr25 Hi 2 Lmt		Alta2 corr.25 Límite de corr.		Alta2 Corr25Lmt		
270	32			15			Current1 Break Value				Curr1 Brk Val	Talla disyuntor corriente 1	Talla Disy I1
271	32			15			Current2 Break Value				Curr2 Brk Val	Talla disyuntor corriente 2	Talla Disy I2
272	32			15			Current3 Break Value				Curr3 Brk Val	Talla disyuntor corriente 3	Talla Disy I3
273	32			15			Current4 Break Value				Curr4 Brk Val	Talla disyuntor corriente 4	Talla Disy I4
274	32			15			Current5 Break Value				Curr5 Brk Val	Talla disyuntor corriente 5	Talla Disy I5
275	32			15			Current6 Break Value				Curr6 Brk Val	Talla disyuntor corriente 6	Talla Disy I6
276	32			15			Current7 Break Value				Curr7 Brk Val	Talla disyuntor corriente 7	Talla Disy I7
277	32			15			Current8 Break Value				Curr8 Brk Val	Talla disyuntor corriente 8	Talla Disy I8
278	32			15			Current9 Break Value				Curr9 Brk Val	Talla disyuntor corriente 9	Talla Disy I9
279	32			15			Current10 Break Value				Curr10 Brk Val	Talla disyuntor corriente 10	Talla Disy I10
280	32			15			Current11 Break Value				Curr11 Brk Val	Talla disyuntor corriente 11	Talla Disy I11
281	32			15			Current12 Break Value				Curr12 Brk Val	Talla disyuntor corriente 12	Talla Disy I12
282	32			15			Current13 Break Value				Curr13 Brk Val	Talla disyuntor corriente 13	Talla Disy I13
283	32			15			Current14 Break Value				Curr14 Brk Val	Talla disyuntor corriente 14	Talla Disy I14
284	32			15			Current15 Break Value				Curr15 Brk Val	Talla disyuntor corriente 15	Talla Disy I15
285	32			15			Current16 Break Value				Curr16 Brk Val	Talla disyuntor corriente 16	Talla Disy I16
286	32			15			Current17 Break Value				Curr17 Brk Val	Talla disyuntor corriente 17	Talla Disy I17
287	32			15			Current18 Break Value				Curr18 Brk Val	Talla disyuntor corriente 18	Talla Disy I18
288	32			15			Current19 Break Value				Curr19 Brk Val	Talla disyuntor corriente 19	Talla Disy I19
289	32			15			Current20 Break Value				Curr20 Brk Val	Talla disyuntor corriente 20	Talla Disy I20
290	32			15			Current21 Break Value				Curr21 Brk Val	Talla disyuntor corriente 21	Talla Disy I21
291	32			15			Current22 Break Value				Curr22 Brk Val	Talla disyuntor corriente 22	Talla Disy I22
292	32			15			Current23 Break Value				Curr23 Brk Val	Talla disyuntor corriente 23	Talla Disy I23
293	32			15			Current24 Break Value				Curr24 Brk Val	Talla disyuntor corriente 24	Talla Disy I24
294	32			15			Current25 Break Value				Curr25 Brk Val	Talla disyuntor corriente 25	Talla Disy I25

295	32			15			Shunt 1 Voltage					Shunt1 Voltage			Tensión Shunt1		Tensión Shunt1
296	32			15			Shunt 1 Current					Shunt1 Current			Corriente Shunt 1	Corri Shunt 1
297	32			15			Shunt 2 Voltage					Shunt2 Voltage			Tensión Shunt2		Tensión Shunt2
298	32			15			Shunt 2 Current					Shunt2 Current			Corriente Shunt 2	Corri Shunt 2
299	32			15			Shunt 3 Voltage					Shunt3 Voltage			Tensión Shunt3		Tensión Shunt3
300	32			15			Shunt 3 Current					Shunt3 Current			Corriente Shunt 3	Corri Shunt 3
301	32			15			Shunt 4 Voltage					Shunt4 Voltage			Tensión Shunt4		Tensión Shunt4
302	32			15			Shunt 4 Current					Shunt4 Current			Corriente Shunt 4	Corri Shunt 4
303	32			15			Shunt 5 Voltage					Shunt5 Voltage			Tensión Shunt5		Tensión Shunt5
304	32			15			Shunt 5 Current					Shunt5 Current			Corriente Shunt 5	Corri Shunt 5
305	32			15			Shunt 6 Voltage					Shunt6 Voltage			Tensión Shunt6		Tensión Shunt6
306	32			15			Shunt 6 Current					Shunt6 Current			Corriente Shunt 6	Corri Shunt 6
307	32			15			Shunt 7 Voltage					Shunt7 Voltage			Tensión Shunt7		Tensión Shunt7
308	32			15			Shunt 7 Current					Shunt7 Current			Corriente Shunt 7	Corri Shunt 7
309	32			15			Shunt 8 Voltage					Shunt8 Voltage			Tensión Shunt8		Tensión Shunt8
310	32			15			Shunt 8 Current					Shunt8 Current			Corriente Shunt 8	Corri Shunt 8
311	32			15			Shunt 9 Voltage					Shunt9 Voltage			Tensión Shunt9		Tensión Shunt9
312	32			15			Shunt 9 Current					Shunt9 Current			Corriente Shunt 9	Corri Shunt 9
313	32			15			Shunt 10 Voltage				Shunt10 Voltage			Tensión Shunt10		Tensión Shunt10
314	32			15			Shunt 10 Current				Shunt10 Current			Corriente Shunt 10	Corri Shunt 10
315	32			15			Shunt 11 Voltage				Shunt11 Voltage			Tensión Shunt11		Tensión Shunt11
316	32			15			Shunt 11 Current				Shunt11 Current			Corriente Shunt 11	Corri Shunt 11
317	32			15			Shunt 12 Voltage				Shunt12 Voltage			Tensión Shunt12		Tensión Shunt12
318	32			15			Shunt 12 Current				Shunt12 Current			Corriente Shunt 12	Corri Shunt 12
319	32			15			Shunt 13 Voltage				Shunt13 Voltage			Tensión Shunt13		Tensión Shunt13
320	32			15			Shunt 13 Current				Shunt13 Current			Corriente Shunt 13	Corri Shunt 13
321	32			15			Shunt 14 Voltage				Shunt14 Voltage			Tensión Shunt14		Tensión Shunt14
322	32			15			Shunt 14 Current				Shunt14 Current			Corriente Shunt 14	Corri Shunt 14
323	32			15			Shunt 15 Voltage				Shunt15 Voltage			Tensión Shunt15		Tensión Shunt15
324	32			15			Shunt 15 Current				Shunt15 Current			Corriente Shunt 15	Corri Shunt 15
325	32			15			Shunt 16 Voltage				Shunt16 Voltage			Tensión Shunt16		Tensión Shunt16
326	32			15			Shunt 16 Current				Shunt16 Current			Corriente Shunt 16	Corri Shunt 16
327	32			15			Shunt 17 Voltage				Shunt17 Voltage			Tensión Shunt17		Tensión Shunt17
328	32			15			Shunt 17 Current				Shunt17 Current			Corriente Shunt 17	Corri Shunt 17
329	32			15			Shunt 18 Voltage				Shunt18 Voltage			Tensión Shunt18		Tensión Shunt18
330	32			15			Shunt 18 Current				Shunt18 Current			Corriente Shunt 18	Corri Shunt 18
331	32			15			Shunt 19 Voltage				Shunt19 Voltage			Tensión Shunt19		Tensión Shunt19
332	32			15			Shunt 19 Current				Shunt19 Current			Corriente Shunt 19	Corri Shunt 19
333	32			15			Shunt 20 Voltage				Shunt20 Voltage			Tensión Shunt20		Tensión Shunt20
334	32			15			Shunt 20 Current				Shunt20 Current			Corriente Shunt 20	Corri Shunt 20
335	32			15			Shunt 21 Voltage				Shunt21 Voltage			Tensión Shunt21		Tensión Shunt21
336	32			15			Shunt 21 Current				Shunt21 Current			Corriente Shunt 21	Corri Shunt 21
337	32			15			Shunt 22 Voltage				Shunt22 Voltage			Tensión Shunt22		Tensión Shunt22
338	32			15			Shunt 22 Current				Shunt22 Current			Corriente Shunt 22	Corri Shunt 22
339	32			15			Shunt 23 Voltage				Shunt23 Voltage			Tensión Shunt23		Tensión Shunt23
340	32			15			Shunt 23 Current				Shunt23 Current			Corriente Shunt 23	Corri Shunt 23
341	32			15			Shunt 24 Voltage				Shunt24 Voltage			Tensión Shunt24		Tensión Shunt24
342	32			15			Shunt 24 Current				Shunt24 Current			Corriente Shunt 24	Corri Shunt 24
343	32			15			Shunt 25 Voltage				Shunt25 Voltage			Tensión Shunt25		Tensión Shunt25
344	32			15			Shunt 25 Current				Shunt25 Current			Corriente Shunt 25	Corri Shunt 25

345	32			15			Shunt Size Settable				Shunt Settable			Shunt Size Settable				Shunt Settable
346	32			15			By Software					By Software			Software				Software																														
347	32			15			By Dip-Switch					By Dip-Switch			Microint				Microint																														
348	32			15			Not Supported					Not Supported			Ninguno					Ninguno
349	32			15			Shunt Coefficient Conflict			Shunt Conflict			Shunt Coefficient Conflict			Shunt Conflict																														
350	32			15			Disabled					Disabled			Deshabilitado			Deshabilitado				
351	32			15			Enable						Enable				Habilitado			Habilitado				
352	32			15			Shunt 1						Shunt 1					Shunt 1						Shunt 1	
353	32			15			Shunt 2						Shunt 2					Shunt 2						Shunt 2	
354	32			15			Shunt 3						Shunt 3					Shunt 3						Shunt 3	
355	32			15			Shunt 4						Shunt 4					Shunt 4						Shunt 4	
356	32			15			Shunt 5						Shunt 5					Shunt 5						Shunt 5	
357	32			15			Shunt 6						Shunt 6					Shunt 6						Shunt 6	
358	32			15			Shunt 7						Shunt 7					Shunt 7						Shunt 7	
359	32			15			Shunt 8						Shunt 8					Shunt 8						Shunt 8	
360	32			15			Shunt 9						Shunt 9					Shunt 9						Shunt 9	
361	32			15			Shunt 10					Shunt 10				Shunt 10					Shunt 10
362	32			15			Shunt 11					Shunt 11				Shunt 11					Shunt 11
363	32			15			Shunt 12					Shunt 12				Shunt 12					Shunt 12
364	32			15			Shunt 13					Shunt 13				Shunt 13					Shunt 13
365	32			15			Shunt 14					Shunt 14				Shunt 14					Shunt 14
366	32			15			Shunt 15					Shunt 15				Shunt 15					Shunt 15
367	32			15			Shunt 16					Shunt 16				Shunt 16					Shunt 16
368	32			15			Shunt 17					Shunt 17				Shunt 17					Shunt 17
369	32			15			Shunt 18					Shunt 18				Shunt 18					Shunt 18
370	32			15			Shunt 19					Shunt 19				Shunt 19					Shunt 19
371	32			15			Shunt 20					Shunt 20				Shunt 20					Shunt 20
372	32			15			Shunt 21					Shunt 21				Shunt 21					Shunt 21
373	32			15			Shunt 22					Shunt 22				Shunt 22					Shunt 22
374	32			15			Shunt 23					Shunt 23				Shunt 23					Shunt 23
375	32			15			Shunt 24					Shunt 24				Shunt 24					Shunt 24
376	32			15			Shunt 25					Shunt 25				Shunt 25					Shunt 25
377	32			15			Not Used				Not Used			No usado				No usado
378	32			15			General					General				General				General
379	32			15			Load					Load				Carga				Carga
380	32			15			Load 1					Load 1		Carga 1			Carga 1
381	32			15			Load 2					Load 2		Carga 2			Carga 2
382	32			15			Load 3					Load 3		Carga 3			Carga 3
383	32			15			Load 4					Load 4		Carga 4			Carga 4
384	32			15			Load 5				Load 5		Carga 5			Carga 5
385	32			15			Load 6				Load 6		Carga 6			Carga 6
386	32			15			Load 7				Load 7		Carga 7			Carga 7
387	32			15			Load 8				Load 8		Carga 8			Carga 8
388	32			15			Load 9				Load 9		Carga 9			Carga 9
389	32			15			Load 10			Load 10		Carga 10			Carga 10
390	32			15			Load 11			Load 11		Carga 11			Carga 11
391	32			15			Load 12			Load 12		Carga 12			Carga 12
392	32			15			Load 13			Load 13		Carga 13			Carga 13
393	32			15			Load 14			Load 14		Carga 14			Carga 14
394	32			15			Load 15			Load 15		Carga 15			Carga 15
395	32			15			Load 16			Load 16		Carga 16			Carga 16
396	32			15			Load 17			Load 17		Carga 17			Carga 17
397	32			15			Load 18			Load 18		Carga 18			Carga 18
398	32			15			Load 19			Load 19		Carga 19			Carga 19
399	32			15			Load 20			Load 20		Carga 20			Carga 20
400	32			15			Load 21			Load 21		Carga 21			Carga 21
401	32			15			Load 22			Load 22		Carga 22			Carga 22
402	32			15			Load 23			Load 23		Carga 23			Carga 23
403	32			15			Load 24			Load 24		Carga 24			Carga 24
404	32			15			Load 25			Load 25		Carga 25			Carga 25
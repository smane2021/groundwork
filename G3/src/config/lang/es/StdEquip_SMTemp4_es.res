#
#  Locale language support:Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			SMTemp4 Temperature 1			SMTemp4 Temp1		SMTemp4 Temperature 1			SMTemp4 Temp1
2	32			15			SMTemp4 Temperature 2			SMTemp4 Temp2		SMTemp4 Temperature 2			SMTemp4 Temp2
3	32			15			SMTemp4 Temperature 3			SMTemp4 Temp3		SMTemp4 Temperature 3			SMTemp4 Temp3
4	32			15			SMTemp4 Temperature 4			SMTemp4 Temp4		SMTemp4 Temperature 4			SMTemp4 Temp4
5	32			15			SMTemp4 Temperature 5			SMTemp4 Temp5		SMTemp4 Temperature 5			SMTemp4 Temp5	
6	32			15			SMTemp4 Temperature 6			SMTemp4 Temp6		SMTemp4 Temperature 6			SMTemp4 Temp6
7	32			15			SMTemp4 Temperature 7			SMTemp4 Temp7		SMTemp4 Temperature 7			SMTemp4 Temp7	
8	32			15			SMTemp4 Temperature 8			SMTemp4 Temp8		SMTemp4 Temperature 8			SMTemp4 Temp8
9	32			15			Temperature Probe 1 Status		Probe1 Status		Temperature Probe 1 Status		Probe1 Status
10	32			15			Temperature Probe 2 Status		Probe2 Status		Temperature Probe 2 Status		Probe2 Status
11	32			15			Temperature Probe 3 Status		Probe3 Status		Temperature Probe 3 Status		Probe3 Status
12	32			15			Temperature Probe 4 Status		Probe4 Status		Temperature Probe 4 Status		Probe4 Status	
13	32			15			Temperature Probe 5 Status		Probe5 Status		Temperature Probe 5 Status		Probe5 Status
14	32			15			Temperature Probe 6 Status		Probe6 Status		Temperature Probe 6 Status		Probe6 Status
15	32			15			Temperature Probe 7 Status		Probe7 Status		Temperature Probe 7 Status		Probe7 Status
16	32			15			Temperature Probe 8 Status		Probe8 Status		Temperature Probe 8 Status		Probe8 Status	
17	32			15			Normal					Normal			Normal					Normal	
18	32			15			Shorted					Shorted			Shorted					Shorted	
19	32			15			Open					Open			Open					Open	
20	32			15			Not Installed				Not Installed		Not Installed				Not Installed
21	32			15			Module ID Overlap			ID Overlap		Module ID Overlap			ID Overlap
22	32			15			AD Converter Failure			AD Conv Fail		AD Converter Failure			AD Conv Fail
23	32			15			SMTemp EEPROM Failure			EEPROM Fail		SMTemp EEPROM Failure			EEPROM Fail

24	32			15			Communication Fail			Comm Fail		Communication Fail			Comm Fail
25	32			15			Existence State				Exist State		Existence State				Exist State
26	32			15			Communication Fail			Comm Fail		Communication Fail			Comm Fail

27	32			15			Temperature Probe 1 Shorted		Probe1 Short		Temperature Probe 1 Shorted		Probe1 Short
28	32			15			Temperature Probe 2 Shorted		Probe2 Short		Temperature Probe 2 Shorted		Probe2 Short
29	32			15			Temperature Probe 3 Shorted		Probe3 Short		Temperature Probe 3 Shorted		Probe3 Short
30	32			15			Temperature Probe 4 Shorted		Probe4 Short		Temperature Probe 4 Shorted		Probe4 Short	
31	32			15			Temperature Probe 5 Shorted		Probe5 Short		Temperature Probe 5 Shorted		Probe5 Short	
32	32			15			Temperature Probe 6 Shorted		Probe6 Short		Temperature Probe 6 Shorted		Probe6 Short	
33	32			15			Temperature Probe 7 Shorted		Probe7 Short		Temperature Probe 7 Shorted		Probe7 Short
34	32			15			Temperature Probe 8 Shorted		Probe8 Short		Temperature Probe 8 Shorted		Probe8 Short	
35	32			15			Temperature Probe 1 Open		Probe1 Open		Temperature Probe 1 Open		Probe1 Open
36	32			15			Temperature Probe 2 Open		Probe2 Open		Temperature Probe 2 Open		Probe2 Open
37	32			15			Temperature Probe 3 Open		Probe3 Open		Temperature Probe 3 Open		Probe3 Open
38	32			15			Temperature Probe 4 Open		Probe4 Open		Temperature Probe 4 Open		Probe4 Open
39	32			15			Temperature Probe 5 Open		Probe5 Open		Temperature Probe 5 Open		Probe5 Open
40	32			15			Temperature Probe 6 Open		Probe6 Open		Temperature Probe 6 Open		Probe6 Open
41	32			15			Temperature Probe 7 Open		Probe7 Open		Temperature Probe 7 Open		Probe7 Open
42	32			15			Temperature Probe 8 Open		Probe8 Open		Temperature Probe 8 Open		Probe8 Open	
43	32			15			SMTemp 4				SMTemp 4		SMTemp 4		SMTemp 4
44	32			15			Abnormal				Abnormal		Abnormal				Abnormal
45	32			15			Clear					Clear			Clear					Clear	
46	32			15			Clear Probe Alarm			Clr Probe Alm		Clear Probe Alarm			Clr Probe Alm

51	32			15			Temperature 1 Assign Equipment		T1 Assign Equip		Temperature 1 Assign Equipment		T1 Assign Equip	
54	32			15			Temperature 2 Assign Equipment		T2 Assign Equip		Temperature 2 Assign Equipment		T2 Assign Equip	
57	32			15			Temperature 3 Assign Equipment		T3 Assign Equip		Temperature 3 Assign Equipment		T3 Assign Equip	
60	32			15			Temperature 4 Assign Equipment		T4 Assign Equip		Temperature 4 Assign Equipment		T4 Assign Equip	
63	32			15			Temperature 5 Assign Equipment		T5 Assign Equip		Temperature 5 Assign Equipment		T5 Assign Equip	
66	32			15			Temperature 6 Assign Equipment		T6 Assign Equip		Temperature 6 Assign Equipment		T6 Assign Equip	
69	32			15			Temperature 7 Assign Equipment		T7 Assign Equip		Temperature 7 Assign Equipment		T7 Assign Equip	
72	32			15			Temperature 8 Assign Equipment		T8 Assign Equip		Temperature 8 Assign Equipment		T8 Assign Equip	
150	32			15			None					None				None					None	
151	32			15			Ambient					Ambient			Ambient					Ambient	
152	32			15			Battery					Battery			Battery					Battery
﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE			
1		32			15			Analog Input 1				Analog Input 1				Entrada Analógica 1			Entrada analog1
2		32			15			Analog Input 2				Analog Input 2				Entrada Analógica 2			Entrada analog2
3		32			15			Analog Input 3				Analog Input 3				Entrada Analógica 3			Entrada analog3
4		32			15			Analog Input 4				Analog Input 4				Entrada Analógica 4			Entrada analog4
5		32			15			Analog Input 5				Analog Input 5				Entrada Analógica 5			Entrada analog5
6		32			15			Frequency Input			Frequency Input				Entrada de Frecuencia			Ent frecuencia
7		32			15			Access Switch Closed		Access Sw Close			Puerta cerrada y bloqueada		Puerta cerrada
8		32			15			Main Switch Open			Switch open		Disyuntor principal abierto		Disyun abierto
9		32			15			Automatic Operation			Auto Operation		Operación automática			Operacion auto
10		32			15			Differential Relay Tripped		Relay Tripped		Relé diferencial disparado		Relé disparado
11		32			15			Digital Input 5			Digital Input 5			Entrada Digital 5			Entrada Digit5
12		32			15			Digital Input 6			Digital Input 6			Entrada Digital 6			Entrada Digit6
13		32			15			Digital Input 7			Digital Input 7			Entrada Digital 7			Entrada Digit7
14		32			15			Relay 1 Status			Relay 1 Status		Estado Relé 1				Estado Relé1
15		32			15			Relay 2 Status			Relay 2 Status		Estado Relé 2				Estado Relé2
16		32			15			Relay 3 Status			Relay 3 Status		Estado Relé 3				Estado Relé3
17		32			15			Close Main Switch			Close Mn Switch		Cerrar disyuntor principal		Cerrar disyunt
18		32			15			Open Main Switch			Open Mn Switch		Abrir disyuntor principal		Abrir disyunt
19		32			15			Reset Differential Protection		Rst DiffProtect			Iniciar proteción diferencial		Iniciar protec
23		32			15			High Analog Input 1 Limit		Hi AI 1 Limit				Lim Alta Entrada analógica 1		Lim Alta AI1
24		32			15			Low Analog Input 1 Limit		Low AI 1 Limit					Lim Baja Entrada analógica 1		Lim Baja AI1
25		32			15			High Analog Input 2 Limit		Hi AI 2 Limit				Lim Alta Entrada analógica 2		Lim Alta AI2
26		32			15			Low Analog Input 2 Limit		Low AI 2 Limit					Lim Baja Entrada analógica 2		Lim Baja AI2
27		32			15			High Analog Input 3 Limit		Hi AI 3 Limit				Lim Alta Entrada analógica 3		Lim Alta AI3
28		32			15			Low Analog Input 3 Limit		Low AI 3 Limit					Lim Baja Entrada analógica 3		Lim Baja AI3
29		32			15			High Analog Input 4 Limit		Hi AI 4 Limit				Lim Alta Entrada analógica 4		Lim Alta AI4
30		32			15			Low Analog Input 4 Limit		Low AI 4 Limit					Lim Baja Entrada analógica 4		Lim Baja AI4
31		32			15			High Analog Input 5 Limit		Hi AI 5 Limit				Lim Alta Entrada analógica 5		Lim Alta AI5
32		32			15			Low Analog Input 5 Limit		Low AI 5 Limit					Lim Baja Entrada analógica 5		Lim Baja AI5
33		32			15			High Frequency Limit		High Freq Limit			Límite Alta frecuencia			Lim alta frec
34		32			15			Low Frequency Limit		Low Freq Limit			Límite Baja frecuencia			Lim baja frec
35		32			15			High Analog Input 1 Alarm		Hi AI 1 Alarm				Alarma Alta entrada analógica 1		Alarma alta AI1
36		32			15			Low Analog Input 1 Alarm		Low AI 1 Alarm					Alarma Baja entrada analógica 1		Alarma baja AI1
37		32			15			High Analog Input 2 Alarm		Hi AI 2 Alarm				Alarma Alta entrada analógica 2		Alarma alta AI2
38		32			15			Low Analog Input 2 Alarm		LowAI 2 Alarm					Alarma Baja entrada analógica 2		Alarma baja AI2
39		32			15			High Analog Input 3 Alarm		Hi AI 3 Alarm				Alarma Alta entrada analógica 3		Alarma alta AI3
40		32			15			Low Analog Input 3 Alarm		Low AI 3 Alarm					Alarma Baja entrada analógica 3		Alarma baja AI3
41		32			15			High Analog Input 4 Alarm		Hi AI 4 Alarm				Alarma Alta entrada analógica 4		Alarma alta AI4
42		32			15			Low Analog Input 4 Alarm		Low AI 4 Alarm					Alarma Baja entrada analógica 4		Alarma baja AI4
43		32			15			High Analog Input 5 Alarm		Hi AI 5 Alarm		Alarma Alta entrada analógica 5		Alarma alta AI5
44		32			15			Low Analog Input 5 Alarm		Low AI 5 Alarm			Alarma Baja entrada analógica 5		Alarma baja AI5
45		32			15			High Frequency Input Alarm		Hi-Freq In Alm		Alarma entrada alta frecuencia		Alarma alt frec
46		32			15			Low Frequency Input Alarm		Low-Freq In Alm			Alarma entrada baja frecuencia		Alarma bja frec
47		32			15			Inactive				Inactive	Desactivar				Desactivar
48		32			15			Activate				Activate	Activar					Activar
49		32			15			Inactive				Inactive	Desactivar				Desactivar
50		32			15		Activate				Activate		Activar					Activar
51		32			15			Inactive				Inactive	Desactivar				Desactivar
52		32			15			Activate				Activate		Activar					Activar
53		32			15			Inactive				Inactive		Desactivar				Desactivar
54		32			15			Activate				Activate		Activar					Activar
55		32			15			Off					Off			Desconectar				Desconectar
56		32			15			On					On			Conectar				Conectar
57		32			15			Off					Off			Desconectar				Desconectar
58		32			15			On					On			Conectar				Conectar
59		32			15			Off					Off			Desconectar				Desconectar
60		32			15			On					On			Conectar				Conectar
61		32			15			Off					Off			Desconectar				Desconectar
62		32			15			On					On			Conectar				Conectar
63		32			15			Off					Off			Desconectar				Desconectar
64		32			15			On					On			Conectar				Conectar
65		32			15			Off					Off			Desconectar				Desconectar
66		32			15			On					On			Conectar				Conectar
67		32			15			Inactive				Inactive	Desactivar				Desactivar
68		32			15			Activate				Activate	Activar					Activar
69		32			15			Inactive				Inactive	Desactivar				Desactivar
70		32			15			Activate				Activate	Activar					Activar
71		32			15			Inactive				Inactive	Desactivar				Desactivar
72		32			15			Activate				Activate	Activar					Activar
73		32			15			SMIO Generic Unit 1			SMIO 1		Unidad Genérica SMIO 1			SMIO 1
74		32			15			SMIO Failure				SMIO Fail		Fallo SMIO				Fallo SMIO
75		32			15			SMIO Failure				SMIO Fail		Fallo SMIO				Fallo SMIO
76		32			15			No					No			No					No
77		32			15			Yes					Yes			Sí					Sí

﻿#
#  Locale language support:Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			BMS Group			BMS Group		Grupo BMS			Grupo BMS
2		32			15			Number of BMS		NumOfBMS		Número de BMS	Número de BMS
3		32			15			Communication Fail		Comm Fail		Fallo Comunicación		Fallo COM
4		32			15			Existence State			Existence State		Detección		Detección
5		32			15			Existent			Existent		Existente			Existente
6		32			15			Not Existent			Not Existent	No Existente	No Existente
7		32			15			BMS Existence State	BMSState		Detección de BMS		Detección BMS
8		32			15			Charge Current			Charge Current		Corriente de Carga		Corri.Carga


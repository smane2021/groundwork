﻿#
#  Locale language support:spanish
#
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE	
1		32			16			OBFuel Tank				OBFuel Tank		OB Depósito Combustible		OB Dep.Combus
2		32			15			Remaining Fuel Height			Remained Height		Altura Fuel Restante	Altura Rest
3		32			15			Remaining Fuel Volume			Remained Volume		Volumen Fuel Restante		Volumen Rest
4		32			15			Remaining Fuel Percent			RemainedPercent		Fuel Restante		Fuel Restante
5		32			15			Fuel Theft Alarm			Fuel Theft Alm		Alarma Robo Combustible		Alm Robo Combus
6		32			15			No					No			No			No
7		32			15			Yes					Yes			Sí			Sí
8		32			15			Multi-Shape Height Error Status		Multi Hgt Err		Error Altura	Error Altura
9		32			15			Setting Configuration Done		Set Config Done		Configuración realizada		Config Hecha
10		32			15			Reset Theft Alarm			Reset Theft Alm		Iniciar Alarma Robo		Res Alarma Robo
11		32			15			Fuel Tank Type				Fuel Tank Type		Configuración realizada		Config Hecha
12		32			15			Square Tank Length			Square Tank L		Largo Tanque Cuadrado		Largo Dep Cuadr
13		32			15			Square Tank Width			Square Tank W		Ancho Tanque Cuadrado		Ancho Dep Cuadr
14		32			15			Square Tank Height			Square Tank H		Alto Tanque Cuadrado		Alto Dep Cuadr
15		32			15			Vertical Cylinder Tank Diameter	Vertical Tank D		Diámetro Cilindro Vertical	Diám CilindroV
16		32			15			Vertical Cylinder Tank Height		Vertical Tank H		Altura Cilindro Vertical		Alto CilindroV
17		32			15			Horiz Cylinder Tank Diameter		Horiz Tank D		Diámetro Cilindro Horizontal	Diám CilindroH
18		32			15			Horiz Cylinder Tank Length		Horiz Tank L		Largo Cilindro Horizontal		Largo CilindroH
#for multi sharp 20 calibration points
20		32			15			Number of Calibration Points		Num Cal Points		Núm Puntos Calibración		N Ptos Calibra
21		32			15			Height 1 of Calibration Point		Height 1 Point		Alto Punto Calibrado 1		Alto Punto1
22		32			15			Volume 1 of Calibration Point		Volume 1 Point		Volumen Punto Calibrado 1		Volumen Pto1
23		32			15			Height 2 of Calibration Point		Height 2 Point		Alto Punto Calibrado 2		Alto Punto2
24		32			15			Volume 2 of Calibration Point		Volume 2 Point		Volumen Punto Calibrado 2	Volumen Pto2
25		32			15			Height 3 of Calibration Point		Height 3 Point		Alto Punto Calibrado 3		Alto Punto3
26		32			15			Volume 3 of Calibration Point		Volume 3 Point		Volumen Punto Calibrado 3		Volumen Pto3
27		32			15			Height 4 of Calibration Point		Height 4 Point		Alto Punto Calibrado 4		Alto Punto4
28		32			15			Volume 4 of Calibration Point		Volume 4 Point		Volumen Punto Calibrado 4		Volumen Pto4
29		32			15			Height 5 of Calibration Point		Height 5 Point		Alto Punto Calibrado 5		Alto Punto5
30		32			15			Volume 5 of Calibration Point		Volume 5 Point		Volumen Punto Calibrado 5		Volumen Pto5
31		32			15			Height 6 of Calibration Point		Height 6 Point		Alto Punto Calibrado 6		Alto Punto6
32		32			15			Volume 6 of Calibration Point		Volume 6 Point		Volumen Punto Calibrado 6		Volumen Pto6
33		32			15			Height 7 of Calibration Point		Height 7 Point		Alto Punto Calibrado 7		Alto Punto7
34		32			15			Volume 7 of Calibration Point		Volume 7 Point		Volumen Punto Calibrado 7		Volumen Pto7
35		32			15			Height 8 of Calibration Point		Height 8 Point		Alto Punto Calibrado 8	Alto Punto8
36		32			15			Volume 8 of Calibration Point		Volume 8 Point		Volumen Punto Calibrado 8		Volumen Pto8
37		32			15			Height 9 of Calibration Point		Height 9 Point		Alto Punto Calibrado 9		Alto Punto9
38		32			15			Volume 9 of Calibration Point		Volume 9 Point		Volumen Punto Calibrado 9		Volumen Pto9
39		32			15			Height 10 of Calibration Point		Height 10 Point		Alto Punto Calibrado 10	Alto Punto10
40		32			15			Volume 10 of Calibration Point		Volume 10 Point		Volumen Punto Calibrado 10	Volumen Pto10
41		32			15			Height 11 of Calibration Point		Height 11 Point		Alto Punto Calibrado 11	Alto Punto11
42		32			15			Volume 11 of Calibration Point		Volume 11 Point		Volumen Punto Calibrado 11	Volumen Pto11
43		32			15			Height 12 of Calibration Point		Height 12 Point		Alto Punto Calibrado 12	Alto Punto12
44		32			15			Volume 12 of Calibration Point		Volume 12 Point		Volumen Punto Calibrado 12	Volumen Pto12
45		32			15			Height 13 of Calibration Point		Height 13 Point		Alto Punto Calibrado 13	Alto Punto13
46		32			15			Volume 13 of Calibration Point		Volume 13 Point		Volumen Punto Calibrado 13	Volumen Pto13
47		32			15			Height 14 of Calibration Point		Height 14 Point		Alto Punto Calibrado 14	Alto Punto14
48		32			15			Volume 14 of Calibration Point		Volume 14 Point		Volumen Punto Calibrado 14	Volumen Pto14
49		32			15			Height 15 of Calibration Point		Height 15 Point		Alto Punto Calibrado 15		Alto Punto15  
50		32			15			Volume 15 of Calibration Point		Volume 15 Point		Volumen Punto Calibrado 15	Volumen Pto15
51		32			15			Height 16 of Calibration Point		Height 16 Point		Alto Punto Calibrado 16	Alto Punto16
52		32			15			Volume 16 of Calibration Point		Volume 16 Point		Volumen Punto Calibrado 16	Volumen Pto16
53		32			15			Height 17 of Calibration Point		Height 17 Point		Alto Punto Calibrado 17	Alto Punto17
54		32			15			Volume 17 of Calibration Point		Volume 17 Point		Volumen Punto Calibrado 17	Volumen Pto17
55		32			15			Height 18 of Calibration Point		Height 18 Point		Alto Punto Calibrado 18	Alto Punto18
56		32			15			Volume 18 of Calibration Point		Volume 18 Point		Volumen Punto Calibrado 18	Volumen Pto18
57		32			15			Height 19 of Calibration Point		Height 19 Point		Alto Punto Calibrado 19	Alto Punto19
58		32			15			Volume 19 of Calibration Point		Volume 19 Point		Volumen Punto Calibrado 19	Volumen Pto19
59		32			15			Height 20 of Calibration Point		Height 20 Point		Alto Punto Calibrado 20	Alto Punto20
60		32			15			Volume 20 of Calibration Point		Volume 20 Point		Volumen Punto Calibrado 20	Volumen Pto20
#61		32			15			None					None			Ninguno			Ninguno
62		32			15			Square Tank				Square Tank		Tanque Cuadrado		Tanque Cuadrado
63		32			15			Vertical Cylinder Tank			Vert Cyl Tank		Cilindro Vertical		CilindroV
64		32			15			Horizontal Cylinder Tank		Horiz Cyl Tank		Cilindro Horizontal		CilindroH
65		32			15			Multi Shape Tank			MultiShapeTank		Depósito Multiforma		Multiforma
66		32			15			Low Fuel Level Limit			Low Level Limit		Bajo Nivel Combustible	Bajo Nivel Fuel
67		32			15			High Fuel Level Limit			Hi Level Limit		Alto Nivel Combustible	Alto Nivel Fuel
68		32			15			Maximum Consumption Speed		Max Flow Speed		Consumo Máximo		Consumo Max
#for alarm of the volume of remained fuel	
71		32			15			High Fuel Level Alarm			Hi Level Alarm		Alto Nivel Combustible		Alto Nivel Fuel
72		32			15			Low Fuel Level Alarm			Low Level Alarm		Bajo Nivel Combustible		Bajo Nivel Fuel
73		32			15			Fuel Theft Alarm			Fuel Theft Alm		Alarma Robo Combustible		Alarm Robo Fuel
74		32			15			Square Tank Height Error		Sq Tank Hgt Err		Error Altura Dep Cuadrado		Error Altura
75		32			15			Vert Cylinder Tank Height Error		Vt Tank Hgt Err		Error Altura Cilindro Vertical	Error Altura
76		32			15			Horiz Cylinder Tank Height Error	Hr Tank Hgt Err		Error Altura Cilindro Horizontal	Error Altura
77		32			15			Tank Height Error			Tank Height Err		Error Altura Depósito	Error Altura
78		32			15			Fuel Tank Config Error			Fuel Config Err		Error Config Depósito Fuel	Err Cfg Fuel
80		32			15			Fuel Tank Config Error Status		Config Err		Error Config Depósito Fuel	Err Cfg Fuel
81		32			15			X1		X1		X1	X1
82		32			15			Y1		Y1		Y1	Y1
83		32			15			X2		X2		X2	X2
84		32			15			Y2		Y2		Y2	Y2
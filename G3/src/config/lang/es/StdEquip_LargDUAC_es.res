﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
es

[RES_INFO]
#ResId	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Manual					Manual			Manual				Manual
2	32			15			Auto					Auto			Auto				Auto
3	32			15			Off					Off			No				No
4	32			15			On					On			Sí				Sí
5	32			15			No Input				No Input		Sin entrada			Sin entrada
6	32			15			Input 1					Input 1			Entrada 1			Entrada 1
7	32			15			Input 2					Input 2			Entrada 2			Entrada 2
8	32			15			Input 3					Input 3			Entrada 3			Entrada 3
9	32			15			No Input				No Input		Sin entrada			Sin entrada
10	32			15			Input					Input			Entrada				Entrada
11	32			15			Closed					Closed			Cerrado				Cerrado
12	32			15			Open					Open			Abierto				Abierto
13	32			15			Closed					Closed			Cerrado				Cerrado
14	32			15			Open					Open			Abierto				Abierto
15	32			15			Closed					Closed			Cerrado				Cerrado
16	32			15			Open					Open			Abierto				Abierto
17	32			15			Closed					Closed			Cerrado				Cerrado
18	32			15			Open					Open			Abierto				Abierto
19	32			15			Closed					Closed			Cerrado				Cerrado
20	32			15			Open					Open			Abierto				Abierto
21	32			15			Closed					Closed			Cerrado				Cerrado
22	32			15			Open					Open			Abierto				Abierto
23	32			15			Closed					Closed			Cerrado				Cerrado
24	32			15			Open					Open			Abierto				Abierto
25	32			15			Closed					Closed			Cerrado				Cerrado
26	32			15			Open					Open			Abierto				Abierto
27	32			15			1-Phase					1-Phase			Monofásico			Monofásico
28	32			15			3-Phase					3-Phase			Trifásico			Trifásico
29	32			15			No Measurement				No Measurement		Ninguno				Ninguno
30	32			15			1-Phase					1-Phase			Monofásico			Monofásico
31	32			15			3-Phase					3-Phase			Trifásico			Trifásico
32	32			15			Communication OK		Comm OK			Comunicación OK		Comunicación OK
33	32			15			Communication Fail		Comm Fail		Fallo de Comunicación		Fallo de Com
34	32			15			AC Distribution			AC Distribution		Distribución CA			Distribución CA
35	32			15			Mains 1 Uab/Ua			Mains 1 Uab/Ua		Red 1 Vrs/Vr			Red 1 Vrs/Vr
36	32			15			Mains 1 Ubc/Ub			Mains 1 Ubc/Ub		Red 1 Vst/Vs			Red 1 Vst/Vs
37	32			15			Mains 1 Uca/Uc			Mains 1 Uca/Uc		Red 1 Vrt/Vt			Red 1 Vrt/Vt
38	32			15			Mains 2 Uab/Ua			Mains 2 Uab/Ua		Red 2 Vrs/Vr			Red 2 Vrs/Vr
39	32			15			Mains 2 Ubc/Ub			Mains 2 Ubc/Ub		Red 2 Vst/Vs			Red 2 Vst/Vs
40	32			15			Mains 2 Uca/Uc			Mains 2 Uca/Uc		Red 2 Vrt/Vt			Red 2 Vrt/Vt
41	32			15			Mains 3 Uab/Ua			Mains 3 Uab/Ua		Red 3 Vrs/Vr			Red 3 Vrs/Vr
42	32			15			Mains 3 Ubc/Ub			Mains 3 Ubc/Ub		Red 3 Vst/Vs			Red 3 Vst/Vs
43	32			15			Mains 3 Uca/Uc			Mains 3 Uca/Uc		Red 3 Vrt/Vt			Red 3 Vrt/Vt
53	32			15			Working Phase A Current			Phase A Curr		Corriente Fase R		Corr Fase R
54	32			15			Working Phase B Current			Phase B Curr		Corriente Fase S		Corr Fase S
55	32			15			Working Phase C Current			Phase C Curr		Corriente Fase T		Corr Fase T
56	32			15			AC Input Frequency			AC Input Freq		Frecuencia Entrada CA		Frec Entr CA
57	32			15			AC Input Switch Mode			AC Switch Mode		Modo conmutador Entrada CA	Modo Entr CA
58	32			15			Fault Lighting Status			Fault Lighting		Fallo iluminación		Fallo ilumin
59	32			15			Mains 1 Input Status			1 Input Status		Estado Entrada Red 1		Estado Ent Red1
60	32			15			Mains 2 Input Status			2 Input Status		Estado Entrada Red 2		Estado Ent Red2
61	32			15			Mains 3 Input Status			3 Input Status		Estado Entrada Red 3		Estado Ent Red3
62	32			15			AC Output 1 Status			Output 1 Status		Estado Salida CA 1		Estado Sal CA 1
63	32			15			AC Output 2 Status			Output 2 Status		Estado Salida CA 2		Estado Sal CA 2
64	32			15			AC Output 3 Status			Output 3 Status		Estado Salida CA 3		Estado Sal CA 3
65	32			15			AC Output 4 Status			Output 4 Status		Estado Salida CA 4		Estado SAl CA 4
66	32			15			AC Output 5 Status			Output 5 Status		Estado Salida CA 5		Estado Sal CA 5
67	32			15			AC Output 6 Status			Output 6 Status		Estado Salida CA 6		Estado Sal CA 6
68	32			15			AC Output 7 Status			Output 7 Status		Estado Salida CA 7		Estado Sal CA 7
69	32			15			AC Output 8 Status			Output 8 Status		Estado Salida CA 8		Estado Sal CA 8
70	32			15			AC Input Frequency High			Frequency High		Alta frecuencia Entrada CA	Alta frecuencia
71	32			15			AC Input Frequency Low			Frequency Low		Baja frecuencia Entrada CA	Baja frecuencia
72	32			15			AC Input MCCB Trip			Input MCCB Trip		Entrada Trip MCCB CA		Ent Trip MCCB
73	32			15			SPD Trip				SPD Trip		Trip SPD			Trip SPD
74	32			15			AC Output MCCB Trip		OutputMCCB Trip		Salida Trip MCCB CA		Sal Trip MCCB
75	32			15			AC Input 1 Failure		Input 1 Failure		Fallo entrada CA 1		Fallo Ent1 CA
76	32			15			AC Input 2 Failure		Input 2 Failure		Fallo entrada CA 2		Fallo Ent2 CA
77	32			15			AC Input 3 Failure		Input 3 Failure		Fallo entrada CA 3		Fallo Ent3 CA
78	32			15			Mains 1 Uab/Ua Under Voltage	M1Uab/Ua UnderV	Red 1 Baja Vrs/Vr		RD1 Baja Vrs/Vr
79	32			15			Mains 1 Ubc/Ub Under Voltage	M1Ubc/Ub UnderV	Red 1 Baja Vst/Vs		RD1 Baja Vst/Vs
80	32			15			Mains 1 Uca/Uc Under Voltage	M1Uca/Uc UnderV	Red 1 Baja Vrt/Vt		RD1 Baja Vrt/Vt
81	32			15			Mains 2 Uab/Ua Under Voltage	M2Uab/Ua UnderV	Red 2 Baja Vrs/Vr		RD2 Baja Vrs/Vr
82	32			15			Mains 2 Ubc/Ub Under Voltage	M2Ubc/Ub UnderV	Red 2 Baja Vst/Vs		RD2 Baja Vst/Vs
83	32			15			Mains 2 Uca/Uc Under Voltage	M2Uca/Uc UnderV	Red 2 Baja Vrt/Vt		RD2 Baja Vrt/Vt
84	32			15			Mains 3 Uab/Ua Under Voltage	M3Uab/Ua UnderV	Red 3 Baja Vrs/Vr		RD3 Baja Vrs/Vr
85	32			15			Mains 3 Ubc/Ub Under Voltage	M3Ubc/Ub UnderV	Red 3 Baja Vst/Vs		RD3 Baja Vst/Vs
86	32			15			Mains 3 Uca/Uc Under Voltage	M3Uca/Uc UnderV	Red 3 Baja Vrt/Vt		RD3 Baja Vrt/Vt
87	32			15			Mains 1 Uab/Ua Over Voltage	M1 Uab/Ua OverV		Red 1 Alta Vrs/Vr		RD1 Alta Vrs/Vr
88	32			15			Mains 1 Ubc/Ub Over Voltage	M1 Ubc/Ub OverV		Red 1 Alta Vst/Vs		RD1 Alta Vst/Vs
89	32			15			Mains 1 Uca/Uc Over Voltage	M1 Uca/Uc OverV		Red 1 Alta Vrt/Vt		RD1 Alta Vrt/Vt
90	32			15			Mains 2 Uab/Ua Over Voltage	M2 Uab/Ua OverV		Red 2 Alta Vrs/Vr		RD2 Alta Vrs/Vr
91	32			15			Mains 2 Ubc/Ub Over Voltage	M2 Ubc/Ub OverV		Red 2 Alta Vst/Vs		RD2 Alta Vst/Vs
92	32			15			Mains 2 Uca/Uc Over Voltage	M2 Uca/Uc OverV		Red 2 Alta Vrt/Vt		RD2 Alta Vrt/Vt
93	32			15			Mains 3 Uab/Ua Over Voltage	M3 Uab/Ua OverV		Red 3 Alta Vrs/Vr		RD3 Alta Vrs/Vr
94	32			15			Mains 3 Ubc/Ub Over Voltage	M3 Ubc/Ub OverV		Red 3 Alta Vst/Vs		RD3 Alta Vst/Vs
95	32			15			Mains 3 Uca/Uc Over Voltage	M3 Uca/Uc OverV		Red 3 Alta Vrt/Vt		RD3 Alta Vrt/Vt
96	32			15			Mains 1 Uab/Ua Failure		M1 Uab/Ua Fail		Fallo Red1 Vrs/Vr		FalloRed1 RS/R
97	32			15			Mains 1 Ubc/Ub Failure		M1 Ubc/Ub Fail		Fallo Red1 Vst/Vs		FalloRed1 ST/S
98	32			15			Mains 1 Uca/Uc Failure		M1 Uca/Uc Fail		Fallo Red1 Vrt/Vt		FalloRed1 RT/T
99	32			15			Mains 2 Uab/Ua Failure		M2 Uab/Ua Fail		Fallo Red2 Vrs/Vr		FalloRed2 RS/R
100	32			15			Mains 2 Ubc/Ub Failure		M2 Ubc/Ub Fail		Fallo Red2 Vst/Vs		FalloRed2 ST/S
101	32			15			Mains 2 Uca/Uc Failure		M2 Uca/Uc Fail		Fallo Red2 Vrt/Vt		FalloRed2 RT/T
102	32			15			Mains 3 Uab/Ua Failure		M3 Uab/Ua Fail		Fallo Red3 Vrs/Vr		FalloRed3 RS/R
103	32			15			Mains 3 Ubc/Ub Failure		M3 Ubc/Ub Fail		Fallo Red3 Vst/Vs		FalloRed3 ST/S
104	32			15			Mains 3 Uca/Uc Failure		M3 Uca/Uc Fail		Fallo Red3 Vrt/Vt		FalloRed3 RT/T
105	32			15			Communication Fail		Comm Fail	Fallo de Comunicación		Fallo COM
106	32			15			Over Voltage Limit		Over Volt Limit			Nivel de sobretensión		Nivel Sobretens
107	32			15			Under Voltage Limit		Under Volt Lmt		Nivel de subtensión		Nivel Subtens
108	32			15			Phase Failure Voltage		Phase Fail Volt	Tensión Fallo Fase		V Fallo Fase
109	32			15			Over Frequency Limit		Over Freq Limit		Límite de Alta Frecuencia	Lim Alta Frec
110	32			15			Under Frequency Limit		Under Freq Lmt	Límite de Baja Frecuencia	Lim Baja Frec
111	32			15			Current Transformer Coef	Curr Trans Coef	Coeficiente Transformador	Coef corriente
112	32			15			Input Type			Input Type	Tipo de Entrada			Tipo Entrada
113	32			15			Input Number			Input Number	Número de Entrada		Num Entrada
114	32			15			Current Measurement		Cur Measurement			Medida de corriente		Medida Corr
115	32			15			Output Number			Output Number	Número de Salida		Núm Salida
116	32			15			Distribution Address		Distri Address	Dirección de Distribución	Dir Distrib
117	32			15			Mains 1 Failure				Mains 1 Fail		Fallo de Red 1			Fallo Red 1
118	32			15			Mains 2 Failure				Mains 2 Fail		Fallo de Red 2			Fallo Red 2
119	32			15			Mains 3 Failure				Mains 3 Fail		Fallo de Red 3			Fallo Red 3
120	32			15			Mains 1 Uab/Ua Failure		M1 Uab/Ua Fail		Fallo Red1 Vrs/Vr		FalloRed1 RS/R
121	32			15			Mains 1 Ubc/Ub Failure		M1 Ubc/Ub Fail		Fallo Red1 Vst/Vs		FalloRed1 ST/S
122	32			15			Mains 1 Uca/Uc Failure		M1 Uca/Uc Fail		Fallo Red1 Vrt/Vt		FalloRed1 RT/T
123	32			15			Mains 2 Uab/Ua Failure		M2 Uab/Ua Fail		Fallo Red2 Vrs/Vr		FalloRed2 RS/R
124	32			15			Mains 2 Ubc/Ub Failure		M2 Ubc/Ub Fail		Fallo Red2 Vst/Vs		FalloRed2 ST/S
125	32			15			Mains 2 Uca/Uc Failure		M2 Uca/Uc Fail		Fallo Red2 Vrt/Vt		FalloRed2 RT/T
126	32			15			Mains 3 Uab/Ua Failure		M3 Uab/Ua Fail		Fallo Red3 Vrs/Vr		FalloRed3 RS/R
127	32			15			Mains 3 Ubc/Ub Failure		M3 Ubc/Ub Fail		Fallo Red3 Vst/Vs		FalloRed3 ST/S
128	32			15			Mains 3 Uca/Uc Failure		M3 Uca/Uc Fail		Fallo Red3 Vrt/Vt		FalloRed3 RT/T
129	32			15			Over Frequency			Over Frequency		Alta Frecuencia			Alta Frecuencia
130	32			15			Under Frequency			Under Frequency		Baja Frecuencia			Baja Frecuencia
131	32			15			Mains 1 Uab/Ua Under Voltage	M1Uab/Ua UnderV	Subtensión Red1 R-S/R		SubV Red1 RS/R
132	32			15			Mains 1 Ubc/Ub Under Voltage	M1Ubc/Ub UnderV	Subtensión Red1 S-T/S		SubV Red1 ST/S
133	32			15			Mains 1 Uca/Uc Under Voltage	M1Uca/Uc UnderV	Subtensión Red1 R-T/T		SubV Red1 RT/T
134	32			15			Mains 2 Uab/Ua Under Voltage	M2Uab/Ua UnderV	Subtensión Red2 R-S/R		SubV Red2 RS/R
135	32			15			Mains 2 Ubc/Ub Under Voltage	M2Ubc/Ub UnderV	Subtensión Red2 S-T/S		SubV Red2 ST/S
136	32			15			Mains 2 Uca/Uc Under Voltage	M2Uca/Uc UnderV	Subtensión Red2 R-T/T		SubV Red2 RT/T
137	32			15			Mains 3 Uab/Ua Under Voltage	M3Uab/Ua UnderV	Subtensión Red3 R-S/R		SubV Red3 RS/R
138	32			15			Mains 3 Ubc/Ub Under Voltage	M3Ubc/Ub UnderV	Subtensión Red3 S-T/S		SubV Red3 ST/S
139	32			15			Mains 3 Uca/Uc Under Voltage	M3Uca/Uc UnderV	Subtensión Red3 R-T/T		SubV Red3 RT/T
140	32			15			Mains 1 Uab/Ua Over Voltage	M1 Uab/Ua OverV		SObretensión Red1 R-S/R		SobrV Red1 RS/R
141	32			15			Mains 1 Ubc/Ub Over Voltage	M1 Ubc/Ub OverV		Sobretensión Red1 S-T/S		SobrV Red1 ST/S
142	32			15			Mains 1 Uca/Uc Over Voltage	M1 Uca/Uc OverV		Sobretensión Red1 R-T/T		SobrV Red1 RT/T
143	32			15			Mains 2 Uab/Ua Over Voltage	M2 Uab/Ua OverV		SObretensión Red2 R-S/R		SobrV Red2 RS/R
144	32			15			Mains 2 Ubc/Ub Over Voltage	M2 Ubc/Ub OverV		Sobretensión Red2 S-T/S		SobrV Red2 ST/S
145	32			15			Mains 2 Uca/Uc Over Voltage	M2 Uca/Uc OverV		Sobretensión Red2 R-T/T		SobrV Red2 RT/T
146	32			15			Mains 3 Uab/Ua Over Voltage	M3 Uab/Ua OverV		SObretensión Red3 R-S/R		SobrV Red3 RS/R
147	32			15			Mains 3 Ubc/Ub Over Voltage	M3 Ubc/Ub OverV		Sobretensión Red3 S-T/S		SobrV Red3 ST/S
148	32			15			Mains 3 Uca/Uc Over Voltage	M3 Uca/Uc OverV		Sobretensión Red3 R-T/T		SobrV Red3 RT/T
149	32			15			AC Input MCCB Trip		Input MCCB Trip		Entrada CA disparo MCCB		EntDisparo MCCB
150	32			15			AC Output MCCB Trip		OutputMCCB Trip		Salida CA disparo MCCB		SalDisparo MCCB
151	32			15			SPD Trip				SPD Trip		Disparo SPD			Disparo SPD
169	32			15			Communication Fail		Comm Fail	Fallo de Comunicación			Fallo COM
170	32			15			Mains Failure				Mains Failure		Fallo de Red			Fallo de Red
171	32			15			LargeDU AC Distribution		AC Distribution		Unidad Distr CA grande		Distr CA grande
172	32			15			No Alarm				No Alarm		Sin alarmas			Sin alarmas
173	32			15			Over Voltage			Over Volt		Sobretensión			Sobretensión
174	32			15			Under Voltage			Under Volt		Subtensión			Subtensión
175	32			15			AC Phase Failure			AC Phase Fail		Fallo Fase CA			Fallo Fase CA
176	32			15			No Alarm				No Alarm		Sin alarmas			Sin Alarmas
177	32			15			Over Frequency			Over Frequency		Alta frecuencia			Alta frecuencia
178	32			15			Under Frequency			Under Frequency		Baja frecuencia			Baja frecuencia
179	32			15			No Alarm				No Alarm		Sin alarmas			Sin alarmas
180	32			15			AC Over Voltage			AC Over Volt		Sobretensión CA			Sobretensión CA
181	32			15			AC Under Voltage		AC Under Volt		Subtensión CA			Subtensión CA
182	32			15			AC Phase Failure		AC Phase Fail		Fallo Fase CA			Fallo Fase CA
183	32			15			No Alarm				No Alarm		Sin Alarmas			Sin Alarmas
184	32			15			AC Over Voltage			AC Over Volt		Sobretensión CA			Sobretensión CA
185	32			15			AC Under Voltage		AC Under Volt		Subtensión CA			Subtensión CA
186	32			15			AC Phase Failure		AC Phase Fail		Fallo Fase CA			Fallo Fase CA
187	32			15			Mains 1 Uab/Ua Alarm		M1 Uab/Ua Alarm		Alarma Red 1 Vrs/Vr		Alarm Red1 RS/R
188	32			15			Mains 1 Ubc/Ub Alarm		M1 Ubc/Ub Alarm		Alarma Red 1 Vst/Vs		Alarm Red1 ST/S
189	32			15			Mains 1 Uca/Uc Alarm		M1 Uca/Uc Alarm		Alarma Red 1 Vrt/Vt		Alarm Red1 RT/T
190	32			15			Frequency Alarm			Frequency Alarm			Alarma Frecuencia		Alarma Frec
191	32			15			Communication Fail		Comm Fail	Fallo Comunicación			Fallo COM
192	32			15			Normal					Normal			Normal				Normal
193	32			15			Failure					Failure			Fallo				Fallo
194	32			15			Communication Fail		Comm Fail	Fallo Comunicación			Fallo COM
195	32			15			Mains Input Number		Mains Input Num			Núm Entrada Red			Núm Ent Red
196	32			15			Number 1			Number 1			N 1				N 1
197	32			15			Number 2			Number 2			N 2				N 2
198	32			15			Number 3			Number 3			N 3				N 3
199	32			15			None					None			Ninguna				Ninguna
200	32			15			Emergency Light				Emergency Light		Luz de emergencia		Luz emergencia
201	32			15			Closed					Closed			No				No
202	32			15			Open					Open			Sí				Sí
203	32			15			Mains 2 Uab/Ua Alarm		M2 Uab/Ua Alarm		Alarma Red 2 Vrs/Vr		Alarm Red2 RS/R
204	32			15			Mains 2 Ubc/Ub Alarm		M2 Ubc/Ub Alarm		Alarma Red 2 Vst/Vs		Alarm Red2 ST/S
205	32			15			Mains 2 Uca/Uc Alarm		M2 Uca/Uc Alarm		Alarma Red 2 Vrt/Vt		Alarm Red2 RT/T
206	32			15			Mains 3 Uab/Ua Alarm		M3 Uab/Ua Alarm		Alarma Red 3 Vrs/Vr		Alarm Red3 RS/R
207	32			15			Mains 3 Ubc/Ub Alarm		M3 Ubc/Ub Alarm		Alarma Red 3 Vst/Vs		Alarm Red3 ST/S
208	32			15			Mains 3 Uca/Uc Alarm		M3 Uca/Uc Alarm		Alarma Red 3 Vrt/Vt		Alarm Red3 RT/T
209	32			15			Normal					Normal			Normal				Normal
210	32			15			Alarm					Alarm			Alarma				Alarma
211	32			15			AC Fuse Number			AC Fuse Num	Núm de Fusible CA		Núm Fusible CA
212	32			15			Existence State				Existence State		Detección			Detección
213	32			15			Existent				Existent		Existente			Existente
214	32			15			Not Existent			Not Existent		No existente			No existente

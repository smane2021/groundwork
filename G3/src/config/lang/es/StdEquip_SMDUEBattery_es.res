﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Battery Current				Batt Current		Corriente a batería		Corriente bat
2	32			15			Battery Rating(Ah)			Batt Rating(Ah)		Capacidad batería		Capacidad bat
3	32			15			Exceed Current Limit			Exceed Curr Lmt			Límite de corriente pasado	Lim corr pasado
4	32			15			Battery					Battery			Batería				Batería
5	32			15			Over Battery Current			Over Current		Sobrecorriente a batería	Sobrecorriente
6	32			15			Battery Capacity (%)			Batt Cap (%)		Capacidad batería (%)		Capacidad
7	32			15			Battery Voltage				Batt Voltage		Tensión batería			Tensión bat
8	32			15			Low Capacity				Low Capacity		Baja capacidad			Baja capacidad
9	32			15			On					On			Conectado			Conectado
10	32			15			Off					Off			Desconectado			Desconectado
11	32			15			Battery Fuse Voltage			Fuse Voltage		Tensión fusible de batería	Tensión fus bat
12	32			15			Battery Fuse Status			Fuse Status		Estado fusible bat		Estado fusible
13	32			15			Fuse Alarm				Fuse Alarm		Alarma de fusible		Alarma fusible
14	32			15			SMDUE Battery				SMDUE Battery		Batería Extensión (SMDUE)	Batería Ext
15	32			15			State					State			Estado				Estado
16	32			15			Off					Off			Apagado				Apagado
17	32			15			On					On			Conectado			Conectado
18	32			15			Switch					Switch			Conmutador			Conmutador
19	32			15			Battery Over Current			Batt Over Curr		Sobrecorriente a batería	Sobrecorriente
20	32			15			Battery Management			Batt Management			Gestión de Baterías		Gestión Bat
21	32			15			Yes					Yes			No				No
22	32			15			No					No			Sí				Sí
23	32			15			Over Voltage Limit			Over Volt Limit		Nivel de Sobretensión		Sobretensión
24	32			15			Low Voltage Limit			Low Volt Limit		Nivel de Subtensión		Niv Subtensión
25	32			15			Battery Over Voltage			Over Voltage		Sobretensión Batería		Sobretensión
26	32			15			Battery Under Voltage			Under Voltage		Subtensión Batería		Subtensión Bat
27	32			15			Over Current				Over Current		Sobrecorriente			Sobrecorriente
28	32			15			Communication Fail			Comm Fail		Interrupción Comunicación	Interrup COM
29	32			15			Times of Communication Fail			Times Comm Fail			Fallos de Comunicación			Fallos COM
44	32			15			Battery Temp Number			Batt Temp Num		Sensor Temperatura		Sensor Temp
87	32			15			No					No		Ninguno				Ninguno

91	32			15			Temperature 1			Temperature 1		Temperatura 1		Temperatura 1
92	32			15			Temperature 2			Temperature 2		Temperatura 2		Temperatura 2
93	32			15			Temperature 3			Temperature 3		Temperatura 3		Temperatura 3
94	32			15			Temperature 4			Temperature 4		Temperatura 4		Temperatura 4
95	32			15			Temperature 5			Temperature 5		Temperatura 5		Temperatura 5
96	32			15			Rated Capacity				Rated Capacity		Capacidad Estimada		Capacidad Est
97	32			15			Battery Temperature			Battery Temp		Temperatura Batería		Temperatura
98	32			15			Battery Temperature Sensor		BattTemp Sensor		Sensor Temperatura Batería	Sensor Temp Bat
99	32			15			None					None			Ninguno				Ninguno
100	32			15			Temperature 1				Temperature 1		Temperatura 1			Temperatura 1
101	32			15			Temperature 2				Temperature 2		Temperatura 2			Temperatura 2
102	32			15			Temperature 3				Temperature 3		Temperatura 3			Temperatura 3
103	32			15			Temperature 4				Temperature 4		Temperatura 4			Temperatura 4
104	32			15			Temperature 5				Temperature 5		Temperatura 5			Temperatura 5
105	32			15			Temperature 6				Temperature 6		Temperatura 6			Temperatura 6
106	32			15			Temperature 7				Temperature 7		Temperatura 7			Temperatura 7
107	32			15			Temperature 8				Temperature 8		Temperatura 8			Temperatura 8
108	32			15			Temperature 9				Temperature 9		Temperatura 9			Temperatura 9
109	32			15			Temperature 10				Temperature 10		Temperatura 10			Temperatura 10

110	32			15			SMDUE1 Battery1			SMDUE1 Battery1			Batería1 Extensión1 (SMDUE1)	Batería1 ext1
111	32			15			SMDUE1 Battery2			SMDUE1 Battery2			Batería2 Extensión1 (SMDUE1)	Batería2 ext1
112	32			15			SMDUE1 Battery3			SMDUE1 Battery3			Batería3 Extensión1 (SMDUE1)	Batería3 ext1
113	32			15			SMDUE1 Battery4			SMDUE1 Battery4			Batería4 Extensión1 (SMDUE1)	Batería4 ext1
114	32			15			SMDUE1 Battery5			SMDUE1 Battery5			Batería5 Extensión1 (SMDUE1)	Batería5 ext1
115	32			15			SMDUE1 Battery6			SMDUE1 Battery6			Batería6 Extensión1 (SMDUE1)	Batería6 ext1
116	32			15			SMDUE1 Battery7			SMDUE1 Battery7			Batería7 Extensión1 (SMDUE1)	Batería7 ext1
117	32			15			SMDUE1 Battery8			SMDUE1 Battery8			Batería8 Extensión1 (SMDUE1)	Batería8 ext1
118	32			15			SMDUE1 Battery9			SMDUE1 Battery9			Batería9 Extensión1 (SMDUE1)	Batería9 ext1
119	32			15			SMDUE1 Battery10		SMDUE1 Battery10		Batería10 Extensión1 (SMDUE1)	Batería10 ext1
120	32			15			SMDUE2 Battery1			SMDUE2 Battery1			Batería1 Extensión2 (SMDUE2)	Batería1 ext2
121	32			15			SMDUE2 Battery2			SMDUE2 Battery2			Batería2 Extensión2 (SMDUE2)	Batería2 ext2
122	32			15			SMDUE2 Battery3			SMDUE2 Battery3			Batería3 Extensión2 (SMDUE2)	Batería3 ext2
123	32			15			SMDUE2 Battery4			SMDUE2 Battery4			Batería4 Extensión2 (SMDUE2)	Batería4 ext2
124	32			15			SMDUE2 Battery5			SMDUE2 Battery5			Batería5 Extensión2 (SMDUE2)	Batería5 ext2
125	32			15			SMDUE2 Battery6			SMDUE2 Battery6			Batería6 Extensión2 (SMDUE2)	Batería6 ext2
126	32			15			SMDUE2 Battery7			SMDUE2 Battery7			Batería7 Extensión2 (SMDUE2)	Batería7 ext2
127	32			15			SMDUE2 Battery8			SMDUE2 Battery8			Batería8 Extensión2 (SMDUE2)	Batería8 ext2
128	32			15			SMDUE2 Battery9			SMDUE2 Battery9			Batería9 Extensión2 (SMDUE2)	Batería9 ext2
129	32			15			SMDUE2 Battery10		SMDUE2 Battery10		Batería10 Extensión2 (SMDUE2)	Batería10 ext2

130	32			15			Battery 1			Batt 1			Batería 1			Bat 1	
131	32			15			Battery 2			Batt 2			Batería 2			Bat 2	
132	32			15			Battery 3			Batt 3			Batería 3			Bat 3	
133	32			15			Battery 4			Batt 4			Batería 4			Bat 4	
134	32			15			Battery 5			Batt 5			Batería 5			Bat 5	
135	32			15			Battery 6			Batt 6			Batería 6			Bat 6	
136	32			15			Battery 7			Batt 7			Batería 7			Bat 7	
137	32			15			Battery 8			Batt 8			Batería 8			Bat 8	
138	32			15			Battery 9			Batt 9			Batería 9			Bat 9	
139	32			15			Battery 10			Batt 10			Batería 10			Bat 10

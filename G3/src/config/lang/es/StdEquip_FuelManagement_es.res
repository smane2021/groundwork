﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE	
1		32			16			Fuel Tank				Fuel Tank		Depósito Combustible		Depósito Comb
2		32			15			Remaining Fuel Height			Remained Height		Altura Combustible		Altura Comb
3		32			15			Remaining Fuel Volume			Remained Volume			Volumen Combustible		Volumen Comb
4		32			15			Remaining Fuel Percent			RemainedPercent		Porcentaje Combustible		Nivel Comb
5		32			15			Fuel Theft Alarm			Fuel Theft Alm		Estado Alarma Robo		Alarma Robo
6		32			15			No					No			No				No
7		32			15			Yes					Yes			Sí				Sí
8		32			15			Multi-Shape Height Error Status		Multi Hgt Err	Error Altura Forma		Error Altura
9		32			15			Setting Configuration Done		Set Config Done		Confirmar Config Depósito	Conf Depósito
10		32			15			Reset Theft Alarm			Reset Theft Alm	Reponer Alarma Robo		Reponer Robo
11		32			15			Fuel Tank Type				Fuel Tank Type		Tipo de depósito		Tipo Depósito
12		32			15			Square Tank Length			Square Tank L		Largo Depósito Prisma		Largo Prisma
13		32			15			Square Tank Width			Square Tank W		Ancho Depósito Prisma		Ancho Prisma
14		32			15			Square Tank Height			Square Tank H		Altura Depósito Prisma		Altura Prisma
15		32			15			Vertical Cylinder Tank Diameter	Vertical Tank D		Diámetro Cilindro Vertical	Diámetr Cilin-V
16		32			15			Vertical Cylinder Tank Height		Vertical Tank H			Altura Cilindro Vertical	Alto Cilin-V
17		32			15			Horiz Cylinder Tank Diameter		Horiz Tank D		Diámetro Cilindro Horizontal	Diámetr Cilin-H
18		32			15			Horiz Cylinder Tank Length		Horiz Tank L		Largo Cilindro Horizontal	Largo Cilin-H
#For 20 multi-shape calibration points					
20		32			15			Number of Calibration Points		Num Cal Points			Número de Puntos Calibración	Puntos Calibra
21		32			15			Height 1 of Calibration Point		Height 1 Point				Altura Calibración Punto 1	Altura Punto 1
22		32			15			Volume 1 of Calibration Point		Volume 1 Point				Volumen Calibración Punto 1	Volumen Punto 1
23		32			15			Height 2 of Calibration Point		Height 2 Point				Altura Calibración Punto 2	Altura Punto 2
24		32			15			Volume 2 of Calibration Point		Volume 2 Point				Volumen Calibración Punto 2	Volumen Punto 2
25		32			15			Height 3 of Calibration Point		Height 3 Point				Altura Calibración Punto 3	Altura Punto 3
26		32			15			Volume 3 of Calibration Point		Volume 3 Point				Volumen Calibración Punto 3	Volumen Punto 3
27		32			15			Height 4 of Calibration Point		Height 4 Point				Altura Calibración Punto 4	Altura Punto 4
28		32			15			Volume 4 of Calibration Point		Volume 4 Point				Volumen Calibración Punto 4	Volumen Punto 4
29		32			15			Height 5 of Calibration Point		Height 5 Point				Altura Calibración Punto 5	Altura Punto 5
30		32			15			Volume 5 of Calibration Point		Volume 5 Point				Volumen Calibración Punto 5	Volumen Punto 5
31		32			15			Height 6 of Calibration Point		Height 6 Point				Altura Calibración Punto 6	Altura Punto 6
32		32			15			Volume 6 of Calibration Point		Volume 6 Point				Volumen Calibración Punto 6	Volumen Punto 6
33		32			15			Height 7 of Calibration Point		Height 7 Point				Altura Calibración Punto 7	Altura Punto 7
34		32			15			Volume 7 of Calibration Point		Volume 7 Point				Volumen Calibración Punto 7	Volumen Punto 7
35		32			15			Height 8 of Calibration Point		Height 8 Point			Altura Calibración Punto 8	Altura Punto 8
36		32			15			Volume 8 of Calibration Point		Volume 8 Point			Volumen Calibración Punto 8	Volumen Punto 8
37		32			15			Height 9 of Calibration Point		Height 9 Point			Altura Calibración Punto 9	Altura Punto 9
38		32			15			Volume 9 of Calibration Point		Volume 9 Point			Volumen Calibración Punto 9	Volumen Punto 9
39		32			15			Height 10 of Calibration Point		Height 10 Point				Altura Calibración Punto 10	Altura Punto10
40		32			15			Volume 10 of Calibration Point		Volume 10 Point				Volumen Calibración Punto 10	Volumen Punto10
41		32			15			Height 11 of Calibration Point		Height 11 Point				Altura Calibración Punto 11	Altura Punto11
42		32			15			Volume 11 of Calibration Point		Volume 11 Point				Volumen Calibración Punto 11	Volumen Punto11
43		32			15			Height 12 of Calibration Point		Height 12 Point				Altura Calibración Punto 12	Altura Punto12
44		32			15			Volume 12 of Calibration Point		Volume 12 Point				Volumen Calibración Punto 12	Volumen Punto12
45		32			15			Height 13 of Calibration Point		Height 13 Point				Altura Calibración Punto 13	Altura Punto13
46		32			15			Volume 13 of Calibration Point		Volume 13 Point				Volumen Calibración Punto 13	Volumen Punto13
47		32			15			Height 14 of Calibration Point		Height 14 Point				Altura Calibración Punto 14	Altura Punto14
48		32			15			Volume 14 of Calibration Point		Volume 14 Point				Volumen Calibración Punto 14	Volumen Punto14
49		32			15			Height 15 of Calibration Point		Height 15 Point				Altura Calibración Punto 15	Altura Punto15
50		32			15			Volume 15 of Calibration Point		Volume 15 Point				Volumen Calibración Punto 15	Volumen Punto15
51		32			15			Height 16 of Calibration Point		Height 16 Point				Altura Calibración Punto 16	Altura Punto16
52		32			15			Volume 16 of Calibration Point		Volume 16 Point				Volumen Calibración Punto 16	Volumen Punto16
53		32			15			Height 17 of Calibration Point		Height 17 Point				Altura Calibración Punto 17	Altura Punto17
54		32			15			Volume 17 of Calibration Point		Volume 17 Point				Volumen Calibración Punto 17	Volumen Punto17
55		32			15			Height 18 of Calibration Point		Height 18 Point				Altura Calibración Punto 18	Altura Punto18
56		32			15			Volume 18 of Calibration Point		Volume 18 Point				Volumen Calibración Punto 18	Volumen Punto18
57		32			15			Height 19 of Calibration Point		Height 19 Point				Altura Calibración Punto 19	Altura Punto19
58		32			15			Volume 19 of Calibration Point		Volume 19 Point				Volumen Calibración Punto 19	Volumen Punto19
59		32			15			Height 20 of Calibration Point		Height 20 Point				Altura Calibración Punto 20	Altura Punto20
60		32			15			Volume 20 of Calibration Point		Volume 20 Point				Volumen Calibración Punto 20	Volumen Punto20
#61		32			15			None					None			No				No
62		32			15			Square Tank				Square Tank			Prisma				Prisma
63		32			15			Vertical Cylinder Tank			Vert Cyl Tank		Cilindro Vertical		Cilindro V
64		32			15			Horizontal Cylinder Tank		Horiz Cyl Tank		Cilindro Horizontal		Cilindro H
65		32			15			Multi Shape Tank			MultiShapeTank	Multiforma			Multiforma
66		32			15			Low Fuel Level Limit			Low Level Limit		Límite Bajo Nivel Combustible	Bajo Nivel Comb
67		32			15			High Fuel Level Limit			Hi Level Limit		Límite Alto Nivel Combustible	Alto Nivel Comb
68		32			15			Maximum Consumption Speed		Max Flow Speed			Consumo máximo			Max Consumo
#For alarm of the volume of remained fuel			
71		32			15			High Fuel Level Alarm			Hi Level Alarm			Alto Nivel de Combustible	Combust Alto
72		32			15			Low Fuel Level Alarm			Low Level Alarm		Bajo Nivel de Combustible	Combust Bajo
73		32			15			Fuel Theft Alarm			Fuel Theft Alm	Alarma Robo Combustible		Robo Comb
74		32			15			Square Tank Height Error		Sq Tank Hgt Err	Error Altura Prisma		Err Alt Prisma
75		32			15			Vert Cylinder Tank Height Error		Vt Tank Hgt Err		Error Altura Cilindro V		Err Alt Cilin-V
76		32			15			Horiz Cylinder Tank Height Error	Hr Tank Hgt Err			Error Altura Cilindro H		Err Alt Cilin-H
77		32			15			Tank Height Error			Tank Height Err	Error Altura Multiforma		Err Alt M-forma
78		32			15			Fuel Tank Config Error			Fuel Config Err	Error Config Depósito		ErrCfg Depósito
80		32			15			Fuel Tank Config Error Status		Config Err		Error Config Depósito		ErrCfg Depósito

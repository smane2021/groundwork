﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN						ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE			
1		32			15			Battery Current						Batt Current		Corriente a batería		Corriente a bat
2	32			15			Battery Capacity					Batt Capacity		Capacidad baterías		Capacidad bat
3		32			15			Battery Voltage						Batt Voltage		Tensión de baterías		Tension bat
4		32			15			Ambient Temperature					Ambient Temp		Temperatura ambiente		Temp ambiente
5		32			15			Acid Temperature					Acid Temp		Temperatura de ácido		Temp acido
6		32			15			Total Time Batt Temp GT 30 deg.C			ToT GT 30 deg.C		Tiempo total bat t mayor 30ºC	Bat mayor 30ºC
7		32			15			Total Time Batt Temp LT 10 deg.C			ToT LT 10 deg.C		Tiempo total bat t menor 10ºC	Bat menor 10ºC
8		32			15			Battery Block 1 Voltage					Block 1 Volt		Tensión de celda 1		Tens celda 1
9		32			15			Battery Block 2 Voltage					Block 2 Volt		Tensión de celda 2		Tens celda 2
10		32			15			Battery Block 3 Voltage					Block 3 Volt		Tensión de celda 3		Tens celda 3
11		32			15			Battery Block 4 Voltage					Block 4 Volt		Tensión de celda 4		Tens celda 4
12		32			15			Battery Block 5 Voltage					Block 5 Volt		Tensión de celda 5		Tens celda 5
13		32			15			Battery Block 6 Voltage					Block 6 Volt		Tensión de celda 6		Tens celda 6
14		32			15			Battery Block 7 Voltage					Block 7 Volt		Tensión de celda 7		Tens celda 7
15		32			15			Battery Block 8 Voltage					Block 8 Volt		Tensión de celda 8		Tens celda 8
16		32			15			Battery Block 9 Voltage					Block 9 Volt		Tensión de celda 9		Tens celda 9
17		32			15			Battery Block 10 Voltage				Block 10 Volt		Tensión de celda 10		Tens celda 10
18		32			15			Battery Block 11 Voltage				Block 11 Volt		Tensión de celda 11		Tens celda 11
19		32			15			Battery Block 12 Voltage				Block 12 Volt		Tensión de celda 12		Tens celda 12
20		32			15			Battery Block 13 Voltage				Block 13 Volt		Tensión de celda 13		Tens celda 13
21		32			15			Battery Block 14 Voltage				Block 14 Volt		Tensión de celda 14		Tens celda 14
22		32			15			Battery Block 15 Voltage				Block 15 Volt		Tensión de celda 15		Tens celda 15
23		32			15			Battery Block 16 Voltage				Block 16 Volt		Tensión de celda 16		Tens celda 16
24		32			15			Battery Block 17 Voltage				Block 17 Volt		Tensión de celda 17		Tens celda 17
25		32			15			Battery Block 18 Voltage				Block 18 Volt		Tensión de celda 18		Tens celda 18
26		32			15			Battery Block 19 Voltage				Block 19 Volt		Tensión de celda 19		Tens celda 19
27		32			15			Battery Block 20 Voltage				Block 20 Volt		Tensión de celda 20		Tens celda 20
28		32			15			Battery Block 21 Voltage				Block 21 Volt		Tensión de celda 21		Tens celda 21
29		32			15			Battery Block 22 Voltage				Block 22 Volt		Tensión de celda 22		Tens celda 22
30		32			15			Battery Block 23 Voltage				Block 23 Volt		Tensión de celda 23		Tens celda 23
31		32			15			Battery Block 24 Voltage				Block 24 Volt		Tensión de celda 24		Tens celda 24
32		32			15			Battery Block 25 Voltage				Block 25 Volt		Tensión de celda 25		Tens celda 25
33		32			15			Shunt Voltage						Shunt Voltage		Tensión Shunt			Tension Shunt
34		32			15			Battery Leakage						Batt Leakage		Fuga de electrolito		Fuga bateria
35		32			15			Low Acid Level						Low Acid Level		Nivel de ácido bajo		Nivel bjo acid
36		32			15			Battery Disconnected					Batt Disconnec				Batería desconectada		Bat desconec
37		32			15			Battery High Temperature				Batt High Temp				Alta temperatura de batería	Alta temp bat
38		32			15			Battery Low Temperature					Batt Low Temp			Baja temperatura de batería	Baja temp bat
39		32			15			Block Voltage Difference				Block Volt Diff		Diferencia tensión de celda		Dif V elem
40	32			15			Battery Shunt Size					Batt Shunt Size		Shunt de batería		Shunt bat
41		32			15			Block Voltage Difference				Block Volt Diff		Diferencia tensión de celda	Dif tens elem
42		32			15			Battery High Temp Limit				High Temp Limit		Límite alta temperatura bat		Lim alta temp
43		32			15			Batt High Temp Limit Hysteresis			Batt HiTemp Hys				Histéresis lim alta temp bat	Hist alta temp
44		32			15			Battery Low Temp Limit			Low Temp Limit				Límite baja temperatura bat	Lim baja temp
45		32			15			Batt Low Temp Limit Hysteresis			Batt LoTemp Hys			Histéresis lim baja temp bat	Hist baja temp
46		32			15			Exceed Batt Current Limit				Over Curr Limit		Límite de corriente pasado	Lim corr pasdo
47	32			15			Battery Leakage						Battery Leakage		Fuga de electrolito		Fuga bateria
48	32			15			Low Acid Level						Low Acid Level		Nivel de ácido bajo		Nivel bjo acid
49		32			15			Battery Disconnected					Batt Disconnec				Batería desconectada		Bat desconec
50		32			15			Battery High Temperature				Batt High Temp				Alta temperatura batería	Alta temp bat
51		32			15			Battery Low Temperature					Batt Low Temp			Baja temperatura batería	Baja temp bat
52		32			15			Cell Voltage Difference					Cell Volt Diff		Diferencia tensión de celda	Dif tens elem
53		32			15			SM Unit Fail						SM Unit Fail		Fallo unidad SM			Fallo SM-BAT
54		32			15			Battery Disconnected					Batt Disconnec				Batería desconectada		Bat desconect
55		32			15			No							No			No				No
56		32			15			Yes							Yes			Sí				Sí
57		32			15			No							No			No				No
58		32			15			Yes							Yes			Sí				Sí
59		32			15			No							No			No				No
60		32			15			Yes							Yes			Sí				Sí
61		32			15			No							No			No				No
62		32			15			Yes							Yes			Sí				Sí
63		32			15			No							No			No				No
64		32			15			Yes							Yes			Sí				Sí
65		32			15			No							No			No				No
66		32			15			Yes							Yes			Sí				Sí
67		32			15			No							No			No				No
68		32			15			Yes							Yes			Sí				Sí
69		32			15			SM Battery						SM Battery		SM BAT				SM Bat
70		32			15			Over Battery Current					Over Batt Curr	Sobrecorriente a batería	Sobrecorr bat
71		32			15			Battery Capacity (%)					Batt Cap (%)	Capacidad batería (%)		Cap bat(%)
72		32			15			SMBAT Fail						SMBAT Fail	Fallo SM-BAT			Fallo SM-BAT
73		32			15			No							No			No				No
74		32			15			Yes							Yes			Sí				Sí
75		32			15			AI 4							AI 4			AI 4				AI 4
76		32			15			AI 7							AI 7			AI 7				AI 7
77		32			15			DI 4							DI 4			DI 4				DI 4
78		32			15			DI 5							DI 5			DI 5				DI 5
79		32			15			DI 6							DI 6			DI 6				DI 6
80		32			15			DI 7							DI 7			DI 7				DI 7
81		32			15			DI 8							DI 8			DI 8				DI 8
82		32			15			Relay 1 Status						Relay 1 Status		Estado relé 1			Estado Rel 1
83		32			15			Relay 2 Status						Relay 2 Status		Estado relé 2			Estado Rel 2
84		32			15			No							No			No				No
85		32			15			Yes							Yes			Sí				Sí
86		32			15			No							No			No				No
87		32			15			Yes							Yes			Sí				Sí
88		32			15			No							No			No				No
89		32			15			Yes							Yes			Sí				Sí
90		32			15			No							No			No				No
91		32			15			Yes							Yes			Sí				Sí
92		32			15			No							No			No				No
93		32			15			Yes							Yes			Sí				Sí
94		32			15			Off							Off			Apagado				Apagado
95		32			15			On							On			Conectado			Conectado
96		32			15			Off							Off			Apagado				Apagado
97		32			15			On							On			Conectado			Conectado
98		32			15			Relay 1 On/Off					Relay 1 On/Off		Relé 1				Rele 1
99		32			15			Relay 2 On/Off					Relay 2 On/Off		Relé 2				Rele 2
100		32			15			AI 2							AI 2			AI 2				AI 2
101		32			15			Battery Temperature Sensor Fail			T Sensor Fail		Fallo sensor de temperatura	Fallo sensTemp
102		32			15			Low Capacity						Low Capacity		Baja capacidad			Baja capacidad
103		32			15			Existence State						Existence State		Detección			Detección
104		32			15			Existent						Existent		Existente			Existente
105		32			15			Not Existent						Not Existent		Inexistente			Inexistente
106		32			15			Battery Communication Fail				Batt Comm Fail		Todas las baterías en Fallo Com		FalloComTotBat
107		32			15			Communication OK					Comm OK			Comunicación OK			Comunicación OK
108		32			15			Communication Fail					Comm Fail		Fallo de Comunicación			Fallo COM
109		32			15			Rated Capacity						Rated Capacity		Capacidad estimada		Capacidad Est
110		32			15			Battery Management					Batt Management		Used by Batt Management		Batt Manager
111		32			15			SM Batt Temp High Limit					SMBat TempHiLmt	SMBAT Alta temperatura batería	SM Alta tempBat
112		32			15			SM Batt Temp Low Limit					SMBat TempLoLmt	SMBAT Baja temperatura batería	SM Baja tempBat
113		32			15			SM Batt Temp						SM Bat Temp			Sensor Temperatura		Sensor Temp
114		32			15			Battery Communication Fail				Batt Comm Fail	SMBAT No responde		No responde
115		32			15			Battery Temp not Used					Bat Temp No Use		Sensor Temp no utilizado	Sin sensor Temp

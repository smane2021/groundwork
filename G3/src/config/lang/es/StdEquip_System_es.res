﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name 
    
[LOCALE_LANGUAGE]
es

[RES_INFO]
#R_ID	MAX_LEN_FULL	MAX_LEN_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32		16		Power System			Power System		Sistema				Sistema
2	32		15		System Voltage			Sys Volt		Tensión del Sistema		Tensión Sistema
3	32		15		System Load			System Load		Carga del Sistema		Carga Sistema
4	32		15		System Power			System Power		Potencia del Sistema		Potencia Sist
5	32		15		Total Power Consumption		Pwr Consumption		Consumo potencia total		Consumo total
6	32		15		Power Peak in 24 Hours		Power Peak		Pico de potencia en 24h		Pico Pot 24h
7	32		15		Average Power in 24 Hours	Ave Power		Potencia media en 24h		Pot media 24h
8	32		15		Hardware Write-protect Switch	Hardware Switch		Conmut Hw protector escritura	Proteccion Hw
9	32		15		Ambient Temperature		Amb Temp		Temperatura ambiente		Temp ambiente
18	32		15		DO 1-DO 1			DO 1-DO 1		DO 1-DO 1			DO 1-DO 1
19	32		15		DO 2-DO 2			DO 2-DO 2		DO 2-DO 2			DO 2-DO 2
20	32		15		DO 3-DO 3			DO 3-DO 3		DO 3-DO 3			DO 3-DO 3
21	32		15		DO 4-DO 4			DO 4-DO 4		DO 4-DO 4			DO 4-DO 4
22	32		15		DO 5-DO 5			DO 5-DO 5		DO 5-DO 5			DO 5-DO 5
23	32		15		DO 6-DO 6			DO 6-DO 6		DO 6-DO 6			DO 6-DO 6
24	32		15		DO 7-DO 7			DO 7-DO 7		DO 7-DO 7			DO 7-DO 7
25	32		15		DO 8-DO 8			DO 8-DO 8		DO 8-DO 8			DO 8-DO 8
27	32		15		Under Voltage 1 Level		Under Voltage 1		Nivel de subtensión 1		Subtensión 1
28	32		15		Under Voltage 2 Level		Under Voltage 2		Nivel de subtensión 2		Subtensión 2
29	32		15		Over Voltage 1 Level		Over Voltage 1		Nivel de sobretensión 1		Sobretensión 1
31	32		15		Temperature 1 High 1		Temp 1 High 1	Lim alta temperatura 1		Alta temp 1
32	32		15		Temperature 1 Low 1		Temp 1 Low 1		Lim baja temperatura 1		Baja temp 1
33	32		15		Auto/Manual State		Auto/Man State	Estado				Estado
34	32		15		Outgoing Alarms Blocked	Alarm Blocked		Alarmas salientes inhibidas	Alarmas inhib
35	32		15		Supervision Unit Fail		SupV Unit Fail		Fallo Interno			Fallo Interno
36	32		15		CAN Communication Failure	CAN Comm Fail		Fallo comunicación CAN		Fallo Com CAN
37	32		15		Mains Failure			Mains Failure		Fallo de Red			Fallo de Red
38	32		15		Under Voltage 1			Under Voltage 1			Subtensión 1			Subtensión 1
39	32		15		Under voltage 2			Under Voltage 2		Subtensión 2			Subtensión 2
40	32		15		Over Voltage			Over Voltage		Sobretensión 1			Sobretensión 1
41	32		15		High Temperature 1	High Temp 1		Alta temperatura 1		Alta temp 1
42	32		15		Low Temperature 1		Low Temp 1		Baja temperatura 1		Baja temp 1
43	32		15		Temperature 1 Sensor Fail	T1 Sensor Fail	Fallo sensor temperatura 1	Fallo sens 1
44	32		15		Outgoing Alarms Blocked		Alarm Blocked		Alarmas salientes inhibidas	Alarmas inhib
45	32		15		Maintenance Time Limit Alarm	Mtnc Time Alarm	Alarma de Mantenimiento		Alarma manten
46	32		15		Unprotected			Unprotected		No protegido			No protegido
47	32		15		Protected			Protected		Protegido			Protegido
48	32		15		Normal				Normal			Normal				Normal
49	32		15		Fail				Fail			Fallo				Fallo
50	32		15		Off				Off			Normal				Normal
51	32		15		On				On			Fallo				Fallo
52	32		15		Off				Off			Normal				Normal
53	32		15		On				On			Fallo				Fallo
54	32		15		Off				Off			Normal				Normal
55	32		15		On				On			Fallo				Fallo
56	32		15		Off				Off			Normal				Normal
57	32		15		On				On			Fallo				Fallo
58	32		15		Off				Off			Normal				Normal
59	32		15		On				On			Fallo				Fallo
60	32		15		Off				Off			Normal				Normal
61	32		15		On				On			Fallo				Fallo
62	32		15		Off				Off			Normal				Normal
63	32		15		On				On			Fallo				Fallo
64	32		15		Open				Open			Abierto				Abierto
65	32		15		Closed				Closed			Cerrado				Cerrado
66	32		15		Open				Open			Abierto				Abierto
67	32		15		Closed				Closed			Cerrado				Cerrado
78	32		15		Off				Off			Apagar				Apagar
79	32		15		On				On			Encender			Encender
80	32		15		Auto				Auto			Auto				Auto
81	32		15		Manual				Manual			Manual				Manual
82	32		15		Normal				Normal			Normal				Normal
83	32		15		Blocked				Blocked			Inhibidas			Inhibidas
85	32		15		Set to Auto Mode		To Auto Mode		Fijar modo automático		Modo auto
86	32		15		Set to Manual Mode		To Manual Mode		Fijar modo manual		Modo manual
87	32		15		Power Rate Level		PowerRate Level		Nivel de potencia		Nivel potencia
88	32		15		Power Peak Limit		Power Limit	Límite Pico de Potencia		Lim Potencia
89	32		15		Peak				Peak			Pico				Pico
90	32		15		High				High			Alto				Alta
91	32		15		Flat				Flat			Plano				Plano
92	32		15		Low				Low			Bajo				Bajo
93	32		15		Lower Consumption		Lwr Consumption		Bajo consumo habilitado		Bajo consumo
94	32		15		Power Peak Savings		P Peak Savings		Límite de potencia habilitado	Lim Pot habil
95	32		15		Disabled			Disabled		Deshabilitar			Deshabilitar
96	32		15		Enabled				Enabled			Habilitar			Habilitar
97	32		15		Disabled			Disabled		Deshabilitar			Deshabilitar
98	32		15		Enabled				Enabled			Habilitar			Habilitar
99	32		15		Over Maximum Power		Over Power		Potencia máxima alacanzada	Sobrepotencia
100	32		15		Normal				Normal			Normal				Normal
101	32		15		Alarm				Alarm			Alarma				Alarma
102	32		15		Over Maximum Power Alarm	Over Power		Alarma de Sobrepotencia		Sobrepotencia
104	32		15		System Alarm Status		Alarm Status		Estado de alarma		Estado
105	32		15		No Alarm			No Alm		Sin alarmas			Sin alarmas
106	32		15		Observation Alarm		Observation		Alarma O1			Alarma O1
107	32		15		Major Alarm			Major			Alarma A2			Alarma A2
108	32		15		Critical Alarm			Critical		Alarma A1			Alarma A1
109	32		15		Maintenance Run Time		Mtnc Run Time		Tiempo desde mantenimiento	Tiempo manten
110	32		15		Maintenance Cycle Time		Mtnc Cycle Time		Intervalo de mantenimiento	Intervalo mant
111	32		15		Maintenance Time Delay		Mtnc Time Delay		Retardo tiempo mantenimiento	Retard t mant
112	32		15		Maintenance Time Out		Mtnc Time Out		Fin tiempo de mantenimiento	Fin t manten
113	32		15		No				No			No				No
114	32		15		Yes				Yes			Sí		Sí
115	32		15		Power Split Mode		Power Split		Modo control Power Split	Power Split
116	32		15		Slave Current Limit Value	Slave Curr Lmt		Límite corriente esclavo	Lim corr esclav
117	32		15		Delta Voltage			Delta Volt	Tensión delta esclavo		V delta esclavo
118	32		15		Proportional Coefficient	Proportion Coef		Coeficiente proporcional	Coef propnal
119	32		15		Integral Time			Integral Time		Tiempo integral			Tiempo Intgr
120	32		15		Master Mode			Master			Maestro				Modo maestro
121	32		15		Slave Mode			Slave			Esclavo				Modo esclavo
122	32		15		MPCL Control Step		MPCL Ctrl Step		Paso control MPCL		Paso Ctrl MPCL
123	32		15		MPCL Pwr Range			MPCL Pwr Range		Umbral control MPCL		Umbr Ctrl MPCL
124	32		15		MPCL Battery Discharge On	MPCL BatDsch On			Descarga bat MPCL habilitada	Dscarg MPCL hab
125	32		15		MPCL Diesel Control On		MPCL DslCtrl On			GE MPCL habilitado Ctl		GE MPCL hab
126	32		15		Disabled			Disabled		Deshabilitado			Deshabilitado
127	32		15		Enabled				Enabled			Habilitado			Habilitado
128	32		15		Disabled			Disabled		Deshabilitado			Deshabilitado
129	32		15		Enabled				Enabled			Habilitado			Habilitado
130	32		15		System Time			System Time		Hora del sistema		Hora sistema
131	32		15		LCD Language			LCD Language		Idioma LCD			Idioma LCD
#changed by Frank Wu,1/1,20140221 for TR196 which text “Alarm Voice” should change to “Audible alarm”
132	32		15		Audible Alarm			Audible Alarm		Sonido alarma LCD		Sonido alarma
133	32		15		English				English			Inglés				Inglés
134	32		15		Spanish				Spanish			Español				Español
135	32		15		On				On			Conectado			Conectado
136	32		15		Off				Off			Apagado				Apagado
137	32		15		3 min				3 min			3 Min				3 Min
138	32		15		10 min				10 min				10 Min				10 Min
139	32		15		1 hour				1 h			1 Hora				1 Hora
140	32		15		4 hours				4 h			4 Horas				4 Horas
141	32		15		Clear Maintenance Run Time	Clr MtncRunTime		Iniciar Tiempo Mantenimiento	Inic t Mant
142	32		15		No				No			No				No
143	32		15		Yes				Yes			Sí		Sí
144	32		15		Alarm				Alarm			Alarma				Alarma
145	32		15		HW Status			HW Status		Estado ACU+			Estado ACU+
146	32		15		Normal				Normal			Normal				Normal
147	32		15		Config Error			Config Error		Error de configuración		Error config
148	32		15		Flash Fail			Flash Fail		Fallo flash			Fallo flash
150	32		15		LCD Time Zone			Time Zone		Zona horaria LCD		Zona horaria
151	32		15		Time Sync Main Time Server	Time Sync Svr		Tiempo sincr Servidor		T sinc serv
152	32		15		Time Sync Backup Time Server	Backup Sync Svr	Servidor sincr backup		Serv sincBckup
153	32		15		Time Sync Interval		Sync Interval		Intervalo sincr servidor	Interval sincr
155	32		15		Reset SCU			Reset SCU		Iniciar SCU			Iniciar SCU
156	32		15		Running Config Type		Config Type		Tipo de configuración		Tipo config
157	32		15		Normal Config			Normal Config		Configuración normal		Config normal
158	32		15		Backup Config			Backup Config		Configuración backup		Config backup
159	32		15		Default Config			Default Config		Configuración por defecto	Config defecto
160	32		15		Config Error(Backup Config)	Config Error 1		Error configuración backup	Error config 1
161	32		15		Config Error(Default Config)	Config Error 2		Error configuración defecto	Error config 2
162	32		15		Control System Alarm LED	Ctrl Sys Alarm		Control LED alarma Sistema	Ctrl alrm sist
163	32		15		Imbalance System Current	Imbalance Curr	Corriente anormal de carga	I carg anormal
164	32		15		Normal				Normal			Normal				Normal
165	32		15		Abnormal			Abnormal		Abnormal			Abnormal
167	32		15		MainSwitch Block Condition	MnSw Block Cond		Condición bloqueo disy CA	Bloq. disy CA
168	32		15		No				No			No				No
169	32		15		Yes				Yes			Sí			Sí
170	32		15		Total Run Time			Total Run Time		Tiempo total de operación	T en operación
171	32		15		Total Alarm Number		Total Alm Num		Total alarmas			Total alarmas
172	32		15		Total OA Number			Total OA Num		Total alarmas O1		Alarmas O1
173	32		15		Total MA Number			Total MA Num		Total alarmas A2		Alarmas A2
174	32		15		Total CA Number			Total CA Num		Total alarmas A1		Alarmas A1
175	32		15		SPD Fail			SPD Fail		Fallo SPD			Fallo SPD
176	32		15		Over Voltage 2 Level		Over Voltage 2		Nivel de sobretensión 2		Sobretensión 2
177	32		15		Very Over Voltage		Very Over Volt		Sobretensión 2			Sobretensión 2
178	32		15		Regulation Voltage		Regulate Volt		Permitir regulación de tensión	Reg tensión
179	32		15		Regulation Voltage Range	Regu Volt Range		Rango de regulación de tensión	Rango reg tens
180	32		15		Keypad Voice			Keypad Voice		Sonido teclado			Sonido teclas
181	32		15		On				On			Conectado			Conectado
182	32		15		Off				Off			Apagado				Apagado
183	32		15		Load Shunt Full Current		Load Shunt Curr		Corriente Shunt de carga	Corr Shunt carg
184	32		15		Load Shunt Full Voltage		Load Shunt Volt		Tensión Shunt de carga		Tens Shunt carg
185	32		15		Bat 1 Shunt Full Current	Bat1 Shunt Curr		Corriente Shunt Bat 1		Corr Shunt Bat1
186	32		15		Bat 1 Shunt Full Voltage	Bat1 Shunt Volt		Tensión Shunt Bat 1		Tens Shunt Bat1
187	32		15		Bat 2 Shunt Full Current	Bat2 Shunt Curr		Corriente Shunt Bat 2		Corr Shunt Bat2
188	32		15		Bat 2 Shunt Full Voltage	Bat2 Shunt Volt		Tensión Shunt Bat 2		Tens Shunt Bat2
219	32		15				Relay 6			Relay 6			Relé 6				Relé 6
220	32		15				Relay 7			Relay 7			Relé 7				Relé 7
221	32		15				Relay 8			Relay 8			Relé 8				Relé 8
222	32		15		RS232 Baud Rate			RS232 Baud Rate		Velocidad puerto RS232		Velocidad RS232
223	32		15		Local Address			Local Address		Dirección Local			Dirección Local
224	32		15		Callback Number			Callback Number			Número Retrollamada		N Retrollamada
225	32		15		Interval Time of Callback	Interval		Intervalo Retrollamada		Intervalo Retro
#226	32		15		Communication Password		Comm Password		Contraseña de Comunicación	Contraseña COM
227	32		15		DC Data Flag (On-Off)		DC Data Flag 1		Flag1 Datos CC			Flag1 Datos CC
228	32		15		DC Data Flag (Alarm)		DC Data Flag 2		Flag2 Datos CC			Flag2 Datos CC
229	32		15		Relay Reporting			Relay Reporting		Método Informe por Relés	Modo InfoRelés
230	32		15		Fixed				Fixed			Fijado				Fijado
231	32		15		User Defined			User Defined		Usuario Definido		Usuario Def
232	32		15		Rect Data Flag (Alarm)		RectDataFlag 2		Alarma Rectificador		Alarma Rec
233	32		15		Master Controlled		Master Ctrl		Maestro				Maestro
234	32		15		Slave Controlled		Slave Ctrl		Esclavo				Esclavo
236	32		15		Over Load Alarm Point		Over Load Point		Nivel de Sobrecarga		Nivl Sobrecarga
237	32		15		Over Load			Over Load		Sobrecarga			Sobrecarga
238	32		15		System Data Flag (On-Off)	Sys Data Flag 1		Flag1 Sistema			Flag1 Sistema
239	32		15		System Data Flag (Alarm)	Sys Data Flag 2	Flag2 Sistema			Flag2 Sistema
240	32		15		Spanish Language		Spanish			Español				Español
241	32		15		Imbalance Protection		Imb Protect		Protección Desequilibrio	Prot Desequilb
242	32		15		Enabled				Enabled			Habilitado			Habilitado
243	32		15		Disabled			Disabled			Deshabilitado			Deshabilitado
244	32		15		Buzzer Control			Buzzer Control		Sonido				Sonido
245	32		15		Normal				Normal			Normal				Normal
246	32		15		Disabled			Disabled		Deshabilitado			Deshabilitado
247	32		15		Ambient Temp Alarm		Temp Alarm		Alarma Temperatura Ambiente	Temp Ambiente
248	32		15		Normal				Normal			Normal				Normal
249	32		15		Low				Low			Baja				Baja
250	32		15		High				High			Alta				Alta
251	32		15		Reallocate Rect Address		Reallocate Addr		Renumerar Rectificadores	Renum Rectif
252	32		15		Normal				Normal			Normal				Normal
253	32		15		Reallocate Address		Reallocate Addr			Reasignar dirección		Reasignar dir
254	32		15		Test State Set			Test State Set	Fijar Estado Prueba		Estado Prueba
255	32		15		Normal				Normal			Normal				Normal
256	32		15		Test State			Test State		Estado de prueba		Estado Prueba
257	32		15		Minification for Test		Minification		Minimización Prueba		Minimizar
258	32		15		Digital Input 1			DI 1			DI 1				DI 1
259	32		15		Digital Input 2			DI 2			DI 2				DI 2
260	32		15		Digital Input 3			DI 3			DI 3				DI 3
261	32		15		Digital Input 4			DI 4			DI 4				DI 4
262	32		15		System Type Value		Sys Type Value		Tipo de Sistema			Tipo Sistema
263	32		15		AC/DC Board Type		AC/DC BoardType		Tipo Tarjeta CA/CC		Tarjeta CA/CC
264	32		15		B14C3U1				B14C3U1			B14C3U1				B14C3U1
265	32		15		Large DU			Large DU		UD Grande			UD Grande
266	32		15		SPD				SPD			SPD				SPD
267	32		15		Temp1				Temp1			Temperatura 1			Temp 1
268	32		15		Temp2				Temp2			Temperatura 2			Temp 2
269	32		15		Temp3				Temp3			Temperatura 3			Temp 3
270	32		15		Temp4				Temp4			Temperatura 4			Temp 4
271	32		15		Temp5				Temp5			Temperatura 5			Temp 5
272	32		15		Auto Mode			Auto Mode		Modo automático			Modo auto
273	32		15		EMEA				EMEA			EMEA				EMEA
274	32		15		Normal				Normal				Normal				Normal	
275	32		15		Nominal Voltage			Nom Voltage			Tensión Nominal			Tensión Nominal
276	32		15		EStop/EShutdown			EStop/EShutdown			EStop/EShutdown			EStop/EShutdown
277	32		15		Shunt 1 Full Current		Shunt 1 Current		Corriente Shunt 1		Corr Shunt 1
278	32		15		Shunt 2 Full Current		Shunt 2 Current		Corriente Shunt 2		Corr Shunt 2
279	32		15		Shunt 3 Full Current		Shunt 3 Current		Corriente Shunt 3		Corr Shunt 3
280	32		15		Shunt 1 Full Voltage		Shunt 1 Voltage		Tensión Shunt 1			Tens Shunt 1
281	32		15		Shunt 2 Full Voltage		Shunt 2 Voltage		Tensión Shunt 1			Tens Shunt 1
282	32		15		Shunt 3 Full Voltage		Shunt 3 Voltage		Tensión Shunt 1			Tens Shunt 1
283	32		15		Temperature 1			Temp 1			Sensor temperatura 1		Sens Temp 1
284	32		15		Temperature 2			Temp 2			Sensor temperatura 2		Sens Temp 2
285	32		15		Temperature 3			Temp 3			Sensor temperatura 3		Sens Temp 3
286	32		15		Temperature 4			Temp 4			Sensor temperatura 4		Sens Temp 4
287	32		15		Temperature 5			Temp 5			Sensor temperatura 5		Sens Temp 5
288	32		15		Very High Temperature 1 Limit	Very Hi Temp 1		Límite temperatura 1 muy alta	Muy alta Temp1
289	32		15		Very High Temperature 2 Limit	Very Hi Temp 2		Límite temperatura 2 muy alta	Muy alta Temp2
290	32		15		Very High Temperature 3 Limit	Very Hi Temp 3		Límite temperatura 3 muy alta	Muy alta Temp3
291	32		15		Very High Temperature 4 Limit	Very Hi Temp 4		Límite temperatura 4 muy alta	Muy alta Temp4
292	32		15		Very High Temperature5 Limit	Very Hi Temp 5		Límite temperatura 5 muy alta	Muy alta Temp5
293	32		15		High Temperature 2 Limit	High Temp 2		Límite alta temperatura 2	Lim alta Temp2
294	32		15		High Temperature 3 Limit	High Temp 3		Límite alta temperatura 3	Lim alta Temp3
295	32		15		High Temperature 4 Limit	High Temp 4		Límite alta temperatura 4	Lim alta Temp4
296	32		15		High Temperature 5 Limit	High Temp 5		Límite alta temperatura 5	Lim alta Temp5
297	32		15		Low Temperature 2 Limit		Low Temp 2		Límite baja temperatura 2	Lim baja Temp2
298	32		15		Low Temperature 3 Limit		Low Temp 3		Límite baja temperatura 3	Lim baja Temp3
299	32		15		Low Temperature 4 Limit		Low Temp 4		Límite baja temperatura 4	Lim baja Temp4
300	32		15		Low Temperature 5 Limit		Low Temp 5		Límite baja temperatura 5	Lim baja Temp5
301	32		15		Temperature 1 Not Used		Temp 1 Not Used		Temperatura 1 no utilizada	Temp1 no usada
302	32		15		Temperature 2 Not Used		Temp 2 Not Used		Temperatura 2 no utilizada	Temp2 no usada
303	32		15		Temperature 3 Not Used		Temp 3 Not Used		Temperatura 3 no utilizada	Temp3 no usada
304	32		15		Temperature 4 Not Used		Temp 4 Not Used		Temperatura 4 no utilizada	Temp4 no usada
305	32		15		Temperature 5 Not Used		Temp 5 Not Used		Temperatura 5 no utilizada	Temp5 no usada
306	32		15		High Temperature 2		High Temp 2		Alta temperatura 2		Alta temp 2
307	32		15		Low Temperature 2		Low Temp 2		Baja temperatura 2		Baja temp 2
308	32		15		Temperature 2 Sensor Fail	T2 Sensor Fail	Fallo sensor temperatura 2	Fallo sens 2
309	32		15		High Temperature 3	High Temp 3		Alta temperatura 3		Alta temp 3
310	32		15		Low Temperature 3		Low Temp 3		Baja temperatura 3		Baja temp 3
311	32		15		Temperature 3 Sensor Fail	T3 Sensor Fail		Fallo sensor temperatura 3	Fallo sens 3
312	32		15		High Temperature 4	High Temp 4		Alta temperatura 4		Alta temp 4
313	32		15		Low Temperature 4		Low Temp 4		Baja temperatura 4		Baja temp 4
314	32		15		Temperature 4 Sensor Fail	T4 Sensor Fail		Fallo sensor temperatura 4	Fallo sens 4
315	32		15		High Temperature 5	High Temp 5		Alta temperatura 5		Alta temp 5
316	32		15		Low Temperature 5		Low Temp 5		Baja temperatura 5		Baja temp 5
317	32		15		Temperature 5 Sensor Fail	T5 Sensor Fail		Fallo sensor temperatura 5	Fallo sens 5
318	32		15		Very High Temperature 1		Very Hi Temp 1		Temperatura 1 muy alta		Muy alta temp1
319	32		15		Very High Temperature 2		Very Hi Temp 2		Temperatura 2 muy alta		Muy alta temp2
320	32		15		Very High Temperature 3		Very Hi Temp 3		Temperatura 3 muy alta		Muy alta temp3
321	32		15		Very High Temperature 4		Very Hi Temp 4		Temperatura 4 muy alta		Muy alta temp4
322	32		15		Very High Temperature 5		Very Hi Temp 5		Temperatura 5 muy alta		Muy alta temp5
323	32		15		ECO Mode Status			ECO Mode Status	Modo Ahorro Energía		Ahorro Energía
324	32		15		ECO Mode		ECO Mode		Modo Ahorro Energía		Ahorro Energía
325	32		15		Internal Use (I2C)		Internal Use		Uso Interno (I2C)		Uso Interno
326	32		15		Disabled			Disabled		No				No
327	32		15		EStop				EStop			Parada de Emergencia		Parada Emergen
328	32		15		EShutdown			EShutdown		Cierre de Emergencia		Cierre Emergen
329	32		15		Ambient				Ambient			Ambiente			Ambiente
330	32		15		Battery				Battery			Batería				Batería
331	32		15		Temperature 6			Temperature 6				Temperatura 6			Temp 6
332	32		15		Temperature 7			Temperature 7				Temperatura 7			Temp 7
333	32		15		Temperature 6			Temp 6						Sensor temperatura 6		Sens Temp 6
334	32		15		Temperature 7			Temp 7						Sensor temperatura 7		Sens Temp 7
335	32		15		Very High Temperature 6 Limit	Very Hi Temp 6		Temperatura 6 muy alta		Muy alta temp6
336	32		15		Very High Temperature 7 Limit	Very Hi Temp 7		Temperatura 7 muy alta		Muy alta temp7
337	32		15		High Temperature6 Limit		High Temp 6		Límite alta temperatura 6	Lim alta Temp6
338	32		15		High Temperature7 Limit		High Temp 7		Límite alta temperatura 7	Lim alta Temp7
339	32		15		Low Temperature6 Limit		Low Temp 6		Límite baja temperatura 6	Lim baja Temp6
340	32		15		Low Temperature7 Limit		Low Temp 7		Límite baja temperatura 7	Lim baja Temp7
341	32		15		EIB Temp1 Not Used		EIB T1 Not Used		EIB Temperatura 1 no utilizada	EIB T1 no usada
342	32		15		EIB Temp2 Not Used		EIB T2 Not Used		EIB Temperatura 2 no utilizada	EIB T2 no usada
343	32		15		High Temperature 6		High Temp 6		Alta temperatura 6		Alta temp 6
344	32		15		Low Temperature 6		Low Temp 6		Baja temperatura 6		Baja temp 6
345	32		15		Temperature6 Sensor Fail	T6 Sensor Fail		Fallo sensor temperatura 6	Fallo sens 6
346	32		15		High Temperature 7		High Temp 7		Alta temperatura 7		Alta temp 7
347	32		15		Low Temperature 7		Low Temp 7		Baja temperatura 7		Baja temp 7
348	32		15		Temperature 7 Sensor Fail	T7 Sensor Fail		Fallo sensor temperatura 7	Fallo sens 7
349	32		15		Very High Temperature 6		Very Hi Temp 6		Temperatura 6 muy alta		Muy alta temp6
350	32		15		Very High Temperature 7		Very Hi Temp 7		Temperatura 7 muy alta		Muy alta temp7

1111	32		15		System Temp 1			System Temp1		Temperatura 1			Temperatura 1
1131	32		15		System Temp 1			System Temp1		Temperatura 1			Temperatura 1
1132	32		15		Battery Temperature 1 High 2	Batt Temp1 Hi2		Temperatura 1 Alta2	Alta2 Temp1
1133	32		15		Battery Temperature 1 High 1	Batt Temp1 Hi1		Temperatura 1 Alta1	Alta1 Temp1
1134	32		15		Battery Temperature 1 Low	Batt Temp1 Low		Baja temperatura 1	Baja Temp 1       
1141	32		15		System Temp 1 Not Used		Sys T1 Not Used		Temperatura 1 no utilizada	Temp1 no usada
1142	32		15		System Temp 1 Sensor Fail	SysT1SensorFail		Fallo sensor temperatura 1	Fallo Sens T1
1143	32		15		Battery Temperature 1 High 2	Batt Temp1 Hi2		Temperatura 1 Alta2	Alta2 Temp1 
1144	32		15		Battery Temperature 1 High 1	Batt Temp1 Hi1		Temperatura 1 Alta1	Alta1 Temp1
1145	32		15		Battery Temperature 1 Low	Batt Temp1 Low		Baja temperatura 1	Baja Temp 1
#
1211	32		15		System Temp 2		System Temp2		Temperatura 2			Temperatura 2
1231	32		15		System Temp 2			System Temp2		Temperatura 2			Temperatura 2
1232	32		15		Battery Temperature 2 High 2	Batt Temp2 Hi2		Temperatura 2 Alta2	Alta2 Temp2 
1233	32		15		Battery Temperature 2 High 1	Batt Temp2 Hi1		Temperatura 2 Alta1	Alta1 Temp2
1234	32		15		Battery Temperature 2 Low	Batt Temp2 Low		Baja temperatura 2	Baja Temp 2
1241	32		15		System Temp 2 Not Used		Sys T2 Not Used	Temperatura 2 no utilizada	Temp2 no usada
1242	32		15		System Temp 2 Sensor Fail	SysT2SensorFail		Fallo sensor temperatura 2	Fallo Sens T2
1243	32		15		Battery Temperature 2 High 2	Batt Temp2 Hi2		Temperatura 2 Alta2	Alta2 Temp2
1244	32		15		Battery Temperature 2 High 1	Batt Temp2 Hi1		Temperatura 2 Alta1	Alta1 Temp2
1245	32		15		Battery Temperature 2 Low	Batt Temp2 Low		Baja temperatura 2	Baja Temp 2
#
1311	32		15		System Temp 3			System Temp3		Temperatura 3			Temperatura 3
1331	32		15		System Temp 3			System Temp3		Temperatura 3	Temperatura 3
1332	32		15		Battery Temperature 3 High 2	Batt Temp3 Hi2		Temperatura 3 Alta2	Alta2 Temp3 
1333	32		15		Battery Temperature 3 High 1	Batt Temp3 Hi1		Temperatura 3 Alta1	Alta1 Temp3
1334	32		15		Battery Temperature 3 Low	Batt Temp3 Low		Baja temperatura 3	Baja Temp 3
1341	32		15		System Temp 3 Not Used		Sys T3 Not Used		Temperatura 3 no utilizada	Temp3 no usada
1342	32		15		System Temp 3 Sensor Fail	SysT3SensorFail		Fallo sensor temperatura 3	Fallo Sens T3
1343	32		15		Battery Temperature 3 High 2	Batt Temp3 Hi2		Temperatura 3 Alta2	Alta2 Temp3 
1344	32		15		Battery Temperature 3 High 1	Batt Temp3 Hi1		Temperatura 3 Alta1	Alta1 Temp3
1345	32		15		Battery Temperature 3 Low	Batt Temp3 Low		Baja temperatura 3	Baja Temp 3
1411	32		15		IB2-1 Temp 1			IB2-1 Temp1			IB2-Temp1			IB2-Temp1
1431	32		15		IB2-1 Temp 1			IB2-1 Temp1			IB2-Temp1			IB2-Temp1
1432	32		15		Battery Temperature 4 High 2	Batt Temp4 Hi2			Límite temperatura 4 muy alta	Muy alta Temp4
1433	32		15		Battery Temperature 4 High 1	Batt Temp4 Hi1		Límite alta temperatura 4	Lim alta Temp4
1434	32		15		Battery Temperature 4 Low	Batt Temp4 Low			Límite baja temperatura 4	Lim baja Temp4
1441	32		15		IB2-1 Temp 1 Not Used		IB2-1T1NotUsed		IB2-Temp1 no utilizada		IB2-T1 no usada
1442	32		15		IB2-1 Temp 1 Sensor Fail	IB2-1T1SensFail		Fallo sensor IB2-Temp1		Fallo IB2-T1
1443	32		15		Battery Temperature 4 High 2	Batt Temp4 Hi2			Temperatura 4 Alta2	Alta2 Temp4
1444	32		15		Battery Temperature 4 High 1	Batt Temp4 Hi1			Temperatura 4 Alta1	Alta1 Temp4
1445	32		15		Battery Temperature 4 Low	Batt Temp4 Low			Baja temperatura 4	Baja Temp 4
1511	32		15		IB2-1 Temp 2			IB2-1 Temp2			Temperatura 5			Temperatura 5
1531	32		15		IB2-1 Temp 2			IB2-1 Temp2			IB2-Temp2			IB2-Temp2
1532	32		15		Battery Temperature 5 High 2	Batt Temp5 Hi2			Temperatura 5 Alta2	Alta2 Temp5
1533	32		15		Battery Temperature 5 High 1	Batt Temp5 Hi1			Temperatura 5 Alta1	Alta1 Temp5
1534	32		15		Battery Temperature 5 Low	Batt Temp5 Low			Baja temperatura 5	Baja Temp 5
1541	32		15		IB2-1 Temp 2 Not Used		IB2-1T2NotUsed		IB2-Temp2 no utilizada		IB2-T2 no usada
1542	32		15		IB2-1 Temp 2 Sensor Fail	IB2-1T2SensFail		Fallo sensor IB2-Temp2		Fallo IB2-T2
1543	32		15		Battery Temperature 5 High 2	Batt Temp5 Hi2			Temperatura 5 Alta2	Alta2 Temp5
1544	32		15		Battery Temperature 5 High 1	Batt Temp5 Hi1			Temperatura 5 Alta1	Alta1 Temp5
1545	32		15		Battery Temperature 5 Low	Batt Temp5 Low			Baja temperatura 5	Baja Temp 5
1611	32		15		EIB-1 Temp 1			EIB-1 Temp1			EIB-Temp1			EIB-Temp1
1631	32		15		EIB-1 Temp 1			EIB-1 Temp1			EIB-Temp1			EIB-Temp1
1632	32		15		Battery Temperature 6 High 2	Batt Temp6 Hi2			Temperatura 6 Alta2	Alta2 Temp6 
1633	32		15		Battery Temperature 6 High 1	Batt Temp6 Hi1			Temperatura 6 Alta1	Alta1 Temp6
1634	32		15		Battery Temperature 6 Low	Batt Temp6 Low			Baja temperatura 6	Baja Temp 6
1641	32		15		EIB-1 Temp 1 Not Used		EIB-1T1NotUsed		EIB-Temp1 no utilizada		EIB-T1 no usada
1642	32		15		EIB-1 Temp 1 Sensor Fail	EIB-1T1SensFail		Fallo sensor EIB-Temp1		Fallo EIB-T1
1643	32		15		Battery Temperature 6 High 2	Batt Temp6 Hi2			Temperatura 6 Alta2	Alta2 Temp6 
1644	32		15		Battery Temperature 6 High 1	Batt Temp6 Hi1			Temperatura 6 Alta1	Alta1 Temp6
1645	32		15		Battery Temperature 6 Low	Batt Temp6 Low			Baja temperatura 6	Baja Temp 6
1711	32		15		EIB-1 Temp 2			EIB-1 Temp2			EIB-Temp2			EIB-Temp2
1731	32		15		EIB-1 Temp 2			EIB-1 Temp2			EIB-Temp2			EIB-Temp2
1732	32		15		Battery Temperature 7 High 2	Batt Temp7 Hi2			Temperatura 7 Alta2	Alta2 Temp7 
1733	32		15		Battery Temperature 7 High 1	Batt Temp7 Hi1			Temperatura 7 Alta1	Alta1 Temp7
1734	32		15		Battery Temperature 7 Low	Batt Temp7 Low			Baja temperatura 7	Baja Temp 7
1741	32		15		EIB-1 Temp 2 Not Used		EIB-1T2NotUsed		EIB-Temp2 no utilizada		EIB-T2 no usada
1742	32		15		EIB-1 Temp 2 Sensor Fail	EIB-1T2SensFail		Fallo sensor EIB-Temp2		Fallo EIB-T2
1743	32		15		Battery Temperature 7 High 2	Batt Temp7 Hi2		Temperatura 7 Alta2	Alta2 Temp7
1744	32		15		Battery Temperature 7 High 1	Batt Temp7 Hi1		Temperatura 7 Alta1	Alta1 Temp7
1745	32		15		Battery Temperature 7 Low	Batt Temp7 Low		Baja temperatura 7	Baja Temp 7
1746	32		15		DHCP Failure			DHCP Failure		Fallo DHCP			Fallo DHCP
1747	32		15		Internal Use (CAN)		Internal Use		Uso Interno(CAN)		Uso Interno
1748	32		15		Internal Use (485)		Internal Use		Uso Interno(485)		Uso Interno
1749	32		15		Address				Address			Address				Address	
1750	32		15		201				201			201				201
1751	32		15		202				202			202				202
1752	32		15		203				203			203				203
1753	32		15		Master/Slave Mode		Mast/Slave Mode		Modo Maestro/Esclavo		Modo
1754	32		15		Master				Master			Maestro				Modo maestro
1755	32		15		Slave				Slave			Esclavo				Modo esclavo
1756	32		15		Alone				Alone			Alone				Alone
1757	32		15		SM Batt Temp High Limit		SM Bat Temp Hi		Límite Alta Temp SMBAT		Lim AltaT SMBAT
1758	32		15		SM Batt Temp Low Limit		SM Bat Temp Low	Límite Baja Temp SMBAT		Lim BajaT SMBAT
1759	32		15		PLC Config Error		PLC Config Err		Error Configuración PLC		Error Cfg PLC
1760	32		15		Rectifier Expansion		Rect Expansion		Modo Extensión			Modo EXt 
1761	32		15		Inactive			Inactive		No				No
1762	32		15		Primary				Primary			Principal			Principal
1763	32		15		Secondary			Secondary		Extensión			Extensión
1764	128		64		Mode Changed. Auto Configuration Will Start.	Mode Changed. Auto Configuration Will Start.		El modo ha cambiado! Se va a iniciar la autoconfiguración!		Se va a iniciar Autoconfig!
1765	32		15		Previous Language		Previous Lang	Idioma anterior			Idioma anterior
1766	32		15		Current Language		Current Lang		Idioma actual			Idioma actual
1767	32		15		Eng				Eng		Inglés				Inglés
1768	32		15		Spanish				Spanish			Español				Español
1769	32		15		485 Communication Failure	485 Comm Fail		Fallo COM RS-485		Fallo COM 485
1770	64		32		SMDU Sampler Mode		SMDU Samp Mode		Modo muestreo SMDU		Muestreo SMDU
1771	32		15		CAN				CAN			CAN				CAN
1772	32		15		RS485				RS485			RS485				RS485
1773	32		15		Manual Mode Time Limit		Manual Mode Lmt	Retardo Automático		Autoretardo
1774	32		15		Observation Summary		OA Summary		Total O1			Total O1
1775	32		15		Major Summary			MA Summary		Total A2			Total A2
1776	32		15		Critical Summary		CA Summary		Total A1			Total A1
1777	32		15		All Rectifiers Current		All Rects Curr		Total corriente Rectif		Total Corr Rec
1778	32		15		Rectifier Group Lost Status	Rect Grp Lost		Rectificador Perdido		Rec Perdido
1779	32		15		Reset Rectifier Group Lost Alarm	Rst RectGrpLost		Cesar Rectificador Perdido	Cesar R perdido
1780	32		15		Rect Group Num on Last Power-On	RectGrp On Num	Ultimo Num Rectif Esclavos	N Rec Esclavos
1781	32		15		Rectifier Group Lost		Rect Group Lost	Rectificador Perdido		Rec Perdido
1782	32		15		ETemperature 1 High 1		ETemp 1 High 1		Límite Alta Temp Ambiente 1	Alta Temp1 Amb
1783	32		15		ETemperature 1 Low		ETemp 1 Low				Límite Baja Temp Ambiente 1	Baja Temp1 Amb
1784	32		15		ETemperature 2 High 1		ETemp 2 High 1		Límite Alta Temp Ambiente 2	Alta Temp2 Amb
1785	32		15		ETemperature 2 Low		ETemp 2 Low				Límite Baja Temp Ambiente 2	Baja Temp2 Amb
1786	32		15		ETemperature 3 High 1		ETemp 3 High 1		Límite Alta Temp Ambiente 3	Alta Temp3 Amb
1787	32		15		ETemperature 3 Low		ETemp 3 Low				Límite Baja Temp Ambiente 3	Baja Temp3 Amb
1788	32		15		ETemperature 4 High 1		ETemp 4 High 1		Límite Alta Temp Ambiente 4	Alta Temp4 Amb
1789	32		15		ETemperature 4 Low		ETemp 4 Low				Límite Baja Temp Ambiente 4	Baja Temp4 Amb
1790	32		15		ETemperature 5 High 1		ETemp 5 High 1		Límite Alta Temp Ambiente 5	Alta Temp5 Amb
1791	32		15		ETemperature 5 Low		ETemp 5 Low				Límite Baja Temp Ambiente 5	Baja Temp5 Amb
1792	32		15		ETemperature 6 High 1		ETemp 6 High 1		Límite Alta Temp Ambiente 6	Alta Temp6 Amb
1793	32		15		ETemperature 6 Low		ETemp 6 Low				Límite Baja Temp Ambiente 6	Baja Temp6 Amb
1794	32		15		ETemperature 7 High 1		ETemp 7 High 1		Límite Alta Temp Ambiente 7	Alta Temp7 Amb
1795	32		15		ETemperature 7 Low		ETemp 7 Low				Límite Baja Temp Ambiente 7	Baja Temp7 Amb
1796	32		15		ETemperature 1 High 1		ETemp 1 High 1		Alta Temperatura Ambiente 1	Alta Temp1 Amb
1797	32		15		ETemperature 1 Low		ETemp 1 Low				Baja Temperatura Ambiente 1	Baja Temp1 Amb
1798	32		15		ETemperature 2 High 1		ETemp 2 High 1		Alta Temperatura Ambiente 2	Alta Temp2 Amb
1799	32		15		ETemperature 2 Low		ETemp 2 Low				Baja Temperatura Ambiente 2	Baja Temp2 Amb
1800	32		15		ETemperature 3 High 1		ETemp 3 High 1		Alta Temperatura Ambiente 3	Alta Temp3 Amb
1801	32		15		ETemperature 3 Low		ETemp 3 Low				Baja Temperatura Ambiente 3	Baja Temp3 Amb
1802	32		15		ETemperature 4 High 1		ETemp 4 High 1		Alta Temperatura Ambiente 4	Alta Temp4 Amb
1803	32		15		ETemperature 4 Low		ETemp 4 Low				Baja Temperatura Ambiente 4	Baja Temp4 Amb
1804	32		15		ETemperature 5 High 1		ETemp 5 High 1		Alta Temperatura Ambiente 5	Alta Temp5 Amb
1805	32		15		ETemperature 5 Low		ETemp 5 Low				Baja Temperatura Ambiente 5	Baja Temp5 Amb
1806	32		15		ETemperature 6 High 1		ETemp 6 High 1		Alta Temperatura Ambiente 6	Alta Temp6 Amb
1807	32		15		ETemperature 6 Low		ETemp 6 Low				Baja Temperatura Ambiente 6	Baja Temp6 Amb
1808	32		15		ETemperature 7 High 1		ETemp 7 High 1		Alta Temperatura Ambiente 7	Alta Temp7 Amb
1809	32		15		ETemperature 7 Low		ETemp 7 Low				Baja Temperatura Ambiente 7	Baja Temp7 Amb
1810	32		15		Fast Sampler Flag		Fast Sampl Flag		Flag Muestreo Rápido		Flag M Rápido
1811	32		15		LVD Quantity			LVD Quantity		Número de LVDs			Núm LVDs
1812	32		15		LCD Rotation			LCD Rotation		Rotación LCD			Rotación LCD
1813	32		15		0 deg				0 deg			0 grados			0 grados
1814	32		15		90 deg				90 deg			90 grados			90 grados
1815	32		15		180 deg				180 deg			180 grados			180 grados
1816	32		15		270 deg				270 deg			270 grados			270 grados
1817	32		15		Over Voltage 1			Over Voltage 1			Sobretensión 1			Sobretensión 1
1818	32		15		Over Voltage 2			Over Voltage 2			Sobretensión 2			Sobretensión 2 
1819	32		15		Under Voltage 1			Under Voltage 1				Subtensión 1			Subtensión 1
1820	32		15		Under Voltage 2			Under Voltage 2				Subtensión 2			Subtensión 2
1821	32		15		Over Voltage 1 (24V)		24V Over Volt1		Sobretensión 1			Sobretensión 1
1822	32		15		Over Voltage 2 (24V)		24V Over Volt2		Sobretensión 2			Sobretensión 2 
1823	32		15		Under Voltage 1 (24V)		24V Under Volt1		Subtensión 1			Subtensión 1
1824	32		15		Under Voltage 2 (24V)		24V Under Volt2		Subtensión 2			Subtensión 2
1825	32		15		Fail Safe Mode			Fail Safe		Modo protegido			Modo protegido
1826	32	15		Disabled			Disabled				Deshabilitar			Deshabilitar
1827	32		15		Enabled				Enabled					Habilitar			Habilitar
1828	32		15		Hybrid Mode			Hybrid Mode		Modo Híbrido			Modo Híbrido
1829	32		15	Disabled			Disabled		No				No
1830	32		15		Capacity			Capacity		Capacidad			Capacidad
1831	32		15	Fixed Daily			Fixed Daily			Cíclico				Cíclico
1832	32		15		DG Run at High Temp		DG Run Overtemp	GE con Alta Temperatura		GE Alta Temp
1833	32		15		DG Used for Hybrid		DG Used		Utilizar GE con Híbrido		GE en uso
1834	32		15		DG1				DG1			GE1				GE1
1835	32		15		DG2				DG2			GE2				GE2
1836	32		15		Both				Both			Ambos				Ambos
1837	32		15		DI for Grid			DI for Grid		DI de Red			DI de Red
1838	32		15		IB2-1 DI1				IB2-1 DI1			IB2-1 DI 1				IB2-1 DI 1
1839	32		15		IB2-1 DI2				IB2-1 DI2			IB2-1 DI 2				IB2-1 DI 2
1840	32		15		IB2-1 DI3				IB2-1 DI3			IB2-1 DI 3				IB2-1 DI 3
1841	32		15		IB2-1 DI4				IB2-1 DI4			IB2-1 DI 4				IB2-1 DI 4
1842	32		15		IB2-1 DI5				IB2-1 DI5			IB2-1 DI 5				IB2-1 DI 5
1843	32		15		IB2-1 DI6				IB2-1 DI6			IB2-1 DI 6				IB2-1 DI 6
1844	32		15		IB2-1 DI7				IB2-1 DI7			IB2-1 DI 7				IB2-1 DI 7
1845	32		15		IB2-1 DI8				IB2-1 DI8			IB2-1 DI 8				IB2-1 DI 8
1846	32		15		DOD				DOD			Profundidad de descarga		Profun descarg
1847	32		15		Discharge Duration		Dsch Duration		Duración Descarga		Duración Descar
1848	32		15		Start Discharge Time		Start Dsch Time		Inicio Descarga			Inicio Descarga
1849	32		15		Diesel Run Over Temp		DG Run OverTemp		Sobretemperatura GE		SobreTemp GE
1850	32		15		DG1 is Running			DG1 is Running		GE1 operando			GE1 operando
1851	32		15		DG2 is Running			DG2 is Running		GE2 operando			GE2 operando
1852	32		15		Hybrid High Load Setting	High Load Set		Alta carga Híbrido		Alta Carg Hibr
1853	32		15		Hybrid is High Load		High Load		Alta carga Híbrido		Alta Carg Hibr
1854	32		15		DG Run Time at High Temp	DG Run Time		Tiempo Alta Temp GE		Alta Temp GE
1855	32		15		Equalizing Start Time		Equal StartTime		Tiempo inicio Equalización	Inicio Equaliz
1856	32		15		DG1 Failure			DG1 Failure		Fallo GE1			Fallo GE1
1857	32		15		DG2 Failure			DG2 Failure		Fallo GE2			Fallo GE2
1858	32		15		Diesel Alarm Delay		DG Alarm Delay		Retardo Alarma G.E.		Retard Alar GE
1859	32		15		Grid is on			Grid is on		Red conectada			Hay Red
1860	32		15		PowerSplit Contactor Mode	Contactor Mode		Modo Control PowerSplit		Modo PowerSplit
1861	32		15		Ambient Temp		Amb Temp		Temperatura Ambiente		Temp Amb
1862	32		15		Ambient Temp Sensor		Amb Temp Sensor		Sensor Temperatura Ambiente	Sensor Temp Amb
1863	32		15		None				None			No				No
1864	32		15		Temperature 1			Temperature 1		Temperatura 1			Temperatura 1
1865	32		15		Temperature 2			Temperature 2		Temperatura 2			Temperatura 2
1866	32		15		Temperature 3			Temperature 3		Temperatura 3			Temperatura 3
1867	32		15		Temperature 4			Temperature 4		Temperatura 4			Temperatura 4
1868	32		15		Temperature 5			Temperature 5		Temperatura 5			Temperatura 5
1869	32		15		Temperature 6			Temperature 6		Temperatura 6			Temperatura 6
1870	32		15		Temperature 7			Temperature 7		Temperatura 7			Temperatura 7
1871	32		15		Temperature 8			Temperature 8		Temperatura 8			Temperatura 8
1872	32		15		Temperature 9			Temperature 9		Temperatura 9			Temperatura 9
1873	32		15		Temperature 10			Temperature 10		Temperatura 10			Temperatura 10
1874	32		15		Ambient Temp High1		Amb Temp Hi1		Alta 1 temperatura ambiente	Alta1 Temp Amb
1875	32		15		Ambient Temp Low		Amb Temp Low		Baja temperatura ambiente	Baja Temp Amb
1876	32		15		Ambient Temp High1		Amb Temp Hi1	Alta1 temperatura ambiente	Alta1 Temp Amb
1877	32		15		Ambient Temp Low		Amb Temp Low	Baja temperatura ambiente	Baja Temp Amb
1878	32		15		Ambient Sensor Fail		AmbSensor Fail		Fallo sensor temperatura amb		Fallo sensT Amb
1879	32		15		Digital Input 1			DI1			DI 1				DI 1
1880	32		15		Digital Input 2			DI2			DI 2				DI 2
1881	32		15		Digital Input 3			DI3			DI 3				DI 3
1882	32		15		Digital Input 4			DI4			DI 4				DI 4
1883	32		15		Digital Input 5			DI5			DI 5				DI 5
1884	32		15		Digital Input 6			DI6			DI 6				DI 6
1885	32		15		Digital Input 7			DI7			DI 7				DI 7
1886	32		15		Digital Input 8			DI8			DI 8				DI 8
1887	32		15		Off				Off			Apagado				Apagado
1888	32		15		On				On			Conectado			Conectado
1889	32		15		DO1-Relay Output		DO1-RelayOutput		DO1-Salida Relé		DO1-SalidaRelé
1890	32		15		DO2-Relay Output		DO2-RelayOutput		DO2-Salida Relé		DO2-SalidaRelé
1891	32		15		DO3-Relay Output		DO3-RelayOutput		DO3-Salida Relé		DO3-SalidaRelé
1892	32		15		DO4-Relay Output		DO4-RelayOutput		DO4-Salida Relé		DO4-SalidaRelé
1893	32		15		DO5-Relay Output		DO5-RelayOutput		DO5-Salida Relé		DO5-SalidaRelé
1894	32		15		DO6-Relay Output		DO6-RelayOutput		DO6-Salida Relé		DO6-SalidaRelé
1895	32		15		DO7-Relay Output		DO7-RelayOutput		DO7-Salida Relé		DO7-SalidaRelé
1896	32		15		DO8-Relay Output		DO8-RelayOutput		DO8-Salida Relé		DO8-SalidaRelé
1897	32		15		DI1 Alarm State			DI1 Alm State		Nivel de alarma DI 1		Nivel act DI1
1898	32		15		DI2 Alarm State			DI2 Alm State		Nivel de alarma DI 2		Nivel act DI2
1899	32		15		DI3 Alarm State			DI3 Alm State		Nivel de alarma DI 3		Nivel act DI3
1900	32		15		DI4 Alarm State			DI4 Alm State		Nivel de alarma DI 4		Nivel act DI4
1901	32		15		DI5 Alarm State			DI5 Alm State		Nivel de alarma DI 5		Nivel act DI5
1902	32		15		DI6 Alarm State			DI6 Alm State		Nivel de alarma DI 6		Nivel act DI6
1903	32		15		DI7 Alarm State			DI7 Alm State		Nivel de alarma DI 7		Nivel act DI7
1904	32		15		DI8 Alarm State			DI8 Alm State		Nivel de alarma DI 8		Nivel act DI8
1905	32		15		DI1 Alarm			DI1 Alarm		Alarma DI 1			Alarma DI 1
1906	32		15		DI2 Alarm			DI2 Alarm		Alarma DI 2			Alarma DI 2
1907	32		15		DI3 Alarm			DI3 Alarm		Alarma DI 3			Alarma DI 3
1908	32		15		DI4 Alarm			DI4 Alarm		Alarma DI 4			Alarma DI 4
1909	32		15		DI5 Alarm			DI5 Alarm		Alarma DI 5			Alarma DI 5
1910	32		15		DI6 Alarm			DI6 Alarm		Alarma DI 6			Alarma DI 6
1911	32		15		DI7 Alarm			DI7 Alarm		Alarma DI 7			Alarma DI 7
1912	32		15		DI8 Alarm			DI8 Alarm		Alarma DI 8			Alarma DI 8
1913	32		15		On				On			Conectado		Conectado
1914	32		15		Off				Off			Apagado				Apagado
1915	32		15		High				High			Alto				Alto
1916	32		15		Low				Low			Bajo				Bajo
1917	32		15		IB Num				IB Num		Número de IB			Núm IB
1918	32		15		IB Type				IB Type			Tipo IB				Tipo IB
1919	32		15		CSU_Undervoltage 1		Undervoltage 1				CSU Subtensión 1		CSU Subtens1
1920	32		15		CSU_Undervoltage 2		Undervoltage 2			CSU Subtensión 2		CSU Subtens2
1921	32		15		CSU_Overvoltage			Overvoltage			CSU Sobretensión		CSU Sobretens
1922	32		15		CSU_Communication Fail		Comm Fail			Fallo comunicación CSU		Fallo COM CSU
1923	32		15		CSU_External Alarm 1		Exter_Alarm1			CSU Alarma EXterna 1		CSU Alarma EXt1
1924	32		15		CSU_External Alarm 2		Exter_Alarm2			CSU Alarma EXterna 2		CSU Alarma EXt2
1925	32		15		CSU_External Alarm 3		Exter_Alarm3			CSU Alarma EXterna 3		CSU Alarma EXt3
1926	32		15		CSU_External Alarm 4		Exter_Alarm4			CSU Alarma EXterna 4		CSU Alarma EXt4
1927	32		15		CSU_External Alarm 5		Exter_Alarm5			CSU Alarma EXterna 5		CSU Alarma EXt5
1928	32		15		CSU_External Alarm 6		Exter_Alarm6			CSU Alarma EXterna 6		CSU Alarma EXt6
1929	32		15		CSU_External Alarm 7		Exter_Alarm7			CSU Alarma EXterna 7		CSU Alarma EXt7
1930	32		15		CSU_External Alarm 8		Exter_Alarm8			CSU Alarma EXterna 8		CSU Alarma EXt8
1931	32		15	CSU_External Comm Fail		Ext_Comm Fail		Fallo comunicación Ext CSU		Fallo COM Ext
1932	32		15	CSU_Bat_Curr Limit Alarm	Bat_Curr Limit			CSU limitando corriente a Bat	Lim corr Bat
1933	32		15		DC LC Number			DC LC Num		DCLCNumber			DCLCNumber
1934	32		15		Bat LC Number			Bat LC Num		BatLCNumber			BatLCNumber
1935	32		15		Ambient Temp High2		Amb Temp Hi2		Muy Alta temperatura ambiente	Muy Alta T amb
1936	32		15		System Type			System Type		System Type			System Type
1937	32		15		Normal				Normal			Normal				Normal
1938	32		15		Test				Test			Test				Test
1939	32		15		Auto Mode			Auto Mode		Modo				Modo
1940	32		15		EMEA				EMEA			EMEA				EMEA
1941	32		15		Normal				Normal			Normal				Normal
1942	32		15		Bus Run Mode			Bus Run Mode		Modo COM SMDU			COM SMDU
1943	32		15		NO				NO		NO				NO
1944	32		15		NC				NC		NC				NC
1945	32		15		Fail Safe Mode(Hybrid)		Fail Safe		Modo protegido(Híbrido)		Modo protegido
1946	32		15		IB Communication Fail		IB Comm Fail		Fallo IB			Fallo IB
1947	32		15		Relay Test			Relay Test	Prueba de relés			Prueba Relés	
1948	32		15		NCU DO1 Test			NCU DO1 Test		Relé de prueba 1		Relé prueba 1
1949	32		15		NCU DO2 Test			NCU DO2 Test		Relé de prueba 2		Relé prueba 2
1950	32		15		NCU DO3 Test			NCU DO3 Test		Relé de prueba 3		Relé prueba 3
1951	32		15		NCU DO4 Test			NCU DO4 Test		Relé de prueba 4		Relé prueba 4
1952	32		15		Relay 5 Test			Relay 5 Test		Relé de prueba 5		Relé prueba 5
1953	32		15		Relay 6 Test			Relay 6 Test		Relé de prueba 6		Relé prueba 6
1954	32		15		Relay 7 Test			Relay 7 Test		Relé de prueba 7		Relé prueba 7
1955	32		15		Relay 8 Test			Relay 8 Test		Relé de prueba 8		Relé prueba 8
1956	32		15		Relay Test			Relay Test		Prueba de Relés			Prueba Relés
1957	32		15		Relay Test Time			Relay Test Time		Tiempo de prueba Relés		Tiempo prba Rel
1958	32		15		Manual			Manual		Manual			Manual
1959	32		15		Automatic			Automatic		Automática			Automática
1960	32		15		System Temp1		System T1	Temperatura 1			Temperatura 1
1961	32		15		System Temp2		System T2	Temperatura 2			Temperatura 2
1962	32		15		System Temp3		System T3	Temperatura 3			Temperatura 3
1963	32		15		IB2-1 Temp1		IB2-1 T1		IB2-1-Temp1			IB2-1-Temp1
1964	32		15		IB2-1 Temp2		IB2-1 T2		IB2-1-Temp2			IB2-1-Temp2
1965	32		15		EIB-1 Temp1		EIB-1 T1		EIB-1-Temp1			EIB-1-Temp1
1966	32		15		EIB-1 Temp2		EIB-1 T2		EIB-1-Temp2			EIB-1-Temp2
1967	32		15		SMTemp1 Temp1		SMTemp1 T1		SMTemp1-T1			SMTemp1-T1
1968	32		15		SMTemp1 Temp2		SMTemp1 T2		SMTemp1-T2			SMTemp1-T2
1969	32		15		SMTemp1 Temp3		SMTemp1 T3		SMTemp1-T3			SMTemp1-T3
1970	32		15		SMTemp1 Temp4		SMTemp1 T4		SMTemp1-T4			SMTemp1-T4
1971	32		15		SMTemp1 Temp5		SMTemp1 T5		SMTemp1-T5			SMTemp1-T5
1972	32		15		SMTemp1 Temp6		SMTemp1 T6		SMTemp1-T6			SMTemp1-T6
1973	32		15		SMTemp1 Temp7		SMTemp1 T7		SMTemp1-T7			SMTemp1-T7
1974	32		15		SMTemp1 Temp8		SMTemp1 T8		SMTemp1-T8			SMTemp1-T8
1975	32		15		SMTemp2 Temp1		SMTemp2 T1		SMTemp2-T1			SMTemp2-T1
1976	32		15		SMTemp2 Temp2		SMTemp2 T2		SMTemp2-T2			SMTemp2-T2
1977	32		15		SMTemp2 Temp3		SMTemp2 T3		SMTemp2-T3			SMTemp2-T3
1978	32		15		SMTemp2 Temp4		SMTemp2 T4		SMTemp2-T4			SMTemp2-T4
1979	32		15		SMTemp2 Temp5		SMTemp2 T5		SMTemp2-T5			SMTemp2-T5
1980	32		15		SMTemp2 Temp6		SMTemp2 T6		SMTemp2-T6			SMTemp2-T6
1981	32		15		SMTemp2 Temp7		SMTemp2 T7		SMTemp2-T7			SMTemp2-T7
1982	32		15		SMTemp2 Temp8		SMTemp2 T8		SMTemp2-T8			SMTemp2-T8
1983	32		15		SMTemp3 Temp1		SMTemp3 T1		SMTemp3-T1			SMTemp3-T1
1984	32		15		SMTemp3 Temp2		SMTemp3 T2		SMTemp3-T2			SMTemp3-T2
1985	32		15		SMTemp3 Temp3		SMTemp3 T3		SMTemp3-T3			SMTemp3-T3
1986	32		15		SMTemp3 Temp4		SMTemp3 T4		SMTemp3-T4			SMTemp3-T4
1987	32		15		SMTemp3 Temp5		SMTemp3 T5		SMTemp3-T5			SMTemp3-T5
1988	32		15		SMTemp3 Temp6		SMTemp3 T6		SMTemp3-T6			SMTemp3-T6
1989	32		15		SMTemp3 Temp7		SMTemp3 T7		SMTemp3-T7			SMTemp3-T7
1990	32		15		SMTemp3 Temp8		SMTemp3 T8		SMTemp3-T8			SMTemp3-T8
1991	32		15		SMTemp4 Temp1		SMTemp4 T1		SMTemp4-T1			SMTemp4-T1
1992	32		15		SMTemp4 Temp2		SMTemp4 T2		SMTemp4-T2			SMTemp4-T2
1993	32		15		SMTemp4 Temp3		SMTemp4 T3		SMTemp4-T3			SMTemp4-T3
1994	32		15		SMTemp4 Temp4		SMTemp4 T4		SMTemp4-T4			SMTemp4-T4
1995	32		15		SMTemp4 Temp5		SMTemp4 T5		SMTemp4-T5			SMTemp4-T5
1996	32		15		SMTemp4 Temp6		SMTemp4 T6		SMTemp4-T6			SMTemp4-T6
1997	32		15		SMTemp4 Temp7		SMTemp4 T7		SMTemp4-T7			SMTemp4-T7
1998	32		15		SMTemp4 Temp8		SMTemp4 T8		SMTemp4-T8			SMTemp4-T8
1999	32		15		SMTemp5 Temp1		SMTemp5 T1		SMTemp5-T1			SMTemp5-T1
2000	32		15		SMTemp5 Temp2		SMTemp5 T2		SMTemp5-T2			SMTemp5-T2
2001	32		15		SMTemp5 Temp3		SMTemp5 T3		SMTemp5-T3			SMTemp5-T3
2002	32		15		SMTemp5 Temp4		SMTemp5 T4		SMTemp5-T4			SMTemp5-T4
2003	32		15		SMTemp5 Temp5		SMTemp5 T5		SMTemp5-T5			SMTemp5-T5
2004	32		15		SMTemp5 Temp6		SMTemp5 T6		SMTemp5-T6			SMTemp5-T6
2005	32		15		SMTemp5 Temp7		SMTemp5 T7		SMTemp5-T7			SMTemp5-T7
2006	32		15		SMTemp5 Temp8		SMTemp5 T8		SMTemp5-T8			SMTemp5-T8
2007	32		15		SMTemp6 Temp1		SMTemp6 T1		SMTemp6-T1			SMTemp6-T1
2008	32		15		SMTemp6 Temp2		SMTemp6 T2		SMTemp6-T2			SMTemp6-T2
2009	32		15		SMTemp6 Temp3		SMTemp6 T3		SMTemp6-T3			SMTemp6-T3
2010	32		15		SMTemp6 Temp4		SMTemp6 T4		SMTemp6-T4			SMTemp6-T4
2011	32		15		SMTemp6 Temp5		SMTemp6 T5		SMTemp6-T5			SMTemp6-T5
2012	32		15		SMTemp6 Temp6		SMTemp6 T6		SMTemp6-T6			SMTemp6-T6
2013	32		15		SMTemp6 Temp7		SMTemp6 T7		SMTemp6-T7			SMTemp6-T7
2014	32		15		SMTemp6 Temp8		SMTemp6 T8		SMTemp6-T8			SMTemp6-T8
2015	32		15		SMTemp7 Temp1		SMTemp7 T1		SMTemp7-T1			SMTemp7-T1
2016	32		15		SMTemp7 Temp2		SMTemp7 T2		SMTemp7-T2			SMTemp7-T2
2017	32		15		SMTemp7 Temp3		SMTemp7 T3		SMTemp7-T3			SMTemp7-T3
2018	32		15		SMTemp7 Temp4		SMTemp7 T4		SMTemp7-T4			SMTemp7-T4
2019	32		15		SMTemp7 Temp5		SMTemp7 T5		SMTemp7-T5			SMTemp7-T5
2020	32		15		SMTemp7 Temp6		SMTemp7 T6		SMTemp7-T6			SMTemp7-T6
2021	32		15		SMTemp7 Temp7		SMTemp7 T7		SMTemp7-T7			SMTemp7-T7
2022	32		15		SMTemp7 Temp8		SMTemp7 T8		SMTemp7-T8			SMTemp7-T8
2023	32		15		SMTemp8 Temp1		SMTemp8 T1		SMTemp8-T1			SMTemp8-T1
2024	32		15		SMTemp8 Temp2		SMTemp8 T2		SMTemp8-T2			SMTemp8-T2
2025	32		15		SMTemp8 Temp3		SMTemp8 T3		SMTemp8-T3			SMTemp8-T3
2026	32		15		SMTemp8 Temp4		SMTemp8 T4		SMTemp8-T4			SMTemp8-T4
2027	32		15		SMTemp8 Temp5		SMTemp8 T5		SMTemp8-T5			SMTemp8-T5
2028	32		15		SMTemp8 Temp6		SMTemp8 T6		SMTemp8-T6			SMTemp8-T6
2029	32		15		SMTemp8 Temp7		SMTemp8 T7		SMTemp8-T7			SMTemp8-T7
2030	32		15		SMTemp8 Temp8		SMTemp8 T8		SMTemp8-T8			SMTemp8-T8
2031	32		15		System Temp1 High 2	System T1 Hi2		Alta2 Temperatura 1		Alta2 Temp1
2032	32		15		System Temp1 High 1	System T1 Hi1	Alta1 Temperatura 1		Alta1 Temp1
2033	32		15		System Temp1 Low	System T1 Low	Baja Temperatura 1		Baja Temp1
2034	32		15		System Temp2 High 2	System T2 Hi2		Alta2 Temperatura 2		Alta2 Temp2
2035	32		15		System Temp2 High 1	System T2 Hi1	Alta1 Temperatura 2		Alta1 Temp2
2036	32		15		System Temp2 Low	System T2 Low	Baja Temperatura 2		Baja Temp2
2037	32		15		System Temp3 High 2	System T3 Hi2		Alta3 Temperatura 3		Alta3 Temp3
2038	32		15		System Temp3 High 1	System T3 Hi1	Alta Temperatura 3		Alta Temp3
2039	32		15		System Temp3 Low	System T3 Low	Baja Temperatura 3		Baja Temp3
2040	32		15		IB2-1 Temp1 High 2	IB2-1 T1 Hi2	Alta2 IB2-1-Temp1			Alta2 IB2-1-T1
2041	32		15		IB2-1 Temp1 High 1	IB2-1 T1 Hi1	Alta1 IB2-1-Temp 1		Alta1 IB2-1-T1
2042	32		15		IB2-1 Temp1 Low		IB2-1 T1 Low	Baja IB2-1-Temp 1			Baja IB2-1-T1
2043	32		15		IB2-1 Temp2 High 2	IB2-1 T2 Hi2	Alta2 IB2-1-Temp2			Alta2 IB2-1-T2
2044	32		15		IB2-1 Temp2 High 1	IB2-1 T2 Hi1	Alta1 IB2-1-Temp 2		Alta1 IB2-1-T2
2045	32		15		IB2-1 Temp2 Low		IB2-1 T2 Low	Baja IB2-1-Temp 2			Baja IB2-1-T2
2046	32		15		EIB-1 Temp1 High 2	EIB-1 T1 Hi2	Alta2 EIB-1-Temp1			Alta2 EIB-1-T1
2047	32		15		EIB-1 Temp1 High 1	EIB-1 T1 Hi1	Alta1 EIB-1-Temp 1		Alta1 EIB-1-T1
2048	32		15		EIB-1 Temp1 Low		EIB-1 T1 Low	Baja EIB-1-Temp 1			Baja EIB-1-T1
2049	32		15		EIB-1 Temp2 High 2	EIB-1 T2 Hi2	Alta2 EIB-1-Temp2			Alta2 EIB-1-T2
2050	32		15		EIB-1 Temp2 High 1	EIB-1 T2 Hi1	Alta1 EIB-1-Temp 2		Alta1 EIB-1-T2
2051	32		15		EIB-1 Temp2 Low		EIB-1 T2 Low	Baja EIB-1-Temp 2			Baja EIB-1-T2
#
2052		32		15		SMTemp1 Temp1 High 2	SMTemp1 T1 Hi2		Alta2 SMTemp1 Temp1	Al2 SMTemp1 T1
2053		32		15		SMTemp1 Temp1 High 1	SMTemp1 T1 Hi1		Alta1 SMTemp1 Temp1	Al1 SMTemp1 T1
2054		32		15		SMTemp1 Temp1 Low	SMTemp1 T1 Low		Baja SMTemp1 Temp1	Ba SMTemp1 T1
2055		32		15		SMTemp1 Temp2 High 2	SMTemp1 T2 Hi2		Alta2 SMTemp1 Temp2	Al2 SMTemp1 T2
2056		32		15		SMTemp1 Temp2 High 1	SMTemp1 T2 Hi1		Alta1 SMTemp1 Temp2	Al1 SMTemp1 T2
2057		32		15		SMTemp1 Temp2 Low	SMTemp1 T2 Low		Baja SMTemp1 Temp2	Ba SMTemp1 T2
2058		32		15		SMTemp1 Temp3 High 2	SMTemp1 T3 Hi2		Alta2 SMTemp1 Temp3	Al2 SMTemp1 T3
2059		32		15		SMTemp1 Temp3 High 1	SMTemp1 T3 Hi1		Alta1 SMTemp1 Temp3	Al1 SMTemp1 T3
2060		32		15		SMTemp1 Temp3 Low	SMTemp1 T3 Low		Baja SMTemp1 Temp3	Ba SMTemp1 T3
2061		32		15		SMTemp1 Temp4 High 2	SMTemp1 T4 Hi2		Alta2 SMTemp1 Temp4	Al2 SMTemp1 T4
2062		32		15		SMTemp1 Temp4 High 1	SMTemp1 T4 Hi1		Alta1 SMTemp1 Temp4	Al1 SMTemp1 T4
2063		32		15		SMTemp1 Temp4 Low	SMTemp1 T4 Low		Baja SMTemp1 Temp4	Ba SMTemp1 T4
2064		32		15		SMTemp1 Temp5 High 2	SMTemp1 T5 Hi2		Alta2 SMTemp1 Temp5	Al2 SMTemp1 T5
2065		32		15		SMTemp1 Temp5 High 1	SMTemp1 T5 Hi1		Alta1 SMTemp1 Temp5	Al1 SMTemp1 T5
2066		32		15		SMTemp1 Temp5 Low	SMTemp1 T5 Low		Baja SMTemp1 Temp5	Ba SMTemp1 T5
2067		32		15		SMTemp1 Temp6 High 2	SMTemp1 T6 Hi2		Alta2 SMTemp1 Temp6	Al2 SMTemp1 T6
2068		32		15		SMTemp1 Temp6 High 1	SMTemp1 T6 Hi1		Alta1 SMTemp1 Temp6	Al1 SMTemp1 T6
2069		32		15		SMTemp1 Temp6 Low	SMTemp1 T6 Low		Baja SMTemp1 Temp6	Ba SMTemp1 T6
2070		32		15		SMTemp1 Temp7 High 2	SMTemp1 T7 Hi2		Alta2 SMTemp1 Temp7	Al2 SMTemp1 T7
2071		32		15		SMTemp1 Temp7 High 1	SMTemp1 T7 Hi1		Alta1 SMTemp1 Temp7	Al1 SMTemp1 T7
2072		32		15		SMTemp1 Temp7 Low	SMTemp1 T7 Low		Baja SMTemp1 Temp7	Ba SMTemp1 T7
2073		32		15		SMTemp1 Temp8 High 2	SMTemp1 T8 Hi2		Alta2 SMTemp1 Temp8	Al2 SMTemp1 T8
2074		32		15		SMTemp1 Temp8 High 1	SMTemp1 T8 Hi1		Alta1 SMTemp1 Temp8	Al1 SMTemp1 T8
2075		32		15		SMTemp1 Temp8 Low	SMTemp1 T8 Low		Baja SMTemp1 Temp8	Ba SMTemp1 T8
2076		32		15		SMTemp2 Temp1 High 2	SMTemp2 T1 Hi2		Alta2 SMTemp2 Temp1	Al2 SMTemp2 T1
2077		32		15		SMTemp2 Temp1 High 1	SMTemp2 T1 Hi1		Alta1 SMTemp2 Temp1	Al1 SMTemp2 T1
2078		32		15		SMTemp2 Temp1 Low	SMTemp2 T1 Low		Baja SMTemp2 Temp1	Ba SMTemp2 T1
2079		32		15		SMTemp2 Temp2 High 2	SMTemp2 T2 Hi2		Alta2 SMTemp2 Temp2	Al2 SMTemp2 T2
2080		32		15		SMTemp2 Temp2 High 1	SMTemp2 T2 Hi1		Alta1 SMTemp2 Temp2	Al1 SMTemp2 T2
2081		32		15		SMTemp2 Temp2 Low	SMTemp2 T2 Low		Baja SMTemp2 Temp2	Ba SMTemp2 T2
2082		32		15		SMTemp2 Temp3 High 2	SMTemp2 T3 Hi2		Alta2 SMTemp2 Temp3	Al2 SMTemp2 T3
2083		32		15		SMTemp2 Temp3 High 1	SMTemp2 T3 Hi1		Alta1 SMTemp2 Temp3	Al1 SMTemp2 T3
2084		32		15		SMTemp2 Temp3 Low	SMTemp2 T3 Low		Baja SMTemp2 Temp3	Ba SMTemp2 T3
2085		32		15		SMTemp2 Temp4 High 2	SMTemp2 T4 Hi2		Alta2 SMTemp2 Temp4	Al2 SMTemp2 T4
2086		32		15		SMTemp2 Temp4 High 1	SMTemp2 T4 Hi1		Alta1 SMTemp2 Temp4	Al1 SMTemp2 T4
2087		32		15		SMTemp2 Temp4 Low	SMTemp2 T4 Low		Baja SMTemp2 Temp4	Ba SMTemp2 T4
2088		32		15		SMTemp2 Temp5 High 2	SMTemp2 T5 Hi2		Alta2 SMTemp2 Temp5	Al2 SMTemp2 T5
2089		32		15		SMTemp2 Temp5 High 1	SMTemp2 T5 Hi1		Alta1 SMTemp2 Temp5	Al1 SMTemp2 T5
2090		32		15		SMTemp2 Temp5 Low	SMTemp2 T5 Low		Baja SMTemp2 Temp5	Ba SMTemp2 T5
2091		32		15		SMTemp2 Temp6 High 2	SMTemp2 T6 Hi2		Alta2 SMTemp2 Temp6	Al2 SMTemp2 T6
2092		32		15		SMTemp2 Temp6 High 1	SMTemp2 T6 Hi1		Alta1 SMTemp2 Temp6	Al1 SMTemp2 T6
2093		32		15		SMTemp2 Temp6 Low	SMTemp2 T6 Low		Baja SMTemp2 Temp6	Ba SMTemp2 T6
2094		32		15		SMTemp2 Temp7 High 2	SMTemp2 T7 Hi2		Alta2 SMTemp2 Temp7	Al2 SMTemp2 T7
2095		32		15		SMTemp2 Temp7 High 1	SMTemp2 T7 Hi1		Alta1 SMTemp2 Temp7	Al1 SMTemp2 T7
2096		32		15		SMTemp2 Temp7 Low	SMTemp2 T7 Low		Baja SMTemp2 Temp7	Ba SMTemp2 T7
2097		32		15		SMTemp2 Temp8 High 2	SMTemp2 T8 Hi2		Alta2 SMTemp2 Temp8	Al2 SMTemp2 T8
2098		32		15		SMTemp2 Temp8 High 1	SMTemp2 T8 Hi1		Alta1 SMTemp2 Temp8	Al1 SMTemp2 T8
2099		32		15		SMTemp2 Temp8 Low	SMTemp2 T8 Low		Baja SMTemp2 Temp8	Ba SMTemp2 T8
2100		32		15		SMTemp3 Temp1 High 2	SMTemp3 T1 Hi2		Alta2 SMTemp3 Temp1	Al2 SMTemp3 T1
2101		32		15		SMTemp3 Temp1 High 1	SMTemp3 T1 Hi1		Alta1 SMTemp3 Temp1	Al1 SMTemp3 T1
2102		32		15		SMTemp3 Temp1 Low	SMTemp3 T1 Low		Baja SMTemp3 Temp1	Ba SMTemp3 T1
2103		32		15		SMTemp3 Temp2 High 2	SMTemp3 T2 Hi2		Alta2 SMTemp3 Temp2	Al2 SMTemp3 T2
2104		32		15		SMTemp3 Temp2 High 1	SMTemp3 T2 Hi1		Alta1 SMTemp3 Temp2	Al1 SMTemp3 T2
2105		32		15		SMTemp3 Temp2 Low	SMTemp3 T2 Low		Baja SMTemp3 Temp2	Ba SMTemp3 T2
2106		32		15		SMTemp3 Temp3 High 2	SMTemp3 T3 Hi2		Alta2 SMTemp3 Temp3	Al2 SMTemp3 T3
2107		32		15		SMTemp3 Temp3 High 1	SMTemp3 T3 Hi1		Alta1 SMTemp3 Temp3	Al1 SMTemp3 T3
2108		32		15		SMTemp3 Temp3 Low	SMTemp3 T3 Low		Baja SMTemp3 Temp3	Ba SMTemp3 T3
2109		32		15		SMTemp3 Temp4 High 2	SMTemp3 T4 Hi2		Alta2 SMTemp3 Temp4	Al2 SMTemp3 T4
2110		32		15		SMTemp3 Temp4 High 1	SMTemp3 T4 Hi1		Alta1 SMTemp3 Temp4	Al1 SMTemp3 T4
2111		32		15		SMTemp3 Temp4 Low	SMTemp3 T4 Low		Baja SMTemp3 Temp4	Ba SMTemp3 T4
2112		32		15		SMTemp3 Temp5 High 2	SMTemp3 T5 Hi2		Alta2 SMTemp3 Temp5	Al2 SMTemp3 T5
2113		32		15		SMTemp3 Temp5 High 1	SMTemp3 T5 Hi1		Alta1 SMTemp3 Temp5	Al1 SMTemp3 T5
2114		32		15		SMTemp3 Temp5 Low	SMTemp3 T5 Low		Baja SMTemp3 Temp5	Ba SMTemp3 T5
2115		32		15		SMTemp3 Temp6 High 2	SMTemp3 T6 Hi2		Alta2 SMTemp3 Temp6	Al2 SMTemp3 T6
2116		32		15		SMTemp3 Temp6 High 1	SMTemp3 T6 Hi1		Alta1 SMTemp3 Temp6	Al1 SMTemp3 T6
2117		32		15		SMTemp3 Temp6 Low	SMTemp3 T6 Low		Baja SMTemp3 Temp6	Ba SMTemp3 T6
2118		32		15		SMTemp3 Temp7 High 2	SMTemp3 T7 Hi2		Alta2 SMTemp3 Temp7	Al2 SMTemp3 T7
2119		32		15		SMTemp3 Temp7 High 1	SMTemp3 T7 Hi1		Alta1 SMTemp3 Temp7	Al1 SMTemp3 T7
2120		32		15		SMTemp3 Temp7 Low	SMTemp3 T7 Low		Baja SMTemp3 Temp7	Ba SMTemp3 T7
2121		32		15		SMTemp3 Temp8 High 2	SMTemp3 T8 Hi2		Alta2 SMTemp3 Temp8	Al2 SMTemp3 T8
2122		32		15		SMTemp3 Temp8 High 1	SMTemp3 T8 Hi1		Alta1 SMTemp3 Temp8	Al1 SMTemp3 T8
2123		32		15		SMTemp3 Temp8 Low	SMTemp3 T8 Low		Baja SMTemp3 Temp8	Ba SMTemp3 T8
2124		32		15		SMTemp4 Temp1 High 2	SMTemp4 T1 Hi2		Alta2 SMTemp4 Temp1	Al2 SMTemp4 T1
2125		32		15		SMTemp4 Temp1 High 1	SMTemp4 T1 Hi1		Alta1 SMTemp4 Temp1	Al1 SMTemp4 T1
2126		32		15		SMTemp4 Temp1 Low	SMTemp4 T1 Low		Baja SMTemp4 Temp1	Ba SMTemp4 T1
2127		32		15		SMTemp4 Temp2 High 2	SMTemp4 T2 Hi2		Alta2 SMTemp4 Temp2	Al2 SMTemp4 T2
2128		32		15		SMTemp4 Temp2 High 1	SMTemp4 T2 Hi1		Alta1 SMTemp4 Temp2	Al1 SMTemp4 T2
2129		32		15		SMTemp4 Temp2 Low	SMTemp4 T2 Low		Baja SMTemp4 Temp2	Ba SMTemp4 T2
2130		32		15		SMTemp4 Temp3 High 2	SMTemp4 T3 Hi2		Alta2 SMTemp4 Temp3	Al2 SMTemp4 T3	
2131		32		15		SMTemp4 Temp3 High 1	SMTemp4 T3 Hi1		Alta1 SMTemp4 Temp3	Al1 SMTemp4 T3
2132		32		15		SMTemp4 Temp3 Low	SMTemp4 T3 Low		Baja SMTemp4 Temp3	Ba SMTemp4 T3
2133		32		15		SMTemp4 Temp4 High 2	SMTemp4 T4 Hi2		Alta2 SMTemp4 Temp4	Al2 SMTemp4 T4
2134		32		15		SMTemp4 Temp4 High 1	SMTemp4 T4 Hi1		Alta1 SMTemp4 Temp4	Al1 SMTemp4 T4
2135		32		15		SMTemp4 Temp4 Low	SMTemp4 T4 Low		Baja SMTemp4 Temp4	Ba SMTemp4 T4
2136		32		15		SMTemp4 Temp5 High 2	SMTemp4 T5 Hi2		Alta2 SMTemp4 Temp5	Al2 SMTemp4 T5
2137		32		15		SMTemp4 Temp5 High 1	SMTemp4 T5 Hi1		Alta1 SMTemp4 Temp5	Al1 SMTemp4 T5
2138		32		15		SMTemp4 Temp5 Low	SMTemp4 T5 Low		Baja SMTemp4 Temp5	Ba SMTemp4 T5
2139		32		15		SMTemp4 Temp6 High 2	SMTemp4 T6 Hi2		Alta2 SMTemp4 Temp6	Al2 SMTemp4 T6
2140		32		15		SMTemp4 Temp6 High 1	SMTemp4 T6 Hi1		Alta1 SMTemp4 Temp6	Al1 SMTemp4 T6
2141		32		15		SMTemp4 Temp6 Low	SMTemp4 T6 Low		Baja SMTemp4 Temp6	Ba SMTemp4 T6	
2142		32		15		SMTemp4 Temp7 High 2	SMTemp4 T7 Hi2		Alta2 SMTemp4 Temp7	Al2 SMTemp4 T7
2143		32		15		SMTemp4 Temp7 High 1	SMTemp4 T7 Hi1		Alta1 SMTemp4 Temp7	Al1 SMTemp4 T7
2144		32		15		SMTemp4 Temp7 Low	SMTemp4 T7 Low		Baja SMTemp4 Temp7	Ba SMTemp4 T7
2145		32		15		SMTemp4 Temp8 High 2	SMTemp4 T8 Hi2		Alta2 SMTemp4 Temp8	Al2 SMTemp4 T8
2146		32		15		SMTemp4 Temp8 Hi1	SMTemp4 T8 Hi1		Alta1 SMTemp4 Temp8	Al1 SMTemp4 T8
2147		32		15		SMTemp4 Temp8 Low	SMTemp4 T8 Low		Baja SMTemp4 Temp8	Ba SMTemp4 T8	
2148		32		15		SMTemp5 Temp1 High 2	SMTemp5 T1 Hi2		Alta2 SMTemp5 Temp1	Al2 SMTemp5 T1
2149		32		15		SMTemp5 Temp1 High 1	SMTemp5 T1 Hi1		Alta1 SMTemp5 Temp1	Al1 SMTemp5 T1
2150		32		15		SMTemp5 Temp1 Low	SMTemp5 T1 Low		Baja SMTemp5 Temp1	Ba SMTemp5 T1
2151		32		15		SMTemp5 Temp2 High 2	SMTemp5 T2 Hi2		Alta2 SMTemp5 Temp2	Al2 SMTemp5 T2
2152		32		15		SMTemp5 Temp2 High 1	SMTemp5 T2 Hi1		Alta1 SMTemp5 Temp2	Al1 SMTemp5 T2
2153		32		15		SMTemp5 Temp2 Low	SMTemp5 T2 Low		Baja SMTemp5 Temp2	Ba SMTemp5 T2
2154		32		15		SMTemp5 Temp3 High 2	SMTemp5 T3 Hi2		Alta2 SMTemp5 Temp3	Al2 SMTemp5 T3
2155		32		15		SMTemp5 Temp3 High 1	SMTemp5 T3 Hi1		Alta1 SMTemp5 Temp3	Al1 SMTemp5 T3
2156		32		15		SMTemp5 Temp3 Low	SMTemp5 T3 Low		Baja SMTemp5 Temp3	Ba SMTemp5 T3
2157		32		15		SMTemp5 Temp4 High 2	SMTemp5 T4 Hi2		Alta2 SMTemp5 Temp4	Al2 SMTemp5 T4
2158		32		15		SMTemp5 Temp4 High 1	SMTemp5 T4 Hi1		Alta1 SMTemp5 Temp4	Al1 SMTemp5 T4
2159		32		15		SMTemp5 Temp4 Low	SMTemp5 T4 Low		Baja SMTemp5 Temp4	Ba SMTemp5 T4
2160		32		15		SMTemp5 Temp5 High 2	SMTemp5 T5 Hi2		Alta2 SMTemp5 Temp5	Al2 SMTemp5 T5
2161		32		15		SMTemp5 Temp5 High 1	SMTemp5 T5 Hi1		Alta1 SMTemp5 Temp5	Al1 SMTemp5 T5
2162		32		15		SMTemp5 Temp5 Low	SMTemp5 T5 Low		Baja SMTemp5 Temp5	Ba SMTemp5 T5
2163		32		15		SMTemp5 Temp6 High 2	SMTemp5 T6 Hi2		Alta2 SMTemp5 Temp6	Al2 SMTemp5 T6
2164		32		15		SMTemp5 Temp6 High 1	SMTemp5 T6 Hi1		Alta1 SMTemp5 Temp6	Al1 SMTemp5 T6
2165		32		15		SMTemp5 Temp6 Low	SMTemp5 T6 Low		Baja SMTemp5 Temp6	Ba SMTemp5 T6
2166		32		15		SMTemp5 Temp7 High 2	SMTemp5 T7 Hi2		Alta2 SMTemp5 Temp7	Al2 SMTemp5 T7
2167		32		15		SMTemp5 Temp7 High 1	SMTemp5 T7 Hi1		Alta1 SMTemp5 Temp7	Al1 SMTemp5 T7
2168		32		15		SMTemp5 Temp7 Low	SMTemp5 T7 Low		Baja SMTemp5 Temp7	Ba SMTemp5 T7
2169		32		15		SMTemp5 Temp8 High 2	SMTemp5 T8 Hi2		Alta2 SMTemp5 Temp8	Al2 SMTemp5 T8
2170		32		15		SMTemp5 Temp8 High 1	SMTemp5 T8 Hi1		Alta1 SMTemp5 Temp8	Al1 SMTemp5 T8
2171		32		15		SMTemp5 Temp8 Low	SMTemp5 T8 Low		Baja SMTemp5 Temp8	Ba SMTemp5 T8
2172		32		15		SMTemp6 Temp1 High 2	SMTemp6 T1 Hi2		Alta2 SMTemp6 Temp1	Al2 SMTemp6 T1
2173		32		15		SMTemp6 Temp1 High 1	SMTemp6 T1 Hi1		Alta1 SMTemp6 Temp1	Al1 SMTemp6 T1
2174		32		15		SMTemp6 Temp1 Low	SMTemp6 T1 Low		Baja SMTemp6 Temp1	Ba SMTemp6 T1
2175		32		15		SMTemp6 Temp2 High 2	SMTemp6 T2 Hi2		Alta2 SMTemp6 Temp2	Al2 SMTemp6 T2
2176		32		15		SMTemp6 Temp2 High 1	SMTemp6 T2 Hi1		Alta1 SMTemp6 Temp2	Al1 SMTemp6 T2
2177		32		15		SMTemp6 Temp2 Low	SMTemp6 T2 Low		Baja SMTemp6 Temp2	Ba SMTemp6 T2
2178		32		15		SMTemp6 Temp3 High 2	SMTemp6 T3 Hi2		Alta2 SMTemp6 Temp3	Al2 SMTemp6 T3
2179		32		15		SMTemp6 Temp3 High 1	SMTemp6 T3 Hi1		Alta1 SMTemp6 Temp3	Al1 SMTemp6 T3
2180		32		15		SMTemp6 Temp3 Low	SMTemp6 T3 Low		Baja SMTemp6 Temp3	Ba SMTemp6 T3
2181		32		15		SMTemp6 Temp4 High 2	SMTemp6 T4 Hi2		Alta2 SMTemp6 Temp4	Al2 SMTemp6 T4
2182		32		15		SMTemp6 Temp4 High 1	SMTemp6 T4 Hi1		Alta1 SMTemp6 Temp4	Al1 SMTemp6 T4
2183		32		15		SMTemp6 Temp4 Low	SMTemp6 T4 Low		Baja SMTemp6 Temp4	Ba SMTemp6 T4
2184		32		15		SMTemp6 Temp5 High 2	SMTemp6 T5 Hi2		Alta2 SMTemp6 Temp5	Al2 SMTemp6 T5
2185		32		15		SMTemp6 Temp5 High 1	SMTemp6 T5 Hi1		Alta1 SMTemp6 Temp5	Al1 SMTemp6 T5
2186		32		15		SMTemp6 Temp5 Low	SMTemp6 T5 Low		Baja SMTemp6 Temp5	Ba SMTemp6 T5
2187		32		15		SMTemp6 Temp6 High 2	SMTemp6 T6 Hi2		Alta2 SMTemp6 Temp6	Al2 SMTemp6 T6
2188		32		15		SMTemp6 Temp6 High 1	SMTemp6 T6 Hi1		Alta1 SMTemp6 Temp6	Al1 SMTemp6 T6
2189		32		15		SMTemp6 Temp6 Low	SMTemp6 T6 Low		Baja SMTemp6 Temp6	Ba SMTemp6 T6
2190		32		15		SMTemp6 Temp7 High 2	SMTemp6 T7 Hi2		Alta2 SMTemp6 Temp7	Al2 SMTemp6 T7
2191		32		15		SMTemp6 Temp7 High 1	SMTemp6 T7 Hi1		Alta1 SMTemp6 Temp7	Al1 SMTemp6 T7
2192		32		15		SMTemp6 Temp7 Low	SMTemp6 T7 Low		Baja SMTemp6 Temp7	Ba SMTemp6 T7
2193		32		15		SMTemp6 Temp8 High 2	SMTemp6 T8 Hi2		Alta2 SMTemp6 Temp8	Al2 SMTemp6 T8
2194		32		15		SMTemp6 Temp8 High 1	SMTemp6 T8 Hi1		Alta1 SMTemp6 Temp8	Al1 SMTemp6 T8
2195		32		15		SMTemp6 Temp8 Low	SMTemp6 T8 Low		Baja SMTemp6 Temp8	Ba SMTemp6 T8
2196		32		15		SMTemp7 Temp1 High 2	SMTemp7 T1 Hi2		Alta2 SMTemp7 Temp1	Al2 SMTemp7 T1
2197		32		15		SMTemp7 Temp1 High 1	SMTemp7 T1 Hi1		Alta1 SMTemp7 Temp1	Al1 SMTemp7 T1
2198		32		15		SMTemp7 Temp1 Low	SMTemp7 T1 Low		Baja SMTemp7 Temp1	Ba SMTemp7 T1
2199		32		15		SMTemp7 Temp2 High 2	SMTemp7 T2 Hi2		Alta2 SMTemp7 Temp2	Al2 SMTemp7 T2
2200		32		15		SMTemp7 Temp2 High 1	SMTemp7 T2 Hi1		Alta1 SMTemp7 Temp2	Al1 SMTemp7 T2
2201		32		15		SMTemp7 Temp2 Low	SMTemp7 T2 Low		Baja SMTemp7 Temp2	Ba SMTemp7 T2
2202		32		15		SMTemp7 Temp3 High 2	SMTemp7 T3 Hi2		Alta2 SMTemp7 Temp3	Al2 SMTemp7 T3
2203		32		15		SMTemp7 Temp3 High 1	SMTemp7 T3 Hi1		Alta1 SMTemp7 Temp3	Al1 SMTemp7 T3
2204		32		15		SMTemp7 Temp3 Low	SMTemp7 T3 Low		Baja SMTemp7 Temp3	Ba SMTemp7 T3
2205		32		15		SMTemp7 Temp4 High 2	SMTemp7 T4 Hi2		Alta2 SMTemp7 Temp4	Al2 SMTemp7 T4
2206		32		15		SMTemp7 Temp4 High 1	SMTemp7 T4 Hi1		Alta1 SMTemp7 Temp4	Al1 SMTemp7 T4
2207		32		15		SMTemp7 Temp4 Low	SMTemp7 T4 Low		Baja SMTemp7 Temp4	Ba SMTemp7 T4
2208		32		15		SMTemp7 Temp5 High 2	SMTemp7 T5 Hi2		Alta2 SMTemp7 Temp5	Al2 SMTemp7 T5
2209		32		15		SMTemp7 Temp5 High 1	SMTemp7 T5 Hi1		Alta1 SMTemp7 Temp5	Al1 SMTemp7 T5
2210		32		15		SMTemp7 Temp5 Low	SMTemp7 T5 Low		Baja SMTemp7 Temp5	Ba SMTemp7 T5
2211		32		15		SMTemp7 Temp6 High 2	SMTemp7 T6 Hi2		Alta2 SMTemp7 Temp6	Al2 SMTemp7 T6
2212		32		15		SMTemp7 Temp6 High 1	SMTemp7 T6 Hi1		Alta1 SMTemp7 Temp6	Al1 SMTemp7 T6
2213		32		15		SMTemp7 Temp6 Low	SMTemp7 T6 Low		Baja SMTemp7 Temp6	Ba SMTemp7 T6
2214		32		15		SMTemp7 Temp7 High 2	SMTemp7 T7 Hi2		Alta2 SMTemp7 Temp7	Al2 SMTemp7 T7
2215		32		15		SMTemp7 Temp7 High 1	SMTemp7 T7 Hi1		Alta1 SMTemp7 Temp7	Al1 SMTemp7 T7
2216		32		15		SMTemp7 Temp7 Low	SMTemp7 T7 Low		Baja SMTemp7 Temp7	Ba SMTemp7 T7
2217		32		15		SMTemp7 Temp8 High 2	SMTemp7 T8 Hi2		Alta2 SMTemp7 Temp8	Al2 SMTemp7 T8
2218		32		15		SMTemp7 Temp8 High 1	SMTemp7 T8 Hi1		Alta1 SMTemp7 Temp8	Al1 SMTemp7 T8
2219		32		15		SMTemp7 Temp8 Low	SMTemp7 T8 Low		Baja SMTemp7 Temp8	Ba SMTemp7 T8
2220		32		15		SMTemp8 Temp1 High 2	SMTemp8 T1 Hi2		Alta2 SMTemp8 Temp1	Al2 SMTemp8 T1
2221		32		15		SMTemp8 Temp1 High 1	SMTemp8 T1 Hi1		Alta1 SMTemp8 Temp1	Al1 SMTemp8 T1
2222		32		15		SMTemp8 Temp1 Low	SMTemp8 T1 Low		Baja SMTemp8 Temp1	Ba SMTemp8 T1
2223		32		15		SMTemp8 Temp2 High 2	SMTemp8 T2 Hi2		Alta2 SMTemp8 Temp2	Al2 SMTemp8 T2
2224		32		15		SMTemp8 Temp2 High 1	SMTemp8 T2 Hi1		Alta1 SMTemp8 Temp2	Al1 SMTemp8 T2
2225		32		15		SMTemp8 Temp2 Low	SMTemp8 T2 Low		Baja SMTemp8 Temp2	Ba SMTemp8 T2
2226		32		15		SMTemp8 Temp3 High 2	SMTemp8 T3 Hi2		Alta2 SMTemp8 Temp3	Al2 SMTemp8 T3
2227		32		15		SMTemp8 Temp3 High 1	SMTemp8 T3 Hi1		Alta1 SMTemp8 Temp3	Al1 SMTemp8 T3
2228		32		15		SMTemp8 Temp3 Low	SMTemp8 T3 Low		Baja SMTemp8 Temp3	Ba SMTemp8 T3
2229		32		15		SMTemp8 Temp4 High 2	SMTemp8 T4 Hi2		Alta2 SMTemp8 Temp4	Al2 SMTemp8 T4
2230		32		15		SMTemp8 Temp4 High 1	SMTemp8 T4 Hi1		Alta1 SMTemp8 Temp4	Al1 SMTemp8 T4
2231		32		15		SMTemp8 Temp4 Low	SMTemp8 T4 Low		Baja SMTemp8 Temp4	Ba SMTemp8 T4
2232		32		15		SMTemp8 Temp5 High 2	SMTemp8 T5 Hi2		Alta2 SMTemp8 Temp5	Al2 SMTemp8 T5
2233		32		15		SMTemp8 Temp5 High 1	SMTemp8 T5 Hi1		Alta1 SMTemp8 Temp5	Al1 SMTemp8 T5
2234		32		15		SMTemp8 Temp5 Low	SMTemp8 T5 Low		Baja SMTemp8 Temp5	Ba SMTemp8 T5
2235		32		15		SMTemp8 Temp6 High 2	SMTemp8 T6 Hi2		Alta2 SMTemp8 Temp6	Al2 SMTemp8 T6
2236		32		15		SMTemp8 Temp6 High 1	SMTemp8 T6 Hi1		Alta1 SMTemp8 Temp6	Al1 SMTemp8 T6
2237		32		15		SMTemp8 Temp6 Low	SMTemp8 T6 Low		Baja SMTemp8 Temp6	Ba SMTemp8 T6
2238		32		15		SMTemp8 Temp7 High 2	SMTemp8 T7 Hi2		Alta2 SMTemp8 Temp7	Al2 SMTemp8 T7
2239		32		15		SMTemp8 Temp7 High 1	SMTemp8 T7 Hi1		Alta1 SMTemp8 Temp7	Al1 SMTemp8 T7
2240		32		15		SMTemp8 Temp7 Low	SMTemp8 T7 Low		Baja SMTemp8 Temp7	Ba SMTemp8 T7
2241		32		15		SMTemp8 Temp8 High 2	SMTemp8 T8 Hi2		Alta2 SMTemp8 Temp8	Al2 SMTemp8 T8
2242		32		15		SMTemp8 Temp8 High 1	SMTemp8 T8 Hi1		Alta1 SMTemp8 Temp8	Al1 SMTemp8 T8
2243		32		15		SMTemp8 Temp8 Low	SMTemp8 T8 Low		Baja SMTemp8 Temp8	Ba SMTemp8 T8
2244	32		15		None				None			Ninguno				Ninguno
2245	32		15		High Load Level1		HighLoadLevel1		Nivel Alta carga 1		Alta Carga 1
2246	32		15		High Load Level2		HighLoadLevel2		Nivel Alta carga 2		Alta Carga 2
2247	32		15		Maximum				Maximum			Máxima				Máxima
2248	32		15		Average				Average			Media				Media

2249		32		15		Solar Mode		Solar Mode		Modo Solar		Modo Solar
2250		32		15		Running Way(For Solar)	Running Way		Running Way(For Solar)	Running Way
2251		32		15		Disabled			Disabled		Deshabilitado			Deshabilitado
2252		32		15		RECT-SOLAR		RECT-SOLAR		RECT-SOLAR		RECT-SOLAR
2253		32		15		SOLAR			SOLAR			SOLAR			SOLAR
2254		32		15		RECT First		RECT First		RECT First		RECT First
2255		32		15		Solar First		Solar First	SOLAR First		SOLAR First
2256		32		15		Please reboot after changing			Please reboot		Please reboot after changing			Please reboot
2257		32		15		CSU Failure			CSU Failure		CSU Fail		CSU Fail	
2258		32		15		SSL and SNMPV3		SSL and SNMPV3		SSL y SNMPV3		SSL y SNMPV3	
2259		32		15		Adjust Bus Input Voltage	Adjust In-Volt		Ajuste Tensi¨®n Bus Entrada	Ajustar V-Ent	
2260		32		15		Confirm Voltage Supplied	Confirm Voltage		Confirmar Tensi¨®n		Confirm V	
2261		32		15			Converter Only				Converter Only		S¨®lo Convertidores			S¨®lo Conver
2262		32		15		Net-Port Reset Interval		Reset Interval		Intervalo reinicio Net-Port		Intervalo Reset
2263		32		15		DC1 Load			DC1 Load		Carga CC1		Carga CC1

2264	32		15		NCU DI1				NCU DI1				NCU DI1					NCU DI1
2265	32		15		NCU DI2				NCU DI2				NCU DI2					NCU DI2
2266	32		15		NCU DI3				NCU DI3				NCU DI3					NCU DI3
2267	32		15		NCU DI4				NCU DI4				NCU DI4					NCU DI4
2268	32			15		Digital Input 1		DI 1		Entrada Digital DI1			DI 1
2269	32			15		Digital Input 2		DI 2		Entrada Digital DI2			DI 2
2270	32			15		Digital Input 3		DI 3		Entrada Digital DI3			DI 3
2271	32			15		Digital Input 4		DI 4		Entrada Digital DI4			DI 4
2272	32			15		DI1 Alarm State		DI1 Alm State		Estado Alarma DI1		Alarma DI1
2273	32			15		DI2 Alarm State		DI2 Alm State		Estado Alarma DI2		Alarma DI2
2274	32			15		DI3 Alarm State		DI3 Alm State		Estado Alarma DI3		Alarma DI3
2275	32			15		DI4 Alarm State		DI4 Alm State		Estado Alarma DI4		Alarma DI4
2276	32			15		DI1 Alarm			DI1 Alarm			Alarma DI1			Alarma DI1
2277	32			15		DI2 Alarm			DI2 Alarm			Alarma DI2			Alarma DI2
2278	32			15		DI3 Alarm			DI3 Alarm			Alarma DI3			Alarma DI3
2279	32			15		DI4 Alarm			DI4 Alarm			Alarma DI4			Alarma DI4
2280	32			15		None			None		No			No
2281	32			15		Exist			Exist		Existe			Existe
2282	32			15		IB01 State			IB01 State		Estado IB01			Estado IB01
2283	32		15		DO1-Relay Output			DO1-RelayOutput			DO1-Salida Relé		DO1-SalidaRelé
2284	32		15		DO2-Relay Output			DO2-RelayOutput			DO2-Salida Relé		DO2-SalidaRelé
2285	32		15		DO3-Relay Output			DO3-RelayOutput			DO3-Salida Relé		DO3-SalidaRelé
2286	32		15		DO4-Relay Output			DO4-RelayOutput			DO4-Salida Relé		DO4-SalidaRelé
2287	32		15		Time Display Format		Time Format		Formato Hora Pantalla		Formato Hora
2288	32		15		DD/MM/YYYY			DD/MM/YYYY		DD/MM/YYYY			DD/MM/YYYY
2289	32		15		MM/DD/YYYY			MM/DD/YYYY		MM/DD/YYYY			MM/DD/YYYY
2290	32		15		YYYY/MM/DD			YYYY/MM/DD		YYYY/MM/DD			YYYY/MM/DD
2291	32		15		Help				Help		Ayuda				Ayuda
2292	32		15		Current Protocol Type		Protocol	Tipo de Protocolo			Protocolo 
2293	32		15		EEM				EEM		EEM				EEM  
2294	32		15		YDN23				YDN23		YDN23				YDN23
2295	32		15		Modbus				ModBus		ModBus				ModBus
2296	32		15		SMS Alarm Level			SMS Alarm Level		Nivel Alarma SMS				Niv Alarma SMS
2297	32		15		Disabled			Disabled			Ninguna			Ninguna
2298	32		15		Observation		Observation		Observación	Observación
2299	32		15		Major			Major			Urgente		Urgente
2300	32		15		Critical		Critical		Crítica		Crítica
2301	64		64		SPD is not connected to system or SPD is broken.	SPD is not connected to system or SPD is broken.	SPD no conectado o averiado	Falta SPD
2302	32		15		Big Screen		Big Screen		Pantalla Grande			Pantalla Grande
2303	32		15		EMAIL Alarm Level			EMAIL Alm Level		Nivel Alarma e-mail				Niv Alm e-mail
2304	32		15		HLMS Protocol Type	Protocol Type		Tipo de Protocolo		Tipo Protocolo
2305	32		15		EEM			EEM			EEM			EEM
2306	32		15		YDN23			YDN23			YDN23			YDN23
2307	32		15		Modbus			Modbus			Modbus			Modbus
2308		32		15		24V Display State		24V Display		Estado Display 24V		Display 24V
2309		32		15		Syst Volt Level				Syst Volt Level				Tensión Sistema				Tensión Sistema
2310		32		15		48 V System				48 V System				Sistema 48V					Sistema 48V
2311		32		15		24 V System				24 V System				Sistema 24V					Sistema 24V
2312		32		15		Power Input Type			Input Type			Tipo Alimentación				Tipo Alimentaci
2313		32		15		AC Input				AC Input				Entrada CA			Entrada CA
2314		32		15		Diesel Input				Diesel Input				Entrada GE				Entrada GE
2315		32		15		2nd IB2 Temp1		2nd IB2 Temp1	Temp1 IB2 2		Temp1 IB2 2
2316		32		15		2nd IB2 Temp2		2nd IB2 Temp2	Temp2 IB2 2		Temp2 IB2 2
2317		32		15		EIB2 Temp1		EIB2 Temp1	Temp1 EIB2		Temp1 EIB2
2318		32		15		EIB2 Temp2		EIB2 Temp2	Temp2 EIB2		Temp2 EIB2
2319		32		15		2nd IB2 Temp1 High 2	2nd IB2 T1 Hi2	Temp1 Alta2 IB2 2	T1 Alta2 IB2-2
2320		32		15		2nd IB2 Temp1 High 1	2nd IB2 T1 Hi1	Temp1 Alta1 IB2 2	T1 Alta1 IB2-2
2321		32		15		2nd IB2 Temp1 Low	2nd IB2 T1 Low	Temp1 Baja IB2 2	T1 Baja IB2-2
2322		32		15		2nd IB2 Temp2 High 2	2nd IB2 T2 Hi2	Temp2 Alta2 IB2 2	T2 Alta2 IB2-2
2323		32		15		2nd IB2 Temp2 High 1	2nd IB2 T2 Hi1	Temp2 Alta1 IB2 2	T2 Alta1 IB2-2
2324		32		15		2nd IB2 Temp2 Low	2nd IB2 T2 Low	2Temp2 Baja IB2 2	T2 Baja IB2-2
2325		32		15		EIB2 Temp1 High 2	EIB2 T1 Hi2	Temp1 Alta2 EIB2	T1 Alta2 EIB2
2326		32		15		EIB2 Temp1 High 1	EIB2 T1 Hi1	Temp1 Alta1 EIB2	T1 Alta1 EIB2
2327		32		15		EIB2 Temp1 Low		EIB2 T1 Low	Temp1 Baja EIB2		T1 Baja EIB2
2328		32		15		EIB2 Temp2 High 2	EIB2 T2 Hi2	Temp2 Alta2 EIB2	T2 Alta2 EIB2
2329		32		15		EIB2 Temp2 High 1	EIB2 T2 Hi1	Temp2 Alta1 EIB2	T2 Alta1 EIB2
2330		32		15		EIB2 Temp2 Low		EIB2 T2 Low	Temp2 Baja EIB2		T2 Baja EIB2
2331	32		15		NCU DO1 Test			NCU DO1 Test		DO1 prueba		DO1 prueba
2332	32		15		NCU DO2 Test			NCU DO2 Test		DO2 prueba		DO2 prueba
2333	32		15		NCU DO3 Test			NCU DO3 Test		DO3 prueba		DO3 prueba
2334	32		15		NCU DO4 Test			NCU DO4 Test		DO4 prueba		DO4 prueba
2335	32	15		Total Output Rated			Total Output	Salida Total Estimada		Salida Est
2336	32	15		Load current capacity			Load capacity		Capacidad Carga			Capacidad Carga
2337	32		15		EES System Mode			EES System Mode		Modo Sistema EES		Modo Sist EES
2338	32		15		SMS Modem Fail		SMS Modem Fail		Fallo Modem SMS			Fallo Modem SMS
2339		32		15		SMDU-EIB Mode		SMDU-EIB Mode		Modo SMDU-EIB			Modo SMDU-EIB
2340		32		15		Load Switch		Load Switch		Disyuntor de Carga		Disyun Carga
2341		32		15		Manual State		Manual State		Estado Manual			Estado Manual
2342		32		15		Converter Voltage Level	Volt Level		Nivel Tensión Convertidor	Nivel Vconv
2343		32		15		CB Threshold  Value	Threshold Value		Umbral disyuntor		Umb disyuntor
2344		32		15		CB Overload  Value	Overload Value		Sobrecarga			Sobrecarga
2345		32		15		SNMP Config Error	SNMP Config Err		Error Config SNMP	Error Cfg SNMP
2346		32		15		Fiamm Battery		Fiamm Battery		Batería FIAMM	Batería FIAMM
2347		32		15		TL1			TL1			TL1		TL1
2348		32		15		TL1 Port Activation	Port Activation		Activación Puerto TL1	ActivPuerto TL1
2349		32		15		TL1 Protocol Media	Protocol Media		Medio protocolo TL1		MedioProtoc TL1
2350		32		15		TL1 Access Port		Access Port		Medio protocolo TL1			MedioProtoc TL1
2351		32		15		TL1 Port Keep Alive	Keep Alive		Puerto Vivo TL1	Puerto Vivo TL1
2352		32		15		TL1 Session Timeout	Session Timeout		Caducidad Sesión TL1	Expira Sessión
2353		32		15		IPV4			IPV4			IPV4		IPV4
2354		32		15		IPV6			IPV6			IPV6		IPV6
2355		32		15		Ambient Temp Summary Alarm	AmbTempSummAlm		Alm Suma Temp Ambiente		Alarma SumTAmb
2356		32		15		DHCP Enable		DHCP Enable		DHCP Activado		DHCP Activado

2357		32		15		Modbus Config Error	ModbusConfigErr		Error de configuración de Modbus		Error de Modbus
2358		32		15		TL1 AID Delimeter	AID Delimeter		Separador TL1 AID	Separador AID

2400	32		15		Not Active			Not Active		Apagado				Apagado
2401	32		15		Active				Active			Conectado			Conectado
2402		32		15		IB4 Communication Status		IB4 Comm Status		IB4 Estado de comunicación		IB4 EstadoCom
2403		32		15		IB4 Communication Fail			IB4 Comm Fail		IB4 Fallo comunicación		IB4 Fallo Com	
2406	32		15		IB2-2 Temp1		IB2-2 T1		IB2-2Temperatura1		IB2-2T1
2407	32		15		IB2-2 Temp2		IB2-2 T2		IB2-2Temperatura2		IB2-2T2
2408	32		15		EIB-2 Temp1		EIB-2 T1		EIB-2Temperatura1		EIB-2T1
2409	32		15		EIB-2 Temp2		EIB-2 T2		EIB-2Temperatura2		EIB-2T2

2410		32		15		IB2-2 Temp1 High 2	IB2-2 T1 Hi2	Alta2 IB2-1-Temp1		Alta2 IB2-2-T1
2411		32		15		IB2-2 Temp1 High 1	IB2-2 T1 Hi1	Alta1 IB2-1-Temp 1		Alta1 IB2-2-T1
2412		32		15		IB2-2 Temp1 Low		IB2-2 T1 Low	Baja IB2-1-Temp 1		Baja IB2-2-T1
2413		32		15		IB2-2 Temp2 High 2	IB2-2 T2 Hi2	Alta2 IB2-1-Temp2		Alta2 IB2-2-T2
2414		32		15		IB2-2 Temp2 High 1	IB2-2 T2 Hi1	Alta1 IB2-1-Temp 2		Alta1 IB2-2-T2
2415		32		15		IB2-2 Temp2 Low		IB2-2 T2 Low	Baja IB2-1-Temp 2		Baja IB2-2-T2
2416		32		15		EIB-2 Temp1 High 2	EIB-2 T1 Hi2	Alta2 EIB-1-Temp1		Alta2 EIB-2-T1
2417		32		15		EIB-2 Temp1 High 1	EIB-2 T1 Hi1	Alta1 EIB-1-Temp 1		Alta1 EIB-2-T1
2418		32		15		EIB-2 Temp1 Low		EIB-2 T1 Low	Baja EIB-1-Temp 1		Baja EIB-2-T1
2419		32		15		EIB-2 Temp2 High 2	EIB-2 T2 Hi2	Alta2 EIB-1-Temp2		Alta2 EIB-2-T2
2420		32		15		EIB-2 Temp2 High 1	EIB-2 T2 Hi1	Alta1 EIB-1-Temp 2		Alta1 EIB-2-T2
2421		32		15		EIB-2 Temp2 Low		EIB-2 T2 Low	Baja EIB-1-Temp 2		Baja EIB-2-T2

2422	32		15		IB2-2 Temp 1			IB2-2 Temp1		IB2-2 Temperatura 1		IB2-2 T1
2423	32		15		IB2-2 Temp 1			IB2-2 Temp1		IB2-2 Temp1			IB2-2 Temp1
2427	32		15		IB2-2 Temp 1 Not Used		IB2-2T1NotUsed		IB2-2 Temp1 no utilizada	IB2-2T1NoUsada
2428	32		15		IB2-2 Temp 1 Sensor Fail	IB2-2T1SensFail		IB2-2 Fallo sensor EIB-Temp1	Fallo IB2-2T1
2432	32		15		IB2-2 Temp 2			IB2-2 Temp2		IB2-2 Temperatura 2		IB2-2 T2
2433	32		15		IB2-2 Temp 2			IB2-2 Temp2		IB2-2 Temp2			IB2-2 Temp2
2437	32		15		IB2-2 Temp 2 Not Used		IB2-2T2NotUsed		IB2-2 Temp2 no utilizada	IB2-2T2NoUsada
2438	32		15		IB2-2 Temp 2 Sensor Fail	IB2-2T2SensFail		IB2-2 Fallo sensor EIB-Temp2	Fallo IB2-2T2
2442	32		15		EIB-2 Temp 1			EIB-2 Temp1		EIB-2 Temperatura 1		EIB-2 T1
2443	32		15		EIB-2 Temp 1			EIB-2 Temp1		EIB-2 Temp1			EIB-2 Temp1
2447	32		15		EIB-2 Temp 1 Not Used		EIB-2T1NotUsed		EIB-2 Temp1 no utilizada	EIB-T1NoUsada
2448	32		15		EIB-2 Temp 1 Sensor Fail	EIB-2T1SensFail		EIB-2 Fallo sensor EIB-Temp1	Fallo EIB-2T1
2452	32		15		EIB-2 Temp 2			EIB-2 Temp2		EIB-2 Temperatura 2		EIB-2 T2
2453	32		15		EIB-2 Temp 2			EIB-2 Temp2		EIB-2 Temp2			EIB-2 Temp2
2457	32		15		EIB-2 Temp 2 Not Used		EIB-2T2NotUsed		EIB-2 Temp2 no utilizada	EIB-2T2NoUsada
2458	32		15		EIB-2 Temp 2 Sensor Fail	EIB-2T2SensFail		EIB-2 Fallo sensor EIB-Temp2	Fallo EIB-2T2

2480	32		15		Temperature Format		Temp Format		Formato de Temperatura		FormatoTemp	
2481	32		15		Celsius				Celsius			Celsius				Celsius			
2482	32		15		Fahrenheit			Fahrenheit		Fahrenheit			Fahrenheit		
2483	32		15		System Alarm Function		Sys Alarm Func		Función alarma sistema		FuncAlarmaSis
2484	32		15		CR Only				CR Only			solamente CR			solamente CR
2485	32		15		CR and MJ			CR and MJ		CR y MJ		CR y MJ
2486	32		15		CR, MJ, and OB			CR, MJ, and OB		CR,MJ y OB		CR,MJ y OB
2487	32		15		Verizon Display			Verizon Display		Verizon Monitor		Verizon Monitor	
2488	32		15		Normal Mode			Normal Mode		Modo Normal		Modo Normal	
2489	32		15		Verizon Mode			Verizon Mode		Modo Verizon		Modo Verizon	
	
2500	32		15		Source Current		Source Current		Corriente de origen		CorrOrigen
2501	32		15		Source Current Number	SourceCurrNum		Fuente Número actual		FuNúmActual
2502	32		15		Source Rated Current	SourceRatedCurr		Corriente nominal de origen	CorrNomOri
2503	32		15		NTP Function Enable	NTPFuncEnable		Habilitar la Función NTP	HabilFuncNTP

2510	32		15		Alarm Sound Activation	Alm Sound Act	告警音激活		告警音激活
2511	32		15		Close Alarm Sound	Close Alm Sound		关告警音		关告警音
2512	32		15		Close				Close		关闭			关闭
2513	32		15		Open				Open		开启			开启
2521	32		15		SMDUE1 Temp1		SMDUE1 T1		SMDUE1-T1			SMDUE1-T1
2522	32		15		SMDUE1 Temp2		SMDUE1 T2		SMDUE1-T2			SMDUE1-T2
2523	32		15		SMDUE1 Temp3		SMDUE1 T3		SMDUE1-T3			SMDUE1-T3
2524	32		15		SMDUE1 Temp4		SMDUE1 T4		SMDUE1-T4			SMDUE1-T4
2525	32		15		SMDUE1 Temp5		SMDUE1 T5		SMDUE1-T5			SMDUE1-T5
2526	32		15		SMDUE1 Temp6		SMDUE1 T6		SMDUE1-T6			SMDUE1-T6
2527	32		15		SMDUE1 Temp7		SMDUE1 T7		SMDUE1-T7			SMDUE1-T7
2528	32		15		SMDUE1 Temp8		SMDUE1 T8		SMDUE1-T8			SMDUE1-T8
2529	32		15		SMDUE1 Temp9		SMDUE1 T9		SMDUE1-T9			SMDUE1-T9
2530	32		15		SMDUE1 Temp10		SMDUE1 T10	SMDUE1-T10		SMDUE1-T10
2531	32		15		SMDUE2 Temp1		SMDUE2 T1		SMDUE2-T1			SMDUE2-T1
2532	32		15		SMDUE2 Temp2		SMDUE2 T2		SMDUE2-T2			SMDUE2-T2
2533	32		15		SMDUE2 Temp3		SMDUE2 T3		SMDUE2-T3			SMDUE2-T3
2534	32		15		SMDUE2 Temp4		SMDUE2 T4		SMDUE2-T4			SMDUE2-T4
2535	32		15		SMDUE2 Temp5		SMDUE2 T5		SMDUE2-T5			SMDUE2-T5
2536	32		15		SMDUE2 Temp6		SMDUE2 T6		SMDUE2-T6			SMDUE2-T6
2537	32		15		SMDUE2 Temp7		SMDUE2 T7		SMDUE2-T7			SMDUE2-T7
2538	32		15		SMDUE2 Temp8		SMDUE2 T8		SMDUE2-T8			SMDUE2-T8
2539	32		15		SMDUE2 Temp9		SMDUE2 T9		SMDUE2-T9			SMDUE2-T9
2540	32		15		SMDUE2 Temp10		SMDUE2 T10	SMDUE2-T10		SMDUE2-T10
2541		32		15		SMDUE1 Temp1 High 2	SMDUE1 T1 Hi2		Alta2 SMDUE1 Temp1		Al2 SMDUE1 T1
2542		32		15		SMDUE1 Temp1 High 1	SMDUE1 T1 Hi1		Alta1 SMDUE1 Temp1		Al1 SMDUE1 T1
2543		32		15		SMDUE1 Temp1 Low		SMDUE1 T1 Low		Baja SMDUE1 Temp1		Ba SMDUE1 T1
2544		32		15		SMDUE1 Temp2 High 2	SMDUE1 T2 Hi2		Alta2 SMDUE1 Temp2		Al2 SMDUE1 T2
2545		32		15		SMDUE1 Temp2 High 1	SMDUE1 T2 Hi1		Alta1 SMDUE1 Temp2		Al1 SMDUE1 T2
2546		32		15		SMDUE1 Temp2 Low		SMDUE1 T2 Low		Baja SMDUE1 Temp2		Ba SMDUE1 T2
2547		32		15		SMDUE1 Temp3 High 2	SMDUE1 T3 Hi2		Alta2 SMDUE1 Temp3		Al2 SMDUE1 T3
2548		32		15		SMDUE1 Temp3 High 1	SMDUE1 T3 Hi1		Alta1 SMDUE1 Temp3		Al1 SMDUE1 T3
2549		32		15		SMDUE1 Temp3 Low		SMDUE1 T3 Low		Baja SMDUE1 Temp3		Ba SMDUE1 T3
2550		32		15		SMDUE1 Temp4 High 2	SMDUE1 T4 Hi2		Alta2 SMDUE1 Temp4		Al2 SMDUE1 T4
2551		32		15		SMDUE1 Temp4 High 1	SMDUE1 T4 Hi1		Alta1 SMDUE1 Temp4		Al1 SMDUE1 T4
2552		32		15		SMDUE1 Temp4 Low		SMDUE1 T4 Low		Baja SMDUE1 Temp4		Ba SMDUE1 T4
2553		32		15		SMDUE1 Temp5 High 2	SMDUE1 T5 Hi2		Alta2 SMDUE1 Temp5		Al2 SMDUE1 T5
2554		32		15		SMDUE1 Temp5 High 1	SMDUE1 T5 Hi1		Alta1 SMDUE1 Temp5		Al1 SMDUE1 T5
2555		32		15		SMDUE1 Temp5 Low		SMDUE1 T5 Low		Baja SMDUE1 Temp5		Ba SMDUE1 T5
2556		32		15		SMDUE1 Temp6 High 2	SMDUE1 T6 Hi2		Alta2 SMDUE1 Temp6		Al2 SMDUE1 T6
2557		32		15		SMDUE1 Temp6 High 1	SMDUE1 T6 Hi1		Alta1 SMDUE1 Temp6		Al1 SMDUE1 T6
2558		32		15		SMDUE1 Temp6 Low		SMDUE1 T6 Low		Baja SMDUE1 Temp6		Ba SMDUE1 T6
2559		32		15		SMDUE1 Temp7 High 2	SMDUE1 T7 Hi2		Alta2 SMDUE1 Temp7		Al2 SMDUE1 T7
2560		32		15		SMDUE1 Temp7 High 1	SMDUE1 T7 Hi1		Alta1 SMDUE1 Temp7		Al1 SMDUE1 T7
2561		32		15		SMDUE1 Temp7 Low		SMDUE1 T7 Low		Baja SMDUE1 Temp7		Ba SMDUE1 T7
2562		32		15		SMDUE1 Temp8 High 2	SMDUE1 T8 Hi2		Alta2 SMDUE1 Temp8		Al2 SMDUE1 T8
2563		32		15		SMDUE1 Temp8 High 1	SMDUE1 T8 Hi1		Alta1 SMDUE1 Temp8		Al1 SMDUE1 T8
2564		32		15		SMDUE1 Temp8 Low		SMDUE1 T8 Low		Baja SMDUE1 Temp8		Ba SMDUE1 T8
2565		32		15		SMDUE1 Temp9 High 2	SMDUE1 T9 Hi2		Alta2 SMDUE1 Temp9		Al2 SMDUE1 T9
2566		32		15		SMDUE1 Temp9 High 1	SMDUE1 T9 Hi1		Alta1 SMDUE1 Temp9		Al1 SMDUE1 T9
2567		32		15		SMDUE1 Temp9 Low		SMDUE1 T9 Low		Baja SMDUE1 Temp9		Ba SMDUE1 T9
2568		32		15		SMDUE1 Temp10 High 2	SMDUE1 T10 Hi2		Alta2 SMDUE1 Temp10		Al2 SMDUE1 T10
2569		32		15		SMDUE1 Temp10 High 1	SMDUE1 T10 Hi1		Alta1 SMDUE1 Temp10		Al1 SMDUE1 T10
2570		32		15		SMDUE1 Temp10 Low	SMDUE1 T10 Low		Baja SMDUE1 Temp10		Ba SMDUE1 T10
2571		32		15		SMDUE2 Temp1 High 2	SMDUE2 T1 Hi2		Alta2 SMDUE2 Temp1		Al2 SMDUE2 T1
2572		32		15		SMDUE2 Temp1 High 1	SMDUE2 T1 Hi1		Alta1 SMDUE2 Temp1		Al1 SMDUE2 T1
2573		32		15		SMDUE2 Temp1 Low		SMDUE2 T1 Low		Baja SMDUE2 Temp1		Ba SMDUE2 T1
2574		32		15		SMDUE2 Temp2 High 2	SMDUE2 T2 Hi2		Alta2 SMDUE2 Temp2		Al2 SMDUE2 T2
2575		32		15		SMDUE2 Temp2 High 1	SMDUE2 T2 Hi1		Alta1 SMDUE2 Temp2		Al1 SMDUE2 T2
2576		32		15		SMDUE2 Temp2 Low		SMDUE2 T2 Low		Baja SMDUE2 Temp2		Ba SMDUE2 T2
2577		32		15		SMDUE2 Temp3 High 2	SMDUE2 T3 Hi2		Alta2 SMDUE2 Temp3		Al2 SMDUE2 T3
2578		32		15		SMDUE2 Temp3 High 1	SMDUE2 T3 Hi1		Alta1 SMDUE2 Temp3		Al1 SMDUE2 T3
2579		32		15		SMDUE2 Temp3 Low		SMDUE2 T3 Low		Baja SMDUE2 Temp3		Ba SMDUE2 T3
2580		32		15		SMDUE2 Temp4 High 2	SMDUE2 T4 Hi2		Alta2 SMDUE2 Temp4		Al2 SMDUE2 T4
2581		32		15		SMDUE2 Temp4 High 1	SMDUE2 T4 Hi1		Alta1 SMDUE2 Temp4		Al1 SMDUE2 T4
2582		32		15		SMDUE2 Temp4 Low		SMDUE2 T4 Low		Baja SMDUE2 Temp4		Ba SMDUE2 T4
2583		32		15		SMDUE2 Temp5 High 2	SMDUE2 T5 Hi2		Alta2 SMDUE2 Temp5		Al2 SMDUE2 T5
2584		32		15		SMDUE2 Temp5 High 1	SMDUE2 T5 Hi1		Alta1 SMDUE2 Temp5		Al1 SMDUE2 T5
2585		32		15		SMDUE2 Temp5 Low		SMDUE2 T5 Low		Baja SMDUE2 Temp5		Ba SMDUE2 T5
2586		32		15		SMDUE2 Temp6 High 2	SMDUE2 T6 Hi2		Alta2 SMDUE2 Temp6		Al2 SMDUE2 T6
2587		32		15		SMDUE2 Temp6 High 1	SMDUE2 T6 Hi1		Alta1 SMDUE2 Temp6		Al1 SMDUE2 T6
2588		32		15		SMDUE2 Temp6 Low		SMDUE2 T6 Low		Baja SMDUE2 Temp6		Ba SMDUE2 T6
2589		32		15		SMDUE2 Temp7 High 2	SMDUE2 T7 Hi2		Alta2 SMDUE2 Temp7		Al2 SMDUE2 T7
2590		32		15		SMDUE2 Temp7 High 1	SMDUE2 T7 Hi1		Alta1 SMDUE2 Temp7		Al1 SMDUE2 T7
2591		32		15		SMDUE2 Temp7 Low		SMDUE2 T7 Low		Baja SMDUE2 Temp7		Ba SMDUE2 T7
2592		32		15		SMDUE2 Temp8 High 2	SMDUE2 T8 Hi2		Alta2 SMDUE2 Temp8		Al2 SMDUE2 T8
2593		32		15		SMDUE2 Temp8 High 1	SMDUE2 T8 Hi1		Alta1 SMDUE2 Temp8		Al1 SMDUE2 T8
2594		32		15		SMDUE2 Temp8 Low		SMDUE2 T8 Low		Baja SMDUE2 Temp8		Ba SMDUE2 T8
2595		32		15		SMDUE2 Temp9 High 2	SMDUE2 T9 Hi2		Alta2 SMDUE2 Temp9		Al2 SMDUE2 T9
2596		32		15		SMDUE2 Temp9 High 1	SMDUE2 T9 Hi1		Alta1 SMDUE2 Temp9		Al1 SMDUE2 T9
2597		32		15		SMDUE2 Temp9 Low		SMDUE2 T9 Low		Baja SMDUE2 Temp9		Ba SMDUE2 T9
2598		32		15		SMDUE2 Temp10 High 2	SMDUE2 T10 Hi2		Alta2 SMDUE2 Temp10		Al2 SMDUE2 T10
2599		32		15		SMDUE2 Temp10 High 1	SMDUE2 T10 Hi1		Alta1 SMDUE2 Temp10		Al1 SMDUE2 T10
2600		32		15		SMDUE2 Temp10 Low	SMDUE2 T10 Low		Baja SMDUE2 Temp10		Ba SMDUE2 T10

2900		32		15		Virtual SMDUP Control	Vir SMDUP Ctl		Control SMDUP virtual		Ctl SMDUP vir
2901		32		15		Yes						Yes					sí					sí

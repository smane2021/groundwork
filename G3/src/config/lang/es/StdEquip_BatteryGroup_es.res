﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support           
# FULL_IN_LOCALE2: Full name in locale2 language 
# ABBR_IN_LOCALE2: Abbreviated locale2 name      
#
[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID	MAX_LEN_FULL	MAX_LEN_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32		15		Battery Voltage				Battery Voltage		Tensión Batería				Tensión Batería
2	32		15		Total Battery Current			Total Batt Curr		Corriente total a Batería		Corriente total
3	32		15		Battery Temperature			Battery Temp		Temperatura Batería			Temp Bat
4	32		15		Short BOD Total Time			Short BOD Time		Total en descarga superficial		Descarga leve
5	32		15		Long BOD Total Time			Long BOD Time		Total en descarga media			Descarga media
6	32		15		Full BOD Total Time			Full BOD Time		Total en descarga profunda		Descarg profund
7	32		15		Short BOD Counter			ShortBODCounter		Descargas superficiales			Descargas leves
8	32		15		Long BOD Counter			LongBODCounter		Descargas medias			Descarg medias
9	32		15		Full BOD Counter			FullBODCounter		Descargas profundas			Descg profundas
14	32		15		End Test on Voltage			End Test Volt		Final prueba por tensión		Fin tens prueba 
15	32		15		Discharge Current Imbalance		Dsch Curr Imb		Corriente descarga desequilib		Desq I descarga
19	32		15		Abnormal Battery Current		AbnormalBatCurr		Corriente batería anormal		I Bat anormal
21	32		15		System Current Limit Active		SystemCurrLimit	Limitando corriente a baterías		Lim corr bat
23	32		15		Equalize/Float Charge Control		EQ/FLT Control		Control carga/flotación			Ctrl carg/flot
25	32		15		Battery Test Control			BattTestControl		Control prueba baterías			Ctrl prueba
30	32		15		Number of Battery Blocks		Num Batt Blocks		Número de elementos de Batería		Elementos Bat
31	32		15		End Test Time				End Test Time		Tiempo fin de prueba			T fin prueba
32	32		15		End Test Voltage			End Test Volt		Tensión fin de prueba			V fin prueba
33	32		15		End Test Capacity			EndTestCapacity		Capacidad fin de prueba			Cap fin prueba
34	32		15		Constant Current Test			Const Curr Test		Prueba intensidad cte			Prueba I cte 
35	32		15		Constant Current Test Current		ConstCurrT Curr		Corriente prueba I constante		I prueba cte
37	32		15		AC Fail Test				AC Fail Test		Autoprueba en fallo de red		Prba Fallo Red
38	32		15		Short Test				Short Test		Prueba corta				Prueba corta
39	32		15		Short Test Cycle			ShortTest Cycle		Ciclo de prueba corta			Cicl prb corta
40	32		15		Short Test Max Difference Curr		Max Diff Curr		Max dif corriente prueba corta		Max dif corr
41	32		15		Short Test Time				Short Test Time		Duración prueba corta			T prba corta
42	32		15		Float Charge Voltage			Float Voltage		Tensión nominal				Tens nominal
43	32		15		Equalize Charge Voltage			EQ Voltage		Tensión de carga			Tensión carga
44	32		15		Maximum Equalize Charge Time		Maximum EQ Time		Tiempo máximo de carga			Max t carga
45	32		15		Equalize Stop Current			EQ Stop Curr		Corriente Final de Carga		I Final Carga
46	32		15		Equalize Stop Delay Time		EQ Stop Delay		Retardo Fin de Carga			Retard Fin Carg
47	32		15		Automatic Equalize			Auto EQ			Carga automática habilitada		Autocarga
48	32		15		Equalize Start Current			EQ Start Curr		Corriente inicio Carga			I autocarga
49	32		15		Equalize Start Capacity			EQ Start Cap		Capacidad inicio Carga			Cap autocarga
50	32		15		Cyclic Equalize				Cyc EQ		Carga cíclica habilitada		Carga cíclica
51	32		15		Cyclic Equalize Interval		Cyc EQ Interval		Intervalo carga cíclica			Ciclo de Carga
52	32		15		Cyclic Equalize Duration	Cyc EQ Duration		Duración carga cíclica			T carga cíclica
53	32		20		Temperature Compensation Center		TempComp Center		Base compensación temperatura		Base comp temp
54	32		15		Temp Comp Coefficient (slope)		Temp Comp Coeff		Factor Compensación Temp		Coef comp temp
55	32		15		Battery Current Limit			Batt Curr Limit		Límite corriente a batería		Lim corr bat
56	32		15		Battery Type Number			Batt Type Num		Número tipo de batería			Num tipo bat
57	32		15		Rated Capacity				Rated Capacity		Capacidad nominal C10			Capacidad C10	
58	32		15		Charging Efficiency			Charging Eff		Coeficiente de capacidad		Coef capacidad
59	32		15		Time to 0.1C10 Discharge Current	Time 0.1C10		Tiempo I descarga 0.1C10		Tiempo 0.1C10
60	32		15		Time to 0.2C10 Discharge Current	Time 0.2C10		Tiempo I descarga 0.2C10		Tiempo 0.2C10
61	32		15		Time to 0.3C10 Discharge Current	Time 0.3C10		Tiempo I descarga 0.3C10		Tiempo 0.3C10
62	32		15		Time to 0.4C10 Discharge Current	Time 0.4C10		Tiempo I descarga 0.4C10		Tiempo 0.4C10
63	32		15		Time to 0.5C10 Discharge Current	Time 0.5C10		Tiempo I descarga 0.5C10		Tiempo 0.5C10
64	32		15		Time to 0.6C10 Discharge Current	Time 0.6C10		Tiempo I descarga 0.6C10		Tiempo 0.6C10
65	32		15		Time to 0.7C10 Discharge Current	Time 0.7C10		Tiempo I descarga 0.7C10		Tiempo 0.7C10
66	32		15		Time to 0.8C10 Discharge Current	Time 0.8C10		Tiempo I descarga 0.8C10		Tiempo 0.8C10
67	32		15		Time to 0.9C10 Discharge Current	Time 0.9C10		Tiempo I descarga 0.9C10		Tiempo 0.9C10
68	32		15		Time to 1.0C10 Discharge Current	Time 1.0C10		Tiempo I descarga 1.0C10		Tiempo 1.0C10
70	32		15		Temperature Sensor Failure		TempSensorFail		Fallo sensor temperatura		Fallo sensor T
71	32		15		High Temperature 1			High Temp 1		Alta1 temperatura1			Alta1 temperat1
72	32		15		High Temperature 2			High Temp 2	Alta1 temperatura2			Alta1 temperat2
73	32		15		Low Temperature				Low Temperature		Baja temperatura			Baja temp 
74	32		15		Planned Battery Test Running		PlanBattTestRun		Prueba programada en curso		P bat planif
77	32		15		Short Battery Test Running		ShortBatTestRun		Prueba corta				Prueba corta
81	32		15		Automatic Equalize			Auto EQ				Carga automática			Autocarga
83	32		15		Abnormal Battery Current		Abnl Batt Curr		Corriente anormal a batería		Corr Bat mal
84	32		15		Temperature Compensation Active		TempComp Active		Compensación temperatura activa		Comp Temp Act
85	32		15		Battery Current Limit Active		Batt Curr Limit		Limitando corriente a baterías		Lim corr Bat
86	32		15		Battery Charge Prohibited Alarm		BattChgProhiAlm		Carga no permitida			Carga prohib
87	32		15		No					No			No					No
88	32		15		Yes					Yes			Sí					Sí
90	32		15		None					None			Ninguno					Ninguno
91	32		15		Temperature 1				Temperature 1	Temperatura 1				Temperatura 1
92	32		15		Temperature 2				Temperature 2	Temperatura 2				Temperatura 2
93	32		15		Temperature 3				Temperature 3	Temperatura 3				Temperatura 3
94	32		15		Temperature 4				Temperature 4	Temperatura 4				Temperatura 4
95	32		15		Temperature 5				Temperature 5	Temperatura 5				Temperatura 5
96	32		15		Temperature 6				Temperature 6		Temperatura 6				Temperatura 6
97	32		15		Temperature 7				Temperature 7		Temperatura 7				Temperatura 7
98	32		15		0					0			0					0
99	32		15		1					1			1					1
100	32		15		2					2			2					2
113	32		15		Float Charge				Float Charge		Carga/Flotación				Carga/Flota
114	32		15		Equalize Charge				EQ Charge		Carga					Carga
121	32		15		Disabled					Disabled			Deshabilitar				Deshabilitar
122	32		15		Enabled					Enabled		Habilitar				Habilitar
136	32		15		Record Threshold			RecordThreshold			Umbral de registro			Umbral registr
137	32		15		Remaining Time				Remaining		Tiempo restante estimado		Restante
138	32		15		Battery Management State		Batt Management		Estado batería				Estado bat
139	32		15		Float Charge				Float Charge		Carga/Flotación				Carga/Flota
140	32		15		Short Test				Short Test		Prueba corta				Prueba corta
141	32		15		Equalize for Test			EQ for Test		Carga antes de prueba			Precarga prueba
142	32		15		Manual Test				Manual Test		Prueba manual				Prueba manual
143	32		15		Planned Test				Planned Test		Prueba programada			Prueba program
144	32		15		AC Fail Test				AC Fail Test		Prueba en Fallo de Red			Prueba Fallo CA
145	32		15		AC Fail				AC Fail		Fallo de Red				Fallo de Red
146	32		15		Manual Equalize				Manual EQ		Carga manual				Carga manual
147	32		15		Auto Equalize				Auto EQ			Carga automática			Autocarga
148	32		15		Cyclic Equalize		Cyclic EQ		Carga cíclica				Carga cíclica
152	32		15		Over Current Limit			Over Curr Lmt		Nivel de sobrecorriente			Sobrecorriente
153	32		15		Stop Battery Test			Stop Batt Test		Parar prueba de baterías		Parar prueba
154	32		15		Battery Group				Battery Group		Grupo de baterías			Grupo Batería
157	32		15		Master Battery Test			Master Bat Test	Prueba maestra bat en curso		Prueba maestra
158	32		15		Master Equalize			Master EQ		Carga maestra en curso			Carga maestra
165	32		15		Test Voltage Level			Test Volt Level		Tensión de prueba			Tensión prueba
166	32		15		Bad Battery				Bad Battery		Batería Mal				Bateria Mal
168	32		15		Clear Bad Battery Alarm			Clr Bad Bat Alm			Cesar alarma Batería Mal		Cesar Bat Mal
170	32		15		Cyclic Equalize Start Time		Cyc EQ Time		Hora de inicio ciclico BC		Hora ciclico
171	32		15		Valid EQ Enable				Valid EQ		Habilitar válida BC			Válida BC
172	32		15		Start Battery Test			Start Batt Test		Iniciar prueba bat			Iniciar prueba
173	32		15		Stop					Stop			Parar					Parar
174	32		15		Start					Start			Iniciar					Iniciar
175	32		15		Number of Planned Tests per Year	Planned Tests	Num pruebas programadas por año		Num pruebas/año
176	32		15		Planned Test 1 (MM-DD Hr)		Test1 (M-D Hr)		Prueba programada 1			Prueba prog1
177	32		15		Planned Test 2 (MM-DD Hr)		Test2 (M-D Hr)		Prueba programada 2			Prueba prog2  
178	32		15		Planned Test 3 (MM-DD Hr)		Test3 (M-D Hr)		Prueba programada 3			Prueba prog3  
179	32		15		Planned Test 4 (MM-DD Hr)		Test4 (M-D Hr)		Prueba programada 4			Prueba prog4
180	32		15		Planned Test 5 (MM-DD Hr)		Test5 (M-D Hr)		Prueba programada 5			Prueba prog5
181	32		15		Planned Test 6 (MM-DD Hr)		Test6 (M-D Hr)		Prueba programada 6			Prueba prog6
182	32		15		Planned Test 7 (MM-DD Hr)		Test7 (M-D Hr)		Prueba programada 7			Prueba prog7
183	32		15		Planned Test 8 (MM-DD Hr)		Test8 (M-D Hr)		Prueba programada 8			Prueba prog8
184	32		15		Planned Test 9 (MM-DD Hr)		Test9 (M-D Hr)		Prueba programada 9			Prueba prog9
185	32		15		Planned Test 10 (MM-DD Hr)		Test10 (M-D Hr)		Prueba programada 10			Prueba prog10
186	32		15		Planned Test 11 (MM-DD Hr)		Test11 (M-D Hr)		Prueba programada 11			Prueba prog11
187	32		15		Planned Test 12 (MM-DD Hr)		Test12 (M-D Hr)		Prueba programada 12			Prueba prog12
188	32		15		Reset Battery Capacity			Reset Batt Cap	Reiniciar Capacidad Baterías		Res Cap Bat
191	32		15		Clear Abnormal Bat Current Alarm	Clr AbnlCur Alm		Cesar alarma corr anormal a bat		Cesar I bat mal
192	32		15		Clear Discharge Curr Imbalance		Clr Cur Imb Alm			Cesar alrm I descarga desequilb		Cesar I desqulb
193	32		15		Expected Current Limit			ExpectedCurrLmt		Limitación corriente prevista		Lim corr prev
194	32		15		Battery Test Running			BatTestRunning		Prueba de batería en progeso		Prueba de bat
195	32		15		Low Capacity Point			Low Capacity Pt		Nivel de Baja capacidad			Nivel Baja Cap
196	32		15		Battery Discharge			Battery Disch		Descarga Batería			Descarga Bat
197	32		15		Over Voltage				Over Voltage		Sobretensión				Sobretensión
198	32		15		Low Voltage				Low Voltage		Subtensión				Subtensión
200	32		15		Number of Battery Shunts		Num Batt Shunts		Número de shunts de batería		Shunts Batería
201	32		15		Imbalance Protection			Imb Protection		Protección desequilibrio		Prot Desequilb
202	32		15		Temp Compensation Probe Number		TempComp Sensor			Sensor Compensación Temp		Sensor CompTemp
203	32		15		EIB Battery Number			EIB Battery Num		Número de baterías EIB			Num Bat EIB
204	32		15		Normal					Normal			Normal					Normal
205	32		15		Special for NA				Special for NA		Especial				Especial
206	32		15		Battery Voltage Type			Batt Volt Type		Tipo tensión batería			Tensión Bat
207	32		15		Voltage on Very High Batt Temp		VoltVHiBatTemp		Tensión a Alta2 temperatura		V Alta2 Temp
#208	32		15		Current Limited				Current Limited		Limitación de corriente			Limit Corriente
209	32		15		Battery Temperature Probe Number	BatTempProbeNum		Sensor de baterías			Sensor baterías
212	32		15		Action on Very High Battery Temp	Action on VHBT		Acción a Alta2 temperatura		Acción AltaTemp
213	32		15		Disabled				Disabled		Ninguna					Ninguna
214	32		15		Lowering Voltage			Lowering Volt		Reducir tensión				Reducir tensión
215	32		15		Disconnection				Disconnection		Desconectar				Desconectar
216	32		15		Reconnection Temperature		ReconnectTemp		Temperatura de reconexión		T reconexión
217	32		15		Very Hi Temp Volt Setpoint (24V)	VHigh Temp Volt			Tensión a Alta2 Temp(24V)		V Alta2 Temp
218	32		15		Float Charge Voltage (24V)		Float Voltage		Tensión nominal(24V)			Tens nominal
219	32		15		Equalize Charge Voltage (24V)		EQ Voltage		Tensión carga batería(24V)		Tens carga
220	32		15		Test Voltage Limit (24V)		Test Volt Lmt		Tensión de prueba(24V)			Tensión prueba
221	32		15		Test End Voltage (24V)			Test End Volt		Tensión fin de prueba(24V)		V fin prueba
222	32		15		Current Limited				Current Limited		Limitación de corriente			Limit Corriente
223	32		15		Batt Voltage for North America		Batt Volt NA	Tensión Bat para Norteamérica		TensBat USA
224	32		15		Battery Changed				Battery Changed		Batería cambiada			Bat cambiada
225	32		15		Battery Test Lowest Capacity		BattTestLowCap	Capacidad mínima para prueba		Mín Cap prueba
226	32		15		Temperature 8				Temperature 8			Temperatura8				Temperatura8
227	32		15		Temperature 9				Temperature 9			Temperatura9				Temperatura9
228	32		15		Temperature 10				Temperature 10			Temperatura10				Temperatura10
229	32		15		LargeDU Rated Capacity			Rated Capacity	Capacidad UD grande			Cap UD grande
230	32		15		Clear Battery Test Fail Alarm		ClrBatTestFail		Cesar Fallo Prueba Baterías		Cesar F Prueba
231	32		15		Battery Test Fail			BatteryTestFail	Fallo prueba de baterías		Fallo Prueba
232	32		15		Maximum					Maximum			Máxima					Máxima
233	32		15		Average					Average			Media					Media
234	32		15		Average SMBRC				Average SMBRC		Media SMBRC				Media SMBRC
235	32		15		Compensation Temperature		Comp Temp		Temperatura Compensación		Temp Comp
236	32		15		Comp Temp High1			Comp Temp Hi1	Nivel Alta1 Temp Compensación		Alta1 Temp Comp
237	32		15		Comp Temp Low				Comp Temp Low		Nivel Baja Temp Compensación		Baja Temp Comp
238	32		15		Comp Temp High1			Comp Temp Hi1		Alta1 temperatura Compensación		Alta1 Temp Comp
239	32		15		Comp Temp Low				Comp Temp Low		Baja temperatura Compensación		Baja Temp Comp
240	32		15		Comp Temp High2			Comp Temp Hi2		Nivel Alta2 Temp Compensación	Alta2 TComp
241	32		15		Comp Temp High2			Comp Temp Hi2		Alta2 Temp Compensación		Alta2 TComp
242	32		15		Compensation Sensor Fault		CompTempFail		Fallo sensor Temperatura Comp		Fallo Sens Comp
243	32		15		Calculate Battery Current		Calc Batt Curr		Calcular Corriente a Batería		Calc corr Bat
244	32		15		Start Equalize Charge (for EEM)	Star EQ(EEM)			Iniciar Carga (EEM)			Inic Carga(EEM)
245	32		15		Start Float Charge (for EEM)	Star FLT(EEM)		Start FC (EEM)				Start FC (EEM)
246	32		15		Start Resistance Test(for EEM)		StartTest(EEM)		Start Res Test(EEM)			Start RTest EEM
247	32		15		Stop Resistance Test(for EEM)		Stop Test(EEM)	Stop RTest(EEM)				Stop RTest(EEM)
248	32		15		Reset Batt Cap(Internal Use)	ResetCap(Int)		Reset Cap(Int)				Reset Cap(Int)
249	32		15		Reset Battery Capacity(for EEM)		ResetCap(EEM)			Reset Battery Capacity (for EEM)	Reset Cap(EEM)
250	32		15		Clear Bad Battery Alarm(for EEM)	ClrAlm(EEM)			Clear Bad Battery Alarm(for EEM)	ClrBadBatt(EEM)
251	32		15		System Temp1		System T1	Temperatura 1				Temperatura 1
252	32		15		System Temp2		System T2	Temperatura 2				Temperatura 2
253	32		15		System Temp3		System T3	Temperatura 3				Temperatura 3
254	32		15		IB2-1 Temp1		IB2-1 T1		IB2-1-Temp1				IB2-1-Temp1
255	32		15		IB2-1 Temp2		IB2-1 T2		IB2-1-Temp2				IB2-1-Temp2
256	32		15		EIB-1 Temp1		EIB-1 T1		EIB-1-Temp1				EIB-1-Temp1
257	32		15		EIB-1 Temp2		EIB-1 T2		EIB-1-Temp2				EIB-1-Temp2
258	32		15		SMTemp1 Temp1		SMTemp1 T1			SMTemp1-T1				SMTemp1-T1
259	32		15		SMTemp1 Temp2		SMTemp1 T2			SMTemp1-T2				SMTemp1-T2
260	32		15		SMTemp1 Temp3		SMTemp1 T3			SMTemp1-T3				SMTemp1-T3
261	32		15		SMTemp1 Temp4		SMTemp1 T4			SMTemp1-T4				SMTemp1-T4
262	32		15		SMTemp1 Temp5		SMTemp1 T5			SMTemp1-T5				SMTemp1-T5
263	32		15		SMTemp1 Temp6		SMTemp1 T6			SMTemp1-T6				SMTemp1-T6
264	32		15		SMTemp1 Temp7		SMTemp1 T7			SMTemp1-T7				SMTemp1-T7
265	32		15		SMTemp1 Temp8		SMTemp1 T8			SMTemp1-T8				SMTemp1-T8
266	32		15		SMTemp2 Temp1		SMTemp2 T1			SMTemp2-T1				SMTemp2-T1
267	32		15		SMTemp2 Temp2		SMTemp2 T2			SMTemp2-T2				SMTemp2-T2
268	32		15		SMTemp2 Temp3		SMTemp2 T3			SMTemp2-T3				SMTemp2-T3
269	32		15		SMTemp2 Temp4		SMTemp2 T4			SMTemp2-T4				SMTemp2-T4
270	32		15		SMTemp2 Temp5		SMTemp2 T5			SMTemp2-T5				SMTemp2-T5
271	32		15		SMTemp2 Temp6		SMTemp2 T6			SMTemp2-T6				SMTemp2-T6
272	32		15		SMTemp2 Temp7		SMTemp2 T7			SMTemp2-T7				SMTemp2-T7
273	32		15		SMTemp2 Temp8		SMTemp2 T8			SMTemp2-T8				SMTemp2-T8
274	32		15		SMTemp3 Temp1		SMTemp3 T1			SMTemp3-T1				SMTemp3-T1
275	32		15		SMTemp3 Temp2		SMTemp3 T2			SMTemp3-T2				SMTemp3-T2
276	32		15		SMTemp3 Temp3		SMTemp3 T3			SMTemp3-T3				SMTemp3-T3
277	32		15		SMTemp3 Temp4		SMTemp3 T4			SMTemp3-T4				SMTemp3-T4
278	32		15		SMTemp3 Temp5		SMTemp3 T5			SMTemp3-T5				SMTemp3-T5
279	32		15		SMTemp3 Temp6		SMTemp3 T6			SMTemp3-T6				SMTemp3-T6
280	32		15		SMTemp3 Temp7		SMTemp3 T7			SMTemp3-T7				SMTemp3-T7
281	32		15		SMTemp3 Temp8		SMTemp3 T8			SMTemp3-T8				SMTemp3-T8
282	32		15		SMTemp4 Temp1		SMTemp4 T1			SMTemp4-T1				SMTemp4-T1
283	32		15		SMTemp4 Temp2		SMTemp4 T2			SMTemp4-T2				SMTemp4-T2
284	32		15		SMTemp4 Temp3		SMTemp4 T3			SMTemp4-T3				SMTemp4-T3
285	32		15		SMTemp4 Temp4		SMTemp4 T4			SMTemp4-T4				SMTemp4-T4
286	32		15		SMTemp4 Temp5		SMTemp4 T5			SMTemp4-T5				SMTemp4-T5
287	32		15		SMTemp4 Temp6		SMTemp4 T6			SMTemp4-T6				SMTemp4-T6
288	32		15		SMTemp4 Temp7		SMTemp4 T7			SMTemp4-T7				SMTemp4-T7
289	32		15		SMTemp4 Temp8		SMTemp4 T8			SMTemp4-T8				SMTemp4-T8
290	32		15		SMTemp5 Temp1		SMTemp5 T1		SMTemp5-T1				SMTemp5-T1
291	32		15		SMTemp5 Temp2		SMTemp5 T2		SMTemp5-T2				SMTemp5-T2
292	32		15		SMTemp5 Temp3		SMTemp5 T3		SMTemp5-T3				SMTemp5-T3
293	32		15		SMTemp5 Temp4		SMTemp5 T4		SMTemp5-T4				SMTemp5-T4
294	32		15		SMTemp5 Temp5		SMTemp5 T5		SMTemp5-T5				SMTemp5-T5
295	32		15		SMTemp5 Temp6		SMTemp5 T6		SMTemp5-T6				SMTemp5-T6
296	32		15		SMTemp5 Temp7		SMTemp5 T7		SMTemp5-T7				SMTemp5-T7
297	32		15		SMTemp5 Temp8		SMTemp5 T8		SMTemp5-T8				SMTemp5-T8
298	32		15		SMTemp6 Temp1		SMTemp6 T1		SMTemp6-T1				SMTemp6-T1
299	32		15		SMTemp6 Temp2		SMTemp6 T2		SMTemp6-T2				SMTemp6-T2
300	32		15		SMTemp6 Temp3		SMTemp6 T3		SMTemp6-T3				SMTemp6-T3
301	32		15		SMTemp6 Temp4		SMTemp6 T4		SMTemp6-T4				SMTemp6-T4
302	32		15		SMTemp6 Temp5		SMTemp6 T5		SMTemp6-T5				SMTemp6-T5
303	32		15		SMTemp6 Temp6		SMTemp6 T6		SMTemp6-T6				SMTemp6-T6
304	32		15		SMTemp6 Temp7		SMTemp6 T7		SMTemp6-T7				SMTemp6-T7
305	32		15		SMTemp6 Temp8		SMTemp6 T8		SMTemp6-T8				SMTemp6-T8
306	32		15		SMTemp7 Temp1		SMTemp7 T1		SMTemp7-T1				SMTemp7-T1
307	32		15		SMTemp7 Temp2		SMTemp7 T2		SMTemp7-T2				SMTemp7-T2
308	32		15		SMTemp7 Temp3		SMTemp7 T3		SMTemp7-T3				SMTemp7-T3
309	32		15		SMTemp7 Temp4		SMTemp7 T4		SMTemp7-T4				SMTemp7-T4
310	32		15		SMTemp7 Temp5		SMTemp7 T5		SMTemp7-T5				SMTemp7-T5
311	32		15		SMTemp7 Temp6		SMTemp7 T6		SMTemp7-T6				SMTemp7-T6
312	32		15		SMTemp7 Temp7		SMTemp7 T7		SMTemp7-T7				SMTemp7-T7
313	32		15		SMTemp7 Temp8		SMTemp7 T8		SMTemp7-T8				SMTemp7-T8
314	32		15		SMTemp8 Temp1		SMTemp8 T1		SMTemp8-T1				SMTemp8-T1
315	32		15		SMTemp8 Temp2		SMTemp8 T2		SMTemp8-T2				SMTemp8-T2
316	32		15		SMTemp8 Temp3		SMTemp8 T3		SMTemp8-T3				SMTemp8-T3
317	32		15		SMTemp8 Temp4		SMTemp8 T4		SMTemp8-T4				SMTemp8-T4
318	32		15		SMTemp8 Temp5		SMTemp8 T5		SMTemp8-T5				SMTemp8-T5
319	32		15		SMTemp8 Temp6		SMTemp8 T6		SMTemp8-T6				SMTemp8-T6
320	32		15		SMTemp8 Temp7		SMTemp8 T7		SMTemp8-T7				SMTemp8-T7
321	32		15		SMTemp8 Temp8		SMTemp8 T8		SMTemp8-T8				SMTemp8-T8
322	32		15		Minimum			Minimum			Mínimo					Mínimo
#---------------------------------------12345678901234567890123456789012--------123456789012345---------12345678901234567890123456789012--------123456789012345
351	32		15		System Temp1 High 2	System T1 Hi2		Alta2 Sys Temperatura 1			Alta2 Sys Temp1
352	32		15		System Temp1 High 1	System T1 Hi1	Alta1 Sys Temperatura 1			Alta1 Sys Temp1
353	32		15		System Temp1 Low	System T1 Low		Baja Sys Temperatura 1			Baja Sys Temp1
354	32		15		System Temp2 High 2	System T2 Hi2		Alta2 Sys Temperatura 2			Alta2 Sys Temp2
355	32		15		System Temp2 High 1	System T2 Hi1		Alta1 Sys Temperatura 2			Alta1 Sys Temp2
356	32		15		System Temp2 Low	System T2 Low		Baja Sys Temperatura 2			Baja Sys Temp2
357	32		15		System Temp3 High 2	System T3 Hi2		Alta2 Sys Temp3			Alta2 Sys Temp3
358	32		15		System Temp3 High 1	System T3 Hi1		Alta1 Sys Temp3			Alta1 Sys Temp3 
359	32		15		System Temp3 Low	System T3 Low		Baja Sys Temp3				Baja Temp3 
360	32		15		IB2-1 Temp1 High 2	IB2-1 T1 Hi2		Alta2 IB2-1 Temp1			Alta2 IB2-1T1
361	32		15		IB2-1 Temp1 High 1	IB2-1 T1 Hi1		Alta1 IB2-1 Temp1			Alta1 IB2-1-T1
362	32		15		IB2-1 Temp1 Low		IB2-1 T1 Low		Baja IB2-1 Temp1			Baja IB2-1 T1
363	32		15		IB2-1 Temp2 High 2	IB2-1 T2 Hi2		Alta2 IB2-1 Temp2			Alta2 IB2-1T2
364	32		15		IB2-1 Temp2 High 1	IB2-1 T2 Hi1		Alta1 IB2-1 Temp2			Alta1 IB2-1-T2
365	32		15		IB2-1 Temp2 Low		IB2-1 T2 Low		Baja IB2-1 Temp2			Baja IB2-1 T2
366	32		15		EIB-1 Temp1 High 2	EIB-1 T1 Hi2		Alta2 EIB-1 Temp1			Alta2 EIB-1-T1
367	32		15		EIB-1 Temp1 High 1	EIB-1 T1 Hi1		Alta1 EIB-1 Temp1			Alta1 EIB-1-T1
368	32		15		EIB-1 Temp1 Low		EIB-1 T1 Low		Baja EIB-1 Temp1			Baja EIB-1 T1
369	32		15		EIB-1 Temp2 High 2	EIB-1 T2 Hi2		Alta2 EIB-1 Temp2			Alta2 EIB-1-T2
370	32		15		EIB-1 Temp2 High 1	EIB-1 T2 Hi1		Alta1 EIB-1 Temp2			Alta1 EIB-1-T2
371	32		15		EIB-1 Temp2 Low		EIB-1 T2 Low		Baja EIB-1 Temp2			Baja EIB-1 T2
372	32		15		SMTemp1 Temp1 High 2	SMTemp1 T1 Hi2		Alta2 SMTemp1 T1			Alta2 SMTemp1T1
373	32		15		SMTemp1 Temp1 High 1	SMTemp1 T1 Hi1		Alta1 SMTemp1 T1			Alta1 SMTemp1T1
374	32		15		SMTemp1 Temp1 Low	SMTemp1 T1 Low		Baja SMTemp1 T1			Baja SMTemp1 T1
375	32		15		SMTemp1 Temp2 High 2	SMTemp1 T2 Hi2		Alta2 SMTemp1 T2			Alta2 SMTemp1T2
376	32		15		SMTemp1 Temp2 High 1	SMTemp1 T2 Hi1		Alta1 SMTemp1 T2			Alta1 SMTemp1T2
377	32		15		SMTemp1 Temp2 Low	SMTemp1 T2 Low		Baja SMTemp1 T2			Baja SMTemp1 T2
378	32		15		SMTemp1 Temp3 High 2	SMTemp1 T3 Hi2		Alta2 SMTemp1 T3			Alta2 SMTemp1T3
379	32		15		SMTemp1 Temp3 High 1	SMTemp1 T3 Hi1		Alta1 SMTemp1 T3			Alta1 SMTemp1T3
380	32		15		SMTemp1 Temp3 Low	SMTemp1 T3 Low		Baja SMTemp1 T3		Baja SMTemp1 T3
381	32		15		SMTemp1 Temp4 High 2	SMTemp1 T4 Hi2		Alta2 SMTemp1 T4			Alta2 SMTemp1T4
382	32		15		SMTemp1 Temp4 High 1	SMTemp1 T4 Hi1		Alta1 SMTemp1 T4			Alta1 SMTemp1T4
383	32		15		SMTemp1 Temp4 Low	SMTemp1 T4 Low		Baja SMTemp1 T4		Baja SMTemp1 T4
384	32		15		SMTemp1 Temp5 High 2	SMTemp1 T5 Hi2	Alta2 SMTemp1 T5			Alta2 SMTemp1T5
385	32		15		SMTemp1 Temp5 High 1	SMTemp1 T5 Hi1		Alta1  SMTemp1 T5			Alta1 SMTemp1T5
386	32		15		SMTemp1 Temp5 Low	SMTemp1 T5 Low		Baja  SMTemp1 T5		Baja  SMTemp1T5
387	32		15		SMTemp1 Temp6 High 2	SMTemp1 T6 Hi2		Alta2 SMTemp1 T6			Alta2 SMTemp1T6
388	32		15		SMTemp1 Temp6 High 1	SMTemp1 T6 Hi1		Alta1 SMTemp1 T6			Alta1 SMTemp1T6
389	32		15		SMTemp1 Temp6 Low	SMTemp1 T6 Low		Baja SMTemp1 T6			Baja SMTemp1 T6
390	32		15		SMTemp1 Temp7 High 2	SMTemp1 T7 Hi2		Alta2 SMTemp1 T7			Alta2 SMTemp1T7
391	32		15		SMTemp1 Temp7 High 1	SMTemp1 T7 Hi1		Alta1 SMTemp1 T7			Alta1 SMTemp1T7
392	32		15		SMTemp1 Temp7 Low	SMTemp1 T7 Low	Baja SMTemp1 T7			Baja SMTemp1 T7
393	32		15		SMTemp1 Temp8 High 2	SMTemp1 T8 Hi2		Alta2 SMTemp1 T8			Alta2 SMTemp1T8
394	32		15		SMTemp1 Temp8 High 1	SMTemp1 T8 Hi1		Alta1 SMTemp1 T8			Alta1 SMTemp1T8
395	32		15		SMTemp1 Temp8 Low	SMTemp1 T8 Low		Baja SMTemp1 T8			Baja SMTemp1 T8
#---------------------------------------12345678901234567890123456789012--------123456789012345---------12345678901234567890123456789012--------123456789012345
396	32		15		SMTemp2 Temp1 High 2	SMTemp2 T1 Hi2		Alta2 SMTemp2 T1			Alta2 SMTemp2T1
397	32		15		SMTemp2 Temp1 High 1	SMTemp2 T1 Hi1		Alta1 SMTemp2 T1			Alta1 SMTemp2T1
398	32		15		SMTemp2 Temp1 Low	SMTemp2 T1 Low		Baja SMTemp2 T1			Baja SMTemp2 T1
399	32		15		SMTemp2 Temp2 High 2	SMTemp2 T2 Hi2		Alta2 SMTemp2 T2			Alta2 SMTemp2T2
400	32		15		SMTemp2 Temp2 High 1	SMTemp2 T2 Hi1		Alta1 SMTemp2 T2			Alta1 SMTemp2T2
401	32		15		SMTemp2 Temp2 Low	SMTemp2 T2 Low	Baja SMTemp2 T2			Baja SMTemp2 T2
402	32		15		SMTemp2 Temp3 High 2	SMTemp2 T3 Hi2		Alta2 SMTemp2 T3			Alta2 SMTemp2T3
403	32		15		SMTemp2 Temp3 High 1	SMTemp2 T3 Hi1		Alta1 SMTemp2 T3			Alta1 SMTemp2T3
404	32		15		SMTemp2 Temp3 Low	SMTemp2 T3 Low		Baja SMTemp2 T3			Baja SMTemp2 T3
405	32		15		SMTemp2 Temp4 High 2	SMTemp2 T4 Hi2		Alta2 SMTemp2 T4			Alta2 SMTemp2T4
406	32		15		SMTemp2 Temp4 High 1	SMTemp2 T4 Hi1		Alta1 SMTemp2 T4			Alta1 SMTemp2T4
407	32		15		SMTemp2 Temp4 Low	SMTemp2 T4 Low		Baja SMTemp2 T4			Baja SMTemp2 T4
408	32		15		SMTemp2 Temp5 High 2	SMTemp2 T5 Hi2		Alta2 SMTemp2 T5			Alta2 SMTemp2T5
409	32		15		SMTemp2 Temp5 High 1	SMTemp2 T5 Hi1		Alta1 SMTemp2 T5			Alta1 SMTemp2T5
410	32		15		SMTemp2 Temp5 Low	SMTemp2 T5 Low		Baja SMTemp2 T5			Baja SMTemp2 T5
411	32		15		SMTemp2 Temp6 High 2	SMTemp2 T6 Hi2		Alta2 SMTemp2 T6			Alta2 SMTemp2T6
412	32		15		SMTemp2 Temp6 High 1	SMTemp2 T6 Hi1		Alta1 SMTemp2 T6			Alta1 SMTemp2T6
413	32		15		SMTemp2 Temp6 Low	SMTemp2 T6 Low		Baja SMTemp2 T6			Baja SMTemp2 T6
414	32		15		SMTemp2 Temp7 High 2	SMTemp2 T7 Hi2		Alta2 SMTemp2 T7			Alta2 SMTemp2T7
415	32		15		SMTemp2 Temp7 High 1	SMTemp2 T7 Hi1		Alta1 SMTemp2 T7			Alta1 SMTemp2T7
416	32		15		SMTemp2 Temp7 Low	SMTemp2 T7 Low		Baja SMTemp2 T7			Baja SMTemp2 T7
417	32		15		SMTemp2 Temp8 High 2	SMTemp2 T8 Hi2		Alta2 SMTemp2 T8			Alta2 SMTemp2T8
418	32		15		SMTemp2 Temp8 High 1	SMTemp2 T8 Hi1		Alta1 SMTemp2 T8			Alta1 SMTemp2T8
419	32		15		SMTemp2 Temp8 Low	SMTemp2 T8 Low	Baja SMTemp2 T8			Baja SMTemp2 T8
420	32		15		SMTemp3 Temp1 High 2	SMTemp3 T1 Hi2		Alta2 SMTemp3 T1			Alta2 SMTemp3T1
421	32		15		SMTemp3 Temp1 High 1	SMTemp3 T1 Hi1		Alta1 SMTemp3 T1			Alta1 SMTemp3T1
422	32		15		SMTemp3 Temp1 Low	SMTemp3 T1 Low		Baja SMTemp3 T1			Baja SMTemp3 T1
423	32		15		SMTemp3 Temp2 High 2	SMTemp3 T2 Hi2		Alta2 SMTemp3 T2			Alta2 SMTemp3T2
424	32		15		SMTemp3 Temp2 High 1	SMTemp3 T2 Hi1		Alta1 SMTemp3 T2			Alta1 SMTemp3T2
425	32		15		SMTemp3 Temp2 Low	SMTemp3 T2 Low		Baja SMTemp3 T2			Baja SMTemp3 T2
426	32		15		SMTemp3 Temp3 High 2	SMTemp3 T3 Hi2		Alta2 SMTemp3 T3			Alta2 SMTemp3T3
427	32		15		SMTemp3 Temp3 High 1	SMTemp3 T3 Hi1		Alta1 SMTemp3 T3			Alta1 SMTemp3T3
428	32		15		SMTemp3 Temp3 Low	SMTemp3 T3 Low		Baja SMTemp3 T3			Baja SMTemp3 T3
429	32		15		SMTemp3 Temp4 High 2	SMTemp3 T4 Hi2		Alta2 SMTemp3 T4			Alta2 SMTemp3T4
430	32		15		SMTemp3 Temp4 High 1	SMTemp3 T4 Hi1		Alta1 SMTemp3 T4			Alta1 SMTemp3T4
431	32		15		SMTemp3 Temp4 Low	SMTemp3 T4 Low		Baja SMTemp3 T4			Baja SMTemp3 T4
432	32		15		SMTemp3 Temp5 High 2	SMTemp3 T5 Hi2		Alta2 SMTemp3 T5			Alta2 SMTemp3T5
433	32		15		SMTemp3 Temp5 High 1	SMTemp3 T5 Hi1		Alta1 SMTemp3 T5			Alta1 SMTemp3T5
434	32		15		SMTemp3 Temp5 Low	SMTemp3 T5 Low	Baja SMTemp3 T5			Baja SMTemp3 T5
435	32		15		SMTemp3 Temp6 High 2	SMTemp3 T6 Hi2	Alta2 SMTemp3 T6			Alta2 SMTemp3T6
436	32		15		SMTemp3 Temp6 High 1	SMTemp3 T6 Hi1		Alta1 SMTemp3 T6			Alta1 SMTemp3T6
437	32		15		SMTemp3 Temp6 Low	SMTemp3 T6 Low		Baja SMTemp3 T6			Baja SMTemp3 T6
438	32		15		SMTemp3 Temp7 High 2	SMTemp3 T7 Hi2		Alta2 SMTemp3 T7			Alta2 SMTemp3T7
439	32		15		SMTemp3 Temp7 High 1	SMTemp3 T7 Hi1		Alta1 SMTemp3 T7			Alta1 SMTemp3T7
440	32		15		SMTemp3 Temp7 Low	SMTemp3 T7 Low		Baja SMTemp3 T7			Baja SMTemp3 T7
#---------------------------------------12345678901234567890123456789012--------123456789012345---------12345678901234567890123456789012--------123456789012345
441	32		15		SMTemp3 Temp8 High 2	SMTemp3 T8 Hi2		Alta2 SMTemp3 T8			Alta2 SMTemp3T8
442	32		15		SMTemp3 Temp8 High 1	SMTemp3 T8 Hi1		Alta1 SMTemp3 T8			Alta1 SMTemp3T8
443	32		15		SMTemp3 Temp8 Low	SMTemp3 T8 Low		Baja SMTemp3 T8			Baja SMTemp3 T8
444	32		15		SMTemp4 Temp1 High 2	SMTemp4 T1 Hi2		Alta2 SMTemp4 T1			Alta2 SMTemp4T1
445	32		15		SMTemp4 Temp1 High 1	SMTemp4 T1 Hi1		Alta1 SMTemp4 T1			Alta1 SMTemp4T1
446	32		15		SMTemp4 Temp1 Low	SMTemp4 T1 Low		Baja SMTemp4 T1			Baja SMTemp4 T1
447	32		15		SMTemp4 Temp2 High 2	SMTemp4 T2 Hi2		Alta2 SMTemp4 T2			Alta2 SMTemp4T2
448	32		15		SMTemp4 Temp2 High 1	SMTemp4 T2 Hi1		Alta1 SMTemp4 T2			Alta1 SMTemp4T2
449	32		15		SMTemp4 Temp2 Low	SMTemp4 T2 Low		Baja SMTemp4 T2			Baja SMTemp4 T2
450	32		15		SMTemp4 Temp3 High 2	SMTemp4 T3 Hi2	Alta2 SMTemp4 T3			Alta2 SMTemp4T3
451	32		15		SMTemp4 Temp3 High 1	SMTemp4 T3 Hi1		Alta1 SMTemp4 T3			Alta1 SMTemp4T3
452	32		15		SMTemp4 Temp3 Low	SMTemp4 T3 Low		Baja SMTemp4 T3		Baja SMTemp4 T3
453	32		15		SMTemp4 Temp4 High 2	SMTemp4 T4 Hi2		Alta2 SMTemp4 T4			Alta2 SMTemp4T4
454	32		15		SMTemp4 Temp4 High 1	SMTemp4 T4 Hi1			Alta1 SMTemp4 T4			Alta1 SMTemp4T4
455	32		15		SMTemp4 Temp4 Low	SMTemp4 T4 Low		Baja SMTemp4 T4			Baja SMTemp4 T4
456	32		15		SMTemp4 Temp5 High 2	SMTemp4 T5 Hi2		Alta2 SMTemp4 T5			Alta2 SMTemp4T5
457	32		15		SMTemp4 Temp5 High 1	SMTemp4 T5 Hi1		Alta1 SMTemp4 T5			Alta1 SMTemp4T5
458	32		15		SMTemp4 Temp5 Low	SMTemp4 T5 Low		Baja SMTemp4 T5			Baja SMTemp4 T5
459	32		15		SMTemp4 Temp6 High 2	SMTemp4 T6 Hi2		Alta2 SMTemp4 T6			Alta2 SMTemp4T6
460	32		15		SMTemp4 Temp6 High 1	SMTemp4 T6 Hi1		Alta1 SMTemp4 T6			Alta1 SMTemp4T6
461	32		15		SMTemp4 Temp6 Low	SMTemp4 T6 Low		Baja SMTemp4 T6			Baja SMTemp4 T6
462	32		15		SMTemp4 Temp7 High 2	SMTemp4 T7 Hi2		Alta2 SMTemp4 T7			Alta2 SMTemp4T7
463	32		15		SMTemp4 Temp7 High 1	SMTemp4 T7 Hi1		Alta1 SMTemp4 T7			Alta1 SMTemp4T7
464	32		15		SMTemp4 Temp7 Low	SMTemp4 T7 Low		Baja SMTemp4 T7			Baja SMTemp4 T7
465	32		15		SMTemp4 Temp8 High 2	SMTemp4 T8 Hi2		Alta2 SMTemp4 T8			Alta2 SMTemp4T8
466	32		15		SMTemp4 Temp8 Hi1	SMTemp4 T8 Hi1		Alta1 SMTemp4 T8			Alta1 SMTemp4T8
467	32		15		SMTemp4 Temp8 Low	SMTemp4 T8 Low		Baja SMTemp4 T8			Baja SMTemp4 T8
468	32		15		SMTemp5 Temp1 High 2	SMTemp5 T1 Hi2		Alta2 SMTemp5 T1			Alta2 SMTemp5T1
469	32		15		SMTemp5 Temp1 High 1	SMTemp5 T1 Hi1		Alta1 SMTemp5 T1			Alta1 SMTemp5T1
470	32		15		SMTemp5 Temp1 Low	SMTemp5 T1 Low	Baja SMTemp5 T1			Baja SMTemp5 T1
#---------------------------------------12345678901234567890123456789012--------123456789012345---------12345678901234567890123456789012--------123456789012345
471	32		15		SMTemp5 Temp2 High 2	SMTemp5 T2 Hi2		Alta2 SMTemp5 T2			Alta2 SMTemp5T2
472	32		15		SMTemp5 Temp2 High 1	SMTemp5 T2 Hi1		Alta1 SMTemp5 T2			Alta1 SMTemp5T2
473	32		15		SMTemp5 Temp2 Low	SMTemp5 T2 Low		Baja SMTemp5 T2			Baja SMTemp5 T2
474	32		15		SMTemp5 Temp3 High 2	SMTemp5 T3 Hi2		Alta2 SMTemp5 T3			Alta2 SMTemp5T3
475	32		15		SMTemp5 Temp3 High 1	SMTemp5 T3 Hi1		Alta1 SMTemp5 T3			Alta1 SMTemp5T3
476	32		15		SMTemp5 Temp3 Low	SMTemp5 T3 Low		Baja SMTemp5 T3		Baja SMTemp5 T3
477	32		15		SMTemp5 Temp4 High 2	SMTemp5 T4 Hi2		Alta2 SMTemp5 T4			Alta2 SMTemp5T4
478	32		15		SMTemp5 Temp4 High 1	SMTemp5 T4 Hi1		Alta1 SMTemp5 T4			Alta1 SMTemp5T4
479	32		15		SMTemp5 Temp4 Low	SMTemp5 T4 Low		Baja SMTemp5 T4			Baja SMTemp5 T4
480	32		15		SMTemp5 Temp5 High 2	SMTemp5 T5 Hi2		Alta2 SMTemp5 T5			Alta2 SMTemp5T5
481	32		15		SMTemp5 Temp5 High 1	SMTemp5 T5 Hi1		Alta1 SMTemp5 T5			Alta1 SMTemp5T5
482	32		15		SMTemp5 Temp5 Low	SMTemp5 T5 Low		Baja SMTemp5 T5			Baja SMTemp5 T5
483	32		15		SMTemp5 Temp6 High 2	SMTemp5 T6 Hi2		Alta2 SMTemp5 T6			Alta2 SMTemp5T6
484	32		15		SMTemp5 Temp6 High 1	SMTemp5 T6 Hi1		Alta1 SMTemp5 T6			Alta1 SMTemp5T6
485	32		15		SMTemp5 Temp6 Low	SMTemp5 T6 Low		Baja SMTemp5 T6			Baja SMTemp5 T6
486	32		15		SMTemp5 Temp7 High 2	SMTemp5 T7 Hi2		Alta2 SMTemp5 T7			Alta2 SMTemp5T7
487	32		15		SMTemp5 Temp7 High 1	SMTemp5 T7 Hi1		Alta1 SMTemp5 T7			Alta1 SMTemp5T7
488	32		15		SMTemp5 Temp7 Low	SMTemp5 T7 Low		Baja SMTemp5 T7			Baja SMTemp5 T7
489	32		15		SMTemp5 Temp8 High 2	SMTemp5 T8 Hi2		Alta2 SMTemp5 T8			Alta2 SMTemp5T8
490	32		15		SMTemp5 Temp8 High 1	SMTemp5 T8 Hi1		Alta1 SMTemp5 T8			Alta1 SMTemp5T8
491	32		15		SMTemp5 Temp8 Low	SMTemp5 T8 Low	Baja SMTemp5 T8			Baja SMTemp5 T8
492	32		15		SMTemp6 Temp1 High 2	SMTemp6 T1 Hi2		Alta2 SMTemp6 T1			Alta2 SMTemp6T1
493	32		15		SMTemp6 Temp1 High 1	SMTemp6 T1 Hi1		Alta1 SMTemp6 T1			Alta1 SMTemp6T1
494	32		15		SMTemp6 Temp1 Low	SMTemp6 T1 Low	Baja SMTemp6 T1			Baja SMTemp6 T1
495	32		15		SMTemp6 Temp2 High 2	SMTemp6 T2 Hi2		Alta2 SMTemp6 T2			Alta2 SMTemp6T2
496	32		15		SMTemp6 Temp2 High 1	SMTemp6 T2 Hi1		Alta1 SMTemp6 T2			Alta1 SMTemp6T2
497	32		15		SMTemp6 Temp2 Low	SMTemp6 T2 Low		Baja SMTemp6 T2			Baja SMTemp6 T2
498	32		15		SMTemp6 Temp3 High 2	SMTemp6 T3 Hi2		Alta2 SMTemp6 T3			Alta2 SMTemp6T3
499	32		15		SMTemp6 Temp3 High 1	SMTemp6 T3 Hi1		Alta1 SMTemp6 T3			Alta1 SMTemp6T3
500	32		15		SMTemp6 Temp3 Low	SMTemp6 T3 Low		Baja SMTemp6 T3			Baja SMTemp6 T3
#---------------------------------------12345678901234567890123456789012--------123456789012345---------12345678901234567890123456789012--------123456789012345
501	32		15		SMTemp6 Temp4 High 2	SMTemp6 T4 Hi2		Alta2 SMTemp6 T4			Alta2 SMTemp6T4
502	32		15		SMTemp6 Temp4 High 1	SMTemp6 T4 Hi1		Alta1 SMTemp6 T4			Alta1 SMTemp6T4
503	32		15		SMTemp6 Temp4 Low	SMTemp6 T4 Low		Baja SMTemp6 T4			Baja SMTemp6 T4
504	32		15		SMTemp6 Temp5 High 2	SMTemp6 T5 Hi2		Alta2 SMTemp6 T5			Alta2 SMTemp6T5
505	32		15		SMTemp6 Temp5 High 1	SMTemp6 T5 Hi1		Alta1 SMTemp6 T5			Alta1 SMTemp6T5
506	32		15		SMTemp6 Temp5 Low	SMTemp6 T5 Low		Baja SMTemp6 T5			Baja SMTemp6 T5
507	32		15		SMTemp6 Temp6 High 2	SMTemp6 T6 Hi2		Alta2 SMTemp6 T6			Alta2 SMTemp6T6
508	32		15		SMTemp6 Temp6 High 1	SMTemp6 T6 Hi1		Alta1 SMTemp6 T6			Alta1 SMTemp6T6
509	32		15		SMTemp6 Temp6 Low	SMTemp6 T6 Low		Baja SMTemp6 T6			Baja SMTemp6 T6
510	32		15		SMTemp6 Temp7 High 2	SMTemp6 T7 Hi2		Alta2 SMTemp6 T7			Alta2 SMTemp6T7
511	32		15		SMTemp6 Temp7 High 1	SMTemp6 T7 Hi1		Alta1 SMTemp6 T7			Alta1 SMTemp6T7
512	32		15		SMTemp6 Temp7 Low	SMTemp6 T7 Low		Baja SMTemp6 T7			Baja SMTemp6 T7
513	32		15		SMTemp6 Temp8 High 2	SMTemp6 T8 Hi2		Alta2 SMTemp6 T8			Alta2 SMTemp6T8
514	32		15		SMTemp6 Temp8 High 1	SMTemp6 T8 Hi1		Alta1 SMTemp6 T8			Alta1 SMTemp6T8
515	32		15		SMTemp6 Temp8 Low	SMTemp6 T8 Low		Baja SMTemp6 T8		Baja SMTemp6 T8
516	32		15		SMTemp7 Temp1 High 2	SMTemp7 T1 Hi2	Alta2 SMTemp7 T1			Alta2 SMTemp7T1
517	32		15		SMTemp7 Temp1 High 1	SMTemp7 T1 Hi1		Alta1 SMTemp7 T1			Alta1 SMTemp7T1
518	32		15		SMTemp7 Temp1 Low	SMTemp7 T1 Low	Baja SMTemp7 T1			Baja SMTemp7 T1
519	32		15		SMTemp7 Temp2 High 2	SMTemp7 T2 Hi2	Alta2 SMTemp7T2			Alta2 SMTemp7T2
520	32		15		SMTemp7 Temp2 High 1	SMTemp7 T2 Hi1		Alta1 SMTemp7T2			Alta1 SMTemp7T2
521	32		15		SMTemp7 Temp2 Low	SMTemp7 T2 Low	Baja SMTemp7T2			Baja SMTemp7T2
522	32		15		SMTemp7 Temp3 High 2	SMTemp7 T3 Hi2		Alta2 SMTemp7T3			Alta2 SMTemp7T3
523	32		15		SMTemp7 Temp3 High 1	SMTemp7 T3 Hi1		Alta1 SMTemp7T3			Alta1 SMTemp7T3
524	32		15		SMTemp7 Temp3 Low	SMTemp7 T3 Low		Baja SMTemp7T3			Baja SMTemp7T3
525	32		15		SMTemp7 Temp4 High 2	SMTemp7 T4 Hi2		Alta2 SMTemp7T4			Alta2 SMTemp7T4
526	32		15		SMTemp7 Temp4 High 1	SMTemp7 T4 Hi1		Alta1 SMTemp7T4			Alta1 SMTemp7T4
527	32		15		SMTemp7 Temp4 Low	SMTemp7 T4 Low		Baja SMTemp7T4			Baja SMTemp7T4
528	32		15		SMTemp7 Temp5 High 2	SMTemp7 T5 Hi2		Alta2 SMTemp7T5			Alta2 SMTemp7T5
529	32		15		SMTemp7 Temp5 High 1	SMTemp7 T5 Hi1		Alta1 SMTemp7T5			Alta1 SMTemp7T5
530	32		15		SMTemp7 Temp5 Low	SMTemp7 T5 Low		Baja SMTemp7T5			Baja SMTemp7T5
531	32		15		SMTemp7 Temp6 High 2	SMTemp7 T6 Hi2		Alta2 SMTemp7T6			Alta2 SMTemp7T6
532	32		15		SMTemp7 Temp6 High 1	SMTemp7 T6 Hi1		Alta1 SMTemp7T6			Alta1 SMTemp7T6
533	32		15		SMTemp7 Temp6 Low	SMTemp7 T6 Low		Baja SMTemp7T6			Baja SMTemp7T6
534	32		15		SMTemp7 Temp7 High 2	SMTemp7 T7 Hi2	Alta2 SMTemp7T7			Alta2 SMTemp7T7
535	32		15		SMTemp7 Temp7 High 1	SMTemp7 T7 Hi1		Alta1 SMTemp7T7			Alta1 SMTemp7T7
536	32		15		SMTemp7 Temp7 Low	SMTemp7 T7 Low		Baja SMTemp7T7			Baja SMTemp7T7
537	32		15		SMTemp7 Temp8 High 2	SMTemp7 T8 Hi2		Alta2 SMTemp7T8			Alta2 SMTemp7T8
538	32		15		SMTemp7 Temp8 High 1	SMTemp7 T8 Hi1		Alta1 SMTemp7T8			Alta1 SMTemp7T8
539	32		15		SMTemp7 Temp8 Low	SMTemp7 T8 Low		Baja SMTemp7T8			Baja SMTemp7T8
540	32		15		SMTemp8 Temp1 High 2	SMTemp8 T1 Hi2		Alta2 SMTemp8T1			Alta2 SMTemp8T1
541	32		15		SMTemp8 Temp1 High 1	SMTemp8 T1 Hi1		Alta1 SMTemp8T1			Alta1 SMTemp8T1
542	32		15		SMTemp8 Temp1 Low	SMTemp8 T1 Low			Baja SMTemp8T1			Baja SMTemp8T1
543	32		15		SMTemp8 Temp2 High 2	SMTemp8 T2 Hi2		Alta2 SMTemp8T2			Alta2 SMTemp8T2
544	32		15		SMTemp8 Temp2 High 1	SMTemp8 T2 Hi1		Alta1 SMTemp8T2			Alta1 SMTemp8T2
545	32		15		SMTemp8 Temp2 Low	SMTemp8 T2 Low		Baja SMTemp8T2			Baja SMTemp8T2
546	32		15		SMTemp8 Temp3 High 2	SMTemp8 T3 Hi2			Alta2 SMTemp8T3			Alta2 SMTemp8T3
547	32		15		SMTemp8 Temp3 High 1	SMTemp8 T3 Hi1		Alta1 SMTemp8T3		Alta1 SMTemp8T3
548	32		15		SMTemp8 Temp3 Low	SMTemp8 T3 Low		Baja SMTemp8T3			Baja SMTemp8T3
549	32		15		SMTemp8 Temp4 High 2	SMTemp8 T4 Hi2		Alta2 SMTemp8T4			Alta2 SMTemp8T4
550	32		15		SMTemp8 Temp4 High 1	SMTemp8 T4 Hi1		Alta1 SMTemp8T4			Alta1 SMTemp8T4
551	32		15		SMTemp8 Temp4 Low	SMTemp8 T4 Low		Baja SMTemp8T4			Baja SMTemp8T4
552	32		15		SMTemp8 Temp5 High 2	SMTemp8 T5 Hi2		Alta2 SMTemp8T5			Alta2 SMTemp8T5
553	32		15		SMTemp8 Temp5 High 1	SMTemp8 T5 Hi1		Alta1 SMTemp8T5			Alta1 SMTemp8T5
554	32		15		SMTemp8 Temp5 Low	SMTemp8 T5 Low	Baja SMTemp8T5			Baja SMTemp8T5
555	32		15		SMTemp8 Temp6 High 2	SMTemp8 T6 Hi2		Alta2 SMTemp8T6		Alta2 SMTemp8T6
556	32		15		SMTemp8 Temp6 High 1	SMTemp8 T6 Hi1		Alta1 SMTemp8T6			Alta1 SMTemp8T6
557	32		15		SMTemp8 Temp6 Low	SMTemp8 T6 Low		Baja SMTemp8T6			Baja SMTemp8T6
558	32		15		SMTemp8 Temp7 High 2	SMTemp8 T7 Hi2		Alta2 SMTemp8T7			Alta2 SMTemp8T7
559	32		15		SMTemp8 Temp7 High 1	SMTemp8 T7 Hi1		Alta1 SMTemp8T7			Alta1 SMTemp8T7
560	32		15		SMTemp8 Temp7 Low	SMTemp8 T7 Low			Baja SMTemp8T7			Baja SMTemp8T7
561	32		15		SMTemp8 Temp8 High 2	SMTemp8 T8 Hi2		Alta2 SMTemp8T8			Alta2 SMTemp8T8
562	32		15		SMTemp8 Temp8 High 1	SMTemp8 T8 Hi1		Alta1 SMTemp8T8			Alta1 SMTemp8T8
563	32		15		SMTemp8 Temp8 Low	SMTemp8 T8 Low		Baja SMTemp8T8			Baja SMTemp8T8
#---------------------------------------12345678901234567890123456789012--------123456789012345---------12345678901234567890123456789012--------123456789012345
564	32		15		Temp Comp Voltage Clamp		Temp Comp Clamp		Modo Compensación ESNA			Modo CompESNA
565	32		15		Temp Comp Max Voltage		Temp Comp Max V	Alta1 tensión Compensación ESNA		Alta1 V (ESNA)
566	32		15		Temp Comp Min Voltage		Temp Comp Min V	Baja tensión Compensación ESNA		Baja V (ESNA)
567	32		15		BTRM Temperature		BTRM Temp		Temperatura BTRM			Temp BTRM
568	32		15		BTRM Temp High 2		BTRM Temp High2		Alta2 Temp BTRM			TAlta2 BTRM
569	32		15		BTRM Temp High 1		BTRM Temp High1		Alta1 Temp BTRM				T Alta1 BTRM
570	32		15		Temp Comp Max Voltage (24V)	Temp Comp Max V		Tensión Max Compensación (24V)		Vmax Comp(24V)
571	32		15		Temp Comp Min Voltage (24V)		Temp Comp Min V		Tensión Min Compensación (24V)		Vmin Comp(24V)
572	32		15		BTRM Temp Sensor		BTRM TempSensor			Sensor Temperatura BTRM			SensorTemp BTRM
573	32		15		BTRM Sensor Fail		BTRM TempFail		Fallo Sensor temperatura BTRM		Fallo Sens BTRM
574	32		15		Li-Ion Group Avg Temperature		LiBatt AvgTemp		Temperatura media Grupo Li-Ion		T media Bat-Li
575	32		15		Number of Installed Batteries		Num Installed		Número de baterías instaladas		Núm instaladas
576	32		15		Number of Disconnected Batteries	NumDisconnected		Número de baterías desconectadas	Núm desconect
577	32		15		Inventory Update In Process		InventUpdating		Actualizando Inventario			Actualiz Invent
578	32		15		Number of Comm Fail Batt	Num Comm Fail		Núm baterías sin comunicación		N Bat No Resp
579	32		15		Li-Ion Battery Lost			LiBatt Lost		Batería Li-Ion perdida			Bat-Li perdida
580	32		15		System Battery Type			Sys Batt Type		Tipo de batería				Tipo Batería
581	32		15		1 Li-Ion Battery Disconnect		1 LiBattDiscon		1 Batería Li-Ion desconectada		1 Bat-Li descon
582	32		15		2+Li-Ion Battery Disconnect		2+LiBattDiscon		2+Batería Li-Ion desconectada		2+Bat-Li descon
583	32		15		1 Li-Ion Battery No Reply		1 LiBattNoReply		1 Batería Li-Ion no responde		1 BatLi no resp
584	32		15		2+Li-Ion Battery No Reply		2+LiBattNoReply		2+Batería Li-Ion no responde		2+BatLi no resp
585	32		15		Clear Li-Ion Battery Lost		Clr LiBatt Lost		Cesar Batería Li-Ion perdida		Cesar B-Li perd
586	32		15		Clear					Clear			Borrar					Borrar
587	32		15		Float Charge Voltage(Solar)		Float Volt(S)		Tensión de flotación(Solar)		V flotación(S)
588	32		15		Equalize Charge Voltage(Solar)		EQ Voltage(S)		Tensión de carga (Solar)		V Carga (Sol)
589	32		15		Float Charge Voltage(RECT)		Float Volt(R)		Tensión de carga (Rec)			V Carga (Rec)
590	32		15		Equalize Charge Voltage(RECT)		EQ Voltage(R)		Tensión de carga (Rec)			V Carga (Rec)
591	32		15		Active Battery Current Limit		ABCL Point		Límite corriente a Batería-Li		Lim corr Bat-Li
592	32		15		Clear Li Battery CommInterrupt		ClrLiBatComFail		Cesar fallo COM Batería Li-Ion		Cesa COM Bat-Li
593	32		15		ABCL is active			ABCL Active		ABCL is Active				ABCL Active
594	32		15		Last SMBAT Battery Number	Last SMBAT Num		Número último SMBAT		N último SMBAT

595	32		15		Voltage Adjust Gain		VoltAdjustGain		Ajuste Ganancia Tensión		Ganancia Tens

596	32		15		Curr Limited Mode		Curr Limit Mode		Modo Limitación Corriente	Modo Lim Corr
597	32		15		Current				Current			Corriente			Corriente
598	32		15		Voltage				Voltage			Tensión		Tensión
599	32		15		Battery Charge Prohibited Status		Charge Prohibit	Carga Batería Prohibida			Carga Prohibida
600	32		15		Battery Charge Prohibited Alarm			Charge Prohibit		Carga Batería Prohibida		Carga Prohibida
601	32		15		Battery Lower Capacity		Lower Capacity		Baja Capacidad Batería			Baja Capacidad
602	32		15		Charge Current			Charge Current		Corriente de Carga			Corri. de Carga
603	32		15		Upper Limit			Upper Lmt		Límite superior				Límite superior
604	32		15		Stable Range Upper Limit	Stable Up Lmt		Límite Superior de Rango Estable	Lim Sup R.Est.
605	32		15		Stable Range Lower Limit	Stable Low Lmt		Límite Inferior de Rango Estable	Lim Inf R.Est.
606	32		15		Speed Set Point			Speed Set Point		Velocidad				Velocidad
607	32		15		Slow Speed Coefficient		Slow Coeff		Coeficiente Baja velocidad		Coef Baja vel.
608	32		15		Fast Speed Coefficient		Fast Coeff		Coeficiente Alta1 velocidad		Coef Alta1 vel.
609	32		15		Min Amplitude			Min Amplitude		Amplitud Mínima			Amplitud Min
610	32		15		Max Amplitude			Max Amplitude		Amplitud Máxima			Amplitud Max
611	32		15		Cycle Number			Cycle Num		Número de Ciclo			Núm Ciclo
612	32		15		Battery Temp Summary Alarm	BattTempSummAlm		Alarma de batería Temp. Resumen			Alm.Bat.Temp
613	32		15		EQTemp Comp Coefficient		EQCompCoeff		EQ Coef. de temp. de un borrador		EQ.Coef.Temp

620	32		15		IB2-2 Temp1		IB2-2 T1		IB2-2 Temp1		IB2-2 T1
621	32		15		IB2-2 Temp2		IB2-2 T2		IB2-2 Temp2		IB2-2 T2
622	32		15		EIB-2 Temp1		EIB-2 T1		EIB-2 Temp1		EIB-2 T1
623	32		15		EIB-2 Temp2		EIB-2 T2		EIB-2 Temp2		EIB-2 T2

624	32		15		IB2-2 Temp1 High 2	IB2-2 T1 Hi2		Alta2 IB2-2 Temp1			Alta2 IB2-2-T1
625	32		15		IB2-2 Temp1 High 1	IB2-2 T1 Hi1		Alta1 IB2-2 Temp1			Alta1 IB2-2-T1
626	32		15		IB2-2 Temp1 Low		IB2-2 T1 Low		Baja IB2-2 Temp1			Baja IB2-2-T1
627	32		15		IB2-2 Temp2 High 2	IB2-2 T2 Hi2		Alta2 IB2-2 Temp2			Alta2 IB2-2-T2
628	32		15		IB2-2 Temp2 High 1	IB2-2 T2 Hi1		Alta1 IB2-2 Temp2			Alta1 IB2-2-T2
629	32		15		IB2-2 Temp2 Low		IB2-2 T2 Low		Baja IB2-2 Temp2			Baja IB2-2-T2
630	32		15		EIB-2 Temp1 High 2	EIB-2 T1 Hi2		Alta2 EIB-2 Temp1			Alta2 EIB-2-T1
631	32		15		EIB-2 Temp1 High 1	EIB-2 T1 Hi1		Alta1 EIB-2 Temp1			Alta1 EIB-2-T1
632	32		15		EIB-2 Temp1 Low		EIB-2 T1 Low		Baja EIB-2 Temp1			Baja EIB-2-T1
633	32		15		EIB-2 Temp2 High 2	EIB-2 T2 Hi2		Alta2 EIB-2 Temp2			Alta2 EIB-2-T2
634	32		15		EIB-2 Temp2 High 1	EIB-2 T2 Hi1		Alta1 EIB-2 Temp2			Alta1 EIB-2-T2
635	32		15		EIB-2 Temp2 Low		EIB-2 T2 Low		Baja EIB-2 Temp2			Baja EIB-2-T2

685	32		15		Temp Comp Off When		TC Off When		Comp Temp desactiv cuando	CT DesactivCuan
686	32		15		All Probes Fail			All Probes Fail		Todas sondas fallan		Todas SondFall
687	32		15		Any Probe Fails			Any Probe Fails		Cualquier sonda falla		CualqSondaFalla
688	32		15		Temp Compensation Enabled	Temp Comp		Comp Temp habilitada		Comp Temp
689	32		15		Temp Comp Threshold Enabled	TC Threshold		Umbral Comp Temp habilitado	Umbral CT
690	32		15		Temp for Temp Comp Off		Off Temp		Temp para Comp Temp Off		Temp Off
691	32		15		Temp for Temp Comp On		On Temp			Temp para Comp Temp On		Temp On

701	32		15		SMDUE1 Temp1		SMDUE1 T1		SMDUE1-T1			SMDUE1-T1
702	32		15		SMDUE1 Temp2		SMDUE1 T2		SMDUE1-T2			SMDUE1-T2
703	32		15		SMDUE1 Temp3		SMDUE1 T3		SMDUE1-T3			SMDUE1-T3
704	32		15		SMDUE1 Temp4		SMDUE1 T4		SMDUE1-T4			SMDUE1-T4
705	32		15		SMDUE1 Temp5		SMDUE1 T5		SMDUE1-T5			SMDUE1-T5
706	32		15		SMDUE1 Temp6		SMDUE1 T6		SMDUE1-T6			SMDUE1-T6
707	32		15		SMDUE1 Temp7		SMDUE1 T7		SMDUE1-T7			SMDUE1-T7
708	32		15		SMDUE1 Temp8		SMDUE1 T8		SMDUE1-T8			SMDUE1-T8
709	32		15		SMDUE1 Temp9		SMDUE1 T9		SMDUE1-T9			SMDUE1-T9
710	32		15		SMDUE1 Temp10		SMDUE1 T10	SMDUE1-T10		SMDUE1-T10
711	32		15		SMDUE2 Temp1		SMDUE2 T1		SMDUE2-T1			SMDUE2-T1
712	32		15		SMDUE2 Temp2		SMDUE2 T2		SMDUE2-T2			SMDUE2-T2
713	32		15		SMDUE2 Temp3		SMDUE2 T3		SMDUE2-T3			SMDUE2-T3
714	32		15		SMDUE2 Temp4		SMDUE2 T4		SMDUE2-T4			SMDUE2-T4
715	32		15		SMDUE2 Temp5		SMDUE2 T5		SMDUE2-T5			SMDUE2-T5
716	32		15		SMDUE2 Temp6		SMDUE2 T6		SMDUE2-T6			SMDUE2-T6
717	32		15		SMDUE2 Temp7		SMDUE2 T7		SMDUE2-T7			SMDUE2-T7
718	32		15		SMDUE2 Temp8		SMDUE2 T8		SMDUE2-T8			SMDUE2-T8
719	32		15		SMDUE2 Temp9		SMDUE2 T9		SMDUE2-T9			SMDUE2-T9
720	32		15		SMDUE2 Temp10		SMDUE2 T10	SMDUE2-T10		SMDUE2-T10
721		32		15		SMDUE1 Temp1 High 2	SMDUE1 T1 Hi2		Alta2 SMDUE1 Temp1		Al2 SMDUE1 T1
722		32		15		SMDUE1 Temp1 High 1	SMDUE1 T1 Hi1		Alta1 SMDUE1 Temp1		Al1 SMDUE1 T1
723		32		15		SMDUE1 Temp1 Low		SMDUE1 T1 Low		Baja SMDUE1 Temp1		Ba SMDUE1 T1
724		32		15		SMDUE1 Temp2 High 2	SMDUE1 T2 Hi2		Alta2 SMDUE1 Temp2		Al2 SMDUE1 T2
725		32		15		SMDUE1 Temp2 High 1	SMDUE1 T2 Hi1		Alta1 SMDUE1 Temp2		Al1 SMDUE1 T2
726		32		15		SMDUE1 Temp2 Low		SMDUE1 T2 Low		Baja SMDUE1 Temp2		Ba SMDUE1 T2
727		32		15		SMDUE1 Temp3 High 2	SMDUE1 T3 Hi2		Alta2 SMDUE1 Temp3		Al2 SMDUE1 T3
728		32		15		SMDUE1 Temp3 High 1	SMDUE1 T3 Hi1		Alta1 SMDUE1 Temp3		Al1 SMDUE1 T3
729		32		15		SMDUE1 Temp3 Low		SMDUE1 T3 Low		Baja SMDUE1 Temp3		Ba SMDUE1 T3
730		32		15		SMDUE1 Temp4 High 2	SMDUE1 T4 Hi2		Alta2 SMDUE1 Temp4		Al2 SMDUE1 T4
731		32		15		SMDUE1 Temp4 High 1	SMDUE1 T4 Hi1		Alta1 SMDUE1 Temp4		Al1 SMDUE1 T4
732		32		15		SMDUE1 Temp4 Low		SMDUE1 T4 Low		Baja SMDUE1 Temp4		Ba SMDUE1 T4
733		32		15		SMDUE1 Temp5 High 2	SMDUE1 T5 Hi2		Alta2 SMDUE1 Temp5		Al2 SMDUE1 T5
734		32		15		SMDUE1 Temp5 High 1	SMDUE1 T5 Hi1		Alta1 SMDUE1 Temp5		Al1 SMDUE1 T5
735		32		15		SMDUE1 Temp5 Low		SMDUE1 T5 Low		Baja SMDUE1 Temp5		Ba SMDUE1 T5
736		32		15		SMDUE1 Temp6 High 2	SMDUE1 T6 Hi2		Alta2 SMDUE1 Temp6		Al2 SMDUE1 T6
737		32		15		SMDUE1 Temp6 High 1	SMDUE1 T6 Hi1		Alta1 SMDUE1 Temp6		Al1 SMDUE1 T6
738		32		15		SMDUE1 Temp6 Low		SMDUE1 T6 Low		Baja SMDUE1 Temp6		Ba SMDUE1 T6
739		32		15		SMDUE1 Temp7 High 2	SMDUE1 T7 Hi2		Alta2 SMDUE1 Temp7		Al2 SMDUE1 T7
740		32		15		SMDUE1 Temp7 High 1	SMDUE1 T7 Hi1		Alta1 SMDUE1 Temp7		Al1 SMDUE1 T7
741		32		15		SMDUE1 Temp7 Low		SMDUE1 T7 Low		Baja SMDUE1 Temp7		Ba SMDUE1 T7
742		32		15		SMDUE1 Temp8 High 2	SMDUE1 T8 Hi2		Alta2 SMDUE1 Temp8		Al2 SMDUE1 T8
743		32		15		SMDUE1 Temp8 High 1	SMDUE1 T8 Hi1		Alta1 SMDUE1 Temp8		Al1 SMDUE1 T8
744		32		15		SMDUE1 Temp8 Low		SMDUE1 T8 Low		Baja SMDUE1 Temp8		Ba SMDUE1 T8
745		32		15		SMDUE1 Temp9 High 2	SMDUE1 T9 Hi2		Alta2 SMDUE1 Temp9		Al2 SMDUE1 T9
746		32		15		SMDUE1 Temp9 High 1	SMDUE1 T9 Hi1		Alta1 SMDUE1 Temp9		Al1 SMDUE1 T9
747		32		15		SMDUE1 Temp9 Low		SMDUE1 T9 Low		Baja SMDUE1 Temp9		Ba SMDUE1 T9
748		32		15		SMDUE1 Temp10 High 2	SMDUE1 T10 Hi2		Alta2 SMDUE1 Temp10		Al2 SMDUE1 T10
749		32		15		SMDUE1 Temp10 High 1	SMDUE1 T10 Hi1		Alta1 SMDUE1 Temp10		Al1 SMDUE1 T10
750		32		15		SMDUE1 Temp10 Low	SMDUE1 T10 Low		Baja SMDUE1 Temp10		Ba SMDUE1 T10
751		32		15		SMDUE2 Temp1 High 2	SMDUE2 T1 Hi2		Alta2 SMDUE2 Temp1		Al2 SMDUE2 T1
752		32		15		SMDUE2 Temp1 High 1	SMDUE2 T1 Hi1		Alta1 SMDUE2 Temp1		Al1 SMDUE2 T1
753		32		15		SMDUE2 Temp1 Low		SMDUE2 T1 Low		Baja SMDUE2 Temp1		Ba SMDUE2 T1
754		32		15		SMDUE2 Temp2 High 2	SMDUE2 T2 Hi2		Alta2 SMDUE2 Temp2		Al2 SMDUE2 T2
755		32		15		SMDUE2 Temp2 High 1	SMDUE2 T2 Hi1		Alta1 SMDUE2 Temp2		Al1 SMDUE2 T2
756		32		15		SMDUE2 Temp2 Low		SMDUE2 T2 Low		Baja SMDUE2 Temp2		Ba SMDUE2 T2
757		32		15		SMDUE2 Temp3 High 2	SMDUE2 T3 Hi2		Alta2 SMDUE2 Temp3		Al2 SMDUE2 T3
758		32		15		SMDUE2 Temp3 High 1	SMDUE2 T3 Hi1		Alta1 SMDUE2 Temp3		Al1 SMDUE2 T3
759		32		15		SMDUE2 Temp3 Low		SMDUE2 T3 Low		Baja SMDUE2 Temp3		Ba SMDUE2 T3
760		32		15		SMDUE2 Temp4 High 2	SMDUE2 T4 Hi2		Alta2 SMDUE2 Temp4		Al2 SMDUE2 T4
761		32		15		SMDUE2 Temp4 High 1	SMDUE2 T4 Hi1		Alta1 SMDUE2 Temp4		Al1 SMDUE2 T4
762		32		15		SMDUE2 Temp4 Low		SMDUE2 T4 Low		Baja SMDUE2 Temp4		Ba SMDUE2 T4
763		32		15		SMDUE2 Temp5 High 2	SMDUE2 T5 Hi2		Alta2 SMDUE2 Temp5		Al2 SMDUE2 T5
764		32		15		SMDUE2 Temp5 High 1	SMDUE2 T5 Hi1		Alta1 SMDUE2 Temp5		Al1 SMDUE2 T5
765		32		15		SMDUE2 Temp5 Low		SMDUE2 T5 Low		Baja SMDUE2 Temp5		Ba SMDUE2 T5
766		32		15		SMDUE2 Temp6 High 2	SMDUE2 T6 Hi2		Alta2 SMDUE2 Temp6		Al2 SMDUE2 T6
767		32		15		SMDUE2 Temp6 High 1	SMDUE2 T6 Hi1		Alta1 SMDUE2 Temp6		Al1 SMDUE2 T6
768		32		15		SMDUE2 Temp6 Low		SMDUE2 T6 Low		Baja SMDUE2 Temp6		Ba SMDUE2 T6
769		32		15		SMDUE2 Temp7 High 2	SMDUE2 T7 Hi2		Alta2 SMDUE2 Temp7		Al2 SMDUE2 T7
770		32		15		SMDUE2 Temp7 High 1	SMDUE2 T7 Hi1		Alta1 SMDUE2 Temp7		Al1 SMDUE2 T7
771		32		15		SMDUE2 Temp7 Low		SMDUE2 T7 Low		Baja SMDUE2 Temp7		Ba SMDUE2 T7
772		32		15		SMDUE2 Temp8 High 2	SMDUE2 T8 Hi2		Alta2 SMDUE2 Temp8		Al2 SMDUE2 T8
773		32		15		SMDUE2 Temp8 High 1	SMDUE2 T8 Hi1		Alta1 SMDUE2 Temp8		Al1 SMDUE2 T8
774		32		15		SMDUE2 Temp8 Low		SMDUE2 T8 Low		Baja SMDUE2 Temp8		Ba SMDUE2 T8
775		32		15		SMDUE2 Temp9 High 2	SMDUE2 T9 Hi2		Alta2 SMDUE2 Temp9		Al2 SMDUE2 T9
776		32		15		SMDUE2 Temp9 High 1	SMDUE2 T9 Hi1		Alta1 SMDUE2 Temp9		Al1 SMDUE2 T9
777		32		15		SMDUE2 Temp9 Low		SMDUE2 T9 Low		Baja SMDUE2 Temp9		Ba SMDUE2 T9
778		32		15		SMDUE2 Temp10 High 2	SMDUE2 T10 Hi2		Alta2 SMDUE2 Temp10		Al2 SMDUE2 T10
779		32		15		SMDUE2 Temp10 High 1	SMDUE2 T10 Hi1		Alta1 SMDUE2 Temp10		Al1 SMDUE2 T10
780		32		15		SMDUE2 Temp10 Low	SMDUE2 T10 Low		Baja SMDUE2 Temp10		Ba SMDUE2 T10

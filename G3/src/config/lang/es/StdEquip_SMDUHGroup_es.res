﻿#
#  Locale language support:spanish
#
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			SMDUH Group		SMDUH Group		Grupo SMDUH			Grupo SMDUH
2		32			15			Standby		Standby		En espera			En espera
3		32			15			Refresh			Refresh			Refrescar			Refrescar
4		32			15			Setting Refresh		Setting Refresh		Refrescar Ajustes		Refrescar Ajust
5		32			15			E-Stop		E-Stop			Parada Emergencia		Parada Emerg
6		32			15			Yes			Yes			Sí			Sí
7		32			15			Existence State		Existence State		Detección		Detección
8		32			15			Existent		Existent		Existente			Existente
9		32			15			Not Existent		Not Existent		No Existente			No Existente
10		32			15			Number of SMDUHs	Num of SMDUHs		Número de SMDUHs		Núm SMDUHs
11		32			15			SMDUH Config Changed	Cfg Changed		Config SMDUH cambiada		Cambio Cfg
12		32			15			Not Changed		Not Changed		No Cambiada			No Cambiada
13		32			15			Changed			Changed			Cambiada			Cambiada


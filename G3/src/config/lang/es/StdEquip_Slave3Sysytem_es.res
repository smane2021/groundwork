﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			System Voltage				System Voltage		Tensión del Sistema		Tensión Sistema
2	32			15			Rectifiers Number					Num of G3Rect			Número de Rectificadores	Núm Rect Ext3
3	32			15			Total Rectifiers Current				Total Rect Curr		Corriente total rectificadores	Corr tot rect
4	32			15			Rectifier Lost				Rectifier Lost		Rectificador perdido		Rect perdido
5	32			15			All Rectifiers Comm Fail		AllRectCommFail		Ningún rectificador responde	No Responden
6	32			15			Comm Fail					Comm Fail		Fallo comunicación		Fallo com
7	32			15			Existence State				Existence State		Detección			Detección
8	32			15			Existent				Existent		Existente			Existente
9	32			15			Not Existent			Not Existent		Inexistente			Inexistente
10	32			15			Normal					Normal			Normal				Normal
11	32			15			Fail						Fail			Fallo				Fallo
12	32			15			Rectifier Current Limit					Current Limit		Límite corriente rectificador	Lím corr rect
13	32			15			Rectifiers Trim						Rect Trim		Control tensión Rectificador	Control CC Rec
14	32			15			DC On/Off Control			DC On/Off Ctrl		Control CC rectificador		Ctrl CC rect
15	32			15			AC On/Off Control			AC On/Off Ctrl		Control CA rectificador		Ctrl CA rect
16	32			15			G3 Rectifiers LED Control		Rects LED Ctrl		Control LEDs			Control LEDs
17	32			15			Switch Off All				Switch Off All		Apagar todos			Apagar todos
18	32			15			Switch On All				Switch On All		Encender todos			Encender todos
19	32			15			All					All		Todos intermitente		Intermitentes
20	32			15			Stop Flashing						Stop Flashing		Parar intermitente		Parar Intermit
21	32			32			Current Limit Control				Curr Limit Ctrl		Control límite de corriente	Ctrl lim corriente
22	32			32			Full Capability Control				Full Capab Ctrl		Control a plena capacidad	Control a plena capacidad
23	32			15			G3 Clear Rectifier Lost Alarm		G3 ClrRectLost			Reiniciar alarma rect perdido	Inic R perdido
24	32			15			G3 Reset Cycle Alarm			G3 ClrCycleAlm			Reniciar alarma Redun Oscila	Inic Oscilante
25	32			15			Yes					Yes			Borrar				Borrar
26	32			15			Rectifier Group III					Rect Group III	Rectificadores GrupoIII		Rectif GrupoIII
27	32			15			E-Stop Function				E-Stop Function		Función Parada de Emergencia	Parada Emergen
36	32			15			Normal					Normal			Normal				Normal	
37	32			15			Failure					Failure			Fallo				Fallo         
38	32			15			Switch Off All				Switch Off All		Apagar todos			Apagar todos
39	32			15			Switch On All				Switch On All		Encender todos			Encender todos
83	32			15			No					No			No				No
84	32			15			Yes					Yes			Sí			Sí
96	32			15			Input Current Limit					Input Curr Lmt		Límite Corriente de entrada	Lim Corr Ent
97	32			15			Mains Failure				Mains Failure		Fallo de Red			Fallo de Red
98	32			15			G3 Clear Rect Comm Fail Alarm		G3 ClrCommFail		Cesar Alarma Fallo COM rect		Cesar Fallo COM
99	32			15			Rectifier Capacity Used			Rect Cap Used		Capacidad utilizada Rectifier		Cap usada Rect	
100	32			15			Maximum Used Capacity			Max Cap Used		Máxima Capacidad Utilizada		Max Cap Usada	
101	32			15			Minimum Used Capacity			Min Cap Used		Mínima Capacidad Utilizada		Min Cap Usada	
102	32			15			Total Rated Current		Total Rated Cur		Corriente Total Estimada		Total Corr Est
103	32			15			Total Rectifiers Communicating		Num Rects Comm		Total Rectif en Comunicación		Total Rec Com	
104	32			15			Rated Voltage				Rated Voltage		Nivel de Tensión			Nivel Tensión
105	32			15			G3 Fan Speed Control			Fan Speed Ctrl		Control ventilador			Ventilador
106	32			15			Full Speed				Full Speed		Max velocidad ventilador´		Vmax ventilador
107	32			15			Automatic Speed				Auto Speed		Automatic Speed				Auto Speed	
108	32			15			G3 Confirm Rectifier ID/Feed		Confirm ID/Feed			Confirmar ID/Fase Rectificador		Confirm ID/Fase
109	32			15			Yes					Yes			Sí					Sí
110	32			15			Multiple Rectifiers Fail		Multi-Rect Fail		Fallo múltiple rectificador		Fallo MultiRect	
111	32			15			Total Output Power			OutputPower		Potencia Total Estimada			Potencia Salida
112	32			15			G3 Reset Rectifier IDs			G3 ResetRectIDs		Reiniciar ID G3rectificadores		Res ID G3rectif

﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			SM Temp Group			SMTemp Group		Grupo SMTemp			Grupo SMTemp
2		32			15			SM Temp Number			SMTemp Num		Número de SMTemp		Núm. SMTemp
3		32			15			Communication Fail	Comm Fail		Interrupción			Interrupción
4		32			15			Existence State			Existence State		Detección			Detección
5		32			15			Existent			Existent		Existente			Existente
6		32			15			Not Existent		Not Existent		Inexistente			Inexistente
11		32			15			SM Temp Lost			SMTemp Lost		SMTemp perdido			SMTemp perdido
12		32			15			SM Temp Num Last Time	SMTemp Num Last			Último número de SMTemp		Ultim NumSMTemp
13		32			15			Clear SM Temp Lost Alarm	Clr SMTemp Lost		Cesar Alarma SMTemp perdido	Cesar SMTperdid
14		32			15			Clear				Clear			Borrar				Borrar

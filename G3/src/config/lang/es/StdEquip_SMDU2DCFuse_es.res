﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es



[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32			15			Fuse 1			Fuse 1		Fusible 1				Fusible 1
2	32			15			Fuse 2			Fuse 2		Fusible 2				Fusible 2
3	32			15			Fuse 3			Fuse 3		Fusible 3				Fusible 3
4	32			15			Fuse 4			Fuse 4		Fusible 4				Fusible 4
5	32			15			Fuse 5			Fuse 5		Fusible 5				Fusible 5
6	32			15			Fuse 6			Fuse 6		Fusible 6				Fusible 6
7	32			15			Fuse 7			Fuse 7		Fusible 7				Fusible 7
8	32			15			Fuse 8			Fuse 8		Fusible 8				Fusible 8
9	32			15			Fuse 9			Fuse 9		Fusible 9				Fusible 9
10	32			15			Fuse 10			Fuse 10		Fusible 10				Fusible 10
11	32			15			Fuse 11			Fuse 11		Fusible 11				Fusible 11
12	32			15			Fuse 12			Fuse 12		Fusible 12				Fusible 12
13	32			15			Fuse 13			Fuse 13		Fusible 13				Fusible 13
14	32			15			Fuse 14			Fuse 14		Fusible 14				Fusible 14
15	32			15			Fuse 15			Fuse 15		Fusible 15				Fusible 15
16	32			15			Fuse 16			Fuse 16		Fusible 16				Fusible 16
17	32			15			SMDU2 DC Fuse			SMDU2 DC Fuse		Fusible CC SMDU2			Fus CC SMDU2
18	32			15			State				State			Estado					Estado
19	32			15			Off				Off			Desconectado				Desconectado
20	32			15			On				On			Conectado				Conectado
21	32			15			Fuse 1 Alarm		DC Fuse 1 Alm		Alarma Fusible 1			Alarma fus1
22	32			15			Fuse 2 Alarm		DC Fuse 2 Alm		Alarma Fusible 2			Alarma fus2
23	32			15			Fuse 3 Alarm		DC Fuse 3 Alm		Alarma Fusible 3			Alarma fus3
24	32			15			Fuse 4 Alarm		DC Fuse 4 Alm		Alarma Fusible 4			Alarma fus4
25	32			15			Fuse 5 Alarm		DC Fuse 5 Alm		Alarma Fusible 5			Alarma fus5
26	32			15			Fuse 6 Alarm		DC Fuse 6 Alm		Alarma Fusible 6			Alarma fus6
27	32			15			Fuse 7 Alarm		DC Fuse 7 Alm		Alarma Fusible 7			Alarma fus7
28	32			15			Fuse 8 Alarm		DC Fuse 8 Alm		Alarma Fusible 8			Alarma fus8
29	32			15			Fuse 9 Alarm		DC Fuse 9 Alm		Alarma Fusible 9			Alarma fus9
30	32			15			Fuse 10 Alarm		DC Fuse 10 Alm		Alarma Fusible 10			Alarma fus10
31	32			15			Fuse 11 Alarm		DC Fuse 11 Alm		Alarma Fusible 11			Alarma fus11
32	32			15			Fuse 12 Alarm		DC Fuse 12 Alm		Alarma Fusible 12			Alarma fus12
33	32			15			Fuse 13 Alarm		DC Fuse 13 Alm		Alarma Fusible 13			Alarma fus13
34	32			15			Fuse 14 Alarm		DC Fuse 14 Alm		Alarma Fusible 14			Alarma fus14
35	32			15			Fuse 15 Alarm		DC Fuse 15 Alm		Alarma Fusible 15			Alarma fus15
36	32			15			Fuse 16 Alarm		DC Fuse 16 Alm		Alarma Fusible 16			Alarma fus16
37	32			15			Times of Communication Fail		Times Comm Fail		Interrupciones				Interrupciones
38	32			15			Communication Fail	Comm Fail		Interrupción Comunicación		Interrup COM
39	32			15			Load 1 Current			Load 1 Current		Corriente 1				Corriente 1
40	32			15			Load 2 Current			Load 2 Current		Corriente 2				Corriente 2
41	32			15			Load 3 Current			Load 3 Current		Corriente 3				Corriente 3
42	32			15			Load 4 Current			Load 4 Current		Corriente 4				Corriente 4
43	32			15			Load 5 Current			Load 5 Current		Corriente 5				Corriente 5

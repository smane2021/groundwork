﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
es



[RES_INFO]
#R_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32		15			GroupIII Rectifier		GroupIII Rect	Rectificador SIII			Rectif SIII
2		32		15			DC Status			DC Status		Estado CC			Estado CC
3		32		15			DC Output Voltage		DC Voltage			Tensión CC			Tensión CC
4		32		15			Origin Current			Origin Current		Corriente origen		Corr origen
5		32		15			Temperature			Temperature		Temperatura			Temp Rectif
6		32		15			Used Capacity			Used Capacity		Capacidad utilizada		Cap usada
7		32		15			AC Input Voltage		AC Voltage		Tensión entrada CA		Tensión CA Rec
8		32		15			DC Output Current		DC Current		Corriente CC			Corriente CC
9		32		15			SN				SN		Numero de Serie Rectificador	Num Serie Rect
10		32		15			Total Running Time		Running Time		Tiempo de operación		Tiempo operan
11		32		15			Communication Fail Count		Comm Fail Count		Contador No response		Conta No Resp
12		32		15			Derated by AC			Derated by AC		Potencia disminuida por CA	Pot dism por CA
13		32		15			Derated by Temp			Derated by Temp		Potencia dism por Temperatura	Pot dism temp
14		32		15			Derated				Derated			Reducido			Reducido
15		32		15			Full Fan Speed			Full Fan Speed		Alta velocidad ventilador	Alta veloc vent
16		32		15			Walk-In		Walk-In		Función Entrada Suave		Entrada Suave
17		32		15			AC On/Off			AC On/Off		Estado CA			Estado CA
18		32		15			Current Limit			Curr Limit		Límite de corriente		Lim corriente
19		32		15			Voltage High Limit		Volt Hi-limit		Límite de Alta tensión		Lim alta tens
20		32		15			AC Input Status			AC Status		Estado CA			Estado CA
21		32		15			Rect Temperature High		Rect Temp High			Alta Temperatura rect		Alta Temp Rect
22		32		15			Rectifier Fail			Rect Fail		Fallo rectificador		Fallo rectif
23		32		15			Rectifier Protected		Rect Protected		Rectificador protegido		Rect protegido
24		32		15			Fan Fail		Fan Fail	Fallo ventilador rectificador	Fallo vent rec
25		32		15			Current Limit State		CurrLimit State	Límite corriente rectificador	Lim corr rect
26		32		15			EEPROM Fail		EEPROM Fail		Fallo EEPROM			Fallo EEPROM
27		32		15			DC On/Off Control		DC On/Off Ctrl		Control CC rectificador		Ctrl CC rect
28		32		15			AC On/Off Control		AC On/Off Ctrl		Control CA rectificador		Ctrl CA rect
29		32		15			LED Control			LED Control		Control LED rectificador	Ctrl LED rect
30		32		15			Rectifier Reset			Rectifier Reset			Reiniciar rectificador		Reset rectif
31		32		15			AC Input Fail	AC Fail			Fallo red			Fallo red rect
34		32		15			Over Voltage			Over Voltage		Sobretensión			Sobretens rect
37		32		15			Current Limit			Current Limit		Límite corriente		Lim corr
39		32		15			Normal				Normal			Normal				Normal
40		32		15			Limited				Limited			Limitado			Limitado
45		32		15			Normal				Normal			Normal				Normal
46		32		15			Full				Full			Completo			Completo
47		32		15			Disabled			Disabled		Deshabilitado			Deshabilitado
48		32		15			Enabled				Enabled			Habilitado			Habilitado
49		32		15			On				On			Conectado			Conectado
50		32		15			Off				Off			Desconectado			Desconectado
51		32		15			Normal				Normal			Normal				Normal
52		32		15			Fail				Fail			Fallo				Fallo
53		32		15			Normal				Normal			Normal				Normal
54		32		15			Over Temperature		Over Temp		Sobretemperatura		Sobretemp
55		32		15			Normal				Normal			Normal				Normal
56		32		15			Fail				Fail			Fallo				Fallo
57		32		15			Normal				Normal			Normal				Normal
58		32		15			Protected			Protected		Protegido			Protegido
59		32		15			Normal				Normal			Normal				Normal
60		32		15			Fail				Fail			Fallo				Fallo
61		32		15			Normal				Normal			Normal				Normal
62		32		15			Alarm				Alarm			Alarma				Alarma
63		32		15			Normal				Normal			Normal				Normal
64		32		15			Fail				Fail			Fallo				Fallo
65		32		15			Off				Off			Apagado				Apagado
66		32		15			On				On			Conectado			Conectado
67		32		15			Off				Off			Apagado				Apagado
68		32		15			On				On			Conectado			Conectado
69		32		15			LED Control			LED Control		Intermitente			Intermitente
70		32		15			Cancel				Cancel			Normal				Normal
71		32		15			Off				Off			Apagado				Apagado
72		32		15			Reset				Reset			Reiniciar			Reset
73		32		15			Open Rectifier			Rectifier On		Conectado			Conectado
74		32		15			Close Rectifier			Rectifier Off			Apagado				Apagado
75		32		15			Off				Off			Apagado				Apagado
76		32		15			LED Control			LED Control			Intermitente			Intermitente
77		32		15			Rectifier Reset			Rectifier Reset			Reiniciar rectificador		Reset rectif
80		32		15			Rectifier Communication Fail			Rect Comm Fail			Fallo comunicación		Fallo COM
84		32		15			Rectifier High SN		Rect High SN		N serie rectificador		N serie Rec
85		32		15			Rectifier Version		Rect Version		Versión rectificador		Version Rec
86		32		15			Rectifier Part Number		Rect Part Num		NumParte Rectificador		Parte Rec
87		32		15			Current Share State		CurrShare State		Corriente reparto de carga	Corr rep carga
88		32		15			Current Share Alarm		CurrShare Alarm		Reparto de carga		Reparto carga
89		32		15			Over Voltage			Over Voltage		Sobretensión			Sobretens Rec
90		32		15			Normal				Normal			Normal				Normal
91		32		15			Over Voltage			Over Voltage	Sobretensión			Sobretensión
92		32		15			Line AB Voltage			Line AB Volt		Tensión RS			Tensión RS
93		32		15			Line BC Voltage			Line BC Volt		Tensión ST			Tensión ST
94		32		15			Line CA Voltage			Line CA Volt		Tensión RT			Tensión RT
95		32		15			Low Voltage			Low Voltage		Baja tensión			Baja tensión
96		32		15			Low AC Voltage Protection	Low AC Protect		Protección Subtensión CA	Protec SubVCA
97		32		15			Rectifier ID		Rectifier ID		Posición rectificador		Posición Rec
98		32		15			DC Output Turned Off		DC Output Off		Salida CC apagada		Salida CC Desc
99		32		15			Rectifier Phase			Rect Phase		Fase Rectificador		Fase Rectif
100		32		15			A				A			R				R
101		32		15			B				B			S				S
102		32		15			C				C			T				T
103		32		15			Severe Sharing CurrAlarm	SevereCurrShare			Fallo Reparto Carga Severo	Reparto Severo
104		32		15			Barcode 1			Barcode 1		Bar Code1			Bar Code1
105		32		15			Barcode 2			Barcode 2		Bar Code2			Bar Code2
106		32		15			Barcode 3			Barcode 3		Bar Code3			Bar Code3
107		32		15			Barcode 4			Barcode 4		Bar Code4			Bar Code4
108		32		15			Rectifier Communication Fail		Rect Comm Fail	Fallo comunicación Rectificador		Fallo Com Rec
109		32		15			No				No			No				No
110		32		15			Yes				Yes			Sí				Sí
111		32		15			Existence State			Existence State		Detección			Detección
113		32		15			Comm OK				Comm OK			Comunicación OK			COM OK
114		32		15			All Rectifiers Comm Fail			AllRectCommFail		Fallo comunicación Rectificador		Fallo Com Rec
115		32		15			Communication Fail			Comm Fail	Fallo Comunicación			Fallo Com
116		32		15			Rated Current			Rated Current		Corriente			Corriente
117		32		15			Efficiency			Efficiency		Eficiencia			Eficiencia
118		32		15			LT 93				LT 93			Menor que 93			Menor que 93
119		32		15			GT 93			GT 93			Mayor que 93			Mayor que 93
120		32		15			GT 95			GT 95			Mayor que 95			Mayor que 95
121		32		15			GT 96			GT 96			Mayor que 96			Mayor que 96
122		32		15			GT 97			GT 97			Mayor que 97			Mayor que 97
123		32		15			GT 98			GT 98			Mayor que 98			Mayor que 98
124		32		15			GT 99			GT 99			Mayor que 99			Mayor que 99
125		32		15			Redundancy Related Alarm	Redundancy Alm	Alarma Redundancia		Alarma Redund
126		32		15		Rect HVSD Status		HVSD Status		Estado HVSD			Estado HVSD
276		32		15			EStop/EShutdown			EStop/EShutdown		Desconex Emergencia		Desc Emergen

#
#  Locale language support:Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
es



[RES_INFO]
#R_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE		
1		32			15			Li-Ion Battery String		LiBattString		Li-Ion Battery String		LiBattString
2		32			15			Batt Voltage			Batt Voltage		Batt Voltage			Batt Voltage
3		32			15			Battery Terminal Voltage	Batt Term Volt		Battery Terminal Voltage	Bat TermVoltg
4		32			15			Battery Current			Batt Current		Battery Current			Bat Current
5		32			15			Cell Temperature		Cell Temp		Cell Temperature		Cell Temp
6		32			15			Switch Temperature		Switch Temp		Switch Temperature		Switch Temp
7		32			15			Capacity of Charge		Cap of Charge		State of Charge			StateOfCharge
8		32			15			Battery LED Status		Batt LED Status		Battery LED Status		BatLED Status
9		32			15			Battery Relay Status		BattRelayStatus		Battery Relay Status		BatRelyStatus
10		32			15			Charge Enabled			Charge Enabled		Charge Enabled			Charge Enab
11		32			15			Battery Discharging		Batt Discharge		Battery Discharging		Batt Discharge
12		32			15			Battery Charging		Batt Charge		Battery Charging		Bat Charg
13		32			15			DisCharging			DisCharging		DisCharging			DisCharging
14		32			15			Charging(5A)			Charging(5A)		Charging(5A)			Charging(5A)
15		32			15			DisCharging(5A)			DisCharging(5A)		DisCharging(5A)			DisCharging(5A)
16		32			15			DisChar Enabled14		DisCharEnable14		DisChar Enabled14		DisCharEnable14	
17		32			15			Char Enabled15			Char Enabled15		Char Enabled15			Char Enabled15

18		32			15			Battery Temperature Fail	Batt Temp Fail		Battery Temperature Fail	BattTemp Fail
19		32			15			Battery Current Fail		Current Fail		Battery Current Fail		Current Fail
20		32			15			Battery Hardware Failure	Hardware Fail		Battery Hardware Fail	Hardware Fail
21		32			15			Battery Over-voltage		Over-volt		Battery Over-voltage		Over-volt
22		32			15			Battery Low-Voltage		Low-volt		Battery Low-Voltage		Low-volt
23		32			15			Cell Volt Deviation		CellVoltDeviat		Cell Volt Deviation		CellVoltDeviat
24		32			15			Low Cell Voltage		Lo Cell Volt		Low Cell Voltage		Lo Cell Volt
25		32			15			High Cell Voltage		Hi Cell Volt		High Cell Voltage		Hi Cell Volt
26		32			15			High Cell Temperature		Hi Cell Temp		High Cell Temperature		Hi Cell Temp
27		32			15			High Switch DisTemp		HiSwitchDisTemp		High Switch DisTemp		HiSwitchDisTemp
28		32			15			Charge Short Circuit		Char ShortCirc		Charge Short Circuit		Char ShortCirc
29		32			15			DisChar Short Circuit		DisChar SC	DisChar Short Circuit		DisChar SC
30		32			15			High Switch CharTemp		Hi SW Char Temp		High Switch CharTemp		Hi SW Char Temp
31		32			15			Hardware Fail 20		HardwareFail20		Hardware Fail 20		HardwareFail20
32		32			15			Hardware Fail 21		HardwareFail21		Hardware Fail 21		HardwareFail21
33		32			15			High Charge Current		Hi Charge Curr		High Charge Curr		Hi Charge Curr
34		32			15			High DisCharge Current		Hi Dischar Curr			High DisCharge Curr		Hi DisCharCurr
35		32			15			Communication Fail		Comm Fail	Communication Fail		Comm Fail
36		32			15			Address Of Li-Ion Module	Module Address		Address Of Li-Ion Module	Module Address

37		32			15			Alarm				Alarm			Alarm				Alarm
38		32			15			Normal				Normal			Normal				Normal

39		32			15			Full On Green			Full on Green	Full On Green			Full on Green
40		32			15			Blinking Green			Blink Green		Blinking Green			Blink Green
41		32			15			Full On Yellow			Full on Yellow		Full On Yellow			Full on Yellow
42		32			15			Blinking Yellow			Blink Yellow		Blinking Yellow			Blink Yellow
43		32			15			Blinking Red			Blink Red		Blinking Red			Blink Red
44		32			15			Full On Red			Full on Red		Full On Red			FullOnRed
45		32			15			LED Off				LED Off			LED Off				LED Off
46		32			15			Batt Rating(Ah)			Batt Rating(Ah)		Batt Rating(Ah)			Batt Rating(Ah)

48		32			15			Battery Disconnected Status	Batt Disconnect		Battery Disconnected Status	Batt Disconnect	
49		32			15			Battery Disconnected		Batt Disconnect		Battery Disconnected		Batt Disconnect

96		32			15			Rated Capacity			Rated Capacity		Rated Capacity			Rated Capacity
99		32			15			Communication Fail		Comm Fail		Communication Fail		Comm Fail
100		32			15			Exist State			Exist State		Exist State			Exist State
101		32			15			True				True			True				True
102		32			15			False				False			False				False


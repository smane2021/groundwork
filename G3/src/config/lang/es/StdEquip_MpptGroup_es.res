﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
#---------------------------------------------------------------12345678901234567890123456789012345-----123456789012345---------12345678901234567890123456789012--------123456789012345
1		32			15			Solar Converter Group			Solar ConvGrp		Grupo Convertidor Solar			Grpo Conv Solar
2		32			15			Total Current				Total Current		Corriente Total				Corriente Total
3		32			15			Average Voltage				Average Voltage		Tensión media				Tensión media
4		32			15			Solar Converter Capacity Used		Solar Cap Used		Capacidad usada Conv Solar			Cap usada Solar
5		32			15			Maximum Used Capacity			Max Cap Used		Máxima Capacidad utilizada		Max Cap Usada
6		32			15			Minimum Used Capacity			Min Cap Used		Mínima Capacidad utilizada		Min Cap Usada
7		36			15			Total Solar Converters Communicating	Num Solar Convs	Total Convertidores en comunicación	Num Conv Solar
8		32			15			Valid Solar Converter			Valid SolarConv		Convertidores válidos			ConvSol válidos
9		32			15			Number of Solar Converters		Num Solar Convs		Número de convertidores solares		Núm ConvSolar
#---------------------------------------------------------------12345678901234567890123456789012345-----123456789012345---------12345678901234567890123456789012--------123456789012345
11		34			15			Solar Converter(s) Failure Status	SolarConv Fail		Solar Converter(s) Failure Status	SolarConv Fail
12		32			15			Solar Converter Current Limit		SolConvCurrLmt		Solar Converter Current Limit		SolConvCurrLim
13		32			15			Solar Converter Trim			Solar Conv Trim		Solar Converter Trim			Solar Conv Trim
14		32			15			DC On/Off Control			DC On/Off Ctrl		DC On/Off Control			DC On/Off Ctrl
16		32			15			Solar Converter LED Control		SolConv LEDCtrl		Solar Converter LED Control		SolConv LEDCtrl
17		32			15			Fan Speed Control			Fan Speed Ctrl		Fan Speed Control			Fan Speed Ctrl
18		32			15			Rated Voltage				Rated Voltage		Rated Voltage				Rated Voltage
19		32			15			Rated Current				Rated Current		Rated Current				Rated Current
20		32			15			High Voltage Shutdown Limit		HVSD Limit		High Voltage Shutdown Limit		HVSD Limit
21		32			15			Low Voltage Limit			Low Volt Limit		Low Voltage Limit			Low Volt Limit
22		32			15			High Temperature Limit			High Temp Limit		High Temperature Limit			High Temp Limit
24		32			15			HVSD Restart Time			HVSD Restart T		HVSD Restart Time			HVSD Restart T
25		32			15			Walk-In Time				Walk-In Time		Walk-In Time				Walk-In Time
26		32			15			Walk-In					Walk-In			Walk-In					Walk-In
27		32			15			Minimum Redundancy			Min Redundancy		Minimum Redundancy			Min Redundancy
28		32			15			Maximum Redundancy			Max Redundancy		Maximum Redundancy			Max Redundancy
29		32			15			Turn Off Delay				Turn Off Delay		Turn Off Delay				Turn Off Delay
30		32			15			Cycle Period				Cycle Period		Cycle Period				Cycle Period
31		32			15			Cycle Activation Time			Cyc Active Time		Cycle Activation Time			Cyc Active Time
33		32			15			Multiple Solar Converter Failure	MultSolConvFail		Fallo múltiple Convertidor		MultiFallo Conv
#---------------------------------------------------------------12345678901234567890123456789012345-----123456789012345---------12345678901234567890123456789012--------123456789012345
36		32			15			Normal					Normal			Normal					Normal
37		32			15			Failure					Failure			Fallo					Fallo
38		32			15			Switch Off All				Switch Off All		Switch Off All				Switch Off All		
39		32			15			Switch On All				Switch On All		Switch On All				Switch On All		
42		32			15			All Flashing				All Flashing		All Flashing				All Flashing		
43		32			15			Stop Flashing				Stop Flashing		Stop Flashing				Stop Flashing		
44		32			15			Full Speed				Full Speed		Full Speed				Full Speed
45		32			15			Automatic Speed				Auto Speed		Automatic Speed				Auto Speed
46		32			32			Current Limit Control			Curr Limit Ctrl		Current Limit Control			Curr Limit Ctrl
47		32			32			Full Capability Control			Full Cap Ctrl		Full Capability Control			Full Cap Ctrl
54		32			15			Disabled				Disabled		No					No
55		32			15			Enabled					Enabled			Sí				Sí
68		32			15			ECO Mode				ECO Mode		ECO Mode				ECO Mode
73		32			15			No					No			No					No
74		32			15			Yes					Yes			Sí				Sí
77		32			15			Pre-CurrLimit Turn-On			Pre-Curr Limit		Pre-CurrLimit Turn-On			Pre-Curr Limit
78		32			15			Solar Converter Power Type		SolConv PwrType		Solar Converter Power Type		SolConv PwrType	
79		32			15			Double Supply				Double Supply		Double Supply				Double Supply
80		32			15			Single Supply				Single Supply		Single Supply				Single Supply
81		32			15			Last Solar Converter Quantity		LastSolConvQty		Last Solar Converter Quantity		LastSolConvQty
83		32			15			Solar Converter Lost			Solar Conv Lost		Convertidor Solar perdido		CvSolar Perdido
#---------------------------------------------------------------12345678901234567890123456789012345-----123456789012345---------12345678901234567890123456789012--------123456789012345
84		32			15			Clear Solar Converter Lost Alarm	ClearSolarLost		Clear Solar Converter Lost Alarm	ClearSConv Lost
85		32			15			Clear					Clear			Clear					Clear
86		32			15			Confirm Solar Converter ID		Confirm ID		Confirmar ID Convertidor Solar		Confirmar ID
87		32			15			Confirm					Confirm			Confirm					Confirm	
88		32			15			Best Operating Point			Best Oper Point		Best Operating Point			Best Oper Point
89		32			15			Solar Converter Redundancy		SolConv Redund		Solar Converter Redundancy		SolConv Redund
90		32			15			Load Fluctuation Range			Fluct Range		Load Fluctuation Range			Fluct Range	
91		32			15			System Energy Saving Point		Energy Save Pt		System Energy Saving Point		Energy Save Pt
92		32			15			E-Stop Function				E-Stop Function		E-Stop Function				E-Stop Function
94		32			15			Single Phase				Single Phase		Single Phase				Single Phase
95		32			15			Three Phases				Three Phases		Three Phases				Three Phases
96		32			15			Input Current Limit			Input Curr Lmt		Input Current Limit			Input Curr Lmt
97		32			15			Double Supply				Double Supply		Double Supply				Double Supply
98		32			15			Single Supply				Single Supply		Single Supply				Single Supply
99		32			15			Small Supply				Small Supply		Small Supply				Small Supply
100		32			15			Restart on HVSD				Restart on HVSD		Restart on HVSD				Restart on HVSD
101		32			15			Sequence Start Interval			Start Interval		Sequence Start Interval			Start Interval
102		32			15			Rated Voltage				Rated Voltage		Rated Voltage				Rated Voltage
103		32			15			Rated Current				Rated Current		Rated Current				Rated Current
104		32			15			All Solar Converters Comm Fail		SolarsComFail		All Solar Converters Comm Fail		SolarCv ComFail
105		32			15			Inactive				Inactive		No					No
106		32			15			Active					Active			Activo					Activo
112		32			15			Rated Voltage (Internal Use)		Rated Voltage		Rated Voltage (Internal Use)		Rated Voltage
113		32			15			SolarConv Info Change(Internal)	InfoChange		SolarConv Info Change(Internal)	InfoChange
114		32			15			MixHE Power				MixHE Power		MixHE Power				MixHE Power
115		32			15			Derated					Derated			Reducido				Reducido
116		32			15			Non-Derated				Non-Derated		No reducido				No Reducido
117		32			15			All Solar Converter ON Time		SolarConvONTime		All Solar Converters On Time		SolarConvONTime
118		32			15			All Solar Converter are On		AllSolarConvOn		All Solar Converters are On		AllSolarConvOn
119		39			15			Clear Solar Converter Comm Fail Alarm	ClrSolCommFail		Clear Solar Converter Comm Fail Alarm	ClrSolCommFail
120		32			15			HVSD					HVSD			HVSD					HVSD 
121		32			15			HVSD Voltage Difference			HVSD Volt Diff		HVSD Voltage Difference			HVSD Volt Diff
122		32			15			Total Rated Current			Total Rated Cur		Total Rated Current			Total Rated Cur
123		32			15			Diesel Generator Power Limit		DG Pwr Lmt		Diesel Generator Power Limit		DG Pwr Lmt 
124		32			15			Diesel Generator Digital Input		Diesel DI Input		Diesel Generator Digital Input		Diesel DI Input
125		32			15			Diesel Gen Power Limit Point Set	DG Pwr Lmt Pt		Diesel Gen Power Limit Point Set	DG Pwr Lmt Pt
126		32			15			None					None			No					No
#---------------------------------------------------------------12345678901234567890123456789012345-----123456789012345---------12345678901234567890123456789012--------123456789012345
140		32			15			Solar Converter Delta Voltage		SolConvDeltaVol		Solar Converter Delta Voltage		SolConvDeltaVol
141		32			15			Solar Status				Solar Status		Solar Status				Solar Status
142		32			15			Day					Day			Día					Día
143		32			15			Night					Night			Noche					Noche
144		32			15			Existence State				Existence State		Detección				Detección
145		32			15			Existent				Existent		Existe					Existe
146		32			15			Not Existent				Not Existent		No existe				No existe
147	32			15			Total Input Current			Input Current		Total Corriente Entrada			Corriente Ent
148	32			15			Total Input Power			Input Power		Total Potencia Entrada			Potencia Ent	
149	32			15			Total Rated Current			Total Rated Cur		Corriente Total Estimada		Total Corr Est
150	32			15			Total Output Power			Output Power		Total Potencia Salida			Potencia Salida
151	32			15			Reset Solar Converter IDs		Reset Solar IDs		Reiniciar ID conv Solares	Res ID CV Solar
152	32			15			Clear Solar Converter Comm Fail		ClrSolCommFail		Cesar Alarma Fallo COM Solar		CesarFalCOMSol
153	32			15			Solar Failure Min Time			SolFailMinTime		Tiempo Mínimo de Falla Solar		TieMínFallaSol

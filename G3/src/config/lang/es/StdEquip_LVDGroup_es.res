﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			LVD Group				LVD Group		Grupo LVD			Grupo LVD
2		32			15			Battery LVD				Battery LVD		LVD de batería			LVD de batería
3		32			15			None					None			No				No
4		32			15			LVD 1					LVD 1			LVD 1				LVD 1
5		32			15			LVD 2					LVD 2			LVD 2				LVD 2
6		32			15			LVD 3					LVD 3			LVD 3				LVD 3
7		32			15			AC Fail Required			ACFail Required		Requiere Fallo de Red		Con Fallo Red
8		32			15			Disabled					Disabled			No				No
9		32			15			Enabled					Enabled			Sí				Sí
10		32			15			Number of LVD				Number of LVD		Número de LVDs			Núm LVDs
11		32			15			0					0			0				0
12		32			15			1					1			1				1
13		32			15			2					2			2				2
14		32			15			3					3			3				3
15		32			15			HTD Point				HTD Point		Nivel desconexión alta Temp	Desc Alta Temp
16		32			15			HTD Reconnect Point			HTD Recon Point		Nivel reconexión alta Temp	Recon Alta Temp
17		32			15			HTD Temperature Sensor			HTD Temp Sensor		Sensor Desconex alta Temp	Sens Des Alta T
18		32			15			Ambient Temperature			Ambient Temp		Ambiente			Ambiente
19		32			15			Battery Temperature			Battery Temp		Batería				Batería
20		32			15			Temperature 1				Temperature 1			Temperatura 1			Temp 1
21		32			15			Temperature 2				Temperature 2			Temperatura 2			Temp 2
22		32			15			Temperature 3				Temperature 3			Temperatura 3			Temp 3
23		32			15			Temperature 4				Temperature 4			Temperatura 4			Temp 4
24		32			15			Temperature 5				Temperature 5			Temperatura 5			Temp 5
25		32			15			Temperature 6				Temperature 6			Temperatura 6			Temp 6
26		32			15			Temperature 7				Temperature 7			Temperatura 7			Temp 7
27		32			15			Existence State				Existence State		Detección			Detección
28		32			15			Existent				Existent		Existente			Existente
29		32			15			Not Existent				Not Existent		No existente			No existente
31		32			15			LVD 1					LVD 1			LVD1				LVD1
32		32			15			LVD 1 Mode				LVD 1 Mode		Modo LVD1			Modo LVD1
33		32			15			LVD 1 Voltage				LVD 1 Voltage		Tensión LVD1			Tensión LVD1
34		32			15			LVD 1 Reconnect Voltage			LVD1 Recon Volt		Tensión Reconexión LVD1		Reconex LVD1
35		32			15			LVD 1 Reconnect Delay			LVD1 ReconDelay		Retardo Reconexión LVD1		Retardo RLVD1
36		32			15			LVD 1 Time				LVD 1 Time		Tiempo LVD1			Tiempo LVD1
37		32			15			LVD 1 Dependency				LVD1 Dependency		Dependencia LVD1		Depend LVD1
41		32			15			LVD 2					LVD 2			LVD2				LVD2
42		32			15			LVD 2 Mode				LVD 2 Mode		Modo LVD2			Modo LVD2
43		32			15			LVD 2 Voltage				LVD 2 Voltage		Tensión LVD2			Tensión LVD2
44		32			15			LVD 2 Reconnect Voltage			LVD2 Recon Volt	Tensión Reconexión LVD2		Reconex LVD2
45		32			15			LVD 2 Reconnect Delay			LVD2 ReconDelay		Retardo Reconexión LVD2		Retardo RLVD2
46		32			15			LVD 2 Time				LVD 2 Time		Tiempo LVD2			Tiempo LVD2
47		32			15			LVD 2 Dependency				LVD2 Dependency		Dependencia LVD2		Depend LVD2
51		32			15			Disabled				Disabled		No				No
52		32			15			Enabled					Enabled			Sí				Sí
53		32			15			Voltage					Voltage			Tensión				Tensión
54		32			15			Time					Time			Tiempo				Tiempo
55		32			15			None					None			No				No
56		32			15			LVD 1					LVD 1			LVD1				LVD1
57		32			15			LVD 2					LVD 2			LVD2				LVD2
103		32			15			High Temp Disconnect 1			HTD 1			Desconexión Alta Temp1		Desc Alta T1
104		32			15			High Temp Disconnect 2			HTD 2		Desconexión Alta Temp2		Desc Alta T2
105		32			15			Battery LVD				Battery LVD		LVD Batería			LVD Batería
106		32			15			No Battery				No Battery		Sin Batería			Sin Batería
107		32			15			LVD 1					LVD 1			LVD 1				LVD 1
108		32			15			LVD 2					LVD 2			LVD 2				LVD 2
109		32			15			Battery Always On			Batt Always On		Batería siempre conectada	Siempre con Bat
110		32			15			LVD Contactor Type			LVD Type		Tipo contactor LVD		Tipo Contac LVD
111		32			15			Bistable				Bistable		Biestable			Biestable
112		32			15			Mono-Stable				Mono-Stable	Monoestable			Monoestable
113		32			15			Mono w/Sample				Mono w/Sample		Estable con señal		Estable Señal
116		32			15			LVD 1 Disconnect			LVD1 Disconnect		LVD1 desconectado		LVD1 Abierto
117		32			15			LVD 2 Disconnect			LVD2 Disconnect	LVD2 desconectado		LVD2 Abierto
118		32			15			LVD 1 Mono w/Sample			LVD1 Mono Sampl		LVD1 Estable con señal		LVD1 Est.Señal
119		32			15			LVD 2 Mono w/Sample			LVD2 Mono Sampl		LVD2 Estable con señal		LVD2 Est.Señal
125		32			15			State					State			Estado				Estado
126		32			15			LVD 1 Voltage (24V)			LVD 1 Voltage		Tensión LVD1(24V)		Tensión LVD1
127		32			15			LVD 1 Reconnect Voltage (24V)		LVD1 Recon Volt		Tensión Reconex LVD1(24V)	V Reconex LVD1
128		32			15			LVD 2 Voltage (24V)			LVD 2 Voltage		Tensión LVD2 (24V)		Tensión LVD2
129		32			15			LVD 2 Reconnect Voltage (24V)		LVD2 Recon Volt		Tensión Reconex LVD2 (24V)	V Reconex LVD2
130		32			15			LVD 1 Control				LVD 1			Control LVD1			LVD1
131		32			15			LVD 2 Control				LVD 2			Control LVD2			LVD2
132		32			15			LVD 1 Reconnect				LVD 1 Reconnect		Reconectar LVD1			Recon LVD1
133		32			15			LVD 2 Reconnect				LVD 2 Reconnect		Reconectar LVD2			Recon LVD2
134		32			15			HTD Point				HTD Point		Nivel desconexión alta Temp	Desc Alta Temp
135		32			15			HTD Reconnect Point			HTD Recon Point		Nivel reconexión alta Temp	Recon Alta Temp
136		32			15			HTD Temperature Sensor			HTD Temp Sensor		Sensor Desconex alta Temp	Sens Des Alta T
137		32			15			Ambient Temperature			Ambient Temp		Ambiente			Ambiente
138		32			15			Battery Temperature			Battery Temp		Batería				Batería
139		32			15			LVD Synchronize				LVD Synchronize		Sincronización LVD		Sincronizar LVD
140		32			15			Relay for LVD3				Relay for LVD3		Relé para LVD3			Relé para LVD3
141		32			15			Relay Output 1				Relay Output 1		Salida Relé 1			Salida Relé 1
142		32			15			Relay Output 2				Relay Output 2		Salida Relé 2			Salida Relé 2
143		32			15			Relay Output 3				Relay Output 3		Salida Relé 3			Salida Relé 3
144		32			15			Relay Output 4				Relay Output 4		Salida Relé 4			Salida Relé 4
145		32			15			Relay Output 5				Relay Output 5		Salida Relé 5			Salida Relé 5
146		32			15			Relay Output 6				Relay Output 6		Salida Relé 6			Salida Relé 6
147		32			15			Relay Output 7				Relay Output 7		Salida Relé 7			Salida Relé 7
148		32			15			Relay Output 8				Relay Output 8		Salida Relé 8			Salida Relé 8
149		32			15			Relay Output 14				Relay Output 14		Salida Relé 14			Salida Relé 14
150		32			15			Relay Output 15				Relay Output 15		Salida Relé 15			Salida Relé 15
151		32			15			Relay Output 16				Relay Output 16		Salida Relé 16			Salida Relé 16
152		32			15			Relay Output 17				Relay Output 17		Salida Relé 17			Salida Relé 17
153		32			15			LVD 3 Enable				LVD 3 Enable		LVD3 Sí				LVD3 Sí

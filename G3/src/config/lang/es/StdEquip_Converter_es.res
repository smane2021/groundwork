﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
es



[RES_INFO]
#RES_ID		MAX_LEN_FULL	MAX_LEN_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32		15		Converter			Converter		Convertidor			Convertidor
2		32		15		Output Voltage			Output Voltage		Tensión de salida		Tensión salida
3		32		15		Output Current			Output Current		Corriente			Corriente
4		32		15		Temperature			Temperature		Temperatura			Temperatura
5		32		15		Converter High Serial Number	Conv High SN		SN Convertidor Alta		SN Conv Alta
6		32		15		Converter SN			Converter SN		SN Convertidor			SN Conv
7		32		15		Total Running Time		Running Time		Total en operación		Tot operando
8		32		15		Converter ID Overlap		Conv ID Overlap		Cruce ID Convertidor		Cruce ID Conv
9		32		15		Converter Identification Status	Conv ID Status		Estado identificación Conv	Estado ID Conv
10		32		15		Fan Full Speed Status		Fan Full Speed		Max velocidad ventilador	Vmax ventilador
11		32		15		EEPROM Fail Status		EEPROM Fail		Fallo EEPROM			Fallo EEPROM
12		32		15		Thermal Shutdown Status		Thermal SD		Apagado térmico			Apagado térmico
13		32		15		Input Low Voltage Status	Input Low Volt		Baja tensión entrada		Baja Tens Ent
14		32		15		High Ambient Temperature Status	High Amb Temp		Alta temperatura ambiente	Alta Temp Amb
15		32		15		Walk-In				Walk-In		Entrada Suave activada		EntSuave Activ
16		32		15		On/Off Status			On/Off Status		Encendido			Encendido
17		32		15		Stopped Status			Stopped Status		Estado Parada			Parado
18		32		15		Power Limit			Power Limit		Potencia limitada por Temp	Pot Lim x Temp
19		32		15		Over Voltage Status (DC)	Over Volt (DC)			Sobretensión CC			Sobretens CC
20		32		15		Fan Fail Status		Fan Fail		Fallo ventilador		Fallo ventl
21		32		15		Converter Fail Status		Converter Fail		Fallo convertidor		Fallo Convert
22		32		15		Barcode 1			Barcode 1		Código de barras 1		Cod Barras 1
23		32		15		Barcode 2			Barcode 2		Código de barras 2		Cod Barras 2
24		32		15		Barcode 3			Barcode 3		Código de barras 3		Cod Barras 3
25		32		15		Barcode 4			Barcode 4		Código de barras 4		Cod Barras 4
26		32		15		EStop/EShutdown Status		EStop/EShutdown		Parada Emergencia		Parada Emerg
27		32		15		Communication Status		Comm Status		Estado Comunicación		Estado Com
28		32		15		Existence State			Existence State		Detección			Detección
29		32		15		DC On/Off Control		DC On/Off Ctrl		Control CC			Control CC
30		32		15		Converter Reset			Converter Reset			Reinicio sobretensión		Inicio SobreV
31		32		15		LED Control			LED Control		Control LED			Control LED
32		32		15		Converter Reset			Converter Reset		Reinicio Convertidor		Inicio Conv
33		32		15		AC Input Fail			AC Input Fail		Fallo Red			Fallo Red
34		32		15		Over Voltage			Over Voltage		Sobretensión			Sobretens
37		32		15		Current Limit			Current Limit		Límite de corriente		Lim Corriente
39		32		15		Normal				Normal			Normal				Normal
40		32		15		Limited				Limited			Limitado			Limitado
45		32		15		Normal				Normal			Normal				Normal
46		32		15		Full				Full			Completo			Completo
47		32		15		Disabled			Disabled		Deshabilitado			Deshabilitado
48		32		15		Enabled				Enabled			Habilitado			Habilitado
49		32		15		On				On			Conectado			Conectado
50		32		15		Off				Off			Apagado			Apagado
51		32		15		Normal				Normal			Normal				Normal
52		32		15		Fail				Fail			Fallo				Fallo
53		32		15		Normal				Normal			Normal				Normal
54		32		15		Over Temperature		Over Temp		Sobretemperatura		Sobre Temp
55		32		15		Normal				Normal			Normal				Normal
56		32		15		Fail				Fail			Fallo				Fallo
57		32		15		Normal				Normal			Normal				Normal
58		32		15		Protected			Protected		Protegido			Protegido
59		32		15		Normal				Normal			Normal				Normal
60		32		15		Fail				Fail			Fallo				Fallo
61		32		15		Normal				Normal			Normal				Normal
62		32		15		Alarm				Alarm			Alarma				Alarma
63		32		15		Normal				Normal			Normal				Normal
64		32		15		Fail				Fail			Fallo				Fallo
65		32		15		Off				Off			Apagado				Apagado
66		32		15		On				On			Encendido			Encendido
67		32		15		Reset				Reset		Reiniciar			Reiniciar
68		32		15		Normal				Normal		Normal				Normal
69		32		15		Flash				Flash			Intermitente			Intermitente
70		32		15		Stop Flashing			Stop Flashing			Deja de Parpadear		Deja.Parpadear
71		32		15		Off				Off			Apagado				Apagado
72		32		15		Reset				Reset			Reiniciar			Reiniciar
73		32		15		Converter On			Converter On		Conversor de		Conversor de
74		32		15		Converter Off			Converter Off		Cnvertidor Off		Convertidor Off
75		32		15		Off				Off			Apagado				Apagado
76		32		15		LED Control			LED Control			Intermitente			Intermitente
77		32		15		Converter Reset			Converter Reset		Reiniciar Convertidor		Iniciar Conv
80		32		15		Communication Fail		Comm Fail		Fallo de Comunicación		Fallo COM
84		32		15		Converter High Serial Number	Conv High SN		Alto NS Convertidor		Alto NS Conv
85		32		15		Converter Version		Conv Version		Versión Convertidor		Versión Conv
86		32		15		Converter Part Number		Conv Part Num		Num Parte Convertidor		Num Parte Conv
87		32		15		Current Share State		Current Share		Reparto de carga		Reparto Carga
88		32		15		Current Share Alarm		Curr Share Alm	Reparto de carga		Reparto Carga
89		32		15		HVSD Alarm			HVSD Alarm		Sobretensión			Sobretensión
90		32		15		Normal				Normal			Normal				Normal
91		32		15		Over Voltage			Over Voltage		Sobretensión			Sobretensión
92		32		15		Line AB Voltage			Line AB Volt		Tensión R-S			Tensión R-S
93		32		15		Line BC Voltage			Line BC Volt		Tensión S-T			Tensión S-T
94		32		15		Line CA Voltage			Line CA Volt		Tensión R-T			Tensión R-T
95		32		15		Low Voltage			Low Voltage		Baja tensión			Baja tensión
96		32		15		AC Under Voltage Protection	U-Volt Protect		Protección subtensión CA	Protec subVca
97		32		15		Converter ID			Converter ID		Posición Convertidor		Posición Conv
98		32		15		DC Output Shut Off		DC Output Off		Salida CC apagada		Sal CC Apagada
99		32		15		Converter Phase			Converter Phase			Fase Convertidor		Fase Conv
100		32		15		A				A			R				R
101		32		15		B				B			S				S
102		32		15		C				C			T				T
103		32		15		Severe Sharing Current Alarm	SevereCurrShare		Reparto Carga grave		Rep Carga grave
104		32		15		Barcode 1			Barcode 1		Código Barras 1			Código Barras1
105		32		15		Barcode 2			Barcode 2		Código Barras 2			Código Barras2
106		32		15		Barcode 3			Barcode 3		Código Barras 3			Código Barras3
107		32		15		Barcode 4			Barcode 4		Código Barras 4			Código Barras4
108		32		15		Converter Communication Fail	Conv Comm Fail		Fallo Convertidor		Fallo Conv
109		32		15		No				No			No				No
110		32		15		Yes				Yes			Sí			Sí
111		32		15		Existence State			Existence State			Detección			Detección
112		32		15		Converter Fail		Converter Fail		Fallo convertidor		Fallo Conv
113		32		15		Communication OK		Comm OK			Comunicación correcta		Com bien
114		32		15		All Converters Comm Fail	AllConvCommFail		Rectificadores no responden	Recs no Resp
115		32		15			Communication Fail		Comm Fail		Fallo de Comunicación		Fallo COM
116		32		15			Valid Rated Current		Rated Current		Corriente Validada			Corriente Est
117		32		15			Efficiency			Efficiency		Eficiencia			Eficiencia
118		32		15			Input Rated Voltage		Input RatedVolt		Tensión Entrada Estimada			V Entrada Est		
119		32		15			Output Rated Voltage		OutputRatedVolt		Tensión Salida Estimada			V Salida Est
120		32		15			LT 93				LT 93			LT 93				LT 93
121		32		15			GT 93				GT 93			GT 93				GT 93
122		32		15			GT 95				GT 95			GT 95				GT 95
123		32		15			GT 96				GT 96			GT 96				GT 96
124		32		15			GT 97				GT 97			GT 97				GT 97
125		32		15			GT 98				GT 98			GT 98				GT 98
126		32		15			GT 99				GT 99			GT 99				GT 99
276		32		15		EStop/EShutdown			EStop/EShutdown	Parada emergencia		Parada Emergen
277		32		15		Fan Fail			Fan Fail		Fallo ventilador		Fallo vent
278		32		15		Input Low Voltage		Input Low Volt		Baja tensión Entrada		Baja tensión
279		32		15		Converter ID			Converter ID		Fijar ID de Convertidor		Fijar ID Conv
280		32		15		EEPROM Fail			EEPROM Fail		Fallo EEPROM			Fallo EEPROM
281		32		15		Thermal Shutdown		Thermal SD		Apagado por temperatura		Apagado Térmico
282		32		15		High Temperature		High Temp		Alta Temperatura		Alta Temperat
283		32		15		Thermal Power Limit		Therm Power Lmt		Thermal Power Limit		Therm Power Lmt
284		32		15		Fan Fail			Fan Fail	Fan Fail			Fan Fail
285		32		15		Converter Fail			Converter Fail	Converter Fail			Converter Fail
286		32		15		Mod ID Overlap			Mod ID Overlap		ID de Convertido duplicada	ID Conv duplic
287		32		15		Low Input Volt			Low Input Volt		Baja tensión entrada		Baja Tens Ent
288		32		15		Under Voltage			Under Voltage		Subtensión			Subtensión
289		32		15		Over Voltage			Over Voltage	Sobretensión			Sobretensión
290		32		15		Over Current			Over Current		Sobrecorriente			Sobrecorriente
291		32		15			GT 94				GT 94			GT 94				GT 94
292		32		15			Under Voltage(24V)		Under Volt(24V)		Subtensión (24V)				Subtensión
293		32		15			Over Voltage(24V)		Over Volt(24V)		Sobretensión (24V)				Sobretensión
294		32		15			Converter Summary Alarm		ConvSummaryAlm	Resumen de alarmas del conv.	Resum.Alm.Conv.

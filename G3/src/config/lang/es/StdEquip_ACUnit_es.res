﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Phase A Voltage				R						R				R
2	32			15			Phase B Voltage		S						S				S
3	32			15			Phase C Voltage		T						T				T
4	32			15			Line AB Voltage		R			Tension R-S			R-S
5	32			15			Line BC Voltage		S			Tension S-T			S-T
6	32			15			Line CA Voltage		T			Tension R-T			R-T
7	32			15			Phase A Current		Phase A Current		Corriente Fase R		Corriente R	
8	32			15			Phase B Current		Phase B Current		Corriente Fase S		Corriente S	
9	32			15			Phase C Current		Phase C Current		Corriente Fase T		Corriente T	
10	32			15			AC Frequency		AC Frequency		Frecuencia CA			Frecuencia CA
11	32			15		Total Real Power		Total Real Pwr		Potencia real total		Potencia real
12	32			15			Phase A Real Power		PH-A Real Power		Potencia real fase R		Pot real R
13	32			15			Phase B Real Power		PH-B Real Power		Potencia real fase S		Pot real S
14	32			15			Phase C Real Power		PH-C Real Power		Potencia real fase T		Pot real T
15	32			15			Total Reactive Power		Total React Pwr			Potencia reactiva total		Pot reactiva
16	32			15			Phase A Reactive Power		PH-A React Pwr			Potencia reactiva R		Pot reactiva R
17	32			15			Phase B Reactive Power		PH-B React Pwr			Potencia reactiva S		Pot reactiva S
18	32			15			Phase C Reactive Power		PH-C React Pwr			Potencia reactiva T		Pot reactiva T
19	32			15			Total Apparent Power		Total Appar Pwr			Potencia aparente total	Pot aparente
20	32			15			Phase A Apparent Power		PH-A Appar Pwr		Potencia aparente fase R	Pot aparente R
21	32			15			Phase B Apparent Power		PH-B Appar Pwr		Potencia aparente fase S	Pot aparente S
22	32			15			Phase C Apparent Power		PH-C Appar Pwr		Potencia aparente fase T	Pot aparente T
23	32			15			Power Factor		Power Factor		Factor de potencia		Factor potencia
24	32			15			Phase A Power Factor		PH-A Pwr Factor		Factor potencia fase R		Factor pot R
25	32			15			Phase B Power Factor		PH-B Pwr Factor		Factor potencia fase S		Factor pot S
26	32			15			Phase C Power Factor		PH-C Pwr Factor		Factor potencia fase T		Factor pot T
27	32			15			Phase A Current Crest Factor		Ia Crest Factor		Factor cresta corriente R	Fact cresta IR
28	32			15			Phase B Current Crest Factor		Ib Crest Factor		Factor cresta corriente S	Fact cresta IS
29	32			15			Phase C Current Crest Factor		Ic Crest Factor		Factor cresta corriente T	Fact cresta IT
30	32			15			Phase A Current THD		PH-A Curr THD		THD corriente fase R		THD I fase R
31	32			15			Phase B Current THD		PH-B Curr THD		THD corriente fase S		THD I fase S
32	32			15			Phase C Current THD		PH-C Curr THD		THD corriente fase T		THD I fase T
33	32			15			Phase A Voltage THD		PH-A Volt THD		THD tensión fase R		THD V fase R
34	32			15			Phase B Voltage THD		PH-B Volt THD		THD tensión fase S		THD V fase S
35	32			15			Phase C Voltage THD		PH-C Volt THD		THD tensión fase T		THD V fase T
36	32			15			Total Real Energy		TotalRealEnergy	Energía Real total		Energia Real
37	32			15			Total Reactive Energy		TotalReacEnergy			Energía reactiva total		Energia react
38	32			15			Total Apparent Energy		TotalAppaEnergy		Energía aparente total		Energ Aparente
39	32			15			Ambient Temperature		Ambient Temp	Temperatura ambiente		Temp ambiente
40	32			15			Nominal Line Voltage			NominalLineVolt			Tensión nominal sistema		Tension nominal
41	32			15			Nominal Phase Voltage			Nominal PH-Volt				Tensión nominal de fase		Tens nom fase
42	32			15			Nominal Frequency			Nominal Freq		Frecuencia nominal		Frecuencia nom
43	32			15			Mains Failure Alarm Limit 1		Mains Fail Alm1		Umbral alarma Fallo Red 1	Alarm FalloCA1
44	32			15			Mains Failure Alarm Limit 2		Mains Fail Alm2		Umbral alarma Fallo Red 2	Alarm FalloCA2
45	32			15			Voltage Alarm Limit 1			Volt Alarm Lmt1		Umbral alarma tensión 1		Umb alarma V 1
46	32			15			Voltage Alarm Limit 2			Volt Alarm Lmt2		Umbral alarma tensión 2		Umb alarma V 2
47	32			15			Frequency Alarm Limit			Freq Alarm Lmt		Umbral alarma frecuencia	Umb alm frec
48	32			15			High Temperature Limit			High Temp Limit		Límite Alta Temperatura		Lim alta temp
49	32			15			Low Temperature Limit			Low Temp Limit		Límite Baja Temperatura		Lim baja temp
50	32			15			Supervision Fail			SupervisionFail	Fallo supervisión		Fallosupervsn
51	32			15			Line AB Over Voltage 1			L-AB Over Volt1		Alta tensión R-S		Alta tens R-S
52	32			15			Line AB Over Voltage 2			L-AB Over Volt2		Muy alta tensión R-S		Muy alta V R-S
53	32			15			Line AB Under Voltage 1			L-AB UnderVolt1		Baja tensión R-S		Baja tens R-S
54	32			15			Line AB Under Voltage 2			L-AB UnderVolt2		Muy baja tensión R-S		Muy baja V R-S
55	32			15			Line BC Over Voltage 1			L-BC Over Volt1		Alta tensión S-T		Alta tens S-T
56	32			15			Line BC Over Voltage 2			L-BC Over Volt2		Muy alta tensión S-T		Muy alta V S-T
57	32			15			Line BC Under Voltage 1			L-BC UnderVolt1		Baja tensión S-T		Baja tens S-T
58	32			15			Line BC Under Voltage 2			L-BC UnderVolt2		Muy baja tensión S-T		Muy baja V S-T
59	32			15			Line CA Over Voltage 1			L-CA Over Volt1		Alta tenisón R-T		Alta tens R-T
60	32			15			Line CA Over Voltage 2			L-CA Over Volt2		Muy alta tensión R-T		Muy alta V R-T
61	32			15			Line CA Under Voltage 1			L-CA UnderVolt1		Baja tensión R-T		Baja tens R-T
62	32			15			Line CA Under Voltage 2			L-CA UnderVolt2		Muy baja tensión R-T		Muy baja V R-T
63	32			15			Phase A Over Voltage 1			PH-A Over Volt1		Alta tensión fase R		Alta tension R
64	32			15			Phase A Over Voltage 2			PH-A Over Volt2		Muy alta tensión fase R		Muy alta tens R
65	32			15			Phase A Under Voltage 1			PH-A UnderVolt1		Baja tensión fase R		Baja tension R
66	32			15			Phase A Under Voltage 2			PH-A UnderVolt2		Muy baja tensión fase R		Muy baja tens R
67	32			15			Phase B Over Voltage 1			PH-B Over Volt1		Alta tensión fase S		Alta tension S
68	32			15			Phase B Over Voltage 2			PH-B Over Volt2		Muy alta tensión fase S		Muy alta tens S
69	32			15			Phase B Under Voltage 1			PH-B UnderVolt1		Baja tensión fase S		Baja tension S
70	32			15			Phase B Under Voltage 2			PH-B UnderVolt2		Muy baja tensión S		Muy baja tens S
71	32			15			Phase C Over Voltage 1			PH-C Over Volt1		Alta tensión fase T		Alta tension T
72	32			15			Phase C Over Voltage 2			PH-C Over Volt2		Muy alta tensión fase T		Muy alta tens T
73	32			15			Phase C Under Voltage 1			PH-C UnderVolt1		Baja tensión fase T		Baja tension T
74	32			15			Phase C Under Voltage 2			PH-C UnderVolt2		Muy baja tensión fase T		Muy baja tens T
75	32			15			Mains Failure				Mains Failure		Fallo de Red			Fallo de Red
76	32			15			Severe Mains Failure			SevereMainsFail	Fallo de Red severo		Fallo Red sever
77	32			15			High Frequency				High Frequency		Alta frecuencia			Alta frecuencia
78	32			15			Low Frequency				Low Frequency		Baja frecuencia			Baja frecuencia
79	32			15			High Temperature			High Temp		Alta temperatura		Alta temp
80	32			15			Low Temperature				Low Temperature		Baja temperatura		Baja temp
81	32			15			Rectifier AC				Rectifier AC		Alterna Rectificadores		CA Rectificador
82	32			15			Supervision Fail			SupervisionFail	Fallo supervisión		Fallosupervsn
83	32			15			No					No			No				No
84	32			15			Yes					Yes			Sí				Sí
85	32			15			Phase A Mains Failure Counter		PH-A Fail Count	Contador Fallos fase R		Cont fallos R
86	32			15			Phase B Mains Failure Counter		PH-B Fail Count	Contador Fallos fase S		Cont fallos S
87	32			15			Phase C Mains Failure Counter		PH-C Fail Count	Contador Fallos fase T		Cont fallos T
88	32			15			Frequency Failure Counter		FreqFailCounter		Contador fallos frecuencia	Cont fallosFrec
89	32			15			Reset Phase A Mains Fail Counter	RstPH-AFailCnt	Iniciar cont Fallos fase R	Inic fallos R
90	32			15			Reset Phase B Mains Fail Counter	RstPH-BFailCnt	Iniciar cont Fallos fase S	Inic fallos S
91	32			15			Reset Phase C Mains Fail Counter	RstPH-CFailCnt	Iniciar cont Fallos fase T	Inic fallos T
92	32			15			Reset Frequency Fail Counter	RstFreqFailCnt	Iniciar cont Fallos fre.	Inic fallosFrec
93	32			15			Current Alarm Limit			Curr Alm Limit	Umbral alarma de corriente	Umb alarm corr
94	32			15			Phase A High Current			PH-A High Curr		Alta corriente fase R		Alta I fase R
95	32			15			Phase B High Current			PH-B High Curr		Alta corriente fase S		Alta I fase S
96	32			15			Phase C High Current			PH-C High Curr		Alta corriente fase T		Alta I fase T
97	32			15			Minimum Phase Voltage			Min Phase Volt		Tensión mínima de fase		Tens min fase
98	32			15			Maximum Phase Voltage			Max Phase Volt		Tensión máxima de fase		Tens max fase
99	32			15			Raw Data 1				Raw Data 1		Datos en bruto 1		Datos brutos 1
100	32			15			Raw Data 2				Raw Data 2		Datos en bruto 2		Datos brutos 2
101	32			15			Raw Data 3				Raw Data 3		Datos en bruto 3		Datos brutos 3
102	32			15			Reference Voltage			Reference Volt		Tensión de referencia		Tensión ref
103	32			15			State					State			Estado				Estado
104	32			15			Off					Off			Apagado				Apagado
105	32			15			On					On			Conectado			Conectado
106	32			15			High Phase Voltage			High Ph-Volt		Alta tensión de Fase		Alta V Fase
107	32			15			Very High Phase Voltage		VHigh Ph-Volt		Muy alta tensión de fase	Muy alta V Fase
108	32			15			Low Phase Voltage			Low Ph-Volt		Baja tensión de fase		Baja V Fase
109	32			15			Very Low Phase Voltage			VLow Ph-Volt		Muy baja tensión de fase	Muy baja V Fase
110	32			15			All Rectifiers Comm Fail		AllRectCommFail		Fallo comunicación Rectificador	Rects No Resp
120		32			32			Input PhaseA Current		Input PhaseA Current			Corriente de fase A		Corriente de fase A
121		32			32			Input PhaseB Current		Input PhaseB Current			Corriente de fase B		Corriente de fase B
122		32			32			Input PhaseC Current		Input PhaseC Current			Corriente de fase C		Corriente de fase C

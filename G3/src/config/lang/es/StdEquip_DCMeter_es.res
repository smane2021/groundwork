﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#ResID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
##======================================================12345678901234567890123456789012--------123456789012345---------123456789012345678901234567890--123456789012345
1	32			15			Bus Bar Voltage				Bus Bar Voltage		Tensión (bus)			Tensión bus
2	32			15			Channel 1 Current			Channel1 Curr		Corriente Canal 1		Corr Canal 1
3	32			15			Channel 2 Current			Channel2 Curr		Corriente Canal 2		Corr Canal 2
4	32			15			Channel 3 Current			Channel3 Curr		Corriente Canal 3		Corr Canal 3
5	32			15			Channel 4 Current			Channel4 Curr		Corriente Canal 4		Corr Canal 4
6	32			15			Channel 1 Energy Consumption		Channel1 Energy		Consumo Energía Canal 1		Consumo Canal1
7	32			15			Channel 2 Energy Consumption		Channel2 Energy		Consumo Energía Canal 2		Consumo Canal2
8	32			15			Channel 3 Energy Consumption		Channel3 Energy		Consumo Energía Canal 3		Consumo Canal3
9	32			15			Channel 4 Energy Consumption		Channel4 Energy		Consumo Energía Canal 4		Consumo Canal4
10	32			15			Channel 1			Channel1	Canal 1		Canal1 
11	32			15			Channel 2			Channel2	Canal 2		Canal2 
12	32			15			Channel 3			Channel3	Canal 3		Canal3 
13	32			15			Channel 4			Channel4	Canal 4		Canal4 
14	32			15			Clear Channel 1 Energy			ClrChan1Energy		Iniciar Consumo Canal 1		Inic Canal 1
15	32			15			Clear Channel 2 Energy			ClrChan2Energy		Iniciar Consumo Canal 2		Inic Canal 2
16	32			15			Clear Channel 3 Energy			ClrChan3Energy		Iniciar Consumo Canal 3		Inic Canal 3
17	32			15			Clear Channel 4 Energy			ClrChan4Energy		Iniciar Consumo Canal 4		Inic Canal 4
18	32			15			Shunt 1 Voltage				Shunt1 Volt		Tensión Shunt 1			Tensión Shunt1
19	32			15			Shunt 1 Current				Shunt1 Curr		Corriente Shunt 1		Corrien Shunt1
20	32			15			Shunt 2 Voltage				Shunt2 Volt		Tensión Shunt 2			Tensión Shunt2
21	32			15			Shunt 2 Current				Shunt2 Curr		Corriente Shunt 2		Corrien Shunt2
22	32			15			Shunt 3 Voltage				Shunt3 Volt		Tensión Shunt 3			Tensión Shunt3
23	32			15			Shunt 3 Current				Shunt3 Curr		Corriente Shunt 3		Corrien Shunt3
24	32			15			Shunt 4 Voltage				Shunt4 Volt		Tensión Shunt 4			Tensión Shunt4
25	32			15			Shunt 4 Current				Shunt4 Curr		Corriente Shunt 4		Corrien Shunt4
26	32			15			Enabled					Enabled			Sí				Sí
27	32			15			Disabled				Disabled		No				No
28	32			15			Existence State				Exist State		Detección			Detección
29	32			15			Communication Fail			Comm Fail	Fallo de comunicación		Fallo COM
30	32			15			DC Meter				DC Meter		Contador CC			Contador CC
31	32			15			Clear					Clear			Sí				Sí

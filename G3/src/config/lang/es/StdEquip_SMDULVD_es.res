﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			LVD Unit			LVD			LVD Extensión			LVD Ext
2		32			15			SMDU LVD			SMDU LVD		SMDU LVD			SMDU LVD
11		32			15			Connected			Connected		Cerrado				Cerrado
12		32			15			Disconnected			Disconnected		Abierto				Abierto
13		32			15			No				No			No				No
14		32			15			Yes				Yes			Sí				Sí
21		32			15			LVD 1 Status			LVD 1 Status	Estado LVD1			Estado LVD1
22		32			15			LVD 2 Status			LVD 2 Status	Estado LVD2			Estado LVD2
23		32			15			LVD 1 Disconnect		LVD1 Disconnect		Abierto LVD1			Abierto LVD1
24		32			15			LVD 2 Disconnect		LVD2 Disconnect		Abierto LVD2			Abierto LVD2
25		32			15			Communication Fail		Comm Fail		Fallo Comunicación		Fallo COM
26		32			15			State				State			Estado				Estado
27		32			15			LVD 1 Control			LVD 1 Control		Control LVD1			Control LVD1
28		32			15			LVD 2 Control			LVD 2 Control		Control LVD2			Control LVD2
31		32			15			LVD 1				LVD 1			Desconexión LVD1		Función LVD1
32		32			15			LVD 1 Mode			LVD 1 Mode		Modo LVD1			Modo LVD1
33		32			15			LVD 1 Voltage			LVD 1 Voltage		Tensión LVD1			Tensión LVD1
34		32			15			LVD 1 Reconnect Voltage		LVD1 Recon Volt		Tensión reconexión LVD1		Tens recon LVD1
35		32			15			LVD 1 Reconnect Delay		LVD1 ReconDelay		Retardo reconexión LVD1		Ret recon LVD1
36		32			15			LVD 1 Time			LVD 1 Time		Tiempo LVD1			Tiempo LVD1
37		32			15			LVD 1 Dependency			LVD1 Dependency		Dependencia LVD1		Depend LVD1
41		32			15			LVD 2				LVD 2			Desconexión LVD2		Función LVD2
42		32			15			LVD 2 Mode			LVD 2 Mode		Modo LVD2			Modo LVD2
43		32			15			LVD 2 Voltage			LVD 2 Voltage		Tensión LVD2			Tensión LVD2
44		32			15			LVD 2 Reconnect Voltage		LVD2 Recon Volt		Tensión reconexión LVD2		Tens recon LVD2
45		32			15			LVD 2 Reconnect Delay		LVD2 ReconDelay		Retardo reconexión LVD2		Ret recon LVD2
46		32			15			LVD 2 Time			LVD 2 Time		Tiempo LVD2			Tiempo LVD2
47		32			15			LVD 2 Dependency			LVD2 Dependency		Dependencia LVD2		Depend LVD2
51		32			15			Disabled			Disabled		Deshabilitada			Deshabilitada
52		32			15			Enabled				Enabled			Habilitada			Habilitada
53		32			15			Voltage				Voltage			Tensión				Tensión
54		32			15			Time				Time			Tiempo				Tiempo
55		32			15			None				None			No				No
56		32			15			LVD 1				LVD 1			LVD1			LVD1
57		32			15			LVD 2				LVD 2			LVD2			LVD2
103		32			15			High Temperature Disconnect 1	HTD 1			Habilitar HTD1			Habilitar HTD1
104		32			15			High Temperature Disconnect 2	HTD 2			Habilitar HTD2			Habilitar HTD2
105		32			15			Battery LVD			Batt LVD		LVD de batería			LVD de batería
110		32			15			Communication Fail		Comm Fail		Fallo Comunicación	Fallos COM
111		32			15			Times of Communication Fail	Times Comm Fail	Fallos de Comunicación			Fallos COM
116		32			15			LVD 1 Contactor Fail	LVD 1 Fail		Fallo Contactor LVD1		Fallo Cont LVD1
117		32			15			LVD 2 Contactor Fail	LVD 2 Fail		Fallo Contactor LVD2		Fallo Cont LVD2
118		32			15			LVD 1 Voltage (24V)		LVD 1 Voltage		Tensión LVD1(24V)			Tensión LVD1
119		32			15			LVD 1 Reconnect Voltage (24V)	LVD1 Recon Volt		Tensión reconexión LVD1(24V)		Tens recon LVD1
120		32			15			LVD 2 Voltage (24V)		LVD 2 Voltage	Tensión LVD2(24V)	Tensión LVD2
121		32			15			LVD 2 Reconnect Voltage (24V)	LVD2 Recon Volt		Tensión reconexión LVD2(24V)		Tens recon LVD2

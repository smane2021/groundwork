﻿#
#  Locale language support:spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			24V Converter Group		24V Conv Group		Grupo Convertidores 24V	Grupo Conv24V
19		32			15			Shunt 3 Rated Current		Shunt 3 Current		Corriente Shunt		Corr shunt3
21		32			15			Shunt 3 Rated Voltage		Shunt 3 Voltage		Tensión Shunt		V shunt3
26		32			15			Closed				Closed			Cerrado			Cerrado
27		32			15			Open				Open			Abierto			Abierto
29		32			15			No				No			No			No
30		32			15			Yes				Yes			Sí			Sí
3002		32			15			Converter Installed		Conv Installed		Convertidor instalado		Conv instalado

3003		32			15			No				No			No			No
3004		32			15			Yes				Yes			Sí			Sí
3005		32			15			Under Voltage			Under Volt		Subtensión			Subtensión
3006		32			15			Over Voltage			Over Volt		Sobretensión			Sobretensión
3007		32			15			Over Current			Over Current		Sobrecorriente			Sobrecorriente
3008		32			15			Under Voltage			Under Volt		Subtensión			Subtensión
3009		32			15			Over Voltage			Over Volt		Sobretensión			Sobretensión
3010		32			15			Over Current			Over Current		Sobrecorriente			Sobrecorriente
3011		32			15			Voltage				Voltage			Tensión			Tensión
3012		32			15			Total Current			Total Current		Corriente Total			Corriente Total
3013		32			15			Input Current			Input Current		Corriente Entrada		Corri. Entrada
3014		32			15			Efficiency			Efficiency		Eficiencia			Eficiencia


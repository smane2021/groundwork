﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE			
1		32			15			Fuse 1 Voltage		Fuse 1 Voltage		Tensión fusible 1		Tens fus 1
2		32			15			Fuse 2 Voltage		Fuse 2 Voltage		Tensión fusible 2		Tens fus 2
3		32			15			Fuse 3 Voltage		Fuse 3 Voltage		Tensión fusible 3		Tens fus 3
4		32			15			Fuse 4 Voltage		Fuse 4 Voltage		Tensión fusible 4		Tens fus 4
5		32			15			Batt Fuse 1 Alarm	Batt Fuse 1 Alm		Alarma fusible 1		Alarma fus1
6		32			15			Batt Fuse 2 Alarm	Batt Fuse 2 Alm		Alarma fusible 2		Alarma fus2
7		32			15			Batt Fuse 3 Alarm	Batt Fuse 3 Alm		Alarma fusible 3		Alarma fus3
8		32			15			Batt Fuse 4 Alarm	Batt Fuse 4 Alm		Alarma fusible 4		Alarma fus4
9		32			15			SMDU4 Battery Fuse Unit	SMDU4 Bat Fuse		Fusible Batería SMDU4		Fus Bat SMDU4
10		32			15			On			On			Conectado			Conectado
11		32			15			Off			Off			Desconectado			Desconectado
12		32			15			Fuse 1 Status		Fuse 1 Status		Estado fusible batería 1	Estado fus bat1
13		32			15			Fuse 2 Status		Fuse 2 Status		Estado fusible batería 2	Estado fus bat2
14		32			15			Fuse 3 Status		Fuse 3 Status		Estado fusible batería 3	Estado fus bat3
15		32			15			Fuse 4 Status		Fuse 4 Status		Estado fusible batería 4	Estado fus bat4
16		32			15			State			State			Estado				Estado
17		32			15			Normal			Normal			Normal				Normal
18		32			15			Low			Low			Bajo				Bajo
19		32			15			High			High			Alto				Alto
20		32			15			Very Low		Very Low		Muy bajo			Muy bajo
21		32			15			Very High		Very High		Muy alto			Muy alto
22		32			15			On			On			Conectado			Conectado
23		32			15			Off			Off			Desconectado			Desconectado
24		32			15			Communication Fail	Comm Fail		Interrupción Comunicación	Interrup COM
25		32			15			Times of Communication Fail	Times Comm Fail		Núm de Interrupciones		Interrupciones
26		32			15			Fuse 5 Status		Fuse 5 Status		Estado fusible batería 5	Estado fus bat5	
27		32			15			Fuse 6 Status		Fuse 6 Status		Estado fusible batería 6	Estado fus bat6	
28		32			15			Batt Fuse 5 Alarm	Batt Fuse 5 Alm		Alarma fusible 5		Alarma fus5	
29		32			15			Batt Fuse 6 Alarm	Batt Fuse 6 Alm		Alarma fusible 6		Alarma fus6		

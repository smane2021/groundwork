﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			SMDUP Group			SMDUP Group		Grupo SMDUP			Grupo SMDUP
2		32			15			Standby				Standby			Alerta				Alerta
3		32			15			Refresh				Refresh			Refrescar			Refrescar
4		32			15			Setting Refresh			Setting Refresh		Configurar Refresco		Ajuste Refresco
5		32			15			E-Stop			E-Stop			Función Parada Emergencia	Parada Emergen
6		32			15			Yes				Yes			Sí				Sí
7		32			15			Existence State			Existence State		Detección			Detección
8		32			15			Existent			Existent		Existente			Existente
9		32			15			Not Existent		Not Existent		Inexistente			Inexistente
10		32			15			Number of SMDUPs	Num of SMDUPs		Número de SMDUP			Núm SMDUP
11		32			15			SMDU Config Changed	Config Changed		Configuración SMDUP cambiada	Config Cambiada
12		32			15			Not Changed			Not Changed		No cambiada			No cambiada
13		32			15			Changed				Changed			Cambiada			Cambiada
14		32			15			SMDUP Total Curr			SMDUP Total Curr			Corriente total		Corriente total

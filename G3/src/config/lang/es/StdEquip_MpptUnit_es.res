﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
es



[RES_INFO]
#ResID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
#-------------------------------------------------------12345678901234567890123456789012--------123456789012345---------12345678901234567890123456789012--------123456789012345
1		32		15			Solar Converter				Solar Conv		Convertidor Solar			Convert Solar
2		32		15			DC Status				DC Status		Estado CC				Estado CC
3		32		15			DC Output Voltage			DC Voltage		Tensión Salida CC			Tensión CC
4		32		15			Origin Current				Origin Current		Corriente Origen			Corr Origen
5		32		15			Temperature				Temperature		Temperatura				Temperatura
6		32		15			Used Capacity				Used Capacity		Capacidad utilizada			Cap usada
7		32		15			Input Voltage				Input Voltage		Tensión de Entrada			Tensión Entrada
8		32		15			DC Output Current			DC Current		Corriente de salida CC			Corriente CC
9		32		15			SN					SN			SN					SN
10		32		15			Total Running Time			Running Time		Tiempo Total en Operación		Tiempo Operando
11		32		15			Communication Fail Count		Comm Fail Count		Communication Fail Count		Comm Fail Count
13		32		15			Derated by Temp				Derated by Temp		Derated by Temp				Derated by Temp
14		32		15			Derated					Derated			Derated					Derated
15		32		15			Full Fan Speed				Full Fan Speed		Full Fan Speed				Full Fan Speed
16		32		15			Walk-In				Walk-In		Walk-In				Walk-In
18		32		15			Current Limit				Current Limit		Límite de Corriente			Lim Corriente
19		32		15			Voltage High Limit			Volt Hi-limit		Voltage High Limit			Volt Hi-limit
20		32		15			Solar Status				Solar Status		Solar Status				Solar Status
21		32		15			Solar Converter Temperature High	SolarConvTempHi		Alta temperatura			Alta Temp
22		32		15			Solar Converter Fail			SolarConv Fail		Fallo Convertidor Solar			Fallo ConvSolar
23		32		15			Solar Converter Protected		SolarConv Prot		Convertidor Protegido			Protegido
24		32		15			Fan Fail			Fan Fail		Falo Ventilador				Fallo Ventila
25		32		15			Current Limit State			Curr Lmt State		Current Limit State			Curr Lmt State
26		32		15			EEPROM Fail				EEPROM Fail		EEPROM Fallo				EEPROM Fallo
27		32		15			DC On/Off Control		DC On/Off Ctrl			Output On/Off Control			DC On/Off Ctrl
29		32		15			LED Control				LED Control		Control LED				Control LED
30		32		15			Solar Converter Reset			SolarConvReset		Reiniciar Convertidor			Reset ConvSolar
31		32		15			Input Fail			Input Fail		Input Voltage Fail			Input Fail
34		32		15			Over Voltage				Over Voltage		Sobretensión				Sobretensión
37		32		15			Current Limit				Current Limit		Límite de Corriente			Lim Corriente
39		32		15			Normal					Normal			Normal					Normal
40		32		15			Limited					Limited			Limitaedo				Limitado
45		32		15			Normal				Normal			Normal				Normal
46		32		15			Full					Full			Full					Full
47		32		15			Disabled				Disabled		Disabled				Disabled
48		32		15			Enabled					Enabled			Enabled					Enabled
49		32		15			On					On			On					On
50		32		15			Off					Off			Off					Off
51		32		15			Day					Day			Día					Día
52		32		15			Fail					Fail			Fallo					Fallo
53		32		15			Normal					Normal			Normal					Normal
54		32		15			Over Temperature		Over Temp		Sobretemperatura			Sobretemp
55		32		15			Normal					Normal			Normal					Normal
56		32		15			Fail					Fail			Fallo					Fallo
57		32		15			Normal					Normal			Normal					Normal
58		32		15			Protected				Protected		Protected				Protected
59		32		15			Normal				Normal			Normal				Normal
60		32		15			Fail					Fail			Fallo					Fallo
61		32		15			Normal				Normal			Normal				Normal
62		32		15			Alarm					Alarm			Alarma					Alarma
63		32		15			Normal				Normal			Normal				Normal
64		32		15			Fail					Fail			Fallo					Fallo
65		32		15			Off					Off			Off					Off
66		32		15			On					On			On					On
67		32		15			Off					Off			Off					Off
68		32		15			On					On			On					On
69		32		15			LED Control				LED Control		Control LED				Control LED
70		32		15			Cancel				Cancel			Cancel				Cancel
71		32		15			Off					Off			Off					Off
72		32		15			Reset					Reset			Reset					Reset
73		32		15			Solar Converter On			Solar Conv On		Solar Converter On			Solar Conv On
74		32		15			Solar Converter Off			Solar Conv Off		Solar Converter Off			Solar Conv Off
75		32		15			Off					Off			Off					Off
76		32		15			LED Control				LED Control		Control LED				Control LED
77		32		15			Solar Converter Reset			SolarConv Reset		Solar Converter Reset			SolarConv Reset
80		34		15			Solar Converter Comm Fail	SolConvCommFail		Fallo de comunicación			Fallo COM
84		32		15			Solar Converter High SN			SolConvHighSN		Solar Converter High SN			SolConvHighSN
85		32		15			Solar Converter Version			SolarConv Ver		Solar Converter Version			SolarConv Ver
86		32		15			Solar Converter Part Number		SolConv PartNum			Solar Converter Part Number		SolarConvPartNo
87		32		15			Current Share State			Current Share		Current Share State			Current Share
88		32		15			Current Share Alarm			Curr Share Alm		Current Share Alarm			Curr Share Alm
89		32		15			Over Voltage				Over Voltage		Over Voltage				Over Voltage
90		32		15			Normal					Normal			Normal					Normal
91		32		15			Over Voltage				Over Voltage		Over Voltage				Over Voltage
95		32		15			Low Voltage				Low Voltage		Low Voltage				Low Voltage
96		32		15			Input Under Voltage Protection	Low Input		Input Under Voltage Protection	Low Input
97		32		15			Solar Converter ID			Solar Conv ID		Solar Converter ID			Solar Conv ID
98		32		15			DC Output Shut Off			DC Output Off		DC Output Shut Off			DC Output Off¹Ø
99		32		15			Solar Converter Phase			SolarConvPhase		Solar Converter Phase			SolarConvPhase
#-------------------------------------------------------12345678901234567890123456789012--------123456789012345---------12345678901234567890123456789012--------123456789012345
103		32		15			Severe Current Share Alarm		SevereCurrShare		Severe Current Share Alarm		SevereCurrShare
104		32		15			Barcode 1				Barcode 1		Bar Code1				Bar Code1
105		32		15			Barcode 2				Barcode 2		Bar Code2				Bar Code2
106		32		15			Barcode 3				Barcode 3		Bar Code3				Bar Code3
107		32		15			Barcode 4				Barcode 4		Bar Code4				Bar Code4
108		34		15			Solar Converter Comm Fail	SolConvComFail		Solar Converter Communication Fail	SolConvComFail
109		32		15			No					No			No					No
110		32		15			Yes					Yes			Sí					Sí
111		32		15			Existence State				Existence State		Existence State				Existence State
113		32		15			Comm OK					Comm OK			Comm OK					Comm OK
114		32		15			All Solar Converters Comm Fail	All Comm Fail		All Solar Converters Comm Fail		AllSolConvComF
115		32		15			Communication Fail			Comm Fail		Communication Fail			Comm Fail
116		32		15			Valid Rated Current			Rated Current		Valid Rated Current			Rated Current
117		32		15			Efficiency			Efficiency		Efficiency			Efficiency
118		32		15			LT 93					LT 93			LT 93					LT 93
119		32		15			GT 93					GT 93			GT 93					GT 93
120		32		15			GT 95					GT 95			GT 95					GT 95
121		32		15			GT 96					GT 96			GT 96					GT 96
122		32		15			GT 97					GT 97			GT 97					GT 97
123		32		15			GT 98					GT 98			GT 98					GT 98
124		32		15			GT 99					GT 99			GT 99					GT 99
125		32		15			Solar Converter HVSD Status		HVSD Status		Solar Converter HVSD Status		HVSD Status
126		32		15			Solar Converter Reset		SolConvResetEEM	Solar Converter Reset (EEM)		EEMSolConvReset
127		32		15			Night					Night			Noche					Noche
128		32		15			Input Current				Input Current		Input Current				Input Current
129		32		15			Input Power			Input Power		Potencia de Entrada			Potencia Ent
130		32		15			Input Not DC			Input Not DC		La entrada no es CC			Entrada no CC
131		32		15			Output Power			Output Power		Potencia de Salida			Pot Salida


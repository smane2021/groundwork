﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Battery Current				Batt Current		Corriente a batería		Corriente a bat
2	32			15			Battery Rating(Ah)			Batt Rating(Ah)		Capacidad baterías(Ah)		Capacidad bat
3	32			15			Exceed Current Limit			Exceed Curr Lmt		Límite de corriente pasado	Lim corr pasdo
4	32			15			Battery					Battery			Batería				Batería
5	32			15			Over Battery Current			Over Current		Sobrecorriente a batería	Sobrecorr bat
6	32			15			Battery Capacity (%)			Batt Cap (%)	Capacidad batería (%)		Cap bat(%)
7	32			15			Battery Voltage				Batt Voltage		Tensión de baterías		Tensión bat
8	32			15			Low Capacity				Low Capacity		Baja capacidad			Baja capacidad
9	32			15			On					On			Conectado			Conectado
10	32			15			Off					Off			Apagado				Apagado
11	32			15			Battery Fuse Voltage			Batt Fuse Volt		Tensión fusible batería		Tensión Fus Bat
12	32			15			Battery Fuse Status			Batt Fuse State		Estado Fusible			Estado Fusible
13	32			15			Fuse Alarm				Fuse Alarm		Alarma Fusible			Alarma Fusible
14	32			15			SMBRC Battery				SMBRC Battery		SMBRC Bat			SMBRC Bat
15	32			15			State					State			Estado				Estado
16	32			15			Off					Off			Apagado				Apagado
17	32			15			On					On			Conectado			Conectado
18	32			15			Switch					Switch			Switch				Switch
19	32			15			Battery Over Current			Batt Over Curr		Sobrecorriente a batería	Sobrecorr bat
20	32			15			Battery Management			Batt Management		Gestión de Baterías		Gestión Bat
21	32			15			Yes					Yes			Sí				Sí
22	32			15			No					No			No				No
23	32			15			Over Voltage Setpoint			Over Volt Limit		Nivel Sobretensión		Sobretensión
24	32			15			Low Voltage Setpoint			Low Volt Limit		Nivel Baja Tensión		Baja tensión
25	32			15			Battery Over Voltage			Batt Over Volt		Sobretensión Batería		Sobretensión
26	32			15			Battery Under Voltage			Batt Under Volt			Subtensión Batería		Subtensión
27	32			15			Over Current				Over Current	Sobrecorriente			Sobrecorriente
28	32			15			Communication Fail			Comm Fail		Fallo Comunicación		Fallo COM
29	32			15			Times of Communication Fail				Times Comm Fail		Fallos de Comunicación		Fallos COM
44	32			15			Temp Num of Battery			Batt Temp Num		Sensor Temperatura		Sensor Temp
87	32			15			No					No			Ninguno				Ninguno
91	32			15			Battery Temp 1				Batt Temp 1			Temperatura 1 Batería		Temp1 Bat 
92	32			15			Battery Temp 2				Batt Temp 2			Temperatura 2 Batería		Temp2 Bat
93	32			15			Battery Temp 3				Batt Temp 3			Temperatura 3 Batería		Temp3 Bat
94	32			15			Battery Temp 4				Batt Temp 4			Temperatura 4 Batería		Temp4 Bat
95	32			15			Battery Temp 5				Batt Temp 5			Temperatura 5 Batería		Temp5 Bat
96	32			15			Rated Capacity				Rated Capacity		Capacidad Estimada		Capacidad Est
97	32			15			Low					Low			Bajo				Bajo
98	32			15			High					High			Alto				Alto
#99
100	32			15			Overall Voltage				Overall Volt	Tensión total			Tensión total
101	32			15			String Current				String Curr		Corriente cadena Bat		Corr Cadena
102	32			15			Float Current				Float Curr		Corriente Flotación		Corr Flotación
103	32			15			Ripple Current				Ripple Curr		Corriente de rizado		Corr rizado
104	32			15			Cell Number				Cell Number		Número de Batería		Núm Batería
105	32			15			Cell 1 Voltage				Cell 1 Voltage		Tensión de celda 1		Tens celda 1
106	32			15			Cell 2 Voltage				Cell 2 Voltage		Tensión de celda 2		Tens celda 2
107	32			15			Cell 3 Voltage				Cell 3 Voltage		Tensión de celda 3		Tens celda 3
108	32			15			Cell 4 Voltage				Cell 4 Voltage		Tensión de celda 4		Tens celda 4
109	32			15			Cell 5 Voltage				Cell 5 Voltage		Tensión de celda 5		Tens celda 5
110	32			15			Cell 6 Voltage				Cell 6 Voltage		Tensión de celda 6		Tens celda 6
111	32			15			Cell 7 Voltage				Cell 7 Voltage		Tensión de celda 7		Tens celda 7
112	32			15			Cell 8 Voltage				Cell 8 Voltage		Tensión de celda 8		Tens celda 8
113	32			15			Cell 9 Voltage				Cell 9 Voltage		Tensión de celda 9		Tens celda 9
114	32			15			Cell 10 Voltage				Cell 10 Voltage		Tensión de celda 10		Tens celda 10
115	32			15			Cell 11 Voltage				Cell 11 Voltage		Tensión de celda 11		Tens celda 11
116	32			15			Cell 12 Voltage				Cell 12 Voltage		Tensión de celda 12		Tens celda 12
117	32			15			Cell 13 Voltage				Cell 13 Voltage		Tensión de celda 13		Tens celda 13
118	32			15			Cell 14 Voltage				Cell 14 Voltage		Tensión de celda 14		Tens celda 14
119	32			15			Cell 15 Voltage				Cell 15 Voltage		Tensión de celda 15		Tens celda 15
120	32			15			Cell 16 Voltage				Cell 16 Voltage		Tensión de celda 16		Tens celda 16
121	32			15			Cell 17 Voltage				Cell 17 Voltage		Tensión de celda 17		Tens celda 17
122	32			15			Cell 18 Voltage				Cell 18 Voltage		Tensión de celda 18		Tens celda 18
123	32			15			Cell 19 Voltage				Cell 19 Voltage		Tensión de celda 19		Tens celda 19
124	32			15			Cell 20 Voltage				Cell 20 Voltage		Tensión de celda 20		Tens celda 20
125	32			15			Cell 21 Voltage				Cell 21 Voltage		Tensión de celda 21		Tens celda 21
126	32			15			Cell 22 Voltage				Cell 22 Voltage		Tensión de celda 22		Tens celda 22
127	32			15			Cell 23 Voltage				Cell 23 Voltage		Tensión de celda 23		Tens celda 23
128	32			15			Cell 24 Voltage				Cell 24 Voltage		Tensión de celda 24		Tens celda 24
129	32			15			Cell 1 Temperature			Cell 1 Temp	Temperatura celda 1		Temp celda 1
130	32			15			Cell 2 Temperature			Cell 2 Temp	Temperatura celda 2		Temp celda 2
131	32			15			Cell 3 Temperature			Cell 3 Temp	Temperatura celda 3		Temp celda 3
132	32			15			Cell 4 Temperature			Cell 4 Temp	Temperatura celda 4		Temp celda 4
133	32			15			Cell 5 Temperature			Cell 5 Temp	Temperatura celda 5		Temp celda 5
134	32			15			Cell 6 Temperature			Cell 6 Temp	Temperatura celda 6		Temp celda 6
135	32			15			Cell 7 Temperature			Cell 7 Temp	Temperatura celda 7		Temp celda 7
136	32			15			Cell 8 Temperature			Cell 8 Temp	Temperatura celda 8		Temp celda 8
137	32			15			Cell 9 Temperature			Cell 9 Temp	Temperatura celda 9		Temp celda 9
138	32			15			Cell 10 Temperature			Cell 10 Temp		Temperatura celda 10		Temp celda 10
139	32			15			Cell 11 Temperature			Cell 11 Temp		Temperatura celda 11		Temp celda 11
140	32			15			Cell 12 Temperature			Cell 12 Temp		Temperatura celda 12		Temp celda 12
141	32			15			Cell 13 Temperature			Cell 13 Temp		Temperatura celda 13		Temp celda 13
142	32			15			Cell 14 Temperature			Cell 14 Temp		Temperatura celda 14		Temp celda 14
143	32			15			Cell 15 Temperature			Cell 15 Temp		Temperatura celda 15		Temp celda 15
144	32			15			Cell 16 Temperature			Cell 16 Temp		Temperatura celda 16		Temp celda 16
145	32			15			Cell 17 Temperature			Cell 17 Temp		Temperatura celda 17		Temp celda 17
146	32			15			Cell 18 Temperature			Cell 18 Temp		Temperatura celda 18		Temp celda 18
147	32			15			Cell 19 Temperature			Cell 19 Temp		Temperatura celda 19		Temp celda 19
148	32			15			Cell 20 Temperature			Cell 20 Temp		Temperatura celda 20		Temp celda 20
149	32			15			Cell 21 Temperature			Cell 21 Temp		Temperatura celda 21		Temp celda 21
150	32			15			Cell 22 Temperature			Cell 22 Temp		Temperatura celda 22		Temp celda 22
151	32			15			Cell 23 Temperature			Cell 23 Temp		Temperatura celda 23		Temp celda 23
152	32			15			Cell 24 Temperature			Cell 24 Temp		Temperatura celda 24		Temp celda 24
153	32			15			Overall Volt Alarm			Overall VoltAlm			Alarma tensión total		Alarma Tensión
154	32			15			String Current Alarm		String Curr Alm			Alarma corriente Cadena Bat	Corrien Cadena
155	32			15			Float Current Alarm			Float Curr Alm		Alarma corriente Flotación	Corr Flotación
156	32			15			Ripple Current Alarm		Ripple Curr Alm			Alarma corrienet Rizado		Corrien Rizado
157	32			15			Cell Ambient Alarm			Cell Amb Alm			Alarma Ambiente Celda		Ambiente Celda
158	32			15			Cell 1 Voltage Alarm			Cell 1 Volt Alm			Tensión de Celda 1		Tens Celda 1
159	32			15			Cell 2 Voltage Alarm			Cell 2 Volt Alm			Tensión de Celda 2		Tens Celda 2
160	32			15			Cell 3 Voltage Alarm			Cell 3 Volt Alm			Tensión de Celda 3		Tens Celda 3
161	32			15			Cell 4 Voltage Alarm			Cell 4 Volt Alm			Tensión de Celda 4		Tens Celda 4
162	32			15			Cell 5 Voltage Alarm			Cell 5 Volt Alm			Tensión de Celda 5		Tens Celda 5
163	32			15			Cell 6 Voltage Alarm			Cell 6 Volt Alm			Tensión de Celda 6		Tens Celda 6
164	32			15			Cell 7 Voltage Alarm			Cell 7 Volt Alm			Tensión de Celda 7		Tens Celda 7
165	32			15			Cell 8 Voltage Alarm			Cell 8 Volt Alm			Tensión de Celda 8		Tens Celda 8
166	32			15			Cell 9 Voltage Alarm			Cell 9 Volt Alm			Tensión de Celda 9		Tens Celda 9
167	32			15			Cell 10 Voltage Alarm			Cell10 Volt Alm			Tensión de Celda 10		Tens Celda 10
168	32			15			Cell 11 Voltage Alarm			Cell11 Volt Alm			Tensión de Celda 11		Tens Celda 11
169	32			15			Cell 12 Voltage Alarm			Cell12 Volt Alm			Tensión de Celda 12		Tens Celda 12
170	32			15			Cell 13 Voltage Alarm			Cell13 Volt Alm			Tensión de Celda 13		Tens Celda 13
171	32			15			Cell 14 Voltage Alarm			Cell14 Volt Alm			Tensión de Celda 14		Tens Celda 14
172	32			15			Cell 15 Voltage Alarm			Cell15 Volt Alm			Tensión de Celda 15		Tens Celda 15
173	32			15			Cell 16 Voltage Alarm			Cell16 Volt Alm			Tensión de Celda 16		Tens Celda 16
174	32			15			Cell 17 Voltage Alarm			Cell17 Volt Alm			Tensión de Celda 17		Tens Celda 17
175	32			15			Cell 18 Voltage Alarm			Cell18 Volt Alm			Tensión de Celda 18		Tens Celda 18
176	32			15			Cell 19 Voltage Alarm			Cell19 Volt Alm			Tensión de Celda 19		Tens Celda 19
177	32			15			Cell 20 Voltage Alarm			Cell20 Volt Alm			Tensión de Celda 20		Tens Celda 20
178	32			15			Cell 21 Voltage Alarm			Cell21 Volt Alm			Tensión de Celda 21		Tens Celda 21
179	32			15			Cell 22 Voltage Alarm			Cell22 Volt Alm			Tensión de Celda 22		Tens Celda 22
180	32			15			Cell 23 Volt Alarm			Cell23 Volt Alm		Tensión de Celda 23		Tens Celda 23
181	32			15			Cell 24 Volt Alarm			Cell24 Volt Alm		Tensión de Celda 24		Tens Celda 24
182	32			15			Cell 1 Temperature Alarm		Cell 1 Temp Alm				Alarma Temp Celda 1		Temp Celda 1
183	32			15			Cell 2 Temperature Alarm		Cell 2 Temp Alm				Alarma Temp Celda 2		Temp Celda 2
184	32			15			Cell 3 Temperature Alarm		Cell 3 Temp Alm				Alarma Temp Celda 3		Temp Celda 3
185	32			15			Cell 4 Temperature Alarm		Cell 4 Temp Alm				Alarma Temp Celda 4		Temp Celda 4
186	32			15			Cell 5 Temperature Alarm		Cell 5 Temp Alm				Alarma Temp Celda 5		Temp Celda 5
187	32			15			Cell 6 Temperature Alarm		Cell 6 Temp Alm				Alarma Temp Celda 6		Temp Celda 6
188	32			15			Cell 7 Temperature Alarm		Cell 7 Temp Alm				Alarma Temp Celda 7		Temp Celda 7
189	32			15			Cell 8 Temperature Alarm		Cell 8 Temp Alm				Alarma Temp Celda 8		Temp Celda 8
190	32			15			Cell 9 Temperature Alarm		Cell 9 Temp Alm				Alarma Temp Celda 9		Temp Celda 9
191	32			15			Cell 10 Temperature Alarm		Cell10 Temp Alm				Alarma Temp Celda 10		Temp Celda 10
192	32			15			Cell 11 Temperature Alarm		Cell11 Temp Alm				Alarma Temp Celda 11		Temp Celda 11
193	32			15			Cell 12 Temperature Alarm		Cell12 Temp Alm				Alarma Temp Celda 12		Temp Celda 12
194	32			15			Cell 13 Temperature Alarm		Cell13 Temp Alm				Alarma Temp Celda 13		Temp Celda 13
195	32			15			Cell 14 Temperature Alarm		Cell14 Temp Alm				Alarma Temp Celda 14		Temp Celda 14
196	32			15			Cell 15 Temperature Alarm		Cell15 Temp Alm				Alarma Temp Celda 15		Temp Celda 15
197	32			15			Cell 16 Temperature Alarm		Cell16 Temp Alm				Alarma Temp Celda 16		Temp Celda 16
198	32			15			Cell 17 Temperature Alarm		Cell17 Temp Alm				Alarma Temp Celda 17		Temp Celda 17
199	32			15			Cell 18 Temperature Alarm		Cell18 Temp Alm				Alarma Temp Celda 18		Temp Celda 18
200	32			15			Cell 19 Temperature Alarm		Cell19 Temp Alm				Alarma Temp Celda 19		Temp Celda 19
201	32			15			Cell 20 Temperature Alarm		Cell20 Temp Alm				Alarma Temp Celda 20		Temp Celda 20
202	32			15			Cell 21 Temperature Alarm		Cell21 Temp Alm				Alarma Temp Celda 21		Temp Celda 21
203	32			15			Cell 22 Temperature Alarm		Cell22 Temp Alm				Alarma Temp Celda 22		Temp Celda 22
204	32			15			Cell 23 Temperature Alarm		Cell23 Temp Alm				Alarma Temp Celda 23		Temp Celda 23
205	32			15			Cell 24 Temperature Alarm		Cell24 Temp Alm				Alarma Temp Celda 24		Temp Celda 24
206	32			15			Cell 1 Resistance Alarm			Cell 1  Res Alm	Error Resistencia Celda 1	Res Celda 1
207	32			15			Cell 2 Resistance Alarm			Cell 2  Res Alm	Error Resistencia Celda 2	Res Celda 2
208	32			15			Cell 3 Resistance Alarm			Cell 3  Res Alm	Error Resistencia Celda 3	Res Celda 3
209	32			15			Cell 4 Resistance Alarm			Cell 4  Res Alm	Error Resistencia Celda 4	Res Celda 4
210	32			15			Cell 5 Resistance Alarm			Cell 5  Res Alm	Error Resistencia Celda 5	Res Celda 5
211	32			15			Cell 6 Resistance Alarm			Cell 6  Res Alm	Error Resistencia Celda 6	Res Celda 6
212	32			15			Cell 7 Resistance Alarm			Cell 7  Res Alm	Error Resistencia Celda 7	Res Celda 7
213	32			15			Cell 8 Resistance Alarm			Cell 8  Res Alm	Error Resistencia Celda 8	Res Celda 8
214	32			15			Cell 9 Resistance Alarm			Cell 9  Res Alm	Error Resistencia Celda 9	Res Celda 9
215	32			15			Cell 10 Resistance Alarm		Cell 10 Res Alm		Error Resistencia Celda 10	Res Celda 10
216	32			15			Cell 11 Resistance Alarm		Cell 11 Res Alm		Error Resistencia Celda 11	Res Celda 11
217	32			15			Cell 12 Resistance Alarm		Cell 12 Res Alm		Error Resistencia Celda 12	Res Celda 12
218	32			15			Cell 13 Resistance Alarm		Cell 13 Res Alm		Error Resistencia Celda 13	Res Celda 13
219	32			15			Cell 14 Resistance Alarm		Cell 14 Res Alm		Error Resistencia Celda 14	Res Celda 14
220	32			15			Cell 15 Resistance Alarm		Cell 15 Res Alm		Error Resistencia Celda 15	Res Celda 15
221	32			15			Cell 16 Resistance Alarm		Cell 16 Res Alm		Error Resistencia Celda 16	Res Celda 16
222	32			15			Cell 17 Resistance Alarm		Cell 17 Res Alm		Error Resistencia Celda 17	Res Celda 17
223	32			15			Cell 18 Resistance Alarm		Cell 18 Res Alm		Error Resistencia Celda 18	Res Celda 18
224	32			15			Cell 19 Resistance Alarm		Cell 19 Res Alm		Error Resistencia Celda 19	Res Celda 19
225	32			15			Cell 20 Resistance Alarm		Cell 20 Res Alm		Error Resistencia Celda 20	Res Celda 20
226	32			15			Cell 21 Resistance Alarm		Cell 21 Res Alm		Error Resistencia Celda 21	Res Celda 21
227	32			15			Cell 22 Resistance Alarm		Cell 22 Res Alm		Error Resistencia Celda 22	Res Celda 22
228	32			15			Cell 23 Resistance Alarm		Cell 23 Res Alm		Error Resistencia Celda 23	Res Celda 23
229	32			15			Cell 24 Resistance Alarm		Cell 24 Res Alm		Error Resistencia Celda 24	Res Celda 24
230	32			15			Cell 1 Intercell Alarm			Cell 1 InterAlm	Fallo interno Celda 1		Fallo Celda 1
231	32			15			Cell 2 Intercell Alarm			Cell 2 InterAlm	Fallo interno Celda 2		Fallo Celda 2
232	32			15			Cell 3 Intercell Alarm			Cell 3 InterAlm	Fallo interno Celda 3		Fallo Celda 3
233	32			15			Cell 4 Intercell Alarm			Cell 4 InterAlm	Fallo interno Celda 4		Fallo Celda 4
234	32			15			Cell 5 Intercell Alarm			Cell 5 InterAlm	Fallo interno Celda 5		Fallo Celda 5
235	32			15			Cell 6 Intercell Alarm			Cell 6 InterAlm	Fallo interno Celda 6		Fallo Celda 6
236	32			15			Cell 7 Intercell Alarm			Cell 7 InterAlm	Fallo interno Celda 7		Fallo Celda 7
237	32			15			Cell 8 Intercell Alarm			Cell 8 InterAlm	Fallo interno Celda 8		Fallo Celda 8
238	32			15			Cell 9 Intercell Alarm			Cell 9 InterAlm	Fallo interno Celda 9		Fallo Celda 9
239	32			15			Cell 10 Intercell Alarm			Cell10 InterAlm		Fallo interno Celda 10		Fallo Celda 10
240	32			15			Cell 11 Intercell Alarm			Cell11 InterAlm		Fallo interno Celda 11		Fallo Celda 11
241	32			15			Cell 12 Intercell Alarm			Cell12 InterAlm		Fallo interno Celda 12		Fallo Celda 12
242	32			15			Cell 13 Intercell Alarm			Cell13 InterAlm		Fallo interno Celda 13		Fallo Celda 13
243	32			15			Cell 14 Intercell Alarm			Cell14 InterAlm		Fallo interno Celda 14		Fallo Celda 14
244	32			15			Cell 15 Intercell Alarm			Cell15 InterAlm		Fallo interno Celda 15		Fallo Celda 15
245	32			15			Cell 16 Intercell Alarm			Cell16 InterAlm		Fallo interno Celda 16		Fallo Celda 16
246	32			15			Cell 17 Intercell Alarm			Cell17 InterAlm		Fallo interno Celda 17		Fallo Celda 17
247	32			15			Cell 18 Intercell Alarm			Cell18 InterAlm		Fallo interno Celda 18		Fallo Celda 18
248	32			15			Cell 19 Intercell Alarm			Cell19 InterAlm		Fallo interno Celda 19		Fallo Celda 19
249	32			15			Cell 20 Intercell Alarm			Cell20 InterAlm		Fallo interno Celda 20		Fallo Celda 20
250	32			15			Cell 21 Intercell Alarm			Cell21 InterAlm		Fallo interno Celda 21		Fallo Celda 21
251	32			15			Cell 22 Intercell Alarm			Cell22 InterAlm		Fallo interno Celda 22		Fallo Celda 22
252	32			15			Cell 23 Intercell Alarm			Cell23 InterAlm		Fallo interno Celda 23		Fallo Celda 23
253	32			15			Cell 24 Intercell Alarm			Cell24 InterAlm		Fallo interno Celda 24		Fallo Celda 24
254	32			15			Cell1 Ambient Alm		Cell1 Amb Alm		Alarma Ambiente Celda 1		Amb Celda 1
255	32			15			Cell2 Ambient Alm		Cell2 Amb Alm		Alarma Ambiente Celda 2		Amb Celda 2
256	32			15			Cell3 Ambient Alm		Cell3 Amb Alm		Alarma Ambiente Celda 3		Amb Celda 3
257	32			15			Cell4 Ambient Alm		Cell4 Amb Alm		Alarma Ambiente Celda 4		Amb Celda 4
258	32			15			Cell5 Ambient Alm		Cell5 Amb Alm		Alarma Ambiente Celda 5		Amb Celda 5
259	32			15			Cell6 Ambient Alm			Cell6 Amb Alm	Alarma Ambiente Celda 6		Amb Celda 6
260	32			15			Cell7 Ambient Alm			Cell7 Amb Alm	Alarma Ambiente Celda 7		Amb Celda 7
261	32			15			Cell8 Ambient Alm		Cell8 Amb Alm			Alarma Ambiente Celda 8		Amb Celda 8
262	32			15			Cell9 Ambient Alm			Cell9 Amb Alm	Alarma Ambiente Celda 9		Amb Celda 9
263	32			15			Cell10 Ambient Alm			Cell10 Amb Alm		Alarma Ambiente Celda 10	Amb Celda 10
264	32			15			Cell11 Ambient Alm			Cell11 Amb Alm		Alarma Ambiente Celda 11	Amb Celda 11
265	32			15			Cell12 Ambient Alm			Cell12 Amb Alm		Alarma Ambiente Celda 12	Amb Celda 12
266	32			15			Cell13 Ambient Alm			Cell13 Amb Alm		Alarma Ambiente Celda 13	Amb Celda 13
267	32			15			Cell14 Ambient Alm			Cell14 Amb Alm		Alarma Ambiente Celda 14	Amb Celda 14
268	32			15			Cell15 Ambient Alm			Cell15 Amb Alm		Alarma Ambiente Celda 15	Amb Celda 15
269	32			15			Cell16 Ambient Alm			Cell16 Amb Alm		Alarma Ambiente Celda 16	Amb Celda 16
270	32			15			Cell17 Ambient Alm			Cell17 Amb Alm		Alarma Ambiente Celda 17	Amb Celda 17
271	32			15			Cell18 Ambient Alm		Cell18 Amb Alm		Alarma Ambiente Celda 18	Amb Celda 18
272	32			15			Cell19 Ambient Alm			Cell19 Amb Alm		Alarma Ambiente Celda 19	Amb Celda 19
273	32			15			Cell20 Ambient Alm			Cell20 Amb Alm		Alarma Ambiente Celda 20	Amb Celda 20
274	32			15			Cell21 Ambient Alm			Cell21 Amb Alm		Alarma Ambiente Celda 21	Amb Celda 21
275	32			15			Cell22 Ambient Alm			Cell22 Amb Alm		Alarma Ambiente Celda 22	Amb Celda 22
276	32			15			Cell23 Ambient Alm			Cell23 Amb Alm		Alarma Ambiente Celda 23	Amb Celda 23
277	32			15			Cell24 Ambient Alm			Cell24 Amb Alm		Alarma Ambiente Celda 24	Amb Celda 24
278	32			15			String Addr Value			String Addr Val		Dirección Cadena		Dir Cadena
279	32			15			String Seq Num				String Seq Num		Secuencia Cadena		Secuen Cadena
280	32			15			Cell Volt Low Alarm				Volt Low Alm		Tensión de Celda Baja		V Celda Baja
281	32			15			Cell Temp Low Alarm			Temp Low Alm		Temperatura de Celda baja	Temp Celda Baja
282	32			15			Cell Resist Low Alarm			Resist Low Alm			Baja Resistencia de Celda	Baja Res Celda
283	32			15			Cell Inter Low Alarm				Inter Low Alm		Baja interna Celda		Baja Int Celda
284	32			15			Cell Ambient Low Alarm			Amb Low Alm		Baja Ambiente Celda		Baja Amb Celda
285	32			15			Ambient Temperature Value		Amb Temp Value		Temp Ambiente		Temp Ambiente
290	32			15			None					None			No				No
291	32			15			Alarm					Alarm			Sí				Sí
292	32			15			Overall Voltage High				Overall Volt Hi		Tensión Alta total		Tens Alta Total
293	32			15			Overall Voltage Low			Overall Volt Lo			Tensión Baja total		Tens Baja Total
294	32			15			String Current High				String Curr Hi	Corriente Cadena Alta		Alta Corr Caden
295	32			15			String Current Low			String Curr Lo			Corriente Cadena Baja		Baja Corr Caden
296	32			15			Float Current High				Float Curr Hi		Corriente Flotación Alta	Corr Flota Alta
297	32			15			Float Current Low				Float Curr Lo		Corriente Flotación Baja	Corr Flota Baja
298	32			15			Ripple Current High			Ripple Curr Hi		Corriente Rizado Alta		Corr Rizad Alta
299	32			15			Ripple Current Low			Ripple Curr Lo			Corriente Rizado Baja		Corr Rizad Baja
#Test Resistances
300	32			15			Test Resistance 1			Test Resist 1		Resistencia 1 de Prueba		Resis1 Prueba
301	32			15			Test Resistance 2			Test Resist 2		Resistencia 2 de Prueba		Resis2 Prueba
302	32			15			Test Resistance 3			Test Resist 3		Resistencia 3 de Prueba		Resis3 Prueba
303	32			15			Test Resistance 4			Test Resist 4		Resistencia 4 de Prueba		Resis4 Prueba
304	32			15			Test Resistance 5			Test Resist 5		Resistencia 5 de Prueba		Resis5 Prueba
305	32			15			Test Resistance 6			Test Resist 6		Resistencia 6 de Prueba		Resis6 Prueba
307	32			15			Test Resistance 7			Test Resist 7		Resistencia 7 de Prueba		Resis7 Prueba
308	32			15			Test Resistance 8			Test Resist 8		Resistencia 8 de Prueba		Resis8 Prueba
309	32			15			Test Resistance 9			Test Resist 9		Resistencia 9 de Prueba		Resis9 Prueba
310	32			15			Test Resistance 10			Test Resist 10		Resistencia 10 de Prueba	Resis10 Prueba
311	32			15			Test Resistance 11			Test Resist 11		Resistencia 11 de Prueba	Resis11 Prueba
312	32			15			Test Resistance 12			Test Resist 12		Resistencia 12 de Prueba	Resis12 Prueba
313	32			15			Test Resistance 13			Test Resist 13		Resistencia 13 de Prueba	Resis13 Prueba
314	32			15			Test Resistance 14			Test Resist 14		Resistencia 14 de Prueba	Resis14 Prueba
315	32			15			Test Resistance 15			Test Resist 15		Resistencia 15 de Prueba	Resis15 Prueba
316	32			15			Test Resistance 16			Test Resist 16		Resistencia 16 de Prueba	Resis16 Prueba
317	32			15			Test Resistance 17			Test Resist 17		Resistencia 17 de Prueba	Resis17 Prueba
318	32			15			Test Resistance 18			Test Resist 18		Resistencia 18 de Prueba	Resis18 Prueba
319	32			15			Test Resistance 19			Test Resist 19		Resistencia 19 de Prueba	Resis19 Prueba
320	32			15			Test Resistance 20			Test Resist 20		Resistencia 20 de Prueba	Resis20 Prueba
321	32			15			Test Resistance 21			Test Resist 21		Resistencia 21 de Prueba	Resis21 Prueba
322	32			15			Test Resistance 22			Test Resist 22		Resistencia 22 de Prueba	Resis22 Prueba
323	32			15			Test Resistance 23			Test Resist 23		Resistencia 23 de Prueba	Resis23 Prueba
324	32			15			Test Resistance 24			Test Resist 24		Resistencia 24 de Prueba	Resis24 Prueba
325	32			15			Test Intercell 1				Test Intercel1	Resis1 Intercelda		Res1 Intercel
326	32			15			Test Intercell 2				Test Intercel2	Resis2 Intercelda		Res2 Intercel
327	32			15			Test Intercell 3				Test Intercel3	Resis3 Intercelda		Res3 Intercel
328	32			15			Test Intercell 4				Test Intercel4	Resis4 Intercelda		Res4 Intercel
329	32			15			Test Intercell 5				Test Intercel5	Resis5 Intercelda		Res5 Intercel
330	32			15			Test Intercell 6				Test Intercel6	Resis6 Intercelda		Res6 Intercel
331	32			15			Test Intercell 7				Test Intercel7	Resis7 Intercelda		Res7 Intercel
332	32			15			Test Intercell 8				Test Intercel8	Resis8 Intercelda		Res8 Intercel
333	32			15			Test Intercell 9				Test Intercel9	Resis9 Intercelda		Res9 Intercel
334	32			15			Test Intercell 10				Test Intercel10		Resis10 Intercelda		Res10 Intercel
335	32			15			Test Intercell 11				Test Intercel11		Resis11 Intercelda		Res11 Intercel
336	32			15			Test Intercell 12				Test Intercel12		Resis12 Intercelda		Res12 Intercel
337	32			15			Test Intercell 13				Test Intercel13		Resis13 Intercelda		Res13 Intercel
338	32			15			Test Intercell 14				Test Intercel14		Resis14 Intercelda		Res14 Intercel
339	32			15			Test Intercell 15				Test Intercel15		Resis15 Intercelda		Res15 Intercel
340	32			15			Test Intercell 16				Test Intercel16		Resis16 Intercelda		Res16 Intercel
341	32			15			Test Intercell 17				Test Intercel17		Resis17 Intercelda		Res17 Intercel
342	32			15			Test Intercell 18				Test Intercel18		Resis18 Intercelda		Res18 Intercel
343	32			15			Test Intercell 19				Test Intercel19		Resis19 Intercelda		Res19 Intercel
344	32			15			Test Intercell 20				Test Intercel20		Resis20 Intercelda		Res20 Intercel
345	32			15			Test Intercell 21				Test Intercel21		Resis21 Intercelda		Res21 Intercel
346	32			15			Test Intercell 22				Test Intercel22		Resis22 Intercelda		Res22 Intercel
347	32			15			Test Intercell 23				Test Intercel23		Resis23 Intercelda		Res23 Intercel
348	32			15			Test Intercell 24				Test Intercel24		Resis24 Intercelda		Res24 Intercel
349	32			15			Cell High Voltage Alarm				Cell HiVolt Alm		Tensión de Celda Alta		V Celda Alta
350	32			15			Cell High Cell Temperature Alarm		Cell HiTemp Alm			Temperatura de Celda Alta	Temp Celda Alta
351	32			15			Cell High Resistance Alarm			Cell HiRes Alm		Alta Resistencia de Celda	Alta Res Celda
352	32			15			Cell High Intercell Resist Alarm		Inter HiRes Alm			Alta interna Celda		Alta Int Celda
353	32			15			Cell High Ambient Temp Alarm			Cell HiAmb Alm		Alta Ambiente Celda		Alta Amb Celda
354	32			15			Temperature 1 Not Used				Temp1 Not Used			Fallo Sensor Temperatura Celda 1	Fallo Sen Tcel1
355	32			15			Temperature 2 Not Used				Temp2 Not Used			Fallo Sensor Temperatura Celda 2	Fallo Sen Tcel2
356	32			15			Temperature 3 Not Used				Temp3 Not Used			Fallo Sensor Temperatura Celda 3	Fallo Sen Tcel3
357	32			15			Temperature 4 Not Used				Temp4 Not Used			Fallo Sensor Temperatura Celda 4	Fallo Sen Tcel4
358	32			15			Temperature 5 Not Used				Temp5 Not Used			Fallo Sensor Temperatura Celda 5	Fallo Sen Tcel5
359	32			15			Temperature 6 Not Used				Temp6 Not Used			Fallo Sensor Temperatura Celda 6	Fallo Sen Tcel6
360	32			15			Temperature 7 Not Used				Temp7 Not Used			Fallo Sensor Temperatura Celda 7	Fallo Sen Tcel7
361	32			15			Temperature 8 Not Used				Temp8 Not Used			Fallo Sensor Temperatura Celda 8	Fallo Sen Tcel8
362	32			15			Temperature 9 Not Used				Temp9 Not Used		Temperatura 9 no utilizada	Temp 9 no usada
363	32			15			Temperature 10 Not Used				Temp10 Not Used	Temperatura 10 no utilizada	Temp10 no usada
364	32			15			Temperature 11 Not Used				Temp11 Not Used	Temperatura 11 no utilizada	Temp11 no usada
365	32			15			Temperature 12 Not Used				Temp12 Not Used	Temperatura 12 no utilizada	Temp12 no usada
366	32			15			Temperature 13 Not Used				Temp13 Not Used	Temperatura 13 no utilizada	Temp13 no usada
367	32			15			Temperature 14 Not Used				Temp14 Not Used	Temperatura 14 no utilizada	Temp14 no usada
368	32			15			Temperature 15 Not Used				Temp15 Not Used	Temperatura 15 no utilizada	Temp15 no usada
369	32			15			Temperature 16 Not Used				Temp16 Not Used	Temperatura 16 no utilizada	Temp16 no usada
370	32			15			Temperature 17 Not Used				Temp17 Not Used	Temperatura 17 no utilizada	Temp17 no usada
371	32			15			Temperature 18 Not Used				Temp18 Not Used	Temperatura 18 no utilizada	Temp18 no usada
372	32			15			Temperature 19 Not Used				Temp19 Not Used	Temperatura 19 no utilizada	Temp19 no usada
373	32			15			Temperature 20 Not Used				Temp20 Not Used	Temperatura 20 no utilizada	Temp20 no usada
374	32			15			Temperature 21 Not Used				Temp21 Not Used	Temperatura 21 no utilizada	Temp21 no usada
375	32			15			Temperature 22 Not Used				Temp22 Not Used	Temperatura 22 no utilizada	Temp22 no usada
376	32			15			Temperature 23 Not Used				Temp23 Not Used	Temperatura 23 no utilizada	Temp23 no usada
377	32			15			Temperature 24 Not Used				Temp24 Not Used	Temperatura 24 no utilizada	Temp24 no usada

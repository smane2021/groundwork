﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
es



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			LVD1				LVD1			LVD1				LVD1
2		32			15			LVD2				LVD2			LVD2				LVD2
3		32			15			LVD3				LVD3			LVD3				LVD3
4		32			15			DC Distribution Number		DC Distr Number		Núm de Distribuciones		Núm Distrib
5		32			15			LargeDU DC Distribution Group	DC Distr Group		Grupo Distribución grande	Grupo granDist
6		32			15			Temp1 High Temperature Limit	Temp1 High Lmt		Límite alta temperatura 1	Lim alta Temp1
7		32			15			Temp2 High Temperature Limit	Temp2 High Lmt		Límite alta temperatura 2	Lim alta Temp2
8		32			15			Temp3 High Temperature Limit	Temp3 High Lmt		Límite alta temperatura 3	Lim alta Temp3
9		32			15			Temp1 Low Temperature Limit	Temp1 Low Lmt		Límite baja temperatura 1	Lim baja Temp1
10		32			15			Temp2 Low Temperature Limit	Temp2 Low Lmt		Límite baja temperatura 2	Lim baja Temp2
11		32			15			Temp3 Low Temperature Limit	Temp3 Low Lmt	Límite baja temperatura 3	Lim baja Temp3
12		32			15			LVD1 Voltage			LVD1 Voltage		Tensión LVD1			Tension LVD1
13		32			15			LVD2 Voltage			LVD2 Voltage		Tensión LVD2			Tension LVD2
14		32			15			LVD3 Voltage			LVD3 Voltage		Tensión LVD3			Tension LVD3
15		32			15			Over Voltage			Over Voltage		Sobretensión			Sobretensión
16		32			15			Under Voltage			Under Voltage		Subtensión			Subtensión
17		32			15			On				On			Conectado			Conectado
18		32			15			Off				Off			Apagado				Apagado
19		32			15			On				On			Conectado			Conectado
20		32			15			Off				Off			Apagado				Apagado
21		32			15			On				On			Conectado			Conectado
22		32			15			Off				Off			Apagado				Apagado
23		32			15			Total Load Current		Total Load Curr		Corriente total carga		Carga Total
24		32			15			Total DC Distribution Current	TotalDCDistCurr		Corriente total Distrib		Total Corr Dist
25		32			15			DC Distribution Average Voltage	DCDistrAveVolt	Tensión promedio Distribución	Tens media Dist
26		32			15		LVD1			LVD1		LVD1			LVD1 
27		32			15			LVD1 Mode			LVD1 Mode		Modo LVD1			Modo LVD1
28		32			15			LVD1 Time		LVD1 Time	Tiempo LVD1			Tiempo LVD1
29		32			15			LVD1 Reconnect Voltage		LVD1 Recon Volt		Tensión reconexión LVD1		Tens rec LVD1
30		32			15			LVD1 Reconnect Delay		LVD1 ReconDelay			Retardo reconexión LVD1		Ret recn LVD1
31		32			15			LVD1 Dependency			LVD1 Dependency		Dependencia LVD1		Depend LVD1
32		32			15			LVD2			LVD2			LVD2			LVD2	
33		32			15			LVD2 Mode			LVD2 Mode		Modo LVD2			Modo LVD2
34		32			15			LVD2 Time		LVD2 Time		Tiempo LVD2			Tiempo LVD2
35		32			15			LVD2 Reconnect Voltage		LVD2 Recon Volt			Tensión reconexión LVD2		Tens rec LVD2
36		32			15			LVD2 Reconnect Delay		LVD2 ReconDelay			Retardo reconexión LVD2		Ret recn LVD2
37		32			15			LVD2 Dependency			LVD2 Dependency			Dependencia LVD2		Depend LVD2
38		32			15		LVD3			LVD3		LVD3			LVD3
39		32			15			LVD3 Mode			LVD3 Mode		Modo LVD3			Modo LVD3
40		32			15			LVD3 Time		LVD3 Time		Tiempo LVD3			Tiempo LVD3
41		32			15			LVD3 Reconnect Voltage		LVD3 Recon Volt		Tensión reconexión LVD3		Tens rec LVD3
42		32			15			LVD3 Reconnect Delay		LVD3 ReconDelay			Retardo reconexión LVD3		Ret recn LVD3
43		32			15			LVD3 Dependency			LVD3 Dependency	Dependencia LVD3		Depend LVD3
44		32			15			Disabled			Disabled		Deshabilitada			Deshabilitada
45		32			15			Enabled				Enabled			Habilitada			Habilitada
46		32			15			Voltage				Voltage			Tensión				Tensión
47		32			15		Time			Time			Tiempo				Tiempo
48		32			15			None				None			No				No
49		32			15			LVD1				LVD1			LVD1				LVD1
50		32			15			LVD2				LVD2			LVD2				LVD2
51		32			15			LVD3				LVD3			LVD3				LVD3
52		32			15			Number of LVD			Num of LVD		Número de LVDs			Núm LVDs
53		32			15			0				0			0				0
54		32			15			1				1			1				1
55		32			15			2				2			2				2
56		32			15			3				3			3				3
57		32			15			Existence State			Existence State		Detección			Detección
58		32			15			Existent			Existent		Existente			Existente
59		32			15			Not Existent			Not Existent		No existente			No existente
60		32			15			Battery Over Voltage		Batt Over Volt		Sobretensión Batería		Sobretens Bat
61		32			15			Battery Under Voltage		Batt Under Volt		Subtensión Batería		Subtensión Bat

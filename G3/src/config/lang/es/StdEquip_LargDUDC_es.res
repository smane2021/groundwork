﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#


[LOCALE_LANGUAGE]
es



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			Temperature 1			Temperature 1			Temperatura 1			Temp 1
2		32			15			Temperature 2			Temperature 2			Temperatura 2			Temp 2
3		32			15			Temperature 3			Temperature 3			Temperatura 3			Temp 3
4		32			15			DC Voltage			DC Voltage		Tensión CC			Tensión CC
5		32			15			Load Current			Load Current		Corriente Carga			Corriente
6		32			15			Branch 1 Current		Branch 1 Curr		Corriente Rama 1		Corrient Rama1
7		32			15			Branch 2 Current		Branch 2 Curr		Corriente Rama 2		Corrient Rama2
8		32			15			Branch 3 Current		Branch 3 Curr		Corriente Rama 3		Corrient Rama3
9		32			15			Branch 4 Current		Branch 4 Curr		Corriente Rama 4		Corrient Rama4
10		32			15			Branch 5 Current		Branch 5 Curr		Corriente Rama 5		Corrient Rama5
11		32			15			Branch 6 Current		Branch 6 Curr		Corriente Rama 6		Corrient Rama6
12		32			15			DC Over Voltage			DC Over Volt		Sobretensión CC			Sobretensión CC
13		32			15			DC Under Voltage		DC Under Volt		Subtensión CC			Subtensión CC
14		32			15			Temperature 1 Over Temperature	T1 Over Temp		Alta temperatura 1		Alta temp 1
15		32			15			Temperature 2 Over Temperature	T2 Over Temp		Alta temperatura 2		Alta temp 2
16		32			15			Temperature 3 Over Temperature	T3 Over Temp		Alta temperatura 3		Alta temp 3
17		32			15			DC Output 1 Disconnected	Output 1 Discon		Salida 1 CC desconectada	Salida 1 desc
18		32			15			DC Output 2 Disconnected	Output 2 Discon		Salida 2 CC desconectada	Salida 2 desc
19		32			15			DC Output 3 Disconnected	Output 3 Discon		Salida 3 CC desconectada	Salida 3 desc
20		32			15			DC Output 4 Disconnected	Output 4 Discon		Salida 4 CC desconectada	Salida 4 desc
21		32			15			DC Output 5 Disconnected	Output 5 Discon		Salida 5 CC desconectada	Salida 5 desc
22		32			15			DC Output 6 Disconnected	Output 6 Discon		Salida 6 CC desconectada	Salida 6 desc
23		32			15			DC Output 7 Disconnected	Output 7 Discon		Salida 7 CC desconectada	Salida 7 desc
24		32			15			DC Output 8 Disconnected	Output 8 Discon		Salida 8 CC desconectada	Salida 8 desc
25		32			15			DC Output 9 Disconnected	Output 9 Discon		Salida 9 CC desconectada	Salida 9 desc
26		32			15			DC Output 10 Disconnected	Output10 Discon		Salida 10 CC desconectada	Salida 10 desc
27		32			15			DC Output 11 Disconnected	Output11 Discon		Salida 11 CC desconectada	Salida 11 desc
28		32			15			DC Output 12 Disconnected	Output12 Discon		Salida 12 CC desconectada	Salida 12 desc
29		32			15			DC Output 13 Disconnected	Output13 Discon		Salida 13 CC desconectada	Salida 13 desc
30		32			15			DC Output 14 Disconnected	Output14 Discon		Salida 14 CC desconectada	Salida 14 desc
31		32			15			DC Output 15 Disconnected	Output15 Discon		Salida 15 CC desconectada	Salida 15 desc
32		32			15			DC Output 16 Disconnected	Output16 Discon		Salida 16 CC desconectada	Salida 16 desc
33		32			15			DC Output 17 Disconnected	Output17 Discon		Salida 17 CC desconectada	Salida 17 desc
34		32			15			DC Output 18 Disconnected	Output18 Discon		Salida 18 CC desconectada	Salida 18 desc
35		32			15			DC Output 19 Disconnected	Output19 Discon		Salida 19 CC desconectada	Salida 19 desc
36		32			15			DC Output 20 Disconnected	Output20 Discon		Salida 20 CC desconectada	Salida 20 desc
37		32			15			DC Output 21 Disconnected	Output21 Discon		Salida 21 CC desconectada	Salida 21 desc
38		32			15			DC Output 22 Disconnected	Output22 Discon		Salida 22 CC desconectada	Salida 22 desc
39		32			15			DC Output 23 Disconnected	Output23 Discon		Salida 23 CC desconectada	Salida 23 desc
40		32			15			DC Output 24 Disconnected	Output24 Discon		Salida 24 CC desconectada	Salida 24 desc
41		32			15			DC Output 25 Disconnected	Output25 Discon		Salida 25 CC desconectada	Salida 25 desc
42		32			15			DC Output 26 Disconnected	Output26 Discon		Salida 26 CC desconectada	Salida 26 desc
43		32			15			DC Output 27 Disconnected	Output27 Discon		Salida 27 CC desconectada	Salida 27 desc
44		32			15			DC Output 28 Disconnected	Output28 Discon		Salida 28 CC desconectada	Salida 28 desc
45		32			15			DC Output 29 Disconnected	Output29 Discon		Salida 29 CC desconectada	Salida 29 desc
46		32			15			DC Output 30 Disconnected	Output30 Discon		Salida 30 CC desconectada	Salida 30 desc
47		32			15			DC Output 31 Disconnected	Output31 Discon		Salida 31 CC desconectada	Salida 31 desc
48		32			15			DC Output 32 Disconnected	Output32 Discon		Salida 32 CC desconectada	Salida 32 desc
49		32			15			DC Output 33 Disconnected	Output33 Discon		Salida 33 CC desconectada	Salida 33 desc
50		32			15			DC Output 34 Disconnected	Output34 Discon		Salida 34 CC desconectada	Salida 34 desc
51		32			15			DC Output 35 Disconnected	Output35 Discon		Salida 35 CC desconectada	Salida 35 desc
52		32			15			DC Output 36 Disconnected	Output36 Discon		Salida 36 CC desconectada	Salida 36 desc
53		32			15			DC Output 37 Disconnected	Output37 Discon		Salida 37 CC desconectada	Salida 37 desc
54		32			15			DC Output 38 Disconnected	Output38 Discon		Salida 38 CC desconectada	Salida 38 desc
55		32			15			DC Output 39 Disconnected	Output39 Discon		Salida 39 CC desconectada	Salida 39 desc
56		32			15			DC Output 40 Disconnected	Output40 Discon		Salida 40 CC desconectada	Salida 40 desc
57		32			15			DC Output 41 Disconnected	Output41 Discon		Salida 41 CC desconectada	Salida 41 desc
58		32			15			DC Output 42 Disconnected	Output42 Discon		Salida 42 CC desconectada	Salida 42 desc
59		32			15			DC Output 43 Disconnected	Output43 Discon		Salida 43 CC desconectada	Salida 43 desc
60		32			15			DC Output 44 Disconnected	Output44 Discon		Salida 44 CC desconectada	Salida 44 desc
61		32			15			DC Output 45 Disconnected	Output45 Discon		Salida 45 CC desconectada	Salida 45 desc
62		32			15			DC Output 46 Disconnected	Output46 Discon		Salida 46 CC desconectada	Salida 46 desc
63		32			15			DC Output 47 Disconnected	Output47 Discon		Salida 47 CC desconectada	Salida 47 desc
64		32			15			DC Output 48 Disconnected	Output48 Discon		Salida 48 CC desconectada	Salida 48 desc
65		32			15			DC Output 49 Disconnected	Output49 Discon		Salida 49 CC desconectada	Salida 49 desc
66		32			15			DC Output 50 Disconnected	Output50 Discon		Salida 50 CC desconectada	Salida 50 desc
67		32			15			DC Output 51 Disconnected	Output51 Discon		Salida 51 CC desconectada	Salida 51 desc
68		32			15			DC Output 52 Disconnected	Output52 Discon		Salida 52 CC desconectada	Salida 52 desc
69		32			15			DC Output 53 Disconnected	Output53 Discon		Salida 53 CC desconectada	Salida 53 desc
70		32			15			DC Output 54 Disconnected	Output54 Discon		Salida 54 CC desconectada	Salida 54 desc
71		32			15			DC Output 55 Disconnected	Output55 Discon		Salida 55 CC desconectada	Salida 55 desc
72		32			15			DC Output 56 Disconnected	Output56 Discon		Salida 56 CC desconectada	Salida 56 desc
73		32			15			DC Output 57 Disconnected	Output57 Discon		Salida 57 CC desconectada	Salida 57 desc
74		32			15			DC Output 58 Disconnected	Output58 Discon		Salida 58 CC desconectada	Salida 58 desc
75		32			15			DC Output 59 Disconnected	Output59 Discon		Salida 59 CC desconectada	Salida 59 desc
76		32			15			DC Output 60 Disconnected	Output60 Discon		Salida 60 CC desconectada	Salida 60 desc
77		32			15			DC Output 61 Disconnected	Output61 Discon		Salida 61 CC desconectada	Salida 61 desc
78		32			15			DC Output 62 Disconnected	Output62 Discon		Salida 62 CC desconectada	Salida 62 desc
79		32			15			DC Output 63 Disconnected	Output63 Discon		Salida 63 CC desconectada	Salida 63 desc
80		32			15			DC Output 64 Disconnected	Output64 Discon		Salida 64 CC desconectada	Salida 64 desc
81		32			15			LVD1 State			LVD1 State		Estado LVD1			Estado LVD1
82		32			15			LVD2 State			LVD2 State		Estado LVD2			Estado LVD2
83		32			15			LVD3 State			LVD3 State		Estado LVD3			Estado LVD3
84		32			15			Communication Fail			Comm Fail		Fallo de Comunicación			Fallo COM
85		32			15			LVD1				LVD1			LVD1				LVD1
86		32			15			LVD2				LVD2			LVD2				LVD2
87		32			15			LVD3				LVD3			LVD3				LVD3
88		32			15			Temperature 1 High Limit	Temp 1 Hi Limit		Límite alta temperatura 1	Lim alta Temp1
89		32			15			Temperature 2 High Limit	Temp 2 Hi Limit		Límite alta temperatura 2	Lim alta Temp2
90		32			15			Temperature 3 High Limit	Temp 3 Hi Limit		Límite alta temperatura 3	Lim alta Temp3
91		32			15			LVD1 Limit			LVD1 Limit		Límite LVD1			Límite LVD1
92		32			15			LVD2 Limit			LVD2 Limit		Límite LVD2			Límite LVD2
93		32			15			LVD3 Limit			LVD3 Limit		Límite LVD3			Límite LVD3
94		32			15			Battery Over Voltage Limit	BatOverVoltLmt		Nivel de sobretensión		Sobretensión
95		32			15			Battery Under Voltage Limit	BatUnderVoltLmt		Nivel de subtensión		Subtensión
96		32			15			Temperature Coefficient		Temp Coeff		Factor de Temperatura		Factor Temp
97		32			15			Current Sensor Coefficient	Sensor Coeff	Coeficiente Sensor corriente	Coef Sensor I
98		32			15			Number of Battery		Num of Battery		Número de Batería		Núm Batería
99		32			15			Temperature Number		Temp Number		Número de Temperatura		Núm Temp
100		32			15			Branch Current Coefficient	BranchCurrCoeff			Coef Corriente Rama		Coef I-Rama
101		32			15			Distribution Address		Distri Address		Dirección Distribución		Dir Distrib
102		32			15			Current Measurement Output Num	Curr Output Num	Núm Salida medida Corriente	Salida Med Corr
103		32			15			Number of Output		Num of Output		Número de Salida		Núm de Salida
104		32			15			DC Over Voltage			DC Over Volt	Sobretensión CC			Sobretensión
105		32			15			DC Under Voltage		DC Under Volt		Subtensión CC			Subtensión CC
106		32			15			DC Output 1 Disconnected	Output1 Discon		Salida 1 CC desconectada	Salida 1 desc
107		32			15			DC Output 2 Disconnected	Output2 Discon		Salida 2 CC desconectada	Salida 2 desc
108		32			15			DC Output 3 Disconnected	Output3 Discon		Salida 3 CC desconectada	Salida 3 desc
109		32			15			DC Output 4 Disconnected	Output4 Discon		Salida 4 CC desconectada	Salida 4 desc
110		32			15			DC Output 5 Disconnected	Output5 Discon		Salida 5 CC desconectada	Salida 5 desc
111		32			15			DC Output 6 Disconnected	Output6 Discon		Salida 6 CC desconectada	Salida 6 desc
112		32			15			DC Output 7 Disconnected	Output7 Discon		Salida 7 CC desconectada	Salida 7 desc
113		32			15			DC Output 8 Disconnected	Output8 Discon		Salida 8 CC desconectada	Salida 8 desc
114		32			15			DC Output 9 Disconnected	Output9 Discon		Salida 9 CC desconectada	Salida 9 desc
115		32			15			DC Output 10 Disconnected	Output10 Discon		Salida 10 CC desconectada	Salida 10 desc
116		32			15			DC Output 11 Disconnected	Output11 Discon		Salida 11 CC desconectada	Salida 11 desc
117		32			15			DC Output 12 Disconnected	Output12 Discon		Salida 12 CC desconectada	Salida 12 desc
118		32			15			DC Output 13 Disconnected	Output13 Discon		Salida 13 CC desconectada	Salida 13 desc
119		32			15			DC Output 14 Disconnected	Output14 Discon		Salida 14 CC desconectada	Salida 14 desc
120		32			15			DC Output 15 Disconnected	Output15 Discon		Salida 15 CC desconectada	Salida 15 desc
121		32			15			DC Output 16 Disconnected	Output16 Discon		Salida 16 CC desconectada	Salida 16 desc
122		32			15			DC Output 17 Disconnected	Output17 Discon		Salida 17 CC desconectada	Salida 17 desc
123		32			15			DC Output 18 Disconnected	Output18 Discon		Salida 18 CC desconectada	Salida 18 desc
124		32			15			DC Output 19 Disconnected	Output19 Discon		Salida 19 CC desconectada	Salida 19 desc
125		32			15			DC Output 20 Disconnected	Output20 Discon		Salida 20 CC desconectada	Salida 20 desc
126		32			15			DC Output 21 Disconnected	Output21 Discon		Salida 21 CC desconectada	Salida 21 desc
127		32			15			DC Output 22 Disconnected	Output22 Discon		Salida 22 CC desconectada	Salida 22 desc
128		32			15			DC Output 23 Disconnected	Output23 Discon		Salida 23 CC desconectada	Salida 23 desc
129		32			15			DC Output 24 Disconnected	Output24 Discon		Salida 24 CC desconectada	Salida 24 desc
130		32			15			DC Output 25 Disconnected	Output25 Discon		Salida 25 CC desconectada	Salida 25 desc
131		32			15			DC Output 26 Disconnected	Output26 Discon		Salida 26 CC desconectada	Salida 26 desc
132		32			15			DC Output 27 Disconnected	Output27 Discon		Salida 27 CC desconectada	Salida 27 desc
133		32			15			DC Output 28 Disconnected	Output28 Discon		Salida 28 CC desconectada	Salida 28 desc
134		32			15			DC Output 29 Disconnected	Output29 Discon		Salida 29 CC desconectada	Salida 29 desc
135		32			15			DC Output 30 Disconnected	Output30 Discon		Salida 30 CC desconectada	Salida 30 desc
136		32			15			DC Output 31 Disconnected	Output31 Discon		Salida 31 CC desconectada	Salida 31 desc
137		32			15			DC Output 32 Disconnected	Output32 Discon		Salida 32 CC desconectada	Salida 32 desc
138		32			15			DC Output 33 Disconnected	Output33 Discon		Salida 33 CC desconectada	Salida 33 desc
139		32			15			DC Output 34 Disconnected	Output34 Discon		Salida 34 CC desconectada	Salida 34 desc
140		32			15			DC Output 35 Disconnected	Output35 Discon		Salida 35 CC desconectada	Salida 35 desc
141		32			15			DC Output 36 Disconnected	Output36 Discon		Salida 36 CC desconectada	Salida 36 desc
142		32			15			DC Output 37 Disconnected	Output37 Discon		Salida 37 CC desconectada	Salida 37 desc
143		32			15			DC Output 38 Disconnected	Output38 Discon		Salida 38 CC desconectada	Salida 38 desc
144		32			15			DC Output 39 Disconnected	Output39 Discon		Salida 39 CC desconectada	Salida 39 desc
145		32			15			DC Output 40 Disconnected	Output40 Discon		Salida 40 CC desconectada	Salida 40 desc
146		32			15			DC Output 41 Disconnected	Output41 Discon		Salida 41 CC desconectada	Salida 41 desc
147		32			15			DC Output 42 Disconnected	Output42 Discon		Salida 42 CC desconectada	Salida 42 desc
148		32			15			DC Output 43 Disconnected	Output43 Discon		Salida 43 CC desconectada	Salida 43 desc
149		32			15			DC Output 44 Disconnected	Output44 Discon		Salida 44 CC desconectada	Salida 44 desc
150		32			15			DC Output 45 Disconnected	Output45 Discon		Salida 45 CC desconectada	Salida 45 desc
151		32			15			DC Output 46 Disconnected	Output46 Discon		Salida 46 CC desconectada	Salida 46 desc
152		32			15			DC Output 47 Disconnected	Output47 Discon		Salida 47 CC desconectada	Salida 47 desc
153		32			15			DC Output 48 Disconnected	Output48 Discon		Salida 48 CC desconectada	Salida 48 desc
154		32			15			DC Output 49 Disconnected	Output49 Discon		Salida 49 CC desconectada	Salida 49 desc
155		32			15			DC Output 50 Disconnected	Output50 Discon		Salida 50 CC desconectada	Salida 50 desc
156		32			15			DC Output 51 Disconnected	Output51 Discon		Salida 51 CC desconectada	Salida 51 desc
157		32			15			DC Output 52 Disconnected	Output52 Discon		Salida 52 CC desconectada	Salida 52 desc
158		32			15			DC Output 53 Disconnected	Output53 Discon		Salida 53 CC desconectada	Salida 53 desc
159		32			15			DC Output 54 Disconnected	Output54 Discon		Salida 54 CC desconectada	Salida 54 desc
160		32			15			DC Output 55 Disconnected	Output55 Discon		Salida 55 CC desconectada	Salida 55 desc
161		32			15			DC Output 56 Disconnected	Output56 Discon		Salida 56 CC desconectada	Salida 56 desc
162		32			15			DC Output 57 Disconnected	Output57 Discon		Salida 57 CC desconectada	Salida 57 desc
163		32			15			DC Output 58 Disconnected	Output58 Discon		Salida 58 CC desconectada	Salida 58 desc
164		32			15			DC Output 59 Disconnected	Output59 Discon		Salida 59 CC desconectada	Salida 59 desc
165		32			15			DC Output 60 Disconnected	Output60 Discon		Salida 60 CC desconectada	Salida 60 desc
166		32			15			DC Output 61 Disconnected	Output61 Discon		Salida 61 CC desconectada	Salida 61 desc
167		32			15			DC Output 62 Disconnected	Output62 Discon		Salida 62 CC desconectada	Salida 62 desc
168		32			15			DC Output 63 Disconnected	Output63 Discon		Salida 63 CC desconectada	Salida 63 desc
169		32			15			DC Output 64 Disconnected	Output64 Discon		Salida 64 CC desconectada	Salida 64 desc
170		32			15			Communication Fail			Comm Fail		Fallo Comunicación		Fallo COM
171		32			15			LVD1				LVD1			LVD1				LVD1
172		32			15			LVD2				LVD2			LVD2				LVD2
173		32			15			LVD3				LVD3			LVD3				LVD3
174		32			15			Temperature 1 Over Temperature	T1 Over Temp		Alta temperatura 1		Alta temp 1
175		32			15			Temperature 2 Over Temperature	T2 Over Temp	Alta temperatura 2		Alta temp 2
176		32			15			Temperature 3 Over Temperature	T3 Over Temp	Alta temperatura 3		Alta temp 3
177		32			15			LargeDU DC Distribution		DC Distribution		Distribución CC grande		Distrib grande
178		32			15			Temperature 1 Low Limit		Temp1 Low Limit		Límite baja temperatura 1	Lim baja Temp1
179		32			15			Temperature 2 Low Limit		Temp2 Low Limit		Límite baja temperatura 2	Lim baja Temp2
180		32			15			Temperature 3 Low Limit		Temp3 Low Limit		Límite baja temperatura 3	Lim baja Temp3
181		32			15			Temperature 1 Under Temperature	T1 Under Temp		Baja temperatura 1		Baja temp 1
182		32			15			Temperature 2 Under Temperature	T2 Under Temp		Baja temperatura 2		Baja temp 2
183		32			15			Temperature 3 Under Temperature	T3 Under Temp		Baja temperatura 3		Baja temp 3
184		32			15			Temperature 1 Alarm		Temp1 Alarm		Alarma temperatura 1		Alarma temp 1
185		32			15			Temperature 2 Alarm		Temp2 Alarm		Alarma temperatura 2		Alarma temp 2
186		32			15			Temperature 3 Alarm		Temp3 Alarm		Alarma temperatura 3		Alarma temp 3
187		32			15			Voltage Alarm			Voltage Alarm		Alarma de Tensión		Alarma Tensión
188		32			15			No Alarm			No Alarm		Sin alarmas			Sin alarmas
189		32			15			Over Temperature		Over Temp	Alta temperatura		Alta temp
190		32			15			Under Temperature		Under Temp	Baja temperatura		Baja temp
191		32			15			No Alarm			No Alarm		Sin alarmas			Sin alarmas
192		32			15			Over Temperature		Over Temp	Alta temperatura		Alta temp
193		32			15			Under Temperature		Under Temp	Baja temperatura		Baja temp
194		32			15			No Alarm			No Alarm		Sin alarmas			Sin alarmas
195		32			15			Over Temperature		Over Temp	Alta temperatura		Alta temp
196		32			15			Under Temperature		Under Temp	Baja temperatura		Baja temp
197		32			15			No Alarm			No Alarm		Sin alarmas			Sin alarmas
198		32			15			Over Voltage			Over Voltage		Sobretensión CC			Sobretensión CC
199		32			15			Under Voltage			Under Voltage		Subtensión CC			Subtensión CC
200		32			15			Voltage Alarm			Voltage Alarm		Alarma de tensión		Alarma Tensión
201		32			15			DC Distr Communication Fail	DCD Comm Fail		Distribución Fallo Comunicación	Fallo COM
202		32			15			Normal				Normal			Normal				Normal
203		32			15			Failure				Failure			Fallo				Fallo
204		32			15			DC Distr Communication Fail	DCD Comm Fail		Distribución Fallo Comunicación	Fallo COM
205		32			15			On				On			Conectado			Conectado
206		32			15			Off				Off			Desconectado			Desconectado
207		32			15			On				On			Conectado			Conectado
208		32			15			Off				Off			Desconectado			Desconectado
209		32			15			On				On			Conectado			Conectado
210		32			15			Off				Off			Desconectado			Desconectado
211		32			15			Temperature 1 Sensor Failure	T1 Sensor Fail		Fallo sensor temperatura 1	Fallo sens 1
212		32			15			Temperature 2 Sensor Failure	T2 Sensor Fail		Fallo sensor temperatura 2	Fallo sens 2
213		32			15			Temperature 3 Sensor Failure	T3 Sensor Fail	Fallo sensor temperatura 3	Fallo sens 3
214		32			15			Connected			Connected		Cerrado				Cerrado
215		32			15			Disconnected			Disconnected		Abierto				Abierto
216		32			15			Connected			Connected		Cerrado				Cerrado
217		32			15			Disconnected			Disconnected		Abierto				Abierto
218		32			15			Connected			Connected		Cerrado				Cerrado
219		32			15			Disconnected			Disconnected		Abierto				Abierto
220		32			15			Normal				Normal			Normal				Normal
221		32			15			DC Distr Communication Fail	DCD Comm Fail		Distribución Fallo Comunicación	Fallo COM
222		32			15			Normal				Normal			Normal				Normal
223		32			15			Alarm				Alarm			Alarma				Alarma
224		32			15			Branch 7 Current		Branch 7 Curr		Corriente Rama 7		Corrient Rama7
225		32			15			Branch 8 Current		Branch 8 Curr		Corriente Rama 8		Corrient Rama8
226		32			15			Branch 9 Current		Branch 9 Curr		Corriente Rama 9		Corrient Rama9
227		32			15			Existence State			Existence State		Detección			Detección
228		32			15			Existent			Existent		Existente			Existente
229		32			15			Not Existent			Not Existent		No existente			No existente
230		32			15			LVD Number			LVD Number	Número de LVDs			Núm LVDs
231		32			15			0				0			0				0
232		32			15			1				1			1				1
233		32			15			2				2			2				2
234		32			15			3				3			3				3
235		32			15			Battery Shutdown Number		Batt SD Num		Núm desconexión Batería		N descon Bat
236		32			15			Rated Capacity			Rated Capacity		Capacidad estimada		Cap estimada

﻿#
#  Locale language support:Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			SMDUE Group		SMDUE Group		SMDUE Grupo			SMDUE Grupo
2		32			15			Number of SMDUEs	Num of SMDUEs		Número SMDUEs		Num SMDUEs
3		32			15			Communication Fail	Comm Fail		Falla de comunicación		Falla Com
4		32			15			Existence State		Existence State		Estado existencia		Estado exist
5		32			15			Comm OK			Comm OK			Com ok		Com ok
6		32			15			Communication Fail	Comm Fail		Falla de comunicación		Falla Com
7		32			15			Existent		Existent		Existente			Existente
8		32			15			Not Existent		Not Existent		No Existe			No Existe
9		32			15			SMDUE Changed(LCD)		SMDUE Chg(LCD)		SMDUECambiado(LCD)			SMDUECam(LCD)
10		32			15			SMDUE Changed(WEB)		SMDUE Chg(WEB)		SMDUECambiado(WEB)			SMDUECam(WEB)
11		32			15			Total SMDUELoad Current		Total Load Curr		CorriCarga Total SMDUE		CorrCargaTotal
12		32			15			Not Changed		Not Changed		Sin Cambio			Sin Cambio
13		32			15			Changed			Changed			Cambiado			Cambiado


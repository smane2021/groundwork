﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Battery Current				Batt Current		Corriente a batería		Corriente bat
2	32			15			Battery Rating(Ah)			Batt Rating(Ah)		Capacidad batería		Capacidad bat
3	32			15			Exceed Current Limit			Exceed Curr Lmt			Límite de corriente pasado	Lim corr pasado
4	32			15			Battery					Battery			Batería				Batería
5	32			15			Over Battery Current			Over Current		Sobrecorriente a batería	Sobrecorriente
6	32			15			Battery Capacity (%)			Batt Cap (%)		Capacidad batería (%)		Capacidad
7	32			15			Battery Voltage				Batt Voltage		Tensión batería			Tensión bat
8	32			15			Low Capacity				Low Capacity		Baja capacidad			Baja capacidad
9	32			15			On					On			Conectado			Conectado
10	32			15			Off					Off			Desconectado			Desconectado
11	32			15			Battery Fuse Voltage			Fuse Voltage		Tensión fusible de batería	Tensión fus bat
12	32			15			Battery Fuse Status			Fuse Status		Estado fusible bat		Estado fusible
13	32			15			Fuse Alarm				Fuse Alarm		Alarma de fusible		Alarma fusible
14	32			15			SMDU Battery				SMDU Battery		Batería Extensión (SMDU)	Batería Ext
15	32			15			State					State			Estado				Estado
16	32			15			Off					Off			Apagado				Apagado
17	32			15			On					On			Conectado			Conectado
18	32			15			Switch					Switch			Conmutador			Conmutador
19	32			15			Battery Over Current			Batt Over Curr		Sobrecorriente a batería	Sobrecorriente
20	32			15			Battery Management			Batt Management			Gestión de Baterías		Gestión Bat
21	32			15			Yes					Yes			No				No
22	32			15			No					No			Sí				Sí
23	32			15			Over Voltage Limit			Over Volt Limit		Nivel de Sobretensión		Sobretensión
24	32			15			Low Voltage Limit			Low Volt Limit		Nivel de Subtensión		Niv Subtensión
25	32			15			Battery Over Voltage			Over Voltage		Sobretensión Batería		Sobretensión
26	32			15			Battery Under Voltage			Under Voltage		Subtensión Batería		Subtensión Bat
27	32			15			Over Current				Over Current		Sobrecorriente			Sobrecorriente
28	32			15			Communication Fail			Comm Fail		Interrupción Comunicación	Interrup COM
29	32			15			Times of Communication Fail			Times Comm Fail			Fallos de Comunicación			Fallos COM
44	32			15			Battery Temp Number			Batt Temp Num		Sensor Temperatura		Sensor Temp
87	32			15			No					No		Ninguno				Ninguno

91	32			15			Temperature 1			Temperature 1		Temperatura 1		Temperatura 1
92	32			15			Temperature 2			Temperature 2		Temperatura 2		Temperatura 2
93	32			15			Temperature 3			Temperature 3		Temperatura 3		Temperatura 3
94	32			15			Temperature 4			Temperature 4		Temperatura 4		Temperatura 4
95	32			15			Temperature 5			Temperature 5		Temperatura 5		Temperatura 5
96	32			15			Rated Capacity				Rated Capacity		Capacidad Estimada		Capacidad Est
97	32			15			Battery Temperature			Battery Temp		Temperatura Batería		Temperatura
98	32			15			Battery Temperature Sensor		BattTemp Sensor		Sensor Temperatura Batería	Sensor Temp Bat
99	32			15			None					None			Ninguno				Ninguno
100	32			15			Temperature 1				Temperature 1		Temperatura 1			Temperatura 1
101	32			15			Temperature 2				Temperature 2		Temperatura 2			Temperatura 2
102	32			15			Temperature 3				Temperature 3		Temperatura 3			Temperatura 3
103	32			15			Temperature 4				Temperature 4		Temperatura 4			Temperatura 4
104	32			15			Temperature 5				Temperature 5		Temperatura 5			Temperatura 5
105	32			15			Temperature 6				Temperature 6		Temperatura 6			Temperatura 6
106	32			15			Temperature 7				Temperature 7		Temperatura 7			Temperatura 7
107	32			15			Temperature 8				Temperature 8		Temperatura 8			Temperatura 8
108	32			15			Temperature 9				Temperature 9		Temperatura 9			Temperatura 9
109	32			15			Temperature 10				Temperature 10		Temperatura 10			Temperatura 10
110	32			15			SMDU1 Battery1			SMDU1 Battery1		Batería1 Extensión1 (SMDU1)		Batería1 ext1
111	32			15			SMDU1 Battery2			SMDU1 Battery2		Batería2 Extensión1 (SMDU1)		Batería2 ext1
112	32			15			SMDU1 Battery3			SMDU1 Battery3		Batería3 Extensión1 (SMDU1)		Batería3 ext1
113	32			15			SMDU1 Battery4			SMDU1 Battery4		Batería4 Extensión1 (SMDU1)		Batería4 ext1
114	32			15			SMDU1 Battery5			SMDU1 Battery5		Batería5 Extensión1 (SMDU1)		Batería5 ext1
115	32			15			SMDU2 Battery1			SMDU2 Battery1		Batería1 Extensión2 (SMDU2)		Batería1 ext2
116	32			15			SMDU2 Battery2			SMDU2 Battery2		Batería2 Extensión2 (SMDU2)		Batería2 ext2
117	32			15			SMDU2 Battery3			SMDU2 Battery3		Batería3 Extensión2 (SMDU2)		Batería3 ext2
118	32			15			SMDU2 Battery4			SMDU2 Battery4		Batería4 Extensión2 (SMDU2)		Batería4 ext2
119	32			15			SMDU2 Battery5			SMDU2 Battery5		Batería5 Extensión2 (SMDU2)		Batería5 ext2
120	32			15			SMDU3 Battery1			SMDU3 Battery1		Batería1 Extensión3 (SMDU3)		Batería1 ext3
121	32			15			SMDU3 Battery2			SMDU3 Battery2		Batería2 Extensión3 (SMDU3)		Batería2 ext3
122	32			15			SMDU3 Battery3			SMDU3 Battery3		Batería3 Extensión3 (SMDU3)		Batería3 ext3
123	32			15			SMDU3 Battery4			SMDU3 Battery4		Batería4 Extensión3 (SMDU3)		Batería4 ext3
124	32			15			SMDU3 Battery5			SMDU3 Battery5		Batería5 Extensión3 (SMDU3)		Batería5 ext3
125	32			15			SMDU4 Battery1			SMDU4 Battery1		Batería1 Extensión4 (SMDU4)		Batería1 ext4
126	32			15			SMDU4 Battery2			SMDU4 Battery2		Batería2 Extensión4 (SMDU4)		Batería2 ext4
127	32			15			SMDU4 Battery3			SMDU4 Battery3		Batería3 Extensión4 (SMDU4)		Batería3 ext4
128	32			15			SMDU4 Battery4			SMDU4 Battery4		Batería4 Extensión4 (SMDU4)		Batería4 ext4
129	32			15			SMDU4 Battery5			SMDU4 Battery5		Batería5 Extensión4 (SMDU4)		Batería5 ext4
130	32			15			SMDU5 Battery1			SMDU5 Battery1		Batería1 Extensión5 (SMDU5)		Batería1 ext5
131	32			15			SMDU5 Battery2			SMDU5 Battery2		Batería2 Extensión5 (SMDU5)		Batería2 ext5
132	32			15			SMDU5 Battery3			SMDU5 Battery3		Batería3 Extensión5 (SMDU5)		Batería3 ext5
133	32			15			SMDU5 Battery4			SMDU5 Battery4		Batería4 Extensión5 (SMDU5)		Batería4 ext5
134	32			15			SMDU5 Battery5			SMDU5 Battery5		Batería5 Extensión5 (SMDU5)		Batería5 ext5
135	32			15			SMDU6 Battery1			SMDU6 Battery1		Batería1 Extensión6 (SMDU6)		Batería1 ext6
136	32			15			SMDU6 Battery2			SMDU6 Battery2		Batería2 Extensión6 (SMDU6)		Batería2 ext6
137	32			15			SMDU6 Battery3			SMDU6 Battery3		Batería3 Extensión6 (SMDU6)		Batería3 ext6
138	32			15			SMDU6 Battery4			SMDU6 Battery4		Batería4 Extensión6 (SMDU6)		Batería4 ext6
139	32			15			SMDU6 Battery5			SMDU6 Battery5		Batería5 Extensión6 (SMDU6)		Batería5 ext6
140	32			15			SMDU7 Battery1			SMDU7 Battery1		Batería1 Extensión7 (SMDU7)		Batería1 ext7
141	32			15			SMDU7 Battery2			SMDU7 Battery2		Batería2 Extensión7 (SMDU7)		Batería2 ext7
142	32			15			SMDU7 Battery3			SMDU7 Battery3		Batería3 Extensión7 (SMDU7)		Batería3 ext7
143	32			15			SMDU7 Battery4			SMDU7 Battery4		Batería4 Extensión7 (SMDU7)		Batería4 ext7
144	32			15			SMDU7 Battery5			SMDU7 Battery5		Batería5 Extensión7 (SMDU7)		Batería5 ext7
145	32			15			SMDU8 Battery1			SMDU8 Battery1		Batería1 Extensión8 (SMDU8)		Batería1 ext8
146	32			15			SMDU8 Battery2			SMDU8 Battery2		Batería2 Extensión8 (SMDU8)		Batería2 ext8
147	32			15			SMDU8 Battery3			SMDU8 Battery3		Batería3 Extensión8 (SMDU8)		Batería3 ext8
148	32			15			SMDU8 Battery4			SMDU8 Battery4		Batería4 Extensión8 (SMDU8)		Batería4 ext8
149	32			15			SMDU8 Battery5			SMDU8 Battery5		Batería5 Extensión8 (SMDU8)		Batería5 ext8
151	32			15			Battery 1			Batt 1			Batería 1			Bat 1
152	32			15			Battery 2			Batt 2			Batería 2			Bat 2
153	32			15			Battery 3			Batt 3			Batería 3			Bat 3
154	32			15			Battery 4			Batt 4			Batería 4			Bat 4
155	32			15			Battery 5			Batt 5			Batería 5			Bat 5


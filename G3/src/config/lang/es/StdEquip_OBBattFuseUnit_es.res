﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1		32			15			Fuse 1 Voltage		Fuse 1 Voltage		Tensión fusible 1	Tensión fus1	
2		32			15			Fuse 2 Voltage		Fuse 2 Voltage		Tensión fusible 2	Tensión fus2	
3		32			15			Fuse 3 Voltage		Fuse 3 Voltage		Tensión fusible 3	Tensión fus3	
4		32			15			Fuse 4 Voltage		Fuse 4 Voltage		Tensión fusible 4	Tensión fus4
5		32			15			Fuse 1 Alarm		Fuse 1 Alarm		Fallo fusible 1		Fallo fus1	
6		32			15			Fuse 2 Alarm		Fuse 2 Alarm		Fallo fusible 2		Fallo fus2
7		32			15			Fuse 3 Alarm		Fuse 3 Alarm		Fallo fusible 3		Fallo fus3	
8		32			15			Fuse 4 Alarm		Fuse 4 Alarm		Fallo fusible 4		Fallo fus4
9	32			15	Battery Fuse		Batt Fuse		Fusibles batería	Fus Batería
10		32			15			On			On			Normal			Normal
11		32			15			Off			Off			Alarma			Alarma
12		32			15			Fuse 1 Status		Fuse 1 Status		Estado fusible 1	Estado fus1
13		32			15			Fuse 2 Status		Fuse 2 Status		Estado fusible 2	Estado fus2	
14		32			15			Fuse 3 Status		Fuse 3 Status		Estado fusible 3	Estado fus3	
15		32			15			Fuse 4 Status		Fuse 4 Status		Estado fusible 4	Estado fus4
16		32			15			State			State			Estado			Estado
17		32			15			Communication Fail			Comm Fail		Fallo de Comunicación		Fallo COM
18		32			15			No			No			No			No
19		32			15			Yes			Yes			Sí		Sí
20		32			15			Number of Battery Fuses	Num of BatFuses	Número Fusible Batería	Núm Fus Bat
21		32			15			0			0			0			0	
22		32			15			1			1			1			1	
23		32			15			2			2			2			2	
24		32			15			3			3			3			3	
25		32			15			4			4			4			4	
26	32			15		5			5			5			5	
27	32			15		6			6			6			6	

28	32			15		Fuse 5 Voltage		Fuse 5 Voltage		Tensión Fusible 5		Tensión Fusi 5	
29	32			15		Fuse 6 Voltage		Fuse 6 Voltage		Tensión Fusible 6		Tensión Fusi 6
30	32			15		Fuse 5 Alarm		Fuse 5 Alarm		Fallo fusible 5			Fallo fus5	
31	32			15		Fuse 6 Alarm		Fuse 6 Alarm		Fallo fusible 6			Fallo fus6	
32	32			15		Fuse 5 Status		Fuse 5 Status		Estado Fusible 5		Estado Fusible5
33	32			15		Fuse 6 Status		Fuse 6 Status		Estado Fusible 6		Estado Fusible6
34	32			15		7			7			7			7	
35	32			15		8			8			8			8	
36	32			15		Fuse 7 Voltage		Fuse 7 Voltage		Tensión Fusible 7		Tensión Fusi 7		
37	32			15		Fuse 8 Voltage		Fuse 8 Voltage		Tensión Fusible 8		Tensión Fusi 8
38	32			15		Fuse 7 Alarm		Fuse 7 Alarm		Alarma Fusible 7		Alarma Fusible7	
39	32			15		Fuse 8 Alarm		Fuse 8 Alarm		Alarma Fusible 8		Alarma Fusible8	
40	32			15		Fuse 7 Status		Fuse 7 Status		Estado Fusible 7		Estado Fusible7
41	32			15		Fuse 8 Status		Fuse 8 Status		Estado Fusible 8		Estado Fusible8





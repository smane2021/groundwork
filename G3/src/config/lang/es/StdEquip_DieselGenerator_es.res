﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			Diesel Generator Battery Voltage	Diesel Bat Volt	Tensión Batería de Grupo	Tens Bat Grupo
2		32			15			Diesel Generator Running		Diesel Running	Grupo Operando			Grupo Operando
3		32			15			Relay 2 Status				Relay 2 Status		Estado Relé2			Estado Relé2
4		32			15			Relay 3 Status				Relay 3 Status		Estado Relé3			Estado Relé3
5		32			15			Relay 4 Status				Relay 4 Status		Estado Relé4			Estado Relé4
6		32			15			Diesel Generator Failure Status		Diesel Fail		Fallo Grupo Electrógeno		Fallo Grupo
7		32			15			DG Connected Status		DG Connect			Grupo E. conectado		Grupo conect.
8		32			15			Low Fuel Level Status			Low Fuel Level		Bajo nivel combustible		Bajo nivel comb
9		32			15			High Water Temperature Status		High Water Temp		Alta temperatura de agua	Alta Temp agua
10		32			15			Low Oil Pressure Status			Low Oil Press		Baja presión aceite		Baja pres aceit
11		32			15			Start Diesel Generator			Start Diesel		Arrancar Grupo Elect.		Arrancar Grupo
12		32			15			Relay 2 On/Off				Relay 2 On/Off		Relé2 conectado		Relé2 conect
13		32			15			Relay 3 On/Off				Relay 3 On/Off		Relé3 conectado		Relé3 conect
14		32			15			Relay 4 On/Off				Relay 4 On/Off		Relé4 conectado		Relé4 conect
15		32			15			Battery Voltage Limit			Batt Volt Limit		Límite tensión Batería		Lim Tens Bat
16		32			15			Relay 1 Pulse Time			Relay1 Pulse T		Tiempo de pulso Relé1		T pulso rele1
17		32			15			Relay 2 Pulse Time			Relay2 Pulse T	Tiempo de pulso Relé2		T pulso rele2
18		32			15			Relay 1 Pulse Time			Relay1 Pulse T		Tiempo de pulso Relé1		T pulso rele1
19		32			15			Relay 2 Pulse Time			Relay2 Pulse T		Tiempo de pulso Relé2		T pulso rele2
20		32			15			Low DC Voltage				Low DC Voltage		Baja tensión DC			Baja Tens DC
21		32			15			DG Supervision Failure			SupervisionFail	Fallo supervisión G.E.		Fallo sup GE
22		32			15			Diesel Generator Failure		Diesel Fail		Fallo Grupo Electrógeno		Fallo GE
23		32			15			Diesel Generator Connected		Diesel Connect		Grupo Electrógeno conectado	GE conectado
24		32			15			Not Running				Not Running		No Operando			No operando
25		32			15			Running					Running			Operando			Operando
26		32			15			Off					Off			apagado				apagado
27		32			15			On					On			conectado			conectado
28		32			15			Off					Off			apagado				apagado
29		32			15			On					On			conectado			conectado
30		32			15			Off					Off			apagado				apagado
31		32			15			On					On			conectado			conectado
32		32			15			No					No			No				No
33		32			15			Yes					Yes			Sí		Sí
34		32			15			No					No			No				No
35		32			15			Yes					Yes			Sí		Sí
36		32			15			Off					Off			apagado				apagado
37		32			15			On					On			conectado			conectado
38		32			15			Off					Off			apagado				apagado
39		32			15			On					On			conectado			conectado
40		32			15			Off					Off			apagado				apagado
41		32			15			On					On			conectado			conectado
42		32			15			Diesel Generator			Dsl Generator		Grupo electrógeno		G.Electr
43		32			15			Mains Connected				Mains Connected		Disyuntor Red conectado		Red conec
44		32			15			Diesel Generator Shutdown		Diesel Shutdown	Desconexión G.E.		Desc. G.E.
45		32			15			Low Fuel Level				Low Fuel Level		Bajo nivel de combustible	Bajo comb
46		32			15			High Water Temperature			High Water Temp		Alta temperatura agua		Alta temp agua
47		32			15			Low Oil Pressure			Low Oil Press		Baja presión de aceite		Baja P aceite
48		32			15			Communication Failure			Comm Fail		Fallo supervisión SM-AC		Fallo SM-AC
49		32			15			Low Oil Pressure Alarm Status		Low Oil Press		Alarma baja presión aceite	Baja P aceite
50		32			15			Clear Low Oil Pressure Alarm		Clr Oil Press		Cesar alarma Baja pres aceite	Cesar BP aceite
51		32			15			State					State			Estado				Estado
52		32			15			Existence State				Existence State		Estado presencia		Presencia
53		32			15			Existent				Existent		Existente			Existente
54		32			15			Not Existent				Not Existent		Inexistente			Inexistente
55		32			15			Total Run Time				Total Run Time			Total en operación		Total en oper
56		32			15			Maintenance Time Limit			Mtn Time Limit			Límite de Mantenimiento		Lim Manten
57		32			15			Clear Total Run Time			Clr Run Time			Iniciar Tiempo Operación		Inic T operando
58		32			15			Periodical Maintenance Required		Mtn Required			Mantenimiento Periódico			Manten Req
59		32			15			Maintenance Countdown Timer		Mtn DownTimer			Contador Mantenimiento			Cont Manten

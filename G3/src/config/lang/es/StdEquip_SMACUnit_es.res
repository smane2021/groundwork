﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15		Phase A Voltage				Phase A Volt		Tensión fase R			Tensión R
2		32			15			Phase B Voltage		Phase B Volt	Tensión fase S			Tensión S
3		32			15			Phase C Voltage		Phase C Volt	Tensión fase T			Tensión T
4		32			15			AB Line Voltage		AB Line Volt	Tensión R-S			Tensión R-S
5		32			15			BC Line Voltage		BC Line Volt	Tensión S-T			Tensión S-T
6		32			15			CA Line Voltage		CA Line Volt	Tensión R-T			Tensión R-T
7		32			15			Phase A Current		Phase A Curr	Corriente fase R		Corriente R
8		32			15			Phase B Current		Phase B Curr	Corriente fase S		Corriente S
9		32			15			Phase C Current		Phase C Curr	Corriente fase T		Corriente T
10		32			15			AC Frequency		AC Frequency	Frecuencia			Frecuencia
11		32			15			Total Real Power		Tot Real Power	Potencia real total		Potencia real
12		32			15			Phase A Real Power		PH-A Real Power	Potencia real fase R		Pot real R
13		32			15			Phase B Real Power		PH-B Real Power	Potencia real fase S		Pot real S
14		32			15			Phase C Real Power		PH-C Real Power	Potencia real fase T		Pot real T
15		32			15			Total Reactive Power		Tot React Power		Potencia reactiva total		Pot reactiva
16		32			15			Phase A Reactive Power		PH-A React Pwr		Potencia reactiva fase R	Pot reactiva R
17		32			15			Phase B Reactive Power		PH-B React Pwr		Potencia reactiva fase S	Pot reactiva S
18		32			15			Phase C Reactive Power		PH-C React Pwr		Potencia reactiva fase T	Pot reactiva T
19		32			15			Total Apparent Power		Total App Power		Potencia aparente total		Pot aparente
20		32			15			Phase A Apparent Power		PH-A App Power			Potencia aparente fase R	Pot aparente R
21		32			15			Phase B Apparent Power		PH-B App Power			Potencia aparente fase S	Pot aparente S
22		32			15			Phase C Apparent Power		PH-C App Power			Potencia aparente fase T	Pot aparente T
23		32			15			Power Factor		Power Factor		Factor de Potencia		Factor potencia
24		32			15			Phase A Power Factor		PH-A Pwr Fact				Factor de Potencia fase R	Factor pot R
25		32			15			Phase B Power Factor		PH-B Pwr Fact				Factor de Potencia fase S	Factor pot S
26		32			15			Phase C Power Factor		PH-C Pwr Fact				Factor de Potencia fase T	Factor pot T
27		32			15			Phase A Current Crest Factor		Ia Crest Factor		Factor cresta corriente R	Fact cresta IR
28		32			15			Phase B Current Crest Factor		Ib Crest Factor		Factor cresta corriente S	Fact cresta IS
29		32			15			Phase C Current Crest Factor		Ic Crest Factor		Factor cresta corriente T	Fact cresta IT
30		32			15			Phase A Current THD		PH-A Curr THD		THD corriente fase R		THD I fase R
31		32			15			Phase B Current THD		PH-B Curr THD		THD corriente fase S		THD I fase S
32		32			15			Phase C Current THD		PH-C Curr THD		THD corriente fase T		THD I fase T
33		32			15			Phase A Voltage THD		PH-A Volt THD		THD tensión fase R		THD V fase R
34		32			15			Phase B Voltage THD		PH-B Volt THD		THD tensión fase S		THD V fase S
35		32			15			Phase C Voltage THD		PH-C Volt THD		THD tensión fase T		THD V fase T
36		32			15			Total Real Energy		Tot Real Energy		Energía Real total		Energía Real
37		32			15			Total Reactive Energy		Tot ReactEnergy		Energía Reactiva total		Energ Reactiva
38		32			15			Total Apparent Energy		Tot App Energy		Energía Aparente total		Energ Aparente
39		32			15			Ambient Temperature		Ambient Temp		Temperatura ambiente		Temp ambiente
40		32			15			Nominal Line Voltage			Nom Line Volt		Tensión nominal sistema		Tensión nominal
41		32			15			Nominal Phase Voltage			Nom Phase Volt			Tensión nominal de fase		Tens Nom fase
42		32			15			Nominal Frequency			Nom Frequency				Frecuencia Nominal		Frecuencia Nom
43		32			15			Mains Failure Alarm Threshold 1		MFA Threshold 1		Umbral alarma Fallo Red		Umb Fallo Red
44		32			15			Mains Failure Alarm Threshold 2	MFA Threshold 2		Umbral alarma Fallo Red Sev	Fallo Red Sev
45		32			15			Voltage Alarm Threshold 1		Volt Alm Trld 1		Umbral alarma tensión 1		Umb alarma V1
46		32			15			Voltage Alarm Threshold 2		Volt Alm Trld 2		Umbral alarma tensión 2		Umb alarma V2
47		32			15			Frequency Alarm Threshold		Freq Alarm Trld		Umbral alarma frecuencia	Umb alarm frec
48		32			15			High Temperature Limit			High Temp Limit		Límite Alta Temperatura		Lim alta temp
49		32			15			Low Temperature Limit			Low Temp Limit		Límite Baja Temperatura		Lim baja temp
50		32			15			Supervision Fail			Supervise Fail	Fallo supervisión		Fallo Com SM
51		32			15			High Line Voltage AB			Hi LineVolt AB	Alta tensión R-S		Alta Tens R-S
52		32			15			Very High Line Voltage AB		VHi LineVolt AB		Muy alta tensión R-S		Muy alta V R-S
53		32			15			Low Line Voltage AB			Lo LineVolt AB		Baja tensión R-S		Baja Tens R-S
54		32			15			Very Low Line Voltage AB		VLo LineVolt AB		Muy baja tensión R-S		Muy baja V R-S
55		32			15			High Line Voltage BC			Hi LineVolt BC	Alta tensión S-T		Alta Tens S-T
56		32			15			Very High Line Voltage BC		VHi LineVolt BC		Muy alta tensión S-T		Muy alta V S-T
57		32			15			Low Line Voltage BC			Lo LineVolt BC		Baja tensión S-T		Baja Tens S-T
58		32			15			Very Low Line Voltage BC		VLo LineVolt BC	Muy baja tensión S-T		Muy baja V S-T
59		32			15			High Line Voltage CA			Hi LineVolt CA	Alta tensión R-T		Alta Tens R-T
60		32			15			Very High Line Voltage CA		VHi LineVolt CA		Muy alta tensión R-T		Muy alta V R-T
61		32			15			Low Line Voltage CA			Lo LineVolt CA		Baja tensión R-T		Baja Tens R-T
62		32			15			Very Low Line Voltage CA		VLo LineVolt CA		Muy baja tensión R-T		Muy baja V R-T
63		32			15			High Phase Voltage A			Hi PhaseVolt A	Alta tensión fase R		Alta tensión R
64		32			15			Very High Phase Voltage A		VHi PhaseVolt A		Muy alta tensión fase R		Muy alta Tens R
65		32			15			Low Phase Voltage A			Lo PhaseVolt A		Baja tensión fase R		Baja tensión R
66		32			15			Very Low Phase Voltage A		VLo PhaseVolt A		Muy baja tensión fase R		Muy baja Tens R
67		32			15			High Phase Voltage B			Hi PhaseVolt B	Alta tensión fase S		Alta tensión S
68		32			15			Very High Phase Voltage B		VHi PhaseVolt B		Muy alta tensión fase S		Muy alta Tens S
69		32			15			Low Phase Voltage B			Lo PhaseVolt B		Baja tensión fase S		Baja tensión S
70		32			15			Very Low Phase Voltage B		VLo PhaseVolt B		Muy baja tensión fase S		Muy baja Tens S
71		32			15			High Phase Voltage C			Hi PhaseVolt C	Alta tensión fase T		Alta tensión T
72		32			15			Very High Phase Voltage C		VHi PhaseVolt C		Muy alta tensión fase T		Muy alta Tens T
73		32			15			Low Phase Voltage C			Lo PhaseVolt C		Baja tensión fase T		Baja tensión T
74		32			15			Very Low Phase Voltage C		VLo PhaseVolt C		Muy baja tensión fase T		Muy baja Tens T
75		32			15			Mains Failure				Mains Failure		Fallo de Red			Fallo de Red
76		32			15			Severe Mains Failure		SevereMainsFail	Fallo de Red Severo		Fallo Red Sev
77		32			15			High Frequency				High Frequency		Alta frecuencia			Alta frecuencia
78		32			15			Low Frequency				Low Frequency		Baja frecuencia			Baja frecuencia
79		32			15			High Temperature			High Temp		Alta temperatura		Alta temp
80		32			15			Low Temperature				Low Temperature		Baja temperatura		Baja temp
81		32			15			SMAC					SMAC		SMAC				SMAC
82		32			15			Supervision Fail			SMAC Fail		Fallo de supervisión		Fallo sup SMAC
83		32			15			No					No			No				No
84		32			15			Yes					Yes			Sí				Sí
85		32			15			Phase A Mains Failure Counter		PH-A ACFail Cnt		Contador Fallos fase R		Cont fallos R
86		32			15			Phase B Mains Failure Counter		PH-B ACFail Cnt		Contador Fallos fase S		Cont fallos S
87		32			15			Phase C Mains Failure Counter		PH-C ACFail Cnt		Contador Fallos fase T		Cont fallos T
88		32			15			Frequency Failure Counter		Freq Fail Cnt		Contador fallos frecuencia	Cont fallosFrec
89		32			15			Reset Phase A Mains Fail Counter	Reset A FailCnt		Iniciar cont Fallos fase R	Inic fallos R
90		32			15			Reset Phase B Mains Fail Counter	Reset B FailCnt		Iniciar cont Fallos fase S	Inic fallos S
91		32			15			Reset Phase C Mains Fail Counter	Reset C FailCnt		Iniciar cont Fallos fase T	Inic fallos T
92		32			15			Reset Frequency Counter			Rst Frq FailCnt		Iniciar cont Fallos frecuencia	Inic fallosFrec
93		32			15			Current Alarm Threshold			Curr Alm Limit	Alarma alta corriente		Alta corriente
94		32			15			Phase A  High Current			PH-A Hi Current		Alta corriente fase R		Alta I fase R
95		32			15			Phase B  High Current			PH-B Hi Current		Alta corriente fase S		Alta I fase S
96		32			15			Phase C  High Current			PH-C Hi Current		Alta corriente fase T		Alta I fase T
97		32			15			State					State			Estado				Estado
98		32			15			Off					Off			apagado				apagado
99		32			15			On					On			conectado			conectado
100		32			15			System Power				System Power		Potencia del Sistema		Potencia Sistem
101		32			15			Total System Power Consumption		Pwr Consumption		Consumo total Sistema		Consumo total
102		32			15			Existence State				Existence State		Detección			Detección
103		32			15			Existent				Existent		Existente			Existente
104		32			15			Not Existent			Not Existent		Inexistente			Inexistente


﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			SMBRC Group				SMBRC Group		Grupo SMBRC			Grupo SMBRC
2		32			15			Standby				Standby			Alerta				Alerta
3		32			15			Refresh				Refresh			Refrescar			Refrescar
4		32			15			Setting Refresh				Setting Refresh			Configurar Refresco		Ajuste Refresco
5		32			15			E-Stop					E-Stop				Función E-Stop			E-Stop
6		32			15			Yes				Yes			Sí				Sí
7		32			15			Existence State			Existence State		Detección			Detección
8		32			15			Existent			Existent		Existente			Existente
9		32			15			Not Existent				Not Existent		Inexistente			Inexistente
10		32			15			Num of SMBRCs				Num of SMBRCs		Número de SMBRCs		Núm SMBRCs
11		32			15			SMBRC Config Changed			Cfg Changed	Config SMBRC cambiada		Cfg cambiada
12		32			15			Not Changed			Not Changed		No Cambiada			No Cambiada
13		32			15			Changed				Changed			Cambiada			Cambiada
14		32			15			SMBRC Configuration			SMBRC Cfg	Configuración SMBRC		Cfg SMBRC
15		32			15			SMBRC Configuration Number		SMBRC Cfg Num		Núm Config SMBRC		Núm Cfg SMBRC
16		32			15			SMBRC Interval				SMBRC Interval		Intervalo de prueba SMBRC	Interval prueba
17		32			15			SMBRC Reset Test			SMBRC Rst Test	Prueba de Resistencia SMBRC	Prueba Resist
18		32			15			Start				Start			Iniciar				Iniciar
19		32			15			Stop				Stop			Parar				Parar
20		32			15			Cell Voltage High			Cell Volt High		Alta Tensión Celda		Alta V celda
21		32			15			Cell Voltage Low			Cell Volt Low			Baja Tensión Celda		Baja V celda
22		32			15			Cell Temperature High		Cell Temp High		Alta Temperatura Celda		AltaTemp Celda
23		32			15			Cell Temperature Low		Cell Temp Low		Baja temperatura Celda		BajaTemp Celda
24		32			15			String Current High			String Curr Hi		Alta Corriente Cadena		AltaCorr Caden
25		32			15			Ripple Current High			Ripple Curr Hi		Alta Corriente Rizado		AltaCorr Rizad
26		32			15			High Cell Resistance			Hi Cell Resist		Alta Resistencia Celda		Alta Res Celda
27		32			15			Low Cell Resistance			Low Cell Resist		Baja Resistencia Celda		Baja Res Celda
28		32			15			High Intercell Resistance		Hi InterResist		Alta R intercelda		Alta R intercel
29		32			15			Low Intercell Resistance		Low InterResist		Baja R intercelda		Baja R intercel
30		32			15			String Current Low			String Curr Low		Baja Corriente Cadena	Baja Corr Cad
31		32			15			Ripple Current Low			Ripple Curr Low		Baja Corriente Rizado	Baja Corr Rizad


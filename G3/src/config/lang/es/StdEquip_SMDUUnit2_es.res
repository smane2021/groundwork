﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32			15			Bus Bar Voltage			Bus Bar Voltage		Tensión barra distribución		Tensión barra D
2	32			15			Load 1					Load 1			Carga 1			Carga 1
3	32			15			Load 2					Load 2			Carga 2			Carga 2
4	32			15			Load 3					Load 3			Carga 3			Carga 3
5	32			15			Load 4					Load 4			Carga 4			Carga 4
6	32			15			Load Fuse 1				Load Fuse 1				Fusible carga 1				Fusible carga1
7	32			15			Load Fuse 2				Load Fuse 2				Fusible carga 2				Fusible carga2
8	32			15			Load Fuse 3				Load Fuse 3				Fusible carga 3				Fusible carga3
9	32			15			Load Fuse 4				Load Fuse 4				Fusible carga 4				Fusible carga4
10	32			15			Load Fuse 5				Load Fuse 5				Fusible carga 5				Fusible carga5
11	32			15			Load Fuse 6				Load Fuse 6				Fusible carga 6				Fusible carga6
12	32			15			Load Fuse 7				Load Fuse 7				Fusible carga 7				Fusible carga7
13	32			15			Load Fuse 8				Load Fuse 8				Fusible carga 8				Fusible carga8
14	32			15			Load Fuse 9				Load Fuse9				Fusible carga 9				Fusible carga9
15	32			15			Load Fuse 10				Load Fuse 10		Fusible carga 10			Fusible carga10
16	32			15			Load Fuse 11				Load Fuse 11		Fusible carga 11			Fusible carga11
17	32			15			Load Fuse 12				Load Fuse 12		Fusible carga 12			Fusible carga12
18	32			15			Load Fuse 13				Load Fuse 13		Fusible carga 13			Fusible carga13
19	32			15			Load Fuse 14				Load Fuse 14		Fusible carga 14			Fusible carga14
20	32			15			Load Fuse 15				Load Fuse 15		Fusible carga 15			Fusible carga15
21	32			15			Load Fuse 16				Load Fuse 16		Fusible carga 16			Fusible carga16
22	32			15			Run Time			Run Time		Tiempo de Operación			Tiempo Operando
23	32			15			LV Disconnect 1 Control			LVD 1 Control		Control LVD1				Control LVD1
24	32			15			LV Disconnect 2 Control			LVD 2 Control		Control LVD2				Control LVD2
25	32			15			LV Disconnect 1 Voltage			LVD 1 Voltage		Tensión LVD1				Tensión LVD1
26	32			15			LV Resconnect 1 Voltage			LVR 1 Voltage		Tensión LVR1				Tensión LVR1
27	32			15			LV Disconnect 2 Voltage			LVD 2 Voltage		Tensión LVD2				Tensión LVD2
28	32			15			LV Resconnect 2 Voltage			LVR 2 Voltage		Tensión LVR2				Tensión LVR2
29	32			15			On				On			Conectado				Conectado
30	32			15			Off				Off			Desconectado				Desconectado
31	32			15			Normal				Normal			Normal					Normal
32	32			15			Error				Error			Error					Error
33	32			15			On				On			Conectado				Conectado
34	32			15			Fuse 1 Alarm				Fuse 1 Alarm		Alarma Fusible 1			Alarma fus1
35	32			15			Fuse 2 Alarm				Fuse 2 Alarm		Alarma Fusible 2			Alarma fus2
36	32			15			Fuse 3 Alarm				Fuse 3 Alarm	Alarma Fusible 3			Alarma fus3
37	32			15			Fuse 4 Alarm				Fuse 4 Alarm	Alarma Fusible 4			Alarma fus4
38	32			15			Fuse 5 Alarm				Fuse 5 Alarm	Alarma Fusible 5			Alarma fus5
39	32			15			Fuse 6 Alarm				Fuse 6 Alarm	Alarma Fusible 6			Alarma fus6
40	32			15			Fuse 7 Alarm				Fuse 7 Alarm	Alarma Fusible 7			Alarma fus7
41	32			15			Fuse 8 Alarm				Fuse 8 Alarm	Alarma Fusible 8			Alarma fus8
42	32			15			Fuse 9 Alarm				Fuse 9 Alarm	Alarma Fusible 9			Alarma fus9
43	32			15			Fuse 10 Alarm				Fuse 10 Alarm	Alarma Fusible 10			Alarma fus10
44	32			15			Fuse 11 Alarm				Fuse 11 Alarm	Alarma Fusible 11			Alarma fus11
45	32			15			Fuse 12 Alarm				Fuse 12 Alarm	Alarma Fusible 12			Alarma fus12
46	32			15			Fuse 13 Alarm				Fuse 13 Alarm	Alarma Fusible 13			Alarma fus13
47	32			15			Fuse 14 Alarm				Fuse 14 Alarm	Alarma Fusible 14			Alarma fus14
48	32			15			Fuse 15 Alarm				Fuse 15 Alarm	Alarma Fusible 15			Alarma fus15
49	32			15			Fuse 16 Alarm				Fuse 16 Alarm	Alarma Fusible 16			Alarma fus16
50	32			15			HW Test Alarm				HW Test Alarm		Alarma prueba HW			Alarma test HW
51	32			15			SMDU 2					SMDU 2			SMDU 2			SMDU 2
52	32			15			Battery Fuse 1 Voltage			BattFuse 1 Volt				Tensión fusible batería 1		Tens fus bat1
53	32			15			Battery Fuse 2 Voltage			BattFuse 2 Volt				Tensión fusible batería 2		Tens fus bat2
54	32			15			Battery Fuse 3 Voltage			BattFuse 3 Volt				Tensión fusible batería 3		Tens fus bat3
55	32			15			Battery Fuse 4 Voltage			BattFuse 4 Volt				Tensión fusible batería 4		Tens fus bat4
56	32			15			Battery Fuse 1 Status			Batt Fuse 1				Estado fusible batería 1		Estado fus bat1
57	32			15			Battery Fuse 2 Status			Batt Fuse 2				Estado fusible batería 2		Estado fus bat2
58	32			15			Battery Fuse 3 Status			Batt Fuse 3				Estado fusible batería 3		Estado fus bat3
59	32			15			Battery Fuse 4 Status			Batt Fuse 4				Estado fusible batería 4		Estado fus bat4
60	32			15			On				On			Conectado				Conectado
61	32			15			Off				Off			Desconectado				Desconectado
62	32			15			Battery Fuse 1 Alarm			BattFuse 1 Alm		Alarma fusible batería 1		Alarma fus1 bat
63	32			15			Battery Fuse 2 Alarm			BattFuse 2 Alm		Alarma fusible batería 2		Alarma fus2 bat
64	32			15			Battery Fuse 3 Alarm			BattFuse 3 Alm		Alarma fusible batería 3		Alarma fus3 bat
65	32			15			Battery Fuse 4 Alarm			BattFuse 4 Alm		Alarma fusible batería 4		Alarma fus4 bat
66	32			15			All Load Current			All Load Curr		Corriente total Carga			Total Carga
67	32			15			Over Current Point(Load)		Over Curr Point		Valor de sobrecorriente			Sobrecorriente
68	32			15			Over Current(Load)			Over Current		Sobrecorriente				Sobrecorriente
69	32			15		LVD 1					LVD 1			LVD1 habilitado				LVD1 habilitado
70	32			15			LVD 1 Mode			LVD 1 Mode		Modo LVD1				Modo LVD1
71	32			15			LVD 1 Reconnect Delay			LVD1 ReconDelay	Retardo reconexión LVD1			RetarRecon LVD1
72	32			15			LVD 2					LVD 2		LVD2 habilitado				LVD2 habilitado
73	32			15			LVD 2 Mode			LVD2 Mode		Modo LVD2				Modo LVD2
74	32			15			LVD 2 Reconnect Delay			LVD2 ReconDelay			Retardo reconexión LVD2			RetarRecon LVD2
75	32			15			LVD 1 Status				LVD 1 Status		Estado LVD1				Estado LVD1
76	32			15			LVD 2 Status				LVD 2 Status		Estado LVD2				Estado LVD2
77	32			15			Disabled			Disabled		Deshabilitado				Deshabilitado
78	32			15			Enabled				Enabled			Habilitado				Habilitado
79	32			15			Voltage					Voltage				Por tensión				Por tensión
80	32			15			Time					Time		Por tiempo				Por tiempo
81	32			15			Bus Bar Voltage Alarm			Bus Bar Alarm		Alarma Barra Distribución		Alarma bus Dist
82	32			15			Normal				Normal			Normal					Normal
83	32			15			Low				Low			Bajo					Bajo
84	32			15			High				High			Alto					Alto
85	32			15			Under Voltage				Under Voltage	Baja tensión				Baja tensión
86	32			15			Over Voltage				Over Voltage		Alta tensión				Alta tensión
87	32			15			Shunt 1 Current Alarm			Shunt 1 Alarm		Alarma corriente shunt1			Alarma shunt1
88	32			15			Shunt 2 Current Alarm			Shunt 2 Alarm		Alarma corriente shunt2			Alarma shunt2
89	32			15			Shunt 3 Current Alarm			Shunt 3 Alarm		Alarma corriente shunt3			Alarma shunt3
90	32			15			Shunt 4 Current Alarm			Shunt 4 Alarm		Alarma corriente shunt4			Alarma shunt4
91	32			15			Shunt 1 Over Current			Shunt 1 OverCur		Sobrecorriente en shunt1		Sobrecor shunt1
92	32			15			Shunt 2 Over Current			Shunt 2 OverCur		Sobrecorriente en shunt2		Sobrecor shunt2
93	32			15			Shunt 3 Over Current			Shunt 3 OverCur		Sobrecorriente en shunt3		Sobrecor shunt3
94	32			15			Shunt 4 Over Current			Shunt 4 OverCur		Sobrecorriente en shunt4		Sobrecor shunt4
95	32			15			Times of Communication Fail		Times Comm Fail				Interrupciones				Interrupciones
96	32			15			Existent			Existent		Existente				Existente
97	32			15			Not Existent				Not Existent		Inexistente				Inexistente
98	32			15			Very Low			Very Low		Muy bajo				Muy bajo
99	32			15			Very High			Very High		Muy alto				Muy alto
100	32			15			Switch				Switch			Interruptor				Interruptor
101	32			15			LVD1 Failure			LVD 1 Failure		Fallo LVD1				Fallo LVD1
102	32			15			LVD2 Failure			LVD 2 Failure		Fallo LVD2				Fallo LVD2
103	32			15			High Temperature Disconnect 1	HTD 1		Habilitar HTD1				Habilitar HTD1
104	32			15			High Temperature Disconnect 2	HTD 2		Habilitar HTD2				Habilitar HTD2
105	32			15			Battery LVD			Battery LVD		LVD de batería				LVD de batería
106	32			15			No Battery			No Battery		Sin batería				Sin batería
107	32			15			LVD 1				LVD 1			LVD1					LVD1
108	32			15			LVD 2				LVD 2			LVD2					LVD2
109	32			15			Battery Always On			Batt Always On		Batería siempre conectada		Siempre con Bat
110	32			15			Barcode				Barcode			Barcode					Barcode
111	32			15			DC Over Voltage				DC Over Volt	Sobretensión CC				Sobretensión CC
112	32			15			DC Under Voltage			DC Under Volt		Subtensión CC				Subtensión CC
113	32			15			Over Current 1				Over Curr 1		Sobrecorriente 1			Sobrecorrien 1
114	32			15			Over Current 2				Over Curr 2		Sobrecorriente 2			Sobrecorrien 2
115	32			15			Over Current 3				Over Curr 3		Sobrecorriente 3			Sobrecorrien 3
116	32			15			Over Current 4				Over Curr 4		Sobrecorriente 4			Sobrecorrien 4
117	32			15			Existence State			Existence State		Detección				Detección
118	32			15			Communication Fail			Comm Fail		Fallo de Comunicación		Fallo COM
119	32			15			Bus Voltage Status			Bus Volt Status			Estado Bus Tensión			Estado Bus V
120	32			15			Comm OK					Comm OK			Comunicación OK			Comunicación OK
121	32			15			All Batteries Comm Fail			AllBattCommFail		Todas las baterías en Fallo Com		FalloComTotBat
122	32			15			Communication Fail			Comm Fail		Fallo Comunicación			Fallo COM
123	32			15			Rated Capacity				Rated Capacity		Capacidad Estimada			Capacidad Est
124	32			15			Load 5					Load 5			Corriente 5 (Carga)			Corriente 5(C)
125	32			15			Shunt 1 Voltage			Shunt 1 Voltage	Tensión Shunt 1				Tensión Shunt1
126	32			15			Shunt 1 Current			Shunt 1 Current		Corriente Shunt 1			Corrien Shunt1
127	32			15			Shunt 2 Voltage			Shunt 2 Voltage	Tensión Shunt 2				Tensión Shunt2
128	32			15			Shunt 2 Current			Shunt 2 Current		Corriente Shunt 2			Corrien Shunt2
129	32			15			Shunt 3 Voltage			Shunt 3 Voltage	Tensión Shunt 3				Tensión Shunt3
130	32			15			Shunt 3 Current			Shunt 3 Current		Corriente Shunt 3			Corrien Shunt3
131	32			15			Shunt 4 Voltage			Shunt 4 Voltage	Tensión Shunt 4				Tensión Shunt4
132	32			15			Shunt 4 Current			Shunt 4 Current		Corriente Shunt 4			Corrien Shunt4
133	32			15			Shunt 5 Voltage			Shunt 5 Voltage	Tensión Shunt 5				Tensión Shunt5
134	32			15			Shunt 5 Current			Shunt 5 Current		Corriente Shunt 5			Corrien Shunt5
170	32			15			Current1 High 1 Current		Curr1 Hi1		Alta1 Corriente 1		Alta1 Corr1
171	32			15			Current1 High 2 Current		Curr1 Hi2		Alta2 Corriente 1		Alta2 Corr1
172	32			15			Current2 High 1 Current			Curr2 Hi1		Alta1 Corriente 2		Alta1 Corr2
173	32			15			Current2 High 2 Current			Curr2 Hi2		Alta2 Corriente 2		Alta2 Corr2
174	32			15			Current3 High 1 Current			Curr3 Hi1		Alta1 Corriente 3		Alta1 Corr3
175	32			15			Current3 High 2 Current			Curr3 Hi2		Alta2 Corriente 3		Alta2 Corr3
176	32			15			Current4 High 1 Current			Curr4 Hi1		Alta1 Corriente 4		Alta1 Corr4
177	32			15			Current4 High 2 Current			Curr4 Hi2		Alta2 Corriente 4		Alta2 Corr4
178	32			15			Current5 High 1 Current			Curr5 Hi1		Alta1 Corriente 5		Alta1 Corr5
179	32			15			Current5 High 2 Current			Curr5 Hi2		Alta2 Corriente 5		Alta2 Corr5
220	32			15			Current1 High 1 Current Limit		Curr1 Hi1 Lmt		Alta1 corr.1 Límite de corr.		Alta1 Corr1Lmt	
221	32			15			Current1 High 2 Current Limit		Curr1 Hi2 Lmt		Alta2 corr.1 Límite de corr.		Alta2 Corr1Lmt	
222	32			15			Current2 High 1 Current Limit		Curr2 Hi1 Lmt		Alta1 corr.2 Límite de corr.		Alta1 Corr2Lmt	
223	32			15			Current2 High 2 Current Limit		Curr2 Hi2 Lmt		Alta2 corr.2 Límite de corr.		Alta2 Corr2Lmt	
224	32			15			Current3 High 1 Current Limit		Curr3 Hi1 Lmt		Alta1 corr.3 Límite de corr.		Alta1 Corr3Lmt	
225	32			15			Current3 High 2 Current Limit		Curr3 Hi2 Lmt		Alta2 corr.3 Límite de corr.		Alta2 Corr3Lmt	
226	32			15			Current4 High 1 Current Limit		Curr4 Hi1 Lmt		Alta1 corr.4 Límite de corr.		Alta1 Corr4Lmt	
227	32			15			Current4 High 2 Current Limit		Curr4 Hi2 Lmt		Alta2 corr.4 Límite de corr.		Alta2 Corr4Lmt	
228	32			15			Current5 High 1 Current Limit		Curr5 Hi1 Lmt		Alta1 corr.5 Límite de corr.		Alta1 Corr5Lmt	
229	32			15			Current5 High 2 Current Limit		Curr5 Hi2 Lmt		Alta2 corr.5 Límite de corr.		Alta2 Corr5Lmt
270	32			15			Current1 Break Value			Curr 1 Brk Val		Talla disyuntor corriente 1		Talla Disy I1
271	32			15			Current2 Break Value			Curr 2 Brk Val		Talla disyuntor corriente 2		Talla Disy I2
272	32			15			Current3 Break Value			Curr 3 Brk Val		Talla disyuntor corriente 3		Talla Disy I3
273	32			15			Current4 Break Value			Curr 4 Brk Val		Talla disyuntor corriente 4		Talla Disy I4
274	32			15			Current5 Break Value			Curr 5 Brk Val		Talla disyuntor corriente 5		Talla Disy I5

281	32			15			Set Shunt Coefficient			Set Shunt Coeff	Configuración valor shunt		Cfg Valor Shunt
#282	32			15		Shunt 5 Coefficient Display		Shunt5 Display	Disp Shunt5				Disp Shunt5
283	32			15			Shunt1 Coefficient Conflict		Shunt1 Conflict		Conflicto config Shunt 1		Conflict Shunt1
284	32			15			Shunt2 Coefficient Conflict		Shunt2 Conflict		Conflicto config Shunt 2		Conflict Shunt2
285	32			15			Shunt3 Coefficient Conflict		Shunt3 Conflict		Conflicto config Shunt 3		Conflict Shunt3
286	32			15			Shunt4 Coefficient Conflict		Shunt4 Conflict		Conflicto config Shunt 4		Conflict Shunt4
287	32			15			Shunt5 Coefficient Conflict		Shunt5 Conflict		Conflicto config Shunt 5		Conflict Shunt5

290	32			15			By Software			By Software		Software				Software
291	32			15			By Dip-Switch				By Dip-Switch		Microint				Microint
292	32			15			Not Supported				Not Supported		Ninguno					Ninguno
293	32			15			Not Used				Not Used				No Utilizada			No Utilizada
294	32			15			General				General				General	General
295	32			15			Load						Load						Carga	Carga
296	32			15			Battery					Battery					Batería				Batería
297	32			15			Shunt1 Set As			Shunt1SetAs			Shunt1 fijado como	Shunt1 como
298	32			15			Shunt2 Set As			Shunt2SetAs			Shunt2 fijado como	Shunt2 como
299	32			15			Shunt3 Set As			Shunt3SetAs			Shunt3 fijado como	Shunt3 como
300	32			15			Shunt4 Set As			Shunt4SetAs			Shunt4 fijado como	Shunt4 como
301	32			15			Shunt5 Set As			Shunt5SetAs			Shunt5 fijado como	Shunt5 como
302	32			15			Shunt 1		Shunt 1		Lectura Shunt1	Lectura Shunt1
303	32			15			Shunt 2		Shunt 2		Lectura Shunt2	Lectura Shunt2
304	32			15			Shunt 3		Shunt 3		Lectura Shunt3	Lectura Shunt3
305	32			15			Shunt 4		Shunt 4		Lectura Shunt4	Lectura Shunt4
306	32			15			Shunt 5		Shunt 5		Lectura Shunt5	Lectura Shunt5
307	32			15			Source 1		Source 1		Fuente 1		Fuente 1
308	32			15			Source 2		Source 2		Fuente 2		Fuente 2
309	32			15			Source 3		Source 3		Fuente 3		Fuente 3
310	32			15			Source 4		Source 4		Fuente 4		Fuente 4
311	32			15			Source 5		Source 5		Fuente 5		Fuente 5
500	32			15			Current1 High 1 Curr			Curr1 Hi1Cur			Alta1 Corriente 1		Alta1 Corr1
501	32			15			Current1 High 2 Curr			Curr1 Hi2Cur		Alta2 Corriente 1		Alta2 Corr1
502	32			15			Current2 High 1 Curr			Curr2 Hi1Cur					Alta1 Corriente 2		Alta1 Corr2
503	32			15			Current2 High 2 Curr			Curr2 Hi2Cur				Alta2 Corriente 2		Alta2 Corr2
504	32			15			Current3 High 1 Curr			Curr3 Hi1Cur					Alta1 Corriente 3		Alta1 Corr3
505	32			15			Current3 High 2 Curr			Curr3 Hi2Cur				Alta2 Corriente 3		Alta2 Corr3
506	32			15			Current4 High 1 Curr			Curr4 Hi1Cur					Alta1 Corriente 4		Alta1 Corr4
507	32			15			Current4 High 2 Curr			Curr4 Hi2Cur				Alta2 Corriente 4		Alta2 Corr4
508	32			15			Current5 High 1 Curr			Curr5 Hi1Cur					Alta1 Corriente 5		Alta1 Corr5
509	32			15			Current5 High 2 Curr			Curr5 Hi2Cur				Alta2 Corriente 5		Alta2 Corr5

550		32			15			Source			Source			Fuente					Fuente

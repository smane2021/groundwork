﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			DC Distribution			DC Distr		Distribución		Distribución
2		32			15			DC Voltage			DC Voltage	Tensión CC		Tensión CC
3		32			15			Load Current			LoadCurr		Corriente de carga	Corriente
4		32			15			Load Shunt			Load Shunt		Shunt de carga SCU	Shunt carga SCU
5		32			15			Disabled			Disabled		No			No
6		32			15			Enabled				Enabled			Sí			Sí
7		32			15			Shunt Current			Shunt Current		Corriente Shunt		Corriente Shunt
8		32			15			Shunt Voltage			Shunt Voltage		Tensión Shunt		Tensión Shunt
9		32			15			Load Shunt Exist		LoadShuntExist		Shunt de carga		Shunt Carga
10		32			15			Yes				Yes			Sí			Sí
11		32			15			No				No			No			No
12		32			15			Over Voltage 1			Over Voltage 1		Sobretensión 1		Sobretensión 1
13		32			15			Over Voltage 2			Over Voltage 2	Sobretensión 2		Sobretensión 2
14		32			15			Under Voltage 1			Under Voltage 1	Subtensión 1		Subtensión 1
15		32			15			Under Voltage 2			Under Voltage 2		Subtensión 2		Subtensión 2
16		32			15			Over Voltage 1 (24V)		24V Over Volt1		Sobretensión 1(24V)	Sobretens1(24V)
17		32			15			Over Voltage 2 (24V)		24V Over Volt2			Sobretensión 2(24V)	Sobretens2(24V)
18		32			15			Under Voltage 1 (24V)		24V Under Volt1	Subtensión 1(24V)	Subtens 1(24V)
19		32			15			Under Voltage 2 (24V)		24V Under Volt2		Subtensión 2(24V)	Subtens 2(24V)
20		32				15			Total Load Current			TotalLoadCurr		Corriente total carga		Total carga
500	32			15			Current Break Size				Curr1 Brk Size		Tamaño actual rotura		TamActualRotura																														
501	32			15			Current High 1 Current Limit			Curr1 Hi1 Limit		Alta corr.1 Límite de corr.		Alta1 Corr1Lmt																														
502	32			15			Current High 2 Current Limit		Curr1 Hi2 Lmt		Alta corr.2 Límite de corr.		Alta2 Corr1Lmt																														
503	32			15			Current High 1 Curr		Curr Hi1Cur		Alta corriente 1 corriente		Alta1 Corr																																															
504	32			15			Current High 2 Curr		Curr Hi2Cur		Alta corriente 2 corriente		Alta2 Corr
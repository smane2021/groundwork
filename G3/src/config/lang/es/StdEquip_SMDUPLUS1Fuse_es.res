﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es



[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Fuse 1			Fuse 1			Fusible 1			Fusible 1
2	32			15			Fuse 2			Fuse 2			Fusible 2			Fusible 2
3	32			15			Fuse 3			Fuse 3			Fusible 3			Fusible 3
4	32			15			Fuse 4			Fuse 4			Fusible 4			Fusible 4
5	32			15			Fuse 5			Fuse 5			Fusible 5			Fusible 5
6	32			15			Fuse 6			Fuse 6			Fusible 6			Fusible 6
7	32			15			Fuse 7			Fuse 7			Fusible 7			Fusible 7
8	32			15			Fuse 8			Fuse 8			Fusible 8			Fusible 8
9	32			15			Fuse 9			Fuse 9			Fusible 9			Fusible 9
10	32			15			Fuse 10			Fuse 10			Fusible 10			Fusible 10
11	32			15			Fuse 11			Fuse 11			Fusible 11			Fusible 11
12	32			15			Fuse 12			Fuse 12			Fusible 12			Fusible 12
13	32			15			Fuse 13			Fuse 13			Fusible 13			Fusible 13
14	32			15			Fuse 14			Fuse 14			Fusible 14			Fusible 14
15	32			15			Fuse 15			Fuse 15			Fusible 15			Fusible 15
16	32			15			Fuse 16			Fuse 16			Fusible 16			Fusible 16
17	32			15		SMDUP1 DC Fuse		SMDUP1 DC Fuse		Fusible CC SMDUP1		Fus CC SMDUP1
18	32			15			State			State			Estado				Estado
19	32			15			Off			Off			Desconectado			Desconectado
20	32			15			On			On			Conectado			Conectado
21	32			15			DC Fuse 1 Alarm			Fuse 1 Alm		Alarma Fusible 1		Alarma fus1
22	32			15			DC Fuse 2 Alarm			Fuse 2 Alm		Alarma Fusible 2		Alarma fus2
23	32			15			DC Fuse 3 Alarm			Fuse 3 Alm		Alarma Fusible 3		Alarma fus3
24	32			15			DC Fuse 4 Alarm			Fuse 4 Alm		Alarma Fusible 4		Alarma fus4
25	32			15			DC Fuse 5 Alarm			Fuse 5 Alm		Alarma Fusible 5		Alarma fus5
26	32			15			DC Fuse 6 Alarm			Fuse 6 Alm		Alarma Fusible 6		Alarma fus6
27	32			15			DC Fuse 7 Alarm			Fuse 7 Alm		Alarma Fusible 7		Alarma fus7
28	32			15			DC Fuse 8 Alarm			Fuse 8 Alm		Alarma Fusible 8		Alarma fus8
29	32			15			DC Fuse 9 Alarm			Fuse 9 Alm		Alarma Fusible 9		Alarma fus9
30	32			15			DC Fuse 10 Alarm		Fuse 10 Alm		Alarma Fusible 10		Alarma fus10
31	32			15			DC Fuse 11 Alarm		Fuse 11 Alm		Alarma Fusible 11		Alarma fus11
32	32			15			DC Fuse 12 Alarm		Fuse 12 Alm		Alarma Fusible 12		Alarma fus12
33	32			15			DC Fuse 13 Alarm		Fuse 13 Alm		Alarma Fusible 13		Alarma fus13
34	32			15			DC Fuse 14 Alarm		Fuse 14 Alm		Alarma Fusible 14		Alarma fus14
35	32			15			DC Fuse 15 Alarm		Fuse 15 Alm		Alarma Fusible 15		Alarma fus15
36	32			15			DC Fuse 16 Alarm		Fuse 16 Alm		Alarma Fusible 16		Alarma fus16
37	32			15			Times of Communication Fail		Times Comm Fail		Fallos de Comunicación		Fallos COM
38	32			15			Communication Fail			Comm Fail		Fallo Comunicación	Fallo Com
39	32			15		Fuse 17			Fuse 17			Fusible 17			Fusible 17
40	32			15		Fuse 18			Fuse 18			Fusible 18			Fusible 18
41	32			15		Fuse 19			Fuse 19			Fusible 19			Fusible 19
42	32			15		Fuse 20			Fuse 20			Fusible 20			Fusible 20
43	32			15		Fuse 21			Fuse 21			Fusible 21			Fusible 21
44	32			15		Fuse 22			Fuse 22			Fusible 22			Fusible 22
45	32			15		Fuse 23			Fuse 23			Fusible 23			Fusible 23
46	32			15		Fuse 24			Fuse 24			Fusible 24			Fusible 24
47	32			15		Fuse 25			Fuse 25			Fusible 25			Fusible 25
48	32			15		DC Fuse 17 Alarm		Fuse 17 Alm		Alarma Fusible 17		Alarma fus17
49	32			15		DC Fuse 18 Alarm		Fuse 18 Alm		Alarma Fusible 18		Alarma fus18
50	32			15		DC Fuse 19 Alarm		Fuse 19 Alm		Alarma Fusible 19		Alarma fus19
51	32			15		DC Fuse 20 Alarm		Fuse 20 Alm		Alarma Fusible 20		Alarma fus20
52	32			15		DC Fuse 21 Alarm		Fuse 21 Alm		Alarma Fusible 21		Alarma fus21
53	32			15		DC Fuse 22 Alarm		Fuse 22 Alm		Alarma Fusible 22		Alarma fus22
54	32			15		DC Fuse 23 Alarm		Fuse 23 Alm		Alarma Fusible 23		Alarma fus23
55	32			15		DC Fuse 24 Alarm		Fuse 24 Alm		Alarma Fusible 24		Alarma fus24
56	32			15		DC Fuse 25 Alarm		Fuse 25 Alm		Alarma Fusible 25		Alarma fus25

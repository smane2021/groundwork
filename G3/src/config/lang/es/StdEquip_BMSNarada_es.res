﻿#
#  Locale language support:Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			BMS				BMS		BMS			BMS
2	32			15			Rated Capacity				Rated Capacity		Capacidad Nominal		Cap Nominal
3	32			15			Communication Fail			Comm Fail		Fallo Comunicación		Fallo COM
4	32			15			Existence State				Existence State		Detección		Detección
5	32			15			Existent				Existent		Existente			Existente
6	32			15			Not Existent				Not Existent		No Existente			No Existente
7	32			15			Battery Voltage				Batt Voltage		Tensión Batería	Tensión Batería
8	32			15			Battery Current				Batt Current		Corriente Batería		Corriente Bat
9	32			15			Battery Rating (Ah)			Batt Rating(Ah)		Capacidad Estimada (Ah)		C.Estimada (Ah)
10	32			15			Battery Capacity (%)			Batt Cap (%)		Capacidad Batería (%)	Cap Batería (%)
11	32			15			Bus Voltage				Bus Voltage		Tensión Bus		Tensión Bus
12	32			15			Battery Center Temperature(AVE)		Batt Temp(AVE)		Temperatura media Batería		Temp medio Bat
13	32			15			Ambient Temperature			Ambient Temp		Temperatura Ambiente		Temp Ambiente
29	32			15			Yes					Yes			Sí			Sí
30	32			15			No					No			No		No
31	32			15			Single Over Voltage Alarm		SingleOverVolt		Alarma Sobretensión Celda		SobreV Celda
32	32			15			Single Under Voltage Alarm		SingleUnderVolt		Alarma Subtensión Celda		Subtens Celda
33	32			15			Whole Over Voltage Alarm		WholeOverVolt		Sobretensión Cadena Baterías		SobreV Cadena
34	32			15			Whole Under Voltage Alarm		WholeUnderVolt			Subtensión Cadena Baterías		Subtens Cadena
35	32			15			Charge Over Current Alarm		ChargeOverCurr		Alarma Sobrecorriente Carga		SobreCorr Carga
36	32			15			Discharge Over Current Alarm		DisCharOverCurr		Alarma Sobrecorriente Descarga		SobreCorr Desc
37	32			15			High Battery Temperature Alarm		HighBattTemp		Alta Temperatura Batería		Alta Temp Bat
38	32			15			Low Battery Temperature Alarm		LowBattTemp		Baja Temperatura Batería		Baja Temp Bat
39	32			15			High Ambient Temperature Alarm		HighAmbientTemp		Alta Temperatura Ambiente		Alta Temp Amb
40	32			15			Low Ambient Temperature Alarm		LowAmbientTemp		Baja Temperatura Ambiente		Baja Temp Amb
41	32			15			High PCB Temperature Alarm		HighPCBTemp		Alta Temperatura PCB		Alta Temp PCB
42	32			15			Low Battery Capacity Alarm		LowBattCapacity		Baja Capacidad Batería		Baja Cap Bat
43	32			15			High Voltage Difference Alarm		HighVoltDiff		Alarma Alta Diferencia Tensión		Alta Dif Tens
44	32			15			Single Over Voltage Protect		SingOverVProt		Protección Sobretensión Celda		Prot SbrV Celda
45	32			15			Single Under Voltage Protect		SingUnderVProt		Protección Subtensión Celda		Prot SubV Celda
46	32			15			Whole Over Voltage Protect		WholeOverVProt	Protección Sobretens Cadena Bat		Prot SbrV CaBat
47	32			15			Whole Under Voltage Protect		WholeUnderVProt		Protección Subtensión Cadena Bat		Prot SubV CaBat
48	32			15			Short Circuit Protect			ShortCircProt		Protección Cortocircuito		P Cortocircuito
49	32			15			Over Current Protect			OverCurrProt		Protección Sobrecorriente		Prot SobreCorr
50	32			15			Charge High Temperature Protect		CharHiTempProt		Protección Carga a Alta Temp		ProtCarga AltaT
51	32			15			Charge Low Temperature Protect		CharLoTempProt		Protección Carga a Baja Temp		ProtCarga BajaT
52	32			15			Discharge High Temp Protect		DischHiTempProt		Protección Descarga Alta Temp		ProtDescg AltaT
53	32			15			Discharge Low Temp Protect		DischLoTempProt		Protección Descarga Baja Temp		ProtDescg BajaT
54	32			15			Front End Sample Comm Fault		SampCommFault		Fallo Comunicación Web	Fallo COM Web
55	32			15			Temp Sensor Disconnect Fault		TempSensDiscFlt		Fallos Sensor Temperatura	Fallo Sensor T
56	32			15			Charging				Charging		Cargando			Cargando
57	32			15			Discharging				Discharging		Descargando			Descargando
58	32			15			Charging MOS Breakover			CharMOSBrkover		Ruptura MOS Carga		MOS Carga roto
59	32			15			Dischanging MOS Breakover		DischMOSBrkover		Ruptura MOS Descarga		MOS Descg roto
60	32			15			Communication Address			CommAddress		Dirección Comunicación		Dir COM
61	32			15			Current Limit Activation		CurrLmtAct		Activación Límite de Corriente		Lim corriente




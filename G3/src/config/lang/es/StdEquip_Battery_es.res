﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Battery Current			BattCurr			Corriente Batería		CorrBat
2	32			15			Battery Rating (Ah)			Batt Rating(Ah)		Capacidad (Ah)			Capacidad (Ah)
3	32			15			Battery Current Limit Exceeded		Ov Batt Cur Lmt		Limite corriente excedido	Lim corr pasado
4	32			15			Battery				Battery			Batería				Batería
5	32			15			Battery Over Current			Batt Over Curr		Sobrecorriente			Sobrecorriente
6	32			15			Battery Capacity (%)			Batt Cap (%)		Capacidad batería (%)		Cap Bat (%)
7	32			15			Battery Voltage				Battery Voltage		Tensión batería			Tensión batería
8	32			15			Low Battery Capacity			Low Batt Cap		Baja capacidad			Baja capacidad
9	32			15			Battery Fuse Failure			Batt Fuse Fail		Fallo fusible batería		Fallo fus Bat
10	32			15			DC Distribution Sequence Number		DC Distr SeqNum		Num Seq de distribución		NSeq Distrib
11	32			15			Battery Over Voltage			Batt Over Volt		Sobretensión			Sobretensión
12	32			15			Battery Under Voltage			Batt Under Volt		Subtensión			Subtensión
13	32			15			Battery Over Current			Batt Over Curr		Sobrecorriente			Sobrecorriente
14	32			15			Battery Fuse Failure			Batt Fuse Fail		Fallo fusible batería		Fallo fus Bat
15	32			15			Battery Over Voltage			Batt Over Volt		Sobretensión			Sobretensión
16	32			15			Battery Under Voltage			Batt Under Volt		Subtensión			Subtensión
17	32			15			Battery Over Current			Batt Over Curr		Sobrecorriente			Sobrecorriente
18	32			15			Battery				Battery			Batería				Batería
19	32			15			Battery Sensor Coefficient		BattSensorCoef		Coeficiente sensor Bat		Coef Sens Bat
20	32			15			Over Voltage Limit			Over Volt Limit		Nivel sobretensión		Sobretensión
21	32			15			Low Voltage Limit			Low Volt Limit		Nivel subtensión		Subtensión
22	32			15			Battery Communication Fail			Batt Comm Fail		Todas las baterías en Fallo Com		FalloComTotBat
23	32			15			Communication OK		Comm OK			Comunicación OK			Comunicación OK
24	32			15			Communication Fail		Comm Fail		Fallo Comunicación		Fallo Com
25	32			15			Communication Fail		Comm Fail		Fallo Comunicación		Fallo Com
26	32			15			Shunt Current				Shunt Current		Corriente Shunt			Corr Shunt
27	32			15			Shunt Voltage				Shunt Voltage		Tensión Shunt			Tens Shunt
28	32			15			Battery Management		Batt Management		Utilizada en Gestión Bat	Incl Gestión
29	32			15			Yes				Yes			Sí				Sí
30	32			15			No				No			No				No
31	32			15			On				On			Conectado			Conectado
32	32			15			Off				Off			Apagado				Apagado
33	32			15			State				State			Estado				Estado
44	32			15			Battery Temperature Probe Number	BatTempProbeNum			Sensor Temp utilizado		SensT usado
87	32			15			No					No			Ninguno				Ninguno
91	32			15			Temperature 1				Temp 1		Sensor temperatura 1		Sens Temp 1
92	32			15			Temperature 2				Temp 2		Sensor temperatura 2		Sens Temp 2
93	32			15			Temperature 3				Temp 3		Sensor temperatura 3		Sens Temp 3
94	32			15			Temperature 4				Temp 4		Sensor temperatura 4		Sens Temp 4
95	32			15			Temperature 5				Temp 5		Sensor temperatura 5		Sens Temp 5
96	32			15			Rated Capacity			Rated Capacity		Capacidad nominal C10		Capacidad C10
97	32			15			Battery Temperature		Battery Temp		Temperatura de Batería		Temp Batería
98	32			15			Battery Temperature Sensor		Bat Temp Sensor		Sensor Temperatura Batería	Sensor Temp
99	32			15			None				None			Ninguno				Ninguno
100	32			15			Temperature 1			Temp 1			Temperatura 1			Temp 1
101	32			15			Temperature 2			Temp 2			Temperatura 2			Temp 2
102	32			15			Temperature 3			Temp 3			Temperatura 3			Temp 3
103	32			15			Temperature 4			Temp 4			Temperatura 4			Temp 4
104	32			15			Temperature 5			Temp 5			Temperatura 5			Temp 5
105	32			15			Temperature 6			Temp 6			Temperatura 6			Temp 6
106	32			15			Temperature 7			Temp 7			Temperatura 7			Temp 7
107	32			15			Temperature 8			Temp 8			Temperatura 8			Temp 8
108	32			15			Temperature 9			Temp 9			Temperatura 9			Temp 9
109	32			15			Temperature 10			Temp 10			Temperatura 10			Temp 10
500	32			15			Current Break Size			Curr1 Brk Size		Tamaño actual rotura		TamActualRotura
501	32			15			Current High 1 Current Limit		Curr1 Hi1 Lmt		Alta corr.1 Límite de corr.		Alta1 Corr1Lmt
502	32			15			Current High 2 Current Limit		Curr1 Hi2 Lmt		Alta corr.2 Límite de corr.		Alta2 Corr1Lmt
503	32			15			Battery Current High 1 Curr		BattCurr Hi1Cur		Corriente Batería Alta 1		CorrBat Alta 1
504	32			15			Battery Current High 2 Curr		BattCurr Hi2Cur		Corriente Batería Alta 2		CorrBat Alta 2
505	32			15			Battery 1				Battery 1		Batería 1			Batería 1
506	32			15			Battery 2				Battery 2		Batería 2			Batería 2

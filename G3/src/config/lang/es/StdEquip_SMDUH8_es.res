﻿#
#  Locale language support:spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Bus Bar Voltage				Bus Bar Voltage		Tensión de barras		V de barras
2	32			15			Load 1 Current				Load 1 Current		Corriente Carga 1		Carga 1
3	32			15			Load 2 Current				Load 2 Current		Corriente Carga 2		Carga 2
4	32			15			Load 3 Current				Load 3 Current		Corriente Carga 3		Carga 3
5	32			15			Load 4 Current				Load 4 Current		Corriente Carga 4		Carga 4
6	32			15			Load 5 Current				Load 5 Current		Corriente Carga 5		Carga 5
7	32			15			Load 6 Current				Load 6 Current		Corriente Carga 6		Carga 6
8	32			15			Load 7 Current				Load 7 Current		Corriente Carga 7		Carga 7
9	32			15			Load 8 Current				Load 8 Current		Corriente Carga 8		Carga 8
10	32			15			Load 9 Current				Load 9 Current		Corriente Carga 9		Carga 9
11	32			15			Load 10 Current				Load 10 Current		Corriente Carga 10		Carga 10
12	32			15			Load 11 Current				Load 11 Current		Corriente Carga 11		Carga 11
13	32			15			Load 12 Current				Load 12 Current		Corriente Carga 12		Carga 12
14	32			15			Load 13 Current				Load 13 Current		Corriente Carga 13		Carga 13
15	32			15			Load 14 Current				Load 14 Current		Corriente Carga 14		Carga 14
16	32			15			Load 15 Current				Load 15 Current		Corriente Carga 15		Carga 15
17	32			15			Load 16 Current				Load 16 Current		Corriente Carga 16		Carga 16
18	32			15			Load 17 Current				Load 17 Current		Corriente Carga 17		Carga 17
19	32			15			Load 18 Current				Load 18 Current		Corriente Carga 18		Carga 18
20	32			15			Load 19 Current				Load 19 Current		Corriente Carga 19		Carga 19
21	32			15			Load 20 Current				Load 20 Current		Corriente Carga 20		Carga 20
22	32			15			Power1					Power1			Potencia1			Potencia1
23	32			15			Power2					Power2			Potencia2			Potencia2
24	32			15			Power3					Power3			Potencia3			Potencia3
25	32			15			Power4					Power4			Potencia4			Potencia4
26	32			15			Power5					Power5			Potencia5			Potencia5
27	32			15			Power6					Power6			Potencia6			Potencia6
28	32			15			Power7					Power7			Potencia7			Potencia7
29	32			15			Power8					Power8			Potencia8			Potencia8
30	32			15			Power9					Power9			Potencia9			Potencia9
31	32			15			Power10					Power10			Potencia10			Potencia10
32	32			15			Power11					Power11			Potencia11			Potencia11
33	32			15			Power12					Power12			Potencia12			Potencia12
34	32			15			Power13					Power13			Potencia13			Potencia13
35	32			15			Power14					Power14			Potencia14			Potencia14
36	32			15			Power15					Power15			Potencia15			Potencia15
37	32			15			Power16					Power16			Potencia16			Potencia16
38	32			15			Power17					Power17			Potencia17			Potencia17
39	32			15			Power18					Power18			Potencia18			Potencia18
40	32			15			Power19					Power19			Potencia19			Potencia19
41	32			15			Power20					Power20			Potencia20			Potencia20
42	32			15			Energy Yesterday in Channel 1		CH1EnergyYTD		Energía Hoy Canal 1		EnerHoy Canal1
43	32			15			Energy Yesterday in Channel 2		CH2EnergyYTD		Energía Hoy Canal 2		EnerHoy Canal2
44	32			15			Energy Yesterday in Channel 3		CH3EnergyYTD		Energía Hoy Canal 3		EnerHoy Canal3
45	32			15			Energy Yesterday in Channel 4		CH4EnergyYTD		Energía Hoy Canal 4		EnerHoy Canal4
46	32			15			Energy Yesterday in Channel 5		CH5EnergyYTD		Energía Hoy Canal 5		EnerHoy Canal5
47	32			15			Energy Yesterday in Channel 6		CH6EnergyYTD		Energía Hoy Canal 6		EnerHoy Canal6
48	32			15			Energy Yesterday in Channel 7		CH7EnergyYTD		Energía Hoy Canal 7		EnerHoy Canal7
49	32			15			Energy Yesterday in Channel 8		CH8EnergyYTD		Energía Hoy Canal 8		EnerHoy Canal8
50	32			15			Energy Yesterday in Channel 9		CH9EnergyYTD		Energía Hoy Canal 9		EnerHoy Canal9
51	32			15			Energy Yesterday in Channel 10		CH10EnergyYTD		Energía Hoy Canal 10		EnerHoy Canal10
52	32			15			Energy Yesterday in Channel 11		CH11EnergyYTD		Energía Hoy Canal 11		EnerHoy Canal11
53	32			15			Energy Yesterday in Channel 12		CH12EnergyYTD		Energía Hoy Canal 12		EnerHoy Canal12
54	32			15			Energy Yesterday in Channel 13		CH13EnergyYTD		Energía Hoy Canal 13		EnerHoy Canal13
55	32			15			Energy Yesterday in Channel 14		CH14EnergyYTD		Energía Hoy Canal 14		EnerHoy Canal14
56	32			15			Energy Yesterday in Channel 15		CH15EnergyYTD		Energía Hoy Canal 15		EnerHoy Canal15
57	32			15			Energy Yesterday in Channel 16		CH16EnergyYTD		Energía Hoy Canal 16		EnerHoy Canal16
58	32			15			Energy Yesterday in Channel 17		CH17EnergyYTD		Energía Hoy Canal 17		EnerHoy Canal17
59	32			15			Energy Yesterday in Channel 18		CH18EnergyYTD		Energía Hoy Canal 18		EnerHoy Canal18
60	32			15			Energy Yesterday in Channel 19		CH19EnergyYTD		Energía Hoy Canal 19		EnerHoy Canal19
61	32			15			Energy Yesterday in Channel 20		CH20EnergyYTD		Energía Hoy Canal 20		EnerHoy Canal20
62	32			15			Total Energy in Channel 1		CH1TotalEnergy		Energía Total Canal 1		TotEner Canal1
63	32			15			Total Energy in Channel 2		CH2TotalEnergy		Energía Total Canal 2		TotEner Canal2
64	32			15			Total Energy in Channel 3		CH3TotalEnergy		Energía Total Canal 3		TotEner Canal3
65	32			15			Total Energy in Channel 4		CH4TotalEnergy		Energía Total Canal 4		TotEner Canal4
66	32			15			Total Energy in Channel 5		CH5TotalEnergy		Energía Total Canal 5		TotEner Canal5
67	32			15			Total Energy in Channel 6		CH6TotalEnergy		Energía Total Canal 6		TotEner Canal6
68	32			15			Total Energy in Channel 7		CH7TotalEnergy		Energía Total Canal 7		TotEner Canal7
69	32			15			Total Energy in Channel 8		CH8TotalEnergy		Energía Total Canal 8		TotEner Canal8
70	32			15			Total Energy in Channel 9		CH9TotalEnergy		Energía Total Canal 9		TotEner Canal9
71	32			15			Total Energy in Channel 10		CH10TotalEnergy		Energía Total Canal 10		TotEner Canal10
72	32			15			Total Energy in Channel 11		CH11TotalEnergy		Energía Total Canal 11		TotEner Canal11
73	32			15			Total Energy in Channel 12		CH12TotalEnergy		Energía Total Canal 12		TotEner Canal12
74	32			15			Total Energy in Channel 13		CH13TotalEnergy		Energía Total Canal 13		TotEner Canal13
75	32			15			Total Energy in Channel 14		CH14TotalEnergy		Energía Total Canal 14		TotEner Canal14
76	32			15			Total Energy in Channel 15		CH15TotalEnergy		Energía Total Canal 15		TotEner Canal15
77	32			15			Total Energy in Channel 16		CH16TotalEnergy		Energía Total Canal 16		TotEner Canal16
78	32			15			Total Energy in Channel 17		CH17TotalEnergy		Energía Total Canal 17		TotEner Canal17
79	32			15			Total Energy in Channel 18		CH18TotalEnergy		Energía Total Canal 18		TotEner Canal18
80	32			15			Total Energy in Channel 19		CH19TotalEnergy		Energía Total Canal 19		TotEner Canal19
81	32			15			Total Energy in Channel 20		CH20TotalEnergy		Energía Total Canal 20		TotEner Canal20
82	32			15			Normal					Normal			Normal				Normal
83	32			15			Low					Low			Bajo				Bajo
84	32			15			High					High			Alto				Alto
85	32			15			Under Voltage				Under Voltage		Subtensión			Subtensión
86	32			15			Over Voltage				Over Voltage		Sobretensión			Sobretensión
87	32			15			Shunt 1 Current Alarm			Shunt 1 Alarm		Alarma Corriente Shunt 1	Alarma Shunt1
88	32			15			Shunt 2 Current Alarm			Shunt 2 Alarm		Alarma Corriente Shunt 2	Alarma Shunt2
89	32			15			Shunt 3 Current Alarm			Shunt 3 Alarm		Alarma Corriente Shunt 3	Alarma Shunt3
90	32			15			Bus Voltage Alarm			BusVolt Alarm		Status Busschiene		Stat. Busspann.
91	32			15			SMDUH Fault				SMDUH Fault		Status SMDUH			Status SMDUH
92	32			15			Shunt 2 Over Current			Shunt 2 OverCur		Sobrecorriente Shunt 2		SobrCorr Shunt2
93	32			15			Shunt 3 Over Current			Shunt 3 OverCur		Sobrecorriente Shunt 3		SobrCorr Shunt3
94	32			15			Shunt 4 Over Current			Shunt 4 OverCur		Sobrecorriente Shunt 4		SobrCorr Shunt4
95	32			15			Times of Communication Fail		Times Comm Fail		Fallos de Comunicación		Fallos COM
96	32			15			Existent				Existent		Existente			Existente
97	32			15			Not Existent				Not Existent		No Existente			No Existente
98	32			15			Very Low				Very Low		Muy Bajo			Muy Bajo
99	32			15			Very High				Very High		Muy Alto			Muy Alto
100	32			15			Switch					Switch			Interruptor			Interruptor
101	32			15			LVD1 Fail				LVD 1 Fail		Fallo LVD1			Fallo LVD1
102	32			15			LVD2 Fail				LVD 2 Fail		Fallo LVD2			Fallo LVD2
103	32			15			High Temperature Disconnect 1	HTD 1			Desconex Alta Temperatura 1	HTD1
104	32			15			High Temperature Disconnect 2	HTD 2			Desconex Alta Temperatura 2	HTD2
105	32			15			Battery LVD				Battery LVD		LVD de Batería			LVD de Batería
106	32			15			No Battery				No Battery		Sin Batería			Sin Batería
107	32			15			LVD 1					LVD 1			LVD1				LVD1
108	32			15			LVD 2					LVD 2			LVD2				LVD2
109	32			15			Battery Always On			Batt Always On		Batería siempre Conectada	Bat Siempre Con
110	32			15			Barcode					Barcode			Código Barra			Código Barra
111	32			15			DC Over Voltage				DC Over Volt		Sobretensión CC			SobreV CC
112	32			15			DC Under Voltage			DC Under Volt		Subtensión CC			Subtensión CC
113	32			15			Over Current 1				Over Curr 1		Sobrecorriente 1		Sobrecorr 1
114	32			15			Over Current 2				Over Curr 2		Sobrecorriente 2		Sobrecorr 2
115	32			15			Over Current 3				Over Curr 3		Sobrecorriente 3		Sobrecorr 3
116	32			15			Over Current 4				Over Curr 4		Sobrecorriente 4		Sobrecorr 4
117	32			15			Existence State				Existence State		Detección			Detección
118	32			15			Communication Fail			Comm Fail		Fallo Comunicación		Fallo COM
119	32			15			Bus Voltage Status			Bus Volt Status		Estado Tensión Bus		Estado V-Bus
120	32			15			Comm OK					Comm OK			Comunicación OK			Comunicación OK
121	32			15			All Batteries Comm Fail			AllBattCommFail		Fallo COM todas las baterías	FalloComTotBat
122	32			15			Communication Fail			Comm Fail		Fallo Comunicación		Fallo COM
123	32			15			Rated Capacity				Rated Capacity		Capacidad Estimada		Capacidad Estim
124	32			15			Load 5 Current				Load 5 Current		Corriente Carga 5		Carga 5
125	32			15			Shunt 1 Voltage				Shunt 1 Voltage		Tensión Shunt 1			Tens Shunt 1
126	32			15			Shunt 1 Current				Shunt 1 Current			Corriente Shunt 1		Corr Shunt 1
127	32			15			Shunt 2 Voltage				Shunt 2 Voltage		Tensión Shunt 2			Tens Shunt 2
128	32			15			Shunt 2 Current				Shunt 2 Current			Corriente Shunt 2		Corr Shunt 2
129	32			15			Shunt 3 Voltage				Shunt 3 Voltage		Tensión Shunt 3			Tens Shunt 3
130	32			15			Shunt 3 Current				Shunt 3 Current			Corriente Shunt 3		Corr Shunt 3
131	32			15			Shunt 4 Voltage				Shunt 4 Voltage		Tensión Shunt 4			Tens Shunt 4
132	32			15			Shunt 4 Current				Shunt 4 Current			Corriente Shunt 4		Corr Shunt 4
133	32			15			Shunt 5 Voltage				Shunt 5 Voltage		Tensión Shunt 5			Tens Shunt 5
134	32			15			Shunt 5 Current				Shunt 5 Current			Corriente Shunt 5		Corr Shunt 6
135	32			15			Normal					Normal			Normal				Normal
136	32			15			Fail					Fail			Fallo				Fallo
#added by Frank Wu,20140123,8/57, for SMDUH TR129 and Hall Calibrate
137	32			15			Hall Calibrate Point 1			HallCalibrate1		Punto 1 Calibración Hall	Calibra Hall1
138	32			15			Hall Calibrate Point 2			HallCalibrate2		Punto 2 Calibración Hall	Calibra Hall2
139	32			15			Energy Clear				EnergyClear		Borrar Energía Diaria		Borra EnerDía
140	32			15			All Channels				All Channels		Todos				Todos
141	32			15			Channel 1				Channel 1		Canal 1				Canal 1
142	32			15			Channel 2				Channel 2		Canal 2				Canal 2
143	32			15			Channel 3				Channel 3		Canal 3				Canal 3
144	32			15			Channel 4				Channel 4		Canal 4				Canal 4
145	32			15			Channel 5				Channel 5		Canal 5				Canal 5
146	32			15			Channel 6				Channel 6		Canal 6				Canal 6
147	32			15			Channel 7				Channel 7		Canal 7				Canal 7
148	32			15			Channel 8				Channel 8		Canal 8				Canal 8
149	32			15			Channel 9				Channel 9		Canal 9				Canal 9
150	32			15			Channel 10				Channel 10		Canal 10			Canal 10
151	32			15			Channel 11				Channel 11		Canal 11			Canal 11
152	32			15			Channel 12				Channel 12		Canal 12			Canal 12
153	32			15			Channel 13				Channel 13		Canal 13			Canal 13
154	32			15			Channel 14				Channel 14		Canal 14			Canal 14
155	32			15			Channel 15				Channel 15		Canal 15			Canal 15
156	32			15			Channel 16				Channel 16		Canal 16			Canal 16
157	32			15			Channel 17				Channel 17		Canal 17			Canal 17
158	32			15			Channel 18				Channel 18		Canal 18			Canal 18
159	32			15			Channel 19				Channel 19		Canal 19			Canal 19
160	32			15			Channel 20				Channel 20		Canal 20			Canal 20
161	32			15			Hall Calibrate Channel			CalibrateChan		Calibración Hall		Calibrar Hall
162	32			15			Hall Coeff 1				Hall Coeff 1		Coeficiente Hall 1		Coef Hall 1
163	32			15			Hall Coeff 2				Hall Coeff 2		Coeficiente Hall 2		Coef Hall 2
164	32			15			Hall Coeff 3				Hall Coeff 3		Coeficiente Hall 3		Coef Hall 3
165	32			15			Hall Coeff 4				Hall Coeff 4		Coeficiente Hall 4		Coef Hall 4
166	32			15			Hall Coeff 5				Hall Coeff 5		Coeficiente Hall 5		Coef Hall 5
167	32			15			Hall Coeff 6				Hall Coeff 6		Coeficiente Hall 6		Coef Hall 6
168	32			15			Hall Coeff 7				Hall Coeff 7		Coeficiente Hall 7		Coef Hall 7
169	32			15			Hall Coeff 8				Hall Coeff 8		Coeficiente Hall 8		Coef Hall 8
170	32			15			Hall Coeff 9				Hall Coeff 9		Coeficiente Hall 9		Coef Hall 9
171	32			15			Hall Coeff 10				Hall Coeff 10		Coeficiente Hall 10		Coef Hall 10
172	32			15			Hall Coeff 11				Hall Coeff 11		Coeficiente Hall 11		Coef Hall 11
173	32			15			Hall Coeff 12				Hall Coeff 12		Coeficiente Hall 12		Coef Hall 12
174	32			15			Hall Coeff 13				Hall Coeff 13		Coeficiente Hall 13		Coef Hall 13
175	32			15			Hall Coeff 14				Hall Coeff 14		Coeficiente Hall 14		Coef Hall 14
176	32			15			Hall Coeff 15				Hall Coeff 15		Coeficiente Hall 15		Coef Hall 15
177	32			15			Hall Coeff 16				Hall Coeff 16		Coeficiente Hall 16		Coef Hall 16
178	32			15			Hall Coeff 17				Hall Coeff 17		Coeficiente Hall 17		Coef Hall 17
179	32			15			Hall Coeff 18				Hall Coeff 18		Coeficiente Hall 18		Coef Hall 18
180	32			15			Hall Coeff 19				Hall Coeff 19		Coeficiente Hall 19		Coef Hall 19
181	32			15			Hall Coeff 20				Hall Coeff 20		Coeficiente Hall 20		Coef Hall 20
182	32			15			All Days				All Days		Todos los días			Todos
183	32			15			SMDUH 8					SMDUH 8			SMDUH 8				SMDUH 8
#//changed by Frank Wu,8/19,20140408, for the function which clearing total energy of single channel
184	32			15			Reset Energy Channel X		RstEnergyChanX		Borrar Energía Canal		Borr EnerCanal


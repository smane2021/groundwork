﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15			Battery Current				Batt Current		Corriente Batería		Corr Bat			
2	32			15			Battery Rating (Ah)		Batt Rating(Ah)		Valoración batería(Ah)		Valora Bat(Ah)			
3	32			15			Bus Voltage				Bus Voltage		Tensión del bus		Tensión Bus		
5	32			15			Battery Over Current			Batt Over Curr		Batería Con corriente		Bat Con Corr		
6	32			15			Battery Capacity (%)			Batt Cap (%)		batería Capacidad(%)		Bat Cap(%)		
7	32			15			Battery Voltage				Batt Voltage		Tensión de la batería		Tensión Bat		
18	32			15			SoNick Battery				SoNick Batt		SoNick Batería		SoNick Bat		
29	32			15			Yes					Yes			Sí			Sí
30	32			15			No					No			No.			No.
31	32			15			On					On			Diez			Diez
32	32			15			Off					Off			De			De
33	32			15			State					State			Estado			Estado
87	32			15			No					No			No.			No.	
96	32			15			Rated Capacity				Rated Capacity		Capacidad nominal		Cap Nominal	
99	32			15			Battery Temperature(AVE)		Batt Temp(AVE)		Batería Temperatura		Batería Temp
100	32			15			Board Temperature			Board Temp		Temperatura Junta		Temp Junta
101	32			15			Tc Center Temperature			Tc Center Temp		Tc Centro Temperatura		Tc Centro Temp
102	32			15			Tc Left Temperature			Tc Left Temp		Tc Temperatura izquierda	Tc Temp Izq
103	32			15			Tc Right Temperature			Tc Right Temp		Tc Temperatura derecha		Tc Temp Derecha
114	32			15			Battery Communication Fail		Batt Comm Fail		Batería Fallo comunicación		Bat Fallo Com
129	32			15			Low Ambient Temperature			Low Amb Temp		Baja Temperatura Ambiente		Baja Temp Amb
130	32			15			High Ambient Temperature Warning	High Amb Temp W	Ambiente alta Temp Adver		Amb Alta Temp	
131	32			15			High Ambient Temperature		High Amb Temp		Temperatura ambiente alta		Temp Amb Alta	
132	32			15			Low Battery Internal Temperature	Low Int Temp		Baja Temperatura Batería interna		Baja Temp Int
133	32			15			High Batt Internal Temp Warning		Hi Int Temp W		Adver Alta Temp interna Batten		Alta Temp Int	
134	32			15			High Batt Internal Temperature		Hi Bat Int Temp		Alta Temperatura Interna Bat		Alta Temp. Bat	
135	32			15			Bus Voltage Below 40V			Bus V Below 40V		Debajo 40V Tensión del Bus	Deb.40V V.Bus
136	32			15			Bus Voltage Below 39V			Bus V Below 39V		Debajo 39V Tensión del Bus	Deb.39V V.Bus
137	32			15			Bus Voltage Above 60V			Bus V Above 60V		Ante 60V Tensión del Bus	Ante 60V V.Bus
138	32			15			Bus Voltage Above 65V			Bus V Above 65V		Ante 65V Tensión del Bus	Ante 65V V.Bus
139	32			15			High Discharge Current Warning		Hi Disch Curr W		Desc. de Alta Corr.de Peligro		Desc.Alta Corr.
140	32			15			High Discharge Current			High Disch Curr		Corriente de Descarga de Alta		Corr.Desc.Alta	
141	32			15			Main Switch Error			Main Switch Err		Interruptor Principal de Error		Inter.Prin.Err.
142	32			15			Fuse Blown				Fuse Blown		Fusible Fundido		Fus.Fundido
143	32			15			Heaters Failure				Heaters Failure		Calentadores Falla		Calent.Falla
144	32			15			Thermocouple Failure			Thermocple Fail		Termopar Falla		Termopar Falla
145	32			15			Voltage Measurement Circuit Fail	V M Cir Fail		Med.de V del cir.a prueba		V M Cir.Prueba
146	32			15			Current Measurement Circuit Fail	C M Cir Fail		Med.de Corr.del cir.a prueba		C M Cir.Prueba
147	32			15			BMS Hardware Failure			BMS Hdw Failure		BMS Falla de hardware			BMS Falla HW
148	32			15			Hardware Protection Sys Active		HdW Protect Sys		Protección de HW Sys Activo	HdW Protec.Sys	
149	32			15			High Heatsink Temperature		Hi Heatsink Tmp		Alta Temperatura del Radiador		Alta Temp.Rad.
150	32			15			Battery Voltage Below 39V		Bat V Below 39V		Debajo 39V V del Bus	Deb.39V V.Bus
151	32			15			Battery Voltage Below 38V		Bat V Below 38V		Debajo 38V V del Bus	Deb.38V V.Bus
152	32			15			Battery Voltage Above 53.5V		Bat V Abv 53.5V		Ante 53.5V V del Bus	Ante53.5V V.Bus
153	32			15			Battery Voltage Above 53.6V		Bat V Abv 53.6V		Ante 53.6V V del Bus	Ante53.6V V.Bus
154	32			15			High Charge Current Warning		Hi Chrge Curr W		Adver.alta Corr.de carga	Alta Corr.Carga
155	32			15			High Charge Current			Hi Charge Curr		Corriente de carga alta		CorrCarga Alta	
156	32			15			High Discharge Current Warning		Hi Disch Curr W		Desc. de alta corri.de peligro		Desc.Alta Corr.
157	32			15			High Discharge Current			Hi Disch Curr		Desc. de alta corriente		Desc.Alta Corr.
158	32			15			Voltage Unbalance Warning		V unbalance W		Desequilibrio de V Adver.	Desequilibrio V
159	32			15			Voltages Unbalance			Volt Unbalance		Desequilibrio de Voltaje		Desequilibrio V
160	32			15			Dc Bus Pwr Too Low for Charging		DC Low for Chrg		DC Pwr Bus Baja Para Cargar	DCBajaParaCarg.
161	32			15			Charge Regulation Failure		Charge Reg Fail		Reglamento falla en la carga		Reg.FallaCarga
162	32			15			Capacity Below 12.5%			Cap Below 12.5%		Debajo Capacidad12.5%		Deb.Cap.12.5%
163	32			15			Thermocouples Mismatch			Tcples Mismatch		No coinciden los termopares		NoCoincidenTerm
164	32			15			Heater Fuse Blown			Heater FA		Calentador de fusible fundido		Cal.FusiFundido

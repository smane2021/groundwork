﻿#
#  Locale language support:spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			Voltage L1-N		Volt L1-N		Tensión R-N		V R-N
2		32			15			Voltage L2-N		Volt L2-N		Tensión S-N		V S-N
3		32			15			Voltage L3-N		Volt L3-N		Tensión T-N		V T-N
4		32			15			Voltage L1-L2		Volt L1-L2		Tensión R-S		V R-S
5		32			15			Voltage L2-L3		Volt L2-L3		Tensión S-T		V S-T
6		32			15			Voltage L3-L1		Volt L3-L1		Tensión T-R		V T-R
7		32			15			Current L1		Curr L1			Corriente R	Corr R

8		32			15			Current L2		Curr L2			Corriente S	Corr S

9		32			15			Current L3		Curr L3			Corriente T	Corr T

10		32			15			Watt L1			Watt L1			Potencia R	Pot R

11		32			15			Watt L2			Watt L2			Potencia S	Pot S

12		32			15			Watt L3			Watt L3			Potencia T	Pot T


13		32			15			VA L1			VA L1			VA R			VA R
14		32			15			VA L2			VA L2			VA S			VA S
15		32			15			VA L3			VA L3			VA T			VA T

16		32			15			VAR L1			VAR L1			VAR R		VAR R
17		32			15			VAR L2			VAR L2			VAR S		VAR S
18		32			15			VAR L3			VAR L3			VAR T		VAR T

19		32			15			AC Frequency		AC Frequency		Frecuencia CA		Frecuencia CA



20		32			15			Communication State	Comm State		Estado Comunicación	Estado COM

21		32			15			Existence State		Existence State		Detección		Detección

22		32			15			AC Meter		AC Meter		Contador CA	Contador CA


23		32			15			On					On			Conectado			Conectado
24		32			15			Off					Off			Apagado			Apagado

25		32			15			Existent		Existent		Existent		Existent
26		32			15			Not Existent		Not Existent		Not Existent		Not Existent
27		32			15			Communication Fail			Comm Fail		Fallo de Comunicación	Fallo COM


28		32			15			V L-N ACC		V L-N ACC		V L-N ACC		V L-N ACC
29		32			15			V L-L ACC		V L-L ACC		V L-L ACC		V L-L ACC
30		32			15			Watt ACC		Watt ACC		Watt ACC		Watt ACC
31		32			15			VA ACC			VA ACC			VAR ACC			VAR ACC
32		32			15			VAR ACC			VAR ACC			VAR ACC			VAR ACC
33		32			15			DMD Watt ACC		DMD Watt ACC		DMD Watt ACC		DMD Watt ACC
34		32			15			DMD VA ACC		DMD VA ACC		DMD VA ACC		DMD VA ACC
35		32			15			PF L1			PF L1			PF R			PF R
36		32			15			PF L2			PF L2			PFS			PF S
37		32			15			PF L3			PF L3			PF T			PF T
38		32			15			PF ACC			PF ACC			PF ACC			PF ACC
39		32			15			Phase Sequence		Phase Sequence		Secuencia Fase		Secuencia Fase
40		32			15			L1-L2-L3		L1-L2-L3		R-S-T	R-S-T
41		32			15			L1-L3-L2		L1-L3-L2		R-T-S		R-T-S


42	32			15		Nominal Line Voltage			NominalLineVolt		Tensión Nominal de Línea		Vnom Línea
43	32			15		Nominal Phase Voltage			Nominal PH-Volt		Tensión Nominal de Fase		Vnom Fase
44	32			15		Nominal Frequency			Nominal Freq		Frecuencia Nominal		Frec Nom
45	32			15		Mains Failure Alarm Limit 1		Mains Fail Alm1		Límite Alarma Fallo de Red 1		Fallo Red1
46	32			15		Mains Failure Alarm Limit 2		Mains Fail Alm2		Límite Alarma Fallo de Red 2		Fallo Red2
47	32			15		Frequency Alarm Limit			Freq Alarm Lmt		Límite Alarma de Frecuencia		Lim Alar Frec
48	32			15			Current Alarm Limit			Curr Alm Limit		Límite Alarma de Corriente		Alarm Corr

51	32			15		Line AB Over Voltage 1			L-AB Over Volt1		Sobretensión1 R-S		SobreV1 R-S
52	32			15			Line AB Over Voltage 2			L-AB Over Volt2		Sobretensión2 R-S		SobreV2 R-S
53	32			15			Line AB Under Voltage 1			L-AB UnderVolt1		Subtensión1 R-S	Subtens1 R-S

54	32			15			Line AB Under Voltage 2			L-AB UnderVolt2		Subtensión2 R-S	Subtens2 R-S

55	32			15			Line BC Over Voltage 1			L-BC Over Volt1		Sobretensión1 S-T		SobreV1 S-T
56	32			15			Line BC Over Voltage 2			L-BC Over Volt2		Sobretensión2 S-T		SobreV2 S-T
57	32			15			Line BC Under Voltage 1			L-BC UnderVolt1		Subtensión1 S-T	Subtens1 S-T
58	32			15			Line BC Under Voltage 2			L-BC UnderVolt2		Subtensión2 S-T	Subtens2 S-T
59	32			15			Line CA Over Voltage 1			L-CA Over Volt1		Sobretensión1 T-R		SobreV1 T-R
60	32			15			Line CA Over Voltage 2			L-CA Over Volt2		Sobretensión2 T-R		SobreV2 T-R
61	32			15			Line CA Under Voltage 1			L-CA UnderVolt1		Subtensión1 T-R	Subtens1 T-R
62	32			15			Line CA Under Voltage 2			L-CA UnderVolt2		Subtensión2 T-R	Subtens2 T-R
63	32			15			Phase A Over Voltage 1			PH-A Over Volt1		Sobretensión1 Fase R	SobreV1 Fase R


64	32			15			Phase A Over Voltage 2			PH-A Over Volt2		Sobretensión2 Fase R	SobreV2 Fase R
65	32			15			Phase A Under Voltage 1			PH-A UnderVolt1		Subtensión1 Fase R		Subten1 Fase R

66	32			15			Phase A Under Voltage 2			PH-A UnderVolt2		Subtensión2 Fase R		Subten2 Fase R
67	32			15			Phase B Over Voltage 1			PH-B Over Volt1		Sobretensión1 Fase S	SobreV1 Fase S
68	32			15			Phase B Over Voltage 2			PH-B Over Volt2		Sobretensión2 Fase S	SobreV2 Fase S
69	32			15			Phase B Under Voltage 1			PH-B UnderVolt1		Subtensión1 Fase S		Subten1 Fase S
70	32			15			Phase B Under Voltage 2			PH-B UnderVolt2		Subtensión2 Fase S		Subten2 Fase S
71	32			15			Phase C Over Voltage 1			PH-C Over Volt1		Sobretensión1 Fase T	SobreV1 Fase T
72	32			15			Phase C Over Voltage 2			PH-C Over Volt2		Sobretensión2 Fase T	SobreV2 Fase T
73	32			15			Phase C Under Voltage 1			PH-C UnderVolt1		Subtensión1 Fase T	Subten1 Fase T
74	32			15			Phase C Under Voltage 2			PH-C UnderVolt2		Subtensión2 Fase T	Subten2 Fase T
75	32			15			Mains Failure				Mains Failure		Fallo de Red		Fallo de Red
76	32			15			Severe Mains Failure			SevereMainsFail		Fallo de Red Severo	Fallo Red Sev
77	32			15			High Frequency				High Frequency		Alta Frecuencia		Alta Frecuencia
78	32			15			Low Frequency				Low Frequency		Baja Frecuencia		Baja Frecuencia
79	32			15			Phase A High Current			PH-A High Curr		Alta Corriente Fase R	Alta Corr R
80	32			15			Phase B High Current			PH-B High Curr		Alta Corriente Fase S	Alta Corr S
81	32			15			Phase C High Current			PH-C High Curr		Alta Corriente Fase T	Alta Corr T

82	32			15			DMD W MAX				DMD W MAX		DMD W MAX		DMD W MAX
83	32			15			DMD VA MAX				DMD VA MAX		DMD VA MAX		DMD VA MAX
84	32			15			DMD A MAX				DMD A MAX		DMD A MAX		DMD A MAX
85	32			15			KWH(+) TOT				KWH(+) TOT		KWH(+) TOT		KWH(+) TOT
86	32			15			KVARH(+) TOT				KVARH(+) TOT		KVARH(+) TOT		KVARH(+) TOT
87	32			15			KWH(+) PAR				KWH(+) PAR		KWH(+) PAR		KWH(+) PAR
88	32			15			KVARH(+) PAR				KVARH(+) PAR		KVARH(+) PAR		KVARH(+) PAR
89	32			15			KWH(+) L1				KWH(+) L1		KWH(+) L1		KWH(+) L1
90	32			15			KWH(+) L2				KWH(+) L2		KWH(+) L2		KWH(+) L2
91	32			15			KWH(+) L3				KWH(+) L3		KWH(+) L3		KWH(+) L3
92	32			15			KWH(+) T1				KWH(+) T1		KWH(+) T1		KWH(+) T1
93	32			15			KWH(+) T2				KWH(+) T2		KWH(+) T2		KWH(+) T2
94	32			15			KWH(+) T3				KWH(+) T3		KWH(+) T3		KWH(+) T3
95	32			15			KWH(+) T4				KWH(+) T4		KWH(+) T4		KWH(+) T4
96	32			15			KVARH(+) T1				KVARH(+) T1		KVARH(+) T1		KVARH(+) T1
97	32			15			KVARH(+) T2				KVARH(+) T2		KVARH(+) T2		KVARH(+) T2
98	32			15			KVARH(+) T3				KVARH(+) T3		KVARH(+) T3		KVARH(+) T3
99	32			15			KVARH(+) T4				KVARH(+) T4		KVARH(+) T4		KVARH(+) T4

100	32			15			KWH(-) TOT				KWH(-) TOT		KWH(-) TOT		KWH(-) TOT
101	32			15			KVARH(-) TOT				KVARH(-) TOT		KVARH(-) TOT		KVARH(-) TOT
102	32			15			HOUR					HOUR			HORA			HORA
103	32			15			COUNTER 1				COUNTER 1		CONTADOR 1		CONTADOR 1
104	32			15			COUNTER 2				COUNTER 2		CONTADOR 2		CONTADOR 2
105	32			15			COUNTER 3				COUNTER 3		CONTADOR 3		CONTADOR 3

﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			IB2-1			IB2-1			IB2-1			IB2-1
8		32			15			DI1			DI1			DI 1			DI 1
9		32			15			DI2			DI2			DI 2			DI 2
10		32			15			DI3			DI3			DI 3			DI 3
11		32			15			DI4			DI4			DI 4			DI 4
12		32			15			DI5			DI5			DI 5			DI 5
13		32			15			DI6			DI6			DI 6			DI 6
14		32			15			DI7			DI7			DI 7			DI 7
15		32			15			DI8			DI8			DI 8			DI 8
16		32			15			Not Active			Not Active		No Activo		No Activo
17		32			15			Active				Active			Activo			Activo
18		32			15			DO1-Relay Output		DO1-RelayOutput		Salida de relé DO1		SalidaReléDO1
19		32			15			DO2-Relay Output		DO2-RelayOutput		Salida de relé DO2		SalidaReléDO2
20		32			15			DO3-Relay Output		DO3-RelayOutput		Salida de relé DO3		SalidaReléDO3
21		32			15			DO4-Relay Output		DO4-RelayOutput		Salida de relé DO4		SalidaReléDO4
22		32			15			DO5-Relay Output		DO5-RelayOutput		Salida de relé DO5		SalidaReléDO5
23		32			15			DO6-Relay Output		DO6-RelayOutput		Salida de relé DO6		SalidaReléDO6
24		32			15			DO7-Relay Output		DO7-RelayOutput		Salida de relé DO7		SalidaReléDO7
25		32			15			DO8-Relay Output		DO8-RelayOutput		Salida de relé DO8		SalidaReléDO8
26		32			15			DI1 Alarm State	DI1 Alarm State		Estado Alarma DI1	Estado DI1
27		32			15			DI2 Alarm State	DI2 Alarm State		Estado Alarma DI2	Estado DI2
28		32			15			DI3 Alarm State	DI3 Alarm State		Estado Alarma DI3	Estado DI3
29		32			15			DI4 Alarm State	DI4 Alarm State		Estado Alarma DI4	Estado DI4
30		32			15			DI5 Alarm State	DI5 Alarm State		Estado Alarma DI5	Estado DI5
31		32			15			DI6 Alarm State	DI6 Alarm State		Estado Alarma DI6	Estado DI6
32		32			15			DI7 Alarm State	DI7 Alarm State		Estado Alarma DI7	Estado DI7
33		32			15			DI8 Alarm State	DI8 Alarm State		Estado Alarma DI8	Estado DI8
34		32			15			State			State			Estado			Estado
35		32			15			Communication Fail		Comm Fail		Fallo IB		Fallo IB
36		32			15			Barcode			Barcode			Barcode			Barcode
37		32			15			On			On			Sí		Sí
38		32			15			Off			Off			No			No
39		32			15			High			High			Alto			Alto
40		32			15			Low			Low			Bajo			Bajo
41		32			15			DI1 Alarm		DI1 Alarm		Alarma DI1		Alarma DI1
42		32			15			DI2 Alarm		DI2 Alarm		Alarma DI2		Alarma DI2
43		32			15			DI3 Alarm		DI3 Alarm		Alarma DI3		Alarma DI3
44		32			15			DI4 Alarm		DI4 Alarm		Alarma DI4		Alarma DI4
45		32			15			DI5 Alarm		DI5 Alarm		Alarma DI5		Alarma DI5
46		32			15			DI6 Alarm		DI6 Alarm		Alarma DI6		Alarma DI6
47		32			15			DI7 Alarm		DI7 Alarm		Alarma DI7		Alarma DI7
48		32			15			DI8 Alarm		DI8 Alarm		Alarma DI8		Alarma DI8
78		32			15			IB2-1 DO1 Test			IB2-1 DO1 Test		Prueba de DO1		Prueba de DO1
79		32			15			IB2-1 DO2 Test			IB2-1 DO2 Test		Prueba de DO2		Prueba de DO2
80		32			15			IB2-1 DO3 Test			IB2-1 DO3 Test		Prueba de DO3		Prueba de DO3
81		32			15			IB2-1 DO4 Test			IB2-1 DO4 Test		Prueba de DO4		Prueba de DO4
82		32			15			IB2-1 DO5 Test			IB2-1 DO5 Test		Prueba de DO5		Prueba de DO5
83		32			15			IB2-1 DO6 Test			IB2-1 DO6 Test		Prueba de DO6		Prueba de DO6
84		32			15			IB2-1 DO7 Test			IB2-1 DO7 Test		Prueba de DO7		Prueba de DO7
85		32			15			IB2-1 DO8 Test			IB2-1 DO8 Test		Prueba de DO8		Prueba de DO8
86		32			15			Temp1				Temp1			Temp1			Temp1
87		32			15			Temp2				Temp2			Temp2			Temp2
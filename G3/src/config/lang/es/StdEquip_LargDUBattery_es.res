﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			Battery Current				Battery Current		Corriente Batería		Corriente Bat
2		32			15			Battery Rating(Ah)			Batt Rating(Ah)		Capacidad (Ah)			Capacidad (Ah)
3		32			15			Battery Current Limit Exceeded		Ov Batt Cur Lmt		Límite corriente excedido	Lim corr pasado
4		32			15			Battery					Battery			Batería				Batería
5		32			15			Battery Over Current			Batt Over Curr		Sobrecorriente			Sobrecorriente
6		32			15			Battery Capacity (%)			Batt Cap(%)		Capacidad batería (%)		Cap Bat (%)
7		32			15			Battery Voltage				Battery Voltage			Tensión batería			Tensión batería
8		32			15			Battery Low Capacity			Batt Low Cap		Baja capacidad			Baja capacidad
9		32			15			Battery Fuse Failure			Batt Fuse Fail		Fallo fusible batería		Fallo Fusible
10		32			15			DC Distribution Sequence Number		DC Dist Seq Num	Núm Sec Distribución		Num Sec Distrib
11		32			15			Battery Over Voltage			Batt Over Volt		Sobretensión Batería		Sobretensión
12		32			15			Battery Under Voltage			Batt Under Volt			Subtensión Batería		Subtensión
13		32			15			Battery Over Current			Batt Over Curr		Sobrecorriente a Batería	Sobrecorriente
14		32			15			Battery Fuse Failure			Batt Fuse Fail		Fallo fusible batería		Fallo Fusible
15		32			15			Battery Over Voltage			Batt Over Volt		Sobretensión Batería		Sobretensión
16		32			15			Battery Under Voltage			Batt Under Volt			Subtensión Batería		Subtensión
17		32			15			Battery Over Current			Batt Over Curr		Sobrecorriente a Batería	Sobrecorriente
18		32			15			LargeDU Battery				LargeDU Batt	Batería GranDU			Batería GranDU
19		32			15			Battery Shunt Coefficient		Bat Shunt Coeff		Coeficiente Sensor Batería	Coef Sensor Bat
20		32			15			Over Voltage Limit			Over Volt Limit		Nivel de Sobretensión		Sobretensión
21		32			15			Low Voltage Limit			Low Volt Limit		Nivel de Subtensión		Subtensión
22		32			15			Battery Communication Fail		Batt Comm Fail	Todas las baterías en Fallo Com		FalloComTotBat
23		32			15			Communication OK			Comm OK			Comunicación OK			Comunicación OK
24		32			15			Communication Fail			Comm Fail	Fallo de Comunicación		Fallo COM
25		32			15			Communication Fail			Comm Fail	Fallo de Comunicación		Fallo COM
26		32			15			Existence State				Existence State		Detección			Detección
27		32			15			Existent				Existent		Existente			Existente
28		32			15			Not Existent				Not Existent		No existente			No existente
29		32			15			Rated Capacity				Rated Capacity		Capacidad estimada		Capacidad
30		32			15			Battery Management		Batt Management		Gestión de Baterías		Gestión Bat
31		32			15			Enabled					Enabled				Habilitado			Habilitado
32		32			15			Disabled				Disabled		Deshabilitado			Deshabilitado
97		32			15			Battery Temperature			Battery Temp		Temperatura de Batería		Temp Batería
98		32			15			Battery Temperature Sensor		Bat Temp Sensor		Sensor Temperatura Batería	Sensor Temp
99		32			15			None					None			Ninguno				Ninguno
100		32			15			Temperature 1				Temperature 1		Temperatura 1			Temp 1
101		32			15			Temperature 2				Temperature 2		Temperatura 2			Temp 2
102		32			15			Temperature 3				Temperature 3		Temperatura 3			Temp 3
103		32			15			Temperature 4				Temperature 4		Temperatura 4			Temp 4
104		32			15			Temperature 5				Temperature 5		Temperatura 5			Temp 5
105		32			15			Temperature 6				Temperature 6		Temperatura 6			Temp 6
106		32			15			Temperature 7				Temperature 7		Temperatura 7			Temp 7
107		32			15			Temperature 8				Temperature 8		Temperatura 8			Temp 8
108		32			15			Temperature 9				Temperature 9		Temperatura 9			Temp 9
109		32			15			Temperature 10				Temperature 10			Temperatura 10			Temp 10

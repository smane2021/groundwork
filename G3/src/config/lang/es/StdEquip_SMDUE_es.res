﻿#
#  Locale language support:Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Bus Bar Voltage			Bus Bar Voltage		Tensión barra distri		Tensión barra D
2	32			15			Shunt 1					Shunt 1			Derivación1		Derivac1	
3	32			15			Shunt 2					Shunt 2			Derivación2		Derivac2	
4	32			15			Shunt 3					Shunt 3			Derivación3		Derivac3
5	32			15			Shunt 4					Shunt 4			Derivación4		Derivac4
6	32			15			Shunt 5					Shunt 5			Derivación5		Derivac5
7	32			15			Shunt 6					Shunt 6			Derivación6		Derivac6
8	32			15			Shunt 7					Shunt 7			Derivación7		Derivac7	
9	32			15			Shunt 8					Shunt 8			Derivación8		Derivac8
10	32			15			Shunt 9					Shunt 9			Derivación9		Derivac9
11	32			15			Shunt 10				Shunt 10		Derivación10		Derivac10

12	32			15			Tran1(0-20mA)			Tran1(0-20mA)		Tran1(0-20mA)			Tran1(0-20mA)	
13	32			15			Tran2(0-20mA)			Tran2(0-20mA)		Tran2(0-20mA)			Tran2(0-20mA)	
14	32			15			Tran3(0-20mA)			Tran3(0-20mA)		Tran3(0-20mA)			Tran3(0-20mA)	
15	32			15			Tran4(0-20mA)			Tran4(0-20mA)		Tran4(0-20mA)			Tran4(0-20mA)	
16	32			15			Tran5(0-20mA)			Tran5(0-20mA)		Tran5(0-20mA)			Tran5(0-20mA)	
17	32			15			Tran6(0-20mA)			Tran6(0-20mA)		Tran6(0-20mA)			Tran6(0-20mA)	
18	32			15			Tran7(0-20mA)			Tran7(0-20mA)		Tran7(0-20mA)			Tran7(0-20mA)	
19	32			15			Tran8(0-20mA)			Tran8(0-20mA)		Tran8(0-20mA)			Tran8(0-20mA)	
20	32			15			Tran9(0-20mA)			Tran9(0-20mA)		Tran9(0-20mA)			Tran9(0-20mA)	
21	32			15			Tran10(0-20mA)			Tran10(0-20mA)		Tran10(0-20mA)			Tran10(0-20mA)	
22	32			15			Tran1(0-10V)				Tran1(0-10V)	Tran1(0-10V)				Tran1(0-10V)
23	32			15			Tran2(0-10V)				Tran2(0-10V)	Tran2(0-10V)				Tran2(0-10V)
24	32			15			Tran3(0-10V)				Tran3(0-10V)	Tran3(0-10V)				Tran3(0-10V)
25	32			15			Tran4(0-10V)				Tran4(0-10V)	Tran4(0-10V)				Tran4(0-10V)
26	32			15			Tran5(0-10V)				Tran5(0-10V)	Tran5(0-10V)				Tran5(0-10V)
27	32			15			Tran6(0-10V)				Tran6(0-10V)	Tran6(0-10V)				Tran6(0-10V)
28	32			15			Tran7(0-10V)				Tran7(0-10V)	Tran7(0-10V)				Tran7(0-10V)
29	32			15			Tran8(0-10V)				Tran8(0-10V)	Tran8(0-10V)				Tran8(0-10V)
30	32			15			Tran9(0-10V)				Tran9(0-10V)	Tran9(0-10V)				Tran9(0-10V)
31	32			15			Tran10(0-10V)				Tran10(0-10V)	Tran10(0-10V)				Tran10(0-10V)

32	32			15			Temperature 1				Temperature 1		Temperatura 1			Temperatura 1
33	32			15			Temperature 2				Temperature 2		Temperatura 2			Temperatura 2
34	32			15			Temperature 3				Temperature 3		Temperatura 3			Temperatura 3
35	32			15			Temperature 4				Temperature 4		Temperatura 4			Temperatura 4
36	32			15			Temperature 5				Temperature 5		Temperatura 5			Temperatura 5
37	32			15			Temperature 6				Temperature 6		Temperatura 6			Temperatura 6
38	32			15			Temperature 7				Temperature 7		Temperatura 7			Temperatura 7
39	32			15			Temperature 8				Temperature 8		Temperatura 8			Temperatura 8
40	32			15			Temperature 9				Temperature 9		Temperatura 9			Temperatura 9
41	32			15			Temperature 10				Temperature 10		Temperatura 10			Temperatura 10

42	32			15			Voltage1			Voltage1	Voltaje1		Voltaje1
43	32			15			Voltage2			Voltage2	Voltaje2		Voltaje2
44	32			15			Voltage3			Voltage3	Voltaje3		Voltaje3
45	32			15			Voltage4			Voltage4	Voltaje4		Voltaje4
46	32			15			Voltage5			Voltage5	Voltaje5		Voltaje5
47	32			15			Voltage6			Voltage6	Voltaje6		Voltaje6
48	32			15			Voltage7			Voltage7	Voltaje7		Voltaje7
49	32			15			Voltage8			Voltage8	Voltaje8		Voltaje8
50	32			15			Voltage9			Voltage9	Voltaje9		Voltaje9
51	32			15			Voltage10			Voltage10		Voltaje10		Voltaje10

52	32			15			Bus Bar Volt				Bus Bar Volt		Barra bus Volt			Barra bus Volt
53	32			15			Version No.				Version No.		Versión No			Versión No
54	32			15			Serial No. High				Serial No. High		Número Serie Alto		Núm Serie Alto
55	32			15			Serial No. Low				Serial No. Low		Número Serie bajo		Núm Serie bajo
56	32			15			Bar Code 1				Bar Code 1		Código Barras1			Código Barras1
57	32			15			Bar Code 2				Bar Code 2		Código Barras2			Código Barras2
58	32			15			Bar Code 3				Bar Code 3		Código Barras3			Código Barras3
59	32			15			Bar Code 4				Bar Code 4		Código Barras4			Código Barras4
60	32			15			Comm OK					Comm OK			Comm OK					Comm OK	
61	32			15			Communication Fail			Comm Fail		Fallo Com		Fallo Com
62	32			15			Existent				Existent		Existente			Existente
63	32			15			Not Existent				Not Existent		No Existe			No Existe
64	32			15			Shunt 1 Voltage					Shunt1 Voltage			Shunt1 Voltaje		Shunt1 Volt
65	32			15			Shunt 1 Current					Shunt1 Current			Shunt1 Corriente	Shunt1 Corr
66	32			15			Shunt 2 Voltage					Shunt2 Voltage			Shunt2 Voltaje		Shunt2 Volt
67	32			15			Shunt 2 Current					Shunt2 Current			Shunt2 Corriente	Shunt2 Corr
68	32			15			Shunt 3 Voltage					Shunt3 Voltage			Shunt3 Voltaje		Shunt3 Volt
69	32			15			Shunt 3 Current					Shunt3 Current			Shunt3 Corriente	Shunt3 Corr
70	32			15			Shunt 4 Voltage					Shunt4 Voltage			Shunt4 Voltaje		Shunt4 Volt
71	32			15			Shunt 4 Current					Shunt4 Current			Shunt4 Corriente	Shunt4 Corr
72	32			15			Shunt 5 Voltage					Shunt5 Voltage			Shunt5 Voltaje		Shunt5 Volt
73	32			15			Shunt 5 Current					Shunt5 Current			Shunt5 Corriente	Shunt5 Corr
74	32			15			Shunt 6 Voltage					Shunt6 Voltage			Shunt6 Voltaje		Shunt6 Volt
75	32			15			Shunt 6 Current					Shunt6 Current			Shunt6 Corriente	Shunt6 Corr
76	32			15			Shunt 7 Voltage					Shunt7 Voltage			Shunt7 Voltaje		Shunt7 Volt
77	32			15			Shunt 7 Current					Shunt7 Current			Shunt7 Corriente	Shunt7 Corr
78	32			15			Shunt 8 Voltage					Shunt8 Voltage			Shunt8 Voltaje		Shunt8 Volt
79	32			15			Shunt 8 Current					Shunt8 Current			Shunt8 Corriente	Shunt8 Corr
80	32			15			Shunt 9 Voltage					Shunt9 Voltage			Shunt9 Voltaje		Shunt9 Volt
81	32			15			Shunt 9 Current					Shunt9 Current			Shunt9 Corriente	Shunt9 Corr
82	32			15			Shunt 10 Voltage				Shunt10 Voltage			Shunt10 Voltaje		Shunt10 Volt
83	32			15			Shunt 10 Current				Shunt10 Current			Shunt10 Corriente	Shunt10 Corr
84	32			15			Shunt 1						Shunt 1				Shunt 1						Shunt 1	
85	32			15			Shunt 2						Shunt 2				Shunt 2						Shunt 2	
86	32			15			Shunt 3						Shunt 3				Shunt 3						Shunt 3	
87	32			15			Shunt 4						Shunt 4				Shunt 4						Shunt 4	
88	32			15			Shunt 5						Shunt 5				Shunt 5						Shunt 5	
89	32			15			Shunt 6						Shunt 6				Shunt 6						Shunt 6	
90	32			15			Shunt 7						Shunt 7				Shunt 7						Shunt 7	
91	32			15			Shunt 8						Shunt 8				Shunt 8						Shunt 8	
92	32			15			Shunt 9						Shunt 9				Shunt 9						Shunt 9	
93	32			15			Shunt 10					Shunt 10			Shunt 10					Shunt 10
94	32			15			Disabled					Disabled			Discapacitado			Discapacitado																														
95	32			15			Enabled						Enabled				Habilitado			Habilitado																														
96	32			15			Communication Fail				Comm Fail		Falla Comunicación		Falla Com
97	32			15			Existence State					Existence State		Estado Existencia		Estado Exist
98	32			15			SMDUE1						SMDUE1				SMDUE1			SMDUE1
99	32			15			SMDUE2						SMDUE2				SMDUE2			SMDUE2
100	32			15			SMDUE3						SMDUE3				SMDUE3			SMDUE3
101	32			15			SMDUE4						SMDUE4				SMDUE4			SMDUE4
102	32			15			SMDUE5						SMDUE5				SMDUE5			SMDUE5
103	32			15			SMDUE6						SMDUE6				SMDUE6			SMDUE6
104	32			15			SMDUE7						SMDUE7				SMDUE7			SMDUE7
105	32			15			SMDUE8						SMDUE8				SMDUE8			SMDUE8
106	32			15			All SMDUE Comm Fail				AllSMCommFail			Todo SMDUE Fallo Com		Todo Fallo Com
107	32			15			ESNA Fuse Alarm Mode				ESNA Fuse Alarm			ESNA Modo Alarma Fusible	ESNA Alm Fusi
108	32			15			Normal Mode							Normal Mode				Modo normal					Modo normal
109	32			15		Fuse1 Alarm Mode		Fuse1 Mode		Modo Alm Fuse1			Modo Fuse1	
110	32			15		Fuse2 Alarm Mode		Fuse2 Mode		Modo Alm Fuse2			Modo Fuse2	
111	32			15		Fuse3 Alarm Mode		Fuse3 Mode		Modo Alm Fuse3			Modo Fuse3	
112	32			15		Fuse4 Alarm Mode		Fuse4 Mode		Modo Alm Fuse4			Modo Fuse4	
113	32			15		Fuse5 Alarm Mode		Fuse5 Mode		Modo Alm Fuse5			Modo Fuse5
114	32			15		Fuse6 Alarm Mode		Fuse6 Mode		Modo Alm Fuse6			Modo Fuse6
115	32			15		Fuse7 Alarm Mode		Fuse7 Mode		Modo Alm Fuse7			Modo Fuse7	
116	32			15		Fuse8 Alarm Mode		Fuse8 Mode		Modo Alm Fuse8			Modo Fuse8	
117	32			15		Fuse9 Alarm Mode		Fuse9 Mode		Modo Alm Fuse9			Modo Fuse9	
118	32			15		Fuse10 Alarm Mode		Fuse10 Mode		Modo Alm Fuse10			Modo Fuse10	

120	32			15			Current1 Break Value				Curr1 Brk Val		Valor corte actual1		ValCortActual1																														
121	32			15			Current2 Break Value				Curr2 Brk Val		Valor corte actual2		ValCortActual2																														
122	32			15			Current3 Break Value				Curr3 Brk Val		Valor corte actual3		ValCortActual3																														
123	32			15			Current4 Break Value				Curr4 Brk Val		Valor corte actual4		ValCortActual4																														
124	32			15			Current5 Break Value				Curr5 Brk Val		Valor corte actual5		ValCortActual5																														
125	32			15			Current6 Break Value				Curr6 Brk Val		Valor corte actual6		ValCortActual6																														
126	32			15			Current7 Break Value				Curr7 Brk Val		Valor corte actual7		ValCortActual7																														
127	32			15			Current8 Break Value				Curr8 Brk Val		Valor corte actual8		ValCortActual8																														
128	32			15			Current9 Break Value				Curr9 Brk Val		Valor corte actual9		ValCortActual9																														
129	32			15			Current10 Break Value				Curr10 Brk Val		Valor corte actual10		ValCortActual10																														

130	32			15			Current1 High 1 Current Limit		Curr1 Hi 1 Lmt		Corr1 Alta 1 Límite Corr		Corr1 Alta1Lmt																														
131	32			15			Current1 High 2 Current Limit		Curr1 Hi 2 Lmt		Corr1 Alta 2 Límite Corr		Corr1 Alta2Lmt																																
132	32			15			Current2 High 1 Current Limit		Curr2 Hi 1 Lmt		Corr2 Alta 1 Límite Corr		Corr2 Alta1Lmt																													
133	32			15			Current2 High 2 Current Limit		Curr2 Hi 2 Lmt		Corr2 Alta 2 Límite Corr		Corr2 Alta2Lmt																															
134	32			15			Current3 High 1 Current Limit		Curr3 Hi 1 Lmt		Corr3 Alta 1 Límite Corr		Corr3 Alta1Lmt																													
135	32			15			Current3 High 2 Current Limit		Curr3 Hi 2 Lmt		Corr3 Alta 2 Límite Corr		Corr3 Alta2Lmt																															
136	32			15			Current4 High 1 Current Limit		Curr4 Hi 1 Lmt		Corr4 Alta 1 Límite Corr		Corr4 Alta1Lmt																													
137	32			15			Current4 High 2 Current Limit		Curr4 Hi 2 Lmt		Corr4 Alta 2 Límite Corr		Corr4 Alta2Lmt																															
138	32			15			Current5 High 1 Current Limit		Curr5 Hi 1 Lmt		Corr5 Alta 1 Límite Corr		Corr5 Alta1Lmt																													
139	32			15			Current5 High 2 Current Limit		Curr5 Hi 2 Lmt		Corr5 Alta 2 Límite Corr		Corr5 Alta2Lmt																															
140	32			15			Current6 High 1 Current Limit		Curr6 Hi 1 Lmt		Corr6 Alta 1 Límite Corr		Corr6 Alta1Lmt																													
141	32			15			Current6 High 2 Current Limit		Curr6 Hi 2 Lmt		Corr6 Alta 2 Límite Corr		Corr6 Alta2Lmt																															
142	32			15			Current7 High 1 Current Limit		Curr7 Hi 1 Lmt		Corr7 Alta 1 Límite Corr		Corr7 Alta1Lmt																													
143	32			15			Current7 High 2 Current Limit		Curr7 Hi 2 Lmt		Corr7 Alta 2 Límite Corr		Corr7 Alta2Lmt																															
144	32			15			Current8 High 1 Current Limit		Curr8 Hi 1 Lmt		Corr8 Alta 1 Límite Corr		Corr8 Alta1Lmt																													
145	32			15			Current8 High 2 Current Limit		Curr8 Hi 2 Lmt		Corr8 Alta 2 Límite Corr		Corr8 Alta2Lmt																															
146	32			15			Current9 High 1 Current Limit		Curr9 Hi 1 Lmt		Corr9 Alta 1 Límite Corr		Corr9 Alta1Lmt																												
147	32			15			Current9 High 2 Current Limit		Curr9 Hi 2 Lmt		Corr9 Alta 2 Límite Corr		Corr9 Alta2Lmt																														
148	32			15			Current10 High 1 Current Limit		Curr10 Hi 1 Lmt		Corr10 Alta 1 Límite Corr		Corr10 Alta1Lmt																														
149	32			15			Current10 High 2 Current Limit		Curr10 Hi 2 Lmt		Corr10 Alta 2 Límite Corr		Corr10 Alta2Lmt																														

150	32			15			Current1 High 1 Current			Curr 1 Hi 1		Corr1 Alta 1 Corr			Corr1 Alta 1																																														
151	32			15			Current1 High 2 Current			Curr 1 Hi 2		Corr1 Alta 2 Corr			Corr1 Alta 2																																															
152	32			15			Current2 High 1 Current			Curr 2 Hi 1		Corr2 Alta 1 Corr			Corr2 Alta 1																																														
153	32			15			Current2 High 2 Current			Curr 2 Hi 2		Corr2 Alta 2 Corr			Corr2 Alta 2																																															
154	32			15			Current3 High 1 Current			Curr 3 Hi 1		Corr3 Alta 1 Corr			Corr3 Alta 1																																														
155	32			15			Current3 High 2 Current			Curr 3 Hi 2		Corr3 Alta 2 Corr			Corr3 Alta 2																																															
156	32			15			Current4 High 1 Current			Curr 4 Hi 1		Corr4 Alta 1 Corr			Corr4 Alta 1																																														
157	32			15			Current4 High 2 Current			Curr 4 Hi 2		Corr4 Alta 2 Corr			Corr4 Alta 2																																														
158	32			15			Current5 High 1 Current			Curr 5 Hi 1		Corr5 Alta 1 Corr			Corr5 Alta 1																																														
159	32			15			Current5 High 2 Current			Curr 5 Hi 2		Corr5 Alta 2 Corr			Corr5 Alta 2																																															
160	32			15			Current6 High 1 Current			Curr 6 Hi 1		Corr6 Alta 1 Corr			Corr6 Alta 1																																														
161	32			15			Current6 High 2 Current			Curr 6 Hi 2		Corr6 Alta 2 Corr			Corr6 Alta 2																																															
162	32			15			Current7 High 1 Current			Curr 7 Hi 1		Corr7 Alta 1 Corr			Corr7 Alta 1																																														
163	32			15			Current7 High 2 Current			Curr 7 Hi 2		Corr7 Alta 2 Corr			Corr7 Alta 2																																															
164	32			15			Current8 High 1 Current			Curr 8 Hi 1		Corr8 Alta 1 Corr			Corr8 Alta 1																																														
165	32			15			Current8 High 2 Current			Curr 8 Hi 2		Corr8 Alta 2 Corr			Corr8 Alta 2																																															
166	32			15			Current9 High 1 Current			Curr 9 Hi 1		Corr9 Alta 1 Corr			Corr9 Alta 1																																														
167	32			15			Current9 High 2 Current			Curr 9 Hi 2		Corr9 Alta 2 Corr			Corr9 Alta 2																																														
168	32			15			Current10 High 1 Current		Curr 10 Hi 1		Corr10 Alta 1 Corr			Corr10 Alta 1																																													
169	32			15			Current10 High 2 Current		Curr 10 Hi 2		Corr10 Alta 2 Corr			Corr10 Alta 2																																															

214	32			15			Not Used				Not Used				No Utilizada			No Utilizada
215	32			15			General					General					General					General
216	32			15			Load					Load					Carga					Carga
217	32			15			Battery					Battery					Batería					Batería

221	32			15			SMDUE1 Temp 1 Assign Equip		T1 Assign Equip		SMDUE1 Temp 1 Assign Equip		T1 Assign Equip
222	32			15			SMDUE1 Temp 2 Assign Equip		T2 Assign Equip		SMDUE1 Temp 2 Assign Equip		T2 Assign Equip
223	32			15			SMDUE1 Temp 3 Assign Equip		T3 Assign Equip		SMDUE1 Temp 3 Assign Equip		T3 Assign Equip
224	32			15			SMDUE1 Temp 4 Assign Equip		T4 Assign Equip		SMDUE1 Temp 4 Assign Equip		T4 Assign Equip
225	32			15			SMDUE1 Temp 5 Assign Equip		T5 Assign Equip		SMDUE1 Temp 5 Assign Equip		T5 Assign Equip
226	32			15			SMDUE1 Temp 6 Assign Equip		T6 Assign Equip		SMDUE1 Temp 6 Assign Equip		T6 Assign Equip
227	32			15			SMDUE1 Temp 7 Assign Equip		T7 Assign Equip		SMDUE1 Temp 7 Assign Equip		T7 Assign Equip
228	32			15			SMDUE1 Temp 8 Assign Equip		T8 Assign Equip		SMDUE1 Temp 8 Assign Equip		T8 Assign Equip
229	32			15			SMDUE1 Temp 9 Assign Equip		T9 Assign Equip		SMDUE1 Temp 9 Assign Equip		T9 Assign Equip
230	32			15			SMDUE1 Temp 10 Assign Equip		T10 AssignEquip		SMDUE1 Temp 10 Assign Equip		T10 AssignEquip
231	32			15			None					None			None					None
232	32			15			Ambient					Ambient			Ambient					Ambient	
233	32			15			Battery					Battery			Battery					Battery	
234	32			15			BTRM					BTRM			BTRM			BTRM

241	32			15			Load 1					Load 1			Carga1		Carga1	
242	32			15			Load 2					Load 2			Carga2		Carga2	
243	32			15			Load 3					Load 3			Carga3		Carga3	
244	32			15			Load 4					Load 4			Carga4		Carga4	
245	32			15			Load 5					Load 5			Carga5		Carga5	
246	32			15			Load 6					Load 6			Carga6		Carga6	
247	32			15			Load 7					Load 7			Carga7		Carga7	
248	32			15			Load 8					Load 8			Carga8		Carga8	
249	32			15			Load 9					Load 9			Carga9		Carga9	
250	32			15			Load 10					Load 10			Carga10		Carga10

251	32			15			SMDUE2 Temp 1 Assign Equip		T1 Assign Equip		SMDUE2 Temp 1 Assign Equip		T1 Assign Equip	
252	32			15			SMDUE2 Temp 2 Assign Equip		T2 Assign Equip		SMDUE2 Temp 2 Assign Equip		T2 Assign Equip	
253	32			15			SMDUE2 Temp 3 Assign Equip		T3 Assign Equip		SMDUE2 Temp 3 Assign Equip		T3 Assign Equip	
254	32			15			SMDUE2 Temp 4 Assign Equip		T4 Assign Equip		SMDUE2 Temp 4 Assign Equip		T4 Assign Equip	
255	32			15			SMDUE2 Temp 5 Assign Equip		T5 Assign Equip		SMDUE2 Temp 5 Assign Equip		T5 Assign Equip	
256	32			15			SMDUE2 Temp 6 Assign Equip		T6 Assign Equip		SMDUE2 Temp 6 Assign Equip		T6 Assign Equip	
257	32			15			SMDUE2 Temp 7 Assign Equip		T7 Assign Equip		SMDUE2 Temp 7 Assign Equip		T7 Assign Equip	
258	32			15			SMDUE2 Temp 8 Assign Equip		T8 Assign Equip		SMDUE2 Temp 8 Assign Equip		T8 Assign Equip	
259	32			15			SMDUE2 Temp 9 Assign Equip		T9 Assign Equip		SMDUE2 Temp 9 Assign Equip		T9 Assign Equip	
260	32			15			SMDUE2 Temp 10 Assign Equip		T10 Assign Equip	SMDUE2 Temp 10 Assign Equip		T10 Assign Equip	
261		32		15		SMDUE1 Temp1 High 2	SMDUE1 T1 Hi2		Alta2 SMDUE1 Temp1		Al2 SMDUE1 T1
262		32		15		SMDUE1 Temp1 High 1	SMDUE1 T1 Hi1		Alta1 SMDUE1 Temp1		Al1 SMDUE1 T1
263		32		15		SMDUE1 Temp1 Low		SMDUE1 T1 Low		Baja SMDUE1 Temp1		Ba SMDUE1 T1
264		32		15		SMDUE1 Temp2 High 2	SMDUE1 T2 Hi2		Alta2 SMDUE1 Temp2		Al2 SMDUE1 T2
265		32		15		SMDUE1 Temp2 High 1	SMDUE1 T2 Hi1		Alta1 SMDUE1 Temp2		Al1 SMDUE1 T2
266		32		15		SMDUE1 Temp2 Low		SMDUE1 T2 Low		Baja SMDUE1 Temp2		Ba SMDUE1 T2
267		32		15		SMDUE1 Temp3 High 2	SMDUE1 T3 Hi2		Alta2 SMDUE1 Temp3		Al2 SMDUE1 T3
268		32		15		SMDUE1 Temp3 High 1	SMDUE1 T3 Hi1		Alta1 SMDUE1 Temp3		Al1 SMDUE1 T3
269		32		15		SMDUE1 Temp3 Low		SMDUE1 T3 Low		Baja SMDUE1 Temp3		Ba SMDUE1 T3
270		32		15		SMDUE1 Temp4 High 2	SMDUE1 T4 Hi2		Alta2 SMDUE1 Temp4		Al2 SMDUE1 T4
271		32		15		SMDUE1 Temp4 High 1	SMDUE1 T4 Hi1		Alta1 SMDUE1 Temp4		Al1 SMDUE1 T4
272		32		15		SMDUE1 Temp4 Low		SMDUE1 T4 Low		Baja SMDUE1 Temp4		Ba SMDUE1 T4
273		32		15		SMDUE1 Temp5 High 2	SMDUE1 T5 Hi2		Alta2 SMDUE1 Temp5		Al2 SMDUE1 T5
274		32		15		SMDUE1 Temp5 High 1	SMDUE1 T5 Hi1		Alta1 SMDUE1 Temp5		Al1 SMDUE1 T5
275		32		15		SMDUE1 Temp5 Low		SMDUE1 T5 Low		Baja SMDUE1 Temp5		Ba SMDUE1 T5
276		32		15		SMDUE1 Temp6 High 2	SMDUE1 T6 Hi2		Alta2 SMDUE1 Temp6		Al2 SMDUE1 T6
277		32		15		SMDUE1 Temp6 High 1	SMDUE1 T6 Hi1		Alta1 SMDUE1 Temp6		Al1 SMDUE1 T6
278		32		15		SMDUE1 Temp6 Low		SMDUE1 T6 Low		Baja SMDUE1 Temp6		Ba SMDUE1 T6
279		32		15		SMDUE1 Temp7 High 2	SMDUE1 T7 Hi2		Alta2 SMDUE1 Temp7		Al2 SMDUE1 T7
280		32		15		SMDUE1 Temp7 High 1	SMDUE1 T7 Hi1		Alta1 SMDUE1 Temp7		Al1 SMDUE1 T7
281		32		15		SMDUE1 Temp7 Low		SMDUE1 T7 Low		Baja SMDUE1 Temp7		Ba SMDUE1 T7
282		32		15		SMDUE1 Temp8 High 2	SMDUE1 T8 Hi2		Alta2 SMDUE1 Temp8		Al2 SMDUE1 T8
283		32		15		SMDUE1 Temp8 High 1	SMDUE1 T8 Hi1		Alta1 SMDUE1 Temp8		Al1 SMDUE1 T8
284		32		15		SMDUE1 Temp8 Low		SMDUE1 T8 Low		Baja SMDUE1 Temp8		Ba SMDUE1 T8
285		32		15		SMDUE1 Temp9 High 2	SMDUE1 T9 Hi2		Alta2 SMDUE1 Temp9		Al2 SMDUE1 T9
286		32		15		SMDUE1 Temp9 High 1	SMDUE1 T9 Hi1		Alta1 SMDUE1 Temp9		Al1 SMDUE1 T9
287		32		15		SMDUE1 Temp9 Low		SMDUE1 T9 Low		Baja SMDUE1 Temp9		Ba SMDUE1 T9
288		32		15		SMDUE1 Temp10 High 2	SMDUE1 T10 Hi2		Alta2 SMDUE1 Temp10		Al2 SMDUE1 T10
289		32		15		SMDUE1 Temp10 High 1	SMDUE1 T10 Hi1		Alta1 SMDUE1 Temp10		Al1 SMDUE1 T10
290		32		15		SMDUE1 Temp10 Low	SMDUE1 T10 Low		Baja SMDUE1 Temp10		Ba SMDUE1 T10
291		32		15		SMDUE2 Temp1 High 2	SMDUE2 T1 Hi2		Alta2 SMDUE2 Temp1		Al2 SMDUE2 T1
292		32		15		SMDUE2 Temp1 High 1	SMDUE2 T1 Hi1		Alta1 SMDUE2 Temp1		Al1 SMDUE2 T1
293		32		15		SMDUE2 Temp1 Low		SMDUE2 T1 Low		Baja SMDUE2 Temp1		Ba SMDUE2 T1
294		32		15		SMDUE2 Temp2 High 2	SMDUE2 T2 Hi2		Alta2 SMDUE2 Temp2		Al2 SMDUE2 T2
295		32		15		SMDUE2 Temp2 High 1	SMDUE2 T2 Hi1		Alta1 SMDUE2 Temp2		Al1 SMDUE2 T2
296		32		15		SMDUE2 Temp2 Low		SMDUE2 T2 Low		Baja SMDUE2 Temp2		Ba SMDUE2 T2
297		32		15		SMDUE2 Temp3 High 2	SMDUE2 T3 Hi2		Alta2 SMDUE2 Temp3		Al2 SMDUE2 T3
298		32		15		SMDUE2 Temp3 High 1	SMDUE2 T3 Hi1		Alta1 SMDUE2 Temp3		Al1 SMDUE2 T3
299		32		15		SMDUE2 Temp3 Low		SMDUE2 T3 Low		Baja SMDUE2 Temp3		Ba SMDUE2 T3
300		32		15		SMDUE2 Temp4 High 2	SMDUE2 T4 Hi2		Alta2 SMDUE2 Temp4		Al2 SMDUE2 T4
301		32		15		SMDUE2 Temp4 High 1	SMDUE2 T4 Hi1		Alta1 SMDUE2 Temp4		Al1 SMDUE2 T4
302		32		15		SMDUE2 Temp4 Low		SMDUE2 T4 Low		Baja SMDUE2 Temp4		Ba SMDUE2 T4
303		32		15		SMDUE2 Temp5 High 2	SMDUE2 T5 Hi2		Alta2 SMDUE2 Temp5		Al2 SMDUE2 T5
304		32		15		SMDUE2 Temp5 High 1	SMDUE2 T5 Hi1		Alta1 SMDUE2 Temp5		Al1 SMDUE2 T5
305		32		15		SMDUE2 Temp5 Low		SMDUE2 T5 Low		Baja SMDUE2 Temp5		Ba SMDUE2 T5
306		32		15		SMDUE2 Temp6 High 2	SMDUE2 T6 Hi2		Alta2 SMDUE2 Temp6		Al2 SMDUE2 T6
307		32		15		SMDUE2 Temp6 High 1	SMDUE2 T6 Hi1		Alta1 SMDUE2 Temp6		Al1 SMDUE2 T6
308		32		15		SMDUE2 Temp6 Low		SMDUE2 T6 Low		Baja SMDUE2 Temp6		Ba SMDUE2 T6
309		32		15		SMDUE2 Temp7 High 2	SMDUE2 T7 Hi2		Alta2 SMDUE2 Temp7		Al2 SMDUE2 T7
310		32		15		SMDUE2 Temp7 High 1	SMDUE2 T7 Hi1		Alta1 SMDUE2 Temp7		Al1 SMDUE2 T7
311		32		15		SMDUE2 Temp7 Low		SMDUE2 T7 Low		Baja SMDUE2 Temp7		Ba SMDUE2 T7
312		32		15		SMDUE2 Temp8 High 2	SMDUE2 T8 Hi2		Alta2 SMDUE2 Temp8		Al2 SMDUE2 T8
313		32		15		SMDUE2 Temp8 High 1	SMDUE2 T8 Hi1		Alta1 SMDUE2 Temp8		Al1 SMDUE2 T8
314		32		15		SMDUE2 Temp8 Low		SMDUE2 T8 Low		Baja SMDUE2 Temp8		Ba SMDUE2 T8
315		32		15		SMDUE2 Temp9 High 2	SMDUE2 T9 Hi2		Alta2 SMDUE2 Temp9		Al2 SMDUE2 T9
316		32		15		SMDUE2 Temp9 High 1	SMDUE2 T9 Hi1		Alta1 SMDUE2 Temp9		Al1 SMDUE2 T9
317		32		15		SMDUE2 Temp9 Low		SMDUE2 T9 Low		Baja SMDUE2 Temp9		Ba SMDUE2 T9
318		32		15		SMDUE2 Temp10 High 2	SMDUE2 T10 Hi2		Alta2 SMDUE2 Temp10		Al2 SMDUE2 T10
319		32		15		SMDUE2 Temp10 High 1	SMDUE2 T10 Hi1		Alta1 SMDUE2 Temp10		Al1 SMDUE2 T10
320		32		15		SMDUE2 Temp10 Low	SMDUE2 T10 Low		Baja SMDUE2 Temp10		Ba SMDUE2 T10

321	32			32			SMDUE1 Temp 1 Not Used		S1-T1 Not Used			SMDUE1-T1 no utilizada		S1-T1 no usada
322	32			32			SMDUE1 Temp 2 Not Used		S1-T2 Not Used			SMDUE1-T2 no utilizada		S1-T2 no usada
323	32			32			SMDUE1 Temp 3 Not Used		S1-T3 Not Used			SMDUE1-T3 no utilizada		S1-T3 no usada
324	32			32			SMDUE1 Temp 4 Not Used		S1-T4 Not Used			SMDUE1-T4 no utilizada		S1-T4 no usada
325	32			32			SMDUE1 Temp 5 Not Used		S1-T5 Not Used			SMDUE1-T5 no utilizada		S1-T5 no usada
326	32			32			SMDUE1 Temp 6 Not Used		S1-T6 Not Used			SMDUE1-T6 no utilizada		S1-T6 no usada
327	32			32			SMDUE1 Temp 7 Not Used		S1-T7 Not Used			SMDUE1-T7 no utilizada		S1-T7 no usada
328	32			32			SMDUE1 Temp 8 Not Used		S1-T8 Not Used			SMDUE1-T8 no utilizada		S1-T8 no usada
329	32			32			SMDUE1 Temp 9 Not Used		S1-T9 Not Used			SMDUE1-T9 no utilizada		S1-T9 no usada
330	32			32			SMDUE1 Temp 10 Not Used		S1-T10 Not Used			SMDUE1-T10 no utilizada		S1-T10 no usada
331	32			32			SMDUE1 Temp 1 Sensor Fail	S1-T1 SensFail			Fallo sensor SMDUE1-T1		Fallo SMDUE1-T1
332	32			32			SMDUE1 Temp 2 Sensor Fail	S1-T2 SensFail			Fallo sensor SMDUE1-T2		Fallo SMDUE1-T2
333	32			32			SMDUE1 Temp 3 Sensor Fail	S1-T3 SensFail			Fallo sensor SMDUE1-T3		Fallo SMDUE1-T3
334	32			32			SMDUE1 Temp 4 Sensor Fail	S1-T4 SensFail			Fallo sensor SMDUE1-T4		Fallo SMDUE1-T4
335	32			32			SMDUE1 Temp 5 Sensor Fail	S1-T5 SensFail			Fallo sensor SMDUE1-T5		Fallo SMDUE1-T5
336	32			32			SMDUE1 Temp 6 Sensor Fail	S1-T6 SensFail			Fallo sensor SMDUE1-T6		Fallo SMDUE1-T6
337	32			32			SMDUE1 Temp 7 Sensor Fail	S1-T7 SensFail			Fallo sensor SMDUE1-T7		Fallo SMDUE1-T7
338	32			32			SMDUE1 Temp 8 Sensor Fail	S1-T8 SensFail			Fallo sensor SMDUE1-T8		Fallo SMDUE1-T8
339	32			32			SMDUE1 Temp 9 Sensor Fail	S1-T9 SensFail			Fallo sensor SMDUE1-T9		Fallo SMDUE1-T9
340	32			32			SMDUE1 Temp 10 Sensor Fail	S1-T10SensFail			Fallo sensor SMDUE1-T10		Fallo SMDUE1-T10

341	32			32			SMDUE2 Temp 1 Not Used		S2-T1 Not Used			SMDUE2-T1 no utilizada		S2-T1 no usada
342	32			32			SMDUE2 Temp 2 Not Used		S2-T2 Not Used			SMDUE2-T2 no utilizada		S2-T2 no usada
343	32			32			SMDUE2 Temp 3 Not Used		S2-T3 Not Used			SMDUE2-T3 no utilizada		S2-T3 no usada
344	32			32			SMDUE2 Temp 4 Not Used		S2-T4 Not Used			SMDUE2-T4 no utilizada		S2-T4 no usada
345	32			32			SMDUE2 Temp 5 Not Used		S2-T5 Not Used			SMDUE2-T5 no utilizada		S2-T5 no usada
346	32			32			SMDUE2 Temp 6 Not Used		S2-T6 Not Used			SMDUE2-T6 no utilizada		S2-T6 no usada
347	32			32			SMDUE2 Temp 7 Not Used		S2-T7 Not Used			SMDUE2-T7 no utilizada		S2-T7 no usada
348	32			32			SMDUE2 Temp 8 Not Used		S2-T8 Not Used			SMDUE2-T8 no utilizada		S2-T8 no usada
349	32			32			SMDUE2 Temp 9 Not Used		S2-T9 Not Used			SMDUE2-T9 no utilizada		S2-T9 no usada
350	32			32			SMDUE2 Temp 10 Not Used		S2-T10 Not Used			SMDUE2-T10 no utilizada		S2-T9 no usada
351	32			32			SMDUE2 Temp 1 Sensor Fail	S2-T1 SensFail			Fallo sensor S2-T1			Fallo S2-T1 
352	32			32			SMDUE2 Temp 2 Sensor Fail	S2-T2 SensFail			Fallo sensor S2-T2			Fallo S2-T2 
353	32			32			SMDUE2 Temp 3 Sensor Fail	S2-T3 SensFail			Fallo sensor S2-T3			Fallo S2-T3 
354	32			32			SMDUE2 Temp 4 Sensor Fail	S2-T4 SensFail			Fallo sensor S2-T4			Fallo S2-T4 
355	32			32			SMDUE2 Temp 5 Sensor Fail	S2-T5 SensFail			Fallo sensor S2-T5			Fallo S2-T5 
356	32			32			SMDUE2 Temp 6 Sensor Fail	S2-T6 SensFail			Fallo sensor S2-T6			Fallo S2-T6 
357	32			32			SMDUE2 Temp 7 Sensor Fail	S2-T7 SensFail			Fallo sensor S2-T7			Fallo S2-T7 
358	32			32			SMDUE2 Temp 8 Sensor Fail	S2-T8 SensFail			Fallo sensor S2-T8			Fallo S2-T8 
359	32			32			SMDUE2 Temp 9 Sensor Fail	S2-T9 SensFail			Fallo sensor S2-T9			Fallo S2-T9 
360	32			32			SMDUE2 Temp 10 Sensor Fail	S2-T10 SensFail			Fallo sensor S2-T10			Fallo S2-T10

361		32			15			AI Ch1 Type 	AI Ch1 Type		AI Ch1 Tipo 	AI Ch1 Tipo	
362		32			15			AI Ch2 Type 	AI Ch2 Type		AI Ch2 Tipo 	AI Ch2 Tipo	
363		32			15			AI Ch3 Type 	AI Ch3 Type		AI Ch3 Tipo 	AI Ch3 Tipo	
364		32			15			AI Ch4 Type 	AI Ch4 Type		AI Ch4 Tipo		AI Ch4 Tipo	
365		32			15			AI Ch5 Type 	AI Ch5 Type		AI Ch5 Tipo 	AI Ch5 Tipo	
366		32			15			AI Ch6 Type 	AI Ch6 Type		AI Ch6 Tipo 	AI Ch6 Tipo	
367		32			15			AI Ch7 Type 	AI Ch7 Type		AI Ch7 Tipo		AI Ch7 Tipo	
368		32			15			AI Ch8 Type 	AI Ch8 Type		AI Ch8 Tipo 	AI Ch8 Tipo	
369		32			15			AI Ch9 Type 	AI Ch9 Type		AI Ch9 Tipo 	AI Ch9 Tipo	
370		32			15			AI Ch10 Type	AI Ch10 Type	AI Ch10 Tipo	AI Ch10 Tipo

371		32			15			Use For Shunt 		Use For Shunt		Uso Para Derivación 	Uso Para Deriv 
372		32			15			Use For Current 	Use For Current		Uso Para Corriente		Uso Para Corr 
373		32			15			Use For Volt 		Use For Volt		Uso Para Volt 			Uso Para Volt 
374		32			15			Use For Temp		Use For Temp		Uso Para Temp 			Uso Para Temp

400		32			15			Cabinet1 Volt Enable		Volt1 Enable		Gabinete 1 Volte Habilitado		Volt1 Habilit
401		32			15			Cabinet2 Volt Enable		Volt2 Enable		Gabinete 2 Volte Habilitado		Volt2 Habilit
402		32			15			Cabinet3 Volt Enable		Volt3 Enable		Gabinete 3 Volte Habilitado		Volt3 Habilit
403		32			15			Cabinet4 Volt Enable		Volt4 Enable		Gabinete 4 Volte Habilitado		Volt4 Habilit
404		32			15			Cabinet5 Volt Enable		Volt5 Enable		Gabinete 5 Volte Habilitado		Volt5 Habilit
405		32			15			Cabinet6 Volt Enable		Volt6 Enable		Gabinete 6 Volte Habilitado		Volt6 Habilit
406		32			15			Cabinet7 Volt Enable		Volt7 Enable		Gabinete 7 Volte Habilitado		Volt7 Habilit
407		32			15			Cabinet8 Volt Enable		Volt8 Enable		Gabinete 8 Volte Habilitado		Volt8 Habilit
408		32			15			Cabinet9 Volt Enable		Volt9 Enable		Gabinete 9 Volte Habilitado		Volt9 Habilit
409		32			15			Cabinet10 Volt Enable		Volt10 Enable		Gabinete 10 Volte Habilitado		Volt10 Habilit

410		32			15			Fuse1 Alarm Enable			Fuse1 Alm Enable		Fuse1 Alarma Habilitar		Fuse1 Alm
411		32			15			Fuse2 Alarm Enable			Fuse2 Alm Enable		Fuse2 Alarma Habilitar		Fuse2 Alm
412		32			15			Fuse3 Alarm Enable			Fuse3 Alm Enable		Fuse3 Alarma Habilitar		Fuse3 Alm
413		32			15			Fuse4 Alarm Enable			Fuse4 Alm Enable		Fuse4 Alarma Habilitar		Fuse4 Alm
414		32			15			Fuse5 Alarm Enable			Fuse5 Alm Enable		Fuse5 Alarma Habilitar		Fuse5 Alm
415		32			15			Fuse6 Alarm Enable			Fuse6 Alm Enable		Fuse6 Alarma Habilitar		Fuse6 Alm
416		32			15			Fuse7 Alarm Enable			Fuse7 Alm Enable		Fuse7 Alarma Habilitar		Fuse7 Alm
417		32			15			Fuse8 Alarm Enable			Fuse8 Alm Enable		Fuse8 Alarma Habilitar		Fuse8 Alm
418		32			15			Fuse9 Alarm Enable			Fuse9 Alm Enable		Fuse9 Alarma Habilitar		Fuse9 Alm
419		32			15			Fuse10 Alarm Enable			Fuse10 Alm Enable		Fuse10 Alarma Habilitar		Fuse10 Alm
420		32			15			Analog1 Input Type			Ana1 InputType		Analog1 Tipo de entrada		Ana1TipoEntrada
421		32			15			Analog2 Input Type			Ana2 InputType		Analog2 Tipo de entrada		Ana2TipoEntrada
422		32			15			Analog3 Input Type			Ana3 InputType		Analog3 Tipo de entrada		Ana3TipoEntrada
423		32			15			Analog4 Input Type			Ana4 InputType		Analog4 Tipo de entrada		Ana4TipoEntrada
424		32			15			Analog5 Input Type			Ana5 InputType		Analog5 Tipo de entrada		Ana5TipoEntrada
425		32			15			Analog6 Input Type			Ana6 InputType		Analog6 Tipo de entrada		Ana6TipoEntrada
426		32			15			Analog7 Input Type			Ana7 InputType		Analog7 Tipo de entrada		Ana7TipoEntrada
427		32			15			Analog8 Input Type			Ana8 InputType		Analog8 Tipo de entrada		Ana8TipoEntrada
428		32			15			Analog9 Input Type			Ana9 InputType		Analog9 Tipo de entrada		Ana9TipoEntrada
429		32			15			Analog10 Input Type			Ana10 InputType		Analog10 Tipo de entrada		Ana10TipoEntrad
430		32			15			Disable						Disable					Inhabilitar				Inhabilitar
431		32			15			Enable						Enable					Habilitar				Habilitar
432		32			15			Shunt						Shunt					Derivación				Derivación
433		32			15			Temperature					Temperature				Temperatura				Temperatura
434		32			15			Transducer (0-10V)			Tsr (0-10V)				Transductor(0-10V)		Tsr (0-10V)	
435		32			15			Transducer (0-20mA)			Tsr (0-20mA)			Transductor(0-20mA) 	Tsr (0-20mA)

441		32			15			Input Block 1		Input Block 1	Input Block 1		Input Block 1
442		32			15			Input Block 2		Input Block 2	Input Block 2		Input Block 2
443		32			15			Input Block 3		Input Block 3	Input Block 3		Input Block 3
444		32			15			Input Block 4		Input Block 4	Input Block 4		Input Block 4
445		32			15			Input Block 5		Input Block 5	Input Block 5		Input Block 5
446		32			15			Input Block 6		Input Block 6	Input Block 6		Input Block 6
447		32			15			Input Block 7		Input Block 7	Input Block 7		Input Block 7
448		32			15			Input Block 8		Input Block 8	Input Block 8		Input Block 8
449		32			15			Input Block 9		Input Block 9	Input Block 9		Input Block 9
450		32			15			Input Block 10		Input Block 10	Input Block 10		Input Block 10

451		32			15			Current Transducer1		C Tran 1	Transductor Corriente1			C Tran 1
452		32			15			Current Transducer2		C Tran 2	Transductor Corriente1			C Tran 2	
453		32			15			Current Transducer3		C Tran 3	Transductor Corriente1			C Tran 3
454		32			15			Current Transducer4		C Tran 4	Transductor Corriente1			C Tran 4	
455		32			15			Current Transducer5		C Tran 5	Transductor Corriente1			C Tran 5
456		32			15			Current Transducer6		C Tran 6	Transductor Corriente1			C Tran 6	
457		32			15			Current Transducer7		C Tran 7	Transductor Corriente1			C Tran 7
458		32			15			Current Transducer8		C Tran 8	Transductor Corriente1			C Tran 8
459		32			15			Current Transducer9		C Tran 9	Transductor Corriente1			C Tran 9
460		32			15			Current Transducer10	C Tran 10	Transductor Corriente1			C Tran 10

461		32			15		Voltage Transducer1			V Tran 1	Transductor Voltaje 1		V Tran 1	
462		32			15		Voltage Transducer2			V Tran 2	Transductor Voltaje 1		V Tran 2
463		32			15		Voltage Transducer3			V Tran 3	Transductor Voltaje 1		V Tran 3	
464		32			15		Voltage Transducer4			V Tran 4	Transductor Voltaje 1		V Tran 4	
465		32			15		Voltage Transducer5			V Tran 5	Transductor Voltaje 1		V Tran 5
466		32			15		Voltage Transducer6			V Tran 6	Transductor Voltaje 1		V Tran 6
467		32			15		Voltage Transducer7			V Tran 7	Transductor Voltaje 1		V Tran 7
468		32			15		Voltage Transducer8			V Tran 8	Transductor Voltaje 1		V Tran 8
469		32			15		Voltage Transducer9			V Tran 9	Transductor Voltaje 1		V Tran 9
470		32			15		Voltage Transducer10		V Tran 10	Transductor Voltaje 1		V Tran 10

471		32			15			X1					X1					X1					X1	
472		32			15			Y1(Value at X1)		Y1(Value at X1)		Y1(Valor en X1)		Y1(Valor en X1)	
473		32			15			X2					X2					X2					X2
474		32			15			Y2(Value at X2)		Y2(Value at X2)		Y2(Valor en X2)		Y2(Valor en X2)	


600		32			15			Voltage1 High2 Volt Limit	Vol1 Hi2 Vol		Límite Alto Volt de Volt1		Vol1 Alto2 Vol
601		32			15			Voltage1 High1 Volt Limit	Vol1 Hi1 Vol		Límite Alto Volt de Volt1		Vol1 Alto2 Vol 	
602		32			15			Voltage1 Low1 Volt Limit	Vol1 Lw1 Vol		Límite Bajo Volt de Volt1		Vol1 Bajo2 Vol
603		32			15			Voltage1 Low2 Volt Limit	Vol1 Lw2 Vol		Límite Bajo Volt de Volt1		Vol1 Bajo2 Vol	
604		32			15			Voltage2 High2 Volt Limit	Vol2 Hi2 Vol		Límite Alto Volt de Volt2		Vol2 Alto2 Vol
605		32			15			Voltage2 High1 Volt Limit	Vol2 Hi1 Vol		Límite Alto Volt de Volt2		Vol2 Alto2 Vol
606		32			15			Voltage2 Low1 Volt Limit	Vol2 Lw1 Vol		Límite Bajo Volt de Volt2		Vol2 Bajo2 Vol
607		32			15			Voltage2 Low2 Volt Limit	Vol2 Lw2 Vol		Límite Bajo Volt de Volt2		Vol2 Bajo2 Vol
608		32			15			Voltage3 High2 Volt Limit	Vol3 Hi2 Vol		Límite Alto Volt de Volt3		Vol3 Alto2 Vol
609		32			15			Voltage3 High1 Volt Limit	Vol3 Hi1 Vol		Límite Alto Volt de Volt3		Vol3 Alto2 Vol
610		32			15			Voltage3 Low1 Volt Limit	Vol3 Lw1 Vol		Límite Bajo Volt de Volt3		Vol3 Bajo2 Vol
611		32			15			Voltage3 Low2 Volt Limit	Vol3 Lw2 Vol		Límite Bajo Volt de Volt3		Vol3 Bajo2 Vol
612		32			15			Voltage4 High2 Volt Limit	Vol4 Hi2 Vol		Límite Alto Volt de Volt4		Vol4 Alto2 Vol
613		32			15			Voltage4 High1 Volt Limit	Vol4 Hi1 Vol		Límite Alto Volt de Volt4		Vol4 Alto2 Vol
614		32			15			Voltage4 Low1 Volt Limit	Vol4 Lw1 Vol		Límite Bajo Volt de Volt4		Vol4 Bajo2 Vol
615		32			15			Voltage4 Low2 Volt Limit	Vol4 Lw2 Vol		Límite Bajo Volt de Volt4		Vol4 Bajo2 Vol
616		32			15			Voltage5 High2 Volt Limit	Vol5 Hi2 Vol		Límite Alto Volt de Volt5		Vol5 Alto2 Vol
617		32			15			Voltage5 High1 Volt Limit	Vol5 Hi1 Vol		Límite Alto Volt de Volt5		Vol5 Alto2 Vol
618		32			15			Voltage5 Low1 Volt Limit	Vol5 Lw1 Vol		Límite Bajo Volt de Volt5		Vol5 Bajo2 Vol
619		32			15			Voltage5 Low2 Volt Limit	Vol5 Lw2 Vol		Límite Bajo Volt de Volt5		Vol5 Bajo2 Vol
620		32			15			Voltage6 High2 Volt Limit	Vol6 Hi2 Vol		Límite Alto Volt de Volt6		Vol6 Alto2 Vol
621		32			15			Voltage6 High1 Volt Limit	Vol6 Hi1 Vol		Límite Alto Volt de Volt6		Vol6 Alto2 Vol
622		32			15			Voltage6 Low1 Volt Limit	Vol6 Lw1 Vol		Límite Bajo Volt de Volt6		Vol6 Bajo2 Vol
623		32			15			Voltage6 Low2 Volt Limit	Vol6 Lw2 Vol		Límite Bajo Volt de Volt6		Vol6 Bajo2 Vol
624		32			15			Voltage7 High2 Volt Limit	Vol7 Hi2 Vol		Límite Alto Volt de Volt7		Vol7 Alto2 Vol
625		32			15			Voltage7 High1 Volt Limit	Vol7 Hi1 Vol		Límite Alto Volt de Volt7		Vol7 Alto2 Vol
626		32			15			Voltage7 Low1 Volt Limit	Vol7 Lw1 Vol		Límite Bajo Volt de Volt7		Vol7 Bajo2 Vol
627		32			15			Voltage7 Low2 Volt Limit	Vol7 Lw2 Vol		Límite Bajo Volt de Volt7		Vol7 Bajo2 Vol
628		32			15			Voltage8 High2 Volt Limit	Vol8 Hi2 Vol		Límite Alto Volt de Volt8		Vol8 Alto2 Vol
629		32			15			Voltage8 High1 Volt Limit	Vol8 Hi1 Vol		Límite Alto Volt de Volt8		Vol8 Alto2 Vol
630		32			15			Voltage8 Low1 Volt Limit	Vol8 Lw1 Vol		Límite Bajo Volt de Volt8		Vol8 Bajo2 Vol
631		32			15			Voltage8 Low2 Volt Limit	Vol8 Lw2 Vol		Límite Bajo Volt de Volt8		Vol8 Bajo2 Vol
632		32			15			Voltage9 High2 Volt Limit	Vol9 Hi2 Vol		Límite Alto Volt de Volt9		Vol9 Alto2 Vol
633		32			15			Voltage9 High1 Volt Limit	Vol9 Hi1 Vol		Límite Alto Volt de Volt9		Vol9 Alto2 Vol
634		32			15			Voltage9 Low1 Volt Limit	Vol9 Lw1 Vol		Límite Bajo Volt de Volt9		Vol9 Bajo2 Vol
635		32			15			Voltage9 Low2 Volt Limit	Vol9 Lw2 Vol		Límite Bajo Volt de Volt9		Vol9 Bajo2 Vol
636		32			15			Voltage10 High2 Volt Limit	Vol10 Hi2 Vol		Límite Alto Volt de Volt10		Vol10 Alto2 Vol
637		32			15			Voltage10 High1 Volt Limit	Vol10 Hi1 Vol		Límite Alto Volt de Volt10		Vol10 Alto2 Vol
638		32			15			Voltage10 Low1 Volt Limit	Vol10 Lw1 Vol		Límite Bajo Volt de Volt10		Vol10 Bajo2 Vol
639		32			15			Voltage10 Low2 Volt Limit	Vol10 Lw2 Vol		Límite Bajo Volt de Volt10		Vol10 Bajo2 Vol

640		32			15			Voltage1 Low1 Volt Alarm			Vol1 Lw1 Vol Alm		Alarma Bajo1 Volt de Volt1		VoltBajo1Volt1
641		32			15			Voltage1 Low2 Volt Alarm			Vol1 Lw2 Vol Alm		Alarma Bajo2 Volt de Volt1		VoltBajo2Volt1	
642		32			15			Voltage1 High1 Volt Alarm			Vol1 Hi1 Vol Alm		Alarma Alto1 Volt de Volt1		VoltAlto1Volt1
643		32			15			Voltage1 High2 Volt Alarm			Vol1 Hi2 Vol Alm		Alarma Alto2 Volt de Volt1		VoltAlto2Volt1		
644		32			15			Voltage2 Low1 Volt Alarm			Vol2 Lw1 Vol Alm		Alarma Bajo1 Volt de Volt2		VoltBajo1Volt2	
645		32			15			Voltage2 Low2 Volt Alarm			Vol2 Lw2 Vol Alm		Alarma Bajo2 Volt de Volt2		VoltBajo2Volt2
646		32			15			Voltage2 High1 Volt Alarm			Vol2 Hi1 Vol Alm		Alarma Alto1 Volt de Volt2		VoltAlto1Volt2
647		32			15			Voltage2 High2 Volt Alarm			Vol2 Hi2 Vol Alm		Alarma Alto2 Volt de Volt2		VoltAlto2Volt2	
648		32			15			Voltage3 Low1 Volt Alarm			Vol3 Lw1 Vol Alm		Alarma Bajo1 Volt de Volt3		VoltBajo1Volt3
649		32			15			Voltage3 Low2 Volt Alarm			Vol3 Lw2 Vol Alm		Alarma Bajo2 Volt de Volt3		VoltBajo2Volt3
650		32			15			Voltage3 High1 Volt Alarm			Vol3 Hi1 Vol Alm		Alarma Alto1 Volt de Volt3		VoltAlto1Volt3
651		32			15			Voltage3 High2 Volt Alarm			Vol3 Hi2 Vol Alm		Alarma Alto2 Volt de Volt3		VoltAlto2Volt3
652		32			15			Voltage4 Low1 Volt Alarm			Vol4 Lw1 Vol Alm		Alarma Bajo1 Volt de Volt4		VoltBajo1Volt4
653		32			15			Voltage4 Low2 Volt Alarm			Vol4 Lw2 Vol Alm		Alarma Bajo2 Volt de Volt4		VoltBajo2Volt4
654		32			15			Voltage4 High1 Volt Alarm			Vol4 Hi1 Vol Alm		Alarma Alto1 Volt de Volt4		VoltAlto1Volt4
655		32			15			Voltage4 High2 Volt Alarm			Vol4 Hi2 Vol Alm		Alarma Alto2 Volt de Volt4		VoltAlto2Volt4
656		32			15			Voltage5 Low1 Volt Alarm			Vol5 Lw1 Vol Alm		Alarma Bajo1 Volt de Volt5		VoltBajo1Volt5
657		32			15			Voltage5 Low2 Volt Alarm			Vol5 Lw2 Vol Alm		Alarma Bajo2 Volt de Volt5		VoltBajo2Volt5
658		32			15			Voltage5 High1 Volt Alarm			Vol5 Hi1 Vol Alm		Alarma Alto1 Volt de Volt5		VoltAlto1Volt5
659		32			15			Voltage5 High2 Volt Alarm			Vol5 Hi2 Vol Alm		Alarma Alto2 Volt de Volt5		VoltAlto2Volt5
660		32			15			Voltage6 Low1 Volt Alarm			Vol6 Lw1 Vol Alm		Alarma Bajo1 Volt de Volt6		VoltBajo1Volt6	
661		32			15			Voltage6 Low2 Volt Alarm			Vol6 Lw2 Vol Alm		Alarma Bajo2 Volt de Volt6		VoltBajo2Volt6
662		32			15			Voltage6 High1 Volt Alarm			Vol6 Hi1 Vol Alm		Alarma Alto1 Volt de Volt6		VoltAlto1Volt6
663		32			15			Voltage6 High2 Volt Alarm			Vol6 Hi2 Vol Alm		Alarma Alto2 Volt de Volt6		VoltAlto2Volt6	
664		32			15			Voltage7 Low1 Volt Alarm			Vol7 Lw1 Vol Alm		Alarma Bajo1 Volt de Volt7		VoltBajo1Volt7	
665		32			15			Voltage7 Low2 Volt Alarm			Vol7 Lw2 Vol Alm		Alarma Bajo2 Volt de Volt7		VoltBajo2Volt7
666		32			15			Voltage7 High1 Volt Alarm			Vol7 Hi1 Vol Alm		Alarma Alto1 Volt de Volt7		VoltAlto1Volt7
667		32			15			Voltage7 High2 Volt Alarm			Vol7 Hi2 Vol Alm		Alarma Alto2 Volt de Volt7		VoltAlto2Volt7	
668		32			15			Voltage8 Low1 Volt Alarm			Vol8 Lw1 Vol Alm		Alarma Bajo1 Volt de Volt8		VoltBajo1Volt8	
669		32			15			Voltage8 Low2 Volt Alarm			Vol8 Lw2 Vol Alm		Alarma Bajo2 Volt de Volt8		VoltBajo2Volt8
670		32			15			Voltage8 High1 Volt Alarm			Vol8 Hi1 Vol Alm		Alarma Alto1 Volt de Volt8		VoltAlto1Volt8
671		32			15			Voltage8 High2 Volt Alarm			Vol8 Hi2 Vol Alm		Alarma Alto2 Volt de Volt8		VoltAlto2Volt8	
672		32			15			Voltage9 Low1 Volt Alarm			Vol9 Lw1 Vol Alm		Alarma Bajo1 Volt de Volt9		VoltBajo1Volt9
673		32			15			Voltage9 Low2 Volt Alarm			Vol9 Lw2 Vol Alm		Alarma Bajo2 Volt de Volt9		VoltBajo2Volt9
674		32			15			Voltage9 High1 Volt Alarm			Vol9 Hi1 Vol Alm		Alarma Alto1 Volt de Volt9		VoltAlto1Volt9
675		32			15			Voltage9 High2 Volt Alarm			Vol9 Hi2 Vol Alm		Alarma Alto2 Volt de Volt9		VoltAlto2Volt9
676		32			15			Voltage10 Low1 Volt Alarm			Vol10 Lw1 Vol Alm		Alarma Bajo1 Volt de Volt10		VoltBajo1Volt10
677		32			15			Voltage10 Low2 Volt Alarm			Vol10 Lw2 Vol Alm		Alarma Bajo2 Volt de Volt10		VoltBajo2Volt10
678		32			15			Voltage10 High1 Volt Alarm			Vol10 Hi1 Vol Alm		Alarma Alto1 Volt de Volt10		VoltAlto1Volt10
679		32			15			Voltage10 High2 Volt Alarm			Vol10 Hi2 Vol Alm		Alarma Alto2 Volt de Volt10		VoltAlto2Volt10

680		32			15			Transducer1 High2 Volt Limit		Vol1 Hi2 Vol		Límite Alto 2 Voltios Trans1		VoltAlto2Volt1
681		32			15			Transducer1 High1 Volt Limit		Vol1 Hi1 Vol		Límite Alto 1 Voltios Trans1		VoltAlto1Volt1	
682		32			15			Transducer1 Low1 Volt Limit			Vol1 Lw1 Vol		Límite Bajo 2 Voltios Trans1		VoltBajo2Volt1
683		32			15			Transducer1 Low2 Volt Limit			Vol1 Lw2 Vol		Límite Bajo 1 Voltios Trans1		VoltBajo1Volt1	
684		32			15			Transducer2 High2 Volt Limit		Vol2 Hi2 Vol		Límite Alto 2 Voltios Trans2		VoltAlto2Volt2
685		32			15			Transducer2 High1 Volt Limit		Vol2 Hi1 Vol		Límite Alto 1 Voltios Trans2		VoltAlto1Volt2
686		32			15			Transducer2 Low1 Volt Limit			Vol2 Lw1 Vol		Límite Bajo 2 Voltios Trans2		VoltBajo2Volt2
687		32			15			Transducer2 Low2 Volt Limit			Vol2 Lw2 Vol		Límite Bajo 1 Voltios Trans2		VoltBajo1Volt2
688		32			15			Transducer3 High2 Volt Limit		Vol3 Hi2 Vol		Límite Alto 2 Voltios Trans3		VoltAlto2Volt3
689		32			15			Transducer3 High1 Volt Limit		Vol3 Hi1 Vol		Límite Alto 1 Voltios Trans3		VoltAlto1Volt3
690		32			15			Transducer3 Low1 Volt Limit			Vol3 Lw1 Vol		Límite Bajo 2 Voltios Trans3		VoltBajo2Volt3
691		32			15			Transducer3 Low2 Volt Limit			Vol3 Lw2 Vol		Límite Bajo 1 Voltios Trans3		VoltBajo1Volt3
692		32			15			Transducer4 High2 Volt Limit		Vol4 Hi2 Vol		Límite Alto 2 Voltios Trans4		VoltAlto2Volt4
693		32			15			Transducer4 High1 Volt Limit		Vol4 Hi1 Vol		Límite Alto 1 Voltios Trans4		VoltAlto1Volt4
694		32			15			Transducer4 Low1 Volt Limit			Vol4 Lw1 Vol		Límite Bajo 2 Voltios Trans4		VoltBajo2Volt4
695		32			15			Transducer4 Low2 Volt Limit			Vol4 Lw2 Vol		Límite Bajo 1 Voltios Trans4		VoltBajo1Volt4
696		32			15			Transducer5 High2 Volt Limit		Vol5 Hi2 Vol		Límite Alto 2 Voltios Trans5		VoltAlto2Volt5
697		32			15			Transducer5 High1 Volt Limit		Vol5 Hi1 Vol		Límite Alto 1 Voltios Trans5		VoltAlto1Volt5
698		32			15			Transducer5 Low1 Volt Limit			Vol5 Lw1 Vol		Límite Bajo 2 Voltios Trans5		VoltBajo2Volt5
699		32			15			Transducer5 Low2 Volt Limit			Vol5 Lw2 Vol		Límite Bajo 1 Voltios Trans5		VoltBajo1Volt5
700		32			15			Transducer6 High2 Volt Limit		Vol6 Hi2 Vol		Límite Alto 2 Voltios Trans6		VoltAlto2Volt6
701		32			15			Transducer6 High1 Volt Limit		Vol6 Hi1 Vol		Límite Alto 1 Voltios Trans6		VoltAlto1Volt6
702		32			15			Transducer6 Low1 Volt Limit			Vol6 Lw1 Vol		Límite Bajo 2 Voltios Trans6		VoltBajo2Volt6
703		32			15			Transducer6 Low2 Volt Limit			Vol6 Lw2 Vol		Límite Bajo 1 Voltios Trans6		VoltBajo1Volt6
704		32			15			Transducer7 High2 Volt Limit		Vol7 Hi2 Vol		Límite Alto 2 Voltios Trans7		VoltAlto2Volt7
705		32			15			Transducer7 High1 Volt Limit		Vol7 Hi1 Vol		Límite Alto 1 Voltios Trans7		VoltAlto1Volt7
706		32			15			Transducer7 Low1 Volt Limit			Vol7 Lw1 Vol		Límite Bajo 2 Voltios Trans7		VoltBajo2Volt7
707		32			15			Transducer7 Low2 Volt Limit			Vol7 Lw2 Vol		Límite Bajo 1 Voltios Trans7		VoltBajo1Volt7
708		32			15			Transducer8 High2 Volt Limit		Vol8 Hi2 Vol		Límite Alto 2 Voltios Trans8		VoltAlto2Volt8
709		32			15			Transducer8 High1 Volt Limit		Vol8 Hi1 Vol		Límite Alto 1 Voltios Trans8		VoltAlto1Volt8
710		32			15			Transducer8 Low1 Volt Limit			Vol8 Lw1 Vol		Límite Bajo 2 Voltios Trans8		VoltBajo2Volt8
711		32			15			Transducer8 Low2 Volt Limit			Vol8 Lw2 Vol		Límite Bajo 1 Voltios Trans8		VoltBajo1Volt8
712		32			15			Transducer9 High2 Volt Limit		Vol9 Hi2 Vol		Límite Alto 2 Voltios Trans9		VoltAlto2Volt9
713		32			15			Transducer9 High1 Volt Limit		Vol9 Hi1 Vol		Límite Alto 1 Voltios Trans9		VoltAlto1Volt9
714		32			15			Transducer9 Low1 Volt Limit			Vol9 Lw1 Vol		Límite Bajo 2 Voltios Trans9		VoltBajo2Volt9
715		32			15			Transducer9 Low2 Volt Limit			Vol9 Lw2 Vol		Límite Bajo 1 Voltios Trans9		VoltBajo1Volt9
716		32			15			Transducer10 High2 Volt Limit		Vol10 Hi2 Vol		Límite Alto 2 Voltios Trans10		VoltAlto2Volt10
717		32			15			Transducer10 High1 Volt Limit		Vol10 Hi1 Vol		Límite Alto 1 Voltios Trans10		VoltAlto1Volt10
718		32			15			Transducer10 Low1 Volt Limit		Vol10 Lw1 Vol		Límite Bajo 2 Voltios Trans10		VoltBajo2Volt10
719		32			15			Transducer10 Low2 Volt Limit		Vol10 Lw2 Vol		Límite Bajo 1 Voltios Trans10		VoltBajo1Volt10

720		32			15			TranVolt1 Low1 Volt Alarm			TranVol1 Lw1 Alm		TranVolt1Bajo1 Volt Alm		TranVolt1Bajo1	
721		32			15			TranVolt1 Low2 Volt Alarm			TranVol1 Lw2 Alm		TranVolt1Bajo2 Volt Alm		TranVolt1Bajo2		
722		32			15			TranVolt1 High1 Volt Alarm			TranVol1 Hi1 Alm		TranVolt1Alto1 Volt Alm		TranVolt1Alto1
723		32			15			TranVolt1 High2 Volt Alarm			TranVol1 Hi2 Alm		TranVolt1Alto2 Volt Alm		TranVolt1Alto2		
724		32			15			TranVolt2 Low1 Volt Alarm			TranVol2 Lw1 Alm		TranVolt2Bajo1 Volt Alm		TranVolt2Bajo1	
725		32			15			TranVolt2 Low2 Volt Alarm			TranVol2 Lw2 Alm		TranVolt2Bajo2 Volt Alm		TranVolt2Bajo2	
726		32			15			TranVolt2 High1 Volt Alarm			TranVol2 Hi1 Alm		TranVolt2Alto1 Volt Alm		TranVolt2Alto1
727		32			15			TranVolt2 High2 Volt Alarm			TranVol2 Hi2 Alm		TranVolt2Alto2 Volt Alm		TranVolt2Alto2		
728		32			15			TranVolt3 Low1 Volt Alarm			TranVol3 Lw1 Alm		TranVolt3Bajo1 Volt Alm		TranVolt3Bajo1
729		32			15			TranVolt3 Low2 Volt Alarm			TranVol3 Lw2 Alm		TranVolt3Bajo2 Volt Alm		TranVolt3Bajo2	
730		32			15			TranVolt3 High1 Volt Alarm			TranVol3 Hi1 Alm		TranVolt3Alto1 Volt Alm		TranVolt3Alto1
731		32			15			TranVolt3 High2 Volt Alarm			TranVol3 Hi2 Alm		TranVolt3Alto2 Volt Alm		TranVolt3Alto2	
732		32			15			TranVolt4 Low1 Volt Alarm			TranVol4 Lw1 Alm		TranVolt4Bajo1 Volt Alm		TranVolt4Bajo1
733		32			15			TranVolt4 Low2 Volt Alarm			TranVol4 Lw2 Alm		TranVolt4Bajo2 Volt Alm		TranVolt4Bajo2	
734		32			15			TranVolt4 High1 Volt Alarm			TranVol4 Hi1 Alm		TranVolt4Alto1 Volt Alm		TranVolt4Alto1
735		32			15			TranVolt4 High2 Volt Alarm			TranVol4 Hi2 Alm		TranVolt4Alto2 Volt Alm		TranVolt4Alto2	
736		32			15			TranVolt5 Low1 Volt Alarm			TranVol5 Lw1 Alm		TranVolt5Bajo1 Volt Alm		TranVolt5Bajo1
737		32			15			TranVolt5 Low2 Volt Alarm			TranVol5 Lw2 Alm		TranVolt5Bajo2 Volt Alm		TranVolt5Bajo2	
738		32			15			TranVolt5 High1 Volt Alarm			TranVol5 Hi1 Alm		TranVolt5Alto1 Volt Alm		TranVolt5Alto1
739		32			15			TranVolt5 High2 Volt Alarm			TranVol5 Hi2 Alm		TranVolt5Alto2 Volt Alm		TranVolt5Alto2	
740		32			15			TranVolt6 Low1 Volt Alarm			TranVol6 Lw1 Alm		TranVolt6Bajo1 Volt Alm		TranVolt6Bajo1	
741		32			15			TranVolt6 Low2 Volt Alarm			TranVol6 Lw2 Alm		TranVolt6Bajo2 Volt Alm		TranVolt6Bajo2	
742		32			15			TranVolt6 High1 Volt Alarm			TranVol6 Hi1 Alm		TranVolt6Alto1 Volt Alm		TranVolt6Alto1
743		32			15			TranVolt6 High2 Volt Alarm			TranVol6 Hi2 Alm		TranVolt6Alto2 Volt Alm		TranVolt6Alto2		
744		32			15			TranVolt7 Low1 Volt Alarm			TranVol7 Lw1 Alm		TranVolt7Bajo1 Volt Alm		TranVolt7Bajo1	
745		32			15			TranVolt7 Low2 Volt Alarm			TranVol7 Lw2 Alm		TranVolt7Bajo2 Volt Alm		TranVolt7Bajo2	
746		32			15			TranVolt7 High1 Volt Alarm			TranVol7 Hi1 Alm		TranVolt7Alto1 Volt Alm		TranVolt7Alto1
747		32			15			TranVolt7 High2 Volt Alarm			TranVol7 Hi2 Alm		TranVolt7Alto2 Volt Alm		TranVolt7Alto2		
748		32			15			TranVolt8 Low1 Volt Alarm			TranVol8 Lw1 Alm		TranVolt8Bajo1 Volt Alm		TranVolt8Bajo1	
749		32			15			TranVolt8 Low2 Volt Alarm			TranVol8 Lw2 Alm		TranVolt8Bajo2 Volt Alm		TranVolt8Bajo2	
750		32			15			TranVolt8 High1 Volt Alarm			TranVol8 Hi1 Alm		TranVolt8Alto1 Volt Alm		TranVolt8Alto1
751		32			15			TranVolt8 High2 Volt Alarm			TranVol8 Hi2 Alm		TranVolt8Alto2 Volt Alm		TranVolt8Alto2		
752		32			15			TranVolt9 Low1 Volt Alarm			TranVol9 Lw1 Alm		TranVolt9Bajo1 Volt Alm		TranVolt9Bajo1	
753		32			15			TranVolt9 Low2 Volt Alarm			TranVol9 Lw2 Alm		TranVolt9Bajo2 Volt Alm		TranVolt9Bajo2	
754		32			15			TranVolt9 High1 Volt Alarm			TranVol9 Hi1 Alm		TranVolt9Alto1 Volt Alm		TranVolt9Alto1
755		32			15			TranVolt9 High2 Volt Alarm			TranVol9 Hi2 Alm		TranVolt9Alto2 Volt Alm		TranVolt9Alto2	
756		32			15			TranVolt10 Low1 Volt Alarm			TranVol10 Lw1 Alm		TranVolt10Bajo1 Volt Alm		TranVolt10Bajo1	
757		32			15			TranVolt10 Low2 Volt Alarm			TranVol10 Lw2 Alm		TranVolt10Bajo2 Volt Alm		TranVolt10Bajo2	
758		32			15			TranVolt10 High1 Volt Alarm			TranVol10 Hi1 Alm		TranVolt10Alto1 Volt Alm		TranVolt10Alto1
759		32			15			TranVolt10 High2 Volt Alarm			TranVol10 Hi2 Alm		TranVolt10Alto2 Volt Alm		TranVolt10Alto2	

760		32			15			Transducer1 High2 Curr Limit		Curr1 Hi2 Curr		Transducer1 Alto 2. Límite Corr		Corr1Alta2Corr	
761		32			15			Transducer1 High1 Curr Limit		Curr1 Hi1 Curr		Transducer1 Alto 1. Límite Corr		Corr1Alta1Corr
762		32			15			Transducer1 Low1 Curr Limit			Curr1 Lw1 Curr		Transducer1 Bajo 1. Límite Corr		Corr1Bajo1Corr
763		32			15			Transducer1 Low2 Curr Limit			Curr1 Lw2 Curr		Transducer1 Bajo 2. Límite Corr		Corr1Bajo2Corr
764		32			15			Transducer2 High2 Curr Limit		Curr2 Hi2 Curr		Transducer2 Alto 2. Límite Corr		Corr2Alta2Corr
765		32			15			Transducer2 High1 Curr Limit		Curr2 Hi1 Curr		Transducer2 Alto 1. Límite Corr		Corr2Alta1Corr
766		32			15			Transducer2 Low1 Curr Limit			Curr2 Lw1 Curr		Transducer2 Bajo 1. Límite Corr		Corr2Bajo1Corr
767		32			15			Transducer2 Low2 Curr Limit			Curr2 Lw2 Curr		Transducer2 Bajo 2. Límite Corr		Corr2Bajo2Corr
768		32			15			Transducer3 High2 Curr Limit		Curr3 Hi2 Curr		Transducer3 Alto 2. Límite Corr		Corr3Alta2Corr
769		32			15			Transducer3 High1 Curr Limit		Curr3 Hi1 Curr		Transducer3 Alto 1. Límite Corr		Corr3Alta1Corr
770		32			15			Transducer3 Low1 Curr Limit			Curr3 Lw1 Curr		Transducer3 Bajo 1. Límite Corr		Corr3Bajo1Corr
771		32			15			Transducer3 Low2 Curr Limit			Curr3 Lw2 Curr		Transducer3 Bajo 2. Límite Corr		Corr3Bajo2Corr
772		32			15			Transducer4 High2 Curr Limit		Curr4 Hi2 Curr		Transducer4 Alto 2. Límite Corr		Corr4Alta2Corr
773		32			15			Transducer4 High1 Curr Limit		Curr4 Hi1 Curr		Transducer4 Alto 1. Límite Corr		Corr4Alta1Corr
774		32			15			Transducer4 Low1 Curr Limit			Curr4 Lw1 Curr		Transducer4 Bajo 1. Límite Corr		Corr4Bajo1Corr
775		32			15			Transducer4 Low2 Curr Limit			Curr4 Lw2 Curr		Transducer4 Bajo 2. Límite Corr		Corr4Bajo2Corr
776		32			15			Transducer5 High2 Curr Limit		Curr5 Hi2 Curr		Transducer5 Alto 2. Límite Corr		Corr5Alta2Corr
777		32			15			Transducer5 High1 Curr Limit		Curr5 Hi1 Curr		Transducer5 Alto 1. Límite Corr		Corr5Alta1Corr
778		32			15			Transducer5 Low1 Curr Limit			Curr5 Lw1 Curr		Transducer5 Bajo 1. Límite Corr		Corr5Bajo1Corr
779		32			15			Transducer5 Low2 Curr Limit			Curr5 Lw2 Curr		Transducer5 Bajo 2. Límite Corr		Corr5Bajo2Corr
780		32			15			Transducer6 High2 Curr Limit		Curr6 Hi2 Curr		Transducer6 Alto 2. Límite Corr		Corr6Alta2Corr
781		32			15			Transducer6 High1 Curr Limit		Curr6 Hi1 Curr		Transducer6 Alto 1. Límite Corr		Corr6Alta1Corr
782		32			15			Transducer6 Low1 Curr Limit			Curr6 Lw1 Curr		Transducer6 Bajo 1. Límite Corr		Corr6Bajo1Corr
783		32			15			Transducer6 Low2 Curr Limit			Curr6 Lw2 Curr		Transducer6 Bajo 2. Límite Corr		Corr6Bajo2Corr
784		32			15			Transducer7 High2 Curr Limit		Curr7 Hi2 Curr		Transducer7 Alto 2. Límite Corr		Corr7Alta2Corr
785		32			15			Transducer7 High1 Curr Limit		Curr7 Hi1 Curr		Transducer7 Alto 1. Límite Corr		Corr7Alta1Corr
786		32			15			Transducer7 Low1 Curr Limit			Curr7 Lw1 Curr		Transducer7 Bajo 1. Límite Corr		Corr7Bajo1Corr
787		32			15			Transducer7 Low2 Curr Limit			Curr7 Lw2 Curr		Transducer7 Bajo 2. Límite Corr		Corr7Bajo2Corr
788		32			15			Transducer8 High2 Curr Limit		Curr8 Hi2 Curr		Transducer8 Alto 2. Límite Corr		Corr8Alta2Corr
789		32			15			Transducer8 High1 Curr Limit		Curr8 Hi1 Curr		Transducer8 Alto 1. Límite Corr		Corr8Alta1Corr
790		32			15			Transducer8 Low1 Curr Limit			Curr8 Lw1 Curr		Transducer8 Bajo 1. Límite Corr		Corr8Bajo1Corr
791		32			15			Transducer8 Low2 Curr Limit			Curr8 Lw2 Curr		Transducer8 Bajo 2. Límite Corr		Corr8Bajo2Corr
792		32			15			Transducer9 High2 Curr Limit		Curr9 Hi2 Curr		Transducer9 Alto 2. Límite Corr		Corr9Alta2Corr
793		32			15			Transducer9 High1 Curr Limit		Curr9 Hi1 Curr		Transducer9 Alto 1. Límite Corr		Corr9Alta1Corr
794		32			15			Transducer9 Low1 Curr Limit			Curr9 Lw1 Curr		Transducer9 Bajo 1. Límite Corr		Corr9Bajo1Corr
795		32			15			Transducer9 Low2 Curr Limit			Curr9 Lw2 Curr		Transducer9 Bajo 2. Límite Corr		Corr9Bajo2Corr
796		32			15			Transducer10 High2 Curr Limit		Curr10 Hi2 Curr		Transducer10 Alto 2. Límite Corr		Corr10Alta2Corr
797		32			15			Transducer10 High1 Curr Limit		Curr10 Hi1 Curr		Transducer10 Alto 1. Límite Corr		Corr10Alta1Corr
798		32			15			Transducer10 Low1 Curr Limit		Curr10 Lw1 Curr		Transducer10 Bajo 1. Límite Corr		Corr10Bajo1Corr
799		32			15			Transducer10 Low2 Curr Limit		Curr10 Lw2 Curr		Transducer10 Bajo 2. Límite Corr		Corr10Bajo2Corr

800		32			15			TranCurr1 Low1 Curr Alarm			TranCurr1 Lw1 		TransCorr1Baja1Corr1Alm		TranCorr1Baja1	
801		32			15			TranCurr1 Low2 Curr Alarm			TranCurr1 Lw2 		TransCorr1Baja2Corr1Alm		TranCorr1Baja2
802		32			15			TranCurr1 High1 Curr Alarm			TranCurr1 Hi1 		TransCorr1Alto1Corr1Alm		TranCorr1Alto1
803		32			15			TranCurr1 High2 Curr Alarm			TranCurr1 Hi2 		TransCorr1Alto2Corr1Alm		TranCorr1Alto2	
804		32			15			TranCurr2 Low1 Curr Alarm			TranCurr2 Lw1 		TransCorr2Baja1Corr1Alm		TranCorr2Baja1	
805		32			15			TranCurr2 Low2 Curr Alarm			TranCurr2 Lw2 		TransCorr2Baja2Corr1Alm		TranCorr2Baja2
806		32			15			TranCurr2 High1 Curr Alarm			TranCurr2 Hi1 		TransCorr2Alto1Corr1Alm		TranCorr2Alto1
807		32			15			TranCurr2 High2 Curr Alarm			TranCurr2 Hi2 		TransCorr2Alto2Corr1Alm		TranCorr2Alto2	
808		32			15			TranCurr3 Low1 Curr Alarm			TranCurr3 Lw1 		TransCorr3Baja1Corr1Alm		TranCorr3Baja1
809		32			15			TranCurr3 Low2 Curr Alarm			TranCurr3 Lw2 		TransCorr3Baja2Corr1Alm		TranCorr3Baja2
810		32			15			TranCurr3 High1 Curr Alarm			TranCurr3 Hi1 		TransCorr3Alto1Corr1Alm		TranCorr3Alto1
811		32			15			TranCurr3 High2 Curr Alarm			TranCurr3 Hi2 		TransCorr3Alto2Corr1Alm		TranCorr3Alto2
812		32			15			TranCurr4 Low1 Curr Alarm			TranCurr4 Lw1 		TransCorr4Baja1Corr1Alm		TranCorr4Baja1
813		32			15			TranCurr4 Low2 Curr Alarm			TranCurr4 Lw2 		TransCorr4Baja2Corr1Alm		TranCorr4Baja2
814		32			15			TranCurr4 High1 Curr Alarm			TranCurr4 Hi1 		TransCorr4Alto1Corr1Alm		TranCorr4Alto1
815		32			15			TranCurr4 High2 Curr Alarm			TranCurr4 Hi2 		TransCorr4Alto2Corr1Alm		TranCorr4Alto2
816		32			15			TranCurr5 Low1 Curr Alarm			TranCurr5 Lw1 		TransCorr5Baja1Corr1Alm		TranCorr5Baja1
817		32			15			TranCurr5 Low2 Curr Alarm			TranCurr5 Lw2 		TransCorr5Baja2Corr1Alm		TranCorr5Baja2
818		32			15			TranCurr5 High1 Curr Alarm			TranCurr5 Hi1 		TransCorr5Alto1Corr1Alm		TranCorr5Alto1
819		32			15			TranCurr5 High2 Curr Alarm			TranCurr5 Hi2 		TransCorr5Alto2Corr1Alm		TranCorr5Alto2
820		32			15			TranCurr6 Low1 Curr Alarm			TranCurr6 Lw1 		TransCorr6Baja1Corr1Alm		TranCorr6Baja1
821		32			15			TranCurr6 Low2 Curr Alarm			TranCurr6 Lw2 		TransCorr6Baja2Corr1Alm		TranCorr6Baja2
822		32			15			TranCurr6 High1 Curr Alarm			TranCurr6 Hi1 		TransCorr6Alto1Corr1Alm		TranCorr6Alto1
823		32			15			TranCurr6 High2 Curr Alarm			TranCurr6 Hi2 		TransCorr6Alto2Corr1Alm		TranCorr6Alto2
824		32			15			TranCurr7 Low1 Curr Alarm			TranCurr7 Lw1 		TransCorr7Baja1Corr1Alm		TranCorr7Baja1
825		32			15			TranCurr7 Low2 Curr Alarm			TranCurr7 Lw2 		TransCorr7Baja2Corr1Alm		TranCorr7Baja2
826		32			15			TranCurr7 High1 Curr Alarm			TranCurr7 Hi1 		TransCorr7Alto1Corr1Alm		TranCorr7Alto1
827		32			15			TranCurr7 High2 Curr Alarm			TranCurr7 Hi2 		TransCorr7Alto2Corr1Alm		TranCorr7Alto2
828		32			15			TranCurr8 Low1 Curr Alarm			TranCurr8 Lw1 		TransCorr8Baja1Corr1Alm		TranCorr8Baja1
829		32			15			TranCurr8 Low2 Curr Alarm			TranCurr8 Lw2 		TransCorr8Baja2Corr1Alm		TranCorr8Baja2
830		32			15			TranCurr8 High1 Curr Alarm			TranCurr8 Hi1 		TransCorr8Alto1Corr1Alm		TranCorr8Alto1
831		32			15			TranCurr8 High2 Curr Alarm			TranCurr8 Hi2 		TransCorr8Alto2Corr1Alm		TranCorr8Alto2
832		32			15			TranCurr9 Low1 Curr Alarm			TranCurr9 Lw1 		TransCorr9Baja1Corr1Alm		TranCorr9Baja1	
833		32			15			TranCurr9 Low2 Curr Alarm			TranCurr9 Lw2 		TransCorr9Baja2Corr1Alm		TranCorr9Baja2
834		32			15			TranCurr9 High1 Curr Alarm			TranCurr9 Hi1 		TransCorr9Alto1Corr1Alm		TranCorr9Alto1
835		32			15			TranCurr9 High2 Curr Alarm			TranCurr9 Hi2 		TransCorr9Alto2Corr1Alm		TranCorr9Alto2	
836		32			15			TranCurr10 Low1 Curr Alarm			TranCurr10 Lw1		TransCorr10Baja1Corr1Alm		TranCorr10Baja1
837		32			15			TranCurr10 Low2 Curr Alarm			TranCurr10 Lw2		TransCorr10Baja2Corr1Alm		TranCorr10Baja2
838		32			15			TranCurr10 High1 Curr Alarm			TranCurr10 Hi1		TransCorr10Alto1Corr1Alm		TranCorr10Alto1
839		32			15			TranCurr10 High2 Curr Alarm			TranCurr10 Hi2		TransCorr10Alto2Corr1Alm		TranCorr10Alto2

840		32			15			Transducer1 Units		Trans1 Units		Transductor 1 Unidades			Trans1 Unid	
841		32			15			Transducer2 Units		Trans2 Units		Transductor 2 Unidades			Trans2 Unid
842		32			15			Transducer3 Units		Trans3 Units		Transductor 3 Unidades			Trans3 Unid
843		32			15			Transducer4 Units		Trans4 Units		Transductor 4 Unidades			Trans4 Unid
844		32			15			Transducer5 Units		Trans5 Units		Transductor 5 Unidades			Trans5 Unid
845		32			15			Transducer6 Units		Trans6 Units		Transductor 6 Unidades			Trans6 Unid	
846		32			15			Transducer7 Units		Trans7 Units		Transductor 7 Unidades			Trans7 Unid
847		32			15			Transducer8 Units		Trans8 Units		Transductor 8 Unidades			Trans8 Unid
848		32			15			Transducer9 Units		Trans9 Units		Transductor 9 Unidades			Trans9 Unid
849		32			15			Transducer10 Units		Trans10 Units		Transductor 10 Unidades			Trans10 Unid

850		32			15			None			None			Ninguno			Ninguno
851		32			15			Volts DC		Volts DC		Voltaje DC		Voltaje DC	
852		32			15			Volts AC		Volts AC		Voltaje AC		Voltaje AC	
853		32			15			Amps DC			Amps DC			Amps DC			Amps DC	
854		32			15			Amps AC			Amps AC			Amps AC			Amps AC	
855		32			15			Watts			Watts			Watts			Watts		
856		32			15			KW				KW				KW				KW			
857		32			15			Gallons			Gallons			Galones			Galones	
858		32			15			Liters			Liters			Litros			Litros	
859		32			15			PSI				PSI				PSI			PSI	
860		32			15			CFM				CFM				CFM			CFM		
861		32			15			RPM				RPM				RPM			RPM	
862		32			15			BTU				BTU				BTU			BTU
863		32			15			HP				HP				HP			HP	
864		32			15			W/hr			W/hr			W/hr		W/hr	
865		32			15			KW/hr			KW/hr			KW/hr		KW/hr
866		32			15			None			None			None		None

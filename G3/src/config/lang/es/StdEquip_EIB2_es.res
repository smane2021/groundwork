﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			EIB-2			EIB-2			EIB-2			EIB-2
9		32			15			Bad Battery Block	Bad Batt Block		Elemento Bat mal	Elemento mal
10		32			15			Load 1				Load 1			Carga 1		Carga 1
11		32			15			Load 2				Load 2			Carga 2		Carga 2
12		32			15			DO1-Relay Output		DO1-RelayOutput		Salida de relé DO1		SalidaReléDO1
13		32			15			DO2-Relay Output		DO2-RelayOutput		Salida de relé DO2		SalidaReléDO2
14		32			15			DO3-Relay Output		DO3-RelayOutput		Salida de relé DO3		SalidaReléDO3
15		32			15			DO4-Relay Output		DO4-RelayOutput		Salida de relé DO4		SalidaReléDO4
16		32			15			EIB Communication Fail		EIB Comm Fail		Fallo EIB		Fallo EIB
17		32			15			State			State			Estado			Estado
18		32			15			Shunt 2 Full Current	Shunt 2 Current		Corriente Shunt 2	Corr Shunt 2
19		32			15			Shunt 3 Full Current	Shunt 3 Current		Corriente Shunt 3	Corr Shunt 3
20		32			15			Shunt 2 Full Voltage	Shunt 2 Voltage		Tensión Shunt 3		Tens Shunt 2
21		32			15			Shunt 3 Full Voltage	Shunt 3 Voltage		Tensión Shunt 3		Tens Shunt 3
22		32			15			Load 1			Load 1		Carga 1			Carga 1
23		32			15			Load 2			Load 2		Carga 2			Carga 2
24		32			15			Enabled			Enabled			Habilitar		Habilitar
25		32			15			Disabled			Disabled			Deshabilitar		Deshabilitar
26		32			15			Active				Active		Activo			Activo
27		32			15			Not Active			Not Active			No Activo		No Activo
28		32			15			State			State			Estado			Estado
29		32			15			No			No			No			No
30		32			15			Yes			Yes			Sí		Sí
31		32			15			EIB Communication Fail		EIB Comm Fail		Fallo EIB		Fallo EIB
32		32			15			Voltage 1		Voltage 1		Tensión 1		Tensión 1
33		32			15			Voltage 2		Voltage 2		Tensión 2		Tensión 2
34		32			15			Voltage 3		Voltage 3		Tensión 3		Tensión 3
35		32			15			Voltage 4		Voltage 4		Tensión 4		Tensión 4
36		32			15			Voltage 5		Voltage 5		Tensión 5		Tensión 5
37		32			15			Voltage 6		Voltage 6		Tensión 6		Tensión 6
38		32			15			Voltage 7		Voltage 7		Tensión 7		Tensión 7
39		32			15			Voltage 8		Voltage 8		Tensión 8		Tensión 8
40		32			15			Number of Battery Shunts	Num Batt Shunts			Núm de batería		Num Bat
41		32			15			0			0			0			0
42		32			15			1			1			1			1
43		32			15			2			2			2			2
44		32			15			Load 3			Load 3		Carga 3		Carga 3
45		32			15			3			3			3			3
46		32			15			Number of Load Shunts		Num Load Shunts			Número de carga		Núm Carga
47		32			15			Shunt 1 Full Current		Shunt 1 Current			Corriente Shunt 1	Corr Shunt 1
48		32			15			Shunt 1 Full Voltage		Shunt 1 Voltage		Tensión Shunt 1		Tens Shunt 1
49		32			15			Voltage Type		Voltage Type		Tipo de Tensión		Tipo Tensión
50		32			15			48 (Block4)			48 (Block4)	48(Elem4)		48(Elme4)
51		32			15			Mid Point			Mid Point		Punto medio		Punto medio
52		32			15			24 (Block2)			24 (Block2)		24(Elem2)		24(Elem2)
53		32			15			Block Voltage Diff (12V)	Blk V Diff(12V)			Dif Tensión Celda(12V)	Dif Vcel(12V)
54		32			15			DO5-Relay Output		DO5-RelayOutput		Salida de relé DO5		SalidaReléDO5
55		32			15			Block Voltage Diff (Mid)	Blk V Diff(Mid)		Dif Tensión Celda(Mid)	Dif Vcel(Mid)
56		32			15			Block In-Use Num		Block In-Use	Número de celdas	Núm Celdas
78		32			15			EIB-2 DO1 Test			EIB-2 DO1 Test		Prueba de DO1		Prueba de DO1
79		32			15			EIB-2 DO2 Test			EIB-2 DO2 Test		Prueba de DO2		Prueba de DO2
80		32			15			EIB-2 DO3 Test			EIB-2 DO3 Test		Prueba de DO3		Prueba de DO3
81		32			15			EIB-2 DO4 Test			EIB-2 DO4 Test		Prueba de DO4		Prueba de DO4
82		32			15			EIB-2 DO5 Test			EIB-2 DO5 Test		Prueba de DO5		Prueba de DO5
83		32			15			Not Used				Not Used				No Utilizada				No Utilizada
84		32			15			General				General				General	General
85		32			15			Load						Load						Carga					Carga
86		32			15			Battery					Battery					Batería				Batería
87		32			15			Shunt1 Set As			Shunt1SetAs			Shunt1 fijado como			Shunt1 como
88		32			15			Shunt2 Set As			Shunt2SetAs			Shunt2 fijado como			Shunt2 como
89		32			15			Shunt3 Set As			Shunt3SetAs			Shunt3 fijado como			Shunt3 como	
90		32			15			Shunt 1		Shunt 1		Lectura Shunt1				Lectura Shunt1
91		32			15			Shunt 2		Shunt 2		Lectura Shunt2				Lectura Shunt2
92		32			15			Shunt 3		Shunt 3		Lectura Shunt3				Lectura Shunt3
93		32			15			Temperature1				Temp1			Temp1			Temp1
94		32			15			Temperature2				Temp2			Temp2			Temp2
95		32			15			Source 1		Source 1		Fuente1				Fuente1
96		32			15			Source 2		Source 2		Fuente2				Fuente2
97		32			15			Source 3		Source 3		Fuente3				Fuente3
500		32			15			Current1 Break Size		Curr1 Brk Size		Tamaño Corr1 rotura			TamCorr1Rotura	
501		32			15			Current1 High 1 Current Limit	Curr1 Hi1 Lmt		Alta1 corr.1 Límite de corr		Alta1 Corr1Lmt	
502		32			15			Current1 High 2 Current Limit	Curr1 Hi2 Lmt		Alta2 corr.1 Límite de corr		Alta2 Corr1Lmt	
503		32			15			Current2 Break Size		Curr2 Brk Size		Tamaño Corr2 rotura			TamCorr2Rotura	
504		32			15			Current2 High 1 Current Limit	Curr2 Hi1 Lmt		Alta1 corr.2 Límite de corr		Alta1 Corr2Lmt	
505		32			15			Current2 High 2 Current Limit	Curr2 Hi2 Lmt		Alta2 corr.2 Límite de corr		Alta2 Corr2Lmt	
506		32			15			Current3 Break Size		Curr3 Brk Size		Tamaño Corr3 rotura			TamCorr3Rotura	
507		32			15			Current3 High 1 Current Limit	Curr3 Hi1 Lmt		Alta1 corr.3 Límite de corr		Alta1 Corr3Lmt	
508		32			15			Current3 High 2 Current Limit	Curr3 Hi2 Lmt		Alta2 corr.3 Límite de corr		Alta2 Corr3Lmt	
509		32			15			Current1 High 1 Current		Curr1 Hi1Cur		Alta1 corriente 1 corriente		Alta1 Corr1	
510		32			15			Current1 High 2 Current		Curr1 Hi2Cur		Alta2 corriente 1 corriente		Alta2 Corr1	
511		32			15			Current2 High 1 Current		Curr2 Hi1Cur		Alta1 corriente 2 corriente		Alta1 Corr2	
512		32			15			Current2 High 2 Current		Curr2 Hi2Cur		Alta2 corriente 2 corriente		Alta2 Corr2	
513		32			15			Current3 High 1 Current		Curr3 Hi1Cur		Alta1 corriente 3 corriente		Alta1 Corr3	
514		32			15			Current3 High 2 Current		Curr3 Hi2Cur		Alta2 corriente 3 corriente		Alta2 Corr3	

550		32			15			Source			Source			Fuente					Fuente


﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Distribution Fuse Tripped		Dist Fuse Trip		Fusible Distribución disparado		Fus Distr Disp
2		32			15			Contactor 1 Failure		Contactor1 Fail		Fallo Contactor 1			Fallo Contact1
3		32			15			Distribution Fuse 2 Tripped		Dist Fuse2 Trip		Fusible Distribución 2 disparado	Fus2 Distr Disp
4		32			15			Contactor 2 Failure		Contactor2 Fail		Fallo Contactor 2			Fallo Contact2
5		32			15			Distribution Voltage		Distr Voltage		Tensión Distribución			Tensión Distr
6		32			15			Distribution Current		Distr Current		Corriente Distribución			Corriente Dist
7		32			15			Distribution Temperature		Distr Temp		Temperatura Distribución	Temperatura
8		32			15			Distribution 2 Current		Distr 2 Current			Corriente Distribución 2		Corriente Dist2
9		32			15			Distribution 2 Voltage			Distr 2 Voltage		Tensión Distribución 2			Tensión Distr2
10		32			15			CSU DC Distribution			CSU DC Distr		Distribución CC CSU			CSU Distr CC
11		32			15			CSU DC Distribution Failure		CSU DCDist Fail			Fallo Distribución CSU			Fallo Dist CSU
12		32			15			No					No			No					No
13		32			15			Yes					Yes		Sí					Sí
14		32			15			Existent				Existent		Existente				Existente
15		32			15			Not Existent				Not Existent		Inexistente				Inexistente

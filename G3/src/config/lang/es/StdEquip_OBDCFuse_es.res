﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es



[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1		32			15			Fuse 1			Fuse 1			Fusible 1		Fusible 1
2		32			15			Fuse 2			Fuse 2			Fusible 2		Fusible 2
3		32			15			Fuse 3			Fuse 3			Fusible 3		Fusible 3
4		32			15			Fuse 4			Fuse 4			Fusible 4		Fusible 4
5		32			15			Fuse 5			Fuse 5			Fusible 5		Fusible 5
6		32			15			Fuse 6			Fuse 6			Fusible 6		Fusible 6
7		32			15			Fuse 7			Fuse 7			Fusible 7		Fusible 7
8		32			15			Fuse 8			Fuse 8			Fusible 8		Fusible 8
9		32			15			Fuse 9			Fuse 9			Fusible 9		Fusible 9
10		32			15			Auxiliary Load Fuse		Aux Load Fuse		Carga auxiliar		Carga Aux
11		32			15			Fuse 1 Alarm		Fuse 1 Alarm		Alarma Fusible 1	Alarma Fus1
12		32			15			Fuse 2 Alarm		Fuse 2 Alarm		Alarma Fusible 2	Alarma Fus2
13		32			15			Fuse 3 Alarm		Fuse 3 Alarm		Alarma Fusible 3	Alarma Fus3
14		32			15		Fuse 4 Alarm			Fuse 4 Alarm		Alarma Fusible 4	Alarma Fus4
15		32			15		Fuse 5 Alarm			Fuse 5 Alarm		Alarma Fusible 5	Alarma Fus5
16		32			15		Fuse 6 Alarm			Fuse 6 Alarm		Alarma Fusible 6	Alarma Fus6
17		32			15		Fuse 7 Alarm			Fuse 7 Alarm		Alarma Fusible 7	Alarma Fus7
18		32			15		Fuse 8 Alarm			Fuse 8 Alarm		Alarma Fusible 8	Alarma Fus8
19		32			15		Fuse 9 Alarm			Fuse 9 Alarm		Alarma Fusible 9	Alarma Fus9
20		32			15			Auxiliary Load Fuse Alarm	AuxLoadFuseAlm		Alarma carga auxiliar	Alar Carga Aux
21		32			15			On			On			Conectado		Conectado
22		32			15			Off			Off			Desconectado		Desconectado
23		32			15			DC Fuse Unit		DC Fuse Unit		Distrib Principal	Distr Ppal
24		32			15			Fuse 1 Voltage		Fuse 1 Voltage		Tensión fusible 1	Tensión fus1
25		32			15			Fuse 2 Voltage		Fuse 2 Voltage		Tensión fusible 2	Tensión fus2
26		32			15			Fuse 3 Voltage		Fuse 3 Voltage		Tensión fusible 3	Tensión fus3
27		32			15			Fuse 4 Voltage		Fuse 4 Voltage		Tensión fusible 4	Tensión fus4
28		32			15			Fuse 5 Voltage		Fuse 5 Voltage		Tensión fusible 5	Tensión fus5
29		32			15			Fuse 6 Voltage		Fuse 6 Voltage		Tensión fusible 6	Tensión fus6
30		32			15			Fuse 7 Voltage		Fuse 7 Voltage		Tensión fusible 7	Tensión fus7
31		32			15			Fuse 8 Voltage		Fuse 8 Voltage		Tensión fusible 8	Tensión fus8
32		32			15			Fuse 9 Voltage		Fuse 9 Voltage		Tensión fusible 9	Tensión fus9
33		32			15			Fuse 10 Voltage		Fuse 10 Voltage		Tensión fusible 10	Tensión fus10
34		32			15			Fuse 10			Fuse 10			Fusible 10		Fusible 10
35		32			15			Fuse 10 Alarm		Fuse 10 Alarm		Alarma Fusible 10	Alarma Fus10
36		32			15			State			State			Estado			Estado
37		32			15			Communication Fail		Comm Fail		Fallo Comunicación		Fallo COM
38		32			15			No			No			No			No
39		32			15			Yes			Yes			Sí		Sí

40	32			15		Fuse 11				Fuse 11			Fusible 11			Fusible 11
41	32			15		Fuse 12				Fuse 12			Fusible 12			Fusible 12
42	32			15		Fuse 11 Alarm			Fuse 11 Alarm		Alarma Fusible 11		Alarma Fusi 11
43	32			15		Fuse 12 Alarm			Fuse 12 Alarm		Alarma Fusible 11		Alarma Fusi 11

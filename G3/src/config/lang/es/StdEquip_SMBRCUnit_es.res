﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
51	32			15			SMBRC Unit				SMBRC Unit	Unidad SMBRC				Unidad SMBRC
95	32			15			Times of Communication Fail		Times Comm Fail		Fallos de Comunicación			Interrupciones
96	32			15			Existent			Existent		Existente				Existente
97	32			15			Not Existent		Not Existent		Inexistente				Inexistente
117	32			15			Existence State			Existence State		Detección				Detección
118	32			15			Communication Fail			Comm Fail		Fallo de Comunicación		Fallo COM
120	32			15			Communication OK		Comm OK			Comunicación OK			Comunicación OK
121	32			15			All Communication Fail			All Comm Fail		Ninguno responde			Sin respuesta
122	32			15			Communication Fail			Comm Fail		Fallo de Comunicación				Fallo COM
123	32			15			Rated Capacity			Rated Capacity		Capacidad estimada			Capacidad est
150	32			15			Digital Input Number			Dig Input Num		Núm Digitales de Entrada (DI)		Núm DI
151	32			15			Digital Input 1				Digital Input 1		DI 1					DI 1
152	32			15			Low				Low			Bajo					Bajo
153	32			15			High				High			Alto					Alto
154	32			15			Digital Input 2				Digital Input 2		DI 2					DI 2
155	32			15			Digital Input 3				Digital Input 3		DI 3					DI 3
156	32			15			Digital Input 4				Digital Input 4			DI 4					DI 4
157	32			15			Num of Digital Out			Num of DO		Núm Digitales de Salida (DO)		Núm DO
158	32			15			Digital Output 1			Digital Out 1		DO 1					DO 1
159	32			15			Digital Output 2			Digital Out 2		DO 2					DO 2
160	32			15			Digital Output 3			Digital Out 3		DO 3					DO 3
161	32			15			Digital Output 4			Digital Out 4		DO 4					DO 4
162	32			15			Operation Status			Operation State		Estado Operación			Estado Opera
163	32			15			Normal				Normal			Normal					Normal
164	32			15			Test				Test			Prueba					Prueba
165	32			15			Discharge			Discharge		Descarga				Descarga
166	32			15			Calibration			Calibration		Calibración				Calibración
167	32			15			Diagnostic			Diagnostic		Diagnóstico				Diagnóstico
168	32			15			Maintenance			Maintenance		Mantenimiento				Mantenimiento
169	32			15			Internal Resistance TestInterval	InterR Test Int		Intervalo Prueba de Resistencia		Int Prueba Res
170	32			15			Ambient Temperature Value	Amb Temp Value		Temperatura Ambiente			Temp Ambiente
171	32			15			Ambient High Temperature		Amb Temp High		Alta Temperatura Ambiente		Alta Temp Amb
172	32			15			Battery String Config Number		String Cfg Num			Núm de configuración			Núm Config
173	32			15			Exist Batt Num				Exist Batt Num		Número de Baterías			Núm Baterías
174	32			15			Unit Seq Num				Unit Seq Num		Número de Secuencia Unidad		Núm Secuencia
175	32			15			Start Battery Sequence			Start Batt Seq			Iniciar Secuencia de Batería		Inic SecuenBat
176	32			15			Ambient Low Temperature			Amb Temp Low		Baja temperatura ambiente		Baja Temp Amb
177	32			15			Ambt Temp Not Used			Amb Temp No Use		Fallo sensor Temperatura Amb	Fallo Sens Amb

﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			Battery Fuse Tripped			Batt Fuse Trip		Fusible Bat disparado	FusBat disp
2		32			15			Battery 2 Fuse Tripped		Bat 2 Fuse Trip			Fusible Batería 2 disparado	FusBat2 disp
3		32			15			Battery 1 Voltage			Batt 1 Voltage		Tensión Batería 1		Tensión Bat1
4		32			15			Battery 1 Current			Batt 1 Current		Corriente Batería 1		Corriente Bat1
5		32			15			Battery 1 Temperature		Battery 1 Temp	Temperatura Batería 1		Temp Bat1
6		32			15			Battery 2 Voltage			Batt 2 Voltage		Tensión Batería 2		Tensión Bat2
7		32			15			Battery 2 Current			Batt 2 Current		Corriente Batería 2		Corriente Bat2
8		32			15			Battery 2 Temperature		Battery 2 Temp	Temperatura Batería 2		Temp Bat2
9		32			15			CSU Battery				CSU Battery		Batería CSU			Batería CSU
10		32			15			CSU Battery Failure		CSU BatteryFail	Fallo Batería CSU		Fallo Bat CSU
11		32			15			No					No			No				No
12		32			15			Yes					Yes			Sí				Sí
13		32			15			Battery 2 Connected		Bat 2 Connected		Batería 2 conectada		Bat2 conectada
14		32			15			Battery 1 Connected		Bat 1 Connected			Batería 1 conectada		Bat1 conectada
15		32			15			Existent				Existent		Existente			Existente
16		32			15			Not Existent			Not Existent		Inexistente			Inexistente

﻿
#Language resources file

[LOCAL_LANGUAGE]
es

[LOCAL_LANGUAGE_VERSION]
1.00

[NUM_OF_PAGES]
39

[login.html:Number]
50

[login.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIP1					64			You are requesting access		Acceso a
2		ID_TIP2					32			located at				localizado en
3		ID_TIP3					128			The user name and password for this device is set by the system administrator.		El usuario y contraseña para este dispositivo es fijado por el Administrador.
4		ID_LOGIN				32			Login				Conectar
5		ID_USER					32			User				Usuario
6		ID_PASSWD				32			Password				Contraseña
7		ID_LOCAL_LANGUAGE			32			Español				Español
8		ID_SITE_NAME				32			Site Name				Nombre Central
9		ID_SYSTEM_NAME				32			System Name				Nombre Sistema
10		ID_PRODUCT_MODEL			32			Product Model			Modelo
11		ID_SERIAL				32			Serial Number			Número de Serie
12		ID_HW_VERSION				32			Hardware Version			Versión HW
13		ID_SW_VERSION				32			Software Version			Versión SW
14		ID_CONFIG_VERSION			32			Config Version			Versión Config
15		ID_FORGET_PASSWD			32			Forgot Your Password?			Olvido contraseña?
16		ID_ERROR0				64			Unknown Error										Error Desconocido
17		ID_ERROR1				64			Successful											Correcto
18		ID_ERROR2				128			Wrong Credential or Radius Server is not reachable.	Credencial incorrecta o Radius Server no es accesible.
19		ID_ERROR3				128			Wrong Credential or Radius Server is not reachable.	Credencial incorrecta o Radius Server no es accesible.
20		ID_ERROR4				64			Failed to communicate with the application.							Fallo al comunicar con la aplicación
21		ID_ERROR5				64			Over 5 connections, please retry later.							Más de 5 conexiones. Inténtelo más tarde.
22		ID_ERROR6				128			Controller is starting, please retry later.							Iniciando, pruebe más tarde.
23		ID_ERROR7				256			Automatic configuration in progress, please wait about 2-5 minutes.				En Autoconfiguración. Espere de 2 a 5 minutos.
24		ID_ERROR8				64			Controller in Secondary Mode.								NCU en modo secundario.
25		ID_ERROR9				256			Monitoring has restarted and the password needs to be initialized.						La supervisión se ha reiniciado y la contraseña debe inicializarse.
25		ID_LOGIN1				32			LOGIN				Conectar
26		ID_SELECT				32			Please select language				Por favor, seleccione idioma
27		ID_TIP4					32			Loading...				Cargando...
28		ID_TIP5					256			Sorry, your browser doesn't support cookies or cookies are disabled. Please enable cookie or change your browser and login again.				Su navegador no es soportado o la aceptación de cookies está desactivada. Por favor, solucione el problema y vuelva a conectarse.
29		ID_TIP6					128			Data lost.<a href='login.html' style='margin:0 10px;'>Retry</a>				Datos perdidos.<a href='login.html' style='margin:0 10px;'>Retry</a>
30		ID_TIP7					64			Controller is starting, please wait...							La NCU se está iniciando. Espere...
31		ID_TIP8					32			Logging In ...			Conectando...
32		ID_TIP9					32			LOGIN				Conectar
49		ID_FORGET_PASSWD			32			Forgot Password?						Ha olvidado la contraseña?
50		ID_TIP10				32			Loading, please wait...					Cargando, Espere…
51		ID_TIP11				64			Controller is in Secondary Mode.				La NCU está en modo secundario
52		ID_LOGIN_TITLE				32			Login-Vertiv G3						Conectarse-Vertiv G3 
53		ID_ALARMSOUNDOFF				64			Touch Screen To Silence Alm			Toque la pantalla para silenciar la alarma
54		ID_PLANTLOAD				32			Plant Load			Carga de la planta
55		ID_PLANTVOLTAGE				32			Plant Voltage			Voltaje de la planta
56		ID_ALARMS				32			Alarms			Alarmas
57		ID_OBSERVATION				32			Observation			Observación
58		ID_MAJOR				32			Major			Urgente
59		ID_CRITICAL				32			Critical			Crítica
60		ID_ERROR10				128			Wrong Credential or Radius Server is not reachable.	Wrong Credential or Radius Server is not reachable.
61		ID_ERROR11				128			Your account has been locked. Contact your local administrator for assistance.	Your account has been locked. Contact your local administrator for assistance.
62		ID_ERROR12				128			Your account has been temporarily locked for next 30 minutes.	Your account has been temporarily locked for next 30 minutes.

[index.html:Number]
147

[index.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	STRING				LOCAL
1		ID_ALL_ALARMS				32			All Alarms				Todas las Alarmas
2		ID_OBSERVATION				32			Observation				Observación
3		ID_MAJOR				32			Major				Urgente
4		ID_CRITICAL				32			Critical				Crítica
5		ID_CONTROLLER_SPECIFICATIONS		32			Controller Specifications		Especificaciones NCU
6		ID_SYSTEM_NAME				32			System Name				Nombre Sistema
7		ID_RECTIFIERS_NUMBER			32			Rectifiers				Rectificadores
8		ID_CONVERTERS_NUMBER			32			Converters				Convertidores
9		ID_SYSTEM_SPECIFICATIONS		32			System Specifications		Especificaciones Sistema
10		ID_PRODUCT_MODEL			32			Product Model			Modelo
11		ID_SERIAL_NUMBER			32			Serial Number			Número de Serie	
12		ID_HARDWARE_VERSION			32			Hardware Version			Versión HW
13		ID_SOFTWARE_VERSION			32			Software Version			Versión SW
14		ID_CONFIGURATION_VERSION		32			Config Version			Versión Config
15		ID_HOME					32			Home				Inicio
16		ID_SETTINGS				32			Settings				Ajustes
17		ID_HISTORY_LOG				32			History Log				Histórico
18		ID_SYSTEM_INVENTORY			32			System Inventory			Inventario Sistema
19		ID_ADVANCED_SETTING			32			Advanced Settings			Ajustes Avanzados
20		ID_INDEX				32			Index				Índice
21		ID_ALARM_LEVEL				32			Alarm Level				Nivel Alarma
22		ID_RELATIVE				32			Relative Device			Equipo
23		ID_SIGNAL_NAME				32			Signal Name				Nombre de Señal
24		ID_SAMPLE_TIME				32			Sample Time				Inicio
25		ID_WELCOME				32			Welcome				Bienvenido
26		ID_LOGOUT				32			SIGN OUT				SALIR
27		ID_AUTO_POPUP				32			Auto Popup				Autoemergente
28		ID_COPYRIGHT				64			2018 Vertiv Tech Co.,Ltd.All rights reserved.		2018 Vertiv Tech Co.,Ltd.Todos los derechos reservados.
29		ID_OA					32			Observation				Observación
30		ID_MA					32			Major				Urgente
31		ID_CA					32			Critical				Crítica
32		ID_HOME1				32			Home				Inicio
33		ID_ERROR0				32			Failed.				Fallo
34		ID_ERROR1				32			Successful.				Correcto
35		ID_ERROR2				32			Failed. Conflicting setting.	Fallo: Conflicto configuración.
36		ID_ERROR3				32			Failed. No privileges.		Fallo: Sin autoridad
37		ID_ERROR4				32			No information to send.		No hay información para enviar
38		ID_ERROR5				64			Failed. Controller is hardware protected.		Fallo. NCU protegida por HW.
39		ID_ERROR6				64			Time expired. Please login again.				El tiempo ha expirado. Vuelva a conectarse.
40		ID_HIS_ERROR0				32			Unknown error.				Error desconocido.		
41		ID_HIS_ERROR1				32			Successful.					Correcto			
42		ID_HIS_ERROR2				32			No data.					No hay datos	
43		ID_HIS_ERROR3				32			Failed					Fallo				
44		ID_HIS_ERROR4				32			Failed. No privileges.			Fallo. Sin autoridad			
45		ID_HIS_ERROR5				64			Failed to communicate with the controller.	Fallo al comunicar con la NCU
46		ID_HIS_ERROR6				64			Clear successful.				Borrado con éxito		
47		ID_HIS_ERROR7				64			Time expired. Please login again.		El tiempo ha expirado. Vuelva a conectarse.
48		ID_WIZARD				32			Install Wizard				Asistente de instalación
49		ID_SITE					16			Site					Central
50		ID_PEAK_CURR				32			Peak Current			Corriente Pico
51		ID_INDEX_TIPS1				32			Loading, please wait.			Cargando, Espere…
52		ID_INDEX_TIPS2				128			Data format error, getting the data again...please wait.			Error formato datos. Obteniendo datos de nuevo, espere…
53		ID_INDEX_TIPS3				128			Fail to load the data, please check the network and data file.		Fallo al cargar datos. Compruebe el acceso a red y el archivo de datos.
54		ID_INDEX_TIPS4				128			Template webpage not exist or network error.			La página web plantilla no existe o error conectividad red.
55		ID_INDEX_TIPS5				64			Data lost.			Datos perdidos.
56		ID_INDEX_TIPS6				64			Setting...please wait.			Ajustando, espere…
57		ID_INDEX_TIPS7				64			Confirm logout?			Desea desconectar?
58		ID_INDEX_TIPS8				64			Loading data, please wait.			Cargando, Espere…
59		ID_INDEX_TIPS9				128			Please shut down the browser to return to the home page.		Por favor, cierre el navegador para regresar a la página de inicio.
60		ID_INDEX_TIPS10				16			OK				OK
61		ID_INDEX_TIPS11				16			Error:		Error:
62		ID_INDEX_TIPS12				16			Retry			Reintentar
63		ID_INDEX_TIPS13				16			Loading...			Cargando…
64		ID_INDEX_TIPS14				64			Time expired. Please login again.			El tiempo ha expirado. Vuelva a conectarse.
65		ID_INDEX_TIPS15				32			Re-loading			Re-cargando…
66		ID_INDEX_TIPS16				128			File does not exist or unable to load the file due to a network error.			No se encuentra el archivo o es imposible cargarlo debido a un problema con la red.
67		ID_INDEX_TIPS17				32			id is \"			i La ID es\"
68		ID_INDEX_TIPS18				32			Request error.			Error
69		ID_INDEX_TIPS19				32			Login again.			Conecte otra vez
70		ID_INDEX_TIPS20				32			No Data			No hay datos
71		ID_INDEX_TIPS21				128			Can't be empty, please enter 0 or a positive number within range.		No puede ser nulo. Entre 0 o un número positivo dentro de rango.
72		ID_INDEX_TIPS22				128			Can't be empty, please enter 0 or an integer number or a float number within range.		No puede ser nulo.Entre 0, un entero o número decimal dentro de rango.
73		ID_INDEX_TIPS23				128			Can't be empty, please enter 0 or a positive number or a negative number within range.		No puede ser nulo.Entre 0, o cualquier número positivo o negativo dentro de rango.
74		ID_INDEX_TIPS24				128			No change to the current value.			Valor actual sin cambio.
75		ID_INDEX_TIPS25				128			Please input an email address in the form name@domain.			Entre una dirección de e-mail con formato: nombre@dominio.
76		ID_INDEX_TIPS26				128			Please enter an IP address in the form nnn.nnn.nnn.nnn.			Por favor, entre la dirección IP con formato: xxx.xxx.xxx.xxx.
77		ID_INDEX_TIME1				16			January			Enero
78		ID_INDEX_TIME2				16			February			Febrero
79		ID_INDEX_TIME3				16			March			Marzo
80		ID_INDEX_TIME4				16			April			Abril
81		ID_INDEX_TIME5				16			May				Mayo
82		ID_INDEX_TIME6				16			June			Junio
83		ID_INDEX_TIME7				16			July			Julio
84		ID_INDEX_TIME8				16			August			Agosto
85		ID_INDEX_TIME9				16			September			Septiembre
86		ID_INDEX_TIME10				16			October			Octubre
87		ID_INDEX_TIME11				16			November			Noviembre
88		ID_INDEX_TIME12				16			December			Diciembre
89		ID_INDEX_TIME13				16			Jan			Ene
90		ID_INDEX_TIME14				16			Feb			Feb
91		ID_INDEX_TIME15				16			Mar			Mar
92		ID_INDEX_TIME16				16			Apr			Abr
93		ID_INDEX_TIME17				16			May				May
94		ID_INDEX_TIME18				16			Jun			Jun
95		ID_INDEX_TIME19				16			Jul			Jul
96		ID_INDEX_TIME20				16			Aug			Ago
97		ID_INDEX_TIME21				16			Sep			Sep
98		ID_INDEX_TIME22				16			Oct			Oct
99		ID_INDEX_TIME23				16			Nov			Nov
100		ID_INDEX_TIME24				16			Dec			Dic
101		ID_INDEX_TIME25				16			Sunday			Domingo
102		ID_INDEX_TIME26				16			Monday			Lunes
103		ID_INDEX_TIME27				16			Tuesday			Martes
104		ID_INDEX_TIME28				16			Wednesday			Miércoles
105		ID_INDEX_TIME29				16			Thursday			Jueves
106		ID_INDEX_TIME30				16			Friday			Viernes
107		ID_INDEX_TIME31				16			Saturday			Sábado
108		ID_INDEX_TIME32				16			Sun			Dom
109		ID_INDEX_TIME33				16			Mon			Lun
110		ID_INDEX_TIME34				16			Tue			Mar
111		ID_INDEX_TIME35				16			Wed			Mié
112		ID_INDEX_TIME36				16			Thu			Jue
113		ID_INDEX_TIME37				16			Fri			Vie
114		ID_INDEX_TIME38				16			Sat			Sáb
115		ID_INDEX_TIME39				16			Sun			Dom
116		ID_INDEX_TIME40				16			Mon			Lun
117		ID_INDEX_TIME41				16			Tue			Mar
118		ID_INDEX_TIME42				16			Wed			Mié
119		ID_INDEX_TIME43				16			Thu			Jue
120		ID_INDEX_TIME44				16			Fri			Vie
121		ID_INDEX_TIME45				16			Sat			Sáb
122		ID_INDEX_TIME46				16			Current Time		Hora actual
123		ID_INDEX_TIME47				16			Confirm			Confirmar
124		ID_INDEX_TIME48				16			Time			Tiempo
125		ID_INDEX_TIME49				16			Hour			Hora
126		ID_INDEX_TIME50				16			Minute			Minuto
127		ID_INDEX_TIME51				16			Second			Segundo
128		ID_MPPTS				32			Solar Converters		Convertidores Solares
129		ID_INDEX_TIPS27				32			Refresh			Refrescar
130		ID_INDEX_TIPS28				32			There is no data to show.				No hay datos
131		ID_INDEX_TIPS29				128			Fail to connect to controller, please login again.		Fallo al conectarse a la NCU. Inténtelo de nuevo.
132		ID_INDEX_TIPS30				128			Link attribute \"data\" data structure error, should be {} object format.			Link attribute \"data\" error de estructura de datos. Debería tener {} formato de objecto.
133		ID_INDEX_TIPS31				256			The format of \"validate\" is incorrect.  It should be have {} object format.  Please check the template file.			El formato de \"validate\" es incorrecto.  Debería tener {} formato de objecto. Por favor, compruebe el archivo de plantilla. 
134		ID_INDEX_TIPS32				128			Data format error.			Error de formato
135		ID_INDEX_TIPS33				64			Setting date error.			Error fecha de ajuste
136		ID_INDEX_TIPS34				128			There is an error in the input parameter.  It should be in the format ({}).		Hay un error en el parámetro de entrada.
137		ID_INDEX_TIPS35				64			Too many clicks. Please wait.		Por favor, espere a que se complete la actualización actual.
138		ID_INDEX_TIPS36				32			Refresh webpage			Refrescar página web
139		ID_INDEX_TIPS37				32			Refresh console			Refrescar consola
140		ID_INDEX_TIPS38				128			Special characters are not allowed.			No se permiten caracteres especiales
141		ID_INDEX_TIPS39				64			The value can not be empty.			El valor no puede estar vacío
142		ID_INDEX_TIPS40				64			The value must be a positive integer or 0.			El valor debe ser 0 o un entero positivo
143		ID_INDEX_TIPS41				64			The value must be a floating point number.			El valor debe ser decimal
144		ID_INDEX_TIPS42				32			The value must be an integer.			El valor debe ser entero
145		ID_INDEX_TIPS43				64			The value is out of range.			Valor fuera de rango
146		ID_INDEX_TIPS44				32			Communication fail.			Fallo de Comunicación.
147		ID_INDEX_TIPS45				64			Confirm the change to the control value?			Confirmar cambio del valor de control?
148		ID_INDEX_TIPS46				128			Time format error or time is beyond the range. The format should be: Year/Month/Day Hour/Minute/Second			Error formato Fecha o está fuera de rango. El formato es: Año/Mes/Día Hora/minuto/segundo
149		ID_INDEX_TIPS47				32			Unknown error.				Error Desconocido		
150		ID_INDEX_TIPS48				64			(Data is beyond the normal range.)	(Los datos exceden el rango normal)
151		ID_INDEX_TIPS49				12			Date:				Fecha:
152		ID_INDEX_TIPS50				32			Temperature Trend Diagram		Sentence Translation
153		ID_INDEX_TIPS51				16			Tips					Consejos
154		ID_TIPS1				32			Unknown error.			Error Desconocido
155		ID_TIPS2				16			Successful.							Correcto
156		ID_TIPS3				128			Failed. Incorrect time setting.				Fallo. Ajuste de Fecha/Hora incorrecto
157		ID_TIPS4				128			Failed. Incomplete information.				Fallo. Información incompleta
158		ID_TIPS5				64			Failed. No privileges.					Fallo. Sin autoridad
159		ID_TIPS6				128			Cannot be modified. Controller is hardware protected.	No se puede modificar. NCU protegida por HW.
160		ID_TIPS7				256			Secondary Server IP Address is incorrect. \nThe format should be :'nnn.nnn.nnn.nnn'.	Dirección IP Servidor Secundario incorrecta. \nFormato: 'nnn.nnn.nnn.nnn'
161		ID_TIPS8				128			Format error! There is one blank between time and date.					Error de formato! Hay un espacio en blanco entre la hora y la fecha.
162		ID_TIPS9				128					Incorrect time interval. \nTime interval should be a positive integer.			Intervalo de tiempo incorrecto. \nDebe ser un entero positivo
163		ID_TIPS10				128			Date should be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.			La fecha debe estar entre '1970/01/01 00:00:00' y '2038/01/01 00:00:00'.
164		ID_TIPS11				128					Please clear the IP before time setting.							Por favor, elimine la dirección IP antes de ajustar la fecha.
165		ID_TIPS12				64			Time expired, please login again.						El tiempo ha expirado. Por favor, conéctese otra vez.
166		ID_TIPS13				16			Site						Central
167		ID_TIPS14				16			System Name						Sistema
168		ID_TIPS15				32			Back to Battery List		Volver a Lista de Baterías
169		ID_TIPS16				64			Capacity Trend Diagram		Diagrama Tendencia de Capacidad
170		ID_INDEX_TIPS52				64			Set successfully. Controller is restarting, please wait	Fijado con éxito. La NCU se está reiniciando. Espere.
171		ID_INDEX_TIPS53				16			seconds.					Segundos.
172		ID_INDEX_TIPS54				64			Back to the login page, please wait.		Volviendo a página de conexión. Espere
173		ID_INDEX_TIPS55				32			Confirm to be set to				Confirmar ajuste a
174		ID_INDEX_TIPS56				16			's value is					valor es
175		ID_INDEX_TIPS57				32			controller will restart.					La NCU se reiniciará
176		ID_INDEX_TIPS58				64			Template error, please refresh the page.		Error de plantilla. Refresque la página, por favor
177		ID_INDEX_TIPS59				64			[OK]Reconnect. [Cancel]Stop the page.		[OK] Reconectar    [Cancel] Detener página
178		ID_ENGINEER				32			Engineer Settings				Ajustes Ingeniero
179		ID_INDEX_TIPS60				128			Jump to the engineer web pages, please use IE browser.		Salto a páginas de Ingeniero. Use el navegador IE.
180		ID_INDEX_TIPS61				64			Jumping, please wait.		Saltando, espere…
181		ID_INDEX_TIPS62				64			Fail to jump.		Fallo al saltar
182		ID_INDEX_TIPS63				64			Please select new alarm level.		Por favor, seleccione nuevo nivel de alarma
183		ID_INDEX_TIPS64				64			Please select new relay number.		Por favor, seleccione nuevo relé de alarma
184		ID_INDEX_TIPS65				64			After setting you need to wait		Debe esperar después del ajuste.
185		ID_OA1					16			OA						Observ
186		ID_MA1					16			MA				Urgente
187		ID_CA1					16			CA				Crítica
188		ID_INDEX_TIPS66				128			After restarting, please clear the browser cache and login again.		Borre el cache del navegador tras reiniciar y conecte de nuevo
189		ID_DOWNLOAD_ERROR0			32			Unknown error.								Error Desconocido		
190		ID_DOWNLOAD_ERROR1			64			File downloaded successfully.						Archivo descargado con éxito.
191		ID_DOWNLOAD_ERROR2			64			Failed to download file.							Fallo al descargar archivo.
192		ID_DOWNLOAD_ERROR3			64			Failed to download, the file is too large.				Fallo al descargar. El archivo es demasiado grande.
193		ID_DOWNLOAD_ERROR4			64			Failed. No privileges.							Fallo. No tiene privilegios
194		ID_DOWNLOAD_ERROR5			64			Controller started successfully.						NCU iniciada con éxito.
195		ID_DOWNLOAD_ERROR6			64			File downloaded successfully.						Archivo descargado con éxito.
196		ID_DOWNLOAD_ERROR7			64			Failed to download file.							Fallo al descargar archivo.
197		ID_DOWNLOAD_ERROR8			64			Failed to upload file.							Fallo al cargar archivo.
198		ID_DOWNLOAD_ERROR9			64			File downloaded successfully.						Archivo descargado con éxito.
199		ID_DOWNLOAD_ERROR10			64			Failed to upload file. Hardware is protected.				Fallo al descargar. Protegido por HW.
200		ID_SYSTEM_VOLTAGE			32			Output Voltage			Tensión de Salida
201		ID_SYSTEM_CURRENT			32			Output Current			Corriente de Salida
202		ID_SYS_STATUS				32			System Status			Estado Sistema
203		ID_INDEX_TIPS67				64			There was an error on this page.		Hubo en error en la página.
204		ID_INDEX_TIPS68				16			Error				Error
205		ID_INDEX_TIPS69				16			Line				Línea
206		ID_INDEX_TIPS70				64			Click OK to continue.		Haga clic en OK para continuar.
207		ID_INDEX_TIPS71				32			Request error, please retry.	Error. Inténtelo de nuevo
#//changed by Frank Wu,1/N/27,20140527, for power split
208		ID_INDEX_TIPS72				32				Please select						Seleccionar
209		ID_INDEX_TIPS73				16				NA							NA
210		ID_INDEX_TIPS74				64				Please select equipment.			Seleccionar Equipo
211		ID_INDEX_TIPS75				64				Please select signal type.			Seleccionar Nombre de Señal
212		ID_INDEX_TIPS76				64				Please select signal.				Seleccionar una Señal.
213		ID_INDEX_TIPS77				64				Full name is too long				Nombre Completo demasiado largo.
#//changed by Frank Wu,1/N/N,20140716, for loading different language css file, eg. skin.en.css
#//Note:please don't translate the special ID_CSS_LAN
214		ID_CSS_LAN					16				en					es
215		ID_CON_VOLT				32				Converter Voltage				Tensión Convertidor
216		ID_CON_CURR				32				Converter Current				Corriente Convertidor
217		ID_INDEX_TIPS78				128				The High 2 Curr Limit Alarm must be larger than the High 1 Curr Limit Alarm				  Alarma Alta 2 Límite de corriente debe ser mayor que Alarma Alta 1 Límite de corriente
218		ID_SITE_INFORMATION				32				Site Information			Información del sitio
219		ID_SITE_NAME				32			Site Name				Nombre de Central
220		ID_SITE_LOCATION				32			Site Location			Localización
221		ID_ALARMSOUNDOFF				64			Touch Screen To Silence Alm			Toque la pantalla para silenciar la alarma
222		ID_INDEX_TIPS79				128			Time format error or time is beyond the range. The format should be: Day/Month/Year Hour/Minute/Second			Erreur de format ou l'heure est au-delà de la plage. Le format doit être:Día/Mes/Año Heure / minute / seconde .
223		ID_INDEX_TIPS80				128			Time format error or time is beyond the range. The format should be: Month/Day/Year Hour/Minute/Second			Erreur de format ou l'heure est au-delà de la plage. Le format doit être:Mes/Día/Año Heure / minute / seconde .
224		ID_INDEX_TIPS81					16				en					es
[tmp.index.html:Number]
22

[tmp.index.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	STRING				LOCAL
1		ID_SYSTEM				32			Power System			Sistema de Energía
2		ID_HYBRID				32			Energy Sources			Fuentes de Energía
3		ID_SOLAR				32			Solar				Solar
4		ID_SOLAR_RECT				32			Solar Converter			Convertidor Solar
5		ID_MAINS				32			Mains				Red Eléctrica
6		ID_RECTIFIER				32			Rectifier				Rectificador
7		ID_DG					32			DG					GE
8		ID_CONVERTER				32			Converter				Convertidor
9		ID_DC					32			DC					CC
10		ID_BATTERY				32			Battery				Batería
11		ID_SYSTEM_VOLTAGE			32			Output Voltage			Tensión de Salida
12		ID_SYSTEM_CURRENT			32			Output Current			Corriente de Salida
13		ID_LOAD_TREND				32			Load Trend				Tendencia de Carga
14		ID_DC1					32			DC					CC
15		ID_AMBIENT_TEMP				32			Ambient Temp			Temp Ambiente
16		ID_PEAK_CURRENT				32			Peak Current			Pico de Corriente
17		ID_AVERAGE_CURRENT			32			Average Current			Corriente Media
18		ID_L1					32			R					R
19		ID_L2					32			S					S
20		ID_L3					32			T					T
21		ID_BATTERY1				32			Battery				Batería
22		ID_TIPS					64			Data is beyond the normal range.	Los datos exceden el rango normal
23		ID_CONVERTER1				32			Converter				Convertidor
24		ID_SMIO					16			SMIO				SMIO
25		ID_TIPS2				32			No Sensor				Sin Sensor
26		ID_USER_DEF				32			User Define				Usuario definido
27		ID_CONSUM_MAP				32			Consumption Map			Mapa de Consumo
28		ID_SAMPLE				32			Sample Signal			Muestra de la señal
29		ID_DISTRI_BAY				32			Distribution Bay		Distribución Bay
30		ID_SOURCE				32			Source			Fuente
31		ID_SOLAR_RECT1				32			    Solar Converter			Solar Converter	
32		ID_SOLAR2				32			    Solar				Solar
33		ID_CUSTOMINPUTSTATUS				64			    Custom Input Status				Estado de entrada personalizado
[tmp.hybrid.html:Number]
9

[tmp.hybrid.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	STRING				LOCAL
1		ID_MAINS				32			Mains				Red Eléctrica
2		ID_SOLAR				32			Solar				Solar
3		ID_DG					32			DG					DG
4		ID_WIND					32			Wind				Eólica
5		ID_1_WEEK				32			This Week				Esta semana
6		ID_2_WEEK				32			Last Week				La semana pasada
7		ID_3_WEEK				32			Two Weeks Ago			Hace dos semanas
8		ID_4_WEEK				32			Three Weeks Ago			Hace 3 semanas
9		ID_NO_DATA				32			No Data				No hay datos
10		ID_TIMEFORMAT				32			Date:MM-DD				Fecha:MM-DD

[tmp.system_rectifier.html:Number]
16

[tmp.system_rectifier.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_RECTIFIER				32			Rectifier				Rectificador
2		ID_CONVERTER				32			Converter				Convertidor
3		ID_SOLAR				32			Solar Converter				Convertidor Solar
4		ID_VOLTAGE				32			Average Voltage			Tensión media
5		ID_CURRENT				32			Total Current			Corriente Total
6		ID_CAPACITY_USED			32			System Capacity Used		Capacidad utilizada Sistema
7		ID_NUM_OF_RECT				32			Number of Rectifiers		Número de Rectificadores
8		ID_TOTAL_COMM_RECT			64			Total Rectifiers Communicating	Total Rectificadores en Comunicación
9		ID_MAX_CAPACITY				32			Max Used Capacity			Máxima Capacidad Utilizada
10		ID_SIGNAL				32			Signal				Señal
11		ID_VALUE				32			Value				Valor
12		ID_SOLAR1				32			Solar Converter				Convertidor Solar
13		ID_CURRENT1				32			Total Current			Corriente Total
14		ID_RECTIFIER1				32			GI Rectifier				Rectificadores GI
15		ID_RECTIFIER2				32			GII Rectifier				Rectificadores GII
16		ID_RECTIFIER3				32			GIII Rectifier				Rectificadores GIII
17		ID_RECT_SET				32			Rectifier Settings			Ajustes Rectificador
18		ID_BACK					16			Back				Atrás
[tmp.system_rectifierS1.html:Number]
16

[tmp.system_rectifierS1.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_RECTIFIER				32			Rectifier				Rectificador
2		ID_CONVERTER				32			Converter				Convertidor
3		ID_SOLAR				32			Solar Converter				Convertidor Solar
4		ID_VOLTAGE				32			Average Voltage			Tensión media
5		ID_CURRENT				32			Total Current			Corriente Total
6		ID_CAPACITY_USED			32			System Capacity Used		Capacidad utilizada Sistema
7		ID_NUM_OF_RECT				32			Number of Rectifiers		Número de Rectificadores
8		ID_TOTAL_COMM_RECT			64			Number of Rects in communication	Número de Rectificadores en comunicación
9		ID_MAX_CAPACITY				32			Max Used Capacity			Máxima Capacidad Utilizada
10		ID_SIGNAL				32			Signal				Señal
11		ID_VALUE				32			Value				Valor
12		ID_SOLAR1				32			Solar Converter				Convertidor Solar
13		ID_CURRENT1				32			Total Current			Corriente Total
14		ID_RECTIFIER1				32			GI Rectifier			Rectificadores GI
15		ID_RECTIFIER2				32			GII Rectifier			Rectificadores GII
16		ID_RECTIFIER3				32			GIII Rectifier			Rectificadores GIII
#//changed by Frank Wu,1/1/30,20140527, for add single converter and single solar settings pages
17		ID_RECT_SET				32			Secondary Rectifier Settings			Ajuste Rectificador Esclavo
18		ID_BACK					16			Back					Atrás


[tmp.system_battery.html:Number]
16

[tmp.system_battery.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_BATTERY				32			Battery				Batería
2		ID_MANAGE_STATE				32			Battery Management State		Estado Gestión de Baterías
3		ID_BATT_CURR				32			Total Battery Current		Corriente Total a Batería
4		ID_REMAIN_TIME				32			Estimated Remaining Time		Tiempo Restante Estimado
5		ID_TEMP					32			Battery Temp			Temp Batería
6		ID_SIGNAL				32			Signal				Señal
7		ID_VALUE				32			Value				Valor
8		ID_SIGNAL1				32			Signal				Señal
9		ID_VALUE1				32			Value				Valor
10		ID_TIPS					32			Back to Battery List		Volver a Lista de Baterías
11		ID_SIGNAL2				32			Signal				Señal
12		ID_VALUE2				32			Value				Valor
13		ID_TIP1					64			Capacity Trend Diagram		Diagrama Tendencia de Capacidad
14		ID_TIP2					64			Temperature Trend Diagram		Sentence Translation
15		ID_TIP3					32			No Sensor				Sin Sensor
16		ID_EIB					16			EIB		EIB

[tmp.system_dc.html:Number]
14

[tmp.system_dc.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_DC					32			DC					CC
2		ID_SIGNAL				32			Signal				Señal
3		ID_VALUE				32			Value				Valor
4		ID_SIGNAL1				32			Signal				Señal
5		ID_VALUE1				32			Value				Valor
6		ID_SMDUH				32			SMDUH				SMDUH
7		ID_EIB					32			EIB					EIB
8		ID_DC_METER				32			DC Meter				Contador CC
9		ID_SMDU					32			SMDU				SMDU
10		ID_SMDUP				32			SMDUP				SMDUP
11		ID_NO_DATA				32			No Data				No hay datos
12		ID_SMDUP1				32			SMDUP1				SMDUP1
13		ID_CABINET				32			Cabinet Map			Mapa de Bastidor
14		ID_NARADA_BMS				32			BMS				BMS
15		ID_SMDUE				32			SMDUE				SMDUE

[tmp.history_alarmlog.html:Number]
23

[tmp.history_alarmlog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_ALARM				32			Alarm History Log			Histórico de Alarmas
2		ID_BATT_TEST				32			Battery Test Log			Registro de Prueba de Baterías
3		ID_EVENT				32			Event Log				Registro de Eventos
4		ID_DATA					32			Data History Log			Datos Históricos
5		ID_DEVICE				32			Device				Equipo
6		ID_FROM					32			From				Desde
7		ID_TO					32			To					Hasta
8		ID_QUERY				32			Query				Consulta
9		ID_UPLOAD				32			Upload				Descargar
10		ID_TIPS					64			Displays the last 500 entries		Muestra los últimos 500 registros
11		ID_INDEX				32			Index				Índice
12		ID_DEVICE1				32			Device Name				Equipo
13		ID_SIGNAL				32			Signal Name				Nombre de Señal
14		ID_LEVEL				32			Alarm Level				Nivel de Alarma
15		ID_START				32			Start Time				Inicio
16		ID_END					32			End Time				Fin
17		ID_ALL_DEVICE				32			All Devices				Todos los equipos
#//changed by Frank Wu,1/N/14,20140527, for system log
18		ID_SYSTEMLOG			32				System Log				Registro del Sistema
19		ID_OA					32			OA				OA
20		ID_MA					32			MA				MA
21		ID_CA					32			CA				CA
22		ID_ALARM2				32			Alarm History Log			Histórico de Alarmas
23		ID_ALL_DEVICE2				32			    All Devices				Todos los equipos
[tmp.history_testlog.html:Number]
904

[tmp.history_testlog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS					64			Choose the last battery test	Seleccione la última prueba
2		ID_SUMMARY_HEAD0			32			Start Time				Inicio
3		ID_SUMMARY_HEAD1			32			End Time				Fin
4		ID_SUMMARY_HEAD2			32			Start Reason			Razón Inicio
5		ID_SUMMARY_HEAD3			32			End Reason				Razón Fin
6		ID_SUMMARY_HEAD4			32			Test Result			Resultado Prueba
7		ID_QUERY				32			Query				Consulta
8		ID_UPLOAD				32			Upload				Descargar
9		ID_START_REASON0			64			Start Planned Test			Programar Prueba de Batería
10		ID_START_REASON1			64			Start Manual Test		Iniciar Prueba Manual
11		ID_START_REASON2			64			Start AC Fail Test			Iniciar Prueba en Fallo de Red	
12		ID_START_REASON3			64			Start Master Battery Test		Iniciar Prueba Maestra	
13		ID_START_REASON4			64			Start Test for Other Reasons				Otras Razones
14		ID_END_REASON0				64			End Test Manually						Prueba finalizada manualmente			
15		ID_END_REASON1				64			End Test for Alarm						Prueba finalizada por alarma			
16		ID_END_REASON2				64			End Test for Test Time-Out						Fin de prueba por tiempo		
17		ID_END_REASON3				64			End Test for Capacity Condition				Fin de prueba por Capacidad		
18		ID_END_REASON4				64			End Test for Voltage Condition				Fin de prueba por Tensión
19		ID_END_REASON5				64			End Test for AC Fail					Fin de prueba por Fallo de Red			
20		ID_END_REASON6				64			End AC Fail Test for AC Restore				Fin de prueba en Fallo de Red por vuelta de Red			
21		ID_END_REASON7				64			End AC Fail Test for Being Disabled				Fin de prueba en Fallo de Red por desactivación			
22		ID_END_REASON8				64			End Master Battery Test					Fin de Prueba Maestra			
23		ID_END_REASON9				64			End PowerSplit BT for Auto to Manual		Parada prueba PowerSplit por cambio a modo Manual			
24		ID_END_REASON10				64			End PowerSplit Man-BT for Manual to Auto	Parada prueba PowerSplit por cambio a modo Auto			
25		ID_END_REASON11				64			End Test for Other Reasons						Parada por otros motivos			
26		ID_RESULT0				32			No Result.			Sin resultado de prueba
27		ID_RESULT1				32			Battery is OK			La Batería está bien
28		ID_RESULT2				32			Battery is Bad			La Batería está mal	
29		ID_RESULT3				32			It's PowerSplit Test		Prueba PowerSplit
30		ID_RESULT4				16			Other results.			Otros resultados
31		ID_SUMMARY0				32			Index				Índice	
32		ID_SUMMARY1				32			Record Time				Tiempo Registro
33		ID_SUMMARY2				32			System Voltage			Tensión del Sistema
34		ID_HEAD0				32			Battery1 Current			Corriente Batería 1	
35		ID_HEAD1				32			Battery1 Voltage			Tensión Batería 1	
36		ID_HEAD2				32			Battery1 Capacity			Capacidad Batería 1	
37		ID_HEAD3				32			Battery2 Current			Corriente Batería 2	
38		ID_HEAD4				32			Battery2 Voltage			Tensión Batería 2	
39		ID_HEAD5				32			Battery2 Capacity			Capacidad Batería 2	
40		ID_HEAD6				32			EIB1Battery1 Current		Corriente EIB1-Batería1	
41		ID_HEAD7				32			EIB1Battery1 Voltage		Tensión EIB1-Batería1	
42		ID_HEAD8				32			EIB1Battery1 Capacity		Capacidad EIB1-Batería1	
43		ID_HEAD9				32			EIB1Battery2 Current		Corriente EIB1-Batería2	
44		ID_HEAD10				32			EIB1Battery2 Voltage		Tensión EIB1-Batería2	
45		ID_HEAD11				32			EIB1Battery2 Capacity		Capacidad EIB1-Batería2	
46		ID_HEAD12				32			EIB2Battery1 Current		Corriente EIB2-Batería1	
47		ID_HEAD13				32			EIB2Battery1 Voltage		Tensión EIB2-Batería1	
48		ID_HEAD14				32			EIB2Battery1 Capacity		Capacidad EIB2-Batería1	
49		ID_HEAD15				32			EIB2Battery2 Current		Corriente EIB2-Batería2	
50		ID_HEAD16				32			EIB2Battery2 Voltage		Tensión EIB2-Batería2	
51		ID_HEAD17				32			EIB2Battery2 Capacity		Capacidad EIB2-Batería2	
52		ID_HEAD18				32			EIB3Battery1 Current		Corriente EIB3-Batería1	
53		ID_HEAD19				32			EIB3Battery1 Voltage		Tensión EIB3-Batería1	
54		ID_HEAD20				32			EIB3Battery1 Capacity		Capacidad EIB3-Batería1	
55		ID_HEAD21				32			EIB3Battery2 Current		Corriente EIB3-Batería2	
56		ID_HEAD22				32			EIB3Battery2 Voltage		Tensión EIB3-Batería2	
57		ID_HEAD23				32			EIB3Battery2 Capacity		Capacidad EIB3-Batería2	
58		ID_HEAD24				32			EIB4Battery1 Current		Corriente EIB4-Batería1	
59		ID_HEAD25				32			EIB4Battery1 Voltage		Tensión EIB4-Batería1	
60		ID_HEAD26				32			EIB4Battery1 Capacity		Capacidad EIB4-Batería1	
61		ID_HEAD27				32			EIB4Battery2 Current		Corriente EIB4-Batería2	
62		ID_HEAD28				32			EIB4Battery2 Voltage		Tensión EIB4-Batería2	
63		ID_HEAD29				32			EIB4Battery2 Capacity		Capacidad EIB4-Batería2	
64		ID_HEAD30				32			SMDU1Battery1 Current		Corriente SMDU1-Batería1	
65		ID_HEAD31				32			SMDU1Battery1 Voltage		Tensión SMDU1-Batería1		
66		ID_HEAD32				32			SMDU1Battery1 Capacity		Capacidad SMDU1-Batería1		
67		ID_HEAD33				32			SMDU1Battery2 Current		Corriente SMDU1-Batería2	
68		ID_HEAD34				32			SMDU1Battery2 Voltage		Tensión SMDU1-Batería2		
69		ID_HEAD35				32			SMDU1Battery2 Capacity		Capacidad SMDU1-Batería2		
70		ID_HEAD36				32			SMDU1Battery3 Current		Corriente SMDU1-Batería3		
71		ID_HEAD37				32			SMDU1Battery3 Voltage		Tensión SMDU1-Batería3		
72		ID_HEAD38				32			SMDU1Battery3 Capacity		Capacidad SMDU1-Batería3		
73		ID_HEAD39				32			SMDU1Battery4 Current		Corriente SMDU1-Batería4		
74		ID_HEAD40				32			SMDU1Battery4 Voltage		Tensión SMDU1-Batería4	
75		ID_HEAD41				32			SMDU1Battery4 Capacity		Capacidad SMDU1-Batería4		
76		ID_HEAD42				32			SMDU2Battery1 Current		Corriente SMDU2-Batería1	
77		ID_HEAD43				32			SMDU2Battery1 Voltage		Tensión SMDU2-Batería1		
78		ID_HEAD44				32			SMDU2Battery1 Capacity		Capacidad SMDU2-Batería1	
79		ID_HEAD45				32			SMDU2Battery2 Current		Corriente SMDU2-Batería2	
80		ID_HEAD46				32			SMDU2Battery2 Voltage		Tensión SMDU2-Batería2		
81		ID_HEAD47				32			SMDU2Battery2 Capacity		Capacidad SMDU2-Batería2	
82		ID_HEAD48				32			SMDU2Battery3 Current		Corriente SMDU2-Batería3		
83		ID_HEAD49				32			SMDU2Battery3 Voltage		Tensión SMDU2-Batería3		
84		ID_HEAD50				32			SMDU2Battery3 Capacity		Capacidad SMDU2-Batería3		
85		ID_HEAD51				32			SMDU2Battery4 Current		Corriente SMDU2-Batería4	
86		ID_HEAD52				32			SMDU2Battery4 Voltage		Tensión SMDU2-Batería4		
87		ID_HEAD53				32			SMDU2Battery4 Capacity		Capacidad SMDU2-Batería4		
88		ID_HEAD54				32			SMDU3Battery1 Current		Corriente SMDU3-Batería1		
89		ID_HEAD55				32			SMDU3Battery1 Voltage		Tensión SMDU3-Batería1		
90		ID_HEAD56				32			SMDU3Battery1 Capacity		Capacidad SMDU3-Batería1		
91		ID_HEAD57				32			SMDU3Battery2 Current		Corriente SMDU3-Batería2		
92		ID_HEAD58				32			SMDU3Battery2 Voltage		Tensión SMDU3-Batería2		
93		ID_HEAD59				32			SMDU3Battery2 Capacity		Capacidad SMDU3-Batería2		
94		ID_HEAD60				32			SMDU3Battery3 Current		Corriente SMDU3-Batería3	
95		ID_HEAD61				32			SMDU3Battery3 Voltage		Tensión SMDU3-Batería3		
96		ID_HEAD62				32			SMDU3Battery3 Capacity		Capacidad SMDU3-Batería3		
97		ID_HEAD63				32			SMDU3Battery4 Current		Corriente SMDU3-Batería4		
98		ID_HEAD64				32			SMDU3Battery4 Voltage		Tensión SMDU3-Batería4	
99		ID_HEAD65				32			SMDU3Battery4 Capacity		Capacidad SMDU3-Batería4	
100		ID_HEAD66				32			SMDU4Battery1 Current		SCorriente SMDU4-Batería1		
101		ID_HEAD67				32			SMDU4Battery1 Voltage		Tensión SMDU4-Batería1	
102		ID_HEAD68				32			SMDU4Battery1 Capacity		Capacidad SMDU4-Batería1		
103		ID_HEAD69				32			SMDU4Battery2 Current		Corriente SMDU4-Batería2		
104		ID_HEAD70				32			SMDU4Battery2 Voltage		Tensión SMDU4-Batería2		
105		ID_HEAD71				32			SMDU4Battery2 Capacity		Capacidad SMDU4-Batería2		
106		ID_HEAD72				32			SMDU4Battery3 Current		Corriente SMDU4-Batería3		
107		ID_HEAD73				32			SMDU4Battery3 Voltage		Tensión SMDU4-Batería3		
108		ID_HEAD74				32			SMDU4Battery3 Capacity		Capacidad SMDU4-Batería3		
109		ID_HEAD75				32			SMDU4Battery4 Current		Corriente SMDU4-Batería4	
110		ID_HEAD76				32			SMDU4Battery4 Voltage		Tensión SMDU4-Batería4	
111		ID_HEAD77				32			SMDU4Battery4 Capacity		Capacidad SMDU4-Batería4		
112		ID_HEAD78				32			SMDU5Battery1 Current		Corriente SMDU5-Batería1		
113		ID_HEAD79				32			SMDU5Battery1 Voltage		Tensión SMDU5-Batería1		
114		ID_HEAD80				32			SMDU5Battery1 Capacity		Capacidad SMDU5-Batería1		
115		ID_HEAD81				32			SMDU5Battery2 Current		Corriente SMDU5-Batería2		
116		ID_HEAD82				32			SMDU5Battery2 Voltage		Tensión SMDU5-Batería2		
117		ID_HEAD83				32			SMDU5Battery2 Capacity		Capacidad SMDU5-Batería2			
118		ID_HEAD84				32			SMDU5Battery3 Current		Corriente SMDU5-Batería3		
119		ID_HEAD85				32			SMDU5Battery3 Voltage		Tensión SMDU5-Batería3		
120		ID_HEAD86				32			SMDU5Battery3 Capacity		Capacidad SMDU5-Batería3		
121		ID_HEAD87				32			SMDU5Battery4 Current		Corriente SMDU5-Batería4	
122		ID_HEAD88				32			SMDU5Battery4 Voltage		Tensión SMDU5-Batería4		
123		ID_HEAD89				32			SMDU5Battery4 Capacity		Capacidad SMDU5-Batería4		
124		ID_HEAD90				32			SMDU6Battery1 Current		Corriente SMDU6-Batería1	
125		ID_HEAD91				32			SMDU6Battery1 Voltage		Tensión SMDU6-Batería1		
126		ID_HEAD92				32			SMDU6Battery1 Capacity		Capacidad SMDU6-Batería1			
127		ID_HEAD93				32			SMDU6Battery2 Current		Corriente SMDU6-Batería2		
128		ID_HEAD94				32			SMDU6Battery2 Voltage		Tensión SMDU6-Batería2		
129		ID_HEAD95				32			SMDU6Battery2 Capacity		Capacidad SMDU6-Batería2			
130		ID_HEAD96				32			SMDU6Battery3 Current		Corriente SMDU6-Batería3	
131		ID_HEAD97				32			SMDU6Battery3 Voltage		Tensión SMDU6-Batería3		
132		ID_HEAD98				32			SMDU6Battery3 Capacity		Capacidad SMDU6-Batería3			
133		ID_HEAD99				32			SMDU6Battery4 Current		Corriente SMDU6-Batería4		
134		ID_HEAD100				32			SMDU6Battery4 Voltage		Tensión SMDU6-Batería4		
135		ID_HEAD101				32			SMDU6Battery4 Capacity		Capacidad SMDU6-Batería4			
136		ID_HEAD102				32			SMDU7Battery1 Current		Corriente SMDU7-Batería1		
137		ID_HEAD103				32			SMDU7Battery1 Voltage		Tensión SMDU7-Batería1	
138		ID_HEAD104				32			SMDU7Battery1 Capacity		Capacidad SMDU7-Batería1		
139		ID_HEAD105				32			SMDU7Battery2 Current		Corriente SMDU7-Batería2		
140		ID_HEAD106				32			SMDU7Battery2 Voltage		Tensión SMDU7-Batería2		
141		ID_HEAD107				32			SMDU7Battery2 Capacity		Capacidad SMDU7-Batería2			
142		ID_HEAD108				32			SMDU7Battery3 Current		Corriente SMDU7-Batería3	
143		ID_HEAD109				32			SMDU7Battery3 Voltage		Tensión SMDU7-Batería3		
144		ID_HEAD110				32			SMDU7Battery3 Capacity		Capacidad SMDU7-Batería3		
145		ID_HEAD111				32			SMDU7Battery4 Current		Corriente SMDU7-Batería4	
146		ID_HEAD112				32			SMDU7Battery4 Voltage		Tensión SMDU7-Batería4	
147		ID_HEAD113				32			SMDU7Battery4 Capacity		Capacidad SMDU7-Batería4		
148		ID_HEAD114				32			SMDU8Battery1 Current		Corriente SMDU8-Batería1		
149		ID_HEAD115				32			SMDU8Battery1 Voltage		Tensión SMDU8-Batería1		
150		ID_HEAD116				32			SMDU8Battery1 Capacity		Capacidad SMDU8-Batería1			
151		ID_HEAD117				32			SMDU8Battery2 Current		Corriente SMDU8-Batería2		
152		ID_HEAD118				32			SMDU8Battery2 Voltage		Tensión SMDU8-Batería2		
153		ID_HEAD119				32			SMDU8Battery2 Capacity		Capacidad SMDU8-Batería2			
154		ID_HEAD120				32			SMDU8Battery3 Current		Corriente SMDU8-Batería3		
155		ID_HEAD121				32			SMDU8Battery3 Voltage		Tensión SMDU8-Batería3		
156		ID_HEAD122				32			SMDU8Battery3 Capacity		Capacidad SMDU8-Batería3			
157		ID_HEAD123				32			SMDU8Battery4 Current		Corriente SMDU8-Batería4	
158		ID_HEAD124				32			SMDU8Battery4 Voltage		Tensión SMDU8-Batería4		
159		ID_HEAD125				32			SMDU8Battery4 Capacity		Capacidad SMDU8-Batería4			
160		ID_HEAD126				32			EIB1Battery3 Current		Corriente EIB1-Batería3		
161		ID_HEAD127				32			EIB1Battery3 Voltage		Tensión EIB1-Batería3		
162		ID_HEAD128				32			EIB1Battery3 Capacity		Capacidad EIB1-Batería3		
163		ID_HEAD129				32			EIB2Battery3 Current		Corriente EIB2-Batería3		
164		ID_HEAD130				32			EIB2Battery3 Voltage		Tensión EIB2-Batería3		
165		ID_HEAD131				32			EIB2Battery3 Capacity		Capacidad EIB2-Batería3		
166		ID_HEAD132				32			EIB3Battery3 Current		Corriente EIB3-Batería3		
167		ID_HEAD133				32			EIB3Battery3 Voltage		Tensión EIB3-Batería3		
168		ID_HEAD134				32			EIB3Battery3 Capacity		Capacidad EIB3-Batería3		
169		ID_HEAD135				32			EIB4Battery3 Current		Corriente EIB4-Batería3		
170		ID_HEAD136				32			EIB4Battery3 Voltage		Tensión EIB4-Batería3		
171		ID_HEAD137				32			EIB4Battery3 Capacity		Capacidad EIB4-Batería3		
172		ID_HEAD138				32			EIB1Block1Voltage			Tensión EIB1-Celda1		
173		ID_HEAD139				32			EIB1Block2Voltage			Tensión EIB1-Celda2		
174		ID_HEAD140				32			EIB1Block3Voltage			Tensión EIB1-Celda3		
175		ID_HEAD141				32			EIB1Block4Voltage			Tensión EIB1-Celda4		
176		ID_HEAD142				32			EIB1Block5Voltage			Tensión EIB1-Celda5		
177		ID_HEAD143				32			EIB1Block6Voltage			Tensión EIB1-Celda6		
178		ID_HEAD144				32			EIB1Block7Voltage			Tensión EIB1-Celda7	
179		ID_HEAD145				32			EIB1Block8Voltage			Tensión EIB1-Celda8		
180		ID_HEAD146				32			EIB2Block1Voltage			Tensión EIB2-Celda1		
181		ID_HEAD147				32			EIB2Block2Voltage			Tensión EIB2-Celda2		
182		ID_HEAD148				32			EIB2Block3Voltage			Tensión EIB2-Celda3		
183		ID_HEAD149				32			EIB2Block4Voltage			Tensión EIB2-Celda4		
184		ID_HEAD150				32			EIB2Block5Voltage			Tensión EIB2-Celda5		
185		ID_HEAD151				32			EIB2Block6Voltage			Tensión EIB2-Celda6		
186		ID_HEAD152				32			EIB2Block7Voltage			Tensión EIB2-Celda7		
187		ID_HEAD153				32			EIB2Block8Voltage			Tensión EIB2-Celda8	
188		ID_HEAD154				32			EIB3Block1Voltage			Tensión EIB3-Celda1		
189		ID_HEAD155				32			EIB3Block2Voltage			Tensión EIB3-Celda2	
190		ID_HEAD156				32			EIB3Block3Voltage			Tensión EIB3-Celda3		
191		ID_HEAD157				32			EIB3Block4Voltage			Tensión EIB3-Celda4		
192		ID_HEAD158				32			EIB3Block5Voltage			Tensión EIB3-Celda5		
193		ID_HEAD159				32			EIB3Block6Voltage			Tensión EIB3-Celda6		
194		ID_HEAD160				32			EIB3Block7Voltage			Tensión EIB3-Celda7		
195		ID_HEAD161				32			EIB3Block8Voltage			Tensión EIB3-Celda8	
196		ID_HEAD162				32			EIB4Block1Voltage			Tensión EIB4-Celda1		
197		ID_HEAD163				32			EIB4Block2Voltage			Tensión EIB4-Celda2	
198		ID_HEAD164				32			EIB4Block3Voltage			Tensión EIB4-Celda3		
199		ID_HEAD165				32			EIB4Block4Voltage			Tensión EIB4-Celda4		
200		ID_HEAD166				32			EIB4Block5Voltage			Tensión EIB4-Celda5		
201		ID_HEAD167				32			EIB4Block6Voltage			Tensión EIB4-Celda6		
202		ID_HEAD168				32			EIB4Block7Voltage			Tensión EIB4-Celda7		
203		ID_HEAD169				32			EIB4Block8Voltage			Tensión EIB4-Celda8		
204		ID_HEAD170				32			Temperature1			Temperatura 1			
205		ID_HEAD171				32			Temperature2			Temperatura 2			
206		ID_HEAD172				32			Temperature3			Temperatura 3			
207		ID_HEAD173				32			Temperature4			Temperatura 4			
208		ID_HEAD174				32			Temperature5			Temperatura 5			
209		ID_HEAD175				32			Temperature6			Temperatura 6			
210		ID_HEAD176				32			Temperature7			Temperatura 7			
211		ID_HEAD177				32			Battery1 Current			Corriente Batería 1		
212		ID_HEAD178				32			Battery1 Voltage			Tensión Batería 1		
213		ID_HEAD179				32			Battery1 Capacity			Capacidad Batería 1		
214		ID_HEAD180				32			LargeDUBattery1 Current		Corriente GranDU-Batería1
215		ID_HEAD181				32			LargeDUBattery1 Voltage		Tensión GranDU-Batería1
216		ID_HEAD182				32			LargeDUBattery1 Capacity		Capacidad GranDU-Batería1	
217		ID_HEAD183				32			LargeDUBattery2 Current		Corriente GranDU-Batería2
218		ID_HEAD184				32			LargeDUBattery2 Voltage		Tensión GranDU-Batería2
219		ID_HEAD185				32			LargeDUBattery2 Capacity		Capacidad GranDU-Batería2	
220		ID_HEAD186				32			LargeDUBattery3 Current		Corriente GranDU-Batería3	
221		ID_HEAD187				32			LargeDUBattery3 Voltage		Tensión GranDU-Batería3
222		ID_HEAD188				32			LargeDUBattery3 Capacity		Capacidad GranDU-Batería3		
223		ID_HEAD189				32			LargeDUBattery4 Current		Corriente GranDU-Batería4
224		ID_HEAD190				32			LargeDUBattery4 Voltage		Tensión GranDU-Batería4
225		ID_HEAD191				32			LargeDUBattery4 Capacity		Capacidad GranDU-Batería4	
226		ID_HEAD192				32			LargeDUBattery5 Current		Corriente GranDU-Batería5
227		ID_HEAD193				32			LargeDUBattery5 Voltage		Tensión GranDU-Batería5
228		ID_HEAD194				32			LargeDUBattery5 Capacity		Capacidad GranDU-Batería5		
229		ID_HEAD195				32			LargeDUBattery6 Current		Corriente GranDU-Batería6	
230		ID_HEAD196				32			LargeDUBattery6 Voltage		Tensión GranDU-Batería6
231		ID_HEAD197				32			LargeDUBattery6 Capacity		Capacidad GranDU-Batería6	
232		ID_HEAD198				32			LargeDUBattery7 Current		Corriente GranDU-Batería7
233		ID_HEAD199				32			LargeDUBattery7 Voltage		Tensión GranDU-Batería7	
234		ID_HEAD200				32			LargeDUBattery7 Capacity		Capacidad GranDU-Batería7	
235		ID_HEAD201				32			LargeDUBattery8 Current		Corriente GranDU-Batería8
236		ID_HEAD202				32			LargeDUBattery8 Voltage		Tensión GranDU-Batería8	
237		ID_HEAD203				32			LargeDUBattery8 Capacity		Capacidad GranDU-Batería8	
238		ID_HEAD204				32			LargeDUBattery9 Current		Corriente GranDU-Batería9
239		ID_HEAD205				32			LargeDUBattery9 Voltage		Tensión GranDU-Batería9
240		ID_HEAD206				32			LargeDUBattery9 Capacity		Capacidad GranDU-Batería9		
241		ID_HEAD207				32			LargeDUBattery10 Current		Corriente GranDU-Batería10	
242		ID_HEAD208				32			LargeDUBattery10 Voltage		Tensión GranDU-Batería10	
243		ID_HEAD209				32			LargeDUBattery10 Capacity		Capacidad GranDU-Batería10		
244		ID_HEAD210				32			LargeDUBattery11 Current		Corriente GranDU-Batería11	
245		ID_HEAD211				32			LargeDUBattery11 Voltage		Tensión GranDU-Batería11	
246		ID_HEAD212				32			LargeDUBattery11 Capacity		Capacidad GranDU-Batería11		
247		ID_HEAD213				32			LargeDUBattery12 Current		Corriente GranDU-Batería12	
248		ID_HEAD214				32			LargeDUBattery12 Voltage		Tensión GranDU-Batería12	
249		ID_HEAD215				32			LargeDUBattery12 Capacity		Capacidad GranDU-Batería12		
250		ID_HEAD216				32			LargeDUBattery13 Current		Corriente GranDU-Batería13	
251		ID_HEAD217				32			LargeDUBattery13 Voltage		Tensión GranDU-Batería13	
252		ID_HEAD218				32			LargeDUBattery13 Capacity		Capacidad GranDU-Batería13	
253		ID_HEAD219				32			LargeDUBattery14 Current		Corriente GranDU-Batería14	
254		ID_HEAD220				32			LargeDUBattery14 Voltage		Tensión GranDU-Batería14	
255		ID_HEAD221				32			LargeDUBattery14 Capacity		Capacidad GranDU-Batería14		
256		ID_HEAD222				32			LargeDUBattery15 Current		Corriente GranDU-Batería15	
257		ID_HEAD223				32			LargeDUBattery15 Voltage		Tensión GranDU-Batería15	
258		ID_HEAD224				32			LargeDUBattery15 Capacity		Capacidad GranDU-Batería15		
259		ID_HEAD225				32			LargeDUBattery16 Current		Corriente GranDU-Batería16	
260		ID_HEAD226				32			LargeDUBattery16 Voltage		Tensión GranDU-Batería16	
261		ID_HEAD227				32			LargeDUBattery16 Capacity		Capacidad GranDU-Batería16		
262		ID_HEAD228				32			LargeDUBattery17 Current		Corriente GranDU-Batería17	
263		ID_HEAD229				32			LargeDUBattery17 Voltage		Tensión GranDU-Batería17	
264		ID_HEAD230				32			LargeDUBattery17 Capacity		Capacidad GranDU-Batería17		
265		ID_HEAD231				32			LargeDUBattery18 Current		Corriente GranDU-Batería18	
266		ID_HEAD232				32			LargeDUBattery18 Voltage		Tensión GranDU-Batería18	
267		ID_HEAD233				32			LargeDUBattery18 Capacity		Capacidad GranDU-Batería18		
268		ID_HEAD234				32			LargeDUBattery19 Current		Corriente GranDU-Batería19	
269		ID_HEAD235				32			LargeDUBattery19 Voltage		Tensión GranDU-Batería19	
270		ID_HEAD236				32			LargeDUBattery19 Capacity		Capacidad GranDU-Batería19		
271		ID_HEAD237				32			LargeDUBattery20 Current		Corriente GranDU-Batería1	
272		ID_HEAD238				32			LargeDUBattery20 Voltage		Tensión GranDU-Batería1	
273		ID_HEAD239				32			LargeDUBattery20 Capacity		Capacidad GranDU-Batería1		
274		ID_HEAD240				32			Temperature8			Temperatura 8			
275		ID_HEAD241				32			Temperature9			Temperatura 9			
276		ID_HEAD242				32			Temperature10			Temperatura 10			
277		ID_HEAD243				32			SMBattery1 Current			Corriente SMBAT 1		
278		ID_HEAD244				32			SMBattery1 Voltage			Tensión SMBAT 1		
279		ID_HEAD245				32			SMBattery1 Capacity			Capacidad SMBAT 1		
280		ID_HEAD246				32			SMBattery2 Current			Corriente SMBAT 2		
281		ID_HEAD247				32			SMBattery2 Voltage			Tensión SMBAT 2		
282		ID_HEAD248				32			SMBattery2 Capacity			Capacidad SMBAT 2		
283		ID_HEAD249				32			SMBattery3 Current			Corriente SMBAT 3		
284		ID_HEAD250				32			SMBattery3 Voltage			Tensión SMBAT 3		
285		ID_HEAD251				32			SMBattery3 Capacity			Capacidad SMBAT 3		
286		ID_HEAD252				32			SMBattery4 Current			Corriente SMBAT 4		
287		ID_HEAD253				32			SMBattery4 Voltage			Tensión SMBAT 4		
288		ID_HEAD254				32			SMBattery4 Capacity			Capacidad SMBAT 4		
289		ID_HEAD255				32			SMBattery5 Current			Corriente SMBAT 5		
290		ID_HEAD256				32			SMBattery5 Voltage			Tensión SMBAT 5		
291		ID_HEAD257				32			SMBattery5 Capacity			Capacidad SMBAT 5		
292		ID_HEAD258				32			SMBattery6 Current			Corriente SMBAT 6		
293		ID_HEAD259				32			SMBattery6 Voltage			Tensión SMBAT 6		
294		ID_HEAD260				32			SMBattery6 Capacity			Capacidad SMBAT 6		
295		ID_HEAD261				32			SMBattery7 Current			Corriente SMBAT 7		
296		ID_HEAD262				32			SMBattery7 Voltage			Tensión SMBAT 7		
297		ID_HEAD263				32			SMBattery7 Capacity			Capacidad SMBAT 7		
298		ID_HEAD264				32			SMBattery8 Current			Corriente SMBAT 8		
299		ID_HEAD265				32			SMBattery8 Voltage			Tensión SMBAT 8		
300		ID_HEAD266				32			SMBattery8 Capacity			Capacidad SMBAT 8		
301		ID_HEAD267				32			SMBattery9 Current			Corriente SMBAT 9	
302		ID_HEAD268				32			SMBattery9 Voltage			Tensión SMBAT 9		
303		ID_HEAD269				32			SMBattery9 Capacity			Capacidad SMBAT 9		
304		ID_HEAD270				32			SMBattery10 Current			Corriente SMBAT 10	
305		ID_HEAD271				32			SMBattery10 Voltage			Tensión SMBAT 10
306		ID_HEAD272				32			SMBattery10 Capacity		Capacidad SMBAT 10	
307		ID_HEAD273				32			SMBattery11 Current			Corriente SMBAT 11	
308		ID_HEAD274				32			SMBattery11 Voltage			Tensión SMBAT 11	
309		ID_HEAD275				32			SMBattery11 Capacity		Capacidad SMBAT 11	
310		ID_HEAD276				32			SMBattery12 Current			Corriente SMBAT 12	
311		ID_HEAD277				32			SMBattery12 Voltage			Tensión SMBAT 12	
312		ID_HEAD278				32			SMBattery12 Capacity		Capacidad SMBAT 12	
313		ID_HEAD279				32			SMBattery13 Current			Corriente SMBAT 13	
314		ID_HEAD280				32			SMBattery13 Voltage			Tensión SMBAT 13	
315		ID_HEAD281				32			SMBattery13 Capacity		Capacidad SMBAT 13	
316		ID_HEAD282				32			SMBattery14 Current			Corriente SMBAT 14
317		ID_HEAD283				32			SMBattery14 Voltage			Tensión SMBAT 14	
318		ID_HEAD284				32			SMBattery14 Capacity		Capacidad SMBAT 14	
319		ID_HEAD285				32			SMBattery15 Current			Corriente SMBAT 15	
320		ID_HEAD286				32			SMBattery15 Voltage			Tensión SMBAT 15	
321		ID_HEAD287				32			SMBattery15 Capacity		Capacidad SMBAT 15	
322		ID_HEAD288				32			SMBattery16 Current			Corriente SMBAT 16
323		ID_HEAD289				32			SMBattery16 Voltage			Tensión SMBAT 16	
324		ID_HEAD290				32			SMBattery16 Capacity		Capacidad SMBAT 16	
325		ID_HEAD291				32			SMBattery17 Current			Corriente SMBAT 17	
326		ID_HEAD292				32			SMBattery17 Voltage			Tensión SMBAT 17	
327		ID_HEAD293				32			SMBattery17 Capacity		Capacidad SMBAT 17	
328		ID_HEAD294				32			SMBattery18 Current			Corriente SMBAT 18	
329		ID_HEAD295				32			SMBattery18 Voltage			Tensión SMBAT 18	
330		ID_HEAD296				32			SMBattery18 Capacity		Capacidad SMBAT 18	
331		ID_HEAD297				32			SMBattery19 Current			Corriente SMBAT 19	
332		ID_HEAD298				32			SMBattery19 Voltage			Tensión SMBAT 19
333		ID_HEAD299				32			SMBattery19 Capacity		Capacidad SMBAT 19
334		ID_HEAD300				32			SMBattery20 Current			Corriente SMBAT 20
335		ID_HEAD301				32			SMBattery20 Voltage			Tensión SMBAT 20	
336		ID_HEAD302				32			SMBattery20 Capacity		Capacidad SMBAT 20
337		ID_HEAD303				32			SMDU1Battery5 Current		Corriente SMDU1-Batería5	
338		ID_HEAD304				32			SMDU1Battery5 Voltage		Tensión SMDU1-Batería5	
339		ID_HEAD305				32			SMDU1Battery5 Capacity		Capacidad SMDU1-Batería5	
340		ID_HEAD306				32			SMDU2Battery5 Current		Corriente SMDU2-Batería5	
341		ID_HEAD307				32			SMDU2Battery5 Voltage		Tensión SMDU2-Batería5	
342		ID_HEAD308				32			SMDU2Battery5 Capacity		Capacidad SMDU2-Batería5	
343		ID_HEAD309				32			SMDU3Battery5 Current		Corriente SMDU3-Batería5	
344		ID_HEAD310				32			SMDU3Battery5 Voltage		Tensión SMDU3-Batería5	
345		ID_HEAD311				32			SMDU3Battery5 Capacity		Capacidad SMDU3-Batería5	
346		ID_HEAD312				32			SMDU4Battery5 Current		Corriente SMDU4-Batería5	
347		ID_HEAD313				32			SMDU4Battery5 Voltage		Tensión SMDU4-Batería5	
348		ID_HEAD314				32			SMDU4Battery5 Capacity		Capacidad SMDU4-Batería5	
349		ID_HEAD315				32			SMDU5Battery5 Current		Corriente SMDU5-Batería5	
350		ID_HEAD316				32			SMDU5Battery5 Voltage		Tensión SMDU5-Batería5	
351		ID_HEAD317				32			SMDU5Battery5 Capacity		Capacidad SMDU5-Batería5	
352		ID_HEAD318				32			SMDU6Battery5 Current		Corriente SMDU6-Batería5	
353		ID_HEAD319				32			SMDU6Battery5 Voltage		Tensión SMDU6-Batería5	
354		ID_HEAD320				32			SMDU6Battery5 Capacity		Capacidad SMDU6-Batería5	
355		ID_HEAD321				32			SMDU7Battery5 Current		Corriente SMDU7-Batería5	
356		ID_HEAD322				32			SMDU7Battery5 Voltage		Tensión SMDU7-Batería5	
357		ID_HEAD323				32			SMDU7Battery5 Capacity		Capacidad SMDU7-Batería5	
358		ID_HEAD324				32			SMDU8Battery5 Current		Corriente SMDU8-Batería5	
359		ID_HEAD325				32			SMDU8Battery5 Voltage		Tensión SMDU8-Batería5	
360		ID_HEAD326				32			SMDU8Battery5 Capacity		Capacidad SMDU8-Batería5	
361		ID_HEAD327				32			SMBRCBattery1 Current		Corriente SMBRC-Batería1	
362		ID_HEAD328				32			SMBRCBattery1 Voltage		Tensión SMBRC-Batería1	
363		ID_HEAD329				32			SMBRCBattery1 Capacity		Capacidad SMBRC-Batería1	
364		ID_HEAD330				32			SMBRCBattery2 Current		Corriente SMBRC-Batería2	
365		ID_HEAD331				32			SMBRCBattery2 Voltage		Tensión SMBRC-Batería2	
366		ID_HEAD332				32			SMBRCBattery2 Capacity		Capacidad SMBRC-Batería2		
367		ID_HEAD333				32			SMBRCBattery3 Current		Corriente SMBRC-Batería3	
368		ID_HEAD334				32			SMBRCBattery3 Voltage		Tensión SMBRC-Batería3	
369		ID_HEAD335				32			SMBRCBattery3 Capacity		Capacidad SMBRC-Batería3		
370		ID_HEAD336				32			SMBRCBattery4 Current		Corriente SMBRC-Batería4	
371		ID_HEAD337				32			SMBRCBattery4 Voltage		Tensión SMBRC-Batería4	
372		ID_HEAD338				32			SMBRCBattery4 Capacity		Capacidad SMBRC-Batería4		
373		ID_HEAD339				32			SMBRCBattery5 Current		Corriente SMBRC-Batería5	
374		ID_HEAD340				32			SMBRCBattery5 Voltage		Tensión SMBRC-Batería5	
375		ID_HEAD341				32			SMBRCBattery5 Capacity		Capacidad SMBRC-Batería5		
376		ID_HEAD342				32			SMBRCBattery6 Current		Corriente SMBRC-Batería6	
377		ID_HEAD343				32			SMBRCBattery6 Voltage		Tensión SMBRC-Batería6	
378		ID_HEAD344				32			SMBRCBattery6 Capacity		Capacidad SMBRC-Batería6		
379		ID_HEAD345				32			SMBRCBattery7 Current		Corriente SMBRC-Batería7	
380		ID_HEAD346				32			SMBRCBattery7 Voltage		Tensión SMBRC-Batería7	
381		ID_HEAD347				32			SMBRCBattery7 Capacity		Capacidad SMBRC-Batería7		
382		ID_HEAD348				32			SMBRCBattery8 Current		Corriente SMBRC-Batería8	
383		ID_HEAD349				32			SMBRCBattery8 Voltage		Tensión SMBRC-Batería8	
384		ID_HEAD350				32			SMBRCBattery8 Capacity		Capacidad SMBRC-Batería8		
385		ID_HEAD351				32			SMBRCBattery9 Current		Corriente SMBRC-Batería9	
386		ID_HEAD352				32			SMBRCBattery9 Voltage		Tensión SMBRC-Batería9	
387		ID_HEAD353				32			SMBRCBattery9 Capacity		Capacidad SMBRC-Batería9		
388		ID_HEAD354				32			SMBRCBattery10 Current		Corriente SMBRC-Batería10	
389		ID_HEAD355				32			SMBRCBattery10 Voltage		Tensión SMBRC-Batería10	
390		ID_HEAD356				32			SMBRCBattery10 Capacity		Capacidad SMBRC-Batería10		
391		ID_HEAD357				32			SMBRCBattery11 Current		Corriente SMBRC-Batería11	
392		ID_HEAD358				32			SMBRCBattery11 Voltage		Tensión SMBRC-Batería11	
393		ID_HEAD359				32			SMBRCBattery11 Capacity		Capacidad SMBRC-Batería11		
394		ID_HEAD360				32			SMBRCBattery12 Current		Corriente SMBRC-Batería12	
395		ID_HEAD361				32			SMBRCBattery12 Voltage		Tensión SMBRC-Batería12	
396		ID_HEAD362				32			SMBRCBattery12 Capacity		Capacidad SMBRC-Batería12		
397		ID_HEAD363				32			SMBRCBattery13 Current		Corriente SMBRC-Batería13	
398		ID_HEAD364				32			SMBRCBattery13 Voltage		Tensión SMBRC-Batería13	
399		ID_HEAD365				32			SMBRCBattery13 Capacity		Capacidad SMBRC-Batería13		
400		ID_HEAD366				32			SMBRCBattery14 Current		Corriente SMBRC-Batería14	
401		ID_HEAD367				32			SMBRCBattery14 Voltage		Tensión SMBRC-Batería14	
402		ID_HEAD368				32			SMBRCBattery14 Capacity		Capacidad SMBRC-Batería14		
403		ID_HEAD369				32			SMBRCBattery15 Current		Corriente SMBRC-Batería15	
404		ID_HEAD370				32			SMBRCBattery15 Voltage		Tensión SMBRC-Batería15	
405		ID_HEAD371				32			SMBRCBattery15 Capacity		Capacidad SMBRC-Batería15		
406		ID_HEAD372				32			SMBRCBattery16 Current		Corriente SMBRC-Batería16	
407		ID_HEAD373				32			SMBRCBattery16 Voltage		Tensión SMBRC-Batería16	
408		ID_HEAD374				32			SMBRCBattery16 Capacity		Capacidad SMBRC-Batería16		
409		ID_HEAD375				32			SMBRCBattery17 Current		Corriente SMBRC-Batería17	
410		ID_HEAD376				32			SMBRCBattery17 Voltage		Tensión SMBRC-Batería17	
411		ID_HEAD377				32			SMBRCBattery17 Capacity		Capacidad SMBRC-Batería17		
412		ID_HEAD378				32			SMBRCBattery18 Current		Corriente SMBRC-Batería18	
413		ID_HEAD379				32			SMBRCBattery18 Voltage		Tensión SMBRC-Batería18	
414		ID_HEAD380				32			SMBRCBattery18 Capacity		Capacidad SMBRC-Batería18		
415		ID_HEAD381				32			SMBRCBattery19 Current		Corriente SMBRC-Batería19	
416		ID_HEAD382				32			SMBRCBattery19 Voltage		Tensión SMBRC-Batería19	
417		ID_HEAD383				32			SMBRCBattery19 Capacity		Capacidad SMBRC-Batería19		
418		ID_HEAD384				32			SMBRCBattery20 Current		Corriente SMBRC-Batería20	
419		ID_HEAD385				32			SMBRCBattery20 Voltage		Tensión SMBRC-Batería20	
420		ID_HEAD386				32			SMBRCBattery20 Capacity		Capacidad SMBRC-Batería20	
421		ID_HEAD387				32			SMBAT/BRC1 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC1	
422		ID_HEAD388				32			SMBAT/BRC1 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC1	
423		ID_HEAD389				32			SMBAT/BRC1 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC1	
424		ID_HEAD390				32			SMBAT/BRC1 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC1	
425		ID_HEAD391				32			SMBAT/BRC1 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC1	
426		ID_HEAD392				32			SMBAT/BRC1 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC1	
427		ID_HEAD393				32			SMBAT/BRC1 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC1	
428		ID_HEAD394				32			SMBAT/BRC1 BLOCK8  Voltage		Tensión celda8 SMBAT/BRC1	
429		ID_HEAD395				32			SMBAT/BRC1 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC1	
430		ID_HEAD396				32			SMBAT/BRC1 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC1	
431		ID_HEAD397				32			SMBAT/BRC1 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC1	
432		ID_HEAD398				32			SMBAT/BRC1 BLOCK12 Voltage		Tensión celda12 SMBAT/BRC1	
433		ID_HEAD399				32			SMBAT/BRC1 BLOCK13 Voltage		Tensión celda13 SMBAT/BRC1	
434		ID_HEAD400				32			SMBAT/BRC1 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC1	
435		ID_HEAD401				32			SMBAT/BRC1 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC1	
436		ID_HEAD402				32			SMBAT/BRC1 BLOCK16 Voltage		Tensión celda16 SMBAT/BRC1	
437		ID_HEAD403				32			SMBAT/BRC1 BLOCK17 Voltage		Tensión celda17 SMBAT/BRC1	
438		ID_HEAD404				32			SMBAT/BRC1 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC1	
439		ID_HEAD405				32			SMBAT/BRC1 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC1	
440		ID_HEAD406				32			SMBAT/BRC1 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC1	
441		ID_HEAD407				32			SMBAT/BRC1 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC1	
442		ID_HEAD408				32			SMBAT/BRC1 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC1	
443		ID_HEAD409				32			SMBAT/BRC1 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC1	
444		ID_HEAD410				32			SMBAT/BRC1 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC1	
445		ID_HEAD411				32			SMBAT/BRC2 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC2	
446		ID_HEAD412				32			SMBAT/BRC2 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC2		
447		ID_HEAD413				32			SMBAT/BRC2 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC2		
448		ID_HEAD414				32			SMBAT/BRC2 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC2		
449		ID_HEAD415				32			SMBAT/BRC2 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC2		
450		ID_HEAD416				32			SMBAT/BRC2 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC2		
451		ID_HEAD417				32			SMBAT/BRC2 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC2		
452		ID_HEAD418				32			SMBAT/BRC2 BLOCK8  Voltage		Tensión celda8 SMBAT/BRC2		
453		ID_HEAD419				32			SMBAT/BRC2 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC2		
454		ID_HEAD420				32			SMBAT/BRC2 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC2		
455		ID_HEAD421				32			SMBAT/BRC2 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC2		
456		ID_HEAD422				32			SMBAT/BRC2 BLOCK12 Voltage		Tensión celda12 SMBAT/BRC2		
457		ID_HEAD423				32			SMBAT/BRC2 BLOCK13 Voltage		Tensión celda13 SMBAT/BRC2		
458		ID_HEAD424				32			SMBAT/BRC2 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC2		
459		ID_HEAD425				32			SMBAT/BRC2 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC2		
460		ID_HEAD426				32			SMBAT/BRC2 BLOCK16 Voltage		Tensión celda16 SMBAT/BRC2		
461		ID_HEAD427				32			SMBAT/BRC2 BLOCK17 Voltage		Tensión celda17SMBAT/BRC2		
462		ID_HEAD428				32			SMBAT/BRC2 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC2		
463		ID_HEAD429				32			SMBAT/BRC2 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC2		
464		ID_HEAD430				32			SMBAT/BRC2 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC2		
465		ID_HEAD431				32			SMBAT/BRC2 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC2		
466		ID_HEAD432				32			SMBAT/BRC2 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC2		
467		ID_HEAD433				32			SMBAT/BRC2 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC2		
468		ID_HEAD434				32			SMBAT/BRC2 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC2		
469		ID_HEAD435				32			SMBAT/BRC3 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC3		
470		ID_HEAD436				32			SMBAT/BRC3 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC3		
471		ID_HEAD437				32			SMBAT/BRC3 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC3		
472		ID_HEAD438				32			SMBAT/BRC3 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC3		
473		ID_HEAD439				32			SMBAT/BRC3 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC3		
474		ID_HEAD440				32			SMBAT/BRC3 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC3		
475		ID_HEAD441				32			SMBAT/BRC3 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC3		
476		ID_HEAD442				32			SMBAT/BRC3 BLOCK8  Voltage		Tensión celda8 SMBAT/BRC3		
477		ID_HEAD443				32			SMBAT/BRC3 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC3		
478		ID_HEAD444				32			SMBAT/BRC3 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC3		
479		ID_HEAD445				32			SMBAT/BRC3 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC3		
480		ID_HEAD446				32			SMBAT/BRC3 BLOCK12 Voltage		Tensión celda12 SMBAT/BRC3		
481		ID_HEAD447				32			SMBAT/BRC3 BLOCK13 Voltage		Tensión celda13SMBAT/BRC3		
482		ID_HEAD448				32			SMBAT/BRC3 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC3		
483		ID_HEAD449				32			SMBAT/BRC3 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC3		
484		ID_HEAD450				32			SMBAT/BRC3 BLOCK16 Voltage		Tensión celda16 SMBAT/BRC3		
485		ID_HEAD451				32			SMBAT/BRC3 BLOCK17 Voltage		Tensión celda17 SMBAT/BRC3		
486		ID_HEAD452				32			SMBAT/BRC3 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC3		
487		ID_HEAD453				32			SMBAT/BRC3 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC3		
488		ID_HEAD454				32			SMBAT/BRC3 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC3		
489		ID_HEAD455				32			SMBAT/BRC3 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC3		
490		ID_HEAD456				32			SMBAT/BRC3 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC3		
491		ID_HEAD457				32			SMBAT/BRC3 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC3		
492		ID_HEAD458				32			SMBAT/BRC3 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC3		
493		ID_HEAD459				32			SMBAT/BRC4 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC4		
494		ID_HEAD460				32			SMBAT/BRC4 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC4		
495		ID_HEAD461				32			SMBAT/BRC4 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC4		
496		ID_HEAD462				32			SMBAT/BRC4 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC4		
497		ID_HEAD463				32			SMBAT/BRC4 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC4		
498		ID_HEAD464				32			SMBAT/BRC4 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC4		
499		ID_HEAD465				32			SMBAT/BRC4 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC4		
500		ID_HEAD466				32			SMBAT/BRC4 BLOCK8  Voltage		Tensión celda8 SMBAT/BRC4		
501		ID_HEAD467				32			SMBAT/BRC4 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC4		
502		ID_HEAD468				32			SMBAT/BRC4 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC4		
503		ID_HEAD469				32			SMBAT/BRC4 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC4		
504		ID_HEAD470				32			SMBAT/BRC4 BLOCK12 Voltage		Tensión celda12 SMBAT/BRC4		
505		ID_HEAD471				32			SMBAT/BRC4 BLOCK13 Voltage		Tensión celda13 SMBAT/BRC4		
506		ID_HEAD472				32			SMBAT/BRC4 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC4		
507		ID_HEAD473				32			SMBAT/BRC4 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC4		
508		ID_HEAD474				32			SMBAT/BRC4 BLOCK16 Voltage		Tensión celda16 SMBAT/BRC4		
509		ID_HEAD475				32			SMBAT/BRC4 BLOCK17 Voltage		Tensión celda17 SMBAT/BRC4		
510		ID_HEAD476				32			SMBAT/BRC4 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC4		
511		ID_HEAD477				32			SMBAT/BRC4 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC4		
512		ID_HEAD478				32			SMBAT/BRC4 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC4		
513		ID_HEAD479				32			SMBAT/BRC4 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC4		
514		ID_HEAD480				32			SMBAT/BRC4 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC4		
515		ID_HEAD481				32			SMBAT/BRC4 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC4		
516		ID_HEAD482				32			SMBAT/BRC4 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC4		
517		ID_HEAD483				32			SMBAT/BRC5 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC5		
518		ID_HEAD484				32			SMBAT/BRC5 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC5		
519		ID_HEAD485				32			SMBAT/BRC5 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC5		
520		ID_HEAD486				32			SMBAT/BRC5 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC5		
521		ID_HEAD487				32			SMBAT/BRC5 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC5		
522		ID_HEAD488				32			SMBAT/BRC5 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC5		
523		ID_HEAD489				32			SMBAT/BRC5 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC5		
524		ID_HEAD490				32			SMBAT/BRC5 BLOCK8  Voltage		Tensión celda8 SMBAT/BRC5		
525		ID_HEAD491				32			SMBAT/BRC5 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC5		
526		ID_HEAD492				32			SMBAT/BRC5 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC5		
527		ID_HEAD493				32			SMBAT/BRC5 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC5		
528		ID_HEAD494				32			SMBAT/BRC5 BLOCK12 Voltage		Tensión celda12 SMBAT/BRC5		
529		ID_HEAD495				32			SMBAT/BRC5 BLOCK13 Voltage		Tensión celda13 SMBAT/BRC5		
530		ID_HEAD496				32			SMBAT/BRC5 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC5		
531		ID_HEAD497				32			SMBAT/BRC5 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC5		
532		ID_HEAD498				32			SMBAT/BRC5 BLOCK16 Voltage		Tensión celda16 SMBAT/BRC5		
533		ID_HEAD499				32			SMBAT/BRC5 BLOCK17 Voltage		Tensión celda17 SMBAT/BRC5		
534		ID_HEAD500				32			SMBAT/BRC5 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC5		
535		ID_HEAD501				32			SMBAT/BRC5 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC5		
536		ID_HEAD502				32			SMBAT/BRC5 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC5		
537		ID_HEAD503				32			SMBAT/BRC5 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC5		
538		ID_HEAD504				32			SMBAT/BRC5 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC5		
539		ID_HEAD505				32			SMBAT/BRC5 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC5		
540		ID_HEAD506				32			SMBAT/BRC5 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC5		
541		ID_HEAD507				32			SMBAT/BRC6 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC6		
542		ID_HEAD508				32			SMBAT/BRC6 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC6		
543		ID_HEAD509				32			SMBAT/BRC6 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC6		
544		ID_HEAD510				32			SMBAT/BRC6 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC6		
545		ID_HEAD511				32			SMBAT/BRC6 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC6		
546		ID_HEAD512				32			SMBAT/BRC6 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC6		
547		ID_HEAD513				32			SMBAT/BRC6 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC6		
548		ID_HEAD514				32			SMBAT/BRC6 BLOCK8  Voltage		Tensión celda8 SMBAT/BRC6		
549		ID_HEAD515				32			SMBAT/BRC6 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC6		
550		ID_HEAD516				32			SMBAT/BRC6 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC6		
551		ID_HEAD517				32			SMBAT/BRC6 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC6		
552		ID_HEAD518				32			SMBAT/BRC6 BLOCK12 Voltage		Tensión celda12 SMBAT/BRC6		
553		ID_HEAD519				32			SMBAT/BRC6 BLOCK13 Voltage		Tensión celda13 SMBAT/BRC6		
554		ID_HEAD520				32			SMBAT/BRC6 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC6		
555		ID_HEAD521				32			SMBAT/BRC6 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC6		
556		ID_HEAD522				32			SMBAT/BRC6 BLOCK16 Voltage		Tensión celda16SMBAT/BRC6		
557		ID_HEAD523				32			SMBAT/BRC6 BLOCK17 Voltage		Tensión celda17 SMBAT/BRC6		
558		ID_HEAD524				32			SMBAT/BRC6 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC6		
559		ID_HEAD525				32			SMBAT/BRC6 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC6		
560		ID_HEAD526				32			SMBAT/BRC6 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC6		
561		ID_HEAD527				32			SMBAT/BRC6 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC6		
562		ID_HEAD528				32			SMBAT/BRC6 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC6		
563		ID_HEAD529				32			SMBAT/BRC6 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC6		
564		ID_HEAD530				32			SMBAT/BRC6 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC6		
565		ID_HEAD531				32			SMBAT/BRC7 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC7		
566		ID_HEAD532				32			SMBAT/BRC7 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC7	
567		ID_HEAD533				32			SMBAT/BRC7 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC7	
568		ID_HEAD534				32			SMBAT/BRC7 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC7	
569		ID_HEAD535				32			SMBAT/BRC7 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC7	
570		ID_HEAD536				32			SMBAT/BRC7 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC7	
571		ID_HEAD537				32			SMBAT/BRC7 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC7	
572		ID_HEAD538				32			SMBAT/BRC7 BLOCK8  Voltage		Tensión celda8 SMBAT/BRC7	
573		ID_HEAD539				32			SMBAT/BRC7 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC7	
574		ID_HEAD540				32			SMBAT/BRC7 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC7	
575		ID_HEAD541				32			SMBAT/BRC7 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC7	
576		ID_HEAD542				32			SMBAT/BRC7 BLOCK12 Voltage		Tensión celda12 SMBAT/BRC7	
577		ID_HEAD543				32			SMBAT/BRC7 BLOCK13 Voltage		Tensión celda13 SMBAT/BRC7	
578		ID_HEAD544				32			SMBAT/BRC7 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC7	
579		ID_HEAD545				32			SMBAT/BRC7 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC7	
580		ID_HEAD546				32			SMBAT/BRC7 BLOCK16 Voltage		Tensión celda16SMBAT/BRC7	
581		ID_HEAD547				32			SMBAT/BRC7 BLOCK17 Voltage		Tensión celda17 SMBAT/BRC7	
582		ID_HEAD548				32			SMBAT/BRC7 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC7	
583		ID_HEAD549				32			SMBAT/BRC7 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC7	
584		ID_HEAD550				32			SMBAT/BRC7 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC7	
585		ID_HEAD551				32			SMBAT/BRC7 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC7	
586		ID_HEAD552				32			SMBAT/BRC7 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC7	
587		ID_HEAD553				32			SMBAT/BRC7 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC7	
588		ID_HEAD554				32			SMBAT/BRC7 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC7	
589		ID_HEAD555				32			SMBAT/BRC8 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC8	
590		ID_HEAD556				32			SMBAT/BRC8 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC8		
591		ID_HEAD557				32			SMBAT/BRC8 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC8		
592		ID_HEAD558				32			SMBAT/BRC8 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC8		
593		ID_HEAD559				32			SMBAT/BRC8 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC8		
594		ID_HEAD560				32			SMBAT/BRC8 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC8		
595		ID_HEAD561				32			SMBAT/BRC8 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC8		
596		ID_HEAD562				32			SMBAT/BRC8 BLOCK8  Voltage		Tensión celda8 SMBAT/BRC8		
597		ID_HEAD563				32			SMBAT/BRC8 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC8		
598		ID_HEAD564				32			SMBAT/BRC8 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC8		
599		ID_HEAD565				32			SMBAT/BRC8 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC8		
600		ID_HEAD566				32			SMBAT/BRC8 BLOCK12 Voltage		Tensión celda12SMBAT/BRC8		
601		ID_HEAD567				32			SMBAT/BRC8 BLOCK13 Voltage		Tensión celda13 SMBAT/BRC8		
602		ID_HEAD568				32			SMBAT/BRC8 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC8		
603		ID_HEAD569				32			SMBAT/BRC8 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC8		
604		ID_HEAD570				32			SMBAT/BRC8 BLOCK16 Voltage		Tensión celda16 SMBAT/BRC8		
605		ID_HEAD571				32			SMBAT/BRC8 BLOCK17 Voltage		Tensión celda17 SMBAT/BRC8		
606		ID_HEAD572				32			SMBAT/BRC8 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC8		
607		ID_HEAD573				32			SMBAT/BRC8 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC8		
608		ID_HEAD574				32			SMBAT/BRC8 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC8		
609		ID_HEAD575				32			SMBAT/BRC8 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC8		
610		ID_HEAD576				32			SMBAT/BRC8 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC8		
611		ID_HEAD577				32			SMBAT/BRC8 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC8		
612		ID_HEAD578				32			SMBAT/BRC8 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC8		
613		ID_HEAD579				32			SMBAT/BRC9 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC9		
614		ID_HEAD580				32			SMBAT/BRC9 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC9		
615		ID_HEAD581				32			SMBAT/BRC9 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC9		
616		ID_HEAD582				32			SMBAT/BRC9 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC9		
617		ID_HEAD583				32			SMBAT/BRC9 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC9		
618		ID_HEAD584				32			SMBAT/BRC9 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC9		
619		ID_HEAD585				32			SMBAT/BRC9 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC9		
620		ID_HEAD586				32			SMBAT/BRC9 BLOCK8  Voltage		Tensión celda8 SMBAT/BRC9		
621		ID_HEAD587				32			SMBAT/BRC9 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC9		
622		ID_HEAD588				32			SMBAT/BRC9 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC9		
623		ID_HEAD589				32			SMBAT/BRC9 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC9		
624		ID_HEAD590				32			SMBAT/BRC9 BLOCK12 Voltage		Tensión celda12 SMBAT/BRC9		
625		ID_HEAD591				32			SMBAT/BRC9 BLOCK13 Voltage		Tensión celda13 SMBAT/BRC9		
626		ID_HEAD592				32			SMBAT/BRC9 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC9		
627		ID_HEAD593				32			SMBAT/BRC9 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC9		
628		ID_HEAD594				32			SMBAT/BRC9 BLOCK16 Voltage		Tensión celda16 SMBAT/BRC9		
629		ID_HEAD595				32			SMBAT/BRC9 BLOCK17 Voltage		Tensión celda17 SMBAT/BRC9		
630		ID_HEAD596				32			SMBAT/BRC9 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC9		
631		ID_HEAD597				32			SMBAT/BRC9 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC9		
632		ID_HEAD598				32			SMBAT/BRC9 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC9		
633		ID_HEAD599				32			SMBAT/BRC9 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC9		
634		ID_HEAD600				32			SMBAT/BRC9 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC9		
635		ID_HEAD601				32			SMBAT/BRC9 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC9		
636		ID_HEAD602				32			SMBAT/BRC9 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC9		
637		ID_HEAD603				32			SMBAT/BRC10 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC10	
638		ID_HEAD604				32			SMBAT/BRC10 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC10	
639		ID_HEAD605				32			SMBAT/BRC10 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC10	
640		ID_HEAD606				32			SMBAT/BRC10 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC10	
641		ID_HEAD607				32			SMBAT/BRC10 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC10	
642		ID_HEAD608				32			SMBAT/BRC10 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC10	
643		ID_HEAD609				32			SMBAT/BRC10 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC10	
644		ID_HEAD610				32			SMBAT/BRC10 BLOCK8  Voltage		Tensión celda8 SMBAT/BRC10	
645		ID_HEAD611				32			SMBAT/BRC10 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC10	
646		ID_HEAD612				32			SMBAT/BRC10 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC10	
647		ID_HEAD613				32			SMBAT/BRC10 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC10	
648		ID_HEAD614				32			SMBAT/BRC10 BLOCK12 Voltage		Tensión celda12 SMBAT/BRC10	
649		ID_HEAD615				32			SMBAT/BRC10 BLOCK13 Voltage		Tensión celda13 SMBAT/BRC10	
650		ID_HEAD616				32			SMBAT/BRC10 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC10	
651		ID_HEAD617				32			SMBAT/BRC10 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC10	
652		ID_HEAD618				32			SMBAT/BRC10 BLOCK16 Voltage		Tensión celda16 SMBAT/BRC10	
653		ID_HEAD619				32			SMBAT/BRC10 BLOCK17 Voltage		Tensión celda17 SMBAT/BRC10	
654		ID_HEAD620				32			SMBAT/BRC10 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC10	
655		ID_HEAD621				32			SMBAT/BRC10 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC10	
656		ID_HEAD622				32			SMBAT/BRC10 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC10	
657		ID_HEAD623				32			SMBAT/BRC10 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC10	
658		ID_HEAD624				32			SMBAT/BRC10 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC10	
659		ID_HEAD625				32			SMBAT/BRC10 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC10	
660		ID_HEAD626				32			SMBAT/BRC10 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC10	
661		ID_HEAD627				32			SMBAT/BRC11 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC11	
662		ID_HEAD628				32			SMBAT/BRC11 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC11	
663		ID_HEAD629				32			SMBAT/BRC11 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC11	
664		ID_HEAD630				32			SMBAT/BRC11 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC11	
665		ID_HEAD631				32			SMBAT/BRC11 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC11	
666		ID_HEAD632				32			SMBAT/BRC11 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC11	
667		ID_HEAD633				32			SMBAT/BRC11 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC11	
668		ID_HEAD634				32			SMBAT/BRC11 BLOCK8  Voltage		Tensión celda8 SMBAT/BRC11
669		ID_HEAD635				32			SMBAT/BRC11 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC11	
670		ID_HEAD636				32			SMBAT/BRC11 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC11	
671		ID_HEAD637				32			SMBAT/BRC11 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC11	
672		ID_HEAD638				32			SMBAT/BRC11 BLOCK12 Voltage		Tensión celda12 SMBAT/BRC11	
673		ID_HEAD639				32			SMBAT/BRC11 BLOCK13 Voltage		Tensión celda13 SMBAT/BRC11	
674		ID_HEAD640				32			SMBAT/BRC11 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC11	
675		ID_HEAD641				32			SMBAT/BRC11 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC11	
676		ID_HEAD642				32			SMBAT/BRC11 BLOCK16 Voltage		Tensión celda16 SMBAT/BRC11	
677		ID_HEAD643				32			SMBAT/BRC11 BLOCK17 Voltage		Tensión celda17 SMBAT/BRC11	
678		ID_HEAD644				32			SMBAT/BRC11 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC11	
679		ID_HEAD645				32			SMBAT/BRC11 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC11	
680		ID_HEAD646				32			SMBAT/BRC11 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC11	
681		ID_HEAD647				32			SMBAT/BRC11 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC11	
682		ID_HEAD648				32			SMBAT/BRC11 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC11	
683		ID_HEAD649				32			SMBAT/BRC11 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC11	
684		ID_HEAD650				32			SMBAT/BRC11 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC11	
685		ID_HEAD651				32			SMBAT/BRC12 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC12	
686		ID_HEAD652				32			SMBAT/BRC12 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC12	
687		ID_HEAD653				32			SMBAT/BRC12 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC12	
688		ID_HEAD654				32			SMBAT/BRC12 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC12	
689		ID_HEAD655				32			SMBAT/BRC12 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC12	
690		ID_HEAD656				32			SMBAT/BRC12 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC12	
691		ID_HEAD657				32			SMBAT/BRC12 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC12	
692		ID_HEAD658				32			SMBAT/BRC12 BLOCK8  Voltage		Tensión celda8 SMBAT/BRC12	
693		ID_HEAD659				32			SMBAT/BRC12 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC12	
694		ID_HEAD660				32			SMBAT/BRC12 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC12	
695		ID_HEAD661				32			SMBAT/BRC12 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC12	
696		ID_HEAD662				32			SMBAT/BRC12 BLOCK12 Voltage		Tensión celda12 SMBAT/BRC12	
697		ID_HEAD663				32			SMBAT/BRC12 BLOCK13 Voltage		Tensión celda13 SMBAT/BRC12	
698		ID_HEAD664				32			SMBAT/BRC12 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC12	
699		ID_HEAD665				32			SMBAT/BRC12 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC12	
700		ID_HEAD666				32			SMBAT/BRC12 BLOCK16 Voltage		Tensión celda16 SMBAT/BRC12	
701		ID_HEAD667				32			SMBAT/BRC12 BLOCK17 Voltage		Tensión celda17 SMBAT/BRC12	
702		ID_HEAD668				32			SMBAT/BRC12 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC12	
703		ID_HEAD669				32			SMBAT/BRC12 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC12	
704		ID_HEAD670				32			SMBAT/BRC12 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC12	
705		ID_HEAD671				32			SMBAT/BRC12 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC12	
706		ID_HEAD672				32			SMBAT/BRC12 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC12	
707		ID_HEAD673				32			SMBAT/BRC12 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC12	
708		ID_HEAD674				32			SMBAT/BRC12 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC12	
709		ID_HEAD675				32			SMBAT/BRC13 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC13	
710		ID_HEAD676				32			SMBAT/BRC13 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC13	
711		ID_HEAD677				32			SMBAT/BRC13 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC13	
712		ID_HEAD678				32			SMBAT/BRC13 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC13	
713		ID_HEAD679				32			SMBAT/BRC13 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC13	
714		ID_HEAD680				32			SMBAT/BRC13 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC13	
715		ID_HEAD681				32			SMBAT/BRC13 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC13	
716		ID_HEAD682				32			SMBAT/BRC13 BLOCK8  Voltage		Tensión celda8SMBAT/BRC13	
717		ID_HEAD683				32			SMBAT/BRC13 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC13	
718		ID_HEAD684				32			SMBAT/BRC13 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC13	
719		ID_HEAD685				32			SMBAT/BRC13 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC13	
720		ID_HEAD686				32			SMBAT/BRC13 BLOCK12 Voltage		Tensión celda12 SMBAT/BRC13	
721		ID_HEAD687				32			SMBAT/BRC13 BLOCK13 Voltage		Tensión celda13 SMBAT/BRC13	
722		ID_HEAD688				32			SMBAT/BRC13 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC13	
723		ID_HEAD689				32			SMBAT/BRC13 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC13	
724		ID_HEAD690				32			SMBAT/BRC13 BLOCK16 Voltage		Tensión celda16 SMBAT/BRC13	
725		ID_HEAD691				32			SMBAT/BRC13 BLOCK17 Voltage		Tensión celda17 SMBAT/BRC13	
726		ID_HEAD692				32			SMBAT/BRC13 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC13	
727		ID_HEAD693				32			SMBAT/BRC13 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC13	
728		ID_HEAD694				32			SMBAT/BRC13 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC13	
729		ID_HEAD695				32			SMBAT/BRC13 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC13	
730		ID_HEAD696				32			SMBAT/BRC13 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC13	
731		ID_HEAD697				32			SMBAT/BRC13 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC13	
732		ID_HEAD698				32			SMBAT/BRC13 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC13	
733		ID_HEAD699				32			SMBAT/BRC14 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC14	
734		ID_HEAD700				32			SMBAT/BRC14 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC14	
735		ID_HEAD701				32			SMBAT/BRC14 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC14	
736		ID_HEAD702				32			SMBAT/BRC14 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC14	
737		ID_HEAD703				32			SMBAT/BRC14 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC14	
738		ID_HEAD704				32			SMBAT/BRC14 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC14	
739		ID_HEAD705				32			SMBAT/BRC14 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC14	
740		ID_HEAD706				32			SMBAT/BRC14 BLOCK8  Voltage		Tensión celda8 SMBAT/BRC14	
741		ID_HEAD707				32			SMBAT/BRC14 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC14	
742		ID_HEAD708				32			SMBAT/BRC14 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC14	
743		ID_HEAD709				32			SMBAT/BRC14 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC14	
744		ID_HEAD710				32			SMBAT/BRC14 BLOCK12 Voltage		Tensión celda12 SMBAT/BRC14	
745		ID_HEAD711				32			SMBAT/BRC14 BLOCK13 Voltage		Tensión celda13 SMBAT/BRC14	
746		ID_HEAD712				32			SMBAT/BRC14 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC14	
747		ID_HEAD713				32			SMBAT/BRC14 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC14	
748		ID_HEAD714				32			SMBAT/BRC14 BLOCK16 Voltage		Tensión celda16 SMBAT/BRC14	
749		ID_HEAD715				32			SMBAT/BRC14 BLOCK17 Voltage		Tensión celda17 SMBAT/BRC14	
750		ID_HEAD716				32			SMBAT/BRC14 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC14	
751		ID_HEAD717				32			SMBAT/BRC14 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC14	
752		ID_HEAD718				32			SMBAT/BRC14 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC14	
753		ID_HEAD719				32			SMBAT/BRC14 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC14	
754		ID_HEAD720				32			SMBAT/BRC14 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC14	
755		ID_HEAD721				32			SMBAT/BRC14 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC14	
756		ID_HEAD722				32			SMBAT/BRC14 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC14	
757		ID_HEAD723				32			SMBAT/BRC15 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC15	
758		ID_HEAD724				32			SMBAT/BRC15 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC15	
759		ID_HEAD725				32			SMBAT/BRC15 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC15	
760		ID_HEAD726				32			SMBAT/BRC15 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC15	
761		ID_HEAD727				32			SMBAT/BRC15 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC15	
762		ID_HEAD728				32			SMBAT/BRC15 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC15	
763		ID_HEAD729				32			SMBAT/BRC15 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC15	
764		ID_HEAD730				32			SMBAT/BRC15 BLOCK8  Voltage		Tensión celda8 SMBAT/BRC15	
765		ID_HEAD731				32			SMBAT/BRC15 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC15	
766		ID_HEAD732				32			SMBAT/BRC15 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC15	
767		ID_HEAD733				32			SMBAT/BRC15 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC15	
768		ID_HEAD734				32			SMBAT/BRC15 BLOCK12 Voltage		Tensión celda12 SMBAT/BRC15	
769		ID_HEAD735				32			SMBAT/BRC15 BLOCK13 Voltage		Tensión celda13 SMBAT/BRC15	
770		ID_HEAD736				32			SMBAT/BRC15 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC15	
771		ID_HEAD737				32			SMBAT/BRC15 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC15	
772		ID_HEAD738				32			SMBAT/BRC15 BLOCK16 Voltage		Tensión celda16 SMBAT/BRC15	
773		ID_HEAD739				32			SMBAT/BRC15 BLOCK17 Voltage		Tensión celda17 SMBAT/BRC15	
774		ID_HEAD740				32			SMBAT/BRC15 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC15	
775		ID_HEAD741				32			SMBAT/BRC15 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC15	
776		ID_HEAD742				32			SMBAT/BRC15 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC15	
777		ID_HEAD743				32			SMBAT/BRC15 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC15	
778		ID_HEAD744				32			SMBAT/BRC15 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC15	
779		ID_HEAD745				32			SMBAT/BRC15 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC15	
780		ID_HEAD746				32			SMBAT/BRC15 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC15	
781		ID_HEAD747				32			SMBAT/BRC16 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC16	
782		ID_HEAD748				32			SMBAT/BRC16 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC16	
783		ID_HEAD749				32			SMBAT/BRC16 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC16	
784		ID_HEAD750				32			SMBAT/BRC16 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC16	
785		ID_HEAD751				32			SMBAT/BRC16 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC16	
786		ID_HEAD752				32			SMBAT/BRC16 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC16	
787		ID_HEAD753				32			SMBAT/BRC16 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC16	
788		ID_HEAD754				32			SMBAT/BRC16 BLOCK8  Voltage		Tensión celda8 SMBAT/BRC16	
789		ID_HEAD755				32			SMBAT/BRC16 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC16	
790		ID_HEAD756				32			SMBAT/BRC16 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC16	
791		ID_HEAD757				32			SMBAT/BRC16 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC16	
792		ID_HEAD758				32			SMBAT/BRC16 BLOCK12 Voltage		Tensión celda12 SMBAT/BRC16	
793		ID_HEAD759				32			SMBAT/BRC16 BLOCK13 Voltage		Tensión celda13 SMBAT/BRC16	
794		ID_HEAD760				32			SMBAT/BRC16 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC16	
795		ID_HEAD761				32			SMBAT/BRC16 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC16	
796		ID_HEAD762				32			SMBAT/BRC16 BLOCK16 Voltage		Tensión celda16 SMBAT/BRC16	
797		ID_HEAD763				32			SMBAT/BRC16 BLOCK17 Voltage		Tensión celda17 SMBAT/BRC16	
798		ID_HEAD764				32			SMBAT/BRC16 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC16	
799		ID_HEAD765				32			SMBAT/BRC16 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC16	
800		ID_HEAD766				32			SMBAT/BRC16 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC16	
801		ID_HEAD767				32			SMBAT/BRC16 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC16	
802		ID_HEAD768				32			SMBAT/BRC16 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC16	
803		ID_HEAD769				32			SMBAT/BRC16 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC16	
804		ID_HEAD770				32			SMBAT/BRC16 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC16	
805		ID_HEAD771				32			SMBAT/BRC17 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC17	
806		ID_HEAD772				32			SMBAT/BRC17 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC17	
807		ID_HEAD773				32			SMBAT/BRC17 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC17	
808		ID_HEAD774				32			SMBAT/BRC17 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC17	
809		ID_HEAD775				32			SMBAT/BRC17 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC17	
810		ID_HEAD776				32			SMBAT/BRC17 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC17	
811		ID_HEAD777				32			SMBAT/BRC17 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC17	
812		ID_HEAD778				32			SMBAT/BRC17 BLOCK8  Voltage		Tensión celda8 SMBAT/BRC17	
813		ID_HEAD779				32			SMBAT/BRC17 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC17	
814		ID_HEAD780				32			SMBAT/BRC17 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC17	
815		ID_HEAD781				32			SMBAT/BRC17 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC17	
816		ID_HEAD782				32			SMBAT/BRC17 BLOCK12 Voltage		Tensión celda12 SMBAT/BRC17	
817		ID_HEAD783				32			SMBAT/BRC17 BLOCK13 Voltage		Tensión celda13 SMBAT/BRC17	
818		ID_HEAD784				32			SMBAT/BRC17 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC17	
819		ID_HEAD785				32			SMBAT/BRC17 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC17	
820		ID_HEAD786				32			SMBAT/BRC17 BLOCK16 Voltage		Tensión celda16 SMBAT/BRC17	
821		ID_HEAD787				32			SMBAT/BRC17 BLOCK17 Voltage		Tensión celda17 SMBAT/BRC17	
822		ID_HEAD788				32			SMBAT/BRC17 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC17	
823		ID_HEAD789				32			SMBAT/BRC17 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC17	
824		ID_HEAD790				32			SMBAT/BRC17 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC17	
825		ID_HEAD791				32			SMBAT/BRC17 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC17	
826		ID_HEAD792				32			SMBAT/BRC17 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC17	
827		ID_HEAD793				32			SMBAT/BRC17 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC17	
828		ID_HEAD794				32			SMBAT/BRC17 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC17	
829		ID_HEAD795				32			SMBAT/BRC18 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC18	
830		ID_HEAD796				32			SMBAT/BRC18 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC18	
831		ID_HEAD797				32			SMBAT/BRC18 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC18	
832		ID_HEAD798				32			SMBAT/BRC18 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC18	
833		ID_HEAD799				32			SMBAT/BRC18 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC18	
834		ID_HEAD800				32			SMBAT/BRC18 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC18	
835		ID_HEAD801				32			SMBAT/BRC18 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC18	
836		ID_HEAD802				32			SMBAT/BRC18 BLOCK8  Voltage		Tensión celda8 SMBAT/BRC18	
837		ID_HEAD803				32			SMBAT/BRC18 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC18	
838		ID_HEAD804				32			SMBAT/BRC18 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC18	
839		ID_HEAD805				32			SMBAT/BRC18 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC18	
840		ID_HEAD806				32			SMBAT/BRC18 BLOCK12 Voltage		Tensión celda12 SMBAT/BRC18 
841		ID_HEAD807				32			SMBAT/BRC18 BLOCK13 Voltage		Tensión celda13 SMBAT/BRC18	
842		ID_HEAD808				32			SMBAT/BRC18 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC18 
843		ID_HEAD809				32			SMBAT/BRC18 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC18	
844		ID_HEAD810				32			SMBAT/BRC18 BLOCK16 Voltage		Tensión celda16 SMBAT/BRC18	
845		ID_HEAD811				32			SMBAT/BRC18 BLOCK17 Voltage		Tensión celda17 SMBAT/BRC18	
846		ID_HEAD812				32			SMBAT/BRC18 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC18	
847		ID_HEAD813				32			SMBAT/BRC18 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC18	
848		ID_HEAD814				32			SMBAT/BRC18 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC18	
849		ID_HEAD815				32			SMBAT/BRC18 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC18	
850		ID_HEAD816				32			SMBAT/BRC18 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC18	
851		ID_HEAD817				32			SMBAT/BRC18 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC18	
852		ID_HEAD818				32			SMBAT/BRC18 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC18	
853		ID_HEAD819				32			SMBAT/BRC19 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC19	
854		ID_HEAD820				32			SMBAT/BRC19 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC19	
855		ID_HEAD821				32			SMBAT/BRC19 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC19	
856		ID_HEAD822				32			SMBAT/BRC19 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC19	
857		ID_HEAD823				32			SMBAT/BRC19 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC19	
858		ID_HEAD824				32			SMBAT/BRC19 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC19	
859		ID_HEAD825				32			SMBAT/BRC19 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC19	
860		ID_HEAD826				32			SMBAT/BRC19 BLOCK8  Voltage		Tensión celda8 SMBAT/BRC19	
861		ID_HEAD827				32			SMBAT/BRC19 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC19	
862		ID_HEAD828				32			SMBAT/BRC19 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC19	
863		ID_HEAD829				32			SMBAT/BRC19 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC19	
864		ID_HEAD830				32			SMBAT/BRC19 BLOCK12 Voltage		Tensión celda12 SMBAT/BRC19	
865		ID_HEAD831				32			SMBAT/BRC19 BLOCK13 Voltage		Tensión celda13 SMBAT/BRC19	
866		ID_HEAD832				32			SMBAT/BRC19 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC19	
867		ID_HEAD833				32			SMBAT/BRC19 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC19	
868		ID_HEAD834				32			SMBAT/BRC19 BLOCK16 Voltage		Tensión celda16 SMBAT/BRC19	
869		ID_HEAD835				32			SMBAT/BRC19 BLOCK17 Voltage		Tensión celda17 SMBAT/BRC19	
870		ID_HEAD836				32			SMBAT/BRC19 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC19	
871		ID_HEAD837				32			SMBAT/BRC19 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC19	
872		ID_HEAD838				32			SMBAT/BRC19 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC19	
873		ID_HEAD839				32			SMBAT/BRC19 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC19	
874		ID_HEAD840				32			SMBAT/BRC19 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC19	
875		ID_HEAD841				32			SMBAT/BRC19 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC19	
876		ID_HEAD842				32			SMBAT/BRC19 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC19	
877		ID_HEAD843				32			SMBAT/BRC20 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC20	
878		ID_HEAD844				32			SMBAT/BRC20 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC20	
879		ID_HEAD845				32			SMBAT/BRC20 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC20	
880		ID_HEAD846				32			SMBAT/BRC20 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC20	
881		ID_HEAD847				32			SMBAT/BRC20 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC20	
882		ID_HEAD848				32			SMBAT/BRC20 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC20	
883		ID_HEAD849				32			SMBAT/BRC20 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC20	
884		ID_HEAD850				32			SMBAT/BRC20 BLOCK8  Voltage		Tensión celda8 SMBAT/BRC20	
885		ID_HEAD851				32			SMBAT/BRC20 BLOCK9  Voltage		Tensión celda9 SMBAT/BRC20	
886		ID_HEAD852				32			SMBAT/BRC20 BLOCK10 Voltage		Tensión celda10 SMBAT/BRC20
887		ID_HEAD853				32			SMBAT/BRC20 BLOCK11 Voltage		Tensión celda11 SMBAT/BRC20	
888		ID_HEAD854				32			SMBAT/BRC20 BLOCK12 Voltage		Tensión celda12 SMBAT/BRC20	
889		ID_HEAD855				32			SMBAT/BRC20 BLOCK13 Voltage		Tensión celda13 SMBAT/BRC20	
890		ID_HEAD856				32			SMBAT/BRC20 BLOCK14 Voltage		Tensión celda14 SMBAT/BRC20	
891		ID_HEAD857				32			SMBAT/BRC20 BLOCK15 Voltage		Tensión celda15 SMBAT/BRC20	
892		ID_HEAD858				32			SMBAT/BRC20 BLOCK16 Voltage		Tensión celda16 SMBAT/BRC20
893		ID_HEAD859				32			SMBAT/BRC20 BLOCK17 Voltage		Tensión celda17 SMBAT/BRC20	
894		ID_HEAD860				32			SMBAT/BRC20 BLOCK18 Voltage		Tensión celda18 SMBAT/BRC20	
895		ID_HEAD861				32			SMBAT/BRC20 BLOCK19 Voltage		Tensión celda19 SMBAT/BRC20	
896		ID_HEAD862				32			SMBAT/BRC20 BLOCK20 Voltage		Tensión celda20 SMBAT/BRC20	
897		ID_HEAD863				32			SMBAT/BRC20 BLOCK21 Voltage		Tensión celda21 SMBAT/BRC20	
898		ID_HEAD864				32			SMBAT/BRC20 BLOCK22 Voltage		Tensión celda22 SMBAT/BRC20	
899		ID_HEAD865				32			SMBAT/BRC20 BLOCK23 Voltage		Tensión celda23 SMBAT/BRC20	
900		ID_HEAD866				32			SMBAT/BRC20 BLOCK24 Voltage		Tensión celda24 SMBAT/BRC20	
901		ID_TIPS1				32			Search for data			Buscar datos
902		ID_TIPS2				32			Please select row			Seleccionar línea
903		ID_TIPS3				32			Please select column			Seleccionar fila
904		ID_BATT_TEST				32			    Battery Test Log			Registro de Prueba de Baterías
905		ID_TIPS4				32			Please select column			Seleccionar fila
906		ID_TIPS5				32			Please select row			Seleccionar línea
907		ID_HEAD867				32			SMBAT/BRC20 BLOCK1  Voltage		Tensión celda1 SMBAT/BRC20	
908		ID_HEAD868				32			SMBAT/BRC20 BLOCK2  Voltage		Tensión celda2 SMBAT/BRC20	
909		ID_HEAD869				32			SMBAT/BRC20 BLOCK3  Voltage		Tensión celda3 SMBAT/BRC20	
910		ID_HEAD870				32			SMBAT/BRC20 BLOCK4  Voltage		Tensión celda4 SMBAT/BRC20	
911		ID_HEAD871				32			SMBAT/BRC20 BLOCK5  Voltage		Tensión celda5 SMBAT/BRC20	
912		ID_HEAD872				32			SMBAT/BRC20 BLOCK6  Voltage		Tensión celda6 SMBAT/BRC20	
913		ID_HEAD873				32			SMBAT/BRC20 BLOCK7  Voltage		Tensión celda7 SMBAT/BRC20
[tmp.history_eventlog.html:Number]
23

[tmp.history_eventlog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_INDEX				32			Index				Índice
2		ID_EQUIP				32			Device Name			Nombre de Equipo
3		ID_SIGNAL				32			Signal Name				Nombre de Señal
4		ID_CONTROL_VALUE			32			Value			Valor 
5		ID_UNIT					32			Unit				Unidad
6		ID_TIME					32			Time			Tiempo
7		ID_SENDER_NAME				32			Sender Name				Nombre Remitente
8		ID_FROM					32			From				Desde
9		ID_TO					32			To					Hasta
10		ID_QUERY				32			Query				Consulta
11		ID_UPLOAD				32			Upload				Descargar
12		ID_TIPS					64			Displays the last 500 entries		Muestra los últimos 500 registros
13		ID_QUERY_TYPE				16			Query Type				Tipo de Consulta
14		ID_EVENT_LOG				32			Event Log				Registro de Eventos
15		ID_SENDER_TYPE				32			Sender Type				Tipo de Remitente
16		ID_CTL_RESULT0				64			Successful				Correcto		
17		ID_CTL_RESULT1				64			No Memory				Sin memoria		
18		ID_CTL_RESULT2				64			Time Expired			Tiempo Agotado		
19		ID_CTL_RESULT3				64			Failed				Fallo		
20		ID_CTL_RESULT4				64			Communication Busy			Comunicación Ocupada	
21		ID_CTL_RESULT5				64			Control was suppressed.		El Control fue suprimido	
22		ID_CTL_RESULT6				64			Control was disabled.		El Control fue deshabilitado
23		ID_CTL_RESULT7				64			Control was canceled.		El Control fue cancelado	
24		ID_EVENT_LOG2				16			    Event Log				Registro de Eventos
25		ID_EVENT				32			    Event History Log				Historial de eventos

[tmp.history_datalog.html:Number]
15

[tmp.history_datalog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_DEVICE				32			Device				Equipo
2		ID_FROM					32			From				Desde
3		ID_TO					32			To					Hasta
4		ID_QUERY				32			Query				Consulta
5		ID_UPLOAD				32			Upload				Descargar
6		ID_TIPS					64			Displays the last 500 entries		Muestra los últimos 500 registros
7		ID_INDEX				32			Index				Índice
8		ID_DEVICE1				32			Device Name				Equipo
9		ID_SIGNAL				32			Signal Name				Nombre de Señal
10		ID_VALUE				32			Value				Valor
11		ID_UNIT					32			Unit				Unidad
12		ID_TIME					32			Time				Tiempo
13		ID_ALL_DEVICE				32			All Devices				Todos los equipos
14		ID_DATA					32			    Data History Log			Datos Históricos
15		ID_ALL_DEVICE2				32			    All Devices				Todos los equipos

[tmp.setting_charge.html:Number]
20

[tmp.setting_charge.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
6		ID_SIGNAL				32			Signal				Señal
7		ID_VALUE				32			Value				Valor
8		ID_TIME					32			Time Last Set			Ultimo Ajuste
9		ID_SET_VALUE				32			Set Value				Ajustar Valor
10		ID_SET					32			Set					Fijar
11		ID_SUCCESS				32			Setting Success			Ajuste correcto
12		ID_OVER_TIME				32			Login Time Expired			Tiempo de conexión expirado
13		ID_FAIL					32			Setting Fail			Fallo de ajuste
14		ID_NO_AUTHORITY				32			No privilege			Sin autoridad
15		ID_UNKNOWN_ERROR			32			Unknown Error			Error Desconocido
16		ID_PROTECT				32			Write Protected			Protegido contra escritura
17		ID_SET1					32			Set					Fijar
18		ID_CHARGE1				32			Battery Charge			Carga Batería
23		ID_NO_DATA				16			No data.				No hay datos	




[tmp.system_converter.html:Number]
16

[tmp.system_converter.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_RECTIFIER				32			Rectifier				Rectificadores
2		ID_CONVERTER				32			Converter				Convertidor
3		ID_SOLAR				32			Solar Converter				Convertidor Solar
4		ID_VOLTAGE				32			Average Voltage			Tensión media
5		ID_CURRENT				32			Total Current			Corriente Total
6		ID_CAPACITY_USED			32			System Capacity Used		Capacidad utilizada Sistema
7		ID_NUM_OF_RECT				32			Number of Converters		Número de Convertidores
8		ID_TOTAL_COMM_RECT			64			Total Converters Communicating	Número de Convertidores en comunicación
9		ID_MAX_CAPACITY				32			Max Used Capacity			Máxima Capacidad Utilizada
10		ID_SIGNAL				32			Signal				Señal
11		ID_VALUE				32			Value				Valor
12		ID_SOLAR1				32			Solar Converter				Convertidor Solar
13		ID_CURRENT1				32			Total Current			Corriente Total
14		ID_RECTIFIER1				32			GI Rectifier			Rectificadores GI
15		ID_RECTIFIER2				32			GII Rectifier			Rectificadores GII
16		ID_RECTIFIER3				32			GIII Rectifier			Rectificadores GIII
#//changed by Frank Wu,2/2/30,20140527, for add single converter and single solar settings pages
17		ID_RECT_SET				32			Converter Settings			Ajustes Convertidor
18		ID_BACK					16			Back				Atrás

[tmp.system_solar.html:Number]
16

[tmp.system_solar.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_RECTIFIER				32			Rectifier				Rectificadores
2		ID_CONVERTER				32			Converter				Convertidor
3		ID_SOLAR				32			Solar Converter				Convertidor Solar
4		ID_VOLTAGE				32			Average Voltage			Tensión media
5		ID_CURRENT				32			Total Current			Corriente Tota
6		ID_CAPACITY_USED			32			System Capacity Used		Capacidad utilizada Sistema
7		ID_NUM_OF_RECT				32			Number of Solar Converters		Número de Convertidores Solares
8		ID_TOTAL_COMM_RECT			64			Total Solar Converters Communicating	Número de Convertidores Solares en comunicación
9		ID_MAX_CAPACITY				32			Max Used Capacity			Máxima Capacidad Utilizada
10		ID_SIGNAL				32			Signal				Señal
11		ID_VALUE				32			Value				Valor
12		ID_SOLAR1				32			Solar Converter				Convertidor Solar
13		ID_CURRENT1				32			Total Current			Corriente Total
14		ID_RECTIFIER1				32			GI Rectifier			Rectificadores GI
15		ID_RECTIFIER2				32			GII Rectifier			Rectificadores GII
16		ID_RECTIFIER3				32			GIII Rectifier			Rectificadores GIII
#//changed by Frank Wu,3/3/30,20140527, for add single converter and single solar settings pages
17		ID_RECT_SET				32			Solar Converter Settings		Ajustes Convertidor Solar
18		ID_BACK					16			Back				Atrás



[tmp.setting_eco.html:Number]
7

[tmp.setting_eco.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Señal
2		ID_VALUE				32			Value				Valor
3		ID_TIME					32			Time Last Set			Ultimo Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					Fijar
6		ID_SET1					32			Set					Fijar
7		ID_ECO					16			ECO					ECO
8		ID_NO_DATA				16			No data.				No hay datos

[tmp.setting_lvd.html:Number]
6

[tmp.setting_lvd.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Señal
2		ID_VALUE				32			Value				Valor
3		ID_TIME					32			Time Last Set			Ultimo Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					Fijar
6		ID_SET1					32			Set					Fijar
7		ID_NO_DATA				16			No data.				No hay datos

[tmp.setting_rectifiers.html:Number]
6

[tmp.setting_rectifiers.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Señal
2		ID_VALUE				32			Value				Valor
3		ID_TIME					32			Time Last Set			Ultimo Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					Fijar
6		ID_SET1					32			Set					Fijar
7		ID_RECT					32			Rectifiers				Rectificadores
8		ID_NO_DATA				16			No data.				No hay datos

[tmp.setting_batteryTest.html:Number]
7

[tmp.setting_batteryTest.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Señal
2		ID_VALUE				32			Value				Valor
3		ID_TIME					32			Time Last Set			Ultimo Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					Fijar
6		ID_SET1					32			Set					Fijar
7		ID_BATT_TEST				32			Battery Test			Prueba de Batería
8		ID_NO_DATA				16			No data.				No hay datos
[tmp.setting_temp.html:Number]
7

[tmp.setting_temp.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Señal
2		ID_VALUE				32			Value				Valor
3		ID_TIME					32			Time Last Set			Ultimo Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					Fijar
6		ID_SET1					32			Set					Fijar
7		ID_TEMP					32			Temperature				Temperatura
8		ID_NO_DATA				16			No data.				No hay datos
[tmp.setting_hybrid.html:Number]
7

[tmp.setting_hybrid.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Señal
2		ID_VALUE				32			Value				Valor
3		ID_TIME					32			Time Last Set			Ultimo Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					Fijar
6		ID_SET1					32			Set					Fijar
7		ID_HYBRID				32			Generator					GE
8		ID_NO_DATA				16			No data.				No hay datos

[tmp.install_wizard.html:Number]
30

[tmp.install_wizard.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIME_SET				32			Time Settings			Ajuste Fecha/Hora
2		ID_GET_TIME				32			Get Local Time from Connected PC		Obtener hora local del PC
3		ID_SITE_SET				32			Site Settings			Ajustes de Planta
4		ID_SIGNAL				32			Signal				Señal
5		ID_VALUE				32			Value				Valor
6		ID_SET_VALUE				32			Set Value				Ajustar Valor
7		ID_SET					32			Set					Fijar
8		ID_SIGNAL1				32			Signal				Señal
9		ID_VALUE1				32			Value				Valor
10		ID_TIME1				32			Time Last Set			Ultimo Ajuste
11		ID_SET_VALUE1				32			Set Value				Ajustar Valor
12		ID_SET1					32			Set					Fijar
13		ID_SITE					32			Site Settings			Ajustes de Planta
14		ID_WIZARD				32			Install Wizard			Asistente de Instalación
15		ID_SET2					32			Set					Fijar
16		ID_SIGNAL_SET				32			Signal Settings			Ajustes de Señal
17		ID_SET3					32			Set					Fijar
18		ID_SET4					32			Set					Fijar
19		ID_CHARGE				32			Battery Charge			Carga a Batería
20		ID_ECO					32			ECO					ECO
21		ID_LVD					32			LVD					LVD
22		ID_QUICK_SET				32			Quick Settings			Ajuste Rápido
23		ID_TEMP					32			Temperature				Temperatura
24		ID_RECT					32			Rectifiers				Rectificadores
25		ID_CONVERTER				32			DC/DC Converters			Convertidores CC/CC
26		ID_BATT_TEST				32			Battery Test			Prueba de Batería
27		ID_TIME_CFG				32			Time Settings			Fecha y Hora
28		ID_TIPS13				32			Site Name				Nombre de Central
29		ID_TIPS14				32			Site Location			Localización
30		ID_TIPS15				32			System Name			Nombre de Sistema	
31		ID_DEVICE				32			Device Name				Equipo
32		ID_SIGNAL				32			Signal Name				Nombre de Señal
33		ID_VALUE				32			Value				Valor
34		ID_SETTING_VALUE			32			Set Value				Ajustar Valor
35		ID_SITE1				32			Site				Central
36		ID_POWER_SYS				32			System				Sistema
37		ID_USER_SET1				32			User Config1			Config1 usuario
38		ID_USER_SET2				32			User Config2			Config2 usuario
39		ID_USER_SET3				32			User Config3			Config3 usuario
#//changed by Frank Wu,1/1/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
40		ID_MPPT					32					Solar				Solar
41		ID_TIPS1				32			Unknown error.			Error desconocido
42		ID_TIPS2				16			Successful.			Correcto
43		ID_TIPS3				128			Failed. Incorrect time setting.			Fallo. Ajuste de Fecha/Hora incorrecto.
44		ID_TIPS4				128			Failed. Incomplete information.			Fallo. Información incompleta.
45		ID_TIPS5				64			Failed. No privileges.				Fallo. No tiene privilegios
46		ID_TIPS6				128			Cannot be modified. Controller is hardware protected.		No se puede modificar. NCU protegida por HW.
47		ID_TIPS7				256			Secondary Server IP Address is incorrect. \nThe format should be :'nnn.nnn.nnn.nnn'.	Dirección IP del Servidor Secundario incorrecta. \nFormato: 'nnn.nnn.nnn.nnn'.
48		ID_TIPS8				128			Format error! There is one blank between time and date.					Error de formato! Hay un espacio en blanco entre la hora y la fecha.
49		ID_TIPS9				128			Incorrect time interval. \nTime interval should be a positive integer.			Intervalo de tiempo incorrecto. \nDebe ser un entero positivo
50		ID_TIPS10				128			Date should be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.		La fecha debe estar entre '1970/01/01 00:00:00' y '2038/01/01 00:00:00'
51		ID_TIPS11				128			Please clear the IP before time setting.		Por favor, elimine la dirección IP antes de ajustar la fecha.
52		ID_TIPS12				64			Time expired, please login again.			El tiempo ha expirado. Por favor, vuelva a conectarse.

[tmp.setting_user.html:Number]


[tmp.setting_user.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_USER					32			User Info				Usuarios
2		ID_IPV4					32			Ethernet				Ethernet
3		ID_IPV6					32			IPV6				IPV6
4		ID_HLMS_CONF				32			Monitor Protocol			Protocolos 
5		ID_SITE_INFO				32			Site Info				Información de Planta
6		ID_TIME_CFG				32			Time Sync				Sincronizar Hora
7		ID_AUTO_CONFIG				32			Auto Config				Autoconfig
8		ID_OTHER				32			Other Settings			Otros ajustes
9		ID_WEB_HEAD				32			User Information			Usuarios
10		ID_USER_NAME				32			User Name				Nombre de Usuario
11		ID_USER_AUTHORITY			32			Privilege					Autoridad
12		ID_USER_DELETE				32			Delete				Eliminar
13		ID_MODIFY_ADD				32			Add or Modify User			Añadir o Modificar Usuario
14		ID_USER_NAME1				32			User Name				Nombre de Usuario
15		ID_USER_AUTHORITY1			32			Privilege					Autoridad
16		ID_PASSWORD				32			Password				Contraseña
17		ID_CONFIRM				32			Re-enter Password			        Escriba la contraseña otra vez
18		ID_USER_ADD				32			Add					Añadir
19		ID_USER_MODIFY				32			Modify				Modificar
20		ID_ERROR0				32			Unknown Error						Error Desconocido			
21		ID_ERROR1				32			Successful							Correcto				
22		ID_ERROR2				64			Failed. Incomplete information.				Fallo. Información incompleta				
23		ID_ERROR3				64			Failed. The user name already exists.			Fallo. El nombre de usuario ya existe		
24		ID_ERROR4				64			Failed. No privilege.					Fallo. Sin autoridad			
25		ID_ERROR5				64			Failed. Controller is hardware protected.			Fallo. NCU protegida por HW.
26		ID_ERROR6				64			Failed. You can only change your password.			Fallo. Sólo puede cambiar su propia contraseña		
27		ID_ERROR7				64			Failed. Deleting 'admin' is not allowed.			Fallo. El usuario 'admin' no se puede eliminar		
28		ID_ERROR8				64			Failed. Deleting a logged in user is not allowed.		Fallo. No está permitido eliminar a un usuario conectado.	
29		ID_ERROR9				128			Failed. The user already exists.				Fallo. El usuario ya existe.		
30		ID_ERROR10				128			Failed. Too many users.					Fallo. Demasiados usuarios			
31		ID_ERROR11				128			Failed. The user does not exist.				Fallo. El usuario no existe.			
32		ID_AUTHORITY_LEVEL0			32			Browser				Navegador
33		ID_AUTHORITY_LEVEL1			32			Operator				Operador
34		ID_AUTHORITY_LEVEL2			32			Engineer				Ingeniero
35		ID_AUTHORITY_LEVEL3			32			Administrator			Administrador
36		ID_TIPS1				64			Please enter an user name.						Por favor, entre un nombre de usuario			
37		ID_TIPS2				128			The user name cannot be started or ended with spaces.		El nombre de usuario no puede comenzar ni terminar con espacios en blanco.		
38		ID_TIPS3				128			Passwords do not match.						Las contraseñas no coinciden		
39		ID_TIPS4				128			Please remember the password entered.				Por favor, recuerde su contraseña.					
40		ID_TIPS5				128			Please enter password.						Entre contraseña			
41		ID_TIPS6				128			Please remember the password entered.				Por favor, recuerde su contraseña.					
42		ID_TIPS7				128			Already exists. Please try again.					Ya existe. Inténtelo de nuevo.			
43		ID_TIPS8				128			The user does not exist.						El usuario no existe	
44		ID_TIPS9				128			Please select a user.						Por favor, seleccione un usuario.				
45		ID_TIPS10				128			The follow characters must not be included in user name, please try again.		No se pueden utilizar caracteres especiales en el nombre de usuario
46		ID_TIPS11				32			Please enter password.						Entre contraseña
47		ID_TIPS12				32			Please Re-enter password.						Confirme contraseña
48		ID_RESET				16			Reset								Reiniciar
49		ID_TIPS13				128			Only modifying password can be valid for account 'admin'.		Sólo el usuario admin puede cambiar las contraseñas
50		ID_TIPS14				128			User name or password can only be letter or number or '_'.		Los nombres de usuario o contraseña sólo admiten caracteres alfanuméricos y el guion '-'.
51		ID_TIPS15				32			Are you sure to modify		Está seguro de modificar
52		ID_TIPS16				32			's information?		esta información?
53		ID_TIPS17				64			The password must contain at least six characters.		La contraseña debe tener al menos 6 caracteres.
54		ID_TIPS18				64			OK to delete?		Está seguro de borrar?
55		ID_ERROR12				64			Failed. Deleting 'engineer' is not allowed.			Fallo. No está permitido borrar la cuenta 'ingeniero'.
56		ID_TIPS19				128			Only modifying password can be valid for account 'engineer'.		La cuenta 'ingeniero' sólo puede modificar la contraseña.
#//changed by Frank Wu,1/N/15,20140527, for e-mail
57		ID_LCD_LOGIN				32			LCD Access Only		Lcd Access Only
58		ID_USER_CONTACT1		16				E-Mail								E-Mail
59		ID_TIPS20				128				Valid E-Mail should contain the char '@', eg. name@vertiv.com			La dirección de E-Mail debe incluir el carácter '@', e.g. name@emerson.com

60		ID_RADIUS				64			Radius Server Settings			Configuración del servidor Radius

61		ID_ENABLE_RADIUS			64			Enable Radius			Habilitar el radius
62		ID_NASIDENTIFIER			64			NAS-Identifier			Identificador nasal
63		ID_PRIMARY_SERVER			64			Primary Server			Servidor primario
64		ID_PRIMARY_PORT				64			Primary Port			Puerto primario
65		ID_SECONDARY_SERVER			64			Secondary Server	        Servidor secundario
66		ID_SECONDARY_PORT			64			Secondary Port		        Puerto secundario
67		ID_SECRET_KEY				64			Secret Key		        Llave secreta
68		ID_CONFIRM_SECRET			32			Confirm			        Confirmar
69		ID_SAVE					32			Save			        salvar

70		ID_ERROR13				64			Enabled Radius Settings!					Configuración de radio habilitada			

71		ID_ERROR14				64			Disabled Radius Settings!					Configuraciones de radio deshabilitadas

72		ID_TIPS21				128			Please enter an IP Address.  	Por favor ingrese una dirección IP

73		ID_TIPS22				128			You have entered an invalid primary server IP! 		Has introducido una IP de servidor primario no válida

74		ID_TIPS23				128			You have entered an invalid secondary server IP!.	Has introducido una IP de servidor secundario no válida

75		ID_TIPS24				128			Are you sure to update Radius server configuration.	¿Estás seguro de actualizar la configuración del servidor Radius?

76		ID_TIPS25				128			Please Enter Port Number.	Por favor ingrese el número de puerto.

77		ID_TIPS26				128			Please enter secret key!	Por favor, introduzca la clave secreta!

78		ID_TIPS27				128			Confirm secret key!		Confirmar clave secreta!

79		ID_TIPS28				128			Secret key do not match.	La clave secreta no coincide.

80		ENABLE_LCD_LOGIN			32			LCD Access Only			LCD Access		 Only
81		ID_ACC_LOCK				32			Account Status						Account Status
82		ID_ERROR15				64			User account has been locked.				User account has been locked.
83		ID_LOGIN_LIMIT				32			Limit Login Attempts					Limit Login Attempts
84		ID_UNLOCK_ACCOUNT			32			Account Locked						Account Locked
85		ID_ACC_COUNT_EN				32			Login Attempts						Login Attempts
86		ID_TIPS29				128			LCD Access with Limit Login Attempts or Strong Password is not allowed.		LCD Access with Limit Login Attempts or Strong Password is not allowed.
87		ID_ERROR16				64			User does not exist.					User does not exist.
88		ID_ERROR17				64			Local login only for a remote user is not allowed.	Local login only for aremote user is not allowed.
89		ID_ERROR18				64			User account has been suspended for 30 minutes.		User account has been suspended for 30 minutes.
90		ID_TIPS30				64			The password must contain at least 16 characters.	La contraseña debe contener al menos 16 caracteres.
91		ID_TIPS31				128			Password must contain at least one upper, lower, numeric and special character.	La contraseña debe contener al menos un carácter superior,inferior,numérico y especial.
92		ENABLE_STRONG_PASS			32			Strong Password			Strong Password
93		ENABLE_STRONG_PASS1			32			Strong Password			Strong Password
94		ID_TIPS32				128			The password must contain minimum 6 and maximum 13 characters.	The password must contain minimum 6 and maximum 13 characters.
95		ID_ERROR19				64			Strong password for 'admin' is not allowed.		Strong password for 'admin' is not allowed.

[tmp.setting_ipv4.html:Number]
25

[tmp.setting_ipv4.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_IPV4					32			Ethernet				Ethernet
2		ID_SCUP_IP				32			IP Address				Dirección IP
3		ID_SCUP_MASK				32			Subnet Mask				Máscara Subnet
4		ID_SCUP_GATEWAY				32			Default Gateway			Puerta de Enlace
5		ID_ERROR0				32			Setting Failed.			Fallo de ajuste.		
6		ID_ERROR1				32			Successful.				Correcto.	
7		ID_ERROR2				64			Failed. Incorrect input.				Fallo. Entrada incorrecta.	
8		ID_ERROR3				64			Failed. Incomplete information.			Fallo. Información incompleta.		
9		ID_ERROR4				64			Failed. No privilege.				Fallo. Sin autoridad.		
10		ID_ERROR5				128			Failed. Controller is hardware protected.		Fallo. NCU protegida por HW.
11		ID_ERROR6				64			Failed. DHCP is ON.					Fallo. La función DHCP está activa.
12		ID_TIPS0				32			Set Network Parameter														Fijar Parámetro de Red											
13		ID_TIPS1				128			Units IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171.					La dirección IP de las unidades es incorrecta. \nFormato: 'xxx.xxx.xxx.xxx'. Por ejemplo: 10.75.14.171					
14		ID_TIPS2				128			Mask IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 255.255.0.0.						Mácara incorrecta. \nFormato: 'xxx.xxx.xxx.xxx'. Ejemplo: 255.255.0.0				
15		ID_TIPS3				64			Units IP Address and Mask mismatch.													Conflicto dirección IP y máscara de unidades							
16		ID_TIPS4				256			Gateway IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171. Enter 0.0.0.0 for no gateway	Dirección Puerta de Enlace incorrecta. \nFormato: 'xxx.xxx.xxx.xxx'. Ejemplo: 10.75.14.171. Entre 0.0.0.0 para no Puerta de Enlace.
17		ID_TIPS5				128			Units IP Address, Gateway, Mask mismatch. Enter Address again.									Dirección IP, máscara y Puerta de Enlace incompatibles. Inténtelo de nuevo					
18		ID_TIPS6				64			Please wait. Controller is rebooting.												Reiniciando. Espere…										
19		ID_TIPS7				128			Parameters have been modified.  Controller is rebooting...										Los parámetros han sido modificados. Reiniciando…									
20		ID_TIPS8				64			Controller homepage will be refreshed.												La página principal de la NCU se refrescará									
21		ID_TIPS9				128			Confirm the change to the IP address?				Confirmar el cambio en la dirección IP?
22		ID_SAVE					16			Save						Salvar
23		ID_TIPS10				32			IP Address Error				Error Dirección IP
24		ID_TIPS11				32			Subnet Mask Error				Error Máscara de Subred
25		ID_TIPS12				32			Default Gateway Error			Error Puerta de Enlace
26		ID_USER					32			Users				Usuarios
27		ID_IPV4_1					32			IPV4				IPV4
28		ID_IPV6					32			IPV6				IPV6
29		ID_HLMS_CONF				32			Monitor Protocol			Protocolos
30		ID_SITE_INFO				32			Site Info				Información de Planta
31		ID_AUTO_CONFIG				32			Auto Config				Autoconfig
32		ID_OTHER				32			Alarm Report					Mensajes Alarma
33		ID_NMS					32			SNMP				SNMP
34		ID_ALARM_SET				32			Alarms				Alarmas
35		ID_CLEAR_DATA				16			Clear Data			Borrar Datos
36		ID_RESTORE_DEFAULT			32			Restore Defaults		Restaurar valores de fábrica
37		ID_DOWNLOAD_UPLOAD			32			SW Maintenance			Mantenimiento
38		ID_HYBRID				32			Generator				GE
39		ID_DHCP					16			IPV4 DHCP				IPV4 DHCP
40		ID_IP1					32			Server IP				IP Servidor
41		ID_TIPS13				16			Loading				Cargando
42		ID_TIPS14				16			Loading				Cargando
43		ID_TIPS15				64			failed, data format error!			Fallo, error de formato de datos.
44		ID_TIPS16				32			failed, please refresh the page!		Fallo, refrescar la página.
45		ID_LANG					16			Language			Idioma
#//changed by Frank Wu,2/2/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
46		ID_SHUNT_SET				32			Shunt						Shunt de Carga
#//changed by Frank Wu,1/N/35,20140527, for adding the the web setting tab page 'DI'
47		ID_DI_ALARM_SET			32				DI Alarms			Alarmas DI
#//changed by Frank Wu,1/1/N,00000000, for add PowerSplit
48		ID_POWER_SPLIT_SET		32				Power Split			PowerSplit
49		ID_IPV6_1			16				IPV6				IPV6
50		ID_LOCAL_IP			32				Link-Local Address		Dirección Enlace Local
51		ID_GLOBAL_IP			32				IPV6 Address			Dirección Global
52		ID_SCUP_PREV			16				Subnet Prefix				Prefijo
53		ID_SCUP_GATEWAY1		16				Default Gateway				Puerta de Enlace
54		ID_SAVE1			16				Save				Salvar
55		ID_DHCP1			16				IPV6 DHCP			IPV6 DHCP
56		ID_IP2				32				Server IP			IP Servidor
57		ID_TIPS17			64				Please fill in the correct IPV6 address.	Por favor, introduzca la dirección IPV6 correcta.
58		ID_TIPS18			128				Prefix can not be empty or beyond the range.			El prefijo no puede ser nulo.
59		ID_TIPS19			64				Please fill in the correct IPV6 gateway.	Por favor, introduzca la Puerta de Enlace correcta para IPV6.


[tmp.setting_time_sync.html:Number]
29

[tmp.setting_time_sync.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIME_HEAD				32			Time Synchronization		Fecha y Hora
2		ID_ZONE					64			Local Zone(for synchronization with time servers)				Zona Local(para la sincronización con servidores de tiempo)
3		ID_GET_ZONE				32			Get Local Zone			Obtener Zona Local
4		ID_SELECT1				64			Get time automatically from the following time servers.	Obtener hora automáticamente de los siguientes servidores
5		ID_PRIMARY_SERVER			32			Primary Server IP						IP Servidor Primario			
6		ID_SECONDARY_SERVER			32			Secondary Server IP						IP Servidor Secundario			
7		ID_TIMER_INTERVAL			64			Interval to Adjust Time					Intervalo Ajuste de Hora		
8		ID_SPECIFY_TIME				32			Specify Time						Establecer Hora		
9		ID_GET_TIME				64			Get Local Time from Connected PC				Obtener hora local del PC conectado
10		ID_DATE_TIME				32			Date & Time							Fecha & Hora				
11		ID_SUBMIT				16			Set								Fijar				
12		ID_ERROR0				32			Unknown error.						Error Desconocido		
13		ID_ERROR1				16			Successful.							Correcto				
14		ID_ERROR2				128			Failed. Incorrect time setting.				Fallo. Ajuste de Fecha/Hora incorrecto		
15		ID_ERROR3				128			Failed. Incomplete information.				Fallo. Información incompleta		
16		ID_ERROR4				64			Failed. No privilege.					Fallo. Sin autoridad			
17		ID_ERROR5				128			Cannot be modified. Controller is hardware protected.	No se puede modificar. NCU protegida por HW.
18		ID_MINUTE				16			min								min
19		ID_TIPS0				256			Primary Server IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'.	Dirección IP del Servidor Primario incorrecta. \nFormato: 'xxx.xxx.xxx.xxx'.	
20		ID_TIPS1				256			Secondary Server IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'.	Dirección IP del Servidor Secundario incorrecta. \nFormato: 'xxx.xxx.xxx.xxx'.	
21		ID_TIPS2				128			Incorrect time interval. \nTime interval should be a positive integer.			Intervalo de tiempo incorrecto. \nDebe ser un entero positivo					
22		ID_TIPS3				128			Synchronizing time, please wait.								Sincronizando, Espere…					
23		ID_TIPS4				128			Incorrect format.  \nPlease enter the date as yyyy/mm/dd,e.g., 2000/09/30		Formato incorrecto. \nEl formato fecha es: aaaa/mm/dd, e.g. 2000/09/30			
24		ID_TIPS5				128			Incorrect format.  \nPlease enter the time as  'hh:mm:ss', e.g., 8:23:08		Formato incorrecto. \nEl formato hora es: hh:mm:ss, e.g. 08:23:54	
25		ID_TIPS6				128			Date must be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.			La fecha debe estar entre '1970/01/01 00:00:00' y '2038/01/01 00:00:00'	
26		ID_TIPS7				128			The time server has been set. Time will be set by time server.				El servidor de hora ha sido establecido. La hora será fijada por el servidor.
27		ID_TIPS8				128			Incorrect date and time format.								Formato Fecha/hora incorrecto.
28		ID_TIPS9				128			Please clear the IP address before setting the time.						Por favor, elimine la dirección IP antes de ajustar la fecha.
29		ID_TIPS10				64			Time expired, please login again.						El tiempo ha expirado. Por favor, vuelva a conectarse.

[tmp.system_inventory.html:Number]
6

[tmp.system_inventory.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_INVENTORY				32			System Inventory			Inventario Sistema
2		ID_EQUIP				32			Equipment				Equipo
3		ID_MODEL				32			Product Model			Modelo
4		ID_REVISION				32			Hardware Revision			Versión HW
5		ID_SERIAL				32			Serial Number			Número de Serie
6		ID_SOFT_REVISION			32			Software Revision			Versión SW
7		ID_INVENTORY2				32			    System Inventory			Inventario del sistema
[forgot_password.html:Number]
12

[forgot_password.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_FORGET_PASSWD			32			Forgot Password			¿Olvidó la contraseña?
2		ID_FIND_PASSWD				32			Find Password			Encontrar Contraseña
3		ID_INPUT_USER				32			Input User Name:			Entre Nombre de Usuario
4		ID_FIND_PASSWD1				32			Find Password			Encontrar Contraseña
5		ID_RETURN				32			Back to Login Page			Volver a la página de inicio
6		ID_ERROR0				64			Unknown error.			Error Desconocido
7		ID_ERROR1				128			Password has been sent to appointed mailbox.	La contraseña ha sido enviada al buzón de correo designado.
8		ID_ERROR2				64			No such user.			No este usuario
9		ID_ERROR3				64			No email address.			No hay dirección de e-mail.
10		ID_ERROR4				32			Input User Name			Entre Nombre de Usuario
11		ID_ERROR5				32			Fail.				Fallo
12		ID_ERROR6				32			Error data format.		Error en formato de datos
13		ID_USERNAME				64			    User Name			Nombre de usuario

[tmp.setting_other.html:Number]
61

[tmp.setting_other.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SMTP					16			SMTP				SMTP
2		ID_EMAIL				32			Email To				Enviar e-mail a
3		ID_IP					32			Server IP				IP Servidor
4		ID_PORT					32			Server Port				Puerto Servidor
5		ID_AUTHORIY				32			Privilege			Autoridad
6		ID_ENABLE				32			Enabled				Habilitado
7		ID_DISABLE				32			Disabled				Deshabilitado
8		ID_ACCOUNT				32			SMTP Account			Cuenta SMTP
9		ID_PASSWORD				32			SMTP Password			Contraseña SMTP
10		ID_ALARM_REPORT				32			Alarm Report Level			Reporte de Alarmas
11		ID_OA					32			All Alarms				Todas las Alarmas
12		ID_MA					32			Major and Critical Alarm		Urgentes y Críticas
13		ID_CA					32			Critical Alarm			Alarma Crítica
14		ID_NONE					32			None				Ninguna
15		ID_SET					32			Set					Fijar
18		ID_LOAD					64			Loading data, please wait.		Cargando datos, espere…
19		ID_VPN					32			Open VPN				Abrir VPN
20		ID_ENABLE1				32			Enabled				Habilitado
21		ID_DISABLE1				32			Disabled			Deshabilitado
22		ID_VPN_IP				32			VPN IP				IP VPN
23		ID_VPN_PORT				32			VPN Port				Puerto VPN
24		ID_VPN					16			VPN					VPN
25		ID_LOAD1				64			Loading data, please wait.	Cargando datos, espere…
26		ID_SET1					32			Set					Fijar
27		ID_HANDPHONE1				32			Cell Phone Number 1			Número de móvil 1
28		ID_HANDPHONE2				32			Cell Phone Number 2			Número de móvil 2
29		ID_HANDPHONE3				32			Cell Phone Number 3			Número de móvil 3
30		ID_ALARMLEVEL				32			Alarm Report Level			Reporte de alarmas
31		ID_OA1					64			All Alarms				Todas las Alarmas
32		ID_MA1					64			Major and Critical Alarm		Urgentes y Críticas
33		ID_CA1					32			Critical Alarm			Alarma Crítica
34		ID_NONE1				32			None				Ninguna
35		ID_SMS					16			SMS					SMS
36		ID_LOAD2				64			Loading data, please wait.	Cargando datos, espere…
37		ID_SET2					32			Set					Fijar
38		ID_ERROR0				64			Unknown error.			Error Desconocido
39		ID_ERROR1				32			Success				Éxito
40		ID_ERROR2				128			Failure, administrator privilege required.	Fallo: Se requiere autoridad de administrador.
41		ID_ERROR3				128			Failure, the user name already exists.	Fallo: El nombre de usuario ya existe.
42		ID_ERROR4				128			Failure, incomplete information.		Fallo: Información incompleta
43		ID_ERROR5				128			Failure, NSC is hardware protected.		Fallo: NCU protegida por HW
44		ID_ERROR6				64			Failure, incorrect password.	Fallo: Contraseña incorrecta
45		ID_ERROR7				128			Failure, deleting Admin is not allowed.	Fallo: No está permitido eliminar al usuario Admin
46		ID_ERROR8				64			Failure, not enough space.			Fallo: Espacio insuficiente
47		ID_ERROR9				128			Failure, user already existed.		Fallo: El nombre de usuario ya existe.
48		ID_ERROR10				64			Failure, too many users.			Fallo: Demasiados usuarios
49		ID_ERROR11				64			Failuer, user is not exist.			Fallo: El usuario no existe
50		ID_TIPS1				64			Please select the user.			Seleccione el usuario
51		ID_TIPS2				64			Please fill in user name.			Por favor, rellene el nombre de usuario.
52		ID_TIPS3				64			Please fill in password.			Por favor, teclee una contraseña.
53		ID_TIPS4				64			Please confirm password.			Confirme contraseña
54		ID_TIPS5				64			Password not consistent.			Contraseña inconsistente
55		ID_TIPS6				64			Please input an email address in the form name@domain.			Entre una dirección de e-mail con formato: nombre@dominio.
56		ID_TIPS7				64			Please input the right IP.			Por favor, entre la dirección IP correcta
57		ID_TIPS8				16			Loading					Cargando
58		ID_TIPS9				64			Failure, please refresh the page.		Fallo. Refresque la página, por favor.
59		ID_LOAD3				64			Loading data, please wait...		Cargando datos, espere…
60		ID_LOAD4				64			Loading data, please wait...		Cargando datos, espere…
61		ID_EMAIL1				32			Email From				Enviar e-mail desde
65		ID_TIPS14				64			Only numbers are permitted!			Sólo los números están permitidos .
66		ID_TIPS10				16			Loading				Loading
67		ID_TIPS12				64			failed, data format error!		failed, data format error!
68		ID_TIPS13				64			failed, please refresh the page!	failed, please refresh the page!


[tmp.setting_hlms_configuration.html:Number]
64

[tmp.setting_hlms_configuration.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_ESRTIPS1				64			Range 0-255				Rango desde 0-255
2		ID_ESRTIPS2				64			Range 0-600				Rango desde 0-600
3		ID_ESRTIPS3				64			Main Report Phone Number		Teléfono Informe de Alarmas Principal
4		ID_ESRTIPS4				64			Second Report Phone Number		Teléfono Informe de Alarmas 2
5		ID_ESRTIPS5				64			Callback Phone Number		Teléfono Retrollamada
6		ID_ESRTIPS6				64			Maximum Alarm Report Attempts	Número de intentos Informe de Alarmas
7		ID_ESRTIPS7				64			Call Elapse Time			Tiempo entre intentos
8		ID_YDN23TIPS1				64			Range 0-5				Rango desde 0-5
9		ID_YDN23TIPS2				64			Range 0-300				Rango desde 0-300
10		ID_YDN23TIPS3				64			First Report Phone Number		Teléfono Informe de Alarmas 1
11		ID_YDN23TIPS4				64			Second Report Phone Number		Teléfono Informe de Alarmas 2
12		ID_YDN23TIPS5				64			Third Report Phone Number		Teléfono Informe de Alarmas 3
13		ID_YDN23TIPS6				64			Times of Dialing Attempt		Número de intentos de llamada
14		ID_YDN23TIPS7				64			Interval between Two Dialings	Intervalo entre dos llamadas
15		ID_HLMSERRORS1				32			Successful.				Correcto
16		ID_HLMSERRORS2				32			Failed.				Fallo
17		ID_HLMSERRORS3				64			Failed. ESR Service was exited.	Fallo: El servicio de protocolos EEM se ha detenido.
18		ID_HLMSERRORS4				64			Failed. Invalid parameter.		Fallo: Parámetro no válido
19		ID_HLMSERRORS5				64			Failed. Invalid data.		Fallo: Datos no válidos
20		ID_HLMSERRORS6				128			Cannot be modified. Controller is hardware protected.		No se puede modificar. NCU protegida por HW.
21		ID_HLMSERRORS7				128			Service is busy. Cannot change configuration at this time.		El servicio está ocupado. No se puede cambiar la configuración en este momento.
22		ID_HLMSERRORS8				128			Non-shared port already occupied.		El puerto no compartido ya está ocupado.
23		ID_HLMSERRORS9				64			Failed. No privilege.			Fallo. No está autorizado
24		ID_EEM					16			EEM						EEM
25		ID_YDN23				16			YDN23					YDN23
26		ID_MODBUS				16			Modbus					Modbus
27		ID_RESET				64			Valid after Restart				Válido despues de reiniciar
28		ID_TYPE					32			Protocol Type				Tipo Protocolo
29		ID_EEM1					16			EEM						EEM
30		ID_RSOC					16			RSOC					RSOC
31		ID_SOCTPE				16			SOC/TPE					SOC/TPE
32		ID_YDN231				16			YDN23					YDN23
33		ID_MEDIA				32			Protocol Media				Medio protocolo
34		ID_232					16			RS-232					RS-232
35		ID_MODEM				16			Modem					Modem
36		ID_ETHERNET				16			IPV4					Ethernet
37		ID_ADRESS				32			Self Address				Dirección propia
38		ID_CALLBACKEN				32			Callback Enabled				Retrollamada habilitada
39		ID_REPORTEN				32			Report Enabled				Informe de alarmas habilitado
40		ID_ALARMREP				32			Alarm Reporting				Informe de Alarmas
41		ID_RANGE				32			Range 1-255					Rango: 1 a 255
42		ID_SOCID				32			SOCID					SOCID
43		ID_RANGE1				32			Range 1-20479				Rango: 1 a 20479
44		ID_RANGE2				32			Range 0-255					Rango: 0-255
45		ID_RANGE3				32			Range 0-600s				Rango: 0 a 600 s
46		ID_IP1					32			Main Report IP				IP Informe de Alarmas 1
47		ID_IP2					32			Second Report IP				IP Informe de Alarmas 2
48		ID_SECURITYIP				32			Security Connection IP 1			IP Conexión de seguridad 1
49		ID_SECURITYIP2				32			Security Connection IP 2			IP Conexión de seguridad 2
50		ID_LEVEL				32			Safety Level				Nivel de seguiridad
51		ID_TIPS1				64			All commands are available.			Todos los comandos disponibles
52		ID_TIPS2				128			Only read commands are available.		Sólo comandos de lectura
53		ID_TIPS3				128			Only the Call Back command is available.	Sólo comandos en retrollamada
54		ID_TIPS4				64			Confirm to change the protocol to		Confirme cambio a protocolo
55		ID_SAVE					32			Save					Salvar
56		ID_TIPS5				32			Switch Fail					Fallo interruptor
57		ID_CCID					16			CCID					CCID
58		ID_TYPE1				32			Protocol					Protocolo
59		ID_TIPS6				32			Port Parameter				Parámetro de puerto
60		ID_TIPS7				128			Port Parameters & Phone Number	Parámetro de puerto & Teléfono
61		ID_TIPS8				32			TCP/IP Port Number				Puerto TCP/IP
62		ID_TIPS9				128			Set successfully, controller is restarting, please wait		Fijado con éxito. La NCU se está reiniciando. Espere.
63		ID_TIPS10				64			Main Report Phone Number Error.		Main Report Phone Number Error.
64		ID_TIPS11				64			Second Report Phone Number Error.		Second Report Phone Number Error.
65		ID_ERROR10				32			Unknown error.			Error Desconocido
66		ID_ERROR11				32			Successful.			Correcto
67		ID_ERROR12				32			Failed.				Fallo.
68		ID_ERROR13				64			Insufficient privileges for this function.		Autorización insuficiente para esta función.
69		ID_ERROR14				32			No information to send.		No hay información para enviar
70		ID_ERROR15				64			Failed. Controller is hardware protected.		Fallo. NCU protegida por HW.
71		ID_ERROR16				64			Time expired. Please login again.			El tiempo ha expirado. Vuelva a conectarse.
72		ID_TIPS12				64			Network Error		Error de conectividad con la Red
73		ID_TIPS13				64			CCID not recognized. Please enter a number.		CCID no válida. Entre un número.
74		ID_TIPS14				64			CCID					CCID
75		ID_TIPS15				64			Input Error					Error de entrada
76		ID_TIPS16				64			SOCID not recognized. Please enter a number.		SOCID no válida. Entre un número.
77		ID_TIPS17				64			SOCID					SOCID
78		ID_TIPS18				64			Cannot be zero. Please enter a different number.		No puede ser cero. Entre otro número.
79		ID_TIPS19				64			SOCID			SOCID
80		ID_TIPS20				64			Input error.		Error de entrada
81		ID_TIPS21				64			Unable to recognize maximum number of alarms.		Incapaz de reconocer el máximo número de alarmas.
82		ID_TIPS22				64			Unable to recognize maximum number of alarms.		Incapaz de reconocer el máximo número de alarmas.
83		ID_TIPS23				64			Maximum call elapse time is error.		Tiempo máximo entre llamadas incorrecto.
84		ID_TIPS24				64			Maximum call elapse time is input error.		Error de entrada Tiempo máximo entre llamadas.
85		ID_TIPS25				64			Report IP not recognized.		IP informe de alarmas no reconocida.
86		ID_TIPS26				64			Report IP not recognized.		IP informe de alarmas no reconocida.
87		ID_TIPS27				64			Security IP not recognized.	IP de seguridad no reconocida.
88		ID_TIPS28				64			Security IP not recognized.	IP de seguridad no reconocida.
89		ID_TIPS29				64			Port input error.		Puerto incorrecto.
90		ID_TIPS30				64			Port input error.		Puerto incorrecto.
91		ID_IPV6					16			IPV6			IPV6
92		ID_TIPS31				32			[IPV6 Addr]:Port		[Dir IPV6]:Puerto
93		ID_TIPS32				32			[IPV6 Addr]:Port		[Dir IPV6]:Puerto
94		ID_TIPS33				32			[IPV6 Addr]:Port		[Dir IPV6]:Puerto
95		ID_TIPS34				32			[IPV6 Addr]:Port		[Dir IPV6]:Puerto
96		ID_TIPS35				32			IPV4 Addr:Port		Dir IPV4:Puerto
97		ID_TIPS36				32			IPV4 Addr:Port		Dir IPV4:Puerto
98		ID_TIPS37				32			IPV4 Addr:Port		Dir IPV4:Puerto
99		ID_TIPS38				32			IPV4 Addr:Port		Dir IPV4:Puerto
100		ID_TIPS39				64			Callback Phone Number Error.		Callback Phone Number Error.
101		ID_TL1					16			TL1					TL1
102		ID_PORT_ACT				32			Port Activation			Activación del Puerto
103		ID_DISABLE				16			Disabled				Deshabilitado
104		ID_ENABLE				16			Enabled				Habilitado
105		ID_ETHERNET1				16			IPV4				IPV4
106		ID_IPV6_1				16			IPV6			IPV6
107		ID_TIPS40				16			Access Port				Puerto de acceso
108		ID_RANGE4				32			Range 1024-65534					Rango: 1024-65534
109		ID_TIPS41				32			Port Keep-alive			Puerto keep-alive
110		ID_DISABLE1				16			Disabled				Deshabilitado
111		ID_ENABLE1				16			Enabled				Habilitado
112		ID_TIPS42				32			Session Timeout			Hora de término de la sesión
113		ID_RANGE5				32			Range 0-1440 minutes		Rango:0-1440 minutos
114		ID_TIPS43				32			Auto Login User			Autoconexión del usuario
115		ID_TIPS44				32			System Identifier			Identificador del sistema
116		ID_MEDIA1				32			Protocol Media			Protocolo de medios
117		ID_NA					16			None				Ninguna
118		ID_TIPS45				32			Access Port Error			Error de acceso al puerto
119		ID_TIPS46				32			Session Timeout Error		Sesión de error de espera
120		ID_TIPS47				64			System Identifier Error		Identificador del sistema de error
121		ID_TL1_ERRORS1				32			Successful.				Correcto	
122		ID_TL1_ERRORS2				32			Configuration fail.			Fallo configuración
123		ID_TL1_ERRORS3				64			Failed. Service was exited.		Fallo. Servicio parado
124		ID_TL1_ERRORS4				32			Invalid parameters.			Parámetros inválidos
125		ID_TL1_ERRORS5				32			Invalid parameters.			Parámetros inválidos
126		ID_TL1_ERRORS6				64			Fail to write to Flash.		Fallo escritura Flash.
127		ID_TL1_ERRORS7				32			Service is busy.			Servicio ocupado
128		ID_TL1_ERRORS8				64			Port has already been occupied.	El puerto ya está ocupado
129		ID_TL1_ERRORS9				64			Port has already been occupied.	El puerto ya está ocupado
130		ID_TL1_ERRORS10				64			Port has already been occupied.	El puerto ya está ocupado
131		ID_TL1_ERRORS11				64			Port has already been occupied.	El puerto ya está ocupado
132		ID_TL1_ERRORS12				64			The TL1 feature is not available.	La función TL1 no está disponible.
133		ID_TIPS48				64			    All commands are available.			Todos los comandos disponibles.
134		ID_TIPS49				128			    Only read commands are available.		Sólo comandos de lectura.
135		ID_TIPS50				128			    Only the Call Back command is available.	Sólo comandos en retrollamada.
136		ID_NA2					16			None				Ninguna
[tmp.setting_nms.html:Number]
64

[tmp.setting_nms.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TRAP_LEVEL1				16			Not Used				No Utilizada
2		ID_TRAP_LEVEL2				32			All Alarms				Todas las Alarmas
3		ID_TRAP_LEVEL3				16			Major Alarms			Alarmas Urgentes
4		ID_TRAP_LEVEL4				16			Critical Alarms			Alarmas Críticas
5		ID_NMS_TRAP				32			Accepted Trap Level			Nivel de Trap aceptado
6		ID_SET_TRAP				16			Set					Fijar
7		ID_NMSV2_CONF				32			NMSV2 Configuration			NMSV2 configuración
8		ID_NMS_IP				32			NMS IP				NMS IP
9		ID_NMS_PUBLIC				32			Public Community			Comunidad Pública
10		ID_NMS_PRIVATE				32			Private Community			Comunidad Privada
11		ID_TRAP_ENABLE				16			Trap Enabled			Trap habilitada
12		ID_DELETE				16			Delete				Eliminar
13		ID_NMS_IP1				32			NMS IP				NMS IP
14		ID_NMS_PUBLIC1				32			Public Community			Comunidad Pública
15		ID_NMS_PRIVATE1				32			Private Community			Comunidad Privada
16		ID_TRAP_ENABLE1				16			Trap Enabled			Trap habilitada
17		ID_DISABLE				16			Disabled				Deshabilitado
18		ID_ENABLE				16			Enabled				Habilitado
19		ID_ADD					16			Add					Añadir
20		ID_MODIFY				16			Modify				Modificar
21		ID_RESET				16			Reset				Reiniciar
22		ID_NMSV3_CONF				32			NMSV3 Configuration			NMSV3 configuración
23		ID_NMS_TRAP_LEVEL0			32			NoAuthNoPriv			NoAuthNoPriv
24		ID_NMS_TRAP_LEVEL1			32			AuthNoPriv				AuthNoPriv
25		ID_NMS_TRAP_LEVEL2			32			AuthPriv				AuthPriv
26		ID_NMS_USERNAME				32			User Name				Nombre de Usuario
27		ID_NMS_DES				32			Priv Password DES			Priv Password DES
28		ID_NMS_MD5				32			Auth Password MD5			Auth Password MD5
29		ID_V3TRAP_ENABLE			16			Trap Enabled			Trap habilitada
30		ID_NMS_TRAP_IP				16			Trap IP				Trap IP
31		ID_NMS_V3TRAP_LEVE			32			Trap Security Level			Nivel de seguridad Trap
32		ID_V3DELETE				16			Delete				Eliminar
33		ID_NMS_USERNAME1			32			User Name				Nombre de Usuario
34		ID_NMS_DES1				32			Priv Password DES			Priv Password DES
35		ID_NMS_MD51				32			Auth Password MD5			Auth Password MD5
36		ID_V3TRAP_ENABLE1			16			Trap Enabled			Trap habilitada
37		ID_NMS_TRAP_IP1				16			Trap IP				Trap IP
38		ID_NMS_V3TRAP_LEVE1			32			Trap Security Level			Nivel de seguridad Trap
39		ID_DISABLE1				16			Disabled				Deshabilitado
40		ID_ENABLE1				16			Enabled				Habilitado
41		ID_ADD1					16			Add					Añadir
42		ID_MODIFY1				16			Modify				Modificar
43		ID_RESET1				16			Reset				Reiniciar
44		ID_ERROR0				32			Unknown error.			Error Desconocido
45		ID_ERROR1				32			Successful.				Correcto
46		ID_ERROR2				64			Failed. Controller is hardware protected.		Fallo. NCU protegida por HW.
47		ID_ERROR3				64			SNMPV3 functions are not enabled.			Las funciones SNMPV3 no están habilitadas
48		ID_ERROR4				64			Insufficient privileges for this function.		No tiene privilegios suficientes para esta función.
49		ID_ERROR5				128			Failed. Maximum number (16 for SNMPV2 accounts, 5 for SNMPV3 accounts) exceeded.	Fallo. Número máximo (16 para cuentas SNMPV2, y 5 para SNMPV3) excedido.
50		ID_TIPS1				64			Please select an NMS before continuing.		Seleccione un NMS antes de continuar
51		ID_TIPS2				64			IP address is not valid.		Dirección IP no válida.
52		ID_TIPS3				128			Only numbers, letters and '_' are permitted, can't be over 16 characters or blanks included.		Se permiten hasta 16 caracteres alfanuméricos incluyendo '_' y el espacio en blanco.
53		ID_TIPS4				128			Only numbers, letters and '_' are permitted, the length of characters is between 8 and 16.	Requiere entre 8 y 16 caracteres alfanuméricos, permitiendo el '_'.
54		ID_NMS_PUBLIC2				32			Public Community			Comunidad Pública
55		ID_NMS_PRIVATE2				32			Private Community			Comunidad Privada
56		ID_NMS_USERNAME2			32			User Name				Nombre de Usuario
57		ID_NMS_DES2				32			Priv Password DES			Priv Password DES	
58		ID_NMS_MD52				32			Auth Password MD5			Auth Password MD5
59		ID_LOAD					16			Loading				Cargando
60		ID_LOAD1				16			Loading				Cargando
61		ID_TIPS5				64			Failed, data format error.		Fallo: Error en formato de datos
62		ID_TIPS6				64			Failed. Please refresh the page.	Fallo. Refresque la página, por favor.
63		ID_DISABLE2				16			Disabled				Deshabilitado
64		ID_ENABLE2				16			Enabled				Habilitado
65		ID_DISABLE3				16			Disabled				Deshabilitado
66		ID_ENABLE3				16			Enabled				Habilitado
67		ID_IPV6					64			IPV6 address is not valid.		Dirección IPV6 no válida.
68		ID_IPV6_ADDR				64			IPV6				IPV6
69		ID_IPV6_ADDR1				64			IPV6				IPV6
70		ID_DISABLE2				16			    Disabled				Deshabilitado
71		ID_DISABLE3				16			    Disabled				Deshabilitado
72		ID_DISABLE4				16			Disabled				Deshabilitado
[tmp.setting_system_rect.html:Number]
64

[tmp.setting_system_rect.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Señal
2		ID_VALUE				32			Value				Valor
3		ID_TIME					32			Time Last Set			Ultimo Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					Fijar
6		ID_SET1					32			Set					Fijar

[tmp.system_dg.html:Number]
64

[tmp.system_dg.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Señal
2		ID_VALUE				32			Value				Valor
3		ID_DG					32			DG					DG
4		ID_SIGNAL1				32			Signal				Señal
5		ID_VALUE1				32			Value				Valor
6		ID_NO_DATA				32			No Data				No hay datos


[tmp.system_ac.html:Number]
64

[tmp.system_ac.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Señal
2		ID_VALUE				32			Value				Valor
3		ID_SIGNAL1				32			Signal				Señal
4		ID_VALUE1				32			Value				Valor
5		ID_SMAC					32			SMAC				SMAC
6		ID_AC_METER				32			AC Meter				Contador CA
7		ID_NO_DATA				32			No Data				No hay datos
8		ID_AC					32				AC				CA
9		ID_AC_UNIT				32			    AC Unit				Unidad de aire acondicionado

[tmp.system_smio.html:Number]
64

[tmp.system_smio.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Señal
2		ID_VALUE				32			Value				Valor
3		ID_SIGNAL1				32			Signal				Señal
4		ID_VALUE1				32			Value				Valor
5		ID_NO_DATA				32			No Data				No hay datos

[tmp.setting_alarm_equipment.html:Number]
1

[tmp.setting_alarm_equipment.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS					128			Please select equipment type	Seleccione un Tipo de Equipo
[tmp.setting_alarm_content.html:Number]
20

[tmp.setting_alarm_content.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS1					32			New Alarm Level			Nivel de Alarma
2		ID_TIPS2					32		Please select			Seleccione, por favor
3		ID_NA						16		NA					NA
4		ID_OA						16			OA				Observ
5		ID_MA						16		MA					Urgente
6		ID_CA						16		CA					Crítica
7		ID_TIPS3					32			New Relay Number		Número de relé
8		ID_TIPS4					32		Please select			Seleccione, por favor
9		ID_SET						16		Set					Fijar
10		ID_INDEX					16		Index				Índice
11		ID_NAME						16		Name				Nombre
12		ID_LEVEL					32		Alarm Level				Nivel Alarma
13		ID_ALARMREG					32		Relay Number			Número de relé
14		ID_MODIFY					32		Modify				Modificar
15		ID_NA1						16		NA					NA
16		ID_OA1						16			OA				Observ
17		ID_MA1						16		MA					Urgente
18		ID_CA1						16		CA					Crítica
19		ID_MODIFY1					32		Modify				Modificar
20		ID_TIPS5					32		No Data				No hay datos
21		ID_NA2						16		None			Ninguna
22		ID_NA3						16		None			Ninguna
23		ID_NA4						16		None				Ninguna	
24		ID_TIPS6					32		Please select				Seleccione, por favor
25		ID_TIPS7					32		Please select			Seleccione, por favor
26		ID_NA5						16		None				None	
[tmp.setting_converter.html:Number]
6

[tmp.setting_converter.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Señal
2		ID_VALUE				32			Value				Valor
3		ID_TIME					32			Time Last Set			Ultimo Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					Fijar
6		ID_SET1					32			Set					Fijar
7		ID_CONVERTER				32			Converter				Convertidores
8		ID_NO_DATA				16			No data.				No hay datos

[tmp.setting_clear_data.html:Number]
14

[tmp.setting_clear_data.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_CLEAR				16			Clear Data				Borrar Datos
2		ID_HISTORY_ALARM			64			Alarm History			Histórico de Alarmas
3		ID_HISTORY_DATA				64			Data History			Datos históricos
4		ID_HISTORY_CONTROL			64			Event Log				Registro de Eventos
5		ID_HISTORY_BATTERY			64			Battery Test Log			Registro de Prueba de Baterías
6		ID_HISTORY_DISEL_TEST			64			Diesel Test Log			Registro de pruebas G.E.
7		ID_CLEAR1				16			Clear			Borrar
8		ID_ERROR0				64			Failed to clear data.				Error al borrar datos
9		ID_ERROR1				64			Cleared.						Borrado
10		ID_ERROR2				64			Unknown error.					Error Desconocido			
11		ID_ERROR3				128			Failed. No privilege.				Fallo. No tiene privilegios
12		ID_ERROR4				64			Failed to communicate with the controller.		Fallo de comunicación con la NCU.		
13		ID_ERROR5				64			Failed. Controller is hardware protected.		Fallo. NCU protegida por HW.
14		ID_TIPS1				64			Clearing...please wait.		Borrando… espere.
15		ID_HISTORY_ALARM2			64			    Alarm History			Histórico de Alarmas
[tmp.setting_upload_download.html:Number]
14

[tmp.setting_upload_download.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_HEAD					128			Restore Factory Defaults						Restaurar valores de fábrica
2		ID_TIPS0				128			Restore default configuration? The system will reboot.		Restaurar configuración de fábrica? El Sistema se reiniciará.
3		ID_RESTORE_DEFAULT			64			Restore Defaults							Restaurar valores de fábrica
4		ID_TIPS1				128			Restore default config will cause system to reboot, are you sure?	Al restaurar valores de fábrica el Sistema se reiniciará. ¿Está seguro? 
5		ID_TIPS2				128			Cannot be restored. Controller is hardware protected.		No se puede restaurar. Sistema protegido por HW.
6		ID_TIPS3				128			Are you sure you want to reboot the controller?			¿Está seguro de que quiere reiniciar la NCU?
7		ID_TIPS4				128			Failed. No privilege.						Fallo. No tiene privilegios
8		ID_START_SCUP				32			Reboot controller							Reiniciar
9		ID_TIPS5				64			Restoring defaults...please wait.					Restaurando configuración de fábrica. Espere…
10		ID_TIPS6				64			Rebooting controller...please wait.					Reiniciando NCU. Espere…
11		ID_HEAD1				32			Upload/Download							Cargar/Descargar
12		ID_TIPS7				128			Upload/Download needs to stop the Controller. Do you want to stop the Controller?		La Carga/Descarga requiere parar la NCU. ¿Parar NCU?
13		ID_CLOSE_SCUP				16			Stop Controller				Parar NCU
14		ID_HEAD2				32			Upload/Download File			Cargar/Descargar archivo
15		ID_TIPS8				256			Caution: Only the file SettingParam.run or files with extension .tar or .tar.gz can be downloaded. If the downloaded file is NOT correct, the Controller will run abnormally. You must hit the START CONTROLLER button before leaving this screen.		Precaución: Solo se pueden cargar archivos de configuración (con extensión tar o tar.gz). Si el archivo no es correcto, puede que la NCU no funcione bien. ¡Tras la carga utilice el botón Iniciar NCU para reiniciar el sistema!
16		ID_FILE1				32			Select File					Seleccione archivo
17		ID_TIPS9				32			Browse...					Examinar...
18		ID_DOWNLOAD				64			Download to Controller			Cargar en NCU
19		ID_CONFIG_TAR				32			Configuration Package			Paquete de Configuración
20		ID_LANG_TAR				32			Language Package				Paquete de Lenguaje
21		ID_UPLOAD				64			Upload to Computer				Descargar en PC.
22		ID_STARTSCUP				64			Start Controller				Iniciar NCU
23		ID_STARTSCUP1				64			Start Controller			Iniciar NCU
24		ID_TIPS10				64			Stop controller...please wait.		Parando NCU…Espere.
25		ID_TIPS11				64			A file name is required.		Se requiere nombre de archivo.
26		ID_TIPS12				64			Are you sure you want to download?		¿Está seguro de que desea descargar?
27		ID_TIPS13				64			Downloading...please wait.			Descargando...Espere
28		ID_TIPS14				128			Incorrect file type or file name contains invalid characters.			Tipo o nombre de archivo incorrecto. Por favor, cargue archivos *.tar.gz o *.tar
29		ID_TIPS15				32			please wait...				Espere...
30		ID_TIPS16				128			Are you sure you want to start the controller?	¿Está seguro de iniciar la NCU?
31		ID_TIPS17				64			Controller is rebooting...			Reiniciando…
32		ID_FILE0				32			File in controller			Archivo en NCU
33		ID_CLOSE_ACU				32			Auto Config												Autoconfiguración							
34		ID_ERROR0				32			Unknown error.												Error Desconocido
35		ID_ERROR1				128			Auto configuration started. Please wait.									Autoconfiguración iniciada. Espere.							
36		ID_ERROR2				64			Failed to get.												Fallo									
37		ID_ERROR3				64			Insufficient privileges to stop the controller.								No tiene autorización para parar la NCU
38		ID_ERROR4				64			Failed to communicate with the controller.								Fallo de comunicación con la NCU.
39		ID_AUTO_CONFIG1				256		The controller will auto configure then restart. Please wait 2-5 minutes.		NCU en Autoconfiguración. Espere de 2 a 5 minutos.
40		ID_TIPS					128			This function will automatically configure SM units and modbus devices that have been connected to RS485 bus.	Esta función configurará los SM y dispositivos Modbus conectados al bus RS485.									
41		ID_HEAD3				32			Auto Config											Autoconfig
42		ID_TIPS18				32			Confirm auto configuration?										Confirmar autoconfiguración?
50		ID_TIPS3				128			Set successfully, controller is restarting, please wait		Fijado con éxito. La NCU se está reiniciando. Espere.
51		ID_TIPS4				16			seconds.				Segundos
52		ID_TIPS5				64			Returning to login page. Please wait.		Regresando a página de entrada. Espere…
59		ID_ERROR5				32			Unknown error.												Error Desconocido										
60		ID_ERROR6				128			Controller was stopped successfully. You can upload/download the file.						La NCU se ha parado con éxito. Ya puede cargar/descargar el archivo.
61		ID_ERROR7				64			Failed to stop the controller.										Fallo al parar la NCU.
62		ID_ERROR8				64			Insufficient privileges to stop the controller.								No tiene derechos para parar la NCU
63		ID_ERROR9				64			Failed to communicate with the controller.									Fallo al comunicar con la NCU
64		ID_GET_PARAM				32			Retrieve SettingParam.tar											Recuperar SettingParam.run
65		ID_TIPS19				128			Retrieve the current settings of the controller's adjustable parameters.					Recuperar la configuración actual de los parámetros configurables de la NCU.
66		ID_RETRIEVE				32			Retrieve File												Recuperar archivo
67		ID_ERROR10				32			Unknown error.												Error Desconocido	
68		ID_ERROR11				128			Retrieval successful.											Recuperado con éxito
69		ID_ERROR12				64			Failed to get.													Fallo			
70		ID_ERROR13				64			Insufficient privileges to stop the controller.								No tiene derechos para parar la NCU
71		ID_ERROR14				64			Failed to communicate with the controller.									Fallo de comunicación con la NCU.
72		ID_TIPS20				32			Please wait...												Espere…
73		ID_DOWNLOAD_ERROR0			32			Unknown error.							Error Desconocido.	
74		ID_DOWNLOAD_ERROR1			64			File downloaded successfully.					Descarga de archivo con éxito.	
75		ID_DOWNLOAD_ERROR2			64			Failed to download file.						Fallo en la descarga de archivo.	
76		ID_DOWNLOAD_ERROR3			64			Failed to download, the file is too large.				Fallo en la descarga, el archivo es demasiado grande.	
77		ID_DOWNLOAD_ERROR4			64			Failed. No privileges.						Fallo. Sin privilegios.?	
78		ID_DOWNLOAD_ERROR5			64			Controller started successfully.					Inicio de la controladora con éxito.	
79		ID_DOWNLOAD_ERROR6			64			File downloaded successfully.					Descarga de archivo con éxito.	
80		ID_DOWNLOAD_ERROR7			64			Failed to download file.						Fallo en la descarga de archivo.	
81		ID_DOWNLOAD_ERROR8			64			Failed to upload file.						Fallo al cargar el archivo.		
82		ID_DOWNLOAD_ERROR9			64			File downloaded successfully.					Descarga de archivo con éxito.	
83		ID_DOWNLOAD_ERROR10			64			Failed to upload file. Hardware is protected.			Fallo al cargar el archivo. El HW está protegido.
84		ID_CONFIG_TAR2				32			    Configuration Package			Paquete de Configuración
85		ID_DIAGNOSE					32			Retrieve Diagnostics Package		Recuperar paquete de diagnósticos
86		ID_TIPS21					128			Retrieve a diagnostics package to help troubleshoot controller issues			Recupere un paquete de diagnóstico para ayudar a solucionar problemas del controlador
87		ID_RETRIEVE1				32			Retrieve File						Recuperar archivo
[tmp.setting_power_system.html:Number]
14

[tmp.setting_power_system.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Señal
2		ID_VALUE				32			Value				Valor
3		ID_TIME					32			Time Last Set			Ultimo Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					Fijar
6		ID_SET1					32			Set					Fijar
7		ID_NO_DATA				16			No data.				No hay datos	
#8		ID_EIB					32			EIB Equipment			Equipo EIB
#9		ID_SMDU					32			SMDU Equipment			Equipo SMDU
#10		ID_LVD_GROUP				32			LVD Group				Grupo LVD
#changed by Frank Wu,1/1/32,20140312, for adding new web pages to the tab of "power system"
11		ID_SYSTEM_GROUP				32				Power System			Sistema
12		ID_AC_GROUP				32				AC Group			Grupo CA
13		ID_AC_UNIT				32				AC Equipment			Equipo CA
14		ID_ACMETER_GROUP			32				ACMeter Group			Grupo Contador CA
15		ID_ACMETER_UNIT				32				ACMeter Equipment		Equipo Contador CA
16		ID_DC_UNIT				32				DC Equipment			Equipo CC
17		ID_DCMETER_GROUP			32				DCMeter Group			Grupo Contador CC
18		ID_DCMETER_UNIT				32				DCMeter Equipment		Equipo Contador CC
19		ID_LVD_GROUP				32				LVD Group			Grupo LVD
20		ID_DIESEL_GROUP				32				Diesel Group			Grupo GE
21		ID_DIESEL_UNIT				32				Diesel Equipment		Equipo GE
22		ID_FUEL_GROUP				32				Fuel Group			Grupo Fuel
23		ID_FUEL_UNIT				32				Fuel Equipment			Equipo Fuel
24		ID_IB_GROUP				32				IB Group			Grupo IB
25		ID_IB_UNIT				32				IB Equipment			Equipo IB
26		ID_EIB_GROUP				32				EIB Group			Grupo EIB
27		ID_EIB_UNIT				32				EIB Equipment			Equipo EIB
28		ID_OBAC_UNIT				32				OBAC Equipment			Equipo OBAC
29		ID_OBLVD_UNIT				32				OBLVD Equipment			Equipo OBLVD
30		ID_OBFUEL_UNIT				32				OBFuel Equipment		Equipo OBFuel
31		ID_SMDU_GROUP				32				SMDU Group			Grupo SMDU
32		ID_SMDU_UNIT				32				SMDU Equipment			Equipo SMDU
33		ID_SMDUP_GROUP				32				SMDUP Group			Grupo SMDUP
34		ID_SMDUP_UNIT				32				SMDUP Equipment			Equipo SMDUP
35		ID_SMDUH_GROUP				32				SMDUH Group			Grupo SMDUH
36		ID_SMDUH_UNIT				32				SMDUH Equipment			Equipo SMDUH
37		ID_SMBRC_GROUP				32				SMBRC Group			Grupo SMBRC
38		ID_SMBRC_UNIT				32				SMBRC Equipment			Equipo SMBRC
39		ID_SMIO_GROUP				32				SMIO Group			Grupo SMIO
40		ID_SMIO_UNIT				32				SMIO Equipment			Equipo SMIO
41		ID_SMTEMP_GROUP				32				SMTemp Group			Grupo SMTemp
42		ID_SMTEMP_UNIT				32				SMTemp Equipment		Equipo SMTemp
43		ID_SMAC_UNIT				32				SMAC Equipment			Equipo SMAC
44		ID_SMLVD_UNIT				32				SMDU-LVD Equipment			Equipo SMDU-LVD
45		ID_LVD3_UNIT				32				LVD3 Equipment			Equipo LVD3
46		ID_SELECT_TYPE				48				Please select equipment type	Seleccione un Tipo de Equipo
47		ID_EXPAND				32				Expand				Expand
48		ID_COLLAPSE				32				Collapse			Collapse
49		ID_OBBATTFUSE_UNIT			32				OBBattFuse Equipment		Equipo OBBattFuse
50		ID_NARADA_BMS_UNIT			32				BMS			BMS
51		ID_NARADA_BMS_GROUP			32				BMS Group		GrupoBMS

[tmp.setting_language.html:Number]
14

[tmp.setting_language.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS1				16			Local Language			Idioma Local
2		ID_TIPS2				64			Please select language		Por favor, seleccione idioma
3		ID_GERMANY_TIP				64			Kontroller wird mit deutscher Sprache neustarten.					Kontroller wird mit deutscher Sprache neustarten.
4		ID_GERMANY_TIP1				128			Ausgewählte Sprache identisch mit lokaler Sprache, keine Änderung notwendig.	Ausgewählte Sprache identisch mit lokaler Sprache, keine Änderung notwendig.
5		ID_SPAISH_TIP				64			La controladora se va a reiniciar en Español.					La controladora se va a reiniciar en Español.
6		ID_SPAISH_TIP1				64			El idioma seleccionado es el mismo. No se necesita cambiar.				El idioma seleccionado es el mismo. No se necesita cambiar.
7		ID_FRANCE_TIP				64			Le controleur  redemarrera en Français.						Le controleur  redemarrera en Français.
8		ID_FRANCE_TIP1				128			La langue selectionnée est la langue locale, pas besoin de changer.			La langue selectionnée est la langue locale, pas besoin de changer.
9		ID_ITALIAN_TIP				64			Il controllore ripartirà in Italiano.						Il controllore ripartirà in Italiano.
10		ID_ITALIAN_TIP1				128			La Lingua selezionata è lo stessa della lingua locale, non serve cambiare.		La Lingua selezionata è lo stessa della lingua locale, non serve cambiare.
11		ID_RUSSIAN_TIP				128			Контроллер будет перезагружен на русском языке.					Контроллер будет перезагружен на русском языке.
12		ID_RUSSIAN_TIP1				128			Выбранный язык такой же, как и локальный, без необходимости изменения.		Выбранный язык такой же, как и локальный, без необходимости изменения.
13		ID_TURKEY_TIP				64			Controller will restart in Turkish.							Controller will restart in Turkish.
14		ID_TURKEY_TIP1				64			Selected language is same as local language, no need to change.			Selected language is same as local language, no need to change.
15		ID_TW_TIP				64			监控将会重启, 更改成\"繁体中文\".				监控将会重启, 更改成\"繁体中文\".
16		ID_TW_TIP1				64			选择的语言与本地语言一致, 无需更改.				选择的语言与本地语言一致, 无需更改.
17		ID_ZH_TIP				64			监控将会重启, 更改成\"简体中文\".				监控将会重启, 更改成\"简体中文\".
18		ID_ZH_TIP1				64			选择的语言与本地语言一致, 无需更改.				选择的语言与本地语言一致, 无需更改.
19		ID_SET					32			Set					Fijar
20		ID_LANGUAGETIPS				16			    Language			Idioma
21		ID_GERMANY				16			    Germany			Alemania
22		ID_SPAISH				16			    Spain			España
23		ID_FRANCE				16			    France			Francia
24		ID_ITALIAN				16			    Italy			Italia
25		ID_CHINA				16			    China			China
26		ID_CHINA2				16			    China			China
[tmp.system_udef.html:Number]
14

[tmp.system_udef.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_NO_DATA				32			No data				No hay datos
2		ID_USER_DEF				32			User Define				Usuario definido
3		ID_SIGNAL				32			Signal				Señal
4		ID_VALUE				32			Value				Valor
5		ID_SIGNAL1				32			Signal				Señal
6		ID_VALUE1				32			Value				Valor

[tmp.udef_setting.html:Number]
14

[tmp.udef_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_USER_SET				16			User Config			Config Usuario
2		ID_SIGNAL				32			Signal				Señal
3		ID_VALUE				32			Value				Valor
4		ID_TIME					32			Time Last Set			Ultimo Ajuste
5		ID_SET_VALUE				32			Set Value				Ajustar Valor
6		ID_SET					32			Set					Fijar
7		ID_SET1					32			Set					Fijar
8		ID_NO_DATA				16			No data				No hay datos

[tmp.udef_setting_2.html:Number]
14

[tmp.udef_setting_2.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_USER_SET				32			User Config2			Config2 de usuario
2		ID_SIGNAL				32			Signal				Señal
3		ID_VALUE				32			Value				Valor
4		ID_TIME					32			Time Last Set			Ultimo Ajuste
5		ID_SET_VALUE				32			Set Value				Ajustar Valor
6		ID_SET					32			Set					Fijar
7		ID_SET1					32			Set					Fijar
8		ID_NO_DATA				16			No data				No hay datos

[tmp.udef_setting_3.html:Number]
14

[tmp.udef_setting_3.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_USER_SET				16			User Config3			Config3 usuario
2		ID_SIGNAL				32			Signal				Señal
3		ID_VALUE				32			Value				Valor
4		ID_TIME					32			Time Last Set			Ultimo Ajuste
5		ID_SET_VALUE				32			Set Value				Ajustar Valor
6		ID_SET					32			Set					Fijar
7		ID_SET1					32			Set					Fijar
8		ID_NO_DATA				16			No data				No hay datos


#//changed by Frank Wu,3/3/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
[tmp.setting_mppt.html:Number]
14

[tmp.setting_mppt.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL						32					Signal				Señal
2		ID_VALUE						32					Value				Valor
3		ID_TIME							32					Time Last Set		Ultimo Ajuste
4		ID_SET_VALUE					32					Set Value			Ajustar Valor
5		ID_SET							32					Set					Fijar
6		ID_SET1							32					Set					Fijar
7		ID_NO_DATA						32					No Data				No hay datos
8		ID_MPPT							32					Solar				Solar

[tmp.shunt_setting.html:Number]
14

[tmp.shunt_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL						32					Signal			Señal
2		ID_VALUE						32					Value			Valor
3		ID_TIME							32					Time Last Set		Ultimo Ajuste
4		ID_SET_VALUE						32					Set Value		Ajustar Valor
5		ID_SET							32					Set			Fijar
6		ID_SET1							32					Set			Fijar
7		ID_NO_DATA						32					No Data			No hay datos
8		ID_SHUNT_SET						32					Shunt			Shunt de carga
9		ID_EQUIP						32					Equipment		Equipo

#//changed by Frank Wu,4/4/30,20140527, for add single converter and single solar settings pages
[tmp.single_converter_setting.html:Number]
6

[tmp.single_converter_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL					32						Signal				Señal
2		ID_VALUE					32						Value				Valor
3		ID_TIME						32						Time Last Set		Ultimo Ajuste
4		ID_SET_VALUE				32						Set Value			Ajustar Valor
5		ID_SET						32						Set					Fijar
6		ID_SET1						32						Set					Fijar
7		ID_RECT					32			Converter			Convertidor
8		ID_NO_DATA					16						No data.			No hay datos

[tmp.single_solar_setting.html:Number]
6

[tmp.single_solar_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL					32						Signal				Señal
2		ID_VALUE					32						Value				Valor
3		ID_TIME						32						Time Last Set		Ultimo Ajuste
4		ID_SET_VALUE				32						Set Value			Ajustar Valor
5		ID_SET						32						Set					Fijar
6		ID_SET1						32						Set					Fijar
7		ID_RECT						32						Solar Conv			Conv Solar
8		ID_NO_DATA					16						No data.			No hay datos

[tmp.cabinet_branch.html:Number]
6

[tmp.cabinet_branch.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS1				32			Please select		Seleccionar
2		ID_TIPS2				32			Please select		Seleccionar
3		ID_TIPS3				32			Please select		Seleccionar
4		ID_TIPS4				32			Please select		Seleccionar
[tmp.cabinet_map.html:Number]
6

[tmp.cabinet_map.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_MAJOR				16			Major				Urgente
2		ID_OBS					16			Observation				Observación
3		ID_NORMAL				16			Normal				Normal
4		ID_NO_DATA				16			No Data				No hay datos
5		ID_SELECT				32			Please select			Seleccionar
6		ID_NO_DATA1				16			No Data				No hay datos


[tmp.cabinet_set.html:Number]
6

[tmp.cabinet_set.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SET					16			Set					Fijar
2		ID_BRANCH				16			Branches				Ramas
3		ID_TIPS					16			is selected				seleccionada
4		ID_RESET				32			Reset				Reiniciar
5		ID_TIPS1				32			Show Designation			Mostrar Designación
6		ID_TIPS2				32			Close Detail			Cerrar detalle
9		ID_TIPS3				32			Success to select.			Seleccionado con éxito
10		ID_TIPS4				32			Click to delete			Clic para borrar
11		ID_TIPS5				128			Please select a cabinet to set.	Seleccione un bastidor a configurar
12		ID_TIPS6				64			This Cabinet has been set		Este bastidor ha sido configurado
13		ID_TIPS7				64			branches, confirm to cancel?	Ramas, confirma Cancelar?
14		ID_TIPS8				32			Please select			Seleccionar
15		ID_TIPS9				32			no allocation			Sin asignación
16		ID_TIPS10				128			Please add more branches for the selected cabinet.		Añada más ramas al bastidor seleccionado.
17		ID_TIPS11				64			Success to deselect.		Deseleccionado con éxito
18		ID_TIPS12				64			Reset cabinet...please wait.	Reiniciando Bastidor…Espere
19		ID_TIPS13				64			Reset Successful, please wait.	Reiniciado con éxito. Espere…
20		ID_ERROR1				16			Failed.				Fallo
21		ID_ERROR2				16			Successful.				Éxito
22		ID_ERROR3				32			Insufficient privileges.		Autoridad insuficiente
23		ID_ERROR4				64			Failed. Controller is hardware protected.		Fallo. NCU protegida por HW
24		ID_TIPS14				32			Exceed 20 branches.			Excede las 20 ramas
25		ID_TIPS15				128			More than 16 characters, or you have input invalid characters.		Más de 16 caracteres, o que tienen caracteres de entrada no válida.
26		ID_TIPS16				256			Alarm level value can only be integer or just a decimal, and Alarm Level2 must be larger than or equal to Alarm Level1.	El valor del Nivel de Alarma sólo puede ser un número entero o un decimal,y alarma de nivel 2 debe ser mayor o igual que la alarma de nivel 1.
27		ID_TIPS17				32			Show Parameter			Mostrar Parámetro
28		ID_SET1					16			Designate				Designar
29		ID_SET2					16			Set					Fijar
30		ID_NAME					16			Name				Nombre
31		ID_LEVEL1				32			Alarm Level1			Nivel1 de alarma
32		ID_LEVEL2				32			Alarm Level2			Nivel2 de alarma
33		ID_RATING				32			Rating Current			Corriente estimada
34		ID_TIPS18				128			Rating current must be from 0 to 10000, and can only have one decimal at most.	El rango de corriente estimada es de 0 a 10000, con un decimal como mínimo.
35		ID_SET3					16			Set					Fijar
36		ID_TIPS19				64			Confirm to reset the current cabinet?	Confirmar reinicio bastidor actual?

#//changed by Frank Wu,2/N/35,20140527, for adding the the web setting tab page 'DI'
[tmp.setting_di_content.html:Number]
6

[tmp.setting_di_content.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS1					32			Signal Full Name				Nombre Completo Señal
2		ID_TIPS2					32			Signal Abbr Name			Nombre abrev Señal
3		ID_TIPS3					32			New Alarm Level					Nuevo Nivel de Alarma
4		ID_TIPS4					32			Please select					Seleccionar
5		ID_TIPS5					32			New Relay Number				Nuevo Número de relé
6		ID_TIPS6					32			Please select					Seleccionar
7		ID_TIPS7					32			New Alarm State					Estado Nueva Alarma
8		ID_TIPS8					32			Please select					Seleccionar
9		ID_NA						16			NA						NA
10		ID_OA						16			OA						OA
11		ID_MA						16			MA						MA
12		ID_CA						16			CA						CA
13		ID_NA2						16			None							Ninguno
14		ID_LOW						16			Low							Bajo
15		ID_HIGH						16			High							Alto
16		ID_SET						16			Set							Fijar
17		ID_TITLE					16			DI Alarms						Alarmas DI
18		ID_INDEX					16			Index							Índice
19		ID_EQUIPMENT				32			Equipment Name					Nombre Equipo
20		ID_SIGNAL_NAME				32			Signal Name						Nombre Señal
21		ID_ALARM_LEVEL				32			Alarm Level						Nivel Alarma
22		ID_ALARM_STATE				32			Alarm State						Estado Alarma
23		ID_REPLAY_NUMBER			32			Alarm Relay						Relé de Alarma
24		ID_MODIFY					32			Modify							Modificar
25		ID_NA1						16			NA							NA
26		ID_OA1						16			OA							OA
27		ID_MA1						16			MA							MA
28		ID_CA1						16			CA							CA
29		ID_LOW1						16			Low							Bajo
30		ID_HIGH1					16			High							Alto
31		ID_NA3						16			None							Ninguno
32		ID_MODIFY1					32			Modify							Modificar
33		ID_NO_DATA					32			No Data							No hay datos
34		ID_NA4						16			NA							NA
35		ID_POWER_SYSTEM					16			Power System						Sis Energía
36		ID_DO_NCU					32			NCU				NCU
37		ID_TIPS9					32			Please select					Seleccionar
38		ID_TIPS10					32			Please select					Seleccionar
39		ID_TIPS11					32			Please select					Seleccionar
40		ID_NA5						16			NA							NA
#//changed by Frank Wu,2/N/14,20140527, for system log
[tmp.history_systemlog.html:Number]
23

[tmp.history_systemlog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_INDEX				32			Index				Índice
2		ID_TASK_NAME			32			Task Name				Nombre de Tarea
3		ID_INFO_LEVEL			32			Info Level				Nivel Info
4		ID_LOG_TIME				32			Time			Hora
5		ID_INFORMATION			32			Information				Información
8		ID_FROM					32			From			De
9		ID_TO					32			To				a
10		ID_QUERY				32			Query			Consulta
11		ID_UPLOAD				32			Upload			Descargar
12		ID_TIPS					64			Displays the last 500 entries		Muestras las últimas 500 entradas
13		ID_QUERY_TYPE				16			Query Type				Tipo de Consulta
14		ID_SYSTEM_LOG				32			System Log				Registro del Sistema
16		ID_CTL_RESULT0				64			Successful				Con éxito		
17		ID_CTL_RESULT1				64			No Memory				Sin memoria		
18		ID_CTL_RESULT2				64			Time Expired			Tiempo Agotado		
19		ID_CTL_RESULT3				64			Failed				Fallo		
20		ID_CTL_RESULT4				64			Communication Busy			Comunicación ocupada	
21		ID_CTL_RESULT5				64			Control was suppressed.		Control suprimido	
22		ID_CTL_RESULT6				64			Control was disabled.		Control deshabilitado	
23		ID_CTL_RESULT7				64			Control was canceled.		Control cancelado	
24		ID_SYSTEM_LOG2				16			    System Log				Registro del Sistema
25		ID_SYSTEMLOG			32				System Log				Registro del Sistema

#//changed by Frank Wu,2/N/27,20140527, for power split
[tmp.setting_power_split.html:Number]
6

[tmp.setting_power_split.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_LVD1							32			LVD1							LVD1
2		ID_LVD2							32			LVD2							LVD2
3		ID_LVD3							32			LVD3							LVD3
4		ID_BATTERY_TEST					32			BATTERY_TEST					PRUEBA_BATERIA
5		ID_EQUALIZE_CHARGE				32			EQUALIZE_CHARGE					CARGA_BATERIA
6		ID_SAMP_TYPE					16			Sample							Muestra
7		ID_CTRL_TYPE					16			Control							Control
8		ID_SET_TYPE						16			Setting						Ajustes
9		ID_ALARM_TYPE					16			Alarm							Alarma
10		ID_SLAVE						16			SLAVE							ESCLAVO
11		ID_MASTER						16			MASTER							MAESTRO
12		ID_SELECT						32			Please select					Seleccionar
13		ID_NA							16			NA								NA
14		ID_EQUIP						16			Equipment						Equipo
15		ID_SIG_TYPE						16			Signal Type						Tipo de Señal
16		ID_SIG							16			Signal							Señal
17		ID_MODE							32			Power Split Mode				Modo PowerSplit
18		ID_SET							16			Set							Fijar
19		ID_SET1							16			Set							Fijar
20		ID_TITLE						32			Power Split						Power Split
21		ID_INDEX						16			Index							Índice
22		ID_SIG_NAME						32			Signal							Señal
23		ID_EQUIP_NAME					32			Equipment						Equipo
24		ID_SIG_TYPE						32			Signal Type						Tipo de Señal
25		ID_SIG_NAME1					32			Signal Name						Nombre Señal
26		ID_MODIFY						32			Modify							Modificar
27		ID_MODIFY1						32			Modify							Modificar
28		ID_NO_DATA						32			No Data							No hay datos

[tmp.batt_setting.html:Number]
7

[tmp.batt_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Señal
2		ID_VALUE				32			Value				Valor
3		ID_TIME					32			Time Last Set			Ultimo Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					Fijar
6		ID_SET1					32			Set					Fijar
7		ID_BACK					16			Back				Atrás
8		ID_NO_DATA				32			No Data				No hay datos
[tmp.system_lvd_fuse.html:Number]
7

[tmp.system_lvd_fuse.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Señal
2		ID_VALUE				32			Value				Valor
3		ID_TIME					32			Time Last Set			Ultimo Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					Fijar
6		ID_SET1					32			Set					Fijar
7		ID_BACK					16			Back				Atrás
8		ID_NO_DATA				32			No Data				No hay datos
9		ID_SIGNAL1				32			Signal				Señal
10		ID_VALUE1				32			Value				Valor

[tmp.system_battery_tabs.html:Number]
7

[tmp.system_battery_tabs.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_BATTERY				32			Battery				Batería
2		ID_BATT_FUSE_GROUP			32			Battery Fuse Group			Grupo Fusibles Batería
3		ID_BATT_FUSE				32			Battery Fuse			Fusible Batería
4		ID_LVD_GROUP				32			LVD Group				Grupo LVD
5		ID_LVD_UNIT				32			LVD Unit				Unidad LVD
6		ID_SMDU_BATT_FUSE			32			SMDU Battery Fuse			Fusible Batería SMDU
7		ID_SMDU_LVD				32			SMDU LVD				LVD SMDU
8		ID_LVD3_UNIT				32			LVD3 Unit				Unidad LVD3
#//changed by Frank Wu,N/N/N,20140613, for two tabs
[tmp.setting_tabs.html:Number]
7

[tmp.setting_tabs.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_CHARGE				32			Battery Charge			Carga Batería
2		ID_ECO					32			ECO						ECO
3		ID_LVD					32			LVD						LVD
4		ID_QUICK_SET			32			Quick Settings			Ajuste Rápido
5		ID_TEMP					32			Temperature				Temperatura
6		ID_RECT					32			Rectifiers				Rectificadores
7		ID_CONVERTER				32			DC/DC Converters			Convertidores CC/CC
8		ID_BATT_TEST				32			Battery Test			Prueba de Batería
9		ID_TIME_CFG				32			Time Settings			Fecha y Hora
10		ID_USER_DEF_SET_1			32			User Config1			Config Usuario 1
11		ID_USER_SET2				32			User Config2			Config Usuario 2
12		ID_USER_SET3				32			User Config3			Config Usuario 3
13		ID_MPPT					32			Solar				Solar
14		ID_POWER_SYS				32			System				Sistema
15		ID_CABINET_SET				32			Set Cabinet				Bastidores
16		ID_USER_SET4			32			User Config4				Config Usuario 4

[tmp.system_tabs_a.html:Number]
7

[tmp.system_tabs_a.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_RECTIFIER				32			Rectifier				Rectificador
2		ID_RECTIFIER1				32			GI Rectifier			Rectificadores GI
3		ID_RECTIFIER2				32			GII Rectifier			Rectificadores GII
4		ID_RECTIFIER3				32			GIII Rectifier			Rectificadores GIII
5		ID_CONVERTER				32			Converter				Convertidor
6		ID_SOLAR					32			Solar Converter			Convertidor Solar

[tmp.consumption_map.html:Number]
7

[tmp.consumption_map.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_TIPS1				32			branch number			Número de ramas
2		ID_TIPS2				32			total current			Corriente total
3		ID_TIPS3				32			total power				Potencia total
4		ID_TIPS4				32			total energy			Energía total
5		ID_TIPS5				32			peak power last 24h			Pico Potencia últimas 24h
6		ID_TIPS6				32			peak power last week		Pico Potencia última semana
7		ID_TIPS7				32			peak power last month		Pico Potencia último mes

[tmp.system_dc_smdup.html:Number]
7

[tmp.system_dc_smdup.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Señal
2		ID_VALUE				32			Value				Valor
3		ID_SIGNAL1				32			Signal				Señal
4		ID_VALUE1				32			Value				Valor
5		ID_NO_DATA				32			No Data				No hay datos

[tmp.adv_setting_tabs.html:Number]
25

[tmp.adv_setting_tabs.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_IPV4					32			Ethernet				Ethernet
2		ID_SCUP_IP				32			IP Address				Dirección IP
3		ID_SCUP_MASK				32			Subnet Mask				Máscara Subred	
4		ID_SCUP_GATEWAY				32			Default Gateway			Puerta de Enlace
5		ID_ERROR0				32			Setting Failed.			Fallo Ajuste			
6		ID_ERROR1				32			Successful.				Correcto.		
7		ID_ERROR2				64			Failed. Incorrect input.				Fallo. Entrada Incorrecta.			
8		ID_ERROR3				64			Failed. Incomplete information.			Fallo. Información incompleta		
9		ID_ERROR4				64			Failed. No privilege.				Fallo. No autorizado			
10		ID_ERROR5				128			Failed. Controller is hardware protected.		Fallo. Controladora protegida por HW	
11		ID_ERROR6				32			Failed. DHCP is ON.					Fallo. DHCP activado			
12		ID_TIPS0				32			Set Network Parameter														Fijar Parámetros de Red											
13		ID_TIPS1				128			Units IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171.					Dirección IP Unidades incorrecta. \nFormato: 'nnn.nnn.nnn.nnn'. Ejemplo 10.75.14.171				
14		ID_TIPS2				128			Mask IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 255.255.0.0.						Máscara Subred incorrecta. \nFormato: 'nnn.nnn.nnn.nnn'. Ejemplo 255.255.0.0				
15		ID_TIPS3				64			Units IP Address and Mask mismatch.													Discrepancia Dirección IP y Máscara.												
16		ID_TIPS4				128			Gateway IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171. Enter 0.0.0.0 for no gateway	Puerta de Enlace incorrecta (Gateway). \nFormato: 'nnn.nnn.nnn.nnn'. Ejemplo 10.75.14.171. Deje 0.0.0.0 para no Gateway
17		ID_TIPS5				128			Units IP Address, Gateway, Mask mismatch. Enter Address again.									Discrepancia Dirección IP, Máscara y Gateway. Entre la dirección otra vez								
18		ID_TIPS6				64			Please wait. Controller is rebooting.												Reiniciando. Espere…											
19		ID_TIPS7				128			Parameters have been modified.  Controller is rebooting...										Los parámetros han sido modificados. Reiniciando…									
20		ID_TIPS8				64			Controller homepage will be refreshed.												La página inicial de la controladora se refrescará.											
21		ID_TIPS9				128			Confirm the change to the IP address?				Confirma el cambio de dirección IP?
22		ID_SAVE					16			Save					Salvar			
23		ID_TIPS10				32			IP Address Error				Error Dirección IP		
24		ID_TIPS11				32			Subnet Mask Error				Error Máscara Subred		
25		ID_TIPS12				32			Default Gateway Error			Error Puerta de Enlace (Gateway)	
26		ID_USER					32			Users					Usuarios			
27		ID_IPV4_1					32		IPV4					IPV4			
28		ID_IPV6					32			IPV6					IPV6			
29		ID_HLMS_CONF				32			Monitor Protocol				Protocolos		
30		ID_SITE_INFO				32			Site Info					Info Planta			
31		ID_AUTO_CONFIG				32			Auto Config					Autoconfig			
32		ID_OTHER				32			Alarm Report				Informe Alarmas		
33		ID_NMS					32			SNMP					SNMP			
34		ID_ALARM_SET				32			Alarms					Alarmas		
35		ID_CLEAR_DATA				16			Clear Data					Borrar Datos		
36		ID_RESTORE_DEFAULT			32			Restore Defaults				Restaurar Valores de Fábrica		
37		ID_DOWNLOAD_UPLOAD			32			SW Maintenance				Mantenimiento SW	
38		ID_HYBRID				32			Generator					Generador			
39		ID_DHCP					16			IPV4 DHCP					DHCP IPV4			
40		ID_IP1					32			Server IP					IP Sevidor			
41		ID_TIPS13				16			Loading					Cargando		
42		ID_TIPS14				16			Loading					Cargando		
43		ID_TIPS15				32			failed, data format error!				Fallo, error formato datos!	
44		ID_TIPS16				32			failed, please refresh the page!		Fallo. Refresque la página!
45		ID_LANG					16			Language			Idioma
#//changed by Frank Wu,2/2/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
46		ID_SHUNT_SET			32				Shunts				Shunts
#//changed by Frank Wu,1/N/35,20140527, for adding the the web setting tab page 'DI'
47		ID_DI_ALARM_SET			32				DI Alarms			Alarmas DI
#//changed by Frank Wu,1/1/N,00000000, for add PowerSplit
48		ID_POWER_SPLIT_SET		32				Power Split			Potencia dividida
49		ID_IPV6_1			16				IPV6				IPV6
50		ID_LOCAL_IP			32				Link-Local Address		Dirección Link-Local	
51		ID_GLOBAL_IP			32				IPV6 Address			Dirección IPV6		
52		ID_SCUP_PREV			16				Subnet Prefix			Prefijo Subred		
53		ID_SCUP_GATEWAY1		32				Default Gateway			Puerta de Enlace (Gateway)	
54		ID_SAVE1			16				Save				Salvar			
55		ID_DHCP1			16				IPV6 DHCP			DHCP IPV6		
56		ID_IP2				32				Server IP			IP Sevidor		
57		ID_TIPS17			64				Please fill in the correct IPV6 address.	Entre una dirección IPV6 correcta	
58		ID_TIPS18			128				Prefix can not be empty or beyond the range.	Prefijo vacío o fuera de rango.
59		ID_TIPS19			64				Please fill in the correct IPV6 gateway.	Introduzca un Gateway IPV6 correcto	
60		ID_TL1_GROUP			16				TL1 AID Group					Grupo AID TL1				
61		ID_TL1_SIGNAL			16				TL1 AID Signal					Señal AID TL1					
62		ID_DO_RELAY				32			DO(relays)						DO(Relé)
63		ID_SHUNTS_SET				32			Shunts						Shunts
64		ID_CR_SET				32			Fuses						Fusibles
65		ID_CUSTOM_INPUT				32			Custom Inputs						Entradas personalizadas
66		ID_ANALOG_SET				32			Analogs						Término análogo

																
[tmp.setting_TL1_group.html:Number]
25

[tmp.setting_TL1_group.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_AID_GROUP				16			AID Group				Grupo AID	
2		ID_AID_NAME				16			AID Name				Nombre AID		
3		ID_AID_PREFIX				32			Sub AID Name Prefix			Prefijo Nombre SubAID
4		ID_AID_TYPE				16			AID Type				Tipo AID		
5		ID_EQPT					16			EQPT				EQPT		
6		ID_ENV					16			ENV					ENV			
7		ID_AID_INDEX				16			index				Índice	
8		ID_SUBMIT				16			Submit				Aceptar	
12		ID_ERROR3				64			Insufficient privileges for this function.		No está autorizado para esta función
13		ID_ERROR4				32			Invalid character(s).			Carácter inválido			
14		ID_ERROR5				32			Can't be empty.			No puede ser vacío			
15		ID_AID_NAME1				16			AID Name				Nombre AID				
16		ID_AID_PREFIX1				32			Sub AID Name Prefix			Prefijo Nombre SubAID		
17		ID_AID_TYPE1				16			AID Type				Tipo AID				
18		ID_TL1_ERRORS1				32			Successful.				Correcto				
19		ID_TL1_ERRORS2				32			Configuration fail.			Fallo configuración			
20		ID_TL1_ERRORS3				64			Failed. Service was exited.		Fallo. Servicio parado		
21		ID_TL1_ERRORS4				32			Invalid parameters.			Parámetros inválidos			
22		ID_TL1_ERRORS5				32			Invalid parameters.			Parámetros inválidos			
23		ID_TL1_ERRORS6				64			Fail to write to Flash.		Fallo escritura Flash	
24		ID_TL1_ERRORS7				32			Service is busy.			Servicio ocupado			
25		ID_TL1_ERRORS8				64			Port has already been occupied.	El puerto ya está ocupado
26		ID_TL1_ERRORS9				64			Port has already been occupied.	El puerto ya está ocupado
27		ID_TL1_ERRORS10				64			Port has already been occupied.	El puerto ya está ocupado
28		ID_TL1_ERRORS11				64			Port has already been occupied.	El puerto ya está ocupado
29		ID_TL1_ERRORS12				64			The TL1 feature is not available.	La función TL1 no está disponible
30		ID_ERROR6				128			The length sum of AID name and Sub AID name prefix exceed range.	La suma de longitud del nombre AID y el prefijo SubAID excede el rango

[tmp.setting_TL1_signal.html:Number]
25

[tmp.setting_TL1_signal.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_AID_GROUP				16			AID Group				Grupo AID		
2		ID_AID_PREFIX				32			Sub AID Name Prefix			Prefijo Nombre SubAID
3		ID_AID_INDEX				16			index				Índice		
4		ID_AID_NAME				16			AID Name				Nombre AID		
8		ID_ERROR3				64			Insufficient privileges for this function.		No está autorizado para esta función.
9		ID_ERROR4				32			Invalid character(s).			Carácter inválido.		
10		ID_ERROR5				32			Can't be empty.			No puede ser vacío.		
11		ID_ACTIVE				16			Signal Active			Señal Activa		
12		ID_SAMPLE				16			Sample Signal			Señal Muestreo		
13		ID_ALARM				16			Alarm Signal			Señal Alarma		
14		ID_SETTING				16			Setting Signal			Señal Ajuste	
15		ID_CON_TYPE				32			Condition Type			Consulta	
16		ID_CON_DESC				32			Condition Description		Descripción Consulta	
17		ID_NOTI_CODE				32			Notification Code		Código Notificación		
18		ID_SERV_CODE				32			Service Affect Code			Código Servicio Afectado		
19		ID_MONI_FORT				32			Monitor Format			Formato Monitor	
20		ID_MONI_TYPE				32			Monitor Type			Tipo Monitor		
21		ID_SUBMIT				16			Submit				Presentar		
22		ID_TIPS1				64			Please Select AID Group and Equip		Seleccione Grupo AID y Equipo
23		ID_TIPS2				32			Please Select Signal		Seleccione Señal
24		ID_TIPS3				16			Please Select			Seleccionar	
25		ID_EQUIP_LIST				16			Equip List				Lista de Equipos		
26		ID_TIPS4				16			Please Select			Seleccionar	
27		ID_TIPS5				64			First select AID Group, then select Equip.		Primero seleccione el Grupo AID y luego el Equipo
28		ID_TL1_ERRORS1				32			Successful.				Correcto.			
29		ID_TL1_ERRORS2				32			Configuration fail.			Fallo configuración.		
30		ID_TL1_ERRORS3				64			Failed. Service was exited.		Fallo. Servicio parado
31		ID_TL1_ERRORS4				32			Invalid parameters.			Parámetros inválidos.		
32		ID_TL1_ERRORS5				32			Invalid parameters.			Parámetros inválidos.		
33		ID_TL1_ERRORS6				64			Fail to write to Flash.		Fallo escritura Flash.	
34		ID_TL1_ERRORS7				32			Service is busy.			Servicio ocupado.		
35		ID_TL1_ERRORS8				64			Port has already been occupied.	El puerto ya está ocupado.
36		ID_TL1_ERRORS9				64			Port has already been occupied.	El puerto ya está ocupado.
37		ID_TL1_ERRORS10				64			Port has already been occupied.	El puerto ya está ocupado.
38		ID_TL1_ERRORS11				64			Port has already been occupied.	El puerto ya está ocupado.
39		ID_TL1_ERRORS12				64			Error, TL1 is an option and is not active.	Error, TL1 es opcional y no está activo..
[tmp.setting_relay_content.html:Number]
12

[tmp.setting_relay_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_DO_RELAY				32			DO(Relays)				DO(Relé)
2		ID_SIGNAL_NAME				32			Signal Name				Nombre de Señal
3		ID_VALUE				32			Value						Valor
4		ID_SET_VALUE			32			Set Value					Ajustar Valor
5		ID_SET					32			Set							Fijar
6		ID_SET1					32			Set							Fijar
7		ID_SET2					32			Set							Fijar
8		ID_SET3					32			Set							Fijar
9		ID_MODIFY				32			Modify							Modificar
10		ID_MODIFY1				32			Modify							Modificar
11		ID_NO_DATA				32			No Data							No hay datos
12		ID_TIPS1				32			Signal Full Name				Nombre de Señal Completa
13		ID_INPUTTIP				32			Max Characters:20				Carácter Max:20
14		ID_INPUTTIP2				32			Max Characters:32				Carácter Max:32
15		ID_INPUTTIP3				32			Max Characters:16				Carácter Max:16
16		ID_DO_NCU				32			NCU						NCU		
17		ID_DO_POEWRSYSTEM			32			Power System					Power System	
18		ID_INPUTTIP4				32			Max Characters:20				Max Characters:20

[tmp.setting_shunts_content.html:Number]
11

[tmp.setting_shunts_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_REFERENCE				32			Reference				Referencia
2		ID_SIGNAL_SHUNTNAME				32			Shunt Name				Nombre Shunt
3		ID_MODIFY				32			Modify						Modificar
4		ID_VIEW				32			View					Ver
5		ID_FULLSC				32			Full Scale Current					Corriente Escala Completa
6		ID_FULLSV			32			Full Scale Voltage					Voltaje Escala Completa
7		ID_BREAK_VALUE				32			Break Value					Valor Receso
8		ID_CURRENT_SETTING				32			Current Setting					Configuración Actual
9		ID_NEW_SETTING				32			New Setting					Nuevo Configuración
10		ID_RANGE				32			Range					Oscilar
11		ID_SETAS				32			Set As					Fijar Como
12		ID_SIGNAL_FULL_NAME				32			Signal Full Name					Nombre de Señal Completa
13		ID_SIGNAL_ABBR_NAME				32			Signal Abbr Name					Nombre de señal Abbr
14		ID_SHUNT1				32			Shunt 1					Derivación 1
15		ID_SHUNT2				32			Shunt 2					Derivación 2
16		ID_SHUNT3				32			Shunt 3					Derivación 3
17		ID_SHUNT4				32			Shunt 4					Derivación 4
18		ID_SHUNT5				32			Shunt 5					Derivación 5
19		ID_SHUNT6				32			Shunt 6					Derivación 6
20		ID_SHUNT7				32			Shunt 7					Derivación 7
21		ID_SHUNT8				32			Shunt 8					Derivación 8
22		ID_SHUNT9				32			Shunt 9					Derivación 9
23		ID_SHUNT10				32			Shunt 10					Derivación 10
24		ID_SHUNT11				32			Shunt 11					Derivación 11
25		ID_SHUNT12				32			Shunt 12					Derivación 12
26		ID_SHUNT13				32			Shunt 13					Derivación 13
27		ID_SHUNT14				32			Shunt 14					Derivación 14
28		ID_SHUNT15				32			Shunt 15					Derivación 15
29		ID_SHUNT16				32			Shunt 16					Derivación 16
30		ID_SHUNT17				32			Shunt 17					Derivación 17
31		ID_SHUNT18				32			Shunt 18					Derivación 18
32		ID_SHUNT19				32			Shunt 19					Derivación 19
33		ID_SHUNT20				32			Shunt 20					Derivación 20
34		ID_SHUNT21				32			Shunt 21					Derivación 21
35		ID_SHUNT22				32			Shunt 22					Derivación 22
36		ID_SHUNT23				32			Shunt 23					Derivación 23
37		ID_SHUNT24				32			Shunt 24					Derivación 24
38		ID_SHUNT25				32			Shunt 25					Derivación 25
39		ID_LOADSHUNT				32			Load Shunt					Carga Derivación
40		ID_BATTERYSHUNT				32			Battery Shunt					Derivación Batería
41		ID_TIPS1					32			Please select					Favor Seleccione
42		ID_TIPS2					32			Please select					Favor Seleccione
43		ID_NA						16			NA								NA
44		ID_OA						16			OA								OA
45		ID_MA						16			MA								MA
46		ID_CA						16			CA								CA
47		ID_NA2						16			None							Ninguna
48		ID_NOTUSEd						16			Not Used							No Utilizado
49		ID_GENERL						16			General							General
50		ID_LOAD						16			Load							Carga
51		ID_BATTERY						16			Battery							Batería
52		ID_NA3						16			NA								NA
53		ID_OA2						16			OA								OA
54		ID_MA2						16			MA								MA
55		ID_CA2						16			CA								CA
56		ID_NA4						16			None							Ninguna
57		ID_MODIFY				32			Modify						Modificar
58		ID_VIEW				32			View					Ver
59		ID_SET						16			set							Fijar
60		ID_NA1						16			None								Ninguna
61		ID_Severity1				32			Alarm Relay				Relé de Alarma
62		ID_Relay1				32			Alarm Severity				Gravedad Alarma
63		ID_Severity2				32			Alarm Relay				Relé de Alarma
64		ID_Relay2				32			Alarm Severity				Gravedad Alarma
65		ID_Alarm				32			Alarm				Alarma
66		ID_Alarm2				32			Alarm				Alarma
67		ID_INPUTTIP				32			Max Characters:20					Carácter Max:20
68		ID_INPUTTIP2				32			Max Characters:20					Carácter Max:20
69		ID_INPUTTIP3				32			Max Characters:8					Carácter Max:8
70		ID_FULLSV				32			Full Scale Voltage				Voltaje Escala Completa
71		ID_FULLSC				32			Full Scale Current				Corriente Escala Completa
72		ID_BREAK_VALUE2				32			Break Value					Valor Receso
73		ID_High1CLA				64			High 1 Curr Limit Alarm					Alarma Alta 1 Límite de corriente
74		ID_High1CAS				64			High 1 Curr Alarm Severity			Alta 1 Corriente Gravedad de Alarma
75		ID_High1CAR				32			High 1 Curr Alarm Relay					Alta 1 Relé de Alarma Corriente
76		ID_High2CLA				64			High 2 Curr Limit Alarm					Alarma Alta 2 Límite de corriente
77		ID_High2CAS				64			High 2 Curr Alarm Severity			Alta 2 Corriente Gravedad de Alarma
78		ID_High2CAR				32			High 2 Curr Alarm Relay					Alta 2 Relé de Alarma Corriente
79		ID_HI1CUR				32			Hi1Cur				Al1Cor
80		ID_HI2CUR				32			Hi2Cur				Al2Cor
81		ID_HI1CURRENT				32			High 1 Curr				Alta 1 Corr
82		ID_HI2CURRENT				32			High 2 Curr				Alta 2 Corr
83		ID_NA4					16			NA								NA


[tmp.setting_fuse_content.html:Number]
12

[tmp.setting_fuse_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL_NAME				32			Signal Name				Nombre de Señal
2		ID_MODIFY				32			Modify				Modificar
3		ID_SET				32			Set				Fijar
4		ID_TIPS				32			Signal Full Name				Nombre de Señal Completa
5		ID_MODIFY1				32			Modify				Modificar
6		ID_TIPS2				32			Signal Abbr Name				Nombre de señal Abbr
7		ID_INPUTTIP				32			Max Characters:20				Carácter Max:20
8		ID_INPUTTIP2				32			Max Characters:32				Carácter Max:32
9		ID_INPUTTIP3				32			Max Characters:15				Carácter Max:15
10		ID_NO_DATA				32			No Data				No hay datos
[tmp.shunts_data_content.html:Number]
11
[tmp.shunts_data_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_NA5						16			NA								NA
2		ID_REFERENCE2				32			Reference				Referencia
3		ID_SIGNAL_SHUNTNAME2				32			Shunt Name				Nombre Shunt
4		ID_MODIFY2				32			Modify						Modificar
5		ID_VIEW2				32			View					Ver
6		ID_MODIFY3				32			Modify						Modificar
7		ID_VIEW3				32			View					Ver
8		ID_BATTERYSHUNT2				32			Battery Shunt					Shunt Batería
9		ID_LOADSHUNT2				32			Load Shunt					Carga Shunt
10		ID_BATTERYSHUNT2				32			Battery Shunt					Shunt Batería

[tmp.system_source_smdu.html:Number]
1
[tmp.system_source_smdu.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Señal
2		ID_VALUE				32			Value				Valor
3		ID_SIGNAL1				32			Signal				Señal
4		ID_VALUE1				32			Value				Valor
5		ID_NO_DATA				32			No Data				No hay datos

[tmp.system_source.html:Number]
1
[tmp.system_source.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL				32			Signal				Señal
2		ID_VALUE				32			Value				Valor
3		ID_SIGNAL1				32			Signal				Señal
4		ID_VALUE1				32			Value				Valor
5		ID_NO_DATA				32			No Data				No hay datos

[tmp.setting_custominputs.html:Number]
1
[tmp.setting_custominputs.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_REFERENCE					32			Reference				Referencia
2		ID_VOLTAGE_INPUT				32			Voltage Input			Entrada de voltaje
3		ID_FUSE_ALARM_INPUT				32			Fuse Alarm Input		Entrada de alarma de fusible
4		ID_ANALOG_INPUT					32			Analog Input			Entrada analogica
5		ID_MODIFY						32			Modify					Modificar
6		ID_MODIFY2						32			Modify					Modificar
7		ID_ADVANCEDSETTING_ANALOGS		32			See Advanced Settings > Analogs				Ver Ajustes avanzados> Analógicos
8		ID_ADVANCEDSETTING_ANALOGS2		32			See Advanced Settings > Analogs				Ver Ajustes avanzados> Analógicos
9		ID_ADVANCEDSETTING_ANALOGS3		32			See Advanced Settings > Analogs				Ver Ajustes avanzados> Analógicos
10		ID_SETTINGS_FUSES				32			See Advanced Settings > Fuses				Ver Ajustes Ajustes> Fusibles
11		ID_ADVANCESSETTING_SHUNTS		64			See Advanced Settings > Shunts				Ver Configuraciones avanzadas> Derivaciones
12		ID_SETTINGS_TEMPERATURE			32			See Settings > Temp Probes					Ver Ajustes> Sondas tempe
13		ID_FUNCTION						32			Function								Función
14		ID_CURRENT_SETTING				32			Current Setting				Configuración actual
15		ID_NEW_SETTING					32			New Setting					Nueva configuración
16		ID_VOLTAGE_INPUT2				32			Voltage Input				Entrada de voltaje
17		ID_FUSE_ALARM_INPUT2				32			Fuse Alarm Input			Entrada de alarma de fusible
18		ID_ANALOG_INPUT2					32			Analog Input				Entrada analogica
19		ID_SET							32			Set							Conjunto



[tmp.setting_analog.html:Number]
1
[tmp.setting_analog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_REFERENCE					32			Reference				Referencia
2		ID_SIGNALFULLNAME				32			Signal Full Name			Nombre completo de la señal
3		ID_MODIFY						32			Modify					Modificar
4		ID_SIGNAL					32			Signal				Señal
5		ID_CURRENT_SETTING				32			Current Setting			Configuración actual
6		ID_NEW_CURRENT						32			New Current					Nueva corriente
7		ID_RANGE						32			Range					Distancia
8		ID_SIGNAL_FULL_NAME				32			Signal Full Name					Nombre de Señal Completa
9		ID_SIGNAL_ABBR_NAME				32			Signal Abbr Name					Nombre de señal Abbr
10		ID_LOW1_ALARM_LIMIT				32			Low 1 Alarm Limit					Límite de alarma bajo 1
11		ID_LOW1_ALARM_SEVERITY				32			Low 1 Alarm Severity					Baja 1 alarma de severidad
12		ID_LOW1_ALARM_RELAY				32			Low 1 Alarm Relay					Relé de alarma baja 1
13		ID_LOW2_ALARM_LIMIT				32			Low 2 Alarm Limit					Límite de alarma bajo 2
14		ID_LOW2_ALARM_SEVERITY				32			Low 2 Alarm Severity					Baja 2 alarma de severidad
15		ID_LOW2_ALARM_RELAY				32			Low 2 Alarm Relay					Relé de alarma baja 2
16		ID_HIGH1_ALARM_LIMIT				32			High 1 Alarm Limit					Límite de alarma alto 1
17		ID_HIGH1_ALARM_SEVERITY				32			High 1 Alarm Severity					High 1 Alarm Severity
18		ID_HIGH1_ALARM_RELAY				32			High 1 Alarm Relay					Relé de alarma alto 1
19		ID_HIGH2_ALARM_LIMIT				32			High 2 Alarm Limit					Límite de alarma alto 2
20		ID_HIGH2_ALARM_SEVERITY				32			High 2 Alarm Severity					High 2 Alarm Severity
21		ID_HIGH2_ALARM_RELAY				32			High 2 Alarm Relay					Relé de alarma alto 2
22		ID_SET							32			Set							Conjunto
23		ID_NA							32			NA							NA
24		ID_OA							32			OA							OA
25		ID_MA							32			MA							MA
26		ID_CA							32			CA							CA
27		ID_NA2							32			NA							NA
28		ID_OA2							32			OA							OA
29		ID_MA2							32			MA							MA
30		ID_CA2							32			CA							CA
31		ID_NA3							32			NA							NA
32		ID_OA3							32			OA							OA
33		ID_MA3							32			MA							MA
34		ID_CA3							32			CA							CA
35		ID_NA4							32			NA							NA
36		ID_OA4							32			OA							OA
37		ID_MA4							32			MA							MA
38		ID_CA4							32			CA							CA
39		ID_MODIFY2						32			Modify					Modificar
40		ID_INPUTTIP						32			Max Characters:25				Carácter Max:25
41		ID_INPUTTIP2						32			Max Characters:11				Carácter Max:11
42		ID_NA5							32			None								Ninguna
43		ID_NA6							32			None								Ninguna
44		ID_NA7							32			None								Ninguna
45		ID_NA8							32			None								Ninguna
46		ID_TIPS1							32			Please select					Favor Seleccione
47		ID_TIPS2							32			Please select					Favor Seleccione
48		ID_TIPS3							32			Please select					Favor Seleccione
49		ID_TIPS4							32			Please select					Favor Seleccione
50		ID_LOW							32			Low					bajo
51		ID_HIGH							32			High					Alto
52		ID_LOW2							32			Lo					ba
53		ID_HIGH2							32			Hi					Al
54		ID_SIGNAL_UNITS							32			Signal Units					Unidades de Señal
55		ID_INPUTTIP3							32			Max Characters:8					Carácter Max:8
56		ID_SIGNAL_X1							32			X1					X1
57		ID_SIGNAL_Y1							32			Y1(value at X1)					Y1 (valor en X1)
58		ID_SIGNAL_X2							32			X2					X2
59		ID_SIGNAL_Y2							32			Y2(value at X2)					Y2 (valor en X2)
60		ID_NO_DATA				64			No SMDUE inputs being used at this time				No se utilizan entradas SMDUE en este momento
61		ID_VOLTAGE			32			Voltage					Voltaje
62		ID_CURTRAN			32			Current Transducer					Transductor Corriente
63		ID_VOLTRAN				64			Voltage Transducer				Transductor Voltaje
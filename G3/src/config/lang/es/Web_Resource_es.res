﻿#
#  Web Private Configuration File, Version 1.00
#                All rights reserved.
# Copyright (c) 2012, Vertiv Tech Co., Ltd.
#
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
#Standard mandatory section

[LOCAL_LANGUAGE]
es

[LOCAL_LANGUAGE_VERSION]
1.00


#Define the web pages's resource file
#The field of Default Name is the english language
#The field of Local Name must be transfered  the local language according to the Default Name
#Define web pages number
[NUM_OF_PAGES]
83

#WebPages Resource
[control_cgi.htm:Number]
0

[control_cgi.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[data_sampler.htm:Number]
0

[data_sampler.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[dialog.htm:Number]
6

[dialog.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE		Description
1		ID_OK			32			N/A			OK			N/A		Aceptar			N/A			NA
2		ID_CANCEL		32			N/A			Cancel			N/A		Cancelar		N/A			NA
3		ID_SIGNAL_NAME		32			N/A		Signal Name			N/A		Nombre de la señal	N/A			NA
4		ID_SAMPLER		32			N/A			Sampler			N/A		Dechado		N/A			NA
5		ID_SAMPLE_CHANNEL	32			N/A			Sample Channel		N/A		Canal de Dechado		N/A			NA
6		ID_SIGNAL_NEW_NAME	32			N/A			New Name		N/A		Nuevo nombre		N/A			NA
	

[editsignalname.js:Number]
0

[editsignalname.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[equip_data.htm:Number]
12

[equip_data.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN										ABBR_IN_EN	FULL_IN_LOCALE											ABBR_IN_LOCALE
1		ID_TIPS0	32			N/A			Communication Failure								N/A		Fallo de comunicación con									N/A
2		ID_TIPS1	64			N/A			[OK]Stop.							N/A		[Aceptar]Detener						N/A
3		ID_TIPS2	64			N/A			Failed to communicate with the application.						N/A		Fallo de comunicación con la Aplicación								N/A
4		ID_TIPS3	128			N/A			Number of Rectifiers changed. The pages will refresh.				N/A		El número de rectificadores ha cambiado. Las páginas se refrescarán para mostrar el cambio	N/A
5		ID_TIPS4	128			N/A			System configuration changed. The pages will refresh.				N/A		La configuración del sistema ha cambiado. Las páginas se refrescarán!				N/A
6		ID_TIPS5	128			N/A			DC distribution number changed. The pages will refresh.			N/A		Ha cambiado el número de unidades de distribución. Las páginas se refrescarán!			N/A
7		ID_TIPS6	128			N/A			Number of Batteries changed. The pages will refresh.				N/A		El número de baterías ha cambiado. Las páginas se refrescarán!					N/A
8		ID_TIPS8	256			N/A			Automatic configuration in progress. Please wait a moment (about 1 minute).	N/A		En proceso de autoconfiguración. Por favor, Espere (1 minuto)!					N/A
9		ID_TIPS9	256			N/A			System configuration changed. The pages will refresh.				N/A		La configuración del sistema ha cambiado. Las páginas se refrescarán!				N/A
10		ID_TIPS10	256			N/A			Number of Converters changed. The pages will refresh.				N/A		Ha cambiado el número de Convertidores. Las páginas se refrescarán!				N/A
11		ID_TIPS11	256			N/A			Controller in Slave Mode.						N/A		Controladora en modo Secundario de Extensión!							N/A
12		ID_TIPS12	256			N/A			System configuration changed. The pages will refresh.			N/A		La configuración del sistema ha cambiado. Las páginas se refrescarán!				N/A



[j01_tree_maker.js:Number]
1

[j01_tree_maker.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_DIR		32			N/A			/cgi-bin/eng/				N/A		/cgi-bin/loc/		N/A

[j02_tree_view.js:Number]
36

[j02_tree_view.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE				ABBR_IN_LOCALE
1		ID_SCUP			16		N/A			System					N/A		Sistema					N/A
2		ID_DEVICE_EXPLORE	32		N/A			DEVICE INFORMATION				N/A		DEVICE INFORMATION				N/A
3		ID_SYSTEM		16		N/A			MAINTENANCE				N/A		MANTENIMIENTO				N/A
4		ID_NETWORK_SETTING	32		N/A			Network Configuration			N/A		Configuración de Red			N/A
5		ID_NMS_SETTING		32		N/A			NMSV2 Configuration			N/A		Configurar NMSV2				N/A
6		ID_ESR_SETTING		32		N/A			ESR Configuration			N/A		Configuración ESR			N/A
7		ID_USER			64		N/A			User Information Settings		N/A		La información de usuario				N/A
8		ID_MAINTENANCE		16		N/A			CONFIGURATION				N/A		CONFIGURACIÓN				N/A
9		ID_FILE_MANAGE		32		N/A			Download			N/A		Descarga			N/A
10		ID_MODIFY_CFG		64		N/A			Site Info Modification		N/A		Editar Información del sitio		N/A
11		ID_TIME_CFG		64		N/A			Time Sync				N/A		Fecha y Hora				N/A
12		ID_QUERY		16		N/A			QUERY					N/A		CONSULTA				N/A
13		ID_SITEMAP		16		N/A			Site Map				N/A		Mapa de Planta				N/A
14		ID_ALARM		32		N/A			ALARMS					N/A		ALARMAS					N/A
15		ID_ACTIVE_ALARM		32		N/A			Active Alarms					N/A		Activas Alarmas				N/A
16		ID_HISTORY_ALARM	32		N/A			Alarm History Log				N/A		Histórico Alarmas			N/A
17		ID_QUERY_HIS_DATA	32		N/A			Data History Log				N/A		Datos históricos			N/A
18		ID_QUERY_LOG_DATA	32		N/A			Control/System/Diesel Log				N/A		Control / Sistema / Diesel Entrar			N/A
19		ID_QUERY_BATT_DATA	32		N/A			Battery Test Log			N/A		Pruebas de Batería			N/A
20		ID_CLEAR_DATA		32		N/A			Clear Data				N/A		Borrar datos				N/A
21		ID_RESTORE_DEFAULT	32		N/A			Restore Factory Defaults			N/A		Restaurar Valores de Fábrica		N/A
22		ID_PRODUCT_INFO		32		N/A			System Inventory				N/A		Inventario de planta			N/A
23		ID_EDIT_CFGFILE		64		N/A			Configuration Info Modification		N/A		Modificar Información de Configuración	N/A
24		ID_YDN23_SETTING	32		N/A			HLMS Configuration			N/A		Configuración HLMS			N/A
25		ID_MODIFY_CFG1		64		N/A			Site Info Modification		N/A		Información de Planta			N/A
26		ID_MODIFY_CFG2		64		N/A			Equipment Info Modification	N/A		Información de Equipos			N/A
27		ID_MODIFY_CFG3		64		N/A			Signal Information Modification		N/A		Modificar Señales			N/A
28		ID_EDIT_CFGFILE1	64		N/A			Alarm Suppression	N/A		Supresión de Alarmas			N/A
29		ID_EDIT_CFGFILE2	64		N/A			Alarm Relays		N/A		Relés de Alarma				N/A
30		ID_EDIT_CFGFILE3	64		N/A			PLC			N/A		PLC			N/A
31		ID_USER_DEF_PAGES	64		N/A			QUICK SETTINGS				N/A		AJUSTE RÁPIDO				N/A
32		ID_EDIT_CFGGCPS		32		N/A			Edit PowerSplit					N/A		Editar Power Split				N/A
33		ID_GET_PARAM		32		N/A			Retrieve SettingParam.run			N/A		Salvar configuración			N/A
34		ID_AUTO_CONFIG		32		N/A			Auto Configuration			N/A		Autoconfiguración			N/A
35		ID_SYS_STATUS		32		N/A			SYSTEM STATUS				N/A		ESTADO DEL SISTEMA			N/A
36		ID_NMSV3_SETTING	32		N/A			NMSV3 Configuration			N/A		Configurar NMSV3			N/A



[j04_gfunc_def.js:Number]
0

[j04_gfunc_def.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j05_signal_def.js:Number]
0

[j05_signal_def.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j07_equiplist_def.js:Number]
0

[j07_equiplist_def.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j31_menu_script.js:Number]
0

[j31_menu_script.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j31_table_script.js:Number]
0


[j31_table_script.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j50_event_log.js:Number]
0

[j50_event_log.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j76_device_def.htm:Number]
0

[j76_device_def.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[j77_equiplist_def.htm:Number]
0

[j77_equiplist_def.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[login.htm:Number]
41

[login.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE				ABBR_IN_LOCALE
1		ID_USER_NAME		8			N/A			User				N/A		Usuario					N/A
2		ID_PASSWORD		16			N/A			Password			N/A		Contraseña				N/A
3		ID_CANCEL		16			N/A			Cancel				N/A		Cancelar				N/A
4		ID_OK			8			N/A			OK				N/A		Aceptar					N/A
5		ID_LOGIN		16			N/A			Login				N/A		Conectar				N/A
6		ID_TIPS0		64			N/A			Incorrect user name.	N/A		Nombre de usuario incorrecto.	N/A
7		ID_TIPS1		64			N/A			Name is too long.	N/A		El nombre es demasiado largo.	N/A
8		ID_TIPS2		64			N/A			Your browser is not supported.		N/A		Versión de Navegador no soportada	N/A
9		ID_TIPS3		64			N/A		The cookie was closed, you may not browse the pages normally.		N/A		Cookie no aceptada. Puede que las páginas no se muestren correctamente.			N/A
10		ID_TIPS4		128			N/A		The system supports Microsoft IE 5.5 or above. \nYour browser is not fully supported. Update browser please.		N/A	El Sistema soporta Microsoft IE 5.5 o superior.\nTu navegador no es plenamente soportado. Actualizar!		N/A
11		ID_ERROR0		64			N/A			Unknown error.			N/A		Error desconocido			N/A
12		ID_ERROR1		64			N/A			Successful.			N/A		Exito					N/A
13		ID_ERROR2		64			N/A			Incorrect password.	N/A		Contraseña incorrecta			N/A
14		ID_ERROR3		64			N/A			Incorrect user name.	N/A		Nombre de usuario incorrecto.			N/A
15		ID_ERROR4		64			N/A			Failed to communicate with the application.	N/A		Fallo de comunicación			N/A
16		ID_ERROR5		64			N/A			Over 5 connections, please retry later.	N/A	Más de 5 connexiones, espere!		N/A
17		ID_ENGLISH_VERSION	16			N/A			English				N/A		English					N/A
18		ID_LOCAL_VERSION	16			N/A			Español				N/A		Español					N/A
19		ID_SERIAL_NUMBER	20			N/A			Controller Serial Num		N/A		Num controlador de serie			N/A
20		ID_HARDWARE_VERSION	32			N/A			Hardware Version		N/A		Versión Hardware			N/A
21		ID_SOFTWARE_VERSION	32			N/A			Software Version		N/A		Versión Software			N/A
22		ID_PRODUCT_NUM		32			N/A			Product Model			N/A		Modelo					N/A
23		ID_PRO_NUMBER		32			N/A			Controller			N/A		Controladora				N/A
24		ID_CFG_VERSION		22			N/A			Configuration Version		N/A	Versión Configuración				N/A
25		ID_LOCAL2_VERSION	16			N/A			Local 2				N/A		Local 2					N/A
26		ID_ERROR6		128			N/A			Controller is starting. \nPlease wait.	N/A	La ACU está arrancando, \nEspere!	N/A
27		ID_OPT_RESOLUTION	32			N/A			Optimal Resolution		N/A		Resolución óptima			N/A
28		ID_ERROR7		256			N/A			Automatic configuration in progress. Please wait a moment (about 1-2 minutes).	N/A	En proceso de autoconfiguración. Por favor, Espere 1 minuto!	N/A
29		ID_ERROR8		64			N/A			Controller in Secondary Mode.						N/A		Controladora en modo Secundario de Extensión!			N/A
30		ID_LOGIN		64			N/A			Login				N/A		Conectar				N/A
31		ID_SITENAME		64			N/A			Site Name			N/A		Nombre Central				N/A
32		ID_SYSTEMNAME		64			N/A			System Name			N/A		Nombre de Sistema			N/A
33		ID_TEXT1		128			N/A			You are requesting access	N/A		Acceso a				N/A
34		ID_TEXT2		64			N/A			located at			N/A		localizado en				N/A
35		ID_TEXT3		256			N/A			The user name and password for this device is set by the system administrator		N/A		El Usuario y la contraseña para este dispositivo lo fija el administrador		N/A
36		ID_LOGIN		64			N/A			Login				N/A		Conectar				N/A
37		ID_CSSDIR		32			N/A			/cgi-bin/eng			N/A		/cgi-bin/loc				N/A
38		ID_CSSDIR2		32			N/A			/cgi-bin/eng			N/A		/cgi-bin/loc				N/A
39		ID_CSSDIR3		32			N/A			/cgi-bin/eng			N/A		/cgi-bin/loc				N/A
40		ID_LOGIN2		64			N/A			Login				N/A		Conectar				N/A
41		ID_CSSDIR4		32			N/A			/cgi-bin/eng			N/A		/cgi-bin/loc				N/A

[p01_main_frame.htm:Number]
2

[p01_main_frame.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_EXPLORE	32			16			Controller Web Browser	N/A			Explorador ACU		N/A
2		ID_SITE		16			16			Site			N/A			Central			N/A


[p02_tree_view.htm:Number]
15

[p02_tree_view.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN						ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_TIPS0		64			N/A			This page was locked, please log in.		N/A		Página bloqueada, vuelva a conectarse por favor		N/A
2		ID_LANGUAGE_VERSION	64			N/A			Local Web Pages Link					N/A		Enlace local páginas web				N/A
3		ID_SITE_MAP		64			N/A			Site Map						N/A		Mapa del sitio						N/A
4		ID_ALARM		128			N/A			Too many clicks. Please wait.	N/A		Por favor, espere a que la página requerida se cargue!	N/A
5		ID_SYS_STATUS		32			N/A			System Status						N/A		Estado del Sistema					N/A
6		ID_SERIAL_NUMBER	32			N/A			Serial Num						N/A		Número de Serie						N/A
7		ID_HARDWARE_VERSION	32			N/A			Hardware Version					N/A		Versión Hardware					N/A
8		ID_SOFTWARE_VERSION	32			N/A			Software Version					N/A		Versión Software					N/A
9		ID_PRODUCT_NUM		32			N/A			Product Model						N/A		Modelo							N/A
10		ID_CFG_VERSION		32			N/A			Configuration Version					N/A		Configuración						N/A
11		ID_SYSTEMNAME		64			N/A			System Name						N/A		Sistema							N/A
12		ID_RECTNAME		32			N/A			Rectifiers						N/A		Rectificadores						N/A
13		ID_RECTNAME2		32			N/A			Rectifiers						N/A		Rectificadores						N/A
14		ID_SYS_SPEC		64			N/A			System Specifications					N/A		Datos del Sistema					N/A
15		ID_CONTROL_SPEC		64			N/A			Controller Specifications				N/A		Datos Controladora					N/A


[p03_main_menu.htm:Number]
0

[p03_main_menu.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[p04_main_title.htm:Number]
3

[p04_main_title.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_SAMPLE	16			N/A			STATUS		N/A		Muestra			N/A
2		ID_CONTROL	16			N/A			CONTROL		N/A		Control			N/A
3		ID_SETTING	16			N/A			SETTINGS	N/A		Configuración		N/A



[p05_equip_sample.htm:Number]
8


[p05_equip_sample.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL		MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_INDEX		16				N/A			Index		N/A		Indice			N/A
2		ID_SIGNAL_NAME		16				N/A			Signal Name		N/A		Nombre de Señal		N/A
3		ID_SIGNAL_VALUE		16				N/A			Value		N/A		Valor			N/A
4		ID_SIGNAL_UNIT		16				N/A			Unit		N/A		Unidad			N/A
5		ID_SAMPLE_TIME		16				N/A			Sample Time		N/A		Tiempo muestra		N/A
6		ID_SAMPLER		16				N/A			Sampler		N/A		Dechado		N/A
7		ID_CHANNEL		16				N/A			Channel		N/A		Canal			N/A
8		ID_NO_SIG		64				N/A		There is no Sampler signal in this equipment.	N/A	No hay señales en este equipo!		N/A

[p06_equip_control.htm:Number]
24

[p06_equip_control.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_INDEX		16			N/A			Index				N/A		Indice						N/A
2		ID_SIGNAL_NAME		16			N/A		Signal Name				N/A		Nombre de Señal					N/A
3		ID_SIGNAL_VALUE		16			N/A			Value				N/A		Valor						N/A
4		ID_SIGNAL_UNIT		16			N/A			Unit				N/A		Unidad						N/A
5		ID_SAMPLE_TIME		16			N/A			Time				N/A		Modificado					N/A
6		ID_SET_VALUE		16			N/A			Set Value			N/A		Fijar valor					N/A
7		ID_SET			16			N/A			Set				N/A		Fijar						N/A
8		ID_ERROR0		32			N/A			Failed.			N/A		Fallo!						N/A
9		ID_ERROR1		32			N/A			Successful.			N/A		Con éxito!					N/A
10		ID_ERROR2		64			N/A			Failed. Conflicting setting.		N/A	Conflicto en configuración			N/A
11		ID_ERROR3		32			N/A			Failed. No authority.		N/A	Fallo. No tiene la autorización requerida!	N/A
12		ID_ERROR4		64			N/A			No information to send.		N/A		No hay información que enviar			N/A
13		ID_ERROR5		64			N/A			Failed. Controller is protected.	N/A	Fallo, Protección HW!			N/A
14		ID_SET_TYPE		16			N/A			Set				N/A		Fijar						N/A
15		ID_SHOW_TIPS0		64			N/A			Greater than the maximum value.	N/A		Supera valor máximo				N/A
16		ID_SHOW_TIPS1		64			N/A			Less than the minimum value.	N/A		Menor que el mínimo				N/A
17		ID_SHOW_TIPS2		64			N/A			Cannot be null.			N/A		No puede ser nulo				N/A
18		ID_SHOW_TIPS3		64			N/A			Input number please.		N/A		Entre número, por favor				N/A
19		ID_SHOW_TIPS4		64			N/A			Are you sure?			N/A		Está seguro?					N/A
20		ID_SHOW_TIPS5		64			N/A			Failed. No authority.	N/A	Fallo, no tiene la autoridad requerida!		N/A
21		ID_TIPS1		64			N/A			Send Control			N/A		Enviar control					N/A
22		ID_SAMPLER		16			N/A			Sampler					N/A		Dechado					N/A
23		ID_CHANNEL		16			N/A			Channel				N/A		Canal						N/A
24		ID_NO_SIG		64			N/A			There is no Control signal in this equipment.	N/A	Equipo sin señales de control		N/A

[p07_equip_setting.htm:Number]
29


[p07_equip_setting.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_INDEX	16				N/A			Index						N/A		Indice							N/A
2		ID_SIGNAL_NAME	16				N/A		Signal Name						N/A		Nombre de Señal						N/A
3		ID_SIGNAL_VALUE	16				N/A			Value						N/A		Valor							N/A
4		ID_SIGNAL_UNIT	16				N/A			Unit						N/A		Unidad							N/A
5		ID_SAMPLE_TIME	16				N/A			Time						N/A		Modificado						N/A
6		ID_SET_VALUE	16				N/A			Set Value					N/A		Fijar valor						N/A
7		ID_SET		16				N/A			Set						N/A		Fijar							N/A
8		ID_ERROR0	32				N/A			Failed.					N/A		Fallo!							N/A
9		ID_ERROR1	32				N/A			Successful.					N/A		Exito!							N/A
10		ID_ERROR2	64				N/A			Failed. Conflicting setting.			N/A		Fallo por conflicto en configuración			N/A
11		ID_ERROR3	32				N/A			Failed. No authority.		N/A		Fallo, no tiene el nivel de autorización requerido!	N/A
12		ID_ERROR4	64				N/A			No information to send.				N/A		No hay información que enviar				N/A
13		ID_ERROR5	128				N/A			Failed. Controller is hardware protected.	N/A		Fallo, ACU protegida por HW				N/A
14		ID_SET_TYPE	16				N/A			Set						N/A		Fijar							N/A
15		ID_SHOW_TIPS0	64				N/A			Greater than the maximum value:			N/A		Mayor que el máximo:					N/A
16		ID_SHOW_TIPS1	64				N/A			Less than the minimum value:			N/A		Menor que el mínimo:					N/A
17		ID_SHOW_TIPS2	64				N/A			Cannot be null.						N/A		No puede ser nulo					N/A
18		ID_SHOW_TIPS3	64				N/A			Input number please.				N/A		Entre número por favor					N/A
19		ID_SHOW_TIPS4	64				N/A			The control value is equal to the last value.	N/A		El valor de control es igual al último			N/A
20		ID_SHOW_TIPS5	64				N/A			Failed. No authority.		N/A		Fallo, no tiene el nivel de autorización requerido!	N/A
21		ID_TIPS1	64				N/A			Setting						N/A		Resultado:						N/A
22		ID_SAMPLER	16				N/A			Sampler						N/A		Dechado						N/A
23		ID_CHANNEL	16				N/A			Channel						N/A		Canal							N/A
24		ID_MONTH_ERROR	32				N/A			Incorrect month.				N/A		Mes incorrecto!						N/A
25		ID_DAY_ERROR	32				N/A			Incorrect day.					N/A		Día incorrecto!						N/A
26		ID_HOUR_ERROR	32				N/A			Incorrect hour.					N/A		Hora incorrecta!					N/A
27		ID_FORMAT_ERROR	64				N/A			Incorrect format.		N/A		Formato incorrecto, debe seguir				N/A
28		ID_RELOAD_PAGE	64				N/A			./eng/p07_equip_setting.htm			N/A		./loc/p07_equip_setting.htm				N/A
29		ID_NO_SIG	64				N/A			There is no Setting signal in this equipment.	N/A		Este equipo no tiene señales de configuración.		N/A
#30		ID_ADJUST_CONFIRM	64			N/A			Confirm the voltage has been supplied to the bus.	N/A	Confirmar que se ha suministrado tensión al bus.				N/A

[p08_alarm_frame.htm:Number]
1

[p08_alarm_frame.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_DIR		32			N/A			/cgi-bin/eng/				N/A		/cgi-bin/loc/		N/A

[p09_alarm_title.htm:Number]
9

[p09_alarm_title.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_ALL_ALARM		16			N/A			All Alarms	N/A		Todas			N/A
2		ID_OBSERVATION_ALARM	16			N/A			Observation	N/A		O1			N/A
3		ID_MAJOR_ALARM		16			N/A			Major		N/A		A2			N/A
4		ID_CRITICAL_ALARM	16			N/A			Critical	N/A		A1			N/A
5		ID_AUTO			16			N/A			Auto Popup	N/A		Autoemergente		N/A
6		ID_ALL_ALARM1		16			N/A			All Alarms	N/A		Todas			N/A
7		ID_OBSERVATION_ALARM1	16			N/A			Observation	N/A		O1			N/A
8		ID_MAJOR_ALARM1		16			N/A			Major		N/A		A2			N/A
9		ID_CRITICAL_ALARM1	16			N/A			Critical	N/A		A1			N/A

[p10_alarm_show.htm:Number]
8

[p10_alarm_show.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_INDEX		16			N/A			Index				N/A		Indice			N/A
2		ID_SIGNAL_NAME		16			N/A			Signal Name				N/A		Nombre de Señal		N/A
3		ID_ALARM_LEVEL		16			N/A			Alarm Level			N/A		Nivel de alarma		N/A
4		ID_SAMPLE_TIME		16			N/A			Sample Time				N/A		Tiempo muestra		N/A
5		ID_RELATIVE_DEVICE	16			N/A			Relative Device				N/A		Dispositivo Relativa			N/A
6		ID_OA			16			N/A			OA				N/A		O1			N/A
7		ID_MA			16			N/A			MA				N/A		A2			N/A
8		ID_CA			16			N/A			CA				N/A		A1			N/A

[p11_network_config.htm:Number]
21

#In ID_TIPS1,ID_TIPS2, ID_TIPS4, the '\n' and 'nnn.nnn.nnn.nnn','10.75.14.171' is about the format of show pages, so that you don't need to tranfer it.
[p11_network_config.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN											ABBR_IN_EN	FULL_IN_LOCALE										ABBR_IN_LOCALE
1		ID_SCUP_IP		32			N/A			Controller Network IP										N/A		Dirección IP										N/A
2		ID_SCUP_MASK		8			N/A			Subnet Mask												N/A		Máscara											N/A
3		ID_SCUP_GATEWAY		8			N/A			Gateway												N/A		Gateway											N/A
4		ID_SAVE_PARAMETER	16			N/A			Save Parameter											N/A		Aceptar											N/A
5		ID_SCUP_NETWORK_HEAD	32			N/A		Controller Network Parameter Set								N/A		Parámetros de Red									N/A
6		ID_ERROR0		32			N/A			Unknown error.											N/A		Error desconocido									N/A
7		ID_ERROR1		32			N/A			Successful. Controller is rebooting.								N/A		Exito! ACU está reiniciando								N/A
8		ID_ERROR2		32			N/A			Failed. Incorrect input.									N/A		Entrada incorrecta									N/A
9		ID_ERROR3		32			N/A			Failed. Incomplete information.									N/A		Fallo: Información incompleta								N/A
10		ID_ERROR4		32			N/A			Failed. No authority.								N/A		Fallo: Nivel autorización!								N/A
11		ID_ERROR5		32			N/A			Failed. Controller is hardware protected.							N/A		Fallo: ACU protegida por HW								N/A
12		ID_TIPS0		32			N/A			Set Network Parameter										N/A		Fijar parámetro de red									N/A
13		ID_TIPS1		128			N/A			Units IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171.			N/A		Error entrada IP!\n Formato 'nnn.nnn.nnn.nnn', e.g. 10.75.14.171			N/A
14		ID_TIPS2		128			N/A			Mask IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 255.255.0.0.				N/A		Máscara incorrecta\nFormato:'nnn.nnn.nnn.nnn', e.g. 255.255.0.0				N/A
15		ID_TIPS3		32			N/A			Units IP Address and Mask mismatch.										N/A		IP y máscara incorrecta									N/A
16		ID_TIPS4		128			N/A			Gateway IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171. Enter 0.0.0.0 for no gateway.	N/A		Gateway incorrecto¡\nFormato: 'nnn.nnn.nnn.nnn' e.g. 10.75.14.254, 0.0.0.0 sin gateway	N/A
17		ID_TIPS5		64			N/A			Units IP Address, Gateway, Mask mismatch. Enter Address again.							N/A		Discordancia IP Gateway. Entre otra vez							N/A
18		ID_TIPS6		64			N/A			Please wait. Controller is rebooting.								N/A		Reiniciando, espere...									N/A
19		ID_TIPS7		64			N/A			Parameters have been modified.  Controller is rebooting...				N/A		Reiniciando para aplicar los cambios							N/A
20		ID_TIPS8		64			N/A			Controller homepage will be refreshed.						N/A		La página de inicio será refrescada más tarde						N/A
21		ID_ERROR6		32			N/A			Failed. DHCP is ON.										N/A		Fallo: Servicio DHCP activo.								N/A


[p12_nms_config.htm:Number]
40

[p12_nms_config.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL		MAX_LEN_OF_BYTE_ABBR		FULL_IN_EN										ABBR_IN_EN		FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_ERROR0		16				N/A				Unknown error.										N/A			Error desconocido!				N/A
2		ID_ERROR1		64				N/A				Failed. The NMS already exists.								N/A			Error. El servidor NMS ya existe		N/A
3		ID_ERROR2		32				N/A				Successful										N/A			Exito						N/A
4		ID_ERROR3		64				N/A				Failed. Incomplete information.							N/A			Fallo, información incompleta!			N/A
5		ID_ERROR4		64				N/A				Failed. No authority.							N/A			Fallo. No tiene la autorización requerida	N/A
6		ID_ERROR5		64				N/A				Cannot be modified. Controller is hardware protected.					N/A			ACU protegida por HW. No se puede modificar!	N/A
7		ID_ERROR6		64				N/A				Failed. Maximum number exceeded.						N/A			Fallo: Máximo excedido				N/A
8		ID_NMS_HEAD1		32				N/A				NMS Configuration									N/A			Configuración NMS				N/A
9		ID_NMS_HEAD2		32				N/A				Current NMS										N/A			NMS actual					N/A
10		ID_NMS_IP		16				N/A				NMS IP											N/A			IP NMS						N/A
11		ID_NMS_AUTHORITY	16				N/A				Authority										N/A			Autoridad					N/A
12		ID_NMS_TRAP		32				N/A				Accepted Trap Level									N/A			Aceptar nivel Trap				N/A
13		ID_NMS_IP		16				N/A				NMS IP											N/A			IP NMS						N/A
14		ID_NMS_AUTHORITY	16				N/A				Authority										N/A			Autoridad					N/A
15		ID_NMS_TRAP		32				N/A				Accepted Trap Level									N/A			Aceptar nivel Trap				N/A
16		ID_NMS_ADD		16				N/A				Add New NMS										N/A			Añadir nuevo NMS				N/A
17		ID_NMS_MODIFY		32				N/A				Modify NMS										N/A			Modificar NMS					N/A
18		ID_NMS_DELETE		32				N/A				Delete NMS										N/A			Borrar NMS					N/A
19		ID_NMS_PUBLIC		32				N/A				Public Community									N/A			Comunidad pública				N/A
20		ID_NMS_PRIVATE		32				N/A				Private Community									N/A			Comunidad privada				N/A
21		ID_NMS_LEVEL0		16				N/A				Not Used										N/A			No utilizado					N/A
22		ID_NMS_LEVEL1		16				N/A				No Access										N/A			Sin acceso					N/A
23		ID_NMS_LEVEL2		32				N/A				Query Authority										N/A			Autoridad de consulta				N/A
24		ID_NMS_LEVEL3		32				N/A				Control Authority									N/A			Autoridad de Control				N/A
25		ID_NMS_LEVEL4		32				N/A				Administrator										N/A			Administrador					N/A
26		ID_NMS_TRAP_LEVEL0	16				N/A				Not Used										N/A			No utilizado					N/A
27		ID_NMS_TRAP_LEVEL1	16				N/A				All Alarms										N/A			Todas						N/A
28		ID_NMS_TRAP_LEVEL2	16				N/A				Major Alarms										N/A			A2						N/A
29		ID_NMS_TRAP_LEVEL3	16				N/A				Critical Alarms										N/A			A1						N/A
30		ID_NMS_TRAP_LEVEL4	16				N/A				No Trap										N/A			No hay Traps					N/A
31		ID_TIPS0		128				N/A				Incorrect IP address of NMS. \nShould be in format 'nnn.nnn.nnn.nnn'. \nExample 10.76.8.29	N/A			Dirección IP de NMS incorrecta.\nFormato 'nnn.nnn.nnn.nnn'\ne.g.: 10.76.8.29	N/A
32		ID_TIPS1		128				N/A				Public Community and Private Community cannot be empty. Please try again.		N/A			La comunidad pública y privada no pueden estar vacías. Inténtelo de nuevo.	N/A
33		ID_TIPS2		128				N/A				Already exists. Please try again.							N/A			Ya existe, pruebe otra vez.							N/A
34		ID_TIPS3		128				N/A				Does not exist. Password cannot be modified. Please try again.		N/A			No existe, así que la contraseña no puede ser modificada. Pruebe otra vez.	N/A
35		ID_TIPS4		128				N/A				Please select one or more NMS before clicking this button.				N/A			Por favor, seleccione uno o más NMS antes de pulsar				N/A
36		ID_TIPS5		128				N/A				NMS Info Configuration									N/A			Configuración información NMS							N/A
37		ID_NMS_PUBLIC		128				N/A				Public Community									N/A			Comunidad Pública								N/A
38		ID_NMS_PRIVATE		128				N/A				Private Community									N/A			Comunidad Privada								N/A
39		ID_TRAP_HEAD		128				N/A				Change NMS trap alarm level (all current NMS traps will be changed).			N/A			Cambio Trap de NMS (Todas las Traps de NMS cambiarán)				N/A
40		ID_SET_TRAP		128				N/A				Change Trap										N/A			Cambiar Trap									N/A


[p13_esr_config.htm:Number]
93

[p13_esr_config.htm]
#Sequence ID	RES_ID				MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN						ABBR_IN_EN		FULL_IN_LOCALE							ABBR_IN_LOCALE
1		ID_ERROR0			64			N/A			Successful.						N/A			Exito!								N/A
2		ID_ERROR1			64			N/A			Failed.						N/A			Fallo!								N/A
3		ID_ERROR2			64			N/A			Failed. ESR Service was exited.			N/A			Fallo, Servicio ESR detenido					N/A
4		ID_ERROR3			64			N/A			Failed. Invalid parameter.				N/A			Fallo, parámetro inválido!					N/A
5		ID_ERROR4			64			N/A			Failed. Invalid data.				N/A			Error, datos no válidos						N/A
6		ID_ERROR5			64			N/A			Cannot be modified. Controller is hardware protected.	N/A			ACU protegida por HW. No se puede modificar!			N/A
7		ID_ERROR6			64			N/A			Service is busy. Cannot change configuration at this time.		N/A			Servicio ocupado. Ahora no se puede cambiar la configuración	N/A
8		ID_ERROR7			64			N/A			Non-shared port already occupied.		N/A			El puerto no compartido ya está ocupado				N/A
9		ID_ERROR8			64			N/A			Failed. No authority.			N/A			Error! No tiene el nivel de autorización requerido		N/A
10		ID_ESR_HEAD			32			N/A			HLMS Configuration					N/A			Configuración comunicaciones MC					N/A
11		ID_PROTOCOL_TYPE		32			N/A			Protocol Type						N/A			Protocolo							N/A
12		ID_PROTOCOL_MEDIA		32			N/A			Protocol Media						N/A			Medio comunicación						N/A
13		ID_CALLBACK_IN_USE		32			N/A			Callback Enabled					N/A			Retrollamada en uso						N/A
14		ID_REPORT_IN_USER		32			N/A			Report Enabled						N/A			Informe alarmas en uso						N/A
15		ID_MAX_ALARM_REPORT		32			N/A			Maximum alarm report attempts.				N/A			Número Intentos Informe Alarmas					N/A
16		ID_RANGE_FROM			32			N/A			Range							N/A			Rango desde							N/A
17		ID_CALL_ELAPSE_TIME		32			N/A			Call Elapse Time					N/A			Tiempo entre llamadas						N/A
18		ID_RANGE_FROM			32			N/A			Range							N/A			Rango desde							N/A
19		ID_MAIN_REPORT_PHONE		32			N/A			Main Report Phone Number				N/A			Teléfono Informe de Alarmas 1					N/A
20		ID_SECOND_REPORT_PHONE		32			N/A			Second Report Phone Number				N/A			Teléfono Informe de Alarmas 2					N/A
21		ID_CALLBACK_PHONE		32			N/A			Callback Phone Number					N/A			Teléfono de Retrollamada					N/A
22		ID_REPORT_IP			32			N/A			Main Report IP					N/A			IP Informe Alarmas 1						N/A
23		ID_SECOND_IP			32			N/A			Second Report IP				N/A			IP Informe Alarmas 2						N/A
24		ID_SECURITY_IP			32			N/A			Security Connection IP					N/A			IP conexión de seguridad					N/A
25		ID_SECURITY_IP			32			N/A			Security Connection IP					N/A			IP conexión de seguridad					N/A
26		ID_SAFETY_LEVEL			32			N/A			Safety Level						N/A			Nivel de Seguridad						N/A
27		ID_MODIFY			32			N/A			Modify							N/A			Modificar							N/A
28		ID_CCID				16			N/A			CCID							N/A			CCID								N/A
29		ID_SOCID			16			N/A			SOCID							N/A			SOCID								N/A
30		ID_PROTOCOL0			16			N/A			EEM							N/A			EEM								N/A
31		ID_PROTOCOL1			16			N/A			RSOC							N/A			RSOC								N/A
32		ID_PROTOCOL2			16			N/A			SOC/TPE								N/A			SOCTPE								N/A
33		ID_MEDIA0			16			N/A			RS-232							N/A			RS-232							N/A
34		ID_MEDIA1			16			N/A			Modem							N/A			Modem								N/A
35		ID_MEDIA2			16			N/A			Ethernet							N/A			Ethernet							N/A
36		ID_USE0				16			N/A			Not Used						N/A			No utilizar							N/A
37		ID_USE1				16			N/A			Used							N/A			Utilizar							N/A
38		ID_SECURITY0			128			N/A			All commands are available.			N/A			Todos los comandos disponibles					N/A
39		ID_SECURITY1			128			N/A			Only read commands are available.			N/A			Leer sólo comandos disponibles					N/A
40		ID_SECURITY2			128			N/A			Only the Call Back ('CB') command is available.			N/A			Sólo el comando Retrollamda ('CB') está disponible		N/A
41		ID_TIPS1			128			N/A			Units IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171	N/A	Error entrada IP¡\nFormato 'nnn.nnn.nnn.nnn' e.g. 10.75.14.171	N/A
42		ID_TIPS2			128			N/A			Mask IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 255.255.0.0	N/A	Error Máscara IP¡\nFormato 'nnn.nnn.nnn.nnn' e.g. 255.255.0.0	N/A
43		ID_TIPS3			32			N/A			Units IP Address and Mask mismatch.					N/A			IP y Máscara incorrecta						N/A
44		ID_TIPS4			128			N/A			Gateway IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.254  Enter 0.0.0.0 for no gateway.	N/A	GateWay incorrecto¡\nFormato'nnn.nnn.nnn.nnn' e.g. 10.75.14.254, 0.0.0.0 sin GateWay	N/A
45		ID_TIPS5			128			N/A			Units IP Address, Gateway, Mask mismatch. Enter Address again.		N/A			Discrepancia IP Gateway. Pruebe otra vez			N/A
46		ID_TIPS6			128			N/A			CCID error. Input number please.		N/A			CCID errónea. Entre un número, por favor!			N/A
47		ID_TIPS7			128			N/A			SOCID error. Input number please.		N/A			SOCID incorrecta. Entre número por favor!			N/A
48		ID_TIPS8			128			N/A			Maximum alarm report attempt is input error.		N/A			Num intentos Informe Alarmas incorrecto.			N/A
49		ID_TIPS9			128			N/A			Maximum call elapse time is input error.			N/A			Máximo tiempo de llamada erróneo.				N/A
50		ID_TIPS10			128			N/A			Main report phone number is input error.			N/A			Error Teléfono Informe de Alarmas 1.				N/A
51		ID_TIPS11			128			N/A			Second report phone number is input error.		N/A			Error Teléfono Informe de Alarmas 2.				N/A
52		ID_TIPS12			128			N/A			Callback report phone number is input error.		N/A			Error Teléfono de Retrollamada					N/A
53		ID_TIPS13			128			N/A			Report IP is input error.				N/A			Error IP informe de Alarmas					N/A
54		ID_TIPS14			128			N/A			Report IP is input error.				N/A			Error IP informe de Alarmas					N/A
55		ID_TIPS15			128			N/A			Security IP is input error.				N/A			Error entrada IP de seguridad					N/A
56		ID_TIPS16			128			N/A			Security IP is input error.				N/A			Error entrada IP de seguridad					N/A
57		ID_TIPS17			128			N/A			ESR Configure				N/A			Configuración MC						N/A
58		ID_TIPS18			128			N/A			Cannot be 0.						N/A			No puede ser cero.						N/A
59		ID_TIPS19			128			N/A			Input error.						N/A			Error de entrada						N/A
60		ID_COMMON_PARAM			64			N/A			Protocol media common configuration.			N/A			Configuración medio comunicación				N/A
61		ID_MEDIA_PARAM0			64			N/A			Port Parameter					N/A			Parámetros del puerto serie					N/A
62		ID_MEDIA_PARAM1			64			N/A			Serial Port Parameters & Phone Number			N/A			Parámetros puerto serie y número de Teléfono			N/A
63		ID_MEDIA_PARAM2			32			N/A			TCP/IP Port Number					N/A			Número de puerto TCP/IP						N/A
64		ID_RANGE_FROM1			64			N/A			Range 0-255						N/A			Rango desde 0-255						N/A
65		ID_RANGE_FROM2			64			N/A			Range 0-600						N/A			Rango desde 0-600						N/A
66		ID_TIPS20			64			N/A			Input Error						N/A			Error de entrada						N/A
67		ID_TIPS21			64			N/A			Maximum alarm report attempt is input error.			N/A			Error entrada Max num de intentos Informe de Alarmas		N/A
68		ID_TIPS22			64			N/A			Maximum call elapse time is input error.			N/A			Error entrada Max num de intentos Informe de Alarmas		N/A
69		ID_TIPS23			64			N/A			Input Error					N/A			Error de entrada						N/A
70		ID_TIPS24			64			N/A			Port input error.					N/A			Error entrada Puerto						N/A
71		ID_NO_PROTOCOL_TIPS		128			N/A			Please input protocol.					N/A			Por favor, seleccione el protocolo!				N/A
72		ID_YDN23_RANGE_FROM1		64			N/A			Range 0-5						N/A			Rango desde 0-5							N/A
73		ID_YDN23_RANGE_FROM2		64			N/A			Range 0-300						N/A			Rango desde 0-300						N/A
74		ID_YDN23_1ST_REPORT_PHONE	32			N/A			First Report Phone Number			N/A			Teléfono Informe de Alarmas 1					N/A
75		ID_YDN23_2ND_REPORT_PHONE	32			N/A			Second Report Phone Number				N/A			Teléfono Informe de Alarmas 2					N/A
76		ID_YDN23_3RD_REPORT_PHONE	32			N/A			Third Report Phone Number				N/A			Teléfono Informe de Alarmas 3					N/A
77		ID_YDN23_REPORT_IN_USER		32			N/A			Alarm Reporting						N/A			Informe alarmas en uso						N/A
78		ID_YDN23_SELF_ADDRESS		16			N/A			Self Address					N/A			Dirección propia						N/A
79		ID_YDN23_MAX_ALARM_REPORT	32			N/A			Times of Dialing Attempt				N/A			Num max intentos Informe Alarmas				N/A
80		ID_YDN23_CALL_ELAPSE_TIME	32			N/A			Interval between Two Dialings						N/A			Tiempo de llamada transcurrido					N/A
81		ID_PROTOCOL3			16			N/A			YDN23							N/A			YDN23								N/A
82		ID_TIPS8			128			N/A			Maximum alarm report attempt is input error.		N/A			Num intentos Informe Alarmas incorrecto.			N/A
83		ID_TIPS9			128			N/A			Maximum call elapse time is input error.			N/A			Máximo tiempo de llamada erróneo.				N/A
84		ID_TIPS21			64			N/A			Maximum alarm report attempt is input error.		N/A			Num intentos Informe Alarmas incorrecto.			N/A
85		ID_TIPS22			64			N/A			Maximum call elapse time is input error.		N/A			Num intentos Informe Alarmas incorrecto.			N/A
86		ID_TIPS23			64			N/A			Input Error						N/A			Error de entrada						N/A
87		ID_RANGE_FROM1			64			N/A			Range 0-255						N/A			Rango desde 0-255						N/A
88		ID_RANGE_FROM2			64			N/A			Range 0-600						N/A			Rango desde 0-600						N/A
89		ID_MAIN_REPORT_PHONE		32			N/A			Main Report Phone Number			N/A			Teléfono Informe de Alarmas 1					N/A
90		ID_SECOND_REPORT_PHONE		32			N/A			Second Report Phone Number				N/A			Teléfono Informe de Alarmas 2					N/A
91		ID_CALLBACK_PHONE		32			N/A			Callback Phone Number					N/A			Teléfono de Retrollamada					N/A
92		ID_MAX_ALARM_REPORT		32			N/A			Maximum Alarm Report Attempts			N/A			Num max intentos Informe Alarmas				N/A
93		ID_CALL_ELAPSE_TIME		32			N/A			Call Elapse Time					N/A			Tiempo de llamada transcurrido					N/A

[p14_user_config.htm:Number]
38

[p14_user_config.htm]
#Sequence ID	RES_ID				MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN									ABBR_IN_EN	FULL_IN_LOCALE								ABBR_IN_LOCALE
1		ID_WEB_HEAD			32			N/A			WEB User Management							N/A		Gestión Usuarios Web							N/A
2		ID_CURRENT_USER			32			N/A			Current Users									N/A		Usuarios actuales							N/A
3		ID_USER_NAME_INTABLE		32			N/A			User Name									N/A		Nombre de usuario							N/A
4		ID_USER_AUTHORITY_INTABLE	32			N/A			Authority									N/A		Autoridad								N/A
5		ID_PASSWORD			32			N/A			Password									N/A		Contraseña								N/A
6		ID_USER_NAME			32			N/A			User Name									N/A		Nombre de usuario							N/A
7		ID_USER_AUTHORITY		32			N/A			Authority									N/A		Autoridad								N/A
8		ID_CONFIRM			32			N/A			Confirm										N/A		Confirmar								N/A
9		ID_USER_ADD			32			N/A			Add New User									N/A		Añadir nuevo usuario							N/A
10		ID_USER_MODIFY			32			N/A			Modify User									N/A		Modificar usuario							N/A
11		ID_USER_DELETE			32			N/A			Delete Selected User								N/A		Borrar usuario seleccionado						N/A
12		ID_ERROR0			32			N/A			Unknown error.									N/A		Error desconocido							N/A
13		ID_ERROR1			32			N/A			Successful.									N/A		Exito									N/A
14		ID_ERROR2			64			N/A			Failed. Incomplete information.							N/A		Error! Información incompleta!						N/A
15		ID_ERROR3			64			N/A			Failed. The User Name already exists.						N/A		Error: El nombre de usuario ya existe					N/A
16		ID_ERROR4			64			N/A			Failed. No authority.						N/A		Fallo: No tiene el nivel de autorización requerido!			N/A
17		ID_ERROR5			64			N/A			Cannot be modified. Controller is hardware protected.				N/A		ACU protegida por HW. No se puede modificar!				N/A
18		ID_ERROR6			64			N/A			Failed. You can only change your password.					N/A		Error: Sólo puede modificar su propia contraseña			N/A
19		ID_ERROR7			64			N/A			Failed. Deleting 'admin' is not allowed.						N/A		Error: No está permitido borrar admin					N/A
20		ID_ERROR8			64			N/A			Failed. Deleting a logged in user is not allowed.					N/A		Fallo, no se puede eliminar el usuario conectado!			N/A
21		ID_ERROR9			128			N/A			The User already exists. Please try again.					N/A		El usuario ya existe! Intente de nuevo					N/A
22		ID_ERROR10			128			N/A			Failed. Too many users.							N/A		Fallo, demasiados usuarios						N/A
23		ID_ERROR11			128			N/A			Failed. User does not exist.							N/A		Fallo, El usuario no existe!						N/A
24		ID_TIPS1			32			N/A			Please enter a User Name.							N/A		Por favor, entre un nombre de usuario!					N/A
25		ID_TIPS2			128			N/A			The User Name cannot be started or ended with a space.					N/A		El nombre de usuario no puede empezar o terminar con espacio en blanco!	N/A
26		ID_TIPS3			128			N/A			Passwords do not match. Please try again.	N/A		Las entradas contraseña y confirmar deben coincidir!			N/A
27		ID_TIPS4			128			N/A			Please remember the password entered.							N/A		Por favor, no olvide la contraseña!					N/A
28		ID_TIPS5			128			N/A			A password must be entered.							N/A		Contraseña requerida!							N/A
29		ID_TIPS6			128			N/A			Please remember the password entered.							N/A		Por favor, recuerde su contraseña!					N/A
30		ID_TIPS7			128			N/A			Already exists. Please try again.						N/A		ya existe. Pruebe otra vez!						N/A
31		ID_TIPS8			128			N/A			Does not exist. The password cannot be modified. Please try again.		N/A		no existe. La contraseña no puede modificarse! Pruebe otra vez		N/A
32		ID_TIPS9			128			N/A			Please select one or more Users before clicking this button.			N/A		Por favor, seleccione uno o más usuarios haciendo click en este botón!	N/A
33		ID_AUTHORITY_LEVEL0		32			N/A			Browser										N/A		Navegador								N/A
34		ID_AUTHORITY_LEVEL1		32			N/A			Operator									N/A		Operador								N/A
35		ID_AUTHORITY_LEVEL2		32			N/A			Engineer									N/A		Ingeniero								N/A
36		ID_AUTHORITY_LEVEL3		32			N/A			Administrator									N/A		Administrador								N/A
37		ID_INVALIDATE_CHAR		64			N/A			Comprised of invalid character in input.						N/A		Entrada con caracter no válido!						N/A
38		ID_TIPS10			128			N/A			The follow characters must not be included in User Name, please try again.	N/A		Los caracteres siguientes no pueden utilizarse en el nombre de usuario	N/A

[p15_show_acutime.htm:Number]
0

[p15_show_acutime.htm.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[p16_filemanage_title.htm:Number]
8

[p16_filemanage_title.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE			ABBR_IN_LOCALE
1		ID_ERROR0	32			N/A			Unknown error.				N/A		Error desconocido!		N/A
2		ID_ERROR1	32			N/A			Successful				N/A		Exito!				N/A
3		ID_ERROR2	32			N/A			Failed. Not enough space.		N/A		Fallo, espacio insuficiente	N/A
4		ID_ERROR3	32			N/A			Failed. No authority.	N/A		Fallo! No tiene autorización	N/A
5		ID_ERROR4	32			N/A			Successful. Stop Controller.		N/A		Parar ACU			N/A
6		ID_ERROR5	32			N/A			Successfull. Start Controller.		N/A		Iniciar ACU			N/A
7		ID_UPLOAD	32			N/A			Upload					N/A		Upload				N/A
8		ID_DOWNLOAD	32			N/A			Download				N/A		Download			N/A
9		ID_TIPS0	128			N/A			Do you want to quit the download pages? Click Yes to start Controller. Click No to cancel this operation.	N/A	¿Cancelar descarga? Seleccione Sí para iniciar la ACU y No para cancelar esta operación		N/A

[p17_login_overtime.htm:Number]
5

[p17_login_overtime.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_LOGIN_HEAD		32			N/A			Login Time Expired		N/A			Fuera de tiempo			N/A
2		ID_OK			32			N/A			Login Again	N/A			Conectar otra vez		N/A
3		ID_CANCEL		32			N/A			OK		N/A			Acuerdo		N/A
4		ID_EXPLORE_INFO		256			N/A			Your login time expired. You can only view data.<br>Click 'OK' to continue viewing data.	N/A		Se ha conectado fuera de tiempo y solo puede explorar.<br>Presione Conectar para conectarse otra vez<br>Presione Cancelar para continuar explorando.<br>		N/A
5		ID_TIPS0		64			N/A			You have connected to the Controller. This window will close.		N/A				Se ha conectado a la ACU. Esta ventana se cerrará!		N/A

[p18_restore_default.htm:Number]
8

[p18_restore_default.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN									ABBR_IN_EN	FULL_IN_LOCALE									ABBR_IN_LOCALE
1		ID_HEAD			128			N/A			Restore Factory Defaults							N/A		Restaurar valores por defecto							N/A
2		ID_TIPS0		128			N/A			Restore default configuration? The system will reboot.			N/A		Restaurar configuración por defecto. El sistema se reiniciará			N/A
3		ID_RESTORE_DEFAULT	64			N/A		Restore Defaults							N/A		Restaurar valores								N/A
4		ID_TIPS1		128			N/A			Restore default config will cause system to reboot, are you sure?	N/A		Restaurar config por defecto implica reiniciar el sistema. Está seguro?		N/A
5		ID_TIPS2		128			N/A			Cannot be restored. Controller is hardware protected.				N/A		ACU protegida por HW. No se puede restaurar					N/A
6		ID_TIPS3		128			N/A			Are you sure you want to reboot the Controller?						N/A		Está seguro que desea reiniciar?						N/A
7		ID_TIPS4		128			N/A			Failed. No authority.						N/A		Fallo, no tiene el nivel de autorización requerido!				N/A
8		ID_START_SCUP		32			N/A			Reboot Controller								N/A		Reiniciar ACU									N/A

[p19_time_config.htm:Number]
28

[p19_time_config.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN																ABBR_IN_EN	FULL_IN_LOCALE							ABBR_IN_LOCALE
1		ID_TIME_HEAD		32			N/A			Time Setting																N/A		Configurar Hora							N/A
2		ID_TIME_HEAD		32			N/A			Time Setting																N/A		Configurar Hora							N/A
3		ID_SELECT1		64			N/A			Get time automatically from the following time servers.											N/A		Tomar hora automáticamente de los siguientes servidores!	N/A
4		ID_PRIMARY_SERVER	32			N/A			Primary Server																N/A		Servidor principal						N/A
5		ID_SECONDARY_SERVER	32			N/A			Secondary Server															N/A		Servidor secundario						N/A
6		ID_TIMER_INTERVAL	64			N/A			Interval to Adjust Time															N/A		Intervalo de ajuste de hora					N/A
7		ID_SPECIFY_TIME		32			N/A			Specify Time																N/A		Especificar la hora						N/A
8		ID_GET_TIME		32			N/A			Get Local Time																N/A		Obtener hora local						N/A
9		ID_DATE			16			N/A			Date																	N/A		Fecha								N/A
10		ID_TIME			16			N/A			Time																	N/A		Hora								N/A
11		ID_SUBMIT		16			N/A			Setting																		N/A		Aplicar								N/A
12		ID_ERROR0		32			N/A			Unknown error.																N/A		Error desconocido!						N/A
13		ID_ERROR1		16			N/A			Successful.																N/A		Exito!								N/A
14		ID_ERROR2		128			N/A			Failed. Incorrect time setting.														N/A		Fallo, configuración hora incorrecta!				N/A
15		ID_ERROR3		128			N/A			Failed. Incomplete information.														N/A		Fallo, información incompleta!					N/A
16		ID_ERROR4		128			N/A			Failed. No authority.													N/A		Error, no tiene el nivel de autorización requerido!		N/A
17		ID_ERROR5		64			N/A			Cannot be modified. Controller is hardware protected.											N/A		ACU protegida por HW. No se puede modificar			N/A
18		ID_TIPS0		128			N/A			Primary Server IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 129.9.1.10. Enter 0.0.0.0 for no time server.	N/A		Dirección IP servidor primario incorrecta.\nFormato 'nnn.nnn.nnn.nnn', e.g, 129.9.1.10. Usar '0.0.0.0' para no servidor de tiempo.		N/A
19		ID_TIPS1		128			N/A			Secondary Server IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 129.9.1.10. Enter 0.0.0.0 for no time server.	N/A		Dirección IP servidor secundario incorrecta.\nFormato 'nnn.nnn.nnn.nnn', e.g, 129.9.1.10. Usar '0.0.0.0' para no servidor de tiempo.		N/A
20		ID_TIPS2		128			N/A			Incorrect time interval. \nTime interval should be a positive integer.									N/A		Intervalo de tiempo incorrecto.\nEl intervalo debe ser un entero positivo.	N/A
21		ID_TIPS3		128			N/A			Synchronizing time, please wait...													N/A		Sincronizando hora. Espere, por favor...			N/A
22		ID_TIPS4		128			N/A			Incorrect date setting.\nCorrect format should be 'yyyy/mm/dd', e.g., 2000/09/30							N/A		Fecha incorrecta.\nFormato: 'yyyy/mm/dd', e.g. 2000/09/30	N/A
23		ID_TIPS5		128			N/A			Incorrect time setting. \nCorrect format should be 'hh:mm:ss', e.g., 8:23:08								N/A		Hora incorrecta.\nFormato: 'hh:mm:ss', e.g. 8:23:08		N/A
24		ID_TIPS6		128			N/A			Date must be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.								N/A		La Fecha debe estar entre'1999/01/01 00:00:00' y '2038/02/06 00:00:00'.		N/A
25		ID_MINUTES		16			N/A			Minutes																	N/A		Minutos								N/A
26		ID_ZONE			16			N/A			Local Zone																N/A		Zona Local							N/A
27		ID_GET_ZONE		32			N/A			Get Local Zone																N/A		Obtener zona local						N/A
28		ID_TIPS7		128			N/A			The time server has been set. Time will be set by time server.									N/A		Se ha establecido el servidor de tiempos. Así que la hora será fijada por el servidor!		N/A

[p20_history_frame.htm:Number]	
0
																									
[p20_history_frame.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[p21_history_title.htm:Number]
4

[p21_history_title.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_DATA			16			N/A			Data History			N/A		Histórico Datos		N/A
2		ID_ALARM		16			N/A			Alarm History			N/A		Histórico Alarm		N/A
3		ID_LOG			16			N/A			Log				N/A		Registro		N/A
4		ID_BAT			16			N/A			Battery				N/A		Batería			N/A


[p22_history_dataquery.htm:Number]
53

[p22_history_dataquery.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_HISTORY_HEAD		32			N/A			Data History Query				N/A		Consulta Histórico datos				N/A
2		ID_DEVICE		16			N/A			Device						N/A		Dispositivo						N/A
3		ID_SIGNAL		16			N/A			Signal Name					N/A		Nombre señal						N/A
4		ID_VALUE		8			N/A			Value						N/A		Valor							N/A
5		ID_QUERY_TYPE		16			N/A			Query Type					N/A		Tipo Consulta						N/A
6		ID_QUERY		8			N/A			Query						N/A		Consulta						N/A
7		ID_HISTORY		16			N/A			History						N/A		Historia						N/A
8		ID_STATISTIC		16			N/A			Statistics					N/A		Estadísticas						N/A
9		ID_YEAR			8			N/A			Year						N/A		Año							N/A
10		ID_MONTH		8			N/A			Month						N/A		Mes							N/A
11		ID_DAY			8			N/A			Day						N/A		Día							N/A
12		ID_FROM			8			N/A			From						N/A		Desde							N/A
13		ID_TO			8			N/A			To						N/A		Hasta							N/A
14		ID_YEAR			8			N/A			Year						N/A		Año							N/A
15		ID_MONTH		8			N/A			Month						N/A		Mes							N/A
16		ID_DAY			8			N/A			Day						N/A		Día							N/A
17		ID_ERROR0		32			N/A			Unknown error.					N/A		Error desconocido					N/A
18		ID_ERROR1		32			N/A			Successful.					N/A		Exito							N/A
19		ID_ERROR2		32			N/A			No data.				N/A		No hay datos para consulta				N/A
20		ID_ERROR3		32			N/A			Failed						N/A		Fallo							N/A
21		ID_ERROR4		32			N/A			Failed. No authority.		N/A		Error, no tiene la autoridad requerida!			N/A
22		ID_ERROR5		64			N/A			Failed to communicate with the Controller.		N/A		Fallo comunicación con ACU				N/A
23		ID_QUERY		16			N/A			Query						N/A		Consulta						N/A
24		ID_SIGNAL		16			N/A			Signal						N/A		Señal							N/A
25		ID_VALUE		16			N/A			Value						N/A		Valor							N/A
26		ID_UNIT			16			N/A			Unit						N/A		Unidad							N/A
27		ID_TIME			16			N/A			Time						N/A		Tiempo							N/A
28		ID_DEVICE_NAME		16			N/A			Device Name					N/A		Dispositivo						N/A
29		ID_INDEX		16			N/A			Index						N/A		Indice							N/A
30		ID_ALARM_STATUS		16			N/A			Status						N/A		Estado							N/A
31		ID_INDEX		16			N/A			Index						N/A		Indice							N/A
32		ID_DEVICE		16			N/A			Device						N/A		Dispositivo						N/A
33		ID_SIGNAL		16			N/A			Signal							N/A		Señal							N/A
34		ID_CURRENT_VALUE	16			N/A			Current Value					N/A		Valor actual						N/A
35		ID_AVERAGE_VALUE	16			N/A			Average Value					N/A		Valor medio						N/A
36		ID_STAT_TIME		16			N/A			Statistic Time					N/A		Tiempo Estadísticas					N/A
37		ID_MAX_VALUE		16			N/A			Maximum Value					N/A		Valor máximo						N/A
38		ID_MAX_TIME		16			N/A			Maximum Time				N/A		Tiempo máximo						N/A
39		ID_MIN_VALUE		16			N/A			Minimum Value					N/A		Valor mínimo						N/A
40		ID_MIN_TIME		16			N/A			Minimum Time				N/A		Tiempo mínimo						N/A
41		ID_UNIT			16			N/A			Unit						N/A		Unidad							N/A
42		ID_DOWNLOAD		32			N/A			Upload						N/A		Subir						N/A
43		ID_TIPS			64			N/A			End time should be later than start time.	N/A		El tiempo final ha de ser mayor que el de comienzo	N/A
44		ID_MONTH_ERROR		32			N/A			Incorrect month.				N/A		Mes incorrecto!						N/A
45		ID_DAY_ERROR		32			N/A			Incorrect day.					N/A		Día incorrecto!						N/A
46		ID_HOUR_ERROR		32			N/A			Incorrect hour.					N/A		Hora incorrecta!					N/A
47		ID_FORMAT_ERROR		64			N/A			Incorrect format.				N/A		Formato incorrecto, debe ser				N/A
48		ID_YEAR_ERROR		32			N/A			Incorrect year.					N/A		Año incorrecto!						N/A
49		ID_MINUTE_ERROR		32			N/A			Incorrect minute.				N/A		Minuto incorrecto!					N/A
50		ID_SECOND_ERROR		32			N/A			Incorrect second.				N/A		Segundo incorrecto!					N/A
51		ID_INCORRECT_PARAM	64			N/A			Incorrect parameter.				N/A		Parámetro incorrecto					N/A
52		ID_ALL_DEVICE		64			N/A			All Devices					N/A		Todos los dispositivos					N/A
53		ID_MAX_NUMBER_TIPS	128			N/A			Maximum number of records is 500.	N/A		El número máximo de registros es 500. Entre los parámetros de consulta adecuados.			N/A



[p23_history_alarmquery.htm:Number]
43

[p23_history_alarmquery.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_ALARM_HEAD		64			N/A			Alarm History Query				N/A		Consulta histórico alarmas				N/A
2		ID_YEAR			16			N/A			Year						N/A		Año							N/A
3		ID_MONTH		16			N/A			Month						N/A		Mes							N/A
4		ID_DAY			16			N/A			Day						N/A		Día							N/A
5		ID_FROM			16			N/A			From						N/A		Desde							N/A
6		ID_TO			16			N/A			To						N/A		Hasta							N/A
7		ID_YEAR			16			N/A			Year						N/A		Año							N/A
8		ID_MONTH		16			N/A			Month						N/A		Mes							N/A
9		ID_DAY			16			N/A			Day						N/A		Día							N/A
10		ID_DEVICE_NAME		16			N/A			Device						N/A		Dispositivo						N/A
11		ID_QUERY		16			N/A			Query						N/A		Consulta						N/A
12		ID_DOWNLOAD		16			N/A			Upload						N/A		Subir							N/A
13		ID_INDEX		16			N/A			Index						N/A		Indice							N/A
14		ID_DEVICE		16			N/A			Device						N/A		Dispositivo						N/A
15		ID_ALARM_LEVEL		16			N/A			Alarm Level					N/A		Nivel de alarma						N/A
16		ID_VALUE		16			N/A			Value						N/A		Valor							N/A
17		ID_START_TIME		16			N/A			Start Time					N/A		Tiempo de inicio					N/A
18		ID_END_TIME		16			N/A			End Time					N/A		Tiempo final						N/A
19		ID_ERROR0		32			N/A			Unknown error.					N/A		Error desconocido					N/A
20		ID_ERROR1		32			N/A			Successful.					N/A		Exito!							N/A
21		ID_ERROR2		32			N/A			No data.					N/A		No hay datos para la consulta!				N/A
22		ID_ERROR3		32			N/A			Failed						N/A		Fallo							N/A
23		ID_ERROR4		32			N/A			Failed. No authority.		N/A		Fallo, no tiene la autoridad requerida!			N/A
24		ID_ERROR5		64			N/A			Failed to communicate with the Controller.	N/A		Fallo de Comunicación con ACU				N/A
25		ID_ERROR6		64			N/A			Clear successful.				N/A		Borrado con éxito					N/A
26		ID_OA			16			N/A			OA						N/A		O1							N/A
27		ID_MA			16			N/A			MA						N/A		A2							N/A
28		ID_CA			16			N/A			CA						N/A		A1							N/A
29		ID_SIGNAL_NAME		16			N/A			Signal Name						N/A		Nombre de señal						N/A
30		ID_CLEAR_CLARM		16			N/A			Clear Alarm					N/A		Borrar alarma						N/A
31		ID_SET			16			N/A			Set						N/A		Fijar							N/A
32		ID_DOWNLOAD		32			N/A			Upload						N/A		Subir						N/A
33		ID_TIPS			64			N/A			End time should be later than start time.	N/A		El tiempo final ha de ser mayor que el de inicio	N/A
34		ID_MONTH_ERROR		32			N/A			Incorrect month.				N/A		Mes incorrecto!						N/A
35		ID_DAY_ERROR		32			N/A			Incorrect day.					N/A		Día incorrecto!						N/A
36		ID_HOUR_ERROR		32			N/A			Incorrect hour.					N/A		Hora incorrecta!					N/A
37		ID_FORMAT_ERROR		64			N/A			Incorrect Format.				N/A		Formato incorrecto, debe ser				N/A
38		ID_YEAR_ERROR		32			N/A			Incorrect year.					N/A		Año incorrecto!						N/A
39		ID_MINUTE_ERROR		32			N/A			Incorrect minute.				N/A		Minuto incorrecto!					N/A
40		ID_SECOND_ERROR		32			N/A			Incorrect second.				N/A		Segundo incorrecto!					N/A
41		ID_INCORRECT_PARAM	64			N/A			Incorrect parameter.				N/A		Parámetro incorrecto					N/A
42		ID_ALL_DEVICE		64			N/A			All Devices					N/A		Todos los dispositivos					N/A
43		ID_MAX_NUMBER_TIPS	128			N/A			Maximum number of records is 500.	N/A		El número máximo de registros es 500. Entre los parámetros de consulta adecuados.					N/A


[p24_history_logquery.htm:Number]
93

[p24_history_logquery.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_LOG_HEAD		32			N/A			History Log Query				N/A		Consulta Histórico				N/A
2		ID_YEAR			16			N/A			Year						N/A		Año						N/A
3		ID_MONTH		16			N/A			Month						N/A		Mes						N/A
4		ID_DAY			16			N/A			Day						N/A		Día						N/A
5		ID_FROM			16			N/A			From						N/A		Desde						N/A
6		ID_TO			16			N/A			To						N/A		Hasta						N/A
7		ID_YEAR			16			N/A			Year						N/A		Año						N/A
8		ID_MONTH		16			N/A			Month						N/A		Mes						N/A
9		ID_DAY			16			N/A			Day						N/A		Día						N/A
10		ID_QUERY_TYPE		16			N/A			Query Type					N/A		Tipo Consulta					N/A
11		ID_QUERY		16			N/A			Query						N/A		Consulta					N/A
12		ID_CONTROL_LOG		16			N/A			Control Log					N/A		Registro Control				N/A
13		ID_SYSTEM_LOG		16			N/A			System Log					N/A		Registro Sistema				N/A
14		ID_ERROR0		32			N/A			Unknown error.					N/A		Error Desconocido!				N/A
15		ID_ERROR1		32			N/A			Acquired Control log successfully.				N/A		Registro Control bien!				N/A
16		ID_ERROR2		32			N/A			No data.				N/A		No hay datos para la Consulta!			N/A
17		ID_ERROR3		32			N/A			Failed.					N/A		Fallo!						N/A
18		ID_ERROR4		64			N/A			Failed. No authority.		N/A		Error, no tiene la autorización requerida!	N/A
19		ID_ERROR5		32			N/A			Failed to communicate with the Controller.		N/A		Fallo comunicación con ACU!			N/A
20		ID_ERROR6		32			N/A			Acquired Battery log successfully.		N/A		Registro batería bien!				N/A
21		ID_ERROR7		32			N/A			Acquired System log successfully.		N/A		Registro sistema bien!				N/A
22		ID_BATTERY_TEST_REASON0	64			N/A			Start Planned Test				N/A		Inicio prueba planificada			N/A
23		ID_BATTERY_TEST_REASON1	64			N/A			Start Manual Test			N/A		Inicio prueba manual				N/A
24		ID_BATTERY_TEST_REASON2	64			N/A			Start AC Fail Test			N/A		Inicio prueba Fallo de Red			N/A
25		ID_BATTERY_TEST_REASON3	64			N/A			Start Master Power Test		N/A		Inicio prueba Master Power			N/A
26		ID_BATTERY_TEST_REASON4	64			N/A			Other Reasons					N/A		Otra razón					N/A
27		ID_BATTERY_END_REASON0	64			N/A			End by Manual					N/A		Parada manual					N/A
28		ID_BATTERY_END_REASON1	64			N/A			End by Alarm					N/A		Parada por alarma				N/A
29		ID_BATTERY_END_REASON2	64			N/A			Time Up						N/A		Tiempo agotado					N/A
30		ID_BATTERY_END_REASON3	64			N/A			Capacity					N/A		Capacidad				N/A
31		ID_BATTERY_END_REASON4	64			N/A			End by Voltage						N/A		Fin por tensión					N/A
32		ID_BATTERY_END_REASON5	64			N/A			End by AC Fail					N/A		Fin por fallo Red				N/A
33		ID_BATTERY_END_REASON6	64			N/A			End by AC Restore				N/A		Fin por retorno Red				N/A
34		ID_BATTERY_END_REASON7	64			N/A			End by AC Fail Test Discharge			N/A		Fin prueba de descarga por Fallo Red		N/A
35		ID_BATTERY_END_REASON8	64			N/A			End test for Master Power. Stop test.	N/A		Fin prueba Master Power				N/A
36		ID_BATTERY_END_REASON9	128			N/A			End a PowerSplit BT for Auto/Man. Turn to Manual.	N/A		Fin prueba PowerSplit por cambio Auto a Manual	N/A
37		ID_BATTERY_END_REASON10	128			N/A			End a PowerSplit Man-BT for Auto/Man. Turn to Auto.	N/A		Fin prueba PowerSplit por cambio Manual a Auto	N/A
38		ID_BATTERY_END_REASON11	64			N/A			End by other reasons.			N/A		Fin por otra razón				N/A
39		ID_BATTERY_TEST_RESULT0	64			N/A			No Result					N/A		Sin resultado					N/A
40		ID_BATTERY_TEST_RESULT1	64			N/A			Good Results				N/A		Buen resultado					N/A
41		ID_BATTERY_TEST_RESULT2	64			N/A			Bad Results					N/A		Mal resultado					N/A
42		ID_BATTERY_TEST_RESULT3	64			N/A			It's a Power Split Test.				N/A		Es una prueba Power Split			N/A
43		ID_BATTERY_TEST_RESULT4	64			N/A			Other Results					N/A		Otro resultado					N/A
44		SYS_LOG_HEAD0		64			N/A			Index						N/A		Indice						N/A
45		SYS_LOG_HEAD1		64			N/A			Task Name					N/A		Nombre de tarea					N/A
46		SYS_LOG_HEAD2		64			N/A			Info Level				N/A		Nivel de información				N/A
47		SYS_LOG_HEAD3		64			N/A			Log Time					N/A		Hora registro					N/A
48		SYS_LOG_HEAD4		64			N/A			Information					N/A		Información					N/A
49		CTL_LOG_HEAD0		64			N/A			Index						N/A		Indice						N/A
50		CTL_LOG_HEAD1		64			N/A			Equipment Name					N/A		Nombre de Equipo				N/A
51		CTL_LOG_HEAD2		64			N/A			Signal Name					N/A		Nombre de Señal					N/A
52		CTL_LOG_HEAD3		64			N/A			Control Value					N/A		Valor de Control				N/A
53		CTL_LOG_HEAD4		64			N/A			Unit						N/A		Unidad						N/A
54		CTL_LOG_HEAD5		64			N/A			Control Time						N/A		Hora de Control					N/A
55		CTL_LOG_HEAD6		64			N/A			Sender Name					N/A		Nombre de Emisor				N/A
56		CTL_LOG_HEAD7		64			N/A			Sender Type					N/A		Tipo de Emisor					N/A
57		CTL_LOG_HEAD8		64			N/A			Control Results				N/A		Resultado Control				N/A
58		ID_CTL_RESULT0		64			N/A			Successful.					N/A		Éxito						N/A
59		ID_CTL_RESULT1		64			N/A			No Memory					N/A		Sin memoria					N/A
60		ID_CTL_RESULT2		64			N/A			Time Out					N/A		Tiempo agotado					N/A
61		ID_CTL_RESULT3		64			N/A			Failed						N/A		Fallo						N/A
62		ID_CTL_RESULT4		64			N/A			Communication Busy				N/A		Comunicación ocupada				N/A
63		ID_CTL_RESULT5		64			N/A			Control was suppressed.				N/A		El control fue suprimido			N/A
64		ID_CTL_RESULT6		64			N/A			Control was disabled.				N/A		El control fue deshabilitado			N/A
65		ID_CTL_RESULT7		64			N/A			Control was disabled.				N/A		El control fue deshabilitado			N/A
66		ID_DOWNLOAD		32			N/A			Upload						N/A		Subir					N/A
67		ID_TIPS			64			N/A			End time should be later than start time.	N/A		El tiempo final ha de ser mayor que el inicial	N/A
68		ID_MONTH_ERROR		32			N/A			Incorrect month.				N/A		Mes incorrecto!					N/A
69		ID_DAY_ERROR		32			N/A			Incorrect day.					N/A		Día incorrecto!					N/A
70		ID_HOUR_ERROR		32			N/A			Incorrect hour.					N/A		Hora incorrecta!				N/A
71		ID_FORMAT_ERROR		64			N/A			Incorrect format.				N/A		Formato incorrecto, debe ser			N/A
72		ID_YEAR_ERROR		32			N/A			Incorrect year.					N/A		Año incorrecto!					N/A
73		ID_MINUTE_ERROR		32			N/A			Incorrect minute.				N/A		Minuto incorrecto!				N/A
74		ID_SECOND_ERROR		32			N/A			Incorrect second.				N/A		Segundo incorrecto!				N/A
75		ID_INCORRECT_PARAM	64			N/A			Incorrect parameter.				N/A		Parámetro incorrecto				N/A
76		ID_DISEL_TEST_REASON0	64			N/A			Planned Test					N/A		Prueba planificada				N/A
77		ID_DISEL_TEST_REASON1	64			N/A			Manual Start					N/A		Inicio Manual					N/A
78		ID_DISEL_TEST_RESULT0	64			N/A			Normal						N/A		Normal						N/A
79		ID_DISEL_TEST_RESULT1	64			N/A			End by Manual					N/A		Parada Manual					N/A
80		ID_DISEL_TEST_RESULT2	64			N/A			Time is up.				N/A		Tiempo agotado					N/A
81		ID_DISEL_TEST_RESULT3	64			N/A			In Manual State					N/A		En modo manual					N/A
82		ID_DISEL_TEST_RESULT4	64			N/A			Low Battery Voltage				N/A		Tensión de batería baja				N/A
83		ID_DISEL_TEST_RESULT5	64			N/A			High Water Temperature				N/A		Alta temperatura de agua			N/A
84		ID_DISEL_TEST_RESULT6	64			N/A			Low Oil Pressure				N/A		Baja presión de aceite				N/A
85		ID_DISEL_TEST_RESULT7	64			N/A			Low Fuel Level					N/A		Baja presión de fuel				N/A
86		ID_DISEL_TEST_RESULT8	64			N/A			Diesel Failure					N/A		Fallo Grupo electrógeno				N/A
87		ID_DISEL_TEST_HEAD0	64			N/A			Index						N/A		Indice						N/A
88		ID_DISEL_TEST_HEAD1	64			N/A			Start Time					N/A		Hora de inicio					N/A
89		ID_DISEL_TEST_HEAD2	64			N/A			End Time					N/A		Hora fin					N/A
90		ID_DISEL_TEST_HEAD3	64			N/A			Start Reason					N/A		Razón de inicio					N/A
91		ID_DISEL_TEST_HEAD4	64			N/A			Test Results					N/A		Resultado de la prueba				N/A
92		ID_DISEL_LOG		64			N/A			Diesel Test Log					N/A		Registro prueba Grupo electrógeno		N/A
93		ID_MAX_NUMBER_TIPS	128			N/A			Maximum number of records is 500.	N/A		El número máximo de registros es 500. Entre los parámetros de consulta adecuados.			N/A


[p25_online_frame.htm:Number]
0

[p25_online_frame.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[p26_online_title.htm:Number]
3

[p26_online_title.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL		MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_MODIFY_SCUP		16				N/A			Modify Controller		N/A		Modificar ACU		N/A
2		ID_MODIFY_DEVICE	16				N/A			Modify Device			N/A		Modificar Disp		N/A
3		ID_MODIFY_ALARM		16				N/A			Modify Signal			N/A		Modificar Señal		N/A

[p27_online_modifysystem.htm:Number]
21

[p27_online_modifysystem.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR		FULL_IN_EN							ABBR_IN_EN	FULL_IN_LOCALE								ABBR_IN_LOCALE
1		ID_ERROR0		32				N/A			Failed.							N/A		Fallo									N/A
2		ID_ERROR1		32				N/A			Successful.							N/A		Éxito									N/A
3		ID_ERROR2		32				N/A			Unknown Error							N/A		Error Desconocido							N/A
4		ID_ERROR3		32				N/A			Failed. No authority.				N/A		Error, no tiene la autorización requerida!				N/A
5		ID_ERROR4		32				N/A			Communication Failure						N/A		Fallo de comunicación							N/A
6		ID_DEVICE		32				N/A			Device Name							N/A		Nombre de dispositivo							N/A
7		ID_SIGNAL		32				N/A			Signal Name							N/A		Nombre de Señal								N/A
8		ID_VALUE		32				N/A			Value								N/A		Valor									N/A
9		ID_SETTING_VALUE	32				N/A			Setting Value							N/A		Fijar valor								N/A
10		ID_SET			32				N/A			Set								N/A		Fijar									N/A
11		ID_TIPS0		128				N/A			Input error.							N/A		Error de entrada							N/A
12		ID_TIPS1		128				N/A			Invalid characters were included in input.\nPlease try again.	N/A		La entrada contiene caracteres no válidos.\nPor favor, pruebe otra vez!	N/A
13		ID_TIPS2		128				N/A			Modify								N/A		Modificar								N/A
14		ID_SIGNAL_TYPE		32				N/A			Signal Type							N/A		Tipo señal								N/A
15		ID_ERROR5		32				N/A			Failed. Controller is hardware protected.			N/A		Fallo: ACU protegida por HW						N/A
16		ID_SET2			32				N/A			Set								N/A		Fijar									N/A
17		ID_SITE			32				N/A			Site								N/A		Planta									N/A
18		ID_ERROR5		64				N/A			Character length must not exceed 32.			N/A		El tamaño máximo es de 32 caracteres					N/A
19		ID_SITE_NAME		64				N/A			Site Name							N/A		Nombre de Central							N/A
20		ID_SITE_LOCATION	64				N/A			Site Location							N/A		Localización								N/A
21		ID_SITE_DESCTIPTION	64				N/A			Site Description						N/A		Descripción								N/A


[p28_online_modifydevice.htm:Number]
23

[p28_online_modifydevice.htm]
#Sequence ID	RES_ID				MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN									ABBR_IN_EN	FULL_IN_LOCALE											ABBR_IN_LOCALE
1		ID_ERROR0			32			N/A			Failed.										N/A		Fallo!												N/A
2		ID_ERROR1			32			N/A			Successful.									N/A		Éxito												N/A
3		ID_ERROR2			32			N/A			Unknown error.									N/A		Error Desconocido										N/A
4		ID_ERROR3			32			N/A			Failed. No authority.						N/A		Fallo, no tiene el nivel de autorización requerido!						N/A
5		ID_ERROR4			64			N/A			Communication Failure								N/A		Fallo de comunicación										N/A
6		ID_DEVICE			32			N/A			Device Name									N/A		Nombre de dispositivo										N/A
7		ID_NEWDEVICE			64			N/A			New Device Name									N/A		Nuevo nombre dispositivo									N/A
8		ID_SET				16			N/A			Set										N/A		Fijar												N/A
9		ID_TIPS0			64			N/A			Input device name please.							N/A		Entre nombre de dispositivo, por favor								N/A
10		ID_TIPS1			128			N/A			User Name must not contain any of the following characters.\nPlease try again.	N/A		Estos caracteres no deben estar incluidos en el nombre de usuario\nPor favor, pruebe otra vez!	N/A
11		ID_TIPS2			32			N/A			No Device									N/A		No hay dispositivo										N/A
12		ID_TIPS3			32			N/A			Modify Device									N/A		Modificar dispositivo										N/A
13		ID_SET				16			N/A			Set										N/A		Fijar												N/A
14		ID_MODIY_FULL_NAME		16			N/A			Full Name									N/A		Completo											N/A
15		ID_MODIY_ABBR_NAME		16			N/A			Abbreviated Name									N/A		Abreviado											N/A
16		ID_INDEX			16			N/A			Index										N/A		Indice												N/A
17		ID_DEVICE_ABBR_NAME		32			N/A			Device Abbreviated Name									N/A		Nombre abreviado										N/A
18		ID_MODIFY_NAME_TYPE		32			N/A			Modify Name Type								N/A		Modificar Nombre										N/A
19		ID_TOOLONG_NAME16		64			N/A			Character length must not exceed 16.					N/A		El máximo es 16 caracteres									N/A
20		ID_TOOLONG_NAME32		64			N/A			Character length must not exceed 32.					N/A		El máximo es 32 caracteres									N/A
21		ID_ERROR5			64			N/A			Failed. Controller is hardware protected.					N/A		Fallo: ACU protegida por HW									N/A
22		ID_INVALID_CHAR			64			N/A			Invalid character.								N/A		Caracter no válido										N/A
23		ID_TIPS4			128			N/A			[Tips]The new device name will be shown in device tree after reconnecting.	N/A		[Aviso]El nuevo nombre de dispositivo aparecerá en el árbol una vez vuelva a conectarse!	N/A

[p29_online_modifyalarm.htm:Number]
48

[p29_online_modifyalarm.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_ERROR0		32			N/A			Failed.						N/A		Fallo!							N/A
2		ID_ERROR1		32			N/A			Successful.					N/A		Exito!							N/A
3		ID_ERROR2		32			N/A			Unknown error.				N/A		Error desconocido					N/A
4		ID_ERROR3		32			N/A			Failed. No authority.		N/A		Fallo, no tiene el nivel de autorización requerido!	N/A
5		ID_ERROR4		32			N/A			Communication Failure				N/A		Fallo de comunicación					N/A
6		ID_ERROR5		64			N/A			Failed. Controller is hardware protected.	N/A		Fallo: ACU protegida por HW				N/A
7		ID_ALARM_HEAD		32			N/A			Query Device Type				N/A		Dispositivo						N/A
8		ID_SINGAL_EXPLORE	32			N/A			Signal Explore				N/A		Explorar señales					N/A
9		ID_INDEX		32			N/A			Index						N/A		Indice							N/A
10		ID_SIGNAL_NAME		32			N/A			Signal Full Name				N/A		Nombre completo de señal				N/A
11		ID_ALARM_LEVEL		32			N/A			Alarm Level					N/A		Nivel de alarma						N/A
12		ID_NEW_NAME		32			N/A			New Name					N/A		Nuevo nombre						N/A
13		ID_NEW_LEVEL		32			N/A			New Level					N/A		Nuevo nivel						N/A
14		ID_SET			8			N/A			Set						N/A		Fijar							N/A
15		ID_INDEX		16			N/A			Index						N/A		Indice							N/A
16		ID_SIGNAL_NAME		32			N/A			Signal Full Name				N/A		Nombre completo señal					N/A
17		ID_NEW_NAME		16			N/A			New Name					N/A		Nuevo nombre						N/A
18		ID_SET			8			N/A			Set						N/A		Fijar							N/A
19		ID_DEVICE_NAME		16			N/A			Device						N/A		Dispositivo						N/A
20		ID_TYPE			16			N/A			Type						N/A		Tipo							N/A
21		ID_NA			16			N/A			NA						N/A		NA							N/A
22		ID_OA			16			N/A			OA						N/A		O1							N/A
23		ID_MA			16			N/A			MA						N/A		A2							N/A
24		ID_CA			16			N/A			CA						N/A		A1							N/A
25		ID_SAMPLE_SIGNAL	32			N/A			Sample Signal					N/A		Señal Muestra						N/A
26		ID_CONTROL_SIGNAL	32			N/A			Control Signal					N/A		Señal Control						N/A
27		ID_SETTING_SIGNAL	32			N/A			Setting Signal					N/A		Señal Configuración					N/A
28		ID_ALARM_SIGNAL		32			N/A			Alarm Signal					N/A		Señal de Alarma						N/A
29		ID_NO_SAMPLE		32			N/A			No Sample Signal				N/A		No hay señal de Muestra					N/A
30		ID_NO_CONTROL		32			N/A			No Control Signal				N/A		No hay señal de Control					N/A
31		ID_NO_SETTING		32			N/A			No Setting Signal				N/A		No hay señal de Configuración				N/A
32		ID_NO_ALARM		32			N/A			No Alarm Signal					N/A		No hay señal de Alarma					N/A
33		ID_NO_SIGNAL_TYPE	32			N/A			No Signal Type			N/A		No este tipo de señal					N/A
34		ID_VAR_SET		8			N/A			Set						N/A		Fijar							N/A
35		ID_SIGNAL_TYPE		32			N/A			Signal Type					N/A		Tipo de señal						N/A
36		ID_SHOW_TIPS0		32			N/A			The new name cannot be null.			N/A		El nuevo nombre no puede ser nulo			N/A
37		ID_SET			8			N/A			Set						N/A		Fijar							N/A
38		ID_SIGNAL_ABBR_NAME	32			N/A			Signal Abbreviated Name				N/A		Nombre Abreviado					N/A
39		ID_SIGNAL_ABBR_NAME	32			N/A			Signal Abbreviated Name				N/A		Nombre Abreviado					N/A
40		ID_MODIY_FULL_NAME	32			N/A			Full Name					N/A		Completo						N/A
41		ID_MODIY_ABBR_NAME	32			N/A			Abbreviated Name					N/A		Abreviado						N/A
42		ID_MODIY_FULL_NAME	32			N/A			Full Name					N/A		Completo						N/A
43		ID_MODIY_ABBR_NAME	32			N/A			Abbreviated Name					N/A		Abreviado						N/A
44		ID_NEW_NAME_TYPE	32			N/A			Modify Type					N/A		Modificar Nombre					N/A
45		ID_NEW_NAME_TYPE	32			N/A			Modify Type				N/A		Modificar Nombre					N/A
46		ID_INVALID_CHAR		64			N/A			Invalid Character				N/A		Caracter no válido					N/A
47		ID_TOOLONG_NAME16	64			N/A			Character length must not exceed 16.	N/A		El máximo es 16 caracteres				N/A
48		ID_TOOLONG_NAME32	64			N/A			Character length must not exceed 32.	N/A		El máximo es 32 caracteres				N/A

[p30_acu_signal_value.htm:Number]
13

[p30_acu_signal_value.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL		MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE			ABBR_IN_LOCALE
1		ID_ERROR0		32			N/A			Failed					N/A		Fallo!				N/A
2		ID_ERROR1		32			N/A			Successful.					N/A		Éxito!				N/A
3		ID_ERROR2		32			N/A			Unknown error.					N/A		Error desconocido		N/A
4		ID_ERROR3		32			N/A			Failed. No authority.		N/A		Fallo, no tiene autorización!	N/A
5		ID_ERROR4		32			N/A			Communication Failure				N/A		Fallo comunicación		N/A
6		ID_ERROR5		32			N/A			Failed. Controller is hardware protected.	N/A		Fallo: ACU protegida por HW	N/A
7		ID_TIPS2		32			N/A			No Device					N/A		Sin dispositivo			N/A
8		ID_DEVICE		32			N/A			Equipment					N/A		Equipo				N/A
9		ID_PRODUCT_NUMBER	32			N/A			Product Model					N/A		Número de producto		N/A
10		ID_PRODUCT_VERSION	32			N/A			Product Revision				N/A		Revisión de producto		N/A
11		ID_PRODUCT_SERIAL	32			N/A			Serial Number					N/A		Número de Serie			N/A
12		ID_PRODUCT_SWVERSION	32			N/A			Software Revision				N/A		Revisión de Software		N/A
13		ID_PRODUCT_INFO_HEAD	32			N/A			Product Information				N/A		Información de Producto		N/A

[p31_close_system.htm:Number]
9

[p31_close_system.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN								ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_HEAD			16			N/A			Stop Controller								N/A		Parar ACU						N/A
2		ID_TIPS			128			N/A			Download needs to stop the Controller. Do you want to stop the Controller?	N/A		La transferencia de archivos requiere parar la ACU. ¿Está seguro?		N/A
3		ID_CLOSE_SCUP		16			N/A			Stop Controller								N/A		Parar ACU						N/A
4		ID_CANCEL		16			N/A			Cancel									N/A		Cancelar						N/A
5		ID_ERROR0		32			N/A			Unknown error.								N/A		Error Desconocido!					N/A
6		ID_ERROR1		128			N/A			Controller was stopped successfully. You can download the file.		N/A		ACU parada con éxito. Ya puede descargar el arhivo	N/A
7		ID_ERROR2		64			N/A			Failed to stop the Controller.						N/A		Fallo al parar ACU					N/A
8		ID_ERROR3		64			N/A			You do not have authority to stop the Controller.				N/A		No está autorizado a parar la ACU!			N/A
9		ID_ERROR4		64			N/A			Failed to communicate with the Controller.				N/A		Fallo comunicación con ACU				N/A

[p32_start_system.htm:Number]
9


[p32_start_system.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN								ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_HEAD			16			N/A			Start Controller							N/A		Iniciar ACU						N/A
2		ID_TIPS			128			N/A			Download has finished. Do you want to start the Controller?		N/A		La descarga está lista. Quiere iniciar la ACU?		N/A
3		ID_START_SCUP		16			N/A			Start Controller							N/A		Iniciar ACU						N/A
4		ID_CANCEL		16			N/A			Cancel									N/A		Cancelar						N/A
5		ID_ERROR0		32			N/A			Unknown error.								N/A		Error desconocido!					N/A
6		ID_ERROR1		128			N/A			Controller was started successfully. You can login to the Controller in 1 minute.	N/A		ACU iniciada con éxito. Puede conectarse en 1 minuto!	N/A
7		ID_ERROR2		64			N/A			Failed to stop the Controller.							N/A		Fallo al parar ACU					N/A
8		ID_ERROR3		64			N/A			You do not have authority to start the Controller.			N/A		No está autorizado para iniciar la ACU!			N/A
9		ID_ERROR4		64			N/A			Failed to communicate with the Controller.			N/A		Fallo de comunicación con ACU				N/A

[p33_replace_file.htm:Number]
43


[p33_replace_file.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN							ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_REPLACE_HEAD		16			N/A			Replace File							N/A		Reemplazar archivo					N/A
2		ID_REPLACE_UPLOADED	32			N/A			Has been downloaded.						N/A		Ha sido cargado						N/A
3		ID_SELECE_FILE_TYPE	64			N/A			Please select download file type.				N/A		Seleccione tipo de archivo a cargar			N/A
4		ID_FILE_CONFIGURE	16			N/A			Configure File						N/A		Archivo de configuración				N/A
5		ID_FILE_APPLICATION	32			N/A			Application File						N/A		Archivo de Aplicación					N/A
6		ID_SELECT_REPLACE_FILE	64			N/A			Please select file that is to be replaced.			N/A		Por favor, seleccione el archivo que será reemplazado	N/A
7		ID_REPLACE		16			N/A			Replace								N/A		Reemplazar						N/A
8		ID_RETURN		16			N/A			Return								N/A		Volver							N/A
9		ID_DOWNLOAD_HEAD	32			N/A			Download File						N/A		Descargar archivo					N/A
10		ID_FILE1		32			N/A			Select File							N/A		Seleccionar archivo					N/A
11		ID_FILE0		32			N/A			File in Controller						N/A		Archivo desde ACU					N/A
12		ID_ERROR0		32			N/A			Unknown error.							N/A		Error desconocido					N/A
13		ID_ERROR1		64			N/A			File downloaded successfully.					N/A		Archivo cargado con éxito!				N/A
14		ID_ERROR2		64			N/A			Failed to download file.					N/A		Fallo al cargar archivo!				N/A
15		ID_ERROR3		64			N/A			Failed to download file, the file is too large.			N/A		Fallo al cargar archivo. El archivo es demasiado grande!	N/A
16		ID_ERROR4		64			N/A			Failed. No authority.				N/A		Fallo, no tiene suficientes derechos!			N/A
17		ID_ERROR5		64			N/A			Controller started successfully.				N/A		ACU iniciada con éxito!					N/A
18		ID_ERROR6		64			N/A			File downloaded successfully.					N/A		Listo para cargar archivo!				N/A
19		ID_ERROR7		64			N/A			Failed to download file.					N/A		Fallo al descargar archivo!				N/A
20		ID_ERROR8		64			N/A			Failed to upload file.						N/A		Fallo al descargar archivo!				N/A
21		ID_ERROR9		64			N/A			File downloaded successfully.				N/A		Archivo cargado con éxito!				N/A
22		ID_UPLOAD		64			N/A			Upload								N/A		Cargar						N/A
23		ID_DOWNLOAD		64			N/A			Download					N/A		Descargar							N/A
24		ID_STARTSCUP		64			N/A			Start Controller						N/A		Iniciar ACU						N/A
25		ID_SHOWTIPS0		64			N/A			Are you sure you want to start the Controller?			N/A		Está seguro de iniciar la ACU?				N/A
26		ID_SHOWTIPS1		128			N/A			Please reboot Controller before you leave this page. \n Are you sure you want to leave?		N/A	Por favor, reinicie la ACU antes de abandonar la página\nEstá seguro?	N/A
27		ID_SHOWTIPS2		128			N/A			Controller will reboot. Wait a few minutes before reconnecting.	N/A	La ACU se va a reiniciar. Cierre el navegador y espere un par de minutos antes de volver a conectarse!		N/A
28		ID_SHOWTIPS3		64			N/A			It's time to start Controller.					N/A		Es hora de iniciar la ACU!				N/A
29		ID_SHOWTIPS4		64			N/A			Format not supported, select again please.		N/A		Formato no soportado, seleccione otra vez!		N/A
30		ID_CONFIG_TAR		32			N/A			Configuration Package						N/A		Paquete de Configuración				N/A
31		ID_LANG_TAR		32			N/A			Language Package						N/A		Paquete de Idioma					N/A
32		ID_PROGRAM_TAR		32			N/A			Program Package							N/A		Paquete de programa					N/A
33		ID_SHOWTIPS5		64			N/A			The file name cannot be null.				N/A		¡El nombre de archivo no puede ser nulo!		N/A
34		ID_SHOWTIPS6		64			N/A			Are you sure you want to download?					N/A		Está seguro de cargar					N/A
35		ID_SHOWTIPS7		128			N/A			Incorrect file type or file name contains invalid characters. Please download *.tar.gz or *.tar.		N/A	Tipo de archivo incorrecto o con caracteres no permitidos. ¡Cargue sólo *tar.gz o *.tar!		N/A
36		ID_SHOWTIPS8		128			N/A			Are you sure you want to start the Controller?				N/A		Está seguro de iniciar la ACU?				N/A
37		ID_TIPS6		64			N/A			Please wait. Controller is rebooting...				N/A		La ACU está reiniciando, espere...			N/A
38		ID_TIPS7		64			N/A			Parameters have been modified. Controller is rebooting...	N/A		Reiniciando para aplicar los cambios			N/A
39		ID_TIPS8		64			N/A			Controller homepage will be refreshed.		N/A		La página de inicio de la ACU se refrescará después	N/A
40		ID_SOLUTION_FILE	64			N/A			Solution File							N/A		Archivo Solution					N/A
41		ID_UPLOAD		64			N/A			Upload					N/A		Cargar						N/A
42		ID_SHOWTIPS9		512			N/A			Caution: Only the file package of tar and tar.gz can be downloaded. If the downloaded file is NOT correct, the Controller will run abnormally. The Controller must be restarted manually after download.	N/A		Precaución: Solo se pueden cargar archivos de configuración (con extensión tar o tar.gz). Si el archivo no es correcto, puede que la controladora no funcione bien. ¡Tras la carga utilice el botón Iniciar ACU para reiniciar el sistema!		N/A
43		ID_ERROR10		64			N/A			Failed to upload file. Hardware is protected.	N/A		Fallo al bajar archivo. La ACU está protegida por HW!		N/A


[p34_history_batterylogquery.htm:Number]
909


[p34_history_batterylogquery.htm]
#Sequence ID	RES_ID					MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN						ABBR_IN_EN	FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_BATTERY_TEST_START_REASON0		64			N/A			Plan battery test.					N/A		Prueba de baterías programada				N/A
2		ID_BATTERY_TEST_START_REASON1		64			N/A			Start Test Manually				N/A		Inicio prueba manual					N/A
3		ID_BATTERY_TEST_START_REASON2		64			N/A			Start AC Fail test.			N/A		Inicio prueba por Fallo de Red				N/A
4		ID_BATTERY_TEST_START_REASON3		64			N/A			Start Master Power test.		N/A		Iniciar prueba Master Power				N/A
5		ID_BATTERY_TEST_START_REASON4		64			N/A			Other Reasons						N/A		Otra razón						N/A
6		ID_BATTERY_TEST_END_REASON0		64			N/A			End Test Manually				N/A		Paro manual Prueba					N/A
7		ID_BATTERY_TEST_END_REASON1		64			N/A			End Test for Alarm				N/A		Paro prueba por alarma					N/A
8		ID_BATTERY_TEST_END_REASON2		64			N/A			End Test for Test Time-Out				N/A		Paro prueba por tiempo					N/A
9		ID_BATTERY_TEST_END_REASON3		64			N/A			End Test for Capacity Condition			N/A		Paro prueba por límite de capacidad excedido		N/A
10		ID_BATTERY_TEST_END_REASON4		64			N/A			End Test for Voltage Condition		N/A		Paro prueba por tensión					N/A
11		ID_BATTERY_TEST_END_REASON5		64			N/A			End Test for AC Fail				N/A		Paro prueba por fallo de red				N/A
12		ID_BATTERY_TEST_END_REASON6		64			N/A			End AC Fail Test for AC Restore		N/A		Paro prueba Fallo Red por retorno CA			N/A
13		ID_BATTERY_TEST_END_REASON7		64			N/A			End AC Fail Test for Disabled		N/A		Paro prueba Fallo Red por deshabilitada			N/A
14		ID_BATTERY_TEST_END_REASON8		64			N/A			End Master Power Test			N/A		Paro prueba por Master Power				N/A
15		ID_BATTERY_TEST_END_REASON9		64			N/A			Stop a PowerSplit BT for Auto/Man. Turn to Manual.		N/A		Paro prueba PowerSplit por cambio Auto/Manual		N/A
16		ID_BATTERY_TEST_END_REASON10		64			N/A			Stop a PowerSplit Man-BT for Auto/Man. Turn to Auto.		N/A		Paro prueba PowerSplit por cambio Manual/Auto		N/A
17		ID_BATTERY_TEST_END_REASON11		64			N/A			Stop by other reasons.			N/A		Parada por otra razón					N/A
18		ID_BATTERY_TEST_RESULT0			16			N/A			No test result.						N/A		Prueba sin resultado					N/A
19		ID_BATTERY_TEST_RESULT1			16			N/A			Battery is OK						N/A		Batería bien						N/A
20		ID_BATTERY_TEST_RESULT2			16			N/A			Battery is Bad						N/A		Batería mal						N/A
21		ID_BATTERY_TEST_RESULT3			16			N/A			It's a Power Split Test					N/A		Es una prueba PowerSplit				N/A
22		ID_BATTERY_TEST_RESULT4			16			N/A			Other Results						N/A		Otro resultado						N/A
23		ID_ERROR0				64			N/A			Unknown error.						N/A		Error desconocido					N/A
24		ID_ERROR1				64			N/A			Acquired control log successfully.			N/A		Registro Control con éxito				N/A
25		ID_ERROR2				64			N/A			No data.				N/A		No hay datos.				N/A
26		ID_ERROR3				64			N/A			Failed.						N/A		Fallo!							N/A
27		ID_ERROR4				64			N/A			Failed. No authority.			N/A		Error, no tiene la autoridad requerida!			N/A
28		ID_ERROR5				64			N/A			Failed to communicate with the Controller.		N/A		Fallo de comunicación con la ACU			N/A
29		ID_ERROR6				64			N/A			Acquired battery log successfully.			N/A		Registro batería con éxito				N/A
30		ID_ERROR7				64			N/A			Acquired System log successfully.			N/A		Registro Sistema con éxito				N/A
31		ID_LOG_HEAD				16			N/A			Battery Test Log Query					N/A		Pruebas Batería						N/A
32		ID_TIPS					32			N/A			Choose the last battery test.				N/A		Nº Registro de prueba:					N/A
33		ID_QUERY				16			N/A			Query							N/A		Consulta						N/A
34		ID_HEAD0				32			N/A			Battery1 Current					N/A		Corriente Batería 1					N/A
35		ID_HEAD1				32			N/A			Battery1 Voltage					N/A		Tensión Batería 1					N/A
36		ID_HEAD2				32			N/A			Battery1 Capacity					N/A		Capacidad Batería 1					N/A
37		ID_HEAD3				32			N/A			Battery2 Current					N/A		Corriente Batería 2					N/A
38		ID_HEAD4				32			N/A			Battery2 Voltage					N/A		Tensión Batería 2					N/A
39		ID_HEAD5				32			N/A			Battery2 Capacity					N/A		Capacidad Batería 2					N/A
40		ID_HEAD6				32			N/A			EIB1Battery1 Current					N/A		EIB1 Corriente Batería 1				N/A
41		ID_HEAD7				32			N/A			EIB1Battery1 Voltage					N/A		EIB1 Tensión Batería 1					N/A
42		ID_HEAD8				32			N/A			EIB1Battery1 Capacity					N/A		EIB1 Capacidad Batería 1				N/A
43		ID_HEAD9				32			N/A			EIB1Battery2 Current					N/A		EIB1 Corriente Batería 2				N/A
44		ID_HEAD10				32			N/A			EIB1Battery2 Voltage					N/A		EIB1 Tensión Batería 2					N/A
45		ID_HEAD11				32			N/A			EIB1Battery2 Capacity					N/A		EIB1 Capacidad Batería 2				N/A
46		ID_HEAD12				32			N/A			EIB2Battery1 Current					N/A		EIB2 Corriente Batería 1				N/A
47		ID_HEAD13				32			N/A			EIB2Battery1 Voltage					N/A		EIB2 Tensión Batería 1					N/A
48		ID_HEAD14				32			N/A			EIB2Battery1 Capacity					N/A		EIB2 Capacidad Batería 1				N/A
49		ID_HEAD15				32			N/A			EIB2Battery2 Current					N/A		EIB2 Corriente Batería 2				N/A
50		ID_HEAD16				32			N/A			EIB2Battery2 Voltage					N/A		EIB2 Tensión Batería 2					N/A
51		ID_HEAD17				32			N/A			EIB2Battery2 Capacity					N/A		EIB2 Capacidad Batería 2				N/A
52		ID_HEAD18				32			N/A			EIB3Battery1 Current					N/A		EIB3 Corriente Batería 1				N/A
53		ID_HEAD19				32			N/A			EIB3Battery1 Voltage					N/A		EIB3 Tensión Batería 1					N/A
54		ID_HEAD20				32			N/A			EIB3Battery1 Capacity					N/A		EIB3 Capacidad Batería 1				N/A
55		ID_HEAD21				32			N/A			EIB3Battery2 Current					N/A		EIB3 Corriente Batería 2				N/A
56		ID_HEAD22				32			N/A			EIB3Battery2 Voltage					N/A		EIB3 Tensión Batería 2					N/A
57		ID_HEAD23				32			N/A			EIB3Battery2 Capacity					N/A		EIB3 Capacidad Batería 2				N/A
58		ID_HEAD24				32			N/A			EIB4Battery1 Current					N/A		EIB4 Corriente Batería 1				N/A
59		ID_HEAD25				32			N/A			EIB4Battery1 Voltage					N/A		EIB4 Tensión Batería 1					N/A
60		ID_HEAD26				32			N/A			EIB4Battery1 Capacity					N/A		EIB4 Capacidad Batería 1				N/A
61		ID_HEAD27				32			N/A			EIB4Battery2 Current					N/A		EIB4 Corriente Batería 2				N/A
62		ID_HEAD28				32			N/A			EIB4Battery2 Voltage					N/A		EIB4 Tensión Batería 2					N/A
63		ID_HEAD29				32			N/A			EIB4Battery2 Capacity					N/A		EIB4 Capacidad Batería 2				N/A
64		ID_BATTERY_TEST_SUMMARY0		32			N/A			Index							N/A		Indice							N/A
65		ID_BATTERY_TEST_SUMMARY1		32			N/A			Record Time						N/A		Hora de registro					N/A
66		ID_BATTERY_TEST_SUMMARY2		32			N/A			System Voltage						N/A		Tensión del Sistema					N/A
67		ID_BATTERY_SUMMARY_HEAD0		32			N/A			Start Time						N/A		Hora de inicio						N/A
68		ID_BATTERY_SUMMARY_HEAD1		32			N/A			End Time						N/A		Hora fin						N/A
69		ID_BATTERY_SUMMARY_HEAD2		32			N/A			Start Reason						N/A		Razón inicio						N/A
70		ID_BATTERY_SUMMARY_HEAD3		32			N/A			End Reason						N/A		Razón fin						N/A
71		ID_BATTERY_SUMMARY_HEAD4		32			N/A			Test Results						N/A		Resultado prueba					N/A
72		ID_DOWNLOAD				32			N/A			Upload						N/A		Cargar						N/A
73		ID_HEAD30				32			N/A			SMDU1Battery1 Current							N/A		SMDU1 Corriente Batería 1				N/A
74		ID_HEAD31				32			N/A			SMDU1Battery1 Voltage							N/A		SMDU1 Tensión Batería 1					N/A
75		ID_HEAD32				32			N/A			SMDU1Battery1 Capacity						N/A		SMDU1 Capacidad Batería 1				N/A
76		ID_HEAD33				32			N/A			SMDU1Battery2 Current							N/A		SMDU1 Corriente Batería 2				N/A
77		ID_HEAD34				32			N/A			SMDU1Battery2 Voltage							N/A		SMDU1 Tensión Batería 2					N/A
78		ID_HEAD35				32			N/A			SMDU1Battery2 Capacity						N/A		SMDU1 Capacidad Batería 2				N/A
79		ID_HEAD36				32			N/A			SMDU1Battery3 Current							N/A		SMDU1 Corriente Batería 3				N/A
80		ID_HEAD37				32			N/A			SMDU1Battery3 Voltage							N/A		SMDU1 Tensión Batería 3					N/A
81		ID_HEAD38				32			N/A			SMDU1Battery3 Capacity						N/A		SMDU1 Capacidad Batería 3				N/A
82		ID_HEAD39				32			N/A			SMDU1Battery4 Current							N/A		SMDU1 Corriente Batería 4				N/A
83		ID_HEAD40				32			N/A			SMDU1Battery4 Voltage							N/A		SMDU1 Tensión Batería 4					N/A
84		ID_HEAD41				32			N/A			SMDU1Battery4 Capacity							N/A		SMDU1 Capacidad Batería 4				N/A
85		ID_HEAD42				32			N/A			SMDU2Battery1 Current							N/A		SMDU2 Corriente Batería 1				N/A
86		ID_HEAD43				32			N/A			SMDU2Battery1 Voltage							N/A		SMDU2 Tensión Batería 1					N/A
87		ID_HEAD44				32			N/A			SMDU2Battery1 Capacity							N/A		SMDU2 Capacidad Batería 1				N/A
88		ID_HEAD45				32			N/A			SMDU2Battery2 Current							N/A		SMDU2 Corriente Batería 2				N/A
89		ID_HEAD46				32			N/A			SMDU2Battery2 Voltage							N/A		SMDU2 Tensión Batería 2					N/A
90		ID_HEAD47				32			N/A			SMDU2Battery2 Capacity							N/A		SMDU2 Capacidad Batería 2				N/A
91		ID_HEAD48				32			N/A			SMDU2Battery3 Current							N/A		SMDU2 Corriente Batería 3				N/A
92		ID_HEAD49				32			N/A			SMDU2Battery3 Voltage							N/A		SMDU2 Tensión Batería 3					N/A
93		ID_HEAD50				32			N/A			SMDU2Battery3 Capacity							N/A		SMDU2 Capacidad Batería 3				N/A
94		ID_HEAD51				32			N/A			SMDU2Battery4 Current							N/A		SMDU2 Corriente Batería 4				N/A
95		ID_HEAD52				32			N/A			SMDU2Battery4 Voltage							N/A		SMDU2 Tensión Batería 4					N/A
96		ID_HEAD53				32			N/A			SMDU2Battery4 Capacity							N/A		SMDU2 Capacidad Batería 4				N/A
97		ID_HEAD54				32			N/A			SMDU3Battery1 Current							N/A		SMDU3 Corriente Batería 1				N/A
98		ID_HEAD55				32			N/A			SMDU3Battery1 Voltage							N/A		SMDU3 Tensión Batería 1					N/A
99		ID_HEAD56				32			N/A			SMDU3Battery1 Capacity							N/A		SMDU3 Capacidad Batería 1				N/A
100		ID_HEAD57				32			N/A			SMDU3Battery2 Current							N/A		SMDU3 Corriente Batería 2				N/A
101		ID_HEAD58				32			N/A			SMDU3Battery2 Voltage							N/A		SMDU3 Tensión Batería 2					N/A
102		ID_HEAD59				32			N/A			SMDU3Battery2 Capacity							N/A		SMDU3 Capacidad Batería 2				N/A
103		ID_HEAD60				32			N/A			SMDU3Battery3 Current							N/A		SMDU3 Corriente Batería 3				N/A
104		ID_HEAD61				32			N/A			SMDU3Battery3 Voltage							N/A		SMDU3 Tensión Batería 3					N/A
105		ID_HEAD62				32			N/A			SMDU3Battery3 Capacity							N/A		SMDU3 Capacidad Batería 3				N/A
106		ID_HEAD63				32			N/A			SMDU3Battery4 Current							N/A		SMDU3 Corriente Batería 4				N/A
107		ID_HEAD64				32			N/A			SMDU3Battery4 Voltage							N/A		SMDU3 Tensión Batería 4					N/A
108		ID_HEAD65				32			N/A			SMDU3Battery4 Capacity							N/A		SMDU3 Capacidad Batería 4				N/A
109		ID_HEAD66				32			N/A			SMDU4Battery1 Current							N/A		SMDU4 Corriente Batería 1				N/A
110		ID_HEAD67				32			N/A			SMDU4Battery1 Voltage							N/A		SMDU4 Tensión Batería 1					N/A
111		ID_HEAD68				32			N/A			SMDU4Battery1 Capacity							N/A		SMDU4 Capacidad Batería 1				N/A
112		ID_HEAD69				32			N/A			SMDU4Battery2 Current							N/A		SMDU4 Corriente Batería 2				N/A
113		ID_HEAD70				32			N/A			SMDU4Battery2 Voltage							N/A		SMDU4 Tensión Batería 2					N/A
114		ID_HEAD71				32			N/A			SMDU4Battery2 Capacity							N/A		SMDU4 Capacidad Batería 2				N/A
115		ID_HEAD72				32			N/A			SMDU4Battery3 Current							N/A		SMDU4 Corriente Batería 3				N/A
116		ID_HEAD73				32			N/A			SMDU4Battery3 Voltage							N/A		SMDU4 Tensión Batería 3					N/A
117		ID_HEAD74				32			N/A			SMDU4Battery3 Capacity							N/A		SMDU4 Capacidad Batería 3				N/A
118		ID_HEAD75				32			N/A			SMDU4Battery4 Current							N/A		SMDU4 Corriente Batería 4				N/A
119		ID_HEAD76				32			N/A			SMDU4Battery4 Voltage							N/A		SMDU4 Tensión Batería 4					N/A
120		ID_HEAD77				32			N/A			SMDU4Battery4 Capacity							N/A		SMDU4 Capacidad Batería 4				N/A
121		ID_HEAD78				32			N/A			SMDU5Battery1 Current							N/A		SMDU5 Corriente Batería 1				N/A
122		ID_HEAD79				32			N/A			SMDU5Battery1 Voltage							N/A		SMDU5 Tensión Batería 1					N/A
123		ID_HEAD80				32			N/A			SMDU5Battery1 Capacity							N/A		SMDU5 Capacidad Batería 1				N/A
124		ID_HEAD81				32			N/A			SMDU5Battery2 Current							N/A		SMDU5 Corriente Batería 2				N/A
125		ID_HEAD82				32			N/A			SMDU5Battery2 Voltage							N/A		SMDU5 Tensión Batería 2					N/A
126		ID_HEAD83				32			N/A			SMDU5Battery2 Capacity							N/A		SMDU5 Capacidad Batería 2				N/A
127		ID_HEAD84				32			N/A			SMDU5Battery3 Current							N/A		SMDU5 Corriente Batería 3				N/A
128		ID_HEAD85				32			N/A			SMDU5Battery3 Voltage							N/A		SMDU5 Tensión Batería 3					N/A
129		ID_HEAD86				32			N/A			SMDU5Battery3 Capacity							N/A		SMDU5 Capacidad Batería 3				N/A
130		ID_HEAD87				32			N/A			SMDU5Battery4 Current							N/A		SMDU5 Corriente Batería 4				N/A
131		ID_HEAD88				32			N/A			SMDU5Battery4 Voltage							N/A		SMDU5 Tensión Batería 4					N/A
132		ID_HEAD89				32			N/A			SMDU5Battery4 Capacity							N/A		SMDU5 Capacidad Batería 4				N/A
133		ID_HEAD90				32			N/A			SMDU6Battery1 Current							N/A		SMDU6 Corriente Batería 1				N/A
134		ID_HEAD91				32			N/A			SMDU6Battery1 Voltage							N/A		SMDU6 Tensión Batería 1					N/A
135		ID_HEAD92				32			N/A			SMDU6Battery1 Capacity							N/A		SMDU6 Capacidad Batería 1				N/A
136		ID_HEAD93				32			N/A			SMDU6Battery2 Current							N/A		SMDU6 Corriente Batería 2				N/A
137		ID_HEAD94				32			N/A			SMDU6Battery2 Voltage							N/A		SMDU6 Tensión Batería 2					N/A
138		ID_HEAD95				32			N/A			SMDU6Battery2 Capacity							N/A		SMDU6 Capacidad Batería 2				N/A
139		ID_HEAD96				32			N/A			SMDU6Battery3 Current							N/A		SMDU6 Corriente Batería 3				N/A
140		ID_HEAD97				32			N/A			SMDU6Battery3 Voltage							N/A		SMDU6 Tensión Batería 3					N/A
141		ID_HEAD98				32			N/A			SMDU6Battery3 Capacity							N/A		SMDU6 Capacidad Batería 3				N/A
142		ID_HEAD99				32			N/A			SMDU6Battery4 Current							N/A		SMDU6 Corriente Batería 4				N/A
143		ID_HEAD100				32			N/A			SMDU6Battery4 Voltage							N/A		SMDU6 Tensión Batería 4					N/A
144		ID_HEAD101				32			N/A			SMDU6Battery4 Capacity							N/A		SMDU6 Capacidad Batería 4				N/A
145		ID_HEAD102				32			N/A			SMDU7Battery1 Current							N/A		SMDU7 Corriente Batería 1				N/A
146		ID_HEAD103				32			N/A			SMDU7Battery1 Voltage							N/A		SMDU7 Tensión Batería 1					N/A
147		ID_HEAD104				32			N/A			SMDU7Battery1 Capacity							N/A		SMDU7 Capacidad Batería 1				N/A
148		ID_HEAD105				32			N/A			SMDU7Battery2 Current							N/A		SMDU7 Corriente Batería 2				N/A
149		ID_HEAD106				32			N/A			SMDU7Battery2 Voltage							N/A		SMDU7 Tensión Batería 2					N/A
150		ID_HEAD107				32			N/A				SMDU7Battery2 Capacity						N/A		SMDU7 Capacidad Batería 2				N/A
151		ID_HEAD108				32			N/A				SMDU7Battery3 Current						N/A		SMDU7 Corriente Batería 3				N/A
152		ID_HEAD109				32			N/A				SMDU7Battery3 Voltage						N/A		SMDU7 Tensión Batería 3					N/A
153		ID_HEAD110				32			N/A				SMDU7Battery3 Capacity						N/A		SMDU7 Capacidad Batería 3				N/A
154		ID_HEAD111				32			N/A				SMDU7Battery4 Current						N/A		SMDU7 Corriente Batería 4				N/A
155		ID_HEAD112				32			N/A				SMDU7Battery4 Voltage						N/A		SMDU7 Tensión Batería 4					N/A
156		ID_HEAD113				32			N/A				SMDU7Battery4 Capacity						N/A		SMDU7 Capacidad Batería 4				N/A
157		ID_HEAD114				32			N/A				SMDU8Battery1 Current						N/A		SMDU8 Corriente Batería 1				N/A
158		ID_HEAD115				32			N/A				SMDU8Battery1 Voltage						N/A		SMDU8 Tensión Batería 1					N/A
159		ID_HEAD116				32			N/A				SMDU8Battery1 Capacity						N/A		SMDU8 Capacidad Batería 1				N/A
160		ID_HEAD117				32			N/A				SMDU8Battery2 Current						N/A		SMDU8 Corriente Batería 2				N/A
161		ID_HEAD118				32			N/A				SMDU8Battery2 Voltage						N/A		SMDU8 Tensión Batería 2					N/A
162		ID_HEAD119				32			N/A				SMDU8Battery2 Capacity						N/A		SMDU8 Capacidad Batería 2				N/A
163		ID_HEAD120				32			N/A				SMDU8Battery3 Current						N/A		SMDU8 Corriente Batería 3				N/A
164		ID_HEAD121				32			N/A				SMDU8Battery3 Voltage						N/A		SMDU8 Tensión Batería 3					N/A
165		ID_HEAD122				32			N/A				SMDU8Battery3 Capacity						N/A		SMDU8 Capacidad Batería 3				N/A
166		ID_HEAD123				32			N/A				SMDU8Battery4 Current						N/A		SMDU8 Corriente Batería 4				N/A
167		ID_HEAD124				32			N/A				SMDU8Battery4 Voltage						N/A		SMDU8 Tensión Batería 4					N/A
168		ID_HEAD125				32			N/A				SMDU8Battery4 Capacity						N/A		SMDU8 Capacidad Batería 4				N/A
169		ID_HEAD126				32			N/A				EIB1Battery3 Current						N/A		EIB1 Corriente Batería 3				N/A
170		ID_HEAD127				32			N/A				EIB1Battery3 Voltage						N/A		EIB1 Tensión Batería 3					N/A
171		ID_HEAD128				32			N/A				EIB1Battery3 Capacity						N/A		EIB1 Capacidad Batería 3				N/A
172		ID_HEAD129				32			N/A				EIB2Battery3 Current						N/A		EIB2 Corriente Batería 3				N/A
173		ID_HEAD130				32			N/A				EIB2Battery3 Voltage						N/A		EIB2 Tensión Batería 3					N/A
174		ID_HEAD131				32			N/A				EIB2Battery3 Capacity						N/A		EIB2 Capacidad Batería 3				N/A
175		ID_HEAD132				32			N/A				EIB3Battery3 Current						N/A		EIB3 Corriente Batería 3				N/A
176		ID_HEAD133				32			N/A				EIB3Battery3 Voltage						N/A		EIB3 Tensión Batería 3					N/A
177		ID_HEAD134				32			N/A				EIB3Battery3 Capacity						N/A		EIB3 Capacidad Batería 3				N/A
178		ID_HEAD135				32			N/A				EIB4Battery3 Current						N/A		EIB4 Corriente Batería 3				N/A
179		ID_HEAD136				32			N/A				EIB4Battery3 Voltage						N/A		EIB4 Tensión Batería 3					N/A
180		ID_HEAD137				32			N/A				EIB4Battery3 Capacity						N/A		EIB4 Capacidad Batería 3				N/A
181		ID_HEAD138				32			N/A				EIB1Block1Voltage							N/A		EIB1 Tensión de celda 1					N/A
182		ID_HEAD139				32			N/A				EIB1Block2Voltage							N/A		EIB1 Tensión de celda 2					N/A
183		ID_HEAD140				32			N/A				EIB1Block3Voltage							N/A		EIB1 Tensión de celda 3					N/A
184		ID_HEAD141				32			N/A				EIB1Block4Voltage							N/A		EIB1 Tensión de celda 4					N/A
185		ID_HEAD142				32			N/A				EIB1Block5Voltage							N/A		EIB1 Tensión de celda 5					N/A
186		ID_HEAD143				32			N/A				EIB1Block6Voltage							N/A		EIB1 Tensión de celda 6					N/A
187		ID_HEAD144				32			N/A				EIB1Block7Voltage							N/A		EIB1 Tensión de celda 7					N/A
188		ID_HEAD145				32			N/A				EIB1Block8Voltage							N/A		EIB1 Tensión de celda 8					N/A
189		ID_HEAD146				32			N/A				EIB2Block1Voltage							N/A		EIB2 Tensión de celda 1					N/A
190		ID_HEAD147				32			N/A				EIB2Block2Voltage							N/A		EIB2 Tensión de celda 2					N/A
191		ID_HEAD148				32			N/A				EIB2Block3Voltage							N/A		EIB2 Tensión de celda 3					N/A
192		ID_HEAD149				32			N/A				EIB2Block4Voltage							N/A		EIB2 Tensión de celda 4					N/A
193		ID_HEAD150				32			N/A				EIB2Block5Voltage							N/A		EIB2 Tensión de celda 5					N/A
194		ID_HEAD151				32			N/A				EIB2Block6Voltage							N/A		EIB2 Tensión de celda 6					N/A
195		ID_HEAD152				32			N/A				EIB2Block7Voltage							N/A		EIB2 Tensión de celda 7					N/A
196		ID_HEAD153				32			N/A				EIB2Block8Voltage							N/A		EIB2 Tensión de celda 8					N/A
197		ID_HEAD154				32			N/A				EIB3Block1Voltage							N/A		EIB3 Tensión de celda 1					N/A
198		ID_HEAD155				32			N/A				EIB3Block2Voltage							N/A		EIB3 Tensión de celda 2					N/A
199		ID_HEAD156				32			N/A				EIB3Block3Voltage							N/A		EIB3 Tensión de celda 3					N/A
200		ID_HEAD157				32			N/A				EIB3Block4Voltage							N/A		EIB3 Tensión de celda 4					N/A
201		ID_HEAD158				32			N/A				EIB3Block5Voltage							N/A		EIB3 Tensión de celda 5					N/A
202		ID_HEAD159				32			N/A				EIB3Block6Voltage							N/A		EIB3 Tensión de celda 6					N/A
203		ID_HEAD160				32			N/A				EIB3Block7Voltage							N/A		EIB3 Tensión de celda 7					N/A
204		ID_HEAD161				32			N/A				EIB3Block8Voltage							N/A		EIB3 Tensión de celda 8					N/A
205		ID_HEAD162				32			N/A				EIB4Block1Voltage							N/A		EIB4 Tensión de celda 1					N/A
206		ID_HEAD163				32			N/A				EIB4Block2Voltage							N/A		EIB4 Tensión de celda 2					N/A
207		ID_HEAD164				32			N/A				EIB4Block3Voltage							N/A		EIB4 Tensión de celda 3					N/A
208		ID_HEAD165				32			N/A				EIB4Block4Voltage							N/A		EIB4 Tensión de celda 4					N/A
209		ID_HEAD166				32			N/A				EIB4Block5Voltage							N/A		EIB4 Tensión de celda 5					N/A
210		ID_HEAD167				32			N/A				EIB4Block6Voltage							N/A		EIB4 Tensión de celda 6					N/A
211		ID_HEAD168				32			N/A				EIB4Block7Voltage							N/A		EIB4 Tensión de celda 7					N/A
212		ID_HEAD169				32			N/A				EIB4Block8Voltage							N/A		EIB4 Tensión de celda 8					N/A
213		ID_HEAD170				32			N/A				Temperature1							N/A		Temperatura 1						N/A
214		ID_HEAD171				32			N/A				Temperature2							N/A		Temperatura 2						N/A
215		ID_HEAD172				32			N/A				Temperature3							N/A		Temperatura 3						N/A
216		ID_HEAD173				32			N/A				Temperature4							N/A		Temperatura 4						N/A
217		ID_HEAD174				32			N/A				Temperature5							N/A		Temperatura 5						N/A
218		ID_HEAD175				32			N/A				Temperature6							N/A		Temperatura 6						N/A
219		ID_HEAD176				32			N/A				Temperature7							N/A		Temperatura 7						N/A
220		ID_HEAD177				32			N/A				Battery1 Current						N/A		Corriente Batería 1					N/A
221		ID_HEAD178				32			N/A				Battery1 Voltage						N/A		Tensión Batería 1					N/A
222		ID_HEAD179				32			N/A				Battery1 Capacity						N/A		Capaciad Batería 1					N/A
223		ID_HEAD180				32			N/A			LargeDUBattery1 Current				N/A		UD Grande Corriente Bat 1				N/A
224		ID_HEAD181				32			N/A			LargeDUBattery1 Voltage				N/A		UD Grande Tensión Bat 1					N/A
225		ID_HEAD182				32			N/A			LargeDUBattery1 Capacity			N/A		UD Grande Capacidad Bat 1				N/A
226		ID_HEAD183				32			N/A			LargeDUBattery2 Current				N/A		UD Grande Corriente Bat 2				N/A
227		ID_HEAD184				32			N/A			LargeDUBattery2 Voltage				N/A		UD Grande Tensión Bat 2					N/A
228		ID_HEAD185				32			N/A			LargeDUBattery2 Capacity			N/A		UD Grande Capacidad Bat 2				N/A
229		ID_HEAD186				32			N/A			LargeDUBattery3 Current				N/A		UD Grande Corriente Bat 3				N/A
230		ID_HEAD187				32			N/A			LargeDUBattery3 Voltage				N/A		UD Grande Tensión Bat 3					N/A
231		ID_HEAD188				32			N/A			LargeDUBattery3 Capacity			N/A		UD Grande Capacidad Bat 3				N/A
232		ID_HEAD189				32			N/A			LargeDUBattery4 Current				N/A		UD Grande Corriente Bat 4				N/A
233		ID_HEAD190				32			N/A			LargeDUBattery4 Voltage				N/A		UD Grande Tensión Bat 4					N/A
234		ID_HEAD191				32			N/A			LargeDUBattery4 Capacity			N/A		UD Grande Capacidad Bat 4				N/A
235		ID_HEAD192				32			N/A			LargeDUBattery5 Current				N/A		UD Grande Corriente Bat 5				N/A
236		ID_HEAD193				32			N/A			LargeDUBattery5 Voltage				N/A		UD Grande Tensión Bat 5					N/A
237		ID_HEAD194				32			N/A			LargeDUBattery5 Capacity			N/A		UD Grande Capacidad Bat 5				N/A
238		ID_HEAD195				32			N/A			LargeDUBattery6 Current				N/A		UD Grande Corriente Bat 6				N/A
239		ID_HEAD196				32			N/A			LargeDUBattery6 Voltage				N/A		UD Grande Tensión Bat 6					N/A
240		ID_HEAD197				32			N/A			LargeDUBattery6 Capacity			N/A		UD Grande Capacidad Bat 6				N/A
241		ID_HEAD198				32			N/A			LargeDUBattery7 Current				N/A		UD Grande Corriente Bat 7				N/A
242		ID_HEAD199				32			N/A			LargeDUBattery7 Voltage				N/A		UD Grande Tensión Bat 7					N/A
243		ID_HEAD200				32			N/A			LargeDUBattery7 Capacity			N/A		UD Grande Capacidad Bat 7				N/A
244		ID_HEAD201				32			N/A			LargeDUBattery8 Current				N/A		UD Grande Corriente Bat 8				N/A
245		ID_HEAD202				32			N/A			LargeDUBattery8 Voltage				N/A		UD Grande Tensión Bat 8					N/A
246		ID_HEAD203				32			N/A			LargeDUBattery8 Capacity			N/A		UD Grande Capacidad Bat 8				N/A
247		ID_HEAD204				32			N/A			LargeDUBattery9 Current				N/A		UD Grande Corriente Bat 9				N/A
248		ID_HEAD205				32			N/A			LargeDUBattery9 Voltage				N/A		UD Grande Tensión Bat 9					N/A
249		ID_HEAD206				32			N/A			LargeDUBattery9 Capacity			N/A		UD Grande Capacidad Bat 9				N/A
250		ID_HEAD207				32			N/A			LargeDUBattery10 Current			N/A		UD Grande Corriente Bat 10				N/A
251		ID_HEAD208				32			N/A			LargeDUBattery10 Voltage			N/A		UD Grande Tensión Bat 10				N/A
252		ID_HEAD209				32			N/A			LargeDUBattery10 Capacity				N/A		UD Grande Capacidad Bat 10				N/A
253		ID_HEAD210				32			N/A			LargeDUBattery11 Current			N/A		UD Grande Corriente Bat 11				N/A
254		ID_HEAD211				32			N/A			LargeDUBattery11 Voltage			N/A		UD Grande Tensión Bat 11				N/A
255		ID_HEAD212				32			N/A			LargeDUBattery11 Capacity				N/A		UD Grande Capacidad Bat 11				N/A
256		ID_HEAD213				32			N/A			LargeDUBattery12 Current			N/A		UD Grande Corriente Bat 12				N/A
257		ID_HEAD214				32			N/A			LargeDUBattery12 Voltage			N/A		UD Grande Tensión Bat 12				N/A
258		ID_HEAD215				32			N/A			LargeDUBattery12 Capacity				N/A		UD Grande Capacidad Bat 12				N/A
259		ID_HEAD216				32			N/A			LargeDUBattery13 Current			N/A		UD Grande Corriente Bat 13				N/A
260		ID_HEAD217				32			N/A			LargeDUBattery13 Voltage			N/A		UD Grande Tensión Bat 13				N/A
261		ID_HEAD218				32			N/A			LargeDUBattery13 Capacity				N/A		UD Grande Capacidad Bat 13				N/A
262		ID_HEAD219				32			N/A			LargeDUBattery14 Current			N/A		UD Grande Corriente Bat 14				N/A
263		ID_HEAD220				32			N/A			LargeDUBattery14 Voltage			N/A		UD Grande Tensión Bat 14				N/A
264		ID_HEAD221				32			N/A			LargeDUBattery14 Capacity				N/A		UD Grande Capacidad Bat 14				N/A
265		ID_HEAD222				32			N/A			LargeDUBattery15 Current			N/A		UD Grande Corriente Bat 15				N/A
266		ID_HEAD223				32			N/A			LargeDUBattery15 Voltage			N/A		UD Grande Tensión Bat 15				N/A
267		ID_HEAD224				32			N/A			LargeDUBattery15 Capacity				N/A		UD Grande Capacidad Bat 15				N/A
268		ID_HEAD225				32			N/A			LargeDUBattery16 Current			N/A		UD Grande Corriente Bat 16				N/A
269		ID_HEAD226				32			N/A			LargeDUBattery16 Voltage			N/A		UD Grande Tensión Bat 16				N/A
270		ID_HEAD227				32			N/A			LargeDUBattery16 Capacity				N/A		UD Grande Capacidad Bat 16				N/A
271		ID_HEAD228				32			N/A			LargeDUBattery17 Current			N/A		UD Grande Corriente Bat 17				N/A
272		ID_HEAD229				32			N/A			LargeDUBattery17 Voltage			N/A		UD Grande Tensión Bat 17				N/A
273		ID_HEAD230				32			N/A			LargeDUBattery17 Capacity				N/A		UD Grande Capacidad Bat 17				N/A
274		ID_HEAD231				32			N/A			LargeDUBattery18 Current			N/A		UD Grande Corriente Bat 18				N/A
275		ID_HEAD232				32			N/A			LargeDUBattery18 Voltage			N/A		UD Grande Tensión Bat 18				N/A
276		ID_HEAD233				32			N/A			LargeDUBattery18 Capacity				N/A		UD Grande Capacidad Bat 18				N/A
277		ID_HEAD234				32			N/A			LargeDUBattery19 Current			N/A		UD Grande Corriente Bat 19				N/A
278		ID_HEAD235				32			N/A			LargeDUBattery19 Voltage			N/A		UD Grande Tensión Bat 19				N/A
279		ID_HEAD236				32			N/A			LargeDUBattery19 Capacity				N/A		UD Grande Capacidad Bat 19				N/A
280		ID_HEAD237				32			N/A			LargeDUBattery20 Current			N/A		UD Grande Corriente Bat 20				N/A
281		ID_HEAD238				32			N/A			LargeDUBattery20 Voltage			N/A		UD Grande Tensión Bat 20				N/A
282		ID_HEAD239				32			N/A			LargeDUBattery20 Capacity				N/A		UD Grande Capacidad Bat 20				N/A
283		ID_HEAD240				32			N/A			Temperature8						N/A		Temperatura 8						N/A
284		ID_HEAD241				32			N/A			Temperature9						N/A		Temperatura 9						N/A
285		ID_HEAD242				32			N/A			Temperature10						N/A		Temperatura 10						N/A
286		ID_HEAD243				32			N/A			SMBattery1 Current				N/A		Corriente SMBAT 1					N/A
287		ID_HEAD244				32			N/A			SMBattery1 Voltage				N/A		Tensión SMBAT 1						N/A
288		ID_HEAD245				32			N/A			SMBattery1 Capacity					N/A		Capacidad SMBAT 1					N/A
289		ID_HEAD246				32			N/A			SMBattery2 Current				N/A		Corriente SMBAT 2					N/A
290		ID_HEAD247				32			N/A			SMBattery2 Voltage				N/A		Tensión SMBAT 2						N/A
291		ID_HEAD248				32			N/A			SMBattery2 Capacity					N/A		Capacidad SMBAT 2					N/A
292		ID_HEAD249				32			N/A			SMBattery3 Current				N/A		Corriente SMBAT 3					N/A
293		ID_HEAD250				32			N/A			SMBattery3 Voltage				N/A		Tensión SMBAT 3						N/A
294		ID_HEAD251				32			N/A			SMBattery3 Capacity					N/A		Capacidad SMBAT 3					N/A
295		ID_HEAD252				32			N/A			SMBattery4 Current				N/A		Corriente SMBAT 4					N/A
296		ID_HEAD253				32			N/A			SMBattery4 Voltage				N/A		Tensión SMBAT 4						N/A
297		ID_HEAD254				32			N/A			SMBattery4 Capacity					N/A		Capacidad SMBAT 4					N/A
298		ID_HEAD255				32			N/A			SMBattery5 Current				N/A		Corriente SMBAT 5					N/A
299		ID_HEAD256				32			N/A			SMBattery5 Voltage				N/A		Tensión SMBAT 5						N/A
300		ID_HEAD257				32			N/A			SMBattery5 Capacity					N/A		Capacidad SMBAT 5					N/A
301		ID_HEAD258				32			N/A			SMBattery6 Current				N/A		Corriente SMBAT 6					N/A
302		ID_HEAD259				32			N/A			SMBattery6 Voltage				N/A		Tensión SMBAT 6						N/A
303		ID_HEAD260				32			N/A			SMBattery6 Capacity					N/A		Capacidad SMBAT 6					N/A
304		ID_HEAD261				32			N/A			SMBattery7 Current				N/A		Corriente SMBAT 7					N/A
305		ID_HEAD262				32			N/A			SMBattery7 Voltage				N/A		Tensión SMBAT 7						N/A
306		ID_HEAD263				32			N/A			SMBattery7 Capacity					N/A		Capacidad SMBAT 7					N/A
307		ID_HEAD264				32			N/A			SMBattery8 Current				N/A		Corriente SMBAT 8					N/A
308		ID_HEAD265				32			N/A			SMBattery8 Voltage				N/A		Tensión SMBAT 8						N/A
309		ID_HEAD266				32			N/A			SMBattery8 Capacity					N/A		Capacidad SMBAT 8					N/A
310		ID_HEAD267				32			N/A			SMBattery9 Current				N/A		Corriente SMBAT 9					N/A
311		ID_HEAD268				32			N/A			SMBattery9 Voltage				N/A		Tensión SMBAT 9						N/A
312		ID_HEAD269				32			N/A			SMBattery9 Capacity					N/A		Capacidad SMBAT 9					N/A
313		ID_HEAD270				32			N/A			SMBattery10 Current					N/A		Corriente SMBAT 10					N/A
314		ID_HEAD271				32			N/A			SMBattery10 Voltage					N/A		Tensión SMBAT 10					N/A
315		ID_HEAD272				32			N/A			SMBattery10 Capacity					N/A		Capacidad SMBAT 10					N/A
316		ID_HEAD273				32			N/A			SMBattery11 Current					N/A		Corriente SMBAT 11					N/A
317		ID_HEAD274				32			N/A			SMBattery11 Voltage					N/A		Tensión SMBAT 11					N/A
318		ID_HEAD275				32			N/A			SMBattery11 Capacity					N/A		Capacidad SMBAT 11					N/A
319		ID_HEAD276				32			N/A			SMBattery12 Current					N/A		Corriente SMBAT 12					N/A
320		ID_HEAD277				32			N/A			SMBattery12 Voltage					N/A		Tensión SMBAT 12					N/A
321		ID_HEAD278				32			N/A			SMBattery12 Capacity					N/A		Capacidad SMBAT 12					N/A
322		ID_HEAD279				32			N/A			SMBattery13 Current					N/A		Corriente SMBAT 13					N/A
323		ID_HEAD280				32			N/A			SMBattery13 Voltage					N/A		Tensión SMBAT 13					N/A
324		ID_HEAD281				32			N/A			SMBattery13 Capacity					N/A		Capacidad SMBAT 13					N/A
325		ID_HEAD282				32			N/A			SMBattery14 Current					N/A		Corriente SMBAT 14					N/A
326		ID_HEAD283				32			N/A			SMBattery14 Voltage					N/A		Tensión SMBAT 14					N/A
327		ID_HEAD284				32			N/A			SMBattery14 Capacity					N/A		Capacidad SMBAT 14					N/A
328		ID_HEAD285				32			N/A			SMBattery15 Current					N/A		Corriente SMBAT 15					N/A
329		ID_HEAD286				32			N/A			SMBattery15 Voltage					N/A		Tensión SMBAT 15					N/A
330		ID_HEAD287				32			N/A			SMBattery15 Capacity					N/A		Capacidad SMBAT 15					N/A
331		ID_HEAD288				32			N/A			SMBattery16 Current					N/A		Corriente SMBAT 16					N/A
332		ID_HEAD289				32			N/A			SMBattery16 Voltage					N/A		Tensión SMBAT 16					N/A
333		ID_HEAD290				32			N/A			SMBattery16 Capacity					N/A		Capacidad SMBAT 16					N/A
334		ID_HEAD291				32			N/A			SMBattery17 Current					N/A		Corriente SMBAT 17					N/A
335		ID_HEAD292				32			N/A			SMBattery17 Voltage					N/A		Tensión SMBAT 17					N/A
336		ID_HEAD293				32			N/A			SMBattery17 Capacity					N/A		Capacidad SMBAT 17					N/A
337		ID_HEAD294				32			N/A			SMBattery18 Current					N/A		Corriente SMBAT 18					N/A
338		ID_HEAD295				32			N/A			SMBattery18 Voltage					N/A		Tensión SMBAT 18					N/A
339		ID_HEAD296				32			N/A			SMBattery18 Capacity					N/A		Capacidad SMBAT 18					N/A
340		ID_HEAD297				32			N/A			SMBattery19 Current					N/A		Corriente SMBAT 19					N/A
341		ID_HEAD298				32			N/A			SMBattery19 Voltage					N/A		Tensión SMBAT 19					N/A
342		ID_HEAD299				32			N/A			SMBattery19 Capacity					N/A		Capacidad SMBAT 19					N/A
343		ID_HEAD300				32			N/A			SMBattery20 Current					N/A		Corriente SMBAT 20					N/A
344		ID_HEAD301				32			N/A			SMBattery20 Voltage					N/A		Tensión SMBAT 20					N/A
345		ID_HEAD302				32			N/A			SMBattery20 Capacity					N/A		Capacidad SMBAT 20					N/A
346		ID_HEAD303				32			N/A			SMDU1Battery5 Current				N/A		SMDU1 Corriente Batería 5				N/A
347		ID_HEAD304				32			N/A			SMDU1Battery5 Voltage				N/A		SMDU1 Tensión Batería 5					N/A
348		ID_HEAD305				32			N/A			SMDU1Battery5 Capacity				N/A		SMDU1 Capacidad Batería 5				N/A
349		ID_HEAD306				32			N/A			SMDU2Battery5 Current				N/A		SMDU2 Corriente Batería 5				N/A
350		ID_HEAD307				32			N/A			SMDU2Battery5 Voltage				N/A		SMDU2 Tensión Batería 5					N/A
351		ID_HEAD308				32			N/A			SMDU2Battery5 Capacity				N/A		SMDU2 Capacidad Batería 5				N/A
352		ID_HEAD309				32			N/A			SMDU3Battery5 Current				N/A		SMDU3 Corriente Batería 5				N/A
353		ID_HEAD310				32			N/A			SMDU3Battery5 Voltage				N/A		SMDU3 Tensión Batería 5					N/A
354		ID_HEAD311				32			N/A			SMDU3Battery5 Capacity				N/A		SMDU3 Capacidad Batería 5				N/A
355		ID_HEAD312				32			N/A			SMDU4Battery5 Current				N/A		SMDU4 Corriente Batería 5				N/A
356		ID_HEAD313				32			N/A			SMDU4Battery5 Voltage				N/A		SMDU4 Tensión Batería 5					N/A
357		ID_HEAD314				32			N/A			SMDU4Battery5 Capacity				N/A		SMDU4 Capacidad Batería 5				N/A
358		ID_HEAD315				32			N/A			SMDU5Battery5 Current				N/A		SMDU5 Corriente Batería 5				N/A
359		ID_HEAD316				32			N/A			SMDU5Battery5 Voltage				N/A		SMDU5 Tensión Batería 5					N/A
360		ID_HEAD317				32			N/A			SMDU5Battery5 Capacity				N/A		SMDU5 Capacidad Batería 5				N/A
361		ID_HEAD318				32			N/A			SMDU6Battery5 Current				N/A		SMDU6 Corriente Batería 5				N/A
362		ID_HEAD319				32			N/A			SMDU6Battery5 Voltage				N/A		SMDU6 Tensión Batería 5					N/A
363		ID_HEAD320				32			N/A			SMDU6Battery5 Capacity				N/A		SMDU6 Capacidad Batería 5				N/A
364		ID_HEAD321				32			N/A			SMDU7Battery5 Current				N/A		SMDU7 Corriente Batería 5				N/A
365		ID_HEAD322				32			N/A			SMDU7Battery5 Voltage				N/A		SMDU7 Tensión Batería 5					N/A
366		ID_HEAD323				32			N/A			SMDU7Battery5 Capacity				N/A		SMDU7 Capacidad Batería 5				N/A
367		ID_HEAD324				32			N/A			SMDU8Battery5 Current				N/A		SMDU8 Corriente Batería 5				N/A
368		ID_HEAD325				32			N/A			SMDU8Battery5 Voltage				N/A		SMDU8 Tensión Batería 5					N/A
369		ID_HEAD326				32			N/A			SMDU8Battery5 Capacity				N/A		SMDU8 Capacidad Batería 5				N/A
370		ID_HEAD327				32			N/A			SMBRCBattery1 Current				N/A		SMBRC Corriente Batería 1				N/A
371		ID_HEAD328				32			N/A			SMBRCBattery1 Voltage				N/A		SMBRC Tensión Batería 1					N/A
372		ID_HEAD329				32			N/A			SMBRCBattery1 Capacity				N/A		SMBRC Capacidad Batería 1				N/A
373		ID_HEAD330				32			N/A			SMBRCBattery2 Current				N/A		SMBRC Corriente Batería 2				N/A
374		ID_HEAD331				32			N/A			SMBRCBattery2 Voltage				N/A		SMBRC Tensión Batería 2					N/A
375		ID_HEAD332				32			N/A			SMBRCBattery2 Capacity				N/A		SMBRC Capacidad Batería 2				N/A
376		ID_HEAD333				32			N/A			SMBRCBattery3 Current				N/A		SMBRC Corriente Batería 3				N/A
377		ID_HEAD334				32			N/A			SMBRCBattery3 Voltage				N/A		SMBRC Tensión Batería 3					N/A
378		ID_HEAD335				32			N/A			SMBRCBattery3 Capacity				N/A		SMBRC Capacidad Batería 3				N/A
379		ID_HEAD336				32			N/A			SMBRCBattery4 Current				N/A		SMBRC Corriente Batería 4				N/A
380		ID_HEAD337				32			N/A			SMBRCBattery4 Voltage				N/A		SMBRC Tensión Batería 4					N/A
381		ID_HEAD338				32			N/A			SMBRCBattery4 Capacity				N/A		SMBRC Capacidad Batería 4				N/A
382		ID_HEAD339				32			N/A			SMBRCBattery5 Current				N/A		SMBRC Corriente Batería 5				N/A
383		ID_HEAD340				32			N/A			SMBRCBattery5 Voltage				N/A		SMBRC Tensión Batería 5					N/A
384		ID_HEAD341				32			N/A			SMBRCBattery5 Capacity				N/A		SMBRC Capacidad Batería 5				N/A
385		ID_HEAD342				32			N/A			SMBRCBattery6 Current				N/A		SMBRC Corriente Batería 6				N/A
386		ID_HEAD343				32			N/A			SMBRCBattery6 Voltage				N/A		SMBRC Tensión Batería 6					N/A
387		ID_HEAD344				32			N/A			SMBRCBattery6 Capacity				N/A		SMBRC Capacidad Batería 6				N/A
388		ID_HEAD345				32			N/A			SMBRCBattery7 Current				N/A		SMBRC Corriente Batería 7				N/A
389		ID_HEAD346				32			N/A			SMBRCBattery7 Voltage				N/A		SMBRC Tensión Batería 7					N/A
390		ID_HEAD347				32			N/A			SMBRCBattery7 Capacity				N/A		SMBRC Capacidad Batería 7				N/A
391		ID_HEAD348				32			N/A			SMBRCBattery8 Current				N/A		SMBRC Corriente Batería 8				N/A
392		ID_HEAD349				32			N/A			SMBRCBattery8 Voltage				N/A		SMBRC Tensión Batería 8					N/A
393		ID_HEAD350				32			N/A			SMBRCBattery8 Capacity				N/A		SMBRC Capacidad Batería 8				N/A
394		ID_HEAD351				32			N/A			SMBRCBattery9 Current				N/A		SMBRC Corriente Batería 9				N/A
395		ID_HEAD352				32			N/A			SMBRCBattery9 Voltage				N/A		SMBRC Tensión Batería 9					N/A
396		ID_HEAD353				32			N/A			SMBRCBattery9 Capacity				N/A		SMBRC Capacidad Batería 9				N/A
397		ID_HEAD354				32			N/A			SMBRCBattery10 Current				N/A		SMBRC Corriente Batería 10				N/A
398		ID_HEAD355				32			N/A			SMBRCBattery10 Voltage				N/A		SMBRC Tensión Batería 10				N/A
399		ID_HEAD356				32			N/A			SMBRCBattery10 Capacity				N/A		SMBRC Capacidad Batería 10				N/A
400		ID_HEAD357				32			N/A			SMBRCBattery11 Current				N/A		SMBRC Corriente Batería 11				N/A
401		ID_HEAD358				32			N/A			SMBRCBattery11 Voltage				N/A		SMBRC Tensión Batería 11				N/A
402		ID_HEAD359				32			N/A			SMBRCBattery11 Capacity				N/A		SMBRC Capacidad Batería 11				N/A
403		ID_HEAD360				32			N/A			SMBRCBattery12 Current				N/A		SMBRC Corriente Batería 12				N/A
404		ID_HEAD361				32			N/A			SMBRCBattery12 Voltage				N/A		SMBRC Tensión Batería 12				N/A
405		ID_HEAD362				32			N/A			SMBRCBattery12 Capacity				N/A		SMBRC Capacidad Batería 12				N/A
406		ID_HEAD363				32			N/A			SMBRCBattery13 Current				N/A		SMBRC Corriente Batería 13				N/A
407		ID_HEAD364				32			N/A			SMBRCBattery13 Voltage				N/A		SMBRC Tensión Batería 13				N/A
408		ID_HEAD365				32			N/A			SMBRCBattery13 Capacity				N/A		SMBRC Capacidad Batería 13				N/A
409		ID_HEAD366				32			N/A			SMBRCBattery14 Current				N/A		SMBRC Corriente Batería 14				N/A
410		ID_HEAD367				32			N/A			SMBRCBattery14 Voltage				N/A		SMBRC Tensión Batería 14				N/A
411		ID_HEAD368				32			N/A			SMBRCBattery14 Capacity				N/A		SMBRC Capacidad Batería 14				N/A
412		ID_HEAD369				32			N/A			SMBRCBattery15 Current				N/A		SMBRC Corriente Batería 15				N/A
413		ID_HEAD370				32			N/A			SMBRCBattery15 Voltage				N/A		SMBRC Tensión Batería 15				N/A
414		ID_HEAD371				32			N/A			SMBRCBattery15 Capacity				N/A		SMBRC Capacidad Batería 15				N/A
415		ID_HEAD372				32			N/A			SMBRCBattery16 Current				N/A		SMBRC Corriente Batería 16				N/A
416		ID_HEAD373				32			N/A			SMBRCBattery16 Voltage				N/A		SMBRC Tensión Batería 16				N/A
417		ID_HEAD374				32			N/A			SMBRCBattery16 Capacity				N/A		SMBRC Capacidad Batería 16				N/A
418		ID_HEAD375				32			N/A			SMBRCBattery17 Current				N/A		SMBRC Corriente Batería 17				N/A
419		ID_HEAD376				32			N/A			SMBRCBattery17 Voltage				N/A		SMBRC Tensión Batería 17				N/A
420		ID_HEAD377				32			N/A			SMBRCBattery17 Capacity				N/A		SMBRC Capacidad Batería 17				N/A
421		ID_HEAD378				32			N/A			SMBRCBattery18 Current				N/A		SMBRC Corriente Batería 18				N/A
422		ID_HEAD379				32			N/A			SMBRCBattery18 Voltage				N/A		SMBRC Tensión Batería 18				N/A
423		ID_HEAD380				32			N/A			SMBRCBattery18 Capacity				N/A		SMBRC Capacidad Batería 18				N/A
424		ID_HEAD381				32			N/A			SMBRCBattery19 Current				N/A		SMBRC Corriente Batería 19				N/A
425		ID_HEAD382				32			N/A			SMBRCBattery19 Voltage				N/A		SMBRC Tensión Batería 19				N/A
426		ID_HEAD383				32			N/A			SMBRCBattery19 Capacity				N/A		SMBRC Capacidad Batería 19				N/A
427		ID_HEAD384				32			N/A			SMBRCBattery20 Current				N/A		SMBRC Corriente Batería 20				N/A
428		ID_HEAD385				32			N/A			SMBRCBattery20 Voltage				N/A		SMBRC Tensión Batería 20				N/A
429		ID_HEAD386				32			N/A			SMBRCBattery20 Capacity				N/A		SMBRC Capacidad Batería 20				N/A
430		ID_HEAD387				32			N/A			SMBAT/BRC1 BLOCK1  Voltage				N/A		SMBAT/BRC1 Tensión Celda 1				N/A
431		ID_HEAD388				32			N/A			SMBAT/BRC1 BLOCK2  Voltage				N/A		SMBAT/BRC1 Tensión Celda 2				N/A
432		ID_HEAD389				32			N/A			SMBAT/BRC1 BLOCK3  Voltage				N/A		SMBAT/BRC1 Tensión Celda 3				N/A
433		ID_HEAD390				32			N/A			SMBAT/BRC1 BLOCK4  Voltage				N/A		SMBAT/BRC1 Tensión Celda 4				N/A
434		ID_HEAD391				32			N/A			SMBAT/BRC1 BLOCK5  Voltage				N/A		SMBAT/BRC1 Tensión Celda 5				N/A
435		ID_HEAD392				32			N/A			SMBAT/BRC1 BLOCK6  Voltage				N/A		SMBAT/BRC1 Tensión Celda 6				N/A
436		ID_HEAD393				32			N/A			SMBAT/BRC1 BLOCK7  Voltage				N/A		SMBAT/BRC1 Tensión Celda 7				N/A
437		ID_HEAD394				32			N/A			SMBAT/BRC1 BLOCK8  Voltage				N/A		SMBAT/BRC1 Tensión Celda 8				N/A
438		ID_HEAD395				32			N/A			SMBAT/BRC1 BLOCK9  Voltage				N/A		SMBAT/BRC1 Tensión Celda 9				N/A
439		ID_HEAD396				32			N/A			SMBAT/BRC1 BLOCK10 Voltage				N/A		SMBAT/BRC1 Tensión Celda 10				N/A
440		ID_HEAD397				32			N/A			SMBAT/BRC1 BLOCK11 Voltage				N/A		SMBAT/BRC1 Tensión Celda 11				N/A
441		ID_HEAD398				32			N/A			SMBAT/BRC1 BLOCK12 Voltage				N/A		SMBAT/BRC1 Tensión Celda 12				N/A
442		ID_HEAD399				32			N/A			SMBAT/BRC1 BLOCK13 Voltage				N/A		SMBAT/BRC1 Tensión Celda 13				N/A
443		ID_HEAD400				32			N/A			SMBAT/BRC1 BLOCK14 Voltage				N/A		SMBAT/BRC1 Tensión Celda 14				N/A
444		ID_HEAD401				32			N/A			SMBAT/BRC1 BLOCK15 Voltage				N/A		SMBAT/BRC1 Tensión Celda 15				N/A
445		ID_HEAD402				32			N/A			SMBAT/BRC1 BLOCK16 Voltage				N/A		SMBAT/BRC1 Tensión Celda 16				N/A
446		ID_HEAD403				32			N/A			SMBAT/BRC1 BLOCK17 Voltage				N/A		SMBAT/BRC1 Tensión Celda 17				N/A
447		ID_HEAD404				32			N/A			SMBAT/BRC1 BLOCK18 Voltage				N/A		SMBAT/BRC1 Tensión Celda 18				N/A
448		ID_HEAD405				32			N/A			SMBAT/BRC1 BLOCK19 Voltage				N/A		SMBAT/BRC1 Tensión Celda 19				N/A
449		ID_HEAD406				32			N/A			SMBAT/BRC1 BLOCK20 Voltage				N/A		SMBAT/BRC1 Tensión Celda 20				N/A
450		ID_HEAD407				32			N/A			SMBAT/BRC1 BLOCK21 Voltage				N/A		SMBAT/BRC1 Tensión Celda 21				N/A
451		ID_HEAD408				32			N/A			SMBAT/BRC1 BLOCK22 Voltage				N/A		SMBAT/BRC1 Tensión Celda 22				N/A
452		ID_HEAD409				32			N/A			SMBAT/BRC1 BLOCK23 Voltage				N/A		SMBAT/BRC1 Tensión Celda 23				N/A
453		ID_HEAD410				32			N/A			SMBAT/BRC1 BLOCK24 Voltage				N/A		SMBAT/BRC1 Tensión Celda 24				N/A
454		ID_HEAD411				32			N/A			SMBAT/BRC2 BLOCK1  Voltage				N/A		SMBAT/BRC2 Tensión Celda 1				N/A
455		ID_HEAD412				32			N/A			SMBAT/BRC2 BLOCK2  Voltage				N/A		SMBAT/BRC2 Tensión Celda 2				N/A
456		ID_HEAD413				32			N/A			SMBAT/BRC2 BLOCK3  Voltage				N/A		SMBAT/BRC2 Tensión Celda 3				N/A
457		ID_HEAD414				32			N/A			SMBAT/BRC2 BLOCK4  Voltage				N/A		SMBAT/BRC2 Tensión Celda 4				N/A
458		ID_HEAD415				32			N/A			SMBAT/BRC2 BLOCK5  Voltage				N/A		SMBAT/BRC2 Tensión Celda 5				N/A
459		ID_HEAD416				32			N/A			SMBAT/BRC2 BLOCK6  Voltage				N/A		SMBAT/BRC2 Tensión Celda 6				N/A
460		ID_HEAD417				32			N/A			SMBAT/BRC2 BLOCK7  Voltage				N/A		SMBAT/BRC2 Tensión Celda 7				N/A
461		ID_HEAD418				32			N/A			SMBAT/BRC2 BLOCK8  Voltage				N/A		SMBAT/BRC2 Tensión Celda 8				N/A
462		ID_HEAD419				32			N/A			SMBAT/BRC2 BLOCK9  Voltage				N/A		SMBAT/BRC2 Tensión Celda 9				N/A
463		ID_HEAD420				32			N/A			SMBAT/BRC2 BLOCK10 Voltage				N/A		SMBAT/BRC2 Tensión Celda 10				N/A
464		ID_HEAD421				32			N/A			SMBAT/BRC2 BLOCK11 Voltage				N/A		SMBAT/BRC2 Tensión Celda 11				N/A
465		ID_HEAD422				32			N/A			SMBAT/BRC2 BLOCK12 Voltage				N/A		SMBAT/BRC2 Tensión Celda 12				N/A
466		ID_HEAD423				32			N/A			SMBAT/BRC2 BLOCK13 Voltage				N/A		SMBAT/BRC2 Tensión Celda 13				N/A
467		ID_HEAD424				32			N/A			SMBAT/BRC2 BLOCK14 Voltage				N/A		SMBAT/BRC2 Tensión Celda 14				N/A
468		ID_HEAD425				32			N/A			SMBAT/BRC2 BLOCK15 Voltage				N/A		SMBAT/BRC2 Tensión Celda 15				N/A
469		ID_HEAD426				32			N/A			SMBAT/BRC2 BLOCK16 Voltage				N/A		SMBAT/BRC2 Tensión Celda 16				N/A
470		ID_HEAD427				32			N/A			SMBAT/BRC2 BLOCK17 Voltage				N/A		SMBAT/BRC2 Tensión Celda 17				N/A
471		ID_HEAD428				32			N/A			SMBAT/BRC2 BLOCK18 Voltage				N/A		SMBAT/BRC2 Tensión Celda 18				N/A
472		ID_HEAD429				32			N/A			SMBAT/BRC2 BLOCK19 Voltage				N/A		SMBAT/BRC2 Tensión Celda 19				N/A
473		ID_HEAD430				32			N/A			SMBAT/BRC2 BLOCK20 Voltage				N/A		SMBAT/BRC2 Tensión Celda 20				N/A
474		ID_HEAD431				32			N/A			SMBAT/BRC2 BLOCK21 Voltage				N/A		SMBAT/BRC2 Tensión Celda 21				N/A
475		ID_HEAD432				32			N/A			SMBAT/BRC2 BLOCK22 Voltage				N/A		SMBAT/BRC2 Tensión Celda 22				N/A
476		ID_HEAD433				32			N/A			SMBAT/BRC2 BLOCK23 Voltage				N/A		SMBAT/BRC2 Tensión Celda 23				N/A
477		ID_HEAD434				32			N/A			SMBAT/BRC2 BLOCK24 Voltage				N/A		SMBAT/BRC2 Tensión Celda 24				N/A
478		ID_HEAD435				32			N/A			SMBAT/BRC3 BLOCK1  Voltage				N/A		SMBAT/BRC3 Tensión Celda 1				N/A
479		ID_HEAD436				32			N/A			SMBAT/BRC3 BLOCK2  Voltage				N/A		SMBAT/BRC3 Tensión Celda 2				N/A
480		ID_HEAD437				32			N/A			SMBAT/BRC3 BLOCK3  Voltage				N/A		SMBAT/BRC3 Tensión Celda 3				N/A
481		ID_HEAD438				32			N/A			SMBAT/BRC3 BLOCK4  Voltage				N/A		SMBAT/BRC3 Tensión Celda 4				N/A
482		ID_HEAD439				32			N/A			SMBAT/BRC3 BLOCK5  Voltage				N/A		SMBAT/BRC3 Tensión Celda 5				N/A
483		ID_HEAD440				32			N/A			SMBAT/BRC3 BLOCK6  Voltage				N/A		SMBAT/BRC3 Tensión Celda 6				N/A
484		ID_HEAD441				32			N/A			SMBAT/BRC3 BLOCK7  Voltage				N/A		SMBAT/BRC3 Tensión Celda 7				N/A
485		ID_HEAD442				32			N/A			SMBAT/BRC3 BLOCK8  Voltage				N/A		SMBAT/BRC3 Tensión Celda 8				N/A
486		ID_HEAD443				32			N/A			SMBAT/BRC3 BLOCK9  Voltage				N/A		SMBAT/BRC3 Tensión Celda 9				N/A
487		ID_HEAD444				32			N/A			SMBAT/BRC3 BLOCK10 Voltage				N/A		SMBAT/BRC3 Tensión Celda 10				N/A
488		ID_HEAD445				32			N/A			SMBAT/BRC3 BLOCK11 Voltage				N/A		SMBAT/BRC3 Tensión Celda 11				N/A
489		ID_HEAD446				32			N/A			SMBAT/BRC3 BLOCK12 Voltage				N/A		SMBAT/BRC3 Tensión Celda 12				N/A
490		ID_HEAD447				32			N/A			SMBAT/BRC3 BLOCK13 Voltage				N/A		SMBAT/BRC3 Tensión Celda 13				N/A
491		ID_HEAD448				32			N/A			SMBAT/BRC3 BLOCK14 Voltage				N/A		SMBAT/BRC3 Tensión Celda 14				N/A
492		ID_HEAD449				32			N/A			SMBAT/BRC3 BLOCK15 Voltage				N/A		SMBAT/BRC3 Tensión Celda 15				N/A
493		ID_HEAD450				32			N/A			SMBAT/BRC3 BLOCK16 Voltage				N/A		SMBAT/BRC3 Tensión Celda 16				N/A
494		ID_HEAD451				32			N/A			SMBAT/BRC3 BLOCK17 Voltage				N/A		SMBAT/BRC3 Tensión Celda 17				N/A
495		ID_HEAD452				32			N/A			SMBAT/BRC3 BLOCK18 Voltage				N/A		SMBAT/BRC3 Tensión Celda 18				N/A
496		ID_HEAD453				32			N/A			SMBAT/BRC3 BLOCK19 Voltage				N/A		SMBAT/BRC3 Tensión Celda 19				N/A
497		ID_HEAD454				32			N/A			SMBAT/BRC3 BLOCK20 Voltage				N/A		SMBAT/BRC3 Tensión Celda 20				N/A
498		ID_HEAD455				32			N/A			SMBAT/BRC3 BLOCK21 Voltage				N/A		SMBAT/BRC3 Tensión Celda 21				N/A
499		ID_HEAD456				32			N/A			SMBAT/BRC3 BLOCK22 Voltage				N/A		SMBAT/BRC3 Tensión Celda 22				N/A
500		ID_HEAD457				32			N/A			SMBAT/BRC3 BLOCK23 Voltage				N/A		SMBAT/BRC3 Tensión Celda 23				N/A
501		ID_HEAD458				32			N/A			SMBAT/BRC3 BLOCK24 Voltage				N/A		SMBAT/BRC3 Tensión Celda 24				N/A
502		ID_HEAD459				32			N/A			SMBAT/BRC4 BLOCK1  Voltage				N/A		SMBAT/BRC4 Tensión Celda 1				N/A
503		ID_HEAD460				32			N/A			SMBAT/BRC4 BLOCK2  Voltage				N/A		SMBAT/BRC4 Tensión Celda 2				N/A
504		ID_HEAD461				32			N/A			SMBAT/BRC4 BLOCK3  Voltage				N/A		SMBAT/BRC4 Tensión Celda 3				N/A
505		ID_HEAD462				32			N/A			SMBAT/BRC4 BLOCK4  Voltage				N/A		SMBAT/BRC4 Tensión Celda 4				N/A
506		ID_HEAD463				32			N/A			SMBAT/BRC4 BLOCK5  Voltage				N/A		SMBAT/BRC4 Tensión Celda 5				N/A
507		ID_HEAD464				32			N/A			SMBAT/BRC4 BLOCK6  Voltage				N/A		SMBAT/BRC4 Tensión Celda 6				N/A
508		ID_HEAD465				32			N/A			SMBAT/BRC4 BLOCK7  Voltage				N/A		SMBAT/BRC4 Tensión Celda 7				N/A
509		ID_HEAD466				32			N/A			SMBAT/BRC4 BLOCK8  Voltage				N/A		SMBAT/BRC4 Tensión Celda 8				N/A
510		ID_HEAD467				32			N/A			SMBAT/BRC4 BLOCK9  Voltage				N/A		SMBAT/BRC4 Tensión Celda 9				N/A
511		ID_HEAD468				32			N/A			SMBAT/BRC4 BLOCK10 Voltage				N/A		SMBAT/BRC4 Tensión Celda 10				N/A
512		ID_HEAD469				32			N/A			SMBAT/BRC4 BLOCK11 Voltage				N/A		SMBAT/BRC4 Tensión Celda 11				N/A
513		ID_HEAD470				32			N/A			SMBAT/BRC4 BLOCK12 Voltage				N/A		SMBAT/BRC4 Tensión Celda 12				N/A
514		ID_HEAD471				32			N/A			SMBAT/BRC4 BLOCK13 Voltage				N/A		SMBAT/BRC4 Tensión Celda 13				N/A
515		ID_HEAD472				32			N/A			SMBAT/BRC4 BLOCK14 Voltage				N/A		SMBAT/BRC4 Tensión Celda 14				N/A
516		ID_HEAD473				32			N/A			SMBAT/BRC4 BLOCK15 Voltage				N/A		SMBAT/BRC4 Tensión Celda 15				N/A
517		ID_HEAD474				32			N/A			SMBAT/BRC4 BLOCK16 Voltage				N/A		SMBAT/BRC4 Tensión Celda 16				N/A
518		ID_HEAD475				32			N/A			SMBAT/BRC4 BLOCK17 Voltage				N/A		SMBAT/BRC4 Tensión Celda 17				N/A
519		ID_HEAD476				32			N/A			SMBAT/BRC4 BLOCK18 Voltage				N/A		SMBAT/BRC4 Tensión Celda 18				N/A
520		ID_HEAD477				32			N/A			SMBAT/BRC4 BLOCK19 Voltage				N/A		SMBAT/BRC4 Tensión Celda 19				N/A
521		ID_HEAD478				32			N/A			SMBAT/BRC4 BLOCK20 Voltage				N/A		SMBAT/BRC4 Tensión Celda 20				N/A
522		ID_HEAD479				32			N/A			SMBAT/BRC4 BLOCK21 Voltage				N/A		SMBAT/BRC4 Tensión Celda 21				N/A
523		ID_HEAD480				32			N/A			SMBAT/BRC4 BLOCK22 Voltage				N/A		SMBAT/BRC4 Tensión Celda 22				N/A
524		ID_HEAD481				32			N/A			SMBAT/BRC4 BLOCK23 Voltage				N/A		SMBAT/BRC4 Tensión Celda 23				N/A
525		ID_HEAD482				32			N/A			SMBAT/BRC4 BLOCK24 Voltage				N/A		SMBAT/BRC4 Tensión Celda 24				N/A
526		ID_HEAD483				32			N/A			SMBAT/BRC5 BLOCK1  Voltage				N/A		SMBAT/BRC5 Tensión Celda 1				N/A
527		ID_HEAD484				32			N/A			SMBAT/BRC5 BLOCK2  Voltage				N/A		SMBAT/BRC5 Tensión Celda 2				N/A
528		ID_HEAD485				32			N/A			SMBAT/BRC5 BLOCK3  Voltage				N/A		SMBAT/BRC5 Tensión Celda 3				N/A
529		ID_HEAD486				32			N/A			SMBAT/BRC5 BLOCK4  Voltage				N/A		SMBAT/BRC5 Tensión Celda 4				N/A
530		ID_HEAD487				32			N/A			SMBAT/BRC5 BLOCK5  Voltage				N/A		SMBAT/BRC5 Tensión Celda 5				N/A
531		ID_HEAD488				32			N/A			SMBAT/BRC5 BLOCK6  Voltage				N/A		SMBAT/BRC5 Tensión Celda 6				N/A
532		ID_HEAD489				32			N/A			SMBAT/BRC5 BLOCK7  Voltage				N/A		SMBAT/BRC5 Tensión Celda 7				N/A
533		ID_HEAD490				32			N/A			SMBAT/BRC5 BLOCK8  Voltage				N/A		SMBAT/BRC5 Tensión Celda 8				N/A
534		ID_HEAD491				32			N/A			SMBAT/BRC5 BLOCK9  Voltage				N/A		SMBAT/BRC5 Tensión Celda 9				N/A
535		ID_HEAD492				32			N/A			SMBAT/BRC5 BLOCK10 Voltage				N/A		SMBAT/BRC5 Tensión Celda 10				N/A
536		ID_HEAD493				32			N/A			SMBAT/BRC5 BLOCK11 Voltage				N/A		SMBAT/BRC5 Tensión Celda 11				N/A
537		ID_HEAD494				32			N/A			SMBAT/BRC5 BLOCK12 Voltage				N/A		SMBAT/BRC5 Tensión Celda 12				N/A
538		ID_HEAD495				32			N/A			SMBAT/BRC5 BLOCK13 Voltage				N/A		SMBAT/BRC5 Tensión Celda 13				N/A
539		ID_HEAD496				32			N/A			SMBAT/BRC5 BLOCK14 Voltage				N/A		SMBAT/BRC5 Tensión Celda 14				N/A
540		ID_HEAD497				32			N/A			SMBAT/BRC5 BLOCK15 Voltage				N/A		SMBAT/BRC5 Tensión Celda 15				N/A
541		ID_HEAD498				32			N/A			SMBAT/BRC5 BLOCK16 Voltage				N/A		SMBAT/BRC5 Tensión Celda 16				N/A
542		ID_HEAD499				32			N/A			SMBAT/BRC5 BLOCK17 Voltage				N/A		SMBAT/BRC5 Tensión Celda 17				N/A
543		ID_HEAD500				32			N/A			SMBAT/BRC5 BLOCK18 Voltage				N/A		SMBAT/BRC5 Tensión Celda 18				N/A
544		ID_HEAD501				32			N/A			SMBAT/BRC5 BLOCK19 Voltage				N/A		SMBAT/BRC5 Tensión Celda 19				N/A
545		ID_HEAD502				32			N/A			SMBAT/BRC5 BLOCK20 Voltage				N/A		SMBAT/BRC5 Tensión Celda 20				N/A
546		ID_HEAD503				32			N/A			SMBAT/BRC5 BLOCK21 Voltage				N/A		SMBAT/BRC5 Tensión Celda 21				N/A
547		ID_HEAD504				32			N/A			SMBAT/BRC5 BLOCK22 Voltage				N/A		SMBAT/BRC5 Tensión Celda 22				N/A
548		ID_HEAD505				32			N/A			SMBAT/BRC5 BLOCK23 Voltage				N/A		SMBAT/BRC5 Tensión Celda 23				N/A
549		ID_HEAD506				32			N/A			SMBAT/BRC5 BLOCK24 Voltage				N/A		SMBAT/BRC5 Tensión Celda 24				N/A
550		ID_HEAD507				32			N/A			SMBAT/BRC6 BLOCK1  Voltage				N/A		SMBAT/BRC6 Tensión Celda 1				N/A
551		ID_HEAD508				32			N/A			SMBAT/BRC6 BLOCK2  Voltage				N/A		SMBAT/BRC6 Tensión Celda 2				N/A
552		ID_HEAD509				32			N/A			SMBAT/BRC6 BLOCK3  Voltage				N/A		SMBAT/BRC6 Tensión Celda 3				N/A
553		ID_HEAD510				32			N/A			SMBAT/BRC6 BLOCK4  Voltage				N/A		SMBAT/BRC6 Tensión Celda 4				N/A
554		ID_HEAD511				32			N/A			SMBAT/BRC6 BLOCK5  Voltage				N/A		SMBAT/BRC6 Tensión Celda 5				N/A
555		ID_HEAD512				32			N/A			SMBAT/BRC6 BLOCK6  Voltage				N/A		SMBAT/BRC6 Tensión Celda 6				N/A
556		ID_HEAD513				32			N/A			SMBAT/BRC6 BLOCK7  Voltage				N/A		SMBAT/BRC6 Tensión Celda 7				N/A
557		ID_HEAD514				32			N/A			SMBAT/BRC6 BLOCK8  Voltage				N/A		SMBAT/BRC6 Tensión Celda 8				N/A
558		ID_HEAD515				32			N/A			SMBAT/BRC6 BLOCK9  Voltage				N/A		SMBAT/BRC6 Tensión Celda 9				N/A
559		ID_HEAD516				32			N/A			SMBAT/BRC6 BLOCK10 Voltage				N/A		SMBAT/BRC6 Tensión Celda 10				N/A
560		ID_HEAD517				32			N/A			SMBAT/BRC6 BLOCK11 Voltage				N/A		SMBAT/BRC6 Tensión Celda 11				N/A
561		ID_HEAD518				32			N/A			SMBAT/BRC6 BLOCK12 Voltage				N/A		SMBAT/BRC6 Tensión Celda 12				N/A
562		ID_HEAD519				32			N/A			SMBAT/BRC6 BLOCK13 Voltage				N/A		SMBAT/BRC6 Tensión Celda 13				N/A
563		ID_HEAD520				32			N/A			SMBAT/BRC6 BLOCK14 Voltage				N/A		SMBAT/BRC6 Tensión Celda 14				N/A
564		ID_HEAD521				32			N/A			SMBAT/BRC6 BLOCK15 Voltage				N/A		SMBAT/BRC6 Tensión Celda 15				N/A
565		ID_HEAD522				32			N/A			SMBAT/BRC6 BLOCK16 Voltage				N/A		SMBAT/BRC6 Tensión Celda 16				N/A
566		ID_HEAD523				32			N/A			SMBAT/BRC6 BLOCK17 Voltage				N/A		SMBAT/BRC6 Tensión Celda 17				N/A
567		ID_HEAD524				32			N/A			SMBAT/BRC6 BLOCK18 Voltage				N/A		SMBAT/BRC6 Tensión Celda 18				N/A
568		ID_HEAD525				32			N/A			SMBAT/BRC6 BLOCK19 Voltage				N/A		SMBAT/BRC6 Tensión Celda 19				N/A
569		ID_HEAD526				32			N/A			SMBAT/BRC6 BLOCK20 Voltage				N/A		SMBAT/BRC6 Tensión Celda 20				N/A
570		ID_HEAD527				32			N/A			SMBAT/BRC6 BLOCK21 Voltage				N/A		SMBAT/BRC6 Tensión Celda 21				N/A
571		ID_HEAD528				32			N/A			SMBAT/BRC6 BLOCK22 Voltage				N/A		SMBAT/BRC6 Tensión Celda 22				N/A
572		ID_HEAD529				32			N/A			SMBAT/BRC6 BLOCK23 Voltage				N/A		SMBAT/BRC6 Tensión Celda 23				N/A
573		ID_HEAD530				32			N/A			SMBAT/BRC6 BLOCK24 Voltage				N/A		SMBAT/BRC6 Tensión Celda 24				N/A
574		ID_HEAD531				32			N/A			SMBAT/BRC7 BLOCK1  Voltage				N/A		SMBAT/BRC7 Tensión Celda 1				N/A
575		ID_HEAD532				32			N/A			SMBAT/BRC7 BLOCK2  Voltage				N/A		SMBAT/BRC7 Tensión Celda 2				N/A
576		ID_HEAD533				32			N/A			SMBAT/BRC7 BLOCK3  Voltage				N/A		SMBAT/BRC7 Tensión Celda 3				N/A
577		ID_HEAD534				32			N/A			SMBAT/BRC7 BLOCK4  Voltage				N/A		SMBAT/BRC7 Tensión Celda 4				N/A
578		ID_HEAD535				32			N/A			SMBAT/BRC7 BLOCK5  Voltage				N/A		SMBAT/BRC7 Tensión Celda 5				N/A
579		ID_HEAD536				32			N/A			SMBAT/BRC7 BLOCK6  Voltage				N/A		SMBAT/BRC7 Tensión Celda 6				N/A
580		ID_HEAD537				32			N/A			SMBAT/BRC7 BLOCK7  Voltage				N/A		SMBAT/BRC7 Tensión Celda 7				N/A
581		ID_HEAD538				32			N/A			SMBAT/BRC7 BLOCK8  Voltage				N/A		SMBAT/BRC7 Tensión Celda 8				N/A
582		ID_HEAD539				32			N/A			SMBAT/BRC7 BLOCK9  Voltage				N/A		SMBAT/BRC7 Tensión Celda 9				N/A
583		ID_HEAD540				32			N/A			SMBAT/BRC7 BLOCK10 Voltage				N/A		SMBAT/BRC7 Tensión Celda 10				N/A
584		ID_HEAD541				32			N/A			SMBAT/BRC7 BLOCK11 Voltage				N/A		SMBAT/BRC7 Tensión Celda 11				N/A
585		ID_HEAD542				32			N/A			SMBAT/BRC7 BLOCK12 Voltage				N/A		SMBAT/BRC7 Tensión Celda 12				N/A
586		ID_HEAD543				32			N/A			SMBAT/BRC7 BLOCK13 Voltage				N/A		SMBAT/BRC7 Tensión Celda 13				N/A
587		ID_HEAD544				32			N/A			SMBAT/BRC7 BLOCK14 Voltage				N/A		SMBAT/BRC7 Tensión Celda 14				N/A
588		ID_HEAD545				32			N/A			SMBAT/BRC7 BLOCK15 Voltage				N/A		SMBAT/BRC7 Tensión Celda 15				N/A
589		ID_HEAD546				32			N/A			SMBAT/BRC7 BLOCK16 Voltage				N/A		SMBAT/BRC7 Tensión Celda 16				N/A
590		ID_HEAD547				32			N/A			SMBAT/BRC7 BLOCK17 Voltage				N/A		SMBAT/BRC7 Tensión Celda 17				N/A
591		ID_HEAD548				32			N/A			SMBAT/BRC7 BLOCK18 Voltage				N/A		SMBAT/BRC7 Tensión Celda 18				N/A
592		ID_HEAD549				32			N/A			SMBAT/BRC7 BLOCK19 Voltage				N/A		SMBAT/BRC7 Tensión Celda 19				N/A
593		ID_HEAD550				32			N/A			SMBAT/BRC7 BLOCK20 Voltage				N/A		SMBAT/BRC7 Tensión Celda 20				N/A
594		ID_HEAD551				32			N/A			SMBAT/BRC7 BLOCK21 Voltage				N/A		SMBAT/BRC7 Tensión Celda 21				N/A
595		ID_HEAD552				32			N/A			SMBAT/BRC7 BLOCK22 Voltage				N/A		SMBAT/BRC7 Tensión Celda 22				N/A
596		ID_HEAD553				32			N/A			SMBAT/BRC7 BLOCK23 Voltage				N/A		SMBAT/BRC7 Tensión Celda 23				N/A
597		ID_HEAD554				32			N/A			SMBAT/BRC7 BLOCK24 Voltage				N/A		SMBAT/BRC7 Tensión Celda 24				N/A
598		ID_HEAD555				32			N/A			SMBAT/BRC8 BLOCK1  Voltage				N/A		SMBAT/BRC8 Tensión Celda 1				N/A
599		ID_HEAD556				32			N/A			SMBAT/BRC8 BLOCK2  Voltage				N/A		SMBAT/BRC8 Tensión Celda 2				N/A
600		ID_HEAD557				32			N/A			SMBAT/BRC8 BLOCK3  Voltage				N/A		SMBAT/BRC8 Tensión Celda 3				N/A
601		ID_HEAD558				32			N/A			SMBAT/BRC8 BLOCK4  Voltage				N/A		SMBAT/BRC8 Tensión Celda 4				N/A
602		ID_HEAD559				32			N/A			SMBAT/BRC8 BLOCK5  Voltage				N/A		SMBAT/BRC8 Tensión Celda 5				N/A
603		ID_HEAD560				32			N/A			SMBAT/BRC8 BLOCK6  Voltage				N/A		SMBAT/BRC8 Tensión Celda 6				N/A
604		ID_HEAD561				32			N/A			SMBAT/BRC8 BLOCK7  Voltage				N/A		SMBAT/BRC8 Tensión Celda 7				N/A
605		ID_HEAD562				32			N/A			SMBAT/BRC8 BLOCK8  Voltage				N/A		SMBAT/BRC8 Tensión Celda 8				N/A
606		ID_HEAD563				32			N/A			SMBAT/BRC8 BLOCK9  Voltage				N/A		SMBAT/BRC8 Tensión Celda 9				N/A
607		ID_HEAD564				32			N/A			SMBAT/BRC8 BLOCK10 Voltage				N/A		SMBAT/BRC8 Tensión Celda 10				N/A
608		ID_HEAD565				32			N/A			SMBAT/BRC8 BLOCK11 Voltage				N/A		SMBAT/BRC8 Tensión Celda 11				N/A
609		ID_HEAD566				32			N/A			SMBAT/BRC8 BLOCK12 Voltage				N/A		SMBAT/BRC8 Tensión Celda 12				N/A
610		ID_HEAD567				32			N/A			SMBAT/BRC8 BLOCK13 Voltage				N/A		SMBAT/BRC8 Tensión Celda 13				N/A
611		ID_HEAD568				32			N/A			SMBAT/BRC8 BLOCK14 Voltage				N/A		SMBAT/BRC8 Tensión Celda 14				N/A
612		ID_HEAD569				32			N/A			SMBAT/BRC8 BLOCK15 Voltage				N/A		SMBAT/BRC8 Tensión Celda 15				N/A
613		ID_HEAD570				32			N/A			SMBAT/BRC8 BLOCK16 Voltage				N/A		SMBAT/BRC8 Tensión Celda 16				N/A
614		ID_HEAD571				32			N/A			SMBAT/BRC8 BLOCK17 Voltage				N/A		SMBAT/BRC8 Tensión Celda 17				N/A
615		ID_HEAD572				32			N/A			SMBAT/BRC8 BLOCK18 Voltage				N/A		SMBAT/BRC8 Tensión Celda 18				N/A
616		ID_HEAD573				32			N/A			SMBAT/BRC8 BLOCK19 Voltage				N/A		SMBAT/BRC8 Tensión Celda 19				N/A
617		ID_HEAD574				32			N/A			SMBAT/BRC8 BLOCK20 Voltage				N/A		SMBAT/BRC8 Tensión Celda 20				N/A
618		ID_HEAD575				32			N/A			SMBAT/BRC8 BLOCK21 Voltage				N/A		SMBAT/BRC8 Tensión Celda 21				N/A
619		ID_HEAD576				32			N/A			SMBAT/BRC8 BLOCK22 Voltage				N/A		SMBAT/BRC8 Tensión Celda 22				N/A
620		ID_HEAD577				32			N/A			SMBAT/BRC8 BLOCK23 Voltage				N/A		SMBAT/BRC8 Tensión Celda 23				N/A
621		ID_HEAD578				32			N/A			SMBAT/BRC8 BLOCK24 Voltage				N/A		SMBAT/BRC8 Tensión Celda 24				N/A
622		ID_HEAD579				32			N/A			SMBAT/BRC9 BLOCK1  Voltage				N/A		SMBAT/BRC9 Tensión Celda 1				N/A
623		ID_HEAD580				32			N/A			SMBAT/BRC9 BLOCK2  Voltage				N/A		SMBAT/BRC9 Tensión Celda 2				N/A
624		ID_HEAD581				32			N/A			SMBAT/BRC9 BLOCK3  Voltage				N/A		SMBAT/BRC9 Tensión Celda 3				N/A
625		ID_HEAD582				32			N/A			SMBAT/BRC9 BLOCK4  Voltage				N/A		SMBAT/BRC9 Tensión Celda 4				N/A
626		ID_HEAD583				32			N/A			SMBAT/BRC9 BLOCK5  Voltage				N/A		SMBAT/BRC9 Tensión Celda 5				N/A
627		ID_HEAD584				32			N/A			SMBAT/BRC9 BLOCK6  Voltage				N/A		SMBAT/BRC9 Tensión Celda 6				N/A
628		ID_HEAD585				32			N/A			SMBAT/BRC9 BLOCK7  Voltage				N/A		SMBAT/BRC9 Tensión Celda 7				N/A
629		ID_HEAD586				32			N/A			SMBAT/BRC9 BLOCK8  Voltage				N/A		SMBAT/BRC9 Tensión Celda 8				N/A
630		ID_HEAD587				32			N/A			SMBAT/BRC9 BLOCK9  Voltage				N/A		SMBAT/BRC9 Tensión Celda 9				N/A
631		ID_HEAD588				32			N/A			SMBAT/BRC9 BLOCK10 Voltage				N/A		SMBAT/BRC9 Tensión Celda 10				N/A
632		ID_HEAD589				32			N/A			SMBAT/BRC9 BLOCK11 Voltage				N/A		SMBAT/BRC9 Tensión Celda 11				N/A
633		ID_HEAD590				32			N/A			SMBAT/BRC9 BLOCK12 Voltage				N/A		SMBAT/BRC9 Tensión Celda 12				N/A
634		ID_HEAD591				32			N/A			SMBAT/BRC9 BLOCK13 Voltage				N/A		SMBAT/BRC9 Tensión Celda 13				N/A
635		ID_HEAD592				32			N/A			SMBAT/BRC9 BLOCK14 Voltage				N/A		SMBAT/BRC9 Tensión Celda 14				N/A
636		ID_HEAD593				32			N/A			SMBAT/BRC9 BLOCK15 Voltage				N/A		SMBAT/BRC9 Tensión Celda 15				N/A
637		ID_HEAD594				32			N/A			SMBAT/BRC9 BLOCK16 Voltage				N/A		SMBAT/BRC9 Tensión Celda 16				N/A
638		ID_HEAD595				32			N/A			SMBAT/BRC9 BLOCK17 Voltage				N/A		SMBAT/BRC9 Tensión Celda 17				N/A
639		ID_HEAD596				32			N/A			SMBAT/BRC9 BLOCK18 Voltage				N/A		SMBAT/BRC9 Tensión Celda 18				N/A
640		ID_HEAD597				32			N/A			SMBAT/BRC9 BLOCK19 Voltage				N/A		SMBAT/BRC9 Tensión Celda 19				N/A
641		ID_HEAD598				32			N/A			SMBAT/BRC9 BLOCK20 Voltage				N/A		SMBAT/BRC9 Tensión Celda 20				N/A
642		ID_HEAD599				32			N/A			SMBAT/BRC9 BLOCK21 Voltage				N/A		SMBAT/BRC9 Tensión Celda 21				N/A
643		ID_HEAD600				32			N/A			SMBAT/BRC9 BLOCK22 Voltage				N/A		SMBAT/BRC9 Tensión Celda 22				N/A
644		ID_HEAD601				32			N/A			SMBAT/BRC9 BLOCK23 Voltage				N/A		SMBAT/BRC9 Tensión Celda 23				N/A
645		ID_HEAD602				32			N/A			SMBAT/BRC9 BLOCK24 Voltage				N/A		SMBAT/BRC9 Tensión Celda 24				N/A
646		ID_HEAD603				32			N/A			SMBAT/BRC10 BLOCK1  Voltage				N/A		SMBAT/BRC10 Tensión Celda 1				N/A
647		ID_HEAD604				32			N/A			SMBAT/BRC10 BLOCK2  Voltage				N/A		SMBAT/BRC10 Tensión Celda 2				N/A
648		ID_HEAD605				32			N/A			SMBAT/BRC10 BLOCK3  Voltage				N/A		SMBAT/BRC10 Tensión Celda 3				N/A
649		ID_HEAD606				32			N/A			SMBAT/BRC10 BLOCK4  Voltage				N/A		SMBAT/BRC10 Tensión Celda 4				N/A
650		ID_HEAD607				32			N/A			SMBAT/BRC10 BLOCK5  Voltage				N/A		SMBAT/BRC10 Tensión Celda 5				N/A
651		ID_HEAD608				32			N/A			SMBAT/BRC10 BLOCK6  Voltage				N/A		SMBAT/BRC10 Tensión Celda 6				N/A
652		ID_HEAD609				32			N/A			SMBAT/BRC10 BLOCK7  Voltage				N/A		SMBAT/BRC10 Tensión Celda 7				N/A
653		ID_HEAD610				32			N/A			SMBAT/BRC10 BLOCK8  Voltage				N/A		SMBAT/BRC10 Tensión Celda 8				N/A
654		ID_HEAD611				32			N/A			SMBAT/BRC10 BLOCK9  Voltage				N/A		SMBAT/BRC10 Tensión Celda 9				N/A
655		ID_HEAD612				32			N/A			SMBAT/BRC10 BLOCK10 Voltage				N/A		SMBAT/BRC10 Tensión Celda 10				N/A
656		ID_HEAD613				32			N/A			SMBAT/BRC10 BLOCK11 Voltage				N/A		SMBAT/BRC10 Tensión Celda 11				N/A
657		ID_HEAD614				32			N/A			SMBAT/BRC10 BLOCK12 Voltage				N/A		SMBAT/BRC10 Tensión Celda 12				N/A
658		ID_HEAD615				32			N/A			SMBAT/BRC10 BLOCK13 Voltage				N/A		SMBAT/BRC10 Tensión Celda 13				N/A
659		ID_HEAD616				32			N/A			SMBAT/BRC10 BLOCK14 Voltage				N/A		SMBAT/BRC10 Tensión Celda 14				N/A
660		ID_HEAD617				32			N/A			SMBAT/BRC10 BLOCK15 Voltage				N/A		SMBAT/BRC10 Tensión Celda 15				N/A
661		ID_HEAD618				32			N/A			SMBAT/BRC10 BLOCK16 Voltage				N/A		SMBAT/BRC10 Tensión Celda 16				N/A
662		ID_HEAD619				32			N/A			SMBAT/BRC10 BLOCK17 Voltage				N/A		SMBAT/BRC10 Tensión Celda 17				N/A
663		ID_HEAD620				32			N/A			SMBAT/BRC10 BLOCK18 Voltage				N/A		SMBAT/BRC10 Tensión Celda 18				N/A
664		ID_HEAD621				32			N/A			SMBAT/BRC10 BLOCK19 Voltage				N/A		SMBAT/BRC10 Tensión Celda 19				N/A
665		ID_HEAD622				32			N/A			SMBAT/BRC10 BLOCK20 Voltage				N/A		SMBAT/BRC10 Tensión Celda 20				N/A
666		ID_HEAD623				32			N/A			SMBAT/BRC10 BLOCK21 Voltage				N/A		SMBAT/BRC10 Tensión Celda 21				N/A
667		ID_HEAD624				32			N/A			SMBAT/BRC10 BLOCK22 Voltage				N/A		SMBAT/BRC10 Tensión Celda 22				N/A
668		ID_HEAD625				32			N/A			SMBAT/BRC10 BLOCK23 Voltage				N/A		SMBAT/BRC10 Tensión Celda 23				N/A
669		ID_HEAD626				32			N/A			SMBAT/BRC10 BLOCK24 Voltage				N/A		SMBAT/BRC10 Tensión Celda 24				N/A
670		ID_HEAD627			32			N/A			SMBAT/BRC11 BLOCK1  Voltage					N/A		SMBAT/BRC11 Tensión Celda 1				N/A
671		ID_HEAD628			32			N/A			SMBAT/BRC11 BLOCK2  Voltage					N/A		SMBAT/BRC11 Tensión Celda 2				N/A
672		ID_HEAD629			32			N/A			SMBAT/BRC11 BLOCK3  Voltage					N/A		SMBAT/BRC11 Tensión Celda 3				N/A
673		ID_HEAD630			32			N/A			SMBAT/BRC11 BLOCK4  Voltage					N/A		SMBAT/BRC11 Tensión Celda 4				N/A
674		ID_HEAD631			32			N/A			SMBAT/BRC11 BLOCK5  Voltage					N/A		SMBAT/BRC11 Tensión Celda 5				N/A
675		ID_HEAD632			32			N/A			SMBAT/BRC11 BLOCK6  Voltage					N/A		SMBAT/BRC11 Tensión Celda 6				N/A
676		ID_HEAD633			32			N/A			SMBAT/BRC11 BLOCK7  Voltage					N/A		SMBAT/BRC11 Tensión Celda 7				N/A
677		ID_HEAD634			32			N/A			SMBAT/BRC11 BLOCK8  Voltage					N/A		SMBAT/BRC11 Tensión Celda 8				N/A
678		ID_HEAD635			32			N/A			SMBAT/BRC11 BLOCK9  Voltage					N/A		SMBAT/BRC11 Tensión Celda 9				N/A
679		ID_HEAD636			32			N/A			SMBAT/BRC11 BLOCK10 Voltage					N/A		SMBAT/BRC11 Tensión Celda 10				N/A
680		ID_HEAD637			32			N/A			SMBAT/BRC11 BLOCK11 Voltage					N/A		SMBAT/BRC11 Tensión Celda 11				N/A
681		ID_HEAD638			32			N/A			SMBAT/BRC11 BLOCK12 Voltage					N/A		SMBAT/BRC11 Tensión Celda 12				N/A
682		ID_HEAD639			32			N/A			SMBAT/BRC11 BLOCK13 Voltage					N/A		SMBAT/BRC11 Tensión Celda 13				N/A
683		ID_HEAD640			32			N/A			SMBAT/BRC11 BLOCK14 Voltage					N/A		SMBAT/BRC11 Tensión Celda 14				N/A
684		ID_HEAD641			32			N/A			SMBAT/BRC11 BLOCK15 Voltage					N/A		SMBAT/BRC11 Tensión Celda 15				N/A
685		ID_HEAD642			32			N/A			SMBAT/BRC11 BLOCK16 Voltage					N/A		SMBAT/BRC11 Tensión Celda 16				N/A
686		ID_HEAD643			32			N/A			SMBAT/BRC11 BLOCK17 Voltage					N/A		SMBAT/BRC11 Tensión Celda 17				N/A
687		ID_HEAD644			32			N/A			SMBAT/BRC11 BLOCK18 Voltage					N/A		SMBAT/BRC11 Tensión Celda 18				N/A
688		ID_HEAD645			32			N/A			SMBAT/BRC11 BLOCK19 Voltage					N/A		SMBAT/BRC11 Tensión Celda 19				N/A
689		ID_HEAD646			32			N/A			SMBAT/BRC11 BLOCK20 Voltage					N/A		SMBAT/BRC11 Tensión Celda 20				N/A
690		ID_HEAD647			32			N/A			SMBAT/BRC11 BLOCK21 Voltage					N/A		SMBAT/BRC11 Tensión Celda 21				N/A
691		ID_HEAD648			32			N/A			SMBAT/BRC11 BLOCK22 Voltage					N/A		SMBAT/BRC11 Tensión Celda 22				N/A
692		ID_HEAD649			32			N/A			SMBAT/BRC11 BLOCK23 Voltage					N/A		SMBAT/BRC11 Tensión Celda 23				N/A
693		ID_HEAD650			32			N/A			SMBAT/BRC11 BLOCK24 Voltage					N/A		SMBAT/BRC11 Tensión Celda 24				N/A
694		ID_HEAD651			32			N/A			SMBAT/BRC12 BLOCK1  Voltage					N/A		SMBAT/BRC12 Tensión Celda 1				N/A
695		ID_HEAD652			32			N/A			SMBAT/BRC12 BLOCK2  Voltage					N/A		SMBAT/BRC12 Tensión Celda 2				N/A
696		ID_HEAD653			32			N/A			SMBAT/BRC12 BLOCK3  Voltage					N/A		SMBAT/BRC12 Tensión Celda 3				N/A
697		ID_HEAD654			32			N/A			SMBAT/BRC12 BLOCK4  Voltage					N/A		SMBAT/BRC12 Tensión Celda 4				N/A
698		ID_HEAD655			32			N/A			SMBAT/BRC12 BLOCK5  Voltage					N/A		SMBAT/BRC12 Tensión Celda 5				N/A
699		ID_HEAD656			32			N/A			SMBAT/BRC12 BLOCK6  Voltage					N/A		SMBAT/BRC12 Tensión Celda 6				N/A
700		ID_HEAD657			32			N/A			SMBAT/BRC12 BLOCK7  Voltage					N/A		SMBAT/BRC12 Tensión Celda 7				N/A
701		ID_HEAD658			32			N/A			SMBAT/BRC12 BLOCK8  Voltage					N/A		SMBAT/BRC12 Tensión Celda 8				N/A
702		ID_HEAD659			32			N/A			SMBAT/BRC12 BLOCK9  Voltage					N/A		SMBAT/BRC12 Tensión Celda 9				N/A
703		ID_HEAD660			32			N/A			SMBAT/BRC12 BLOCK10 Voltage					N/A		SMBAT/BRC12 Tensión Celda 10				N/A
704		ID_HEAD661			32			N/A			SMBAT/BRC12 BLOCK11 Voltage					N/A		SMBAT/BRC12 Tensión Celda 11				N/A
705		ID_HEAD662			32			N/A			SMBAT/BRC12 BLOCK12 Voltage					N/A		SMBAT/BRC12 Tensión Celda 12				N/A
706		ID_HEAD663			32			N/A			SMBAT/BRC12 BLOCK13 Voltage					N/A		SMBAT/BRC12 Tensión Celda 13				N/A
707		ID_HEAD664			32			N/A			SMBAT/BRC12 BLOCK14 Voltage					N/A		SMBAT/BRC12 Tensión Celda 14				N/A
708		ID_HEAD665			32			N/A			SMBAT/BRC12 BLOCK15 Voltage					N/A		SMBAT/BRC12 Tensión Celda 15				N/A
709		ID_HEAD666			32			N/A			SMBAT/BRC12 BLOCK16 Voltage					N/A		SMBAT/BRC12 Tensión Celda 16				N/A
710		ID_HEAD667			32			N/A			SMBAT/BRC12 BLOCK17 Voltage					N/A		SMBAT/BRC12 Tensión Celda 17				N/A
711		ID_HEAD668			32			N/A			SMBAT/BRC12 BLOCK18 Voltage					N/A		SMBAT/BRC12 Tensión Celda 18				N/A
712		ID_HEAD669			32			N/A			SMBAT/BRC12 BLOCK19 Voltage					N/A		SMBAT/BRC12 Tensión Celda 19				N/A
713		ID_HEAD670			32			N/A			SMBAT/BRC12 BLOCK20 Voltage					N/A		SMBAT/BRC12 Tensión Celda 20				N/A
714		ID_HEAD671			32			N/A			SMBAT/BRC12 BLOCK21 Voltage					N/A		SMBAT/BRC12 Tensión Celda 21				N/A
715		ID_HEAD672			32			N/A			SMBAT/BRC12 BLOCK22 Voltage					N/A		SMBAT/BRC12 Tensión Celda 22				N/A
716		ID_HEAD673			32			N/A			SMBAT/BRC12 BLOCK23 Voltage					N/A		SMBAT/BRC12 Tensión Celda 23				N/A
717		ID_HEAD674			32			N/A			SMBAT/BRC12 BLOCK24 Voltage					N/A		SMBAT/BRC12 Tensión Celda 24				N/A
718		ID_HEAD675			32			N/A			SMBAT/BRC13 BLOCK1  Voltage					N/A		SMBAT/BRC13 Tensión Celda 1				N/A
719		ID_HEAD676			32			N/A			SMBAT/BRC13 BLOCK2  Voltage					N/A		SMBAT/BRC13 Tensión Celda 2				N/A
720		ID_HEAD677			32			N/A			SMBAT/BRC13 BLOCK3  Voltage					N/A		SMBAT/BRC13 Tensión Celda 3				N/A
721		ID_HEAD678			32			N/A			SMBAT/BRC13 BLOCK4  Voltage					N/A		SMBAT/BRC13 Tensión Celda 4				N/A
722		ID_HEAD679			32			N/A			SMBAT/BRC13 BLOCK5  Voltage					N/A		SMBAT/BRC13 Tensión Celda 5				N/A
723		ID_HEAD680			32			N/A			SMBAT/BRC13 BLOCK6  Voltage					N/A		SMBAT/BRC13 Tensión Celda 6				N/A
724		ID_HEAD681			32			N/A			SMBAT/BRC13 BLOCK7  Voltage					N/A		SMBAT/BRC13 Tensión Celda 7				N/A
725		ID_HEAD682			32			N/A			SMBAT/BRC13 BLOCK8  Voltage					N/A		SMBAT/BRC13 Tensión Celda 8				N/A
726		ID_HEAD683			32			N/A			SMBAT/BRC13 BLOCK9  Voltage					N/A		SMBAT/BRC13 Tensión Celda 9				N/A
727		ID_HEAD684			32			N/A			SMBAT/BRC13 BLOCK10 Voltage					N/A		SMBAT/BRC13 Tensión Celda 10				N/A
728		ID_HEAD685			32			N/A			SMBAT/BRC13 BLOCK11 Voltage					N/A		SMBAT/BRC13 Tensión Celda 11				N/A
729		ID_HEAD686			32			N/A			SMBAT/BRC13 BLOCK12 Voltage					N/A		SMBAT/BRC13 Tensión Celda 12				N/A
730		ID_HEAD687			32			N/A			SMBAT/BRC13 BLOCK13 Voltage					N/A		SMBAT/BRC13 Tensión Celda 13				N/A
731		ID_HEAD688			32			N/A			SMBAT/BRC13 BLOCK14 Voltage					N/A		SMBAT/BRC13 Tensión Celda 14				N/A
732		ID_HEAD689			32			N/A			SMBAT/BRC13 BLOCK15 Voltage					N/A		SMBAT/BRC13 Tensión Celda 15				N/A
733		ID_HEAD690			32			N/A			SMBAT/BRC13 BLOCK16 Voltage					N/A		SMBAT/BRC13 Tensión Celda 16				N/A
734		ID_HEAD691			32			N/A			SMBAT/BRC13 BLOCK17 Voltage					N/A		SMBAT/BRC13 Tensión Celda 17				N/A
735		ID_HEAD692			32			N/A			SMBAT/BRC13 BLOCK18 Voltage					N/A		SMBAT/BRC13 Tensión Celda 18				N/A
736		ID_HEAD693			32			N/A			SMBAT/BRC13 BLOCK19 Voltage					N/A		SMBAT/BRC13 Tensión Celda 19				N/A
737		ID_HEAD694			32			N/A			SMBAT/BRC13 BLOCK20 Voltage					N/A		SMBAT/BRC13 Tensión Celda 20				N/A
738		ID_HEAD695			32			N/A			SMBAT/BRC13 BLOCK21 Voltage					N/A		SMBAT/BRC13 Tensión Celda 21				N/A
739		ID_HEAD696			32			N/A			SMBAT/BRC13 BLOCK22 Voltage					N/A		SMBAT/BRC13 Tensión Celda 22				N/A
740		ID_HEAD697			32			N/A			SMBAT/BRC13 BLOCK23 Voltage					N/A		SMBAT/BRC13 Tensión Celda 23				N/A
741		ID_HEAD698			32			N/A			SMBAT/BRC13 BLOCK24 Voltage					N/A		SMBAT/BRC13 Tensión Celda 24				N/A
742		ID_HEAD699			32			N/A			SMBAT/BRC14 BLOCK1  Voltage					N/A		SMBAT/BRC14 Tensión Celda 1				N/A
743		ID_HEAD700			32			N/A			SMBAT/BRC14 BLOCK2  Voltage					N/A		SMBAT/BRC14 Tensión Celda 2				N/A
744		ID_HEAD701			32			N/A			SMBAT/BRC14 BLOCK3  Voltage					N/A		SMBAT/BRC14 Tensión Celda 3				N/A
745		ID_HEAD702			32			N/A			SMBAT/BRC14 BLOCK4  Voltage					N/A		SMBAT/BRC14 Tensión Celda 4				N/A
746		ID_HEAD703			32			N/A			SMBAT/BRC14 BLOCK5  Voltage					N/A		SMBAT/BRC14 Tensión Celda 5				N/A
747		ID_HEAD704			32			N/A			SMBAT/BRC14 BLOCK6  Voltage					N/A		SMBAT/BRC14 Tensión Celda 6				N/A
748		ID_HEAD705			32			N/A			SMBAT/BRC14 BLOCK7  Voltage					N/A		SMBAT/BRC14 Tensión Celda 7				N/A
749		ID_HEAD706			32			N/A			SMBAT/BRC14 BLOCK8  Voltage					N/A		SMBAT/BRC14 Tensión Celda 8				N/A
750		ID_HEAD707			32			N/A			SMBAT/BRC14 BLOCK9  Voltage					N/A		SMBAT/BRC14 Tensión Celda 9				N/A
751		ID_HEAD708			32			N/A			SMBAT/BRC14 BLOCK10 Voltage					N/A		SMBAT/BRC14 Tensión Celda 10				N/A
752		ID_HEAD709			32			N/A			SMBAT/BRC14 BLOCK11 Voltage					N/A		SMBAT/BRC14 Tensión Celda 11				N/A
753		ID_HEAD710			32			N/A			SMBAT/BRC14 BLOCK12 Voltage					N/A		SMBAT/BRC14 Tensión Celda 12				N/A
754		ID_HEAD711			32			N/A			SMBAT/BRC14 BLOCK13 Voltage					N/A		SMBAT/BRC14 Tensión Celda 13				N/A
755		ID_HEAD712			32			N/A			SMBAT/BRC14 BLOCK14 Voltage					N/A		SMBAT/BRC14 Tensión Celda 14				N/A
756		ID_HEAD713			32			N/A			SMBAT/BRC14 BLOCK15 Voltage					N/A		SMBAT/BRC14 Tensión Celda 15				N/A
757		ID_HEAD714			32			N/A			SMBAT/BRC14 BLOCK16 Voltage					N/A		SMBAT/BRC14 Tensión Celda 16				N/A
758		ID_HEAD715			32			N/A			SMBAT/BRC14 BLOCK17 Voltage					N/A		SMBAT/BRC14 Tensión Celda 17				N/A
759		ID_HEAD716			32			N/A			SMBAT/BRC14 BLOCK18 Voltage					N/A		SMBAT/BRC14 Tensión Celda 18				N/A
760		ID_HEAD717			32			N/A			SMBAT/BRC14 BLOCK19 Voltage					N/A		SMBAT/BRC14 Tensión Celda 19				N/A
761		ID_HEAD718			32			N/A			SMBAT/BRC14 BLOCK20 Voltage					N/A		SMBAT/BRC14 Tensión Celda 20				N/A
762		ID_HEAD719			32			N/A			SMBAT/BRC14 BLOCK21 Voltage					N/A		SMBAT/BRC14 Tensión Celda 21				N/A
763		ID_HEAD720			32			N/A			SMBAT/BRC14 BLOCK22 Voltage					N/A		SMBAT/BRC14 Tensión Celda 22				N/A
764		ID_HEAD721			32			N/A			SMBAT/BRC14 BLOCK23 Voltage					N/A		SMBAT/BRC14 Tensión Celda 23				N/A
765		ID_HEAD722			32			N/A			SMBAT/BRC14 BLOCK24 Voltage					N/A		SMBAT/BRC14 Tensión Celda 24				N/A
766		ID_HEAD723			32			N/A			SMBAT/BRC15 BLOCK1  Voltage					N/A		SMBAT/BRC15 Tensión Celda 1				N/A
767		ID_HEAD724			32			N/A			SMBAT/BRC15 BLOCK2  Voltage					N/A		SMBAT/BRC15 Tensión Celda 2				N/A
768		ID_HEAD725			32			N/A			SMBAT/BRC15 BLOCK3  Voltage					N/A		SMBAT/BRC15 Tensión Celda 3				N/A
769		ID_HEAD726			32			N/A			SMBAT/BRC15 BLOCK4  Voltage					N/A		SMBAT/BRC15 Tensión Celda 4				N/A
770		ID_HEAD727			32			N/A			SMBAT/BRC15 BLOCK5  Voltage					N/A		SMBAT/BRC15 Tensión Celda 5				N/A
771		ID_HEAD728			32			N/A			SMBAT/BRC15 BLOCK6  Voltage					N/A		SMBAT/BRC15 Tensión Celda 6				N/A
772		ID_HEAD729			32			N/A			SMBAT/BRC15 BLOCK7  Voltage					N/A		SMBAT/BRC15 Tensión Celda 7				N/A
773		ID_HEAD730			32			N/A			SMBAT/BRC15 BLOCK8  Voltage					N/A		SMBAT/BRC15 Tensión Celda 8				N/A
774		ID_HEAD731			32			N/A			SMBAT/BRC15 BLOCK9  Voltage					N/A		SMBAT/BRC15 Tensión Celda 9				N/A
775		ID_HEAD732			32			N/A			SMBAT/BRC15 BLOCK10 Voltage					N/A		SMBAT/BRC15 Tensión Celda 10				N/A
776		ID_HEAD733			32			N/A			SMBAT/BRC15 BLOCK11 Voltage					N/A		SMBAT/BRC15 Tensión Celda 11				N/A
777		ID_HEAD734			32			N/A			SMBAT/BRC15 BLOCK12 Voltage					N/A		SMBAT/BRC15 Tensión Celda 12				N/A
778		ID_HEAD735			32			N/A			SMBAT/BRC15 BLOCK13 Voltage					N/A		SMBAT/BRC15 Tensión Celda 13				N/A
779		ID_HEAD736			32			N/A			SMBAT/BRC15 BLOCK14 Voltage					N/A		SMBAT/BRC15 Tensión Celda 14				N/A
780		ID_HEAD737			32			N/A			SMBAT/BRC15 BLOCK15 Voltage					N/A		SMBAT/BRC15 Tensión Celda 15				N/A
781		ID_HEAD738			32			N/A			SMBAT/BRC15 BLOCK16 Voltage					N/A		SMBAT/BRC15 Tensión Celda 16				N/A
782		ID_HEAD739			32			N/A			SMBAT/BRC15 BLOCK17 Voltage					N/A		SMBAT/BRC15 Tensión Celda 17				N/A
783		ID_HEAD740			32			N/A			SMBAT/BRC15 BLOCK18 Voltage					N/A		SMBAT/BRC15 Tensión Celda 18				N/A
784		ID_HEAD741			32			N/A			SMBAT/BRC15 BLOCK19 Voltage					N/A		SMBAT/BRC15 Tensión Celda 19				N/A
785		ID_HEAD742			32			N/A			SMBAT/BRC15 BLOCK20 Voltage					N/A		SMBAT/BRC15 Tensión Celda 20				N/A
786		ID_HEAD743			32			N/A			SMBAT/BRC15 BLOCK21 Voltage					N/A		SMBAT/BRC15 Tensión Celda 21				N/A
787		ID_HEAD744			32			N/A			SMBAT/BRC15 BLOCK22 Voltage					N/A		SMBAT/BRC15 Tensión Celda 22				N/A
788		ID_HEAD745			32			N/A			SMBAT/BRC15 BLOCK23 Voltage					N/A		SMBAT/BRC15 Tensión Celda 23				N/A
789		ID_HEAD746			32			N/A			SMBAT/BRC15 BLOCK24 Voltage					N/A		SMBAT/BRC15 Tensión Celda 24				N/A
790		ID_HEAD747			32			N/A			SMBAT/BRC16 BLOCK1  Voltage					N/A		SMBAT/BRC16 Tensión Celda 1				N/A
791		ID_HEAD748			32			N/A			SMBAT/BRC16 BLOCK2  Voltage					N/A		SMBAT/BRC16 Tensión Celda 2				N/A
792		ID_HEAD749			32			N/A			SMBAT/BRC16 BLOCK3  Voltage					N/A		SMBAT/BRC16 Tensión Celda 3				N/A
793		ID_HEAD750			32			N/A			SMBAT/BRC16 BLOCK4  Voltage					N/A		SMBAT/BRC16 Tensión Celda 4				N/A
794		ID_HEAD751			32			N/A			SMBAT/BRC16 BLOCK5  Voltage					N/A		SMBAT/BRC16 Tensión Celda 5				N/A
795		ID_HEAD752			32			N/A			SMBAT/BRC16 BLOCK6  Voltage					N/A		SMBAT/BRC16 Tensión Celda 6				N/A
796		ID_HEAD753			32			N/A			SMBAT/BRC16 BLOCK7  Voltage					N/A		SMBAT/BRC16 Tensión Celda 7				N/A
797		ID_HEAD754			32			N/A			SMBAT/BRC16 BLOCK8  Voltage					N/A		SMBAT/BRC16 Tensión Celda 8				N/A
798		ID_HEAD755			32			N/A			SMBAT/BRC16 BLOCK9  Voltage					N/A		SMBAT/BRC16 Tensión Celda 9				N/A
799		ID_HEAD756			32			N/A			SMBAT/BRC16 BLOCK10 Voltage					N/A		SMBAT/BRC16 Tensión Celda 10				N/A
800		ID_HEAD757			32			N/A			SMBAT/BRC16 BLOCK11 Voltage					N/A		SMBAT/BRC16 Tensión Celda 11				N/A
801		ID_HEAD758			32			N/A			SMBAT/BRC16 BLOCK12 Voltage					N/A		SMBAT/BRC16 Tensión Celda 12				N/A
802		ID_HEAD759			32			N/A			SMBAT/BRC16 BLOCK13 Voltage					N/A		SMBAT/BRC16 Tensión Celda 13				N/A
803		ID_HEAD760			32			N/A			SMBAT/BRC16 BLOCK14 Voltage					N/A		SMBAT/BRC16 Tensión Celda 14				N/A
804		ID_HEAD761			32			N/A			SMBAT/BRC16 BLOCK15 Voltage					N/A		SMBAT/BRC16 Tensión Celda 15				N/A
805		ID_HEAD762			32			N/A			SMBAT/BRC16 BLOCK16 Voltage					N/A		SMBAT/BRC16 Tensión Celda 16				N/A
806		ID_HEAD763			32			N/A			SMBAT/BRC16 BLOCK17 Voltage					N/A		SMBAT/BRC16 Tensión Celda 17				N/A
807		ID_HEAD764			32			N/A			SMBAT/BRC16 BLOCK18 Voltage					N/A		SMBAT/BRC16 Tensión Celda 18				N/A
808		ID_HEAD765			32			N/A			SMBAT/BRC16 BLOCK19 Voltage					N/A		SMBAT/BRC16 Tensión Celda 19				N/A
809		ID_HEAD766			32			N/A			SMBAT/BRC16 BLOCK20 Voltage					N/A		SMBAT/BRC16 Tensión Celda 20				N/A
810		ID_HEAD767			32			N/A			SMBAT/BRC16 BLOCK21 Voltage					N/A		SMBAT/BRC16 Tensión Celda 21				N/A
811		ID_HEAD768			32			N/A			SMBAT/BRC16 BLOCK22 Voltage					N/A		SMBAT/BRC16 Tensión Celda 22				N/A
812		ID_HEAD769			32			N/A			SMBAT/BRC16 BLOCK23 Voltage					N/A		SMBAT/BRC16 Tensión Celda 23				N/A
813		ID_HEAD770			32			N/A			SMBAT/BRC16 BLOCK24 Voltage					N/A		SMBAT/BRC16 Tensión Celda 24				N/A
814		ID_HEAD771			32			N/A			SMBAT/BRC17 BLOCK1  Voltage					N/A		SMBAT/BRC17 Tensión Celda 1				N/A
815		ID_HEAD772			32			N/A			SMBAT/BRC17 BLOCK2  Voltage					N/A		SMBAT/BRC17 Tensión Celda 2				N/A
816		ID_HEAD773			32			N/A			SMBAT/BRC17 BLOCK3  Voltage					N/A		SMBAT/BRC17 Tensión Celda 3				N/A
817		ID_HEAD774			32			N/A			SMBAT/BRC17 BLOCK4  Voltage					N/A		SMBAT/BRC17 Tensión Celda 4				N/A
818		ID_HEAD775			32			N/A			SMBAT/BRC17 BLOCK5  Voltage					N/A		SMBAT/BRC17 Tensión Celda 5				N/A
819		ID_HEAD776			32			N/A			SMBAT/BRC17 BLOCK6  Voltage					N/A		SMBAT/BRC17 Tensión Celda 6				N/A
820		ID_HEAD777			32			N/A			SMBAT/BRC17 BLOCK7  Voltage					N/A		SMBAT/BRC17 Tensión Celda 7				N/A
821		ID_HEAD778			32			N/A			SMBAT/BRC17 BLOCK8  Voltage					N/A		SMBAT/BRC17 Tensión Celda 8				N/A
822		ID_HEAD779			32			N/A			SMBAT/BRC17 BLOCK9  Voltage					N/A		SMBAT/BRC17 Tensión Celda 9				N/A
823		ID_HEAD780			32			N/A			SMBAT/BRC17 BLOCK10 Voltage					N/A		SMBAT/BRC17 Tensión Celda 10				N/A
824		ID_HEAD781			32			N/A			SMBAT/BRC17 BLOCK11 Voltage					N/A		SMBAT/BRC17 Tensión Celda 11				N/A
825		ID_HEAD782			32			N/A			SMBAT/BRC17 BLOCK12 Voltage					N/A		SMBAT/BRC17 Tensión Celda 12				N/A
826		ID_HEAD783			32			N/A			SMBAT/BRC17 BLOCK13 Voltage					N/A		SMBAT/BRC17 Tensión Celda 13				N/A
827		ID_HEAD784			32			N/A			SMBAT/BRC17 BLOCK14 Voltage					N/A		SMBAT/BRC17 Tensión Celda 14				N/A
828		ID_HEAD785			32			N/A			SMBAT/BRC17 BLOCK15 Voltage					N/A		SMBAT/BRC17 Tensión Celda 15				N/A
829		ID_HEAD786			32			N/A			SMBAT/BRC17 BLOCK16 Voltage					N/A		SMBAT/BRC17 Tensión Celda 16				N/A
830		ID_HEAD787			32			N/A			SMBAT/BRC17 BLOCK17 Voltage					N/A		SMBAT/BRC17 Tensión Celda 17				N/A
831		ID_HEAD788			32			N/A			SMBAT/BRC17 BLOCK18 Voltage					N/A		SMBAT/BRC17 Tensión Celda 18				N/A
832		ID_HEAD789			32			N/A			SMBAT/BRC17 BLOCK19 Voltage					N/A		SMBAT/BRC17 Tensión Celda 19				N/A
833		ID_HEAD790			32			N/A			SMBAT/BRC17 BLOCK20 Voltage					N/A		SMBAT/BRC17 Tensión Celda 20				N/A
834		ID_HEAD791			32			N/A			SMBAT/BRC17 BLOCK21 Voltage					N/A		SMBAT/BRC17 Tensión Celda 21				N/A
835		ID_HEAD792			32			N/A			SMBAT/BRC17 BLOCK22 Voltage					N/A		SMBAT/BRC17 Tensión Celda 22				N/A
836		ID_HEAD793			32			N/A			SMBAT/BRC17 BLOCK23 Voltage					N/A		SMBAT/BRC17 Tensión Celda 23				N/A
837		ID_HEAD794			32			N/A			SMBAT/BRC17 BLOCK24 Voltage					N/A		SMBAT/BRC17 Tensión Celda 24				N/A
838		ID_HEAD795			32			N/A			SMBAT/BRC18 BLOCK1  Voltage					N/A		SMBAT/BRC18 Tensión Celda 1				N/A
839		ID_HEAD796			32			N/A			SMBAT/BRC18 BLOCK2  Voltage					N/A		SMBAT/BRC18 Tensión Celda 2				N/A
840		ID_HEAD797			32			N/A			SMBAT/BRC18 BLOCK3  Voltage					N/A		SMBAT/BRC18 Tensión Celda 3				N/A
841		ID_HEAD798			32			N/A			SMBAT/BRC18 BLOCK4  Voltage					N/A		SMBAT/BRC18 Tensión Celda 4				N/A
842		ID_HEAD799			32			N/A			SMBAT/BRC18 BLOCK5  Voltage					N/A		SMBAT/BRC18 Tensión Celda 5				N/A
843		ID_HEAD800			32			N/A			SMBAT/BRC18 BLOCK6  Voltage					N/A		SMBAT/BRC18 Tensión Celda 6				N/A
844		ID_HEAD801			32			N/A			SMBAT/BRC18 BLOCK7  Voltage					N/A		SMBAT/BRC18 Tensión Celda 7				N/A
845		ID_HEAD802			32			N/A			SMBAT/BRC18 BLOCK8  Voltage					N/A		SMBAT/BRC18 Tensión Celda 8				N/A
846		ID_HEAD803			32			N/A			SMBAT/BRC18 BLOCK9  Voltage					N/A		SMBAT/BRC18 Tensión Celda 9				N/A
847		ID_HEAD804			32			N/A			SMBAT/BRC18 BLOCK10 Voltage					N/A		SMBAT/BRC18 Tensión Celda 10				N/A
848		ID_HEAD805			32			N/A			SMBAT/BRC18 BLOCK11 Voltage					N/A		SMBAT/BRC18 Tensión Celda 11				N/A
849		ID_HEAD806			32			N/A			SMBAT/BRC18 BLOCK12 Voltage					N/A		SMBAT/BRC18 Tensión Celda 12				N/A
850		ID_HEAD807			32			N/A			SMBAT/BRC18 BLOCK13 Voltage					N/A		SMBAT/BRC18 Tensión Celda 13				N/A
851		ID_HEAD808			32			N/A			SMBAT/BRC18 BLOCK14 Voltage					N/A		SMBAT/BRC18 Tensión Celda 14				N/A
852		ID_HEAD809			32			N/A			SMBAT/BRC18 BLOCK15 Voltage					N/A		SMBAT/BRC18 Tensión Celda 15				N/A
853		ID_HEAD810			32			N/A			SMBAT/BRC18 BLOCK16 Voltage					N/A		SMBAT/BRC18 Tensión Celda 16				N/A
854		ID_HEAD811			32			N/A			SMBAT/BRC18 BLOCK17 Voltage					N/A		SMBAT/BRC18 Tensión Celda 17				N/A
855		ID_HEAD812			32			N/A			SMBAT/BRC18 BLOCK18 Voltage					N/A		SMBAT/BRC18 Tensión Celda 18				N/A
856		ID_HEAD813			32			N/A			SMBAT/BRC18 BLOCK19 Voltage					N/A		SMBAT/BRC18 Tensión Celda 19				N/A
857		ID_HEAD814			32			N/A			SMBAT/BRC18 BLOCK20 Voltage					N/A		SMBAT/BRC18 Tensión Celda 20				N/A
858		ID_HEAD815			32			N/A			SMBAT/BRC18 BLOCK21 Voltage					N/A		SMBAT/BRC18 Tensión Celda 21				N/A
859		ID_HEAD816			32			N/A			SMBAT/BRC18 BLOCK22 Voltage					N/A		SMBAT/BRC18 Tensión Celda 22				N/A
860		ID_HEAD817			32			N/A			SMBAT/BRC18 BLOCK23 Voltage					N/A		SMBAT/BRC18 Tensión Celda 23				N/A
861		ID_HEAD818			32			N/A			SMBAT/BRC18 BLOCK24 Voltage					N/A		SMBAT/BRC18 Tensión Celda 24				N/A
862		ID_HEAD819			32			N/A			SMBAT/BRC19 BLOCK1  Voltage					N/A		SMBAT/BRC19 Tensión Celda 1				N/A
863		ID_HEAD820			32			N/A			SMBAT/BRC19 BLOCK2  Voltage					N/A		SMBAT/BRC19 Tensión Celda 2				N/A
864		ID_HEAD821			32			N/A			SMBAT/BRC19 BLOCK3  Voltage					N/A		SMBAT/BRC19 Tensión Celda 3				N/A
865		ID_HEAD822			32			N/A			SMBAT/BRC19 BLOCK4  Voltage					N/A		SMBAT/BRC19 Tensión Celda 4				N/A
866		ID_HEAD823			32			N/A			SMBAT/BRC19 BLOCK5  Voltage					N/A		SMBAT/BRC19 Tensión Celda 5				N/A
867		ID_HEAD824			32			N/A			SMBAT/BRC19 BLOCK6  Voltage					N/A		SMBAT/BRC19 Tensión Celda 6				N/A
868		ID_HEAD825			32			N/A			SMBAT/BRC19 BLOCK7  Voltage					N/A		SMBAT/BRC19 Tensión Celda 7				N/A
869		ID_HEAD826			32			N/A			SMBAT/BRC19 BLOCK8  Voltage					N/A		SMBAT/BRC19 Tensión Celda 8				N/A
870		ID_HEAD827			32			N/A			SMBAT/BRC19 BLOCK9  Voltage					N/A		SMBAT/BRC19 Tensión Celda 9				N/A
871		ID_HEAD828			32			N/A			SMBAT/BRC19 BLOCK10 Voltage					N/A		SMBAT/BRC19 Tensión Celda 10				N/A
872		ID_HEAD829			32			N/A			SMBAT/BRC19 BLOCK11 Voltage					N/A		SMBAT/BRC19 Tensión Celda 11				N/A
873		ID_HEAD830			32			N/A			SMBAT/BRC19 BLOCK12 Voltage					N/A		SMBAT/BRC19 Tensión Celda 12				N/A
874		ID_HEAD831			32			N/A			SMBAT/BRC19 BLOCK13 Voltage					N/A		SMBAT/BRC19 Tensión Celda 13				N/A
875		ID_HEAD832			32			N/A			SMBAT/BRC19 BLOCK14 Voltage					N/A		SMBAT/BRC19 Tensión Celda 14				N/A
876		ID_HEAD833			32			N/A			SMBAT/BRC19 BLOCK15 Voltage					N/A		SMBAT/BRC19 Tensión Celda 15				N/A
877		ID_HEAD834			32			N/A			SMBAT/BRC19 BLOCK16 Voltage					N/A		SMBAT/BRC19 Tensión Celda 16				N/A
878		ID_HEAD835			32			N/A			SMBAT/BRC19 BLOCK17 Voltage					N/A		SMBAT/BRC19 Tensión Celda 17				N/A
879		ID_HEAD836			32			N/A			SMBAT/BRC19 BLOCK18 Voltage					N/A		SMBAT/BRC19 Tensión Celda 18				N/A
880		ID_HEAD837			32			N/A			SMBAT/BRC19 BLOCK19 Voltage					N/A		SMBAT/BRC19 Tensión Celda 19				N/A
881		ID_HEAD838			32			N/A			SMBAT/BRC19 BLOCK20 Voltage					N/A		SMBAT/BRC19 Tensión Celda 20				N/A
882		ID_HEAD839			32			N/A			SMBAT/BRC19 BLOCK21 Voltage					N/A		SMBAT/BRC19 Tensión Celda 21				N/A
883		ID_HEAD840			32			N/A			SMBAT/BRC19 BLOCK22 Voltage					N/A		SMBAT/BRC19 Tensión Celda 22				N/A
884		ID_HEAD841			32			N/A			SMBAT/BRC19 BLOCK23 Voltage					N/A		SMBAT/BRC19 Tensión Celda 23				N/A
885		ID_HEAD842			32			N/A			SMBAT/BRC19 BLOCK24 Voltage					N/A		SMBAT/BRC19 Tensión Celda 24				N/A
886		ID_HEAD843			32			N/A			SMBAT/BRC20 BLOCK1  Voltage					N/A		SMBAT/BRC20 Tensión Celda 1				N/A
887		ID_HEAD844			32			N/A			SMBAT/BRC20 BLOCK2  Voltage					N/A		SMBAT/BRC20 Tensión Celda 2				N/A
888		ID_HEAD845			32			N/A			SMBAT/BRC20 BLOCK3  Voltage					N/A		SMBAT/BRC20 Tensión Celda 3				N/A
889		ID_HEAD846			32			N/A			SMBAT/BRC20 BLOCK4  Voltage					N/A		SMBAT/BRC20 Tensión Celda 4				N/A
890		ID_HEAD847			32			N/A			SMBAT/BRC20 BLOCK5  Voltage					N/A		SMBAT/BRC20 Tensión Celda 5				N/A
891		ID_HEAD848			32			N/A			SMBAT/BRC20 BLOCK6  Voltage					N/A		SMBAT/BRC20 Tensión Celda 6				N/A
892		ID_HEAD849			32			N/A			SMBAT/BRC20 BLOCK7  Voltage					N/A		SMBAT/BRC20 Tensión Celda 7				N/A
893		ID_HEAD850			32			N/A			SMBAT/BRC20 BLOCK8  Voltage					N/A		SMBAT/BRC20 Tensión Celda 8				N/A
894		ID_HEAD851			32			N/A			SMBAT/BRC20 BLOCK9  Voltage					N/A		SMBAT/BRC20 Tensión Celda 9				N/A
895		ID_HEAD852			32			N/A			SMBAT/BRC20 BLOCK10 Voltage					N/A		SMBAT/BRC20 Tensión Celda 10				N/A
896		ID_HEAD853			32			N/A			SMBAT/BRC20 BLOCK11 Voltage					N/A		SMBAT/BRC20 Tensión Celda 11				N/A
897		ID_HEAD854			32			N/A			SMBAT/BRC20 BLOCK12 Voltage					N/A		SMBAT/BRC20 Tensión Celda 12				N/A
898		ID_HEAD855			32			N/A			SMBAT/BRC20 BLOCK13 Voltage					N/A		SMBAT/BRC20 Tensión Celda 13				N/A
899		ID_HEAD856			32			N/A			SMBAT/BRC20 BLOCK14 Voltage					N/A		SMBAT/BRC20 Tensión Celda 14				N/A
900		ID_HEAD857			32			N/A			SMBAT/BRC20 BLOCK15 Voltage					N/A		SMBAT/BRC20 Tensión Celda 15				N/A
901		ID_HEAD858			32			N/A			SMBAT/BRC20 BLOCK16 Voltage					N/A		SMBAT/BRC20 Tensión Celda 16				N/A
902		ID_HEAD859			32			N/A			SMBAT/BRC20 BLOCK17 Voltage					N/A		SMBAT/BRC20 Tensión Celda 17				N/A
903		ID_HEAD860			32			N/A			SMBAT/BRC20 BLOCK18 Voltage					N/A		SMBAT/BRC20 Tensión Celda 18				N/A
904		ID_HEAD861			32			N/A			SMBAT/BRC20 BLOCK19 Voltage					N/A		SMBAT/BRC20 Tensión Celda 19				N/A
905		ID_HEAD862			32			N/A			SMBAT/BRC20 BLOCK20 Voltage					N/A		SMBAT/BRC20 Tensión Celda 20				N/A
906		ID_HEAD863			32			N/A			SMBAT/BRC20 BLOCK21 Voltage					N/A		SMBAT/BRC20 Tensión Celda 21				N/A
907		ID_HEAD864			32			N/A			SMBAT/BRC20 BLOCK22 Voltage					N/A		SMBAT/BRC20 Tensión Celda 22				N/A
908		ID_HEAD865			32			N/A			SMBAT/BRC20 BLOCK23 Voltage					N/A		SMBAT/BRC20 Tensión Celda 23				N/A
909		ID_HEAD866			32			N/A			SMBAT/BRC20 BLOCK24 Voltage					N/A		SMBAT/BRC20 Tensión Celda 24				N/A


[p35_status_switch.htm:Number]
3

[p35_status_switch.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE				ABBR_IN_LOCALE
1		ID_AUTO_POP		16			N/A			Auto Popup				N/A		Autoemergente				N/A
2		ID_AUTO_POP_1		16			N/A			Auto Popup				N/A		Autoemergente				N/A
3		ID_DISABLE_AUTO_POP	32			N/A			Disable Auto Popup			N/A		Deshabilitar autoemergente		N/A

[p36_clear_data.htm:Number]
18

[p36_clear_data.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_ERROR0		64			N/A			Failed to clear data.				N/A		Fallo al borrar los datos!			N/A
2		ID_ERROR1		64			N/A			Cleared.					N/A		fueron borrados!				N/A
3		ID_ERROR2		64			N/A			Unknown error.					N/A		Error desconocido!				N/A
4		ID_ERROR3		128			N/A			Failed. No authority.		N/A		Error, no tiene la autoridad requerida!		N/A
5		ID_ERROR4		64			N/A			Failed to communicate with the Controller.	N/A		Fallo de comunicación con la ACU		N/A
6		ID_ERROR5		64			N/A			Failed. Controller is hardware protected.	N/A		Fallo, ACU protegida por HW			N/A
7		ID_HISTORY_ALARM	64			N/A			Alarm History					N/A		Histórico de alarmas				N/A
8		ID_HISTORY_DATA		64			N/A			Data History					N/A		Datos históricos				N/A
9		ID_HISTORY_STATDATA	64			N/A			Statistic Data				N/A		Datos Estadísticos				N/A
10		ID_HISTORY_CONTROL	64			N/A			Control Command Log				N/A		Registro comandos de Control			N/A
11		ID_HISTORY_BATTERY	64			N/A			Battery Test Log				N/A		Registro prueba de baterías			N/A
12		ID_HISTORY_PARAM	64			N/A			Runtime Persistent Data				N/A		Datos persistentes de operación			N/A
13		ID_HISTORY_SCUP_RUNNING	32			N/A			System Runtime Log				N/A		Registro del Sistema				N/A
14		ID_HEAD			32			N/A			Clear						N/A		Borrar						N/A
15		ID_TIPS			32			N/A			Clear						N/A		Borrar						N/A
16		ID_CLEAR		32			N/A			Clear						N/A		Borrar						N/A
17		ID_TIPS1		64			N/A			Are you sure you want to clear the data?	N/A		Está seguro de borrar los datos?		N/A
18		ID_HISTORY_DISEL_TEST	64			N/A			Diesel Test Log					N/A		Registro prueba de Grupo Electrógeno		N/A

[p37_edit_config_file.htm:Number]
0

[p37_edit_config_file.htm.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE				


[p38_title_config_file.htm:Number]
3

[p38_title_config_file.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_PLC			16			N/A			PLC Config			N/A		Config PLC		N/A
2		ID_ALARM_REG		32			N/A			ALARM_REG CFG				N/A		Config Reg Alarmas	N/A
3		ID_AlARM		32			N/A			AlARM_SUPPRESS CFG			N/A		Supresión de Alarmas	N/A

[p39_edit_config_plc.htm:Number]
51

[p39_edit_config_plc.htm]
#Sequence ID	RES_ID				MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_Operator			16			N/A			Operator					N/A		Operador					N/A
2		ID_Input1			16			N/A			Input1						N/A		Entrada1					N/A
3		ID_Input2			16			N/A			Input2						N/A		Entrada2					N/A
4		ID_Param1			16			N/A			Parameter1					N/A		Parámetro 1					N/A
5		ID_Param2			16			N/A			Parameter2					N/A		Parámetro 2					N/A
6		ID_Output			16			N/A			Output						N/A		Salida						N/A
7		ID_EquipName/Register1		32			N/A			Equipment/ Register			N/A		NombreEquipo/Registro				N/A
8		ID_SignalType1			16			N/A			Signal Type					N/A		Tipo Señal					N/A
9		ID_SignalName1			16			N/A			Signal Name					N/A		Nombre Señal					N/A
10		ID_EquipName/Register2		32			N/A			Equipment/ Register				N/A		NombreEquipo/Registro				N/A
11		ID_SignalType2			16			N/A			Signal Type					N/A		Tipo Señal					N/A
12		ID_SignalName2			16			N/A			Signal Name					N/A		Nombre Señal					N/A
13		ID_EquipName/Register3		32			N/A			Equipment/ Register				N/A		NombreEquipo/Registro				N/A
14		ID_SignalType3			16			N/A			Signal Type					N/A		Tipo Señal					N/A
15		ID_SignalName3			16			N/A			Signal Name					N/A		Nombre Señal					N/A
16		ID_ADD				16			N/A			Add						N/A		Añadir						N/A
17		ID_Delete			16			N/A			Delete						N/A		Borrar						N/A
18		ID_Sampling			16			N/A			Sampling					N/A		Muestreo					N/A
19		ID_Control			16			N/A			Control						N/A		Control						N/A
20		ID_Setting			16			N/A			Setting					N/A		Ajuste						N/A
21		ID_Alarm			16			N/A			Alarm						N/A		Alarma						N/A
22		ID_ERROR5			64			N/A			PLC configuration modified error.		N/A		Error configuración PLC modificada		N/A
23		ID_ERROR6			128			N/A			PLC configuration modified successfully. \nController must be restarted.	N/A	Configuración PLC modificada con éxito,\n Debe reiniciar la ACU para que los cambios tengan efecto!		N/A
24		ID_CONFIRM_1			64			N/A			Are you sure to delete?				N/A		Está seguro?					N/A
25		ID_ERROR0			64			N/A			PLC configuration file error.			N/A		Error archivo configuración PLC!		N/A
26		ID_ERROR1			128			N/A			Unknow error.					N/A		Error desconocido!				N/A
27		ID_INFO1			64			N/A			SYMBOL  INFORMATION				N/A		INFORMACION DE SIMBOLOS				N/A
28		ID_INFO2			64			N/A			1:R, which defines a Register.			N/A		1:R, que define un Registro.			N/A
29		ID_INFO3			128			N/A			Usage: R(Register_ID); 0 = &lt; Register_ID &lt;= 99	N/A	Uso: R(Register_ID); 0 = &lt; Register_ID &lt;= 99	N/A
30		ID_INFO4			64			N/A			2:P, which defines a Parameter.			N/A		2:P, que define un Parámetro			N/A
31		ID_INFO5			64			N/A			Usage: P(The Value)				N/A		Uso: P(Valor)					N/A
32		ID_INFO6			128			N/A			3:SET, which represent the SET command.		N/A		3:SET, que representa el comando fijar		N/A
33		ID_INFO7			128			N/A			Usage: SET _ _ Parameter1 _ Output		N/A		Uso: SET _ _ Parámetro1 _ Salida		N/A
34		ID_INFO8			128			N/A			4:AND, which represent the AND command.		N/A		4:AND, que representa el comando AND		N/A
35		ID_INFO9			128			N/A			Usage: AND Input1 Input2 _ _ Output		N/A		Uso: AND Entrada1 Entrada2 _ _ Salida		N/A
36		ID_INFO10			128			N/A			5:OR, which represent the OR command.		N/A		5:OR, que representa el comando OR		N/A
37		ID_INFO11			128			N/A			Usage: OR Input1 Input2 _ _ Output		N/A		Uso: OR Entrada1 Entrada2 _ _ Salida		N/A
38		ID_INFO12			128			N/A			6:NOT, which represent the NOT command.		N/A		6:NOT, que representa el comando NOT		N/A
39		ID_INFO13			128			N/A			Usage: NOT Input1 _ _ _ Output			N/A		Uso: NOT Entrada1 _ _ _ Salida			N/A
40		ID_INFO14			128			N/A			7:XOR, which represent the XOR command.		N/A		7:XOR, que representa el comando OR exclusivo	N/A
41		ID_INFO15			128			N/A			Usage: XOR Input1 Input2 _ _ Output		N/A		Uso: XOR Entrada1 Entrada2 _ _ Salida		N/A
42		ID_INFO16			128			N/A			8:GT, which represent the Greater Than command.	N/A		8:GT, que representa el comando Mayor que	N/A
43		ID_INFO17			128			N/A			Usage: GT Input1 _ Parameter1 Parameter2 Output	N/A		Uso, GT Entrada1 _ Parámetro1 Parámetro2 Salida	N/A
44		ID_INFO18			128			N/A			9:LT, which represent the Less Than command.	N/A		9:LT, que representa el comando Menor que.	N/A
45		ID_INFO19			128			N/A			Usage: LT Input1 _ Parameter1 Parameter2 Output	N/A		Uso: LT Entrada1 _ Parámetro1 Parámetro2 Salida	N/A
46		ID_INFO20			128			N/A			10:DS, which represent the Delay command.		N/A		10:DS, que representa un retardo en segundos.	N/A
47		ID_INFO21			128			N/A			Usage: DS Input1 _ Parameter1 _ Output		N/A		Uso: DS Entrada1 _ Parámetro1 _ Salida		N/A
48		ID_INFO22			32			N/A			LIMITATION					N/A		LIMITACION					N/A
49		ID_INFO23			256			N/A			All output signal values must be of the type enum, and it can't be an alarm signal.	N/A	Todas las señales de salida deben ser de tipo enum y no una señal de alarma	N/A
50		ID_INFO24			128			N/A			LT and GT's Input1 value type must be F,U or L type.	N/A	Los valores de entrada para LT y GT deben ser de tipo float, o enteros		N/A
51		ID_Delete2			16			N/A			Delete						N/A		Borrar						N/A




[p40_cfg_plc_Popup.htm:Number]
67

[p40_cfg_plc_Popup.htm]
#Sequence ID	RES_ID				MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_TITLE			32			N/A			PLC Configuration			N/A		CONFIG PLC		N/A
2		ID_Operator			16			N/A			Operator			N/A		Operador		N/A
3		ID_Input1			16			N/A			Input1				N/A		Entrada1		N/A
4		ID_Input2			16			N/A			Input2				N/A		Entrada2		N/A
5		ID_Param1			16			N/A			Parameter1			N/A		Param1			N/A
6		ID_Param2			16			N/A			Parameter2			N/A		Param1			N/A
7		ID_Output			16			N/A			Output				N/A		Salida			N/A
8		ID_Signal1			16			N/A			Signal				N/A		Señal			N/A
9		ID_Register1			16			N/A			Register			N/A		Registro		N/A
10		ID_Signal2			16			N/A			Signal				N/A		Señal			N/A
11		ID_Register2			16			N/A			Register			N/A		Registro		N/A
12		ID_Signal3			16			N/A			Signal				N/A		Señal			N/A
13		ID_Register3			16			N/A			Register			N/A		Registro		N/A
14		ID_EquipName/Register1		32			N/A			Equipment Name/Register		N/A		NombreEquipo/Registro	N/A
15		ID_SignalType1			16			N/A			Signal Type			N/A		Tipo Señal		N/A
16		ID_SignalName1			16			N/A			Signal Name			N/A		Nombre Señal		N/A
17		ID_EquipName/Register2		32			N/A			Equipment Name/Register		N/A		NombreEquipo/Registro	N/A
18		ID_SignalType2			16			N/A			Signal Type			N/A		Tipo Señal		N/A
19		ID_SignalName2			16			N/A			Signal Name			N/A		Nombre Señal		N/A
20		ID_EquipName/Register3		32			N/A			Equipment Name/Register		N/A		NombreEquipo/Registro	N/A
21		ID_SignalType3			16			N/A			Signal Type			N/A		Tipo Señal		N/A
22		ID_SignalName3			16			N/A			Signal Name			N/A		Nombre Señal		N/A
23		ID_Sampling1			16			N/A			Sampling			N/A		Muestreo		N/A
24		ID_Control1			16			N/A			Control				N/A		Control			N/A
25		ID_Setting1			16			N/A			Setting				N/A		Ajuste			N/A
26		ID_Alarm1			16			N/A			Alarm				N/A		Alarma			N/A
27		ID_Sampling2			16			N/A			Sampling			N/A		Muestreo		N/A
28		ID_Control2			16			N/A			Control				N/A		Control			N/A
29		ID_Setting2			16			N/A			Setting				N/A		Ajuste			N/A
30		ID_Alarm2			16			N/A			Alarm				N/A		Alarma			N/A
31		ID_Sampling3			16			N/A			Sampling			N/A		Muestreo		N/A
32		ID_Control3			16			N/A			Control				N/A		Control			N/A
33		ID_Setting3			16			N/A			Setting				N/A		Ajuste			N/A
34		ID_ADD				16			N/A			Add				N/A		AÑADIR			N/A
35		ID_CANCEL			16			N/A			Cancel				N/A		CANCELAR		N/A
36		ID_OUTPUT_ERROR1		32			N/A			Output Error			N/A		Error de Salida		N/A
37		ID_PARAM1_ERROR			32			N/A			Parameter1 Error		N/A		Error parámetro 1	N/A
38		ID_OUTPUT_ERROR2		32			N/A			Output Error			N/A		Error de Salida		N/A
39		ID_ALL_ERROR1			32			N/A			Input/Output Error		N/A		Error Entrada/Salida	N/A
40		ID_INPUT1_ERROR1		32			N/A			Input1 Error			N/A		Error Entrada 1		N/A
41		ID_INPUT2_ERROR1		32			N/A			Input2 Error			N/A		Error Entrada 2		N/A
42		ID_OUTPUT_ERROR3		32			N/A			Output Error			N/A		Error de Salida		N/A
43		ID_ALL_ERROR2			32			N/A			Input/Output Error		N/A		Error Entrada/Salida	N/A
44		ID_INPUT1_ERROR2		32			N/A			Input1 Error			N/A		Error Entrada 1		N/A
45		ID_INPUT2_ERROR2		32			N/A			Input2 Error			N/A		Error Entrada 2		N/A
46		ID_OUTPUT_ERROR4		32			N/A			Output Error			N/A		Error de Salida		N/A
47		ID_ALL_ERROR3			32			N/A			Input/Output Error		N/A		Error Entrada/Salida	N/A
48		ID_INPUT1_ERROR3		32			N/A			Input1 Error			N/A		Error Entrada 1		N/A
49		ID_OUTPUT_ERROR5		32			N/A			Output Error			N/A		Error de Salida		N/A
50		ID_ALL_ERROR4			32			N/A			Input/Output Error		N/A		Error Entrada/Salida	N/A
51		ID_INPUT1_ERROR4		32			N/A			Input1 Error			N/A		Error Entrada 1		N/A
52		ID_INPUT2_ERROR3		32			N/A			Input1 Error			N/A		Error Entrada 2		N/A
53		ID_OUTPUT_ERROR6		32			N/A			Output Error			N/A		Error de Salida		N/A
54		ID_ALL_ERROR5			32			N/A			Input/Output Error		N/A		Error Entrada/Salida	N/A
55		ID_INPUT1_ERROR5		32			N/A			Input1 Error			N/A		Error Entrada 1		N/A
56		ID_OUTPUT_ERROR7		32			N/A			Output Error			N/A		Error de Salida		N/A
57		ID_PARAM1_ERROR2		32			N/A			Parameter1 Error		N/A		Error parámetro 1	N/A
58		ID_PARAM2_ERROR1		32			N/A			Parameter2 Error		N/A		Error parámetro 2	N/A
59		ID_ALL_ERROR6			32			N/A			Input/Output Error		N/A		Error Entrada/Salida	N/A
60		ID_INPUT1_ERROR6		32			N/A			Input1 Error			N/A		Error Entrada 1		N/A
61		ID_OUTPUT_ERROR8		32			N/A			Output Error			N/A		Error de Salida		N/A
62		ID_PARAM1_ERROR3		32			N/A			Parameter1 Error		N/A		Error parámetro 1	N/A
63		ID_PARAM2_ERROR2		32			N/A			Parameter2 Error		N/A		Error parámetro 2	N/A
64		ID_ALL_ERROR7			32			N/A			Input/Output Error		N/A		Error Entrada/Salida	N/A
65		ID_INPUT1_ERROR7		32			N/A			Input1 Error		N/A		Error Entrada 1		N/A
66		ID_OUTPUT_ERROR9		32			N/A			Output Error		N/A		Error de Salida		N/A
67		ID_PARAM1_ERROR4		32			N/A			Parameter1 Error		N/A		Error parámetro 1	N/A


[p41_edit_config_alarmReg.htm:Number]
14


[p41_edit_config_alarmReg.htm]
1		ID_TITLE1			32			N/A			Please select standard equipment:			N/A		Seleccionar Equipo			N/A
2		ID_Choice			32			N/A			Please Select				N/A		Seleccione por favor			N/A
3		ID_Choice2			32			N/A			Please Select				N/A		Seleccione por favor			N/A
4		ID_TITLE2			32			N/A			Alarm Relay Configuration		N/A		Configuración Relés de alarma		N/A
5		ID_STDEQUIPNAME			32			N/A			Standard Equipment Name			N/A		Nombre de Equipo			N/A
6		ID_AlarmID			32			N/A			Alarm Signal ID				N/A		ID Señal de Alarma			N/A
7		ID_AlarmName			32			N/A			Alarm Signal Name			N/A		Nombre Señal de Alarma			N/A
8		ID_AlarmReg			32			N/A			Alarm Relay Number			N/A		Número de Relé de Alarma		N/A
9		ID_Reg				32			N/A			New Relay Number			N/A		Nuevo número de Relé			N/A
10		ID_Edit				16			N/A			Modify					N/A		Modificar				N/A
11		ID_ERROR5			64			N/A			Alarm relay modified error.		N/A		Error al modificar relé de alarma	N/A
12		ID_ERROR6			64			N/A			Alarm relay modified successfully.	N/A		Relé de alarma modificado con éxito!	N/A
13		ID_ERROR4			64			N/A			Failed. No authority.	N/A		Fallo. No está autorizado!		N/A
14		ID_Edit				16			N/A			Modify					N/A		Modificar				N/A



[p42_edit_config_alarm.htm:Number]
19

[p42_edit_config_alarm.htm]
1		ID_Edit				16			N/A			Modify							N/A		Modificar			N/A
2		ID_TITLE1			32			N/A			Please select standard equipment:				N/A		Seleccione Equipo		N/A
3		ID_TITLE2			32			N/A			Alarm Suppression					N/A		Supresión de Alarmas		N/A
4		ID_AlarmID			32			N/A			Alarm Signal ID						N/A		ID Señal de Alarma		N/A
5		ID_AlarmName			32			N/A			Alarm Signal Name					N/A		Nombre de Señal de Alarma	N/A
6		ID_AlarmSuppress		32			N/A			Alarm Suppression Expression				N/A		Expresión de Supresión		N/A
7		ID_TITLE3			64			N/A			How many signals are there in the Alarm Suppression Expression:		N/A		Número de señales en la expresión de supresión de alarmas:	N/A
8		ID_Choice			32			N/A			Please Select						N/A		Seleccionar, por favor		N/A
9		ID_TITLE4			32			N/A			Alarm Suppression Expression:				N/A		Expresión de Supresión:	N/A
10		ID_Submit			16			N/A			Submit							N/A		Aceptar				N/A
11		ID_Cancel			16			N/A			Cancel							N/A		Cancelar			N/A
12		ID_STDEQUIPNAME			32			N/A			Standard Equipment Name					N/A		Nombre de Equipo Estándar	N/A
13		ID_Choice2			32			N/A			Please Select						N/A		Seleccionar, por favor		N/A
14		ID_ERROR5			64			N/A			Alarm suppression expression modified error.		N/A		Error en Expresión de Supresión de Alarmas		N/A
15		ID_ERROR6			128			N/A			Alarm suppression expression modified successfully. \nController must be restarted.	N/A	Expresión de supresión modificada con éxito,\nDebe reiniciar el sistema para que los cambios tengan efecto.		N/A
16		ID_OPERATOR_ERROR1		32			N/A			Operator Error						N/A		Error de operador	N/A
17		ID_OPERATOR_ERROR2		32			N/A			Operator Error						N/A		Error de operador	N/A
18		ID_SELF_ERROR			64			N/A			Do not suppress the Signal by itself.	N/A		Una señal no se puede suprimir a si misma	N/A
19		ID_ERROR4			64			N/A			Failed. No authority.			N/A		Fallo, no dispone de la autorización requerida!		N/A




[p43_ydn_config.htm:Number]
41


[p43_ydn_config.htm]
1		ID_ERROR0		64			N/A			Success.						N/A			Éxito!						N/A
2		ID_ERROR1		64			N/A			Failed.						N/A			Fallo!						N/A
3		ID_ERROR2		64			N/A			Failed. YDN23 service was exited.			N/A			Fallo, servicio YDN23 parado			N/A
4		ID_ERROR3		64			N/A			Failed. Invalid parameter.			N/A			Fallo, parámetro no válido!			N/A
5		ID_ERROR4		64			N/A			Failed. Invalid data.				N/A			Fallo, datos no válidos!			N/A
6		ID_ERROR5		64			N/A			Cannot be modified. Controller is hardware protected.	N/A			ACU protegida por HW. No se puede modificar	N/A
7		ID_ERROR6		64			N/A			Service is busy. Cannot change configuration.	N/A			Servicio ocupado. Ahora no se puede cambiar la configuración	N/A
8		ID_ERROR7		64			N/A			Non-shared port has already been occupied.		N/A			El puerto no compartido ya está ocupado		N/A
9		ID_ERROR8		64			N/A			Failed. No authority.			N/A			Fallo. No tiene autorización!			N/A
10		ID_YDN_HEAD		64			N/A			Background Protocol Configuration Modification.			N/A			Configuración protocolo secundario		N/A
11		ID_PROTOCOL_TYPE	32			N/A			Protocol Type						N/A			Tipo de protocolo				N/A
12		ID_PROTOCOL_MEDIA	32			N/A			Port Type						N/A			Tipo de puerta					N/A
13		ID_REPORT_IN_USER	32			N/A			Alarm Reporting						N/A			Informe de alarmas habilitado			N/A
14		ID_MAX_ALARM_REPORT	32			N/A			Times of Dialing Attempt				N/A			Núm Max Intentos Informe de Alarmas		N/A
15		ID_RANGE_FROM		32			N/A			Range							N/A			Rango						N/A
16		ID_CALL_ELAPSE_TIME	32			N/A			Interval between Two Dialings				N/A			Tiempo entre llamadas				N/A
17		ID_RANGE_FROM		32			N/A			Range							N/A			Rango						N/A
18		ID_MAIN_REPORT_PHONE	32			N/A			First Report Phone Number				N/A			Teléfono 1					N/A
19		ID_SECOND_REPORT_PHONE	32			N/A			Second Report Phone Number				N/A			Teléfono 2					N/A
20		ID_CALLBACK_PHONE	32			N/A			Third Report Phone Number				N/A			Teléfono 3					N/A
21		ID_COMMON_PARAM		64			N/A			Port Parameter						N/A			Parámetro de puerto				N/A
22		ID_MODIFY		32			N/A			Modify							N/A			Modificar					N/A
23		ID_PROTOCOL0		16			N/A			YDN23							N/A			YDN23						N/A
24		ID_PROTOCOL1		16			N/A			RSOC							N/A			RSOC						N/A
25		ID_PROTOCOL2		16			N/A			SOC/TPE							N/A			SOC/TPE						N/A
26		ID_MEDIA0		16			N/A			RS-232							N/A			RS-232						N/A
27		ID_MEDIA1		16			N/A			Modem							N/A			Modem						N/A
28		ID_MEDIA2		16			N/A			Ethernet						N/A			Ethernet					N/A
29		ID_TIPS8		128			N/A			Maximum alarm report alarm attempt is error.		N/A			Núm Max Intentos Informe Alarmas incorrecto	N/A
30		ID_TIPS9		128			N/A			Maximum call elapse time is error.		N/A			Tiempo entre llamadas incorrecto		N/A
31		ID_TIPS10		128			N/A			Main report phone number is input error.			N/A			Teléfono principal de Informe de Alarmas incorrecto.	N/A
32		ID_TIPS11		128			N/A			Second report phone number is input error.		N/A			Teléfono secundario de Informe de Alarmas incorrecto.	N/A
33		ID_TIPS12		128			N/A			Callback report phone number is input error.		N/A			Teléfono Retrollamada incorrecto.		N/A
34		ID_TIPS21		64			N/A			Maximum alarm report attempt is error.			N/A			Núm Intentos Informe Alarmas incorrecto		N/A
35		ID_TIPS22		64			N/A			Maximum call elapse time is error.		N/A			Núm Intentos Informe Alarmas incorrecto		N/A
36		ID_TIPS23		64			N/A			Input Error					N/A			Error de entrada				N/A
37		ID_TIPS24		64			N/A			Port Input Error					N/A			Puerto incorrecto				N/A
38		ID_CCID			16			N/A			Self Address						N/A			Dirección propia				N/A
39		ID_RANGE_FROM		32			N/A			Range							N/A			Rango						N/A
40		ID_TIPS6		128			N/A			Address error, input number please.		N/A			Dirección no válida. Entre un número, por favor!	N/A
41		ID_NO_PROTOCOL_TIPS	128			N/A			Please input protocol.					N/A			Entre protocolo, por favor!			N/A







[p47_web_title.htm:Number]
0

[p47_web_title.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[p78_get_setting_param.htm:Number]
8

[p78_get_setting_param.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN										ABBR_IN_EN	FULL_IN_LOCALE							ABBR_IN_LOCALE
1		ID_HEAD			32			N/A			Retrieve 'SettingParam.run' File									N/A		Guardar configuración						N/A
2		ID_CLOSE_ACU		32			N/A		Retrieve File										N/A		Guardar configuración						N/A
3		ID_ERROR0		32			N/A			Unknown error.										N/A		Error desconocido!						N/A
4		ID_ERROR1		128			N/A			Retrieval successful.						N/A		La configuración de parámetros fue salvada con éxito		N/A
5		ID_ERROR2		64			N/A			Failed to get.						N/A		Error al obtener datos!						N/A
6		ID_ERROR3		64			N/A			You do not have authority to stop the controller.					N/A		No tiene autorización para cerrar la ACU!			N/A
7		ID_ERROR4		64			N/A			Failed to communicate with the controller.						N/A		Fallo de comunicación con la ACU				N/A
8		ID_TIPS			128			N/A			Retrieve the current settings of the Controller's adjustable parameters.	N/A		A través de esta función el usuario puede salvar la configuación actual de la ACU.		N/A

[p79_site_map.htm:Number]
29

[p79_site_map.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE				ABBR_IN_LOCALE
1		ID_SITE_MAP		16			N/A			Site Map				N/A		Mapa del sitio				N/A
2		ID_SITE_MAP		16			N/A			Site Map				N/A		Mapa del sitio				N/A
3		ID_MODIFY_SITE_INFO	32			N/A			Modify Site Information			N/A		Modificar información del sitio		N/A
4		ID_MODIFY_DEVICE_INFO	32			N/A			Modify Device Information		N/A		Modificar info de dispositivo		N/A
5		ID_MODIFY_ALARM_INFO	32			N/A			Modify Alarm Information		N/A		Modificar información de alarma		N/A
6		ID_DEVICE_EXPLORE	16			N/A			DEVICE EXPLORE				N/A		Explorar aparato			N/A
7		ID_SYSTEM		16			N/A			SETTINGS				N/A		Configuración				N/A
8		ID_NETWORK_SETTING	32			N/A			Network Configuration			N/A		Configuración de Red			N/A
9		ID_NMS_SETTING		16			N/A			NMS Configuration			N/A		Configuración NMS			N/A
10		ID_ESR_SETTING		16			N/A			ESR Configuration			N/A		Configuración MC			N/A
11		ID_USER			64			N/A			User Information Configuration		N/A		Configuración info de usuario		N/A
12		ID_MAINTENANCE		16			N/A			MAINTENANCE				N/A		Mantenimiento				N/A
13		ID_FILE_MANAGE		32			N/A			Download				N/A		Descargar				N/A
14		ID_MODIFY_CFG		64			N/A			Modify configuration online.		N/A		Modificar config online			N/A
15		ID_TIME_CFG		64			N/A			Time Synchronization			N/A		Fijar Fecha y hora			N/A
16		ID_QUERY		16			N/A			QUERY					N/A		Consulta				N/A
17		ID_ALARM		32			N/A			ALARMS					N/A		Alarmas					N/A
18		ID_ACTIVE_ALARM		32			N/A			Active Alarms				N/A		Activas					N/A
19		ID_HISTORY_ALARM	32			N/A			Alarm History				N/A		Histórico				N/A
20		ID_QUERY_HIS_DATA	32			N/A			Data History				N/A		Datos históricos			N/A
21		ID_QUERY_LOG_DATA	32			N/A			Log Data				N/A		Datos registro				N/A
22		ID_QUERY_BATT_DATA	32			N/A			Battery Test Data			N/A		Datos prueba batería			N/A
23		ID_CLEAR_DATA		32			N/A			Clear Data				N/A		Borrar datos				N/A
24		ID_EDIT_CONFIGFILE	32			N/A			Edit Configuration File			N/A		Editar Archivo de Configuración		N/A
25		ID_CONFIG_ALARMSUPEXP	32			N/A			Alarm Suppression Expression Configuration		N/A		Configuración Supresión de Alarmas	N/A
26		ID_CONFIG_ALARMRELAY	32			N/A			Alarm Relay Configuration		N/A		Configuración Relés de Alarma		N/A
27		ID_CONFIG_PLC		32			N/A			PLC Configuration			N/A		Editar configuración PLC		N/A
28		ID_YDN_SETTING		32			N/A			HLMS Configuration			N/A		Configuración HLMS			N/A
29		ID_NMSV3_SETTING	16			N/A			NMSV3 Configuration			N/A		Configuración NMS V3			N/A





[p80_status_view.htm:Number]
1

[p80_status_view.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_LEAVE		8			N/A			Logout				N/A		Salir			N/A

[p81_user_def_page.htm:Number]
28


[p81_user_def_page.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN	FULL_IN_LOCALE					ABBR_IN_LOCALE
1		ID_INDEX	16				N/A			Equipment Name					N/A		Dispositivo					N/A
2		ID_SIGNAL_NAME	16				N/A			Signal Name					N/A		Nombre de Señal					N/A
3		ID_SIGNAL_VALUE	16				N/A			Value						N/A		Valor						N/A
4		ID_SIGNAL_UNIT	16				N/A			Unit						N/A		Unidad						N/A
5		ID_SAMPLE_TIME	16				N/A			Time						N/A		Modificado					N/A
6		ID_SET_VALUE	16				N/A			Set Value					N/A		Fijar valor					N/A
7		ID_SET		16				N/A			Set						N/A		Fijar						N/A
8		ID_ERROR0	32				N/A			Failed.						N/A		Fallo!						N/A
9		ID_ERROR1	32				N/A			Successful.					N/A		Éxito						N/A
10		ID_ERROR2	64				N/A			Failed. Conflicting setting.			N/A		Conflicto en configuración			N/A
11		ID_ERROR3	32				N/A			Failed. No authority.		N/A		Error, no tiene la autorización requerida!	N/A
12		ID_ERROR4	64				N/A			No information to send.				N/A		No hay información que enviar			N/A
13		ID_ERROR5	128				N/A			Failed. Controller is hardware protected.	N/A		Fallo, ACU protegida por HW			N/A
14		ID_SET_TYPE	16				N/A			Set						N/A		Fijar						N/A
15		ID_SHOW_TIPS0	64				N/A			Greater than the maximum value.			N/A		Supera valor máximo				N/A
16		ID_SHOW_TIPS1	64				N/A			Less than the minimum value.			N/A		Menor que el mínimo				N/A
17		ID_SHOW_TIPS2	64				N/A			Cannot be null.				N/A		No puede ser nulo				N/A
18		ID_SHOW_TIPS3	64				N/A			Input number please.				N/A		Entre número, por favor			N/A
19		ID_SHOW_TIPS4	64				N/A			The control value is equal to the last value.	N/A		El valor de control es igual al último		N/A
20		ID_SHOW_TIPS5	64				N/A			Failed. No authority.		N/A		Error, no tiene la autorización requerida!	N/A
21		ID_TIPS1	64				N/A			Send modification commands.			N/A		Resultado:					N/A
22		ID_SAMPLER	16				N/A			Sampler							N/A		Muestreo					N/A
23		ID_CHANNEL	16				N/A			Channel						N/A		Canal						N/A
24		ID_MONTH_ERROR	32				N/A			Incorrect month.				N/A		Mes incorrecto!					N/A
25		ID_DAY_ERROR	32				N/A			Incorrect day.					N/A		Día incorrecto!					N/A
26		ID_HOUR_ERROR	32				N/A			Incorrect hour.					N/A		Hora incorrecto!				N/A
27		ID_FORMAT_ERROR	64				N/A			Incorrect format.				N/A		Formato incorrecto.				N/A
28		ID_MENU_PAGE	32				N/A			/cgi-bin/eng/					N/A		/cgi-bin/loc/					N/A



[alai_tree.js:Number]
0

[alai_tree.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[j09_alai_tree_help.js:Number]
0

[j09_alai_tree_help.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[p44_cfg_powersplite.htm:Number]
37

[p44_cfg_powersplite.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE			ABBR_IN_LOCALE
1		ID_Master		32		N/A			Master				N/A		Maestro				N/A
2		ID_Slave		32		N/A			Slave				N/A		Esclavo				N/A
3		ID_CFGMode		32		N/A			Modify				N/A		Modificar			N/A
4		ID_TITLE2		32		N/A			PowerSplit Configuration	N/A		Configuración PowerSplit	N/A
5		ID_PSSigName		32		N/A			Signal Name			N/A		Nombre de Señal			N/A
6		ID_EquipName		32		N/A			Equipment Name			N/A		Nombre de Equipo		N/A
7		ID_SigType		32		N/A			Signal Type			N/A		Tipo de Señal			N/A
8		ID_SigName		32		N/A			Signal Name			N/A		Nombre de Señal			N/A
9		ID_TITLE3		32		N/A			PowerSplit Configuration	N/A		Configuración PowerSplit	N/A
10		ID_PSSigName1		32		N/A			Signal Name			N/A		Nombre de Señal			N/A
11		ID_EquipName1		32		N/A			Equipment Name			N/A		Nombre de Equipo		N/A
12		ID_SigType1		32		N/A			Signal Type			N/A		Tipo de Señal			N/A
13		ID_SigName1		32		N/A			Signal Name			N/A		Nombre de Señal			N/A
14		ID_CFGSubmit		32		N/A			Submit				N/A		Enviar				N/A
15		ID_CFGCancel		32		N/A			Cancel				N/A		Cancelar			N/A
16		ID_PS_MODE		32		N/A			PowerSplit Mode			N/A		Modo PowerSplit			N/A
17		ID_EQUIP_ERROR		32		N/A			Equipment Name Error		N/A		Nombre Equipo erróneo		N/A
18		ID_TYPE_ERROR		32		N/A			Signal Type Error		N/A		Tipo de Señal errónea		N/A
19		ID_SIG_ERROR		32		N/A			Signal Name Error		N/A		Nombre de Señal erróneo		N/A
20		ID_Sampling		32		N/A			Sampling			N/A		Muestreo			N/A
21		ID_Control		32		N/A			Control				N/A		Control				N/A
22		ID_Setting		32		N/A			Setting				N/A		Configuración			N/A
23		ID_Sampling1		32		N/A			Sampling			N/A		Muestreo			N/A
24		ID_Control1		32		N/A			Control				N/A		Control				N/A
25		ID_Setting1		32		N/A			Setting				N/A		Configuración			N/A
26		ID_Alarm		32		N/A			Alarm				N/A		Alarma				N/A
27		ID_Alarm1		32		N/A			Alarm				N/A		Alarma				N/A
28		ID_EDIT			32		N/A			Edit				N/A		Editar				N/A
29		ID_ERROR5		64		N/A			Powersplit modified error.	N/A		Error modificación PowerSplit	N/A
30		ID_ERROR6		128		N/A			PowerSplit modified successfully. \nThe system must be restarted.	N/A	PowerSplit modificado con éxito.\nDebe reiniciar el sistema para que tome efecto!	N/A
31		ID_ERROR0		64		N/A			Power Split configure error	N/A		Error archivo configuración PowerSplit!		N/A
32		ID_ERROR1		128		N/A			Unknow error.			N/A		Error desconocido!		N/A
33		ID_EXPLAIN		128		N/A			Other PowerSplit setting signals are here.	N/A	Otros ajustes de señales del PowerSplit se encuentran aquí.	N/A
34		ID_DIR			32		N/A			./eng				N/A		./loc				N/A
35		ID_CONFIRM_1		64		N/A			Are you sure?			N/A		Está seguro?			N/A
36		ID_SYSTEM		32		N/A			Power System			N/A		Sistema				N/A
37		ID_NO_SIGNAL_ALERT	64		N/A			No such signal.			N/A		Señal no válida.		N/A

[p77_auto_config.htm:Number]
9

[p77_auto_config.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN							ABBR_IN_EN	FULL_IN_LOCALE			ABBR_IN_LOCALE
1		ID_HEAD			32			N/A			Auto Configuration						N/A		Autoconfiguración		N/A
2		ID_CLOSE_ACU		32			N/A			Auto Configuration						N/A		Autoconfiguración		N/A
3		ID_ERROR0		32			N/A			Unknown error.							N/A		Error desconocido!		N/A
4		ID_ERROR1		128			N/A			Auto configuration started. Please wait.			N/A		Se ha iniciado la Autoconfiguración. Espere, por favor!		N/A
5		ID_ERROR2		64			N/A			Failed to get.						N/A		Fallo en recuperación		N/A
6		ID_ERROR3		64			N/A			You do not have authority to stop the controller.		N/A		No tiene autorización para parar la ACU!			N/A
7		ID_ERROR4		64			N/A			Failed to communicate with the controller.			N/A		Fallo de comunicación con la ACU		N/A
8		ID_AUTO_CONFIG		256			N/A		Controller will auto configure. The Web browser will be closed. Please wait a moment (about 2-5 minutes).	N/A	Autoconfiguración en proceso. Se va a cerrar el navegador. Espere de 2 a 5 minutos!		N/A
9		ID_TIPS			128			N/A			This function allows a User to automatically configure the controller.		N/A		A través de esta función puede iniciar el proceso de Autoconfiguración		N/A

[copyright.htm:Number]
0

[copyright.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[global.css:Number]
0

[global.css]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[header.htm:Number]
3

[header.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_SITE		16			N/A			Site		N/A		Central			N/A
2		ID_LOGIN	16			N/A			./eng/		N/A		./loc/			N/A
3		ID_LOGOUT	16			N/A			LOGOUT		N/A		SALIR			N/A


[p01_home.htm:Number]
0

[p01_home.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE


[p01_home_index.htm:Number]
12

[p01_home_index.htm]
#Sequence ID	RES_ID			MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_EVENT_STATUS		32			N/A			Event Status		N/A		Situación		N/A
2		ID_OUT_VOLTAGE		32			N/A			Output Voltage		N/A		Tensión de salida	N/A
3		ID_OUT_CURRENT		32			N/A			Output Current		N/A		Corriente Salida	N/A
4		ID_BATT_STATUS		32			N/A			Battery Status		N/A		Estado Batería		N/A
5		ID_AMB_TEMP		14			N/A			Ambient Temp		N/A		Temp ambiente		N/A
6		ID_LOAD_TREND		32			N/A			Load Trend		N/A		Tendencia Carga		N/A
7		ID_TIME			32			N/A			Time			N/A		Tiempo			N/A
8		ID_PEAK_CURRENT		16			N/A			Peak Current		N/A		Pico Corriente		N/A
9		ID_AVERAGE_CURRENT	20			N/A			Average Current	N/A		Media diaria		N/A
10		ID_R			32			N/A			R			N/A		R			N/A
11		ID_S			32			N/A			S			N/A		S			N/A
12		ID_T			32			N/A			T			N/A		T			N/A


[p01_home_title.htm:Number]
1

[p01_home_title.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_SYS_STATUS	32			N/A			System Status	N/A		Estado Sistema		N/A


[p_main_menu.html:Number]
0

[p_main_menu.html]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[excanvas.js:Number]
0

[excanvas.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[jquery.emsMeter.js:Number]
0

[jquery.emsMeter.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[jquery.emsplot.js:Number]
1

[jquery.emsplot.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_NO_DATA	64			N/A			There is no data to show!	N/A		No hay datos!		N/A

[jquery.emsplot.style.css:Number]
0

[jquery.emsplot.style.css]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[jquery.emsThermometer.js:Number]
0

[jquery.emsThermometer.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[jquery.min.js:Number]
0

[jquery.min.js]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN	ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE

[line_data.htm:Number]
1

[line_data.htm]
#Sequence ID	RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN	FULL_IN_LOCALE		ABBR_IN_LOCALE
1		ID_NO_DATA	64			N/A			There is no data to show!	N/A		No hay datos!		N/A


[p12_nmsv3_config.htm:Number]
50

[p12_nmsv3_config.htm]
#Sequence ID	RES_ID				MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN										ABBR_IN_EN		FULL_IN_LOCALE						ABBR_IN_LOCALE
1		ID_ERROR0			16			N/A			Unknown error.										N/A			Error desconocido!					N/A
2		ID_ERROR1			64			N/A			Failed. The NMS already exists.								N/A			Error. El servidor NMS ya existe			N/A
3		ID_ERROR2			32			N/A			Successful											N/A			Exito							N/A
4		ID_ERROR3			64			N/A			Failed. Incomplete information.							N/A			Fallo, información incompleta!				N/A
5		ID_ERROR4			64			N/A			Failed. No authority.							N/A			Fallo. No tiene la autorización requerida		N/A
6		ID_ERROR5			64			N/A			Cannot be modified. Controller is hardware protected.					N/A			ACU protegida por HW. No se puede modificar!		N/A
7		ID_ERROR6			64			N/A			Failed. Maximum number exceeded.						N/A			Fallo: Máximo excedido					N/A
8		ID_NMS_HEAD1			32			N/A			NMSV3 Configuration									N/A			Configuración NMS V3					N/A
9		ID_NMS_HEAD2			32			N/A			Current NMS										N/A			NMS actual						N/A
10		ID_NMS_IP			16			N/A			NMS IP											N/A			IP NMS							N/A
11		ID_NMS_AUTHORITY		16			N/A			Authority										N/A			Autoridad						N/A
12		ID_NMS_TRAP			32			N/A			Accepted Trap Level									N/A			Aceptar nivel Trap					N/A
13		ID_NMS_IP			16			N/A			NMS IP											N/A			IP NMS							N/A
14		ID_NMS_AUTHORITY		16			N/A			Authority										N/A			Autoridad						N/A
15		ID_NMS_TRAP			32			N/A			Accepted Trap Level									N/A			Aceptar nivel Trap					N/A
16		ID_NMS_ADD			16			N/A			Add New NMS										N/A			Añadir nuevo NMS					N/A
17		ID_NMS_MODIFY			32			N/A			Modify NMS										N/A			Modificar NMS						N/A
18		ID_NMS_DELETE			32			N/A			Delete NMS										N/A			Borrar NMS						N/A
19		ID_NMS_PUBLIC			32			N/A			Public Community									N/A			Comunidad pública					N/A
20		ID_NMS_PRIVATE			32			N/A			Private Community									N/A			Comunidad privada					N/A
21		ID_NMS_LEVEL0			16			N/A			Not Used										N/A			No utilizado						N/A
22		ID_NMS_LEVEL1			16			N/A			No Access										N/A			Sin acceso						N/A
23		ID_NMS_LEVEL2			32			N/A			Query Authority										N/A			Autoridad de consulta					N/A
24		ID_NMS_LEVEL3			32			N/A			Control Authority									N/A			Autoridad de Control					N/A
25		ID_NMS_LEVEL4			32			N/A			Administrator										N/A			Administrador						N/A
26		ID_NMS_TRAP_LEVEL0		16			N/A			NoAuthNoPriv										N/A			NoAuthNoPriv						N/A
27		ID_NMS_TRAP_LEVEL1		16			N/A			AuthNoPriv										N/A			AuthNoPriv						N/A
28		ID_NMS_TRAP_LEVEL2		16			N/A			AuthPriv										N/A			AuthPriv							N/A
29		ID_NMS_TRAP_LEVEL3		16			N/A			Critical Alarms										N/A			A1							N/A
30		ID_NMS_TRAP_LEVEL4		16			N/A			No Trap										N/A			No hay Traps						N/A
31		ID_TIPS0			128			N/A			Incorrect IP address of NMS. \nShould be in format 'nnn.nnn.nnn.nnn'. \nExample 10.76.8.29	N/A			Dirección IP de NMS incorrecta.\nFormato 'nnn.nnn.nnn.nnn'\ne.g.: 10.76.8.29	N/A
32		ID_TIPS1			128			N/A			Priv Password DES or Auth Password MD5 cannot be empty. Please try again.		N/A			Contraseña no válida. Pruebe otra vez.			N/A
33		ID_TIPS2			128			N/A			Already exists. Please try again.							N/A			Ya existe, pruebe otra vez.				N/A
34		ID_TIPS3			128			N/A			Does not exist. Cannot be modified. Please try again.				N/A			No existe. No se puede modificar. Inténtelo de nuevo.				N/A
35		ID_TIPS4			128			N/A			Please select one or more NMS before clicking this button.				N/A			Por favor, seleccione uno o más NMS antes de pulsar	N/A
36		ID_TIPS5			128			N/A			NMS Info Configuration									N/A			Configuración información NMS				N/A
41		ID_TIPS6			128			N/A			User name can't be null									N/A			El nombre de usuario no puede ser nulo.					N/A
42		ID_NMS_USERNAME			128			N/A			User Name										N/A			Nombre de Usuario							N/A
43		ID_NMS_TRAP_IP			128			N/A			Trap IP Address										N/A			Dirección IP de Trap							N/A
44		ID_NMS_TRAP_LEVEL		128			N/A			Trap Security Level									N/A			Nivel de Seguiridad de Trap					N/A
45		ID_NMS_DES			128			N/A			Priv Password DES									N/A			Contraseña DES Priv					N/A
46		ID_NMS_MD5			128			N/A			Auth Password MD5									N/A			Contraseña Autoriz MD5					N/A
47		ID_NMS_ENGINID			128			N/A			Trap Engine ID										N/A			ID de máquina Trap						N/A
48		ID_NMS_USERNAME_DISPLAY		128			N/A			User Name										N/A			Nombre de Usuario							N/A
49		ID_TRAP_IP_DISPLAY		128			N/A			Trap IP Address										N/A			Dirección IP de Trap							N/A
50		ID_NMS_DES_DISPLAY		128			N/A			Priv Password DES									N/A			Contraseña DES Priv					N/A
51		ID_NMS_MD5_DISPLAY		128			N/A			Auth Password MD5									N/A			Contraseña Autoriz MD5					N/A
52		ID_NMS_TRAP_ENGINE		128			N/A			Trap Engine ID										N/A			ID de máquina Trap							N/A
53		ID_NMS_TRAP_LEVE_DISPLAY	128			N/A			Trap Security Level									N/A			Nivel de Seguiridad de Trap						N/A
54		ID_ERROR7			128			N/A			Do not support SNMPV3										N/A			SNMPV3 no soportado					N/A


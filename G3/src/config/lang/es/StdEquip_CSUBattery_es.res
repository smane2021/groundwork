﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			Battery Current				Battery Current		Corriente Batería		Corriente Bat
2		32			15			Battery Rating(Ah)			Batt Rating(Ah)		Capacidad (Ah)			Capacidad (Ah)
3		32			15			Battery Current Limit Exceeded		Ov Bat Cur Lmt	Limite corriente excedido	Lim corr pasado
4		32			15			CSU Battery					CSU Battery		Batería CSU			BateríaCSU
5		32			15			Over Battery Current			Over Batt Curr		Sobrecorriente			Sobrecorriente
6		32			15			Battery Capacity (%)			Batt Cap (%)	Capacidad batería (%)		Cap Bat (%)
7		32			15			Battery Voltage				Battery Voltage		Tensión batería			Tensión batería
8		32			15			Battery Low Capacity			BattLowCapacity		Baja capacidad			Baja capacidad
9		32			15			CSU Battery Temperature			CSU Batt Temp		Temperatura Batería CSU		Temp Bat CSU
10		32			15			CSU Battery Failure				CSU Batt Fail		Fallo batería CSU		Fallo bat CSU
11		32			15			Existent					Existent		Existente			Existente
12		32			15			Not Existent				Not Existent		Inexistente			Inexistente
28		32			15			Battery Management		Batt Management	Utilizada en Gestión Bat	Incl Gestión
29		32			15			Yes						Yes			Sí				Sí
30		32			15			No						No			No				No
96		32			15			Rated Capacity					Rated Capacity		Capacidad nominal C10		Capacidad C10

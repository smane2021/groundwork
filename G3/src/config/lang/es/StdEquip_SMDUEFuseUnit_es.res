﻿#
#  Locale language support:Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Fuse 1						Fuse 1				Fusible 1			Fusible 1
2	32			15			Fuse 2						Fuse 2				Fusible 2			Fusible 2
3	32			15			Fuse 3						Fuse 3				Fusible 3			Fusible 3
4	32			15			Fuse 4						Fuse 4				Fusible 4			Fusible 4
5	32			15			Fuse 5						Fuse 5				Fusible 5			Fusible 5
6	32			15			Fuse 6						Fuse 6				Fusible 6			Fusible 6
7	32			15			Fuse 7						Fuse 7				Fusible 7			Fusible 7
8	32			15			Fuse 8						Fuse 8				Fusible 8			Fusible 8
9	32			15			Fuse 9						Fuse 9				Fusible 9			Fusible 9
10	32			15			Fuse 10						Fuse 10				Fusible 10			Fusible 10
11	32			15			Off						Off				Apagado			Apagado
12	32			15			On						On				En			En
13	32			15			Communication Fail				Comm Fail			Falla de comunicación		Falla Com
14	32			15			Existence State					Existence State		Estado existencia		Estado exist
15	32			15			Comm OK						Comm OK				Com ok		Com ok
16	32			15			Communication Fail				Comm Fail		Falla Com		Falla Com
17	32			15			Existent					Existent			Existente			Existente
18	32			15			Not Existent					Not Existent			No Existe			No Existe
19	32			15			Fuse 1 Alarm					Fuse 1 Alm			Fusible 1 alarma		Fusible 1 Alm
20	32			15			Fuse 2 Alarm					Fuse 2 Alm			Fusible 2 alarma		Fusible 2 Alm
21	32			15			Fuse 3 Alarm					Fuse 3 Alm			Fusible 3 alarma		Fusible 3 Alm
22	32			15			Fuse 4 Alarm					Fuse 4 Alm			Fusible 4 alarma		Fusible 4 Alm
23	32			15			Fuse 5 Alarm					Fuse 5 Alm			Fusible 5 alarma		Fusible 5 Alm
24	32			15			Fuse 6 Alarm					Fuse 6 Alm			Fusible 6 alarma		Fusible 6 Alm
25	32			15			Fuse 7 Alarm					Fuse 7 Alm			Fusible 7 alarma		Fusible 7 Alm
26	32			15			Fuse 8 Alarm					Fuse 8 Alm			Fusible 8 alarma		Fusible 8 Alm
27	32			15			Fuse 9 Alarm					Fuse 9 Alm			Fusible 9 alarma		Fusible 9 Alm
28	32			15			Fuse 10 Alarm					Fuse 10 Alm			Fusible 10 alarma		Fusible 10 Alm
29	32			15			SMDUE1Fuse					SMDUE1Fuse			Fusible SMDUE1		Fusible SMDUE1
30	32			15			SMDUE2Fuse					SMDUE2Fuse			Fusible SMDUE2		Fusible SMDUE2
31	32			15			SMDUE3Fuse					SMDUE3Fuse			Fusible SMDUE3		Fusible SMDUE3
32	32			15			SMDUE4Fuse					SMDUE4Fuse			Fusible SMDUE4		Fusible SMDUE4
33	32			15			SMDUE5Fuse					SMDUE5Fuse			Fusible SMDUE5		Fusible SMDUE5
34	32			15			SMDUE6Fuse					SMDUE6Fuse			Fusible SMDUE6		Fusible SMDUE6
35	32			15			SMDUE7Fuse					SMDUE7Fuse			Fusible SMDUE7		Fusible SMDUE7
36	32			15			SMDUE8Fuse					SMDUE8Fuse			Fusible SMDUE8		Fusible SMDUE8








﻿#
#  Locale language support: Spanish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN			FULL_IN_LOCALE			ABBR_IN_LOCALE			
1		32			15			Last Diesel Generator Test Date		Last Test Date			Fecha última prueba GE		Ultima prueba
2		32			15			Diesel Generator Test Running		Test Running		Prueba GE en curso		Prueba GE
3		32			15			Diesel Generator Test Results		Test Results			Resultado prueba GE		Resultado GE
4		32			15			Start Diesel Generator Test		Start Test			Iniciar prueba GE		Iniciar Test
5		32			15			Diesel Generator Max Test Time		Max Test Time			Max duración prueba GE		Duración Max
6		32			15			Planned Diesel Gen Test			Planned Test			Calendario prueba activado	Plan prueba Act
7		32			15			Diesel Generator Control Inhibit	Control Inhibit				Control GE inhibido		Ctrl Inhibido
8		32			15			Diesel Generator Test Running		Test Running			Prueba GE en curso		Prueba GE
9		32			15			Diesel Generator Test Failure		Test Failure			Fallo prueba GE			Fallo Prueba
10		32			15			No					No				No				No
11		32			15			Yes					Yes				Sí			Sí
12		32			15			Reset Diesel Gen Test Error		Reset Test Err			Reset Fallo prueba GE		Res Fallo Test
13		32			15			New Diesel Generator Test Delay		New Test Delay			Retardo próxima prueba		Retardo prueba
14		32			15			Num of Scheduled Tests per Year		Yr Test Times		Num. de pruebas al año		Num. pruebas
15		32			15			Test 1 Date				Test 1 Date			Fecha prueba 1			Fecha prueba 1
16		32			15			Test 2 Date				Test 2 Date			Fecha prueba 2			Fecha prueba 2
17		32			15			Test 3 Date				Test 3 Date			Fecha prueba 3			Fecha prueba 3
18		32			15			Test 4 Date				Test 4 Date			Fecha prueba 4			Fecha prueba 4
19		32			15			Test 5 Date				Test 5 Date			Fecha prueba 5			Fecha prueba 5
20		32			15			Test 6 Date				Test 6 Date			Fecha prueba 6			Fecha prueba 6
21		32			15			Test 7 Date				Test 7 Date			Fecha prueba 7			Fecha prueba 7
22		32			15			Test 8 Date				Test 8 Date			Fecha prueba 8			Fecha prueba 8
23		32			15			Test 9 Date				Test 9 Date			Fecha prueba 9			Fecha prueba 9  
24		32			15			Test 10 Date				Test 10 Date			Fecha prueba 10			Fecha prueba 10
25		32			15			Test 11 Date				Test 11 Date			Fecha prueba 11			Fecha prueba 11
26		32			15			Test 12 Date				Test 12 Date			Fecha prueba 12			Fecha prueba 12
27		32			15			Normal					Normal				Normal				Normal
28		32			15			End by Manual				End by Manual			Parada Manual			Paro manual
29		32			15			Time is Up				Time is Up			Tiempo agotado			Tiempo agotado
30		32			15			In Manual State				In Manual State			Modo manual			Modo manual
31		32			15			Low Battery Voltage			Low Batt Volt			Baja tensión Bat		Baja V Bat
32		32			15			High Water Temperature			High Water Temp			Alta temp agua			Alta temp agua
33		32			15			Low Oil Pressure			Low Oil Press			Baja presión aceite		Baja Ps aceite
34		32			15			Low Fuel Level				Low Fuel Level			Nivel combustible bajo		Bajo combustib
35		32			15			Diesel Generator Failure		Diesel Gen Fail				Fallo GE			Fallo GE
36		32			15			Diesel Generator Group			Dsl Gen Group			Grupo Generador CA		Grupo GE
37		32			15			State					State				Estado				Estado
38		32			15			Existence State				Existence State			Estado presencia		Presencia
39		32			15			Existent				Existent			Existente			Existente
40		32			15			Not Existent				Not Existent			Inexistente			Inexistente
41		32			15			Total Input Current			Input Current			Total Corriente Entrada			Corr Entrada






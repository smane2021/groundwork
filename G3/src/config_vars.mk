#/*==========================================================================*
# *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
# *                     ALL RIGHTS RESERVED
# *
# *  PRODUCT  : ACU(Advanced Controller Unit)
# *
# *  FILENAME : config_vars.mk
# *  CREATOR  : Frank Mao                DATE: 2004-09-13 15:15
# *  VERSION  : V1.00
# *  PURPOSE  : Defines the variables for the makefiles.
# *
# *  HISTORY  :
# *
# *==========================================================================*/

#complile flags
CFLAGS =

#link flags
LDFLAGS =

ifeq ($(ACU_CROSS_PLATFORM), arm)
#	ACU_CROSS_PLATFORM = arm-linux-
	ACU_CROSS_PLATFORM = arm-wrs-linux-gnueabi-armv5tel-glibc_small-
			     
endif

ifeq ($(ACU_CROSS_PLATFORM), x86)
	ACU_CROSS_PLATFORM = 
endif

ifeq ($(BACKTRACE_SUPPORTING), yes)
	PROFILE_SUPPORTING = yes
endif

CC = $(ACU_CROSS_PLATFORM)gcc
LD = $(ACU_CROSS_PLATFORM)ld
AR = $(ACU_CROSS_PLATFORM)ar
RANLIB=$(ACU_CROSS_PLATFORM)ranlib
STRIP= $(ACU_CROSS_PLATFORM)strip


#link flags
LDFLAGS += -lpthread -lrt -lm -L$(ACU_SOURCE_DIR)/lib/

#Compile flags: Add the debug flag
ifeq ($(ACU_MAKE_VERSION), debug)
	CFLAGS  += -g -ggdb -rdynamic -ldl -D_DEBUG=1
	LDFLAGS += -Wl,-Map,$@.map
	
else
	CFLAGS  += -DNDEBUG #-o2
	LDFLAGS += -Wl
endif

ifeq ($(ACU_MAKE_VERSION_N), debug)
	STRIP= $(ACU_CROSS_PLATFORM)strip -o -s
endif

#for OProfile analysis
ifneq ($(PROFILE_SUPPORTING), yes)
	LDFLAGS += -s	
endif

ifeq ($(PROFILE_SUPPORTING), yes)
	STRIP= $(ACU_CROSS_PLATFORM)strip#	
endif

ifeq ($(ACU_CROSS_PLATFORM), ppc_8xx-)
	CFLAGS += -D_HAS_WATCH_DOG=1 -D_HAS_LCD_UI=1 -D_HAS_FLASH_MEM=1 -D_HAS_ACU_SCC 
endif

CFLAGS +=  -W -Wall -Wstrict-prototypes -Wundef -Wunknown-pragmas -Wunreachable-code -D_HAS_FLASH_MEM=1 -D_HAS_LCD_UI=1
CFLAGS += -Wfloat-equal -W -Wimplicit -Wconversion
#CFLAGS += -Werror #treat warning as error

ifeq ($(APP_FOR_MINI), yes)
	CFLAGS += -D_CODE_FOR_MINI	
endif

ifeq ($(BACKTRACE_SUPPORTING), yes)
	CFLAGS += -g -D_DEBUG_BACKTRACE	
endif

CFLAGS += -I$(ACU_SOURCE_DIR)/include 

#public dependent head files.
PUBINCS = $(ACU_SOURCE_DIR)/include/public.h

#public librarys
PUBLIBS = -lpub -lapp

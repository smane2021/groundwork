/*==========================================================================*
 *    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : get_his_data.h
 *  CREATOR  : LIXIDONG                 DATE: 2005-01-31 19:01
 *  VERSION  : V1.00
 *  PURPOSE  :

 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#ifndef _APP_RUNLOG_H
#define _APP_RUNLOG_H
//Ended Alarm Record
//Definition
#define			LEN_DATA_NAME			20

//FLASH Sector relevant reference definition
// Max times whic every sector can be erased
//#define			MTD5_NAME				"/dev/mtd5"    //Total 8 sector,
//It has been defined in data_mgmt.h

// Bytes per sector 
#define			SIZE_PER_LOG_SECTOR		(65536*2)//65536//8192   //8k per sector     
// Years which flash can be used
#define			MIN_USED_YEARS			10  
// 4M Flash,it has 64 sectors 63 * 64k + 8 * 8k size only use 63sectors * 64k
#define			MAX_LOG_SECTORS			8//12//8	 change from 2 to 12 .2008-7-15 by YangGuoxin
// The first Sector in history data storage region-device:dev/mtd6
#define			START_LOG_SECTOR		1  
//Hisdata's start postion
#define			START_POSITION			0		

//Define CRC16 check relevant const
// CRC16 Check Bytes
#define			LOG_CRC16_BYTES			2	
// Identify record index Bytes
#define			LOG_RECORD_ID_BYTES		4			

// Data  Struct
//#pragma pack(1)

struct tagSECTOR_MAPTABLE
{
	BYTE			byLogSectorNo;					//Logic Sector Index
	BYTE			byPhySectorNo;					//Physical Sector Index

};
typedef struct tagSECTOR_MAPTABLE _SECTOR_MAP_TABLE_;


//Historical Data Tracking Information
struct tagRecord_SetTrack 
{
	char				DataName[LEN_DATA_NAME];	/* LEN_DATA_NAME -16*/
	int					iTotalSectors;				/* Total Sectors of Current Type Data */
    int					iMaxRecords;				/* Max records can be held */
    int					iRecordSize;				/* A record size in byte    */
	////////////////////////////////////////////////////////////////////////////
	//Add erasing sector pre-process function
	ERASE_SECTOR_PRE_PROC
						pfn_preProcess;				/* Before erasing sector,execute this function*/
	HANDLE				hPreProcess;				/* Process handle */
	void				*Params;					/* Process input argument*/
	///////////////////////////////////////////////////////////////////////////
	int					iStartRecordNo;				/* Among latest iMaxRecords records index*/
	///////////////////////////////////////////////////////////////////////////
	BOOL				bSectorEffect;				/* It decide current data can be write in*/
	///////////////////////////////////////////////////////////////////////////
	int					iDataRecordIDIndex;			/* Show different data record id in flash static sector*/
	BYTE				byCurrSectorNo;				/* Current Sector Number*/
	BYTE				byNewSectorFlag;			/* 1 Current records < MaxRecords*/   
	int					iMaxRecordsPerSector;		/* Max records can be held in one Sector*/
	int					iWritableRecords;			/* Records that Current Sector can be written*/
	int					iCurrWritePos;				/* The current write record index  */
	int					iCurrReadPos;				/* The current read record index   */
	DWORD				dwCurrRecordID;				/* The Current Record ID*/
	_SECTOR_MAP_TABLE_	*pbyPhysicsSectorNo;		/* Point the new int [byTotalSectors] start address*/
    	 
 };
typedef struct tagRecord_SetTrack LOG_RECORD_SET_TRACK;

//Historical Data Tracking Information

//#pragma pack()

#define  THE_FIRST_READ		0

#define  ERR_CTRL_INPUT		(-1)
#define  ERR_END_READING	(-2)


#endif






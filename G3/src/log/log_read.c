/*==========================================================================*
*    Copyright(c) 2020, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : get_log.c
 *  CREATOR  : LIXIDONG                 DATE: 2005-01-31 19:01
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include <sys/types.h>
#include <stdarg.h>

#include "stdsys.h"
#include "public.h"
#include "apprunlog.h"


#ifdef _DEBUG
//#define _RUN_LOG_READ_   1
#endif

//The site(ACU) info,include all equips data, signals data and configs
SITE_INFO  g_SiteInfo;

//need to be defined in Miscellaneous Function Module
TIME_SRV_INFO g_sTimeSrvInfo;

//need to be defined in equip management Module

int Equip_Control(IN int nSenderType, IN char *pszSenderName,
				  IN int nSendDirectly,
				  IN int nEquipID, IN int nSigType, IN int nSigID, 
				  IN VAR_VALUE *pCtrlValue, IN DWORD dwTimeout)
{
	UNUSED(nSenderType);
	UNUSED(pszSenderName);
	UNUSED(nSendDirectly);
	UNUSED(nEquipID);
	UNUSED(nSigType);
	UNUSED(nSigID);
	UNUSED(pCtrlValue);
	UNUSED(dwTimeout);
	return 0;
}

//need to be defined in Miscellaneous Function Module
int UpdateSCUPTime(time_t* pTimeRet)
{
	ASSERT(pTimeRet);

	TRACE("\n***UpdateSCUPTime in log_read\n");	//1.Update to hardware

	return stime(pTimeRet);		//2.Update OS time
}

int UpdateNTPTime(void)
{
	return 1;
}

//STUB: used in libapp.so, but NOT need in this APP.mfh
int WriteRunConfigItem(RUN_CONFIG_ITEM* pRunConfigItem)
{
	UNUSED(pRunConfigItem);
	return 0;
}

char g_szACUConfigDir[MAX_FILE_PATH];

SERVICE_MANAGER		g_ServiceManager;

/////////////////////////////////////////////////////////////////////////////
#ifdef _APP_LOG_TO_FLASH    //It is defined in applog.h file
/*==========================================================================*
 * FUNCTION : ReadFlashLog
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-02-03 18:53
 *==========================================================================*/
static int ReadFlashLog(void)
{
	int iExitStatus;
	long iReadPos;
	char *pBuffer;
	int	iRecords;
	int iStartRecordNo;
	BOOL bReadFlag;

	//int iRecordSize;
	iExitStatus = 0;
	iReadPos	= 0;
	
#ifdef _RUN_LOG_READ_
	printf("Come here, please waiting.\n");
#endif
	
	// 1. init log management module
	if (!LOG_IniReadFlashLog())
	{
		LOG_ClearReadFlashLog();
		return 1;
	}
	
	//2. read running log according to global variable pHisTrack and g_iHisFile
	iStartRecordNo = 1;
	iRecords = MAX_LOG_RECORD_COUNT;
	pBuffer = (char *)NEW(char, (iRecords + 1) * (sizeof(LOG_RECORD_SIZE)));

	if (pBuffer == NULL)
	{
		LOG_ClearReadFlashLog();
		return 1;
	}

	bReadFlag = LOG_StorageReadRecords (&iStartRecordNo, 
							&iRecords, 
							pBuffer,
							1,
							1); 
	
	if (!bReadFlag)
	{
#ifdef _RUN_LOG_READ_
		printf("Fail to LOG_StorageReadRecords.\n");
#endif
	}
	else
	{
#ifdef _RUN_LOG_READ_
		printf("iStartRecordNo is %d,iRecords is %d\n", iStartRecordNo, iRecords);	
#endif		
	}
	
	// 3.cleanup resource
	LOG_ClearReadFlashLog();
#ifdef _RUN_LOG_READ_
	printf("Succeed to LOG_ClearReadLog.\n");
#endif

	if (pBuffer != NULL)
	{
		DELETE(pBuffer);
	}

	return iExitStatus;
}

#else
/*==========================================================================*
 * FUNCTION : ReadFileLog
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-02-03 18:55
 *==========================================================================*/
static int ReadFileLog(void)
{
	//system("cat /scup/log/ACU.log");
	_SYSTEM("cat /scup/log/ACU.log");
	//printf("Please read the file under the directory /scup/log/ACU.log\n");
	return 0;	
}

#endif
/////////////////////////////////////////////////////////////////////////////
/*==========================================================================*
 * FUNCTION : main
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : int : 
 * COMMENTS : 
 * CREATOR  : LIXIDONG                 DATE: 2005-01-31 19:05
 *==========================================================================*/
int main(void)
{	
#ifdef _APP_LOG_TO_FLASH

	ReadFlashLog();
#else

	ReadFileLog();
	
#endif

	return 0;	
}


#!/bin/sh

replace()
{
	if [ -f /lib/qt-lib/$1 ]; then
		strCmpResult=$(cmp /lib/qt-lib/$1 /app/qt4/$1)
		if [ $strCmpResult != "" ]; then
			rm -rf /lib/qt-lib/$1
			cp -a /app/qt4/$1 /lib/qt-lib/$1
		fi
	else
		cp -a /app/qt4/$1 /lib/qt-lib/$1
	fi
}

replace "libsysInit.so"
replace "libutility.so"
replace "libdataSource.so"

exit 0

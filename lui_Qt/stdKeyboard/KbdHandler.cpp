#include "KbdHandler.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "/home/girish/Downloads/koustubh/Migration5_17_02_2020/lui_Qt/public/sysInit.h"

#define BUZZ_MAJOR     251
#define IOCTL_BEEP     _IOW(BUZZ_MAJOR, 5, unsigned long)

static ShmData* gs_pShmData = NULL;

KbdHandler::KbdHandler(const QString &device, QObject *parent)
    :QObject(parent), QWSKeyboardHandler()
{
    TRACEDEBUG( ">>> KbdHandler::KbdHandler device<%s>\n", device.toAscii().constData() );
    static int shmid = -1;
    shmid = shmget( (key_t)SHM_KEY, sizeof(ShmData), IPC_CREAT|0666 );
    if(shmid < 0)
    {
        TRACEDEBUG( "KbdHandler shmget failed \n" );

    }
    gs_pShmData = (ShmData*)shmat(shmid, NULL, 0);
    m_fdSound = gs_pShmData->fdDriverGlobal;
    if(m_fdSound < 0)
    {
        TRACEDEBUG( "KbdHandler::KbdHandler m_fdSound < 0 <%d>\n", m_fdSound );
        return;
    }
    //m_bSound  = gs_pShmData->bSound;

    m_fdKeyboard = ::open(
                device.toAscii().constData(),
                O_RDONLY
                );
    if(m_fdKeyboard < 0)
    {
        TRACEDEBUG( "KbdHandler::KbdHandler m_fdKeyboard < 0 <%d>\n", m_fdKeyboard );
        return;
    }
    gs_pShmData->fdKeypad = m_fdKeyboard;

/*     TRACEDEBUG( "KbdHandler::KbdHandler m_fdSound<%d> bSound<%d> m_fdKeyboard<%d> \n",
                m_fdSound,
                gs_pShmData->bSound,
                m_fdKeyboard
                ); */
//    if (shmdt(&pShmData) < -1)
//    {
//        TRACEDEBUG( "KbdHandler shmdt failed \n" );
//    }
    setObjectName("Keyboard Handler");

    this->notifier = new QSocketNotifier(m_fdKeyboard,
          QSocketNotifier::Read, this);
    connect(this->notifier, SIGNAL(activated(int)),
            this, SLOT(kbdReadyRead()));
}

KbdHandler::~KbdHandler()
{
//    if(m_fdKeyboard >= 0)
//    {
//        ::close( kbdFd );
//        kbdFd = -1;
//    }
}

void KbdHandler::kbdReadyRead()
{
    //TRACEDEBUG( "KbdHandler::kbdReadyRead \n" );

#define KEY_BUZZ \
    if (gs_pShmData->bSound) \
    {ioctl(m_fdSound, IOCTL_BEEP, 2);}


    char key;
    // 读取设备文件
    if(read(m_fdKeyboard, &key, sizeof(key)) != sizeof(key))
        return;
    //Qt::KeyboardModifiers modifiers = Qt::NoModifier;
    int keycode = 0;
    //TRACEDEBUG("=================KEY<%d>",key);
    switch (key)
    {
/*     TRACEDEBUG( "KbdHandler::kbdReadyRead gs_pShmData->bSound<%d>\n",
                gs_pShmData->bSound
                ); */
    case VK_ESC:
        KEY_BUZZ;
        keycode = Qt::Key_Escape;
        break;

    case VK_DOWN:
        KEY_BUZZ;
        keycode = Qt::Key_Down;
        break;

    case VK_UP:
        KEY_BUZZ;
        keycode = Qt::Key_Up;
        break;

    case VK_ENT:
        KEY_BUZZ;
        keycode = Qt::Key_Enter;
        break;

    case VK_ESCENT:
        KEY_BUZZ;
        keycode = Qt::Key_Control;
        break;
		
    case VK_UPDOWN:
        KEY_BUZZ;
        keycode = Qt::Key_Home;
        break;

    case VK_DOWNESC:
        KEY_BUZZ;
        keycode = Qt::Key_Shift;
        break;
    default:
        //keycode = key;
//        TRACEDEBUG( "KbdHandler::kbdReadyRead() Unrecognised key code %d\n", key );
        return;
    }
    processKeyEvent( 0, keycode, 0, TRUE, FALSE );

    //this->processKeyEvent(unicode, keycode, modifiers, 1, false);
}

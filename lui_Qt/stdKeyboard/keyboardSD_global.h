#ifndef KEYBOARDSD_GLOBAL_H
#define KEYBOARDSD_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(KEYBOARDSD_LIBRARY)
#  define KEYBOARDSDSHARED_EXPORT Q_DECL_EXPORT
#else
#  define KEYBOARDSDSHARED_EXPORT Q_DECL_IMPORT
#endif

// delete release
//#define _DEBUG

#ifdef _DEBUG
    #define TRACEDEBUG printf
#else
    #define TRACEDEBUG(a, ...)
#endif

#endif // KEYBOARDSD_GLOBAL_H

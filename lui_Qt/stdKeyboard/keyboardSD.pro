#-------------------------------------------------
#
# Project created by QtCreator 2013-05-20T15:04:43
#
#-------------------------------------------------
QT       -= gui

TARGET = stdKeyboard
TEMPLATE = lib
#CONFIG += warn_on release dll thread
OBJECTS_DIR = ./tmp
DESTDIR = ../output
INCLUDEPATH += ../public
LIBS += -L ../output

DEFINES += KEYBOARDSD_LIBRARY

SOURCES += \
    KbdPlugin.cpp \
    KbdHandler.cpp

HEADERS +=\
    keyboardSD_global.h \
    KbdHandler.h \
    KbdPlugin.h \
    KeyboardDef.h

symbian {
    MMP_RULES += EXPORTUNFROZEN
    TARGET.UID3 = 0xE46BA574
    TARGET.CAPABILITY = 
    TARGET.EPOCALLOWDLLDATA = 1
    addFiles.sources = keyboardSD.dll
    addFiles.path = !:/sys/bin
    DEPLOYMENT += addFiles
}

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}

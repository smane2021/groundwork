#ifndef KBDPLUGIN_H
#define KBDPLUGIN_H

#include <QtGui/QKbdDriverPlugin>
#include "KeyboardDef.h"

class KbdPlugin : public QKbdDriverPlugin
{
    Q_OBJECT
public:
    KbdPlugin();
    ~KbdPlugin();

    QWSKeyboardHandler* create( const QString &driverName,
       const QString &deviceName =
            QString(DEV_KEYBOARD_NAME) );
    QStringList keys() const;
};

#endif // KBDPLUGIN_H

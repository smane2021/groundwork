#include "KbdPlugin.h"
#include "KbdHandler.h"
#include <QDebug>

KbdPlugin::KbdPlugin()
{
    TRACEDEBUG( "KbdPlugin::KbdPlugin() 12.31 \n" );
}

KbdPlugin::~KbdPlugin()
{
    TRACEDEBUG( "KbdPlugin::~KbdPlugin() \n" );
}

QWSKeyboardHandler *KbdPlugin::create(const QString &driverName, const QString &deviceName)
{
    //TRACEDEBUG( "KbdPlugin::create <%s>\n", driverName.toStdString().c_str() );
    if ( 0==driverName.compare("stdKeyboard", Qt::CaseInsensitive) )
    {
        TRACEDEBUG( "KbdPlugin::create keyboardSD OK\n" );
        return new KbdHandler(deviceName);
    }
    TRACEDEBUG( "KbdPlugin::create keyboardSD failed\n" );
    return NULL;
}

QStringList KbdPlugin::keys() const
{
    //TRACEDEBUG( "KbdPlugin::keys()\n" );
    return QStringList() << "stdKeyboard";
}

Q_EXPORT_PLUGIN2(stdKeyboard, KbdPlugin)

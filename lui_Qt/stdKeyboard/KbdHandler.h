#ifndef KBDHANDLER_H
#define KBDHANDLER_H

#include "keyboardSD_global.h"
#include <QObject>
#include <QtGui/QWSKeyboardHandler>
#include <QSocketNotifier>
#include "KeyboardDef.h"

class KEYBOARDSDSHARED_EXPORT KbdHandler :
        public QObject, public QWSKeyboardHandler
{
    Q_OBJECT
public:
    KbdHandler(const QString &device = QString(DEV_KEYBOARD_NAME), QObject *parent = 0);
    ~KbdHandler();

private:
    int   m_fdSound;
    //bool  m_bSound;
    int   m_fdKeyboard;
    QSocketNotifier *notifier;

private slots:
    void kbdReadyRead();
};

#endif // KBDHANDLER_H

#include "BaseStyle.h"

#include <QStyle>
#include <QPalette>
#include <QColor>
#include <QWidget>
#include <QStyleOption>
#include <QPainter>
#include <QPixmap>
#include <QPoint>
#include <QLine>
#include "common/uidefine.h"


QColor baseColor(0x4d,0x4d,0x4d);
QColor backColor(0x4d,0x4d,0x4d);
QColor btnTextColor(0x33,0x33,0x33);
QColor buttonClr(0x82,0xbe,0x20);
QColor highLightColor(0x61,0x61,0x61);
QColor highlightedText(0xf1,0xf1,0xf1);

BaseStyle::BaseStyle(): QPlastiqueStyle()
{
    ;
}


//修改调色板, 主要是修改各个部件的颜色，如高亮、按钮、文本、背景等
void BaseStyle::polish(QPalette &palette)
{
    palette.setBrush( QPalette::Button, buttonClr );
    palette.setBrush( QPalette::BrightText, Qt::red );
    palette.setColor( QPalette::Background, backColor );
    //窗口字体颜色
    palette.setColor( QPalette::WindowText, textNormalColor );
    //前景字体颜色
    palette.setColor( QPalette::Foreground, textNormalColor );
    //文本文字
    palette.setColor( QPalette::Text, textNormalColor );
    palette.setColor( QPalette::Base, backColor );

    palette.setBrush( QPalette::Highlight, highLightColor );
    palette.setBrush( QPalette::HighlightedText, highlightedText );
}

/******************************************************************************
文件名：    m822estyle.h
功能：      定义ui的显示风格，包括控件、字体的默认颜色，控件的默认样式等

作者：      刘金煌
创建日期：   2013年6月10日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef BASESTYLE_H
#define BASESTYLE_H

#include <QWidget>
#include <QPlastiqueStyle>

/*跟EMU10统一采用塑胶类型的界面风格，如果需要修改，可以修改继承的父类
  例如QMotifStyle,QWindowsStyle,QCDEStyle,QCleanlooksStyle,
  QWindowsXPStyle,QMacStyle等类型
  M822eStyle的风格类型，是根据继承的父类类型，再在上面重写部分函数，以做修改*/

class BaseStyle : public QPlastiqueStyle
{
    Q_OBJECT

public:
    BaseStyle();

    virtual void polish(QPalette &palette);

signals:

public slots:

};

#endif // BASESTYLE_H

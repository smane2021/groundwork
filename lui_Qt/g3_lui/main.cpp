/******************************************************************************
文件名：    main.cpp
功能：      M830B1.8寸(128*160像素)和M830D3.2寸(240*320像素)
           LUI模块主函数
作者：      刘金煌
创建日期：   2013年3月24日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include <QtGui/QApplication>
#include <QWSServer>
#include <QTextCodec>
#include <QSplashScreen>
#include <QPixmap>
#include <QPlastiqueStyle>
#include <QTranslator>
#include <QDesktopWidget>
#include <QFile>
#include <QTextStream>

#include "style/BaseStyle.h"
#include "basicWidget/mainwindow.h"
#include "common/global.h"
#include "common/splashscreen.h"
#include "common/uidefine.h"
#include "common/pubInclude.h"
#include "util/DlgLoading.h"
#include "util/ThreadFlashLight.h"
#include "config/configparam.h"
#include "configWidget/WdgFCfgGroup.h"

#define GUIDE_WINDOW

#ifdef Q_OS_LINUX
#include <unistd.h>
#include <signal.h>
#include "util/DlgUpdateApp.h"
static void sigAction(const int nSig)
{
//#define SIGINT          2       /* interrupt */
//#define SIGILL          4       /* illegal instruction - invalid function image */
//#define SIGFPE          8       /* floating point exception */
//#define SIGSEGV         11      /* segment violation */
//#define SIGTERM         15      /* Software termination signal from kill */
//#define SIGBREAK        21      /* Ctrl-Break sequence */
//#define SIGABRT         22

    TRACELOG2("sigAction Received a signal #%d", nSig);
    switch (nSig)
    {
        case SIGTERM:
        {
          DlgUpdateApp dlg;
          dlg.exec();
        }
        break;

        case SIGSEGV:
        {
          printf("PANIC:Qt exit~~~~~~~~~~~~~~~~~~~~123456789~~~~~~~~~~~~~~~~~~~\n");
          exit(0);
        }
        case SIGKILL:
        case SIGQUIT:
        case SIGINT:
            exit(0);

        default:
        break;
    }
}
#endif

#ifdef Q_OS_LINUX
unsigned long fileSize(const char *szFileName)
{
  unsigned long filesize = -1;
  struct stat statbuff;
  if(stat(szFileName, &statbuff) < 0){
      return filesize;
  }else{
      filesize = statbuff.st_size;
  }
  return filesize;
}

bool isFileSame(const char *szFileSrc,const char*szFileDst)
{
  int nFileSizeSrc = fileSize(szFileSrc);
  int nFileSizeDst = fileSize(szFileDst);

  if(nFileSizeSrc == nFileSizeDst)
  {
    return true;
  }
  return false;
}

void fileCopy(const char *szFileSrc,const char *szFileDst)
{
  char szShellCmd[200]={0};
  remove(szFileDst);

  sprintf(szShellCmd,"cp -a %s %s",szFileSrc,szFileDst);
  system(szShellCmd);
}

void InitEnv()
{
  if(!isFileSame("/app/qt4/plugins/kbddrivers/libstdKeyboard.so",
                "/lib/qt-lib/plugins/kbddrivers/libstdKeyboard.so"))
  {
      fileCopy("/app/qt4/plugins/kbddrivers/libstdKeyboard.so",
               "/lib/qt-lib/plugins/kbddrivers/libstdKeyboard.so");
  }
}
#endif

int main(int argc, char *argv[])
{
#define _VERSION_ "2017.04.11 1 ESNA"

#ifdef GUIDE_WINDOW
        printf("gui version " _VERSION_ "\n");
#else
        printf("gui version " _VERSION_ "\n");
#endif

#ifdef Q_OS_LINUX
    // -1:close 0:TRACEDEBUG 1:TRACELOG1 2:TRACELOG2
    util_init( -1 );
    InitEnv();
#endif
    QTime testTime;
    testTime.start();

    initGlobalVar();

#ifdef Q_OS_LINUX
    // 加载驱动
    sys_open( DO_WRITE );
    if ( data_open() )
    {
        TRACELOG1( "main data_open error" );
    }

    // 启动的时候让灯闪
    ThreadFlashLight threadLED;
    threadLED.start();
#endif

    QApplication app(argc, argv);

#ifdef Q_OS_LINUX
    int pHandledSignals[] =
    {
        SIGTERM, SIGINT, SIGKILL, SIGQUIT, // Kill signal or Ctrl+C
        SIGPIPE,                           // broken pipe.
        SIGSEGV,                           // segment fault error.
    };
    // 0. init msg handler.
    for (quint32 i=0; i<sizeof(pHandledSignals)/sizeof(pHandledSignals[0]); i++)
    {
        signal(pHandledSignals[i], sigAction);
    }
#endif

#ifdef Q_OS_LINUX
    sys_open( DO_READ );
    // disable keyboard
    sys_setKeyboard( false );
    QWSServer::setCursorVisible( false );//去掉鼠标箭头
#endif

#ifdef GUIDE_WINDOW
    g_cfgParam->readLanguageApp();
#endif

    //设置自定义的UI的显示风格
    BaseStyle style;
    app.setStyle( &style );

    TRACEDEBUG( "main setLanguageFont default english" );
    g_cfgParam->setLanguageFont();
    app.setFont( g_cfgParam->gFontLargeN );

    g_cfgParam->iniPos();

    MainWindow mainWindow;
    mainWindow.setWindowFlags(Qt::FramelessWindowHint);
    mainWindow.setWindowOpacity(0.9);

    g_WdgFCfgGroup = new WdgFCfgGroup( 8 );

    TRACELOG1(">>> mainWindow.CreateWindows() time = %dms\n", testTime.elapsed());
    mainWindow.CreateWindows();

    mainWindow.setEnabled(false);

    TRACELOG1("mainWindow.CreateWindows() OK time = %dms >>>\n", testTime.elapsed());

#ifdef Q_OS_LINUX
    sys_setLED( LED_GREEN_ON );
    threadLED.stop();
#endif

    mainWindow.show();
    TRACELOG1("finished time = %dms >>>...", testTime.elapsed());

    mainWindow.setEnabled(true);

    mainWindow.setWindowOpacity(1.0);

#ifdef Q_OS_LINUX
#ifdef GUIDE_WINDOW
    GuideWindow::ms_bInEnter = true;
    mainWindow.GoToGuideWindow( WGUIDE_LANGUAGE );//WGUIDE_PASSWORD WGUIDE_LANGUAGE changed by hhy
#else
    mainWindow.GoToHomePage();
#endif
#else
    mainWindow.setGeometry(700, 600, PosBase::screenWidth, PosBase::screenHeight);
    mainWindow.GoToHomePage();
#endif

    g_timeElapsedNoKeyPress.start();

    printf("it takes %d sec to start!\n",testTime.elapsed());

    int nMainRet = app.exec();

    if ( data_close() )
        TRACELOG1( "main data_close error" );

    return nMainRet;
}

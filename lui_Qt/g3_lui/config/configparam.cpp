/******************************************************************************
文件名：    configparam.cpp
功能：      此类定义各种参数的配置，在运行时用户可动态更改
作者：      刘金煌
创建日期：   2013年3月24日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "configparam.h"

#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QMap>
#include <QRegExp>
#include <QTextCodec>
#include <QDesktopWidget>
#include <QApplication>
#include "common/pubInclude.h"
#include "common/global.h"
#include "common/uidefine.h"
#include "config/PosBase.h"
#include "config/PosHomepage.h"
#include "config/PosPieChart.h"
#include "config/PosCurve.h"
#include "config/PosBarChart.h"
#include "config/PosThermometer.h"
#include "config/PosTableWidget.h"
#include "config/PosDlgInfo.h"

ConfigParam* g_cfgParam;
Init_Param ConfigParam::ms_initParam;
QString    ConfigParam::pathImg;
int        ConfigParam::ms_maxCharsPerTableRow;

QFont ConfigParam::gFontLargeN;
QFont ConfigParam::gFontLargeB;
QFont ConfigParam::gFontSmallN;

QFontMetrics* ConfigParam::ms_pFmLargeN;
QFontMetrics* ConfigParam::ms_pFmLargeB;
QFontMetrics* ConfigParam::ms_pFmSmallN;

ConfigParam::ConfigParam()
{
}

// read Qt parameter
//
int ConfigParam::iniPos()
{
    PosBase base;
    base.init();

    PosHomepage homepage;
    homepage.init();

    PosPieChart pie;
    pie.init();

    PosCurve curve;
    curve.init();

    PosBarChart barChart;
    barChart.init();

    PosThermometer thermometer;
    thermometer.init();

    PosTableWidget posTableWidget;
    posTableWidget.init();

    PosDlgInfo posDlgInfo;
    posDlgInfo.init();

    return 0;
}

//如果返回0则是版本没变，否则是版本发生变化，需要跳回主页。
int ConfigParam::initParam(bool bWait)
{
    Q_UNUSED(bWait);

    bool blSkipToHomePage = false;
    enum DISPLAY_VERSION enumDispVer;

    //alarmVoice keypadVoice lcdRotation timeFormat
    CmdItem cmdItem;
    cmdItem.CmdType   = CT_READ;
    cmdItem.ScreenID  = SCREEN_ID_Init;
    TRACEDEBUG( "g_cfgParam->initParam() bWait<%d> ScreenID<%x>\n", bWait, cmdItem.ScreenID );
    void* pData = NULL;
    bool bGetted = false;

    bGetted = (data_getDataSync(&cmdItem, &pData) == 0);

    if ( bGetted )
    {
        PACK_DATAINFO* info = (PACK_DATAINFO*)pData;
        int nIdxArr = 0;
        ++nIdxArr;
        ms_initParam.alarmVoice = (enum ALARM_VOICE)info->DataInfo[nIdxArr].vValue.enumValue;

        ++nIdxArr;
        ms_initParam.keypadVoice = (enum KEYPAD_VOICE)info->DataInfo[nIdxArr].vValue.enumValue;
        sys_setBuzzKeyboard(ms_initParam.keypadVoice==KEYPAD_VOICE_ON);

        ++nIdxArr;
        ms_initParam.lcdRotation = (enum LCD_ROTATION)info->DataInfo[nIdxArr].vValue.enumValue;

        ++nIdxArr;
        ms_initParam.timeFormat = (enum TIME_FORMAT)info->DataInfo[nIdxArr].vValue.lValue;

        ++nIdxArr;
        ms_initParam.homePageType =
                (enum HOMEPAGE_TYPE)(info->DataInfo[nIdxArr].vValue.lValue);

        //Added by alpha
        ++nIdxArr;
        enumDispVer = (enum DISPLAY_VERSION)(info->DataInfo[nIdxArr].vValue.enumValue);

        ++nIdxArr;


        if(ms_initParam.dispVersion != enumDispVer)
        {
            blSkipToHomePage = true;
            ms_initParam.dispVersion = enumDispVer;
        }
    }

    ms_initParam.alarmTime = 0;
    if (g_cfgParam->ms_initParam.homePageType != HOMEPAGE_TYPE_SLAVE)
    {
        switch ( ms_initParam.alarmVoice )
        {
          case ALARM_VOICE_ON:
          ms_initParam.alarmTime = -1;
          break;

          case ALARM_VOICE_OFF:
          ms_initParam.alarmTime = 0;
          break;

          case ALARM_VOICE_3M:
          ms_initParam.alarmTime = 3*60;
          break;

          case ALARM_VOICE_10M:
          ms_initParam.alarmTime = 10*60;
          break;

          case ALARM_VOICE_1H:
          ms_initParam.alarmTime = 1*60*60;
          break;

          case ALARM_VOICE_4H:
          ms_initParam.alarmTime = 4*60*60;
          break;

          default:
          break;
        }
    }

    if(blSkipToHomePage == true)
    {
      return (1);
    }

    TRACEDEBUG( "g_cfgParam->initParam() "
                "alarmVoice<%d>1:OFF alarmTime<%d>-1:ON 0:OFF "
                "keypadVoice<%d>0:ON 1:OFF lcdRotation<%d> "
                "timeFormat<%d> homePageType<%d> dispVersion<%d>\n",
                ms_initParam.alarmVoice, ms_initParam.alarmTime,
                ms_initParam.keypadVoice, ms_initParam.lcdRotation,
                ms_initParam.timeFormat, ms_initParam.homePageType,
                ms_initParam.dispVersion
                );
    return 0;
}

void ConfigParam::initAlmVoiceCloseStat()
{
  int nIdxArr = 0;
  CmdItem cmdItem;
  cmdItem.CmdType   = CT_READ;
  cmdItem.ScreenID  = SCREEN_ID_GetWebAlmVoice;
  void* pData = NULL;
  bool bGetted = false;

  bGetted = (data_getDataSync(&cmdItem, &pData) == 0);

  if ( bGetted )
  {
      PACK_DATAINFO* info = (PACK_DATAINFO*)pData;
      ms_initParam.almVoiceType = (enum ALMVOICE_CLOSE_TYPE)info->DataInfo[nIdxArr].vValue.enumValue;
      nIdxArr++;
      ms_initParam.almVoiceActType = (enum ALMVOICE_ACTIVATE_TYPE)info->DataInfo[nIdxArr].vValue.enumValue;
  }
  else
  {
      ms_initParam.almVoiceType = ALMVOICE_CLOSE_YES;
      ms_initParam.almVoiceActType = ALMVOICE_ACTIVATE_TYPE_YES;
  }
}

void ConfigParam::setLanguageFont()
{
    //TRACEDEBUG( "g_cfgParam->setLanguageFont" );
#ifdef TEST_GUI
    {
    ms_initParam.langType = LANG_English;
    // 字库
    gFontLargeN.setFamily("WenQuanYi Zen Hei");
    gFontLargeB.setFamily("WenQuanYi Zen Hei");
    gFontSmallN.setFamily("WenQuanYi Zen Hei");

    QTextCodec *codec = NULL;
    codec = QTextCodec::codecForName( "UTF-8" );
    switch (g_cfgParam->ms_initParam.lcdRotation)
    {
    case LCD_ROTATION_0DEG:
        ms_maxCharsPerTableRow = 18;
        break;
    case LCD_ROTATION_90DEG:
        ms_maxCharsPerTableRow = 16;
        break;
    case LCD_ROTATION_BIG:
        ms_maxCharsPerTableRow = 30;
        break;
    }

    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForCStrings(codec);
    QTextCodec::setCodecForTr(codec);
    if(ms_initParam.lcdRotation == LCD_ROTATION_BIG)
    {

        gFontLargeN.setPixelSize(19);
        gFontLargeB.setPixelSize(20);
        gFontSmallN.setPixelSize(18);
    }
    else if(ms_initParam.lcdRotation == LCD_ROTATION_0DEG)
    {
        gFontLargeN.setPixelSize(12);
        gFontLargeB.setPixelSize(12);
        gFontSmallN.setPixelSize(10);
    }
    else
    {
        g_szTimeFormat[0] =    "dd/MM/yy";
        g_szTimeFormat[1] =    "MM/dd/yy";
        g_szTimeFormat[2] =    "yy/MM/dd";
        /* modify by wufang,20131014, modify 2/2,adjust width for display year "YYYY"  */
        //g_szHomePageDateFmt[0] =     "dd/MM/yy";
        //g_szHomePageDateFmt[1] =    "MM/dd/yy";
        //g_szHomePageDateFmt[2] =    "yy/MM/dd";
        gFontLargeN.setPixelSize(12);
        gFontLargeB.setPixelSize(12);
        gFontSmallN.setPixelSize(10);
    }
    gFontLargeN.setWeight( QFont::Normal );
    gFontLargeB.setWeight( QFont::Black );
    gFontSmallN.setWeight( QFont::Normal );

    ms_pFmLargeN = new QFontMetrics( gFontLargeN );
    ms_pFmLargeB = new QFontMetrics( gFontLargeB );
    ms_pFmSmallN = new QFontMetrics( gFontSmallN );

//    QString strTestWidth( "WWWWWWWWWW" );
//    TRACEDEBUG( "width: %d", ms_pFmLargeN->width( strTestWidth ) );

    // Qt多语言文件
    char szLangFile[128] = "";
    sprintf( szLangFile,
             PATH_APP "translations/lang_%s",
             g_mapLangStr[ms_initParam.langType]
             );
    TRACELOG1( "g_cfgParam->setLanguageFont lang translate langType<%d> file<%s>", ms_initParam.langType, szLangFile );
    g_translator.load( szLangFile );
    qApp->installTranslator( &g_translator );
    }
    return;
#endif

#ifdef Q_OS_LINUX
    QDesktopWidget *d = QApplication::desktop();
    int width  = d->screenGeometry().width();
    int height = d->screenGeometry().height();
#else
    int width  = 320;
    int height = 240;
#endif
    g_cfgParam->ms_initParam.lcdRotation = width > 160 ? LCD_ROTATION_BIG
                                                       : (width == 160 ? LCD_ROTATION_0DEG : LCD_ROTATION_90DEG);
//    LANGUAGE_TYPE langType = ms_initParam.langType;

    // 字库
    gFontLargeN.setFamily("WenQuanYi Zen Hei");
    gFontLargeB.setFamily("WenQuanYi Zen Hei");
    gFontSmallN.setFamily("WenQuanYi Zen Hei");

    QTextCodec *codec = NULL;

    codec = QTextCodec::codecForName( "UTF-8" );
    switch (g_cfgParam->ms_initParam.lcdRotation)
    {
      case LCD_ROTATION_0DEG:
          ms_maxCharsPerTableRow = 18;
          break;

      case LCD_ROTATION_90DEG:
          ms_maxCharsPerTableRow = 16;
          break;

      case LCD_ROTATION_BIG:
          ms_maxCharsPerTableRow = 30;
          break;
    }

    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForCStrings(codec);
    QTextCodec::setCodecForTr(codec);


    if(ms_initParam.lcdRotation == LCD_ROTATION_BIG)
    {

        gFontLargeN.setPixelSize(19);
        gFontLargeB.setPixelSize(20);
        gFontSmallN.setPixelSize(18);
    }
    else if(ms_initParam.lcdRotation == LCD_ROTATION_0DEG)
    {
        gFontLargeN.setPixelSize(12);
        gFontLargeB.setPixelSize(12);
        gFontSmallN.setPixelSize(10);
    }
    else
    {
        g_szTimeFormat[0] =    "dd/MM/yy";
        g_szTimeFormat[1] =    "MM/dd/yy";
        g_szTimeFormat[2] =    "yy/MM/dd";

        gFontLargeN.setPixelSize(12);
        gFontLargeB.setPixelSize(12);
        gFontSmallN.setPixelSize(10);
    }
    gFontLargeN.setWeight( QFont::Normal );
    gFontLargeB.setWeight( QFont::Black );
    gFontSmallN.setWeight( QFont::Normal );

    ms_pFmLargeN = new QFontMetrics( gFontLargeN );
    ms_pFmLargeB = new QFontMetrics( gFontLargeB );
    ms_pFmSmallN = new QFontMetrics( gFontSmallN );

//    QString strTestWidth( "WWWWWWWWWW" );
//    TRACEDEBUG( "width: %d", ms_pFmLargeN->width( strTestWidth ) );

    // Qt多语言文件
    char szLangFile[128] = "";
    sprintf( szLangFile,PATH_APP "translations/lang_%s",g_mapLangStr[ms_initParam.langType]);
    TRACEDEBUG( "g_cfgParam->setLanguageFont lang translate langType<%d> file<%s>\n", ms_initParam.langType, szLangFile );
    g_translator.load( szLangFile );
    qApp->installTranslator( &g_translator );
}

#define EQPID_SYSTEM                  (1)
#define SIGID_CLOSE_ALMVOICE          (500)
#define ALM_VOICE_CLOSE_YES           (0)
#define ALM_VOICE_CLOSE_NO            (1)

void ConfigParam::resetAlmVoiceClose()
{
  void *pData = NULL;
  CMD_INFO cmdItem;
  cmdItem.CmdType = CT_SET;
  cmdItem.ScreenID = SCREEN_ID_SetBuzzStatus;
  cmdItem.setinfo.EquipID = EQPID_SYSTEM;
  cmdItem.setinfo.SigID = SIGID_CLOSE_ALMVOICE;
  cmdItem.setinfo.SigType = SIG_TYPE_SET;
  cmdItem.setinfo.value.enumValue = ALM_VOICE_CLOSE_NO;

  data_getDataSync (&cmdItem,&pData);
}

// 用户不选择语言，超时进入主页，若是英语只能存在本地，app无法获得
int ConfigParam::readLanguageLocal()
{
    int nRet = -1;
    QFile file( FILENAME_LANGUAGE );
    QString strLang;
    if ( file.open( QIODevice::ReadOnly | QIODevice::Text) )
    {
        QTextStream textIn( &file );
        strLang = textIn.readLine();
        if ( !strLang.isNull() )
        {
            nRet = strLang.toInt();
        }
        file.close();
    }
    TRACELOG1( "g_cfgParam->readLanguageLocal() %d", nRet );
    g_cfgParam->ms_initParam.langLocate =
            (enum LANGUAGE_LOCATE)nRet;
    return nRet;
}

int ConfigParam::writeLanguageLocal()
{
    QFile file( FILENAME_LANGUAGE );
    if ( file.open(QIODevice::ReadWrite | QIODevice::Truncate | QIODevice::Text) )
    {
        file.write(
                    QString::number(
                        g_cfgParam->ms_initParam.langLocate).
                    toUtf8().constData()
                    );
        file.close();
    }
    return 0;
}

int ConfigParam::readLanguageApp()
{
#ifdef TEST_GUI
    return 0;
#endif

    QFile file( "/app/config/solution/MonitoringSolution.cfg" );
    QString strLang;
    if ( file.open( QIODevice::ReadOnly | QIODevice::Text) )
    {
        TRACEDEBUG( "g_cfgParam->readLanguageApp() open file OK" );
        int nIdxLang = -1;
        QTextStream textIn( &file );
        strLang = textIn.readLine();
        while ( !strLang.isNull() )
        {
            static bool bReaded = false;
            nIdxLang = strLang.indexOf("LANGUAGE_INFO", 0, Qt::CaseInsensitive);
            if ( !bReaded && nIdxLang>0 )
            {
                TRACEDEBUG( "g_cfgParam->readLanguageApp() find section [LANGUAGE_INFO] index<%d>", nIdxLang );
                bReaded = true;
                strLang = textIn.readLine();
                continue;
            }
            if (bReaded && strLang.trimmed().length()>0 &&
                    strLang.indexOf("#", 0, Qt::CaseInsensitive)<0)
            {
                TRACEDEBUG( "g_cfgParam->readLanguageApp() strLang line <%s>", strLang.toUtf8().constData() );
                QRegExp sep("\\s+");
                strLang = strLang.section(sep, 0, 0, QString::SectionSkipEmpty);
                break;
            }
            strLang = textIn.readLine();
        }

        if (strLang.length() > 0)
        {
			if(strLang == "zh")
            {
                strLang = "en";
            }
            LANGUAGE_TYPE langApp = g_mapStrLangType[QString(strLang).toLower()];
            TRACEDEBUG( "g_cfgParam->readLanguageApp() %s langApp<%d>",
                        strLang.toUtf8().constData(), langApp );
            ms_initParam.langApp = langApp;
        }
        file.close();
    }
    else
    {
        TRACELOG2( "g_cfgParam->readLanguageApp() open file failed" );
    }

    return 0;
}

int ConfigParam::writeLanguageApp()
{
#ifdef TEST_GUI
    return 0;
#endif

    TRACEDEBUG( "g_cfgParam->writeLanguageApp() copy lang files" );
    LANGUAGE_TYPE langType = g_cfgParam->ms_initParam.langApp;
    const char* pszCountry = g_mapLangStr[langType];


    //清空/app/config/lang目录
    QDir langDir("/app/config");
    langDir.rmdir("/app/config/lang");

    QString strFromDir;
    strFromDir.sprintf( "/app/config_default/lang/%s", pszCountry );
    QString strToDir;
    strToDir.sprintf( "/app/config/lang/%s", pszCountry );

    if ( !copyDirectoryFiles(strFromDir, strToDir) )
    {
        TRACELOG2( "g_cfgParam->writeLanguageApp copy Dir<%s> error", strFromDir.toUtf8().constData() );
        return 3;
    }

    TRACEDEBUG( "g_cfgParam->writeLanguageApp() copy MonitoringSolution.cfg" );
    // MonitoringSolution.cfg 拷贝不全
    QTime t;
    t.start();
    while(t.elapsed() < 10000)
    {
        QCoreApplication::processEvents(
                    QEventLoop::AllEvents, 20);
    }
    //mainSleep( 10000 );
    char szFile[32] = "";
    sprintf( szFile, "MonitoringSolution_%s.cfg", pszCountry );
    TRACEDEBUG("g_cfgParam->writeLanguageApp file cp /app/config_default/solution/%s /app/config_default/lang/%s langType<%d>",
               szFile, pszCountry, langType);

    QString strSrcFileName;
    strSrcFileName.sprintf(
                "/app/config_default/solution/%s",
                szFile
                );
    QFile srcFile( strSrcFileName );
    QString strDesFileName( "/app/config/solution/MonitoringSolution.cfg" );
    QFile desFile( strDesFileName );
    if ( !srcFile.exists() )
    {
        TRACEDEBUG( "g_cfgParam->writeLanguageApp <%s> not exist", strSrcFileName.toUtf8().constData() );
        return 1;
    }
    if ( desFile.exists() )
    {
        desFile.remove();
    }

    if ( !srcFile.copy(strDesFileName) )
    {
        TRACEDEBUG( "g_cfgParam->writeLanguageApp copy File<%s> error", strSrcFileName.toUtf8().constData() );
        return 2;
    }
    TRACEDEBUG( "g_cfgParam->writeLanguageApp() OK" );

    return 0;
}

bool ConfigParam::copyDirectoryFiles(
        const QString &fromDir,
        const QString &toDir)
{
    static int nFiles = 0;
    QDir sourceDir(fromDir);
    QDir targetDir(toDir);
    if ( !targetDir.exists() )
    {    /**< 如果目标目录不存在，则进行创建 */
        if ( !targetDir.mkdir(targetDir.absolutePath()) )
        {
            TRACELOG2( "g_cfgParam->copyDirectoryFiles 1" );
            return false;
        }
    }

    QFileInfoList fileInfoList = sourceDir.entryInfoList();
    foreach (QFileInfo fileInfo, fileInfoList)
    {
        if (fileInfo.fileName() == "." ||
                fileInfo.fileName() == "..")
        {
            continue;
        }

        if ( fileInfo.isDir() )
        {
            /**< 当为目录时，递归的进行copy */
            if ( !copyDirectoryFiles(fileInfo.filePath(),
                targetDir.filePath(fileInfo.fileName()))
                 )
            {
                TRACELOG2( "g_cfgParam->copyDirectoryFiles 2" );
                return false;
            }
        }
        else
        {
            /**< 当允许覆盖操作时，将旧文件进行删除操作 */
            if ( targetDir.exists(fileInfo.fileName()) )
            {
                targetDir.remove(fileInfo.fileName());
            }
            /// 进行文件copy
            TRACEDEBUG( "g_cfgParam->copyDirectoryFiles <%d> <%s> <%s>",
                        ++nFiles,
                        fileInfo.filePath().toUtf8().constData(),
                        targetDir.filePath(fileInfo.fileName()).toUtf8().constData() );
            if ( !QFile::copy(fileInfo.filePath(),
                targetDir.filePath(fileInfo.fileName())) )
            {
                TRACELOG2( "g_cfgParam->copyDirectoryFiles 3" );
                    return false;
            }
        }
    }
    nFiles = 0;
    return true;
}

void ConfigParam::getSysLangType(QStringList &strListDirLang)
{
    QDir dirDefaultLang("/app/config_default/lang");

    strListDirLang.append("en");

    QFileInfoList list = dirDefaultLang.entryInfoList(
                QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot);
    for (int i=0; i < list.size(); ++i)
    {
        QFileInfo fileInfo = list.at(i);

        //检查语言包简称的合法性
        //简称只能是"en/fr/es"
        QString strSupportLang = "en/fr/es";
        if(strSupportLang.indexOf (fileInfo.fileName ()) != -1)
        {
            strListDirLang.append (fileInfo.fileName ());
        }
    }
}

bool ConfigParam::removeDirectory(const QString &dirName)
{
    TRACEDEBUG( "g_cfgParam->removeDirectory<%s/*>", dirName.toUtf8().constData() );
    QDir dir(dirName);
    QString tmpdir = "";
    if ( !dir.exists() )
    {
        return false;
    }
    QFileInfoList fileInfoList = dir.entryInfoList();
    foreach (QFileInfo fileInfo, fileInfoList)
    {
        if(fileInfo.fileName() == "." || fileInfo.fileName() == "..")
            continue;

        if (fileInfo.isDir())
        {
            tmpdir = dirName + ("/") + fileInfo.fileName();
            removeDirectory(tmpdir);
            dir.rmdir(fileInfo.fileName()); /**< 移除子目录 */
            //TRACEDEBUG( "g_cfgParam->removeDirectory rmSubDir<%s>", fileInfo.fileName().toUtf8().constData() );
        }
        else if (fileInfo.isFile())
        {
            QFile tmpFile(fileInfo.fileName());
            dir.remove(tmpFile.fileName()); /**< 删除临时文件 */
            //TRACEDEBUG( "g_cfgParam->removeDirectory rmFile<%s>", tmpFile.fileName().toUtf8().constData() );
        }
    }
//    /**< 返回上级目录，因为只有返回上级目录，才可以删除这个目录 */
//    dir.cdUp();
//    if (dir.exists(dirName))
//    {
//        TRACEDEBUG( "g_cfgParam->removeDirectory rmTopDir<%s>", dirName.toUtf8().constData() );
//        if (!dir.rmdir(dirName))
//            return false;
//    }

    return true;
}


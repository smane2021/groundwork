#ifndef POSCURVE_H
#define POSCURVE_H

#include "PosBase.h"

#define SET_STYLE_LABEL_CURVE_BOTTOM(obj) \
{ \
    obj->setGeometry( \
        PosCurve::originX, \
        PosCurve::originY+3, \
        PosCurve::hAxisLen, \
        PosCurve::labelTxtHeight); \
    obj->setAlignment( Qt::AlignCenter ); \
    if (g_cfgParam->ms_initParam.lcdRotation != LCD_ROTATION_BIG) \
    { \
        obj->setFont( g_cfgParam->gFontSmallN ); \
    } \
}

#define SET_STYLE_LABEL_CURVE_TOP(obj) \
{ \
    obj->setGeometry( \
        PosBase::titleX1, \
        PosCurve::originY-PosCurve::vAxisLen-PosCurve::labelTxtHeight-10, \
        PosCurve::titleWidth1, \
        PosCurve::titleHeight); \
    obj->setAlignment( Qt::AlignCenter ); \
    if (g_cfgParam->ms_initParam.lcdRotation != LCD_ROTATION_BIG) \
    { \
        obj->setFont( g_cfgParam->gFontSmallN ); \
    } \
}

// PosBarChart
class PosCurve : public PosBase
{
public:
    PosCurve();
    virtual ~PosCurve();

    void init();

    static int originX;
    static int originY;

    static int hAxisLen;
    static int vAxisLen;

    static int iY_Text_OfSt; //Y������

    static int labelTopX;
    static int labelTxtHeight;
};

#endif // POSCURVE_H

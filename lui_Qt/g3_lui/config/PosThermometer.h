#ifndef POSTHERMOMETER_H
#define POSTHERMOMETER_H

#include "PosBase.h"
#include <QPoint>
#include <QSize>

class PosThermometer : public PosBase
{
public:
    PosThermometer();
    ~PosThermometer();

    void init();

    static int meterWidth;
    static int meterHeight;
    static int barHeight;
    static int penWidth;
    static int shortScaleLen;
    static int longScaleLen;
    static QPoint ptOrigin;
    static QSize  sizePointer;
    //--
    static int leftMargin;
};

#endif // POSTHERMOMETER_H

#include "PosThermometer.h"
#include "config/configparam.h"
int PosThermometer::meterWidth;
int PosThermometer::meterHeight;
int PosThermometer::barHeight;
int PosThermometer::penWidth;
int PosThermometer::shortScaleLen;
int PosThermometer::longScaleLen;
int PosThermometer::leftMargin;
QPoint PosThermometer::ptOrigin;
QSize  PosThermometer::sizePointer;

PosThermometer::PosThermometer()
{
}

PosThermometer::~PosThermometer()
{

}

void PosThermometer::init()
{
    switch (g_cfgParam->ms_initParam.lcdRotation)
    {
      case LCD_ROTATION_0DEG:
      {
          meterWidth     = 130;
          meterHeight    = 12;
          penWidth       = 2;
          shortScaleLen  = 3;
          longScaleLen   = 8;
          leftMargin     = 30;
          ptOrigin       = QPoint(10, 59);
          sizePointer    = QSize(10, 23);
      }
      break;

      case LCD_ROTATION_90DEG:
      {
          meterWidth     = 103;
          meterHeight    = 12;
          penWidth       = 2;
          shortScaleLen  = 3;
          longScaleLen   = 8;
          leftMargin     = 25;
          ptOrigin       = QPoint(10, 80);
          sizePointer    = QSize(10, 23);
      }
      break;

      case LCD_ROTATION_BIG:
      {
          meterWidth     = 260;
          meterHeight    = 32;
          penWidth       = 2;
          shortScaleLen  = 3;
          longScaleLen   = 8;
          leftMargin     = 30;
          ptOrigin       = QPoint(30, 120);
          sizePointer    = QSize(20, 46);
      }
      break;
    }
}

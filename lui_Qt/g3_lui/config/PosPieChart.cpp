#include "PosPieChart.h"
#include "config/configparam.h"
int PosPieChart::originX;
int PosPieChart::originY;
int PosPieChart::originY_DC;
int PosPieChart::nRadius;
int PosPieChart::originY_Batt;

QSize PosPieChart::sizePointer;
QSize PosPieChart::sizeCover;
int   PosPieChart::coverX;
int   PosPieChart::coverY;
QSize PosPieChart::sizeCenter;

int PosPieChart::nRadius_batt;
QSize PosPieChart::sizePointer_batt;
int   PosPieChart::coverY_batt;

int PosPieChart::nOffsetText;
QSize   PosPieChart::sizeTextAC;

QRect  PosPieChart::rectText1AC;
QRect  PosPieChart::rectText2AC;
QRect  PosPieChart::rectText3AC;

int PosPieChart::angleNegativeAxis;
int PosPieChart::anglesPieChart;

PosPieChart::PosPieChart()
{
}

PosPieChart::~PosPieChart()
{
}

void PosPieChart::init()
{
    int x1AC  ;
    int x2AC  ;
    int x3AC  ;
    int txtYAC;

    switch (g_cfgParam->ms_initParam.lcdRotation)
    {
      case LCD_ROTATION_0DEG:
      {
        originX    = 80;
        originY    = 80;
        originY_DC = 80;

        nRadius = 40;
        originY_Batt = 80;
        sizePointer = QSize(39,  10);
        sizeCover   = QSize(160, 75);
        coverX      = sizeCover.width()/2;
        coverY      = 47;
        sizeCenter  = QSize(10,  10);
        nOffsetText       = 20;


        nRadius_batt     = 40;
        sizePointer_batt = QSize(45,  10);
        coverY_batt      = 47;
        // AC label ����
        sizeTextAC   = QSize(40, 13);

        x1AC   = 15;
        x2AC   = 60;
        x3AC   = 105;
        txtYAC = 101;

        rectText1AC  = QRect( QPoint(x1AC, txtYAC), sizeTextAC );
        rectText2AC  = QRect( QPoint(x2AC, txtYAC), sizeTextAC );
        rectText3AC  = QRect( QPoint(x3AC, txtYAC), sizeTextAC );
      }
      break;

      case LCD_ROTATION_90DEG:
      {
        originX    = 64;
        originY    = 90;
        originY_DC = 90;
        nRadius = 37;
        originY_Batt = 120;

        sizePointer = QSize(33,  12);
        sizeCover   = QSize(128, 69);
        coverX      = sizeCover.width()/2+2;
        coverY      = 40;
        sizeCenter  = QSize(12,  12);
        nOffsetText       = 20;

        nRadius_batt     = 40;
        sizePointer_batt = QSize(49,  10);
        coverY_batt      = 42;
        sizeTextAC   = QSize(40, 12);

        x1AC   = 2;
        x2AC   = 44;
        x3AC   = 86;
        txtYAC = 128;

        rectText1AC  = QRect( QPoint( x1AC,txtYAC), sizeTextAC );
        rectText2AC  = QRect( QPoint( x2AC,txtYAC), sizeTextAC );
        rectText3AC  = QRect( QPoint( x3AC,txtYAC), sizeTextAC );
      }
      break;

      case LCD_ROTATION_BIG:
      {
        originX = 160;
        originY = 160;
        originY_DC = 170;
        nRadius = 75;
        originY_Batt = 170;

        sizePointer = QSize(75,  21);
        sizeCover   = QSize(320, 147);
        coverX      = sizeCover.width()/2;
        coverY      = 100;
        sizeCenter  = QSize(20, 20);

        nRadius_batt     = 83;
        sizePointer_batt = QSize(75,  21);
        coverY_batt      = 100;

        nOffsetText       = 30;

        x1AC   = 30;
        x2AC   = 120;
        x3AC   = 210;
        txtYAC = 203;
        sizeTextAC   = QSize(80, 25);

        rectText1AC  = QRect( QPoint(x1AC, txtYAC), sizeTextAC );
        rectText2AC  = QRect( QPoint(x2AC, txtYAC), sizeTextAC );
        rectText3AC  = QRect( QPoint(x3AC, txtYAC), sizeTextAC );
      }
      break;
    }
}

void PosPieChart::setPieChartType(void* pParam)
{
    enum PIE_CHART_TYPE type = *((enum PIE_CHART_TYPE*)pParam);

    switch (g_cfgParam->ms_initParam.lcdRotation)
    {
      case LCD_ROTATION_0DEG:
      {
        angleNegativeAxis = -21;
      }
      break;

      case LCD_ROTATION_90DEG:
      {
        if (type == PIE_CHART_TYPE_BATT)
        {
          angleNegativeAxis = -21;
        }
        else
        {
          angleNegativeAxis = -21;
        }
      }
      break;

      case LCD_ROTATION_BIG:
      {
      if (type == PIE_CHART_TYPE_BATT)
      {
      angleNegativeAxis = -20;
      }
      else
      {
      angleNegativeAxis = -20;
      }
      }
      break;
    }
    anglesPieChart    = 360-(180+angleNegativeAxis*2);
}

float PosPieChart::calcCoordinateAngle(
        float fVal,
        float fLowest,
        float fHighest
        )
{
    float fAngle = 0;
    switch ( g_cfgParam->ms_initParam.lcdRotation )
    {
    case LCD_ROTATION_0DEG:
    {
        fAngle = ((fVal-fLowest)*1.0 /
                  (fHighest-fLowest) *
                  anglesPieChart+angleNegativeAxis);
    }
        break;

    case LCD_ROTATION_90DEG:
    {
        fAngle = ((fVal-fLowest)*1.0 /
                  (fHighest-fLowest) *
                  anglesPieChart+angleNegativeAxis);
    }
        break;

    case LCD_ROTATION_BIG:
    {
        fAngle = ((fVal-fLowest)*1.0 /
                  (fHighest-fLowest) *
                  anglesPieChart+angleNegativeAxis+1);
    }
        break;
    }
    return fAngle;
}

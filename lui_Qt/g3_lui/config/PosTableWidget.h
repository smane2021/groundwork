#ifndef POSTABLEWIDGET_H
#define POSTABLEWIDGET_H

#include "PosBase.h"

class PosTableWidget : public PosBase
{
public:
    PosTableWidget();
    virtual ~PosTableWidget();

    void init();

    static int xHasTitle;
    static int yHasTitle;
    static int widthHasScrool;
    static int heightHasTitle;

    static int xNotTitle;
    static int yNotTitle;
    static int widthNotScrool;
    static int heightNotTitle;
    static int widthWords;

    static int rowsPerpageNotitle;
    static int rowsPerpageTitle;

    static int rowHeight;
    static int langWidth;

    static int yModule;
    static int heightHeader;
    // No. Iout(A) state   rect conv s3
    static int widthHeadRect1;
    static int widthHeadRect2;
    static int widthHeadRect3;
    // No. Vin(V) Iout(A)  sol
    static int widthHeadSol1;
    static int widthHeadSol2;
    static int widthHeadSol3;
};

#endif // POSTABLEWIDGET_H

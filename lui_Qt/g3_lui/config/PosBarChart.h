#ifndef POSBARCHART_H
#define POSBARCHART_H

#include "PosBase.h"

// PosCurve
class PosBarChart : public PosBase
{
public:
    PosBarChart();
    virtual ~PosBarChart();

    void init();

    static void setBarChartType(void* pParam);

    static int originX;
    static int originY;

    static int hAxisLen;
    static int vAxisLen;

    static int nPenHeight;
    static int nPenWidth;
    static int nItemHeight; // 每项占用的高度>nPenHeight
    static int offsetItemTextY; // 写信号值的偏移量

    static int nItemsLess;
    static int nBattInfoNum;

    //以下是Batt Remain页面的参数
    static int iBattHeight; // 单项占用像素
    static int iMargeLeft;  // 左边界
    static int iBattWidth;// 电池长度
    static int iText_Y; // 开始写字的Y点
    static int iText_Gap;  // 距离文字空隙Y
    static int iBatt_Dot_Y;  // 电池凸出的部分Y
    static int iBatt_Dot_Width; //电池凸出部分的宽度
    static int iBattChargeIconX;
    static QSize sizeChargeIcon;
};

#endif // POSBARCHART_H

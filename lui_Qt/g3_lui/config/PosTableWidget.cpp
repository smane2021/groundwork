#include "PosTableWidget.h"
#include "config/configparam.h"

int PosTableWidget::xHasTitle;
int PosTableWidget::yHasTitle;
int PosTableWidget::widthHasScrool;
int PosTableWidget::heightHasTitle;

int PosTableWidget::xNotTitle;
int PosTableWidget::yNotTitle;
int PosTableWidget::widthNotScrool;
int PosTableWidget::heightNotTitle;
int PosTableWidget::widthWords;

int PosTableWidget::rowsPerpageNotitle;
int PosTableWidget::rowsPerpageTitle;

int PosTableWidget::rowHeight;
int PosTableWidget::langWidth;

int PosTableWidget::yModule;
int PosTableWidget::heightHeader;
int PosTableWidget::widthHeadRect1;
int PosTableWidget::widthHeadRect2;
int PosTableWidget::widthHeadRect3;
int PosTableWidget::widthHeadSol1;
int PosTableWidget::widthHeadSol2;
int PosTableWidget::widthHeadSol3;

PosTableWidget::PosTableWidget()
{
}

PosTableWidget::~PosTableWidget()
{

}

void PosTableWidget::init()
{
    switch (g_cfgParam->ms_initParam.lcdRotation)
    {
        case LCD_ROTATION_0DEG:
        {
          langWidth = 160;
          rowHeight = 19;

          xHasTitle = 3;
          yHasTitle = 24;
          widthHasScrool = 153;
          widthWords     = 153;
          heightHasTitle = rowHeight*5;

          xNotTitle = 3;
          yNotTitle = 5;
          widthNotScrool = 153;
          heightNotTitle = rowHeight*6;

          rowsPerpageNotitle = 6;
          rowsPerpageTitle   = 5;

          yModule        = 0;
          heightHeader   = 24;
          widthHeadRect1 = 50;
          widthHeadRect2 = 50;
          widthHeadRect3 = 55;
          widthHeadSol1  = 50;
          widthHeadSol2  = 50;
          widthHeadSol3  = 55;
        }
        break;

        case LCD_ROTATION_90DEG:
        {
          langWidth = 121;
          rowHeight = 21;

          xHasTitle = 3;
          yHasTitle = 24;
          widthHasScrool = 122;
          widthWords     = 122;
          heightHasTitle = rowHeight*6;

          xNotTitle = 3;
          yNotTitle = 5;
          widthNotScrool = 122;
          heightNotTitle = rowHeight*7;

          rowsPerpageNotitle = 7;
          rowsPerpageTitle   = 6;

          yModule        = 0;
          heightHeader   = 24;
          widthHeadRect1 = 36;
          widthHeadRect2 = 48;
          widthHeadRect3 = 42;
          widthHeadSol1  = 35;
          widthHeadSol2  = 44;
          widthHeadSol3  = 47;
        }
        break;

        case LCD_ROTATION_BIG:
        {
          xHasTitle = 6;
          xNotTitle = 6;

          yHasTitle = 45;
          yNotTitle = 15;

          widthHasScrool = 306;
          widthWords     = 302;
          widthNotScrool = 306;

          rowHeight = 30;
          langWidth = 306;

          heightHasTitle = rowHeight * 6;
          heightNotTitle = rowHeight * 7;

          rowsPerpageNotitle = 7;
          rowsPerpageTitle   = 6;

          yModule        = 0;
          heightHeader   = 45;

          widthHeadRect1 = 110;
          widthHeadRect2 = 100;
          widthHeadRect3 = 104;
          widthHeadSol1  = 114;
          widthHeadSol2  = 100;
          widthHeadSol3  = 100;
        }
        break;
    }
}

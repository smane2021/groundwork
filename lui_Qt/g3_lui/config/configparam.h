/******************************************************************************
文件名：    configparam.h
功能：      此类定义各种参数的配置，在运行时用户可动态更改
作者：      刘金煌
创建日期：   2013年3月24日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef CONFIGPARAM_H
#define CONFIGPARAM_H

#include <QStringList>
#include <QObject>
#include <QFont>
#include <QFontMetrics>
#include "dataDef.h"

class ConfigParam: public QObject
{
   Q_OBJECT

public:
    ConfigParam();

public:
    int iniPos();
    int initParam(bool bWait=false);
    void initAlmVoiceCloseStat();
    void setLanguageFont();
    void resetAlmVoiceClose();
    int readLanguageLocal();
    int writeLanguageLocal();
    int readLanguageApp();
    int writeLanguageApp();

public:
    static void getSysLangType(QStringList &stlSysLang);

private:
    static bool removeDirectory(const QString &dirName);
    static bool copyDirectoryFiles(
            const QString &fromDir,
            const QString &toDir);

public:
    static Init_Param ms_initParam;
    static QString    pathImg;
    static int        ms_maxCharsPerTableRow;
    static QFont gFontLargeN;
    static QFont gFontLargeB;
    static QFont gFontSmallN; //G3总共用到的3种字体

    static QFontMetrics* ms_pFmLargeN;
    static QFontMetrics* ms_pFmLargeB;
    static QFontMetrics* ms_pFmSmallN;
};

extern ConfigParam* g_cfgParam;

#endif // CONFIGPARAM_H

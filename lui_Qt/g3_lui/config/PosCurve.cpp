#include "PosCurve.h"
#include "config/configparam.h"
int PosCurve::originX;
int PosCurve::originY;

int PosCurve::hAxisLen;
int PosCurve::vAxisLen;

int PosCurve::labelTopX;
int PosCurve::labelTxtHeight;
int PosCurve::iY_Text_OfSt;

PosCurve::PosCurve()
{
}

PosCurve::~PosCurve()
{

}

void PosCurve::init()
{
    switch (g_cfgParam->ms_initParam.lcdRotation)
    {
      case LCD_ROTATION_0DEG:
      {
        originX = 20;
        originY = 102;
        hAxisLen     = 110;
        vAxisLen     = 62;  // 79

        labelTopX = 4;
        labelTxtHeight = 15;
        iY_Text_OfSt = 22;
      }
      break;

      case LCD_ROTATION_90DEG:
      {
        originX = 20;
        originY = 134;
        hAxisLen     = 74;
        vAxisLen     = 90;

        labelTopX = 2;
        labelTxtHeight = 15;
        iY_Text_OfSt = 20;
      }
      break;

      case LCD_ROTATION_BIG:
      {
        originX = 56;
        originY = 195; //
        hAxisLen     = 287-originX;
        vAxisLen     = 128;

        labelTopX = 4;
        labelTxtHeight = 30;
        iY_Text_OfSt = 35;
      }
      break;
    }
}

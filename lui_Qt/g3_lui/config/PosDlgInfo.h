#ifndef POSDLGINFO_H
#define POSDLGINFO_H

#include "PosBase.h"

class PosDlgInfo : public PosBase
{
public:
    PosDlgInfo();
    virtual ~PosDlgInfo();

    void init();

    static int dlgInfoX;
    static int dlgInfoY;
    static int dlgInfoWidth;
    static int dlgInfoHeight;

    static int labelHeight;
    static int contentX;
    static int contentY;
    static int contentWidth;
    static int okY;
    static int okWidth;
    static int cancelY;
    static int cancelWidth;

};

#endif // POSDLGINFO_H

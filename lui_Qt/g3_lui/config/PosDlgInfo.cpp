#include "PosDlgInfo.h"
#include "config/configparam.h"
int PosDlgInfo::dlgInfoX;
int PosDlgInfo::dlgInfoY;
int PosDlgInfo::dlgInfoWidth;
int PosDlgInfo::dlgInfoHeight;

int PosDlgInfo::labelHeight;
int PosDlgInfo::contentX;
int PosDlgInfo::contentY;
int PosDlgInfo::contentWidth;
int PosDlgInfo::okY;
int PosDlgInfo::okWidth;
int PosDlgInfo::cancelY;
int PosDlgInfo::cancelWidth;
PosDlgInfo::PosDlgInfo()
{
}

PosDlgInfo::~PosDlgInfo()
{

}

void PosDlgInfo::init()
{
  switch (g_cfgParam->ms_initParam.lcdRotation)
  {
    case LCD_ROTATION_0DEG:
    {
      dlgInfoX      = 0;
      dlgInfoY      = 0;
      dlgInfoWidth  = 160;
      dlgInfoHeight = 128;

      labelHeight  = 20;
      contentX = 6;
      contentY  = 3;
      contentWidth = dlgInfoWidth-12;

      cancelY      = 65;
      cancelWidth  = dlgInfoWidth-4;
      okY          = 90;
      okWidth      = dlgInfoWidth-4;
    }
    break;

    case LCD_ROTATION_90DEG:
    {
      dlgInfoX      = 0;
      dlgInfoY      = 0;
      dlgInfoWidth  = 128;
      dlgInfoHeight = 160;

      labelHeight  = 20;

      contentX = 6;
      contentY     = 3;
      contentWidth = dlgInfoWidth-12;

      cancelY      = contentY + 30;
      cancelWidth  = dlgInfoWidth-4;

      okY          = cancelY + 25;
      okWidth      = dlgInfoWidth-4;
    }
    break;

    case LCD_ROTATION_BIG:
    {
      dlgInfoX      = 0;
      dlgInfoY      = 0;
      dlgInfoWidth  = 320;
      dlgInfoHeight = 240;
      labelHeight  = 32;
      contentX = 22;
      contentY     = 6;
      contentWidth = dlgInfoWidth-44;

      cancelY      = contentY + 45;
      cancelWidth  = dlgInfoWidth-25;

      okY          = cancelY + 30;
      okWidth      = dlgInfoWidth-25;
    }
    break;
  }
}

#include "PosBase.h"

#include <QDesktopWidget>
#include <QApplication>
#include <QRect>
#include "common/pubInclude.h"
#include "config/configparam.h"
#include "common/uidefine.h"

int PosBase::screenX;
int PosBase::screenY;
int PosBase::screenWidth;
int PosBase::screenHeight;

int PosBase::screenPopupX;
int PosBase::screenPopupY;
int PosBase::screenPopupWidth;
int PosBase::screenPopupHeight;

int PosBase::titleHeight;
int PosBase::titleHeight2Rows;
int PosBase::titleHeightInv;
int PosBase::titleInvY;
int PosBase::titleX1;
int PosBase::titleY1;
int PosBase::titleWidth1;

int PosBase::titleX2;
int PosBase::titleY2;
int PosBase::titleWidth2;

int PosBase::lastMarkLenAxis;
int PosBase::axisMarkLen;
int PosBase::hAxisTextY;
int PosBase::vAxisTextXOffset;
int PosBase::unitTextOffset;

int PosBase::screenSaverRectWidth;
QRect PosBase::rectScrollBar;

int PosBase::iSpinBox_Dot_Width;
int PosBase::iSpinBox_IP_Width;
int PosBase::iSpinBox_IP_Width_V6;
int PosBase::iSPinBox_Char_Width;
const char* PosBase::strImgBack_Line;
const char* PosBase::strImgBack_Title;
const char* PosBase::strImgBack_None;
const char* PosBase::strImgBack_Line_Title;
QString PosBase::strAlignSpace;

PosBase::PosBase()
{
}

PosBase::~PosBase()
{

}

void PosBase::init()
{
    screenX = 0;
    screenY = 0;

    lastMarkLenAxis = 5;
    hAxisTextY      = 15;
    vAxisTextXOffset= 2;

    strImgBack_Line = "back_line.png";
    strImgBack_None = "back_none.png";
    strImgBack_Line_Title = "back_line_title.png";
    strImgBack_Title = "back_title.png";

    switch (g_cfgParam->ms_initParam.lcdRotation)
    {
      case LCD_ROTATION_0DEG:
      {
        g_cfgParam->pathImg = QString(PATH_APP)+"imgH/";
        screenWidth   = 160;
        screenHeight  = 128;

        screenPopupX  = 10;
        screenPopupY  = 20;
        screenPopupWidth  = screenWidth-screenPopupX*2;
        screenPopupHeight = screenHeight-screenPopupY*2;

        titleHeight   = 24; //16
        titleHeight2Rows = 30;
        titleHeightInv= 28;
        titleInvY     = screenHeight-titleHeightInv;
        titleX1       = 0;
        titleY1       = 0;
        titleWidth1   = 156;

        titleX2       = titleX1;
        titleY2       = 0; //15
        titleWidth2   = titleWidth1;

        axisMarkLen     = 2;
        unitTextOffset  = 6;

        rectScrollBar.setX( 156 );
        rectScrollBar.setY( 0 );
        rectScrollBar.setSize( QSize(4,128) );

        screenSaverRectWidth = 40;

        iSpinBox_Dot_Width   = 3;
        iSpinBox_IP_Width    = 30;
        iSpinBox_IP_Width_V6 = 37;
        iSPinBox_Char_Width  = 17;

        strAlignSpace = "                ";
      }
      break;

      case LCD_ROTATION_90DEG:
      {
        g_cfgParam->pathImg = QString(PATH_APP)+"imgH/";
        strImgBack_Line = "back_line_V.png";
        strImgBack_None = "back_none_V.png";
        strImgBack_Line_Title = "back_line_title_V.png";
        strImgBack_Title = "back_title_V.png";
        screenWidth   = 128;
        screenHeight  = 160;

        screenPopupX  = 20;
        screenPopupY  = 10;
        screenPopupWidth  = screenWidth-screenPopupX*2;
        screenPopupHeight = screenHeight-screenPopupY*2;

        titleHeight   = 24; //16
        titleHeight2Rows = 40;
        titleHeightInv = 30;
        titleInvY     = screenHeight-titleHeightInv;
        titleX1       = 0;
        titleY1       = 0;
        titleWidth1   = 124;

        titleX2       = titleX1;
        titleY2       = 0;
        titleWidth2   = titleWidth1;

        axisMarkLen = 2;
        unitTextOffset  = 6;

        rectScrollBar.setX( 124 );
        rectScrollBar.setY( 0 );
        rectScrollBar.setSize( QSize(4,160) );

        screenSaverRectWidth = 40;

        iSpinBox_Dot_Width   = 3;
        iSpinBox_IP_Width    = 29;
        iSpinBox_IP_Width_V6 = 37;
        iSPinBox_Char_Width  = 17;

        strAlignSpace = "         ";
      }
      break;

      case LCD_ROTATION_BIG:
      {
        g_cfgParam->pathImg = QString(PATH_APP)+"imgB/";
        screenWidth   = 320;
        screenHeight  = 240;

        screenPopupX        = 30;
        screenPopupY        = 20;
        screenPopupWidth    = screenWidth-screenPopupX*2;
        screenPopupHeight   = screenHeight-screenPopupY*2;

        titleHeight   = 45; //28
        titleHeight2Rows = 56; //28*2
        titleHeightInv = 35;
        titleInvY     = screenHeight-titleHeightInv;
        titleX1       = 0; //8
        titleY1       = 0; //3
        titleWidth1   = 316; //300

        titleX2       = titleX1;
        titleY2       = 0;
        titleWidth2   = titleWidth1;

        lastMarkLenAxis  = 10;
        axisMarkLen      = 4;
        unitTextOffset   = 8;//14
        hAxisTextY       = 22;
        vAxisTextXOffset = 2;

        rectScrollBar.setX( 316 );
        rectScrollBar.setY( 0 );
        rectScrollBar.setSize( QSize(4,240) );

        screenSaverRectWidth = 100;

        iSpinBox_Dot_Width   = 6;
        iSpinBox_IP_Width    = 52;
        iSpinBox_IP_Width_V6 = 62;
        iSPinBox_Char_Width  = 23;

        strAlignSpace = "                       ";
      }
      break;
    }

    TRACELOG1( "PosBase::init() g_cfgParam->pathImg<%s>",
               g_cfgParam->pathImg.toUtf8().constData() );
}

#ifndef POSPIECHART_H
#define POSPIECHART_H

#include "PosBase.h"
#include <QSize>
#include <QPoint>
#include <QRect>

enum PIE_CHART_TYPE
{
    PIE_CHART_TYPE_AC,
    PIE_CHART_TYPE_DC,
    PIE_CHART_TYPE_BATT,
    PIE_CHART_TYPE_MAX
};

class PosPieChart : public PosBase
{
public:
    PosPieChart();
    virtual ~PosPieChart();

    void init();

    static void  setPieChartType(void* pParam);
    static float calcCoordinateAngle(float fVal, float fLowest, float fHighest);

    static int originX;
    static int originY;
    static int originY_DC;
    static int originY_Batt;
    static int nRadius;

    static QSize sizePointer;
    static QSize sizeCover;
    static int   coverX;
    static int   coverY;
    static QSize sizeCenter;

    static int   nRadius_batt;
    static QSize sizePointer_batt;
    static int   coverY_batt;

    static int nOffsetText;

    static QSize   sizeTextAC;

    static QRect  rectText1AC;
    static QRect  rectText2AC;
    static QRect  rectText3AC;

    static int angleNegativeAxis;
    static int anglesPieChart;
};

#endif // POSPIECHART_H

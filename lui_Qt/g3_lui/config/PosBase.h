#ifndef POSBASE_H
#define POSBASE_H
#include <QRect>
class PosBase
{
public:
    PosBase();
    virtual ~PosBase();

    virtual void init();

    static int screenX;
    static int screenY;
    static int screenWidth;
    static int screenHeight;

    static int screenPopupX;
    static int screenPopupY;
    static int screenPopupWidth;
    static int screenPopupHeight;

    static int titleHeight;      // title height
    static int titleHeight2Rows; // title height for 2 rows
    static int titleHeightInv;   // title height(WdgInventory)
    static int titleInvY;
    static int titleX1;
    static int titleY1;
    static int titleWidth1;

    // label title 有两种不同位置一种靠顶部，一种往下一些
    static int titleX2;
    static int titleY2;
    static int titleWidth2;

    static QRect rectScrollBar;
    static int screenSaverRectWidth;
    static int lastMarkLenAxis; // the last scale length of axis
    static int axisMarkLen; // axis scale length
    static int hAxisTextY;
    static int vAxisTextXOffset;
    static int unitTextOffset;

    static int iSpinBox_IP_Width;
    static int iSpinBox_IP_Width_V6;
    static int iSpinBox_Dot_Width;
    static int iSPinBox_Char_Width;

    static const char* strImgBack_None;
    static const char* strImgBack_Title;
    static const char* strImgBack_Line;
    static const char* strImgBack_Line_Title;
    static QString     strAlignSpace;

};

#endif // POSBASE_H

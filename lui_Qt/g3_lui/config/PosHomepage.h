#ifndef POSHOMEPAGE_H
#define POSHOMEPAGE_H

#include <QSize>
#include <QRect>
#include "PosBase.h"

class PosHomepage : public PosBase
{
public:
    PosHomepage();
    virtual ~PosHomepage();

    void init();

    static QRect rectBtnAlarm;
    static QRect rectBtnCfg;
    static QRect rectBtnAC;
    static QRect rectBtnModule;
    static QRect rectBtnDC;
    static QRect rectBtnBatt;

    static QRect rectLineH1;
    static QRect rectLineH2;
    static QRect rectLineV;

    static QRect rectLabelDateTime;
    static QRect rectLabelDC;
    static QRect rectLabelBatt;
    static QRect rectLabelSysused;
    static QRect rectAlarmNum;

    ;
    // slave
    static QRect rectSlaveLabelV;
    static QRect rectSlaveLabelA;

private:
    static QSize sizeBtnTitle;
    static QSize sizeBtnMain;
    static QSize sizeLineH;
    static QSize sizeLineV;
};

#endif // POSHOMEPAGE_H

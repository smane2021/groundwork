#include "PosHomepage.h"

#include "config/configparam.h"

QSize PosHomepage::sizeBtnTitle;
QSize PosHomepage::sizeBtnMain;
QSize PosHomepage::sizeLineH;
QSize PosHomepage::sizeLineV;

QRect PosHomepage::rectBtnAlarm;
QRect PosHomepage::rectBtnCfg;
QRect PosHomepage::rectBtnAC;
QRect PosHomepage::rectBtnModule;
QRect PosHomepage::rectBtnDC;
QRect PosHomepage::rectBtnBatt;

QRect PosHomepage::rectLineH1;
QRect PosHomepage::rectLineH2;
QRect PosHomepage::rectLineV;

QRect PosHomepage::rectLabelDateTime;
QRect PosHomepage::rectLabelDC;
QRect PosHomepage::rectLabelBatt;
QRect PosHomepage::rectLabelSysused;

// slave
QRect PosHomepage::rectSlaveLabelV;
QRect PosHomepage::rectSlaveLabelA;

QRect PosHomepage::rectAlarmNum;

PosHomepage::PosHomepage()
{
}

PosHomepage::~PosHomepage()
{

}

void PosHomepage::init()
{
    static QSize sizeSlaveLabel;
    switch (g_cfgParam->ms_initParam.lcdRotation)
    {
      case LCD_ROTATION_0DEG:
      {
        // 24*24
        sizeBtnTitle.setWidth( 19 );
        sizeBtnTitle.setHeight( 19 );

        sizeBtnMain.setWidth( 28 );
        sizeBtnMain.setHeight( 28 );

        sizeLineH.setWidth( 26 );
        sizeLineH.setHeight( 8 );

        sizeLineV.setWidth( 8 );
        sizeLineV.setHeight( 20 );

        // title btn
        rectBtnAlarm.setX( 105 );
        rectBtnAlarm.setY( 3 );
        rectBtnAlarm.setSize( sizeBtnTitle );

        rectBtnCfg.setX( 2 );
        rectBtnCfg.setY( 3 );
        rectBtnCfg.setSize( sizeBtnTitle );

        // main btn
        rectBtnAC.setX( 10 );
        rectBtnAC.setY( 32 );
        rectBtnAC.setSize( sizeBtnMain );

        rectBtnModule.setX( 66 );
        rectBtnModule.setY( 32 );
        rectBtnModule.setSize( sizeBtnMain );

        rectBtnDC.setX( 120 );
        rectBtnDC.setY( 32 );
        rectBtnDC.setSize( sizeBtnMain );

        rectBtnBatt.setX( 66 );
        rectBtnBatt.setY( 76 );
        rectBtnBatt.setSize( sizeBtnMain );

        // line
        rectLineH1.setX( 39 );
        rectLineH1.setY( 44 );
        rectLineH1.setSize( sizeLineH );

        rectLineH2.setX( 94 );
        rectLineH2.setY( 44 );
        rectLineH2.setSize( sizeLineH );

        rectLineV.setX( 78 );
        rectLineV.setY( 59 );
        rectLineV.setSize( sizeLineV );

        // label
        rectLabelDateTime.setX( 25 );
        rectLabelDateTime.setY( 2 );
        rectLabelDateTime.setSize( QSize(70, 20) );

        rectAlarmNum.setX (124);
        rectAlarmNum.setY (2);
        rectAlarmNum.setSize(QSize(35,20));

        rectLabelDC.setX( 100 );
        rectLabelDC.setY( 76 );
        rectLabelDC.setSize( QSize(60,28) );

        rectLabelBatt.setX( 0 );
        rectLabelBatt.setY( 76 );
        rectLabelBatt.setSize( QSize(65, 28) );

        rectLabelSysused.setX(0 );
        rectLabelSysused.setY( 103 );
        rectLabelSysused.setSize( QSize(160,20) );

        // slave
        sizeSlaveLabel.setWidth(  40 );
        sizeSlaveLabel.setHeight( 20 );
        rectSlaveLabelV.setX( 26 );
        rectSlaveLabelV.setY( 60 );
        rectSlaveLabelV.setSize( sizeSlaveLabel );

        rectSlaveLabelA.setX( 76 );
        rectSlaveLabelA.setY( 60 );
        rectSlaveLabelA.setSize( sizeSlaveLabel );
      }
      break;

      case LCD_ROTATION_90DEG:
      {
        // 24*24
        sizeBtnTitle.setWidth( 18 );
        sizeBtnTitle.setHeight( 18 );

        sizeBtnMain.setWidth( 25 );
        sizeBtnMain.setHeight( 25 );

        sizeLineH.setWidth( 19 );
        sizeLineH.setHeight( 8 );

        sizeLineV.setWidth( 8 );
        sizeLineV.setHeight( 29 );

        // title btn
        rectBtnAlarm.setX( 76);
        rectBtnAlarm.setY( 3 );
        rectBtnAlarm.setSize( sizeBtnTitle );

        rectBtnCfg.setX( 3 );
        rectBtnCfg.setY( 3 );
        rectBtnCfg.setSize( sizeBtnTitle );

        // main btn
        rectBtnAC.setX( 8 );
        rectBtnAC.setY( 46 );
        rectBtnAC.setSize( sizeBtnMain );

        rectBtnModule.setX( 51 );
        rectBtnModule.setY( 46 );
        rectBtnModule.setSize( sizeBtnMain );

        rectBtnDC.setX( 95 );
        rectBtnDC.setY( 46 );
        rectBtnDC.setSize( sizeBtnMain );

        rectBtnBatt.setX( 51 );
        rectBtnBatt.setY( 97 );
        rectBtnBatt.setSize( sizeBtnMain );

        // line
        rectLineH1.setX( 32 );
        rectLineH1.setY( 56 );
        rectLineH1.setSize( sizeLineH );

        rectLineH2.setX( 77 );
        rectLineH2.setY( 56 );
        rectLineH2.setSize( sizeLineH );

        rectLineV.setX( 62 );
        rectLineV.setY( 71 );
        rectLineV.setSize( sizeLineV );

        // label
        rectLabelDateTime.setX( 24 );
        rectLabelDateTime.setY( 2 );
        rectLabelDateTime.setSize( QSize(52, 20) );

        rectAlarmNum.setX (94);
        rectAlarmNum.setY (2);
        rectAlarmNum.setSize(QSize(30,20));

        rectLabelDC.setX( 79 );
        rectLabelDC.setY( 81 );
        rectLabelDC.setSize( QSize(48, 32) );

        rectLabelBatt.setX( 3 );
        rectLabelBatt.setY(81 );
        rectLabelBatt.setSize( QSize(45, 42));

        rectLabelSysused.setX( 0 );
        rectLabelSysused.setY(123 );
        rectLabelSysused.setSize( QSize(128, 20) );

        // slave
        sizeSlaveLabel.setWidth(  50 );
        sizeSlaveLabel.setHeight( 30 );
        rectSlaveLabelV.setX( 13 );
        rectSlaveLabelV.setY( 50 );
        rectSlaveLabelV.setSize( sizeSlaveLabel );

        rectSlaveLabelA.setX( 93 );
        rectSlaveLabelA.setY( 50 );
        rectSlaveLabelA.setSize( sizeSlaveLabel );
      }
      break;

      case LCD_ROTATION_BIG:
      {
        sizeBtnTitle.setWidth( 35 );
        sizeBtnTitle.setHeight( 35 );

        sizeBtnMain.setWidth( 42 );
        sizeBtnMain.setHeight( 42 );

        sizeLineH.setWidth( 55 );
        sizeLineH.setHeight( 11 );

        sizeLineV.setWidth( 11 );
        sizeLineV.setHeight( 40 );

        // title btn
        rectBtnAlarm.setX( 230 );
        rectBtnAlarm.setY( 4 );
        rectBtnAlarm.setSize( sizeBtnTitle );

        rectBtnCfg.setX( 22 );
        rectBtnCfg.setY( 4 );
        rectBtnCfg.setSize( sizeBtnTitle );

        // main btn
        rectBtnAC.setX( 40 );
        rectBtnAC.setY( 64 );
        rectBtnAC.setSize( sizeBtnMain );

        rectBtnModule.setX( 139 );
        rectBtnModule.setY( 64 );
        rectBtnModule.setSize( sizeBtnMain );

        rectBtnDC.setX( 237 );
        rectBtnDC.setY( 64 );
        rectBtnDC.setSize( sizeBtnMain );

        rectBtnBatt.setX( 139 );
        rectBtnBatt.setY( 143 );
        rectBtnBatt.setSize( sizeBtnMain );

        // line
        rectLineH1.setX( 82 );
        rectLineH1.setY( 80 );
        rectLineH1.setSize( sizeLineH );

        rectLineH2.setX( 181 );
        rectLineH2.setY( 80 );
        rectLineH2.setSize( sizeLineH );

        rectLineV.setX( 158 );
        rectLineV.setY( 106 );
        rectLineV.setSize( sizeLineV );

        // label
        rectLabelDateTime.setX( 70 );
        rectLabelDateTime.setY( 6 );
        rectLabelDateTime.setSize( QSize(163, 30) );

        rectAlarmNum.setX(270);
        rectAlarmNum.setY(6);
        rectAlarmNum.setSize( QSize(47,30) );

        rectLabelDC.setX( 214 );
        rectLabelDC.setY( 143 );
        rectLabelDC.setSize( QSize(100, 42) );

        rectLabelBatt.setX( 15 );
        rectLabelBatt.setY( 143 );
        rectLabelBatt.setSize( QSize(120, 42) );

        rectLabelSysused.setX( 0 );
        rectLabelSysused.setY( 185 );
        rectLabelSysused.setSize( QSize(320, 26) );

        // slave
        sizeSlaveLabel.setWidth(  70 );
        sizeSlaveLabel.setHeight( 25 );
        rectSlaveLabelV.setX( 82 );
        rectSlaveLabelV.setY( 112 );
        rectSlaveLabelV.setSize( sizeSlaveLabel );

        rectSlaveLabelA.setX( 171 );
        rectSlaveLabelA.setY( 103 );
        rectSlaveLabelA.setSize( sizeSlaveLabel );
      }
      break;
    }
}

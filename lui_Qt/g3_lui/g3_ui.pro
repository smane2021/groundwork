#-------------------------------------------------
#
# Project created by QtCreator 2013-03-14T09:12:29
#
#-------------------------------------------------
win32{
QT += testlib
}

# M830B M830D
# M830B是1.8寸(128*160像素) M830D是3.2寸(240*320像素)

#DEFINES += _DEBUG

QT     += core gui
CONFIG += qt
#    debug

TARGET = g3_lui
TEMPLATE = app
DESTDIR = ../output
INCLUDEPATH += ../public
LIBS += -L ../output

SOURCES += \
    basicWidget/mainwindow.cpp \
    basicWidget/homepagewindow.cpp \
    basicWidget/basewindow.cpp \
    style/BaseStyle.cpp \
    common/splashscreen.cpp \
    common/equipbutton.cpp \
    common/buzztoolbutton_hp.cpp \
    common/buzztoolbutton.cpp \
    common/buzztablewidget.cpp \
    common/buzzpushbutton.cpp \
    common/basicwidget.cpp \
    config/configparam.cpp \
    equipWidget/FirstStackedWdg.cpp \
    equipWidget/SecondStackedWdg.cpp \
    equipWidget/ThirdStackedWdg.cpp \
    common/global.cpp \
    common/BuzzSpinBox.cpp \
    common/BuzzDoubleSpinBox.cpp \
    common/CtrlInputChar.cpp \
    equipWidget/WdgFP2DCV.cpp \
    equipWidget/WdgFP3DCA.cpp \
    equipWidget/WdgFP4Deg1.cpp \
    equipWidget/WdgFP0AC.cpp \
    equipWidget/WdgFP5Deg2Curve.cpp \
    equipWidget/Wdg2DCABranch.cpp \
    equipWidget/Wdg2Table.cpp \
    equipWidget/WdgFP1Module.cpp \
    equipWidget/Wdg2DCDeg1Branch.cpp \
    equipWidget/WdgFP6BattRemainTime.cpp \
    equipWidget/WdgFP7BattDegMeter.cpp \
    equipWidget/WdgFP8BattDegCurve.cpp \
    equipWidget/Wdg2P5BattRemainTime.cpp \
    equipWidget/Wdg2P6BattDeg.cpp \
    equipWidget/FourthStackedWdg.cpp \
    common/MultiDemon.cpp \
    config/PosCurve.cpp \
    config/PosBase.cpp \
    config/PosThermometer.cpp \
    basicWidget/GuideWindow.cpp \
    config/PosTableWidget.cpp \
    equipWidget/WdgFP11ScreenSaver.cpp \
    equipWidget/WdgFP10Inventory.cpp \
    equipWidget/Wdg3Table.cpp \
    util/DlgLoading.cpp \
    config/PosBarChart.cpp \
    config/PosHomepage.cpp \
    config/PosPieChart.cpp \
    util/DlgInfo.cpp \
    util/ThreadFlashLight.cpp \
    common/my_time_edit.cpp \
    common/sysInitEx.cpp \
    basicWidget/LoginWindow.cpp \
    config/PosDlgInfo.cpp \
    common/BuzzDateEdit.cpp \
    configWidget/WdgFCfgGroup.cpp \
    main.cpp \
    configWidget/CMenuData.cpp \
    util/DlgUpdateApp.cpp \
    equipWidget/WdgInventory.cpp \
    equipWidget/WdgAlmMenu.cpp \
    equipWidget/WdgFP6BattInfo.cpp

HEADERS  += \
    basicWidget/mainwindow.h \
    basicWidget/homepagewindow.h \
    basicWidget/basewindow.h \
    style/BaseStyle.h \
    common/uidefine.h \
    common/splashscreen.h \
    common/global.h \
    common/equipbutton.h \
    common/buzztoolbutton_hp.h \
    common/buzztoolbutton.h \
    common/buzztablewidget.h \
    common/buzzpushbutton.h \
    common/basicwidget.h \
    common/sysInitEx.h \
    ../public/dataDef.h \
    config/configparam.h \
    ../public/pubDef.h \
    equipWidget/FirstStackedWdg.h \
    equipWidget/SecondStackedWdg.h \
    equipWidget/ThirdStackedWdg.h \
    common/pubInclude.h \
    common/BuzzSpinBox.h \
    common/BuzzDoubleSpinBox.h \
    common/CtrlInputChar.h \
    equipWidget/WdgFP2DCV.h \
    equipWidget/WdgFP3DCA.h \
    equipWidget/WdgFP4Deg1.h \
    equipWidget/WdgFP0AC.h \
    equipWidget/WdgFP5Deg2Curve.h \
    equipWidget/Wdg2DCABranch.h \
    equipWidget/Wdg2Table.h \
    equipWidget/WdgFP1Module.h \
    equipWidget/Wdg2DCDeg1Branch.h \
    equipWidget/WdgFP6BattRemainTime.h \
    equipWidget/WdgFP7BattDegMeter.h \
    equipWidget/WdgFP8BattDegCurve.h \
    equipWidget/Wdg2P5BattRemainTime.h \
    equipWidget/Wdg2P6BattDeg.h \
    common/InputCtrlDef.h \
    equipWidget/FourthStackedWdg.h \
    common/MultiDemon.h \
    config/PosCurve.h \
    config/PosBase.h \
    config/PosThermometer.h \
    basicWidget/GuideWindow.h \
    config/PosTableWidget.h \
    equipWidget/WdgFP11ScreenSaver.h \
    equipWidget/WdgFP10Inventory.h \
    equipWidget/Wdg3Table.h \
    util/DlgLoading.h \
    common/Macro.h \
    config/PosBarChart.h \
    config/PosHomepage.h \
    config/PosPieChart.h \
    util/DlgInfo.h \
    util/ThreadFlashLight.h \
    common/my_time_edit.h \
    basicWidget/LoginWindow.h \
    config/PosDlgInfo.h \
    common/BuzzDateEdit.h \
    configWidget/WdgFCfgGroup.h \
    configWidget/CMenuData.h \
    util/DlgUpdateApp.h \
    equipWidget/WdgInventory.h \
    equipWidget/WdgAlmMenu.h \
    equipWidget/WdgFP6BattInfo.h

FORMS    += \
    basicWidget/mainwindow.ui \
    basicWidget/homepagewindow.ui \
    basicWidget/basewindow.ui \
    equipWidget/FirstStackedWdg.ui \
    equipWidget/SecondStackedWdg.ui \
    equipWidget/ThirdStackedWdg.ui \
    common/CtrlInputChar.ui \
    equipWidget/WdgFP2DCV.ui \
    equipWidget/WdgFP3DCA.ui \
    equipWidget/WdgFP4Deg1.ui \
    equipWidget/WdgFP0AC.ui \
    equipWidget/WdgFP5Deg2Curve.ui \
    equipWidget/Wdg2DCABranch.ui \
    equipWidget/Wdg2Table.ui \
    equipWidget/WdgFP1Module.ui \
    equipWidget/Wdg2DCDeg1Branch.ui \
    equipWidget/WdgFP6BattRemainTime.ui \
    equipWidget/WdgFP7BattDegMeter.ui \
    equipWidget/WdgFP8BattDegCurve.ui \
    equipWidget/Wdg2P5BattRemainTime.ui \
    equipWidget/Wdg2P6BattDeg.ui \
    equipWidget/FourthStackedWdg.ui \
    basicWidget/GuideWindow.ui \
    equipWidget/WdgFP11ScreenSaver.ui \
    equipWidget/WdgFP10Inventory.ui \
    equipWidget/Wdg3Table.ui \
    util/DlgLoading.ui \
    util/DlgInfo.ui \
    basicWidget/LoginWindow.ui \
    configWidget/WdgFCfgGroup.ui \
    util/DlgUpdateApp.ui \
    equipWidget/WdgInventory.ui \
    equipWidget/WdgAlmMenu.ui \
    equipWidget/WdgFP6BattInfo.ui
	
RESOURCES +=

TRANSLATIONS += \
    ../output/translations/lang_en.ts \
    ../output/translations/lang_zh.ts \
    ../output/translations/lang_fr.ts \
    ../output/translations/lang_de.ts \
    ../output/translations/lang_it.ts \
    #../output/translations/lang_ru.ts \
    ../output/translations/lang_es.ts \
    ../output/translations/lang_tw.ts \
    #../output/translations/lang_tr.ts

unix{
#INCLUDEPATH +=  ./tmp
#指定uic命令将.ui文件转化成ui_*.h文件的存放的目录
UI_DIR +=  ./tmp
#指定rcc命令将.qrc文件转换成qrc_*.h文件的存放目录
RCC_DIR +=  ./tmp
#指定moc命令将含Q_OBJECT的头文件转换成标准.h文件的存放目录
MOC_DIR +=  ./tmp
#指定目标文件(obj)的存放目录
OBJECTS_DIR += ./tmp

HEADERS  += \
    ../public/utility.h \
    ../public/dataSource.h \
    ../public/sysInit.h

SOURCES += \

FORMS    += \

RESOURCES += \

LIBS += \
    -lutility \
    -ldataSource \
    -lsysInit
}

/********************************************************************************
** Form generated from reading UI file 'FourthStackedWdg.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FOURTHSTACKEDWDG_H
#define UI_FOURTHSTACKEDWDG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QStackedWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FourthStackedWdg
{
public:
    QStackedWidget *stackedWidget;

    void setupUi(QWidget *FourthStackedWdg)
    {
        if (FourthStackedWdg->objectName().isEmpty())
            FourthStackedWdg->setObjectName(QString::fromUtf8("FourthStackedWdg"));
        FourthStackedWdg->resize(160, 128);
        stackedWidget = new QStackedWidget(FourthStackedWdg);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setGeometry(QRect(0, 0, 160, 128));

        retranslateUi(FourthStackedWdg);

        QMetaObject::connectSlotsByName(FourthStackedWdg);
    } // setupUi

    void retranslateUi(QWidget *FourthStackedWdg)
    {
        FourthStackedWdg->setWindowTitle(QString());
    } // retranslateUi

};

namespace Ui {
    class FourthStackedWdg: public Ui_FourthStackedWdg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FOURTHSTACKEDWDG_H

/********************************************************************************
** Form generated from reading UI file 'basewindow.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BASEWINDOW_H
#define UI_BASEWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QStackedWidget>
#include <QtGui/QWidget>
#include "equipWidget/FirstStackedWdg.h"
#include "equipWidget/FourthStackedWdg.h"
#include "equipWidget/SecondStackedWdg.h"
#include "equipWidget/ThirdStackedWdg.h"

QT_BEGIN_NAMESPACE

class Ui_BaseWindow
{
public:
    QStackedWidget *stackedWidget;
    FirstStackedWdg *p1;
    SecondStackedWdg *p2;
    ThirdStackedWdg *p3;
    FourthStackedWdg *p4;

    void setupUi(QWidget *BaseWindow)
    {
        if (BaseWindow->objectName().isEmpty())
            BaseWindow->setObjectName(QString::fromUtf8("BaseWindow"));
        BaseWindow->resize(160, 128);
        stackedWidget = new QStackedWidget(BaseWindow);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setGeometry(QRect(0, 0, 160, 128));
        stackedWidget->setAutoFillBackground(false);
        stackedWidget->setFrameShape(QFrame::NoFrame);
        stackedWidget->setFrameShadow(QFrame::Plain);
        p1 = new FirstStackedWdg();
        p1->setObjectName(QString::fromUtf8("p1"));
        p1->setMinimumSize(QSize(800, 0));
        stackedWidget->addWidget(p1);
        p2 = new SecondStackedWdg();
        p2->setObjectName(QString::fromUtf8("p2"));
        stackedWidget->addWidget(p2);
        p3 = new ThirdStackedWdg();
        p3->setObjectName(QString::fromUtf8("p3"));
        stackedWidget->addWidget(p3);
        p4 = new FourthStackedWdg();
        p4->setObjectName(QString::fromUtf8("p4"));
        stackedWidget->addWidget(p4);

        retranslateUi(BaseWindow);

        stackedWidget->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(BaseWindow);
    } // setupUi

    void retranslateUi(QWidget *BaseWindow)
    {
        BaseWindow->setWindowTitle(QString());
    } // retranslateUi

};

namespace Ui {
    class BaseWindow: public Ui_BaseWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BASEWINDOW_H

/****************************************************************************
** Meta object code from reading C++ file 'basewindow.h'
**
** Created: Fri Mar 13 11:21:41 2020
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../basicWidget/basewindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'basewindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_BaseWindow[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x05,
      27,   11,   11,   11, 0x05,
      55,   11,   11,   11, 0x05,
      85,   11,   11,   11, 0x05,
     102,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
     129,  123,   11,   11, 0x0a,
     142,   11,   11,   11, 0x2a,
     150,   11,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_BaseWindow[] = {
    "BaseWindow\0\0goToHomePage()\0"
    "goToBaseWindow(WIDGET_TYPE)\0"
    "goToGuideWindow(WIDGET_GUIDE)\0"
    "sigScreenSaver()\0sigStopDetectAlarm()\0"
    "param\0Enter(void*)\0Enter()\0Leave()\0"
};

const QMetaObject BaseWindow::staticMetaObject = {
    { &BasicWidget::staticMetaObject, qt_meta_stringdata_BaseWindow,
      qt_meta_data_BaseWindow, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &BaseWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *BaseWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *BaseWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_BaseWindow))
        return static_cast<void*>(const_cast< BaseWindow*>(this));
    return BasicWidget::qt_metacast(_clname);
}

int BaseWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: goToHomePage(); break;
        case 1: goToBaseWindow((*reinterpret_cast< WIDGET_TYPE(*)>(_a[1]))); break;
        case 2: goToGuideWindow((*reinterpret_cast< WIDGET_GUIDE(*)>(_a[1]))); break;
        case 3: sigScreenSaver(); break;
        case 4: sigStopDetectAlarm(); break;
        case 5: Enter((*reinterpret_cast< void*(*)>(_a[1]))); break;
        case 6: Enter(); break;
        case 7: Leave(); break;
        default: ;
        }
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void BaseWindow::goToHomePage()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void BaseWindow::goToBaseWindow(WIDGET_TYPE _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void BaseWindow::goToGuideWindow(WIDGET_GUIDE _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void BaseWindow::sigScreenSaver()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void BaseWindow::sigStopDetectAlarm()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}
QT_END_MOC_NAMESPACE

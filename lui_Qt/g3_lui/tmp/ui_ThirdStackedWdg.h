/********************************************************************************
** Form generated from reading UI file 'ThirdStackedWdg.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_THIRDSTACKEDWDG_H
#define UI_THIRDSTACKEDWDG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QStackedWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ThirdStackedWdg
{
public:
    QStackedWidget *stackedWidget;

    void setupUi(QWidget *ThirdStackedWdg)
    {
        if (ThirdStackedWdg->objectName().isEmpty())
            ThirdStackedWdg->setObjectName(QString::fromUtf8("ThirdStackedWdg"));
        ThirdStackedWdg->resize(160, 128);
        stackedWidget = new QStackedWidget(ThirdStackedWdg);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setGeometry(QRect(0, 0, 160, 128));

        retranslateUi(ThirdStackedWdg);

        QMetaObject::connectSlotsByName(ThirdStackedWdg);
    } // setupUi

    void retranslateUi(QWidget *ThirdStackedWdg)
    {
        ThirdStackedWdg->setWindowTitle(QString());
    } // retranslateUi

};

namespace Ui {
    class ThirdStackedWdg: public Ui_ThirdStackedWdg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_THIRDSTACKEDWDG_H

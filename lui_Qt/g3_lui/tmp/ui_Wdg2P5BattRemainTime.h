/********************************************************************************
** Form generated from reading UI file 'Wdg2P5BattRemainTime.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDG2P5BATTREMAINTIME_H
#define UI_WDG2P5BATTREMAINTIME_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QScrollBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Wdg2P5BattRemainTime
{
public:
    QScrollBar *verticalScrollBar;
    QLabel *label_enter;

    void setupUi(QWidget *Wdg2P5BattRemainTime)
    {
        if (Wdg2P5BattRemainTime->objectName().isEmpty())
            Wdg2P5BattRemainTime->setObjectName(QString::fromUtf8("Wdg2P5BattRemainTime"));
        Wdg2P5BattRemainTime->resize(400, 300);
        verticalScrollBar = new QScrollBar(Wdg2P5BattRemainTime);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(320, 10, 16, 160));
        verticalScrollBar->setOrientation(Qt::Vertical);
        label_enter = new QLabel(Wdg2P5BattRemainTime);
        label_enter->setObjectName(QString::fromUtf8("label_enter"));
        label_enter->setGeometry(QRect(90, 30, 54, 12));

        retranslateUi(Wdg2P5BattRemainTime);

        QMetaObject::connectSlotsByName(Wdg2P5BattRemainTime);
    } // setupUi

    void retranslateUi(QWidget *Wdg2P5BattRemainTime)
    {
        Wdg2P5BattRemainTime->setWindowTitle(QString());
        label_enter->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Wdg2P5BattRemainTime: public Ui_Wdg2P5BattRemainTime {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDG2P5BATTREMAINTIME_H

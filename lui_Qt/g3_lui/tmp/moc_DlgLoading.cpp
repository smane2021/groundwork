/****************************************************************************
** Meta object code from reading C++ file 'DlgLoading.h'
**
** Created: Fri Mar 13 11:21:45 2020
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../util/DlgLoading.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DlgLoading.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ThreadQueryData[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_ThreadQueryData[] = {
    "ThreadQueryData\0\0sigReadedData()\0"
};

const QMetaObject ThreadQueryData::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_ThreadQueryData,
      qt_meta_data_ThreadQueryData, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ThreadQueryData::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ThreadQueryData::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ThreadQueryData::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ThreadQueryData))
        return static_cast<void*>(const_cast< ThreadQueryData*>(this));
    return QThread::qt_metacast(_clname);
}

int ThreadQueryData::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: sigReadedData(); break;
        default: ;
        }
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void ThreadQueryData::sigReadedData()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
static const uint qt_meta_data_DlgLoading[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x08,
      19,   11,   11,   11, 0x0a,
      28,   11,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_DlgLoading[] = {
    "DlgLoading\0\0stop()\0accept()\0reject()\0"
};

const QMetaObject DlgLoading::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_DlgLoading,
      qt_meta_data_DlgLoading, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DlgLoading::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DlgLoading::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DlgLoading::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DlgLoading))
        return static_cast<void*>(const_cast< DlgLoading*>(this));
    return QDialog::qt_metacast(_clname);
}

int DlgLoading::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: stop(); break;
        case 1: accept(); break;
        case 2: reject(); break;
        default: ;
        }
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

/****************************************************************************
** Meta object code from reading C++ file 'buzztoolbutton.h'
**
** Created: Fri Mar 13 11:21:41 2020
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../common/buzztoolbutton.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'buzztoolbutton.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_BuzzToolButton[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x0a,
      24,   15,   15,   15, 0x0a,
      34,   15,   15,   15, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_BuzzToolButton[] = {
    "BuzzToolButton\0\0Press()\0Release()\0"
    "StopBuzz()\0"
};

const QMetaObject BuzzToolButton::staticMetaObject = {
    { &QToolButton::staticMetaObject, qt_meta_stringdata_BuzzToolButton,
      qt_meta_data_BuzzToolButton, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &BuzzToolButton::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *BuzzToolButton::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *BuzzToolButton::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_BuzzToolButton))
        return static_cast<void*>(const_cast< BuzzToolButton*>(this));
    return QToolButton::qt_metacast(_clname);
}

int BuzzToolButton::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QToolButton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: Press(); break;
        case 1: Release(); break;
        case 2: StopBuzz(); break;
        default: ;
        }
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

/********************************************************************************
** Form generated from reading UI file 'WdgFP4Deg1.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDGFP4DEG1_H
#define UI_WDGFP4DEG1_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QScrollBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WdgFP4Deg1
{
public:
    QLabel *label_enter;
    QLabel *label_thermometer;
    QLabel *label_point;
    QScrollBar *verticalScrollBar;

    void setupUi(QWidget *WdgFP4Deg1)
    {
        if (WdgFP4Deg1->objectName().isEmpty())
            WdgFP4Deg1->setObjectName(QString::fromUtf8("WdgFP4Deg1"));
        WdgFP4Deg1->resize(160, 128);
        label_enter = new QLabel(WdgFP4Deg1);
        label_enter->setObjectName(QString::fromUtf8("label_enter"));
        label_enter->setGeometry(QRect(6, 15, 147, 16));
        label_thermometer = new QLabel(WdgFP4Deg1);
        label_thermometer->setObjectName(QString::fromUtf8("label_thermometer"));
        label_thermometer->setGeometry(QRect(5, 59, 150, 46));
        label_point = new QLabel(WdgFP4Deg1);
        label_point->setObjectName(QString::fromUtf8("label_point"));
        label_point->setGeometry(QRect(51, 40, 41, 16));
        verticalScrollBar = new QScrollBar(WdgFP4Deg1);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(149, 1, 11, 124));
        verticalScrollBar->setOrientation(Qt::Vertical);

        retranslateUi(WdgFP4Deg1);

        QMetaObject::connectSlotsByName(WdgFP4Deg1);
    } // setupUi

    void retranslateUi(QWidget *WdgFP4Deg1)
    {
        WdgFP4Deg1->setWindowTitle(QString());
        label_enter->setText(QString());
        label_thermometer->setText(QString());
        label_point->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class WdgFP4Deg1: public Ui_WdgFP4Deg1 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDGFP4DEG1_H

/********************************************************************************
** Form generated from reading UI file 'WdgFP7BattDegMeter.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDGFP7BATTDEGMETER_H
#define UI_WDGFP7BATTDEGMETER_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QScrollBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WdgFP7BattDegMeter
{
public:
    QLabel *label_enter;
    QScrollBar *verticalScrollBar;

    void setupUi(QWidget *WdgFP7BattDegMeter)
    {
        if (WdgFP7BattDegMeter->objectName().isEmpty())
            WdgFP7BattDegMeter->setObjectName(QString::fromUtf8("WdgFP7BattDegMeter"));
        WdgFP7BattDegMeter->resize(400, 300);
        label_enter = new QLabel(WdgFP7BattDegMeter);
        label_enter->setObjectName(QString::fromUtf8("label_enter"));
        label_enter->setGeometry(QRect(7, 5, 54, 12));
        verticalScrollBar = new QScrollBar(WdgFP7BattDegMeter);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(340, 30, 16, 160));
        verticalScrollBar->setOrientation(Qt::Vertical);

        retranslateUi(WdgFP7BattDegMeter);

        QMetaObject::connectSlotsByName(WdgFP7BattDegMeter);
    } // setupUi

    void retranslateUi(QWidget *WdgFP7BattDegMeter)
    {
        WdgFP7BattDegMeter->setWindowTitle(QString());
        label_enter->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class WdgFP7BattDegMeter: public Ui_WdgFP7BattDegMeter {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDGFP7BATTDEGMETER_H

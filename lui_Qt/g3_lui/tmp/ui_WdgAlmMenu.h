/********************************************************************************
** Form generated from reading UI file 'WdgAlmMenu.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDGALMMENU_H
#define UI_WDGALMMENU_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QWidget>
#include <common/buzztablewidget.h>

QT_BEGIN_NAMESPACE

class Ui_WdgAlmMenu
{
public:
    QLabel *label_title;
    BuzzTableWidget *tblWdg_alarm;

    void setupUi(QWidget *WdgAlmMenu)
    {
        if (WdgAlmMenu->objectName().isEmpty())
            WdgAlmMenu->setObjectName(QString::fromUtf8("WdgAlmMenu"));
        WdgAlmMenu->resize(160, 128);
        label_title = new QLabel(WdgAlmMenu);
        label_title->setObjectName(QString::fromUtf8("label_title"));
        label_title->setGeometry(QRect(6, 4, 147, 16));
        label_title->setAlignment(Qt::AlignCenter);
        tblWdg_alarm = new BuzzTableWidget(WdgAlmMenu);
        tblWdg_alarm->setObjectName(QString::fromUtf8("tblWdg_alarm"));
        tblWdg_alarm->setGeometry(QRect(0, 20, 160, 108));
        tblWdg_alarm->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);

        retranslateUi(WdgAlmMenu);

        QMetaObject::connectSlotsByName(WdgAlmMenu);
    } // setupUi

    void retranslateUi(QWidget *WdgAlmMenu)
    {
        WdgAlmMenu->setWindowTitle(QString());
        label_title->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class WdgAlmMenu: public Ui_WdgAlmMenu {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDGALMMENU_H

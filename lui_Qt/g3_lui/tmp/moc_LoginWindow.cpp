/****************************************************************************
** Meta object code from reading C++ file 'LoginWindow.h'
**
** Created: Fri Mar 13 11:21:45 2020
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../basicWidget/LoginWindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'LoginWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_LoginWindow[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x05,
      43,   12,   12,   12, 0x05,
      58,   12,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
      86,   12,   12,   12, 0x08,
     116,  112,   12,   12, 0x08,
     138,   12,   12,   12, 0x08,
     156,  151,   12,   12, 0x08,
     181,   12,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_LoginWindow[] = {
    "LoginWindow\0\0goToGuideWindow(WIDGET_GUIDE)\0"
    "goToHomePage()\0goToBaseWindow(WIDGET_TYPE)\0"
    "FocusTableWdg(INPUT_TYPE)\0key\0"
    "sltTableKeyPress(int)\0sltTimeOut()\0"
    "nKey\0sltCellUnitKeyPress(int)\0"
    "on_tableWidget_itemSelectionChanged()\0"
};

const QMetaObject LoginWindow::staticMetaObject = {
    { &BasicWidget::staticMetaObject, qt_meta_stringdata_LoginWindow,
      qt_meta_data_LoginWindow, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LoginWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LoginWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LoginWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LoginWindow))
        return static_cast<void*>(const_cast< LoginWindow*>(this));
    return BasicWidget::qt_metacast(_clname);
}

int LoginWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: goToGuideWindow((*reinterpret_cast< WIDGET_GUIDE(*)>(_a[1]))); break;
        case 1: goToHomePage(); break;
        case 2: goToBaseWindow((*reinterpret_cast< WIDGET_TYPE(*)>(_a[1]))); break;
        case 3: FocusTableWdg((*reinterpret_cast< INPUT_TYPE(*)>(_a[1]))); break;
        case 4: sltTableKeyPress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: sltTimeOut(); break;
        case 6: sltCellUnitKeyPress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: on_tableWidget_itemSelectionChanged(); break;
        default: ;
        }
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void LoginWindow::goToGuideWindow(WIDGET_GUIDE _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void LoginWindow::goToHomePage()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void LoginWindow::goToBaseWindow(WIDGET_TYPE _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE

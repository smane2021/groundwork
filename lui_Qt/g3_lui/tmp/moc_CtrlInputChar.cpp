/****************************************************************************
** Meta object code from reading C++ file 'CtrlInputChar.h'
**
** Created: Fri Mar 13 11:21:42 2020
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../common/CtrlInputChar.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CtrlInputChar.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CtrlInputChar[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x05,
      41,   36,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
      66,   14,   14,   14, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_CtrlInputChar[] = {
    "CtrlInputChar\0\0escapeMe(INPUT_TYPE)\0"
    "nKey\0sigCellUnitKeyPress(int)\0"
    "on_spinBox_char_editingFinished()\0"
};

const QMetaObject CtrlInputChar::staticMetaObject = {
    { &BasicWidget::staticMetaObject, qt_meta_stringdata_CtrlInputChar,
      qt_meta_data_CtrlInputChar, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CtrlInputChar::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CtrlInputChar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CtrlInputChar::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CtrlInputChar))
        return static_cast<void*>(const_cast< CtrlInputChar*>(this));
    return BasicWidget::qt_metacast(_clname);
}

int CtrlInputChar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: escapeMe((*reinterpret_cast< INPUT_TYPE(*)>(_a[1]))); break;
        case 1: sigCellUnitKeyPress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: on_spinBox_char_editingFinished(); break;
        default: ;
        }
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void CtrlInputChar::escapeMe(INPUT_TYPE _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CtrlInputChar::sigCellUnitKeyPress(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE

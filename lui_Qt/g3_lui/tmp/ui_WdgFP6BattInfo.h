/********************************************************************************
** Form generated from reading UI file 'WdgFP6BattInfo.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDGFP6BATTINFO_H
#define UI_WDGFP6BATTINFO_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QScrollBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WdgFP6BattInfo
{
public:
    QScrollBar *verticalScrollBar;
    QLabel *label_enter;

    void setupUi(QWidget *WdgFP6BattInfo)
    {
        if (WdgFP6BattInfo->objectName().isEmpty())
            WdgFP6BattInfo->setObjectName(QString::fromUtf8("WdgFP6BattInfo"));
        WdgFP6BattInfo->resize(400, 300);
        verticalScrollBar = new QScrollBar(WdgFP6BattInfo);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(372, 24, 16, 160));
        verticalScrollBar->setOrientation(Qt::Vertical);
        label_enter = new QLabel(WdgFP6BattInfo);
        label_enter->setObjectName(QString::fromUtf8("label_enter"));
        label_enter->setGeometry(QRect(10, 20, 54, 12));

        retranslateUi(WdgFP6BattInfo);

        QMetaObject::connectSlotsByName(WdgFP6BattInfo);
    } // setupUi

    void retranslateUi(QWidget *WdgFP6BattInfo)
    {
        WdgFP6BattInfo->setWindowTitle(QApplication::translate("WdgFP6BattInfo", "Form", 0, QApplication::UnicodeUTF8));
        label_enter->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class WdgFP6BattInfo: public Ui_WdgFP6BattInfo {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDGFP6BATTINFO_H

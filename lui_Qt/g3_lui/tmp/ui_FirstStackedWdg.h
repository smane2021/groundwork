/********************************************************************************
** Form generated from reading UI file 'FirstStackedWdg.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FIRSTSTACKEDWDG_H
#define UI_FIRSTSTACKEDWDG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QStackedWidget>
#include <QtGui/QWidget>
#include <common/basicwidget.h>
#include <common/buzztablewidget.h>

QT_BEGIN_NAMESPACE

class Ui_FirstStackedWdg
{
public:
    QStackedWidget *stackedWidget;
    BasicWidget *p9_Alarm;
    BuzzTableWidget *tblWdg_alarm;
    QLabel *label_title;

    void setupUi(QWidget *FirstStackedWdg)
    {
        if (FirstStackedWdg->objectName().isEmpty())
            FirstStackedWdg->setObjectName(QString::fromUtf8("FirstStackedWdg"));
        FirstStackedWdg->resize(160, 128);
        stackedWidget = new QStackedWidget(FirstStackedWdg);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setGeometry(QRect(0, 0, 160, 128));
        stackedWidget->setAutoFillBackground(false);
        stackedWidget->setFrameShape(QFrame::NoFrame);
        stackedWidget->setFrameShadow(QFrame::Plain);
        p9_Alarm = new BasicWidget();
        p9_Alarm->setObjectName(QString::fromUtf8("p9_Alarm"));
        tblWdg_alarm = new BuzzTableWidget(p9_Alarm);
        tblWdg_alarm->setObjectName(QString::fromUtf8("tblWdg_alarm"));
        tblWdg_alarm->setGeometry(QRect(0, 21, 160, 108));
        tblWdg_alarm->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        label_title = new QLabel(p9_Alarm);
        label_title->setObjectName(QString::fromUtf8("label_title"));
        label_title->setGeometry(QRect(59, 4, 45, 15));
        stackedWidget->addWidget(p9_Alarm);

        retranslateUi(FirstStackedWdg);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(FirstStackedWdg);
    } // setupUi

    void retranslateUi(QWidget *FirstStackedWdg)
    {
        label_title->setText(QApplication::translate("FirstStackedWdg", "Alarm", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(FirstStackedWdg);
    } // retranslateUi

};

namespace Ui {
    class FirstStackedWdg: public Ui_FirstStackedWdg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FIRSTSTACKEDWDG_H

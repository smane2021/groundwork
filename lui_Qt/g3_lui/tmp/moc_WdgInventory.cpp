/****************************************************************************
** Meta object code from reading C++ file 'WdgInventory.h'
**
** Created: Fri Mar 13 11:21:46 2020
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../equipWidget/WdgInventory.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'WdgInventory.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_WdgInventory[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x05,
      29,   13,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      57,   13,   13,   13, 0x08,
      74,   13,   13,   13, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_WdgInventory[] = {
    "WdgInventory\0\0goToHomePage()\0"
    "goToBaseWindow(WIDGET_TYPE)\0"
    "sltScreenSaver()\0sltTimerHandler()\0"
};

const QMetaObject WdgInventory::staticMetaObject = {
    { &BasicWidget::staticMetaObject, qt_meta_stringdata_WdgInventory,
      qt_meta_data_WdgInventory, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &WdgInventory::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *WdgInventory::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *WdgInventory::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_WdgInventory))
        return static_cast<void*>(const_cast< WdgInventory*>(this));
    return BasicWidget::qt_metacast(_clname);
}

int WdgInventory::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: goToHomePage(); break;
        case 1: goToBaseWindow((*reinterpret_cast< WIDGET_TYPE(*)>(_a[1]))); break;
        case 2: sltScreenSaver(); break;
        case 3: sltTimerHandler(); break;
        default: ;
        }
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void WdgInventory::goToHomePage()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void WdgInventory::goToBaseWindow(WIDGET_TYPE _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE

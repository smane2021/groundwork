/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created: Fri Mar 13 11:21:41 2020
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../basicWidget/mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      19,   12,   11,   11, 0x05,
      41,   11,   11,   11, 0x05,
      58,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
      75,   11,   11,   11, 0x0a,
      93,   90,   11,   11, 0x0a,
     121,   90,   11,   11, 0x0a,
     156,  151,   11,   11, 0x0a,
     184,   11,   11,   11, 0x08,
     208,   11,   11,   11, 0x08,
     226,   11,   11,   11, 0x08,
     243,   11,   11,   11, 0x08,
     264,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0bState\0judgeActiveAlarm(int)\0"
    "SysLangChanged()\0sigScreenSaver()\0"
    "GoToHomePage()\0wt\0GoToBaseWindow(WIDGET_TYPE)\0"
    "GoToGuideWindow(WIDGET_GUIDE)\0type\0"
    "GoToLoginWindow(LOGIN_TYPE)\0"
    "heartbeatTimerRefresh()\0LanguageChanged()\0"
    "sltDetectAlarm()\0sltStopDetectAlarm()\0"
    "reject()\0"
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QDialog::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: judgeActiveAlarm((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: SysLangChanged(); break;
        case 2: sigScreenSaver(); break;
        case 3: GoToHomePage(); break;
        case 4: GoToBaseWindow((*reinterpret_cast< WIDGET_TYPE(*)>(_a[1]))); break;
        case 5: GoToGuideWindow((*reinterpret_cast< WIDGET_GUIDE(*)>(_a[1]))); break;
        case 6: GoToLoginWindow((*reinterpret_cast< LOGIN_TYPE(*)>(_a[1]))); break;
        case 7: heartbeatTimerRefresh(); break;
        case 8: LanguageChanged(); break;
        case 9: sltDetectAlarm(); break;
        case 10: sltStopDetectAlarm(); break;
        case 11: reject(); break;
        default: ;
        }
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::judgeActiveAlarm(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MainWindow::SysLangChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void MainWindow::sigScreenSaver()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE

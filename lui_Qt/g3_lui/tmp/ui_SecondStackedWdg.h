/********************************************************************************
** Form generated from reading UI file 'SecondStackedWdg.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SECONDSTACKEDWDG_H
#define UI_SECONDSTACKEDWDG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QStackedWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SecondStackedWdg
{
public:
    QStackedWidget *stackedWidget;

    void setupUi(QWidget *SecondStackedWdg)
    {
        if (SecondStackedWdg->objectName().isEmpty())
            SecondStackedWdg->setObjectName(QString::fromUtf8("SecondStackedWdg"));
        SecondStackedWdg->resize(160, 128);
        stackedWidget = new QStackedWidget(SecondStackedWdg);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setGeometry(QRect(0, 0, 160, 128));

        retranslateUi(SecondStackedWdg);

        QMetaObject::connectSlotsByName(SecondStackedWdg);
    } // setupUi

    void retranslateUi(QWidget *SecondStackedWdg)
    {
        SecondStackedWdg->setWindowTitle(QString());
    } // retranslateUi

};

namespace Ui {
    class SecondStackedWdg: public Ui_SecondStackedWdg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SECONDSTACKEDWDG_H

/********************************************************************************
** Form generated from reading UI file 'homepagewindow.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HOMEPAGEWINDOW_H
#define UI_HOMEPAGEWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QWidget>
#include "../common/buzzpushbutton.h"

QT_BEGIN_NAMESPACE

class Ui_HomePageWindow
{
public:
    QLabel *dcParamLabel;
    QLabel *dateLabel;
    QLabel *battParamLabel;
    BuzzPushButton *AcEquipBtn;
    BuzzPushButton *ActAlmBtn;
    BuzzPushButton *BattEquipBtn;
    BuzzPushButton *RectEquipBtn;
    BuzzPushButton *DcEquipBtn;
    BuzzPushButton *ConfigBtn;
    QLabel *label_sysused;
    QLabel *label_lineH1;
    QLabel *label_lineH2;
    QLabel *label_lineV;
    QLabel *label_slaveV;
    QLabel *label_slaveA;
    QLabel *label;

    void setupUi(QWidget *HomePageWindow)
    {
        if (HomePageWindow->objectName().isEmpty())
            HomePageWindow->setObjectName(QString::fromUtf8("HomePageWindow"));
        HomePageWindow->setEnabled(true);
        HomePageWindow->resize(160, 128);
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        HomePageWindow->setFont(font);
        HomePageWindow->setAutoFillBackground(false);
        dcParamLabel = new QLabel(HomePageWindow);
        dcParamLabel->setObjectName(QString::fromUtf8("dcParamLabel"));
        dcParamLabel->setGeometry(QRect(100, 63, 61, 34));
        QFont font1;
        font1.setPointSize(9);
        font1.setBold(false);
        font1.setWeight(50);
        dcParamLabel->setFont(font1);
        dcParamLabel->setStyleSheet(QString::fromUtf8(""));
        dcParamLabel->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        dateLabel = new QLabel(HomePageWindow);
        dateLabel->setObjectName(QString::fromUtf8("dateLabel"));
        dateLabel->setGeometry(QRect(40, 1, 61, 16));
        QFont font2;
        font2.setPointSize(9);
        font2.setBold(false);
        font2.setWeight(50);
        font2.setKerning(true);
        dateLabel->setFont(font2);
        dateLabel->setLayoutDirection(Qt::LeftToRight);
        dateLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        battParamLabel = new QLabel(HomePageWindow);
        battParamLabel->setObjectName(QString::fromUtf8("battParamLabel"));
        battParamLabel->setGeometry(QRect(40, 90, 55, 15));
        battParamLabel->setFont(font1);
        battParamLabel->setStyleSheet(QString::fromUtf8(""));
        battParamLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        AcEquipBtn = new BuzzPushButton(HomePageWindow);
        AcEquipBtn->setObjectName(QString::fromUtf8("AcEquipBtn"));
        AcEquipBtn->setGeometry(QRect(8, 30, 34, 34));
        AcEquipBtn->setFont(font1);
        AcEquipBtn->setFocusPolicy(Qt::TabFocus);
        ActAlmBtn = new BuzzPushButton(HomePageWindow);
        ActAlmBtn->setObjectName(QString::fromUtf8("ActAlmBtn"));
        ActAlmBtn->setGeometry(QRect(106, 0, 24, 24));
        ActAlmBtn->setFont(font1);
        ActAlmBtn->setFocusPolicy(Qt::TabFocus);
        BattEquipBtn = new BuzzPushButton(HomePageWindow);
        BattEquipBtn->setObjectName(QString::fromUtf8("BattEquipBtn"));
        BattEquipBtn->setGeometry(QRect(62, 77, 34, 34));
        BattEquipBtn->setFont(font1);
        BattEquipBtn->setFocusPolicy(Qt::TabFocus);
        BattEquipBtn->setStyleSheet(QString::fromUtf8("QPushButton#BattEquipBtn{\n"
"color: rgb(255, 255, 255);\n"
"}"));
        RectEquipBtn = new BuzzPushButton(HomePageWindow);
        RectEquipBtn->setObjectName(QString::fromUtf8("RectEquipBtn"));
        RectEquipBtn->setGeometry(QRect(62, 30, 34, 34));
        RectEquipBtn->setFont(font1);
        RectEquipBtn->setFocusPolicy(Qt::TabFocus);
        DcEquipBtn = new BuzzPushButton(HomePageWindow);
        DcEquipBtn->setObjectName(QString::fromUtf8("DcEquipBtn"));
        DcEquipBtn->setGeometry(QRect(116, 30, 34, 34));
        DcEquipBtn->setFont(font1);
        DcEquipBtn->setFocusPolicy(Qt::TabFocus);
        ConfigBtn = new BuzzPushButton(HomePageWindow);
        ConfigBtn->setObjectName(QString::fromUtf8("ConfigBtn"));
        ConfigBtn->setGeometry(QRect(10, 1, 24, 24));
        ConfigBtn->setFont(font1);
        ConfigBtn->setFocusPolicy(Qt::TabFocus);
        label_sysused = new QLabel(HomePageWindow);
        label_sysused->setObjectName(QString::fromUtf8("label_sysused"));
        label_sysused->setGeometry(QRect(2, 112, 154, 14));
        label_sysused->setFont(font1);
        label_sysused->setStyleSheet(QString::fromUtf8(""));
        label_sysused->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_lineH1 = new QLabel(HomePageWindow);
        label_lineH1->setObjectName(QString::fromUtf8("label_lineH1"));
        label_lineH1->setGeometry(QRect(38, 44, 28, 8));
        label_lineH2 = new QLabel(HomePageWindow);
        label_lineH2->setObjectName(QString::fromUtf8("label_lineH2"));
        label_lineH2->setGeometry(QRect(92, 44, 28, 8));
        label_lineV = new QLabel(HomePageWindow);
        label_lineV->setObjectName(QString::fromUtf8("label_lineV"));
        label_lineV->setGeometry(QRect(76, 59, 8, 22));
        label_slaveV = new QLabel(HomePageWindow);
        label_slaveV->setObjectName(QString::fromUtf8("label_slaveV"));
        label_slaveV->setGeometry(QRect(13, 50, 54, 31));
        label_slaveA = new QLabel(HomePageWindow);
        label_slaveA->setObjectName(QString::fromUtf8("label_slaveA"));
        label_slaveA->setGeometry(QRect(97, 64, 72, 31));
        label = new QLabel(HomePageWindow);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(135, 1, 20, 16));
        QWidget::setTabOrder(ConfigBtn, ActAlmBtn);
        QWidget::setTabOrder(ActAlmBtn, AcEquipBtn);
        QWidget::setTabOrder(AcEquipBtn, RectEquipBtn);
        QWidget::setTabOrder(RectEquipBtn, DcEquipBtn);
        QWidget::setTabOrder(DcEquipBtn, BattEquipBtn);

        retranslateUi(HomePageWindow);

        QMetaObject::connectSlotsByName(HomePageWindow);
    } // setupUi

    void retranslateUi(QWidget *HomePageWindow)
    {
        HomePageWindow->setWindowTitle(QString());
        dcParamLabel->setText(QString());
        dateLabel->setText(QString());
        battParamLabel->setText(QString());
        AcEquipBtn->setText(QString());
        ActAlmBtn->setText(QString());
        BattEquipBtn->setText(QString());
        RectEquipBtn->setText(QString());
        DcEquipBtn->setText(QString());
        ConfigBtn->setText(QString());
        label_sysused->setText(QString());
        label_lineH1->setText(QString());
        label_lineH2->setText(QString());
        label_lineV->setText(QString());
        label_slaveV->setText(QString());
        label_slaveA->setText(QString());
        label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class HomePageWindow: public Ui_HomePageWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HOMEPAGEWINDOW_H

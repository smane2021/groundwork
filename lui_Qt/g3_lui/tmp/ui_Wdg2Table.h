/********************************************************************************
** Form generated from reading UI file 'Wdg2Table.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDG2TABLE_H
#define UI_WDG2TABLE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QScrollBar>
#include <QtGui/QWidget>
#include <common/buzztablewidget.h>

QT_BEGIN_NAMESPACE

class Ui_Wdg2Table
{
public:
    BuzzTableWidget *tableWidget;
    QLabel *label_enter;
    QScrollBar *verticalScrollBar;

    void setupUi(QWidget *Wdg2Table)
    {
        if (Wdg2Table->objectName().isEmpty())
            Wdg2Table->setObjectName(QString::fromUtf8("Wdg2Table"));
        Wdg2Table->resize(260, 172);
        tableWidget = new BuzzTableWidget(Wdg2Table);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(0, 21, 160, 108));
        tableWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        label_enter = new QLabel(Wdg2Table);
        label_enter->setObjectName(QString::fromUtf8("label_enter"));
        label_enter->setGeometry(QRect(31, 4, 115, 15));
        verticalScrollBar = new QScrollBar(Wdg2Table);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(200, 0, 16, 160));
        verticalScrollBar->setOrientation(Qt::Vertical);

        retranslateUi(Wdg2Table);

        QMetaObject::connectSlotsByName(Wdg2Table);
    } // setupUi

    void retranslateUi(QWidget *Wdg2Table)
    {
        Wdg2Table->setWindowTitle(QString());
        label_enter->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Wdg2Table: public Ui_Wdg2Table {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDG2TABLE_H

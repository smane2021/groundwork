/********************************************************************************
** Form generated from reading UI file 'WdgFP6BattRemainTime.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDGFP6BATTREMAINTIME_H
#define UI_WDGFP6BATTREMAINTIME_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QScrollBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WdgFP6BattRemainTime
{
public:
    QLabel *label_enter;
    QScrollBar *verticalScrollBar;

    void setupUi(QWidget *WdgFP6BattRemainTime)
    {
        if (WdgFP6BattRemainTime->objectName().isEmpty())
            WdgFP6BattRemainTime->setObjectName(QString::fromUtf8("WdgFP6BattRemainTime"));
        WdgFP6BattRemainTime->resize(400, 300);
        label_enter = new QLabel(WdgFP6BattRemainTime);
        label_enter->setObjectName(QString::fromUtf8("label_enter"));
        label_enter->setGeometry(QRect(8, 6, 54, 12));
        verticalScrollBar = new QScrollBar(WdgFP6BattRemainTime);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(370, 10, 16, 160));
        verticalScrollBar->setOrientation(Qt::Vertical);

        retranslateUi(WdgFP6BattRemainTime);

        QMetaObject::connectSlotsByName(WdgFP6BattRemainTime);
    } // setupUi

    void retranslateUi(QWidget *WdgFP6BattRemainTime)
    {
        WdgFP6BattRemainTime->setWindowTitle(QString());
        label_enter->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class WdgFP6BattRemainTime: public Ui_WdgFP6BattRemainTime {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDGFP6BATTREMAINTIME_H

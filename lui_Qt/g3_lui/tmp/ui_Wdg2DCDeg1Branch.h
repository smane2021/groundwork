/********************************************************************************
** Form generated from reading UI file 'Wdg2DCDeg1Branch.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDG2DCDEG1BRANCH_H
#define UI_WDG2DCDEG1BRANCH_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QScrollBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Wdg2DCDeg1Branch
{
public:
    QScrollBar *verticalScrollBar;
    QLabel *label_title;

    void setupUi(QWidget *Wdg2DCDeg1Branch)
    {
        if (Wdg2DCDeg1Branch->objectName().isEmpty())
            Wdg2DCDeg1Branch->setObjectName(QString::fromUtf8("Wdg2DCDeg1Branch"));
        Wdg2DCDeg1Branch->resize(400, 300);
        verticalScrollBar = new QScrollBar(Wdg2DCDeg1Branch);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(340, 0, 16, 160));
        verticalScrollBar->setOrientation(Qt::Vertical);
        label_title = new QLabel(Wdg2DCDeg1Branch);
        label_title->setObjectName(QString::fromUtf8("label_title"));
        label_title->setGeometry(QRect(110, 40, 54, 12));

        retranslateUi(Wdg2DCDeg1Branch);

        QMetaObject::connectSlotsByName(Wdg2DCDeg1Branch);
    } // setupUi

    void retranslateUi(QWidget *Wdg2DCDeg1Branch)
    {
        Wdg2DCDeg1Branch->setWindowTitle(QString());
        label_title->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Wdg2DCDeg1Branch: public Ui_Wdg2DCDeg1Branch {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDG2DCDEG1BRANCH_H

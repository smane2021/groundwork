/****************************************************************************
** Meta object code from reading C++ file 'Wdg2Table.h'
**
** Created: Fri Mar 13 11:21:43 2020
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../equipWidget/Wdg2Table.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Wdg2Table.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Wdg2Table[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x05,
      26,   10,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
      58,   54,   10,   10, 0x08,
      80,   10,   10,   10, 0x08,
      97,   10,   10,   10, 0x08,
     115,   10,   10,   10, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Wdg2Table[] = {
    "Wdg2Table\0\0goToHomePage()\0"
    "goToBaseWindow(WIDGET_TYPE)\0key\0"
    "sltTableKeyPress(int)\0sltScreenSaver()\0"
    "sltTimerHandler()\0"
    "on_tableWidget_itemSelectionChanged()\0"
};

const QMetaObject Wdg2Table::staticMetaObject = {
    { &BasicWidget::staticMetaObject, qt_meta_stringdata_Wdg2Table,
      qt_meta_data_Wdg2Table, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Wdg2Table::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Wdg2Table::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Wdg2Table::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Wdg2Table))
        return static_cast<void*>(const_cast< Wdg2Table*>(this));
    return BasicWidget::qt_metacast(_clname);
}

int Wdg2Table::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: goToHomePage(); break;
        case 1: goToBaseWindow((*reinterpret_cast< WIDGET_TYPE(*)>(_a[1]))); break;
        case 2: sltTableKeyPress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: sltScreenSaver(); break;
        case 4: sltTimerHandler(); break;
        case 5: on_tableWidget_itemSelectionChanged(); break;
        default: ;
        }
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void Wdg2Table::goToHomePage()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void Wdg2Table::goToBaseWindow(WIDGET_TYPE _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE

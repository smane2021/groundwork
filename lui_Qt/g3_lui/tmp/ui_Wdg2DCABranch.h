/********************************************************************************
** Form generated from reading UI file 'Wdg2DCABranch.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDG2DCABRANCH_H
#define UI_WDG2DCABRANCH_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QScrollBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Wdg2DCABranch
{
public:
    QScrollBar *verticalScrollBar;
    QLabel *label_title;

    void setupUi(QWidget *Wdg2DCABranch)
    {
        if (Wdg2DCABranch->objectName().isEmpty())
            Wdg2DCABranch->setObjectName(QString::fromUtf8("Wdg2DCABranch"));
        Wdg2DCABranch->resize(160, 128);
        verticalScrollBar = new QScrollBar(Wdg2DCABranch);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(152, 0, 8, 133));
        verticalScrollBar->setAutoFillBackground(false);
        verticalScrollBar->setMaximum(1);
        verticalScrollBar->setSingleStep(1);
        verticalScrollBar->setPageStep(5);
        verticalScrollBar->setValue(0);
        verticalScrollBar->setSliderPosition(0);
        verticalScrollBar->setOrientation(Qt::Vertical);
        verticalScrollBar->setInvertedAppearance(false);
        verticalScrollBar->setInvertedControls(true);
        label_title = new QLabel(Wdg2DCABranch);
        label_title->setObjectName(QString::fromUtf8("label_title"));
        label_title->setGeometry(QRect(6, 4, 147, 16));
        label_title->setAlignment(Qt::AlignCenter);

        retranslateUi(Wdg2DCABranch);

        QMetaObject::connectSlotsByName(Wdg2DCABranch);
    } // setupUi

    void retranslateUi(QWidget *Wdg2DCABranch)
    {
        Wdg2DCABranch->setWindowTitle(QString());
        label_title->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Wdg2DCABranch: public Ui_Wdg2DCABranch {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDG2DCABRANCH_H

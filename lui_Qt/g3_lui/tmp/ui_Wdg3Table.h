/********************************************************************************
** Form generated from reading UI file 'Wdg3Table.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDG3TABLE_H
#define UI_WDG3TABLE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QScrollBar>
#include <QtGui/QWidget>
#include <common/buzztablewidget.h>

QT_BEGIN_NAMESPACE

class Ui_Wdg3Table
{
public:
    BuzzTableWidget *tableWidget;
    QScrollBar *verticalScrollBar;

    void setupUi(QWidget *Wdg3Table)
    {
        if (Wdg3Table->objectName().isEmpty())
            Wdg3Table->setObjectName(QString::fromUtf8("Wdg3Table"));
        Wdg3Table->resize(400, 300);
        tableWidget = new BuzzTableWidget(Wdg3Table);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(0, 0, 160, 128));
        verticalScrollBar = new QScrollBar(Wdg3Table);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(180, 0, 16, 160));
        verticalScrollBar->setOrientation(Qt::Vertical);

        retranslateUi(Wdg3Table);

        QMetaObject::connectSlotsByName(Wdg3Table);
    } // setupUi

    void retranslateUi(QWidget *Wdg3Table)
    {
        Wdg3Table->setWindowTitle(QString());
    } // retranslateUi

};

namespace Ui {
    class Wdg3Table: public Ui_Wdg3Table {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDG3TABLE_H

/********************************************************************************
** Form generated from reading UI file 'WdgFP3DCA.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDGFP3DCA_H
#define UI_WDGFP3DCA_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QScrollBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WdgFP3DCA
{
public:
    QScrollBar *verticalScrollBar;
    QLabel *label_enter;
    QLabel *label_curveBottom;

    void setupUi(QWidget *WdgFP3DCA)
    {
        if (WdgFP3DCA->objectName().isEmpty())
            WdgFP3DCA->setObjectName(QString::fromUtf8("WdgFP3DCA"));
        WdgFP3DCA->resize(160, 128);
        verticalScrollBar = new QScrollBar(WdgFP3DCA);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(155, 0, 8, 128));
        verticalScrollBar->setMinimum(0);
        verticalScrollBar->setMaximum(4);
        verticalScrollBar->setSingleStep(1);
        verticalScrollBar->setPageStep(5);
        verticalScrollBar->setValue(0);
        label_enter = new QLabel(WdgFP3DCA);
        label_enter->setObjectName(QString::fromUtf8("label_enter"));
        label_enter->setGeometry(QRect(6, 2, 147, 15));
        label_curveBottom = new QLabel(WdgFP3DCA);
        label_curveBottom->setObjectName(QString::fromUtf8("label_curveBottom"));
        label_curveBottom->setGeometry(QRect(0, 110, 160, 12));
        label_curveBottom->setAlignment(Qt::AlignCenter);
        label_enter->raise();
        label_curveBottom->raise();
        verticalScrollBar->raise();

        retranslateUi(WdgFP3DCA);

        QMetaObject::connectSlotsByName(WdgFP3DCA);
    } // setupUi

    void retranslateUi(QWidget *WdgFP3DCA)
    {
        WdgFP3DCA->setWindowTitle(QString());
        label_enter->setText(QString());
        label_curveBottom->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class WdgFP3DCA: public Ui_WdgFP3DCA {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDGFP3DCA_H

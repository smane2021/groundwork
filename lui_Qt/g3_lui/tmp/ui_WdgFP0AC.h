/********************************************************************************
** Form generated from reading UI file 'WdgFP0AC.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDGFP0AC_H
#define UI_WDGFP0AC_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WdgFP0AC
{
public:
    QLabel *label_title;
    QLabel *label_L1;
    QLabel *label_L3;
    QLabel *label_L2;

    void setupUi(QWidget *WdgFP0AC)
    {
        if (WdgFP0AC->objectName().isEmpty())
            WdgFP0AC->setObjectName(QString::fromUtf8("WdgFP0AC"));
        WdgFP0AC->resize(160, 128);
        label_title = new QLabel(WdgFP0AC);
        label_title->setObjectName(QString::fromUtf8("label_title"));
        label_title->setGeometry(QRect(0, 3, 160, 19));
        label_title->setAlignment(Qt::AlignCenter);
        label_L1 = new QLabel(WdgFP0AC);
        label_L1->setObjectName(QString::fromUtf8("label_L1"));
        label_L1->setGeometry(QRect(9, 34, 48, 10));
        label_L3 = new QLabel(WdgFP0AC);
        label_L3->setObjectName(QString::fromUtf8("label_L3"));
        label_L3->setGeometry(QRect(115, 34, 48, 10));
        label_L2 = new QLabel(WdgFP0AC);
        label_L2->setObjectName(QString::fromUtf8("label_L2"));
        label_L2->setGeometry(QRect(61, 34, 50, 10));

        retranslateUi(WdgFP0AC);

        QMetaObject::connectSlotsByName(WdgFP0AC);
    } // setupUi

    void retranslateUi(QWidget *WdgFP0AC)
    {
        WdgFP0AC->setWindowTitle(QString());
        label_title->setText(QString());
        label_L1->setText(QString());
        label_L3->setText(QString());
        label_L2->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class WdgFP0AC: public Ui_WdgFP0AC {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDGFP0AC_H

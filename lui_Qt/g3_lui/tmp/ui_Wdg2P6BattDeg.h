/********************************************************************************
** Form generated from reading UI file 'Wdg2P6BattDeg.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDG2P6BATTDEG_H
#define UI_WDG2P6BATTDEG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QScrollBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Wdg2P6BattDeg
{
public:
    QScrollBar *verticalScrollBar;
    QLabel *label_title;

    void setupUi(QWidget *Wdg2P6BattDeg)
    {
        if (Wdg2P6BattDeg->objectName().isEmpty())
            Wdg2P6BattDeg->setObjectName(QString::fromUtf8("Wdg2P6BattDeg"));
        Wdg2P6BattDeg->resize(400, 300);
        verticalScrollBar = new QScrollBar(Wdg2P6BattDeg);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(310, 20, 16, 160));
        verticalScrollBar->setOrientation(Qt::Vertical);
        label_title = new QLabel(Wdg2P6BattDeg);
        label_title->setObjectName(QString::fromUtf8("label_title"));
        label_title->setGeometry(QRect(120, 90, 54, 12));

        retranslateUi(Wdg2P6BattDeg);

        QMetaObject::connectSlotsByName(Wdg2P6BattDeg);
    } // setupUi

    void retranslateUi(QWidget *Wdg2P6BattDeg)
    {
        Wdg2P6BattDeg->setWindowTitle(QString());
        label_title->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Wdg2P6BattDeg: public Ui_Wdg2P6BattDeg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDG2P6BATTDEG_H

/****************************************************************************
** Meta object code from reading C++ file 'FourthStackedWdg.h'
**
** Created: Fri Mar 13 11:21:44 2020
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../equipWidget/FourthStackedWdg.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'FourthStackedWdg.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_FourthStackedWdg[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      18,   17,   17,   17, 0x05,
      33,   17,   17,   17, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_FourthStackedWdg[] = {
    "FourthStackedWdg\0\0goToHomePage()\0"
    "goToBaseWindow(WIDGET_TYPE)\0"
};

const QMetaObject FourthStackedWdg::staticMetaObject = {
    { &BasicWidget::staticMetaObject, qt_meta_stringdata_FourthStackedWdg,
      qt_meta_data_FourthStackedWdg, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FourthStackedWdg::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FourthStackedWdg::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FourthStackedWdg::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FourthStackedWdg))
        return static_cast<void*>(const_cast< FourthStackedWdg*>(this));
    return BasicWidget::qt_metacast(_clname);
}

int FourthStackedWdg::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: goToHomePage(); break;
        case 1: goToBaseWindow((*reinterpret_cast< WIDGET_TYPE(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void FourthStackedWdg::goToHomePage()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void FourthStackedWdg::goToBaseWindow(WIDGET_TYPE _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE

/********************************************************************************
** Form generated from reading UI file 'WdgFP2DCV.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDGFP2DCV_H
#define UI_WDGFP2DCV_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QScrollBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WdgFP2DCV
{
public:
    QLabel *label_title;
    QScrollBar *verticalScrollBar;

    void setupUi(QWidget *WdgFP2DCV)
    {
        if (WdgFP2DCV->objectName().isEmpty())
            WdgFP2DCV->setObjectName(QString::fromUtf8("WdgFP2DCV"));
        WdgFP2DCV->resize(160, 128);
        label_title = new QLabel(WdgFP2DCV);
        label_title->setObjectName(QString::fromUtf8("label_title"));
        label_title->setGeometry(QRect(18, 21, 130, 12));
        verticalScrollBar = new QScrollBar(WdgFP2DCV);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(153, 0, 8, 128));
        verticalScrollBar->setAutoFillBackground(false);
        verticalScrollBar->setMaximum(4);
        verticalScrollBar->setSingleStep(1);
        verticalScrollBar->setPageStep(5);
        verticalScrollBar->setValue(0);
        verticalScrollBar->setSliderPosition(0);
        verticalScrollBar->setOrientation(Qt::Vertical);
        verticalScrollBar->setInvertedAppearance(false);
        verticalScrollBar->setInvertedControls(true);

        retranslateUi(WdgFP2DCV);

        QMetaObject::connectSlotsByName(WdgFP2DCV);
    } // setupUi

    void retranslateUi(QWidget *WdgFP2DCV)
    {
        WdgFP2DCV->setWindowTitle(QString());
        label_title->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class WdgFP2DCV: public Ui_WdgFP2DCV {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDGFP2DCV_H

/****************************************************************************
** Meta object code from reading C++ file 'WdgFP10Inventory.h'
**
** Created: Fri Mar 13 11:21:45 2020
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../equipWidget/WdgFP10Inventory.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'WdgFP10Inventory.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_WdgFP10Inventory[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      22,   18,   17,   17, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_WdgFP10Inventory[] = {
    "WdgFP10Inventory\0\0key\0sltTableKeyPress(int)\0"
};

const QMetaObject WdgFP10Inventory::staticMetaObject = {
    { &BasicWidget::staticMetaObject, qt_meta_stringdata_WdgFP10Inventory,
      qt_meta_data_WdgFP10Inventory, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &WdgFP10Inventory::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *WdgFP10Inventory::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *WdgFP10Inventory::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_WdgFP10Inventory))
        return static_cast<void*>(const_cast< WdgFP10Inventory*>(this));
    return BasicWidget::qt_metacast(_clname);
}

int WdgFP10Inventory::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: sltTableKeyPress((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

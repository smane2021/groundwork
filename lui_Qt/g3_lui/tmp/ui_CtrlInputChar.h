/********************************************************************************
** Form generated from reading UI file 'CtrlInputChar.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CTRLINPUTCHAR_H
#define UI_CTRLINPUTCHAR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QWidget>
#include <common/BuzzDateEdit.h>
#include <common/BuzzSpinBox.h>
#include <common/my_time_edit.h>
#include "common/BuzzDoubleSpinBox.h"

QT_BEGIN_NAMESPACE

class Ui_CtrlInputChar
{
public:
    QLabel *label_old;
    BuzzSpinBox *spinBox_char;
    BuzzSpinBox *spinBox_single;
    BuzzDoubleSpinBox *dSpinBox_single;
    My_Time_Edit *timeEdit;
    QLabel *label_pw;
    QLabel *label_new;
    BuzzSpinBox *spinBox_IP1;
    BuzzSpinBox *spinBox_IP2;
    BuzzSpinBox *spinBox_IP3;
    BuzzSpinBox *spinBox_IP4;
    QLabel *label_IP1;
    QLabel *label_IP2;
    QLabel *label_IP3;
    BuzzDateEdit *dateEdit;
    BuzzSpinBox *spinBox_enum;

    void setupUi(QWidget *CtrlInputChar)
    {
        if (CtrlInputChar->objectName().isEmpty())
            CtrlInputChar->setObjectName(QString::fromUtf8("CtrlInputChar"));
        CtrlInputChar->resize(160, 21);
        QFont font;
        font.setUnderline(false);
        CtrlInputChar->setFont(font);
        CtrlInputChar->setFocusPolicy(Qt::NoFocus);
        CtrlInputChar->setLayoutDirection(Qt::RightToLeft);
        label_old = new QLabel(CtrlInputChar);
        label_old->setObjectName(QString::fromUtf8("label_old"));
        label_old->setGeometry(QRect(0, 0, 160, 21));
        spinBox_char = new BuzzSpinBox(CtrlInputChar);
        spinBox_char->setObjectName(QString::fromUtf8("spinBox_char"));
        spinBox_char->setGeometry(QRect(10, 0, 20, 21));
        spinBox_char->setFont(font);
        spinBox_char->setFocusPolicy(Qt::TabFocus);
        spinBox_char->setFrame(false);
        spinBox_char->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox_single = new BuzzSpinBox(CtrlInputChar);
        spinBox_single->setObjectName(QString::fromUtf8("spinBox_single"));
        spinBox_single->setGeometry(QRect(0, 0, 160, 21));
        spinBox_single->setFont(font);
        spinBox_single->setFocusPolicy(Qt::TabFocus);
        spinBox_single->setFrame(false);
        spinBox_single->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        spinBox_single->setButtonSymbols(QAbstractSpinBox::NoButtons);
        dSpinBox_single = new BuzzDoubleSpinBox(CtrlInputChar);
        dSpinBox_single->setObjectName(QString::fromUtf8("dSpinBox_single"));
        dSpinBox_single->setGeometry(QRect(0, 0, 160, 21));
        dSpinBox_single->setFont(font);
        dSpinBox_single->setFocusPolicy(Qt::TabFocus);
        dSpinBox_single->setAutoFillBackground(false);
        dSpinBox_single->setFrame(false);
        dSpinBox_single->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        dSpinBox_single->setButtonSymbols(QAbstractSpinBox::NoButtons);
        timeEdit = new My_Time_Edit(CtrlInputChar);
        timeEdit->setObjectName(QString::fromUtf8("timeEdit"));
        timeEdit->setGeometry(QRect(0, 0, 160, 21));
        timeEdit->setLayoutDirection(Qt::LeftToRight);
        timeEdit->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        timeEdit->setButtonSymbols(QAbstractSpinBox::NoButtons);
        label_pw = new QLabel(CtrlInputChar);
        label_pw->setObjectName(QString::fromUtf8("label_pw"));
        label_pw->setGeometry(QRect(0, 0, 10, 21));
        label_new = new QLabel(CtrlInputChar);
        label_new->setObjectName(QString::fromUtf8("label_new"));
        label_new->setGeometry(QRect(10, 0, 10, 21));
        spinBox_IP1 = new BuzzSpinBox(CtrlInputChar);
        spinBox_IP1->setObjectName(QString::fromUtf8("spinBox_IP1"));
        spinBox_IP1->setGeometry(QRect(0, 0, 30, 21));
        spinBox_IP1->setFocusPolicy(Qt::TabFocus);
        spinBox_IP1->setFrame(false);
        spinBox_IP1->setAlignment(Qt::AlignCenter);
        spinBox_IP1->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox_IP1->setMinimum(1);
        spinBox_IP1->setMaximum(255);
        spinBox_IP1->setValue(127);
        spinBox_IP2 = new BuzzSpinBox(CtrlInputChar);
        spinBox_IP2->setObjectName(QString::fromUtf8("spinBox_IP2"));
        spinBox_IP2->setGeometry(QRect(35, 0, 30, 21));
        spinBox_IP2->setFocusPolicy(Qt::TabFocus);
        spinBox_IP2->setFrame(false);
        spinBox_IP2->setAlignment(Qt::AlignCenter);
        spinBox_IP2->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox_IP2->setMaximum(255);
        spinBox_IP2->setValue(127);
        spinBox_IP3 = new BuzzSpinBox(CtrlInputChar);
        spinBox_IP3->setObjectName(QString::fromUtf8("spinBox_IP3"));
        spinBox_IP3->setGeometry(QRect(70, 0, 35, 21));
        spinBox_IP3->setFocusPolicy(Qt::TabFocus);
        spinBox_IP3->setFrame(false);
        spinBox_IP3->setAlignment(Qt::AlignCenter);
        spinBox_IP3->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox_IP3->setMaximum(255);
        spinBox_IP3->setValue(127);
        spinBox_IP4 = new BuzzSpinBox(CtrlInputChar);
        spinBox_IP4->setObjectName(QString::fromUtf8("spinBox_IP4"));
        spinBox_IP4->setGeometry(QRect(105, 0, 35, 21));
        spinBox_IP4->setFocusPolicy(Qt::TabFocus);
        spinBox_IP4->setFrame(false);
        spinBox_IP4->setAlignment(Qt::AlignCenter);
        spinBox_IP4->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox_IP4->setMaximum(255);
        spinBox_IP4->setValue(127);
        label_IP1 = new QLabel(CtrlInputChar);
        label_IP1->setObjectName(QString::fromUtf8("label_IP1"));
        label_IP1->setGeometry(QRect(30, 0, 5, 21));
        label_IP2 = new QLabel(CtrlInputChar);
        label_IP2->setObjectName(QString::fromUtf8("label_IP2"));
        label_IP2->setGeometry(QRect(65, 0, 5, 21));
        label_IP3 = new QLabel(CtrlInputChar);
        label_IP3->setObjectName(QString::fromUtf8("label_IP3"));
        label_IP3->setGeometry(QRect(100, 0, 5, 21));
        dateEdit = new BuzzDateEdit(CtrlInputChar);
        dateEdit->setObjectName(QString::fromUtf8("dateEdit"));
        dateEdit->setGeometry(QRect(0, 0, 160, 21));
        dateEdit->setLayoutDirection(Qt::LeftToRight);
        dateEdit->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        dateEdit->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox_enum = new BuzzSpinBox(CtrlInputChar);
        spinBox_enum->setObjectName(QString::fromUtf8("spinBox_enum"));
        spinBox_enum->setGeometry(QRect(0, 0, 160, 21));
        spinBox_enum->setFont(font);
        spinBox_enum->setFocusPolicy(Qt::TabFocus);
        spinBox_enum->setFrame(false);
        spinBox_enum->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        spinBox_enum->setButtonSymbols(QAbstractSpinBox::NoButtons);

        retranslateUi(CtrlInputChar);

        QMetaObject::connectSlotsByName(CtrlInputChar);
    } // setupUi

    void retranslateUi(QWidget *CtrlInputChar)
    {
        CtrlInputChar->setWindowTitle(QString());
        label_old->setText(QString());
        label_pw->setText(QString());
        label_new->setText(QString());
        label_IP1->setText(QApplication::translate("CtrlInputChar", ".", 0, QApplication::UnicodeUTF8));
        label_IP2->setText(QApplication::translate("CtrlInputChar", ".", 0, QApplication::UnicodeUTF8));
        label_IP3->setText(QApplication::translate("CtrlInputChar", ".", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class CtrlInputChar: public Ui_CtrlInputChar {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CTRLINPUTCHAR_H

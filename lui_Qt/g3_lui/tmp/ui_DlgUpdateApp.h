/********************************************************************************
** Form generated from reading UI file 'DlgUpdateApp.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGUPDATEAPP_H
#define UI_DLGUPDATEAPP_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>

QT_BEGIN_NAMESPACE

class Ui_DlgUpdateApp
{
public:
    QLabel *label_content;

    void setupUi(QDialog *DlgUpdateApp)
    {
        if (DlgUpdateApp->objectName().isEmpty())
            DlgUpdateApp->setObjectName(QString::fromUtf8("DlgUpdateApp"));
        DlgUpdateApp->resize(400, 300);
        label_content = new QLabel(DlgUpdateApp);
        label_content->setObjectName(QString::fromUtf8("label_content"));
        label_content->setGeometry(QRect(80, 60, 54, 12));

        retranslateUi(DlgUpdateApp);

        QMetaObject::connectSlotsByName(DlgUpdateApp);
    } // setupUi

    void retranslateUi(QDialog *DlgUpdateApp)
    {
        DlgUpdateApp->setWindowTitle(QString());
        label_content->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class DlgUpdateApp: public Ui_DlgUpdateApp {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DLGUPDATEAPP_H

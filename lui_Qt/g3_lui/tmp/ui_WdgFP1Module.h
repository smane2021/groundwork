/********************************************************************************
** Form generated from reading UI file 'WdgFP1Module.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDGFP1MODULE_H
#define UI_WDGFP1MODULE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QScrollBar>
#include <QtGui/QWidget>
#include <common/buzztablewidget.h>

QT_BEGIN_NAMESPACE

class Ui_WdgFP1Module
{
public:
    BuzzTableWidget *tableWidget;
    QLabel *label_title;
    QScrollBar *verticalScrollBar;

    void setupUi(QWidget *WdgFP1Module)
    {
        if (WdgFP1Module->objectName().isEmpty())
            WdgFP1Module->setObjectName(QString::fromUtf8("WdgFP1Module"));
        WdgFP1Module->resize(400, 300);
        tableWidget = new BuzzTableWidget(WdgFP1Module);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(0, 21, 160, 108));
        tableWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        label_title = new QLabel(WdgFP1Module);
        label_title->setObjectName(QString::fromUtf8("label_title"));
        label_title->setGeometry(QRect(59, 4, 45, 15));
        verticalScrollBar = new QScrollBar(WdgFP1Module);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(190, 10, 16, 160));
        verticalScrollBar->setOrientation(Qt::Vertical);

        retranslateUi(WdgFP1Module);

        QMetaObject::connectSlotsByName(WdgFP1Module);
    } // setupUi

    void retranslateUi(QWidget *WdgFP1Module)
    {
        WdgFP1Module->setWindowTitle(QString());
        label_title->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class WdgFP1Module: public Ui_WdgFP1Module {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDGFP1MODULE_H

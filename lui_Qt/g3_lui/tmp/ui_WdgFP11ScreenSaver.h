/********************************************************************************
** Form generated from reading UI file 'WdgFP11ScreenSaver.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDGFP11SCREENSAVER_H
#define UI_WDGFP11SCREENSAVER_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WdgFP11ScreenSaver
{
public:

    void setupUi(QWidget *WdgFP11ScreenSaver)
    {
        if (WdgFP11ScreenSaver->objectName().isEmpty())
            WdgFP11ScreenSaver->setObjectName(QString::fromUtf8("WdgFP11ScreenSaver"));
        WdgFP11ScreenSaver->resize(400, 300);
        WdgFP11ScreenSaver->setAutoFillBackground(false);
        WdgFP11ScreenSaver->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"color: rgb(0, 0, 0);\n"
"border-color: rgb(0, 0, 0);"));

        retranslateUi(WdgFP11ScreenSaver);

        QMetaObject::connectSlotsByName(WdgFP11ScreenSaver);
    } // setupUi

    void retranslateUi(QWidget *WdgFP11ScreenSaver)
    {
        WdgFP11ScreenSaver->setWindowTitle(QString());
    } // retranslateUi

};

namespace Ui {
    class WdgFP11ScreenSaver: public Ui_WdgFP11ScreenSaver {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDGFP11SCREENSAVER_H

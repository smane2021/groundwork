/********************************************************************************
** Form generated from reading UI file 'WdgFP8BattDegCurve.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDGFP8BATTDEGCURVE_H
#define UI_WDGFP8BATTDEGCURVE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QScrollBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WdgFP8BattDegCurve
{
public:
    QScrollBar *verticalScrollBar;
    QLabel *label_curveBottom;
    QLabel *label_ambTempDeg;

    void setupUi(QWidget *WdgFP8BattDegCurve)
    {
        if (WdgFP8BattDegCurve->objectName().isEmpty())
            WdgFP8BattDegCurve->setObjectName(QString::fromUtf8("WdgFP8BattDegCurve"));
        WdgFP8BattDegCurve->resize(400, 300);
        verticalScrollBar = new QScrollBar(WdgFP8BattDegCurve);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(340, 30, 16, 160));
        verticalScrollBar->setOrientation(Qt::Vertical);
        label_curveBottom = new QLabel(WdgFP8BattDegCurve);
        label_curveBottom->setObjectName(QString::fromUtf8("label_curveBottom"));
        label_curveBottom->setGeometry(QRect(50, 150, 160, 12));
        label_curveBottom->setAlignment(Qt::AlignCenter);
        label_ambTempDeg = new QLabel(WdgFP8BattDegCurve);
        label_ambTempDeg->setObjectName(QString::fromUtf8("label_ambTempDeg"));
        label_ambTempDeg->setGeometry(QRect(10, 20, 54, 12));

        retranslateUi(WdgFP8BattDegCurve);

        QMetaObject::connectSlotsByName(WdgFP8BattDegCurve);
    } // setupUi

    void retranslateUi(QWidget *WdgFP8BattDegCurve)
    {
        WdgFP8BattDegCurve->setWindowTitle(QString());
        label_curveBottom->setText(QString());
        label_ambTempDeg->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class WdgFP8BattDegCurve: public Ui_WdgFP8BattDegCurve {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDGFP8BATTDEGCURVE_H

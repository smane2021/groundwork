/********************************************************************************
** Form generated from reading UI file 'DlgInfo.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGINFO_H
#define UI_DLGINFO_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <common/buzztablewidget.h>

QT_BEGIN_NAMESPACE

class Ui_DlgInfo
{
public:
    BuzzTableWidget *tableWidget;
    QLabel *label_content;
    QLabel *label_OK;
    QLabel *label_Cancel;

    void setupUi(QDialog *DlgInfo)
    {
        if (DlgInfo->objectName().isEmpty())
            DlgInfo->setObjectName(QString::fromUtf8("DlgInfo"));
        DlgInfo->resize(491, 378);
        DlgInfo->setFocusPolicy(Qt::StrongFocus);
        tableWidget = new BuzzTableWidget(DlgInfo);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(210, 10, 160, 128));
        tableWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        label_content = new QLabel(DlgInfo);
        label_content->setObjectName(QString::fromUtf8("label_content"));
        label_content->setGeometry(QRect(0, 36, 320, 20));
        label_OK = new QLabel(DlgInfo);
        label_OK->setObjectName(QString::fromUtf8("label_OK"));
        label_OK->setGeometry(QRect(0, 60, 320, 21));
        label_Cancel = new QLabel(DlgInfo);
        label_Cancel->setObjectName(QString::fromUtf8("label_Cancel"));
        label_Cancel->setGeometry(QRect(0, 85, 320, 21));

        retranslateUi(DlgInfo);

        QMetaObject::connectSlotsByName(DlgInfo);
    } // setupUi

    void retranslateUi(QDialog *DlgInfo)
    {
        DlgInfo->setWindowTitle(QString());
        label_content->setText(QString());
        label_OK->setText(QString());
        label_Cancel->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class DlgInfo: public Ui_DlgInfo {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DLGINFO_H

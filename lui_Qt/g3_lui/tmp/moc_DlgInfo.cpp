/****************************************************************************
** Meta object code from reading C++ file 'DlgInfo.h'
**
** Created: Fri Mar 13 11:21:45 2020
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../util/DlgInfo.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DlgInfo.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ThreadWork[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      20,   12,   11,   11, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_ThreadWork[] = {
    "ThreadWork\0\0nResult\0sigExeced(int)\0"
};

const QMetaObject ThreadWork::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_ThreadWork,
      qt_meta_data_ThreadWork, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ThreadWork::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ThreadWork::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ThreadWork::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ThreadWork))
        return static_cast<void*>(const_cast< ThreadWork*>(this));
    return QThread::qt_metacast(_clname);
}

int ThreadWork::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: sigExeced((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void ThreadWork::sigExeced(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
static const uint qt_meta_data_DlgInfo[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
       9,    8,    8,    8, 0x08,
      36,    8,    8,    8, 0x08,
      64,    8,    8,    8, 0x08,
     100,   92,    8,    8, 0x08,
     115,    8,    8,    8, 0x0a,
     124,    8,    8,    8, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_DlgInfo[] = {
    "DlgInfo\0\0sltTimerHandlerExecShell()\0"
    "sltTimerHandlerExecSetCmd()\0"
    "sltTimerHandlerChangeLang()\0nResult\0"
    "sltExeced(int)\0accept()\0reject()\0"
};

const QMetaObject DlgInfo::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_DlgInfo,
      qt_meta_data_DlgInfo, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DlgInfo::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DlgInfo::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DlgInfo::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DlgInfo))
        return static_cast<void*>(const_cast< DlgInfo*>(this));
    return QDialog::qt_metacast(_clname);
}

int DlgInfo::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: sltTimerHandlerExecShell(); break;
        case 1: sltTimerHandlerExecSetCmd(); break;
        case 2: sltTimerHandlerChangeLang(); break;
        case 3: sltExeced((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: accept(); break;
        case 5: reject(); break;
        default: ;
        }
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

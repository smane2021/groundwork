/********************************************************************************
** Form generated from reading UI file 'DlgLoading.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGLOADING_H
#define UI_DLGLOADING_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>

QT_BEGIN_NAMESPACE

class Ui_DlgLoading
{
public:
    QLabel *label;

    void setupUi(QDialog *DlgLoading)
    {
        if (DlgLoading->objectName().isEmpty())
            DlgLoading->setObjectName(QString::fromUtf8("DlgLoading"));
        DlgLoading->resize(400, 300);
        DlgLoading->setFocusPolicy(Qt::StrongFocus);
        label = new QLabel(DlgLoading);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(140, 90, 100, 100));

        retranslateUi(DlgLoading);

        QMetaObject::connectSlotsByName(DlgLoading);
    } // setupUi

    void retranslateUi(QDialog *DlgLoading)
    {
        DlgLoading->setWindowTitle(QString());
        label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class DlgLoading: public Ui_DlgLoading {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DLGLOADING_H

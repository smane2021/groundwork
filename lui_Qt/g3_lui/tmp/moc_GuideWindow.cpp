/****************************************************************************
** Meta object code from reading C++ file 'GuideWindow.h'
**
** Created: Fri Mar 13 11:21:44 2020
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../basicWidget/GuideWindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'GuideWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_GuideWindow[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x05,
      28,   12,   12,   12, 0x05,
      56,   12,   12,   12, 0x05,
      73,   12,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
     105,  101,   12,   12, 0x08,
     127,   12,   12,   12, 0x08,
     153,   12,   12,   12, 0x08,
     168,   12,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_GuideWindow[] = {
    "GuideWindow\0\0goToHomePage()\0"
    "goToBaseWindow(WIDGET_TYPE)\0"
    "sigDetectAlarm()\0goToLoginWindow(LOGIN_TYPE)\0"
    "key\0sltTableKeyPress(int)\0"
    "FocusTableWdg(INPUT_TYPE)\0TimerHandler()\0"
    "on_tableWidget_input_itemSelectionChanged()\0"
};

const QMetaObject GuideWindow::staticMetaObject = {
    { &BasicWidget::staticMetaObject, qt_meta_stringdata_GuideWindow,
      qt_meta_data_GuideWindow, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &GuideWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *GuideWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *GuideWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_GuideWindow))
        return static_cast<void*>(const_cast< GuideWindow*>(this));
    return BasicWidget::qt_metacast(_clname);
}

int GuideWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: goToHomePage(); break;
        case 1: goToBaseWindow((*reinterpret_cast< WIDGET_TYPE(*)>(_a[1]))); break;
        case 2: sigDetectAlarm(); break;
        case 3: goToLoginWindow((*reinterpret_cast< LOGIN_TYPE(*)>(_a[1]))); break;
        case 4: sltTableKeyPress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: FocusTableWdg((*reinterpret_cast< INPUT_TYPE(*)>(_a[1]))); break;
        case 6: TimerHandler(); break;
        case 7: on_tableWidget_input_itemSelectionChanged(); break;
        default: ;
        }
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void GuideWindow::goToHomePage()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void GuideWindow::goToBaseWindow(WIDGET_TYPE _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void GuideWindow::sigDetectAlarm()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void GuideWindow::goToLoginWindow(LOGIN_TYPE _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE

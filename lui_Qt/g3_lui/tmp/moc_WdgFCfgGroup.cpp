/****************************************************************************
** Meta object code from reading C++ file 'WdgFCfgGroup.h'
**
** Created: Fri Mar 13 11:21:45 2020
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../configWidget/WdgFCfgGroup.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'WdgFCfgGroup.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_WdgFCfgGroup[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x05,
      29,   13,   13,   13, 0x05,
      59,   13,   13,   13, 0x05,
      87,   13,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
     112,  108,   13,   13, 0x08,
     134,   13,   13,   13, 0x08,
     152,   13,   13,   13, 0x08,
     194,  190,   13,   13, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_WdgFCfgGroup[] = {
    "WdgFCfgGroup\0\0goToHomePage()\0"
    "goToGuideWindow(WIDGET_GUIDE)\0"
    "goToBaseWindow(WIDGET_TYPE)\0"
    "sigStopDetectAlarm()\0key\0sltTableKeyPress(int)\0"
    "sltTimerHandler()\0"
    "on_tableWidget_itemSelectionChanged()\0"
    "ipt\0FocusTableWdg(INPUT_TYPE)\0"
};

const QMetaObject WdgFCfgGroup::staticMetaObject = {
    { &BasicWidget::staticMetaObject, qt_meta_stringdata_WdgFCfgGroup,
      qt_meta_data_WdgFCfgGroup, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &WdgFCfgGroup::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *WdgFCfgGroup::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *WdgFCfgGroup::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_WdgFCfgGroup))
        return static_cast<void*>(const_cast< WdgFCfgGroup*>(this));
    return BasicWidget::qt_metacast(_clname);
}

int WdgFCfgGroup::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: goToHomePage(); break;
        case 1: goToGuideWindow((*reinterpret_cast< WIDGET_GUIDE(*)>(_a[1]))); break;
        case 2: goToBaseWindow((*reinterpret_cast< WIDGET_TYPE(*)>(_a[1]))); break;
        case 3: sigStopDetectAlarm(); break;
        case 4: sltTableKeyPress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: sltTimerHandler(); break;
        case 6: on_tableWidget_itemSelectionChanged(); break;
        case 7: FocusTableWdg((*reinterpret_cast< INPUT_TYPE(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void WdgFCfgGroup::goToHomePage()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void WdgFCfgGroup::goToGuideWindow(WIDGET_GUIDE _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void WdgFCfgGroup::goToBaseWindow(WIDGET_TYPE _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void WdgFCfgGroup::sigStopDetectAlarm()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}
QT_END_MOC_NAMESPACE

/********************************************************************************
** Form generated from reading UI file 'WdgFP10Inventory.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDGFP10INVENTORY_H
#define UI_WDGFP10INVENTORY_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QScrollBar>
#include <QtGui/QWidget>
#include <common/buzztablewidget.h>

QT_BEGIN_NAMESPACE

class Ui_WdgFP10Inventory
{
public:
    QScrollBar *verticalScrollBar;
    BuzzTableWidget *tableWidget;
    QLabel *label_enter;

    void setupUi(QWidget *WdgFP10Inventory)
    {
        if (WdgFP10Inventory->objectName().isEmpty())
            WdgFP10Inventory->setObjectName(QString::fromUtf8("WdgFP10Inventory"));
        WdgFP10Inventory->resize(400, 300);
        verticalScrollBar = new QScrollBar(WdgFP10Inventory);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(180, 0, 16, 160));
        verticalScrollBar->setOrientation(Qt::Vertical);
        tableWidget = new BuzzTableWidget(WdgFP10Inventory);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(0, 0, 160, 128));
        label_enter = new QLabel(WdgFP10Inventory);
        label_enter->setObjectName(QString::fromUtf8("label_enter"));
        label_enter->setGeometry(QRect(30, 140, 54, 12));

        retranslateUi(WdgFP10Inventory);

        QMetaObject::connectSlotsByName(WdgFP10Inventory);
    } // setupUi

    void retranslateUi(QWidget *WdgFP10Inventory)
    {
        WdgFP10Inventory->setWindowTitle(QString());
        label_enter->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class WdgFP10Inventory: public Ui_WdgFP10Inventory {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDGFP10INVENTORY_H

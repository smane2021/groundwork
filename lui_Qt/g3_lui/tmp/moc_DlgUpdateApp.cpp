/****************************************************************************
** Meta object code from reading C++ file 'DlgUpdateApp.h'
**
** Created: Fri Mar 13 11:21:46 2020
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../util/DlgUpdateApp.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DlgUpdateApp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DlgUpdateApp[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x0a,
      23,   13,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_DlgUpdateApp[] = {
    "DlgUpdateApp\0\0accept()\0reject()\0"
};

const QMetaObject DlgUpdateApp::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_DlgUpdateApp,
      qt_meta_data_DlgUpdateApp, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DlgUpdateApp::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DlgUpdateApp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DlgUpdateApp::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DlgUpdateApp))
        return static_cast<void*>(const_cast< DlgUpdateApp*>(this));
    return QDialog::qt_metacast(_clname);
}

int DlgUpdateApp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: accept(); break;
        case 1: reject(); break;
        default: ;
        }
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

/****************************************************************************
** Meta object code from reading C++ file 'Wdg2DCABranch.h'
**
** Created: Fri Mar 13 11:21:43 2020
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../equipWidget/Wdg2DCABranch.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Wdg2DCABranch.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Wdg2DCABranch[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_Wdg2DCABranch[] = {
    "Wdg2DCABranch\0\0goToBaseWindow(WIDGET_TYPE)\0"
};

const QMetaObject Wdg2DCABranch::staticMetaObject = {
    { &BasicWidget::staticMetaObject, qt_meta_stringdata_Wdg2DCABranch,
      qt_meta_data_Wdg2DCABranch, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Wdg2DCABranch::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Wdg2DCABranch::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Wdg2DCABranch::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Wdg2DCABranch))
        return static_cast<void*>(const_cast< Wdg2DCABranch*>(this));
    return BasicWidget::qt_metacast(_clname);
}

int Wdg2DCABranch::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = BasicWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: goToBaseWindow((*reinterpret_cast< WIDGET_TYPE(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void Wdg2DCABranch::goToBaseWindow(WIDGET_TYPE _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE

/********************************************************************************
** Form generated from reading UI file 'WdgFP5Deg2Curve.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDGFP5DEG2CURVE_H
#define UI_WDGFP5DEG2CURVE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QScrollBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WdgFP5Deg2Curve
{
public:
    QScrollBar *verticalScrollBar;
    QLabel *label_curveBottom;
    QLabel *label_ambTempDeg;

    void setupUi(QWidget *WdgFP5Deg2Curve)
    {
        if (WdgFP5Deg2Curve->objectName().isEmpty())
            WdgFP5Deg2Curve->setObjectName(QString::fromUtf8("WdgFP5Deg2Curve"));
        WdgFP5Deg2Curve->resize(320, 240);
        verticalScrollBar = new QScrollBar(WdgFP5Deg2Curve);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(310, 0, 16, 231));
        verticalScrollBar->setOrientation(Qt::Vertical);
        label_curveBottom = new QLabel(WdgFP5Deg2Curve);
        label_curveBottom->setObjectName(QString::fromUtf8("label_curveBottom"));
        label_curveBottom->setGeometry(QRect(60, 170, 160, 12));
        label_curveBottom->setAlignment(Qt::AlignCenter);
        label_ambTempDeg = new QLabel(WdgFP5Deg2Curve);
        label_ambTempDeg->setObjectName(QString::fromUtf8("label_ambTempDeg"));
        label_ambTempDeg->setGeometry(QRect(10, 20, 54, 12));

        retranslateUi(WdgFP5Deg2Curve);

        QMetaObject::connectSlotsByName(WdgFP5Deg2Curve);
    } // setupUi

    void retranslateUi(QWidget *WdgFP5Deg2Curve)
    {
        WdgFP5Deg2Curve->setWindowTitle(QString());
        label_curveBottom->setText(QString());
        label_ambTempDeg->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class WdgFP5Deg2Curve: public Ui_WdgFP5Deg2Curve {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDGFP5DEG2CURVE_H

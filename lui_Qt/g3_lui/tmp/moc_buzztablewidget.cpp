/****************************************************************************
** Meta object code from reading C++ file 'buzztablewidget.h'
**
** Created: Fri Mar 13 11:21:41 2020
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../common/buzztablewidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'buzztablewidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_BuzzTableWidget[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      21,   17,   16,   16, 0x05,

 // slots: signature, parameters, type, tag, flags
      45,   43,   16,   16, 0x0a,
      66,   16,   16,   16, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_BuzzTableWidget[] = {
    "BuzzTableWidget\0\0key\0sigTableKeyPress(int)\0"
    ",\0CellClicked(int,int)\0StopBuzz()\0"
};

const QMetaObject BuzzTableWidget::staticMetaObject = {
    { &QTableWidget::staticMetaObject, qt_meta_stringdata_BuzzTableWidget,
      qt_meta_data_BuzzTableWidget, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &BuzzTableWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *BuzzTableWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *BuzzTableWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_BuzzTableWidget))
        return static_cast<void*>(const_cast< BuzzTableWidget*>(this));
    return QTableWidget::qt_metacast(_clname);
}

int BuzzTableWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTableWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: sigTableKeyPress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: CellClicked((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: StopBuzz(); break;
        default: ;
        }
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void BuzzTableWidget::sigTableKeyPress(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE

/********************************************************************************
** Form generated from reading UI file 'GuideWindow.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GUIDEWINDOW_H
#define UI_GUIDEWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QScrollBar>
#include <QtGui/QStackedWidget>
#include <QtGui/QWidget>
#include <common/buzztablewidget.h>

QT_BEGIN_NAMESPACE

class Ui_GuideWindow
{
public:
    QStackedWidget *stackedWidget;
    QWidget *p0_language;
    BuzzTableWidget *tableWidget_language;
    QWidget *p1_enter;
    BuzzTableWidget *tableWidget_enter;
    QWidget *p2_input;
    QLabel *label_name;
    BuzzTableWidget *tableWidget_input;
    QScrollBar *verticalScrollBar;
    QWidget *p3_continue;
    BuzzTableWidget *tableWidget_continue;
    QWidget *p3_finsish;
    BuzzTableWidget *tableWidget_finish;
    QWidget *p4_password;
    BuzzTableWidget *tableWidget_password;

    void setupUi(QWidget *GuideWindow)
    {
        if (GuideWindow->objectName().isEmpty())
            GuideWindow->setObjectName(QString::fromUtf8("GuideWindow"));
        GuideWindow->resize(400, 300);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(GuideWindow->sizePolicy().hasHeightForWidth());
        GuideWindow->setSizePolicy(sizePolicy);
        stackedWidget = new QStackedWidget(GuideWindow);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setGeometry(QRect(0, 0, 160, 128));
        sizePolicy.setHeightForWidth(stackedWidget->sizePolicy().hasHeightForWidth());
        stackedWidget->setSizePolicy(sizePolicy);
        stackedWidget->setAutoFillBackground(false);
        stackedWidget->setFrameShape(QFrame::NoFrame);
        stackedWidget->setFrameShadow(QFrame::Plain);
        p0_language = new QWidget();
        p0_language->setObjectName(QString::fromUtf8("p0_language"));
        sizePolicy.setHeightForWidth(p0_language->sizePolicy().hasHeightForWidth());
        p0_language->setSizePolicy(sizePolicy);
        p0_language->setMinimumSize(QSize(800, 0));
        tableWidget_language = new BuzzTableWidget(p0_language);
        tableWidget_language->setObjectName(QString::fromUtf8("tableWidget_language"));
        tableWidget_language->setGeometry(QRect(0, 0, 160, 128));
        stackedWidget->addWidget(p0_language);
        p1_enter = new QWidget();
        p1_enter->setObjectName(QString::fromUtf8("p1_enter"));
        sizePolicy.setHeightForWidth(p1_enter->sizePolicy().hasHeightForWidth());
        p1_enter->setSizePolicy(sizePolicy);
        tableWidget_enter = new BuzzTableWidget(p1_enter);
        tableWidget_enter->setObjectName(QString::fromUtf8("tableWidget_enter"));
        tableWidget_enter->setGeometry(QRect(0, 0, 160, 128));
        tableWidget_enter->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        stackedWidget->addWidget(p1_enter);
        p2_input = new QWidget();
        p2_input->setObjectName(QString::fromUtf8("p2_input"));
        label_name = new QLabel(p2_input);
        label_name->setObjectName(QString::fromUtf8("label_name"));
        label_name->setGeometry(QRect(0, 0, 160, 21));
        tableWidget_input = new BuzzTableWidget(p2_input);
        tableWidget_input->setObjectName(QString::fromUtf8("tableWidget_input"));
        tableWidget_input->setGeometry(QRect(0, 20, 140, 110));
        tableWidget_input->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        tableWidget_input->setCornerButtonEnabled(true);
        verticalScrollBar = new QScrollBar(p2_input);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(140, 20, 16, 111));
        verticalScrollBar->setOrientation(Qt::Vertical);
        stackedWidget->addWidget(p2_input);
        p3_continue = new QWidget();
        p3_continue->setObjectName(QString::fromUtf8("p3_continue"));
        tableWidget_continue = new BuzzTableWidget(p3_continue);
        tableWidget_continue->setObjectName(QString::fromUtf8("tableWidget_continue"));
        tableWidget_continue->setGeometry(QRect(0, 0, 160, 128));
        tableWidget_continue->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        stackedWidget->addWidget(p3_continue);
        p3_finsish = new QWidget();
        p3_finsish->setObjectName(QString::fromUtf8("p3_finsish"));
        tableWidget_finish = new BuzzTableWidget(p3_finsish);
        tableWidget_finish->setObjectName(QString::fromUtf8("tableWidget_finish"));
        tableWidget_finish->setGeometry(QRect(0, 0, 160, 128));
        tableWidget_finish->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        stackedWidget->addWidget(p3_finsish);
        p4_password = new QWidget();
        p4_password->setObjectName(QString::fromUtf8("p4_password"));
        sizePolicy.setHeightForWidth(p4_password->sizePolicy().hasHeightForWidth());
        p4_password->setSizePolicy(sizePolicy);
        tableWidget_password = new BuzzTableWidget(p4_password);
        tableWidget_password->setObjectName(QString::fromUtf8("tableWidget_password"));
        tableWidget_password->setGeometry(QRect(0, 0, 160, 128));
        stackedWidget->addWidget(p4_password);

        retranslateUi(GuideWindow);

        stackedWidget->setCurrentIndex(5);


        QMetaObject::connectSlotsByName(GuideWindow);
    } // setupUi

    void retranslateUi(QWidget *GuideWindow)
    {
        GuideWindow->setWindowTitle(QString());
        label_name->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class GuideWindow: public Ui_GuideWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GUIDEWINDOW_H

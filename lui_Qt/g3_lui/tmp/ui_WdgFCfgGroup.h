/********************************************************************************
** Form generated from reading UI file 'WdgFCfgGroup.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDGFCFGGROUP_H
#define UI_WDGFCFGGROUP_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QScrollBar>
#include <QtGui/QWidget>
#include <common/buzztablewidget.h>

QT_BEGIN_NAMESPACE

class Ui_WdgFCfgGroup
{
public:
    QScrollBar *verticalScrollBar;
    BuzzTableWidget *tableWidget;
    QLabel *label_title;

    void setupUi(QWidget *WdgFCfgGroup)
    {
        if (WdgFCfgGroup->objectName().isEmpty())
            WdgFCfgGroup->setObjectName(QString::fromUtf8("WdgFCfgGroup"));
        WdgFCfgGroup->resize(320, 240);
        verticalScrollBar = new QScrollBar(WdgFCfgGroup);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(229, 26, 16, 160));
        verticalScrollBar->setOrientation(Qt::Vertical);
        tableWidget = new BuzzTableWidget(WdgFCfgGroup);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(19, 27, 160, 108));
        label_title = new QLabel(WdgFCfgGroup);
        label_title->setObjectName(QString::fromUtf8("label_title"));
        label_title->setGeometry(QRect(50, 10, 115, 15));

        retranslateUi(WdgFCfgGroup);

        QMetaObject::connectSlotsByName(WdgFCfgGroup);
    } // setupUi

    void retranslateUi(QWidget *WdgFCfgGroup)
    {
        WdgFCfgGroup->setWindowTitle(QString());
        label_title->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class WdgFCfgGroup: public Ui_WdgFCfgGroup {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDGFCFGGROUP_H

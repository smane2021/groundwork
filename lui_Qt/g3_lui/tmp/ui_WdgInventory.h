/********************************************************************************
** Form generated from reading UI file 'WdgInventory.ui'
**
** Created: Fri Mar 13 11:21:38 2020
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WDGINVENTORY_H
#define UI_WDGINVENTORY_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QScrollBar>
#include <QtGui/QWidget>
#include <common/buzztablewidget.h>

QT_BEGIN_NAMESPACE

class Ui_WdgInventory
{
public:
    BuzzTableWidget *tableWidget;
    QLabel *label_enter;
    QScrollBar *verticalScrollBar;

    void setupUi(QWidget *WdgInventory)
    {
        if (WdgInventory->objectName().isEmpty())
            WdgInventory->setObjectName(QString::fromUtf8("WdgInventory"));
        WdgInventory->resize(260, 172);
        tableWidget = new BuzzTableWidget(WdgInventory);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(0, 21, 160, 108));
        tableWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        label_enter = new QLabel(WdgInventory);
        label_enter->setObjectName(QString::fromUtf8("label_enter"));
        label_enter->setGeometry(QRect(31, 4, 115, 15));
        verticalScrollBar = new QScrollBar(WdgInventory);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(200, 0, 16, 160));
        verticalScrollBar->setOrientation(Qt::Vertical);

        retranslateUi(WdgInventory);

        QMetaObject::connectSlotsByName(WdgInventory);
    } // setupUi

    void retranslateUi(QWidget *WdgInventory)
    {
        WdgInventory->setWindowTitle(QString());
        label_enter->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class WdgInventory: public Ui_WdgInventory {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WDGINVENTORY_H

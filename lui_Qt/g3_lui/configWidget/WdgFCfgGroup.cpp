#include "WdgFCfgGroup.h"
#include "ui_WdgFCfgGroup.h"

#include <QKeyEvent>
#include <QFile>
#include <QTextStream>
#include "basicWidget/GuideWindow.h"
#include "util/DlgInfo.h"
#include "config/configparam.h"
#include "common/pubInclude.h"
#include "common/global.h"
#include "common/BuzzDoubleSpinBox.h"
#include "common/BuzzSpinBox.h"
#include "common/MultiDemon.h"
#include "basicWidget/homepagewindow.h"


//#define DEBUG_MENU

#define MAX_CTRL_ITEMS_CFG                      (MAXLEN_SET+11)
#define WDG_FCfg_GROUP_NODE_CREATE_INTERVAL     800 //ms
#define CFG_GROUP_MENU_ROW_INVALID              -1

WdgFCfgGroup::WdgFCfgGroup(int iIniInputWdgNum, QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::WdgFCfgGroup)
{
    ui->setupUi(this);

    InitWidget();
    InitConnect();

    m_stMenuData.loadMenu("");
    memset(&m_cmdItem, 0, sizeof(m_cmdItem));//clear the point first
    m_cmdItem.CmdType   = CT_READ;

    m_nRows = 0;
    m_nInvalidRows = 0;
    m_nCtrlInputCreated = 0;
    m_nCtrlInputMax     = MAX_CTRL_ITEMS_CFG;
    m_iMenuNodeId = MENUNODE_ID_ROOT;
    m_iInitSelectedId = MENUNODE_INVALID;

    ui->tableWidget->setColumnCount(1);
    createInputWidget(iIniInputWdgNum);

    m_timerIdSpawn = startTimer(WDG_FCfg_GROUP_NODE_CREATE_INTERVAL);

    installEventFilter( &m_stMenuData );
}

WdgFCfgGroup::~WdgFCfgGroup()
{
    delete ui;
}

void WdgFCfgGroup::InitWidget(void)
{
    SET_BACKGROUND_WIDGET( "WdgFCfgGroup",PosBase::strImgBack_Line_Title );
    SET_GEOMETRY_LABEL_TITLE( ui->label_title );
    SET_TABLEWDG_STYLE( tableWidget, TT_YES_TITLE_YES_SCROOL );
}

void WdgFCfgGroup::InitConnect(void)
{
    connect( ui->tableWidget, SIGNAL(sigTableKeyPress(int)),
             this, SLOT(sltTableKeyPress(int)) );

    connect( &m_oTimer, SIGNAL(timeout()),
             this, SLOT(sltTimerHandler()) );
}

void WdgFCfgGroup::Enter(void* param)
{
    m_timeElapsedKeyPress.restart();
    INIT_VAR;

    if(param != NULL)
    {
        m_iMenuNodeId = MENUNODE_ID_ROOT;
        m_iInitSelectedId = MENUNODE_INVALID;
    }
    Leave();
    initMenuMode();

    if(m_iMenuNodeId != MENUNODE_ID_ROOT)
    {
        RefreshNow();
    }
    else
    {
        ShowData (NULL);
    }

    ui->tableWidget->selectRow( 0 );
    ui->tableWidget->selectRow( 1 );

    initSelectedMenuNode();
    updateScrollBar();
    ui->tableWidget->setFocus();

    restartQTimer();
    g_bEnterFirstly = false;
}

void WdgFCfgGroup::Leave(void)
{
    TRACEDEBUG( "WdgFCfgGroup::Leave" );
    m_oTimer.stop();

    int nSelRow = ui->tableWidget->selectedRow();
    if((nSelRow < 0) || (nSelRow >= m_nCtrlInputCreated))
    {
        return;
    }

    CtrlInputChar* pCtrl = NULL;
    pCtrl = (CtrlInputChar*)(ui->tableWidget->cellWidget(nSelRow, 0));
    TRACEDEBUG( "WdgFCfgGroup::Leave pCtrl<%x>", pCtrl );
    if (pCtrl)
    {
        pCtrl->setHighLight( false );
    }
}

void WdgFCfgGroup::Refresh(void)
{
    MenuNode_t *pNode = NULL;
    pNode = m_stMenuData.getNode(m_iMenuNodeId);
    if(pNode == NULL)
    {
        return;
    }

    int iScreenID = pNode->iDirQueryId;
    TRACEDEBUG("WdgFCfgGroup::Refresh() iScreenID:%x",iScreenID);
    if(iScreenID != MENUSIG_ID_INVALID)
    {
        if ((g_timeElapsedKeyPress.elapsed() >
                TIME_ELAPSED_KEYPRESS) ||
                g_bEnterFirstly)
        {
            TRACEDEBUG("REFRESH_DATA_PERIODICALLY .....");
            REFRESH_DATA_PERIODICALLY(
                        iScreenID,
                        "WdgFCfgGroup::Refresh()");
        }
    }
    else
    {
        ShowData( NULL );
    }
    TRACEDEBUG( "WdgFCfgGroup::Refresh pNode->iDirQueryId=%x", pNode->iDirQueryId);
}

void WdgFCfgGroup::RefreshNow(void)
{
    MenuNode_t *pNode = NULL;
    pNode = m_stMenuData.getNode(m_iMenuNodeId);
    if(pNode == NULL)
    {
        return;
    }

    int iScreenID = pNode->iDirQueryId;
    TRACEDEBUG( "WdgFCfgGroup::RefreshNow pNode->iDirQueryId=%x", pNode->iDirQueryId);
    if(iScreenID != MENUSIG_ID_INVALID)
    {
        m_cmdItem.ScreenID = iScreenID;
        TRACEDEBUG( "WdgFCfgGroup::RefreshNow m_cmdItem.ScreenID=%x, m_cmdItem.CmdType=%d", m_cmdItem.ScreenID, m_cmdItem.CmdType);
        TRY_GET_DATA_MULTY;
    }
    else
    {
        ShowData( NULL );
    }
}

void WdgFCfgGroup::ShowData(void* pData)
{
    PACK_SETINFO* info = NULL;

    //get a copy of data
    info = (PACK_SETINFO*)g_dataBuff;
    if(pData != NULL)
    {
        memcpy( g_dataBuff, pData, SIZEOF_MAPFILE );
    }
    else
    {
        info->SetNum = 0;
    }

    clearTxtInputTable();

    TRACEDEBUG( "WdgFCfgGroup::ShowData m_iMenuNodeId=%d", m_iMenuNodeId);
    ShowMenu(info, m_iMenuNodeId);
}

void WdgFCfgGroup::ShowMenu(PACK_SETINFO* pInfo, int iMenuNodeId)
{
    MenuNode_t* pNode = NULL;
    //PACK_SETINFO* pInfo = (PACK_SETINFO* )pData;
    SET_INFO* pInfoItem = pInfo->SettingInfo;
    int iSigNum = pInfo->SetNum;
    //int iMenuNodeId = m_iMenuNodeId;
    int iListNodeCount = 0;
    int iNeedInsertCount = 0;
    int iNodePos = 0;
    int iSigIndex = 0;
    int i = 0;
    //display menu

    m_nRows= 0;//init valid items count
    m_nInvalidRows= 0;

    pNode = m_stMenuData.getNode(iMenuNodeId);
    TRACEDEBUG( "WdgFCfgGroup::ShowMenu iSigNum=%d, pNode=%x", iSigNum, (unsigned int)pNode);
    if(pNode != NULL)
    {
        QString sTitle(pNode->sNodeName);
        ui->label_title->setText(sTitle);

        iSigIndex = 0;
        pNode = pNode->pFirstSon;
        //add dir and cmd
        while(pNode != NULL)
        {
            if(pNode->bDispEnable)
            {
                iNodePos = pNode->iDispOrder;
                iNeedInsertCount = iNodePos - iListNodeCount - 1;

                TRACEDEBUG( "WdgFCfgGroup::ShowMenu iNodePos=%d, iNeedInsertCount=%d, iListNodeCount=%d",
                           iNodePos, iNeedInsertCount, iListNodeCount);

                //insert signal
                for(i = 0; i < iNeedInsertCount; i++)
                {
                    if(iSigIndex >= iSigNum)
                    {
                        break;
                    }
                    TRACEDEBUG( "\t 1 WdgFCfgGroup::ShowMenu iSigIndex=%d, iListNodeCount=%d", iSigIndex, iListNodeCount);
                    appendShowNode(MENUNODE_SIG, (void *)&pInfoItem[iSigIndex], iSigIndex);
                    iSigIndex++;
                    iListNodeCount++;
                }
                //insert dir or cmd
                appendShowNode(pNode->iNodeType, (void *)pNode, -1);
                iListNodeCount++;
            }
            pNode = pNode->pNextBrother;//next node
       }
        //insert these left signals
        for(i = iSigIndex; i < iSigNum; i++)
        {
            TRACEDEBUG( "\t 2 WdgFCfgGroup::ShowMenu iSigIndex=%d, iListNodeCount=%d", iSigIndex, iListNodeCount);
            appendShowNode(MENUNODE_SIG, (void *)&pInfoItem[iSigIndex], iSigIndex);
            iSigIndex++;
            iListNodeCount++;
        }
        if( (m_iMenuNodeId != MENUNODE_ID_ROOT)
                && (m_iMenuNodeId != MENUNODE_ID_BAT_SETTINGS))
        {
            for(i = m_nRows; i < 12; i++)
            {
                m_nInvalidRows++;
                appendShowNode(MENUNODE_INVALID, NULL, i);
            }
        }
    }

    DisplayValidNode();
}

void WdgFCfgGroup::appendShowNode(int iNodeType, void *pMenuItemData, int iSigIndexId)
{
    if (m_nCtrlInputMax <= m_nRows+2)
    {
        m_nCtrlInputMax += 2;
    }

    if (m_nCtrlInputCreated < m_nCtrlInputMax)
    {
        killTimer( m_timerIdSpawn );
        m_timerIdSpawn = 0;

        createInputWidget( m_nCtrlInputMax-m_nCtrlInputCreated );
    }

    if(iNodeType == MENUNODE_DIR)
    {
        appendShowDir((MenuNode_t *)pMenuItemData);
    }
    else if(iNodeType == MENUNODE_CMD)
    {
        appendShowCmd((MenuNode_t *)pMenuItemData);
    }
    else if(iNodeType == MENUNODE_SIG)
    {
        appendShowSig((SET_INFO *)pMenuItemData, iSigIndexId);
    }
    else if(iNodeType == MENUNODE_INVALID)
    {
        appendInvalidShowNode(iSigIndexId);
    }
}

void WdgFCfgGroup::appendInvalidShowNode(int iSigIndexId)
{
    MenuNode_t stNode;
    MenuNode_t *pNode = &stNode;
    CtrlInputParam stCtrlParam;
    CtrlInputParam *pCtrlParam = &stCtrlParam;
    CtrlInputChar*    pCtrl  = NULL;

    // item name
    pCtrl = (CtrlInputChar*)(ui->tableWidget->cellWidget(m_nRows, 0));
    pCtrlParam->ipt = IPT_LABEL_READONLY;
    pCtrlParam->strInit = "";
    pCtrl->setParam(pCtrlParam);
    pNode->iNodeId = MENUNODE_ID_INVALID;
    pNode->iNodeType = MENUNODE_INVALID;
    pCtrl->setMenuNode(pNode);
    m_nRows++;

    TRACEDEBUG( "WdgFCfgGroup::appendInvalidShowNode iSigIndexId=%d", iSigIndexId);
}

void WdgFCfgGroup::appendShowDir(MenuNode_t *pNode)
{
    CtrlInputParam stCtrlParam;
    CtrlInputParam *pCtrlParam = &stCtrlParam;
    CtrlInputChar*    pCtrl  = NULL;
    QString sName = QString(pNode->sNodeName) + " >";

    // input widget
    pCtrl = (CtrlInputChar*)(ui->tableWidget->cellWidget(m_nRows, 0));
    pCtrlParam->ipt = IPT_LABEL_READONLY;
    pCtrlParam->strInit = sName;
    pCtrl->setParam(pCtrlParam);
    pNode->iNodeType = MENUNODE_DIR;
    pCtrl->setMenuNode(pNode);
    m_nRows++;

    TRACEDEBUG( "WdgFCfgGroup::appendShowDir m_nRows=%d, name=%s, iNodeId=%d, iDispOrder=%d, iDirQueryId=%x",
               m_nRows, pNode->sNodeName, pNode->iNodeId, pNode->iDispOrder, pNode->iDirQueryId);
}

void WdgFCfgGroup::appendShowCmd(MenuNode_t *pNode)
{
    CtrlInputParam stCtrlParam;
    MenuCmd_t stCmdCfg;
    CtrlInputParam *pCtrlParam = &stCtrlParam;
    MenuCmd_t *pCmdCfg = &stCmdCfg;
    CtrlInputChar*    pCtrl  = NULL;
    bool bIsOk = false;

    bIsOk = m_stMenuData.getCmdCfg(pNode->iCmdExecId, pCmdCfg);
    TRACEDEBUG( "WdgFCfgGroup::appendShowCmd bIsOk=%d", bIsOk);
    if(! bIsOk)
    {
        return;
    }

    int iNodeId = pNode->iNodeId;
    QString sName = QString(pCmdCfg->cSigName) + ":";

    // item name
    pCtrl = (CtrlInputChar*)(ui->tableWidget->cellWidget(m_nRows, 0));
    //pCtrl->setFocusPolicy(Qt::NoFocus);
    pCtrlParam->ipt = IPT_LABEL_READONLY;
    pCtrlParam->strInit = sName;
    pCtrl->setParam(pCtrlParam);
    pNode->iNodeId = MENUNODE_ID_INVALID;
    pNode->iNodeType = MENUNODE_INVALID;
    pCtrl->setMenuNode(pNode);
    m_nRows++;
    // input widget
    // SET_INFO and MenuCmd_t are the same struct
    makeCtrlparamBySigtype(pCtrlParam, (SET_INFO *)pCmdCfg);
    pCtrl = (CtrlInputChar*)(ui->tableWidget->cellWidget(m_nRows, 0));
    pCtrl->setParam(pCtrlParam);
    pNode->iNodeId = iNodeId;
    pNode->iNodeType = MENUNODE_CMD;
    pNode->iEquipID  = pCmdCfg->iEquipID;
    pNode->iSigID    = pCmdCfg->iSigID;
    pCtrl->setMenuNode(pNode);
    m_nRows++;

    TRACEDEBUG( "WdgFCfgGroup::appendShowCmd m_nRows=%d, name=%s, iNodeId=%d, iDispOrder=%d, iCmdExecId=%d iEquipID<%d> iSigID<%d>",
               m_nRows, pCmdCfg->cSigName, pNode->iNodeId, pNode->iDispOrder, pNode->iCmdExecId, pNode->iEquipID, pNode->iSigID );
}

void WdgFCfgGroup::appendShowSig(SET_INFO *pInfoItem, int iSigIndexId)
{
    if(pInfoItem->iEquipID == EQUIPID_SPECIAL)
    {
        appendShowSpecialSig(pInfoItem, iSigIndexId);
    }
    else
    {
        appendShowNormalSig(pInfoItem, iSigIndexId);
    }
}

void WdgFCfgGroup::appendShowSpecialSig(SET_INFO *pInfoItem, int iSigIndexId)
{
    TRACEDEBUG( "WdgFCfgGroup::appendShowSpecialSig pInfoItem->iSigID<%d>", pInfoItem->iSigID );
    if (LCD_ROTATION_90DEG != g_cfgParam->ms_initParam.lcdRotation)
    {
        if (pInfoItem->iSigID>=SIGID_SPECIAL_IPV6_IP_1_V &&
                pInfoItem->iSigID<=SIGID_SPECIAL_IPV6_GATEWAY_3_V)
        {
            TRACEDEBUG( "WdgFCfgGroup::appendShowSpecialSig LCD_ROTATION_0DEG return" );
            return;
        }
    }
    else
    {
        if (pInfoItem->iSigID==SIGID_SPECIAL_IPV6_IP_1 ||
              pInfoItem->iSigID==SIGID_SPECIAL_IPV6_IP_2 ||
              pInfoItem->iSigID==SIGID_SPECIAL_IPV6_GATEWAY_1 ||
              pInfoItem->iSigID==SIGID_SPECIAL_IPV6_GATEWAY_2)
        {
            TRACEDEBUG( "WdgFCfgGroup::appendShowSpecialSig LCD_ROTATION_90DEG return" );
            return;
        }
    }

    MenuCmd_t stCmdCfg;
    CtrlInputParam stCtrlParam;
    CtrlInputParam *pCtrlParam = &stCtrlParam;
    MenuCmd_t *pCmdCfg = &stCmdCfg;
    MenuNode_t stNode;
    MenuNode_t *pNode = &stNode;
    CtrlInputChar*    pCtrl  = NULL;
    bool bIsOk = false;

    bIsOk = m_stMenuData.getSpecSigCfg(pInfoItem->iSigID, pCmdCfg);
    TRACEDEBUG( "WdgFCfgGroup::appendShowSpecialSig bIsOk=%d", bIsOk);
    if(! bIsOk)
    {
        return;
    }
    //copy valid data from app
    pCmdCfg->iEquipID = pInfoItem->iEquipID;
    pCmdCfg->iSigID   = pInfoItem->iSigID;
    memcpy(&(pCmdCfg->vSigValue),
           &(pInfoItem->vSigValue),
           sizeof(pInfoItem->vSigValue));

    if (SIGID_SPECIAL_IPV6_PREFIX == pInfoItem->iSigID)
    {
        pCmdCfg->vDnLimit.ulValue = pInfoItem->vDnLimit.ulValue;
        pCmdCfg->vUpLimit.ulValue = pInfoItem->vUpLimit.ulValue;
    }
    TRACEDEBUG( "WdgFCfgGroup::appendShowSpecialSig sigName<%s>", pCmdCfg->cSigName );

    QString sName = QString(pCmdCfg->cSigName) + ":";

    // sig name
    if ( pInfoItem->iSigID!=SIGID_SPECIAL_IPV6_IP_2 &&
         pInfoItem->iSigID!=SIGID_SPECIAL_IPV6_GATEWAY_2 &&
         pInfoItem->iSigID!=SIGID_SPECIAL_IPV6_IP_2_V &&
         pInfoItem->iSigID!=SIGID_SPECIAL_IPV6_IP_3_V &&
         pInfoItem->iSigID!=SIGID_SPECIAL_IPV6_GATEWAY_2_V &&
         pInfoItem->iSigID!=SIGID_SPECIAL_IPV6_GATEWAY_3_V
         )
    {
        pCtrl = (CtrlInputChar*)(ui->tableWidget->cellWidget(m_nRows, 0));
        //pCtrl->setFocusPolicy(Qt::NoFocus);
        pCtrlParam->ipt = IPT_LABEL_READONLY;
        pCtrlParam->strInit = sName;
        pNode->iNodeId = MENUNODE_ID_INVALID;
        pNode->iNodeType = MENUNODE_INVALID;
        pCtrl->setParam( pCtrlParam );
        pCtrl->setMenuNode( pNode );
        m_nRows++;
    }

    TRACEDEBUG( "WdgFCfgGroup::appendShowSpecialSig 2" );
    // sig value
    // input widget
    // SET_INFO and MenuCmd_t are the same struct
    makeCtrlparamBySigtype(pCtrlParam, (SET_INFO *)pCmdCfg);
    pCtrl = (CtrlInputChar*)(ui->tableWidget->cellWidget(m_nRows, 0));
    pNode->iNodeId = MENUNODE_ID_INVALID;
    pNode->iNodeType = MENUNODE_SPECIAL_SIG;
    pNode->iSigIndexId = iSigIndexId;
    pNode->iEquipID  = pCmdCfg->iEquipID;
    pNode->iSigID    = pCmdCfg->iSigID;

    static bool bDHCP   = false;
    static bool bDHCPV6 = false;
    switch ( pCmdCfg->iSigID )
    {
    case SIGID_SPECIAL_DATE:
        pCtrlParam->ipt = IPT_DATE;
        break;

    case SIGID_SPECIAL_TIME:
        pCtrlParam->ipt = IPT_TIME;
        break;

    case SIGID_SPECIAL_IP:
    case SIGID_SPECIAL_MASK:
    case SIGID_SPECIAL_gateway:
    {
        TRACEDEBUG( "WdgFCfgGroup::appendShowSpecialSig IP bDHCP<%d>", bDHCP );
        pCtrlParam->ipt = IPT_IP;
        if ( bDHCP )
        {
            pNode->bReadOnly = true;
        }
        else
        {
            pNode->bReadOnly = false;
        }
    }
        break;

    case SIGID_SPECIAL_DHCP:
    {
        // Disabled Enabled Error
        bDHCP = (pCmdCfg->vSigValue.ulValue != 0);
        TRACEDEBUG( "WdgFCfgGroup::appendShowSpecialSig DHCP<%d>", bDHCP );
    }
        break;

    case SIGID_SPECIAL_IPV6_IP_1:
    {
        pCtrlParam->ipt = IPT_IPV6_1;
        makeIPV6Node(pCtrlParam, pNode, pInfoItem, bDHCPV6);
    }
        break;

    case SIGID_SPECIAL_IPV6_IP_2:
    {
        pCtrlParam->ipt = IPT_IPV6_2;
        makeIPV6Node(pCtrlParam, pNode, pInfoItem, bDHCPV6);
    }
        break;

    case SIGID_SPECIAL_IPV6_PREFIX:
    {
        if ( bDHCPV6 )
        {
            pNode->bReadOnly = true;
        }
        else
        {
            pNode->bReadOnly = false;
        }
    }
        break;

    case SIGID_SPECIAL_IPV6_GATEWAY_1:
    {
        pCtrlParam->ipt = IPT_IPV6_1;
        makeIPV6Node(pCtrlParam, pNode, pInfoItem, bDHCPV6);
    }
        break;

    case SIGID_SPECIAL_IPV6_GATEWAY_2:
    {
        pCtrlParam->ipt = IPT_IPV6_2;
        makeIPV6Node(pCtrlParam, pNode, pInfoItem, bDHCPV6);
    }
        break;

    case SIGID_SPECIAL_IPV6_DHCP:
    {
        // Disabled Enabled Error
        bDHCPV6 = (pCmdCfg->vSigValue.ulValue != 0);
        TRACEDEBUG( "WdgFCfgGroup::appendShowSpecialSig IPV6 value<%d> bDHCPV6<%d>", pCmdCfg->vSigValue.ulValue, bDHCPV6 );
    }
        break;

    case SIGID_SPECIAL_IPV6_IP_1_V:
    {
        pCtrlParam->ipt = IPT_IPV6_1_V;
        makeIPV6Node(pCtrlParam, pNode, pInfoItem, bDHCPV6);
    }
        break;

    case SIGID_SPECIAL_IPV6_IP_2_V:
    {
        pCtrlParam->ipt = IPT_IPV6_2_V;
        makeIPV6Node(pCtrlParam, pNode, pInfoItem, bDHCPV6);
    }
        break;

    case SIGID_SPECIAL_IPV6_IP_3_V:
    {
        pCtrlParam->ipt = IPT_IPV6_3_V;
        makeIPV6Node(pCtrlParam, pNode, pInfoItem, bDHCPV6);
    }
        break;

    case SIGID_SPECIAL_IPV6_GATEWAY_1_V:
    {
        pCtrlParam->ipt = IPT_IPV6_1_V;
        makeIPV6Node(pCtrlParam, pNode, pInfoItem, bDHCPV6);
    }
        break;

    case SIGID_SPECIAL_IPV6_GATEWAY_2_V:
    {
        pCtrlParam->ipt = IPT_IPV6_2_V;
        makeIPV6Node(pCtrlParam, pNode, pInfoItem, bDHCPV6);
    }
        break;

    case SIGID_SPECIAL_IPV6_GATEWAY_3_V:
    {
        pCtrlParam->ipt = IPT_IPV6_3_V;
        makeIPV6Node(pCtrlParam, pNode, pInfoItem, bDHCPV6);
    }
        break;

    default:
        break;
    }
    pCtrl->setParam( pCtrlParam );
    pCtrl->setMenuNode( pNode );
    m_nRows++;

    TRACEDEBUG( "WdgFCfgGroup::appendShowSpecialSig m_nRows=%d, name=%s, iNodeId=%d, iDispOrder=%d, iSigIndexId=%d iEquipID<%d> iSigID<%d>",
                m_nRows, pCmdCfg->cSigName, pNode->iNodeId, pNode->iDispOrder, pNode->iSigIndexId, pNode->iEquipID, pNode->iSigID );
}

void WdgFCfgGroup::makeIPV6Node(CtrlInputParam *pCtrlParam,
                                MenuNode_t *pNode,
                                SET_INFO *pInfoItem,
                                bool bDHCPV6)
{
    int idxStartIPSegment = 0;
    switch (pCtrlParam->ipt)
    {
    case IPT_IPV6_1:
        idxStartIPSegment = 0;
        break;

    case IPT_IPV6_2:
        idxStartIPSegment = 4;
        break;

    case IPT_IPV6_2_V:
        idxStartIPSegment = 3;
        break;

    case IPT_IPV6_3_V:
        idxStartIPSegment = 6;
        break;

    default:
        break;
    }
    int idx = idxStartIPSegment;
    TRACEDEBUG( "WdgFCfgGroup::makeIPV6Node idx<%d> bDHCPV6<%d>", idx, bDHCPV6 );
    pCtrlParam->usInit[idx-idxStartIPSegment] =
            MAKE_WORD( pInfoItem->cSigName+idx*2 );
    ++idx;
    pCtrlParam->usInit[idx-idxStartIPSegment] =
            MAKE_WORD( pInfoItem->cSigName+idx*2 );
    if (IPT_IPV6_3_V != pCtrlParam->ipt)
    {
        ++idx;
        pCtrlParam->usInit[idx-idxStartIPSegment] =
                MAKE_WORD( pInfoItem->cSigName+idx*2 );
        if (IPT_IPV6_1==pCtrlParam->ipt ||
                IPT_IPV6_2==pCtrlParam->ipt)
        {
            ++idx;
            pCtrlParam->usInit[idx-idxStartIPSegment] =
                    MAKE_WORD( pInfoItem->cSigName+idx*2 );
        }
    }

    TRACEDEBUG( "WdgFCfgGroup::makeIPV6Node <%04X:%04X:%04X:%04X> <%02X%02X %02X%02X %02X%02X %02X%02X %02X%02X %02X%02X %02X%02X %02X%02X>",
                pCtrlParam->usInit[0],
                pCtrlParam->usInit[1],
                pCtrlParam->usInit[2],
                pCtrlParam->usInit[3],
                pInfoItem->cSigName[0],
                pInfoItem->cSigName[1],
                pInfoItem->cSigName[2],
                pInfoItem->cSigName[3],
                pInfoItem->cSigName[4],
                pInfoItem->cSigName[5],
                pInfoItem->cSigName[6],
                pInfoItem->cSigName[7],
                pInfoItem->cSigName[8],
                pInfoItem->cSigName[9],
                pInfoItem->cSigName[10],
                pInfoItem->cSigName[11],
                pInfoItem->cSigName[12],
                pInfoItem->cSigName[13],
                pInfoItem->cSigName[14],
                pInfoItem->cSigName[15]
                );

    switch (pInfoItem->iSigID)
    {
    case SIGID_SPECIAL_IPV6_IP_1:
    {
        m_strIPV6_IP.sprintf( "%X:%X:%X:%X:",
                              pCtrlParam->usInit[0],
                              pCtrlParam->usInit[1],
                              pCtrlParam->usInit[2],
                              pCtrlParam->usInit[3] );
    }
        break;
    case SIGID_SPECIAL_IPV6_GATEWAY_1:
    {
        m_strIPV6_Gateway.sprintf( "%X:%X:%X:%X:",
                                   pCtrlParam->usInit[0],
                                   pCtrlParam->usInit[1],
                                   pCtrlParam->usInit[2],
                                   pCtrlParam->usInit[3] );
    }
        break;
        // V
    case SIGID_SPECIAL_IPV6_IP_1_V:
    {
        m_strIPV6_IP_V.sprintf( "%X:%X:%X:",
                                pCtrlParam->usInit[0],
                                pCtrlParam->usInit[1],
                                pCtrlParam->usInit[2] );
    }
        break;
    case SIGID_SPECIAL_IPV6_IP_2_V:
    {
        m_strIPV6_IP_V_2.sprintf( "%X:%X:%X:",
                                  pCtrlParam->usInit[0],
                                  pCtrlParam->usInit[1],
                                  pCtrlParam->usInit[2] );
    }
        break;
    case SIGID_SPECIAL_IPV6_GATEWAY_1_V:
    {
        m_strIPV6_Gateway_V.sprintf( "%X:%X:%X:",
                                     pCtrlParam->usInit[0],
                                     pCtrlParam->usInit[1],
                                     pCtrlParam->usInit[2] );
    }
        break;
    case SIGID_SPECIAL_IPV6_GATEWAY_2_V:
    {
        m_strIPV6_Gateway_V_2.sprintf( "%X:%X:%X:",
                                       pCtrlParam->usInit[0],
                                       pCtrlParam->usInit[1],
                                       pCtrlParam->usInit[2] );
    }
        break;

    default:
        break;
    }

    if ( bDHCPV6 )
    {
        pNode->bReadOnly = true;
    }
    else
    {
        pNode->bReadOnly = false;
    }
}

void WdgFCfgGroup::appendShowNormalSig(SET_INFO *pInfoItem, int iSigIndexId)
{
    CtrlInputParam stCtrlParam;
    MenuNode_t stNode;
    MenuNode_t *pNode = &stNode;
    CtrlInputParam *pCtrlParam = &stCtrlParam;
    CtrlInputChar*    pCtrl  = NULL;

    memset(pNode, 0, sizeof(MenuNode_t));
    QString sName = QString(pInfoItem->cSigName) + ":";

    // item name
    pCtrl = (CtrlInputChar*)(ui->tableWidget->cellWidget(m_nRows, 0));
    //pCtrl->setFocusPolicy(Qt::NoFocus);
    pCtrlParam->ipt = IPT_LABEL_READONLY;
    pCtrlParam->strInit = sName;
    pCtrl->setParam(pCtrlParam);
    pNode->iNodeId = MENUNODE_ID_INVALID;
    pNode->iNodeType = MENUNODE_INVALID;
    pCtrl->setMenuNode(pNode);
    m_nRows++;
    TRACEDEBUG("SigName:%s",qPrintable(sName));

    // input widget
    makeCtrlparamBySigtype(pCtrlParam, pInfoItem);
    pCtrl = (CtrlInputChar*)(ui->tableWidget->cellWidget(m_nRows, 0));
    pCtrl->setParam(pCtrlParam);
    pNode->iNodeId = MENUNODE_ID_INVALID;
    pNode->iNodeType = MENUNODE_SIG;
    pNode->iSigIndexId = iSigIndexId;
    pNode->iEquipID  = pInfoItem->iEquipID;
    pNode->iSigID    = pInfoItem->iSigID;
    pCtrl->setMenuNode(pNode);
    m_nRows++;

    TRACEDEBUG( "WdgFCfgGroup::appendShowNormalSig m_nRows=%d, name=%s, iNodeId=%d, iSigIndexId=%d iEquipID<%d> iSigID<%d>",
               m_nRows, pInfoItem->cSigName, pNode->iNodeId, pNode->iSigIndexId, pNode->iEquipID, pNode->iSigID );
}

void WdgFCfgGroup::selectMenuNode(int iSelectType, int iSelectId)
{
    const char sSelTypeTab[][20] = {
        "SELECT_BY_ID",
        "SELECT_FIRST",
        "SELECT_LAST",
        "SELECT_PREV",
        "SELECT_NEXT",
    };
    TRACEDEBUG( "WdgFCfgGroup::selectMenuNode iSelectType=%s, iSelectId=%d",
               sSelTypeTab[iSelectType], iSelectId);

    int iValidRow = MENUNODE_ID_INVALID;
    switch(iSelectType)
    {
        case SELECT_BY_ID:
        {
            iValidRow = validNodeRowById(iSelectId);
            break;
        }
        case SELECT_FIRST:
        {
            iValidRow = firstValidRow();
            break;
        }
        case SELECT_LAST:
        {
            iValidRow = lastValidRow();
            break;
        }
        case SELECT_PREV:
        {
            iValidRow = prevValidRow();
            break;
        }
        case SELECT_NEXT:
        {
            iValidRow = nextValidRow();
            break;
        }
        default:
        {
            break;
        }
    }
    selectValidRow(iValidRow);
}


void WdgFCfgGroup::selectValidRow(int iValidRow)
{
    TRACEDEBUG( "WdgFCfgGroup::selectValidRow row<%d>", iValidRow );
    if(iValidRow != CFG_GROUP_MENU_ROW_INVALID)
    {
        if(iValidRow < 2)
        {
            ui->tableWidget->setCurrentCell(0, 0);
        }
        else
        {
            ui->tableWidget->setCurrentCell(iValidRow - 2, 0);
        }
        ui->tableWidget->setCurrentCell(iValidRow, 0);
    }
}

void WdgFCfgGroup::initMenuMode(void)
{
    if(HomePageWindow::ms_homePageType == HOMEPAGE_TYPE_SLAVE)
    {
        m_iMenuNodeId = MENUNODE_ID_SLAVE_SETTINGS;
        m_iInitSelectedId = MENUNODE_INVALID;
    }
    else if(m_iMenuNodeId == MENUNODE_ID_BAT_SETTINGS)
    {
        PACK_SETINFO stPackSetInfo;

        //SCREEN_ID_Wdg2P9Cfg_BatSettingGroup
        int iBatGroupNum = 2;
        getCfgBatSettingGroup(&stPackSetInfo, &iBatGroupNum);
        setMenuModeBatSettingGroup(&iBatGroupNum);
        //other config below
    }
}

void WdgFCfgGroup::getCfgBatSettingGroup(PACK_SETINFO *pInfo, void *pCfg)
{
    void *pData = NULL;

    m_cmdItem.ScreenID = SCREEN_ID_Wdg2P9Cfg_BatSettingGroup;
    TRACEDEBUG( "WdgFCfgGroup::getCfgBatSettingGroup m_cmdItem.ScreenID=%x, m_cmdItem.CmdType=%d", m_cmdItem.ScreenID, m_cmdItem.CmdType);
    if ( data_getDataSync(&m_cmdItem, &pData, TIME_WAIT_SYNC) )
    {
        TRACEDEBUG( "WdgFCfgGroup::getCfgBatSettingGroup data_getDataSync ScreenID<%x> no data", m_cmdItem.ScreenID );
    }
    else if(pData != NULL)
    {
        memcpy( pInfo, pData, sizeof(PACK_SETINFO));

        int iIndex = 0;
        int *piBatGroupNum = (int *)pCfg;

        if(pInfo->SetNum >= 1)
        {
            *piBatGroupNum = pInfo->SettingInfo[iIndex].vSigValue.enumValue;
        }
    }
}

void WdgFCfgGroup::setMenuModeBatSettingGroup(void *pCfg)
{
    int iBatGroupNum = *((int *)pCfg);
    MenuNode_t *pNodeBat1 = NULL;
    MenuNode_t *pNodeBat2 = NULL;

    pNodeBat1 = m_stMenuData.getNode(MENUNODE_ID_BAT1_SETTINGS);
    pNodeBat2 = m_stMenuData.getNode(MENUNODE_ID_BAT2_SETTINGS);

    if((pNodeBat1 == NULL) || (pNodeBat2 == NULL) )
    {
        return;
    }

    pNodeBat1->bDispEnable = true;
    pNodeBat2->bDispEnable = true;

    if(iBatGroupNum == 0)
    {
        pNodeBat1->bDispEnable = false;
        pNodeBat2->bDispEnable = false;
    }
    else if(iBatGroupNum == 1)
    {
        pNodeBat1->bDispEnable = true;
        pNodeBat2->bDispEnable = false;
    }
}

void WdgFCfgGroup::DisplayValidNode(void)
{
    int i;

    for(i = 0; i < m_nRows; i++)
    {
        ui->tableWidget->showRow(i);
    }
    //hide these blank items
    for(i = m_nRows; i < m_nCtrlInputCreated; i++)
    {
        ui->tableWidget->hideRow(i);
    }
}

void WdgFCfgGroup::initSelectedMenuNode(void)
{
    if(m_iInitSelectedId == MENUNODE_ID_INVALID)
    {
        selectMenuNode(SELECT_FIRST);
        return;
    }
    selectMenuNode( SELECT_BY_ID, m_iInitSelectedId);
}

void WdgFCfgGroup::clearTxtInputTable(void)
{
    CtrlInputChar*    pCtrl  = NULL;
    int i;

    TRACEDEBUG( "WdgFCfgGroup::clearTxtInputTable m_nCtrlInputCreated=%d", m_nCtrlInputCreated);
    for(i = 0; i < m_nCtrlInputCreated; i++)
    {
        pCtrl = (CtrlInputChar*)(ui->tableWidget->cellWidget(i, 0));
        pCtrl->setCtrlVisible( false );
    }
}

void WdgFCfgGroup::updateScrollBar(void)
{
    int iCurrentRow = ui->tableWidget->currentRow() + 1;
    int iMaxRow = m_nRows - m_nInvalidRows;
    //TRACEDEBUG( "WdgFCfgGroup::updateScrollBar iCurrentRow=%d, iMaxRow=%d", iCurrentRow, iMaxRow);
    SET_STYLE_SCROOLBAR(iMaxRow, iCurrentRow);
}

int WdgFCfgGroup::firstValidRow(void)
{
    int i;
    MenuNode_t stMenuNode;
    CtrlInputChar* pCtrl = NULL;

    for(i = 0; i < m_nRows; i++)//select the first item
    {
        pCtrl = (CtrlInputChar* )ui->tableWidget->cellWidget(i, 0);
        pCtrl->getMenuNode(&stMenuNode);
        if(stMenuNode.iNodeType != MENUNODE_INVALID)
        {
            //TRACEDEBUG( "WdgFCfgGroup::firstValidRow i=%d", i);
            return i;
        }
    }

    return CFG_GROUP_MENU_ROW_INVALID;
}

int WdgFCfgGroup::lastValidRow(void)
{
    int i;
    MenuNode_t stMenuNode;
    CtrlInputChar* pCtrl = NULL;

    for(i = m_nRows -1; i >= 0; i--)//select the first item
    {
        pCtrl = (CtrlInputChar* )ui->tableWidget->cellWidget(i, 0);
        pCtrl->getMenuNode(&stMenuNode);
        if(stMenuNode.iNodeType != MENUNODE_INVALID)
        {
            //TRACEDEBUG( "WdgFCfgGroup::lastValidRow i=%d", i);
            return i;
        }
    }

    return CFG_GROUP_MENU_ROW_INVALID;
}

int WdgFCfgGroup::prevValidRow(void)
{
    int i;
    int iSelRow = ui->tableWidget->currentRow();
    MenuNode_t stMenuNode;
    CtrlInputChar* pCtrl = NULL;

    for(i = iSelRow; i >= 0; i--)
    {
        pCtrl = (CtrlInputChar* )ui->tableWidget->cellWidget(i, 0);
        pCtrl->getMenuNode(&stMenuNode);
        if(stMenuNode.iNodeType != MENUNODE_INVALID)
        {
            TRACEDEBUG( "WdgFCfgGroup::selectPrevNode i=%d", i);
            return i;
        }
    }

    for(i = iSelRow; i < m_nRows; i++)
    {
        pCtrl = (CtrlInputChar* )ui->tableWidget->cellWidget(i, 0);
        pCtrl->getMenuNode(&stMenuNode);
        if(stMenuNode.iNodeType != MENUNODE_INVALID)
        {
            TRACEDEBUG( "WdgFCfgGroup::selectNextNode i=%d", i);
            return i;
        }
    }

    return CFG_GROUP_MENU_ROW_INVALID;
}

int WdgFCfgGroup::nextValidRow(void)
{
    int i;
    int iSelRow = ui->tableWidget->currentRow();
    MenuNode_t stMenuNode;
    CtrlInputChar* pCtrl = NULL;

    for(i = iSelRow; i < m_nRows; i++)
    {
        pCtrl = (CtrlInputChar* )ui->tableWidget->cellWidget(i, 0);
        pCtrl->getMenuNode(&stMenuNode);
        if(stMenuNode.iNodeType != MENUNODE_INVALID)
        {
            TRACEDEBUG( "WdgFCfgGroup::selectNextNode i=%d", i);
            return i;
        }
    }

    for(i = iSelRow; i >= 0; i--)
    {
        pCtrl = (CtrlInputChar* )ui->tableWidget->cellWidget(i, 0);
        pCtrl->getMenuNode(&stMenuNode);
        if(stMenuNode.iNodeType != MENUNODE_INVALID)
        {
            TRACEDEBUG( "WdgFCfgGroup::selectPrevNode i=%d", i);
            return i;
        }
    }

    return CFG_GROUP_MENU_ROW_INVALID;
}

int WdgFCfgGroup::validNodeRowById(int iMenuNodeId)
{
    int i;
    MenuNode_t stMenuNode;
    CtrlInputChar* pCtrl = NULL;

    if(iMenuNodeId != MENUNODE_ID_INVALID)
    {
        for(i = 0; i < m_nRows; i++)//find selected item id
        {
            pCtrl = (CtrlInputChar* )ui->tableWidget->cellWidget(i, 0);
            pCtrl->getMenuNode(&stMenuNode);
            if(stMenuNode.iNodeId == iMenuNodeId)
            {
                TRACEDEBUG( "WdgFCfgGroup::selectNodeById i=%d", i);
                return i;
            }
        }
    }

    return firstValidRow();
}

void WdgFCfgGroup::sltTimerHandler(void)
{
    int iInterval = m_oTimer.interval();
    int iRequireInterval = TIMER_UPDATE_DATA_INTERVAL_SETTING;

    if(m_iMenuNodeId == MENUNODE_ID_COMM_SETTINGS)
    {
        iRequireInterval = 5000;
    }
    else if((m_iMenuNodeId == MENUNODE_ID_ROOT) ||
            (m_iMenuNodeId == MENUNODE_ID_BAT_SETTINGS))
    {
        iRequireInterval = 3000;
    }
    else if((m_iMenuNodeId == MENUNODE_ID_ENERGY_SAVING) ||
            (m_iMenuNodeId == MENUNODE_ID_RECT_SETTINGS))
    {
        iRequireInterval = 1000;
    }
    else if (m_iMenuNodeId == MENUNODE_ID_SYS_SETTINGS)
    {
        iRequireInterval = 500;
    }

    if(iInterval != iRequireInterval)
    {
        restartQTimerInterval(iRequireInterval);
    }

    Refresh();
}

void WdgFCfgGroup::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void WdgFCfgGroup::sltTableKeyPress(int key)
{
    //TRACEDEBUG( "WdgFCfgGroup::sltTableKeyPress m_iMenuNodeId<%d>", m_iMenuNodeId );
    g_timeLoginConfig.restart();
    g_timeElapsedKeyPress.restart();
/*     if (m_iMenuNodeId==MENUNODE_ID_ROOT ||
            m_iMenuNodeId==MENUNODE_ID_BAT_SETTINGS)
    {
        return;
    } */

    switch ( key )
    {
    case Qt::Key_Up:
    {
        int iSelRow = CFG_GROUP_MENU_ROW_INVALID;
        int iFirstValidRow = firstValidRow();
        if(m_nSelRowLast < iFirstValidRow)
        {
            iSelRow = lastValidRow();
        }
        else
        {
            iSelRow = prevValidRow();
        }
        //TRACEDEBUG( "WdgFCfgGroup::sltTableKeyPress m_nSelRowLast<%d> iFirstValidRow<%d> iSelRow<%d>", m_nSelRowLast, iFirstValidRow, iSelRow );
        selectValidRow(iSelRow);
    }
        break;

    case Qt::Key_Down:
    {
        int iSelRow = CFG_GROUP_MENU_ROW_INVALID;
        int iLastValidRow = lastValidRow();
        if(m_nSelRowLast > iLastValidRow)
        {
            iSelRow = firstValidRow();
        }
        else
        {
            iSelRow = nextValidRow();
        }
        //TRACEDEBUG( "WdgFCfgGroup::sltTableKeyPress m_nSelRowLast<%d> iLastValidRow<%d> iSelRow<%d>", m_nSelRowLast, iLastValidRow, iSelRow );
        selectValidRow(iSelRow);
    }
        break;

    default:
        break;
    }

//    m_nSelRowLast = ui->tableWidget->selectedRow();
//    updateScrollBar();
}

void WdgFCfgGroup::keyPressEvent(QKeyEvent* keyEvent)
{
    TRACEDEBUG( "WdgFCfgGroup::keyPressEvent keyEvent=%x", keyEvent->key());
    g_bSendCmd = true;
    switch ( keyEvent->key() )
    {
        case Qt::Key_Return:
        case Qt::Key_Enter:
        {
            m_oTimer.stop();//lock

            int iMenuNodeType = MENUNODE_INVALID;
            MenuNode_t stMenuNode;
            CtrlInputChar* pCtrl = NULL;
            int nSelRow = ui->tableWidget->selectedRow();
            if((nSelRow < 0) || (nSelRow >= m_nCtrlInputCreated))
            {
                break;
            }

            pCtrl = (CtrlInputChar*)(ui->tableWidget->cellWidget(nSelRow, 0));
            pCtrl->getMenuNode(&stMenuNode);
            iMenuNodeType = stMenuNode.iNodeType;
            TRACEDEBUG( "WdgFCfgGroup::keyPressEvent Key_Enter nSelRow<%d> iMenuNodeType=%d", nSelRow, iMenuNodeType );
            if(iMenuNodeType == MENUNODE_DIR)
            {
                int iMenuNodeId = stMenuNode.iNodeId;
                int iSelectedNodeId = MENUNODE_ID_INVALID;
                updateMenu(iMenuNodeId, iSelectedNodeId);
            }
            else if( (iMenuNodeType == MENUNODE_CMD)||
                     (iMenuNodeType == MENUNODE_SIG) )
            {
                int nEquipID = stMenuNode.iEquipID;
                int nSigID   = stMenuNode.iSigID;
                TRACEDEBUG( "WdgFCfgGroup::keyPressEvent Key_Enter 1 EquipID<%d> SigID<%d>", nEquipID, nSigID );
                // LCD rotation items
                if (nEquipID==1 && nSigID==186)
                {
                    CtrlInputParam* pParam = pCtrl->getParam();
                    switch (g_cfgParam->ms_initParam.lcdRotation)
                    {
                    case LCD_ROTATION_0DEG:
                    case LCD_ROTATION_90DEG:
                    {
                        pParam->nMax -= 1;
                        pCtrl->setParam( pParam );
                    }
                        break;

                    case LCD_ROTATION_BIG:
                    {
                        pParam->nMin += 2;
                        pCtrl->setParam( pParam );
                    }
                        break;

                    default:
                        break;
                    }
                }
                pCtrl->Enter();
            }
            else if (iMenuNodeType == MENUNODE_SPECIAL_SIG)
            {
                int nEquipID = stMenuNode.iEquipID;
                int nSigID   = stMenuNode.iSigID;
                TRACEDEBUG( "WdgFCfgGroup::keyPressEvent Key_Enter EquipID<%d> SigID<%d>", nEquipID, nSigID );
                // DHCP set item("Error") disapear
                if (nEquipID == EQUIPID_SPECIAL)
                {
                    switch ( nSigID )
                    {
                    case SIGID_SPECIAL_DHCP:
                    case SIGID_SPECIAL_IPV6_DHCP:
                    {
                        CtrlInputParam* pParam = pCtrl->getParam();
                        pParam->nMax -= 1;
                        pCtrl->setParam( pParam );
                        pCtrl->Enter();
                    }
                        break;

                    case SIGID_SPECIAL_IP:
                    case SIGID_SPECIAL_MASK:
                    case SIGID_SPECIAL_gateway:
                    case SIGID_SPECIAL_IPV6_IP_1:
                    case SIGID_SPECIAL_IPV6_IP_2:
                    case SIGID_SPECIAL_IPV6_PREFIX:
                    case SIGID_SPECIAL_IPV6_IP_1_V:
                    case SIGID_SPECIAL_IPV6_IP_2_V:
                    case SIGID_SPECIAL_IPV6_IP_3_V:
                    {
                        TRACEDEBUG( "WdgFCfgGroup::keyPressEvent Key_Enter ReadOnly<%d>", stMenuNode.bReadOnly );
                        if ( !stMenuNode.bReadOnly )
                        {
                            pCtrl->Enter();
                        }
                    }
                        break;

                    default:
                        pCtrl->Enter();
                        break;
                    }
                }
            }
            break;
        }
        case Qt::Key_Escape:
        {
            int iMenuNodeId = m_stMenuData.getFatherNodeId(m_iMenuNodeId);
            int iSelectedNodeId = m_iMenuNodeId;
            updateMenu(iMenuNodeId, iSelectedNodeId);
            break;
        }
        case Qt::Key_Up:
        {
            selectMenuNode( SELECT_LAST );
            updateScrollBar();
            break;
        }
        case Qt::Key_Down:
        {
            selectMenuNode( SELECT_FIRST );
            updateScrollBar();
            break;
        }
        default:
        {
            break;
        }
    }

    return;// QWidget::keyPressEvent(keyEvent);
}


void WdgFCfgGroup::updateMenu(int iMenuNodeId, int iSelectedNodeId)
{
    TRACEDEBUG( "WdgFCfgGroup::updateMenu iMenuNodeId=%d, iSelectedNodeId=%d",
               iMenuNodeId, iSelectedNodeId);

    if(HomePageWindow::ms_homePageType == HOMEPAGE_TYPE_SLAVE)
    {
        emit goToHomePage();
    }
    else if(iMenuNodeId == MENUNODE_ID_INVALID)
    {
        emit goToHomePage();
    }
    else
    {
        m_iMenuNodeId = iMenuNodeId;
        m_iInitSelectedId = iSelectedNodeId;
        Enter(NULL);
    }
}

void WdgFCfgGroup::Destroy()
{
    m_oTimer.stop();

    if(m_timerIdSpawn != 0)
    {
        killTimer (m_timerIdSpawn);
    }

    int nRowNum = ui->tableWidget->rowCount();

    for(int i = 10; i < nRowNum; i ++)
    {
        CtrlInputChar *pCtrl = (CtrlInputChar*)(ui->tableWidget->cellWidget(i,0));
        delete pCtrl;
        ui->tableWidget->removeCellWidget(i,0);
    }

    m_nRows = 0;
    m_nCtrlInputCreated = 10;

    TRACEDEBUG("WdgFCfgGroup::updateMenu # delete CtrlInputChar");
}

bool WdgFCfgGroup::createInputWidget(int iSpawn)
{
    CtrlInputChar*    pCtrl  = NULL;
    CtrlInputParam ctrlParam;
    bool iIsOK = false;

    TRACEDEBUG( "WdgFCfgGroup::createInputWidget m_nCtrlInputCreated=%d, iSpawn=%d", m_nCtrlInputCreated, iSpawn);

    if (m_nCtrlInputCreated < m_nCtrlInputMax)
    {        
        for(int i = 0; i<iSpawn; i++)
        {
            ui->tableWidget->insertRow( m_nCtrlInputCreated + i );//insert a new row
            pCtrl = new CtrlInputChar( this, &ctrlParam );
            connect( pCtrl,
                     SIGNAL(escapeMe(enum INPUT_TYPE)),
                     this,
                     SLOT(FocusTableWdg(enum INPUT_TYPE)) );
            ui->tableWidget->setCellWidget(m_nCtrlInputCreated + i, 0, pCtrl);
            ui->tableWidget->setRowHeight(m_nCtrlInputCreated + i, TABLEWDG_ROW_HEIGHT);
        }
        m_nCtrlInputCreated += iSpawn;

        iIsOK = true;
    }

    return iIsOK;
}

// submit data
void WdgFCfgGroup::FocusTableWdg(enum INPUT_TYPE ipt)
{
    int nSelRow = ui->tableWidget->selectedRow();
    CtrlInputChar* pCtrl = NULL;
    TRACEDEBUG( "WdgFCfgGroup::FocusTableWdg nSelRow=%d", nSelRow);
    if((nSelRow < 0) || (nSelRow >= m_nCtrlInputCreated))
    {
        return;
    }

    ui->tableWidget->setFocus();
    pCtrl = (CtrlInputChar*)(ui->tableWidget->cellWidget(nSelRow, 0));
    TRACEDEBUG( "WdgFCfgGroup::FocusTableWdg IPT_SUBMIT=%d, ipt=%d",
                IPT_SUBMIT, ipt);
    if (IPT_SUBMIT==ipt &&
            m_timeElapsedKeyPress.elapsed()>1000)
    {
        TRACEDEBUG( "WdgFCfgGroup::FocusTableWdg submit data" );
        m_oTimer.stop();
        m_timeElapsedKeyPress.restart();

        MenuNode_t stMenuNode;
        pCtrl->getMenuNode(&stMenuNode);
        int iNodeType = stMenuNode.iNodeType;
        TRACEDEBUG( "WdgFCfgGroup::FocusTableWdg iNodeType=%d", iNodeType);
        if(iNodeType == MENUNODE_CMD)
        {
            procMenuCmd(&stMenuNode, pCtrl);
            return;
        }

        if(iNodeType == MENUNODE_SIG)
        {
            procMenuSig(&stMenuNode, pCtrl);
        }
        else if(iNodeType == MENUNODE_SPECIAL_SIG)
        {
            int iEquipID = stMenuNode.iEquipID;
            int iSigID   = stMenuNode.iSigID;
            if (iEquipID == EQUIPID_SPECIAL)
            {
                switch (iSigID)
                {
                // ipv6
                case SIGID_SPECIAL_IPV6_IP_1:
                {
                    m_strIPV6_IP = pCtrl->getValue().strVal;
                    ui->tableWidget->selectRow( nSelRow+1 );
                    CtrlInputChar* pCtrl =
                            (CtrlInputChar*)(ui->tableWidget->cellWidget(nSelRow+1, 0));
                    pCtrl->Enter();
                    TRACEDEBUG( "WdgFCfgGroup::FocusTableWdg ipv6_1<%s>", m_strIPV6_IP.toUtf8().constData() );
                }
                    // 第一段IPV6不需要提交
                    return;

                case SIGID_SPECIAL_IPV6_IP_2:
                {
                    m_strIPV6_IP += pCtrl->getValue().strVal;
                    pCtrl->setIPV6( m_strIPV6_IP );
                    TRACEDEBUG( "WdgFCfgGroup::FocusTableWdg strIPV6<%s>", m_strIPV6_IP.toUtf8().constData() );
                }
                    break;
                    // gateway

                case SIGID_SPECIAL_IPV6_GATEWAY_1:
                {
                    m_strIPV6_Gateway = pCtrl->getValue().strVal;
                    ui->tableWidget->selectRow( nSelRow+1 );
                    CtrlInputChar* pCtrl =
                            (CtrlInputChar*)(ui->tableWidget->cellWidget(nSelRow+1, 0));
                    pCtrl->Enter();
                    TRACEDEBUG( "WdgFCfgGroup::FocusTableWdg ipv6_1<%s>", m_strIPV6_Gateway.toUtf8().constData() );
                }
                    return;

                case SIGID_SPECIAL_IPV6_GATEWAY_2:
                {
                    m_strIPV6_Gateway += pCtrl->getValue().strVal;
                    pCtrl->setIPV6( m_strIPV6_Gateway );
                    TRACEDEBUG( "WdgFCfgGroup::FocusTableWdg strIPV6<%s>", m_strIPV6_Gateway.toUtf8().constData() );
                }
                    break;
                    // IPV6 V
                case SIGID_SPECIAL_IPV6_IP_1_V:
                {
                    m_strIPV6_IP_V = pCtrl->getValue().strVal;
                    ui->tableWidget->selectRow( nSelRow+1 );
                    CtrlInputChar* pCtrl =
                            (CtrlInputChar*)(ui->tableWidget->cellWidget(nSelRow+1, 0));
                    pCtrl->Enter();
                    TRACEDEBUG( "WdgFCfgGroup::FocusTableWdg ipv6_1_V<%s>", m_strIPV6_IP_V.toUtf8().constData() );
                }
                    return;
                case SIGID_SPECIAL_IPV6_IP_2_V:
                {
                    m_strIPV6_IP_V_2 = pCtrl->getValue().strVal;
                    ui->tableWidget->selectRow( nSelRow+1 );
                    CtrlInputChar* pCtrl =
                            (CtrlInputChar*)(ui->tableWidget->cellWidget(nSelRow+1, 0));
                    pCtrl->Enter();
                    TRACEDEBUG( "WdgFCfgGroup::FocusTableWdg ipv6_2_V<%s>", m_strIPV6_IP_V_2.toUtf8().constData() );
                }
                    return;
                case SIGID_SPECIAL_IPV6_IP_3_V:
                {
                    m_strIPV6_IP_V = m_strIPV6_IP_V+
                            m_strIPV6_IP_V_2+
                            pCtrl->getValue().strVal;
                    pCtrl->setIPV6( m_strIPV6_IP_V );
                    TRACEDEBUG( "WdgFCfgGroup::FocusTableWdg strIPV6_3_V<%s>", m_strIPV6_IP_V.toUtf8().constData() );
                }
                    break;
                    // gatewayV
                case SIGID_SPECIAL_IPV6_GATEWAY_1_V:
                {
                    m_strIPV6_Gateway_V = pCtrl->getValue().strVal;
                    ui->tableWidget->selectRow( nSelRow+1 );
                    CtrlInputChar* pCtrl =
                            (CtrlInputChar*)(ui->tableWidget->cellWidget(nSelRow+1, 0));
                    pCtrl->Enter();
                    TRACEDEBUG( "WdgFCfgGroup::FocusTableWdg ipv6_1_V<%s>", m_strIPV6_Gateway_V.toUtf8().constData() );
                }
                    return;
                case SIGID_SPECIAL_IPV6_GATEWAY_2_V:
                {
                    m_strIPV6_Gateway_V_2 = pCtrl->getValue().strVal;
                    ui->tableWidget->selectRow( nSelRow+1 );
                    CtrlInputChar* pCtrl =
                            (CtrlInputChar*)(ui->tableWidget->cellWidget(nSelRow+1, 0));
                    pCtrl->Enter();
                    TRACEDEBUG( "WdgFCfgGroup::FocusTableWdg ipv6_2_V<%s>", m_strIPV6_Gateway_V_2.toUtf8().constData() );
                }
                    return;
                case SIGID_SPECIAL_IPV6_GATEWAY_3_V:
                {
                    m_strIPV6_Gateway_V = m_strIPV6_Gateway_V+
                            m_strIPV6_Gateway_V_2+
                            pCtrl->getValue().strVal;
                    pCtrl->setIPV6( m_strIPV6_Gateway_V );
                    TRACEDEBUG( "WdgFCfgGroup::FocusTableWdg strIPV6_V<%s>", m_strIPV6_Gateway_V.toUtf8().constData() );
                }
                    break;

                default:
                    break;
                }
            }
            procMenuSig(&stMenuNode, pCtrl);
        }
    }
    else
    {
        TRACEDEBUG( "WdgFCfgGroup::FocusTableWdg refresh data only" );
    }
    m_timeElapsedKeyPress.restart();
    RefreshNow();
#ifdef DEBUG_MENU
    return;
#endif
    restartQTimer();
}

void WdgFCfgGroup::restartQTimerInterval(int iInterval)
{
    if ( m_oTimer.isActive() )
    {
        m_oTimer.stop();
    }
    m_oTimer.start(iInterval);
    TRACEDEBUG( "WdgFCfgGroup::restartQTimerInterval");
}


void WdgFCfgGroup::restartQTimer(void)
{
#ifdef DEBUG_MENU
    return;
#endif

    if ( m_oTimer.isActive() )
    {
        m_oTimer.stop();
    }
    g_bSendCmd    = true;
    m_oTimer.start(TIMER_UPDATE_DATA_INTERVAL_SETTING);
    TRACEDEBUG( "WdgFCfgGroup::restartQTimer");
}

void WdgFCfgGroup::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerIdSpawn)
    {
        if ( !createInputWidget(1) )
        {
            killTimer( m_timerIdSpawn );
            m_timerIdSpawn = 0;
        }
    }
}

int WdgFCfgGroup::procMenuSig(MenuNode_t */*pMenuNode*/, CtrlInputChar* pCtrl)
{
    MenuNode_t stMenuNode;
    int iRetVal = 0;
//    CtrlInputParam ctrlParamSetting = *(pCtrl->getParam());
//    pCtrl->getMenuNode(&stMenuNode);
//    iRetVal = submitAppParamData(pCtrl,
//                                 &m_cmdItem,
//                                 stMenuNode.iSigIndexId,
//                                 &ctrlParamSetting);
    pCtrl->getMenuNode(&stMenuNode);
    iRetVal = submitAppParamData(pCtrl,
                                 &m_cmdItem,
                                 stMenuNode.iSigIndexId,
                                 pCtrl->getParam()
                                 );
    switch ( iRetVal )
    {
    case SET_PARAM_ERR_OK_ECO:
        // ECO
        TRACEDEBUG( "WdgFCfgGroup::procMenuSig ECO");
        mainSleep( 3000 );
        break;

    case SET_PARAM_ERR_OK_LANG:
        // change language
        TRACEDEBUG( "WdgFCfgGroup::procMenuSig language");
        mainSleep( 1000 );
        break;
    }

    return iRetVal;
}

// return 0 submit custom data  else other
#define SUBMIT_CUSTOM_TABLE_ROW_DONE \
    int iSelRow = ui->tableWidget->currentRow();    \
    ui->tableWidget->setFocus();    \
    ui->tableWidget->selectRow(iSelRow);    \
    restartQTimer();

int WdgFCfgGroup::procMenuCmd(MenuNode_t *pMenuNode, CtrlInputChar* pCtrl)
{
    int iMenuNodeCmdId = pMenuNode->iCmdExecId;
    int iRet = 0;

    TRACEDEBUG( "WdgFCfgGroup::procMenuCmd iMenuNodeCmdId=%d", iMenuNodeCmdId);
    switch (iMenuNodeCmdId)
    {
        case MENUCMD_ID_SYS_RESET:
        {
            iRet = procMenuCmd_SysReset(pCtrl);
            break;
        }
        case MENUCMD_ID_APP_UPDATE:
        {
            iRet = procMenuCmd_AppUpdate(pCtrl);
            break;
        }
//        case MENUCMD_ID_DISP_MODE:
//        {
//            break;
//        }
        case MENUCMD_ID_GUIDE_DISP:
        {
            iRet = procMenuCmd_GuideDisp(pCtrl);
            break;
        }
        case MENUCMD_ID_GUIDE_SET:
        {
            iRet = procMenuCmd_GuideSet(pCtrl);
            break;
        }
        case MENUCMD_ID_AUTOCFG:
        {
            iRet = procMenuCmd_AutoCfg(pCtrl);
            break;
        }
        default:
        {
            break;
        }
    }

    //int nSelRow = ui->tableWidget->selectedRow();
    //QTableWidgetItem *pItem = ui->tableWidget->currentItem();
    //ui->tableWidget->scrollToItem(pItem);

    return iRet;
}

int WdgFCfgGroup::procMenuCmd_SysReset(CtrlInputChar* pCtrl)
{
    int iRet = 0;

    if (0 == pCtrl->getValue().idx)
    {
        //Yes
        int dlgRet = procMenuCmd_SpecialSigDlg(
                    tr("OK to restore default"),
                    tr("Restore Default"),
                    SIGID_SPECIAL_RestoreDefaultCfg );
        if (dlgRet == QDialog::Rejected)
        {
            CtrlInputParam* pCtrlParam = (pCtrl->getParam());
            pCtrlParam->nInit = 1;
            pCtrl->setParam( pCtrlParam );
        }
    }

    SUBMIT_CUSTOM_TABLE_ROW_DONE;

    return iRet;
}

int WdgFCfgGroup::procMenuCmd_GuideDisp(CtrlInputChar* pCtrl)
{
    TRACEDEBUG( "WdgFCfgGroup::procMenuCmd_GuideDisp idx<%d>", pCtrl->getValue().idx );
    int iRet = 0;

    QFile file( FILENAME_NEED_WIZARD );
    if (0 == pCtrl->getValue().idx)
    {
        // Yes
        if ( file.exists() )
        {
            file.remove( FILENAME_NEED_WIZARD );
        }
    }
    else
    {
        // Not
        if ( !file.exists() )
        {
            file.open( QIODevice::ReadWrite | QIODevice::Text );
            file.close();
        }
    }
    m_stMenuData.reloadMenu();
    SUBMIT_CUSTOM_TABLE_ROW_DONE;

    return iRet;
}

int WdgFCfgGroup::procMenuCmd_GuideSet(CtrlInputChar* pCtrl)
{
    int iRet = 0;

    if (0 == pCtrl->getValue().idx)
    {
        pCtrl->Reset();

        GuideWindow::ms_bInEnter = false;
        emit sigStopDetectAlarm();
        TRACEDEBUG("WdgFCfgGroup::procMenuCmd_GuideSet ...");
        emit goToGuideWindow( WGUIDE_WIZARD );
        qApp->processEvents ();
        TRACEDEBUG("WdgFCfgGroup::procMenuCmd_GuideSet ...");
    }
    else
    {
        SUBMIT_CUSTOM_TABLE_ROW_DONE;
    }

    return iRet;
}

int WdgFCfgGroup::procMenuCmd_AppUpdate(CtrlInputChar* pCtrl)
{
    int iRet = 0;

    if (0 == pCtrl->getValue().idx)
    {
        QFile file( "/home/app_script/rc_test.sh" );
        if ( file.exists() )
        {
            FILE* stream = popen( "/home/usb_down/usb_detection.sh", "r" );
            char buf = -1;
            fread(&buf,sizeof(char),sizeof(buf),stream);
            if (buf == '0')
            {
                //0表示进入升级模式，需要保存相关信息后重启
                DlgInfo dlg( tr("OK to update app") + "?",
                             POPUP_TYPE_QUESTION
                             );
                if (dlg.exec() == QDialog::Accepted)
                {
                    system( "cp /home/app_script/rc_test.sh /etc/rc.sh -rf" );

                    TRACEDEBUG( "Wdg2P9Cfg::FocusTableWdg updating app" );
                    tellAppToReboot();
                }
                else
                {
                    pCtrl->Reset();
                }
            }
            else if(buf == '1')
            {
                //1表示没有U盘
                pCtrl->Reset();
                DlgInfo dlg(
                            tr("Without USB drive") +".",
                             POPUP_TYPE_INFOR
                            );
                dlg.exec();
            }
            else if(buf == '2')
            {
                //2表示U盘为空
                pCtrl->Reset();
                DlgInfo dlg( tr("USB drive is empty"),
                             POPUP_TYPE_INFOR
                             );
                dlg.exec();
            }
            else if(buf == '3')
            {
                //3表示程序不需要升级，已是最新版本
                pCtrl->Reset();
                DlgInfo dlg( tr("Update is not needed"),
                             POPUP_TYPE_INFOR
                             );
                dlg.exec();
            }
            else if(buf == '4')
            {
                //4表示不存在升级文件
                pCtrl->Reset();
                DlgInfo dlg( tr("App program not found"),
                             POPUP_TYPE_INFOR
                             );
                dlg.exec();
            }
            else
            {
                pCtrl->Reset();
                TRACELOG2( "Wdg2P9Cfg::FocusTableWdg err<%c>", buf );
            }
        }
        else
        {
            pCtrl->Reset();
            TRACELOG2( "Wdg2P9Cfg::FocusTableWdg /home/app_script/rc_test.sh isn't existed" );
            DlgInfo dlg( tr("Without script file"),
                         POPUP_TYPE_INFOR
                         );
            dlg.exec();
        }
    }

    SUBMIT_CUSTOM_TABLE_ROW_DONE;

    return iRet;
}

int WdgFCfgGroup::procMenuCmd_AutoCfg(CtrlInputChar* pCtrl)
{
    int iRet = 0;

    if (0 == pCtrl->getValue().idx)
    {
        //Yes
        int dlgRet = procMenuCmd_SpecialSigDlg(
                    tr("Start auto config"),
                    tr("Auto Config"),
                    SIGID_SPECIAL_AutoCfg );
        if (dlgRet == QDialog::Rejected)
        {
            CtrlInputParam* pCtrlParam = (pCtrl->getParam());
            pCtrlParam->nInit = 1;
            pCtrl->setParam( pCtrlParam );
        }
    }

    SUBMIT_CUSTOM_TABLE_ROW_DONE;

    return iRet;
}

int WdgFCfgGroup::procMenuCmd_SpecialSigDlg(
        QString strQuestion,
        QString strInfo,
        SIGID_SPECIAL sigID
        )
{
    CmdItem cmdItem = m_cmdItem;
    cmdItem.CmdType = CT_SET;
    CMD_SET_INFO* setinfo = &(cmdItem.setinfo);
    setinfo->EquipID      = -1;
    setinfo->SigID        = sigID;
    setinfo->SigType      = VAR_LONG;
    setinfo->value.lValue = 0;

    ExecCmdParam_t param;
    param.infoType     = RESULT_INFO_TYPE_RESTORE_DEFAULT;
    param.strQuestion  = strQuestion + "?";
    param.nDlgTimeOut  = TIME_OUT_POPUPDLG;
    param.popupType    = POPUP_TYPE_QUERY_CMDEXEC;
    param.nKeyEnter    = 0;
    param.strInfo      = strInfo + "...";
    param.cmdItem      = cmdItem;
    param.bAutoDestroy = false;
    param.bShowResult  = true;
    param.strResultYes = tr("Rebooting")+"...";
    param.strResultNot = tr("Restore failed");

    DlgInfo dlg( strQuestion + "?",
                 POPUP_TYPE_QUESTION
                 );
    dlg.execSetCmd( param );
    return dlg.exec();
}

void WdgFCfgGroup::on_tableWidget_itemSelectionChanged()
{
    int nSelRow = ui->tableWidget->selectedRow();
    CtrlInputChar* pCtrl = NULL;

    if((m_nSelRowLast >= 0) && (m_nSelRowLast < m_nCtrlInputCreated))
    {
        pCtrl = (CtrlInputChar*)(ui->tableWidget->cellWidget(m_nSelRowLast, 0));
        pCtrl->setHighLight( false );
    }
//    int i;
//    for(i = 0; i < m_nRows; i++)
//    {
//        pCtrl = (CtrlInputChar*)(ui->tableWidget->cellWidget(i, 0));
//        pCtrl->setHighLight( false );
//    }

    if((nSelRow >= 0) && (nSelRow < m_nCtrlInputCreated))
    {
        pCtrl = (CtrlInputChar*)(ui->tableWidget->cellWidget(nSelRow, 0));
        pCtrl->setHighLight( true );
    }

    m_nSelRowLast = nSelRow;
    updateScrollBar();
    //TRACEDEBUG( "WdgFCfgGroup::on_tableWidget_itemSelectionChanged m_nSelRowLast=%d, nSelRow=%d", m_nSelRowLast, nSelRow);
}

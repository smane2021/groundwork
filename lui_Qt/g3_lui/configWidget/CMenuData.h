#ifndef CMENUDATA_H
#define CMENUDATA_H

#include <qobject.h>
#include <QEvent>
#include "dataDef.h"

enum MENUNODE_ID_TYPE{
    MENUNODE_ID_INVALID = -1,
    MENUNODE_ID_ROOT,
    MENUNODE_ID_MAINTENANCE,
    MENUNODE_ID_ENERGY_SAVING,
    MENUNODE_ID_ALM_SETTINGS,
    MENUNODE_ID_BAT_SETTINGS,
    MENUNODE_ID_AC_SETTINGS,
    MENUNODE_ID_LVD_SETTINGS,
    MENUNODE_ID_RECT_SETTINGS,
    MENUNODE_ID_SYS_SETTINGS,
    MENUNODE_ID_COMM_SETTINGS,
    MENUNODE_ID_OTHER_SETTINGS,
    MENUNODE_ID_SLAVE_SETTINGS,//slave mode menu
    MENUNODE_ID_BASIC_SETTINGS,
    MENUNODE_ID_CHARGE,
    MENUNODE_ID_BAT_TEST,
    MENUNODE_ID_TEMP_COMP,
    MENUNODE_ID_BAT1_SETTINGS,
    MENUNODE_ID_BAT2_SETTINGS,
    MENUNODE_ID_TEST1,
    MENUNODE_ID_CMD_SYS_RESET,
    MENUNODE_ID_CMD_APP_UPDATE,
    MENUNODE_ID_CMD_DISP_MODE,
    MENUNODE_ID_CMD_AUTO_CFG,
    MENUNODE_ID_CMD_GUIDE_DISP,
    MENUNODE_ID_CMD_GUIDE_SET,
    MENUNODE_ID_MAX_NUM
};

enum MENUSIG_ID_TYPE{
    MENUSIG_ID_INVALID = -1,
    MENUSIG_ID_WIZSITNAME = SCREEN_ID_WizSiteName,
    MENUSIG_ID_WIZCOMMON = SCREEN_ID_WizCommon,
    MENUSIG_ID_WIZBATT = SCREEN_ID_WizBatt,
    MENUSIG_ID_WIZECO = SCREEN_ID_WizECO,
    MENUSIG_ID_WIZALARM = SCREEN_ID_WizAlarm,
    MENUSIG_ID_WIZCOMMUNICATE = SCREEN_ID_WizCommunicate,
    MENUSIG_ID_SLAVE = SCREEN_ID_Wdg2P9Cfg_Slave,//0x050207
    MENUSIG_ID_MAINTENANCE,
    MENUSIG_ID_ENERGY_SAVING,
    MENUSIG_ID_ALM_SETTINGS,
    MENUSIG_ID_BASIC_SETTINGS,
    MENUSIG_ID_CHARGE,
    MENUSIG_ID_BAT_TEST,
    MENUSIG_ID_TEMP_COMP,
    MENUSIG_ID_BAT1_SETTINGS,
    MENUSIG_ID_BAT2_SETTINGS,
    MENUSIG_ID_AC_SETTINGS,
    MENUSIG_ID_LVD_SETTINGS,
    MENUSIG_ID_RECT_SETTINGS,
    MENUSIG_ID_SYS_SETTINGS,
    MENUSIG_ID_COMM_SETTINGS,
    MENUSIG_ID_OTHER_SETTINGS,
    MENUSIG_ID_MAX_NUM
};




enum MENUCMD_ID_TYPE{
    MENUCMD_ID_INVALID = -1,
    MENUCMD_ID_SYS_RESET,
    MENUCMD_ID_APP_UPDATE, //MENUCMD_ID_DISP_MODE,
    MENUCMD_ID_GUIDE_DISP,
    MENUCMD_ID_GUIDE_SET,
    MENUCMD_ID_AUTOCFG,
    MENUCMD_ID_SIG_PROTOCOL,
    MENUCMD_ID_SIG_YDN23_ADR,
    MENUCMD_ID_SIG_YDN23_METHOD,
    MENUCMD_ID_SIG_YDN23_BAUDRATE,
    MENUCMD_ID_SIG_MODBUS_ADR,
    MENUCMD_ID_SIG_MODBUS_METHOD,
    MENUCMD_ID_SIG_MODBUS_BAUDRATE,
    MENUCMD_ID_SIG_DATE,
    MENUCMD_ID_SIG_TIME,
    MENUCMD_ID_SIG_IP,
    MENUCMD_ID_SIG_MASK,
    MENUCMD_ID_SIG_GATEWAY,
    MENUCMD_ID_SIG_DHCP,
    // IPV6
    MENUCMD_ID_SIG_IPV6_IP_1,
    MENUCMD_ID_SIG_IPV6_IP_2,
    MENUCMD_ID_SIG_IPV6_MASK,
    MENUCMD_ID_SIG_IPV6_GATEWAY_1,
    MENUCMD_ID_SIG_IPV6_GATEWAY_2,
    MENUCMD_ID_SIG_IPV6_DHCP,
    // IPV6_v
    MENUCMD_ID_SIG_IPV6_IP_1_V,
    MENUCMD_ID_SIG_IPV6_IP_2_V,
    MENUCMD_ID_SIG_IPV6_IP_3_V,
    MENUCMD_ID_SIG_IPV6_GATEWAY_1_V,
    MENUCMD_ID_SIG_IPV6_GATEWAY_2_V,
    MENUCMD_ID_SIG_IPV6_GATEWAY_3_V,
    MENUCMD_ID_MAX_NUM
};








enum MENUNODE_TYPE{
    MENUNODE_INVALID = -1,
    MENUNODE_DIR,
    MENUNODE_CMD,
    MENUNODE_SIG,
    MENUNODE_SPECIAL_SIG,
    MENUNODE_MAX_NUM
};

#define CMENUDATA_MAXLEN_NAME    MAXLEN_NAME
#define CMENUDATA_MAXLEN_UINT    MAXLEN_UNIT

typedef struct _MenuNode{

    struct _MenuNode *pFather;
    struct _MenuNode *pFirstSon;
    struct _MenuNode *pNextBrother;
    void *pData;//for other data

    int iNodeType;
    int iNodeId;
    char sNodeName[CMENUDATA_MAXLEN_NAME];
    int iSonCount;// not including SIG, but including CMD and DIR

    //display attribute
    bool bDispEnable;
    int iDispOrder;

    int iDirQueryId;//for directory node
    int iCmdExecId;//for command node
    int iSigIndexId;//for signal node

    int      iEquipID;
    int      iSigID;
    bool     bReadOnly;

} MenuNode_t;

//command config struct
typedef struct SettingInfo MenuCmd_t;

class CMenuData:public QObject
{
    Q_OBJECT
public:
    CMenuData(QObject *pParent = NULL);
    ~CMenuData();
    bool loadMenu(QString sFileName);
    MenuNode_t * getNode(int iNodeId);
    int getFatherNodeId(int iNodeId);
    int getFirstSonNodeId(int iNodeId);
    int getLevelCount(void);
    int getNodeCount(void);
    bool getCmdCfg(int iCmdExecId, MenuCmd_t *pCmdCfg);
    int computeLevelCount(MenuNode_t *pNode);
    bool getSpecSigCfg(int iSpecSigId, MenuCmd_t *pCmdCfg);
    void destroyMenu(void);
    void reloadMenu();

protected:
    void initItem(MenuNode_t *pNode,
                  int iNodeType = MENUNODE_DIR,
                  int iNodeId = MENUNODE_ID_INVALID,
                  QString sNodeName = "",
                  int iDirQueryId = MENUSIG_ID_INVALID,
                  int iCmdExecId = MENUCMD_ID_INVALID,
                  int iDispOrder = -1,
                  bool bDispEnable = true);
    void appendSubNode(MenuNode_t *pNewNode, int iFatherNodeId);
    bool loadCmdCfg(QString sFileName);
    virtual bool eventFilter(QObject *object, QEvent *event);

private:    
    void insertBrotherItem(MenuNode_t *pPrevBrotherNode, MenuNode_t *pNewNode);
    void insertSonItem(MenuNode_t *pFatherNode, MenuNode_t *pNewNode);
    void printItem(const char *pInfo, MenuNode_t *pNode);
    void setEnumCmdCfg(int iCmdExecId, double fValue, QString sSigName, QStringList slEnumText);
    void setCmdCfg( int iCmdExecId,
                    int iValueType,
                    double fValue,
                    double fStep,
                    double fDnLimit,
                    double fUpLimit,
                    QString sSigName,
                    QString sSigUnit,
                    QStringList slEnumText);
    void setIntCmdCfg(int iCmdExecId,
                      double fValue,
                      double fStep,
                      double fDnLimit,
                      double fUpLimit,
                      QString sSigName);
    void setUIntCmdCfg(int iCmdExecId,
                       double fValue,
                       double fStep,
                       double fDnLimit,
                       double fUpLimit,
                       QString sSigName);
    void printCmdCfg(const char *pInfo, MenuCmd_t *pMenuCmd);

private:
    MenuNode_t * m_pstNodeTable[MENUNODE_ID_MAX_NUM];
    MenuCmd_t m_stCmdExecTable[MENUCMD_ID_MAX_NUM];
    int m_iLevelCount;
    int m_iNodeCount;
    int m_bIsInit;
};

#endif // CMENUDATA_H

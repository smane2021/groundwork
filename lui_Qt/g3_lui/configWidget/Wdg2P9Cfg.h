/******************************************************************************
文件名：    Wdg2P9Cfg.h
功能：      设置 界面
作者：      刘金煌
创建日期：   2013年5月16日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef WDG2P9CFG_H
#define WDG2P9CFG_H

#include <QTimer>
#include "common/basicwidget.h"
#include "common/InputCtrlDef.h"
#include "common/uidefine.h"
#include "common/CtrlInputChar.h"

#define SIGIDX_SIGNUM                   m_sigNum
#define SIGIDX_RESTORE_DEFAULT         (SIGIDX_SIGNUM+0)
#define SIGIDX_SHOW_WIZARD_START       (SIGIDX_SIGNUM+1)
#define SIGIDX_START_WIZARD_NOW        (SIGIDX_SIGNUM+2)
#define SIGIDX_UPDATE_APP              (SIGIDX_SIGNUM+3)
#define SIGIDX_OTHER_SETTING_BATT1     (SIGIDX_SIGNUM+4)
#define SIGIDX_OTHER_SETTING_BATT2     (SIGIDX_SIGNUM+5)
#define SIGIDX_OTHER_SETTING           (SIGIDX_SIGNUM+6)
#define MAX_CUSTOM_CTRL_ITEMS (7)
// other setting
#define SIGIDX_AUTO_CONFIG_2           (SIGIDX_SIGNUM+MAX_CUSTOM_CTRL_ITEMS)
#define SIGIDX_LCD_ROTATION_2          (SIGIDX_SIGNUM+MAX_CUSTOM_CTRL_ITEMS+1)

#define MAX_CTRL_ITEMS_CFG (MAXLEN_SET+MAX_CUSTOM_CTRL_ITEMS)

#define ROW_SET_LANGUAGE       (1)
#define ROW_SET_WORKMODE_MAIN  (11)
#define ROW_SET_WORKMODE_SLAVE (1)
// old cfg
#define MIN_WT_CFG_SETTING   (WT2_CFG_SETTING)
//#define MIN_WT_CFG_SETTING   (WT2_CFG_Slave)

namespace Ui {
class Wdg2P9Cfg;
}

class Wdg2P9Cfg : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit Wdg2P9Cfg(int nIniInputWdgNum=4, QWidget *parent = 0);
    ~Wdg2P9Cfg();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();
    virtual void ShowData(void* pData);
    bool createInputWidget(int nSpawn);

protected:
    void RefreshNow();
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);

private:
    void restartQTimer();
    void clearTxtInputTable();
    void createYesNoRows(QString strSigName);
    void createEnumTxtRow(const QString &strSigName,
                          const QStringList &strList,
                          const QString &strPrefix="",
                          const QString &strSuffix="",
                          int nInitVal=0);
    void createReadonlyItem(QString strSigName);
    // return 0 submit custom data finished, else other unfinished
    int  submitCustomData(int nSelRow, CtrlInputChar* pCtrl);
    void submitUpdateApp(CtrlInputChar* pCtrl);

private slots:
    void FocusTableWdg(enum INPUT_TYPE);
    void sltTableKeyPress(int key);
    void TimerHandler(void);

    void on_tableWidget_itemSelectionChanged();

signals:
    void goToHomePage();
    void goToGuideWindow(enum WIDGET_GUIDE);
    void goToBaseWindow(enum WIDGET_TYPE);
    void sigStopDetectAlarm();
    
private:
    QTimer   m_QTimer;
    CmdItem  m_cmdItem;
    int      m_sigNum;
    int      m_nRows; // total rows count-1
    int      m_nSigIdx[MAX_CTRL_ITEMS_CFG*2]; // the index of sigvalue

    // create ctrlInput one by one
    int      m_timerIdSpawn;
    // input ctrl widget number created
    int      m_nCtrlInputCreated;
    int      m_wt;
    int      m_nSelRowOld;
    int      m_nSelRowFirst;

private:
    Ui::Wdg2P9Cfg *ui;
};

#endif // WDG2P9CFG_H

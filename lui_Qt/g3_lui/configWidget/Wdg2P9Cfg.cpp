/******************************************************************************
文件名：    Wdg2P9Cfg.cpp
功能：      设置 界面
作者：      刘金煌
创建日期：   2013年5月16日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "Wdg2P9Cfg.h"
#include "ui_Wdg2P9Cfg.h"

#include <QKeyEvent>
#include <QFile>
#include "basicWidget/GuideWindow.h"
#include "util/DlgInfo.h"
#include "config/configparam.h"
#include "common/pubInclude.h"
#include "common/global.h"
#include "common/BuzzDoubleSpinBox.h"
#include "common/BuzzSpinBox.h"
#include "common/MultiDemon.h"

/* modify by wufang,20131010,modify:4/6,for slave mode */
#include "basicWidget/homepagewindow.h"

using namespace InputCtrl;

#define ENTER_WDG2P9CFG(wt) \
    Enter( (void*)&wt );

// old cfg
Wdg2P9Cfg::Wdg2P9Cfg(int nIniInputWdgNum, QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::Wdg2P9Cfg)
{
    ui->setupUi(this);

    m_nRows        = 0;
    m_nCtrlInputCreated    = 0;
    m_timerIdSpawn = startTimer( 800 );
    m_cmdItem.CmdType   = CT_READ;

    //INIT_WIDGET;
    SET_BACKGROUND_WIDGET( PosBase::strImgBack_Line_Title );
    SET_GEOMETRY_LABEL_TITLE( ui->label_title );
    SET_TABLEWDG_STYLE( tableWidget, TT_YES_TITLE_NOT_SCROOL );
    ui->tableWidget->clearItems();

    createInputWidget( nIniInputWdgNum );

    InitConnect();
}

Wdg2P9Cfg::~Wdg2P9Cfg()
{
    delete ui;
}

void Wdg2P9Cfg::InitWidget()
{
#ifdef Q_OS_WIN32
    for (int i=0; i<MAX_CTRL_ITEMS_CFG*2; ++i)
    {
        m_nSigIdx[i] = SIGIDX_INIT;
    }
    int nRows = m_nRows = 1;
    ui->tableWidget->setRowCount( nRows );
    for (int i=0; i<nRows; ++i)
    {
        ui->tableWidget->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
    }
    CtrlInputChar*    pCtrl  = NULL;
    {
        //ECO Mode
        CtrlInputParam ctrlParam;
        ctrlParam.ipt = IPT_SBT_CUSTOM;
        ctrlParam.nMin = 0;
        ctrlParam.nMax = 1;
        ctrlParam.strListEnumText << "Enabled" << "Disable";
        ctrlParam.strInit = "Enabled";
        ctrlParam.nInit   = 0;
        ctrlParam.strPrefix = "ECO Mode:";
        pCtrl = new CtrlInputChar( this, &ctrlParam );
        connect( pCtrl,
                 SIGNAL(escapeMe(enum INPUT_TYPE)),
                 this,
                 SLOT(FocusTableWdg(enum INPUT_TYPE)) );
        ui->tableWidget->setCellWidget(0, 0, pCtrl);
        m_nSigIdx[0] = 0;
        ui->tableWidget->selectRow( 0 );
    }
#endif
}

void Wdg2P9Cfg::InitConnect()
{
    connect( ui->tableWidget, SIGNAL(sigTableKeyPress(int)),
            this, SLOT(sltTableKeyPress(int)) );

    connect( &m_QTimer, SIGNAL(timeout()),
             this, SLOT(TimerHandler()) );
}

void Wdg2P9Cfg::clearTxtInputTable()
{
    QTableWidgetItem* item = NULL;
    CtrlInputChar*    pCtrl  = NULL;
    for(int nSigIdx=0; nSigIdx<m_nCtrlInputCreated; ++nSigIdx)
    {
        item = ui->tableWidget->item(nSigIdx*2, 0);
        item->setText( "" );
        pCtrl = (CtrlInputChar*)
                (ui->tableWidget->cellWidget(nSigIdx*2+1, 0));
        pCtrl->setCtrlVisible( false );
    }
}

bool Wdg2P9Cfg::createInputWidget(int nSpawn)
{
    if (m_nCtrlInputCreated >= MAX_CTRL_ITEMS_CFG)
    {
        //TRACEDEBUG( "Wdg2P9Cfg::createInputWidget() InputNum<%d> return", m_nCtrlInputCreated );
        return false;
    }
    CtrlInputChar*    pCtrl  = NULL;
    QTableWidgetItem* item   = NULL;
    CtrlInputParam ctrlParam;
    QTime timeElapse;
    timeElapse.start();
    for(int i=m_nCtrlInputCreated; i<m_nCtrlInputCreated+nSpawn; ++i)
    {
        ui->tableWidget->insertRow( i*2 );
        item = new QTableWidgetItem;
        item->setText( "" );
        ui->tableWidget->setItem(i*2, 0, item);
        ui->tableWidget->setRowHeight(i*2, TABLEWDG_ROW_HEIGHT);

        ui->tableWidget->insertRow( i*2+1 );
        pCtrl = new CtrlInputChar( this, &ctrlParam );
        connect( pCtrl,
                 SIGNAL(escapeMe(enum INPUT_TYPE)),
                 this,
                 SLOT(FocusTableWdg(enum INPUT_TYPE)) );
        ui->tableWidget->setCellWidget(i*2+1, 0, pCtrl);
        ui->tableWidget->setRowHeight(i*2+1, TABLEWDG_ROW_HEIGHT);
        //TRACEDEBUG( "Wdg2P9Cfg::createInputWidget() time<%d> i<%d>", timeElapse.elapsed(), i );
    }
    m_nCtrlInputCreated += nSpawn;
    //TRACEDEBUG( "Wdg2P9Cfg::createInputWidget() time<%d> nSpawn<%d> InputNum<%d>", timeElapse.elapsed(), nSpawn, m_nCtrlInputCreated );
    return true;
}

void Wdg2P9Cfg::Enter(void* param)
{
    m_nSelRowOld = 1;
    ms_showingWdg = this;
    g_bSendCmd    = true;
    m_wt = *(int*)param;
    TRACELOG1( "Wdg2P9Cfg::Enter(void* param) m_wt<%d>", m_wt );

#ifdef TEST_GUI
    ENTER_FIRSTLY;
#else
    switch ( m_wt )
    {
    case WT2_CFG_SETTING:
    {
        ui->label_title->setText( tr("Settings") );
        if (HOMEPAGE_TYPE_SLAVE == ConfigParam::ms_initParam.homePageType)
        {
            m_cmdItem.ScreenID  = SCREEN_ID_Wdg2P9Cfg_Slave;
        }
        else
        {
            m_cmdItem.ScreenID  = SCREEN_ID_Wdg2P9Cfg;
        }
    }
        break;

    case WT3_CFG_SETTING_OTHER:
    {
        ui->label_title->setText( tr("Other Settings") );
        m_cmdItem.ScreenID  = SCREEN_ID_Wdg2P9Cfg_Other;
    }
        break;

    case WT3_CFG_SETTING_OTHER_BATT1:
    {
        ui->label_title->setText( tr("Batt1 Settings") );
        m_cmdItem.ScreenID  =
                SCREEN_ID_Wdg2P9Cfg_Other_Batt1;
    }
        break;

    case WT3_CFG_SETTING_OTHER_BATT2:
    {
        ui->label_title->setText( tr("Batt2 Settings") );
        m_cmdItem.ScreenID  =
                SCREEN_ID_Wdg2P9Cfg_Other_Batt2;
    }
        break;

    default:
        break;
    }

    for (int i=0; i<MAX_CTRL_ITEMS_CFG*2; ++i)
    {
        m_nSigIdx[i] = SIGIDX_INIT;
    }

    void* pData = NULL;
    data_getDataSync(&m_cmdItem, &pData, TIME_WAIT_SYNC);
    TRACELOG1( "Wdg2P9Cfg::Enter() data_getDataSync ScreenID<%x>", m_cmdItem.ScreenID );
    ShowData( pData );
    m_QTimer.stop();
    m_QTimer.start( TIMER_UPDATE_DATA_INTERVAL_SETTING );
#endif

    SET_STYLE_SCROOLBAR(m_nRows+1, 1);
    ui->tableWidget->selectRow( 0 );
    ui->tableWidget->selectRow( 1 );
    ui->tableWidget->setFocus();
}

void Wdg2P9Cfg::Leave()
{
    int nSelRow = ui->tableWidget->selectedRow();
    TRACEDEBUG( "Wdg2P9Cfg::Leave nSelRow<%d>", nSelRow );
    m_QTimer.stop();

    CtrlInputChar* pCtrl = NULL;
    if (m_nSigIdx[nSelRow]>SIGIDX_INIT &&
            nSelRow%2!=0)
    {
        pCtrl = (CtrlInputChar*)
                (ui->tableWidget->cellWidget(nSelRow, 0));
        pCtrl->setHighLight( false );
    }
}

void Wdg2P9Cfg::Refresh()
{
    REFRESH_DATA_PERIODICALLY(
                m_cmdItem.ScreenID,
                "Wdg2P9Cfg::Refresh()"
                );
}

void Wdg2P9Cfg::RefreshNow()
{
    void* pData = NULL;
    if ( data_getDataSync(&m_cmdItem, &pData, TIME_WAIT_SYNC) )
    {
        TRACELOG1( "Wdg2P9Cfg::RefreshNow data_getDataSync ScreenID<%06x> no data", m_cmdItem.ScreenID );
    }
    else
    {
        ShowData( pData );
    }
}

void Wdg2P9Cfg::restartQTimer()
{
    TRACEDEBUG( "Wdg2P9Cfg::restartQTimer" );
    if ( m_QTimer.isActive() )
    {
        m_QTimer.stop();
    }
    g_bSendCmd    = true;
    m_QTimer.start();
}

void Wdg2P9Cfg::ShowData(void* pData)
{
    TRACEDEBUG( ">>> Wdg2P9Cfg::ShowData" );
    if ( !pData )
        return;

    memcpy( g_dataBuff, pData, sizeof(PACK_SETINFO) );
    PACK_SETINFO* info = (PACK_SETINFO*)g_dataBuff;
    m_sigNum = info->SetNum;
    if (m_nCtrlInputCreated < m_sigNum+MAX_CUSTOM_CTRL_ITEMS)
    {
        createInputWidget( m_sigNum+MAX_CUSTOM_CTRL_ITEMS-m_nCtrlInputCreated );
    }
    TRACEDEBUG( "Wdg2P9Cfg::ShowData m_sigNum %d", m_sigNum );
    clearTxtInputTable();

    SET_INFO* pInfoItem = NULL;
    CtrlInputChar*    pCtrl  = NULL;
    QTableWidgetItem* item   = NULL;

    QString strSigName;
    int nSigValueType = 0;
    m_nRows = 0;
    for(int nSigIdx=0; nSigIdx<m_sigNum; ++nSigIdx)
    {
        pInfoItem = &(info->SettingInfo[nSigIdx]);
        TRACEDEBUG( "> Wdg2P9Cfg::ShowData for nSigIdx<%d> m_nRows<%d>", nSigIdx, nSigIdx*2+1 );
        nSigValueType = pInfoItem->iSigValueType;

        if (nSigValueType > VAR_ENUM)
        {
            TRACEDEBUG( "Wdg2P9Cfg::ShowData nSigValueType %d > 5 !", nSigValueType );
            continue;
        }

		/* modify by wufang,20131010,modify:5/6,for slave mode */
        if (HomePageWindow::ms_homePageType == HOMEPAGE_TYPE_SLAVE)
        {
            if(nSigIdx > 1)
            {
                break;
            }
        }

        // sigName
        strSigName = QString(pInfoItem->cSigName) + ":";
        TRACEDEBUG( "Wdg2P9Cfg::ShowData sigName<%s>", pInfoItem->cSigName );
        item = ui->tableWidget->item(nSigIdx*2, 0);
        item->setText( strSigName );

        // input widget
        CtrlInputParam ctrlParam;
        makeCtrlparamBySigtype(&ctrlParam, pInfoItem);
        pCtrl = (CtrlInputChar*)
                (ui->tableWidget->cellWidget(nSigIdx*2+1, 0));
        TRACEDEBUG( "Wdg2P9Cfg::ShowData nSigValueType<%d> ipt<%d> strSigName Prefix<%s:> SigUnit strSuffix<%s>",
                    nSigValueType, ctrlParam.ipt,
                    pInfoItem->cSigName,
                    pInfoItem->cSigUnit );
        pCtrl->setParam( &ctrlParam );

        m_nSigIdx[nSigIdx*2+1] = nSigIdx;
    }

    m_nRows = m_sigNum*2-1;

    switch ( m_wt )
    {
    case WT2_CFG_SETTING:
    {
        /* modify by wufang,20131010,modify:6/6,for slave mode */
        if (HomePageWindow::ms_homePageType == HOMEPAGE_TYPE_SLAVE)
        {
            strSigName = tr( "Update app" )+":";
            createYesNoRows( strSigName );
            m_nSigIdx[m_nRows] = SIGIDX_UPDATE_APP;
            break;
        }

        // Restore Default setting
        strSigName = tr( "Restore Default" )+":";
        createYesNoRows( strSigName );
        m_nSigIdx[m_nRows] = SIGIDX_RESTORE_DEFAULT;

        // show wizard when starting?
        strSigName = tr( "LCD Display Wizard" )+":";
        createYesNoRows( strSigName );
        pCtrl = (CtrlInputChar*)
                (ui->tableWidget->cellWidget(m_nRows, 0));
        CtrlInputParam* pCtrlParam = pCtrl->getParam();
        QFile file( FILENAME_NEED_WIZARD );
        if ( file.exists() )
        {
            pCtrlParam->nInit   = 1;
            pCtrlParam->strInit = tr("No");
        }
        else
        {
            pCtrlParam->nInit   = 0;
            pCtrlParam->strInit = tr("Yes");
        }
        pCtrl->setParam( pCtrlParam );
        m_nSigIdx[m_nRows] = SIGIDX_SHOW_WIZARD_START;

        // show wizard now?
        strSigName = tr( "Start wizard now" )+":";
        createYesNoRows( strSigName );
        m_nSigIdx[m_nRows] = SIGIDX_START_WIZARD_NOW;

        // update
        strSigName = tr( "Update app" )+":";
        createYesNoRows( strSigName );
        m_nSigIdx[m_nRows] = SIGIDX_UPDATE_APP;

        createReadonlyItem( tr( "Batt1 Setting" )+" >" );
        m_nSigIdx[m_nRows] = SIGIDX_OTHER_SETTING_BATT1;

        createReadonlyItem( tr( "Batt2 Setting" )+" >" );
        m_nSigIdx[m_nRows] = SIGIDX_OTHER_SETTING_BATT2;

        createReadonlyItem( tr( "Other Settings" )+" >" );
        m_nSigIdx[m_nRows] = SIGIDX_OTHER_SETTING;
    }
        break;

    case WT3_CFG_SETTING_OTHER:
    {
        // Auto Config
        strSigName = tr( "Auto Config" )+":";
        createYesNoRows( strSigName );
        m_nSigIdx[m_nRows] = SIGIDX_AUTO_CONFIG_2;

        // lcd rotation
        strSigName = tr( "lcd rotation" )+":";
        static QStringList strList =
                (QStringList() << "0" << "90");
        int nInitVal = 0;
        QFile file( FILENAME_LCD_ROTATION );
        if ( file.exists() )
        {
            nInitVal = 1;
        }
        createEnumTxtRow(
                    strSigName,
                    strList,
                    "",
                    "deg",
                    nInitVal
                    );
        m_nSigIdx[m_nRows] = SIGIDX_LCD_ROTATION_2;
    }
        break;

    default:
        break;
    }

    TRACEDEBUG( "Wdg2P9Cfg::ShowData m_nRows<%d>", m_nRows );
}


void Wdg2P9Cfg::createYesNoRows(QString strSigName)
{
    TRACEDEBUG( "> Wdg2P9Cfg::createYesNoRows strSigName<%s>", strSigName.toUtf8().constData() );
    static QStringList strList =
            (QStringList() << tr("Yes") << tr("No"));
    createEnumTxtRow(strSigName, strList, "", "", 1);

    /*
    QTableWidgetItem* item   = NULL;
    CtrlInputChar*    pCtrl  = NULL;
    // sigName
    ++m_nRows;
    item = ui->tableWidget->item(m_nRows, 0);
    item->setText( strSigName );

    // input widget
    ++m_nRows;
    CtrlInputParam ctrlParam;
    ctrlParam.strPrefix  = "";
    ctrlParam.strSuffix  = "";
    ctrlParam.ipt = IPT_SBT_CUSTOM;
    ctrlParam.nMin = 0;
    ctrlParam.nMax = 1;
    ctrlParam.strListEnumText << tr("Yes") << tr("No");
    ctrlParam.strInit = tr("No");
    ctrlParam.nInit   = 1;
    pCtrl = (CtrlInputChar*)
            (ui->tableWidget->cellWidget(m_nRows, 0));
    pCtrl->setParam( &ctrlParam );
    //TRACEDEBUG( "Wdg2P9Cfg::createYesNoRows 2" );
    */
}

void Wdg2P9Cfg::createEnumTxtRow(const QString &strSigName,
                                 const QStringList &strList,
                                 const QString &strPrefix,
                                 const QString &strSuffix,
                                 int nInitVal)
{
    int nStrSigCount = strList.count();
    TRACEDEBUG( "> Wdg2P9Cfg::createEnumTxtRow strSigName<%s> strList<%d>", strSigName.toUtf8().constData(), nStrSigCount );
    QTableWidgetItem* item   = NULL;
    CtrlInputChar*    pCtrl  = NULL;
    // sigName
    ++m_nRows;
    item = ui->tableWidget->item(m_nRows, 0);
    item->setText( strSigName );

    // input widget
    ++m_nRows;
    CtrlInputParam ctrlParam;
    ctrlParam.strPrefix  = strPrefix;
    ctrlParam.strSuffix  = strSuffix;
    ctrlParam.ipt = IPT_SBT_CUSTOM;
    ctrlParam.nMin = 0;
    ctrlParam.nMax = nStrSigCount-1;
    ctrlParam.strListEnumText = strList;
    ctrlParam.nInit   = nInitVal;
    pCtrl = (CtrlInputChar*)
            (ui->tableWidget->cellWidget(m_nRows, 0));
    pCtrl->setParam( &ctrlParam );
    //TRACEDEBUG( "Wdg2P9Cfg::createEnumTxtRow 2" );
}

void Wdg2P9Cfg::createReadonlyItem(QString strSigName)
{
    TRACEDEBUG( "> Wdg2P9Cfg::createReadonlyItem strSigName<%s>", strSigName.toUtf8().constData() );
    QTableWidgetItem* item   = NULL;
    CtrlInputChar*    pCtrl  = NULL;
    // sigName
    ++m_nRows;
    if (m_nRows%2 == 0)
    {
        item = ui->tableWidget->item(m_nRows, 0);
        item->setText( strSigName );
    }
    else
    {
        // input widget
        CtrlInputParam ctrlParam;
        ctrlParam.ipt = IPT_LABEL_READONLY;
        ctrlParam.strInit = strSigName;
        pCtrl = (CtrlInputChar*)
                (ui->tableWidget->cellWidget(m_nRows, 0));
        pCtrl->setParam( &ctrlParam );
    }
    //TRACEDEBUG( "Wdg2P9Cfg::createReadonlyItem 2" );
}

void Wdg2P9Cfg::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
        if (m_wt == WT3_CFG_SETTING_OTHER)
        {
            ui->label_title->setText( tr("Other Settings") );
        }
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void Wdg2P9Cfg::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerIdSpawn)
    {
        if ( !createInputWidget(1) )
        {
            killTimer( m_timerIdSpawn );
        }
    }
}

void Wdg2P9Cfg::TimerHandler()
{
    //TRACEDEBUG( "Wdg2P9Cfg::TimerHandler()" );
    Refresh();
}

void Wdg2P9Cfg::keyPressEvent(QKeyEvent* keyEvent)
{
    //TRACELOG1( "Wdg2P9Cfg::keyPressEvent" );
    g_timeLoginConfig.restart();
    switch ( keyEvent->key() )
    {
    case Qt::Key_Return:
    case Qt::Key_Enter:
    {
        m_QTimer.stop();
        int nSelRow = ui->tableWidget->selectedRow();
        if (m_nSigIdx[nSelRow] > SIGIDX_INIT)
        {
            if (m_nSigIdx[nSelRow] ==
                    SIGIDX_OTHER_SETTING)
            {
                m_wt = WT3_CFG_SETTING_OTHER;
                m_nSelRowFirst = nSelRow;
                ENTER_WDG2P9CFG( m_wt );
            }
            else if (m_nSigIdx[nSelRow] ==
                     SIGIDX_OTHER_SETTING_BATT1)
            {
                m_wt = WT3_CFG_SETTING_OTHER_BATT1;
                m_nSelRowFirst = nSelRow;
                ENTER_WDG2P9CFG( m_wt );
            }
            else if (m_nSigIdx[nSelRow] ==
                     SIGIDX_OTHER_SETTING_BATT2)
            {
                m_wt = WT3_CFG_SETTING_OTHER_BATT2;
                m_nSelRowFirst = nSelRow;
                ENTER_WDG2P9CFG( m_wt );
            }
            else
            {
                CtrlInputChar* pCtrl = (CtrlInputChar*)
                        (ui->tableWidget->cellWidget(nSelRow, 0));
                pCtrl->Enter();
            }
        }
    }
        break;

    case Qt::Key_Escape:
    {
        if (m_wt == WT2_CFG_SETTING)
        {
            emit goToHomePage();
        }
        else
        {
            Leave();
            m_wt = WT2_CFG_SETTING;
            ENTER_WDG2P9CFG( m_wt );
            ui->tableWidget->selectRow( m_nSelRowFirst );
        }
    }
        return;

    case Qt::Key_Up:
        ui->tableWidget->selectRow( m_nRows );
        SET_STYLE_SCROOLBAR(m_nRows+1, m_nRows+1);
        break;

    case Qt::Key_Down:
        ui->tableWidget->selectRow( 0 );
        if (m_nSigIdx[0] < 0)
        {
            ui->tableWidget->selectRow( 1 );
        }
        SET_STYLE_SCROOLBAR(m_nRows+1, 1);
        break;

    default:
        break;
    }

    return QWidget::keyPressEvent(keyEvent);
}


void Wdg2P9Cfg::sltTableKeyPress(int key)
{
    g_timeLoginConfig.restart();
    int nSelRow = ui->tableWidget->selectedRow();
    TRACEDEBUG( "Wdg2P9Cfg::sltTableKeyPress nSelRow<%d> key<%0x>", nSelRow, key );

    int nGotoRow = nSelRow;
    if (m_nSigIdx[nSelRow] < 0)
    {
        switch ( key )
        {
        case Qt::Key_Up:
            if (nSelRow==0 && m_nSigIdx[0]<0)
            {
                nGotoRow = m_nRows;
            }
            else
            {
                nGotoRow = ( (nSelRow-1)<0 ?
                              (m_nSigIdx[0]>SIGIDX_INIT?0:1) :
                              (nSelRow-1) );
            }
            if (nGotoRow-1 >= 0)
                ui->tableWidget->selectRow( nGotoRow-1 );
            ui->tableWidget->selectRow( nGotoRow );
            TRACELOG1( "Wdg2P9Cfg::sltTableKeyPress Key_Up nGotoRow<%d>", nGotoRow );
            break;

        case Qt::Key_Down:
            if (nSelRow > m_nRows)
            {
                ui->tableWidget->selectRow( 0 );
                nGotoRow = 1;
            }
            else
            {
                nGotoRow = nSelRow+1;
            }
            ui->tableWidget->selectRow( nGotoRow );
            TRACELOG1( "Wdg2P9Cfg::sltTableKeyPress default nGotoRow<%d>", nGotoRow );
            break;

        default:
            break;
        }
    }
    SET_STYLE_SCROOLBAR(m_nRows+1, nGotoRow+1);
}

// submit data
void Wdg2P9Cfg::FocusTableWdg(enum INPUT_TYPE ipt)
{
    int nSelRow = ui->tableWidget->selectedRow();
    TRACELOG1( "Wdg2P9Cfg::FocusTableWdg INPUT_TYPE<%d> nSelRow<%d>", ipt, nSelRow );

    //ui->tableWidget->selectRow( nSelRow );
    ui->tableWidget->setFocus();
    if (m_nSigIdx[nSelRow] < 0)
    {
        return;
    }

    CtrlInputChar* pCtrl = (CtrlInputChar*)
            (ui->tableWidget->cellWidget(nSelRow, 0));
    if (IPT_SUBMIT == ipt)
    {
        m_QTimer.stop();

        if (submitCustomData(nSelRow, pCtrl) == 0)
        {
            return;
        }

        CtrlInputParam ctrlParamSetting = *(pCtrl->getParam());
        if (SET_PARAM_ERR_OK == submitAppParamData(
                    pCtrl,
                    &m_cmdItem,
                    m_nSigIdx[nSelRow],
                    &ctrlParamSetting
                    ) )
        {
//            PACK_SETINFO* pPackInfoFromApp =
//                    (PACK_SETINFO*)g_dataBuff;
//            SET_INFO*     pSetInfoFromApp  =
//                    &(pPackInfoFromApp->SettingInfo[m_nSigIdx[nSelRow]]);

//            int nEquipID = pSetInfoFromApp->iEquipID;
//            int nSigID   = pSetInfoFromApp->iSigID;
//            // chage language
//            if (nEquipID==1 && nSigID==22)
//            {
//                m_wt = WT3_CFG_SETTING_OTHER;
//                ENTER_WDG2P9CFG( m_wt );
//            }
        }
    }
    RefreshNow();
    restartQTimer();
//    else
//    {
//        pCtrl->Reset();
//        restartQTimer();
//    }
//    ui->tableWidget->selectRow( nSelRow );
//    ui->tableWidget->setFocus();
}

// return 0 submit custom data  else other
int Wdg2P9Cfg::submitCustomData(int nSelRow, CtrlInputChar* pCtrl)
{
#define SUBMIT_CUSTOM_TABLE_ROW_DONE \
    ui->tableWidget->setFocus(); \
    ui->tableWidget->selectRow( nSelRow ); \
    RefreshNow(); \
    restartQTimer();

    if (m_nSigIdx[nSelRow] == SIGIDX_RESTORE_DEFAULT)
    {
        if (0 == pCtrl->getValue().idx)
        {
            //Yes
            DlgInfo dlg( tr("Sure to restore default") + "?",
                         POPUP_TYPE_QUESTION
                         );
            CtrlInputParam* pCtrlParam = (pCtrl->getParam());
            if (dlg.exec() == QDialog::Accepted)
            {
                if (SET_PARAM_ERR_OK == setSpecialParam(
                            &m_cmdItem,
                            SIGID_SPECIAL_RestoreDefaultCfg,
                            0
                            ))
                {
                    DlgInfo dlgInfo( tr("Restore default") + "...",
                                     POPUP_TYPE_INFOR_ALIVE
                                     );
                    dlgInfo.exec();
                }
            }
            pCtrlParam->strInit = tr("No");
            pCtrl->setParam( pCtrlParam );
        }
        SUBMIT_CUSTOM_TABLE_ROW_DONE;
        return 0;
    }
    else if (m_nSigIdx[nSelRow] == SIGIDX_SHOW_WIZARD_START)
    {
        QFile file( FILENAME_NEED_WIZARD );
        if (0 == pCtrl->getValue().idx)
        {
            // Yes
            if ( file.exists() )
            {
                file.remove( FILENAME_NEED_WIZARD );
            }
        }
        else
        {
            // Not
            if ( !file.exists() )
            {
                file.open( QIODevice::ReadWrite | QIODevice::Text );
                file.close();
            }
        }
        SUBMIT_CUSTOM_TABLE_ROW_DONE;
        return 0;
    }
    else if (m_nSigIdx[nSelRow] == SIGIDX_START_WIZARD_NOW)
    {
        if (0 == pCtrl->getValue().idx)
        {
            pCtrl->Reset();

            GuideWindow::ms_bInEnter = false;
            emit sigStopDetectAlarm();
            emit goToGuideWindow( WGUIDE_WIZARD );
        }
        else
        {
            SUBMIT_CUSTOM_TABLE_ROW_DONE;
        }
        return 0;
    }
    else if (m_nSigIdx[nSelRow] == SIGIDX_UPDATE_APP)
    {
        submitUpdateApp( pCtrl );

        SUBMIT_CUSTOM_TABLE_ROW_DONE;
        return 0;
    }
    else if (m_nSigIdx[nSelRow] == SIGIDX_OTHER_SETTING)
    {
        if (0 == pCtrl->getValue().idx)
        {
            //Yes
            m_wt = WT3_CFG_SETTING_OTHER;
            ENTER_WDG2P9CFG( m_wt );
        }
        else
        {
            SUBMIT_CUSTOM_TABLE_ROW_DONE;
        }
        return 0;
    }
    // other setting _2
    else if (m_nSigIdx[nSelRow] == SIGIDX_AUTO_CONFIG_2)
    {
        if (0 == pCtrl->getValue().idx)
        {
            //Yes
            DlgInfo dlg( tr("Start auto config") +"?",
                         POPUP_TYPE_QUESTION
                         );
            CtrlInputParam* pCtrlParam = (pCtrl->getParam());
            if (dlg.exec() == QDialog::Accepted)
            {
                if (SET_PARAM_ERR_OK == setSpecialParam(
                            &m_cmdItem,
                            SIGID_SPECIAL_AutoCfg,
                            0
                            ))
                {
                    DlgInfo dlgInfo( tr("Auto config") + "...",
                                     POPUP_TYPE_INFOR_ALIVE
                                     );
                    dlgInfo.exec();
                }
            }
            pCtrlParam->strInit = tr("No");
            pCtrl->setParam( pCtrlParam );
        }
        SUBMIT_CUSTOM_TABLE_ROW_DONE;
        return 0;
    }
    // other lcd rotation
    else if (m_nSigIdx[nSelRow] == SIGIDX_LCD_ROTATION_2)
    {
        int nInitVal = 0;
        QFile file( FILENAME_LCD_ROTATION );
        if ( file.exists() )
        {
            nInitVal = 1;
        }

        int nSelVal = pCtrl->getValue().idx;
        if (nInitVal != nSelVal)
        {
            if (nSelVal == 0)
            {
                file.remove();
            }
            else
            {
                file.open( QIODevice::ReadWrite | QIODevice::Text );
                file.close();
            }
            system( "reboot" );
        }

        SUBMIT_CUSTOM_TABLE_ROW_DONE;
        return 0;
    }

    return 1;
}

void Wdg2P9Cfg::submitUpdateApp(CtrlInputChar* pCtrl)
{
    if (0 == pCtrl->getValue().idx)
    {
        QFile file( "/home/app_script/rc_test.sh" );
        if ( file.exists() )
        {
            FILE* stream = popen( "/home/usb_down/usb_detection.sh", "r" );
            char buf = -1;
            fread(&buf,sizeof(char),sizeof(buf),stream);
            if (buf == '0')
            {
                //0表示进入升级模式，需要保存相关信息后重启
                DlgInfo dlg( tr("Sure to update app") + "?",
                             POPUP_TYPE_QUESTION
                             );
                if (dlg.exec() == QDialog::Accepted)
                {
                    system( "cp /home/app_script/rc_test.sh /etc/rc.sh -rf" );

                    TRACEDEBUG( "Wdg2P9Cfg::FocusTableWdg updating app" );
                    void* pData = NULL;
                    CmdItem cmdItem;
                    cmdItem.CmdType   = CT_REBOOT;
                    cmdItem.ScreenID  = 0;
                    CMD_SET_INFO* setinfo = &(cmdItem.setinfo);
                    setinfo->EquipID = 0;
                    setinfo->SigID = 0;
                    setinfo->SigType = 0;
                    setinfo->value.lValue = 0;
                    if ( !data_getDataSync(&cmdItem, &pData) )
                    {
                        TRACEDEBUG( "Wdg2P9Cfg::FocusTableWdg update app reboot!" );
                    }
                }
                else
                {
                    pCtrl->Reset();
                }
            }
            else if(buf == '1')
            {
                //1表示没有U盘
                pCtrl->Reset();
                DlgInfo dlg(
                            tr("Without USB disk") +".",
                             POPUP_TYPE_INFOR
                            );
                dlg.exec();
            }
            else if(buf == '2')
            {
                //2表示U盘为空
                pCtrl->Reset();
                DlgInfo dlg( tr("USB disk is empty"),
                             POPUP_TYPE_INFOR
                             );
                dlg.exec();
            }
            else if(buf == '3')
            {
                //3表示程序不需要升级，已是最新版本
                pCtrl->Reset();
                DlgInfo dlg( tr("Needn't update"),
                             POPUP_TYPE_INFOR
                             );
                dlg.exec();
            }
            else if(buf == '4')
            {
                //4表示不存在升级文件
                pCtrl->Reset();
                DlgInfo dlg( tr("App program doesn't exist"),
                             POPUP_TYPE_INFOR
                             );
                dlg.exec();
            }
            else
            {
                pCtrl->Reset();
                TRACELOG2( "Wdg2P9Cfg::FocusTableWdg err<%c>", buf );
            }
        }
        else
        {
            pCtrl->Reset();
            TRACELOG2( "Wdg2P9Cfg::FocusTableWdg /home/app_script/rc_test.sh isn't existed" );
            DlgInfo dlg( tr("Without script file"),
                         POPUP_TYPE_INFOR
                         );
            dlg.exec();
        }
    }
}

void Wdg2P9Cfg::on_tableWidget_itemSelectionChanged()
{
    int nSelRow = ui->tableWidget->selectedRow();
    CtrlInputChar* pCtrl = NULL;

    if (m_nSigIdx[m_nSelRowOld]>SIGIDX_INIT &&
            m_nSelRowOld%2!=0)
    {
        pCtrl = (CtrlInputChar*)
                (ui->tableWidget->cellWidget(m_nSelRowOld, 0));
        pCtrl->setHighLight( false );
    }

    if (m_nSigIdx[nSelRow]>SIGIDX_INIT &&
            nSelRow%2!=0)
    {
        TRACEDEBUG( "Wdg2P9Cfg::on_tableWidget_itemSelectionChanged() select row<%d> m_nSelRowOld<%d>", nSelRow, m_nSelRowOld );

        pCtrl = (CtrlInputChar*)
                (ui->tableWidget->cellWidget(nSelRow, 0));
        pCtrl->setHighLight( true );
        m_nSelRowOld = nSelRow;
    }
}

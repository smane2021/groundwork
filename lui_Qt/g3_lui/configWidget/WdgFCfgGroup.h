#ifndef WDGFCFGGROUP_H
#define WDGFCFGGROUP_H


#include <QTimer>

#include "common/basicwidget.h"
#include "common/InputCtrlDef.h"
#include "common/uidefine.h"
#include "common/CtrlInputChar.h"

#include "CMenuData.h"

#define MIN_WT_CFG_SETTING   (WT2_CFG_SETTING)

namespace Ui {
class WdgFCfgGroup;
}

class WdgFCfgGroup : public BasicWidget
{
    Q_OBJECT

public:
    enum SELECT_TYPE {
        SELECT_INVALID = -1,
        SELECT_BY_ID,
        SELECT_FIRST,
        SELECT_LAST,
        SELECT_PREV,
        SELECT_NEXT,
        SELECT_MAX_NUM
    };

public:
    explicit WdgFCfgGroup(int iIniInputWdgNum = 4, QWidget *parent = NULL);
    ~WdgFCfgGroup();

public:
    virtual void Refresh(void);
    void RefreshNow(void);
    virtual void ShowData(void* pData);
    virtual void Enter(void* param=NULL);
    virtual void Leave(void);
    bool createInputWidget(int iSpawn);
    void Destroy();
    void clearTxtInputTable(void);

protected:
    virtual void InitWidget(void);
    virtual void InitConnect(void);
    void ShowMenu(PACK_SETINFO* pInfo, int iMenuNodeId);
    void selectMenuNode(int iSelectType, int iSelectId = -1);
    virtual void changeEvent(QEvent* event);       
    virtual void keyPressEvent(QKeyEvent* keyEvent);
    virtual void timerEvent(QTimerEvent* event);

private:
    void appendShowNode(int iNodeType, void *pMenuItemData, int iSigIndexId);
    void appendShowDir(MenuNode_t *pNode);
    void appendShowCmd(MenuNode_t *pNode);
    void appendShowNormalSig(SET_INFO *pInfoItem, int iSigIndexId);
    void appendShowSpecialSig(SET_INFO *pInfoItem, int iSigIndexId);
    void makeIPV6Node(CtrlInputParam *pCtrlParam,
                      MenuNode_t *pNode,
                      SET_INFO *pInfoItem,
                      bool bDHCPV6);
    void appendShowSig(SET_INFO *pInfoItem, int iSigIndexId);
    void appendInvalidShowNode(int iSigIndexId);
    int firstValidRow(void);
    int lastValidRow(void);
    int prevValidRow(void);
    int nextValidRow(void);
    int validNodeRowById(int iMenuNodeId);
    void selectValidRow(int iValidRow);
    void updateScrollBar(void);
    void updateMenu(int iMenuNodeId, int iSelectedNodeId);    
    void restartQTimer(void);
    void restartQTimerInterval(int iInterval);
    int procMenuSig(MenuNode_t *pMenuNode, CtrlInputChar* pCtrl);
    int procMenuCmd(MenuNode_t *pMenuNode, CtrlInputChar* pCtrl);
    int procMenuCmd_SysReset(CtrlInputChar* pCtrl);
    int procMenuCmd_GuideDisp(CtrlInputChar* pCtrl);
    int procMenuCmd_GuideSet(CtrlInputChar* pCtrl);
    int procMenuCmd_AppUpdate(CtrlInputChar* pCtrl);
    int procMenuCmd_AutoCfg(CtrlInputChar* pCtrl);
    int procMenuCmd_SpecialSigDlg(
            QString strQuestion,
            QString strInfo,
            SIGID_SPECIAL sigID
            );
    void initSelectedMenuNode(void);
    void DisplayValidNode(void);
    void setMenuModeBatSettingGroup(void *pCfg);
    void getCfgBatSettingGroup(PACK_SETINFO *pInfo, void *pCfg);
    void initMenuMode(void);

private slots:
    void sltTableKeyPress(int key);
    void sltTimerHandler(void);
    void on_tableWidget_itemSelectionChanged();
    void FocusTableWdg(enum INPUT_TYPE ipt);

signals:
    void goToHomePage();
    void goToGuideWindow(enum WIDGET_GUIDE);
    void goToBaseWindow(enum WIDGET_TYPE);
    void sigStopDetectAlarm();

private:
    CMenuData m_stMenuData;
    QTimer   m_oTimer;
    CmdItem  m_cmdItem;
    int m_nRows;
    int m_nInvalidRows;
    int m_nCtrlInputCreated;
    int m_nCtrlInputMax;
    int m_iMenuNodeId;
    int m_iInitSelectedId;
    int m_timerIdSpawn;
    int m_nSelRowLast;
    QString m_strIPV6_IP;
    QString m_strIPV6_Gateway;
    QString m_strIPV6_IP_V;
    QString m_strIPV6_IP_V_2;
    QString m_strIPV6_Gateway_V;
    QString m_strIPV6_Gateway_V_2;
    QTime m_timeElapsedKeyPress;

private:
    Ui::WdgFCfgGroup *ui;
};

#endif // WDGFCFGGROUP_H

#include "ThreadFlashLight.h"
#include "common/pubInclude.h"
#include "common/global.h"

ThreadFlashLight::ThreadFlashLight()
    : m_bStopped(false)
{
}

void ThreadFlashLight::run()
{
    TRACELOG1( ">>> ThreadFlashLight::run()" );
    sys_setLED( LED_YELLOW_OFF );
    sys_setLED( LED_RED_OFF );

    while ( !m_bStopped )
    {
        //TRACELOG1( " ThreadFlashLight::run()" );
        sys_setLED( LED_RED_ON );
        sys_setLED( LED_YELLOW_OFF );
        msleep( 1000 );
        //TRACELOG1( " ThreadFlashLight::run() sleep" );
        sys_setLED( LED_RED_OFF );
        sys_setLED( LED_YELLOW_ON );
        msleep( 1000 );
    }
    sys_setLED( LED_YELLOW_OFF );
    sys_setLED( LED_RED_OFF );
    TRACELOG1( "ThreadFlashLight::run() >>>" );
}

void ThreadFlashLight::stop()
{
    m_bStopped = true;
}

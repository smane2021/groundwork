#include "DlgLoading.h"
#include "ui_DlgLoading.h"

#include <QKeyEvent>
#include "common/uidefine.h"
#include "common/basicwidget.h"
#include "common/pubInclude.h"
#include "config/configparam.h"
#include "config/PosBase.h"

ThreadQueryData::ThreadQueryData()
    : m_bStopped(false)
{
}

void ThreadQueryData::setParam(CmdItem *pCmdItem, void** ppData)
{
    m_bStopped = false;
    m_pCmdItem = pCmdItem;
    m_ppData   = ppData;
}

void ThreadQueryData::run()
{
    TRACELOG1( "ThreadQueryData::run() enter m_bStopped<%d>", m_bStopped );
    while ( !m_bStopped )
    {
        if ( !data_getDataSync(m_pCmdItem, m_ppData, TIME_WAIT_SYNC) )
        {
            //sleep( 10 );
            TRACELOG1( "ThreadQueryData data_getDataSync ScreenID<%06x> return<%d>", m_pCmdItem->ScreenID,  *(int*)(*m_ppData) );
            emit sigReadedData();
            break;
        }
    }
    TRACELOG1( "ThreadQueryData::run() finised!!!" );
}

void ThreadQueryData::stop()
{
    m_bStopped = true;
}

DlgLoading::DlgLoading(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgLoading)
{
    ui->setupUi(this);
    setFocus();
    SET_GEOMETRY_WIDGET( this );

    ui->label->clear();
    ui->label->setGeometry(
                PosBase::screenWidth/2-24,
                PosBase::screenHeight/2,
                48,
                48
                );
    setWindowOpacity( 1 );
    setWindowFlags(Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    for(int i=0; i<6; ++i)
    {
        m_pixmap[i].load( PATH_IMG + "loading" +
                          QString::number(i+1) +
                          ".png" );
    }

    connect( &m_thread, SIGNAL(sigReadedData()),
             this, SLOT(stop()) );

    m_idxPixmap = 0;
}

DlgLoading::~DlgLoading()
{
    stop();

    delete ui;
}

void DlgLoading::start(CmdItem *pCmdItem, void** ppData)
{
    setFocus();
    this->show();
    m_thread.setParam( pCmdItem, ppData );

    m_idxPixmap  = 0;
    m_timerId    = startTimer( 500 );
    m_thread.wait();
    m_thread.start();
}

void DlgLoading::stop()
{
    m_thread.stop();
    if ( m_timerId )
    {
        killTimer( m_timerId );
        m_timerId = 0;
    }
    this->hide();
}

void DlgLoading::timerEvent(QTimerEvent *event)
{
    if(event->timerId() == m_timerId)
    {
        m_idxPixmap = (m_idxPixmap>5 ? 0:m_idxPixmap);
        ui->label->setPixmap( m_pixmap[m_idxPixmap] );
        ++m_idxPixmap;
    }
}

void DlgLoading::keyPressEvent(QKeyEvent* keyEvent)
{
    TRACELOG1( "DlgLoading::keyPressEvent" );
    switch ( keyEvent->key() )
    {
    case Qt::Key_Return:
    case Qt::Key_Enter:
    case Qt::Key_Escape:
    case Qt::Key_Up:
    case Qt::Key_Down:
        return;
    }

    return QDialog::keyPressEvent(keyEvent);
}

void DlgLoading::accept()
{
    TRACELOG1( "DlgLoading::accept" );
//    if (m_timerId)
//        killTimer( m_timerId );
}

void DlgLoading::reject()
{
    TRACELOG1( "DlgLoading::reject" );
}

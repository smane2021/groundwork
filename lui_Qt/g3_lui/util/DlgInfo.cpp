#include "DlgInfo.h"
#include "ui_DlgInfo.h"

#include <QKeyEvent>
#include "config/configparam.h"
#include "config/PosDlgInfo.h"
#include "common/pubInclude.h"
#include "common/buzztablewidget.h"
#include "common/basicwidget.h"
#include "common/global.h"


ThreadWork::ThreadWork()
    : m_bStopped(false)
{
}

void ThreadWork::setParam(
        CmdItem *pCmdItem,
        void** ppData,
        enum RESULT_INFO_TYPE infoType)
{
    m_bStopped = false;
    m_pCmdItem = pCmdItem;
    m_ppData   = ppData;
    m_infoType = infoType;
}

void ThreadWork::run()
{
    TRACELOG1( "ThreadWork::run() enter m_bStopped<%d> m_pCmdItem<%x> m_ppData<%x>", m_bStopped, m_pCmdItem, m_ppData );
    while ( !m_bStopped )
    {
        int nResult = 0;
        switch ( m_infoType )
        {
        case RESULT_INFO_TYPE_RESTORE_DEFAULT:
        {
            TRACEDEBUG( "ThreadWork::run() restore default" );
            nResult = data_getDataSync(m_pCmdItem, m_ppData, 7200);
        }
            break;
            
        case RESULT_INFO_TYPE_UPDATE_RECT:
        {
            TRACEDEBUG( "ThreadWork::run() update rect" );
            nResult = data_getDataSync(m_pCmdItem, m_ppData, 7200);
        }
            break;

        default:
        {
            nResult = data_getDataSync(m_pCmdItem, m_ppData, 1800);
        }
            break;
        }
        //sleep( 10 );
        TRACELOG1( "ThreadWork data_getDataSync ScreenID<%06x> return<%d>", m_pCmdItem->ScreenID,  *(int*)(*m_ppData) );
        emit sigExeced( nResult );
        break;
    }
    TRACELOG1( "ThreadWork::run() finised!!!" );
}

void ThreadWork::stop()
{
    m_bStopped = true;
}

/////////////////////////////

DlgInfo::DlgInfo(QString strContent,
                 enum POPUP_TYPE type,
                 int nTimeOut,
                 QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgInfo)
{
    ui->setupUi(this);

    setCtrlVisible( false );
    m_execCmdParam.infoType    = RESULT_INFO_TYPE_NORMAL;
    m_execCmdParam.nDlgTimeOut = -1;
    m_execCmdParam.popupType   = POPUP_TYPE_MAX;


    m_timerId       = 0;
    m_type          = type;
    m_nTimeOut      = nTimeOut;
    m_strContent    = strContent;
    InitWidget();

    connect( &m_thread, SIGNAL(sigExeced(int)),
             this, SLOT(sltExeced(int)) );

    Enter();
}

DlgInfo::~DlgInfo()
{
    delete ui;
}

void DlgInfo::InitWidget()
{
    setWindowFlags( Qt::FramelessWindowHint );

    setGeometry(
                PosDlgInfo::dlgInfoX,
                PosDlgInfo::dlgInfoY,
                PosDlgInfo::dlgInfoWidth,
                PosDlgInfo::dlgInfoHeight
                );

    ui->label_content->setGeometry(
                PosDlgInfo::contentX,
                PosDlgInfo::contentY,
                PosDlgInfo::contentWidth,
                PosDlgInfo::labelHeight
                );

    ui->label_Cancel->setGeometry(
                PosDlgInfo::contentX,
                PosDlgInfo::cancelY,
                PosDlgInfo::cancelWidth,
                PosDlgInfo::labelHeight
                );

    ui->label_OK->setGeometry(
                PosDlgInfo::contentX,
                PosDlgInfo::okY,
                PosDlgInfo::okWidth,
                PosDlgInfo::labelHeight
                );

    ui->label_content->setAlignment( Qt::AlignLeft );
    ui->label_OK->setAlignment( Qt::AlignLeft );
    ui->label_Cancel->setAlignment( Qt::AlignLeft );

    switch (m_type)
    {
    case POPUP_TYPE_QUESTION:
    case POPUP_TYPE_QUESTION_LANGUAGE:
    {
        SET_BACKGROUND_WIDGET( "DlgInfo",PosBase::strImgBack_Title );

//        ui->label_icon->setVisible( true );
//        QIcon icon = qApp->style()->standardIcon(
//                    QStyle::SP_MessageBoxQuestion,
//                    0,
//                    this
//                    );
//        QSize iconSize = icon.actualSize( QSize(PosDlgInfo::iIcon_Size, PosDlgInfo::iIcon_Size) );
//        ui->label_icon->setPixmap( icon.pixmap(iconSize) );
        setContentText( m_strContent );
        ui->label_OK->setVisible( true );
        ui->label_OK->setText(tr("ENT to OK") );
        ui->label_Cancel->setVisible( true );
        ui->label_Cancel->setText(tr("ESC to Cancel") );

        m_timerId = startTimer( TIME_OUT_POPUPDLG );
    }
        break;

    case POPUP_TYPE_INFOR:
    {
        QIcon icon = qApp->style()->standardIcon(
                    QStyle::SP_MessageBoxInformation,
                    0,
                    this
                    );
        setIconContentOK( icon );
    }
        break;

    case POPUP_TYPE_WARNING:
    {
        QIcon icon = qApp->style()->standardIcon(
                    QStyle::SP_MessageBoxWarning,
                    0,
                    this
                    );
        setIconContentOK( icon );
    }
        break;

    case POPUP_TYPE_CRITICAL:
    {
        QIcon icon = qApp->style()->standardIcon(
                    QStyle::SP_MessageBoxCritical,
                    0,
                    this
                    );
        setIconContentOK( icon );
    }
        break;

    case POPUP_TYPE_SETFAILED:
    {
        QIcon icon = qApp->style()->standardIcon(
                    QStyle::SP_MessageBoxCritical,
                    0,
                    this
                    );
        setIconContentOK( icon );
    }
        break;

    case POPUP_TYPE_INFOR_ALIVE:
    {
        SET_BACKGROUND_WIDGET( "DlgInfo",PosBase::strImgBack_Title );

//        ui->label_icon->setVisible( true );
//        QIcon icon = qApp->style()->standardIcon(
//                    QStyle::SP_MessageBoxInformation,
//                    0,
//                    this
//                    );
//        QSize iconSize = icon.actualSize( QSize(PosDlgInfo::iIcon_Size, PosDlgInfo::iIcon_Size) );
//        ui->label_icon->setPixmap( icon.pixmap(iconSize) );
//        ui->label_content->setGeometry(
//                    PosDlgInfo::dlgInfoX,
//                    PosDlgInfo::dlgInfoHeight/2-PosDlgInfo::labelHeight,
//                    PosDlgInfo::contentWidth,
//                    PosDlgInfo::labelHeight
//                    );
        setContentText( m_strContent );

        TRACEDEBUG( "DlgInfo::InitWidget() m_nTimeOut<%d>", m_nTimeOut );
        if (m_nTimeOut > 0)
        {
            m_timerId = startTimer( m_nTimeOut );
        }
//        else
//        {
//            m_timerId = startTimer( 7*60*1000 );
//        }
    }
        break;
    case POPUP_TYPE_QUESTION_PASSWORD:
    {
        SET_BACKGROUND_WIDGET( "DlgInfo",PosBase::strImgBack_Title );

//        ui->label_icon->setVisible( true );
//        QIcon icon = qApp->style()->standardIcon(
//                    QStyle::SP_MessageBoxQuestion,
//                    0,
//                    this
//                    );
//        QSize iconSize = icon.actualSize( QSize(PosDlgInfo::iIcon_Size, PosDlgInfo::iIcon_Size) );
//        ui->label_icon->setPixmap( icon.pixmap(iconSize) );
        setContentText( m_strContent );
        ui->label_OK->setVisible( true );
        ui->label_OK->setText(tr("ENT for YES") );
        ui->label_Cancel->setVisible( true );
        ui->label_Cancel->setText(tr("ESC for NO") );

        m_timerId = startTimer( TIME_OUT_POPUPDLG );
    }
        break;
    default:
        break;
    }
}


void DlgInfo::Enter(void* param)
{
    Q_UNUSED( param );

    g_bDetectWork = false;
    ui->tableWidget->clearFocus();
    this->setFocus();
}

void DlgInfo::Leave()
{
    if ( m_timerId )
    {
        killTimer( m_timerId );
        m_timerId = 0;
    }
    g_bDetectWork = true;
}

void DlgInfo::Refresh()
{
}

void DlgInfo::sltExeced(int nResult)
{
    TRACEDEBUG( "DlgInfo::sltExeced() nResult<%d> ", nResult );

    m_thread.stop();
    if (ERRDS_OK == nResult)
    {
        int nData = (*((int*)m_pRecvData));
        TRACEDEBUG( "DlgInfo::sltExeced() nData<%d> ", nData );
        switch ( m_execCmdParam.infoType )
        {
        case RESULT_INFO_TYPE_RESTORE_DEFAULT:
        {
            if (SET_PARAM_OK == nData)
            {
                m_type = POPUP_TYPE_INFOR_ALIVE;
                setContentText( m_execCmdParam.strResultYes );
            }
            else
            {
                m_type = POPUP_TYPE_SETFAILED;
                m_strContent = m_execCmdParam.strResultNot;
                QIcon icon = qApp->style()->standardIcon(
                            QStyle::SP_MessageBoxInformation,
                            0,
                            this
                            );
                setIconContentOK( icon );
            }
        }
            break;

        case RESULT_INFO_TYPE_UPDATE_RECT:
        {
            // 成功返回：零或正整数，表示升级成功升级模块个数，0,1,2,3，，，
            // 失败返回：-1，表示升级失败；-2，表示通讯超时；-3，表示打开文件失败
            if (nData >= 0)
            {
                m_type = POPUP_TYPE_INFOR;
                m_strContent =
                        tr( "Update OK Num" ) + ": "+
                        QString::number(nData);
            }
            else
            {
                m_type = POPUP_TYPE_SETFAILED;
                switch ( nData )
                {
                case -3:
                    m_strContent = tr( "Open File Failed" );
                    break;

                case -2:
                    m_strContent = tr( "Comm Time-Out" );
                    break;

                default:
                    m_strContent = m_execCmdParam.strResultNot;
                    break;
                }
            }
            QIcon icon = qApp->style()->standardIcon(
                        QStyle::SP_MessageBoxInformation,
                        0,
                        this
                        );
            setIconContentOK( icon );
        }
            break;

        default:
        {
            if (SET_PARAM_OK == nData)
            {
                m_type = POPUP_TYPE_INFOR;
                m_strContent = m_execCmdParam.strResultYes;
            }
            else
            {
                m_type = POPUP_TYPE_SETFAILED;
                m_strContent = m_execCmdParam.strResultNot;
            }
            QIcon icon = qApp->style()->standardIcon(
                        QStyle::SP_MessageBoxInformation,
                        0,
                        this
                        );
            setIconContentOK( icon );
        }
            break;
        }
    }
    else
    {
        m_type = POPUP_TYPE_SETFAILED;
        m_strContent = m_execCmdParam.strResultNot;
        QIcon icon = qApp->style()->standardIcon(
                    QStyle::SP_MessageBoxInformation,
                    0,
                    this
                    );
        setIconContentOK( icon );
    }

    TRACEDEBUG( "DlgInfo::sltExeced()" );

//    if ( m_timerId )
//    {
//        killTimer( m_timerId );
//        m_timerId = 0;
//    }
//    this->hide();
}

void DlgInfo::execShell(const char* strCmd, int nDelayTime)
{
    strcpy(m_szCmd, strCmd);

    QTimer::singleShot( nDelayTime, this, SLOT(sltTimerHandlerExecShell()) );
}

void DlgInfo::execSetCmd(ExecCmdParam_t &param)
{
    m_execCmdParam  = param;

    setCtrlVisible( false );
    m_type          = POPUP_TYPE_QUESTION;
    m_nTimeOut      = m_execCmdParam.nDlgTimeOut;
    m_strContent    = m_execCmdParam.strQuestion;
    InitWidget();
}

void DlgInfo::setLanguageParam(LANGUAGE_TYPE langType, LANGUAGE_LOCATE langLocate)
{
    m_langType   = langType;
    m_langLocate = langLocate;
}

void DlgInfo::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void DlgInfo::sltTimerHandlerExecShell()
{
    TRACEDEBUG( "DlgInfo::sltTimerHandlerExecShell ShellExec<%s>", m_szCmd );
    system( m_szCmd );
}

void DlgInfo::sltTimerHandlerExecSetCmd()
{
    TRACEDEBUG( "DlgInfo::sltTimerHandlerExecSetCmd" );
    m_pRecvData = NULL;
    int nRetSet = data_getDataSync(
                &(m_execCmdParam.cmdItem),
                &m_pRecvData,
                40 );
    if ( !m_execCmdParam.bAutoDestroy )
    {
        if ( m_execCmdParam.bShowResult )
        {
            if ( nRetSet==ERRDS_OK &&
                 SET_PARAM_OK==*((int*)m_pRecvData) )
            {
                m_type = POPUP_TYPE_INFOR;
                m_strContent = m_execCmdParam.strResultYes;
            }
            else
            {
                m_type = POPUP_TYPE_SETFAILED;
                m_strContent = m_execCmdParam.strResultNot;
            }
            QIcon icon = qApp->style()->standardIcon(
                        QStyle::SP_MessageBoxInformation,
                        0,
                        this
                        );
            setIconContentOK( icon );
        }
    }
}

void DlgInfo::sltTimerHandlerChangeLang()
{
    TRACEDEBUG( "DlgInfo::sltTimerHandlerChangeLang" );
    g_cfgParam->writeLanguageApp();
    g_cfgParam->writeLanguageLocal();
    system( "reboot" );
}

void DlgInfo::timerEvent(QTimerEvent * event)
{
    int id = event->timerId();
    if(id == m_timerId)
    {
        TRACEDEBUG( "DlgInfo::timerEvent time out" );
        reject();
    }
}

void DlgInfo::keyPressEvent(QKeyEvent* keyEvent)
{
    if (m_type == POPUP_TYPE_INFOR_ALIVE)
    {
        return;
    }

    switch ( keyEvent->key() )
    {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        TRACELOG1( "DlgInfo::keyPressEvent enter" );
        accept();
        break;

    default:
        break;
    }

    return QDialog::keyPressEvent(keyEvent);
}

void DlgInfo::setCtrlVisible(bool bVisible)
{
    ui->tableWidget->setVisible( bVisible );

//    ui->label_icon->setVisible( bVisible );
    ui->label_content->setVisible( bVisible );

    ui->label_OK->setVisible( bVisible );
    ui->label_Cancel->setVisible( bVisible );
}

void DlgInfo::setContentText(QString &strText)
{
    ui->label_content->setVisible( true );
    ui->label_content->setText( strText );
    // 1.8寸垂直最多13个字符
    //ui->label_content->setText( "WWWWWWWWWWWWW" );
}

void DlgInfo::setIconContentOK( QIcon &icon )
{
    SET_BACKGROUND_WIDGET( "DlgInfo",PosBase::strImgBack_Title );
    ui->label_content->setAlignment (Qt::AlignLeft);
    ui->label_content->setGeometry(
          PosDlgInfo::contentX,
          PosDlgInfo::contentY,
          PosDlgInfo::contentWidth,
          PosDlgInfo::labelHeight
                );
    ui->label_OK->setGeometry(
          PosDlgInfo::contentX,
          PosDlgInfo::cancelY,
          PosDlgInfo::cancelWidth,
          PosDlgInfo::labelHeight
                );

//    ui->label_icon->setVisible( true );
//    QSize iconSize = icon.actualSize( QSize(PosDlgInfo::iIcon_Size, PosDlgInfo::iIcon_Size) );
//    ui->label_icon->setPixmap( icon.pixmap(iconSize) );
    setContentText( m_strContent );
    ui->label_OK->setAlignment( Qt::AlignLeft );
    ui->label_OK->setText( tr("ENT or ESC to exit") );
    ui->label_OK->setVisible( true );

    if (m_nTimeOut > 0)
    {
        m_timerId = startTimer( m_nTimeOut );
    }
}

void DlgInfo::accept()
{
    switch (m_type)
    {
    case POPUP_TYPE_INFOR_ALIVE:
    {
        return;
    }

    case POPUP_TYPE_QUESTION_LANGUAGE:
    {
        TRACEDEBUG( "DlgInfo::accept POPUP_TYPE_QUESTION_LANGUAGE" );
        if ( m_timerId )
        {
            killTimer( m_timerId );
            m_timerId = 0;
        }
        m_type = POPUP_TYPE_INFOR_ALIVE;
        setCtrlVisible( false );
        ui->label_content->setGeometry(
                    PosDlgInfo::dlgInfoX,
                    PosDlgInfo::dlgInfoHeight/2-PosDlgInfo::labelHeight,
                    PosDlgInfo::contentWidth,
                    PosDlgInfo::labelHeight
                    );
        QString str = tr("Rebooting");
        setContentText( str );

        g_cfgParam->ms_initParam.langApp =
                m_langType;
        g_cfgParam->ms_initParam.langLocate =
                m_langLocate;
        QTimer::singleShot( 100, this, SLOT(sltTimerHandlerChangeLang()) );
//        g_cfgParam->writeLanguageApp();
//        g_cfgParam->writeLanguageLocal();
//        system( "reboot" );
        return;
    }

    default:
        break;
    }

    TRACEDEBUG( "DlgInfo::accept m_execCmdParam.popupType<%d>", m_execCmdParam.popupType );
    if (m_execCmdParam.popupType == POPUP_TYPE_QUERY_CMDEXEC)
    {
        if (++m_execCmdParam.nKeyEnter < 2)
        {
            if ( m_timerId )
            {
                killTimer( m_timerId );
                m_timerId = 0;
            }
            setCtrlVisible( false );
            m_type          = POPUP_TYPE_INFOR_ALIVE;
            setContentText( m_execCmdParam.strInfo );
            m_pRecvData = NULL;

            setFocus();
            m_thread.setParam(
                        &(m_execCmdParam.cmdItem),
                        &m_pRecvData,
                        m_execCmdParam.infoType
                        );
            m_thread.wait();
            m_thread.start();
            return;
        }
    }

    TRACELOG1( "DlgInfo::accept" );

    Leave();
    QDialog::accept();
}

void DlgInfo::reject()
{
    TRACELOG1( "DlgInfo::reject" );
    Leave();
    QDialog::reject();
}

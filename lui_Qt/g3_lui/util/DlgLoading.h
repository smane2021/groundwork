#ifndef DLGLOADING_H
#define DLGLOADING_H

#include <QDialog>
#include <QThread>
#include "dataDef.h"

class ThreadQueryData : public QThread
{
    Q_OBJECT
public:
    explicit ThreadQueryData();

public:
    void setParam(CmdItem *pCmdItem, void** ppData);
    void run();
    void stop();

signals:
    void sigReadedData();

private:
    volatile bool   m_bStopped;
    CmdItem*        m_pCmdItem;
    void**          m_ppData;
};


namespace Ui {
class DlgLoading;
}

class DlgLoading : public QDialog
{
    Q_OBJECT
    
public:
    explicit DlgLoading(QWidget *parent = 0);
    ~DlgLoading();

public:
    void start(CmdItem *pCmdItem, void** ppData);

private slots:
    void stop();

public slots:
    virtual void accept();
    virtual void reject();

protected:
    virtual void timerEvent(QTimerEvent *event);
    virtual void keyPressEvent(QKeyEvent *keyEvent);

private:
    int             m_timerId;
    int             m_idxPixmap;
    QPixmap         m_pixmap[6];
    ThreadQueryData m_thread;
    
private:
    Ui::DlgLoading *ui;
};

#endif // DLGLOADING_H

#ifndef THREADFLASHLIGHT_H
#define THREADFLASHLIGHT_H

#include <QThread>

class ThreadFlashLight : public QThread
{
    Q_OBJECT
public:
    explicit ThreadFlashLight();
    
public:
    void run();
    void stop();

//signals:
//    void sigReadedData();

private:
    volatile bool   m_bStopped;
};

#endif // THREADFLASHLIGHT_H

#include "DlgUpdateApp.h"
#include "ui_DlgUpdateApp.h"

#include "config/PosDlgInfo.h"
#include "common/pubInclude.h"

DlgUpdateApp::DlgUpdateApp(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgUpdateApp)
{
    ui->setupUi(this);

    sys_setLCD( LCD_MAX );
    InitWidget();
    TRACEDEBUG( "DlgUpdateApp::DlgUpdateApp" );

    m_timerId    = startTimer( 500 );
}

DlgUpdateApp::~DlgUpdateApp()
{
    delete ui;
}

void DlgUpdateApp::InitWidget()
{
    setWindowFlags( Qt::FramelessWindowHint );

    setGeometry(
                PosDlgInfo::dlgInfoX,
                PosDlgInfo::dlgInfoY,
                PosDlgInfo::dlgInfoWidth,
                PosDlgInfo::dlgInfoHeight
                );

    ui->label_content->setGeometry(
                PosDlgInfo::dlgInfoX,
                PosDlgInfo::dlgInfoHeight/2,
                PosDlgInfo::contentWidth,
                PosDlgInfo::labelHeight
                );

    ui->label_content->setAlignment( Qt::AlignCenter );
    ui->label_content->setText( tr("Qt Exited") + "..." );
}

void DlgUpdateApp::timerEvent(QTimerEvent *event)
{
    TRACEDEBUG( "DlgUpdateApp::timerEvent" );
    if(event->timerId() == m_timerId)
    {
        killTimer( m_timerId );
        m_timerId = 0;
        exit( 0 );
    }
}

void DlgUpdateApp::accept()
{
    TRACEDEBUG( "DlgUpdateApp::accept" );
}

void DlgUpdateApp::reject()
{
    TRACEDEBUG( "DlgUpdateApp::reject" );
}

#ifndef DLGLOGIN_H
#define DLGLOGIN_H

#include <QDialog>
#include <QTimer>
#include "common/InputCtrlDef.h"
#include "common/uidefine.h"

namespace Ui {
class DlgLogin;
}

class DlgLogin : public QDialog
{
    Q_OBJECT
    
public:
    explicit DlgLogin(LOGIN_TYPE type, QWidget *parent = 0);
    ~DlgLogin();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();
    virtual void ShowData(void* pData);

protected:
    virtual void InitWidget();
    virtual void InitConnect();
    virtual void changeEvent(QEvent* event);
    //virtual void timerEvent(QTimerEvent * event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);
    //bool eventFilter(QObject *obj, QEvent *event);

signals:
    void goToHomePage();
    void goToBaseWindow(enum WIDGET_TYPE);

private slots:
    void FocusTableWdg(enum INPUT_TYPE);
    void sltTableKeyPress(int key);
    void sltTimeOut();
    
public slots:
    virtual void accept();
    virtual void reject();

private:
    QTimer   m_timer;
    //int      m_timerId;
    CmdItem  m_cmdItem;
    USER_INFO_STRU	m_Users[MAX_USERS];
    LOGIN_TYPE      m_type;
    int      m_nSelRow;

private:
    Ui::DlgLogin *ui;
};

#endif // DLGLOGIN_H

#ifndef DLGINFO_H
#define DLGINFO_H

#include <QDialog>
#include <QThread>
#include <QTimer>
#include "common/uidefine.h"
#include "dataDef.h"

#define KEY_TIME_OUT ( 0 )

enum RESULT_INFO_TYPE
{
    RESULT_INFO_TYPE_NORMAL,
    RESULT_INFO_TYPE_RESTORE_DEFAULT,
    RESULT_INFO_TYPE_UPDATE_RECT
};

typedef struct _ExecCmdParam
{
    enum RESULT_INFO_TYPE infoType;
    QString strQuestion; // e.g sure to clear?
    int     nDlgTimeOut; // dialog auto destroy when time out
    enum POPUP_TYPE popupType;
    int nKeyEnter;
    QString strInfo;
    CmdItem cmdItem;
    bool    bAutoDestroy;
    bool    bShowResult;
    QString strResultYes;
    QString strResultNot;
} ExecCmdParam_t;

class ThreadWork : public QThread
{
    Q_OBJECT
public:
    explicit ThreadWork();

public:
    void setParam(
            CmdItem *pCmdItem,
            void** ppData,
            enum RESULT_INFO_TYPE infoType);
    void run();
    void stop();

signals:
    void sigExeced(int nResult);

private:
    enum RESULT_INFO_TYPE m_infoType;
    volatile bool   m_bStopped;
    CmdItem*        m_pCmdItem;
    void**          m_ppData;
};

namespace Ui {
class DlgInfo;
}

class DlgInfo : public QDialog
{
    Q_OBJECT
    
public:
    explicit DlgInfo(QString strContent="",
                     enum POPUP_TYPE type=POPUP_TYPE_GUIDE_WINDOW,
                     int nTimeOut=-1,
                     QWidget *parent = 0);
    ~DlgInfo();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();
    void execShell(const char* strCmd, int nDelayTime=100);
    void execSetCmd(ExecCmdParam_t &param);
    void setLanguageParam(LANGUAGE_TYPE langType, LANGUAGE_LOCATE langLocate);

protected:
    virtual void InitWidget();
    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent * event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);

private:
    void setCtrlVisible(bool bVisible);
    void setContentText(QString &strText);
    void setIconContentOK(QIcon &icon);

private slots:
    void sltTimerHandlerExecShell();
    void sltTimerHandlerExecSetCmd();
    void sltTimerHandlerChangeLang();
    void sltExeced(int nResult);

public slots:
    virtual void accept();
    virtual void reject();
    
public:
    void*    m_pRecvData;

private:
    enum POPUP_TYPE m_type;
    int      m_timerId;
    QString  m_strContent;
    int      m_nTimeOut;

    char     m_szCmd[100];
    CmdItem  m_cmdItem;

    ThreadWork m_thread;
    ExecCmdParam_t m_execCmdParam;

    LANGUAGE_TYPE   m_langType;
    LANGUAGE_LOCATE m_langLocate;

private:
    Ui::DlgInfo *ui;
};

#endif // DLGINFO_H

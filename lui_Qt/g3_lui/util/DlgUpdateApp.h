#ifndef DLGUPDATEAPP_H
#define DLGUPDATEAPP_H

#include <QDialog>

namespace Ui {
class DlgUpdateApp;
}

class DlgUpdateApp : public QDialog
{
    Q_OBJECT
    
public:
    explicit DlgUpdateApp(QWidget *parent = 0);
    ~DlgUpdateApp();

public slots:
    virtual void accept();
    virtual void reject();

protected:
    virtual void InitWidget();
    virtual void timerEvent(QTimerEvent *event);

private:
    int             m_timerId;

private:
    Ui::DlgUpdateApp *ui;
};

#endif // DLGUPDATEAPP_H

/******************************************************************************
文件名：    DlgLogin.cpp
功能：      user login
作者：      刘金煌
创建日期：   2013年6月25日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "DlgLogin.h"
#include "ui_DlgLogin.h"

#include <QKeyEvent>
#include "config/configparam.h"
#include "util/DlgInfo.h"
#include "util/timedialog.h"
#include "common/pubInclude.h"
#include "common/buzztablewidget.h"
#include "common/CtrlInputChar.h"

#define ROWIDX_USERNAME 2
#define ROWIDX_PASSWORD 4

DlgLogin::DlgLogin(LOGIN_TYPE type, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgLogin)
{
    ui->setupUi(this);
    SET_GEOMETRY_WIDGET( this );
    InitWidget();//INIT_WIDGET;
    InitConnect();

    m_cmdItem.CmdType   = CT_READ;
    m_cmdItem.ScreenID  = SCREEN_ID_DlgLogin;

    m_type = type;
    TRACELOG1( "DlgLogin::DlgLogin m_type<%d>", m_type );

    Enter();
}

DlgLogin::~DlgLogin()
{
    delete ui;
}

void DlgLogin::InitWidget()
{
    setWindowFlags( Qt::FramelessWindowHint );
    SET_BACKGROUND_WIDGET( "back_line.png" );

    int nRows = 5;
    SET_TABLEWDG_STYLE( tableWidget, TT_NOT_TITLE_NOT_SCROOL );
    ui->tableWidget->setRowCount( nRows );
    for (int i=0; i<nRows; ++i)
    {
        ui->tableWidget->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
    }

    CtrlInputChar*    pCtrl  = NULL;
    QTableWidgetItem* item   = NULL;
    {
        item = new QTableWidgetItem;
        item->setText( "" );
        ui->tableWidget->setItem(1, 0, item);
        // select user
        CtrlInputParam ctrlParam;
        ctrlParam.ipt        = IPT_SBT_CUSTOM;
        ctrlParam.strListEnumText << "Admin" << "User";
        ctrlParam.strInit = "Admin";
        ctrlParam.nInit   = 0;
        ctrlParam.nMin       = 0;
        ctrlParam.nMax       = 1;
        ctrlParam.strPrefix  = "";
        ctrlParam.strSuffix  = "";
        pCtrl = new CtrlInputChar( this, &ctrlParam );
        //pCtrl->installEventFilter( this );
        connect( pCtrl, SIGNAL(escapeMe(enum INPUT_TYPE)),
                 this, SLOT(FocusTableWdg(enum INPUT_TYPE)) );
        ui->tableWidget->setCellWidget(ROWIDX_USERNAME, 0, pCtrl);
    }

    {
        item = new QTableWidgetItem;
        item->setText( "" );
        ui->tableWidget->setItem(3, 0, item);

        // Enter Password
        CtrlInputParam ctrlParam;
        ctrlParam.ipt        = IPT_SBT_CHAR_PW;
        pCtrl = new CtrlInputChar( this, &ctrlParam );
        connect( pCtrl, SIGNAL(escapeMe(enum INPUT_TYPE)),
                 this, SLOT(FocusTableWdg(enum INPUT_TYPE)) );
        connect( ui->tableWidget, SIGNAL(sigTableKeyPress(int)),
                pCtrl, SLOT(sltTableKeyPress(int)) );
        ui->tableWidget->setCellWidget(ROWIDX_PASSWORD, 0, pCtrl);
    }
}

void DlgLogin::InitConnect()
{
    connect( &m_timer, SIGNAL(timeout()),
             this, SLOT(sltTimeOut()) );

    connect( ui->tableWidget, SIGNAL(sigTableKeyPress(int)),
            this, SLOT(sltTableKeyPress(int)) );
}

void DlgLogin::Enter(void* param)
{
    TRACELOG1( "DlgLogin::Enter(void* param)" );
    Q_UNUSED( param );

    QTableWidgetItem* item;
    item = ui->tableWidget->item(1, 0);
    item->setText( tr("Select User:") );

    item = ui->tableWidget->item(3, 0);
    item->setText( tr("Enter Password:") );

    ui->tableWidget->setFocus();
    m_timer.setInterval( TIME_OUT_LOGIN );
    m_timer.start();

#ifdef Q_OS_LINUX
    static void *pData = NULL;
    if ( !data_getDataSync(&m_cmdItem, &pData, TIME_WAIT_SYNC) )
    {
        TRACELOG1( "data_getDataSync ScreenID<%06x> readed data", m_cmdItem.ScreenID );
        ShowData( pData );
    }
    else
    {
        pData = NULL;
        TRACELOG1( "data_getDataSync ScreenID<%06x> no data", m_cmdItem.ScreenID );
    }
#endif
}

void DlgLogin::Leave()
{
    TRACELOG1( "DlgLogin::Leave()" );
    //killTimer( m_timerId );
    m_timer.stop();
}

void DlgLogin::Refresh()
{
}

void DlgLogin::ShowData(void* pData)
{
    TRACELOG1( "DlgLogin::ShowData()" );
    PACK_PWD* info = (PACK_PWD*)pData;
    int nUserNum = info->iNum;
    memcpy(m_Users, info->UserInfo,
           sizeof(USER_INFO_STRU)*nUserNum );

    CtrlInputParam ctrlParam;
    ctrlParam.ipt        = IPT_SBT_CUSTOM;
    for(int i=0; i<nUserNum; ++i)
    {
        ctrlParam.strListEnumText << info->UserInfo[i].szUserName;
        TRACELOG1( "DlgLogin::ShowData user<%s> pw<%s>",
                   info->UserInfo[i].szUserName,
                   info->UserInfo[i].szPassword
                   );
    }
    ctrlParam.strInit    = QString(info->UserInfo[0].szUserName);
    ctrlParam.nInit      = 0;
    ctrlParam.nMin       = 0;
    ctrlParam.nMax       = nUserNum-1;
    ctrlParam.strPrefix  = "";
    ctrlParam.strSuffix  = "";
    CtrlInputChar* pCtrl = (CtrlInputChar*)
            (ui->tableWidget->cellWidget(ROWIDX_USERNAME, 0));
    pCtrl->setParam( &ctrlParam );

    ui->tableWidget->selectRow( ROWIDX_USERNAME );
}

void DlgLogin::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

//void DlgLogin::timerEvent(QTimerEvent * event)
//{
//    if(event->timerId() == m_timerId)
//    {
//        TRACELOG1( "DlgLogin::timerEvent time out" );
//        reject();
//    }
//}

void DlgLogin::keyPressEvent(QKeyEvent* keyEvent)
{
    TRACELOG1( "DlgLogin::keyPressEvent" );
    m_timer.stop();
    m_timer.start();
    switch ( keyEvent->key() )
    {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        TRACELOG1( "DlgLogin::keyPressEvent enter" );
    {
        m_nSelRow = ui->tableWidget->selectedRow();
        switch (m_nSelRow)
        {
        case ROWIDX_USERNAME: case ROWIDX_PASSWORD:
        {
            CtrlInputChar* pCtrl = (CtrlInputChar*)
                    (ui->tableWidget->cellWidget(m_nSelRow, 0));
            pCtrl->Reset();
            pCtrl->Enter();
        }
            break;

        default:
        {
            TRACELOG1( "DlgLogin::keyPressEvent case 1 default m_nSelRow<%d>",
                       m_nSelRow );
        }
            break;
        }
    }
        break;

    case Qt::Key_Escape:
        break;

    default:
        break;
    }

    return QDialog::keyPressEvent(keyEvent);
}


void DlgLogin::sltTableKeyPress(int key)
{
    int nSelRow = ui->tableWidget->selectedRow();
    TRACEDEBUG( "DlgLogin::sltTableKeyPress nSelRow<%d>", nSelRow );
    m_timer.stop();
    m_timer.start();
    switch (nSelRow)
    {
    case 0: case 1:
    {
        ui->tableWidget->selectRow( ROWIDX_USERNAME );
    }
        break;

    case 3:
    {
        switch ( key )
        {
        case Qt::Key_Down:
            //跳转到 password
            ui->tableWidget->selectRow( ROWIDX_PASSWORD );
            break;
        case Qt::Key_Up:
            //跳转到 username
            ui->tableWidget->selectRow( ROWIDX_USERNAME );
            break;
        }
    }
        break;

    default:
        //TRACELOG1( "DlgLogin::sltTableKeyPress default nRow<%d>", nRow );
        break;
    }
}

void DlgLogin::FocusTableWdg(enum INPUT_TYPE ipt)
{
    int nSelRow = ui->tableWidget->selectedRow();
    TRACEDEBUG( "DlgLogin::FocusTableWdg() row<%d>", nSelRow );
    if (ipt==IPT_SUBMIT && nSelRow==ROWIDX_PASSWORD)
    {
#ifdef TEST_GUI
    accept();
#else
        CtrlInputChar* pCtrl1 = (CtrlInputChar*)
                (ui->tableWidget->cellWidget(ROWIDX_USERNAME, 0));

        CtrlInputChar* pCtrl2 = (CtrlInputChar*)
                (ui->tableWidget->cellWidget(ROWIDX_PASSWORD, 0));

        CtrlInputRetval retVal1 = pCtrl1->getValue();
        CtrlInputRetval retVal2 = pCtrl2->getValue();
        if ( retVal2.strVal.compare(
                    m_Users[retVal1.idx].szPassword
                    ) == 0 )
        {
            if (m_type == LOGIN_TYPE_CONFIG)
            {
                emit goToBaseWindow( WT2_CFG_SETTING );
            }

            accept();
        }
        else
        {
            DlgInfo dlg(
                        tr("Password err!"),
                        POPUP_TYPE_CRITICAL
                        );
            dlg.exec();
        }
        TRACELOG1( "DlgLogin::FocusTableWdg nVal<%d> user<%s> pwd<%s>",
                   retVal1.idx,
                   retVal1.strVal.toUtf8().constData(),
                   retVal2.strVal.toUtf8().constData()
                   );
#endif
    }
    ui->tableWidget->selectRow( ROWIDX_PASSWORD );
    ui->tableWidget->setFocus();
    TRACEDEBUG( "DlgLogin::FocusTableWdg()" );
}

void DlgLogin::sltTimeOut()
{
    TRACEDEBUG( "DlgLogin::sltTimeOut" );
    reject();
}

void DlgLogin::accept()
{
    TRACELOG1( "DlgLogin::accept" );
    Leave();
//    if (m_type == LOGIN_TYPE_CONFIG)
//    {
//        emit goToBaseWindow( WT2_CFG_SETTING );
//    }
    QDialog::accept();
}

void DlgLogin::reject()
{
    TRACELOG1( "DlgLogin::reject m_type<%d>", m_type );
    Leave();
    QDialog::reject();
}


//bool DlgLogin::eventFilter(QObject *obj, QEvent *event)
//{
//    TRACEDEBUG( "DlgLogin::eventFilter obj<%x> event type<%d>", obj, event->type() );
//    if (event->type() == QEvent::KeyPress)
//    {
//        QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
//        TRACEDEBUG("DlgLogin::eventFilter key press %d", keyEvent->key());
//    }
//    return QObject::eventFilter(obj, event);
//}

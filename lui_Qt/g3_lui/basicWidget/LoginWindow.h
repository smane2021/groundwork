#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H

#include <QTimer>
#include "common/basicwidget.h"
#include "common/InputCtrlDef.h"
#include "common/uidefine.h"

namespace Ui {
class LoginWindow;
}

class LoginWindow : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit LoginWindow(QWidget *parent = 0);
    ~LoginWindow();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();

protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();

    //virtual void timerEvent(QTimerEvent* event);

    virtual void changeEvent(QEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);

private:
    void accept();
    void reject();

signals:
    void goToGuideWindow(enum WIDGET_GUIDE);
    void goToHomePage();
    void goToBaseWindow(enum WIDGET_TYPE);

private slots:
    void FocusTableWdg(enum INPUT_TYPE);
    void sltTableKeyPress(int key);
    void sltTimeOut();
    void sltCellUnitKeyPress(int nKey);

    void on_tableWidget_itemSelectionChanged();

private:
    //int      m_timerId;
    QTimer   m_QTimer;
    CmdItem  m_cmdItem;
    USER_INFO_STRU	m_Users[MAX_USERS];
    LOGIN_TYPE      m_type;
    int      m_nSelRow;
    int      m_nSelRowOld;

private:
    Ui::LoginWindow *ui;
};

#endif // LOGINWINDOW_H

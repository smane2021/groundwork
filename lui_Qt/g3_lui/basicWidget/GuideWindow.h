#ifndef GUIDEWINDOW_H
#define GUIDEWINDOW_H

#include "common/basicwidget.h"
#include "common/InputCtrlDef.h"
#include "common/uidefine.h"
#include "common/MultiDemon.h"
#include <QTime>
#include <QTimer>

using namespace InputCtrl;

#define MAX_CTRL_ITEMS_GUIDE (MAXLEN_SET)
// ��Ч�У������������� SIGIDX_INIT��������
#define SIGIDX_READONLY      (MAXLEN_SET+10)

namespace Ui {
class GuideWindow;
}

class GuideWindow : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit GuideWindow(QWidget *parent = 0);
    ~GuideWindow();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();
    bool createInputWidget(int nSpawn);

protected:
    void RefreshNow();
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void timerEvent(QTimerEvent* event);

    virtual void changeEvent(QEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);

private slots:
    void sltTableKeyPress(int key);
    void FocusTableWdg(enum INPUT_TYPE);
    void TimerHandler(void);

    void on_tableWidget_input_itemSelectionChanged();

signals:
    void goToHomePage();
    void goToBaseWindow(enum WIDGET_TYPE);
    void sigDetectAlarm(); // �����澯
    void goToLoginWindow(enum LOGIN_TYPE);

private:
    void modifyDHCPItem(CtrlInputChar* pCtrl, int nSelRow, bool bAdd);
    void restartQTimer(int msec=0);
    void setTitleByScreenID(int nScreenID);
    void setTableItemDateTime(SIG_TIME time_t);
    void setTableItemIP(ULONG ulIP, int nSigIdx);
    void clearTxtInputTable();
    void keyPressWizTable(int key);
    bool telledAppLang();
    // tell app language; goto wizard or homepage
    int  enterWhere();
    void enterHomePage();
    void enterWizard();
    int Lcd_GetPaswdChangeFlag();

public:
    static bool ms_bInEnter;

private:
    int      m_timerId;
    int      m_timerIdOpenFrameBuff;
    QTimer   m_QTimer;
    CmdItem  m_cmdItem;
    int      m_sigNum;
    // total rows count-1
    int      m_nRows;
    // indicate sig index of SET_INFO
    int      m_nSigIdx[MAX_CTRL_ITEMS_GUIDE*2];
    enum WIDGET_GUIDE m_wt; // ��ǰ��ʾ��ҳ��
    QString       m_pixmap[22];
    enum LANGUAGE_LOCATE m_LangLocate;

    // create ctrlInput one by one
    int      m_timerIdSpawn;
    // input ctrl widget number created
    int      m_nCtrlInputCreated;
    int      m_nSelRowOld;
    int      m_nWaitAppdata;
    QStringList m_strListLangType;
     int      m_nLastScreenID; //����ʱ����¼����ҳ����������Ϊ0ʱ����Ҫ������ĳҳ�������磺���ط�����0ʱ���Ͳ�����ת����ҳ��
private:
     Ui::GuideWindow *ui;


};

#endif // GUIDEWINDOW_H

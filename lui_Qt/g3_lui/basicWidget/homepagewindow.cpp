/******************************************************************************
文件名：    homepagewindow.cpp
功能：      LUI的主页窗口
作者：      刘金煌
创建日期：   2013年03月25日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "basicWidget/homepagewindow.h"
#include "ui_homepagewindow.h"

#include <QDateTime>
#include <QKeyEvent>
#include <QtGlobal>
#include "mainwindow.h"
#include "config/configparam.h"
#include "config/PosHomepage.h"
#include "util/DlgLogin.h"
#include "common/pubInclude.h"
#include "common/global.h"
#include "common/uidefine.h"
#include "configWidget/Wdg2P9Cfg.h"

enum HOMEPAGE_TYPE HomePageWindow::ms_homePageType;

HomePageWindow::HomePageWindow(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::HomePageWindow)
{
    ui->setupUi(this);

    SET_GEOMETRY_WIDGET(this);

    InitWidget();
    InitConnect();

    TRACELOG1( "HomePageWindow::HomePageWindow width*height <%d*%d>",
               width(), height()
               );

    m_timerId = 0;
    m_nInterTimer     = TIMER_UPDATE_DATA_INTERVAL/5;
    m_nInterDatetime  = 1*1000/m_nInterTimer;
    m_nInterData      = 2*1000/m_nInterTimer;
    m_nInterRollingLine = 400/m_nInterTimer;
    m_nInterRollingBatt = 800/m_nInterTimer;

    g_bLogin = false;
}

HomePageWindow::~HomePageWindow()
{
    delete ui;
}

void HomePageWindow::InitWidget()
{
    ui->label_lineH1->clear();
    ui->label_lineH2->clear();
    ui->label_lineV->clear();
    ui->dcParamLabel->clear();
    ui->battParamLabel->clear();
    ui->label_sysused->clear();
    ui->label->clear();

    ui->dateLabel->setGeometry(
                PosHomepage::rectLabelDateTime
                );
    ui->dcParamLabel->setGeometry(
                PosHomepage::rectLabelDC
                );
    ui->dcParamLabel->setAlignment( Qt::AlignCenter );

    ui->battParamLabel->setGeometry(
                PosHomepage::rectLabelBatt
                );
    ui->label_sysused->setGeometry(
                PosHomepage::rectLabelSysused
                );
    ui->label_lineH1->setGeometry(
                PosHomepage::rectLineH1
                );
    ui->label_lineH2->setGeometry(
                PosHomepage::rectLineH2
                );
    ui->label_lineV->setGeometry(
                PosHomepage::rectLineV
                );
    ui->label->setGeometry (PosHomepage::rectAlarmNum);
    // slave
    ui->label_slaveV->clear();
    ui->label_slaveA->clear();
    ui->label_slaveV->setGeometry(
                PosHomepage::rectSlaveLabelV
                );
    ui->label_slaveA->setGeometry(
                PosHomepage::rectSlaveLabelA
                );

    SET_BACKGROUND_WIDGET( "HomePageWindow",PosBase::strImgBack_Title );

    // 初始化图标数据
    InitButtonData();
    // 创建图标icon
    InitButton();

    ui->BattEquipBtn->setFont(ConfigParam::gFontSmallN);

    ui->label_lineH1->setPixmap( m_pixHLine[0] );
    ui->label_lineH2->setPixmap( m_pixHLine[0] );
    ui->label_lineV->setPixmap(  m_pixVLine[0] );

    for (int i=1; i<=5; ++i)
    {
        QString strTmp;
        strTmp.sprintf(
                    "QPushButton{"
                    "color:#ffffff;"
                    "border-image:url(%s"
                    "hp_btnbat_%d_n.png);"
                    "border-style:flat;}"
                    "QPushButton:focus{"
                    "border-image:url(%s"
                    "hp_btnbat_%d_f.png);"
                    "padding: -1;}",
                    PATH_IMG.toUtf8().constData(),
                    i,
                    PATH_IMG.toUtf8().constData(),
                    i
                    );
        m_slBattImg << strTmp;
    }
    m_pWdgFocus = ui->ActAlmBtn;
}

void HomePageWindow::testGUI()
{
    //TRACEDEBUG( "HomePageWindow::testGUI()" );
    PACK_INFO data[20];
    PACK_INFO* info = (PACK_INFO*)data;
    // Alarm Status
    // 告警
    info->vSigValue.lValue = 0;

    // RectNum ac CM
    info++;
    info->vSigValue.lValue = 1;

    // MPPTNum solar MM
    info++;
    info->vSigValue.lValue = 1;

    // DG Existence
    info++;
    info->vSigValue.lValue = 0;

    // Mains failure 线条
    info++;

    // DG Running
    info++;

    // Solar Running
    info++;

    // BattStat
    info++;

    // DCVolt
    info++;
    info->iFormat = 1;
    info->vSigValue.fValue = 220;
    strcpy(info->cSigUnit, "V");

    // Current
    info++;
    info->iFormat = 1;
    info->vSigValue.fValue = 5;
    strcpy(info->cSigUnit, "A");

    // BattCap
    info++;
    info->vSigValue.fValue = 60;
    info->iFormat = 1;
    strcpy(info->cSigUnit, "%");

    // SysUsed
    info++;
    strcpy(info->cSigName, "Sys Cap Used");
    info->iFormat = 1;
    info->vSigValue.fValue = 50;
    strcpy(info->cSigUnit, "%");

    // BattCurrent
    //浮点数,大于5A则显示充电动画 小于-5A则显示放电动画
    info++;
    // 充电状态
    info->vSigValue.fValue = 4;

    ShowData( data );
}

void HomePageWindow::InitConnect()
{
}

void HomePageWindow::Enter(void*)
{
    TRACEDEBUG( "HomePageWindow::Enter" );

    INIT_VAR;

    g_nLastWTNokey       = WT_HOME_WINDOW;
    g_nLastWTScreenSaver = g_nLastWTNokey;

    m_nPowerType   = POWER_SOURCE_AS;
    m_nBattChargeState = CHARGE_STATE_FLOAT;
    m_idxIconBatt  = 0;

    ms_homePageType = g_cfgParam->ms_initParam.homePageType;
    m_cmdItem.CmdType   = CT_READ;
    m_cmdItem.ScreenID  = SCREEN_ID_HomePageWindow;

    setWdgVisible();

    UpdateTimeLabel();

#ifdef TEST_GUI
        testGUI();
#else
    TRY_GET_DATA_MULTY;
#endif
    m_timerId = startTimer( m_nInterTimer );
    m_pWdgFocus->setFocus();
}

void HomePageWindow::Leave()
{
    LEAVE_WDG( "HomePageWindow" );

    m_pWdgFocus = this->focusWidget();
}

void HomePageWindow::Refresh()
{
    // 滚动线条 电池
    rollingLine();
    rollingBatt();

    static int nUpdateTime = 0;
    nUpdateTime++;
    // 更新时间 1s更新一次
    if (nUpdateTime > m_nInterDatetime)
    {
        nUpdateTime = 0;
        UpdateTimeLabel();
    }

    // 更新数据
    static int nUpdateData = 0;
    nUpdateData++;
    // 更新数据 2s更新一次
    if (nUpdateData > m_nInterData)
    {
        nUpdateData = 0;
#ifdef TEST_GUI
    testGUI();
#else
        //TRACEDEBUG( "HomePageWindow::Refresh() REFRESH_DATA_PERIODICALLY" );
        REFRESH_DATA_PERIODICALLY(
                    m_cmdItem.ScreenID,
                    "HomePageWindow"
                    );
#endif
    }
}

#include <QFile>
void HomePageWindow::ShowData(void* pData)
{
    if ( !pData )
        return;

    PACK_INFO* info = (PACK_INFO*)pData;
    // Alarm Status
    if (MainWindow::ms_bAlarmRed || info->vSigValue.lValue)
    {
        SET_BTN_STYLE(ActAlmBtn, PosHomepage::rectBtnAlarm,
                      hp_btnalarm_f, hp_btnalarm_n);
    }
    else
    {
        SET_BTN_STYLE(ActAlmBtn, PosHomepage::rectBtnAlarm,
                      hp_btnalarm0_f, hp_btnalarm0_n);
    }

    // RectNum ac
    info++;
    long CM = info->vSigValue.lValue;

    // MPPTNum solar
    info++;
    long MM = info->vSigValue.lValue;

    // DG Existence
    info++;
    long lDG = info->vSigValue.lValue;

    //Source number
    info++;
    long lSourceNum = info->vSigValue.ulValue;

    if (CM==1 || MM==0 || MM==1)
    {
        m_bAC = true;
    }
    if (CM==0 && MM==2)
    {
        m_bAC = false;
    }
    if (CM==0 && (MM==1 || MM==2))
    {
        m_bSolar = true;
    }
    if (CM==1 || MM==0)
    {
        m_bSolar = false;
    }
    m_bDG = (lDG == 0)||(lSourceNum > 0);

    if ( m_bAC )
    {
        if ( m_bSolar )
        {
            if ( m_bDG )
            {
                m_nPowerType = POWER_SOURCE_ASG;
            }
            else
            {
                m_nPowerType = POWER_SOURCE_AS;
            }
        }
        else if ( m_bDG )
        {
            m_nPowerType = POWER_SOURCE_AG;
        }
        else
        {
            m_nPowerType = POWER_SOURCE_AC;
        }
    }
    else
    {
        if ( m_bSolar )
        {
            if ( m_bDG )
            {
                m_nPowerType = POWER_SOURCE_GS;
            }
            else
            {
                m_nPowerType = POWER_SOURCE_SO;
            }
        }
        else
        {
            if ( m_bDG )
            {
                m_nPowerType = POWER_SOURCE_DG;
            }
            else
            {
                TRACEDEBUG( "HomePageWindow::ShowData() POWER_TYPE have no type" );
                m_nPowerType = POWER_SOURCE_AC;
            }
        }
    }

    // Mains failure 线条
    info++;

    // DG Running
    info++;

    // Solar Running
    info++;

    // BattStat
    info++;
    QString strBatt = QString(info->cEnumText);

    // DCVolt
    info++;
    QString strDC;
    int iFormat = info->iFormat;
    strDC.append( QString::number(info->vSigValue.fValue, FORMAT_DECIMAL) );
    strDC.append( info->cSigUnit );
    ui->label_slaveV->setText( strDC );

    // Current
    info++;
    QString strA;
    iFormat = info->iFormat;
    strA.append( QString::number(info->vSigValue.fValue, FORMAT_DECIMAL) );
    strA.append( info->cSigUnit );
    ui->dcParamLabel->setText( strDC + "\n" + strA );
    ui->label_slaveA->setText( strA );

    // BattCap
    info++;
    m_fBattCap = info->vSigValue.fValue;

    ui->battParamLabel->setWordWrap (true);
    if(g_cfgParam->ms_initParam.dispVersion == DISPLAY_VERSION_NORMAL)
    {
      iFormat = info->iFormat;
      strBatt = strBatt + '\n' + QString::number(m_fBattCap, FORMAT_DECIMAL) +
          QString(info->cSigUnit);
      ui->battParamLabel->setText(strBatt);
    }
    else
    {
      ui->battParamLabel->setText( "" );
    }

    // SysUsed
    info++;
    QString strSys;
    strSys.append( info->cSigName );
    strSys.append( ":" );
    iFormat = info->iFormat;
    strSys.append( QString::number(info->vSigValue.fValue, FORMAT_DECIMAL) );
    strSys.append( info->cSigUnit );
    ui->label_sysused->setAlignment (Qt::AlignCenter);
    ui->label_sysused->setText( strSys );

    // BattCurrent
    //浮点数,大于5A则显示充电动画 小于-5A则显示放电动画
    info++;

    // 充电状态
    float fBattCurrent = info->vSigValue.fValue;
    if (fBattCurrent > 5)
    {
        m_nBattChargeState = CHARGE_STATE_BOOST;
    }
    else if (fBattCurrent>=-5 && fBattCurrent<=5)
    {
        m_nBattChargeState = CHARGE_STATE_FLOAT;
    }
    else
    {
        m_nBattChargeState = CHARGE_STATE_DISCHARGE;
    }
    rollingBatt();

    // AC图标
    switch (m_nPowerType)
    {
      case POWER_SOURCE_AC:
      {
          SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
                        hp_btnac_a_f, hp_btnac_a_n );
      }
      break;

      case POWER_SOURCE_AS:
      {
          SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
                      hp_btnac_as_f, hp_btnac_as_n );
      }
      break;

      case POWER_SOURCE_AG:
      {
          SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
                      hp_btnac_ag_f, hp_btnac_ag_n );
      }
      break;

      case POWER_SOURCE_ASG:
      {
          SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
                      hp_btnac_asg_f, hp_btnac_asg_n );
      }
      break;

      case POWER_SOURCE_DG:
      {
          SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
                      hp_btnac_g_f, hp_btnac_g_n );
      }
      break;

      case POWER_SOURCE_GS:
      {
          SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
                      hp_btnac_gs_f, hp_btnac_gs_n );
      }
      break;

      case POWER_SOURCE_SO:
      {
          SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
                      hp_btnac_s_f, hp_btnac_s_n );
      }
      break;

      default:
      {
          SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
                      hp_btnac_a_f, hp_btnac_a_n );
      }
      break;
    }


    info++;
    int nOANum = info->vSigValue.ulValue;

    info++;
    int nMANum = info->vSigValue.ulValue;

    info++;
    int nCANum = info->vSigValue.ulValue;

    int nMaxNum= nCANum ? nCANum : (nMANum ? nMANum : (nOANum ? nOANum : 0));
    if(nMaxNum == 0)
    {
        // 活动告警按钮
        SET_BTN_STYLE(ActAlmBtn, PosHomepage::rectBtnAlarm,
                            hp_btnalarm0_f, hp_btnalarm0_n);
    }
    else if(nMaxNum == nCANum)
    {
        // 活动告警按钮
        SET_BTN_STYLE(ActAlmBtn, PosHomepage::rectBtnAlarm,
                      hp_btnalarm3_f, hp_btnalarm3_n);
    }
    else if(nMaxNum == nMANum)
    {
        // 活动告警按钮
        SET_BTN_STYLE(ActAlmBtn, PosHomepage::rectBtnAlarm,
                      hp_btnalarm2_f, hp_btnalarm2_n);
    }
    else if(nMaxNum == nOANum)
    {
        // 活动告警按钮
        SET_BTN_STYLE(ActAlmBtn, PosHomepage::rectBtnAlarm,
                      hp_btnalarm1_f, hp_btnalarm1_n);
    }

    ui->label->setText ("(" + QString::number(nMaxNum) + ")");
}

void HomePageWindow::timerEvent( QTimerEvent * event )
{
    if(event->timerId() == m_timerId)
    {
        //TRACEDEBUG( "HomePageWindow::timerEvent" );
        Refresh();
    }
}

// 滚动线条
void HomePageWindow::rollingLine()
{
    static int nInter;
    ++nInter;

    if(nInter > m_nInterRollingLine)
    {
        nInter = 0;
        static int nLine = -1;
        ++nLine;
        if(nLine > 2)
        {
            nLine = 0;
        }

        ui->label_lineH1->setPixmap( m_pixHLine[nLine] );
        ui->label_lineH2->setPixmap( m_pixHLine[nLine] );
        //TRACEDEBUG( "HomePageWindow::rollingLine() ChargeState<%d>", m_nBattChargeState );
        if (m_nBattChargeState == CHARGE_STATE_DISCHARGE)
        {
            ui->label_lineV->setPixmap( m_pixVLineRev[nLine] );
        }
        else
        {
            ui->label_lineV->setPixmap( m_pixVLine[nLine] );
        }
    }
}

// 滚动充电电池图标
// CHARGE_STATE_BOOST 充电
// CHARGE_STATE_FLOAT 不动，按容量显示图标
void HomePageWindow::rollingBatt()
{
    static int nInter;
    ++nInter;
    if(nInter > m_nInterRollingBatt)
    {
        nInter = 0;

        if (m_nBattChargeState == CHARGE_STATE_BOOST)
        {
            ++m_idxIconBatt;
            if(m_idxIconBatt > 4)
            {
                m_idxIconBatt = 0;
            }
        }
        else
        {
            if (m_fBattCap < 0.1)
            {
                m_idxIconBatt = 0;
            }
            else if (m_fBattCap <= 25)
            {
                m_idxIconBatt = 1;
            }
            else if (m_fBattCap > 25 && m_fBattCap <= 50)
            {
                m_idxIconBatt = 2;
            }
            else if (m_fBattCap > 50 && m_fBattCap <= 75)
            {
                m_idxIconBatt = 3;
            }
            else
            {
                m_idxIconBatt = 4;
            }
        }
        ui->BattEquipBtn->setStyleSheet( m_slBattImg[m_idxIconBatt] );
    }
}

/*更新时间*/
void HomePageWindow::UpdateTimeLabel()
{
    static bool bFlag = false;
    static int  nCount = 0;
    ++nCount;
    if (nCount > 4)
    {
        nCount = 0;
        bFlag = !bFlag;
    }
    QDateTime dt = QDateTime::currentDateTime();
    QString strDT;
    if (bFlag)
    {
        if(g_cfgParam->ms_initParam.lcdRotation == LCD_ROTATION_90DEG)
        {
            strDT = dt.toString( g_szHomePageDateFmtWithoutYear[
                               g_cfgParam->ms_initParam.timeFormat] );
        }
        else
        {
          strDT = dt.toString( g_szHomePageDateFmt[
                             g_cfgParam->ms_initParam.timeFormat] );
        }
    }
    else
    {
        strDT = dt.toString( FORMAT_TIEM );
    }

    ui->dateLabel->setText( strDT );
}

void HomePageWindow::InitButtonData()
{
    if(g_cfgParam->ms_initParam.lcdRotation == LCD_ROTATION_90DEG)
    {
        m_pixHLine[0].load( PATH_IMG + "hp_hline_1V.png" );
        m_pixHLine[1].load( PATH_IMG + "hp_hline_2V.png" );
        m_pixHLine[2].load( PATH_IMG + "hp_hline_3V.png" );

        m_pixVLine[0].load( PATH_IMG + "hp_vline_1V.png" );
        m_pixVLine[1].load( PATH_IMG + "hp_vline_2V.png" );
        m_pixVLine[2].load( PATH_IMG + "hp_vline_3V.png" );

        m_pixVLineRev[0].load( PATH_IMG + "hp_vline_rev_1V.png" );
        m_pixVLineRev[1].load( PATH_IMG + "hp_vline_rev_2V.png" );
        m_pixVLineRev[2].load( PATH_IMG + "hp_vline_rev_3V.png" );
    }
    else
    {
        m_pixHLine[0].load( PATH_IMG + "hp_hline_1.png" );
        m_pixHLine[1].load( PATH_IMG + "hp_hline_2.png" );
        m_pixHLine[2].load( PATH_IMG + "hp_hline_3.png" );

        m_pixVLine[0].load( PATH_IMG + "hp_vline_1.png" );
        m_pixVLine[1].load( PATH_IMG + "hp_vline_2.png" );
        m_pixVLine[2].load( PATH_IMG + "hp_vline_3.png" );

        m_pixVLineRev[0].load( PATH_IMG + "hp_vline_rev_1.png" );
        m_pixVLineRev[1].load( PATH_IMG + "hp_vline_rev_2.png" );
        m_pixVLineRev[2].load( PATH_IMG + "hp_vline_rev_3.png" );
    }
}

void HomePageWindow::InitButton()
{
    // 活动告警按钮
    SET_BTN_STYLE(ActAlmBtn, PosHomepage::rectBtnAlarm,
                  hp_btnalarm0_f, hp_btnalarm0_n);

    // 设置按钮
    SET_BTN_STYLE( ConfigBtn, PosHomepage::rectBtnCfg,
                   hp_btncfg_f, hp_btncfg_n );

    // AC按钮
    SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
                   hp_btnac_a_f, hp_btnac_a_n );
    // 模块按钮
    SET_BTN_STYLE( RectEquipBtn, PosHomepage::rectBtnModule,
                   hp_btnrect_f, hp_btnrect_n );
    // DC按钮
    SET_BTN_STYLE( DcEquipBtn, PosHomepage::rectBtnDC,
                   hp_btndc_f, hp_btndc_n );
    // 电池按钮
    SET_BTN_STYLE( BattEquipBtn, PosHomepage::rectBtnBatt,
                   hp_btnbat_5_f, hp_btnbat_5_n );
}

void HomePageWindow::setWdgVisible()
{
    if (ms_homePageType == HOMEPAGE_TYPE_SLAVE)
    {
        setWdgVisibleMain( false );
        //从机模式不显示  Modified by Minghui Wang
        setWdgVisibleSlave( false );
    }
    else
    {
        setWdgVisibleSlave( false );
        setWdgVisibleMain( true );
    }
}

void HomePageWindow::setWdgVisibleMain(bool bVisible)
{
    ui->AcEquipBtn->setVisible( bVisible );
    ui->BattEquipBtn->setVisible( bVisible );
    ui->DcEquipBtn->setVisible( bVisible );
    ui->RectEquipBtn->setVisible( bVisible );
    ui->battParamLabel->setVisible( bVisible );
    ui->dcParamLabel->setVisible( bVisible );
    ui->label_lineH1->setVisible( bVisible );
    ui->label_lineH2->setVisible( bVisible );
    ui->label_lineV->setVisible( bVisible );
    ui->label_sysused->setVisible( bVisible );
}

void HomePageWindow::setWdgVisibleSlave(bool bVisible)
{
    ui->label_slaveV->setVisible( bVisible );
    ui->label_slaveA->setVisible( bVisible );
}

void HomePageWindow::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);

        if (ms_showingWdg == this)
        {
            UpdateTimeLabel();
            TRY_GET_DATA_MULTY;
        }
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void HomePageWindow::on_ActAlmBtn_clicked()
{
    g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
    if (ms_homePageType != HOMEPAGE_TYPE_SLAVE)
    {
        emit goToBaseWindow(WT1_ALARM);
    }
}

void HomePageWindow::on_ConfigBtn_clicked()
{
    g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
    emit goToBaseWindow( WT2_CFG_SETTING );
}

//AC
void HomePageWindow::on_AcEquipBtn_clicked()
{
    g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
    TRACEDEBUG( "HomePageWindow::on_AcEquipBtn_clicked()" );
    emit goToBaseWindow(WT1_AC);
}

void HomePageWindow::on_RectEquipBtn_clicked()
{
    g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
    emit goToBaseWindow(WT1_MODULE);
}

void HomePageWindow::on_DcEquipBtn_clicked()
{
    g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
    emit goToBaseWindow(WT1_DCV);
}

void HomePageWindow::on_BattEquipBtn_clicked()
{
    g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
    emit goToBaseWindow(WT1_BATT_REMAIN_TIME);
}

void HomePageWindow::keyPressEvent(QKeyEvent* keyEvent)
{
    TRACEDEBUG( "HomePageWindow::keyPressEvent" );
    switch ( keyEvent->key() )
    {
        case Qt::Key_Escape:
        {
            emit goToBaseWindow(WT1_INVENTORY);
        }
        break;

        default:
        break;
    }

    QWidget::keyPressEvent (keyEvent);
}

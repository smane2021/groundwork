/******************************************************************************
文件名：    LoginWindow.cpp
功能：      user login
作者：      刘金煌
创建日期：   2013年8月26日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "LoginWindow.h"
#include "ui_LoginWindow.h"

#include <QKeyEvent>
#include "config/configparam.h"
#include "util/DlgInfo.h"
#include "common/pubInclude.h"
#include "common/buzztablewidget.h"
#include "common/CtrlInputChar.h"
#include "common/global.h"

#define ROWIDX_USERNAME 2
#define ROWIDX_PASSWORD 4

LoginWindow::LoginWindow(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::LoginWindow)
{
    ui->setupUi(this);

    InitWidget();
    InitConnect();

    m_cmdItem.CmdType   = CT_READ;
    m_cmdItem.ScreenID  = SCREEN_ID_DlgLogin;
}

LoginWindow::~LoginWindow()
{
    delete ui;
}


void LoginWindow::InitWidget()
{
    SET_GEOMETRY_WIDGET( this );
    SET_BACKGROUND_WIDGET( "LoginWindow",PosBase::strImgBack_Line );
    SET_TABLEWDG_STYLE( tableWidget, TT_NOT_TITLE_NOT_SCROOL );

    int nRows = 5;
    ui->tableWidget->setRowCount( nRows );
    for (int i=0; i<nRows; ++i)
    {
        ui->tableWidget->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
    }

    CtrlInputChar*    pCtrl  = NULL;
    QTableWidgetItem* item   = NULL;
    {
        item = new QTableWidgetItem;
        item->setText( "" );
        ui->tableWidget->setItem(1, 0, item);
        // select user
        CtrlInputParam ctrlParam;
        ctrlParam.ipt        = IPT_SBT_CUSTOM;
        ctrlParam.strListEnumText << "Admin" << "User";
        ctrlParam.strInit = "Admin";
        ctrlParam.nInit      = 0;
        ctrlParam.nMin       = 0;
        ctrlParam.nMax       = 1;
        ctrlParam.strPrefix  = "";
        ctrlParam.strSuffix  = "";
        pCtrl = new CtrlInputChar( this, &ctrlParam );
        //pCtrl->installEventFilter( this );
        connect( pCtrl,
                 SIGNAL(escapeMe(enum INPUT_TYPE)),
                 this,
                 SLOT(FocusTableWdg(enum INPUT_TYPE)) );

        connect( pCtrl,
                 SIGNAL(sigCellUnitKeyPress(int)),
                 this,
                 SLOT(sltCellUnitKeyPress(int)) );
        ui->tableWidget->setCellWidget(ROWIDX_USERNAME, 0, pCtrl);
    }

    {
        item = new QTableWidgetItem;
        item->setText( "" );
        ui->tableWidget->setItem(3, 0, item);

        // Enter Password
        CtrlInputParam ctrlParam;
        ctrlParam.ipt        = IPT_SBT_CHAR_PW;
        pCtrl = new CtrlInputChar( this, &ctrlParam );
        connect( pCtrl
                 , SIGNAL(escapeMe(enum INPUT_TYPE)),
                 this,
                 SLOT(FocusTableWdg(enum INPUT_TYPE))
                 );

        connect( pCtrl,
                 SIGNAL(sigCellUnitKeyPress(int)),
                 this,
                 SLOT(sltCellUnitKeyPress(int)) );
        ui->tableWidget->setCellWidget(ROWIDX_PASSWORD, 0, pCtrl);
    }
}

void LoginWindow::InitConnect()
{
    connect( &m_QTimer, SIGNAL(timeout()),
             this, SLOT(sltTimeOut()) );

    connect( ui->tableWidget, SIGNAL(sigTableKeyPress(int)),
            this, SLOT(sltTableKeyPress(int)) );
}

void LoginWindow::Enter(void* param)
{
    TRACELOG1( "LoginWindow::Enter(void* param)" );

    INIT_VAR;
    QTableWidgetItem* item;
    item = ui->tableWidget->item(1, 0);
    item->setText( tr("Select User") + ":" );

    item = ui->tableWidget->item(3, 0);
    item->setText( tr("Enter Password") + ":" );

    m_type = *(LOGIN_TYPE*)(param);

    ui->tableWidget->setFocus();
    m_QTimer.setInterval( TIME_OUT_LoginWindow );
    m_QTimer.start();

    m_nSelRowOld  = ROWIDX_USERNAME;
    g_nLastWTNokey       = WT_LOGIN_WINDOW;
    g_nLastWTScreenSaver = g_nLastWTNokey;

#ifdef TEST_GUI
    PACK_PWD info;
    info.iNum = 1;
    for (int i=0; i<info.iNum; ++i)
    {
        strcpy( info.UserInfo[i].szUserName, "Admin" );
        strcpy( info.UserInfo[i].szPassword, "0" );
    }
    ShowData( &info );
#else
    TRY_GET_DATA_MULTY;
#endif
}

void LoginWindow::Leave()
{
    TRACELOG1( "LoginWindow::Leave()" );
    //killTimer( m_timerId );
    m_QTimer.stop();
}

void LoginWindow::Refresh()
{
    ;
}

void LoginWindow::ShowData(void* pData)
{
    PACK_PWD* info = (PACK_PWD*)pData;
    int nUserNum = info->iNum;
    memcpy(m_Users, info->UserInfo,
           sizeof(USER_INFO_STRU)*nUserNum );

    CtrlInputParam ctrlParam;
    ctrlParam.ipt        = IPT_SBT_CUSTOM;
    for(int i=0; i<nUserNum; ++i)
    {
        ctrlParam.strListEnumText << info->UserInfo[i].szUserName;
        TRACELOG1( "LoginWindow::ShowData user<%s> pw<%s>",
                   info->UserInfo[i].szUserName,
                   info->UserInfo[i].szPassword
                   );
    }
    ctrlParam.strInit    = QString(info->UserInfo[0].szUserName);
    ctrlParam.nInit      = 0;
    ctrlParam.nMin       = 0;
    ctrlParam.nMax       = nUserNum-1;
    ctrlParam.strPrefix  = "";
    ctrlParam.strSuffix  = "";
    CtrlInputChar* pCtrl = (CtrlInputChar*)
            (ui->tableWidget->cellWidget(ROWIDX_USERNAME, 0));
    pCtrl->setParam( &ctrlParam );

    CtrlInputChar* pCtrl2 = (CtrlInputChar*)
            (ui->tableWidget->cellWidget(ROWIDX_PASSWORD, 0));
    pCtrl2->Reset();

    ui->tableWidget->selectRow( ROWIDX_USERNAME );
}

void LoginWindow::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void LoginWindow::keyPressEvent(QKeyEvent* keyEvent)
{
    TRACELOG1( "LoginWindow::keyPressEvent" );
    m_QTimer.stop();
    m_QTimer.start();
    switch ( keyEvent->key() )
    {
        case Qt::Key_Return:
        case Qt::Key_Enter:
        {
            TRACELOG1( "LoginWindow::keyPressEvent enter" );
            int nSelRow = ui->tableWidget->selectedRow();
            switch (nSelRow)
            {
                case ROWIDX_USERNAME:
                case ROWIDX_PASSWORD:
                {
                    CtrlInputChar* pCtrl = (CtrlInputChar*)
                    (ui->tableWidget->cellWidget(nSelRow, 0));
                    pCtrl->Reset();
                    pCtrl->Enter();
                }
                break;

                default:
                {
                    TRACELOG1( "LoginWindow::keyPressEvent case 1 default nSelRow<%d>",nSelRow );
                }
                break;
            }
        }
        break;

        case Qt::Key_Escape:
        {
            reject();
        }
        break;

        default:
            break;
    }

    QWidget::keyPressEvent (keyEvent);
}

void LoginWindow::sltCellUnitKeyPress(int key)
{
    Q_UNUSED( key );
    //TRACEDEBUG( "LoginWindow::sltCellUnitKeyPress key<%x>", key );
    m_QTimer.stop();
    m_QTimer.start();
}


void LoginWindow::sltTableKeyPress(int key)
{
    int nSelRow = ui->tableWidget->selectedRow();
    //TRACEDEBUG( "LoginWindow::sltTableKeyPress nSelRow<%d>", nSelRow );
    m_QTimer.stop();
    m_QTimer.start();
    switch (nSelRow)
    {
        case 0:
        case 1:
        {
            ui->tableWidget->selectRow( ROWIDX_USERNAME );
        }
        break;

        case 3:
        {
            switch ( key )
            {
            case Qt::Key_Down:
                //跳转到 password
                ui->tableWidget->selectRow( ROWIDX_PASSWORD );
                break;
            case Qt::Key_Up:
                //跳转到 username
                ui->tableWidget->selectRow( ROWIDX_USERNAME );
                break;
            }
        }
        break;

        default:
        //TRACELOG1( "LoginWindow::sltTableKeyPress default nRow<%d>", nRow );
        break;
    }
}

void LoginWindow::FocusTableWdg(enum INPUT_TYPE ipt)
{
    int nSelRow = ui->tableWidget->selectedRow();
    TRACEDEBUG( "LoginWindow::FocusTableWdg() row<%d>", nSelRow );
    if (ipt==IPT_SUBMIT && nSelRow==ROWIDX_PASSWORD)
    {
#ifdef TEST_GUI
    accept();
    return;
#else
        CtrlInputChar* pCtrl1 = (CtrlInputChar*)
                (ui->tableWidget->cellWidget(ROWIDX_USERNAME, 0));

        CtrlInputChar* pCtrl2 = (CtrlInputChar*)
                (ui->tableWidget->cellWidget(ROWIDX_PASSWORD, 0));
        TRACEDEBUG( "LoginWindow::FocusTableWdg() pCtrl1<%x> pCtrl2<%x>", pCtrl1, pCtrl2 );

        if (pCtrl1 && pCtrl2)
        {
            CtrlInputRetval retVal1 = pCtrl1->getValue();
            CtrlInputRetval retVal2 = pCtrl2->getValue();
            TRACEDEBUG( "LoginWindow::FocusTableWdg() 2" );

            if ( retVal2.strVal.compare(
                     m_Users[retVal1.idx].szPassword
                     ) == 0 )
            {
                TRACEDEBUG( "LoginWindow::FocusTableWdg() 3" );

                accept();
                return;
            }
            else
            {
                DlgInfo dlg(
                            tr("Password error") +".",
                            POPUP_TYPE_CRITICAL
                            );
                dlg.exec();
            }
            TRACELOG1( "LoginWindow::FocusTableWdg nVal<%d> user<%s> pwd<%s>",
                       retVal1.idx,
                       retVal1.strVal.toUtf8().constData(),
                       retVal2.strVal.toUtf8().constData()
                       );
        }
#endif
    }
    ui->tableWidget->selectRow( ROWIDX_PASSWORD );
    ui->tableWidget->setFocus();
    TRACEDEBUG( "LoginWindow::FocusTableWdg()" );
}

void LoginWindow::sltTimeOut()
{
    TRACEDEBUG( "LoginWindow::sltTimeOut" );
    reject();
}

void LoginWindow::accept()
{
    TRACELOG1( "LoginWindow::accept" );
    Leave();

    switch ( m_type )
    {
    case LOGIN_TYPE_GUIDE:
    {
        emit goToGuideWindow( WGUIDE_WIZARD );
    }
        break;

    case LOGIN_TYPE_CONFIG:
    {
        g_timeLoginConfig.restart();
        g_bLogin = true;
        emit goToBaseWindow( WT2_CFG_SETTING );
        qApp->processEvents ();
    }
        break;

    default:
        break;
    }
}

void LoginWindow::reject()
{
    TRACELOG1( "LoginWindow::reject" );
    Leave();

    switch ( m_type )
    {
    case LOGIN_TYPE_GUIDE:
    {
        emit goToGuideWindow( WGUIDE_MAX );
    }
        break;

    case LOGIN_TYPE_CONFIG:
    {
        g_bLogin = false;
        emit goToHomePage();
    }
        break;

    default:
        break;
    }
}

void LoginWindow::on_tableWidget_itemSelectionChanged()
{
    int nSelRow = ui->tableWidget->selectedRow();
    CtrlInputChar* pCtrl = NULL;

    if (nSelRow == ROWIDX_USERNAME ||
            nSelRow == ROWIDX_PASSWORD)
    {
        pCtrl = (CtrlInputChar*)
                (ui->tableWidget->cellWidget(m_nSelRowOld, 0));
        pCtrl->setHighLight( false );

        pCtrl = (CtrlInputChar*)
                (ui->tableWidget->cellWidget(nSelRow, 0));
        pCtrl->setHighLight( true );
        m_nSelRowOld = nSelRow;
    }
}

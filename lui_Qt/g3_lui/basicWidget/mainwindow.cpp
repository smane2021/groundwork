/******************************************************************************
文件名：    mainwindow.cpp
功能：      最上层的管理窗口
           HomePageWindow + BaseWindow
作者：      刘金煌
创建日期：   2013年3月25日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <time.h>
#include "config/configparam.h"
#include "config/PosBase.h"
#include "common/pubInclude.h"
#include "common/sysInitEx.h"
#include "common/global.h"
#include "util/DlgInfo.h"
#include "configWidget/WdgFCfgGroup.h"
#include "equipWidget/Wdg3Table.h"

int  MainWindow::iAutoStartCount = 0;
bool MainWindow::ms_bAlarmRed = false; // 红灯

MainWindow::MainWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //setAttribute(Qt::WA_TranslucentBackground);

    SET_GEOMETRY_WIDGET( this );
    SET_GEOMETRY_WIDGET( ui->stackedWidget );

    TRACELOG1( "MainWindow::MainWindow width*height <%d*%d> stackedWidget<%d*%d>",
               width(), height(),
               ui->stackedWidget->width(),
               ui->stackedWidget->height()
               );
    TRACELOG1( "MainWindow app path: %s", qApp->applicationDirPath().toUtf8().constData() );

    m_timerIdDetect     = 0;
    bMovieStart = false;

    iBackLightOffCounter = 0;
    for (int i=0; i<3; ++i)
    {
        m_nAlarmsOld[i] = 0;
    }

    m_blHomePageIn = false;

    m_timerIdHeartBeat = startTimer( 4000 );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::heartbeatTimerRefresh()
{
}

void MainWindow::CreateWindows()
{
    TRACELOG1( "MainWindow::CreateWindows" );
    //安装事件过滤器， 才可以用eventFilter函数
#ifdef Q_OS_LINUX
    qApp->installEventFilter(this);
#endif

    // homepageWindow
    homePageWindow = new HomePageWindow();
    connect( homePageWindow,
             SIGNAL(goToHomePage()),
             this,
             SLOT(GoToHomePage()) );
    connect( homePageWindow,
             SIGNAL(goToBaseWindow(enum WIDGET_TYPE)),
             this,
             SLOT(GoToBaseWindow(enum WIDGET_TYPE)) );
    connect( homePageWindow,
             SIGNAL(goToLoginWindow(enum LOGIN_TYPE)),
             this,
             SLOT(GoToLoginWindow(enum LOGIN_TYPE)) );
    ui->stackedWidget->addWidget(homePageWindow);
    //TRACELOG1( "MainWindow::CreateWindows HomePageWindow OK" );

    // baseWindow
    baseWindow = new BaseWindow();
    connect( baseWindow,
             SIGNAL(goToHomePage()),
             this,
             SLOT(GoToHomePage()) );
    connect( baseWindow,
             SIGNAL(goToBaseWindow(enum WIDGET_TYPE)),
             this,
             SLOT(GoToBaseWindow(enum WIDGET_TYPE)) );
    connect( baseWindow,
             SIGNAL(goToGuideWindow(enum WIDGET_GUIDE)),
             this,
             SLOT(GoToGuideWindow(enum WIDGET_GUIDE)) );
    connect( baseWindow,
             SIGNAL(sigStopDetectAlarm()),
             this,
             SLOT(sltStopDetectAlarm()) );
    connect( this,
             SIGNAL(sigScreenSaver()),
             baseWindow,
             SIGNAL(sigScreenSaver()) );
    ui->stackedWidget->addWidget(baseWindow);
    //TRACELOG1( "MainWindow::CreateWindows BaseWindow OK" );

    // guideWindow
    guideWindow = new GuideWindow();
    connect( guideWindow,
             SIGNAL(goToHomePage()),
             this,
             SLOT(GoToHomePage()) );
    connect( guideWindow,
             SIGNAL(goToBaseWindow(enum WIDGET_TYPE)),
             this,
             SLOT(GoToBaseWindow(enum WIDGET_TYPE)) );
    connect( guideWindow,
             SIGNAL(sigDetectAlarm()),
             this,
             SLOT(sltDetectAlarm()) );
    connect( guideWindow,
             SIGNAL(goToLoginWindow(enum LOGIN_TYPE)),
             this,
             SLOT(GoToLoginWindow(enum LOGIN_TYPE)) );
    ui->stackedWidget->addWidget(guideWindow);
    //TRACELOG1( "MainWindow::CreateWindows GuideWindow OK" );

    // loginWindow
    loginWindow = new LoginWindow();
    connect( loginWindow,
             SIGNAL(goToGuideWindow(enum WIDGET_GUIDE)),
             this,
             SLOT(GoToGuideWindow(enum WIDGET_GUIDE)) );
    connect( loginWindow,
             SIGNAL(goToHomePage()),
             this,
             SLOT(GoToHomePage()) );
    connect( loginWindow,
             SIGNAL(goToBaseWindow(enum WIDGET_TYPE)),
             this,
             SLOT(GoToBaseWindow(enum WIDGET_TYPE)) );
    ui->stackedWidget->addWidget(loginWindow);

    TRACELOG1( "MainWindow::CreateWindows LoginWindow OK" );
}

void MainWindow::timerEvent( QTimerEvent * event )
{
    if (event->timerId() == m_timerIdDetect)
    {
        resetFrameBuffer();

        g_cfgParam->initAlmVoiceCloseStat();
        // TRACEDEBUG("MainWindow::timerEvent almVoiceType<%d>\n",g_cfgParam->ms_initParam.almVoiceType);
        if(g_cfgParam->ms_initParam.almVoiceType == ALMVOICE_CLOSE_YES)
        {
        //   TRACEDEBUG("MainWindow::timerEvent almVoiceActType<%d>\n",g_cfgParam->ms_initParam.almVoiceActType);

          sys_setBuzzEx( BUZZ_QUIET, BUTTON_BEEP );//消音
          g_cfgParam->resetAlmVoiceClose(); //关掉告警音后复位为1
        }
        if ( g_bDetectWork )
        {
            //检测背光关闭
            DetectBackLightOff();

            //检测活动告警
            DetectActiveAlarm();
        }
    }
    else if (event->timerId() == m_timerIdHeartBeat)
    {
//        TRACELOG1( "MainWindow::timerEvent m_timerIdHeartBeat 44444\n" );
        data_sendHeartBeat();

        if(currentWidget != baseWindow)
        {
          int iRetVal = g_cfgParam->initParam();
          if((iRetVal == 1) && m_blHomePageIn)
          {
            GoToHomePage();
          }
        }
    }
}

void MainWindow::resetFrameBuffer()
{
#ifdef Q_OS_LINUX
    // 重置framebuffer
    static int resetFrameBufferCounter = 0;
    if(resetFrameBufferCounter < TIME_RESET_FRAME_BUFFER)
    {
        ++resetFrameBufferCounter;
    }
    else
    {
        sys_resetFrameBuffer();
        resetFrameBufferCounter = 0;
    }
#endif
}

/*背光关闭*/
void MainWindow::DetectBackLightOff()
{
    if(sys_getLCD() == LCD_OFF_BRIGHTNESS)
    {
        return;
    }

    // 屏保时间
    if(iBackLightOffCounter <
            TIME_SCREEN_SAVER
            //10*1000/TIMER_DETECT_ALARM_INTERVAL
            )
    {
        ++iBackLightOffCounter;
    }
    else
    {
        TRACEDEBUG( "MainWindow::DetectBackLightOff() g_bDetectWork<%d>", g_bDetectWork );
        emit sigScreenSaver();
        sys_setLCD( LCD_OFF_BRIGHTNESS );

        // 屏保时跳转到不刷数据屏
        GoToBaseWindow( WT1_SCREENSAVER );
    }
}


void MainWindow::DetectActiveAlarm()
{
    static int nAlarmSoundOFF = 0;
    //TRACEDEBUG( "MainWindow::DetectActiveAlarm()" );
    if ( ERRDS_HASALARM==data_hasAlarm() )
    {
        TRACEDEBUG( "MainWindow::DetectActiveAlarm() has alarm" );
        static void *pData = NULL;
#ifdef Q_OS_LINUX
        data_recvData_alarm( &pData );
		g_cfgParam->initParam();
#endif
        if(pData == NULL)
        {
            return;
        }

        PACK_ALMNUM* info = (PACK_ALMNUM*)pData;
        int nOA = info->OANum;
        int nMA = info->MANum;
        int nCA = info->CANum;
        TRACELOG1( "MainWindow::DetectActiveAlarm() data_hasAlarm new<%d %d %d> Old<%d %d %d>",
                        nOA,
                        nMA,
                        nCA,
                        m_nAlarmsOld[0],
                        m_nAlarmsOld[1],
                        m_nAlarmsOld[2]
                        );

	if (nOA > 0)
	{
		sys_setLED( LED_YELLOW_ON );
	}
	else
	{
		sys_setLED( LED_YELLOW_OFF );
	}

	if (nMA+nCA > 0)
	{
		sys_setLED( LED_RED_ON );
	}
	else
	{
		sys_setLED( LED_RED_OFF );
		sys_setBuzzEx( BUZZ_QUIET, ALARM_BEEP );
	}

	if ( nOA > m_nAlarmsOld[0] ||
		 nMA > m_nAlarmsOld[1] ||
		 nCA > m_nAlarmsOld[2])
	{
		if (LCD_OFF_BRIGHTNESS==sys_getLCD() ||
				(g_timeElapsedNoKeyPress.elapsed()>=
				 TIME_GOTO_ALARM_PAGE_NOKEY
				 //10*1000
				 )
			)
		{
			// goto active alarm page
			TRACEDEBUG( "MainWindow::DetectActiveAlarm() GoToBaseWindow( WT3_ACT_ALARM )" );
			g_timeElapsedNoKeyPress.restart();
			if (LCD_OFF_BRIGHTNESS==sys_getLCD())
			{
				g_nInAlarmType = IN_ALARM_TYPE_SCREENSAVER;
			}
			else
			{
				g_nInAlarmType = IN_ALARM_TYPE_NOKEYPRESS;
			}

			Wdg3Table::ms_bEscapeFromHelp = false;
			GoToBaseWindow( WT3_ACT_ALARM );
			TRACEDEBUG( "MainWindow::eventFilter 2 sys_setLCD( LCD_MAX )" );
			sys_setLCD( LCD_MAX );
			iBackLightOffCounter = 0;
		}
	}

	// need buzz?
	if ( g_cfgParam->ms_initParam.alarmTime != 0 &&
		 (nMA > m_nAlarmsOld[1] ||
		  nCA > m_nAlarmsOld[2])
		 )
	{
		sys_setBuzzEx( BUZZ_MOO, ALARM_BEEP );
		m_bAlarmSound = true;
		nAlarmSoundOFF = 0;
	}
	if (nMA+nCA == 0)
	{
		sys_setBuzzEx( BUZZ_QUIET );
		m_bAlarmSound = false;
	}

	if (nOA+nMA+nCA > 0)
	{
		ms_bAlarmRed = true;
	}
	else
	{
		ms_bAlarmRed = false;
	}

	m_nAlarmsOld[0] = nOA;
	m_nAlarmsOld[1] = nMA;
	m_nAlarmsOld[2] = nCA;
    }

    if ( g_cfgParam->ms_initParam.alarmTime>0 && m_bAlarmSound )
    {
        nAlarmSoundOFF++;
        if(nAlarmSoundOFF >= (unsigned int)g_cfgParam->ms_initParam.alarmTime/1.5)
        {
            nAlarmSoundOFF = 0;
            m_bAlarmSound = false;
            sys_setBuzzEx( BUZZ_QUIET );  //消音
        }
    }
}

#include <QKeyEvent>
bool MainWindow::eventFilter(QObject *object, QEvent *event)    //按键事件处理
{
    //TRACEDEBUG( "MainWindow::eventFilter event->type()<%d>", event->type() );

    if (event->type() == QEvent::KeyPress)
    {
        g_timeElapsedNoKeyPress.restart();
        QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
        //TRACEDEBUG( "MainWindow::eventFilter key<%x>", keyEvent->key() );
//        if (keyEvent->key() == Qt::Key_Shift)
//            TRACEDEBUG("123123123");//HHY
        if (keyEvent->key() == Qt::Key_Shift)
        {
            TRACEDEBUG("Qt::Key_Shift");
            static bool bResetPasswordFirst = true;
            if ( bResetPasswordFirst )
            {
                TRACELOG1( "MainWindow::eventFilter Qt::Key_Shift" );
                bResetPasswordFirst = false;
                DlgInfo dlg(
                            tr("Reset Admin Password") +"?",
                            POPUP_TYPE_QUESTION_PASSWORD
                            );
                if (dlg.exec() == QDialog::Accepted)
                {
                    TRACELOG1( "MainWindow::eventFilter key Qt::Key_Control resetting..." );

                    CtrlInputChar* pCtrl;
                    CtrlInputParam* pCtrlParamSetting;

                    //tellAppToReboot();//reboot
                    tellAppToResetPassword();
                    //system( "reboot &" );//hhy
                    sys_setLCD( LCD_OFF_BRIGHTNESS );
                }

                bResetPasswordFirst = true;
                return true;
            }
        }
        else if (keyEvent->key() == Qt::Key_Control)
        {
            static bool bRebootFirst = true;
            if ( bRebootFirst )
            {
                TRACELOG1( "MainWindow::eventFilter Qt::Key_Control" );
                bRebootFirst = false;
                DlgInfo dlg(
                            tr("OK to reboot") +"?",
                            POPUP_TYPE_QUESTION
                            );
                if (dlg.exec() == QDialog::Accepted)
                {
                    TRACELOG1( "MainWindow::eventFilter key Qt::Key_Control rebooting..." );

                    CtrlInputChar* pCtrl;
                    CtrlInputParam* pCtrlParamSetting;


                    //tellAppToReboot();
                    system( "reboot &" );
                    sys_setLCD( LCD_OFF_BRIGHTNESS );
                }

                bRebootFirst = true;
                return true;
            }
        }
        else if(keyEvent->key() == Qt::Key_Home)
        {
            //当前界面不是登录界面时处理
            if(currentWidget != loginWindow)
            {
                static bool bLogoutFirst = true;
                if ( bLogoutFirst )
                {
                    bLogoutFirst = false;
                    //当不是登录界面时，如果已经进入设置界面，则提示logout,回到主页，否则
                    //不提示
                    DlgInfo dlg(
                            tr("Logout") +"?",
                            POPUP_TYPE_QUESTION
                            );
                    if (dlg.exec() == QDialog::Accepted)
                    {
                        g_bLogin = false;
                        g_timeLoginConfig.restart();
                        if((g_nLastWTScreenSaver == WT2_CFG_SETTING) ||
                           (g_nLastWTScreenSaver == WT3_CFG_SETTING_OTHER)||
                           (g_nLastWTScreenSaver == WT3_CFG_SETTING_OTHER_BATT1) ||
                           (g_nLastWTScreenSaver == WT3_CFG_SETTING_OTHER_BATT2))
                        {
                            GoToHomePage();
                        }
                    }
                    bLogoutFirst = true;
                    return true;
                }
            }
        }

        iBackLightOffCounter = 0;

        // 打开背光 跳转到屏保页
        if (sys_getLCD() == LCD_OFF_BRIGHTNESS)
        {
            TRACEDEBUG( "MainWindow::eventFilter screen saver out g_nLastWTScreenSaver<%d>", g_nLastWTScreenSaver );
            if (g_nLastWTScreenSaver == WT3_ACT_ALARM)
            {
                TRACEDEBUG( "MainWindow::eventFilter GoToBaseWindow( WT3_ACT_ALARM )" );
                g_nInAlarmType = IN_ALARM_TYPE_SCREENSAVER;

                Wdg3Table::ms_bEscapeFromHelp = false;
                GoToBaseWindow( WT3_ACT_ALARM );
            }
            else
            {
                TRACEDEBUG( "MainWindow::eventFilter GoToHomePage()" );
                GoToHomePage();
            }

            TRACEDEBUG( "MainWindow::eventFilter sys_setLCD( LCD_MAX )" );
            sys_setLCD( LCD_MAX );
            return true;
        }

        //TRACELOG1("MainWindow::eventFilter m_bAlarmSound<%d>", m_bAlarmSound);
        // 点击屏幕使告警蜂鸣器消掉声音
        if ( m_bAlarmSound )
        {
            m_bAlarmSound = false;
            sys_setBuzzEx( BUZZ_QUIET, BUTTON_BEEP );//消音
        }
    }

    return QDialog::eventFilter(object, event);
}

void MainWindow::GoToHomePage()
{
    TRACEDEBUG( "MainWindow::GoToHomePage()" );
    if(currentWidget != homePageWindow)
    {
        ui->stackedWidget->setCurrentWidget(homePageWindow);
    }
    if (BasicWidget::ms_showingWdg)
    {
        TRACEDEBUG( "MainWindow::GoToHomePage() ms_showingWdg->Leave()" );
        BasicWidget::ms_showingWdg->Leave();
    }
    currentWidget = homePageWindow;
    homePageWindow->setFocus();
    homePageWindow->Enter();
    TRACEDEBUG( "MainWindow::GoToHomePage() >>>" );

    m_blHomePageIn = true;
}

void MainWindow::GoToBaseWindow(enum WIDGET_TYPE wt)
{
    TRACEDEBUG( "MainWindow::GoToBaseWindow()" );
    if(currentWidget != baseWindow)
    {
        ui->stackedWidget->setCurrentWidget(baseWindow);
    }
    if (BasicWidget::ms_showingWdg)
    {
        TRACEDEBUG( "MainWindow::GoToBaseWindow() ms_showingWdg->Leave()" );
        BasicWidget::ms_showingWdg->Leave();
    }

    if (wt == WT2_CFG_SETTING)
    {
        TRACEDEBUG( "MainWindow::GoToBaseWindow GoToLoginWindow or not wt<%d> g_bLogin<%d> time<%d>", wt, g_bLogin, g_timeLoginConfig.elapsed() );
        if (!g_bLogin ||
             g_timeLoginConfig.elapsed()>
             TIME_OUT_LOGIN_USER)
        {
            TRACEDEBUG( "MainWindow::GoToBaseWindow() GoToLoginWindow" );
            g_bLogin = false;
            GoToLoginWindow( LOGIN_TYPE_CONFIG );

            return;
        }
    }

    currentWidget = baseWindow;
    currentWidget->setFocus();
    currentWidget->Enter( (void*)&wt );
}

void MainWindow::GoToGuideWindow(enum WIDGET_GUIDE wt)
{
    TRACEDEBUG( "MainWindow::GoToGuideWindow()" );
    if(currentWidget != guideWindow)
    {
        ui->stackedWidget->setCurrentWidget(guideWindow);
    }
    if (BasicWidget::ms_showingWdg)
    {
        TRACEDEBUG( "MainWindow::GoToGuideWindow() ms_showingWdg->Leave()" );
        BasicWidget::ms_showingWdg->Leave();
    }
    currentWidget = guideWindow;
    currentWidget->setFocus();

    //if (wt == WGUIDE_WIZARD)
    {
        EnterGuideParam_t enterParam;
        enterParam.wt_Guide = wt;
        enterParam.screenID = SCREEN_ID_WizSiteName;
        currentWidget->Enter( (void*)&enterParam );
    }
}

void MainWindow::GoToLoginWindow(enum LOGIN_TYPE type)
{
    TRACEDEBUG( "MainWindow::GoToLoginWindow()" );
    if(currentWidget != loginWindow)
    {
        ui->stackedWidget->setCurrentWidget(loginWindow);
    }
    if (BasicWidget::ms_showingWdg)
    {
        TRACEDEBUG( "MainWindow::GoToLoginWindow() ms_showingWdg->Leave()" );
        BasicWidget::ms_showingWdg->Leave();
    }
    currentWidget = loginWindow;
    currentWidget->setFocus();
    currentWidget->Enter( &type );
}

void MainWindow::LanguageChanged()
{
//    if(g_cfgParam->languageType == LUI_LANG_LOCAL)//中文，注意不要搞错了
//    {
//        translator.load(g_cfgParam->appPath + "translations/M822E_LOC");
//    }
//    else if(g_cfgParam->languageType == LUI_LANG_ENGLISH)//英文，注意不要搞错了
//    {
//        translator.load(g_cfgParam->appPath + "translations/M822E_ENG");
//    }

    ui->retranslateUi(this);
}

void MainWindow::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void MainWindow::sltDetectAlarm()
{
    m_timerIdDetect = startTimer( TIMER_DETECT_ALARM_INTERVAL );
    TRACEDEBUG( "MainWindow::sltDetectAlarm()" );
}

void MainWindow::sltStopDetectAlarm()
{
    killTimer( m_timerIdDetect );
    m_timerIdDetect = 0;
    TRACEDEBUG( "MainWindow::sltStopDetectAlarm()" );
}

void MainWindow::reject()
{

}

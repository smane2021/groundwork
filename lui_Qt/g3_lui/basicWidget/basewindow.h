/******************************************************************************
文件名：    basewidow.h
功能：     QStackedWidget,包含信号、设置界面
作者：      刘金煌
创建日期：   2013年03月27日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef BASEWINDOW_H
#define BASEWINDOW_H

#include <QStackedWidget>
#include <QStack>
#include "common/basicwidget.h"
#include "common/uidefine.h"

namespace Ui {
    class BaseWindow;
}

class BaseWindow : public BasicWidget
{
    Q_OBJECT

public:
    explicit BaseWindow(QWidget *parent = 0);
    ~BaseWindow();

public:
    /*初始化堆栈*/
    void InsertWidget();
    /*建立信号与槽的连接*/
    void InitConnect();
    /*向FIFO发送当前Widget类型*/
    bool TransmitAppWidgetType(int iWidgetType);

public slots:
    virtual void Enter(void* param=NULL);
    virtual void Leave();

protected:
    virtual void InitWidget();
    //virtual void InitConnect();

protected:
    virtual void changeEvent(QEvent *event);
//    virtual void keyPressEvent(QKeyEvent* keyEvent);

private:
    void MSleepInQT(int ms);

signals:
    void goToHomePage();
    void goToBaseWindow(enum WIDGET_TYPE);
    void goToGuideWindow(enum WIDGET_GUIDE);
    void sigScreenSaver();
    void sigStopDetectAlarm();

private:
    //指向当前显示的界面
    BasicWidget *currentWidget;

private:
    Ui::BaseWindow *ui;
};

#endif // BASEWINDOW_H

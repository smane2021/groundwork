/******************************************************************************
文件名：    mainwindow.h
功能：      最上层的管理窗口
           HomePageWindow + BaseWindow
作者：      刘金煌
创建日期：   2013年03月25日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDialog>
#include <QStackedWidget>
#include <QTimer>
#include <QTranslator>
#include "homepagewindow.h"
#include "basewindow.h"
#include "GuideWindow.h"
#include "LoginWindow.h"
#include "common/splashscreen.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QDialog
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public:
    //建立初始化各种窗口
    void CreateWindows();

protected:
    virtual bool eventFilter(QObject *object, QEvent *event);
    virtual void timerEvent( QTimerEvent * event );
    virtual void changeEvent( QEvent * event );

public slots:
    void GoToHomePage();
    void GoToBaseWindow(enum WIDGET_TYPE wt);
    void GoToGuideWindow(enum WIDGET_GUIDE wt);
    void GoToLoginWindow(enum LOGIN_TYPE type);

private slots:
    void heartbeatTimerRefresh();
    void LanguageChanged();
    void sltDetectAlarm();
    void sltStopDetectAlarm();

    virtual void reject();

signals:
    void judgeActiveAlarm(int bState);
    void SysLangChanged();
    void sigScreenSaver();

public:
    SplashScreen *pMain_Splash; //从main里面传递过来的指针，用于退出的处理
    /*
    CA+MA > 0时，红灯亮
    CA+MA = 0时，红灯灭；
    OA>0,黄灯亮
    OA=0,黄灯灭

    CA+MA大于上一次的值，且设置告警音开，产生告警音。
    CA+MA=0,关闭告警音。
    CA+MA小于上次的值，不处理告警音。
    告警音按键消音； 时间到消音。
    */
    static bool ms_bAlarmRed;    // 红灯

private:
    bool m_bAlarmSound;  // 声音

private:
    bool bActiveAlarmHasbeenClicked;
    void resetFrameBuffer();
    void DetectBackLightOff();
    void DetectActiveAlarm();

private:
    int m_timerIdDetect; // checking turn off lcd and active alarm
    int m_timerIdHeartBeat; // send heart beat to app
    int m_keypadVoice;
    int m_nAlarmsOld[3]; // last OA MA CA
    CmdItem  m_cmdItemAlarm;

    QTranslator translator;
    //屏幕保护相关
    QTimer *stopMovieTimer;
    QTimer *heartbeatTimer;
    QTimer *pGetAutoCfgTimer;
    bool bMovieStart;
    int iBackLightOffCounter;       //背光计数器
    /***********/
    QStackedWidget*  stackedWidget;
    HomePageWindow*  homePageWindow;
    BaseWindow*      baseWindow;
    BasicWidget*     currentWidget;
    GuideWindow*     guideWindow;
    LoginWindow*     loginWindow;
    static int  iAutoStartCount;
    bool        m_blHomePageIn;

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H

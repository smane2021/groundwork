/******************************************************************************
文件名：    basewidow.cpp
功能：      LUI的基础窗口，在此窗口中段填充所有需要的显示页面
作者：      刘金煌
创建日期：   2013年03月27日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "basewindow.h"
#include "ui_basewindow.h"

#include "config/configparam.h"
#include "common/pubInclude.h"
#include "common/global.h"

BaseWindow::BaseWindow(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::BaseWindow)
{
    ui->setupUi(this);

    SET_GEOMETRY_WIDGET( this );
    SET_GEOMETRY_WIDGET( ui->stackedWidget );

    InitWidget();
    InitConnect();

    currentWidget = NULL;
}

BaseWindow::~BaseWindow()
{
    delete ui;
}

/*初始化QT Creator中设计的widget*/
void BaseWindow::InitWidget()
{
}

void BaseWindow::InsertWidget()
{
}

void BaseWindow::InitConnect()
{
    connect( ui->p1, SIGNAL(goToHomePage()),
             this, SIGNAL(goToHomePage()) );
    connect( ui->p1, SIGNAL(goToBaseWindow(enum WIDGET_TYPE)),
             this, SIGNAL(goToBaseWindow(enum WIDGET_TYPE)) );

    connect( ui->p2, SIGNAL(goToHomePage()),
             this, SIGNAL(goToHomePage()) );
    connect( ui->p2, SIGNAL(goToBaseWindow(enum WIDGET_TYPE)),
             this, SIGNAL(goToBaseWindow(enum WIDGET_TYPE)) );
    connect( ui->p2, SIGNAL(goToGuideWindow(enum WIDGET_GUIDE)),
             this, SIGNAL(goToGuideWindow(enum WIDGET_GUIDE)) );
    connect( ui->p2, SIGNAL(sigStopDetectAlarm()),
             this, SIGNAL(sigStopDetectAlarm()) );

    connect( ui->p3, SIGNAL(goToHomePage()),
             this, SIGNAL(goToHomePage()) );
    connect( ui->p3, SIGNAL(goToBaseWindow(enum WIDGET_TYPE)),
             this, SIGNAL(goToBaseWindow(enum WIDGET_TYPE)) );

    connect( ui->p4, SIGNAL(goToHomePage()),
             this, SIGNAL(goToHomePage()) );
    connect( ui->p4, SIGNAL(goToBaseWindow(enum WIDGET_TYPE)),
             this, SIGNAL(goToBaseWindow(enum WIDGET_TYPE)) );

    connect( this,
             SIGNAL(sigScreenSaver()),
             ui->p2,
             SIGNAL(sigScreenSaver()) );
}

void BaseWindow::Enter(void* param)
{
    int wt = *(int*)param;

    g_nLastWTScreenSaver = g_nLastWTNokey;

    TRACEDEBUG( "BaseWindow::Enter g_nInAlarmType<%d> g_nLastWTNokey<%d> g_nLastWTScreenSaver<%d> wt<%d>", g_nInAlarmType, g_nLastWTNokey, g_nLastWTScreenSaver, wt );

    if ((wt != WT1_SCREENSAVER) && (wt != WT1_FIRST_MAX))
    {
        g_nLastWTScreenSaver = wt;
        if (g_nInAlarmType!=IN_ALARM_TYPE_NOKEYPRESS)
        {
            g_nLastWTNokey = wt;
        }
    }
    TRACEDEBUG( "BaseWindow::Enter g_nInAlarmType<%d> g_nLastWTNokey<%d> g_nLastWTScreenSaver<%d> wt<%d>",
                g_nInAlarmType, g_nLastWTNokey, g_nLastWTScreenSaver, wt );

    if (wt <= WT1_FIRST_MAX)
    {
        ui->stackedWidget->setCurrentIndex(0);
        ui->p1->Enter( param );
    }
    else if((wt>WT1_FIRST_MAX) && (wt<=WT2_SECOND_MAX))
    {
        ui->stackedWidget->setCurrentIndex(1);
        ui->p2->Enter( param );
    }
    else if((wt>WT2_SECOND_MAX) && (wt<=WT3_THIRD_MAX))
    {
        ui->stackedWidget->setCurrentIndex(2);
        ui->p3->Enter( param );
    }
    else
    {
        ui->stackedWidget->setCurrentIndex(3);
        ui->p4->Enter( param );
    }

    currentWidget = (BasicWidget*)ui->stackedWidget->currentWidget();
}

void BaseWindow::Leave()
{
    TRACEDEBUG( "BaseWindow::Leave()" );
    currentWidget->Leave();
}

void BaseWindow::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

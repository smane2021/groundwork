/******************************************************************************
文件名：    GuideWindow.cpp
功能：      language installwizard 界面
作者：      刘金煌
创建日期：   2013年6月19日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "GuideWindow.h"
#include "ui_GuideWindow.h"
#include <unistd.h>
#include <QKeyEvent>
#include <QFile>
#include <QTextStream>
#include <QTextCodec>
#include "config/configparam.h"
#include "common/pubInclude.h"
#include "common/BuzzDoubleSpinBox.h"
#include "common/BuzzSpinBox.h"
#include "common/CtrlInputChar.h"
#include "common/global.h"
#include "util/DlgLogin.h"
#include "util/DlgInfo.h"

#define IF_CONTINUE_SCREEN_ID  SCREEN_ID_WizCapacity
#define MIN_SCREEN_ID_INPUT SCREEN_ID_WizSiteName
#define MAX_SCREEN_ID_INPUT SCREEN_ID_WizCommunicate
#define PAGES_NUMBER (MAX_SCREEN_ID_INPUT-MIN_SCREEN_ID_INPUT+1)

#define ENTER_GUIDE_WINDOW \
    EnterGuideParam_t enterParam; \
    enterParam.wt_Guide = m_wt; \
    enterParam.screenID = m_cmdItem.ScreenID; \
    Enter( (void*)&enterParam );
//每次转换界面都要ENTER_GUIDE_WINDOW，即返回屏幕ID，哪一页

bool GuideWindow::ms_bInEnter = false;

GuideWindow::GuideWindow(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::GuideWindow)
{
    ui->setupUi(this);

    SET_GEOMETRY_WIDGET( this );
    SET_GEOMETRY_WIDGET( ui->stackedWidget );

    m_nWaitAppdata = 0;
    m_timerId      = 0;
    m_timerIdSpawn = startTimer( 100 );
    m_nCtrlInputCreated    = 0;
    m_wt  = WGUIDE_LANGUAGE;
    //m_wt  = WGUIDE_PASSWORD;
    m_nRows    = 0;

    InitWidget();
    InitConnect();
}

GuideWindow::~GuideWindow()
{
    delete ui;
}

void GuideWindow::InitWidget()
{
    TRACEDEBUG( "GuideWindow::InitWidget()" );

    SET_GEOMETRY_LABEL_TITLE( ui->label_name );
    ui->stackedWidget->setCurrentIndex( m_wt );
    ui->tableWidget_enter->setFocusPolicy(Qt::NoFocus);


    int nRow = 0;
    {
        // language
        SET_TABLEWDG_STYLE( tableWidget_language,
                            TT_NOT_TITLE_NOT_SCROOL );
        ui->tableWidget_language->setFixedWidth( PosTableWidget::langWidth );
        QLabel* pLabel;

        //获取系统中的语言种类 Added by wangminghui
        g_cfgParam->getSysLangType(m_strListLangType);

        for(nRow=0;nRow < m_strListLangType.size ();nRow++)
        {
            m_pixmap[nRow*2] = (
                        PATH_IMG + "lang_" + g_mapLangSymbol.value(m_strListLangType.at(nRow)) + "_1.png" );
            m_pixmap[nRow*2+1] = (
                        PATH_IMG + "lang_" + g_mapLangSymbol.value(m_strListLangType.at(nRow)) + "_2.png" );

            pLabel = new QLabel;
            pLabel->setStyleSheet("border-image:url(" + m_pixmap[nRow*2] + ")");
            ui->tableWidget_language->insertRow( nRow );
            ui->tableWidget_language->setCellWidget(
                        nRow, 0, pLabel );
        }

        ui->tableWidget_language->insertRow( nRow );
        QTableWidgetItem* item = new QTableWidgetItem( "" );
        ui->tableWidget_language->setItem(nRow, 0, item);

        for (int i=0; i<nRow; ++i)
        {
            ui->tableWidget_language->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
        }

        LANGUAGE_TYPE langType = g_cfgParam->ms_initParam.langApp;
        g_cfgParam->readLanguageLocal();
        if (LANG_LOCATE_English == g_cfgParam->ms_initParam.langLocate)
        {
            langType = LANG_English;
        }

        //使当前的系统语言选择框显示为选中。
        QString strAttrLangType = g_mapLangStr[langType];
        TRACEDEBUG("1111 GuideWindow::InitWidget # strAttrLangType:%s",qPrintable(strAttrLangType));

        int nLangTypeIndex = m_strListLangType.indexOf (strAttrLangType);
        if(nLangTypeIndex == -1)
        {
            nLangTypeIndex = 0;
        }
        ui->tableWidget_language->selectRow( nLangTypeIndex );
        pLabel = (QLabel*)ui->tableWidget_language->cellWidget(nLangTypeIndex, 0);
        pLabel->setStyleSheet("border-image:url(" + m_pixmap[nLangTypeIndex*2+1] + ")" );
    }

    {
        // tableWidget_enter
        int nRows = 6;
        SET_TABLEWDG_STYLE( tableWidget_enter, TT_NOT_TITLE_NOT_SCROOL );
        //ui->tableWidget_enter->setGeometry(3, 6, 156, 128);
        ui->tableWidget_enter->setRowCount( nRows );
        for (int i=0; i<nRows; ++i)
        {
            ui->tableWidget_enter->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
        }

        QTableWidgetItem * item;
        item = new QTableWidgetItem;
        item->setText( "" );
        item->setTextAlignment( Qt::AlignCenter );
        ui->tableWidget_enter->setItem(2, 0, item);

        item = new QTableWidgetItem;
        item->setText( "" );
        item->setTextAlignment( Qt::AlignRight );
        ui->tableWidget_enter->setItem(3, 0, item);

        item = new QTableWidgetItem;
        item->setText( "" );
        item->setTextAlignment( Qt::AlignRight );
        ui->tableWidget_enter->setItem(4, 0, item);
    }

    {
        // input
        SET_TABLEWDG_STYLE(tableWidget_input, TT_YES_TITLE_YES_SCROOL);
        SET_STYLE_SCROOLBAR( PAGES_NUMBER, 1 );
        ui->label_name->clear();
        ui->label_name->setAlignment( Qt::AlignLeft );
    }

    {
        // continue?
        SET_TABLEWDG_STYLE( tableWidget_continue, TT_NOT_TITLE_NOT_SCROOL );
        int nRows = TABLEWDG_ROWS_PERPAGE_NOTITLE;
        ui->tableWidget_continue->setRowCount( nRows );
        QTableWidgetItem* item   = NULL;
        for (int i=0; i<nRows; ++i)
        {
            item = new QTableWidgetItem;
            item->setText( "" );
            ui->tableWidget_continue->setItem(i, 0, item);
            ui->tableWidget_continue->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
        }
    }

    {
        // tableWidget_finish
        int nRows = 6;
        SET_TABLEWDG_STYLE( tableWidget_finish, TT_NOT_TITLE_NOT_SCROOL );
        //ui->tableWidget_finish->setGeometry(3, 6, 156, 128);
        ui->tableWidget_finish->setRowCount( nRows );
        for (int i=0; i<nRows; ++i)
        {
            ui->tableWidget_finish->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
        }

        QTableWidgetItem * item;
        item = new QTableWidgetItem;
        item->setText( "" );
        item->setTextAlignment( Qt::AlignCenter );
        ui->tableWidget_finish->setItem(2, 0, item);
    }

    //password
    int nRows = 6;
    SET_TABLEWDG_STYLE( tableWidget_password, TT_NOT_TITLE_NOT_SCROOL );
    //ui->tableWidget_enter->setGeometry(3, 6, 156, 128);
    ui->tableWidget_password->setRowCount( nRows );
    for (int i=0; i<nRows; ++i)
    {
        ui->tableWidget_password->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
    }

    QTableWidgetItem * item;
    item = new QTableWidgetItem;
    item->setText( "" );
    item->setTextAlignment( Qt::AlignCenter );
    ui->tableWidget_password->setItem(1, 0, item);

    item = new QTableWidgetItem;
    item->setText( "" );
    item->setTextAlignment( Qt::AlignRight );
    ui->tableWidget_password->setItem(2, 0, item);

    item = new QTableWidgetItem;
    item->setText( "" );
    item->setTextAlignment( Qt::AlignRight );
    ui->tableWidget_password->setItem(3, 0, item);

    item = new QTableWidgetItem;
    item->setText( "" );
    item->setTextAlignment( Qt::AlignRight );
    ui->tableWidget_password->setItem(4, 0, item);

    item = new QTableWidgetItem;
    item->setText( "" );
    item->setTextAlignment( Qt::AlignRight );
    ui->tableWidget_password->setItem(5, 0, item);

}

void GuideWindow::InitConnect()
{
    connect( ui->tableWidget_language, SIGNAL(sigTableKeyPress(int)),
            this, SLOT(sltTableKeyPress(int)) );

    connect( ui->tableWidget_input, SIGNAL(sigTableKeyPress(int)),
            this, SLOT(sltTableKeyPress(int)) );

    connect( &m_QTimer, SIGNAL(timeout()),
             this, SLOT(TimerHandler()) );
}

bool GuideWindow::createInputWidget(int nSpawn)
{
    if (m_nCtrlInputCreated >= MAX_CTRL_ITEMS_GUIDE)
    {
        return false;
    }
    CtrlInputChar*    pCtrl  = NULL;
    QTableWidgetItem* item   = NULL;
    CtrlInputParam ctrlParam;
    QTime timeElapse;
    timeElapse.start();
    for(int i=m_nCtrlInputCreated; i<m_nCtrlInputCreated+nSpawn; ++i)
    {
        ui->tableWidget_input->insertRow( i*2 );
        item = new QTableWidgetItem;
        item->setText( "" );
        ui->tableWidget_input->setItem(i*2, 0, item);
        ui->tableWidget_input->setRowHeight(i*2, TABLEWDG_ROW_HEIGHT);

        ui->tableWidget_input->insertRow( i*2+1 );
        pCtrl = new CtrlInputChar( this, &ctrlParam );
        connect( pCtrl, SIGNAL(escapeMe(enum INPUT_TYPE)),
                 this, SLOT(FocusTableWdg(enum INPUT_TYPE)) );
        ui->tableWidget_input->setCellWidget(i*2+1, 0, pCtrl);
        ui->tableWidget_input->setRowHeight(i*2+1, TABLEWDG_ROW_HEIGHT);
    }
    m_nCtrlInputCreated += nSpawn;
    return true;
}

void GuideWindow::Enter(void* param)
{
    TRACEDEBUG("GuideWindow::Enter");

    EnterGuideParam_t enterParam = *(EnterGuideParam_t*)param;
    m_wt           = enterParam.wt_Guide;
    m_cmdItem.ScreenID  = enterParam.screenID;
    TRACELOG1( "GuideWindow::Enter(void* param) m_wt<%d> ScreenID<%x> bInEnter<%d>",
               m_wt, m_cmdItem.ScreenID, ms_bInEnter );

    TRACEDEBUG("GuideWindow::Enter(void* param) m_wt<%d>",m_wt);//HHY
    g_nLastWTNokey       = WT_GUIDE_WINDOW;
    g_nLastWTScreenSaver = g_nLastWTNokey;
    ms_showingWdg = this;
    g_bSendCmd    = true;


    //ENTER_FIRSTLY;
    ui->stackedWidget->setCurrentIndex( m_wt );
    switch ( m_wt )
    {
    case WGUIDE_PASSWORD:
    {
        //TRACEDEBUG("the size of the screen<%d>",width());//hhy
        //TRACEDEBUG("GuideWindow::Enter WGUIDE_PASSWORD");//HHY
        QString strStyleSheet;
        strStyleSheet = QString("{background:#676767;width:4px;}");
        this->setStyleSheet(strStyleSheet);


        QTableWidgetItem * item;
        item = ui->tableWidget_password->item(1, 0);
        //if (LCD_ROTATION_0DEG == g_cfgParam->ms_initParam.lcdRotation) { //竖屏
        //item->setText( "Please go to webpage " );//Please go to Web to change password. Press ‘Enter’ to continue”
        //}else if(LCD_ROTATION_90DEG == g_cfgParam->ms_initParam.lcdRotation) {
        if (width() > 160){
        item->setText( "Please go to webpage to change" );
        item->setTextAlignment( Qt::AlignCenter );
        } else {
            TRACEDEBUG("SMALL");
        item->setText( "Please go to webpage to " );
        item->setTextAlignment( Qt::AlignLeft );
        }

        item = ui->tableWidget_password->item(2, 0);
        if (width() > 160) {
        item->setText( "the factory default password." );//Please go to Web to change password. Press ‘Enter’ to continue”
        item->setTextAlignment( Qt::AlignCenter );
        } else {
        item->setText( "change the factory default" );
        item->setTextAlignment( Qt::AlignLeft );
        }

        // password.

            item = ui->tableWidget_password->item(3, 0);
            if (width() > 160){
            item->setText( "Press Enter to continue." );//Please go to Web to change password. Press ‘Enter’ to continue”
            item->setTextAlignment( Qt::AlignCenter );
            } else {
            item->setText( "password." );
            item->setTextAlignment( Qt::AlignLeft );
            }
            if (width() <= 160) {
            item = ui->tableWidget_password->item(4, 0);
            item->setText( "Press Enter to continue." );//Please go to Web to change password. Press ‘Enter’ to continue”
            item->setTextAlignment( Qt::AlignLeft );
        }


        ui->tableWidget_password->setFocus();

        TRACEDEBUG("SHOW finished");//hhy
        //restartQTimer();
        this->setFocus();
    }
        break;
      case WGUIDE_LANGUAGE:
      {
        //TRACEDEBUG("GuideWindow::Enter WGUIDE_LANGUAGE");//HHY
        SET_BACKGROUND_WIDGET( "GuideWindow",PosBase::strImgBack_Line );
        ui->tableWidget_language->setFocus();
        restartQTimer();

        #ifdef Q_OS_LINUX
        m_timerIdOpenFrameBuff = startTimer( 500 );
        #endif
      }
      break;

      case WGUIDE_ENTERESC:
      {
        SET_BACKGROUND_WIDGET( "GuideWindow",PosBase::strImgBack_Line_Title );

        QTableWidgetItem * item;
        item = ui->tableWidget_enter->item(2, 0);
        item->setText( tr("Installation Wizard") );
        item->setTextAlignment( Qt::AlignCenter );

        item = ui->tableWidget_enter->item(3, 0);
        item->setText( tr("ENT to continue") );
        item->setTextAlignment( Qt::AlignRight );

        item = ui->tableWidget_enter->item(4, 0);
        item->setText( tr("ESC to skip ") );
        item->setTextAlignment( Qt::AlignRight );

        restartQTimer();
        this->setFocus();
      }
      break;

      case WGUIDE_WIZARD:
      {
        m_nSelRowOld = 1;

        restartQTimer();

        SET_BACKGROUND_WIDGET( "GuideWindow",PosBase::strImgBack_Line_Title );

        ui->tableWidget_input->setFocus();
        m_cmdItem.CmdType   = CT_READ;
        for (int i=0; i<MAX_CTRL_ITEMS_GUIDE*2; ++i)
        {
          m_nSigIdx[i] = SIGIDX_INIT;
        }

        TRY_GET_DATA_MULTY;

        if (m_cmdItem.ScreenID == SCREEN_ID_WizSiteName)
        {
          ui->tableWidget_input->selectRow( ui->tableWidget_input->rowCount()-1 );
          ui->tableWidget_input->selectRow( 1 );
        }
        else
        {
          ui->tableWidget_input->selectRow( 0 );
          ui->tableWidget_input->selectRow( 1 );
        }

        SET_STYLE_SCROOLBAR( PAGES_NUMBER,m_cmdItem.ScreenID-MIN_SCREEN_ID_INPUT+1 );
      }
      break;

      case WGUIDE_CONTINUE:
      {
        restartQTimer();
        SET_BACKGROUND_WIDGET( "GuideWindow",PosBase::strImgBack_Title);

        QTableWidgetItem * item;
        item = ui->tableWidget_continue->item(0, 0);
        item->setText( tr("OK to exit")+"?" );
        item->setTextAlignment( Qt::AlignLeft );

        item = ui->tableWidget_continue->item(1, 0);
        item->setText( tr("ESC to exit") );
        item->setTextAlignment( Qt::AlignLeft );

        item = ui->tableWidget_continue->item(2, 0);
        item->setText( tr("ENT to continue") );
        item->setTextAlignment( Qt::AlignLeft );

        this->setFocus();
      }
      break;

      case WGUIDE_FINISH:
      {
        TRACEDEBUG("WGUIDE_FINISH show");//hhy
        restartQTimer();
        SET_BACKGROUND_WIDGET( "GuideWindow",PosBase::strImgBack_Line_Title );

        QTableWidgetItem * item;
        item = ui->tableWidget_finish->item(2, 0);
        item->setText( tr("Wizard finished") + "." );
        item->setTextAlignment( Qt::AlignCenter );

        this->setFocus();
      }
      break;

      case WGUIDE_MAX:
      {
        TRACEDEBUG( "GuideWindow::Enter enterWhere()" );
        enterWhere();
      }
      break;

      default:
      break;
    }
}

void GuideWindow::Leave()
{
    TRACELOG1( "GuideWindow::Leave()" );
    m_QTimer.stop();
    if ( m_timerId )
    {
        killTimer( m_timerId );
    }
    m_nWaitAppdata = 0;
}

void GuideWindow::Refresh()
{
    REFRESH_DATA_PERIODICALLY(
                m_cmdItem.ScreenID,
                "GuideWindow::Refresh()"
                );
}

void GuideWindow::RefreshNow()
{
    TRACEDEBUG( "GuideWindow::RefreshNow() ***" );
    TRY_GET_DATA_MULTY;
}

void GuideWindow::ShowData(void* pData)
{
    SET_INFO* pInfoItem      = NULL;
    CtrlInputChar*    pCtrl  = NULL;
    QTableWidgetItem* item   = NULL;

    TRACEDEBUG("GuideWindow::ShowData # Enter ...");
    if ( !pData )
    {
        TRACEDEBUG( ">>> GuideWindow::ShowData !pData return" );
        return;
    }

    memcpy( g_dataBuff, pData, sizeof(PACK_SETINFO) );
    PACK_SETINFO* info = (PACK_SETINFO*)g_dataBuff;
    m_sigNum = info->SetNum;
    if(m_sigNum == 0)
    {
        if(m_nLastScreenID > m_cmdItem.ScreenID)
        {
          --(m_cmdItem.ScreenID);
        }
        else
        {
        ++(m_cmdItem.ScreenID);
        }
        restartQTimer();
        ENTER_GUIDE_WINDOW;
        return;
    }

    int nScreenID = m_cmdItem.ScreenID;
    setTitleByScreenID( nScreenID );

    if (m_nCtrlInputCreated < m_sigNum)
    {
        createInputWidget( m_sigNum-m_nCtrlInputCreated );
    }
    TRACEDEBUG( ">>> GuideWindow::ShowData m_sigNum %d", m_sigNum );
    clearTxtInputTable();

    QString strSigName;
    int nSigValueType = 0;
    m_nRows = 0;
    if (m_sigNum > MAX_CTRL_ITEMS_GUIDE)
    {
        m_sigNum = MAX_CTRL_ITEMS_GUIDE;
        TRACEDEBUG( "GuideWindow::ShowData m_sigNum %d too many sig num", m_sigNum );
    }

    for(int nSigIdx=0; nSigIdx<m_sigNum; ++nSigIdx)
    {
        CtrlInputParam ctrlParam;
        TRACEDEBUG( "  > GuideWindow::ShowData for nSigIdx<%d>", nSigIdx);
        pInfoItem = &(info->SettingInfo[nSigIdx]);
        nSigValueType = pInfoItem->iSigValueType;
        TRACEDEBUG( "GuideWindow::ShowData SigValueType<%d>", nSigValueType );

        if (nScreenID == SCREEN_ID_WizSiteName)
        {
            // char input
            ctrlParam.ipt = IPT_SBT_CHAR;
            ctrlParam.strInit =
                    QString(
                        pInfoItem->cEnumText[0]
                        );
            pCtrl = (CtrlInputChar*)
                    (ui->tableWidget_input->cellWidget(nSigIdx*2+1, 0));
            pCtrl->setParam( &ctrlParam );
            m_nRows = 1;
            m_nSigIdx[m_nRows] = nSigIdx;
            ui->tableWidget_input->selectRow( m_nCtrlInputCreated*2-1 );
            ui->tableWidget_input->selectRow( 1 );
            TRACELOG1( "GuideWindow::ShowData SCREEN_ID_WizSiteName value<%s>", pInfoItem->cEnumText[0] );
            break;
        }
        else if (nScreenID == SCREEN_ID_WizCommon)
        {
            // date time
            SIG_TIME time_t = pInfoItem->vSigValue.dtValue;
            setTableItemDateTime( time_t );
            TRACELOG1( "GuideWindow::ShowData SCREEN_ID_WizCommon value<%d>", time_t );
            break;
        }
        else if (nScreenID == SCREEN_ID_WizCommunicate)
        {
            TRACELOG1( "GuideWindow::ShowData SCREEN_ID_WizCommunicate" );
            ULONG ulIP = pInfoItem->vSigValue.ulValue;
            setTableItemIP(ulIP, nSigIdx);
            continue;
        }

        // others
        if (nSigValueType<VAR_LONG || nSigValueType>VAR_ENUM)
        {
            TRACELOG1( "GuideWindow::ShowData nSigValueType %d not martch!", nSigValueType );
            continue;
        }

        // sigName
        m_nRows = nSigIdx*2;
        strSigName = QString(pInfoItem->cSigName) + ":";
        item = ui->tableWidget_input->item(m_nRows, 0);
        TRACEDEBUG("GuideWindow::ShowData 1111 strSigName:%s",qPrintable(strSigName));
        item->setText( strSigName );

        // input widget
        m_nRows = nSigIdx*2+1;
        m_nSigIdx[m_nRows] = nSigIdx;
        makeCtrlparamBySigtype(&ctrlParam, pInfoItem);
        pCtrl = (CtrlInputChar*)
                (ui->tableWidget_input->cellWidget(m_nRows, 0));

        TRACEDEBUG( "GuideWindow::ShowData rows<%d> nSigIdx<%d> nSigValueType<%d> ipt<%d> strSigName<%s:> unit<%s>",
                    m_nRows, nSigIdx,
                    nSigValueType, ctrlParam.ipt,
                    pInfoItem->cSigName,
                    pInfoItem->cSigUnit
                    );
        pCtrl->setParam( &ctrlParam );
    }
    TRACEDEBUG("GuideWindow::ShowData # End ...");
}

void GuideWindow::setTitleByScreenID(int nScreenID)
{
    ui->label_name->setAlignment (Qt::AlignVCenter);
    switch (nScreenID)
    {
        case SCREEN_ID_WizSiteName:
        {
            ui->label_name->setText( tr("  Site Name") );
        }
        break;

        case SCREEN_ID_WizBatt:
        {
            ui->label_name->setText( tr("  Battery Settings") );
        }
        break;

        case SCREEN_ID_WizCapacity:
        {
            ui->label_name->setText( tr("  Capacity Settings") );
        }
        break;

        case SCREEN_ID_WizECO:
        {
            ui->label_name->setText( tr("  ECO Parameter") );
        }
        break;

        case SCREEN_ID_WizAlarm:
        {
            ui->label_name->setText( tr("  Alarm Settings") );
        }
        break;

        case SCREEN_ID_WizCommon:
        {
            ui->label_name->setText( tr("  Common Settings") );
        }
        break;

        case SCREEN_ID_WizCommunicate:
        {
            ui->label_name->setText( tr("  IP address") );
        }
        break;

        default:
        {
            ui->label_name->setText( "" );
        }
        break;
    }
}

void GuideWindow::setTableItemDateTime(SIG_TIME time_t)
{
    CtrlInputChar*    pCtrl  = NULL;
    QTableWidgetItem* item   = NULL;
    QString strSigName;

    CtrlInputParam ctrlParam;
    // Date
    m_nRows = 0;
    strSigName = tr("Date") + ":";
    item = ui->tableWidget_input->item(m_nRows, 0);
    item->setText( strSigName );

    ++m_nRows;
    m_nSigIdx[m_nRows] = 0;
    ctrlParam.ipt      = IPT_DATE;

    ctrlParam.nInit = time_t;
    pCtrl = (CtrlInputChar*)
            (ui->tableWidget_input->cellWidget(m_nRows, 0));
    pCtrl->setParam( &ctrlParam );

    // Time
    ++m_nRows;
    strSigName = tr("Time") + ":";
    item = ui->tableWidget_input->item(m_nRows, 0);
    item->setText( strSigName );

    ++m_nRows;
    m_nSigIdx[m_nRows] = 0; // date and time is in the same value type VAR_DATE_TIME
    ctrlParam.ipt      = IPT_TIME;

    pCtrl = (CtrlInputChar*)
            (ui->tableWidget_input->cellWidget(m_nRows, 0));
    pCtrl->setParam( &ctrlParam );
}

void GuideWindow::setTableItemIP(ULONG ulIP, int nSigIdx)
{
    TRACEDEBUG( "GuideWindow::setTableItemIP(ULONG ulIP <%u>, int nSigIdx <%d>)", ulIP, nSigIdx );
    // 0:DHCP 1:IP 2:MASK 3:Gateway
    CtrlInputChar*    pCtrl  = NULL;
    QTableWidgetItem* item   = NULL;
    QString strSigName;
    static bool bDHCP = false;

    CtrlInputParam ctrlParam;
    ctrlParam.ipt      = IPT_IP;
    switch (nSigIdx)
    {
    case 0:
    {
        strSigName = tr("DHCP") + ":";

        ctrlParam.ipt = IPT_SBT_CUSTOM;
        ctrlParam.nMin = 0;
        ctrlParam.nMax = 2;
        QStringList strList;
        strList << tr("Disabled")
                << tr("Enabled")
                << tr("Error");
        ctrlParam.strListEnumText = strList;
        if (ulIP > 2)
        {
            ulIP = 2;
        }
        if (ulIP == 0)
        {
            bDHCP = false;
        }
        else
        {
            bDHCP = true;
        }
        ctrlParam.strInit = strList[ulIP];
        ctrlParam.nInit   = (long)(ulIP);
    }
        break;

    case 1:
        strSigName = tr("IP Address") + ":";
        break;

    case 2:
        strSigName = tr("MASK") + ":";
        break;

    case 3:
        strSigName = tr("Gateway") + ":";
        break;

    default:
        strSigName = "";
        break;
    }

    // SigName
    m_nRows = nSigIdx*2;
    item = ui->tableWidget_input->item(m_nRows, 0);
    item->setText( strSigName );

    m_nRows = nSigIdx*2+1;
    if (nSigIdx==0 || !bDHCP)
    {
        m_nSigIdx[m_nRows-1] = SIGIDX_INIT;
        m_nSigIdx[m_nRows]   = nSigIdx;
        TRACEDEBUG( "GuideWindow::setTableItemIP Rows<%d> m_nSigIdx[]<%d>", m_nRows, nSigIdx );
    }
    else
    {
        TRACEDEBUG( "GuideWindow::setTableItemIP SIGIDX_READONLY Rows<%d> <%d>", m_nRows-1, m_nRows );
        m_nSigIdx[m_nRows-1] = SIGIDX_READONLY;
        m_nSigIdx[m_nRows]   = SIGIDX_READONLY;
    }

    ctrlParam.ulInit   = ulIP;
    item = ui->tableWidget_input->item(m_nRows, 0);
    pCtrl = (CtrlInputChar*)
            (ui->tableWidget_input->cellWidget(m_nRows, 0));
    pCtrl->setParam( &ctrlParam );
}

void GuideWindow::clearTxtInputTable()
{
    QTableWidgetItem* item = NULL;
    CtrlInputChar*    pCtrl  = NULL;
    for(int nSigIdx=0; nSigIdx<m_nCtrlInputCreated; ++nSigIdx)
    {
        item = ui->tableWidget_input->item(nSigIdx*2, 0);
        item->setText( "" );
        pCtrl = (CtrlInputChar*)
                (ui->tableWidget_input->cellWidget(nSigIdx*2+1, 0));
        pCtrl->setCtrlVisible( false );
    }
}

void GuideWindow::timerEvent(QTimerEvent* event)
{
#ifdef Q_OS_LINUX
    if(event->timerId() == m_timerIdOpenFrameBuff)
    {
        killTimer( m_timerIdOpenFrameBuff );
        sys_actFrameBuffer();
        // enable keyboard
        sys_setKeyboard( true );
//        sys_setLCDGarmar( GAMMA_LEVEL_1 );//调节LCD Garmar值
    }
#endif

    if(event->timerId() == m_timerIdSpawn)
    {
        if ( !createInputWidget(1) )
        {
            killTimer( m_timerIdSpawn );
        }
    }
}

void GuideWindow::changeEvent(QEvent *event)
{
    TRACEDEBUG("GuideWindow::changeEvent");//HHY
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

// 跳转行
void GuideWindow::sltTableKeyPress(int key)
{
    switch (m_wt)
    {
        case WGUIDE_LANGUAGE:
        {
            restartQTimer();
            int nGotoRow = 0;
            int nRowCount = ui->tableWidget_language->rowCount();
            int nSelRow = ui->tableWidget_language->selectedRow();
            int nBackwardRow = nSelRow;

            QLabel* pLabel = NULL;
            switch ( key )
            {
                case Qt::Key_Up:
                {
                    nGotoRow = nSelRow;
                    ++nBackwardRow;
                    TRACEDEBUG("1111 nSelRow:%d\tnRowCount:%d\tnBackwardRow:%d",nSelRow,nRowCount,nBackwardRow);
                    // 前行设置为未选中图片
                    pLabel = (QLabel*)ui->tableWidget_language->cellWidget(nBackwardRow, 0);
                    pLabel->setStyleSheet( "background-image:url(" + m_pixmap[nBackwardRow*2] + ")" );

                    ui->tableWidget_language->selectRow( nGotoRow );
                    pLabel = (QLabel*)ui->tableWidget_language->cellWidget(nGotoRow, 0);
                    pLabel->setStyleSheet( "background-image:url(" + m_pixmap[nGotoRow*2+1] + ")" );
                }
                break;

                case Qt::Key_Down:
                {
                    if (nSelRow > (nRowCount-2))
                    {
                      nGotoRow = 0;
                      nBackwardRow = nRowCount-2;
                    }
                    else
                    {
                      nGotoRow = nSelRow;
                      --nBackwardRow;
                    }

                    TRACEDEBUG("2222 nSelRow:%d\tnRowCount:%d\tnBackwardRow:%d\tnGotoRow:%d",
                               nSelRow,nRowCount,nBackwardRow,nGotoRow);
                    // 前行设置为未选中图片
                    pLabel = (QLabel*)ui->tableWidget_language->cellWidget(nBackwardRow, 0);

                    pLabel->setStyleSheet( "background-image:url(" + m_pixmap[nBackwardRow*2] + ")" );

                    ui->tableWidget_language->selectRow( nGotoRow );
                    pLabel = (QLabel*)ui->tableWidget_language->cellWidget(nGotoRow, 0);
                    pLabel->setStyleSheet( "background-image:url(" + m_pixmap[nGotoRow*2+1] + ")" );
                }
                break;

                default:
                break;
            }
            TRACELOG1( "GuideWindow::slotTableKeyPress nSelRow<%d> nBackwardRow<%d> nGotoRow<%d>",
                       nSelRow, nBackwardRow, nGotoRow );
        }
        break;

        case WGUIDE_WIZARD:
        {
            TRACEDEBUG("3333 GuideWindow::slotTableKeyPress # Before keyPressWizTable ...");
            keyPressWizTable( key );
        }
        break;

        default:
        break;
    }
}

void GuideWindow::keyPressEvent(QKeyEvent* keyEvent)
{
    TRACEDEBUG("GuideWindow::keyPressEvent");//hhy
    if ((m_wt==WGUIDE_LANGUAGE) ||
            (m_wt==WGUIDE_ENTERESC) ||
            (WGUIDE_CONTINUE))
    {
        restartQTimer();
    }

    int key = keyEvent->key();
    TRACEDEBUG( "GuideWindow::keyPressEvent m_wt<%d> key<%x>\n", m_wt, key );
    switch (m_wt)
    {
        case WGUIDE_LANGUAGE:
        {
            TRACEDEBUG("WGUIDE_LANGUAGE");//HHY
            int nSelRow = ui->tableWidget_language->selectedRow();
            switch (key)
            {
            //TRACEDEBUG("KEY<%d>",key);//hhy
                case Qt::Key_Up:
                {
                //TRACEDEBUG("Qt::Key_Up");//HHY
                    if (nSelRow == 0)
                    {
                        int nGotoRow = ui->tableWidget_language->rowCount()-2;
                        int nBackwardRow = 0;
                        QLabel* pLabel = NULL;

                        // 前行设置为未选中图片
                        pLabel = (QLabel*)ui->tableWidget_language->cellWidget(nBackwardRow, 0);
                        pLabel->setStyleSheet( "background-image:url(" + m_pixmap[nBackwardRow*2] + ")" );

                        ui->tableWidget_language->selectRow( nGotoRow );
                        pLabel = (QLabel*)ui->tableWidget_language->cellWidget(nGotoRow, 0);
                        pLabel->setStyleSheet( "background-image:url(" + m_pixmap[nGotoRow*2+1] + ")" );
                    }
                }
                break;

                case Qt::Key_Return:
                case Qt::Key_Enter:
                {
                //TRACEDEBUG("GuideWindow::keyPressEvent Qt::Key_Enter");//HHY
                    LANGUAGE_TYPE langApp = g_cfgParam->ms_initParam.langApp;

                    QString strAttrLangType = g_mapLangStr[langApp];

                    int nLangTypeIndex = m_strListLangType.indexOf (strAttrLangType);
                    if(nLangTypeIndex == -1)
                    {
                        return;
                    }

                    if ((nSelRow != nLangTypeIndex) && \
                            (nSelRow != 0))
                    {
                        //g_cfgParam->ms_initParam.langApp = LANGUAGE_TYPE(nSelRow);
                        m_QTimer.stop();
                        DlgInfo dlg(
                                    tr("Rebooting"),
                                    POPUP_TYPE_QUESTION_LANGUAGE
                                    );

                        //转换成真实的语言类型。
                        QString strAttrLang = m_strListLangType.at (nSelRow);
                        int nLangType = g_mapStrLangType[strAttrLang.toLower ()];
                        dlg.setLanguageParam(
                                    LANGUAGE_TYPE(nLangType),
                                    LANG_LOCATE_Native
                                    );
                        if (QDialog::Accepted == dlg.exec())
                        {
        //                    g_cfgParam->ms_initParam.langApp =
        //                            LANGUAGE_TYPE( nSelRow );
        //                    g_cfgParam->ms_initParam.langLocate =
        //                            LANGUAGE_LOCATE( LANG_LOCATE_Native );
        //                    g_cfgParam->writeLanguageApp();
        //                    g_cfgParam->writeLanguageLocal();
        //                    system( "reboot" );
                        }
                        else
                        {
                            restartQTimer();
                        }

                        return;
                    }

                    if (nSelRow == 0)
                    {
                        g_cfgParam->ms_initParam.langLocate = LANG_LOCATE_English;
                    }
                    else
                    {
                        g_cfgParam->ms_initParam.langLocate = LANG_LOCATE_Native;
                    }
                    g_cfgParam->writeLanguageLocal();

                    TRACEDEBUG( "GuideWindow::keyPressEvent WGUIDE_LANGUAGE Key_Enter enterWhere()\n" );
                            if (Lcd_GetPaswdChangeFlag() == 0)
                            {
                                TRACEDEBUG("==================yes");
                                m_wt = WGUIDE_PASSWORD;
                                ENTER_GUIDE_WINDOW;//go into password
                            } else {
                                //不显示 正常流程
                                TRACEDEBUG("==================no");
                                enterWhere();

                            }
                   //enterWhere();

                }
                break;

                case Qt::Key_Escape:
                {
                    TRACEDEBUG( "GuideWindow::keyPressEvent WGUIDE_LANGUAGE Key_Escape enterWhere()" );
                    enterWhere();
                }
                break;
            } //end switch key
        }
        break;

        case WGUIDE_ENTERESC:
        {
            TRACEDEBUG("GuideWindow::keyPressEvent 2222 WGUIDE_ENTERESC");
            switch (key)
            {
                case Qt::Key_Return:
                case Qt::Key_Enter:
                {
                    m_QTimer.stop();
                    emit goToLoginWindow( LOGIN_TYPE_GUIDE );

                }
                break;

                case Qt::Key_Escape:
                {
                    TRACEDEBUG( "GuideWindow::keyPressEvent WGUIDE_ENTERESC Key_Escape enterWhere()" );
                    enterWhere();
                }
                break;
            }//end switch key
        }
        break;

        case WGUIDE_WIZARD:
        {
            TRACEDEBUG("GuideWindow::keyPressEvent 3333 WGUIDE_WIZARD");
            keyPressWizTable( key );
        }
        break;

        case WGUIDE_CONTINUE:
        {
            TRACEDEBUG("GuideWindow::keyPressEvent 3333 WGUIDE_CONTINUE");
            switch (key)
            {
                case Qt::Key_Return:
                case Qt::Key_Enter:
                {
                    m_wt = WGUIDE_WIZARD;
                    m_cmdItem.ScreenID = SCREEN_ID_WizECO;
                    ENTER_GUIDE_WINDOW;
                }
                break;

                case Qt::Key_Escape:
                {
                    TRACEDEBUG( "GuideWindow::keyPressEvent WGUIDE_CONTINUE Key_Escape enterWhere()" );
                    enterWhere();
                }
                break;

                case Qt::Key_Up:
                {
                    m_wt = WGUIDE_WIZARD;
                    m_cmdItem.ScreenID = IF_CONTINUE_SCREEN_ID;
                    ENTER_GUIDE_WINDOW;
                }
                break;

                default:
                break;
            }
        }
        break;

        case WGUIDE_FINISH:
        {
            TRACEDEBUG("GuideWindow::keyPressEvent 4444 WGUIDE_FINISH");
            switch (key)
            {
                case Qt::Key_Return:
                case Qt::Key_Enter:
                case Qt::Key_Escape:
                {
                    TRACEDEBUG( "GuideWindow::keyPressEvent WGUIDE_FINISH Key_press enterWhere()" );
                    enterWhere();
                }
                break;

                case Qt::Key_Up:
                {
                    m_wt = WGUIDE_WIZARD;
                    m_cmdItem.ScreenID = MAX_SCREEN_ID_INPUT;
                    ENTER_GUIDE_WINDOW;
                }
                break;

                default:
                break;
            }
        }
        break;
        case WGUIDE_PASSWORD:{
        if (key == Qt::Key_Enter) {
        TRACEDEBUG("GuideWindow::keyPressEvent password");//hhy
        enterWhere();
        }
        }
        break;
        default:
        {
            TRACEDEBUG("GuideWindow::keyPressEvent 5555 WGUIDE_FINISH");
            switch (key)
            {
                case Qt::Key_Return:
                case Qt::Key_Enter:
                case Qt::Key_Escape:
                {
                    enterWhere();
                }
                break;
            }
        }
        break;
    }// end switch wdg
}

void GuideWindow::keyPressWizTable(int key)
{
    int nSelRow = ui->tableWidget_input->selectedRow();
//    TRACELOG1( "GuideWindow::keyPressWizTable nSelRow<%d>", nSelRow );
    int nGotoRow = 0;
    switch ( key )
    {
        case Qt::Key_Return:
        case Qt::Key_Enter:
        {
            TRACEDEBUG( "GuideWindow::keyPressWizTable Key_Return nSelRow<%d> sigIdx<%d>", nSelRow, m_nSigIdx[nSelRow] )
            m_QTimer.stop();
            if (nSelRow>-1 && m_nSigIdx[nSelRow]!=SIGIDX_INIT &&
                    m_nSigIdx[nSelRow]!=SIGIDX_READONLY)
            {
                CtrlInputChar* pCtrl = (CtrlInputChar*)
                        (ui->tableWidget_input->cellWidget(nSelRow, 0));

                modifyDHCPItem(pCtrl, nSelRow, false);
                pCtrl->Enter();
            }
        }
        break;

        case Qt::Key_Escape:
        {
            restartQTimer();
        }
        break;

        case Qt::Key_Up:
        {
            m_nLastScreenID = m_cmdItem.ScreenID;
            TRACEDEBUG( "GuideWindow::keyPressWizTable Key_Up ...");
            if (m_cmdItem.ScreenID <= MIN_SCREEN_ID_INPUT)
            {
                nGotoRow = m_nCtrlInputCreated*2-1;
                ui->tableWidget_input->selectRow( nGotoRow );
                nGotoRow = 1;
                ui->tableWidget_input->selectRow( nGotoRow );
            }
            else
            {
                if (nSelRow==0 && m_nSigIdx[0]<0)
                {
                    if (--(m_cmdItem.ScreenID) >= MIN_SCREEN_ID_INPUT)
                    {
                        TRACEDEBUG( "GuideWindow::keyPressWizTable Key_Up Before ENTER_GUIDE_WINDOW show m_wt:%d",m_wt );
                        ENTER_GUIDE_WINDOW;
                    }
                }
                else
                {
                    if (nSelRow%2 == 0)
                    {
                        nGotoRow = nSelRow-1;
                        if (nGotoRow-1 >= 0)
                            ui->tableWidget_input->selectRow( nGotoRow-1 );
                        ui->tableWidget_input->selectRow( nGotoRow );
                    }
                    TRACEDEBUG( "GuideWindow::keyPressWizTable Key_Up nGotoRow<%d>", nGotoRow );
                }
            }
        }
        break;

        case Qt::Key_Down:
        {
            TRACEDEBUG( "GuideWindow::keyPressWizTable Key_Down ...");
            m_nLastScreenID = m_cmdItem.ScreenID;
            if (nSelRow >= m_nRows)
            {
                if (m_cmdItem.ScreenID >= MAX_SCREEN_ID_INPUT)
                {
                    m_wt = WGUIDE_FINISH;
                    TRACEDEBUG( "GuideWindow::keyPressWizTable Qt::Key_Down WGUIDE_FINISH");
                    ENTER_GUIDE_WINDOW;
                }
                else
                {
                    if (m_cmdItem.ScreenID == IF_CONTINUE_SCREEN_ID)
                    {
                        TRACEDEBUG( "GuideWindow::keyPressWizTable Qt::Key_Down SCREEN_ID_WizBatt escape?" );
                        m_QTimer.stop();

                        m_wt = WGUIDE_CONTINUE;
                        TRACEDEBUG( "GuideWindow::keyPressWizTable Qt::Key_Down WGUIDE_CONTINUE %x", m_cmdItem.ScreenID );
                        ENTER_GUIDE_WINDOW;
                    }
                    else
                    {
                        ++(m_cmdItem.ScreenID);
                        m_wt = WGUIDE_WIZARD;
                        TRACEDEBUG( "GuideWindow::keyPressWizTable Qt::Key_Down %x", m_cmdItem.ScreenID );
                        ENTER_GUIDE_WINDOW;
                    }
                }
            }
            else
            {
                TRACEDEBUG( "GuideWindow::keyPressWizTable Key_Down 1111 ...");
                if (nSelRow%2 == 0)
                {
                    nGotoRow = nSelRow+1;
                    ui->tableWidget_input->selectRow( nGotoRow );
                }
            }
        }
        break;
    }
}

// 提交数据
void GuideWindow::FocusTableWdg(enum INPUT_TYPE ipt)
{
    int nSelRow = ui->tableWidget_input->selectedRow();
    TRACELOG1( "GuideWindow::FocusTableWdg() ipt<%d> nSelRow<%d>", ipt, nSelRow );
    if (m_nSigIdx[nSelRow] != SIGIDX_INIT)
    {
        CtrlInputChar* pCtrl = (CtrlInputChar*)
                (ui->tableWidget_input->cellWidget(nSelRow, 0));
        if (IPT_SUBMIT == ipt)
        {
            int nRet = submitAppParamData(
                        pCtrl,
                        &m_cmdItem,
                        m_nSigIdx[nSelRow],
                        pCtrl->getParam()
                        );
            if (nRet == SET_PARAM_ERR_OK)
            {
                switch (m_cmdItem.ScreenID)
                {
                case SCREEN_ID_WizECO:
                {
                    if (nSelRow == 1)
                    {
                        mainSleep( 3000 );
                    }
                }
                    break;

                default:
                    break;
                }
            }
        }
        RefreshNow();
        restartQTimer();
    }

    ui->tableWidget_input->setFocus();
}

void GuideWindow::modifyDHCPItem(CtrlInputChar* pCtrl, int nSelRow, bool bAdd)
{
    if (m_wt == WGUIDE_WIZARD &&
            m_cmdItem.ScreenID == SCREEN_ID_WizCommunicate)
    {
        PACK_SETINFO* pPackInfoFromApp =
                (PACK_SETINFO*)g_dataBuff;
        SET_INFO*     pSetInfoFromApp  =
                &(pPackInfoFromApp->SettingInfo[m_nSigIdx[nSelRow]]);
        int nEquipID = pSetInfoFromApp->iEquipID;
        int nSigID   = pSetInfoFromApp->iSigID;
        TRACEDEBUG( "GuideWindow::modifyDHCPItem EquipID<%d> SigID<%d>", nEquipID, nSigID );
        // DHCP
        if (nEquipID==-1 && nSigID==SIGID_SPECIAL_DHCP)
        {
            CtrlInputParam* pParam = pCtrl->getParam();
            if ( bAdd )
            {
                pParam->nMax += 1;
            }
            else
            {
                pParam->nMax -= 1;
            }
            pCtrl->setParam( pParam );
        }
    }
}

void GuideWindow::restartQTimer(int msec)
{
    TRACEDEBUG( "GuideWindow::restartQTimer(int msec)" );
    if ( m_QTimer.isActive() )
    {
        m_QTimer.stop();
    }

    g_bSendCmd    = true;
    switch (m_wt)
    {
    case WGUIDE_LANGUAGE:
    {
        TRACEDEBUG("TIMER language");//hhy
        QFile file( FILENAME_NEED_WIZARD );
        if ( file.exists() )
            m_QTimer.start( TIME_OUT_LANG );
    }
        break;

    case WGUIDE_ENTERESC:
        m_QTimer.start( TIME_OUT_WIZARD );
        break;

    case WGUIDE_WIZARD:
    {
        TRACEDEBUG("TIMER wizard");//hhy
        switch (m_cmdItem.ScreenID)
        {
        case SCREEN_ID_WizCommon:
            m_QTimer.start( 500 );
            break;

        default:
            m_QTimer.start( 500 );
            break;
        }
    }
        break;

    case WGUIDE_CONTINUE:
    {
        TRACEDEBUG("TIMER continue");//hhy
        m_QTimer.start( TIME_OUT_WIZARD );
    }
        break;

    case WGUIDE_FINISH:
    {
        m_QTimer.start( TIME_OUT_POPUPDLG );
    }
        break;

    default:
    {
        if (msec > 0)
        {
            m_QTimer.start( msec );
        }
        else
        {
            m_QTimer.start();
        }
    }
        break;
    }
}

void GuideWindow::TimerHandler()
{
    switch ( m_wt )
    {
    case WGUIDE_WIZARD:
        Refresh();
        break;

    default:
        m_QTimer.stop();
        TRACEDEBUG( "GuideWindow::TimerHandler enterWhere()" );
        enterWhere();
        break;
    }
}

bool GuideWindow::telledAppLang()
{
    TRACEDEBUG( ">>> GuideWindow::telledAppLang()******\n" );
    int langType = g_cfgParam->ms_initParam.langApp;
    if (g_cfgParam->ms_initParam.langLocate == LANG_LOCATE_English)
    {
        langType = LANG_English;
    }
    g_cfgParam->ms_initParam.langType = (LANGUAGE_TYPE)langType;

    m_QTimer.stop();

    void* pData = NULL;
    CmdItem cmdItem;
    cmdItem.CmdType   = CT_SET;
    cmdItem.ScreenID  = 0x111111;
    CMD_SET_INFO* setinfo = &(cmdItem.setinfo);
    setinfo->EquipID = 1;
    setinfo->SigID   = 22;
    setinfo->SigType = 2;
    setinfo->value.lValue = g_cfgParam->ms_initParam.langLocate;
    TRACEDEBUG( "GuideWindow::telledAppLang() langType<%d> langLocate<%d> ScreenID<%06x>\n", langType, g_cfgParam->ms_initParam.langLocate, cmdItem.ScreenID );

    if ( data_getDataSync(&cmdItem, &pData) )
    {
        TRACEDEBUG( "GuideWindow::keyPressEvent data_getDataSync set language ScreenID<%06x> OK\n", cmdItem.ScreenID );
        return false;
    }
    int nRet = *(int*)pData;

    TRACEDEBUG( "GuideWindow::telledAppLang set language data_getDataSync return<%d> 1:OK\n", nRet );
    if (nRet != 1)
    {
        DlgInfo dlg(
                    tr("Set language failed"),
                    POPUP_TYPE_CRITICAL
                    );
        dlg.exec();
        return false;
    }

    TRACEDEBUG( "GuideWindow::telledAppLang() finished ******>>>\n" );

    g_cfgParam->setLanguageFont();
    return true;
}

int GuideWindow::enterWhere()
{
    TRACEDEBUG( "GuideWindow::enterWhere()\n" );
    if (m_wt == WGUIDE_LANGUAGE)
    {
        ++m_nWaitAppdata;
        TRACEDEBUG( "GuideWindow::enterWhere() m_nWaitAppdata<%d>", m_nWaitAppdata );
        if (m_nWaitAppdata > 1)
            return 0;
    }

//    if (m_wt == WGUIDE_PASSWORD) {
//        //enter
//        TRACEDEBUG("enter password");
//        ENTER_GUIDE_WINDOW;
//        //return 0;
//    }
    static bool bTelledAppLang = false;
    if ( !bTelledAppLang )
    {
        // LCD rotation
        if (0 == g_cfgParam->initParam(true) )
        {
            QFile file( FILENAME_LCD_ROTATION );
            QString strInfo = tr("Adjust LCD")+"...";
            TRACEDEBUG( "GuideWindow::enterWhere width*height <%d*%d>\n",
                        width(), height()
                        );
            if (width() > 160)
            {
                // Big Screen
                TRACEDEBUG( "GuideWindow::enterWhere BigScreen lcdRotation<%d>\n", g_cfgParam->ms_initParam.lcdRotation );
                if (LCD_ROTATION_BIG != g_cfgParam->ms_initParam.lcdRotation)
                {
                    if ( file.exists() )
                        file.remove();

                    if ( SET_PARAM_ERR_OK==
                         setLCDRotation(LCD_ROTATION_BIG) )
                    {
                        g_cfgParam->initParam();
                        g_cfgParam->ms_initParam.lcdRotation =
                                LCD_ROTATION_BIG;
//                        DlgInfo dlg(
//                                    strInfo,
//                                    POPUP_TYPE_INFOR_ALIVE,
//                                    1000
//                                    );
//                        dlg.execShell( "reboot" );
//                        dlg.exec();
                    }
//                    return -1;
                }
                TRACEDEBUG( "GuideWindow::enterWhere 2 lcdRotation<%d>\n", g_cfgParam->ms_initParam.lcdRotation );
            }
            else
            {
                // small Screen
                TRACEDEBUG( "GuideWindow::enterWhere small Screen lcdRotation<%d>", g_cfgParam->ms_initParam.lcdRotation );
                if (LCD_ROTATION_0DEG == g_cfgParam->ms_initParam.lcdRotation)
                {
                    if ( file.exists() )
                    {
                        file.remove();
                        DlgInfo dlg(
                                    strInfo,
                                    POPUP_TYPE_INFOR_ALIVE,
                                    1000
                                    );
                        dlg.execShell( "reboot" );
                        dlg.exec();
                        return -1;
                    }
                }
                else if (LCD_ROTATION_90DEG == g_cfgParam->ms_initParam.lcdRotation)
                {
                    if ( !file.exists() )
                    {
                        file.open( QIODevice::ReadWrite | QIODevice::Text );
                        file.close();
                        DlgInfo dlg(
                                    strInfo,
                                    POPUP_TYPE_INFOR_ALIVE,
                                    1000
                                    );
                        dlg.execShell( "reboot" );
                        dlg.exec();
                        return -1;
                    }
                }
                else
                {
                    if ( file.exists() )
                        file.remove();

                    if ( SET_PARAM_ERR_OK==
                         setLCDRotation(LCD_ROTATION_0DEG) )
                    {
                        DlgInfo dlg(
                                    strInfo,
                                    POPUP_TYPE_INFOR_ALIVE,
                                    1000
                                    );
                        dlg.execShell( "reboot" );
                        dlg.exec();
                        return -1;
                    }
                }
            }
        }
        if ( !telledAppLang() )
        {
            TRACEDEBUG("GOTO language");//hhy
            m_nWaitAppdata = 0;
            m_wt = WGUIDE_LANGUAGE;
            ENTER_GUIDE_WINDOW;
            return 2;
        }
        bTelledAppLang = true;
    }

    QFile file( FILENAME_NEED_WIZARD );
    if ( file.exists() )
    {
        //文件存在就不显示并return
        enterHomePage();
        return 0;
    }

    file.open( QIODevice::ReadWrite | QIODevice::Text );
    file.close();

    if (m_wt == WGUIDE_PASSWORD)
        m_wt = WGUIDE_LANGUAGE;
    TRACEDEBUG("ENTER WIZARD 11111");//HHY
    enterWizard();
    return 1;
}

void GuideWindow::enterHomePage()
{
    emit sigDetectAlarm();
    emit goToHomePage();
}

void GuideWindow::enterWizard()
{
    //g_cfgParam->initParam();
    //WGUIDE_WIZARD

    m_wt = (WIDGET_GUIDE)(m_wt+1);
    if ((m_wt >= WGUIDE_MAX) && (m_wt != WGUIDE_PASSWORD))
    {
        TRACEDEBUG("enterWizard WGUIDE_FINISH");
        m_wt = WGUIDE_FINISH;
    }
    ENTER_GUIDE_WINDOW;
}

void GuideWindow::on_tableWidget_input_itemSelectionChanged()
{
    int nSelRow = ui->tableWidget_input->selectedRow();
    CtrlInputChar* pCtrl = NULL;

    if (m_nSigIdx[m_nSelRowOld]>SIGIDX_INIT &&
            m_nSelRowOld%2!=0)
    {
        pCtrl = (CtrlInputChar*)
                (ui->tableWidget_input->cellWidget(m_nSelRowOld, 0));
        pCtrl->setHighLight( false );
    }

    if (m_nSigIdx[nSelRow]>SIGIDX_INIT &&
            nSelRow%2!=0)
    {
        TRACEDEBUG( "GuideWindow::on_tableWidget_input_itemSelectionChanged() select row<%d> m_nSelRowOld<%d>", nSelRow, m_nSelRowOld );

        pCtrl = (CtrlInputChar*)
                (ui->tableWidget_input->cellWidget(nSelRow, 0));
        pCtrl->setHighLight( true );
        m_nSelRowOld = nSelRow;
    }
}

/*==========================================================================*
* FUNCTION :  Web_GetPaswdChangeFlag
* PURPOSE  :
* CALLS    :
* CALLED BY:
* ARGUMENTS:  void
* RETURN   :  int, 0--prompt; 1-- not need prompt
* COMMENTS :
* CREATOR  :                 DATE:
*==========================================================================*/
int GuideWindow::Lcd_GetPaswdChangeFlag(void)
{

#define   PASS_CHANGE_FILE_ROUTE              "/home/app_script/pass_change_flag.log"

       int iAccess;
       iAccess = access(PASS_CHANGE_FILE_ROUTE, F_OK);
       if( 0 == iAccess )
       {
              TRACEDEBUG("access: File exist~~~~~~~~~~~~~~~~~~~~~~##########################~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

              char line [256];
              FILE *fp;

              fp = fopen (PASS_CHANGE_FILE_ROUTE, "r");


              if (fp == NULL)
              {
                     fp = fopen (PASS_CHANGE_FILE_ROUTE, "r");
                     if (fp == NULL)
                     {
                            TRACEDEBUG("cannot open /home/app_script/pass_change_flag.log- burps\n\r");
                            return -1;
                     }
              }

              while (fgets (line, sizeof(line), fp))
              {
                     if (strstr(line, "yes"))
                     {
                            TRACEDEBUG("strstr: yes~~, Time = %d\n",time(NULL));

                            fclose(fp);
                            return 0;
                   }
                   else if (strstr(line, "no"))
                   {
                          TRACEDEBUG("strstr: no~~\n");
                          fclose(fp);
                          return 1;
                   }
                   else
                   {
                          TRACEDEBUG("strstr: other~~\n");
                          fclose(fp);
                          return 1;
                   }
              }
       }
       else
       {
              TRACEDEBUG("access: File not exist~~~~~~~~~~~~~~~~~~~~~~!!!!!!!!!!!!!!!!!!!!!!~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
              return 1;
       }
}

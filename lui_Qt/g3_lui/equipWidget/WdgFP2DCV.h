/******************************************************************************
文件名：    WdgFP2DCV.h
功能：      第一层界面p2 DC 电压
作者：      刘金煌
创建日期：   2013年04月27日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef WDGFP2DCV_H
#define WDGFP2DCV_H

#include "common/basicwidget.h"
#include "common/uidefine.h"
#include "common/MultiDemon.h"

using namespace PieChart;

namespace Ui {
class WdgFP2DCV;
}

class WdgFP2DCV : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit WdgFP2DCV(QWidget *parent = 0);
    ~WdgFP2DCV();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();
    virtual void ShowData(void* pData);

protected:
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);
    virtual void paintEvent(QPaintEvent* event);

signals:
    void goToHomePage();
    void goToBaseWindow(enum WIDGET_TYPE);

private:
    int      m_timerId;
    CmdItem  m_cmdItem;
    QPixmap  m_pixmapHand;
    float    m_fMargins[LIMNUM];  // [0] [1]-上上限；[2]-上限；[3]-下限；[4]-下下限 [5]
    float    m_fSigVal;     // 电压
    int      iFormat;

private:
    Ui::WdgFP2DCV *ui;
};

#endif // WDGFP2DCV_H

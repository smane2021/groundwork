/******************************************************************************
文件名：    WdgFP6BattInfo.cpp
功能：      第一层界面p6 电池信息环形图
           10.27 非标 电池容量改成电池电流屏
作者：      刘金煌
创建日期：   2014年10月27日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef WDGFP6BATTINFO_H
#define WDGFP6BATTINFO_H

#include "common/basicwidget.h"
#include "common/uidefine.h"
#include "common/MultiDemon.h"

#define MARGIN_COUNT_CURRENT     (2)

using namespace PieChart;

namespace Ui {
class WdgFP6BattInfo;
}

class WdgFP6BattInfo : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit WdgFP6BattInfo(QWidget *parent = 0);
    ~WdgFP6BattInfo();
    
public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();

protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);
    virtual void paintEvent(QPaintEvent* event);

signals:
    void goToHomePage();
    void goToBaseWindow(enum WIDGET_TYPE);

private:
    int      m_timerId;
    CmdItem  m_cmdItem;
    QPixmap  m_pixmapHand;
    float    m_fMargins[LIMNUM];  // [0] [1]-上上限；[2]-上限；[3]-下限；[4]-下下限 [5]
    float    m_fSigVal;     // 电压
    int      iFormat;

private:
    Ui::WdgFP6BattInfo *ui;
};

#endif // WDGFP6BATTINFO_H

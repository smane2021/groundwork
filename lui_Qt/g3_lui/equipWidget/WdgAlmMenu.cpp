#include <QKeyEvent>
#include "WdgAlmMenu.h"

WdgAlmMenu::WdgAlmMenu(QWidget *parent)
    :BasicWidget(parent),ui(new Ui::WdgAlmMenu)

{
    ui->setupUi(this);

    InitWidget();
    InitConnect();

    m_timerId = 0;
    m_cmdItem.CmdType   = CT_READ;
}

WdgAlmMenu::~WdgAlmMenu()
{
    TRACEDEBUG("WdgAlmMenu::~WdgAlmMenu");
    ui->tblWdg_alarm->clearItems();
    delete ui;
}

void WdgAlmMenu::Enter(void* param)
{
    TRACEDEBUG("WdgAlmMenu::Enter....");
    Q_UNUSED(param);
    INIT_VAR;
    ui->tblWdg_alarm->setFocus();

    if (g_nEnterPageType != ENTER_PAGE_TYPE_KEY_ESC)
    {
        ui->tblWdg_alarm->selectRow( 0 );
    }
}

void WdgAlmMenu::Leave()
{
    LEAVE_WDG( "WdgAlmMenu" );
    this->deleteLater();
}

void WdgAlmMenu::Refresh()
{
    ;
}

void WdgAlmMenu::ShowData(void* pData)
{
    ;
}

void WdgAlmMenu::InitWidget()
{
    SET_BACKGROUND_WIDGET("WdgAlmMenu",PosBase::strImgBack_Line_Title);

    SET_GEOMETRY_LABEL_TITLE(ui->label_title);
    ui->label_title->setText(tr("Alarm"));
    ui->tblWdg_alarm->clearItems();

    SET_TABLEWDG_STYLE( tblWdg_alarm,
                        TT_YES_TITLE_NOT_SCROOL );

    QTableWidgetItem * item;
    ui->tblWdg_alarm->insertRow( 0 );
    item = new QTableWidgetItem;
    item->setText( tr("Active Alarms") +
                   " >" );
    ui->tblWdg_alarm->setItem(0, 0, item);
    item->setSelected( true );

    ui->tblWdg_alarm->insertRow( 1 );
    item = new QTableWidgetItem;
    item->setText(tr("Alarm History")+
                  " >" );
    ui->tblWdg_alarm->setItem(1, 0, item);
    for (int i=0; i<2; ++i)
    {
        ui->tblWdg_alarm->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
    }
}

void WdgAlmMenu::InitConnect()
{
    ;
}

void WdgAlmMenu::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);

        QTableWidgetItem * item = NULL;

        item = ui->tblWdg_alarm->item(0,0);
        item->setText( tr("Active Alarms") + " >" );

        item = ui->tblWdg_alarm->item(1,0);
        item->setText( tr("Alarm History")+ " >" );
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void WdgAlmMenu::timerEvent(QTimerEvent* event)
{
    Q_UNUSED(event);
}

void WdgAlmMenu::keyPressEvent(QKeyEvent* keyEvent)
{
    switch(keyEvent->key())
    {
        case Qt::Key_Enter:
        {
            int idxItem = ui->tblWdg_alarm->selectedRow();
            TRACELOG1( "WdgAlmMenu::keyPressEvent Key_Enter idxItem<%d>", idxItem );
            emit goToBaseWindow( WIDGET_TYPE(WT2_ACT_ALARM+idxItem) );
        }
        break;

        case Qt::Key_Escape:
        {
            emit goToHomePage();
        }
        break;

        default:
        break;
    }
}

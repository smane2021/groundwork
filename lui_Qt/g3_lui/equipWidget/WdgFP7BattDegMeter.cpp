/******************************************************************************
文件名：    WdgFP7BattDegMeter.cpp
功能：      第一层界面p7 温度计 与WdgFP4Deg1类似
            与WdgFP4Deg1相似
作者：      刘金煌
创建日期：   2013年5月14日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "WdgFP7BattDegMeter.h"
#include "ui_WdgFP7BattDegMeter.h"

#include <QPainter>
#include <QKeyEvent>
#include <QPixmap>
#include <QPixmapCache>
#include "common/pubInclude.h"
#include "common/global.h"
#include "config/PosThermometer.h"
#include "config/configparam.h"

WdgFP7BattDegMeter::WdgFP7BattDegMeter(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::WdgFP7BattDegMeter)
{
    ui->setupUi(this);

    InitWidget();
    InitConnect();

    m_timerId = 0;
    m_cmdItem.CmdType   = CT_READ;
    m_cmdItem.ScreenID  = SCREEN_ID_WdgFP7BattDegMeter;
}

WdgFP7BattDegMeter::~WdgFP7BattDegMeter()
{
    TRACEDEBUG("WdgFP7BattDegMeter::~WdgFP7BattDegMeter");
    delete ui;
}

void WdgFP7BattDegMeter::InitWidget()
{
    SET_BACKGROUND_WIDGET( "WdgFP7BattDegMeter",PosBase::strImgBack_None);

    m_pixmapHand.load(PATH_IMG + "meter_pointer.png");

    SET_STYLE_SCROOLBAR( 3, 2 );
    SET_STYLE_LABEL_ENTER2;
    if(LCD_ROTATION_90DEG == g_cfgParam->ms_initParam.lcdRotation)
    {
        ui->label_enter->setGeometry(
                    PosBase::titleX2,
                    PosBase::titleY2,
                    PosBase::titleWidth2,
                    PosBase::titleHeight2Rows
                    );
        ui->label_enter->setAlignment (Qt::AlignVCenter);
    }

#ifdef TEST_GUI
    PACK_ICOINFO data;
    PACK_ICOINFO* info = (PACK_ICOINFO*)&data;
    info->LimitValue[5] = MIN_TEMP_VALUE;
    info->LimitValue[4] = -15;
    info->LimitValue[3] = -0;
    info->LimitValue[2] = 25;
    info->LimitValue[1] = 50;
    info->LimitValue[0] = MAX_TEMP_VALUE;

    info->DispData[0].iFormat   = 0;
    info->DispData[0].fSigValue = 50;
    strcpy(info->DispData[0].cSigName, "Temp Comp");
    strcpy(info->DispData[0].cSigUnit, "deg.C");

    ShowData( info );
#endif
}

void WdgFP7BattDegMeter::InitConnect()
{
}


void WdgFP7BattDegMeter::Enter(void* param)
{
    TRACELOG1( "WdgFP7BattDegMeter::Enter(void* param)" );
    Q_UNUSED( param );

    INIT_VAR;

#ifdef TEST_GUI
    ENTER_FIRSTLY;
#else

    m_eTempDispFormat = GetTempFormat();
    ENTER_GET_DATA;
#endif
}

void WdgFP7BattDegMeter::Leave()
{
    LEAVE_WDG( "WdgFP7BattDegMeter" );
    this->deleteLater();
    QPixmapCache::clear ();
}

void WdgFP7BattDegMeter::Refresh()
{
    m_eTempDispFormat = GetTempFormat();

    REFRESH_DATA_PERIODICALLY(
                m_cmdItem.ScreenID,
                "WdgFP7BattDegMeter"
                );
}

// WdgFP4Deg1
void WdgFP7BattDegMeter::ShowData(void* pData)
{
    if ( !pData )
        return;

    PACK_ICOINFO* info = (PACK_ICOINFO*)pData;

    //Modified by wangminghui
    for (int i=0; i<LIMNUM; ++i)
    {
        // 上下限值与获取到的数据排序相反
        m_fMargins[LIMNUM-1-i] =
                info->LimitValue[i];
    }

    int iFormat = info->DispData[0].iFormat;
    m_fSigVal = info->DispData[0].fSigValue;
    m_strSigName = QString( info->DispData[0].cSigName );
    m_strSigUnit = QString( info->DispData[0].cSigUnit );
    QString strSigVal;
    if (m_fSigVal < INVALID_VALUE)
    {
        strSigVal = "--";
    }
    else
    {
        strSigVal = QString::number(m_fSigVal, FORMAT_DECIMAL);
    }

    QString strTitle = m_strSigName+":";
    if (LCD_ROTATION_90DEG == g_cfgParam->ms_initParam.lcdRotation)
    {
        strTitle.append( "\n" );
    }
    strTitle.append(
                strSigVal +
                m_strSigUnit +
                " >"
                );

    if((g_cfgParam->ms_pFmLargeB->width (strTitle)+ g_cfgParam->ms_pFmLargeB->width ('N') * 2) >
       PosBase::titleWidth2)
    {
        SET_GEOMETRY_LABEL_TITLE(ui->label_enter);
        ui->label_enter->setGeometry(
                    PosBase::titleX2,
                    PosBase::titleY2,
                    PosBase::titleWidth2,
                    PosBase::titleHeight2Rows);
    }

    ui->label_enter->setText(strTitle );

    TRACEDEBUG( "WdgFP7BattDegMeter::ShowData() read data "
               "Limit<%f %f %f %f %f %f>"
               "sigVal<%f> strTitle<%s>",
               m_fMargins[0], m_fMargins[1],
               m_fMargins[2], m_fMargins[3],
               m_fMargins[4], m_fMargins[5],
               m_fSigVal, strTitle.toUtf8().constData()
               );

    update();
}

void WdgFP7BattDegMeter::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
}

void WdgFP7BattDegMeter::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void WdgFP7BattDegMeter::keyPressEvent(QKeyEvent* keyEvent)
{
    QWidget::keyPressEvent(keyEvent);
}

// 与WdgFP4Deg1相似
void WdgFP7BattDegMeter::paintEvent(QPaintEvent*)
{
    //样式表支持
    QStyleOption opt;
    opt.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);

    QPainter* paint = &painter;
    degThermometerChart_paint(
            paint,
            gs_meterMarginColors,
            m_fMargins,
            m_fSigVal,
            &m_pixmapHand
            );
}

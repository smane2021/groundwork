/******************************************************************************
文件名：    WdgFP4Deg1.cpp
功能：      第一层界面p4 DC中环境温度图形页 温度计图
           与WdgFP7BattDegMeter相似
作者：      刘金煌
创建日期：   2013年5月2日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "WdgFP4Deg1.h"
#include "ui_WdgFP4Deg1.h"

#include <QPainter>
#include <QKeyEvent>
#include <QPixmap>
#include <QPixmapCache>
#include "common/pubInclude.h"
#include "common/global.h"
#include "config/PosThermometer.h"
#include "config/configparam.h"

WdgFP4Deg1::WdgFP4Deg1(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::WdgFP4Deg1)
{
    ui->setupUi(this);

    InitWidget();
    InitConnect();

    m_timerId = 0;
    m_cmdItem.CmdType   = CT_READ;
    m_cmdItem.ScreenID  = SCREEN_ID_WdgFP4Deg1;
}

WdgFP4Deg1::~WdgFP4Deg1()
{
    TRACEDEBUG("WdgFP4Deg1::~WdgFP4Deg1");
    delete ui;
}

void WdgFP4Deg1::InitWidget()
{
    SET_BACKGROUND_WIDGET( "WdgFP4Deg1",PosBase::strImgBack_None );

    ui->label_thermometer->clear();
    ui->label_thermometer->setVisible( false );

    ui->label_point->clear();
    m_pixmapHand.load(PATH_IMG + "meter_pointer.png");

    SET_STYLE_SCROOLBAR( 4, 3 );
    SET_STYLE_LABEL_ENTER2;
    if (LCD_ROTATION_90DEG == g_cfgParam->ms_initParam.lcdRotation)
    {
        ui->label_enter->setGeometry(
                    PosBase::titleX2,
                    PosBase::titleY2,
                    PosBase::titleWidth2,
                    PosBase::titleHeight2Rows
                    );
        ui->label_enter->setStyleSheet( \
        "background-color:#" LIGHT_COLOR_ENTER ";" \
        "color: rgb(241,241,241);" );
        ui->label_enter->setAlignment (Qt::AlignVCenter);
    }

#ifdef TEST_GUI
    PACK_ICOINFO data;
    PACK_ICOINFO* info = (PACK_ICOINFO*)&data;
    info->LimitValue[5] = MIN_TEMP_VALUE;
    info->LimitValue[4] = -15;
    info->LimitValue[3] = -0;
    info->LimitValue[2] = 25;
    info->LimitValue[1] = 50;
    info->LimitValue[0] = MAX_TEMP_VALUE;

    info->DispData[0].iFormat   = 0;
    info->DispData[0].fSigValue = 50;
    strcpy(info->DispData[0].cSigName, "Ambient Temp");
    strcpy(info->DispData[0].cSigUnit, "deg.C");
    ShowData( info );
#endif
}

void WdgFP4Deg1::InitConnect()
{
    ;
}

void WdgFP4Deg1::Enter(void* param)
{
    TRACELOG1( "WdgFP4Deg1::Enter(void* param)" );
    Q_UNUSED( param );

    INIT_VAR;

#ifdef TEST_GUI
    ENTER_FIRSTLY;
#else
    INIT_VAR;
    ENTER_GET_DATA;
#endif
}

void WdgFP4Deg1::Leave()
{
    LEAVE_WDG( "WdgFP4Deg1" );
    this->deleteLater();
    QPixmapCache::clear ();
}

void WdgFP4Deg1::Refresh()
{
    m_eTempDispFormat = GetTempFormat();

    TRACEDEBUG("WdgFP4Deg1::Refresh");

    REFRESH_DATA_PERIODICALLY(
                m_cmdItem.ScreenID,
                "WdgFP4Deg1"
                );
}

// WdgFP7BattDegMeter
void WdgFP4Deg1::ShowData(void* pData)
{
    TRACEDEBUG("WdgFP4Deg1::ShowData....");

    if ( !pData )
        return;

    PACK_ICOINFO* info = (PACK_ICOINFO*)pData;
    for (int i=0; i<LIMNUM; ++i)
    {
        // 上下限值与获取到的数据排序相反
        m_fMargins[LIMNUM-1-i] =
                info->LimitValue[i];
//        if (i>0 && m_fMargins[LIMNUM-1-i]>MAX_TEMP_VALUE)
//        {
//            m_fMargins[LIMNUM-1-i] = MAX_TEMP_VALUE;
//        }
    }
//    m_fMargins[0] = MIN_TEMP_VALUE;
//    m_fMargins[5] = MAX_TEMP_VALUE;

    int iFormat = info->DispData[0].iFormat;
    m_fSigVal = info->DispData[0].fSigValue;
    m_strSigName = QString( info->DispData[0].cSigName );
    m_strSigUnit = QString( info->DispData[0].cSigUnit );
    QString strSigVal;
    if (m_fSigVal < INVALID_VALUE)
    {
        strSigVal = "--";
    }
    else
    {
        strSigVal = QString::number(m_fSigVal, FORMAT_DECIMAL);
    }

    QString strTitle = m_strSigName+":";
    if (LCD_ROTATION_90DEG == g_cfgParam->ms_initParam.lcdRotation)
    {
        strTitle.append( "\n" );
    }
    strTitle.append(
                strSigVal +
                m_strSigUnit +
                " >"
                );

    if((g_cfgParam->ms_pFmLargeB->width (strTitle)+ g_cfgParam->ms_pFmLargeB->width ('N') * 2) > PosBase::titleWidth2)
    {
        ui->label_enter->setGeometry(
                    PosBase::titleX2,
                    PosBase::titleY2,
                    PosBase::titleWidth2,
                    PosBase::titleHeight2Rows);
        ui->label_enter->setStyleSheet( \
                    "background-color:#" LIGHT_COLOR_ENTER ";" \
                    "color: rgb(241,241,241);" );
        ui->label_enter->setAlignment (Qt::AlignVCenter);
    }

    ui->label_enter->setText(strTitle );

    TRACEDEBUG( "WdgFP4Deg1::ShowData() read data "
               "Limit<%f %f %f %f %f %f>"
               "sigVal<%f> strTitle<%s>",
               m_fMargins[0], m_fMargins[1],
               m_fMargins[2], m_fMargins[3],
               m_fMargins[4], m_fMargins[5],
               m_fSigVal, strTitle.toUtf8().constData()
               );
    update();
}

void WdgFP4Deg1::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
}

void WdgFP4Deg1::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void WdgFP4Deg1::keyPressEvent(QKeyEvent* keyEvent)
{
    QWidget::keyPressEvent(keyEvent);
}

// 与WdgFP7BattDegMeter相似
void WdgFP4Deg1::paintEvent(QPaintEvent*)
{
    //样式表支持
    QStyleOption opt;
    opt.init(this);
    QPainter paint(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &paint, this);

    QPainter* painter = &paint;
    degThermometerChart_paint(
            painter,
            gs_meterMarginColors,
            m_fMargins,
            m_fSigVal,
            &m_pixmapHand
            );
}

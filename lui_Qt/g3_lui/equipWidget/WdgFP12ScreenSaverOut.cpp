/******************************************************************************
文件名：    WdgFP12ScreenSaverOut.cpp
功能：      第一层界面p12 屏保唤醒第一个界面
作者：      刘金煌
创建日期：   2013年06月27日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "WdgFP12ScreenSaverOut.h"
#include "ui_WdgFP12ScreenSaverOut.h"

#include <QKeyEvent>
#include <QPainter>
#include "config/configparam.h"
#include "common/pubInclude.h"
#include "common/global.h"
#include "config/PosBase.h"

WdgFP12ScreenSaverOut::WdgFP12ScreenSaverOut(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::WdgFP12ScreenSaverOut)
{
    ui->setupUi(this);
    SET_GEOMETRY_WIDGET( this );

    m_cmdItem.CmdType   = CT_READ;
    m_cmdItem.ScreenID  = SCREEN_ID_HomePageWindow;
}

WdgFP12ScreenSaverOut::~WdgFP12ScreenSaverOut()
{
    delete ui;
}

void WdgFP12ScreenSaverOut::InitWidget()
{
    SET_BACKGROUND_WIDGET( PosBase::strImgBack_None );
#ifdef TEST_GUI
    m_fSysVal = 40;
    m_strSysVal = QString::number(m_fSysVal)+"%";
#endif
}

void WdgFP12ScreenSaverOut::InitConnect()
{
}


void WdgFP12ScreenSaverOut::Enter(void* param)
{
    TRACELOG1( "WdgFP12ScreenSaverOut::Enter(void* param)" );
    Q_UNUSED( param );

    ui->label_sys->setText( tr("Sys Used") + ":" );
    ENTER_GET_DATA;
}

void WdgFP12ScreenSaverOut::Leave()
{
    LEAVE_WDG( "WdgFP12ScreenSaverOut" );
}

void WdgFP12ScreenSaverOut::Refresh()
{
    REFRESH_DATA_PERIODICALLY(
                m_cmdItem.ScreenID,
                "WdgFP12ScreenSaverOut"
                );
}


void WdgFP12ScreenSaverOut::ShowData(void* pData)
{
    if ( !pData )
        return;

    PACK_INFO* info = (PACK_INFO*)pData;
    // RectNum ac
    info++;
    // MPPTNum solar
    info++;
    // DG Existence
    info++;
    // Mains failure 线条
    info++;
    // DG Running
    info++;
    // Solar Running
    info++;
    // BattStat
    info++;
    // DCVolt
    info++;
    QString strDC;
    int iFormat = info->iFormat;
    strDC.append( QString::number(info->vSigValue.fValue, FORMAT_DECIMAL) );
    strDC.append( info->cSigUnit );
    ui->label_V->setText( strDC );
    // Current
    info++;
    QString strA;
    iFormat = info->iFormat;
    strA.append( QString::number(info->vSigValue.fValue, FORMAT_DECIMAL) );
    strA.append( info->cSigUnit );
    ui->label_A->setText( strA );

    // BattCap
    info++;
    // SysUsed
    info++;
    ui->label_sys->setText( info->cSigName );

    m_fSysVal = info->vSigValue.fValue;
    iFormat   = info->iFormat;
    m_strSysVal = QString::number(m_fSysVal, FORMAT_DECIMAL);
    m_strSysVal.append( info->cSigUnit );

    TRACELOG1( "WdgFP12ScreenSaverOut::ShowData strV<%s> strA<%s> fSysVal<%f> strSys<%s>",
               strDC.toUtf8().constData(),
               strA.toUtf8().constData(),
               m_fSysVal,
               m_strSysVal.toUtf8().constData()
               );

    update();
}

void WdgFP12ScreenSaverOut::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
}

void WdgFP12ScreenSaverOut::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void WdgFP12ScreenSaverOut::keyPressEvent(QKeyEvent* keyEvent)
{
    switch ( keyEvent->key() )
    {
    // return 防止别的页面处理按键事件
    case Qt::Key_Return:
    case Qt::Key_Enter:
    case Qt::Key_Escape:
        emit goToHomePage();
        return;

    default:
        break;
    }

    return QWidget::keyPressEvent(keyEvent);
}

void WdgFP12ScreenSaverOut::paintEvent(QPaintEvent* event)
{
    Q_UNUSED( event );

    static const int CHART_WIDTH_ALL = PosBase::screenSaverRectWidth;
    QRect myRect  = rect();
    int nHeight   = myRect.height();
    int nChartHeightAll = nHeight-30;
    QPainter painter(this);

    painter.setRenderHint(QPainter::Antialiasing, true);//消除锯齿

    QPoint ptOrigin = QPoint(myRect.width()/2 + 5,
                             nHeight - 3);
    painter.translate( ptOrigin );

    QBrush brushAll( QColor(0x7a, 0x7a, 0x7a) );
    painter.setBrush( brushAll );
    QPen penAll( brushAll, 1);
    painter.setPen( penAll );
    painter.drawRect( 0, 0,
                      CHART_WIDTH_ALL, -nChartHeightAll
                      );

    //QBrush brushVal( Qt::green );
    QLinearGradient linearGradient(0,0, 40, 0);
    //创建了一个QLinearGradient对象实例，参数为起点和终点坐标
    linearGradient.setColorAt(0, QColor(0,200,0));
    linearGradient.setColorAt(1.0,QColor(0,150,0));
    //上面的四行分别设置渐变的颜色和路径比例
    QBrush brushVal( linearGradient );
    painter.setBrush( brushVal );
    QPen penVal( brushVal, 1);
    painter.setPen( penVal );
    int nChartHeightVal = (int)(m_fSysVal/100*nChartHeightAll);

    //int nChartHeightVal = (int)(47.5/100*nChartHeightAll);

    painter.drawRect( 0, 0,
                      CHART_WIDTH_ALL, -nChartHeightVal
                      );

    QBrush brushTxt( textFocusColor );
    painter.setBrush( brushTxt );
    QPen penTxt( textFocusColor );
    painter.setPen( penTxt );
    if (nChartHeightVal < 21)
    {
        nChartHeightVal = 21;
    }
    painter.drawText( (CHART_WIDTH_ALL-fmSmallN->width(m_strSysVal))/2,
                      -nChartHeightVal+10,
                      m_strSysVal
                      );
}

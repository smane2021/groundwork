/******************************************************************************
文件名：    WdgFP7BattDegMeter.h
功能：      第一层界面p7 温度曲线图
作者：      刘金煌
创建日期：   2013年5月14日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef WDGFP8BATTDEGCURVE_H
#define WDGFP8BATTDEGCURVE_H

#include "common/basicwidget.h"
#include "common/uidefine.h"
#include "common/MultiDemon.h"

using namespace CurveChart;
using namespace InputCtrl;

namespace Ui {
class WdgFP8BattDegCurve;
}

class WdgFP8BattDegCurve : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit WdgFP8BattDegCurve(QWidget *parent = 0);
    ~WdgFP8BattDegCurve();
    
public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();
    
protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);
    virtual void paintEvent(QPaintEvent* event);

signals:
    void goToBaseWindow(enum WIDGET_TYPE);

private:
    int      m_timerId;
    int      m_timerFreshCurve;
    CmdItem  m_cmdItem;
    CurveParam_t m_curveParam;
    time_t      m_timesAxis[MAXNUM_TREND];
    TEMP_DISP_FORMAT m_eLastTempDispFormat; //上一次温度显示格式
    TEMP_DISP_FORMAT m_eTempDispFormat;

private:
    Ui::WdgFP8BattDegCurve *ui;
};

#endif // WDGFP8BATTDEGCURVE_H

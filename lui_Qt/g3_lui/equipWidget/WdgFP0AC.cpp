/******************************************************************************
文件名：    WdgFP0AC.cpp
功能：      第一层界面p0 AC 环形图
作者：      刘金煌
创建日期：   2013年04月27日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "WdgFP0AC.h"
#include "ui_WdgFP0AC.h"

#include <QPainter>
#include <QKeyEvent>
#include <QPixmapCache>
#include "common/pubInclude.h"
#include "common/global.h"
#include "config/configparam.h"
#include "config/PosPieChart.h"

WdgFP0AC::WdgFP0AC(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::WdgFP0AC)
{
    ui->setupUi(this);

    InitWidget();
    InitConnect();

    m_timerId = 0;
    m_cmdItem.CmdType   = CT_READ;
    m_cmdItem.ScreenID  = SCREEN_ID_WdgFP0AC;
}

WdgFP0AC::~WdgFP0AC()
{
    TRACEDEBUG("WdgFP0AC::~WdgFP0AC");

    delete ui;
    ui = NULL;
}

void WdgFP0AC::InitWidget()
{
    SET_GEOMETRY_WIDGET( this );
    SET_BACKGROUND_WIDGET( "WdgFP0AC",PosBase::strImgBack_Title);
    SET_GEOMETRY_LABEL_TITLE( ui->label_title );

//    ui->label_title->setAlignment(Qt::AlignCenter);

    ui->label_L1->setGeometry( PosPieChart::rectText1AC );
    ui->label_L2->setGeometry( PosPieChart::rectText2AC );
    ui->label_L3->setGeometry( PosPieChart::rectText3AC );

    ui->label_L1->setStyleSheet ("QLabel#label_L1 {"
                                 "border: 2px solid #F1F1F1;"
                                 "background-color: #A4A3A3;"
                                 "color: #F1F1F1; }");

    ui->label_L2->setStyleSheet ("QLabel#label_L2 {"
                                 "border: 2px solid #F1F1F1;"
                                 "background-color: #717171;"
                                 "color: #F1F1F1; }");

    ui->label_L3->setStyleSheet ("QLabel#label_L3 {"
                                 "border: 2px solid #F1F1F1;"
                                 "background-color: #111111;"
                                 "color: #F1F1F1; }");

    if (g_cfgParam->ms_initParam.lcdRotation != LCD_ROTATION_BIG)
    {
        ui->label_L1->setFont( g_cfgParam->gFontSmallN );
        ui->label_L2->setFont( g_cfgParam->gFontSmallN );
        ui->label_L3->setFont( g_cfgParam->gFontSmallN );
    }
    else
    {
        ui->label_L1->setFont( g_cfgParam->gFontLargeN );
        ui->label_L2->setFont( g_cfgParam->gFontLargeN );
        ui->label_L3->setFont( g_cfgParam->gFontLargeN );
    }

    if(g_cfgParam->ms_initParam.lcdRotation == LCD_ROTATION_90DEG)
    {
        m_pixmapHand[0].load(PATH_IMG + "pie_pointer1V.png");
        m_pixmapHand[1].load(PATH_IMG + "pie_pointer2V.png");
        m_pixmapHand[2].load(PATH_IMG + "pie_pointer3V.png");

        pixmapCover.load(PATH_IMG + "pie_coverV.png");
        pixmapCenter.load(PATH_IMG + "pie_centerV.png");
    }
    else
    {
        m_pixmapHand[0].load(PATH_IMG + "pie_pointer1.png");
        m_pixmapHand[1].load(PATH_IMG + "pie_pointer2.png");
        m_pixmapHand[2].load(PATH_IMG + "pie_pointer3.png");

        pixmapCover.load(PATH_IMG + "pie_cover.png");
        pixmapCenter.load(PATH_IMG + "pie_center.png");
    }

#ifdef TEST_GUI
    PACK_ICOINFO* info = (PACK_ICOINFO*)g_dataBuff;

    info->LimitValue[5] = 50;
    info->LimitValue[4] = 165;
    info->LimitValue[3] = 167;
    info->LimitValue[2] = 275;
    info->LimitValue[1] = 277;
    info->LimitValue[0] = 400;


    info->DispData[0].iFormat = 0;

    info->iDataNum = 3;

    int idx = 0;
    info->DispData[idx].fSigValue = 200;
    qstrcpy( info->DispData[idx].cSigName, "L1" );
    qstrcpy( info->DispData[idx].cSigUnit, "V" );

    ++idx;
    info->DispData[idx].fSigValue = 220;
    qstrcpy( info->DispData[idx].cSigName, "L2" );
    qstrcpy( info->DispData[idx].cSigUnit, "V" );

    ++idx;
    info->DispData[2].fSigValue = 250;
    qstrcpy( info->DispData[idx].cSigName, "L3" );
    qstrcpy( info->DispData[idx].cSigUnit, "V" );

    ShowData( info );
#endif
}

void WdgFP0AC::InitConnect()
{
}


void WdgFP0AC::Enter(void* param)
{
    TRACELOG1( "WdgFP0AC::Enter(void* param)" );
    Q_UNUSED( param );

    INIT_VAR;

    enum PIE_CHART_TYPE type = PIE_CHART_TYPE_AC;
    PosPieChart::setPieChartType( (void*)&type );

#ifdef TEST_GUI
    ENTER_FIRSTLY;
#else
    ENTER_GET_DATA;
#endif
    ui->label_title->setText( tr("AC Input") );
}

void WdgFP0AC::Leave()
{
    LEAVE_WDG( "WdgFP0AC" );
    QPixmapCache::clear ();
    this->deleteLater();
}

void WdgFP0AC::Refresh()
{
    REFRESH_DATA_PERIODICALLY(
                m_cmdItem.ScreenID,
                "WdgFP0AC"
                );
}


void WdgFP0AC::ShowData(void* pData)
{
    if ( !pData )
        return;

    PACK_ICOINFO* info = (PACK_ICOINFO*)pData;
    for (int i=0; i<LIMNUM; ++i)
    {
        qDebug("WdgFP0AC::ShowData # %d <%f>",i,info->LimitValue[i]);
        // 环形是逆时针画的，与获取到的数据相反
        m_fMargins[LIMNUM-1-i] =
                info->LimitValue[i];
    }
    iFormat = info->DispData[0].iFormat;
    int nUCnt = info->iDataNum>3 ? 3:info->iDataNum;
    for (int i=0; i<nUCnt; ++i)
    {
        m_fSigVals[i]   = info->DispData[i].fSigValue;
        m_strSigName[i] = QString( info->DispData[i].cSigName );
        m_strSigUnit[i] = QString( info->DispData[i].cSigUnit );
    }

    int nSigIdx = 0;
    QString strSigVal;
    if (m_fSigVals[nSigIdx] < INVALID_VALUE)
    {
        strSigVal = "--";
    }
    else
    {
        strSigVal = QString::number(m_fSigVals[nSigIdx], FORMAT_DECIMAL);
    }
    ui->label_L1->setText( "1:" +
                           strSigVal +
                           m_strSigUnit[nSigIdx] );

    ++nSigIdx;
    if (m_fSigVals[nSigIdx] < INVALID_VALUE)
    {
        strSigVal = "--";
    }
    else
    {
        strSigVal = QString::number(m_fSigVals[nSigIdx], FORMAT_DECIMAL);
    }
    ui->label_L2->setText( "2:" +
                           strSigVal +
                           m_strSigUnit[nSigIdx] );

    ++nSigIdx;
    if (m_fSigVals[nSigIdx] < INVALID_VALUE)
    {
        strSigVal = "--";
    }
    else
    {
        strSigVal = QString::number(m_fSigVals[nSigIdx], FORMAT_DECIMAL);
    }
    ui->label_L3->setText( "3:" +
                           strSigVal +
                           m_strSigUnit[nSigIdx] );
    update();
}

void WdgFP0AC::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
}

void WdgFP0AC::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void WdgFP0AC::keyPressEvent(QKeyEvent* keyEvent)
{
    QWidget::keyPressEvent (keyEvent);
}

// WdgFP2DCV
void WdgFP0AC::paintEvent(QPaintEvent* event)
{
    Q_UNUSED( event );

    //样式表支持
    QStyleOption opt;
    opt.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);

    pieChart_paint(
                &painter,
                gs_pieMarginColors,
                m_fMargins,
                LIMNUM,

                iFormat,
                m_fSigVals,

                m_pixmapHand,
                &pixmapCover,
                &pixmapCenter,
                true);
}

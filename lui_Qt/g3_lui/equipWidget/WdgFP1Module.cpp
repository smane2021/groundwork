/******************************************************************************
文件名：    WdgFP1Module.cpp
功能：      第一层界面p1 Module
作者：      刘金煌
创建日期：   2013年5月13日
最后修改日期：2013年8月6日 增加converter 信号名也从app读取
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "WdgFP1Module.h"
#include "ui_WdgFP1Module.h"

#include <QKeyEvent>
#include "config/configparam.h"
#include "common/pubInclude.h"
#include "config/PosBase.h"
#include "common/global.h"

#define LED_STATE_RED_ON         0x01
#define LED_STATE_YELLOW_ON      0x02
#define LED_STATE_GREEN_ON       0x04
#define LED_STATE_GREEN_FLASH_ON 0x08

#define MAX_ITEMS_NUM_OF_MODULE     (10)

WdgFP1Module::WdgFP1Module(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::WdgFP1Module)
{
    ui->setupUi(this);

    InitWidget();
    InitConnect();

    m_timerId = 0;
    m_cmdItem.CmdType   = CT_READ;
    m_cmdItem.ScreenID  = SCREEN_ID_WdgFP1Module;
    m_nSelRow  = 0;
}

WdgFP1Module::~WdgFP1Module()
{
    TRACEDEBUG("WdgFP1Module::~WdgFP1Module");
    ui->tableWidget->clearItems();
    delete ui;
}

void WdgFP1Module::InitWidget()
{
    SET_GEOMETRY_WIDGET( this );
    SET_BACKGROUND_WIDGET( "WdgFP1Module",PosBase::strImgBack_Line_Title );
    SET_GEOMETRY_LABEL_TITLE( ui->label_title );
    SET_TABLEWDG_STYLE( tableWidget, TT_YES_TITLE_NOT_SCROOL );

    for(int i=0;i< MAX_ITEMS_NUM_OF_MODULE;i++)
    {
        ui->tableWidget->insertRow(i);
        QTableWidgetItem* item = new QTableWidgetItem;
        ui->tableWidget->setItem(i, 0, item);
        ui->tableWidget->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
    }
}

void WdgFP1Module::InitConnect()
{
//    connect( ui->tableWidget, SIGNAL(sigTableKeyPress(int)),
//             this, SLOT(sltTableKeyPress(int)) );
}

void WdgFP1Module::Enter(void* param)
{
    TRACELOG1( "WdgFP1Module::Enter(void* param)" );
    Q_UNUSED( param );

    INIT_VAR;

    m_timeElapsedKeyPress.restart();
    m_bEnterFirstly = true;
#ifdef TEST_GUI
    ENTER_FIRSTLY;
#else
    m_nRectNum = 0;
    m_nSolNum  = 0;
    ENTER_GET_DATA;
#endif
    ui->label_title->setText( tr("Module") );

    if (ui->tableWidget->rowCount() > 0)
    {
        if (g_nEnterPageType != ENTER_PAGE_TYPE_KEY_ESC)
        {
            ui->tableWidget->selectRow( 0 );
        }
    }
    SET_STYLE_SCROOLBAR( m_nRows+1, 1 );
    m_bEnterFirstly = false;
}

void WdgFP1Module::Leave()
{
    LEAVE_WDG( "WdgFP1Module" );
    this->deleteLater();
}

void WdgFP1Module::Refresh()
{
    REFRESH_DATA_PERIODICALLY(
                m_cmdItem.ScreenID,
                "WdgFP1Module"
                );
}


void WdgFP1Module::ShowData(void* pData)
{
//    TRACEDEBUG( "WdgFP1Module::ShowData time<%d> m_bEnterFirstly<%d>",
//                m_timeElapsedKeyPress.elapsed(), m_bEnterFirstly );
    if ( !pData ||
         (m_timeElapsedKeyPress.elapsed()<
          TIME_ELAPSED_KEYPRESS &&
          !m_bEnterFirstly) )
    {
        return;
    }

#define APPEND_READONLY_DATA \
    info++; \
    appendReadOnlyItem(info, infoEnd, nDataNum);

    m_nSelRow = ui->tableWidget->selectedRow();
    ui->tableWidget->clearItemsTexts();

    PACK_INFO* info    = (PACK_INFO*)pData;
    int nDataNum       = info->iDataNum;
    PACK_INFO* infoEnd = info+nDataNum;
    m_nRows = -1;
    for (int i=0; i<MAX_MODULE_ITEMS; ++i)
    {
        m_mapW2nd[i] = 0;
        m_mapNum[i]  = 0;
    }
    bool bShowRect = true;
    bool bShowSol  = true;
    bool bShowConv = true;
    int nSigVal = 0;

    // Converter模式	1-“RectNum”行和“Sol Conv Num”行都不显示
    --info;
    APPEND_READONLY_DATA;
    long nVal1 = info->vSigValue.lValue;
    if (nVal1 == 1)
    {
        bShowRect = false;
        bShowSol  = false;
    }

    // MPPT运行模式	0- “Sol Conv Num”行不显示
    // 1-“RectNum”行和“Sol Conv Num”行都显示
    // 2-“RectNum”行不显示
    APPEND_READONLY_DATA;
    long nVal2 = info->vSigValue.lValue;
    if (nVal2 == 0)
    {
        bShowSol  = false;
    }
    else if (nVal2 == 2)
    {
        bShowRect = false;
    }

    // RectNum	Rect个数
    APPEND_READONLY_DATA;
    long nVal3 = info->vSigValue.lValue;
    if ( bShowRect )
    {
        addTableRow_moduleNum(
                    info,
                    WT2_RECTINFO
                    );
    }

    // Sol Conv Num
    APPEND_READONLY_DATA;
    long nVal4 = info->vSigValue.lValue;
    if ( bShowSol )
    {
        addTableRow_moduleNum(
                    info,
                    WT2_SOLINFO
                    );
    }

    // ConvNum	Conv个数 =0时，ConvNum” 行不显示；
    APPEND_READONLY_DATA;
    long nVal5 = info->vSigValue.lValue;
    if (nVal5 > 0)
    {
        bShowConv = true;
        addTableRow_moduleNum(
                    info,
                    WT2_CONVINFO
                    );
    }
    else
    {
        bShowConv = false;
    }

    bool bShowS1 = true;
    bool bShowS2 = true;
    bool bShowS3 = true;

    // Controller Mode 0-S1Num、S2Num、S3Num都不显示
    APPEND_READONLY_DATA;
    long nVal6 = info->vSigValue.lValue;
    int nControllerMode = info->vSigValue.lValue;
    if (nControllerMode == 0)
    {
        bShowS1  = false;
        bShowS2  = false;
        bShowS3  = false;
    }

    // Slave1 State	Controller Mode 为1时，
    //0-	S1Num显示
    //1-	S1Num不显示
    APPEND_READONLY_DATA;
    long nVal7 = info->vSigValue.lValue;
    if (nControllerMode == 1)
    {
        nSigVal = info->vSigValue.lValue;
        if (nSigVal == 0)
        {
            bShowS1  = true;
        }
        else if (nSigVal == 1)
        {
            bShowS1  = false;
        }
    }

    // Slave2 State	Controller Mode 为1时，
    //0-	S2Num显示
    //1-	S2Num不显示
    APPEND_READONLY_DATA;
    long nVal8 = info->vSigValue.lValue;
    if (nControllerMode == 1)
    {
        nSigVal = info->vSigValue.lValue;
        if (nSigVal == 0)
        {
            bShowS2  = true;
        }
        else if (nSigVal == 1)
        {
            bShowS2  = false;
        }
    }

    // Slave3 State	Controller Mode 为1时，
    //0-	S3Num显示
    //1-	S3Num不显示
    APPEND_READONLY_DATA;
    long nVal9 = info->vSigValue.lValue;
    if (nControllerMode == 1)
    {
        nSigVal = info->vSigValue.lValue;
        if (nSigVal == 0)
        {
            bShowS3  = true;
        }
        else if (nSigVal == 1)
        {
            bShowS3  = false;
        }
    }

    // S1Num
    APPEND_READONLY_DATA;
    long nVal10 = info->vSigValue.lValue;
    if ( bShowS1 )
    {
        addTableRow_moduleNum(
                    info,
                    WT2_SLAVE1INFO
                    );
    }

    // S2Num
    APPEND_READONLY_DATA;
    long nVal11 = info->vSigValue.lValue;
    if ( bShowS2 )
    {
        addTableRow_moduleNum(
                    info,
                    WT2_SLAVE2INFO
                    );
    }

    // S3Num
    APPEND_READONLY_DATA;
    long nVal12 = info->vSigValue.lValue;
    if ( bShowS3 )
    {
        addTableRow_moduleNum(
                    info,
                    WT2_SLAVE3INFO
                    );
    }
    APPEND_READONLY_DATA;

    //show source number
    addTableRow_moduleNum(
                info,
                WT2_SOURCE_INFO
                );

    if (ui->tableWidget->rowCount() >= m_nSelRow)
    {
        ui->tableWidget->selectRow( m_nSelRow );
        SET_STYLE_SCROOLBAR( m_nRows+1, m_nSelRow+1 );
    }


    TRACEDEBUG( "WdgFP1Module::ShowData() readed data "
                "rect<%d> sol<%d> conv<%d> "
                "bShowS1<%d> bShowS2<%d> bShowS3<%d>"
                "nVal1<%d>,nVal2<%d>,nVal3<%d>,nVal4<%d>,nVal5<%d>,nVal6<%d>,nVal7<%d>,nVal8<%d>,nVal9<%d>,nVal10<%d>,nVal11<%d>,nVal12<%d>",
                bShowRect,
                bShowSol,
                bShowConv,
                bShowS1,
                bShowS2,
                bShowS3,
                nVal1,nVal2,nVal3,nVal4,nVal5,nVal6,nVal7,nVal8,nVal9,nVal10,nVal11,nVal12
                );
}

void WdgFP1Module::addTableRow_moduleNum(const PACK_INFO* info,
                                         enum WIDGET_TYPE wt,
                                         bool bReadOnly)
{
    if (NULL == info)
    {
        TRACEDEBUG( "WdgFP1Module::addTableRow_moduleNum NULL == info" );
        return;
    }

    ++m_nRows;
    m_mapW2nd[m_nRows] = wt;
    QString strSigName;
    if ( bReadOnly )
    {
        m_mapNum[m_nRows]  = 0;
        QString strSigVal  =
                varValue2String(info->iSigValueType,
                                info->iFormat,
                                info->vSigValue,
                                info->cEnumText);
        strSigName = QString(info->cSigName)+":"+
                strSigVal + QString(info->cSigUnit);
    }
    else
    {
        int nSigVal = info->vSigValue.lValue;
        m_mapNum[m_nRows]  = nSigVal;
        strSigName = QString(info->cSigName)+":"+
                QString::number(nSigVal) + " >";
    }
//    TRACEDEBUG("WdgFP1Module::addTableRow_moduleNum strSigName:%s",strSigName.toUtf8().constData());
    QTableWidgetItem* item = ui->tableWidget->item(m_nRows,0);
    item->setText(strSigName);
/*
    ui->tableWidget->insertRow( m_nRows );
    QTableWidgetItem* item = new QTableWidgetItem;
    item->setText( strSigName );
    ui->tableWidget->setItem(m_nRows, 0, item);
    ui->tableWidget->setRowHeight(m_nRows, TABLEWDG_ROW_HEIGHT);
*/
}

QString WdgFP1Module::varValue2String(int nSigValueType,
                                      int iFormat,
                                      VAR_VALUE vSigValue,
                                      const char* strEnumText)
{
    QString strValue;
    switch ( nSigValueType )
    {
    case VAR_LONG:
        strValue = QString::number( vSigValue.lValue );
        break;

    case VAR_FLOAT:
        strValue = QString::number(vSigValue.fValue, FORMAT_DECIMAL);
        break;

    case VAR_UNSIGNED_LONG:
        strValue = QString::number( vSigValue.ulValue );
        break;

    case VAR_ENUM:
        strValue = QString( strEnumText );
        break;

    default:
        break;
    }

    return strValue;
}

bool WdgFP1Module::appendReadOnlyItem(PACK_INFO*& info,
                                      PACK_INFO* infoEnd,
                                      const int nDataNum)
{
    static int nData = 0;
//    TRACEDEBUG("WdgFP1Module::appendReadOnlyItem info<%04x> infoEnd<%04x> nDataNum<%d> nData<%d>", info, infoEnd, nDataNum, nData );
    if (info >= infoEnd)
    {
//        TRACEDEBUG("WdgFP1Module::appendReadOnlyItem info >= infoEnd return" );
        nData = 0;
        return false;
    }

    for (; info<infoEnd; ++info)
    {
        if (-2 == info->iEquipID)
        {
            nData++;
//            TRACEDEBUG("WdgFP1Module::appendReadOnlyItem<%s> disp<%d>", info->cSigName, info->stSpecialID2Info.iDispCtrl );
            if (1 == info->stSpecialID2Info.iDispCtrl)
            {
                addTableRow_moduleNum(info, WT1_MODULE, true);
            }
        }
        else
        {
            nData++;
            break;
        }
    }
//    TRACEDEBUG("WdgFP1Module::appendReadOnlyItem info<%04x>", info );

    return true;
}

void WdgFP1Module::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
}

void WdgFP1Module::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void WdgFP1Module::keyPressEvent(QKeyEvent* keyEvent)
{
  TRACEDEBUG("WdgFP1Module::keyPressEvent");
  int iSelectRow = ui->tableWidget->selectedRow ();
  switch ( keyEvent->key() )
  {
    case Qt::Key_Return:
    case Qt::Key_Enter:
    {
      int iSelectRow = ui->tableWidget->selectedRow();
      if (m_mapNum[iSelectRow] > 0)
      {
          emit goToBaseWindow( WIDGET_TYPE(m_mapW2nd[iSelectRow]) );
      }
    }
    break;

    case Qt::Key_Down:
    {
        if(iSelectRow < m_nRows)
        {
            iSelectRow ++;
        }
        else
        {
            iSelectRow = 0;
        }
        ui->tableWidget->setCurrentCell (iSelectRow,0);
    }
    break;

    case Qt::Key_Up:
    {
        if(iSelectRow > 0)
        {
            iSelectRow --;
        }
        else
        {
            iSelectRow = m_nRows;
        }
        ui->tableWidget->setCurrentCell (iSelectRow,0);
    }
    break;

    default:
    break;
  }
  QWidget::keyPressEvent(keyEvent);
}

void WdgFP1Module::sltTableKeyPress(int key)
{
    Q_UNUSED( key );
    TRACEDEBUG( "WdgFP1Module::sltTableKeyPress" );
    m_timeElapsedKeyPress.restart();
}

void WdgFP1Module::on_tableWidget_itemSelectionChanged()
{
    SET_STYLE_SCROOLBAR( m_nRows+1,
                         ui->tableWidget->selectedRow()+1 );
}

/******************************************************************************
文件名：    ThirdStackedWdg.h
功能：      第三级信号窗口，目前只有告警信号 ui只有tableWdg
作者：      刘金煌
创建日期：   2013年5月15日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef THIRDSTACKEDWDG_H
#define THIRDSTACKEDWDG_H

#include "common/basicwidget.h"
#include "common/uidefine.h"
#include "Wdg3Table.h"

namespace Ui {
class ThirdStackedWdg;
}

class ThirdStackedWdg : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit ThirdStackedWdg(QWidget *parent = 0);
    ~ThirdStackedWdg();

protected:
    virtual void InitWidget();
    virtual void InitConnect();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();
    virtual void ShowData(void* pData);

protected:
    virtual void changeEvent(QEvent *event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);

private:
    void CreateDispWdg(int iDispWdgType);

private:
    BasicWidget* m_pCurWdt;
    int  m_wt;

signals:
    void goToHomePage();
    void goToBaseWindow(enum WIDGET_TYPE);
    
private:
    Ui::ThirdStackedWdg *ui;
};

#endif // THIRDSTACKEDWDG_H

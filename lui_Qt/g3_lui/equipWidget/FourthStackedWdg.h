/******************************************************************************
文件名：    FourthStackedWdg.h
功能：      第四层窗口，目前只有告警帮助 ui只有tableWdg
作者：      刘金煌
创建日期：   2013年5月31日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef FOURTHSTACKEDWDG_H
#define FOURTHSTACKEDWDG_H

#include "common/basicwidget.h"
#include "common/uidefine.h"
#include "Wdg3Table.h"

namespace Ui {
class FourthStackedWdg;
}

class FourthStackedWdg : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit FourthStackedWdg(QWidget *parent = 0);
    ~FourthStackedWdg();

protected:
    virtual void InitWidget();
    virtual void InitConnect();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();
    virtual void ShowData(void* pData);

private:
    void CreateDispWdg(int iDispWdgType);
    

protected:
    virtual void changeEvent(QEvent *event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);

private:
    BasicWidget* m_pCurWdt;
    int  m_wt;

signals:
    void goToHomePage();
    void goToBaseWindow(enum WIDGET_TYPE);
    
private:
    Ui::FourthStackedWdg *ui;
};

#endif // FOURTHSTACKEDWDG_H

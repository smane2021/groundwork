/******************************************************************************
文件名：    WdgFP11ScreenSaver.h
功能：      第一层界面p11 屏保时用 不刷数据 空的
作者：      刘金煌
创建日期：   2013年06月27日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef WDGFP11SCREENSAVER_H
#define WDGFP11SCREENSAVER_H

#include "common/basicwidget.h"
#include "common/uidefine.h"

namespace Ui {
class WdgFP11ScreenSaver;
}

class WdgFP11ScreenSaver : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit WdgFP11ScreenSaver(QWidget *parent = 0);
    ~WdgFP11ScreenSaver();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();

protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
//    virtual void keyPressEvent(QKeyEvent* keyEvent);

signals:
    void goToBaseWindow(enum WIDGET_TYPE);

private:
    Ui::WdgFP11ScreenSaver *ui;
};

#endif // WDGFP11SCREENSAVER_H

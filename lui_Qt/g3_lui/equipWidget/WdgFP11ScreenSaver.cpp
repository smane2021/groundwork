#include "WdgFP11ScreenSaver.h"
#include "ui_WdgFP11ScreenSaver.h"

#include <QKeyEvent>
#include "config/configparam.h"
#include "common/pubInclude.h"
#include "common/global.h"

WdgFP11ScreenSaver::WdgFP11ScreenSaver(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::WdgFP11ScreenSaver)
{
    ui->setupUi(this);
    SET_GEOMETRY_WIDGET( this );

    InitWidget();
    InitConnect();
}

WdgFP11ScreenSaver::~WdgFP11ScreenSaver()
{
    delete ui;
}

void WdgFP11ScreenSaver::InitWidget()
{
    QPalette palette;
    palette.setColor(QPalette::Background, QColor(0, 0, 0));
    this->setPalette(palette);
    this->setAutoFillBackground(true);
}

void WdgFP11ScreenSaver::InitConnect()
{
}

void WdgFP11ScreenSaver::Enter(void* param)
{
    TRACELOG1( "WdgFP11ScreenSaver::Enter(void* param)" );
    Q_UNUSED( param );

    g_bLogin = false;
}

void WdgFP11ScreenSaver::Leave()
{
    TRACELOG1( "WdgFP11ScreenSaver::Leave()" );
    this->deleteLater();
}

void WdgFP11ScreenSaver::Refresh()
{
}


void WdgFP11ScreenSaver::ShowData(void* pData)
{
    Q_UNUSED( pData );
}

void WdgFP11ScreenSaver::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

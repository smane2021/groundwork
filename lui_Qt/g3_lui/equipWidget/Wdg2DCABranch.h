/******************************************************************************
文件名：    Wdg2DCABranch.h
功能：      第二层界面p2 DC 电流负载条形图
作者：      刘金煌
创建日期：   2013年5月2日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef WDG2DCABRANCH_H
#define WDG2DCABRANCH_H

#include <QMap>
#include "common/basicwidget.h"
#include "common/uidefine.h"

namespace Ui {
class Wdg2DCABranch;
}

class Wdg2DCABranch : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit Wdg2DCABranch(QWidget *parent = 0);
    ~Wdg2DCABranch();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();

protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);
    virtual void paintEvent(QPaintEvent* event);

signals:
    void goToBaseWindow(enum WIDGET_TYPE);

private:
    int      m_timerId;
    CmdItem  m_cmdItem;
    float    m_sigVals[MAXLEN_SIG];
    bool     m_bResponds[MAXNUM_BRANCH];
    int m_branchNos[MAXLEN_SIG]; // 当前页支路电流序号
    int m_nPageIdx;
    int m_nPages;
    int m_nMaxXval;
    // 当前页开始下标索引
    //int  m_nStartIdxCurPage[MAXNUM_BRANCH];
    int  iFormat;

private:
    Ui::Wdg2DCABranch *ui;
};

#endif // WDG2DCABRANCH_H

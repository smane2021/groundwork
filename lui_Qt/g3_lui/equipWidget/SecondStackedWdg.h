#ifndef SECONDSTACKEDWDG_H
#define SECONDSTACKEDWDG_H

#include <QKeyEvent>
#include <QSplashScreen>

#include "common/basicwidget.h"
#include "common/uidefine.h"

#include "equipWidget/Wdg2Table.h"
#include "equipWidget/WdgInventory.h"
#include "equipWidget/Wdg2DCABranch.h"
#include "equipWidget/Wdg2DCDeg1Branch.h"
#include "equipWidget/Wdg2P5BattRemainTime.h"
#include "equipWidget/Wdg2P6BattDeg.h"
#include "configWidget/WdgFCfgGroup.h"


namespace Ui {
class SecondStackedWdg;
}

class SecondStackedWdg : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit SecondStackedWdg(QWidget *parent = 0);
    ~SecondStackedWdg();

protected:
    virtual void InitWidget();
    virtual void InitConnect();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();
    virtual void ShowData(void* pData);

protected:
    virtual void changeEvent(QEvent *event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);

signals:
    void goToHomePage();
    void goToBaseWindow(enum WIDGET_TYPE);
    void goToGuideWindow(enum WIDGET_GUIDE);
    void sigScreenSaver();
    void sigStopDetectAlarm();

private:
    void CreateDispWidget(int iDispWdgType);

private:
    BasicWidget*         m_pCurWdt;
    int                  m_wt; //保存前一次的页面
    int                  m_curPage; //当前要显示的页面从0开始

private:
    WdgFCfgGroup* wdgFCfgGroup;
    Ui::SecondStackedWdg *ui;
};

#endif // SECONDSTACKEDWDG_H

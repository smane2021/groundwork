/******************************************************************************
文件名：    Wdg2Table.h
功能：      第二层界面 表格
    WT2_RECTINFO, 模块信息页
    WT2_SOLINFO,
    WT2_CONVINFO,
    WT2_SLAVE1INFO,
    WT2_SLAVE2INFO,
    WT2_SLAVE3INFO,
            WT2_ACT_ALARM, 告警个数页
            WT2_HIS_ALARM,
            WT2_EVENT_LOG,
            WT2_INVENTORY  产品信息页
作者：      刘金煌
创建日期：   2013年5月9日
最后修改日期：
    2013年08月08日 增加 converter
    2014年01月21日 模块信息超过100条刷新数据的同时选择行会变慢
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "WdgInventory.h"

#include <QKeyEvent>
#include "common/pubInclude.h"
#include "common/global.h"
#include "config/configparam.h"
#include "config/PosTableWidget.h"

static QColor clrCommNormal(0, 0, 0, 0);
static QColor clrCommAbnormal(255, 0, 0, 255);

WdgInventory::WdgInventory(enum WIDGET_TYPE wt, QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::WdgInventory)
{
    ui->setupUi(this);
    m_wt      = wt;

    InitWidget();
    InitConnect();

    m_timerId  = 0;
    m_cmdItem.CmdType   = CT_READ;
    m_nPageIdx = 1;
    m_nPages   = 1;
}

WdgInventory::~WdgInventory()
{
    TRACEDEBUG("WdgInventory::~WdgInventory");

    this->disconnect();

    ui->tableWidget->clearItems();

    delete []m_pRectInv;
    m_pRectInv = NULL;

    delete []m_pSlaveRectInv;
    m_pSlaveRectInv = NULL;

    delete []m_pSMI2CInv;
    m_pSMI2CInv = NULL;

    delete []m_pRS485Inv;
    m_pRS485Inv = NULL;

    delete []m_pConvInv;
    m_pConvInv = NULL;

    delete []m_pLiBattInv;
    m_pLiBattInv = NULL;

    delete []m_pSolConvInv;
    m_pSolConvInv = NULL;

    delete []m_pSMDUEInv;
    m_pSMDUEInv = NULL;

    delete ui;
}

void WdgInventory::InitWidget()
{
    TRACELOG1( "WdgInventory::InitWidget()" );

    SET_GEOMETRY_WIDGET( this );
    SET_BACKGROUND_WIDGET( "WdgInventory",PosBase::strImgBack_Line);
    SET_TABLEWDG_STYLE( tableWidget, TT_NOT_TITLE_YES_SCROOL );

    //设置背景画面
    ui->label_enter->setVisible( false );
    ui->tableWidget->clearItems();

    //动态分配存放产品信息的结构
    m_pRectInv = new MOD_INV[168];
    m_pSlaveRectInv = new MOD_INV[180];
    m_pSMI2CInv = new MOD_INV[32];
    m_pRS485Inv = new MOD_INV[MAX_RS485_INV_NUM];
    m_pConvInv = new MOD_INV[60];
    m_pLiBattInv = new MOD_INV[69];
    m_pSolConvInv = new MOD_INV[38];
    m_pSMDUEInv = new MOD_INV[8];

    m_cmdItem.ScreenID = SCREEN_ID_Wdg2Table_Rect_Inv;

    ui->tableWidget->setRowCount( TABLEWDG_ROWS_PERPAGE_NOTITLE );

    QTableWidgetItem *item = NULL;
    for (int i=0; i<TABLEWDG_ROWS_PERPAGE_NOTITLE; ++i)
    {
        item = new QTableWidgetItem;
        item->setText( "" );
        ui->tableWidget->setItem(i, 0, item);
        ui->tableWidget->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
    }

#ifdef TEST_GUI
    item = ui->tableWidget->item(0, 0);
    item->setText( "1" );

    item = ui->tableWidget->item(1, 0);
    item->setText( "Rect #1" );

    item = ui->tableWidget->item(2, 0);
    item->setText( "R482900" );

    item = ui->tableWidget->item(3, 0);
    item->setText( "01130605211" );

    item = ui->tableWidget->item(4, 0);
    item->setText( "A00" );

    item = ui->tableWidget->item(5, 0);
    item->setText( "V1.00" );
#endif
}

void WdgInventory::testGUI()
{
        m_pData = g_dataBuff;
        PACK_MODINFO* info = (PACK_MODINFO*)m_pData;
        info->stHead.iScreenID = m_cmdItem.ScreenID;
        info->iModuleNum = 2;

        int sigIdx = 0;
        MODINFO *modInfo = &(info->ModInfo[sigIdx]);
        modInfo->iEquipID = 11;
        strcpy(modInfo->cEqName, "#1");
        int iSigID   = 101;
        int iSigType = 201;
        modInfo->EachMod[0].iSigID = iSigID;
        modInfo->EachMod[0].iSigType = iSigID;
        modInfo->EachMod[0].vSigValue.lValue = 0;

        modInfo->EachMod[1].iSigID = iSigID;
        modInfo->EachMod[1].iSigType = iSigID;
        modInfo->EachMod[1].iFormat = 1;
        modInfo->EachMod[1].vSigValue.fValue = 1.1;

        modInfo->EachMod[2].iSigID = iSigID;
        modInfo->EachMod[2].iSigType = iSigID;
        strcpy(modInfo->EachMod[2].cEnumText, "OFF");

        sigIdx++;

        modInfo = &(info->ModInfo[sigIdx]);
        modInfo->iEquipID = 12;
        strcpy(modInfo->cEqName, "#2");
        iSigID++;
        iSigType++;
        modInfo->EachMod[0].iSigID = iSigID;
        modInfo->EachMod[0].iSigType = iSigID;
        modInfo->EachMod[0].vSigValue.lValue = 0;

        modInfo->EachMod[1].iSigID = iSigID;
        modInfo->EachMod[1].iSigType = iSigID;
        modInfo->EachMod[1].iFormat = 1;
        modInfo->EachMod[1].vSigValue.fValue = 2.2;

        modInfo->EachMod[2].iSigID = iSigID;
        modInfo->EachMod[2].iSigType = iSigID;
        strcpy(modInfo->EachMod[2].cEnumText, "OFF");

        ShowData( m_pData );
}

void WdgInventory::InitConnect()
{
    connect( &m_QTimerLight, SIGNAL(timeout()),
             this, SLOT(sltTimerHandler()) );
}

void WdgInventory::Enter(void* param)
{
    TRACELOG1( "WdgInventory::Enter(void* param) m_wt<%d>", m_wt );
    Q_UNUSED( param );

    INIT_VAR;

    m_nSigNum   = 0;
    m_nPageIdx = 1;
    m_nPages   = 1;
    m_pData = NULL;

    m_nRectInv      = 0;
    m_nSolConvInv   = 0;
    m_nConvInv      = 0;
    m_nRS485Inv     = 0;
    m_nSMI2CInv     = 0;
    m_nLiBattInv    = 0;
    m_nSlaveRectInv = 0;
    m_nSMDUEInv     = 0;

    m_CtrlLightInfo.iEquipID = -1;
    m_CtrlLightInfo.bSendCmd = false;


    m_CtrlLightInfo.timeElapsed.restart();
    this->setFocus();

    ui->tableWidget->clearFocus();
    ui->tableWidget->clearSelection();

    ms_showingWdg = this;
    g_bSendCmd    = true;
    void *pData = NULL;
    for (int nScreenID = SCREEN_ID_Wdg2Table_Rect_Inv;
             nScreenID <= MAX_SCREEN_ID_INV; ++ nScreenID)
    {
        m_cmdItem.ScreenID = nScreenID;
        TRACELOG1( "WdgInventory::Enter data_getDataSync nScreenID<%0x>", nScreenID );
        if ( !data_getDataSync(&m_cmdItem, &pData) )
        {
            if(m_cmdItem.ScreenID != ((Head_t*)pData)->iScreenID)
            {
                continue;
            }

            setInvDataMem( pData );
        }
    }
    SET_STYLE_SCROOLBAR(m_nSigNum, m_nPageIdx);

    ShowData( NULL );
    m_timerId = startTimer( TIMER_UPDATE_DATA_INTERVAL*15 );
    m_QTimerLight.start( TIMER_UPDATE_DATA_INTERVAL/2 );

    m_bEnterFirstly = false;
}

void WdgInventory::Leave()
{
    LEAVE_WDG( "WdgInventory" );

    if ( m_QTimerLight.isActive() )
    {
        m_QTimerLight.stop();
    }

    sendCmdCtrlLight(
        m_CtrlLightInfo.screenID,
        -1,
        MODULE_LIGHT_TYPE_ON,
        m_CtrlLightInfo.moduleType
        );

    this->deleteLater();
}

void WdgInventory::Refresh()
{
  m_cmdItem.CmdType   = CT_READ;
  m_cmdItem.ScreenID  = ( ++(m_cmdItem.ScreenID) > MAX_SCREEN_ID_INV ?
                          SCREEN_ID_Wdg2Table_Rect_Inv : m_cmdItem.ScreenID );

  void *pData = NULL;
  int n = MAX_COUNT_GET_DATA;
  while(n-- > 0)
  {
      if ( !data_getDataSync(&m_cmdItem, &pData) )
      {
          int iScreenID = ((Head_t*)pData)->iScreenID;
          if (iScreenID == m_cmdItem.ScreenID)
          {
              setInvDataMem( pData );
              ShowData( NULL );
              break;
          }
          else
          {
              continue;
          }
      }
      else
      {
          ShowData( NULL );
      }
  }
}

void WdgInventory::ShowData(void* pData)
{
    TRACEDEBUG("WdgInventory::ShowData m_nSigNum:%d",m_nSigNum);
    setInvItem();
}

void WdgInventory::sltScreenSaver()
{
    if (BasicWidget::ms_showingWdg == this)
    {
        ;
    }
}

//    inventory
void WdgInventory::setInvItem()
{
    QTableWidgetItem* item = NULL;

    if (m_nSigNum < 1)
    {
        item = ui->tableWidget->item(2, 0);
        item->setTextAlignment( Qt::AlignCenter );
        item->setText( tr("No Data") );
        item->setFont(g_cfgParam->gFontLargeN);
        m_CtrlLightInfo.bSendCmd = false;
        return;
    }
    if (m_nPageIdx < 1)
    {
        m_nPageIdx = m_nSigNum;
    }
    else if (m_nPageIdx > m_nSigNum)
    {
        m_nPageIdx = 1;
    }
    SET_STYLE_SCROOLBAR(m_nSigNum, m_nPageIdx);

    MOD_INV* sigItem = NULL;
    int ScreenID = 0;
    if (m_nPageIdx <= m_nRectInv)
    {
        ScreenID  = SCREEN_ID_Wdg2Table_Rect_Inv;
        sigItem   = &(m_pRectInv[m_nPageIdx-1]);
        TRACEDEBUG( "WdgInventory::setInvItem 1" );
    }
    else if (m_nPageIdx <= m_nRectInv+m_nSlaveRectInv)
    {
        ScreenID  = SCREEN_ID_Wdg2Table_SlaveRect_Inv;
        sigItem   = &(m_pSlaveRectInv[m_nPageIdx-1-m_nRectInv]);
        TRACEDEBUG( "WdgInventory::setInvItem 2" );
    }
    else if (m_nPageIdx <= m_nRectInv+m_nSlaveRectInv+m_nSMI2CInv)
    {
        ScreenID  = SCREEN_ID_Wdg2Table_SMI2C_Inv;
        sigItem   = &(m_pSMI2CInv[m_nPageIdx-1-(m_nRectInv+m_nSlaveRectInv)]);
        TRACEDEBUG( "WdgInventory::setInvItem 3" );
    }
    else if (m_nPageIdx <= m_nRectInv+m_nSlaveRectInv+m_nSMI2CInv+m_nRS485Inv)
    {
        ScreenID  = SCREEN_ID_Wdg2Table_RS485_Inv;
        sigItem   = &(m_pRS485Inv[m_nPageIdx-1-
            (m_nRectInv+m_nSlaveRectInv+m_nSMI2CInv)]);
        TRACEDEBUG( "WdgInventory::setInvItem 4" );
    }
    else if (m_nPageIdx <= m_nRectInv+m_nSlaveRectInv+m_nSMI2CInv+m_nRS485Inv+m_nConvInv)
    {
        ScreenID  = SCREEN_ID_Wdg2Table_Conv_Inv;
        sigItem   = &(m_pConvInv[m_nPageIdx-1-
            (m_nRectInv+m_nSlaveRectInv+m_nSMI2CInv+m_nRS485Inv)]);
        TRACEDEBUG( "WdgInventory::setInvItem 5" );
    }
    else if (m_nPageIdx <= m_nRectInv+m_nSlaveRectInv+m_nSMI2CInv+m_nRS485Inv+m_nConvInv+m_nLiBattInv)
    {
        ScreenID  = SCREEN_ID_Wdg2Table_LiBatt_Inv;
        sigItem   = &(m_pLiBattInv[m_nPageIdx-1-
            (m_nRectInv+m_nSlaveRectInv+m_nSMI2CInv+m_nRS485Inv+m_nConvInv)]);
        TRACEDEBUG( "WdgInventory::setInvItem 6" );
    }
    else if (m_nPageIdx <= m_nRectInv+m_nSlaveRectInv+m_nSMI2CInv+m_nRS485Inv+m_nConvInv+m_nLiBattInv+m_nSolConvInv)
    {
        ScreenID  = SCREEN_ID_Wdg2Table_SolConv_Inv;
        sigItem   = &(m_pSolConvInv[m_nPageIdx-1-
            (m_nRectInv+m_nSlaveRectInv+m_nSMI2CInv+m_nRS485Inv+m_nConvInv+m_nLiBattInv)]);
        TRACEDEBUG( "WdgInventory::setInvItem 7" );
    }
    else if(m_nPageIdx <= m_nRectInv+m_nSlaveRectInv+m_nSMI2CInv+m_nRS485Inv+m_nConvInv+m_nLiBattInv+m_nSolConvInv+m_nSMDUEInv)
    {
        ScreenID  = SCREEN_ID_Wdg2Table_SMDUE_Inv;
        sigItem   = &(m_pSMDUEInv[m_nPageIdx-1-
            (m_nRectInv+m_nSlaveRectInv+m_nSMI2CInv+m_nRS485Inv+m_nConvInv+m_nLiBattInv+m_nSolConvInv)]);
        TRACEDEBUG( "WdgInventory::setInvItem 8" );
    }
    TRACELOG1( "WdgInventory::setInvItem sigItem idx<%d> equipName<%s>"
               "serialNumber<%s> cPartNumber<%s> cPVer<%s> cSWver<%s> ScreenID<%06x>",
               m_nPageIdx, sigItem->cEquipName,
               sigItem->cSerialNumber, sigItem->cPartNumber,
               sigItem->cPVer, sigItem->cSWver,
               ScreenID
               );

    item = ui->tableWidget->item(0, 0);
    item->setTextAlignment( Qt::AlignLeft );
    item->setText( QString::number(m_nPageIdx) );
    item->setFont(g_cfgParam->gFontLargeN);

    item = ui->tableWidget->item(1, 0);
    item->setTextAlignment( Qt::AlignLeft );
    item->setText( tr("Name") + ":" + sigItem->cEquipName );
    item->setFont(g_cfgParam->gFontLargeN);

    item = ui->tableWidget->item(2, 0);
    item->setTextAlignment( Qt::AlignLeft );
    item->setText( tr("SN") + ":" + sigItem->cSerialNumber );
    item->setFont(g_cfgParam->gFontLargeN);

    item = ui->tableWidget->item(3, 0);
    item->setTextAlignment( Qt::AlignLeft );
    item->setText( tr("Number") + ":" + sigItem->cPartNumber );
    item->setFont(g_cfgParam->gFontLargeN);

    item = ui->tableWidget->item(4, 0);
    item->setTextAlignment( Qt::AlignLeft );
    item->setText( tr("Product Ver") + ":" + sigItem->cPVer );
    item->setFont(g_cfgParam->gFontLargeN);

    item = ui->tableWidget->item(5, 0);
    item->setTextAlignment( Qt::AlignLeft );
    item->setText( tr("SW Ver") + ":" + sigItem->cSWver );
    item->setFont(g_cfgParam->gFontLargeN);

    m_CtrlLightInfo.screenID   = ScreenID;
    m_CtrlLightInfo.iEqIDType  = sigItem->iEqIDType;
    m_CtrlLightInfo.iSigID     = sigItem->iSigID;;
    m_CtrlLightInfo.moduleType = MODULE_TYPE(
                getModuleType(sigItem->iEqIDType) );
    TRACEDEBUG( "WdgInventory::setInvItem iEqIDType<%d> moduleType<%d> "
                "m_nPageIdx<%d> equipID<%d> oldEquipID<%d>",
                sigItem->iEqIDType, m_CtrlLightInfo.moduleType,
                m_nPageIdx, sigItem->iEquipID, m_CtrlLightInfo.iEquipID );
    if (m_CtrlLightInfo.iEquipID != sigItem->iEquipID)
    {
        m_CtrlLightInfo.iEquipID = sigItem->iEquipID;
        m_CtrlLightInfo.bSendCmd = true;
        m_CtrlLightInfo.timeElapsed.restart();
    }
}

void WdgInventory::setInvDataMem(void* pData)
{
    PACK_INVINFO* info = (PACK_INVINFO*)pData;
    int nSigNum = info->iModuleNum;
    int nScreenID = m_cmdItem.ScreenID;

    switch (nScreenID)
    {
        case SCREEN_ID_Wdg2Table_Rect_Inv:
        {
            m_nSigNum -= m_nRectInv;
            m_nRectInv = nSigNum;
            memcpy( m_pRectInv,
                &(info->ModuleInv[0]),
                nSigNum*sizeof(MOD_INV) );
        }
        break;

        case SCREEN_ID_Wdg2Table_SlaveRect_Inv:
        {
            m_nSigNum -= m_nSlaveRectInv;
            m_nSlaveRectInv = nSigNum;
            memcpy( m_pSlaveRectInv,
                &(info->ModuleInv[0]),
                nSigNum*sizeof(MOD_INV) );
        }
        break;

        case SCREEN_ID_Wdg2Table_SMI2C_Inv:
        {
            m_nSigNum -= m_nSMI2CInv;
            m_nSMI2CInv = nSigNum;
            memcpy( m_pSMI2CInv,
                &(info->ModuleInv[0]),
                nSigNum*sizeof(MOD_INV) );
        }
        break;

        case SCREEN_ID_Wdg2Table_RS485_Inv:
        {
            m_nSigNum -= m_nRS485Inv;
            m_nRS485Inv = nSigNum;
            memcpy( m_pRS485Inv,
                &(info->ModuleInv[0]),
                nSigNum*sizeof(MOD_INV) );
        }
        break;

        case SCREEN_ID_Wdg2Table_Conv_Inv:
        {
            m_nSigNum -= m_nConvInv;
            m_nConvInv = nSigNum;
            memcpy( m_pConvInv,
                &(info->ModuleInv[0]),
                nSigNum*sizeof(MOD_INV) );
        }
        break;

        case SCREEN_ID_Wdg2Table_LiBatt_Inv:
        {
            m_nSigNum -= m_nLiBattInv;
            m_nLiBattInv = nSigNum;
            memcpy( m_pLiBattInv,
                &(info->ModuleInv[0]),
                nSigNum*sizeof(MOD_INV) );
        }
        break;

        case SCREEN_ID_Wdg2Table_SolConv_Inv:
        {
            m_nSigNum -= m_nSolConvInv;
            m_nSolConvInv = nSigNum;
            memcpy( m_pSolConvInv,
                &(info->ModuleInv[0]),
                nSigNum*sizeof(MOD_INV) );
        }
        break;

        case SCREEN_ID_Wdg2Table_SMDUE_Inv:
        {
            m_nSigNum -= m_nSMDUEInv;
            m_nSMDUEInv = nSigNum;
            memcpy( m_pSMDUEInv,
                &(info->ModuleInv[0]),
                nSigNum*sizeof(MOD_INV) );
        }
        break;

        default:
        break;
    }

    m_nSigNum  += nSigNum;
}

void WdgInventory::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
}

void WdgInventory::sltTimerHandler()
{
    if ( m_CtrlLightInfo.bSendCmd &&
         m_CtrlLightInfo.timeElapsed.elapsed()>
         TIME_ELAPSED_CTRL_LIGHT )
    {
        TRACEDEBUG( "WdgInventory::sltTimerHandler() send cmd ctrl light "
                    "screenID<%x> iEquipID<%d> SigID<%d>",
                    m_CtrlLightInfo.screenID, m_CtrlLightInfo.iEquipID, m_CtrlLightInfo.iSigID );
        if ( m_CtrlLightInfo.bSendCmd )
        {
            sendCmdCtrlLight( m_CtrlLightInfo.screenID,
                              m_CtrlLightInfo.iEquipID,
                              MODULE_LIGHT_TYPE_GREEN_FLASH,
                              m_CtrlLightInfo.moduleType
                              );
            m_CtrlLightInfo.bSendCmd = false;
        }
    }
}

void WdgInventory::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void WdgInventory::keyPressEvent(QKeyEvent* keyEvent)
{
    switch ( keyEvent->key() )
    {
        case Qt::Key_Return:
        case Qt::Key_Enter:
        {
            g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
        }
        break;

        case Qt::Key_Escape:
        {
            g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ESC;

            emit goToBaseWindow( WT1_INVENTORY );
        }
        break;

        case Qt::Key_Down:
        {
            ++m_nPageIdx;
            ShowData( NULL );
        }
        break;

        case Qt::Key_Up:
        {
            --m_nPageIdx;
            ShowData( NULL );
        }
        break;

        default:
        break;
    }

    return;
}

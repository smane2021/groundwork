/******************************************************************************
文件名：    WdgFP3DCA.cpp
功能：      第一层界面p3 DC 电流负载 曲线图
作者：      刘金煌
创建日期：   2013年04月27日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "WdgFP3DCA.h"
#include "ui_WdgFP3DCA.h"

#include <QPainter>
#include <QKeyEvent>
#include <QDate>
#include "config/configparam.h"
#include "config/PosCurve.h"
#include "config/PosBase.h"
#include "common/pubInclude.h"
#include "common/global.h"

WdgFP3DCA::WdgFP3DCA(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::WdgFP3DCA)
{
    ui->setupUi(this);

    InitWidget();
    InitConnect();

    m_timerId = 0;
    m_cmdItem.CmdType      = CT_READ;
    m_cmdItem.ScreenID     = SCREEN_ID_WdgFP3DCA;
}

WdgFP3DCA::~WdgFP3DCA()
{
    TRACEDEBUG("WdgFP3DCA::~WdgFP3DCA");
    delete ui;
}

void WdgFP3DCA::InitWidget()
{
    SET_GEOMETRY_WIDGET( this );
    SET_STYLE_LABEL_CURVE_BOTTOM( ui->label_curveBottom );
    ui->label_curveBottom->setAlignment( Qt::AlignCenter );

    SET_BACKGROUND_WIDGET( "WdgFP3DCA",PosBase::strImgBack_Title );
    SET_STYLE_LABEL_ENTER;
    if (LCD_ROTATION_90DEG == g_cfgParam->ms_initParam.lcdRotation)
    {
        ui->label_enter->setGeometry(
                    PosBase::titleX1,
                    PosBase::titleY1,
                    PosBase::titleWidth1,
                    PosBase::titleHeight2Rows
                    );
        ui->label_enter->setAlignment (Qt::AlignVCenter);
    }

    SET_STYLE_SCROOLBAR(4, 2);
}

void WdgFP3DCA::InitConnect()
{
}


void WdgFP3DCA::Enter(void* param)
{
    TRACELOG1( "WdgFP3DCA::Enter(void* param)" );
    Q_UNUSED( param );

    INIT_VAR;

    m_eTempDispFormat = GetTempFormat();
    SET_STYLE_SCROOLBAR(4, 2);
#ifdef TEST_GUI
    ENTER_FIRSTLY;
#else
    ENTER_GET_DATA;
#endif

    ui->label_curveBottom->setText( tr("Last 7 days") );
}

void WdgFP3DCA::Leave()
{
    LEAVE_WDG( "WdgFP3DCA" );
    this->deleteLater();
}

void WdgFP3DCA::Refresh()
{
    REFRESH_DATA_PERIODICALLY(
                m_cmdItem.ScreenID,
                "WdgFP3DCA"
                );
}

void WdgFP3DCA::ShowData(void* pData)
{
    if ( !pData )
        return;

    PACK_TRENDINFO* info = (PACK_TRENDINFO*)pData;
    PACK_TRENDINFO* pCurrentParam = &(m_curveParam.currentData);
    m_curveParam.curveType = CURVE_TYPE_CURRENT;
    m_curveParam.nSigNum     = 0;
    m_curveParam.iFormat     = 1;
    m_curveParam.idxMaxVal   = -1;
    m_curveParam.floatMaxVal = INVALID_VALUE;
    m_curveParam.fMaxCurrent = INVALID_VALUE;
    for (int i=0; i<MAXNUM_TREND-1; ++i)
    {
        float fVal = info->fData[i];
        pCurrentParam->fData[i] = fVal;
        if (fVal > INVALID_VALUE)
        {
            if (fVal > m_curveParam.floatMaxVal)
            {
                m_curveParam.idxMaxVal   = i;
                m_curveParam.floatMaxVal = fVal;
                // max current value
                m_curveParam.fMaxCurrent = info->fCurrentData[i];
            }
            m_curveParam.nSigNum++;
            TRACEDEBUG( "     <%d> sigVal<%f>", i, fVal );
        }
    }
    TRACEDEBUG( "WdgFP3DCA::ShowData sigNum<%d>", m_curveParam.nSigNum );

    QString strTitle = tr( "Total Load") + ":";
    strTitle.append(
                QString::number(info->fData[MAXNUM_TREND-1], FORMAT_1DECIMAL) +
                "A (%) >"
                );
    ui->label_enter->setText( strTitle );
    TRACEDEBUG( "WdgFP3DCA::ShowData label_enter<%s>", strTitle.toUtf8().constData() );
    update();
}

void WdgFP3DCA::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
}

void WdgFP3DCA::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void WdgFP3DCA::keyPressEvent(QKeyEvent* keyEvent)
{
    QWidget::keyPressEvent(keyEvent);
}

void WdgFP3DCA::paintEvent(QPaintEvent *)
{
    //样式表支持
    QStyleOption opt;
    opt.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);

    QPainter* paint = &painter;

    curveChart_paint(
                paint,
                m_curveParam,
                m_eTempDispFormat
                );
}

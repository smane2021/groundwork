/******************************************************************************
文件名：    WdgFP5Deg2Curve.h
功能：      第一层界面p5 负载中第四个界面 温度曲线图
作者：      刘金煌
创建日期：   2013年5月2日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef WDGFP5DEG2CURVE_H
#define WDGFP5DEG2CURVE_H

#include "common/basicwidget.h"
#include "common/uidefine.h"
#include "common/MultiDemon.h"

using namespace CurveChart;
using namespace InputCtrl;

namespace Ui {
class WdgFP5Deg2Curve;
}

class WdgFP5Deg2Curve : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit WdgFP5Deg2Curve(QWidget *parent = 0);
    ~WdgFP5Deg2Curve();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();

protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);
    virtual void paintEvent(QPaintEvent* event);

signals:
    void goToBaseWindow(enum WIDGET_TYPE);

private:
    int      m_timerId;
    int     m_timerFreshTempFormat;
    CmdItem  m_cmdItem;
    CurveParam_t m_curveParam;
    time_t      m_timesAxis[MAXNUM_TREND];
    TEMP_DISP_FORMAT m_eTempDispFormat; //当前温度显示的格式
    TEMP_DISP_FORMAT m_eLastTempFormat; //上一次的温度显示格式。

private:
    Ui::WdgFP5Deg2Curve *ui;
};

#endif // WDGFP5DEG2CURVE_H

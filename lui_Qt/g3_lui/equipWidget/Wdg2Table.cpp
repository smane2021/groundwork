/******************************************************************************
文件名：    Wdg2Table.h
功能：      第二层界面 表格
    WT2_RECTINFO, 模块信息页
    WT2_SOLINFO,
    WT2_CONVINFO,
    WT2_SLAVE1INFO,
    WT2_SLAVE2INFO,
    WT2_SLAVE3INFO,
            WT2_ACT_ALARM, 告警个数页
            WT2_HIS_ALARM,
            WT2_EVENT_LOG,
            WT2_INVENTORY  产品信息页
作者：      刘金煌
创建日期：   2013年5月9日
最后修改日期：
    2013年08月08日 增加 converter
    2014年01月21日 模块信息超过100条刷新数据的同时选择行会变慢
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "Wdg2Table.h"
#include "ui_Wdg2Table.h"

#include <QKeyEvent>
#include "common/pubInclude.h"
#include "common/global.h"
#include "config/configparam.h"
#include "config/PosTableWidget.h"
#include "equipWidget/Wdg3Table.h"

static QColor clrCommNormal(0, 0, 0, 0);
static QColor clrCommAbnormal(255, 0, 0, 255);

#define CASE_MODULE_ITEM \
    case WT2_RECTINFO: \
    case WT2_SOLINFO: \
    case WT2_CONVINFO: \
    case WT2_SLAVE1INFO: \
    case WT2_SLAVE2INFO: \
    case WT2_SLAVE3INFO

Wdg2Table::Wdg2Table(enum WIDGET_TYPE wt, QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::Wdg2Table)
{
    ui->setupUi(this);
    m_wt      = wt;

    m_pSourceInfo = NULL;

    InitWidget();
    InitConnect();

    m_timerId  = 0;
    m_cmdItem.CmdType   = CT_READ;
    m_nPageIdx = 1;
    m_nPages   = 1;
}

Wdg2Table::~Wdg2Table()
{
    TRACEDEBUG("Wdg2Table::~Wdg2Table");
    ui->tableWidget->clearItems();

    if(m_pSourceInfo != NULL)
    {
        delete []m_pSourceInfo;

        m_pSourceInfo = NULL;
    }

    delete ui;
}

void Wdg2Table::InitWidget()
{
    SET_GEOMETRY_WIDGET( this );
    SET_STYLE_LABEL_ENTER;
    SET_BACKGROUND_WIDGET( "Wdg2Table",PosBase::strImgBack_Line_Title );

    TRACELOG1( "Wdg2Table::InitWidget()" );

    //设置背景画面
    ui->label_enter->setVisible( false );
    ui->tableWidget->clearItems();

#define SET_TABLE_RECT(screenID, func) \
    SET_BACKGROUND_WIDGET( "Wdg2Table",PosBase::strImgBack_Line ); \
    m_cmdItem.ScreenID = screenID; \
    SET_TABLEWDG_STYLE( tableWidget, TT_NOT_TITLE_NOT_SCROOL ); \
    ui->tableWidget->setGeometry( \
                PosTableWidget::xNotTitle, \
                PosTableWidget::yModule, \
                PosTableWidget::widthHasScrool, \
                PosTableWidget::heightHeader+PosTableWidget::rowHeight*PosTableWidget::rowsPerpageTitle \
                ); \
    ui->tableWidget->horizontalHeader()->setHidden( false ); \
    func;

    QTableWidgetItem *item = NULL;
    switch (m_wt)
    {
        case WT2_RECTINFO:
        {
            SET_TABLE_RECT(
                    SCREEN_ID_Wdg2Table_RectInfo,
                    setRectTableHeader());
        }
        break;

        case WT2_SOLINFO:
        {
            SET_TABLE_RECT(
                SCREEN_ID_Wdg2Table_SolInfo,
                setSolTableHeader());

#ifdef TEST_GUI
            m_nSigNum = 1;
            setMPPTTableRow( m_nSigNum );
            // row item
            item = ui->tableWidget->item(0, 0);
            item->setBackground( clrCommNormal );
            item->setText( "S1" );

            item = ui->tableWidget->item(0, 1);
            item->setBackground( clrCommNormal );
            item->setText( "78.2" );

            item = ui->tableWidget->item(0, 2);
            item->setBackground( clrCommNormal );
            item->setText( "20.5" );
#endif
        }
        break;

        case WT2_CONVINFO:
        {
            SET_TABLE_RECT(
                SCREEN_ID_Wdg2Table_ConvInfo,
                setConvTableHeader());

#ifdef TEST_GUI
            m_nSigNum = 1;
            setRectTableRow( m_nSigNum );
            // row item
            item = ui->tableWidget->item(0, 0);
            item->setBackground( clrCommNormal );
            item->setText( "Con1" );

            item = ui->tableWidget->item(0, 1);
            item->setBackground( clrCommNormal );
            item->setText( "80.2" );

            item = ui->tableWidget->item(0, 2);
            item->setBackground( clrCommNormal );
            item->setText( "OFF" );
#endif
        }
        break;

        case WT2_SLAVE1INFO:
        {
            SET_TABLE_RECT(
                SCREEN_ID_Wdg2Table_S1RectInfo,
                setSlaveRectTableHeader());
        }
        break;

        case WT2_SLAVE2INFO:
        {
            SET_TABLE_RECT(
                SCREEN_ID_Wdg2Table_S2RectInfo,
                setSlaveRectTableHeader());
        }
        break;

        case WT2_SLAVE3INFO:
        {
            SET_TABLE_RECT(
                SCREEN_ID_Wdg2Table_S3RectInfo,
                setSlaveRectTableHeader());
#ifdef TEST_GUI
            m_nSigNum = 1;
            setRectTableRow( m_nSigNum );
            // row item
            item = ui->tableWidget->item(0, 0);
            item->setBackground( clrCommNormal );
            item->setText( "S3# 120" );

            item = ui->tableWidget->item(0, 1);
            item->setBackground( clrCommNormal );
            item->setText( "10.8" );

            item = ui->tableWidget->item(0, 2);
            item->setBackground( clrCommNormal );
            item->setText( "ON" );

            ui->tableWidget->selectRow( 0 );
#endif
        }
        break;

        case WT2_ACT_ALARM:
        {
            m_cmdItem.ScreenID = SCREEN_ID_Wdg2Table_ActAlmNum;
            ui->label_enter->setVisible( true );
            ui->label_enter->setStyleSheet(
                "background-color: rgba(77,77,77, 0);"
                "color: rgb(241, 241, 241);"
                );
            SET_TABLEWDG_STYLE( tableWidget, TT_YES_TITLE_NOT_SCROOL ); // 1列
            ui->tableWidget->clearSelection();
            ui->tableWidget->clearFocus();
            ui->tableWidget->setRowCount( 3 );
            ui->verticalScrollBar->setVisible(false);
            for (int i=0; i<3; ++i)
            {
                item = new QTableWidgetItem;
                item->setText( "" );
                ui->tableWidget->setItem(i, 0, item);
                ui->tableWidget->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
            }
#ifdef TEST_GUI
            item = ui->tableWidget->item(0, 0);
            item->setText( "Observation:1" );

            item = ui->tableWidget->item(1, 0);
            item->setText( "Major:1" );

            item = ui->tableWidget->item(2, 0);
            item->setText( "Critical:1" );
#endif
        }
        break;
        // -- 历史告警不要个数页
        case WT2_HIS_ALARM:
        {
            ui->verticalScrollBar->setVisible(false);
            m_cmdItem.ScreenID = SCREEN_ID_Wdg2Table_HisAlmNum;
            ui->label_enter->setVisible( true );
            SET_TABLEWDG_STYLE( tableWidget, TT_YES_TITLE_NOT_SCROOL ); // 1列
            ui->tableWidget->clearSelection();
            ui->tableWidget->clearFocus();
            ui->tableWidget->setRowCount( 3 );
            for (int i=0; i<3; ++i)
            {
                item = new QTableWidgetItem;
                item->setText( "" );
                ui->tableWidget->setItem(i, 0, item);
                ui->tableWidget->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
            }
#ifdef TEST_GUI
            item = ui->tableWidget->item(0, 0);
            item->setText( "Observation:2" );

            item = ui->tableWidget->item(1, 0);
            item->setText( "Major:2" );

            item = ui->tableWidget->item(2, 0);
            item->setText( "Critical:2" );
#endif
        }
        break;

        case WT2_EVENT_LOG:
        {
            m_cmdItem.ScreenID = SCREEN_ID_Wdg2Table_EventLog;
        }
        break;

        case WT2_SOURCE_INFO:
        {
            SET_BACKGROUND_WIDGET( "Wdg2Table",PosBase::strImgBack_Line_Title);
            SET_TABLEWDG_STYLE( tableWidget, TT_NOT_TITLE_YES_SCROOL );
        }
        break;

        default:
        break;
    }
}

void Wdg2Table::testGUI()
{
        m_pData = g_dataBuff;
        PACK_MODINFO* info = (PACK_MODINFO*)m_pData;
        info->stHead.iScreenID = m_cmdItem.ScreenID;
        info->iModuleNum = 2;

        int sigIdx = 0;
        MODINFO *modInfo = &(info->ModInfo[sigIdx]);
        modInfo->iEquipID = 11;
        strcpy(modInfo->cEqName, "#1");
        int iSigID   = 101;
        int iSigType = 201;
        modInfo->EachMod[0].iSigID = iSigID;
        modInfo->EachMod[0].iSigType = iSigID;
        modInfo->EachMod[0].vSigValue.lValue = 0;

        modInfo->EachMod[1].iSigID = iSigID;
        modInfo->EachMod[1].iSigType = iSigID;
        modInfo->EachMod[1].iFormat = 1;
        modInfo->EachMod[1].vSigValue.fValue = 1.1;

        modInfo->EachMod[2].iSigID = iSigID;
        modInfo->EachMod[2].iSigType = iSigID;
        strcpy(modInfo->EachMod[2].cEnumText, "OFF");

        sigIdx++;

        modInfo = &(info->ModInfo[sigIdx]);
        modInfo->iEquipID = 12;
        strcpy(modInfo->cEqName, "#2");
        iSigID++;
        iSigType++;
        modInfo->EachMod[0].iSigID = iSigID;
        modInfo->EachMod[0].iSigType = iSigID;
        modInfo->EachMod[0].vSigValue.lValue = 0;

        modInfo->EachMod[1].iSigID = iSigID;
        modInfo->EachMod[1].iSigType = iSigID;
        modInfo->EachMod[1].iFormat = 1;
        modInfo->EachMod[1].vSigValue.fValue = 2.2;

        modInfo->EachMod[2].iSigID = iSigID;
        modInfo->EachMod[2].iSigType = iSigID;
        strcpy(modInfo->EachMod[2].cEnumText, "OFF");

        ShowData( m_pData );
}

void Wdg2Table::InitConnect()
{
    connect( &m_QTimerLight, SIGNAL(timeout()),
             this, SLOT(sltTimerHandler()) );
    switch (m_wt)
    {
    CASE_MODULE_ITEM:
    {
        connect( ui->tableWidget, SIGNAL(sigTableKeyPress(int)),
                 this, SLOT(sltTableKeyPress(int)) );
    }
        break;

    default:
        break;
    }
}

void Wdg2Table::Enter(void* param)
{
    TRACELOG1( "Wdg2Table::Enter(void* param) m_wt<%d>", m_wt );
    Q_UNUSED( param );

    INIT_VAR;

    m_nSigNum   = 0;
    m_nPageIdx = 1;
    m_nPages   = 1;
    m_pData = NULL;

    m_CtrlLightInfo.iEquipID = -1;
    m_CtrlLightInfo.bSendCmd = false;

#define ENTER_MODULE \
    m_CtrlLightInfo.timeElapsed.restart(); \
    m_QTimerLight.start( TIMER_UPDATE_DATA_INTERVAL/2 ); \
    {\
    ui->tableWidget->clearItems(); \
    m_timeElapsedKeyPress.restart(); \
    m_bEnterFirstly = true; \
    ENTER_GET_DATA; \
    ui->tableWidget->setFocus ();\
    ui->tableWidget->selectRow( 0 ); \
    SET_STYLE_SCROOLBAR( m_nSigNum, 1 ); \
    }

    switch (m_wt)
    {
        // module
        case WT2_RECTINFO:
        {
#ifdef TEST_GUI
            m_CtrlLightInfo.timeElapsed.restart();
            m_QTimerLight.start( TIMER_UPDATE_DATA_INTERVAL/2 );
            {
                ui->tableWidget->clearItems();
                m_timeElapsedKeyPress.restart();
                m_bEnterFirstly = true;
                ENTER_FIRSTLY;
                testGUI();
                ui->tableWidget->setFocus();
                ui->tableWidget->selectRow( 0 );
                SET_STYLE_SCROOLBAR( m_nSigNum, 1 );
            }
#else
            ENTER_MODULE;
#endif
        }
        break;

        case WT2_SOLINFO:
        {
#ifdef TEST_GUI
            ENTER_FIRSTLY;
#else
            ENTER_MODULE;
#endif
        }
        break;

        case WT2_CONVINFO:
        {
#ifdef TEST_GUI
            ENTER_FIRSTLY;
#else
            ENTER_MODULE;
#endif
        }
        break;

        case WT2_SLAVE1INFO:
        {
#ifdef TEST_GUI
            ENTER_FIRSTLY;
#else
            ENTER_MODULE;
#endif
        }
        break;

        case WT2_SLAVE2INFO:
        {
#ifdef TEST_GUI
            ENTER_FIRSTLY;
#else
            ENTER_MODULE;
#endif
        }
        break;

        case WT2_SLAVE3INFO:
        {
    #ifdef TEST_GUI
            ENTER_FIRSTLY;
    #else
            ENTER_MODULE;
    #endif
        }
        break;

        case WT2_ACT_ALARM:
        {
            m_nSigNum = 0;
#ifdef TEST_GUI
        ENTER_FIRSTLY;
#else
        ENTER_GET_DATA;
#endif
            ui->label_enter->setText( tr("Active Alarms") );
        }
        break;

        case WT2_HIS_ALARM:
        {
            ui->label_enter->setText( tr("Alarm History") );
            emit goToBaseWindow( WIDGET_TYPE(WT3_HIS_ALARM) );
            //ENTER_GET_DATA;
        }
        break;

        case WT2_EVENT_LOG:
        {
            ENTER_GET_DATA;
        }
        break;

        case WT2_SOURCE_INFO:
        {
            m_CtrlLightInfo.timeElapsed.restart();
            this->setFocus();
            //clearFocus();
            ui->tableWidget->clearFocus();
            ui->tableWidget->clearSelection();

            m_cmdItem.ScreenID = SCREEN_ID_Source_Info;

            m_pSourceInfo = new PACK_INFO[52];

            ENTER_GET_DATA;
        }
        break;

        default:
            break;
    }

    m_bEnterFirstly = false;
}

void Wdg2Table::Leave()
{
    LEAVE_WDG( "Wdg2Table" );

    if ( m_QTimerLight.isActive() )
    {
        m_QTimerLight.stop();
    }

    switch ( m_wt )
    {
        CASE_MODULE_ITEM:
        {
            sendCmdCtrlLight(
                    m_CtrlLightInfo.screenID,
                    -1,
                    MODULE_LIGHT_TYPE_ON,
                    m_CtrlLightInfo.moduleType
                    );
        }
        break;

        default:
        break;
    }

    this->deleteLater();
}

void Wdg2Table::Refresh()
{
    REFRESH_DATA_PERIODICALLY(
                m_cmdItem.ScreenID,
                "Wdg2Table"
                );
}

void Wdg2Table::setSourceInfoItem()
{
    ui->tableWidget->clearItems();
    if(m_nSourceInfo == 0)
    {
        return;
    }

    ui->tableWidget->setRowCount( m_nSourceInfo*2 );
    ui->tableWidget->setColumnCount (1);

    //显示这些电流值需要的页码
    m_nPages = (m_nSourceInfo % 3) > 0 ? ((m_nSourceInfo / 3) + 1) : (m_nSourceInfo / 3);

    //如果当前页大于总页码，则不跳转，否则跳转到第一页。
    if(m_nPageIdx > m_nPages)
    {
        m_nPageIdx = 1;
    }
    SET_STYLE_SCROOLBAR( m_nPages, m_nPageIdx );

    //获取当前页应显示的个数
    int nCurPageSigNum = 0;
    if(m_nPageIdx == m_nPages)
    {
        nCurPageSigNum = ((m_nSourceInfo % 3) == 0) ? 3 : (m_nSourceInfo % 3);
    }
    else
    {
        nCurPageSigNum = 3;
    }

    QTableWidgetItem *pItem = NULL;
    for(int i=0; i<nCurPageSigNum; i++)
    {
        //信号名
        QString strSigName = m_pSourceInfo[(m_nPageIdx-1)*3+i].cSigName;
        strSigName += ":";
        pItem = new QTableWidgetItem(strSigName);
        ui->tableWidget->setItem (i*2,0,pItem);
//        TRACEDEBUG("setSourceInfoItem#sigName:%s",qPrintable(strSigName));
        ui->tableWidget->setRowHeight(i*2, TABLEWDG_ROW_HEIGHT );
        pItem->setFont(g_cfgParam->gFontLargeN);

        //信号值 ,因为只显示电流信号，所以默认传过来的信号都是浮点类型的电流信号，
        //不再根据信号值类型显示信号值
        double fSigValue= m_pSourceInfo[(m_nPageIdx-1)*3+i].vSigValue.fValue;
        QString strSigVal = QString::number (fSigValue,'f',m_pSourceInfo[(m_nPageIdx-1)*3+i].iFormat);
        strSigVal += (m_pSourceInfo[(m_nPageIdx-1)*3+i].cSigUnit);
        pItem = new QTableWidgetItem(strSigVal);
        ui->tableWidget->setItem ((i*2)+1,0,pItem);
//        TRACEDEBUG("setSourceInfoItem#sigVal:%s",qPrintable(strSigVal));
        ui->tableWidget->setRowHeight((i*2)+1, TABLEWDG_ROW_HEIGHT );
        pItem->setFont(g_cfgParam->gFontLargeN);
    }

    SET_STYLE_SCROOLBAR( m_nPages, m_nPageIdx );
}

void Wdg2Table::ShowData(void* pData)
{
    //TRACEDEBUG( "Wdg2Table::ShowData(void* pData) %x", pData );
    QTableWidgetItem* item = NULL;
    switch (m_wt)
    {
        CASE_MODULE_ITEM:
        {
            m_pData = pData;
            if ( !pData )
            {
                return;
            }
            // 防止一直刷新，按键查看项时出现卡顿
            if (m_timeElapsedKeyPress.elapsed() >
                TIME_ELAPSED_KEYPRESS ||
                m_bEnterFirstly)
            {
                setModuleItem( pData );
            }
        }
        break;

        case WT2_ACT_ALARM:
        {
            m_pData = pData;
            if ( !pData )
            {
                return;
            }

            PACK_ALMNUM* info = (PACK_ALMNUM*)pData;
            item = ui->tableWidget->item(0, 0);
            item->setText( tr("Observation") + ":" +
            QString::number(info->OANum) );
            item->setFont(g_cfgParam->gFontLargeN);

            item = ui->tableWidget->item(1, 0);
            item->setText( tr("Major") + ":" +
            QString::number(info->MANum) );
            item->setFont(g_cfgParam->gFontLargeN);

            item = ui->tableWidget->item(2, 0);
            item->setText( tr("Critical") + ":" +
            QString::number(info->CANum) );
            m_nSigNum = info->OANum + info->MANum + info->CANum;
            item->setFont(g_cfgParam->gFontLargeN);
        }
        break;

        case WT2_HIS_ALARM:
        break;

        case WT2_EVENT_LOG:
        break;

        case WT2_SOURCE_INFO:
        {
            setSourceInfoDataMem(pData);
            setSourceInfoItem ();
        }
        break;

        default:
        break;
    }
}

//    module
void Wdg2Table::setModuleItem(void *pData)
{
    QTableWidgetItem* item = NULL;
    int nSelRow = ui->tableWidget->selectedRow();
    PACK_MODINFO* info = (PACK_MODINFO*)pData;
    int nSigNum = info->iModuleNum;
    if (nSigNum == 0)
    {
        m_nSigNum = nSigNum;
        ui->tableWidget->clearItems();
        return;
    }

    if (m_nSigNum != nSigNum)
    {
        ui->tableWidget->clearItems();
        m_nSigNum = nSigNum;
        switch (m_wt)
        {
            case WT2_RECTINFO:
            case WT2_CONVINFO:
            case WT2_SLAVE1INFO:
            case WT2_SLAVE2INFO:
            case WT2_SLAVE3INFO:
            {
                setRectTableRow( m_nSigNum );
            }
            break;

            case WT2_SOLINFO:
            {
                setMPPTTableRow( m_nSigNum );
            }
            break;

            default:
            break;
        }
    }
    TRACEDEBUG( "Wdg2Table::setModuleItem()SigNum<%d>", m_nSigNum );
    m_nSigNum = (m_nSigNum>MAXNUM_MODULE ?
                     MAXNUM_MODULE:m_nSigNum);
    nSelRow = (nSelRow>=m_nSigNum-1 ?
                     (m_nSigNum-1):nSelRow);
    int iFormat = 0;
    long nState = 0;
    for (int i=0; i<m_nSigNum; ++i)
    {
        MODINFO *modInfo = &(info->ModInfo[i]);
        // 在EachMod中第一个是通讯状态，0-正常；非0-通讯中断。
        nState = modInfo->EachMod[0].vSigValue.lValue;
        if ( nState )
        {
            item = ui->tableWidget->item(i, 0);
            item->setBackground( clrCommAbnormal );
            item->setFont(g_cfgParam->gFontLargeN);

            item = ui->tableWidget->item(i, 1);
            item->setBackground( clrCommAbnormal );
            item->setFont(g_cfgParam->gFontLargeN);

            item = ui->tableWidget->item(i, 2);
            item->setBackground( clrCommAbnormal );
            item->setFont(g_cfgParam->gFontLargeN);

            TRACEDEBUG( "Wdg2Table::setModuleItem() module<%d> state<%d> is not communicated",
                        i, nState );
        }
        else
        {
            item = ui->tableWidget->item(i, 0);
            item->setBackground( clrCommNormal );
            item->setFont(g_cfgParam->gFontLargeN);

            item = ui->tableWidget->item(i, 1);
            item->setBackground( clrCommNormal );
            item->setFont(g_cfgParam->gFontLargeN);

            item = ui->tableWidget->item(i, 2);
            item->setBackground( clrCommNormal );
            item->setFont(g_cfgParam->gFontLargeN);
        }
        // index
        item = ui->tableWidget->item(i, 0);
        item->setText(modInfo->cEqName );
        item->setFont(g_cfgParam->gFontLargeN);

        if (m_wt == WT2_SOLINFO)
        {
            // No. Vin(V) Iout(A)
            item = ui->tableWidget->item(i, 1);
            iFormat = modInfo->EachMod[1].iFormat;
            item->setText(QString::number(
                               modInfo->EachMod[1].vSigValue.fValue,
                               FORMAT_DECIMAL)
                           );
            item->setFont(g_cfgParam->gFontLargeN);

            iFormat = modInfo->EachMod[2].iFormat;
            item = ui->tableWidget->item(i, 2);
            item->setText(QString::number(
                               modInfo->EachMod[2].vSigValue.fValue,
                               FORMAT_DECIMAL)
                           );
            item->setFont(g_cfgParam->gFontLargeN);
        }
        else
        {
            // NO. Iout(A) State
            item = ui->tableWidget->item(i, 1);
            iFormat = modInfo->EachMod[1].iFormat;
            item->setText(QString::number(
                               modInfo->EachMod[1].vSigValue.fValue,
                               FORMAT_DECIMAL)
                           );
            item->setFont(g_cfgParam->gFontLargeN);

            item = ui->tableWidget->item(i, 2);
            item->setText(modInfo->EachMod[2].cEnumText );
            item->setFont(g_cfgParam->gFontLargeN);
        }
        TRACEDEBUG( "Wdg2Table::setModuleItem() iFormat<%d>", iFormat );
    }

    ui->tableWidget->selectRow( nSelRow );
    SET_STYLE_SCROOLBAR( m_nSigNum, nSelRow+1 );
}

#define SET_HEADER_STYLE \
    ui->tableWidget->horizontalHeader()->setStyleSheet( \
                "QHeaderView::section {" \
                "background-color:#" LIGHT_COLOR_ENTER ";" \
                "}" \
                ); \
    ui->tableWidget->setFrameShape(QFrame::NoFrame); \
    ui->tableWidget->setShowGrid(false); \
    ui->tableWidget->horizontalHeader()->setFixedHeight( PosTableWidget::heightHeader );


// set module table header
void Wdg2Table::setRectTableHeader()
{
    SET_HEADER_STYLE;

    // column
    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->setColumnWidth(0, PosTableWidget::widthHeadRect1);
    ui->tableWidget->setColumnWidth(1, PosTableWidget::widthHeadRect2);
    ui->tableWidget->setColumnWidth(2, PosTableWidget::widthHeadRect3);
}

void Wdg2Table::setSolTableHeader()
{
    SET_HEADER_STYLE;
//    ui->tableWidget->horizontalHeader()->setWindowOpacity( 0 );
    // column
    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->setColumnWidth(0, PosTableWidget::widthHeadSol1);
    ui->tableWidget->setColumnWidth(1, PosTableWidget::widthHeadSol2);
    ui->tableWidget->setColumnWidth(2, PosTableWidget::widthHeadSol3);
}

void Wdg2Table::setConvTableHeader()
{
    setRectTableHeader();
}

void Wdg2Table::setSlaveRectTableHeader()
{
    setRectTableHeader();
}

// set module table row
void Wdg2Table::setRectTableRow(int nRowCount)
{
    // header item
    QBrush brush(Qt::white);

    QTableWidgetItem* item = NULL;
    item = new QTableWidgetItem;
    item->setForeground (brush);
    item->setText( tr("Index") );
    item->setTextAlignment( Qt::AlignCenter );
    ui->tableWidget->setHorizontalHeaderItem(0, item);
    item->setFont(g_cfgParam->gFontLargeN);

    item = new QTableWidgetItem;
    item->setForeground (brush);
    item->setText( tr("Iout(A)") );
    item->setTextAlignment( Qt::AlignCenter );
    ui->tableWidget->setHorizontalHeaderItem(1, item);
    item->setFont(g_cfgParam->gFontLargeN);

    item = new QTableWidgetItem;
    item->setForeground (brush);
    item->setText( tr("State") );
    item->setTextAlignment( Qt::AlignCenter );
    ui->tableWidget->setHorizontalHeaderItem(2, item);
    item->setFont(g_cfgParam->gFontLargeN);

    // row
    ui->tableWidget->setRowCount( nRowCount );
    for (int i=0; i<nRowCount; ++i)
    {
        int j=0;
        item = new QTableWidgetItem;
        item->setText( "" );
        item->setTextAlignment( Qt::AlignCenter );
        ui->tableWidget->setItem(i, j, item);
        item->setFont(g_cfgParam->gFontLargeN);

        for (j=1; j<3; ++j)
        {
            item = new QTableWidgetItem;
            item->setTextAlignment( Qt::AlignCenter );
            item->setText( "" );
            ui->tableWidget->setItem(i, j, item);
            item->setFont(g_cfgParam->gFontLargeN);
        }
        ui->tableWidget->setRowHeight(i, TABLEWDG_ROW_HEIGHT );
    }
}

void Wdg2Table::setMPPTTableRow(int nRowCount)
{
    // header item
    QBrush brush(Qt::white);

    QTableWidgetItem* item = NULL;
    item = new QTableWidgetItem;
    item->setForeground (brush);
    item->setText( tr("Index") );
    item->setTextAlignment( Qt::AlignCenter );
    ui->tableWidget->setHorizontalHeaderItem(0, item);
    item->setFont(g_cfgParam->gFontLargeN);

    item = new QTableWidgetItem;
    item->setForeground (brush);
    item->setText( tr("Vin(V)") );
    ui->tableWidget->setHorizontalHeaderItem(1, item);
    item->setFont(g_cfgParam->gFontLargeN);

    item = new QTableWidgetItem;
    item->setForeground (brush);
    item->setText( tr("Iout(A)") );
    ui->tableWidget->setHorizontalHeaderItem(2, item);
    item->setFont(g_cfgParam->gFontLargeN);

    // row
    ui->tableWidget->setRowCount( nRowCount );
    for (int i=0; i<nRowCount; ++i)
    {
        int j=0;
        item = new QTableWidgetItem;
        item->setText( "" );
        item->setTextAlignment( Qt::AlignCenter );
        ui->tableWidget->setItem(i, j, item);
        item->setFont(g_cfgParam->gFontLargeN);

        for (j=1; j<3; ++j)
        {
            item = new QTableWidgetItem;
            item->setTextAlignment( Qt::AlignCenter );
            item->setText( "" );
            ui->tableWidget->setItem(i, j, item);
            item->setFont(g_cfgParam->gFontLargeN);
        }
        ui->tableWidget->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
    }
}

void Wdg2Table::sltScreenSaver()
{
    if (BasicWidget::ms_showingWdg == this)
    {
        ;
    }
}

void Wdg2Table::setSourceInfoDataMem(void *pData)
{
    memset(&m_pSourceInfo[0],0,sizeof(PACK_INFO) * 52);

    PACK_INFO* info = (PACK_INFO*)pData;
    int iDataNum = info->iDataNum;

    m_nSourceInfo = iDataNum;
    for(int i=0; i<iDataNum; i++)
    {
        //信号名
        memcpy(m_pSourceInfo[i].cSigName,info[i].cSigName,MAXLEN_NAME);

        //单位
        memcpy(m_pSourceInfo[i].cSigUnit,info[i].cSigUnit,MAXLEN_NAME);

        //信号值 ,因为只显示电流信号，所以默认传过来的信号都是浮点类型的电流信号，
        //不再根据信号值类型显示信号值
        double fSigValue= info[i].vSigValue.fValue;
        m_pSourceInfo[i].vSigValue.fValue = fSigValue;
        m_pSourceInfo[i].iFormat = info[i].iFormat;
    }
}

void Wdg2Table::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
}

void Wdg2Table::sltTimerHandler()
{
    if ( m_CtrlLightInfo.bSendCmd &&
         m_CtrlLightInfo.timeElapsed.elapsed()>
         TIME_ELAPSED_CTRL_LIGHT )
    {
        TRACEDEBUG( "Wdg2Table::sltTimerHandler()" );
        switch (m_wt)
        {
        CASE_MODULE_ITEM:
        {
            PACK_MODINFO* info = (PACK_MODINFO*)m_pData;
            if (!info || info->stHead.iScreenID!=m_cmdItem.ScreenID)
                return;

            MODINFO* modInfo = &(info->ModInfo[
                                 ui->tableWidget->selectedRow()]
                                 );
            EACHMOD_INFO itemInfo = modInfo->EachMod[1];

            m_CtrlLightInfo.screenID   = m_cmdItem.ScreenID;
            m_CtrlLightInfo.iSigID     = itemInfo.iSigID;
            m_CtrlLightInfo.moduleType = MODULE_TYPE(
                        getModuleTypeByWt(m_wt) );
            if (m_CtrlLightInfo.iEquipID != modInfo->iEquipID)
            {
                m_CtrlLightInfo.iEquipID = modInfo->iEquipID;
            }
            else
            {
                m_CtrlLightInfo.bSendCmd = false;
            }
        }
        break;

        default:
            break;
        }

        TRACEDEBUG( "Wdg2Table::sltTimerHandler() send cmd ctrl light "
                    "screenID<%x> iEquipID<%d> SigID<%d>",
                    m_CtrlLightInfo.screenID, m_CtrlLightInfo.iEquipID, m_CtrlLightInfo.iSigID );
        if ( m_CtrlLightInfo.bSendCmd )
        {
            sendCmdCtrlLight( m_CtrlLightInfo.screenID,
                              m_CtrlLightInfo.iEquipID,
                              MODULE_LIGHT_TYPE_GREEN_FLASH,
                              m_CtrlLightInfo.moduleType
                              );
            m_CtrlLightInfo.bSendCmd = false;
        }
    }
}

void Wdg2Table::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void Wdg2Table::keyPressEvent(QKeyEvent* keyEvent)
{
    TRACEDEBUG( "Wdg2Table::keyPressEvent" );

    switch ( keyEvent->key() )
    {
        case Qt::Key_Return:
        case Qt::Key_Enter:
        {
            g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
            //TRACELOG1( "Wdg2Table::keyPressEvent Key_Enter" );
            switch (m_wt)
            {
                case WT2_ACT_ALARM:
                {
                    #ifdef TEST_GUI
                    Wdg3Table::ms_bEscapeFromHelp = false;
                    emit goToBaseWindow( WIDGET_TYPE(WT3_ACT_ALARM) );
                    #else
                    if (m_nSigNum > 0)
                    {
                        //int idxItem = ui->tableWidget->selectedRow();
                        Wdg3Table::ms_bEscapeFromHelp = false;
                        emit goToBaseWindow( WIDGET_TYPE(WT3_ACT_ALARM) );
                    }
                    #endif
                }
                break;

                default:
                {
                    TRACELOG1("Wdg2Table Key_Enter default wt<%d>", m_wt);
                }
                break;
            }
        }
        break;

        case Qt::Key_Escape:
        {
            g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ESC;
            switch (m_wt)
            {
                CASE_MODULE_ITEM:
                case WT2_SOURCE_INFO:
                {
                    //            sendCmdCtrlLight_ON();
                    emit goToBaseWindow( WT1_MODULE );
                }
                break;

                case WT2_ACT_ALARM:
                case WT2_HIS_ALARM:
                case WT2_EVENT_LOG:
                {
                    emit goToBaseWindow( WT1_ALARM );
                }
                break;

                default:
                break;
            }
        }
        break;

        case Qt::Key_Down:
        {
            TRACEDEBUG( "Wdg2Table::keyPressEvent Key_Down" );
            switch (m_wt)
            {
                CASE_MODULE_ITEM:
                {
                    ui->tableWidget->selectRow( 0 );
                    SET_STYLE_SCROOLBAR( m_nSigNum, 1 );
                }
                break;

                case WT2_SOURCE_INFO:
                {
                    if(m_nPageIdx == m_nPages)
                    {
                        m_nPageIdx = 1;
                    }
                    else
                    {
                        ++ m_nPageIdx;
                    }
                    SET_STYLE_SCROOLBAR( m_nPages, m_nPageIdx );
                    setSourceInfoItem();
                }
                break;

                default:
                    break;
            }
        }
        break;

        case Qt::Key_Up:
        {
            TRACEDEBUG( "Wdg2Table::keyPressEvent Key_Up" );
            switch (m_wt)
            {
                CASE_MODULE_ITEM:
                {
                    int nRowCount = ui->tableWidget->rowCount();

                    ui->tableWidget->selectRow( nRowCount-1 );
                    SET_STYLE_SCROOLBAR( m_nSigNum, m_nSigNum );
                }
                break;

                case WT2_SOURCE_INFO:
                {
                    if(m_nPageIdx == 1)
                    {
                        m_nPageIdx = m_nPages;
                    }
                    else
                    {
                        -- m_nPageIdx;
                    }
                    SET_STYLE_SCROOLBAR( m_nPages, m_nPageIdx );
                    setSourceInfoItem();
                }
                break;

                default:
                    break;
            }
        }
        break;

        default:
        break;
    }
}

void Wdg2Table::sltTableKeyPress(int key)
{
    TRACEDEBUG( "Wdg2Table::sltTableKeyPress" );
    switch (m_wt)
    {
        CASE_MODULE_ITEM:
        {
            m_timeElapsedKeyPress.restart();
            switch ( key )
            {
                case Qt::Key_Up:
                case Qt::Key_Down:
                {
                    m_CtrlLightInfo.timeElapsed.restart();
                    int nSelRow = ui->tableWidget->selectedRow();
                    SET_STYLE_SCROOLBAR( m_nSigNum, nSelRow+1 );
                }
                break;
            }
        }
        break;
    }
}

void Wdg2Table::on_tableWidget_itemSelectionChanged()
{
    TRACEDEBUG( "Wdg2Table::on_tableWidget_itemSelectionChanged()" );
    switch (m_wt)
    {
        CASE_MODULE_ITEM:
        {
            m_CtrlLightInfo.bSendCmd = true;
        }
        break;

        default:
        break;
    }
}

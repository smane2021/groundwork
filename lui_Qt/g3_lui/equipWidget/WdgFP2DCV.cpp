/******************************************************************************
文件名：    WdgFP2DCV.cpp
功能：      第一层界面p2 DC 电压环形图
作者：      刘金煌
创建日期：   2013年04月27日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "WdgFP2DCV.h"
#include "ui_WdgFP2DCV.h"

#include <QPainter>
#include <QKeyEvent>
#include <QPixmap>
#include <QPixmapCache>
#include "common/pubInclude.h"
#include "common/global.h"
#include "config/configparam.h"
#include "config/PosBase.h"
#include "config/PosPieChart.h"

WdgFP2DCV::WdgFP2DCV(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::WdgFP2DCV)
{
    ui->setupUi(this);

    InitWidget();
    InitConnect();

    m_timerId = 0;
    m_cmdItem.CmdType   = CT_READ;
    m_cmdItem.ScreenID  = SCREEN_ID_WdgFP2DCV;
}

WdgFP2DCV::~WdgFP2DCV()
{
    TRACEDEBUG("WdgFP2DCV::~WdgFP2DCV");
    delete ui;
}

void WdgFP2DCV::InitWidget()
{
    SET_GEOMETRY_WIDGET( this );
    SET_BACKGROUND_WIDGET( "WdgFP2DCV",PosBase::strImgBack_Title );
    SET_STYLE_SCROOLBAR( 4, 1 );
    SET_GEOMETRY_LABEL_TITLE2( ui->label_title );
    if (LCD_ROTATION_90DEG == g_cfgParam->ms_initParam.lcdRotation)
    {
        m_pixmapHand.load(PATH_IMG + "pie_pointer3V.png");
    }
    else
    {
       m_pixmapHand.load(PATH_IMG + "pie_pointer3.png");
    }


#ifdef TEST_GUI
    PACK_ICOINFO* info = (PACK_ICOINFO*)g_dataBuff;
    info->LimitValue[0] = 500;
    info->LimitValue[5] = 0;

    info->LimitValue[1] = 400;
    info->LimitValue[2] = 300;
    info->LimitValue[3] = 200;
    info->LimitValue[4] = 100;
    info->DispData[0].iFormat = 1;

    m_fSigVal      = info->DispData[0].fSigValue = 100;
    qstrcpy( info->DispData[0].cSigName, "Output Voltage" );
    qstrcpy( info->DispData[0].cSigUnit, "V" );

    ShowData( info );
#endif
}

void WdgFP2DCV::InitConnect()
{
}


void WdgFP2DCV::Enter(void* param)
{
    TRACELOG1( "WdgFP2DCV::Enter(void* param)" );
    Q_UNUSED( param );

    INIT_VAR;

    enum PIE_CHART_TYPE type = PIE_CHART_TYPE_DC;
    PosPieChart::setPieChartType( (void*)&type );

#ifdef TEST_GUI
    ENTER_FIRSTLY;
#else
    ENTER_GET_DATA;
#endif
}

void WdgFP2DCV::Leave()
{
    LEAVE_WDG( "WdgFP2DCV" );
    this->deleteLater();
    QPixmapCache::clear ();
}

void WdgFP2DCV::Refresh()
{
    REFRESH_DATA_PERIODICALLY(
                m_cmdItem.ScreenID,
                "WdgFP2DCV"
                );
}

void WdgFP2DCV::ShowData(void* pData)
{
    if ( !pData )
        return;

    PACK_ICOINFO* info = (PACK_ICOINFO*)pData;
    for (int i=0; i<LIMNUM; ++i)
    {
        // 环形是逆时针画的，与获取到的数据相反
        m_fMargins[LIMNUM-1-i] =
                info->LimitValue[i];
    }
//    for (int i=1; i<LIMNUM-1; ++i)
//    {
//        // 上下限值与获取到的数据排序相反
//        m_fMargins[LIMNUM-1-i] = info->LimitValue[i-1];
//    }
//    m_fMargins[0] = (int)(m_fMargins[1]*0.7);
//    m_fMargins[LIMNUM-1] = (int)(m_fMargins[4]*1.3);

    m_fSigVal = info->DispData[0].fSigValue;
    iFormat = info->DispData[0].iFormat;
    QString strTitle = QString( info->DispData[0].cSigName ) + ":";
//    if (LCD_ROTATION_90DEG == g_cfgParam->ms_initParam.lcdRotation)
//    {
//        strTitle.append( "\n" );
//    }

    strTitle.append(
                QString::number(m_fSigVal, FORMAT_DECIMAL)+
                QString(info->DispData[0].cSigUnit)
                );
    ui->label_title->setText(strTitle );

    TRACEDEBUG( "WdgFP2DCV::ShowData() read data "
                "Limit<%f %f %f %f %f %f> "
                "iFormat<%d> SigVal<%f> SigName<%s> sigUnit<%s>",
                m_fMargins[0], m_fMargins[1],
                m_fMargins[2], m_fMargins[3],
                m_fMargins[4], m_fMargins[5],
                iFormat, m_fSigVal,
                info->DispData[0].cSigName,
                info->DispData[0].cSigUnit
                );
    update();
}

void WdgFP2DCV::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
}

void WdgFP2DCV::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void WdgFP2DCV::keyPressEvent(QKeyEvent* keyEvent)
{
    QWidget::keyPressEvent(keyEvent);
}

// WdgFP0AC
void WdgFP2DCV::paintEvent(QPaintEvent *)
{
    //样式表支持
    QStyleOption opt;
    opt.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);

    QPixmap pixmapCover;
    QPixmap pixmapCenter;
    if(g_cfgParam->ms_initParam.lcdRotation == LCD_ROTATION_90DEG)
    {
       pixmapCover.load(PATH_IMG + "pie_coverV.png");
       pixmapCenter.load(PATH_IMG + "pie_centerV.png");
    }
    else
    {
        pixmapCover.load(PATH_IMG + "pie_cover.png");
        pixmapCenter.load(PATH_IMG + "pie_center.png");
    }
    pieChart_paint(
                &painter,
                gs_pieMarginColors,
                m_fMargins,
                LIMNUM,

                iFormat,
                &m_fSigVal,

                &m_pixmapHand,
                &pixmapCover,
                &pixmapCenter,
                false
                );
}

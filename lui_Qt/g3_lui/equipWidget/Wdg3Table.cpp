#include "Wdg3Table.h"
#include "ui_Wdg3Table.h"

#include <QDateTime>
#include <QKeyEvent>
#include "config/configparam.h"
#include "common/pubInclude.h"
#include "common/global.h"
#include "basicWidget/GuideWindow.h"

bool     Wdg3Table::ms_bHasActHelp = false;
//bool     Wdg3Table::ms_bHasHisHelp = false;
bool     Wdg3Table::ms_bEscapeFromHelp;
QString  Wdg3Table::ms_strHelp;
QString  Wdg3Table::ms_strAlarmLevel[4];

#define SPACE_HELP   "       >"

Wdg3Table::Wdg3Table(enum WIDGET_TYPE wt, QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::Wdg3Table)
{
    ui->setupUi(this);

    ms_strAlarmLevel[0] = tr("OA Alarm");
    ms_strAlarmLevel[1] = tr("OA Alarm");
    ms_strAlarmLevel[2] = tr("MA Alarm");
    ms_strAlarmLevel[3] = tr("CA Alarm");

    m_wt      = wt;

    InitWidget();
    InitConnect();

    m_timerId    = 0;
    m_nPageIdx   = 1;
}

Wdg3Table::~Wdg3Table()
{
    TRACEDEBUG("Wdg3Table::~Wdg3Table");
    ui->tableWidget->clearItems();
    delete ui;
}

void Wdg3Table::InitWidget()
{
    //���ñ�������
    SET_GEOMETRY_WIDGET( this );
    SET_BACKGROUND_WIDGET( "Wdg3Table",PosBase::strImgBack_Line_Title );

    SET_STYLE_SCROOLBAR( 1, 1 );

    ui->tableWidget->clearSelection();
    ui->tableWidget->clearFocus();
    this->setFocus();

    SET_TABLEWDG_STYLE( tableWidget, TT_NOT_TITLE_YES_SCROOL );

    ui->tableWidget->clearItems();
    ui->tableWidget->setRowCount( TABLEWDG_ROWS_PERPAGE_NOTITLE );
    QTableWidgetItem * item;
    for (int i=0; i<TABLEWDG_ROWS_PERPAGE_NOTITLE; ++i)
    {
        item = new QTableWidgetItem;
        item->setText( "" );
        ui->tableWidget->setItem(i, 0, item);
        ui->tableWidget->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
    }

    switch (m_wt)
    {
        case WT3_ACT_ALARM:
        {
            m_cmdItem.ScreenID = SCREEN_ID_Wdg3Table_ActAlm;
    #ifdef TEST_GUI
            item = ui->tableWidget->item(0, 0);
            item->setText( "1" SPACE_HELP );
            ms_bHasActHelp = true;
            ms_strHelp = "SPD is not connected to system or SPD is broken!";

            item = ui->tableWidget->item(1, 0);
            item->setText( "SMBRC Battery" );

            item = ui->tableWidget->item(2, 0);
            item->setText( "Low Capacity" );

            item = ui->tableWidget->item(3, 0);
            item->setText( "121203 08:12:20" );

            item = ui->tableWidget->item(4, 0);
            item->setText( "Observation" );
    #endif
        }
        break;

        case WT3_HIS_ALARM:
        {
            m_cmdItem.ScreenID = SCREEN_ID_Wdg3Table_HisAlm;
    #ifdef TEST_GUI
            item = ui->tableWidget->item(0, 0);
            item->setText( "1" );

            item = ui->tableWidget->item(1, 0);
            item->setText( "SMBRC Battery" );

            item = ui->tableWidget->item(2, 0);
            item->setText( "Low Capacity" );

            item = ui->tableWidget->item(3, 0);
            item->setText( "130102 01:02:03" );

            item = ui->tableWidget->item(4, 0);
            item->setText( "Observation" );
    #endif
        }
        break;

        case WT4_ALARM_HELP:
        {
            TRACEDEBUG( "Wdg3Table::InitWidget() WT4_ALARM_HELP" );
        }
        break;

        default:
        break;
    }

    m_cmdItem.CmdType   = CT_READ;

    TRACEDEBUG( "Wdg3Table::InitWidget() m_wt<%d> screenID<%x>", m_wt, m_cmdItem.ScreenID );
}

void Wdg3Table::InitConnect()
{
    connect( &m_QTimerLight, SIGNAL(timeout()),
             this, SLOT(sltTimerHandler()) );
}

void Wdg3Table::Enter(void* param)
{
    TRACELOG1( "Wdg3Table::Enter(void* param) wt<%d>", m_wt );
    Q_UNUSED( param );

    INIT_VAR;
    m_nSigNum     = 0;

    switch (m_wt)
    {
        case WT3_ACT_ALARM:
        {
            if ( !ms_bEscapeFromHelp )
            {
                m_nPageIdx  = 1;
            }

            #ifdef TEST_GUI
                emptyItems();
                InitWidget();
                return;
            #endif
            static void* pData = NULL;
            TRACEDEBUG( "Wdg3Table::Enter ScreenID<%06x>", m_cmdItem.ScreenID );
            if ( !data_getDataSync(&m_cmdItem, &pData) )
            {
                if ( pData )
                {
                    m_CtrlLightInfo.iEquipID = -1;
                    m_CtrlLightInfo.bSendCmd = false;
                    ShowData( pData );
                    if (m_nSigNum > 0)
                    {
                        TRACEDEBUG( "Wdg3Table::Enter WT3_ACT_ALARM startTimer" );
                        m_QTimerLight.start( TIMER_UPDATE_DATA_INTERVAL/2 );
                        m_CtrlLightInfo.timeElapsed.restart();
                        m_timerId = startTimer( TIMER_UPDATE_DATA_INTERVAL );
                        this->setFocus();
                    }
                }
                else
                {
                    Leave();
                    TRACEDEBUG( "Wdg3Table::Enter WT3_ACT_ALARM goToBaseWindow( WT2_ACT_ALARM ) 1" );
                    emit goToBaseWindow( WT2_ACT_ALARM );
                }
            }
            else
            {
                //            TRACELOG1( "Wdg3Table::Enter ScreenID<%06x> no data", m_cmdItem.ScreenID );
                Leave();
                TRACEDEBUG( "Wdg3Table::Enter WT3_ACT_ALARM goToBaseWindow( WT2_ACT_ALARM ) 2" );
                emit goToBaseWindow( WT2_ACT_ALARM );
            }
        }
        break;

        case WT3_HIS_ALARM:
        {
            #ifdef TEST_GUI
                emptyItems();
                InitWidget();
                return;
            #endif
            m_nPageIdx  = 1;
            ENTER_GET_DATA;
            this->setFocus();
        }
        break;

        case WT4_ALARM_HELP:
        {
            emptyItems();
            QTableWidgetItem* item = NULL;
            int nLenStr = ms_strHelp.length();
            int nMaxCharsRow = g_cfgParam->ms_maxCharsPerTableRow;
            TRACELOG1( "Wdg3Table::Enter(void* param) wt<WT4_ALARM_HELP> nLenStr<%d> maxchar<%d>",
                   nLenStr, nMaxCharsRow );

            int rowIdx = 1; //0
            if (nLenStr < nMaxCharsRow)
            {
                item = ui->tableWidget->item(rowIdx, 0);
                item->setText(ms_strHelp );
            }
            else
            {
                int nWordCount = ms_strHelp.split( ' ' ).count();
                QString strSectionBack;
                QString strSectionFore;
                int idxStart = 0;
                for (int i=0; i<nWordCount; ++i)
                {
                    if (rowIdx >= TABLEWDG_ROWS_PERPAGE_NOTITLE-1)
                    {
                        strSectionBack = ms_strHelp.section( ' ', idxStart, -1 );
                        item = ui->tableWidget->item(rowIdx, 0);
                        item->setText(strSectionBack );
                        TRACELOG1( "Wdg3Table::Enter(void* param) wt<WT4_ALARM_HELP> rowIdx<%d> > 6",
                                   rowIdx );
                        break;
                    }

                    strSectionBack = ms_strHelp.section( ' ', idxStart, i );
                    strSectionFore = ms_strHelp.section( ' ', idxStart, i+1 );
                    TRACEDEBUG( "Wdg3Table::Enter(void* param) i<%d> nWordCount<%d> strSectionBack<%s> strSectionFore<%s>",
                                i,
                                nWordCount,
                                strSectionBack.toUtf8().constData(),
                                strSectionFore.toUtf8().constData()
                                );
                    TRACEDEBUG( "Wdg3Table::Enter string width<%d>", fmLargeN->width(strSectionBack) );
                    if (fmLargeN->width(strSectionFore) >= PosTableWidget::widthWords ||
                            i>=nWordCount-1)
                    {
                        TRACELOG1( "Wdg3Table::Enter(void* param) wt<WT4_ALARM_HELP> section<%s> idxRow<%d>",
                                   strSectionBack.toUtf8().constData(),
                                   rowIdx );
                        item = ui->tableWidget->item(rowIdx, 0);
                        item->setText(strSectionBack );
                        ++rowIdx;
                        idxStart = i+1;
                    }
                }
            }
        }
        break;

        default:
        break;
    }
}

void Wdg3Table::Leave()
{
    LEAVE_WDG( "Wdg3Table" );

    if ( m_QTimerLight.isActive() )
    {
        m_QTimerLight.stop();
    }
#ifdef TEST_GUI
    return;
#endif

    if (m_wt == WT3_ACT_ALARM)
    {
        sendCmdCtrlLight(
                    m_CtrlLightInfo.screenID,
                    -1,
                    MODULE_LIGHT_TYPE_ON,
                    m_CtrlLightInfo.moduleType
                    );
    }
    this->deleteLater ();
}

void Wdg3Table::Refresh()
{
    if (WT4_ALARM_HELP != m_wt)
    {
        //TRACEDEBUG( "Wdg3Table::Refresh g_bSendCmd<%d>", g_bSendCmd );
        REFRESH_DATA_PERIODICALLY(
                    m_cmdItem.ScreenID,
                    "Wdg3Table"
                    );
    }
}

void Wdg3Table::ShowData(void* pData)
{
    if ( !pData )
    {
        TRACEDEBUG( "Wdg3Table::ShowData(void* pData) pData==NULL" );
        return;
    }

    switch (m_wt)
    {
        case WT3_ACT_ALARM:
        {
            if (pData != g_dataBuff)
            {
                memcpy( g_dataBuff, pData, sizeof(PACK_ACTALM) );
            }
            //TRACEDEBUG( "Wdg3Table::ShowData setActAlarmItem g_dataBuff<%x>", g_dataBuff );
            setActAlarmItem( g_dataBuff );
        }
        break;

        case WT3_HIS_ALARM:
        {
            if (pData != g_dataBuff)
            {
                memcpy( g_dataBuff, pData, sizeof(PACK_HISALM) );
            }
            //TRACEDEBUG("Wdg3Table::ShowData setHisAlarmItem g_dataBuff<%x>", g_dataBuff);
            setHisAlarmItem( g_dataBuff );
        }
        break;

        default:
        break;
    }
}

void Wdg3Table::setActAlarmItem(void *pData)
{
    PACK_ACTALM* info = (PACK_ACTALM*)pData;
    m_nSigNum = info->ActAlmNum;
    if (m_nSigNum < 1)
    {
        TRACELOG1( "Wdg3Table::setActAlarmItem() AlmNum<%d> < 1", m_nSigNum );
        Leave();
        emit goToBaseWindow( WT2_ACT_ALARM );
        return;
    }

    if (m_nSigNum > MAXNUM_ACTALM)
    {
        TRACELOG1( "Wdg3Table::setActAlarmItem() AlmNum<%d> > 200", m_nSigNum );
        m_nSigNum = MAXNUM_ACTALM;
    }
    emptyItems();
    if (m_nPageIdx < 1)
    {
        m_nPageIdx = m_nSigNum;
    }
    if (m_nPageIdx > m_nSigNum)
    {
        m_nPageIdx = 1;
    }
    SET_STYLE_SCROOLBAR(m_nSigNum, m_nPageIdx);
    TRACEDEBUG( "Wdg3Table::setActAlarmItem() AlmNum<%d> m_nPageIdx<%d>", m_nSigNum, m_nPageIdx );

    ActAlmInfo actAlarmItem =
            info->ActAlmItem[m_nPageIdx-1];

    QTableWidgetItem* item = NULL;
    item = ui->tableWidget->item(0, 0);
    if ( qstrlen(actAlarmItem.AlmHelp)>0 )
    {
        ms_bHasActHelp   = true;
        ms_strHelp    = actAlarmItem.AlmHelp;
        item->setText(QString::number(m_nPageIdx)+"/"+
                       QString::number(m_nSigNum)+
                       SPACE_HELP );
    }
    else
    {
        ms_bHasActHelp   = false;
        ms_strHelp.clear();
        item->setText(QString::number(m_nPageIdx)+"/"+
                       QString::number(m_nSigNum)
                       );
    }
    item->setFont(g_cfgParam->gFontLargeN);

    item = ui->tableWidget->item(1, 0);
    item->setText(actAlarmItem.EquipName );
    item->setFont(g_cfgParam->gFontLargeN);

    item = ui->tableWidget->item(2, 0);
    item->setText(actAlarmItem.AlmName );
    item->setFont(g_cfgParam->gFontLargeN);

    TRACEDEBUG( "Wdg3Table::setActAlarmItem() alarm level<%d>", actAlarmItem.AlmLevel );
    item = ui->tableWidget->item(3, 0);
    if (actAlarmItem.AlmLevel>=0 && actAlarmItem.AlmLevel<=4)
    {
        item->setText(ms_strAlarmLevel[actAlarmItem.AlmLevel] );
        item->setFont(g_cfgParam->gFontLargeN);
    }
    item = ui->tableWidget->item(4, 0);
    QString strFormatDateTime =
            QString(g_szTimeFormat[
                    g_cfgParam->ms_initParam.timeFormat]) +
            " " +FORMAT_TIEM;
    QDateTime dateTime = QDateTime::fromTime_t( actAlarmItem.StartTime );
    QString strDateTime = dateTime.toString( strFormatDateTime );
    item->setText(strDateTime );
    item->setFont(g_cfgParam->gFontLargeN);

    m_CtrlLightInfo.screenID   = m_cmdItem.ScreenID;
    m_CtrlLightInfo.iEqIDType  = actAlarmItem.iEqIDType;
    m_CtrlLightInfo.iSigID     = actAlarmItem.iSigID;;
    m_CtrlLightInfo.moduleType = MODULE_TYPE(
                getModuleType(actAlarmItem.iEqIDType) );
    TRACEDEBUG( "Wdg3Table::setActAlarmItem moduleType<%d> m_nPageIdx<%d> equipID<%d> oldEquipID<%d>",
                m_CtrlLightInfo.moduleType,                m_nPageIdx, actAlarmItem.iEquipID, m_CtrlLightInfo.iEquipID );
    if (m_CtrlLightInfo.iEquipID != actAlarmItem.iEquipID)
    {
        m_CtrlLightInfo.iEquipID = actAlarmItem.iEquipID;
        m_CtrlLightInfo.bSendCmd = true;
        m_CtrlLightInfo.timeElapsed.restart();
    }
}

void Wdg3Table::setHisAlarmItem(void *pData)
{
    emptyItems();
    PackHisAlmInfo* info = (PackHisAlmInfo*)pData;
    m_nSigNum = info->HisAlmNum;
    QTableWidgetItem* item = NULL;
    if (m_nSigNum < 1)
    {
        item = ui->tableWidget->item(2, 0);
        item->setTextAlignment( Qt::AlignCenter );
        item->setText( tr("No Data") );
        item->setFont(g_cfgParam->gFontLargeN);
//        TRACELOG1( "Wdg3Table::setHisAlarmItem() AlmNum<%d> < 1", m_nSigNum );
        return;
    }
    else if (m_nSigNum > MAXNUM_HISALM)
    {
//        TRACELOG1( "Wdg3Table::setHisAlarmItem() AlmNum<%d> > 500", m_nSigNum );
        m_nSigNum = MAXNUM_HISALM;
    }

    //emptyItems();
    if (m_nPageIdx < 1)
    {
        m_nPageIdx = m_nSigNum;
    }
    else if (m_nPageIdx > m_nSigNum)
    {
        m_nPageIdx = 1;
    }
    SET_STYLE_SCROOLBAR(m_nSigNum, m_nPageIdx);

    //TRACEDEBUG( "Wdg3Table::setHisAlarmItem() AlmNum<%d> m_nPageIdx<%d>", m_nSigNum, m_nPageIdx );
    HisAlmInfo hisAlarmItem =
            info->HisAlmItem[m_nPageIdx-1];
    item = ui->tableWidget->item(0, 0);
    item->setTextAlignment( Qt::AlignLeft );
    item->setText( QString::number(m_nPageIdx)+"/"+
                   QString::number(m_nSigNum)
                   );
    item->setFont(g_cfgParam->gFontLargeN);

    // His Alarm hasn't help item
    item = ui->tableWidget->item(1, 0);
    item->setText(hisAlarmItem.EquipName );
    item->setFont(g_cfgParam->gFontLargeN);

    item = ui->tableWidget->item(2, 0);
    item->setText( hisAlarmItem.AlmName );
    item->setFont(g_cfgParam->gFontLargeN);

    item = ui->tableWidget->item(3, 0);
    item->setText( ms_strAlarmLevel[hisAlarmItem.AlmLevel] );
    item->setFont(g_cfgParam->gFontLargeN);

    item = ui->tableWidget->item(4, 0);
    QString strFormatDateTime =
            QString(g_szTimeFormat[
                    g_cfgParam->ms_initParam.timeFormat]) +
            " " +FORMAT_TIEM;
    QDateTime dateTime = QDateTime::fromTime_t( hisAlarmItem.StartTime );
    QString strDateTime = dateTime.toString( strFormatDateTime );
    item->setText( strDateTime );
    item->setFont(g_cfgParam->gFontLargeN);

    item = ui->tableWidget->item(5, 0);
    dateTime = QDateTime::fromTime_t( hisAlarmItem.EndTime );
    strDateTime = dateTime.toString( strFormatDateTime );
    item->setText( strDateTime );
    item->setFont(g_cfgParam->gFontLargeN);
}

void Wdg3Table::emptyItems()
{
    QTableWidgetItem* item = NULL;
    for (int i=0; i<TABLEWDG_ROWS_PERPAGE_NOTITLE; ++i)
    {
        item = ui->tableWidget->item(i, 0);
        item->setTextAlignment( Qt::AlignLeft );
        item->setText( "" );
    }
}

void Wdg3Table::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
}

void Wdg3Table::sltTimerHandler()
{
    if ( m_CtrlLightInfo.bSendCmd &&
         m_CtrlLightInfo.timeElapsed.elapsed()>
         TIME_ELAPSED_CTRL_LIGHT )
    {
        sendCmdCtrlLight( m_CtrlLightInfo.screenID,
                          m_CtrlLightInfo.iEquipID,
                          MODULE_LIGHT_TYPE_GREEN_FLASH,
                          m_CtrlLightInfo.moduleType
                          );
        m_CtrlLightInfo.bSendCmd = false;
        m_CtrlLightInfo.timeElapsed.restart();
    }
}

void Wdg3Table::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
        ms_strAlarmLevel[0] = tr("OA Alarm");
        ms_strAlarmLevel[1] = tr("OA Alarm");
        ms_strAlarmLevel[2] = tr("MA Alarm");
        ms_strAlarmLevel[3] = tr("CA Alarm");
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void Wdg3Table::keyPressEvent(QKeyEvent* keyEvent)
{
    TRACEDEBUG( "Wdg3Table::keyPressEvent" );

    switch ( keyEvent->key() )
    {
    case Qt::Key_Return:
    case Qt::Key_Enter:
    {
        g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
        //TRACELOG1( "Wdg3Table::keyPressEvent Key_Enter" );
        switch (m_wt)
        {
        case WT3_ACT_ALARM:
        {
            if (ms_bHasActHelp)
            {
                emit goToBaseWindow( WT4_ALARM_HELP );
            }
        }
        break;

        default:
            break;
        }
    }
        break;

    case Qt::Key_Escape:
    {
        g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ESC;
        TRACEDEBUG( "Wdg3Table::keyPressEvent Key_Escape g_nInAlarmType<%d> m_wt<%d>", g_nInAlarmType, m_wt );
        ms_bEscapeFromHelp = false;
        switch (m_wt)
        {
        case WT3_ACT_ALARM:
        {
            TRACEDEBUG( "Wdg3Table::keyPressEvent Key_Escape WT3_ACT_ALARM 1" );

            if (g_nInAlarmType==IN_ALARM_TYPE_SCREENSAVER)
            {
                TRACEDEBUG( "Wdg3Table::keyPressEvent Key_Escape IN_ALARM_TYPE_SCREENSAVER emit goToHomePage();" );
                emit goToHomePage();
            }
            else if (g_nInAlarmType==IN_ALARM_TYPE_NOKEYPRESS &&
                     g_nLastWTNokey!=WT3_ACT_ALARM &&
                     g_nLastWTNokey!=WT4_ALARM_HELP)
            {
                TRACEDEBUG( "Wdg3Table::keyPressEvent Key_Escape WT3_ACT_ALARM 2" );
                if (g_nLastWTNokey == WT_HOME_WINDOW)
                {
                    emit goToHomePage();
                }
                else
                {
                    emit goToBaseWindow( WIDGET_TYPE(g_nLastWTNokey) );
                }
            }
            else
            {
                TRACEDEBUG( "Wdg3Table::keyPressEvent Key_Escape WT3_ACT_ALARM" );
                emit goToBaseWindow( WT2_ACT_ALARM );
            }
            g_nInAlarmType = IN_ALARM_TYPE_KEYPRESS;
            TRACEDEBUG( "Wdg3Table::keyPressEvent Key_Escape 2 g_nInAlarmType<%d>", g_nInAlarmType );
        }
            return;

        case WT3_HIS_ALARM:
        {
            emptyItems();
            emit goToBaseWindow( WT1_ALARM );
            //emit goToHomePage();
        }
            return;

        case WT4_ALARM_HELP:
            ms_bEscapeFromHelp = true;
            emit goToBaseWindow( WT3_ACT_ALARM );
            break;

        default:
            break;
        }
    }
        break;

    case Qt::Key_Up:
        --m_nPageIdx;
        ShowData( g_dataBuff );
        break;

    case Qt::Key_Down:
        ++m_nPageIdx;
        ShowData( g_dataBuff );
        break;

    default:
        break;
    }
}

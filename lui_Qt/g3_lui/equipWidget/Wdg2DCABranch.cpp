/******************************************************************************
文件名：    Wdg2DCABranch.cpp
功能：      第二层界面p2 DC 电流负载条形图
作者：      刘金煌
创建日期：   2013年5月2日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "Wdg2DCABranch.h"
#include "ui_Wdg2DCABranch.h"

#include <QPainter>
#include <QKeyEvent>
#include "config/PosBarChart.h"
#include "config/configparam.h"
#include "common/pubInclude.h"
#include "common/MultiDemon.h"
#include "common/global.h"

using namespace BarChart;

Wdg2DCABranch::Wdg2DCABranch(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::Wdg2DCABranch)
{
    ui->setupUi(this);

    InitWidget();
    InitConnect();

    m_timerId = 0;

    m_cmdItem.CmdType   = CT_READ;
    m_cmdItem.ScreenID  = SCREEN_ID_Wdg2DCABranch;
}

Wdg2DCABranch::~Wdg2DCABranch()
{
    TRACEDEBUG("Wdg2DCABranch::~Wdg2DCABranch");
    delete ui;
}

void Wdg2DCABranch::InitWidget()
{
    SET_GEOMETRY_WIDGET( this );
    SET_BACKGROUND_WIDGET( "Wdg2DCABranch",PosBase::strImgBack_Title );
    SET_GEOMETRY_LABEL_TITLE( ui->label_title );
    SET_STYLE_SCROOLBAR( 1, 1 );

#ifdef TEST_GUI
    PACK_LOADINFO info;
    info.LoadNum = 1;
    for (int i=0; i<info.LoadNum; ++i)
    {
        strcpy(info.LoadInfo[i].cEqName, "EBI");
        for (int j=0; j<MAXLEN_SIG; ++j)
        {
            info.LoadInfo[i].iSigNum[j] = j+1;
            info.LoadInfo[i].vValue[j].fValue = (j+1)*10;
            TRACEDEBUG( "Wdg2DCABranch::InitWidget() i<%d> <%f>", i, info.LoadInfo[i].vValue[i].fValue );
        }
        info.LoadInfo[i].iRespond = 0;
        info.LoadInfo[i].iFormat = 1;
    }
    ShowData( &info );
#endif
}

void Wdg2DCABranch::InitConnect()
{
}


void Wdg2DCABranch::Enter(void* param)
{
    TRACELOG1( "Wdg2DCABranch::Enter(void* param)" );
    Q_UNUSED(param);

    g_nMaxItemPerpage = PosBarChart::nItemsLess;
    g_nMaxXMarks      = SCALES_X_BARCHART3;
    PosBarChart::setBarChartType( &g_nMaxItemPerpage );
    m_nPageIdx = 1;

    INIT_VAR;

#ifdef TEST_GUI
    ENTER_FIRSTLY;
#else
    m_nPages   = 0;
    ENTER_GET_DATA;
#endif
    g_bEnterFirstly = false;
}

void Wdg2DCABranch::Leave()
{    
    LEAVE_WDG( "Wdg2DCABranch" );
    this->deleteLater();//this->distconnect(); delete this;
}

void Wdg2DCABranch::Refresh()
{
    if (g_timeElapsedKeyPress.elapsed() >
            TIME_ELAPSED_KEYPRESS ||
            g_bEnterFirstly)
    {
        REFRESH_DATA_PERIODICALLY(
                    m_cmdItem.ScreenID,
                    "Wdg2DCABranch"
                    );
    }
}

void Wdg2DCABranch::ShowData(void* pData)
{
    if ( !pData )
    {
        return;
    }

    if (pData != g_dataBuff)
    {
        memcpy( g_dataBuff, pData, sizeof(PACK_LOADINFO) );
    }
    PACK_LOADINFO* info = (PACK_LOADINFO*)g_dataBuff;

    m_nPages = info->LoadNum;
    TRACEDEBUG( "Wdg2DCABranch::ShowData m_nPages(%d)", m_nPages );
    if (m_nPages<=0 || m_nPages>MAXNUM_BRANCH)
    {
        m_nPageIdx = 0;

        QString strTitle = tr("Load");
        ui->label_title->setText(strTitle + " (A)");

        update();
        return;
    }

    if (m_nPageIdx < 1)
    {
        m_nPageIdx = m_nPages;
    }
    else if (m_nPageIdx > m_nPages)
    {
        m_nPageIdx = 1;
    }

    LOAD_INFO* pLoadInfo   = &(info->LoadInfo[m_nPageIdx-1]);
    iFormat = pLoadInfo->iFormat;
    //TRACEDEBUG()
    for (int i=0; i<m_nPages; ++i)
    {
        m_bResponds[i]  = (pLoadInfo->iRespond==0);
    }

    QString strTitle(pLoadInfo->cEqName);
    ui->label_title->setText(strTitle + " (A)");
    TRACEDEBUG( "Wdg2DCABranch::ShowData title<%s>", pLoadInfo->cEqName );
    TRACEDEBUG("iEquipID %d",pLoadInfo->iEquipID);
    for (int i=0; i<MAXLEN_SIG; ++i)
    {
        m_sigVals[i]   = pLoadInfo->vValue[i].fValue;
        m_branchNos[i] = pLoadInfo->iSigNum[i];//注意，如果没有的，用0来算
        TRACEDEBUG( "Wdg2DCABranch::ShowData m_sigVals[%d] <%f> No<%d>", i, m_sigVals[i], pLoadInfo->iSigNum[i] );
    }

    m_nMaxXval = 0;
    for (int i=0; i<m_nPages; ++i)
    {
        pLoadInfo   = &(info->LoadInfo[i]);
        for (int j=0; j<MAXLEN_SIG; ++j)
        {
            if (pLoadInfo->iSigNum[j] > 0)
            {
                if (pLoadInfo->vValue[j].fValue > m_nMaxXval)
                {
                    m_nMaxXval = qRound(pLoadInfo->vValue[j].fValue);
                    TRACEDEBUG("m_nMaxXval:%d",m_nMaxXval);
                }
            }
        }
    }

    if (m_nMaxXval <= 100)
    {
        m_nMaxXval = 100;
    }
    else if (m_nMaxXval <= 300)
    {
        m_nMaxXval = 300;
    }
    else if (m_nMaxXval <= 500)
    {
        m_nMaxXval = 500;
    }
    else if (m_nMaxXval <= 1000)
    {
        m_nMaxXval = 1000;
    }
    else
    {
        m_nMaxXval = 2000;
    }

    TRACEDEBUG( "Wdg2DCABranch::ShowData 2 m_nMaxXval<%d>", m_nMaxXval );

    SET_STYLE_SCROOLBAR( m_nPages, m_nPageIdx );
    TRACEDEBUG("m_nPages:%dm_nPageIdx:%d",m_nPages,m_nPageIdx);
    update();
}

void Wdg2DCABranch::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
}

void Wdg2DCABranch::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void Wdg2DCABranch::keyPressEvent(QKeyEvent* keyEvent)
{
    TRACEDEBUG( "Wdg2DCABranch::keyPressEvent" );
    g_timeElapsedKeyPress.restart();
    g_bSendCmd = true;
    switch ( keyEvent->key() )
    {
        case Qt::Key_Escape:
        {
            emit goToBaseWindow(WT1_DCA);
        }
        break;

        case Qt::Key_Down:
        {
            ++m_nPageIdx;
            ShowData( g_dataBuff );
        }
        break;

        case Qt::Key_Up:
        {
            --m_nPageIdx;
            ShowData( g_dataBuff );
        }
        break;

        default:
        break;
    }
}

void Wdg2DCABranch::paintEvent(QPaintEvent*)
{
    //样式表支持
    QStyleOption opt;
    opt.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);
    int nItemHeight = (PosBarChart::vAxisLen-PosBarChart::nItemHeight)/MAXLEN_SIG;

    QPainter* pPainter = &painter;
    float* sigVals = m_sigVals;
    int nPageIdx = m_nPageIdx;
    int nMaxXval = m_nMaxXval;

    int idxBranch   = 1;
    pPainter->setFont( g_cfgParam->gFontSmallN );
    // 原点
    pPainter->translate(
                PosBarChart::originX,
                PosBarChart::originY
                );

    // draw no data text
    if (nPageIdx < 1)
    {
        //TRACEDEBUG( "Wdg2DCABranch::paintEvent nPageIdx < 1" );
        QString strNoData = QObject::tr("No Data");
        pPainter->translate(
                    -PosBarChart::originX,
                    -PosBarChart::originY
                    );
        pPainter->setPen( nodataColor );
        pPainter->drawText(
                    PosBase::screenWidth/2-fmSmallN->width(strNoData)/2,
                    PosBase::screenHeight/2,
                    strNoData
                    );
        return;
    }

    //画竖线
    pPainter->setPen( textNormalColor );
    pPainter->setBrush( coordAxisColor );
    for (idxBranch=0; idxBranch<=g_nMaxXMarks; ++idxBranch)
    {
        pPainter->drawLine(
                    QPointF(SCALELENX_BARCHART*idxBranch, 0),
                    QPointF(SCALELENX_BARCHART*idxBranch,-PosBarChart::vAxisLen
                        )
                    );
    }

    // 竖线下端的文字
    pPainter->setPen( textNormalColor );
    QString strX;
    for (idxBranch=0; idxBranch<=g_nMaxXMarks; ++idxBranch)
    {
        if (idxBranch%2 == 0)
        {
            strX = QString::number(
                        nMaxXval/g_nMaxXMarks*idxBranch
                        );
            pPainter->drawText(
                        (int)SCALELENX_BARCHART*idxBranch-(fmSmallN->width(strX)/2),
                        PosBase::hAxisTextY,
                        strX
                        );
        }
    }


    // 画条形图 写信号值文字 Y刻度"Bn"
    float fSigVal   = 0;
    for (idxBranch=MAXLEN_SIG; idxBranch>0; --idxBranch)
    {
        // 写字 信号值 0不显示
        if(m_branchNos[MAXLEN_SIG-idxBranch] != 0)
        {
            float ptYText = -nItemHeight*idxBranch;
            fSigVal = sigVals[MAXLEN_SIG-idxBranch];
            if (fSigVal > 0)
            {
                QRectF rectBar(
                            -5,
                            ptYText,
                            calcPointX_BARCHART3(fSigVal)+5,
                            nItemHeight-PosBarChart::nItemHeight
                            );
                QPen pen;
                // 画条形图
                if ( m_bResponds[nPageIdx-1] )
                {
                    pen.setColor(textNormalColor);
                    pen.setWidth(1);
                    pPainter->setPen( pen );
                    pPainter->setBrush( barColor );
                    pPainter->drawRect( rectBar );
                }
                else
                {
                    pen.setColor(textNormalColor);
                    pen.setWidth(1);
                    pPainter->setPen( pen );
                    pPainter->setBrush( chartNotColor );
                    pPainter->drawRect( rectBar );
                }
            }

            // 写字 Bn、信号值
            QString strItem = "B"+QString::number(m_branchNos[MAXLEN_SIG-idxBranch]);

            //计算Bn的Y轴坐标，也是信号值的Y轴坐标
            int nFontHeight = g_cfgParam->ms_pFmSmallN->height();
            int nValueY = ptYText + (nItemHeight-PosBarChart::nItemHeight - nFontHeight)/2 +
                g_cfgParam->ms_pFmSmallN->ascent ();

            // 信号值竖直居中
            //计算x轴坐标，Y轴使用Bn的Y值
            QString strSigVal = strItem + ": " + QString::number(fSigVal, FORMAT_DECIMAL);

            int nValueX = PosBarChart::iText_Gap;
            pPainter->setPen( textNormalColor );
            pPainter->drawText(
                        nValueX,
                        nValueY,
                        strSigVal
                        );

        }
    }
}

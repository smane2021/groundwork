/******************************************************************************
文件名：    Wdg2DCDeg1Branch.cpp
功能：      第二层界面p2 DC分路环境温度图形 条形图
作者：      刘金煌
创建日期：   2013年5月10日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "Wdg2DCDeg1Branch.h"
#include "ui_Wdg2DCDeg1Branch.h"

#include <QPainter>
#include <QKeyEvent>
#include "common/pubInclude.h"
#include "common/global.h"
#include "config/configparam.h"
#include "config/PosBarChart.h"

Wdg2DCDeg1Branch::Wdg2DCDeg1Branch(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::Wdg2DCDeg1Branch)
{
    ui->setupUi(this);

    InitWidget();
    InitConnect();

    m_timerId = 0;
    m_cmdItem.CmdType   = CT_READ;
    m_cmdItem.ScreenID  = SCREEN_ID_Wdg2DCDeg1Branch;
}

Wdg2DCDeg1Branch::~Wdg2DCDeg1Branch()
{
    TRACEDEBUG("Wdg2DCDeg1Branch::~Wdg2DCDeg1Branch");
    delete ui;
}

void Wdg2DCDeg1Branch::InitWidget()
{
    SET_BACKGROUND_WIDGET( "Wdg2DCDeg1Branch",PosBase::strImgBack_Title );
    SET_GEOMETRY_LABEL_TITLE( ui->label_title );

#ifdef TEST_GUI
    PACK_GRPINFO info;
    info.DataNum = 110;
    for (int i=0; i<info.DataNum; ++i)
    {
        info.DataInfo[i].iFormat = 1;

        info.DataInfo[i].iRespond = 0;
        info.DataInfo[i].vValue.fValue = 50;
        info.DataInfo[i].iIndex = i+1;
        info.DataInfo[i].vaLevel[0].fValue = -20;
        info.DataInfo[i].vaLevel[1].fValue = 50;
        info.DataInfo[i].vaLevel[2].fValue = 70;
    }

    int n = 0;
    info.DataInfo[n].iRespond = 0;
    info.DataInfo[n].vValue.fValue = -15;
    info.DataInfo[n].iIndex = 50;
    info.DataInfo[n].vaLevel[0].fValue = -10;

    ++n;
    info.DataInfo[n].iRespond = 0;
    info.DataInfo[n].vValue.fValue = 75;
    info.DataInfo[n].iIndex = 51;
    info.DataInfo[n].vaLevel[1].fValue = 60;

    ++n;
    info.DataInfo[n].iRespond = 1;
    info.DataInfo[n].vValue.fValue = 50;
    info.DataInfo[n].iIndex = 52;
    info.DataInfo[n].vaLevel[1].fValue = 40;

    ++n;
    info.DataInfo[n].iRespond = 1;
    info.DataInfo[n].vValue.fValue = 25;

    ++n;
    info.DataInfo[n].iRespond = 0;
    info.DataInfo[n].vValue.fValue = 100;

    // 第二页
    ++n;
    info.DataInfo[n].iRespond = 0;
    info.DataInfo[n].vValue.fValue = 75;

    ++n;
    info.DataInfo[n].iRespond = 0;
    info.DataInfo[n].vValue.fValue = 50;
    info.DataInfo[n].vaLevel[0].fValue = 40;

    ++n;
    info.DataInfo[n].iRespond = 0;
    info.DataInfo[n].vValue.fValue = 25;

    ++n;
    info.DataInfo[n].iRespond = 0;
    info.DataInfo[n].vValue.fValue = 100;

    ++n;
    info.DataInfo[n].iRespond = 1;
    info.DataInfo[n].vValue.fValue = 25;

    ShowData( &info );
#endif
}

void Wdg2DCDeg1Branch::InitConnect()
{
}


void Wdg2DCDeg1Branch::Enter(void* param)
{
    TRACELOG1( "Wdg2DCDeg1Branch::Enter(void* param)" );
    Q_UNUSED(param);

    g_nMaxItemPerpage = PosBarChart::nItemsLess;
    g_nMaxXMarks      = SCALES_X_BARCHART;
    PosBarChart::setBarChartType( &g_nMaxItemPerpage );
    m_nPageIdx = 1;
    ms_showingWdg = this;

#ifdef TEST_GUI
    ENTER_FIRSTLY;
#else
    m_nPages   = 0;
    m_eTempFormat = GetTempFormat();
    QString strTitle = tr("Temp");
    if(m_eTempFormat == TEMP_FORMAT_TYPE_CELS)
    {
        ui->label_title->setText(strTitle + " (deg.C)");
    }
    else
    {
        ui->label_title->setText(strTitle + " (deg.F)");
    }
    INIT_VAR;
    ENTER_GET_DATA;
#endif
}

void Wdg2DCDeg1Branch::Leave()
{
    LEAVE_WDG( "Wdg2DCDeg1Branch" );
    this->deleteLater();
}

void Wdg2DCDeg1Branch::Refresh()
{  
    m_eTempFormat = GetTempFormat();

    REFRESH_DATA_PERIODICALLY(
                m_cmdItem.ScreenID,
                "Wdg2DCDeg1Branch"
                );
}

void Wdg2DCDeg1Branch::ShowData(void* pData)
{
    if ( !pData )
    {
        return;
    }

    if (pData != g_dataBuff)
    {
        memcpy( g_dataBuff, pData, sizeof(PACK_GRPINFO) );
    }
    PACK_GRPINFO* info = (PACK_GRPINFO*)g_dataBuff;
    int nSigNum   = info->DataNum;
    TRACEDEBUG( "Wdg2DCDeg1Branch::ShowData nSigNum(%d)", nSigNum );
    if (nSigNum <= 0)
    {
        m_nPageIdx = 0;
        update();
        return;
    }

//    for (int i=0; i<nSigNum; ++i)
//    {
//        TRACEDEBUG( "Wdg2DCDeg1Branch::ShowData i<%d> iRespond<%d> iFormat<%d> value<%f>",
//                    i,
//                    info->DataInfo[i].iRespond,
//                    info->DataInfo[i].iFormat,
//                    info->DataInfo[i].vValue.fValue
//                    );
//    }

    int nModNum   = nSigNum%g_nMaxItemPerpage;
    m_nPages = ( nModNum ? (nSigNum/g_nMaxItemPerpage+1):
               (nSigNum/g_nMaxItemPerpage) );
    if (m_nPageIdx < 1)
    {
        m_nPageIdx = m_nPages;
    }
    if (m_nPageIdx > m_nPages)
    {
        m_nPageIdx = 1;
    }

    if (m_nPageIdx==m_nPages && nModNum)
    {
        m_nShowMaxItemCurPage = nModNum;
    }
    else
    {
        m_nShowMaxItemCurPage = g_nMaxItemPerpage;
    }
    TRACEDEBUG( "Wdg2DCDeg1Branch::ShowData(void* pData) nSigNum<%d> pages<%d> PageIdx<%d> ItemsCurPage<%d>",
                nSigNum,
                m_nPages,
                m_nPageIdx,
                m_nShowMaxItemCurPage
                );

    iFormat = info->DataInfo[0].iFormat;
    for(int i=0; i<m_nShowMaxItemCurPage; ++i)
    {
        m_sigVals[i] = info->DataInfo[(m_nPageIdx-1)*
                            g_nMaxItemPerpage+i].
                            vValue.fValue;
        m_bResponds[i] = (info->DataInfo[(m_nPageIdx-1)*
                            g_nMaxItemPerpage+i].iRespond == 0);
        m_sigIndexs[i] = info->DataInfo[(m_nPageIdx-1)*
                            g_nMaxItemPerpage+i].iIndex;
        for (int j=0; j<MAXNUM_NORLEVEL; ++j)
        {
            m_fWarnSigVals[i][j] = info->DataInfo[(m_nPageIdx-1)*
                    g_nMaxItemPerpage+i].vaLevel[j].fValue;
        }
        TRACEDEBUG( "     <%d> sigVal<%f> respond<%d> m_sigIndexs<%d>"
                    "warn sigVal<%f %f %f>",
                    i,
                    m_sigVals[i],
                    m_bResponds[i],
                    m_sigIndexs[i],
                    m_fWarnSigVals[i][0],
                    m_fWarnSigVals[i][1],
                    m_fWarnSigVals[i][2]
                    );
    }

    SET_STYLE_SCROOLBAR( m_nPages, m_nPageIdx );
    update();
}

void Wdg2DCDeg1Branch::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
}

void Wdg2DCDeg1Branch::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void Wdg2DCDeg1Branch::keyPressEvent(QKeyEvent* keyEvent)
{
    switch ( keyEvent->key() )
    {
    case Qt::Key_Escape:
        emit goToBaseWindow( WT1_DCDEG_METER );
        break;

    case Qt::Key_Down:
        ++m_nPageIdx;
        ShowData( g_dataBuff );
        break;

    case Qt::Key_Up:
        --m_nPageIdx;
        ShowData( g_dataBuff );
        break;

    default:
        break;
    }
}

//Wdg2P6BattDeg
void Wdg2DCDeg1Branch::paintEvent(QPaintEvent*)
{
    //样式表支持
    QStyleOption opt;
    opt.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);

    QPainter* pPainter = &painter;

    TRACEDEBUG("[paintEvent]m_eTempFormat:%d",(int)(m_eTempFormat));

    barChart_paint(
                pPainter,
                iFormat,
                m_sigVals,
                m_bResponds,
                m_sigIndexs,
                m_nShowMaxItemCurPage,
                m_nPageIdx,
                m_fWarnSigVals,
                m_eTempFormat
                );
}

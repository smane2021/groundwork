#ifndef WDG3TABLE_H
#define WDG3TABLE_H

#include <QTimer>
#include "common/basicwidget.h"
#include "common/uidefine.h"
#include "common/MultiDemon.h"
using namespace CtrlLight;

namespace Ui {
class Wdg3Table;
}

class Wdg3Table : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit Wdg3Table(enum WIDGET_TYPE wt, QWidget *parent = 0);
    ~Wdg3Table();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();

protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);

private:
    void setActAlarmItem(void *pData);
    void setHisAlarmItem(void *pData);
    void emptyItems();
//    void setInvItem(void *pData);

private slots:
    // send cmd to ctrl lighting
    void sltTimerHandler(void);

signals:
    void goToHomePage();
    void goToBaseWindow(enum WIDGET_TYPE);

public:
    // 从help页面退出要显示到当前告警号
    static bool     ms_bEscapeFromHelp;

private:
    int m_timerId;
    int m_wt;
    int m_nPageIdx;
    int m_nSigNum;

    CmdItem  m_cmdItem;
    // help info
    static bool     ms_bHasActHelp;
    //static bool     ms_bHasHisHelp; //目前没有
    static QString  ms_strHelp;
    static QString  ms_strAlarmLevel[4];

    // 发控制命令定时器
    QTimer          m_QTimerLight;
    CtrlLightInfo_t m_CtrlLightInfo;

private:
    Ui::Wdg3Table *ui;
};

#endif // WDG3TABLE_H

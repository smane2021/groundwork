/******************************************************************************
文件名：    Wdg2P5BattRemainTime.h
功能：      第二层界面p5 各个电池剩余时间 电池条形图
作者：      刘金煌
创建日期：   2013年5月14日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef WDG2P5BATTREMAINTIME_H
#define WDG2P5BATTREMAINTIME_H

#include "common/basicwidget.h"
#include "common/uidefine.h"
#include "common/MultiDemon.h"

using namespace BarChart;

namespace Ui {
class Wdg2P5BattRemainTime;
}

#define MAX_ITEM_PERPAGE_BARCHART3   PosBarChart::nBattInfoNum

class Wdg2P5BattRemainTime : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit Wdg2P5BattRemainTime(QWidget *parent = 0);
    ~Wdg2P5BattRemainTime();
    
public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();

protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);
    virtual void paintEvent(QPaintEvent* event);

signals:
    void goToBaseWindow(enum WIDGET_TYPE);

private:
    int      m_timerId;
    CmdItem  m_cmdItem;
    // 缓存的battinfo
    BATT_INFO m_battInfoBuffed[MAX_ITEM_PERPAGE_BARCHART];
    QString   m_strEquipName[MAX_ITEM_PERPAGE_BARCHART];
    //float    m_sigVals[MAX_ITEM_PERPAGE_BARCHART];
    int m_nShowMaxItemCurPage; // 当前页显示几条数据
    int m_nPageIdx;
    int m_nPages;

    int     m_nBranchs; //支路数
    int     m_nSigNum;
    //BattRemainTimeVal   m_vals[MAXNUM_BATINFO];

private:
    Ui::Wdg2P5BattRemainTime *ui;
};

#endif // WDG2P5BATTREMAINTIME_H

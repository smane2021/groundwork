/******************************************************************************
文件名：    Wdg2Table.h
功能：      第二层界面 表格
           模块 0 1 WT2_RECTINFO WT2_SOLINFO
           告警 4 5 6 WT2_ACT_ALARM WT2_HIS_ALARM WT2_EVENT_LOG
作者：      刘金煌
创建日期：   2013年5月9日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef WDG2TABLE_H
#define WDG2TABLE_H

#include <QTime>
#include <QTimer>
#include "common/basicwidget.h"
#include "common/uidefine.h"
#include "common/buzztablewidget.h"
#include "common/MultiDemon.h"
using namespace CtrlLight;

#define MAX_SCREEN_ID_INV  SCREEN_ID_Wdg2Table_SolConv_Inv

//Frank Wu,20151103
#define MAX_RS485_INV_NUM   (66+32+16)//FiammBatt(32) + NaradaBMS(16)

namespace Ui {
class Wdg2Table;
}

class Wdg2Table : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit Wdg2Table(enum WIDGET_TYPE wt, QWidget *parent = 0);
    ~Wdg2Table();
    
public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();

protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    void testGUI();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);

private:
    void setModuleItem(void *pData);
    void setSourceInfoDataMem(void *pData);
    // set table header
    void setRectTableHeader();
    void setSolTableHeader();
    void setConvTableHeader();
    void setSlaveRectTableHeader();
    void setRectTableRow(int nRowCount);
    void setMPPTTableRow(int nRowCount);
    void setSourceInfoItem();

private slots:
    void sltTableKeyPress(int key);
    void sltScreenSaver();
    // send cmd to ctrl lighting
    void sltTimerHandler(void);

    void on_tableWidget_itemSelectionChanged();

signals:
    void goToHomePage();
    void goToBaseWindow(enum WIDGET_TYPE);

private:
    int m_wt;
    // 发控制命令定时器
    QTimer          m_QTimerLight;
    CtrlLightInfo_t m_CtrlLightInfo;

    QTime    m_timeElapsedKeyPress;

    int      m_timerId;
    CmdItem  m_cmdItem;
    void*    m_pData;
    int m_nPageIdx;
    int m_nPages;
    int m_nSigNum;
    bool m_bEnterFirstly;

    //Source infomation
    int m_nSourceInfo;
    PACK_INFO *m_pSourceInfo;

private:
    Ui::Wdg2Table *ui;
};

#endif // WDG2TABLE_H

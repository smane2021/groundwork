/******************************************************************************
文件名：    WdgFP5Deg2Curve.cpp
功能：      第一层界面p5 负载中第四个界面 温度曲线图
作者：      刘金煌
创建日期：   2013年5月2日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "WdgFP5Deg2Curve.h"
#include "ui_WdgFP5Deg2Curve.h"

#include <QPainter>
#include <QKeyEvent>
#include "config/configparam.h"
#include "config/PosCurve.h"
#include "common/pubInclude.h"
#include "common/global.h"

WdgFP5Deg2Curve::WdgFP5Deg2Curve(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::WdgFP5Deg2Curve)
{
    ui->setupUi(this);    

    InitWidget();
    InitConnect();

    m_timerId = 0;
    m_cmdItem.CmdType   = CT_READ;
    m_cmdItem.ScreenID  = SCREEN_ID_WdgFP5Deg2Curve;
}

WdgFP5Deg2Curve::~WdgFP5Deg2Curve()
{
    TRACEDEBUG("WdgFP5Deg2Curve::~WdgFP5Deg2Curve");
    delete ui;
}

void WdgFP5Deg2Curve::InitWidget()
{
#ifdef TEST_GUI
    time_t timeAxis = (time_t)(QDateTime::currentDateTime().
                           addDays(-7).toTime_t());

    PACK_TEMPTREND  packTempTrend;
    packTempTrend.iNum = 50;
    for(int i=0; i<packTempTrend.iNum; ++i)
    {
        if (i>20 && i<40)
        {
            packTempTrend.TempTrend[i].fTemp = -9999;
        }
        else
        {
            packTempTrend.TempTrend[i].fTemp = i%20;
        }
        packTempTrend.TempTrend[i].TimeTrend = timeAxis;
        timeAxis += SECONDS_2H;
    }

    packTempTrend.TempTrend[30].fTemp = -25;
//    packTempTrend.TempTrend[31].fTemp = 50;
    packTempTrend.TempTrend[32].fTemp = 0;
//    packTempTrend.TempTrend[33].fTemp = -25;
    packTempTrend.TempTrend[34].fTemp = 25;
//    packTempTrend.TempTrend[35].fTemp = 75;
    packTempTrend.TempTrend[36].fTemp = 50;
//    packTempTrend.TempTrend[37].fTemp = 75;
    packTempTrend.TempTrend[38].fTemp = 75;

    ShowData( &packTempTrend );
#endif

    SET_GEOMETRY_WIDGET( this );
    SET_STYLE_LABEL_CURVE_BOTTOM( ui->label_curveBottom );
    ui->label_curveBottom->setAlignment( Qt::AlignCenter );

    SET_GEOMETRY_LABEL_TITLE( ui->label_ambTempDeg );
    SET_BACKGROUND_WIDGET( "WdgFP5Deg2Curve",PosBase::strImgBack_None);
    SET_STYLE_SCROOLBAR( 4, 4 );
    ui->label_ambTempDeg->setStyleSheet( \
              "background-color:#" LIGHT_COLOR_ENTER ";" \
              "color: rgb(241,241,241);" \
              );
}

void WdgFP5Deg2Curve::InitConnect()
{
}


void WdgFP5Deg2Curve::Enter(void* param)
{
    TRACELOG1( "WdgFP5Deg2Curve::Enter(void* param)" );
    Q_UNUSED( param );

#ifdef TEST_GUI
    ENTER_FIRSTLY;
#else
    INIT_VAR;
    m_eTempDispFormat = GetTempFormat();
    m_eLastTempFormat = m_eTempDispFormat;
    TRY_GET_DATA_MULTY;
    m_timerId = startTimer( TIMER_UPDATE_DATA_INTERVAL_DEGCURVE );
    m_timerFreshTempFormat = startTimer(TIMER_UPDATE_DATA_INTERVAL);
#endif

    ui->label_curveBottom->setText( tr("Last 7 days") );

    QString strTitle = tr("Ambient Temp");
    if(m_eLastTempFormat == TEMP_FORMAT_TYPE_CELS)
    {
        strTitle = strTitle+" (deg.C)";
    }
    else
    {
        strTitle = strTitle+" (deg.F)";
    }

    if((g_cfgParam->ms_pFmLargeB->width (strTitle)+ g_cfgParam->ms_pFmLargeB->width ('N') * 2) > PosBase::titleWidth2)
    {
        SET_GEOMETRY_LABEL_TITLE2(ui->label_ambTempDeg);
        ui->label_ambTempDeg->setGeometry(
                    PosBase::titleX2,
                    PosBase::titleY2,
                    PosBase::titleWidth2,
                    PosBase::titleHeight2Rows);
        ui->label_ambTempDeg->setStyleSheet( \
                  "background-color:#" LIGHT_COLOR_ENTER ";" \
                  "color: rgb(241,241,241);" \
                  );
    }


    ui->label_ambTempDeg->setText( strTitle);
}

void WdgFP5Deg2Curve::Leave()
{
    LEAVE_WDG( "WdgFP5Deg2Curve" );
    this->deleteLater();
}

void WdgFP5Deg2Curve::Refresh()
{
    REFRESH_DATA_PERIODICALLY(
                m_cmdItem.ScreenID,
                "WdgFP5Deg2Curve::Refresh()"
                );
}

// WdgFP8BattDegCurve
void WdgFP5Deg2Curve::ShowData(void* pData)
{
    if ( !pData )
        return;

    PACK_TEMPTREND* info = (PACK_TEMPTREND*)pData;
    PACK_TEMPTREND* pTempParam = &(m_curveParam.tempData);
    m_curveParam.curveType   = CURVE_TYPE_TEMP;
    m_curveParam.nSigNum     = (info->iNum>MAXNUM_TREND ?
                                    MAXNUM_TREND:info->iNum);
    m_curveParam.iFormat     = 1;
    m_curveParam.idxMaxVal   = -1;
    m_curveParam.floatMaxVal = INVALID_VALUE;
    // temperature
    m_curveParam.idxMin = MAXNUM_TREND;
    m_curveParam.idxMax = -1;

    m_curveParam.nSigNum = (info->iNum>MAXNUM_TREND ?
                    MAXNUM_TREND:info->iNum);
    TRACEDEBUG( "WdgFP5Deg2Curve::ShowData sigNum<%d>", m_curveParam.nSigNum );
    if (m_curveParam.nSigNum <= 0)
    {
        update();
        return;
    }

    QDateTime dateTimeNow = QDateTime::currentDateTime();
//    int nYear  = dateTimeNow.date().year();
//    int nMonth = dateTimeNow.date().month();
//    int nDay   = dateTimeNow.date().day();
//    int nHour = dateTimeNow.time().hour();
//    if ( nHour%2 != 0 )
//    {
//        nHour -= 1;
//    }
//    dateTimeNow = QDateTime(QDate(nYear, nMonth,nDay),
//                            QTime(nHour, 0, 0));
//    TRACEDEBUG( "  datetime now:<%d-%d-%d %d:%d:%d>",
//                dateTimeNow.date().year(),
//                dateTimeNow.date().month(),
//                dateTimeNow.date().day(),
//                dateTimeNow.time().hour(),
//                dateTimeNow.time().minute(),
//                dateTimeNow.time().second()
//                );
    time_t timeAxisStart = (time_t)(
                dateTimeNow.addDays(-7).toTime_t());
    time_t timeSig;
    int idxSigStart = 0;
    for(int idxDes=0; idxDes<MAXNUM_TREND; ++idxDes)
    {
        pTempParam->TempTrend[idxDes].fTemp = -9999;
        m_timesAxis[idxDes] = timeAxisStart+SECONDS_2H*idxDes;
    }
    float fSigVal = 0;
    for(int idxSig=0; idxSig<m_curveParam.nSigNum; ++idxSig)
    {
        if(m_eTempDispFormat == TEMP_FORMAT_TYPE_FAHR)
        {
            fSigVal = ((info->TempTrend[idxSig].fTemp)*1.8)+32;
        }
        else
        {
            fSigVal = info->TempTrend[idxSig].fTemp;
        }


        timeSig = info->TempTrend[idxSig].TimeTrend;
        QDateTime dateTime  = QDateTime::fromTime_t( timeSig );
        TRACEDEBUG( "   src idxSig<%d> sigVal<%f> sigTime<%d-%d-%d %d:%d:%d>",
                    idxSig,
                    fSigVal,
                    dateTime.date().year(),
                    dateTime.date().month(),
                    dateTime.date().day(),
                    dateTime.time().hour(),
                    dateTime.time().minute(),
                    dateTime.time().second()
                    );

        for(int idxDes=idxSigStart; idxDes<MAXNUM_TREND-1; ++idxDes)
        {
            if ( timeSig<m_timesAxis[idxDes] )
            {
                break;
            }
            else if ( timeSig>=m_timesAxis[idxDes] &&
                 timeSig<=m_timesAxis[idxDes]+SECONDS_2H )
            {
                pTempParam->TempTrend[idxDes].fTemp = fSigVal;
                //找最大最小X轴值
                if (idxDes < m_curveParam.idxMin)
                    m_curveParam.idxMin = idxDes;
                if (idxDes > m_curveParam.idxMax)
                    m_curveParam.idxMax = idxDes;

                if(m_eTempDispFormat == TEMP_FORMAT_TYPE_FAHR)
                {
                    if (fSigVal>=MIN_DEG_F_TEMP_VALUE &&
                            fSigVal<=MAX_DEG_F_TEMP_VALUE)
                    {
                        // 找最大值
                        if (m_curveParam.floatMaxVal < pTempParam->TempTrend[idxDes].fTemp)
                        {
                            m_curveParam.floatMaxVal = pTempParam->TempTrend[idxDes].fTemp;
                            m_curveParam.idxMaxVal = idxDes;
                        }
                    }
                }
                else
                {
                    if (fSigVal>=MIN_TEMP_VALUE &&
                            fSigVal<=MAX_TEMP_VALUE)
                    {
                        // 找最大值
                        if (m_curveParam.floatMaxVal < pTempParam->TempTrend[idxDes].fTemp)
                        {
                            m_curveParam.floatMaxVal = pTempParam->TempTrend[idxDes].fTemp;
                            m_curveParam.idxMaxVal = idxDes;
                        }
                    }
                }

                idxSigStart = idxDes+1; //(--idxDes>0 ? idxDes:0);
                QDateTime dateTime2 = QDateTime::fromTime_t( m_timesAxis[idxDes] );
                QDateTime dateTime3 = QDateTime::fromTime_t( m_timesAxis[idxDes]+SECONDS_2H );
            TRACEDEBUG( " new idxDes<%d> idxSigStart<%d> desVal<%f> dateTimeAxis:<%d-%d-%d %d:%d:%d>-<%d-%d-%d %d:%d:%d>",
                            idxDes,
                            idxSigStart,
                            pTempParam->TempTrend[idxDes].fTemp,

                            dateTime2.date().year(),
                            dateTime2.date().month(),
                            dateTime2.date().day(),
                            dateTime2.time().hour(),
                            dateTime2.time().minute(),
                            dateTime2.time().second(),

                            dateTime3.date().year(),
                            dateTime3.date().month(),
                            dateTime3.date().day(),
                            dateTime3.time().hour(),
                            dateTime3.time().minute(),
                            dateTime3.time().second()
                            );
                break;
            }
            else
            {
                continue;
            }
        }
    }
    TRACEDEBUG( "     *** m_curveParam.idxMin<%d> m_curveParam.idxMax<%d> m_curveParam.floatMaxVal<%f>",
                m_curveParam.idxMin, m_curveParam.idxMax, m_curveParam.floatMaxVal );

    update();
}

void WdgFP5Deg2Curve::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
    else if(m_timerFreshTempFormat == event->timerId())
    {

        m_eTempDispFormat = GetTempFormat();

        if(m_eLastTempFormat != m_eTempDispFormat)
        {
            Refresh();
        }
        update();
        m_eLastTempFormat = m_eTempDispFormat;
    }
}

void WdgFP5Deg2Curve::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void WdgFP5Deg2Curve::keyPressEvent(QKeyEvent* keyEvent)
{
    QWidget::keyPressEvent(keyEvent);
}

// WdgFP8BattDegCurve
void WdgFP5Deg2Curve::paintEvent(QPaintEvent *)
{
    //样式表支持
    QStyleOption opt;
    opt.init(this);
    QPainter paint(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &paint, this);

    QPainter* painter = &paint;

    curveChart_paint(
                painter,
                m_curveParam,
                m_eTempDispFormat
                );
}

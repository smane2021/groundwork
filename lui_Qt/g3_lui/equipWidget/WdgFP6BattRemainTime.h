/******************************************************************************
文件名：    WdgFP6BattRemainTime.h
功能：      第一层界面p6 电池剩余时间
作者：      刘金煌
创建日期：   2013年5月14日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef WDGFP6BATTREMAINTIME_H
#define WDGFP6BATTREMAINTIME_H

#include "common/basicwidget.h"
#include "common/uidefine.h"
#include "common/MultiDemon.h"

using namespace PieChart;

namespace Ui {
class WdgFP6BattRemainTime;
}

class WdgFP6BattRemainTime : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit WdgFP6BattRemainTime(QWidget *parent = 0);
    ~WdgFP6BattRemainTime();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();

protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);
    virtual void paintEvent(QPaintEvent* event);

signals:
    void goToBaseWindow(enum WIDGET_TYPE);

private:
    int m_timerId;
    CmdItem  m_cmdItem;
    QPixmap  m_pixmapHand;
    QPixmap  m_pixmapCover;
    QPixmap  m_pixmapCenter;
    float    m_fMargins[MARGIN_COUNT_REMAIN_TIME];
    float    m_fSigVal; // 值
    QString  m_strSigName;
    QString  m_strSigUnit;
    int      iFormat;

private:
    Ui::WdgFP6BattRemainTime *ui;
};

#endif // WDGFP6BATTREMAINTIME_H

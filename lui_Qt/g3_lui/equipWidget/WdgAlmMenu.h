#ifndef WDGALMMENU_H
#define WDGALMMENU_H

#include "common/basicwidget.h"
#include "common/uidefine.h"
#include "common/global.h"
#include "common/Macro.h"
#include "common/pubInclude.h"
#include "config/configparam.h"
#include "ui_WdgAlmMenu.h"

namespace Ui
{
    class WdgAlmMenu;
}

class WdgAlmMenu : public BasicWidget
{
    Q_OBJECT
public:
    WdgAlmMenu(QWidget *parent = 0);
    ~WdgAlmMenu();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();

protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);

signals:
    void goToHomePage();
    void goToBaseWindow(enum WIDGET_TYPE);

private:
    int      m_timerId;
    CmdItem  m_cmdItem;

private:
    Ui::WdgAlmMenu *ui;
};

#endif // WDGALMMENU_H

/******************************************************************************
文件名：    WdgFP6BattRemainTime.cpp
功能：      第一层界面p6 电池剩余时间 环形图
作者：      刘金煌
创建日期：   2013年5月14日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "WdgFP6BattRemainTime.h"
#include "ui_WdgFP6BattRemainTime.h"

#include <QPainter>
#include <QKeyEvent>
#include <QPixmapCache>
#include "common/pubInclude.h"
#include "common/global.h"
#include "config/configparam.h"
#include "config/PosPieChart.h"

WdgFP6BattRemainTime::WdgFP6BattRemainTime(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::WdgFP6BattRemainTime)
{
    ui->setupUi(this);

    InitWidget();
    InitConnect();

    m_timerId = 0;
    m_cmdItem.CmdType   = CT_READ;
    m_cmdItem.ScreenID  = SCREEN_ID_WdgFP6BattRemainTime;
}

WdgFP6BattRemainTime::~WdgFP6BattRemainTime()
{
    TRACEDEBUG("WdgFP6BattRemainTime::~WdgFP6BattRemainTime");
    delete ui;
}

void WdgFP6BattRemainTime::InitWidget()
{
    m_fMargins[0] = 0;
    m_fMargins[1] = 5;
    m_fMargins[2] = 10;
    m_fMargins[3] = 18;

    if(g_cfgParam->ms_initParam.lcdRotation == LCD_ROTATION_90DEG)
    {
        m_pixmapHand.load(PATH_IMG + "pie_batt_pointerV.png");
        m_pixmapCover.load(PATH_IMG + "pie_coverV.png");
        m_pixmapCenter.load(PATH_IMG + "pie_centerV.png");
    }
    else
    {
       m_pixmapHand.load(PATH_IMG + "pie_batt_pointer.png");
       m_pixmapCover.load(PATH_IMG + "pie_cover.png");
       m_pixmapCenter.load(PATH_IMG + "pie_center.png");

    }
    SET_BACKGROUND_WIDGET( "WdgFP6BattRemainTime",PosBase::strImgBack_Title );
    SET_STYLE_LABEL_ENTER2;
    if (LCD_ROTATION_90DEG == g_cfgParam->ms_initParam.lcdRotation)
    {
        ui->label_enter->setGeometry(
                    PosBase::titleX2,
                    PosBase::titleY2,
                    PosBase::titleWidth2,
                    PosBase::titleHeight2Rows
                    );
        ui->label_enter->setAlignment (Qt::AlignVCenter);
    }
    SET_STYLE_SCROOLBAR( 3, 1 );

#ifdef TEST_GUI
    PACK_ICOINFO data;
    PACK_ICOINFO* info = (PACK_ICOINFO*)&data;
    info->DispData[0].iFormat   = 0;
    info->DispData[0].fSigValue = 8;
    strcpy(info->DispData[0].cSigName, "Remaining Time");
    strcpy(info->DispData[0].cSigUnit, "hrs");
    ShowData( info );
#endif
}

void WdgFP6BattRemainTime::InitConnect()
{
    ;
}

void WdgFP6BattRemainTime::Enter(void* param)
{
    TRACELOG1( "WdgFP6BattRemainTime::Enter(void* param)" );
    Q_UNUSED( param );

    INIT_VAR;

    enum PIE_CHART_TYPE type = PIE_CHART_TYPE_BATT;
    PosPieChart::setPieChartType( (void*)&type );

#ifdef TEST_GUI
    ENTER_FIRSTLY;
#else
    ENTER_GET_DATA;
#endif
}

void WdgFP6BattRemainTime::Leave()
{
    LEAVE_WDG( "WdgFP6BattRemainTime" );
    this->deleteLater();
    QPixmapCache::clear ();
}

void WdgFP6BattRemainTime::Refresh()
{
    REFRESH_DATA_PERIODICALLY(
                m_cmdItem.ScreenID,
                "WdgFP6BattRemainTime"
                );
}


void WdgFP6BattRemainTime::ShowData(void* pData)
{
    if ( !pData )
        return;

    PACK_ICOINFO* info = (PACK_ICOINFO*)pData;

    iFormat = info->DispData[0].iFormat;
    m_fSigVal = info->DispData[0].fSigValue;
    m_strSigName = QString( info->DispData[0].cSigName );
    m_strSigUnit = QString( info->DispData[0].cSigUnit );

    TRACEDEBUG( "WdgFP6BattRemainTime::ShowData() read data "
               "val<%f>"
               "sigName<%s> sigUnit<%s>",
               m_fSigVal,
               info->DispData[0].cSigName,
               info->DispData[0].cSigUnit
               );

    QString strSigVal;
    if (m_fSigVal < INVALID_VALUE)
    {
        strSigVal = "--";
    }
    else
    {
        strSigVal = QString::number(m_fSigVal, FORMAT_DECIMAL);
    }

    QString strTitle = m_strSigName+":";
    if (LCD_ROTATION_90DEG == g_cfgParam->ms_initParam.lcdRotation)
    {
        strTitle.append( "\n" );
    }
    strTitle.append(
                strSigVal +
                m_strSigUnit +
                " >"
                );
    ui->label_enter->setText(strTitle );

    update();
}

void WdgFP6BattRemainTime::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
}

void WdgFP6BattRemainTime::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void WdgFP6BattRemainTime::keyPressEvent(QKeyEvent* keyEvent)
{
    QWidget::keyPressEvent(keyEvent);
}
// pieChart_paint
void WdgFP6BattRemainTime::paintEvent(QPaintEvent*)
{
    //样式表支持
    QStyleOption opt;
    opt.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);

    QPainter* pPainter = &painter;

    int nMarginNum          = MARGIN_COUNT_REMAIN_TIME-1;
    float* fSigVals         = &m_fSigVal;
    QPixmap* pixmapPointers = &m_pixmapHand;    
    QPixmap* pixmapCover    = &m_pixmapCover;
    QPixmap* pixmapCenter   = &m_pixmapCenter;

    pPainter->setFont( g_cfgParam->gFontSmallN );
    pPainter->translate(
                PosPieChart::originX,
                PosPieChart::originY_Batt
                );
    pPainter->setRenderHint(
                QPainter::Antialiasing,
                true
                );

    //画三个区域的色块 Added by alpha
    QColor arrColor[3] = {QColor(0xFE,0x8C,0x00),QColor(0xFE,0xCC,0x00),QColor(0x82,0xBE,0x1F)};
    pPainter->setPen( Qt::NoPen );
    for (int i=0; i<nMarginNum; i++)
    {
        pPainter->setBrush( arrColor[i] );
        int startAngle = (int)calcPieAngle_PieChart(m_fMargins[i+1],m_fMargins[0],m_fMargins[nMarginNum]);
        int endAngle = (int)calcPieAngle_PieChart(m_fMargins[i],m_fMargins[0],m_fMargins[nMarginNum])-
                          (int)calcPieAngle_PieChart(m_fMargins[i+1],m_fMargins[0], m_fMargins[nMarginNum]);
        pPainter->drawPie( QRect(-PosPieChart::nRadius,-PosPieChart::nRadius,PosPieChart::nRadius*2,PosPieChart::nRadius*2),
                           startAngle,endAngle);
    }
    //Added end

    // 盖罩
    pPainter->drawPixmap(
                -PosPieChart::sizeCover.width()/2,
                -PosPieChart::coverY,
                *pixmapCover
                );

    // 写字
    float fAngle = 0;
    pPainter->setPen( textNormalColor );
    QString strSigVal;
    for (int i=0; i<nMarginNum; ++i)
    {
        strSigVal = QString::number(m_fMargins[i], FORMAT_DECIMAL);
        fAngle = calcCoordinateAngle_PieChart_Batt(
                    m_fMargins[i],
                    m_fMargins[0],
                    m_fMargins[nMarginNum-1]
                    );

        pPainter->save();
        pPainter->rotate( fAngle );
        pPainter->translate(
                    QPoint(-PosPieChart::nRadius_batt, 0)
                    );
        pPainter->rotate( -fAngle );

        pPainter->drawText(
                    QPoint(-fmSmallN->width(strSigVal), 0),
                    strSigVal);

        pPainter->restore();
    }

    // 指针
    {
        if (*fSigVals < m_fMargins[0])
        {
            *fSigVals = m_fMargins[0];
            fAngle = calcCoordinateAngle_PieChart_Batt(
                        *fSigVals,
                        m_fMargins[0],
                        m_fMargins[nMarginNum-1]
                        );
        }
        else if (*fSigVals > m_fMargins[nMarginNum])
        {
            if (*fSigVals < 100)
            {
                fAngle = 130;
            }
            else
            {
                fAngle = 170;
            }
        }
        else
        {
            fAngle = calcCoordinateAngle_PieChart_Batt(
                        *fSigVals,
                        m_fMargins[0],
                        m_fMargins[nMarginNum-1]
                        );
        }
        fAngle = fAngle<Angle_NegativeX ? Angle_NegativeX:fAngle;
        pPainter->save();
        pPainter->rotate( fAngle );
        pPainter->drawPixmap(
                    -PosPieChart::sizePointer.width(),
                    -PosPieChart::sizePointer.height()/2,
                    *pixmapPointers
                    );
        pPainter->restore();
    }
    // 中心
    pPainter->drawPixmap(
                -PosPieChart::sizeCenter.width()/2,
                -PosPieChart::sizeCenter.height()/2,
                *pixmapCenter
                );
}

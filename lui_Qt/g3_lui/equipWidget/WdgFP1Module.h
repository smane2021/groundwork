/******************************************************************************
文件名：    WdgFP1Module.h
功能：      第一层界面p1 Module
作者：      刘金煌
创建日期：   2013年5月13日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef WDGFP1MODULE_H
#define WDGFP1MODULE_H

#include "common/basicwidget.h"
#include "common/uidefine.h"
#include <QTime>
#include <QTimer>

#define MAX_MODULE_ITEMS (30)

namespace Ui {
class WdgFP1Module;
}

class WdgFP1Module : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit WdgFP1Module(QWidget *parent = 0);
    ~WdgFP1Module();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();

protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);

private:
    void addTableRow_moduleNum(const PACK_INFO* info,
                               enum WIDGET_TYPE wt,
                               bool bReadOnly=false);
    QString varValue2String(int nSigValueType,
                            int iFormat,
                            VAR_VALUE vSigValue,
                            const char *strEnumText);
    bool appendReadOnlyItem(PACK_INFO*& info,
                            PACK_INFO* infoEnd,
                            const int nDataNum);

signals:
    void goToBaseWindow(enum WIDGET_TYPE);

private slots:
    void sltTableKeyPress(int key);
    void on_tableWidget_itemSelectionChanged();

private:
    int      m_timerId;
    CmdItem  m_cmdItem;
    int      m_nSelRow;
    bool     m_bEnterFirstly;
    QTime    m_timeElapsedKeyPress;

    int  m_nRows;
    // 映射行号到第二层界面
    int  m_mapW2nd[MAX_MODULE_ITEMS];
    // 映射个数
    int  m_mapNum[MAX_MODULE_ITEMS];

    int  m_nRectNum;
    int  m_nSolNum;
    int  m_nConvNum;

private:
    Ui::WdgFP1Module *ui;
};

#endif // WDGFP1MODULE_H

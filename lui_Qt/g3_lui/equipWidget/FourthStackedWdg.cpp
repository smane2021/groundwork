/******************************************************************************
文件名：    FourthStackedWdg.cpp
功能：      第四层窗口，目前只有告警帮助 ui只有tableWdg
作者：      刘金煌
创建日期：   2013年5月31日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "FourthStackedWdg.h"
#include "ui_FourthStackedWdg.h"

#include <QKeyEvent>
#include "common/pubInclude.h"
#include "common/global.h"
#include "config/configparam.h"

FourthStackedWdg::FourthStackedWdg(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::FourthStackedWdg)
{
    ui->setupUi(this);

    InitWidget();
    InitConnect();

    SET_GEOMETRY_WIDGET( this );
    SET_GEOMETRY_WIDGET( ui->stackedWidget );

    m_wt = -1;
}

FourthStackedWdg::~FourthStackedWdg()
{
    delete ui;

//    delete m_tbl_WT4_ALARM_HELP;
}

void FourthStackedWdg::InitWidget()
{

}

void FourthStackedWdg::InitConnect()
{
    ;
}

void FourthStackedWdg::CreateDispWdg(int iDispWdgType)
{
    Q_UNUSED(iDispWdgType);

    Wdg3Table* tbl_WT4_ALARM_HELP = new Wdg3Table(WT4_ALARM_HELP, this);
    connect( tbl_WT4_ALARM_HELP, SIGNAL(goToBaseWindow(enum WIDGET_TYPE)),
             this, SIGNAL(goToBaseWindow(enum WIDGET_TYPE)) );
    ui->stackedWidget->insertWidget(0,tbl_WT4_ALARM_HELP);
}

void FourthStackedWdg::Enter(void* param)
{
    INIT_VAR;

    int wt = *(int*)param;
    if (wt<WT4_FOURTH_MAX)
    {
        m_wt = wt;
    }

    CreateDispWdg(m_wt);

    ui->stackedWidget->setCurrentIndex(0);

    m_pCurWdt = (BasicWidget*)ui->stackedWidget->currentWidget();
    if(m_pCurWdt != NULL)
    {
        m_pCurWdt->setFocus();
        m_pCurWdt->Enter();
    }
}

void FourthStackedWdg::Leave()
{
    TRACELOG1( "FourthStackedWdg::Leave()" );
    m_pCurWdt = (BasicWidget*)ui->stackedWidget->currentWidget();
    m_pCurWdt->Leave();

    ui->stackedWidget->removeWidget(m_pCurWdt);

    TRACEDEBUG("delete SecondStackedWdg");

    delete m_pCurWdt;
    m_pCurWdt = NULL;
}

void FourthStackedWdg::Refresh()
{
}

void FourthStackedWdg::ShowData(void* pData)
{
    Q_UNUSED( pData );
}

void FourthStackedWdg::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void FourthStackedWdg::keyPressEvent(QKeyEvent* keyEvent)
{
    //TRACEDEBUG( "FourthStackedWdg::keyPressEvent" );
    switch ( keyEvent->key() )
    {
    case Qt::Key_Escape:
        emit goToBaseWindow(WT3_THIRD_MAX);
        break;

    default:
        break;
    }

    QWidget::keyPressEvent (keyEvent);
}

#ifndef WDGFP12SCREENSAVEROUT_H
#define WDGFP12SCREENSAVEROUT_H

#include "common/basicwidget.h"
#include "common/uidefine.h"

namespace Ui {
class WdgFP12ScreenSaverOut;
}

class WdgFP12ScreenSaverOut : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit WdgFP12ScreenSaverOut(QWidget *parent = 0);
    ~WdgFP12ScreenSaverOut();
    
public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();

protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);
    virtual void paintEvent(QPaintEvent* event);

signals:
    void goToHomePage();

private:
    int      m_timerId;
    CmdItem  m_cmdItem;
    float    m_fSysVal;
    QString  m_strSysVal;

private:
    Ui::WdgFP12ScreenSaverOut *ui;
};

#endif // WDGFP12SCREENSAVEROUT_H

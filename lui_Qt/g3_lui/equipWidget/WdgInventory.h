/******************************************************************************
文件名：    Wdg2Table.h
功能：      第二层界面 表格
           模块 0 1 WT2_RECTINFO WT2_SOLINFO
           告警 4 5 6 WT2_ACT_ALARM WT2_HIS_ALARM WT2_EVENT_LOG
作者：      刘金煌
创建日期：   2013年5月9日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef INVENTORY_H
#define INVENTORY_H

#include <QTime>
#include <QTimer>
#include "common/basicwidget.h"
#include "common/uidefine.h"
#include "common/buzztablewidget.h"
#include "common/MultiDemon.h"
#include "ui_WdgInventory.h"

using namespace CtrlLight;

#define MAX_SCREEN_ID_INV  SCREEN_ID_Wdg2Table_SMDUE_Inv

//Frank Wu,20151103
#define MAX_RS485_INV_NUM   (66+32+16)//FiammBatt(32) + NaradaBMS(16)

namespace Ui {
class WdgInventory;
}

class WdgInventory : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit WdgInventory(enum WIDGET_TYPE wt, QWidget *parent = 0);
    ~WdgInventory();
    
public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();

protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();
    void testGUI();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);

private:
    void setInvItem();
    void setInvDataMem(void* pData); // set inv data in memory

private slots:
    void sltScreenSaver();
    // send cmd to ctrl lighting
    void sltTimerHandler(void);

signals:
    void goToHomePage();
    void goToBaseWindow(enum WIDGET_TYPE);

private:
    int m_wt;
    QTimer          m_QTimerLight;
    CtrlLightInfo_t m_CtrlLightInfo;
    QTime    m_timeElapsedKeyPress;

    int      m_timerId;
    CmdItem  m_cmdItem;
    void*    m_pData;
    int m_nPageIdx;
    int m_nPages;
    int m_nSigNum;
    bool m_bEnterFirstly;

    // Inv
    int     m_nRectInv;
    MOD_INV *m_pRectInv;

    int     m_nSlaveRectInv;
    MOD_INV *m_pSlaveRectInv;

    int     m_nSMI2CInv;
    MOD_INV *m_pSMI2CInv;

    int     m_nRS485Inv;
    MOD_INV *m_pRS485Inv;

    int     m_nConvInv;
    MOD_INV *m_pConvInv;

    int     m_nLiBattInv;
    MOD_INV *m_pLiBattInv;

    int     m_nSolConvInv;
    MOD_INV *m_pSolConvInv;

    int m_nSMDUEInv;
    MOD_INV *m_pSMDUEInv;

private:
    Ui::WdgInventory *ui;
};

#endif // INVENTORY_H

/******************************************************************************
文件名：    Wdg2P5BattRemainTime.cpp
功能：      第二层界面p5 各个电池剩余时间 电池条形图
作者：      刘金煌
创建日期：   2013年5月14日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "Wdg2P5BattRemainTime.h"
#include "ui_Wdg2P5BattRemainTime.h"

#include <QPainter>
#include <QKeyEvent>
#include "config/configparam.h"
#include "common/pubInclude.h"
#include "common/global.h"
#include "config/PosBarChart.h"

Wdg2P5BattRemainTime::Wdg2P5BattRemainTime(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::Wdg2P5BattRemainTime)
{
    ui->setupUi(this);

    InitWidget();
    InitConnect();

    m_timerId = 0;
    m_cmdItem.CmdType   = CT_READ;
    m_cmdItem.ScreenID  = SCREEN_ID_Wdg2P5BattRemainTime;
}

Wdg2P5BattRemainTime::~Wdg2P5BattRemainTime()
{
    TRACEDEBUG("Wdg2P5BattRemainTime::~Wdg2P5BattRemainTime");
    delete ui;
}

void Wdg2P5BattRemainTime::InitWidget()
{
  SET_GEOMETRY_LABEL_TITLE(ui->label_enter);
  SET_BACKGROUND_WIDGET( "Wdg2P5BattRemainTime",PosBase::strImgBack_Title );
  ui->label_enter->setText (tr("Batteries"));

#ifdef TEST_GUI
    PACK_BATTINFO  data;
    PACK_BATTINFO* info = &data;
    info->BattNum = 10;

    m_nPageIdx = 1;
    for (int i=0; i<info->BattNum; ++i)
    {
        BATT_INFO* battInfo = &(info->BattInfo[i]);

        battInfo->iRespond = 0;
        sprintf(battInfo->cEquipName, "batt_%d", i+1);

        // 500Ah
        battInfo->BattItem[0].iFormat = 1;
        battInfo->BattItem[0].vValue.fValue = 500;
        // 200A
        battInfo->BattItem[1].iFormat = 1;
        battInfo->BattItem[1].vValue.fValue = 200;
        // 100%
        battInfo->BattItem[2].iFormat =1;
        battInfo->BattItem[2].vValue.fValue = 100;
    }
    ShowData( info );
#endif
}

void Wdg2P5BattRemainTime::InitConnect()
{
}

void Wdg2P5BattRemainTime::Enter(void* param)
{
    TRACELOG1( "Wdg2P5BattRemainTime::Enter(void* param)" );

    Q_UNUSED(param);

    INIT_VAR;

#ifdef TEST_GUI
    ENTER_FIRSTLY;
#else
    m_nPageIdx = 1;
    m_nPages   = 0;

    ENTER_GET_DATA;
#endif
}

void Wdg2P5BattRemainTime::Leave()
{
    LEAVE_WDG( "Wdg2P5BattRemainTime" );
    this->deleteLater();
}

void Wdg2P5BattRemainTime::Refresh()
{
    REFRESH_DATA_PERIODICALLY(
                m_cmdItem.ScreenID,
                "Wdg2P5BattRemainTime"
                );
}

void Wdg2P5BattRemainTime::ShowData(void* pData)
{
    if ( !pData )
    {
        return;
    }

    //TRACEDEBUG( "Wdg2P5BattRemainTime::ShowData(void* pData) %x", pData );
    if (pData != g_dataBuff)
    {
        memcpy( g_dataBuff, pData, sizeof(PACK_BATTINFO) );
    }
    PACK_BATTINFO* info = (PACK_BATTINFO*)g_dataBuff;
    m_nSigNum   = info->BattNum;
    TRACEDEBUG( "Wdg2P5BattRemainTime::ShowData nSigNum(%d)", m_nSigNum );
    if (m_nSigNum < 1)
    {
        TRACELOG1( "Wdg2P5BattRemainTime::ShowData m_nSigNum < 1" );
        update();
        return;
    }

    int nModNum   = m_nSigNum%MAX_ITEM_PERPAGE_BARCHART3;
    m_nPages = ( nModNum ? (m_nSigNum/MAX_ITEM_PERPAGE_BARCHART3+1):
               (m_nSigNum/MAX_ITEM_PERPAGE_BARCHART3) );
    if (m_nPageIdx < 1)
    {
        m_nPageIdx = m_nPages;
    }
    if (m_nPageIdx > m_nPages)
    {
        m_nPageIdx = 1;
    }

    if (m_nPageIdx==m_nPages && nModNum)
    {
        m_nShowMaxItemCurPage = nModNum;
    }
    else
    {
        m_nShowMaxItemCurPage = MAX_ITEM_PERPAGE_BARCHART3;
    }
    TRACEDEBUG( ">>>   Wdg2P5BattRemainTime::ShowData SigNum<%d> m_nPageIdx<%d> m_nShowMaxItemCurPage<%d>",
                m_nSigNum,
                m_nPageIdx,
                m_nShowMaxItemCurPage
                );

    for (int i=0; i<m_nShowMaxItemCurPage; ++i)
    {
        BATT_INFO* battInfo = &(info->BattInfo[(m_nPageIdx-1)*
                     MAX_ITEM_PERPAGE_BARCHART3+i]);

        m_battInfoBuffed[i].iRespond = battInfo->iRespond;
        m_strEquipName[i] = battInfo->cEquipName;

        m_battInfoBuffed[i].BattItem[0].vValue.fValue =
                battInfo->BattItem[0].vValue.fValue;
        m_battInfoBuffed[i].BattItem[0].iFormat =
                battInfo->BattItem[0].iFormat;

        m_battInfoBuffed[i].BattItem[1].vValue.fValue =
                battInfo->BattItem[1].vValue.fValue;
        m_battInfoBuffed[i].BattItem[1].iFormat =
                battInfo->BattItem[1].iFormat;

        m_battInfoBuffed[i].BattItem[2].vValue.fValue =
                battInfo->BattItem[2].vValue.fValue;
        m_battInfoBuffed[i].BattItem[2].iFormat =
                battInfo->BattItem[2].iFormat;

//        TRACEDEBUG( "Wdg2P5BattRemainTime::ShowData des i<%d> sigName<%s>\nSigVal<%f> iFormat<%d> \nSigVal<%f> iFormat<%d> \nSigVal<%f> iFormat<%d>",
//                    i,
//                    battInfo->cEquipName,
//                    m_battInfoBuffed[i].BattItem[0].vValue.fValue,
//                    m_battInfoBuffed[i].BattItem[0].iFormat,
//                    m_battInfoBuffed[i].BattItem[1].vValue.fValue,
//                    m_battInfoBuffed[i].BattItem[1].iFormat,
//                    m_battInfoBuffed[i].BattItem[2].vValue.fValue,
//                    m_battInfoBuffed[i].BattItem[2].iFormat
//                    );
    }

    SET_STYLE_SCROOLBAR( m_nPages, m_nPageIdx );
    update();
}

void Wdg2P5BattRemainTime::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
}

void Wdg2P5BattRemainTime::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void Wdg2P5BattRemainTime::keyPressEvent(QKeyEvent* keyEvent)
{
    switch ( keyEvent->key() )
    {
    case Qt::Key_Escape:
        emit goToBaseWindow( WT1_BATT_REMAIN_TIME );
        break;

    case Qt::Key_Down:
        ++m_nPageIdx;
        ShowData( g_dataBuff );
        break;

    case Qt::Key_Up:
        --m_nPageIdx;
        ShowData( g_dataBuff );
        break;

    default:
        break;
    }
}

void Wdg2P5BattRemainTime::paintEvent(QPaintEvent* event)
{
    Q_UNUSED( event );

    static const int ITEM_HEIGHT = PosBarChart::iBattHeight;
    static const int MARGIN_LEFT = PosBarChart::iMargeLeft;
    static const int BATT_LENGHTH= PosBarChart::iBattWidth;
    static const int START_Y     = PosBarChart::iText_Y;
    static const int GAP_TEXT    = PosBarChart::iText_Gap;
    static const int BATT_CONVEX = PosBarChart::iBatt_Dot_Y;

#define calcPointX(val) (BATT_LENGHTH*(val/100))

    //样式表支持
    QStyleOption opt;
    opt.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);

    painter.setFont( g_cfgParam->gFontLargeN );

    QString strNoData = tr("No Data");
    if (m_nSigNum < 1)
    {
        painter.setFont( g_cfgParam->gFontSmallN );
        painter.setPen( nodataColor );
        painter.drawText(
                    PosBase::screenWidth/2-fmSmallN->width(strNoData)/2,
                    PosBase::screenHeight/2,
                    strNoData
                    );
        return;
    }

    // background points
    static QPoint ptsBattBackGround[4] = {
        QPoint( MARGIN_LEFT,     START_Y+GAP_TEXT ),
        QPoint( MARGIN_LEFT+BATT_LENGHTH,
        START_Y+GAP_TEXT ),

        QPoint( MARGIN_LEFT+BATT_LENGHTH,
        START_Y+GAP_TEXT+BATT_CONVEX*3 ),
        QPoint( MARGIN_LEFT,
        START_Y+GAP_TEXT+BATT_CONVEX*3 )
    };

    // foreground points
    static QPoint ptsBattForeGround[8] = {
        QPoint( MARGIN_LEFT,     START_Y+GAP_TEXT ),
        QPoint( MARGIN_LEFT,     START_Y+GAP_TEXT ),//1

        QPoint( MARGIN_LEFT,
        START_Y+GAP_TEXT+BATT_CONVEX ),//2 同3
        QPoint( MARGIN_LEFT,
        START_Y+GAP_TEXT+BATT_CONVEX ),
        QPoint( MARGIN_LEFT,
        START_Y+GAP_TEXT+BATT_CONVEX*2 ),//4 同5
        QPoint( MARGIN_LEFT,
        START_Y+GAP_TEXT+BATT_CONVEX*2 ),

        QPoint( MARGIN_LEFT,
        START_Y+GAP_TEXT+BATT_CONVEX*3 ),//6
        QPoint( MARGIN_LEFT,
        START_Y+GAP_TEXT+BATT_CONVEX*3 )
    };

    // copy foreground points to draw green color
    static QPoint ptsBattForeGround2[8];

    int idxBranch   = 1;
    float fCapacity = 0;
    int iFormat = 1;
    DATA_INFO* pDataInfo = NULL;
    for (idxBranch=1; idxBranch<=m_nShowMaxItemCurPage; ++idxBranch)
    {
        painter.save();
        painter.translate( MARGIN_LEFT, ITEM_HEIGHT*(idxBranch-1) );

        painter.setPen( textNormalColor );
        painter.setBrush( textNormalColor );
        // BAT1_500Ah
        pDataInfo = &(m_battInfoBuffed[idxBranch-1].
                      BattItem[0]);
        iFormat = pDataInfo->iFormat;
        QString strFirstRowTxt = m_strEquipName[idxBranch-1]+"-"+
                QString::number(pDataInfo->vValue.fValue, FORMAT_DECIMAL)+
                "Ah";
        painter.drawText(
                    -MARGIN_LEFT+PosBarChart::iBattChargeIconX,
                    START_Y,
                    strFirstRowTxt
                    );

        //绘制电池绿色
        pDataInfo = &(m_battInfoBuffed[idxBranch-1].
                      BattItem[2]);
        fCapacity = pDataInfo->vValue.fValue;

        iFormat = pDataInfo->iFormat;
        if (fCapacity > 0)
        {
            painter.setPen( Qt::NoPen );
            painter.setBrush( barColor );
            if (fCapacity >= 99.9)
            {
                painter.drawConvexPolygon( ptsBattBackGround, 4 );
            }
            else
            {
                for (int i=0; i<8; ++i)
                {
                    ptsBattForeGround2[i] = ptsBattForeGround[i];
                }
                for (int i=1; i<=6; ++i)
                {
                    ptsBattForeGround2[i].setX(
                                (int)(MARGIN_LEFT+calcPointX(fCapacity)) );
                }
                painter.drawConvexPolygon( ptsBattForeGround2,8 );
            }
        }

        //绘制外围线框
        QPen pen;
        pen.setColor(textNormalColor);
        pen.setWidth(PosBarChart::nPenWidth);
        painter.setPen( pen );
        painter.setBrush( Qt::NoBrush );
        painter.drawConvexPolygon( ptsBattBackGround, 4 );

        //画充电符号
        QPixmap pixmap;
        pixmap.load (PATH_IMG + "batt_charging.png");
        painter.drawPixmap(
                    -MARGIN_LEFT+PosBarChart::iBattChargeIconX,
                    ptsBattBackGround[1].y(),
                    pixmap);

        // 50%
        painter.setPen( textNormalColor );
        painter.setBrush(textNormalColor );
        //Modified by alpha
        if(g_cfgParam->ms_initParam.dispVersion == DISPLAY_VERSION_NORMAL)
        {
            painter.drawText( ptsBattBackGround[3].x()+7, ptsBattBackGround[3].y()-BATT_CONVEX+2,
                          QString::number(fCapacity, FORMAT_DECIMAL)+"%" );
        }

        // 49.5A
        pDataInfo = &(m_battInfoBuffed[idxBranch-1].
                      BattItem[1]);
        iFormat = pDataInfo->iFormat;

        //Modified by alpha
        static int nWidthPercent = 0;
        if(g_cfgParam->ms_initParam.dispVersion == DISPLAY_VERSION_NORMAL)
        {
          nWidthPercent = fmLargeN->width( "100.0% " );
        }
        else
        {
          nWidthPercent = 0;
        }
        //Modified end
        painter.drawText( ptsBattBackGround[3].x()+7+nWidthPercent, ptsBattBackGround[3].y()-BATT_CONVEX+2,
                          QString::number(pDataInfo->vValue.fValue, FORMAT_DECIMAL)+"A" );

        painter.restore();

//        TRACEDEBUG( "Wdg2P5BattRemainTime::paintEvent idxBranch<%d> <%f>Ah <%f>A <%f>%%",
//                    idxBranch-1,
//                    m_battInfoBuffed[idxBranch-1].BattItem[0].vValue.fValue,
//                    m_battInfoBuffed[idxBranch-1].BattItem[1].vValue.fValue,
//                    fCapacity
//                    );
    }
}

/******************************************************************************
文件名：    WdgFP3DCA.h
功能：      第一层界面p3 DC 电流负载趋势图
作者：      刘金煌
创建日期：   2013年04月27日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef WDGFP3DCA_H
#define WDGFP3DCA_H

#include "common/basicwidget.h"
#include "common/uidefine.h"
#include "common/MultiDemon.h"

using namespace CurveChart;
using namespace InputCtrl;

namespace Ui {
class WdgFP3DCA;
}

class WdgFP3DCA : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit WdgFP3DCA(QWidget *parent = 0);
    ~WdgFP3DCA();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();

protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);
    virtual void paintEvent(QPaintEvent *event);

signals:
    void goToBaseWindow(enum WIDGET_TYPE);

private:
    int      m_timerId;
    CmdItem  m_cmdItem;
    CurveParam_t m_curveParam;
    TEMP_DISP_FORMAT m_eTempDispFormat;

private:
    Ui::WdgFP3DCA *ui;
};

#endif // WDGFP3DCA_H

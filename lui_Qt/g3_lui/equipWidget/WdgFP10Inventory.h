#ifndef WDGFP10INVENTORY_H
#define WDGFP10INVENTORY_H

#include "common/basicwidget.h"
#include "common/uidefine.h"

namespace Ui {
class WdgFP10Inventory;
}

class WdgFP10Inventory : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit WdgFP10Inventory(QWidget *parent = 0);
    ~WdgFP10Inventory();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();

protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);

private:
    void setInvItem();
    void clearItemText();
    QString splitString(const QString strSep,
                     const QString& string,
                     const int nMaxCharsRow);

private slots:
    void sltTableKeyPress(int key);

private:
    int      m_timerId;
    CmdItem  m_cmdItem;
    int      m_nRows;

    typedef struct _Inv_Item{
        QString strItem;
        bool    bIsSigValue;
        int     iAlignType;//left,0; right,1;center,2;
    }Inv_Item_t;

    Inv_Item_t m_InvItems[20];
    int m_nPages;
    int m_nPageIdx;
    
private:
    Ui::WdgFP10Inventory *ui;
};

#endif // WDGFP10INVENTORY_H

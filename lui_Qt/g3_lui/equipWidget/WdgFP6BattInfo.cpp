/******************************************************************************
文件名：    WdgFP6BattInfo.cpp
功能：      第一层界面p6 电池信息环形图
           10.27 非标 电池容量改成电池电流屏
作者：      刘金煌
创建日期：   2014年10月27日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "WdgFP6BattInfo.h"
#include "ui_WdgFP6BattInfo.h"

#include <QPainter>
#include <QKeyEvent>
#include <QPixmap>
#include "common/pubInclude.h"
#include "common/global.h"
#include "config/configparam.h"
#include "config/PosBase.h"
#include "common/Macro.h"
#include "config/PosPieChart.h"

WdgFP6BattInfo::WdgFP6BattInfo(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::WdgFP6BattInfo)
{
    ui->setupUi(this);

    InitWidget ();
    InitConnect ();

    m_timerId = 0;
    m_cmdItem.CmdType   = CT_READ;
    m_cmdItem.ScreenID  = SCREEN_ID_WdgFP6BattInfo;
}

WdgFP6BattInfo::~WdgFP6BattInfo()
{
    delete ui;
}
void WdgFP6BattInfo::InitWidget()
{
    SET_BACKGROUND_WIDGET( "WdgFP6BattInfo",PosBase::strImgBack_Title );
    SET_STYLE_LABEL_ENTER2;
    if (LCD_ROTATION_90DEG == ConfigParam::ms_initParam.lcdRotation)
    {
        ui->label_enter->setGeometry(
                    PosBase::titleX2,
                    PosBase::titleY2,
                    PosBase::titleWidth2,
                    PosBase::titleHeight2Rows
                    );
        ui->label_enter->setAlignment (Qt::AlignVCenter);
        m_pixmapHand.load(PATH_IMG + "pie_pointer3V.png");
    }
    else
    {
       m_pixmapHand.load(PATH_IMG + "pie_pointer3.png");
    }
    SET_STYLE_SCROOLBAR( 3, 1 );

#ifdef TEST_GUI
    PACK_ICOINFO* info = (PACK_ICOINFO*)g_dataBuff;
    info->iDataNum = 2;
    info->DispData[0].iFormat   = 1;
    info->DispData[0].fSigValue = 0;
    strcpy(info->DispData[0].cSigName, "Batt Current");
    strcpy(info->DispData[0].cSigUnit, "A");

    info->DispData[1].iFormat   = 1;
    info->DispData[1].fSigValue = 100;

    ShowData( g_dataBuff );
#endif
}

void WdgFP6BattInfo::InitConnect()
{
}


void WdgFP6BattInfo::Enter(void* param)
{
    TRACELOG1( "WdgFP6BattInfo::Enter(void* param)" );
    Q_UNUSED( param );

    enum PIE_CHART_TYPE type = PIE_CHART_TYPE_DC;
    PosPieChart::setPieChartType( (void*)&type );

#ifdef TEST_GUI
    ENTER_FIRSTLY;
#else
    ENTER_GET_DATA;
#endif
}

void WdgFP6BattInfo::Leave()
{
    LEAVE_WDG( "WdgFP6BattInfo" );
}

void WdgFP6BattInfo::Refresh()
{
    REFRESH_DATA_PERIODICALLY(
                m_cmdItem.ScreenID,
                "WdgFP6BattInfo"
                );
}

void WdgFP6BattInfo::ShowData(void* pData)
{
    if ( !pData )
        return;

    PACK_ICOINFO* info = (PACK_ICOINFO*)pData;
    m_fSigVal = info->DispData[0].fSigValue;
    iFormat   = info->DispData[0].iFormat;
    QString strSigName( info->DispData[0].cSigName );
    QString strSigUnit( info->DispData[0].cSigUnit );
    float fMax = info->DispData[1].fSigValue;
    if (fMax < 0.01)
    {
        fMax = 0.1;
    }
    m_fMargins[0] = -fMax;
    m_fMargins[1] = fMax;

    TRACEDEBUG( "WdgFP6BattInfo::ShowData() read data "
                "dataNum<%d> sigVal<%f> "
                "m_fMargins<%f %f>",
                info->iDataNum, m_fSigVal,
                m_fMargins[0], m_fMargins[1]
                );

    QString strSigVal;
    if (m_fSigVal < INVALID_VALUE)
    {
        strSigVal = "--";
    }
    else
    {
        strSigVal = QString::number(m_fSigVal, FORMAT_DECIMAL);
    }
    QString strTitle = strSigName + ":";
    if (LCD_ROTATION_90DEG == ConfigParam::ms_initParam.lcdRotation)
    {
        strTitle.append( "\n" );
    }
    strTitle.append(
                strSigVal +
                strSigUnit +
                " >"
                );
    ui->label_enter->setText(strTitle );

    //update();
}

void WdgFP6BattInfo::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
}

void WdgFP6BattInfo::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void WdgFP6BattInfo::keyPressEvent(QKeyEvent* keyEvent)
{
    return QWidget::keyPressEvent(keyEvent);
}

// WdgFP0AC
void WdgFP6BattInfo::paintEvent(QPaintEvent *)
{
    // 11.4 不要图形显示
    return;

    QPainter painter(this);
    static QPixmap pixmapCover;//( PATH_IMG + "pie_cover.png" );
    static QPixmap pixmapCenter;//( PATH_IMG + "pie_center.png" );
    if(ConfigParam::ms_initParam.lcdRotation == LCD_ROTATION_90DEG)
    {
       pixmapCover.load(PATH_IMG + "pie_coverV.png");
       pixmapCenter.load(PATH_IMG + "pie_centerV.png");
    }
    else
    {
        pixmapCover.load(PATH_IMG + "pie_cover.png");
        pixmapCenter.load(PATH_IMG + "pie_center.png");
    }
    static QColor pieMarginColors[MARGIN_COUNT_CURRENT-1] = {
        QColor(0,255,0)
    };
    pieChart_paint(
                &painter,
                pieMarginColors,
                m_fMargins,
                MARGIN_COUNT_CURRENT,

                iFormat,
                &m_fSigVal,

                &m_pixmapHand,
                &pixmapCover,
                &pixmapCenter,
                false
                );
}


/******************************************************************************
文件名：    FirstStackedWdg.cpp
功能：     QStackedWidget 第一层界面
作者：      刘金煌
创建日期：   2013年04月27日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "FirstStackedWdg.h"
#include "ui_FirstStackedWdg.h"

#include <QComboBox>
#include <QKeyEvent>
#include "common/pubInclude.h"
#include "common/global.h"
#include "config/configparam.h"

FirstStackedWdg::FirstStackedWdg(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::FirstStackedWdg)
{
    ui->setupUi(this);

    InitWidget();
    InitConnect();

    m_wt = -1;
}

FirstStackedWdg::~FirstStackedWdg()
{
    delete ui;
}

void FirstStackedWdg::InitWidget()
{
    SET_GEOMETRY_WIDGET( this );
    SET_GEOMETRY_WIDGET( ui->stackedWidget );
}

void FirstStackedWdg::InitConnect()
{
    ;
}

void FirstStackedWdg::CreateDispWidget(int iWidgetType)
{
    switch(iWidgetType)
    {
        case WT1_AC:
        {
            WdgFP0AC *wdgFP0AC    = new WdgFP0AC(this);
            ui->stackedWidget->insertWidget(0, wdgFP0AC);
        }
        break;

        case WT1_MODULE:
        {
            WdgFP1Module* wdgFP1Module= new WdgFP1Module(this);
            connect( wdgFP1Module, SIGNAL(goToBaseWindow(enum WIDGET_TYPE)),
                     this, SIGNAL(goToBaseWindow(enum WIDGET_TYPE)) );
            ui->stackedWidget->insertWidget(0, wdgFP1Module);
        }
        break;

        case WT1_DCV:
        {
            WdgFP2DCV *wdgFP2DCV   = new WdgFP2DCV(this);
            ui->stackedWidget->insertWidget(0, wdgFP2DCV);
        }
        break;

        case WT1_DCA:
        {
            WdgFP3DCA* wdgFP3DCA   = new WdgFP3DCA(this);
            ui->stackedWidget->insertWidget(0, wdgFP3DCA);
        }
        break;


        case WT1_DCDEG_METER:
        {
            WdgFP4Deg1* wdgFP4Deg1  = new WdgFP4Deg1(this);
            ui->stackedWidget->insertWidget(0, wdgFP4Deg1);
        }
        break;

        case WT1_DCDEG_CURVE:
        {
            WdgFP5Deg2Curve* wdgFP5Deg2Curve = new WdgFP5Deg2Curve(this);
            ui->stackedWidget->insertWidget(0, wdgFP5Deg2Curve);
        }
        break;

        case WT1_BATT_REMAIN_TIME:
        {
            //Modified by alpha
            if(g_cfgParam->ms_initParam.dispVersion == DISPLAY_VERSION_NORMAL)
            {
              WdgFP6BattRemainTime* wdgFP6BattRemainTime = new WdgFP6BattRemainTime(this);
              ui->stackedWidget->insertWidget(0, wdgFP6BattRemainTime);
            }
            else
            {
              WdgFP6BattInfo* wdgFP6BattInfo = new WdgFP6BattInfo(this);
              ui->stackedWidget->insertWidget(0, wdgFP6BattInfo);
            }
        }
        break;

        case WT1_BATT_DEG_METER:
        {
            WdgFP7BattDegMeter* wdgFP7BattDegMeter = new WdgFP7BattDegMeter(this);
            ui->stackedWidget->insertWidget(0, wdgFP7BattDegMeter);
        }
        break;

        case WT1_BATT_DEG_CURVE:
        {
            WdgFP8BattDegCurve* wdgFP8BattDegCurve = new WdgFP8BattDegCurve(this);
            ui->stackedWidget->insertWidget(0, wdgFP8BattDegCurve);
        }
        break;

        case WT1_ALARM:
        {
            WdgAlmMenu* wdgAlmMenu = new WdgAlmMenu(this);
            TRACEDEBUG("FirstStackedWdg::CreateDispWidget wdgAlmMenu:%p",wdgAlmMenu);
            connect( wdgAlmMenu, SIGNAL(goToBaseWindow(enum WIDGET_TYPE)),
                     this, SIGNAL(goToBaseWindow(enum WIDGET_TYPE)) );

            connect(wdgAlmMenu,SIGNAL(goToHomePage()),
                    this,SIGNAL(goToHomePage()));

            ui->stackedWidget->insertWidget(0,wdgAlmMenu);
        }
        break;

        case WT1_INVENTORY:
        {
            // 10 inventory
            WdgFP10Inventory* wdgFP10Inventory      = new WdgFP10Inventory(this);
            ui->stackedWidget->insertWidget(0, wdgFP10Inventory);
        }
        break;


        case WT1_SCREENSAVER:
        {
            // 11 screenSaver
            WdgFP11ScreenSaver* wdgFP11ScreenSaver    = new WdgFP11ScreenSaver(this);
            ui->stackedWidget->insertWidget(0, wdgFP11ScreenSaver);
        }
        break;

        default:
        break;
    }
}

void FirstStackedWdg::Enter(void* param)
{
    INIT_VAR;

    int wt = *(int*)param;
    TRACEDEBUG( "FirstStackedWdg::Enter wt<%d> m_wt<%d>", wt, m_wt );
    if (wt<WT1_FIRST_MAX)
    {
        m_wt = wt;
    }

    CreateDispWidget(m_wt);

    ui->stackedWidget->setCurrentIndex( 0 );

    m_pCurWdt = (BasicWidget*)ui->stackedWidget->currentWidget();
    m_pCurWdt->setFocus();
    m_pCurWdt->Enter( param );
    Refresh();
}

void FirstStackedWdg::Leave()
{
    TRACEDEBUG( "FirstStackedWdg::Leave()" );

    m_pCurWdt = (BasicWidget*)ui->stackedWidget->currentWidget();
    m_pCurWdt->Leave();

    ui->stackedWidget->removeWidget(m_pCurWdt);

    TRACEDEBUG("FirstStackedWdg delete");
    delete m_pCurWdt;
    m_pCurWdt = NULL;
}

void FirstStackedWdg::Refresh()
{
    ;
}

void FirstStackedWdg::ShowData(void* pData)
{
    Q_UNUSED( pData );
}

void FirstStackedWdg::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void FirstStackedWdg::keyPressEvent(QKeyEvent* keyEvent)
{
    TRACEDEBUG("FirstStackedWdg::keyPressEvent");
    switch ( keyEvent->key() )
    {
        case Qt::Key_Return:
        case Qt::Key_Enter:
        {
            //TRACELOG1( "FirstStackedWdg::keyPressEvent Key_Enter" );
            switch (m_wt)
            {
                case WT1_DCA:
                {
                    emit goToBaseWindow( WT2_DCA_BRANCH );
                }
                break;

                case WT1_DCDEG_METER:
                {
                    emit goToBaseWindow( WT2_DCDEG_METER_BRANCH );
                }
                break;

                case WT1_BATT_REMAIN_TIME:
                {
                    emit goToBaseWindow( WT2_BATT_SINGLE_REMAIN_TIME );
                }
                break;

                case WT1_BATT_DEG_METER:
                {
                    emit goToBaseWindow( WT2_BATT_SINGLE_DEG );
                }
                break;

                case WT1_INVENTORY:
                {
                    emit goToBaseWindow( WT2_INVENTORY );
                }
                break;

                default:
                break;
            }
        }
        break;

        case Qt::Key_Escape:
        {
            emit goToHomePage();
        }
        break;

        case Qt::Key_Up:
        {
            switch (m_wt)
            {
                case WT1_DCA:
                case WT1_DCDEG_METER:
                case WT1_DCDEG_CURVE:
                case WT1_BATT_DEG_METER:
                case WT1_BATT_DEG_CURVE:
                {
                    emit goToBaseWindow( WIDGET_TYPE(m_wt-1) );
                }
                break;

                case WT1_DCV:
                {
                    emit goToBaseWindow( WT1_DCDEG_CURVE );
                }
                break;

                case WT1_BATT_REMAIN_TIME:
                {
                    emit goToBaseWindow( WT1_BATT_DEG_CURVE );
                }
                break;

                default:
                break;
            }
        }
        break;

        case Qt::Key_Down:
        {
            switch (m_wt)
            {
                case WT1_DCV:
                case WT1_DCA:
                case WT1_DCDEG_METER:

                case WT1_BATT_REMAIN_TIME:
                case WT1_BATT_DEG_METER:
                {
                    emit goToBaseWindow( WIDGET_TYPE(m_wt+1) );
                }
                break;

                case WT1_DCDEG_CURVE:
                {
                    emit goToBaseWindow( WT1_DCV );
                }
                break;

                case WT1_BATT_DEG_CURVE:
                {
                    emit goToBaseWindow( WT1_BATT_REMAIN_TIME );
                }
                break;

                default:
                break;
            }
        }
        break;
    }

    QWidget::keyPressEvent (keyEvent);
}

/******************************************************************************
文件名：    FirstStackedWdg.h
功能：     QStackedWidget 第一层界面p0 WdgFAC
作者：      刘金煌
创建日期：   2013年04月27日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef FIRSTSTACKEDWDG_H
#define FIRSTSTACKEDWDG_H

#include <QWidget>
#include <QStack>
#include "common/basicwidget.h"
#include "common/uidefine.h"

#include "equipWidget/WdgFP0AC.h"
#include "equipWidget/WdgFP1Module.h"
#include "equipWidget/WdgFP2DCV.h"
#include "equipWidget/WdgFP3DCA.h"
#include "equipWidget/WdgFP4Deg1.h"
#include "equipWidget/WdgFP5Deg2Curve.h"
#include "equipWidget/WdgFP6BattRemainTime.h"
#include "equipWidget/WdgFP7BattDegMeter.h"
#include "equipWidget/WdgFP8BattDegCurve.h"
#include "equipWidget/WdgFP10Inventory.h"
#include "equipWidget/WdgFP11ScreenSaver.h"
#include "equipWidget/WdgAlmMenu.h"
#include "equipWidget/WdgFP6BattInfo.h"

namespace Ui {
class FirstStackedWdg;
}

class FirstStackedWdg : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit FirstStackedWdg(QWidget *parent = 0);
    ~FirstStackedWdg();

protected:
    virtual void InitWidget();
    virtual void InitConnect();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();
    virtual void ShowData(void* pData);
    
protected:
    virtual void changeEvent(QEvent *event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);

private:
    void CreateDispWidget(int iWidgetType);

private:
    BasicWidget*         m_pCurWdt;
    int                  m_wt;

signals:
    void goToHomePage();
    void goToBaseWindow(enum WIDGET_TYPE);

private:
    Ui::FirstStackedWdg *ui;
};

#endif // FIRSTSTACKEDWDG_H

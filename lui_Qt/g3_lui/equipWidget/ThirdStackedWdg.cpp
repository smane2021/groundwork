/******************************************************************************
文件名：    ThirdStackedWdg.cpp
功能：      第三级信号窗口，告警 产品信息
作者：      刘金煌
创建日期：   2013年5月15日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "ThirdStackedWdg.h"
#include "ui_ThirdStackedWdg.h"

#include <QKeyEvent>
#include "common/pubInclude.h"
#include "common/global.h"
#include "config/configparam.h"
ThirdStackedWdg::ThirdStackedWdg(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::ThirdStackedWdg)
{
    ui->setupUi(this);

    InitWidget();
    InitConnect();

    SET_GEOMETRY_WIDGET( this );
    SET_GEOMETRY_WIDGET( ui->stackedWidget );

    m_wt = -1;
}

ThirdStackedWdg::~ThirdStackedWdg()
{
    delete ui;
}

void ThirdStackedWdg::InitWidget()
{

}

void ThirdStackedWdg::InitConnect()
{

}

void ThirdStackedWdg::CreateDispWdg(int iDispWdgType)
{
#define INI_TABLE_WDG(wt) \
    Wdg3Table* tbl_##wt = new Wdg3Table(wt, this); \
    connect( tbl_##wt, SIGNAL(goToBaseWindow(enum WIDGET_TYPE)), \
             this, SIGNAL(goToBaseWindow(enum WIDGET_TYPE)) ); \
    ui->stackedWidget->insertWidget(0,tbl_##wt);

    switch(iDispWdgType)
    {
        case WT3_ACT_ALARM:
        {
            INI_TABLE_WDG( WT3_ACT_ALARM );
            connect( tbl_WT3_ACT_ALARM, SIGNAL(goToHomePage()),
                    this, SIGNAL(goToHomePage()) );
        }
        break;

        case WT3_HIS_ALARM:
        {
            INI_TABLE_WDG( WT3_HIS_ALARM );
            connect( tbl_WT3_HIS_ALARM, SIGNAL(goToHomePage()),
                    this, SIGNAL(goToHomePage()) );
        }
        break;

        default:
          break;
    }
}

void ThirdStackedWdg::Enter(void* param)
{
    INIT_VAR;

    int wt = *(int*)param;
    if (wt<WT3_THIRD_MAX)
    {
        m_wt = wt;
    }

    CreateDispWdg(m_wt);

    ui->stackedWidget->setCurrentIndex( 0 );
    m_pCurWdt = (BasicWidget*)ui->stackedWidget->currentWidget();
    if(m_pCurWdt != NULL)
    {
        m_pCurWdt->setFocus();
        m_pCurWdt->Enter();
    }
}

void ThirdStackedWdg::Leave()
{
    TRACELOG1( "ThirdStackedWdg::Leave()" );
    m_pCurWdt = (BasicWidget*)ui->stackedWidget->currentWidget();
    m_pCurWdt->Leave();

    ui->stackedWidget->removeWidget(m_pCurWdt);

    TRACEDEBUG("delete SecondStackedWdg");
    delete m_pCurWdt;
    m_pCurWdt = NULL;
}

void ThirdStackedWdg::Refresh()
{
}

void ThirdStackedWdg::ShowData(void* pData)
{
    Q_UNUSED( pData );
}

void ThirdStackedWdg::timerEvent(QTimerEvent* event)
{
    Q_UNUSED( event );
//    if(event->timerId() == m_timerId)
//    {
//        TRACEDEBUG( "ThirdStackedWdg::timerEvent" );
//        Refresh();
//    }
}

void ThirdStackedWdg::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void ThirdStackedWdg::keyPressEvent(QKeyEvent* keyEvent)
{
    QWidget::keyPressEvent (keyEvent);
}

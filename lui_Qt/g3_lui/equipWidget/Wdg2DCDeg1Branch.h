/******************************************************************************
文件名：    Wdg2DCDeg1Branch.h
功能：      第二层界面p2 DC 温度条形图
作者：      刘金煌
创建日期：   2013年5月10日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef WDG2DCDEG1BRANCH_H
#define WDG2DCDEG1BRANCH_H

#include "common/basicwidget.h"
#include "common/uidefine.h"
#include "common/MultiDemon.h"

using namespace BarChart;
using namespace InputCtrl;

namespace Ui {
class Wdg2DCDeg1Branch;
}

class Wdg2DCDeg1Branch : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit Wdg2DCDeg1Branch(QWidget *parent = 0);
    ~Wdg2DCDeg1Branch();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();

protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);
    virtual void paintEvent(QPaintEvent* event);

signals:
    void goToBaseWindow(enum WIDGET_TYPE);

private:
    int      m_timerId;
    CmdItem  m_cmdItem;
    float    m_sigVals[MAX_ITEM_PERPAGE_BARCHART];
    bool     m_bResponds[MAX_ITEM_PERPAGE_BARCHART];
    int      m_sigIndexs[MAX_ITEM_PERPAGE_BARCHART];
    int m_nShowMaxItemCurPage; // 当前页显示几条数据
    int m_nPageIdx;
    int m_nPages;
    int  iFormat;
    float    m_fWarnSigVals[MAX_ITEM_PERPAGE_BARCHART][MAXNUM_NORLEVEL];
    TEMP_DISP_FORMAT m_eTempFormat;

private:
    Ui::Wdg2DCDeg1Branch *ui;
};

#endif // WDG2DCDEG1BRANCH_H

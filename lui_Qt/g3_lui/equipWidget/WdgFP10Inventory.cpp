/******************************************************************************
文件名：    WdgFP10Inventory.cpp
功能：      第一层界面p10 产品信息页
作者：      刘金煌
创建日期：   2013年06月27日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "WdgFP10Inventory.h"
#include "ui_WdgFP10Inventory.h"

#include <QKeyEvent>
#include "config/configparam.h"
#include "common/pubInclude.h"
#include "common/global.h"

/* modify by wufang,20131010,modify:1/6,for slave mode */
#include "basicWidget/homepagewindow.h"

WdgFP10Inventory::WdgFP10Inventory(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::WdgFP10Inventory)
{
    ui->setupUi(this);
    SET_GEOMETRY_WIDGET( this );

    InitWidget();
    InitConnect();

    m_timerId = 0;
    m_cmdItem.CmdType   = CT_READ;
    m_cmdItem.ScreenID  = SCREEN_ID_WdgFP10Inventory;
}

WdgFP10Inventory::~WdgFP10Inventory()
{
    TRACEDEBUG("WdgFP10Inventory::~WdgFP10Inventory");
    ui->tableWidget->clearItems();
    delete ui;
}

void WdgFP10Inventory::InitWidget()
{
    SET_BACKGROUND_WIDGET( "WdgFP10Inventory",PosBase::strImgBack_Line );
    SET_TABLEWDG_STYLE( tableWidget, TT_NOT_TITLE_YES_SCROOL );
    ui->tableWidget->setGeometry(
                PosTableWidget::xNotTitle,
                PosTableWidget::yNotTitle,
                PosTableWidget::widthHasScrool,
                PosTableWidget::heightHasTitle
                );

    ui->label_enter->setGeometry(
                PosTableWidget::xHasTitle,
                PosBase::screenHeight-PosBase::titleHeightInv,
                PosTableWidget::widthHasScrool,
                PosBase::titleHeightInv
                );
    ui->label_enter->setAlignment( Qt::AlignCenter );

    ui->label_enter->setStyleSheet( \
    "color:#" LIGHT_COLOR_ENTER ";" \
    "background-color: rgb(241,241,241);" \
    ); \
    ui->label_enter->setFont(g_cfgParam->gFontLargeB); \

    ui->tableWidget->clearItems();
    ui->tableWidget->setRowCount( 6 );
    QTableWidgetItem *item = NULL;
    for (int i=0; i<6; ++i)
    {
        item = new QTableWidgetItem;
        item->setText( "" );
        ui->tableWidget->setItem(i, 0, item);
        ui->tableWidget->setRowHeight(i, TABLEWDG_ROW_HEIGHT);
    }

    // modify by wufang,20131010,modify:2/6,for slave mode
    ui->label_enter->show();
    if (HomePageWindow::ms_homePageType == HOMEPAGE_TYPE_SLAVE)
    {
        ui->label_enter->hide();
    }

    m_nPages = 1;
    m_nPageIdx = 1;

    ui->label_enter->setText( tr("ENT to inventory") );
    SET_STYLE_SCROOLBAR(m_nPages, m_nPageIdx);
}

void WdgFP10Inventory::InitConnect()
{
    connect( ui->tableWidget, SIGNAL(sigTableKeyPress(int)),
             this, SLOT(sltTableKeyPress(int)) );
}


void WdgFP10Inventory::Enter(void* param)
{
    TRACELOG1( "WdgFP10Inventory::Enter(void* param)" );
    Q_UNUSED( param );

    INIT_VAR;

    m_nPageIdx = 1;
    setFocus();

#ifdef TEST_GUI
    ENTER_FIRSTLY;
#else
    ENTER_GET_DATA;
#endif
}

void WdgFP10Inventory::Leave()
{
    LEAVE_WDG( "WdgFP10Inventory" );
    ui->tableWidget->clearItems();
    this->deleteLater();
}

void WdgFP10Inventory::Refresh()
{
    REFRESH_DATA_PERIODICALLY(
                m_cmdItem.ScreenID,
                "WdgFP10Inventory"
                );
}

void WdgFP10Inventory::ShowData(void* pData)
{
    if ( !pData )
        return;

    TRACEDEBUG( "WdgFP10Inventory::ShowData()" );

    m_nRows    = 0;

    PACK_SELFINV* info = (PACK_SELFINV*)pData;
    QString strTmp;

    strTmp = tr("Name") + ": " + info->cPartNumber;
    m_InvItems[m_nRows].strItem     = strTmp;
    m_InvItems[m_nRows].bIsSigValue = false;
    m_InvItems[m_nRows].iAlignType = 0;//left,0; right,1;center,2;

    strTmp = tr("SN") + ": " + info->cSN;
    m_nRows++;
    m_InvItems[m_nRows].strItem     = strTmp;
    m_InvItems[m_nRows].bIsSigValue = false;
    m_InvItems[m_nRows].iAlignType = 0;//left,0; right,1;center,2;

    //changed by Hrishikesh Oak,20190904,for CR - Improve the ability to determine a controller’s IP address
    unsigned long mainIP = (unsigned long)info->MAIN_IP;
    unsigned long FrontCraftIP = (unsigned long)info->FRONT_CRAFT_IP;
    if (mainIP != 0xffffffff && mainIP != FrontCraftIP)
    {
        strTmp = tr("Network") + ": ";
        //end -- changed by Hrishikesh Oak,20190904,for CR - Improve the ability to determine a controller’s IP address
        BYTE bysIp[4];
        DEPART_IP( bysIp, mainIP );
        for (int i=3; i>0; --i)
        {
            strTmp.append( QString::number(bysIp[i]) );
            strTmp.append( "." );
        }
        strTmp.append( QString::number(bysIp[0]) );
        m_nRows++;
        m_InvItems[m_nRows].strItem     = strTmp;
        m_InvItems[m_nRows].bIsSigValue = false;
        m_InvItems[m_nRows].iAlignType = 0;//left,0; right,1;center,2;
    }

    //changed by Hrishikesh Oak,20190904,for CR - Improve the ability to determine a controller’s IP address
    strTmp = QObject::tr("Front") + ": ";
    BYTE bysIp[4];
    DEPART_IP( bysIp, FrontCraftIP );
    for (int i=3; i>0; --i)
    {
        strTmp.append( QString::number(bysIp[i]) );
        strTmp.append( "." );
    }
    strTmp.append( QString::number(bysIp[0]) );
    m_nRows++;
    m_InvItems[m_nRows].strItem     = strTmp;
    m_InvItems[m_nRows].bIsSigValue = false;
    m_InvItems[m_nRows].iAlignType = 0;//left,0; right,1;center,2;

    strTmp = tr("SW Ver") + ": " + info->cSWver;
    m_nRows++;
    m_InvItems[m_nRows].strItem     = strTmp;
    m_InvItems[m_nRows].bIsSigValue = false;
    m_InvItems[m_nRows].iAlignType = 0;//left,0; right,1;center,2;

    QString strSigName = tr("Config Ver");
    QString strCfgFileVer( info->cCfgFileVer );
    strTmp = strSigName + ": " + strCfgFileVer;
    if ( fmLargeN->width(strTmp) >= PosTableWidget::widthWords )
    {
        strTmp = strSigName + ": ";
        m_nRows++;
        m_InvItems[m_nRows].strItem     = strTmp;
        m_InvItems[m_nRows].bIsSigValue = false;
        m_InvItems[m_nRows].iAlignType = 0;//left,0; right,1;center,2;

        strTmp = strCfgFileVer;
        m_nRows++;
        m_InvItems[m_nRows].strItem     = strTmp;
        m_InvItems[m_nRows].bIsSigValue = true;
        m_InvItems[m_nRows].iAlignType = 0;//left,0; right,1;center,2;
    }
    else
    {
        m_nRows++;
        m_InvItems[m_nRows].strItem     = strTmp;
        m_InvItems[m_nRows].bIsSigValue = false;
        m_InvItems[m_nRows].iAlignType = 0;//left,0; right,1;center,2;
    }

    strTmp = tr("HW Ver") + ": " + info->cPVer;
    m_nRows++;
    m_InvItems[m_nRows].strItem     = strTmp;
    m_InvItems[m_nRows].bIsSigValue = false;
    m_InvItems[m_nRows].iAlignType = 0;//left,0; right,1;center,2;

    strTmp = tr("File Sys") + ": " + info->cFileSystemVer;
    m_nRows++;
    m_InvItems[m_nRows].strItem     = strTmp;
    m_InvItems[m_nRows].bIsSigValue = false;
    m_InvItems[m_nRows].iAlignType = 0;//left,0; right,1;center,2;

    strTmp = tr("MAC Address") + ":";
    m_nRows++;
    m_InvItems[m_nRows].strItem     = strTmp;
    m_InvItems[m_nRows].bIsSigValue = false;
    m_InvItems[m_nRows].iAlignType = 0;//left,0; right,1;center,2;

    strTmp = QString(info->cEthaddr);
    m_nRows++;
    m_InvItems[m_nRows].strItem     = strTmp;
    m_InvItems[m_nRows].bIsSigValue = true;
    m_InvItems[m_nRows].iAlignType = 0;//left,0; right,1;center,2;
#if 0
    unsigned long dhcpIP = (unsigned long)info->DHCP_IP;
    if (dhcpIP != 0xffffffff)
    {
        strTmp = tr("DHCP Server IP") + ":";
        m_nRows++;
        m_InvItems[m_nRows].strItem     = strTmp;
        m_InvItems[m_nRows].bIsSigValue = false;
		m_InvItems[m_nRows].iAlignType = 0;//left,0; right,1;center,2;

        BYTE bysIp[4];
        DEPART_IP( bysIp, dhcpIP );
        strTmp.clear();
        for (int i=3; i>0; --i)
        {
            strTmp.append( QString::number(bysIp[i]) );
            strTmp.append( "." );
        }
        strTmp.append( QString::number(bysIp[0]) );
        m_nRows++;
        m_InvItems[m_nRows].strItem     = strTmp;
        m_InvItems[m_nRows].bIsSigValue = true;
        m_InvItems[m_nRows].iAlignType = 0;//left,0; right,1;center,2;
    }
#endif
//end -- changed by Hrishikesh Oak,20190904,for CR - Improve the ability to determine a controller’s IP address
    //ipv6 Local ip
    if(info->cIpv6Local[0] != 0)
    {
        strTmp = tr("Link-Local Addr") + ":";
        m_nRows++;
        m_InvItems[m_nRows].strItem     = strTmp;
        m_InvItems[m_nRows].bIsSigValue = false;
        m_InvItems[m_nRows].iAlignType = 0;//left,0; right,1;center,2;

        strTmp = QString(info->cIpv6Local).toUpper();
        strTmp = splitString(":", strTmp, g_cfgParam->ms_maxCharsPerTableRow);
        QStringList strList = strTmp.split( "^", QString::KeepEmptyParts);
        int nSectionNum = strList.count();
        for (int i=0; i<nSectionNum; ++i)
        {
            m_nRows++;
            m_InvItems[m_nRows].strItem     = strList[i];
            m_InvItems[m_nRows].bIsSigValue = true;
            m_InvItems[m_nRows].iAlignType = 0;
        }
    }

    //ipv6 Global ip
    if(info->cIpv6Global[0] != 0)
    {
        strTmp = tr("Global Addr") + ":";
        m_nRows++;
        m_InvItems[m_nRows].strItem     = strTmp;
        m_InvItems[m_nRows].bIsSigValue = false;
        m_InvItems[m_nRows].iAlignType = 0;//left,0; right,1;center,2;

        strTmp = QString(info->cIpv6Global).toUpper();
        strTmp = splitString(":", strTmp, g_cfgParam->ms_maxCharsPerTableRow);
        QStringList strList = strTmp.split( "^", QString::KeepEmptyParts);
        int nSectionNum = strList.count();
        for (int i=0; i<nSectionNum; ++i)
        {
            m_nRows++;
            m_InvItems[m_nRows].strItem     = strList[i];
            m_InvItems[m_nRows].bIsSigValue = true;
            m_InvItems[m_nRows].iAlignType = 0;
        }
    }

    //ipv6 DHCP
    if (info->cIpv6DHCPServer[0] != 0)
    {
        strTmp = tr("DHCP Server IPV6") + ":";
        m_nRows++;
        m_InvItems[m_nRows].strItem     = strTmp;
        m_InvItems[m_nRows].bIsSigValue = false;
        m_InvItems[m_nRows].iAlignType = 0;//left,0; right,1;center,2;

        strTmp = QString(info->cIpv6DHCPServer).toUpper();
        strTmp = splitString(":", strTmp, g_cfgParam->ms_maxCharsPerTableRow);
        QStringList strList = strTmp.split( "^", QString::KeepEmptyParts);
        int nSectionNum = strList.count();
        for (int i=0; i<nSectionNum; ++i)
        {
            m_nRows++;
            m_InvItems[m_nRows].strItem     = strList[i];
            m_InvItems[m_nRows].bIsSigValue = true;
            m_InvItems[m_nRows].iAlignType = 0;
        }
    }

    m_nRows++;
    TRACEDEBUG( "WdgFP10Inventory::ShowData() read data "
                "IPV6 Local<%s> Global<%s> DHCP<%s> "
                "cPartNumber<%s> cSN<%s> cSWver<%s> dhcpIP<%x> mainIP<%x> MAC<%s> m_nRows<%d>",
                info->cIpv6Local,
                info->cIpv6Global,
                info->cIpv6DHCPServer,
                info->cPartNumber,
                info->cSN,
                info->cSWver,
                //dhcpIP,
                mainIP,
                info->cEthaddr,
                m_nRows
               );

    setInvItem();
}

void WdgFP10Inventory::setInvItem()
{
	//Frank Wu,20140814
//    int nModNum   = m_nRows%TABLEWDG_ROWS_PERPAGE_TITLE;
//    m_nPages = ( nModNum ? (m_nRows/TABLEWDG_ROWS_PERPAGE_TITLE+1):
//                (m_nRows/TABLEWDG_ROWS_PERPAGE_TITLE) );

    int i;
    int iPageStartList[70];
    int iCurPageIdStart, iCurPageIdStop;

    m_nPages = 0;
    iCurPageIdStart = 0;
    iCurPageIdStop = iCurPageIdStart;
    iPageStartList[m_nPages] = iCurPageIdStart;
    for(i = 0; i < m_nRows; i++)
    {
        if( !m_InvItems[i].bIsSigValue )//a key-value start or stop
        {
            if((i - iCurPageIdStart) > TABLEWDG_ROWS_PERPAGE_TITLE)//start a new page
            {
                m_nPages++;
                iCurPageIdStart = iCurPageIdStop;//new page start is the last page stop
                iPageStartList[m_nPages] = iCurPageIdStart;
            }

            iCurPageIdStop = i;
        }
    }
    if((i - iCurPageIdStart) > TABLEWDG_ROWS_PERPAGE_TITLE)//start a new page
    {
        m_nPages++;
        iCurPageIdStart = iCurPageIdStop;//new page start is the last page stop
        iPageStartList[m_nPages] = iCurPageIdStart;
    }
    m_nPages++;
    iPageStartList[m_nPages] = m_nRows;

    if (m_nPageIdx < 1)
    {
        m_nPageIdx = m_nPages;
    }
    if (m_nPageIdx > m_nPages)
    {
        m_nPageIdx = 1;
    }

    clearItemText();
    SET_STYLE_SCROOLBAR(m_nPages, m_nPageIdx);
    QTableWidgetItem* item = NULL;
    int nTableRow = 0;
    for(i = iPageStartList[m_nPageIdx - 1]; i < iPageStartList[m_nPageIdx]; i++)
    {
        item = ui->tableWidget->item( nTableRow, 0 );
        item->setText( m_InvItems[i].strItem );

        if ( m_InvItems[i].iAlignType == 0 )
        {
            item->setTextAlignment( Qt::AlignLeft );
        }
        else if ( m_InvItems[i].iAlignType == 1 )
        {
            item->setTextAlignment( Qt::AlignRight );
        }
        else if ( m_InvItems[i].iAlignType == 2 )
        {
            item->setTextAlignment( Qt::AlignCenter );
        }
        else
        {
            item->setTextAlignment( Qt::AlignCenter );
        }

        nTableRow++;
    }
    TRACEDEBUG("WdgFP10Inventory::setInvItem # Ending ...");
}

void WdgFP10Inventory::clearItemText()
{
    QTableWidgetItem *item = NULL;
    for (int i=0; i<6; ++i)
    {
        item = ui->tableWidget->item(i, 0);
        item->setText( "" );
    }
}

void WdgFP10Inventory::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
}

void WdgFP10Inventory::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void WdgFP10Inventory::keyPressEvent(QKeyEvent* keyEvent)
{
    switch ( keyEvent->key() )
    {
    case Qt::Key_Up:
        --m_nPageIdx;
        setInvItem();
        break;

    case Qt::Key_Down:
        ++m_nPageIdx;
        setInvItem();
        break;

	/* modify by wufang,20131010,modify:3/6,for slave mode */
    case Qt::Key_Enter:
    {
        if (HomePageWindow::ms_homePageType == HOMEPAGE_TYPE_SLAVE)
        {
            return;
        }
        break;
    }

    default:
        break;
    }
	QWidget::keyPressEvent(keyEvent);
}

void WdgFP10Inventory::sltTableKeyPress(int key)
{
    Q_UNUSED( key );
}

// 根据屏大小分割字符串用于分行显示
// 如"DDDD:DDDD:DDDD:DDDD:DDDD:DDDD:DDDD:DDDD"
// 返回"DDDD:DDDD:DDDD DDDD:DDDD:DDDD DDDD:DDDD"
QString WdgFP10Inventory::splitString(const QString strSep,
                                   const QString& string,
                                   const int nMaxCharsRow)
{
    int nLenStr = string.length();
    if (nLenStr < nMaxCharsRow)
    {
        return string;
    }

    QString strReturn;
    int nWordCount = string.split(strSep).count();
    QString strSectionBack;
    QString strSectionFore;
    int idxStart = 0;
    for (int i=0; i<nWordCount; ++i)
    {
        strSectionBack = string.section( strSep, idxStart, i );
        strSectionFore = string.section( strSep, idxStart, i+1 );
//        TRACEDEBUG( "WdgFP10Inventory::splitString i<%d> nWordCount<%d> strSectionBack<%s> strSectionFore<%s>",
//                    i,
//                    nWordCount,
//                    strSectionBack.toUtf8().constData(),
//                    strSectionFore.toUtf8().constData()
//                    );
//        TRACEDEBUG( "WdgFP10Inventory::splitString string width<%d>", fmLargeN->width(strSectionFore+strSep+strSep) );
        if (fmLargeN->width(strSectionFore+strSep) >= (PosTableWidget::widthWords-5) ||
                i>=nWordCount-1)
        {
            strReturn += (strSectionBack+strSep+"^");
            idxStart = i+1;
        }
    }

    TRACEDEBUG( "WdgFP10Inventory::splitString strReturn<%s>", strReturn.mid(0, strReturn.length()-2).toUtf8().constData() );
    return strReturn.mid(0, strReturn.length()-2);
}

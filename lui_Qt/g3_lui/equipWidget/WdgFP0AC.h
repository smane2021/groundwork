/******************************************************************************
文件名：    WdgFP0AC.h
功能：      第一层界面p0 AC
作者：      刘金煌
创建日期：   2013年04月27日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef WDGFP0AC_H
#define WDGFP0AC_H

#include "common/basicwidget.h"
#include "common/uidefine.h"
#include "common/MultiDemon.h"

using namespace PieChart;

namespace Ui {
class WdgFP0AC;
}

class WdgFP0AC : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit WdgFP0AC(QWidget *parent = 0);
    ~WdgFP0AC();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();

protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);
    virtual void paintEvent(QPaintEvent* event);

signals:
    void goToBaseWindow(enum WIDGET_TYPE);

private:
    int      m_timerId;
    CmdItem  m_cmdItem;
    QPixmap  m_pixmapHand[3];
    float    m_fMargins[LIMNUM];  // [0]-上上限；[1]-上限；[2]-下限；[3]-下下限
    float    m_fSigVals[3];     // 相电压
    QString  m_strSigName[3];
    QString  m_strSigUnit[3];
    int      iFormat;

    QPixmap pixmapCover;
    QPixmap pixmapCenter;

private:
    Ui::WdgFP0AC *ui;
};

#endif // WDGFP0AC_H

/******************************************************************************
文件名：    WdgFP7BattDegMeter.h
功能：      第一层界面p7 温度计
作者：      刘金煌
创建日期：   2013年5月14日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef WDGFP7BATTDEGMETER_H
#define WDGFP7BATTDEGMETER_H

#include "common/basicwidget.h"
#include "common/uidefine.h"
#include "common/MultiDemon.h"

using namespace ThermometerChart;
using namespace InputCtrl;

namespace Ui {
class WdgFP7BattDegMeter;
}

class WdgFP7BattDegMeter : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit WdgFP7BattDegMeter(QWidget *parent = 0);
    ~WdgFP7BattDegMeter();
    
public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();

protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);
    virtual void paintEvent(QPaintEvent* event);

signals:

private:
    int m_timerId;
    CmdItem  m_cmdItem;
    QPixmap  m_pixmapHand;
    float    m_fMargins[LIMNUM];  // [0] [1]-下下限；[2]-下限；[3]-上限；[4]-上上限 [5]
    float    m_fSigVal;
    QString  m_strSigName;
    QString  m_strSigUnit;
    TEMP_DISP_FORMAT m_eTempDispFormat;

private:
    Ui::WdgFP7BattDegMeter *ui;
};

#endif // WDGFP7BATTDEGMETER_H

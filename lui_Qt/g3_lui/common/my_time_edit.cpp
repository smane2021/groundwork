#include "my_time_edit.h"
#include "pubInclude.h"

My_Time_Edit::My_Time_Edit(QWidget *parent) :
    QTimeEdit(parent)
{
}

My_Time_Edit::~My_Time_Edit()
{
}

void My_Time_Edit::Enter(void*)
{
    setFocus();
    lineEdit()->setCursorPosition( 0 );
}

void My_Time_Edit::Leave()
{
}

void My_Time_Edit::keyPressEvent(QKeyEvent *keyEvent)
{
    int sectionIdx = currentSectionIndex();
    lineEdit()->setCursorPosition( sectionIdx*3 );
    TRACEDEBUG( "My_Time_Edit::keyPressEvent cursorPosition<%d> section<%d>",
                lineEdit()->cursorPosition(),
                sectionIdx );
    int key = keyEvent->key();
    if (key == Qt::Key_Up)
    {
        QTime tm = this->time();
        QTime tmSet = tm;
        switch ( sectionIdx )
        {
        case 0:
            if(tm.hour() == 23)
            {
                tmSet.setHMS(0,tm.minute(),tm.second());
            }
            break;

        case 1:
            if(tm.minute() == 59)
            {
                tmSet.setHMS(tm.hour(),0,tm.second());
            }
            break;

        case 2:
            if(tm.second() == 59)
            {
                tmSet.setHMS(tm.hour(),tm.minute(),0);
            }
            break;
        }
        if(tm != tmSet)
        {
            this->setTime(tmSet);
            return;
        }
    }
    else if(key == Qt::Key_Down)
    {
        QTime tm = this->time();
        QTime tmSet = tm;
        switch ( sectionIdx )
        {
        case 0:
            if(tm.hour() == 0)
            {
                tmSet.setHMS(23,tm.minute(),tm.second());
            }
            break;

        case 1:
            if(tm.minute() == 0)
            {
                tmSet.setHMS(tm.hour(),59,tm.second());
            }
            break;

        case 2:
            if(tm.second() == 0)
            {
                tmSet.setHMS(tm.hour(),tm.minute(),59);
            }
            break;
        }
        if(tm != tmSet)
        {
            this->setTime(tmSet);
            return;
        }
    }

    return QTimeEdit::keyPressEvent( keyEvent );
}

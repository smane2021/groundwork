#ifndef MY_TIME_EDIT_H
#define MY_TIME_EDIT_H

#include <QTimeEdit>
#include <QKeyEvent>
#include <QSpinBox>
#include <QLineEdit>

class My_Time_Edit : public QTimeEdit
{
    Q_OBJECT
public:
    explicit My_Time_Edit(QWidget *parent = 0);
    ~My_Time_Edit();
    
public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();

protected:
    virtual void keyPressEvent(QKeyEvent *keyEvent);
    
};

#endif // MY_TIME_EDIT_H

/******************************************************************************
文件名：    MultiDemo.h
功能：      提取的复用部分代码
作者：      刘金煌
创建日期：   2013年6月5日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef MULTIDEMON_H
#define MULTIDEMON_H

#include "common/uidefine.h"
#include "CtrlInputChar.h"
#include "dataSource.h"
//#include "config/configparam.h"

#define MIN_TEMP_VALUE  (-25)
#define MAX_TEMP_VALUE  (75)

#define MIN_DEG_F_TEMP_VALUE    (-13)
#define MAX_DEG_F_TEMP_VALUE    (167)

#define MIN_CURRENT_VALUE (0)
#define MAX_CURRENT_VALUE (100)


void tellAppToReboot();
void tellAppToResetPassword();
//////////////////////////////////////////////////////////
// 输入控件
namespace InputCtrl {
#define SET_PARAM_ERR_OK          (0)
#define SET_PARAM_ERR_SAME_VALUE  (0x8001)  // the same value
#define SET_PARAM_ERR_FAILED      (0x8002)
#define SET_PARAM_ERR_REJECTED    (0x8003)

#define SET_PARAM_ERR_OK_ECO      (0x8011)
#define SET_PARAM_ERR_OK_LANG     (0x8012)

int makeCtrlparamBySigtype(
        CtrlInputParam* pCtrlParam,
        SET_INFO* pSetInfoItem
        );
//
// describe: set parameter
// parameter:
//   @bSubmitSameData:whether to set when same data
int submitAppParamData(
        CtrlInputChar* pCtrl,
        CmdItem* pCmdItemSetting,
        int sigIdx,
        CtrlInputParam* pCtrlParamSetting
        );

// set special parameter including enum SIGID_SPECIAL
int setSpecialParam(
        CmdItem *pCmdItemOld,
        int sigID,
        long lValue
        );

// LCD rotation
int setLCDRotation(enum LCD_ROTATION rotation);

//获取温度的显示格式
TEMP_DISP_FORMAT GetTempFormat();

} // end namespace

//////////////////////////////////////////////////////////
// 控件模块灯闪
// 需要控制亮灯的类型ID
//模块类型ID:201
//Converter 类型ID: 1101
//MPPT 类型ID:2501
namespace CtrlLight {
#define TIME_ELAPSED_CTRL_LIGHT ( 600 )
typedef struct tagCtrlLightInfo {
    tagCtrlLightInfo()
    {
        screenID   = -1;
        iEqIDType  = -1;
        iEquipID   = -1;
        iSigID     = -1;
        bSendCmd   = false;
    }
    int   screenID;
    int   iEqIDType;
    int   iEquipID;
    int   iSigID;
    bool  bSendCmd;
    QTime timeElapsed;
    enum MODULE_TYPE moduleType;
} CtrlLightInfo_t;

int sendCmdCtrlLight(
        int screenID,
        int equipID,
        enum MODULE_LIGHT_TYPE lightType,
        enum MODULE_TYPE moduleType/*SetInformation.SigID*/
        );

int getModuleType(int iEqIDType);
int getModuleTypeByWt(int wt);

} // end namespace CtrlLight

//////////////////////////////////////////////////////////
// 条形图 Wdg2DCDeg1Branch Wdg2P6BattDeg
// (excluding Wdg2DCABranch Wdg2P5BattRemainTime)
namespace BarChart {

extern int g_nMaxItemPerpage;
extern int g_nMaxXMarks;
#define MAX_ITEM_PERPAGE_BARCHART          7
#define SCALES_X_DEG_F_BARCHART            5  // 华氏度X轴刻度个数
#define SCALES_X_BARCHART                  4  // X轴刻度个数
// X各刻度占用像素
#define SCALELENX_BARCHART ( \
    (PosBarChart::hAxisLen-PosBase::lastMarkLenAxis)*1.0/ \
    g_nMaxXMarks )

// Y各刻度占用像素
#define SCALELENY_BARCHART ( \
    (PosBarChart::vAxisLen-PosBase::lastMarkLenAxis)*1.0/ \
    g_nMaxItemPerpage )

/*
 -75~100
#define calcPointX_BARCHART(val) \
    ((PosBarChart::hAxisLen-PosBase::lastMarkLenAxis)*1.0)/175*(val+75)
*/

// -25~75
//#define calcPointX_BARCHART(val) \
//    ((PosBarChart::hAxisLen-PosBase::lastMarkLenAxis)*1.0)/100*(val+25)

//-13-167
float calcPointX_BARCHART(int val,int base);

#define SCALES_X_BARCHART3    4  // X轴刻度个数
#define calcPointX_BARCHART3(val) \
    ((PosBarChart::hAxisLen-PosBase::lastMarkLenAxis)*1.0)/nMaxXval*val

void barChart_paint(QPainter  *pPainter,
                    int       iFormat,
                    float     *sigVals,
                    bool      *bResponds,
                    int       *sigIndexs,
                    int       nShowMaxItemCurPage,
                    int       nPageIdx,
                    float     warnSigVals[MAX_ITEM_PERPAGE_BARCHART][MAXNUM_NORLEVEL],
                    TEMP_DISP_FORMAT tempDispFormat
                    );
} // end namespace


//////////////////////////////////////////////////////////
// 饼图 WdgFP0AC WdgFP2DCV
namespace PieChart {

#define MARGIN_COUNT_REMAIN_TIME (4)
// the angle of -x
//#define Angle_NegativeX (-28)
#define Angle_NegativeX (PosPieChart::angleNegativeAxis)
// 总角度 240
//#define Angles_PieChart (360-(180+Angle_NegativeX*2))
#define Angles_PieChart (PosPieChart::anglesPieChart)

// 饼图角度 与+x轴的逆时针方向角度
#define calcPieAngle_PieChart(fVal, fLowest, fHighest) \
    (int)((180-Angle_NegativeX-(fVal-fLowest)*1.0/ \
    (fHighest-fLowest)*Angles_PieChart)*16)

// 指针 旋转坐标系角度 与-x顺时针方向角度
#define calcCoordinateAngle_PieChart(fVal, fLowest, fHighest) \
    (PosPieChart::calcCoordinateAngle(fVal, fLowest, fHighest))

// 指针 旋转坐标系角度 顺时针方向 电池
#define calcCoordinateAngle_PieChart_Batt(fVal, fLowest, fHighest) \
    ((fVal-fLowest)*1.0/ \
    (fHighest-fLowest)*130+Angle_NegativeX)

/* 180 度饼图
// 饼图角度 逆时针方向
#define calcPieAngle_PieChart(fVal, fLowest, fHighest) \
    (int)((180-(fVal-fLowest)*1.0/ \
    (fHighest-fLowest)*180)*16)

// 指针 旋转坐标系角度 顺时针方向
#define calcCoordinateAngle_PieChart(fVal, fLowest, fHighest) \
    (fVal-fLowest)*1.0/ \
    (fHighest-fLowest)*180
// 指针 旋转坐标系角度 顺时针方向 电池
#define calcCoordinateAngle_PieChart_Batt(fVal, fLowest, fHighest) \
    (fVal-fLowest)*1.0/ \
    (fHighest-fLowest)*103
*/

void pieChart_paint(QPainter* pPainter,
                    QColor* colorMargins,
                    float* fMargins,
                    int nMarginNum,

                    int iFormat,
                    float* fSigVals,

                    QPixmap* pPixmapPointers,
                    QPixmap* pPixmapCover,
                    QPixmap* pPixmapCenter,
                    bool bAC
                    );
} // end namespace

//////////////////////////////////////////////////////////
// 温度计图 WdgFP4Deg1 WdgFP7BattDegMeter
namespace ThermometerChart {

#define WIDTH_PEN_THERMOMETER      3

float calcPtX(float fVal, int nWidthThermometer, float* fMargins);

#define calcPtX_Thermometer(fVal) \
  (((PosThermometer::meterWidth - 8*PosThermometer::penWidth) /(fMargins[LIMNUM-1]-fMargins[0]))* (fVal-fMargins[0]))

#define calcPtX_Rect(fVal) \
  ((PosThermometer::meterWidth /(fMargins[LIMNUM-1]-fMargins[0]))* (fVal-fMargins[0]))

void degThermometerChart_paint(
        QPainter* painter,
        QColor* colorMargins,
        float*  fMargins,
        float   fSigVal,
        QPixmap* pixmapPointer
        );

} // end namespace

//////////////////////////////////////////////////////////
// 曲线图 WdgFP3DCA WdgFP5Deg2Curve WdgFP8BattDegCurve
namespace CurveChart {
// X刻度个数
#define MARKS_X_CurveChart         (7)

// Y刻度个数
#define MARKS_Y_CURRENT_CurveChart  (4)
#define MARKS_Y_CURRENT_DEG_F_CurveChart    (5)
#define MARKS_Y_DEG_CurveChart     (4)

#define CURVE_WIDTH_CURVECHART      (2)

#define pixsPerMarkX_CurveChart ( (PosCurve::hAxisLen- \
    PosBase::lastMarkLenAxis)/MARKS_X_CurveChart )

#define pixsPerMarkY_CurveChart(marksY) ( (PosCurve::vAxisLen- \
    PosBase::lastMarkLenAxis)/marksY )

#define calcPtY_CURRENT_CurveChart(fVal, pixsPerMarkY) \
    (-fVal/25.0*pixsPerMarkY);

//摄氏度
#define calcPtY_DEG_C_CurveChart(fVal, pixsPerMarkY) \
    (-(fVal-MIN_TEMP_VALUE)/25.0*pixsPerMarkY);

//华氏度
#define calcPtY_DEG_F_CurveChart(fVal, pixsPerMarkY) \
    (-(fVal-MIN_DEG_F_TEMP_VALUE)/36.0*pixsPerMarkY);

#define SECONDS_2H (2*60*60)
// --
//float calcPtY_CurveChart(float fVal, int nPixsPerMark);

enum CURVE_TYPE
{
    CURVE_TYPE_CURRENT,
    CURVE_TYPE_TEMP
};

typedef struct _CurveParam
{
    PACK_TRENDINFO currentData;
    PACK_TEMPTREND tempData;
    enum CURVE_TYPE curveType;
    int         nSigNum;
    int         iFormat;     // the decimal of float value
    int         idxMaxVal;   // the index of max value
    float       floatMaxVal;
    // current
    float       fMaxCurrent;
    // temperature
    int         idxMin;
    int         idxMax;
} CurveParam_t;

void curveChart_paint(
        QPainter* painter,
        CurveParam_t &curveParam,
        TEMP_DISP_FORMAT tempDispFormat
        );

void drawCurveLine(
        QPainter *painter,
        CurveParam_t &curveParam,
        const int nMarksY,
        TEMP_DISP_FORMAT tempDispFormat
        );

} // end namespace

#endif // MULTIDEMON_H

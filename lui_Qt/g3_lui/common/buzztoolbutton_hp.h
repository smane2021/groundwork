/******************************************************************************
文件名：    buzztoolbutton_hp.h
功能：      BuzzToolButton的继承类，设置主页按钮中字体样式
作者：      刘金煌
创建日期：   2013年2月5日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/
#ifndef BUZZTOOLBUTTON_HP_H
#define BUZZTOOLBUTTON_HP_H

#include "buzztoolbutton.h"

class BuzzToolButton_HP : public BuzzToolButton
{
Q_OBJECT
public:
    explicit BuzzToolButton_HP(QWidget *parent = 0);
    virtual ~BuzzToolButton_HP(){}

signals:

public slots:
    /***************************************************************
    功能：按钮被按下，按钮被释放处理
    输入：无
    输出：同功能描述
    ****************************************************************/
    void Press();
    void Release();

protected:
    //virtual void keyPressEvent(QKeyEvent * event);

    //virtual void mousePressEvent(QMouseEvent* e);
    //virtual void mouseReleaseEvent(QMouseEvent* e);
private slots:
    /***************************************************************
    功能：停止鸣叫
    输入：无
    输出：同功能描述
    ****************************************************************/
    void StopBuzz();

};

#endif // BUZZTOOLBUTTON_HP_H

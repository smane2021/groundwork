#include "BuzzDateEdit.h"
#include "pubInclude.h"

BuzzDateEdit::BuzzDateEdit(QWidget *parent) :
    QDateEdit(parent)
{
}

BuzzDateEdit::~BuzzDateEdit()
{
}

void BuzzDateEdit::Enter(void*)
{
    setFocus();
    lineEdit()->setCursorPosition( 0 );
}

void BuzzDateEdit::Leave()
{
}

void BuzzDateEdit::keyPressEvent(QKeyEvent *keyEvent)
{
    int sectionIdx = currentSectionIndex();
    switch ( sectionIdx )
    {
    case 0:
        lineEdit()->setCursorPosition( 0 );
        break;

    case 1:
        lineEdit()->setCursorPosition( 5 );
        break;

    case 2:
        lineEdit()->setCursorPosition( 8 );
        break;
    }

    TRACEDEBUG( "BuzzDateEdit::keyPressEvent cursorPosition<%d> section<%d>",
                lineEdit()->cursorPosition(),
                sectionIdx );
    return QDateEdit::keyPressEvent( keyEvent );
}

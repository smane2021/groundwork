/******************************************************************************
文件名：    CtrlInputChar.cpp
功能：      输入数据控件
作者：      刘金煌
创建日期：   2013年04月10日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef CTRLINPUTCHAR_H
#define CTRLINPUTCHAR_H

#include "common/basicwidget.h"
#include "common/InputCtrlDef.h"

#include "configWidget/CMenuData.h"


namespace Ui {
class CtrlInputChar;
}

class CtrlInputChar : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit CtrlInputChar(QWidget *parent=0,
        CtrlInputParam* pCtrlParam=NULL);
    ~CtrlInputChar();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();

    void setCtrlVisible(bool bVisible);
    void Reset(); // when press escape key reset
    void setHighLight(bool bHighLight=true);
    void     setParam(CtrlInputParam* pCtrlParam);
    CtrlInputParam* getParam();
    CtrlInputRetval  getValue();
    void  setIPV6(QString& strIPV6);
    QString getIPV6();
    int              getKey();

    //Frank Wu,20131219
    void setMenuNode(MenuNode_t *pMenuNode);
    void getMenuNode(MenuNode_t *pMenuNode);

protected:
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent *event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);

private slots:
    void on_spinBox_char_editingFinished();

signals:
    void escapeMe(enum INPUT_TYPE);
    void sigCellUnitKeyPress(int nKey);

private:
    CtrlInputParam m_ctrlParam;
    int            m_key;
    int            m_nPrefixLen;
    int            m_nSuffixLen;
    int            m_nIPCtrlNow; // focus IP Ctrl idx
    int            m_nEnter;// press key enter times

    QDateTime      m_dateTime;
    CtrlInputRetval m_retVal;
    //Frank Wu,20131219
    MenuNode_t m_stMenuNode;
    BOOL       m_bIsMenuNode;
    static QString m_strIPV6;

private:
    Ui::CtrlInputChar *ui;
};

#endif // CTRLINPUTCHAR_H

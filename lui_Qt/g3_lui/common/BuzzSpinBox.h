#ifndef BUZZSPINBOX_H
#define BUZZSPINBOX_H

#include <QSpinBox>
#include "common/uidefine.h"
#include "common/InputCtrlDef.h"
#include <QTime>
class QRegExpValidator;

class BuzzSpinBox : public QSpinBox
{
    Q_OBJECT
public:
    explicit BuzzSpinBox(QWidget *parent = 0);
    ~BuzzSpinBox();

public:
    void setType(CtrlInputParam* ctrlParam);
    virtual void Enter(void* param=NULL);
    virtual void Leave();

private:
    void setKeySpeedUp(int nLevel);//设置加速按键

signals:
    void sigCellUnitKeyPress(int nKey);

protected:
    //void changeEvent(QEvent *event);
    //QValidator::State validate(QString &text, int &pos) const;
    QString textFromValue(int value) const;
    int valueFromText(const QString &text) const;
    virtual void keyPressEvent(QKeyEvent *keyEvent);
    //virtual void keyReleaseEvent(QKeyEvent *keyEvent);

private:
    CtrlInputParam m_ctrlParam;
    QTime timeLastKeyUp;//上一次按键抬起动作的时间，用于判断是不是长按
    int iKeyPressCount; //连续按键次数
};

#endif // BUZZSPINBOX_H

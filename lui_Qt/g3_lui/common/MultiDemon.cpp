#include <QPainter>
#include <QDate>
#include <QPainterPath>
#include <QStaticText>

#include "MultiDemon.h"
#include "global.h"
#include "pubInclude.h"
#include "config/PosBarChart.h"
#include "config/PosThermometer.h"
#include "config/PosCurve.h"
#include "config/PosPieChart.h"
#include "config/configparam.h"
#include "util/DlgInfo.h"

#define DEG_C_INTERVAL      (25)
#define DEG_F_INTERVAL      (36)

void tellAppToReboot()
{
    void* pData = NULL;
    CmdItem cmdItem;
    cmdItem.CmdType   = CT_REBOOT;
    cmdItem.ScreenID  = 0;
    CMD_SET_INFO* setinfo = &(cmdItem.setinfo);
    setinfo->EquipID = 0;
    setinfo->SigID = 0;
    setinfo->SigType = 0;
    setinfo->value.lValue = 0;
    if ( !data_getDataSync(&cmdItem, &pData) )
    {
        TRACEDEBUG( "tellAppToReboot ........" );
    }
}
//==================
void tellAppToResetPassword()
{
    void* pData = NULL;
    CmdItem cmdItem;
    cmdItem.CmdType   = CT_RESETPASSWORD;
    cmdItem.ScreenID  = 0;
    CMD_SET_INFO* setinfo = &(cmdItem.setinfo);
    setinfo->EquipID = 0;
    setinfo->SigID = 0;
    setinfo->SigType = 0;
    setinfo->value.lValue = 0;
    if ( !data_getDataSync(&cmdItem, &pData) )
    {
        TRACEDEBUG( "tellAppToResetPassword ........" );
    }
}

namespace InputCtrl {

//#define  VAR_LONG            1
//#define  VAR_FLOAT           2
//#define  VAR_UNSIGNED_LONG  3
//#define  VAR_DATE_TIME      4  // 现在用的VAR_LONG
//#define  VAR_ENUM           5

static int gs_mapInputType[6]=
{
    IPT_SBT_INT, // nothing
    IPT_SBT_INT,
    IPT_DSBT_SINGLE,
    IPT_SBT_ULONG,
    IPT_DATE,
    IPT_SBT_CUSTOM
};

//Frank Wu,20131231
static int computeDecimals(float fData)
{
    int i;
    for(i = 0; i < 7; i++)
    {
        if(fData > 0.9999)
        {
            break;
        }
        fData *= 10;
    }

    return i;
}

// excluding
// IPT_SBT_CHAR IPT_SBT_CHAR_PW
// IPT_DATE     IPT_IP
int makeCtrlparamBySigtype(
        CtrlInputParam* pCtrlParam,
        SET_INFO* pSetInfoItem
        )
{
  int nSigValueType = pSetInfoItem->iSigValueType;
  pCtrlParam->ipt =
  INPUT_TYPE(gs_mapInputType[nSigValueType]);
  pCtrlParam->strPrefix  = "";
  pCtrlParam->strSuffix  = pSetInfoItem->cSigUnit;
  switch ( nSigValueType )
  {
    case VAR_LONG:
    {
      pCtrlParam->nMin  = pSetInfoItem->vDnLimit.lValue;
      pCtrlParam->nMax  = pSetInfoItem->vUpLimit.lValue;
      pCtrlParam->nInit = pSetInfoItem->vSigValue.lValue;
      pCtrlParam->nStep = pSetInfoItem->iStep.lValue;
      TRACEDEBUG( "makeCtrlparamBySigtype SigType VAR_LONG<%d>", pCtrlParam->nInit );
    }
    break;

    case VAR_FLOAT:
    {
      pCtrlParam->dbMin  = pSetInfoItem->vDnLimit.fValue;
      pCtrlParam->dbMax  = pSetInfoItem->vUpLimit.fValue;
      pCtrlParam->dbInit = pSetInfoItem->vSigValue.fValue;
      pCtrlParam->dbStep = pSetInfoItem->iStep.fValue;
      //Frank Wu,20131231
      pCtrlParam->nDecimals = computeDecimals(pSetInfoItem->iStep.fValue);
      TRACEDEBUG( "makeCtrlparamBySigtype SigType VAR_FLOAT<%f> dbStep<%f>",
                  pCtrlParam->dbInit, pCtrlParam->dbStep );
    }
    break;

    case VAR_UNSIGNED_LONG:
    {
      pCtrlParam->nMin  = pSetInfoItem->vDnLimit.ulValue;
      pCtrlParam->nMax  = pSetInfoItem->vUpLimit.ulValue;
      pCtrlParam->ulInit= pSetInfoItem->vSigValue.ulValue;
      pCtrlParam->nStep = pSetInfoItem->iStep.ulValue;
      TRACEDEBUG( "makeCtrlparamBySigtype SigType VAR_UNSIGNED_LONG min<%d> max<%lu> value<%lu> step<%d>",
                pCtrlParam->nMin,  pSetInfoItem->vUpLimit.ulValue, pCtrlParam->nInit, pCtrlParam->nStep );
    }
    break;

    case VAR_ENUM:
    {
      pCtrlParam->nStep = 1;
      pCtrlParam->nMin  = pSetInfoItem->vDnLimit.lValue;
      pCtrlParam->nMax  = pSetInfoItem->vUpLimit.lValue;
      pCtrlParam->nInit = pSetInfoItem->vSigValue.enumValue;
      pCtrlParam->strInit = QString(pSetInfoItem->cEnumText[pCtrlParam->nInit]);
      TRACEDEBUG( "makeCtrlparamBySigtype SigType VAR_ENUM "
                  "nStep<%d> "
                  "nMin<%d> nMax<%d> "
                  " enumValue<%d> strInit<%s>",
                  pCtrlParam->nStep,
                  pCtrlParam->nMin, pCtrlParam->nMax,
                  pSetInfoItem->vSigValue.enumValue,
                  pCtrlParam->strInit.toUtf8().constData()
                  );
      for(int i=pCtrlParam->nMin; i<=pCtrlParam->nMax; ++i)
      {
          pCtrlParam->strListEnumText <<
          QString(pSetInfoItem->cEnumText[i]);
          TRACEDEBUG( "                pSetInfoItem->cEnumText <%d> <%s>",
                                                i,
                                                pSetInfoItem->cEnumText[i]
                                                );
      }
    }
    break;

    default:
    {
          TRACEDEBUG( "makeCtrlparamBySigtype SigType default<%d>", nSigValueType );
    }
    break;
  }
  return 0;
}

int submitAppParamData(
        CtrlInputChar* pCtrl,
        CmdItem* pCmdItemSetting,
        int sigIdx,
        CtrlInputParam* pCtrlParamSetting
        )
{
    //function: set to app
//    QTime time;
//    time.start();

    bool bSameValue = false;
    CmdItem cmdItem = *pCmdItemSetting;

    PACK_SETINFO* pPackInfoFromApp =
            (PACK_SETINFO*)g_dataBuff;
    SET_INFO*     pSetInfoFromApp =
            &(pPackInfoFromApp->SettingInfo[sigIdx]);
    bool bIsCtrlCmd   = (pSetInfoFromApp->iSigType==1);
    // 自定义的命令类型，此处下发的都是设置类型
    cmdItem.CmdType = CT_SET;
    CMD_SET_INFO* pCmdInfo = &(cmdItem.setinfo);
    pCmdInfo->EquipID = pSetInfoFromApp->iEquipID;
    pCmdInfo->SigID   = pSetInfoFromApp->iSigID;
    int nSigValueType = pSetInfoFromApp->iSigValueType;
    TRACELOG1( "submitAppParamData bIsCtrlCmd<%d> "
               "SigValueType<%d> sigName<%s> "
               "EquipID<%d> SigID<%d>",
               bIsCtrlCmd,
               nSigValueType, pSetInfoFromApp->cSigName,
               pCmdInfo->EquipID, pCmdInfo->SigID );
    pCmdInfo->SigType = pSetInfoFromApp->iSigType;
    qstrcpy(pCmdInfo->Name, pSetInfoFromApp->cSigName);

    CtrlInputRetval retCtrlVal = pCtrl->getValue();
    QString strVal = retCtrlVal.strVal;
    TRACEDEBUG( "submitAppParamData pCtrl->getValue().strVal<%s> ",
                strVal.toUtf8().constData() );
    switch ( nSigValueType )
    {
    case VAR_LONG:
        pCmdInfo->value.lValue = strVal.toLong();
        if (pSetInfoFromApp->vSigValue.lValue ==
                pCmdInfo->value.lValue)
        {
            bSameValue = true;
            break;
        }
        pCtrlParamSetting->nInit = pCmdInfo->value.lValue;
        TRACEDEBUG( "submitAppParamData VAR_LONG<%d>", pCmdInfo->value.lValue );
        break;

    case VAR_FLOAT:
        pCmdInfo->value.fValue = strVal.toFloat();
        if ( qAbs(pSetInfoFromApp->vSigValue.fValue-
                 pCmdInfo->value.fValue)<(pSetInfoFromApp->iStep.fValue*0.1) )
                //pCmdInfo->value.fValue)<0.01 )
        {
            bSameValue = true;
            break;
        }
        pCtrlParamSetting->dbInit = pCmdInfo->value.fValue;
        TRACEDEBUG( "submitAppParamData VAR_FLOAT<%f>", pCmdInfo->value.fValue );
        break;

    case VAR_UNSIGNED_LONG:
        pCmdInfo->value.ulValue = strVal.toULong();
        if (pSetInfoFromApp->vSigValue.ulValue ==
                pCmdInfo->value.ulValue)
        {
            bSameValue = true;
            break;
        }
        pCtrlParamSetting->ulInit = pCmdInfo->value.ulValue;
        TRACEDEBUG( "submitAppParamData VAR_UNSIGNED_LONG<%u>", pCtrlParamSetting->ulInit );
        break;

    case VAR_DATE_TIME:
        TRACEDEBUG( "submitAppParamData VAR_DATE_TIME" );
        break;

    case VAR_ENUM:
    {
        if (cmdItem.ScreenID == SCREEN_ID_WizSiteName)
        {
            qstrncpy( pCmdInfo->Name, strVal.toUtf8().constData(), MAXLEN_NAME-1 );
            TRACELOG1( "submitAppParamData VAR_ENUM<%s>!", pCmdInfo->Name);
        }
        else
        {
            if ( pSetInfoFromApp->vSigValue.enumValue ==
                 retCtrlVal.idx )
            {
                if ( !bIsCtrlCmd )
                {
                    bSameValue = true;
                    TRACEDEBUG( "submitAppParamData enumValue the same value" );
                    break;
                }
            }
            pCtrlParamSetting->nInit   = retCtrlVal.idx;
            pCmdInfo->value.enumValue  = retCtrlVal.idx;
        }
    }
        break;

    default:
        TRACELOG1( "submitAppParamData default nSigValueType not match<%d>!", nSigValueType );
        break;
    }
    // IPV6
    if (pSetInfoFromApp->iEquipID==EQUIPID_SPECIAL &&
            (pSetInfoFromApp->iSigID==SIGID_SPECIAL_IPV6_IP_2 ||
             pSetInfoFromApp->iSigID==SIGID_SPECIAL_IPV6_GATEWAY_2 ||
             pSetInfoFromApp->iSigID==SIGID_SPECIAL_IPV6_IP_3_V ||
             pSetInfoFromApp->iSigID==SIGID_SPECIAL_IPV6_GATEWAY_3_V)
            )
    {
        QStringList lsIPV6 = pCtrl->getIPV6().split(":");
        int nSegNum = lsIPV6.count();
        if (nSegNum > MAX_IPV6_LEN)
        {
            TRACELOG2( "submitAppParamData IPV6 nSegNum>16 <%d>", nSegNum );
        }
        for (int i=0; i<nSegNum; ++i)
        {
            bool ok;
            int nVal = QString(lsIPV6[i]).toInt(&ok, 16);
            pCmdInfo->Name[i*2+0] = (nVal>>8);
            pCmdInfo->Name[i*2+1] = (nVal&0X00FF);
        }
        if (memcmp(pCmdInfo->Name,
                   pSetInfoFromApp->cSigName,
                   MAX_IPV6_LEN) == 0)
        {
            bSameValue = true;
        }
        else
        {
            bSameValue = false;
        }
        TRACEDEBUG( "submitAppParamData IPV6 <%02X%02X %02X%02X %02X%02X %02X%02X %02X%02X %02X%02X %02X%02X %02X%02X> bSameValue<%d>",
                    pCmdInfo->Name[0],
                    pCmdInfo->Name[1],
                    pCmdInfo->Name[2],
                    pCmdInfo->Name[3],
                    pCmdInfo->Name[4],
                    pCmdInfo->Name[5],
                    pCmdInfo->Name[6],
                    pCmdInfo->Name[7],
                    pCmdInfo->Name[8],
                    pCmdInfo->Name[9],
                    pCmdInfo->Name[10],
                    pCmdInfo->Name[11],
                    pCmdInfo->Name[12],
                    pCmdInfo->Name[13],
                    pCmdInfo->Name[14],
                    pCmdInfo->Name[15],
                    bSameValue
                    );
    }

    if (pCtrlParamSetting->ipt==IPT_TIME || pCtrlParamSetting->ipt==IPT_DATE)
    {
        bSameValue = false;
    }
    if ( bSameValue && !bIsCtrlCmd)
    {
        TRACELOG1( "submitAppParamData the same value" );
        return SET_PARAM_ERR_SAME_VALUE;
    }


    QString strQuestion = QObject::tr("OK to clear");
    QString strInfo     = QObject::tr("Please wait")+"...";
    QString strResultYes= QObject::tr("Set successful");
    QString strResultNot= QObject::tr("Set failed");
    bool bPromptDlg = false;
    int nSigID = pCmdInfo->SigID;
    ExecCmdParam_t param;
    param.infoType     = RESULT_INFO_TYPE_NORMAL;
    param.nDlgTimeOut  = TIME_OUT_POPUPDLG;
    switch ( pCmdInfo->EquipID )
    {
    case EQUIPID_SPECIAL:
    {
        if (SIGID_SPECIAL_PROTOCOL == nSigID)
        {
            // 换协议
            bPromptDlg = true;
            strQuestion = QObject::tr("OK to change");
        }
    }
        break;

    case 1:
    {
        if (180 == nSigID)
        {
            // Change work mode
            bPromptDlg = true;
            strQuestion = QObject::tr("OK to change");
        }
    }
        break;

    case 2:
    {
        if ( (8==nSigID || 11==nSigID || 14==nSigID) &&
             bIsCtrlCmd )
        {
            // 8:清模块丢失告警 11:清节能振荡告警 14:LCD清模块通讯中断要等7-8S
            bPromptDlg = true;
        }
        else if (54==nSigID || 55==nSigID)
        {
            // update rect
            // 0:No 1:Yes
            if (pCtrl->getValue().idx == 0)
            {
                TRACELOG1( "submitAppParamData the same value" );
                return SET_PARAM_ERR_REJECTED;
            }

            param.infoType     = RESULT_INFO_TYPE_UPDATE_RECT;
            param.nDlgTimeOut  = -1;
            bPromptDlg   = true;
            strQuestion  = QObject::tr("OK to update");
            strInfo      = QObject::tr("Please wait");
            strResultYes = QObject::tr("Update successful");
            strResultNot = QObject::tr("Update failed");
        }
    }
        break;

    case 115:
    {
        if ( (3==nSigID || 8==nSigID || 9==nSigID || 10==nSigID) &&
             bIsCtrlCmd )
        {
            // 3:清除坏电池告警 8:清电流异常告警 9:清放电不平衡 10:清电池测试失败
            bPromptDlg = true;
        }
    }
        break;

    case 210:
    {
        if ( (8==nSigID || 14==nSigID) && bIsCtrlCmd )
        {
            // 8:清Conv丢失告警 14:清Conv通讯中断
            bPromptDlg = true;
        }
    }
        break;

    case 1350:
    {
        if ( (8==nSigID) && bIsCtrlCmd )
        {
            // 清Solar模块丢失
            bPromptDlg = true;
        }
    }
        break;

    default:
        break;
    }

    if ( bPromptDlg )
    {
        param.strQuestion  = strQuestion + "?";
        param.popupType    = POPUP_TYPE_QUERY_CMDEXEC;
        param.nKeyEnter    = 0;
        param.strInfo      = strInfo + "...";
        param.cmdItem      = cmdItem;
        param.bAutoDestroy = false;
        param.bShowResult  = true;
        param.strResultYes = strResultYes;
        param.strResultNot = strResultNot;

        DlgInfo dlg( strQuestion + "?",
                     POPUP_TYPE_QUESTION
                     );
        dlg.execSetCmd( param );
        return dlg.exec();
    }

    void* pData = NULL;
    int nRetSet = data_getDataSync(&cmdItem, &pData, 40);
    //TRACEDEBUG( "submitAppParamData data_getDataSync time<%d>", time.elapsed() );
    if (nRetSet == ERRDS_OK)
    {
        int nRet = *((int*)pData);
        TRACEDEBUG( "submitAppParamData data_getDataSync nRet<%d>", nRet );

        if (nRet == SET_PARAM_OK)
        {
            // ctrl cmd
            if ( bIsCtrlCmd )
            {
                DlgInfo dlg(
                            strResultYes,
                            POPUP_TYPE_INFOR
                            );
                dlg.exec();
                return SET_PARAM_ERR_OK;
            }

            if (pCmdInfo->EquipID == 1)
            {
                switch ( pCmdInfo->SigID)
                {
                case 22:
                {
                    // chage language
                    // 0:english 1:local
                    LANGUAGE_LOCATE langLocate = g_cfgParam->ms_initParam.langLocate;
                    int idxSelLang = retCtrlVal.idx;
                    if (idxSelLang != langLocate)
                    {
                        langLocate = (LANGUAGE_LOCATE)idxSelLang;
                        if (langLocate == LANG_LOCATE_English)
                        {
                            g_cfgParam->ms_initParam.langType =
                                    LANG_English;
                        }
                        else
                        {
                            g_cfgParam->ms_initParam.langType =
                                    g_cfgParam->ms_initParam.langApp;
                        }
                        g_cfgParam->ms_initParam.langLocate =
                                (LANGUAGE_LOCATE)langLocate;

                        g_cfgParam->writeLanguageLocal();
                        g_cfgParam->setLanguageFont();
                        g_cfgParam->initParam();
                    }
                }
                    TRACEDEBUG( "submitAppParamData chage language OK" );
                    return SET_PARAM_ERR_OK_LANG;

                case 23:
                {
                    // alarm voice
                    g_cfgParam->initParam();

                }
                    return SET_PARAM_ERR_OK;

                case 35:
                {
                    // keypad voice
                    g_cfgParam->initParam();
                }
                    return SET_PARAM_ERR_OK;

                case 186:
                {
                    // LCD rotation
                    QString strInfo = QObject::tr("Adjust LCD")+"...";
                    DlgInfo dlg(
                                strInfo,
                                POPUP_TYPE_INFOR_ALIVE
                                );
                    dlg.execShell( "reboot" );
                    dlg.exec();
                }
                    return SET_PARAM_ERR_OK;

                default:
                    break;
                }
            }
            else if (pCmdInfo->EquipID == 2)
            {
                switch ( pCmdInfo->SigID)
                {
                case 22:
                    // ECO
                    return SET_PARAM_ERR_OK_ECO;
                }
            }

            if ( !bSameValue )
            {
                TRACEDEBUG( "submitAppParamData SET_PARAM_ERR_OK<%u>", pCtrlParamSetting->ulInit );
                pCtrl->setParam( pCtrlParamSetting );
            }
        }
        else
        {
            DlgInfo dlg(
                        strResultNot,
                        POPUP_TYPE_SETFAILED
                        );
            dlg.exec();

            pCtrl->Reset();
            TRACELOG1( "submitAppParamData data_sendCmd setting failed!" );
            return SET_PARAM_ERR_FAILED;
        }
    }
    else
    {
        pCtrl->Reset();
        return nRetSet;
    }

    return SET_PARAM_ERR_OK;
}

int setSpecialParam(
        CmdItem* pCmdItemOld,
        int sigID,
        long lValue
        )
{
    void* pData = NULL;
    CmdItem cmdItem = *pCmdItemOld;
    cmdItem.CmdType = CT_SET;
    CMD_SET_INFO* setinfo = &(cmdItem.setinfo);
    setinfo->EquipID      = -1;
    setinfo->SigID        = sigID;
    setinfo->SigType      = VAR_LONG;
    setinfo->value.lValue = lValue;
    int nRet = data_getDataSync(&cmdItem, &pData);
    if ( !nRet )
    {
        int nRet = *((int*)pData);
        if (nRet == SET_PARAM_OK)
        {
            TRACELOG1( "setSpecialParam data_getDataSync setting OK!" );
        }
        else
        {
            TRACELOG1( "setSpecialParam data_getDataSync setting failed!" );
            DlgInfo dlg(
                        QObject::tr("Set failed"),
                        POPUP_TYPE_SETFAILED
                        );
            dlg.exec();
            return SET_PARAM_ERR_FAILED;
        }
    }

    return SET_PARAM_ERR_OK;
}

int setLCDRotation(enum LCD_ROTATION rotation)
{
    SET_INFO setInfo;
    bool bFinded = false;
    {
    CmdItem cmdItem;
    cmdItem.ScreenID  = SCREEN_ID_Wdg2P9Cfg_SysSetting;
    cmdItem.CmdType   = CT_READ;
    void* pData = NULL;
    data_getDataSync(&cmdItem, &pData, TIME_WAIT_SYNC);
    PACK_SETINFO* info = (PACK_SETINFO*)pData;
    int nSigNum = info->SetNum;
    for (int i=0; i<nSigNum; ++i)
    {
        if (info->SettingInfo[i].iEquipID == 1 &&
                info->SettingInfo[i].iSigID == 186)
        {
            setInfo = info->SettingInfo[i];
            TRACEDEBUG( "setLCDRotation finded equipID<%d> iSigID<%d>", setInfo.iEquipID, setInfo.iSigID );
            bFinded = true;
            break;
        }
    }
    }

    if ( bFinded )
    {
        CmdItem cmdItem;
        cmdItem.CmdType   = CT_SET;
        cmdItem.setinfo.EquipID = setInfo.iEquipID;
        cmdItem.setinfo.SigID   = setInfo.iSigID;
        cmdItem.setinfo.SigType = setInfo.iSigType;
        cmdItem.setinfo.value.enumValue = rotation;
        qstrncpy( cmdItem.setinfo.Name, setInfo.cEnumText[rotation], MAXLEN_NAME-1 );
        void* pData = NULL;
        int nRetSet = data_getDataSync(&cmdItem, &pData, 40);
        TRACEDEBUG( "setLCDRotation data_getDataSync return<%d>", nRetSet );
        if (nRetSet == ERRDS_OK)
        {
            int nRet = *((int*)pData);
            TRACEDEBUG( "setLCDRotation data_getDataSync pData<%d>", nRet );
            if (nRet == SET_PARAM_OK)
            {
                return SET_PARAM_ERR_OK;
            }
        }
    }

    return SET_PARAM_ERR_FAILED;
}


//获取温度的显示格式
TEMP_DISP_FORMAT GetTempFormat()
{
    CmdItem cmdItem;
    PACK_INFO* pInfo = NULL;

    cmdItem.CmdType = CT_READ;
    cmdItem.ScreenID = SCREEN_ID_Temp_Format;

    void *pData = NULL;
    if(!data_getDataSync(&cmdItem, &pData) )
    {
        pInfo = (PACK_INFO*)pData;
        if(pInfo->iDataNum > 0)
        {
            return ((TEMP_DISP_FORMAT)(pInfo->vSigValue.enumValue));
        }
    }
}

} // end namespace InputCtrl

namespace CtrlLight{

int sendCmdCtrlLight(
        int screenID,
        int equipID,
        enum MODULE_LIGHT_TYPE lightType,
        enum MODULE_TYPE moduleType/*SetInformation.SigID*/
        )
{
    // send this cmd event if moduleType == MODULE_TYPE_INVALID
    CmdItem cmdItem;
    cmdItem.ScreenID = screenID;
    cmdItem.CmdType  = CT_CTRL;
    SetInformation* pSetinfo = &(cmdItem.setinfo);
    pSetinfo->EquipID      = equipID;
    pSetinfo->value.lValue = lightType;
    pSetinfo->SigID        = moduleType;

    TRACEDEBUG( "sendCmdCtrlLight screenID<%x> iEquipID<%d> lightType<%d> SigID<%d>",
                screenID, equipID, lightType, moduleType );

    void *pData = NULL;
    data_getDataSync(&cmdItem, &pData);
    g_bSendCmd    = true;
    return 0;
}

//模块类型ID:201
//Converter 类型ID: 1101
//MPPT 类型ID:2501
// 3-S1           2-S2          3-S3
int getModuleType(int iEqIDType)
{
    enum MODULE_TYPE moduleType = MODULE_TYPE_INVALID;
    switch (iEqIDType)
    {
    case 201:
        moduleType = MODULE_TYPE_RECT;
        break;

    case 1101:
        moduleType = MODULE_TYPE_CONV;
        break;

    case 2501:
        moduleType = MODULE_TYPE_MPPT;
        break;

    case 1601:
        moduleType = MODULE_TYPE_S1RECT;
        break;

    case 1701:
        moduleType = MODULE_TYPE_S2RECT;
        break;

    case 1801:
        moduleType = MODULE_TYPE_S3RECT;
        break;

    default:
        break;
    }

    return moduleType;
}

int getModuleTypeByWt(int wt)
{
    enum MODULE_TYPE moduleType = MODULE_TYPE_INVALID;
    switch (wt)
    {
    case WT2_RECTINFO:
        moduleType = MODULE_TYPE_RECT;
        break;

    case WT2_CONVINFO:
        moduleType = MODULE_TYPE_CONV;
        break;

    case WT2_SOLINFO:
        moduleType = MODULE_TYPE_MPPT;
        break;

    case WT2_SLAVE1INFO:
        moduleType = MODULE_TYPE_S1RECT;
        break;

    case WT2_SLAVE2INFO:
        moduleType = MODULE_TYPE_S2RECT;
        break;

    case WT2_SLAVE3INFO:
        moduleType = MODULE_TYPE_S3RECT;
        break;

    default:
        break;
    }

    return moduleType;
}

} // end namespace CtrlLight

namespace BarChart {

int g_nMaxItemPerpage = 0;
int g_nMaxXMarks      = 0;

float calcPointX_BARCHART(int val,int base){
	if(base == 25)
	{
		return (((PosBarChart::hAxisLen-PosBase::lastMarkLenAxis)*1.0)/100*(val+base));		
	}
	else
	{
		return (((PosBarChart::hAxisLen-PosBase::lastMarkLenAxis)*1.0)/180*(val+base));
	}
	
}

void barChart_paint(QPainter  *pPainter,
                    int       iFormat,
                    float     *sigVals,
                    bool      *bResponds,
                    int       *sigIndexs,
                    int       nShowMaxItemCurPage,
                    int       nPageIdx,
                    float     warnSigVals[MAX_ITEM_PERPAGE_BARCHART][MAXNUM_NORLEVEL],
                    TEMP_DISP_FORMAT tempDispFormat
                    )
{
    int idxBranch   = 1;
    pPainter->setFont( g_cfgParam->gFontSmallN );
    // 原点
    pPainter->translate(
                PosBarChart::originX,
                PosBarChart::originY
                );

    // 轴线颜色
    pPainter->setPen( coordAxisTextColor );
    pPainter->drawLine(
                0,
                0,
                0,
                -PosBarChart::vAxisLen
                );

    int nXDigitals[SCALES_X_DEG_F_BARCHART+1] = {0};

    int nTempBase = 0;
    if(tempDispFormat == TEMP_FORMAT_TYPE_CELS)
    {
        g_nMaxXMarks = SCALES_X_BARCHART;

        nXDigitals[0] = -25;
        nXDigitals[1] = 0;
        nXDigitals[2] = 25;
        nXDigitals[3] = 50;
        nXDigitals[4] = 75;
		
        nTempBase = 25;
    }
    else
    {
        g_nMaxXMarks = SCALES_X_DEG_F_BARCHART;

        nXDigitals[0] = -13;
        nXDigitals[1] = 23;
        nXDigitals[2] = 59;
        nXDigitals[3] = 95;
        nXDigitals[4] = 131;
        nXDigitals[5] = 167;
		
        nTempBase = 13;
    }

    // x轴文字
    pPainter->setPen( coordAxisTextColor );
    QString strX;

    for (idxBranch=0; idxBranch<=g_nMaxXMarks; ++idxBranch)
    {
      strX = QString::number( nXDigitals[idxBranch] );
      pPainter->drawText(
                  (int)SCALELENX_BARCHART*idxBranch-fmSmallN->width(strX)/4,
                  PosBase::hAxisTextY,
                  strX
                  );
    }

    //绘制竖线
    pPainter->setPen( coordAxisTextColor );
    for (idxBranch=1; idxBranch<=g_nMaxXMarks; ++idxBranch)
    {
        //画X刻度线
        pPainter->drawLine(
                    QPointF(SCALELENX_BARCHART*idxBranch, 0),
                    QPointF(SCALELENX_BARCHART*idxBranch,
                        -PosBarChart::vAxisLen)
                    );
    }

    // draw no data text
    if (nPageIdx < 1 || nShowMaxItemCurPage == 0)
    {
        QString strNoData = QObject::tr("No Data");
        pPainter->translate(
                    -PosBarChart::originX,
                    -PosBarChart::originY
                    );
        pPainter->setPen( nodataColor );
        pPainter->drawText(
                    PosBase::screenWidth/2-fmSmallN->width(strNoData)/2,
                    PosBase::screenHeight/2,
                    strNoData
                    );
        return;
    }

    // 画条形图 写信号值文字 Y刻度 "Tn"
    float fSigVal   = 0;
    int   idxSigval    = 0;
    for (idxBranch=nShowMaxItemCurPage; idxBranch>0; --idxBranch)
    {
        idxSigval = nShowMaxItemCurPage-idxBranch;
        fSigVal = sigVals[idxSigval];
        if (fSigVal>nXDigitals[0] && fSigVal<=nXDigitals[g_nMaxXMarks])
        {
            QRectF rectBar(
                        -5,
                        -PosBarChart::nPenHeight*idxBranch,
                        calcPointX_BARCHART(fSigVal,nTempBase)+5,
                        PosBarChart::nPenHeight-PosBarChart::nItemHeight
                        );
            // 画条形图
            if ( bResponds[idxSigval] )
            {
                if ((fSigVal<=warnSigVals[idxSigval][0]) ||
                        (fSigVal>=warnSigVals[idxSigval][2]))
                {
                    pPainter->setPen( textNormalColor );
                    pPainter->setBrush( chartWarnColor );
                }
                else
                {
                    pPainter->setPen( textNormalColor );
                    pPainter->setBrush( barColor );
                }
                pPainter->drawRect( rectBar );
            }
            else
            {
                pPainter->setPen( textNormalColor );
                pPainter->setBrush( chartNotColor );
                pPainter->drawRect( rectBar );
            }
        }
        else if(fSigVal > nXDigitals[g_nMaxXMarks])
        {
            QRectF rectBar(-5,
                        -PosBarChart::nPenHeight*idxBranch,
                        calcPointX_BARCHART(nXDigitals[g_nMaxXMarks],nTempBase)+8,
                        PosBarChart::nPenHeight-PosBarChart::nItemHeight
                        );
            pPainter->setPen( textNormalColor );
            pPainter->setBrush( chartWarnColor );
            pPainter->drawRect( rectBar );
        }

        // 写字 信号值 居中显示
        //竖直居中
        int nFontHeight = g_cfgParam->ms_pFmSmallN->height();
        int ptYText =
                -PosBarChart::nPenHeight*idxBranch +
            (PosBarChart::nPenHeight-PosBarChart::nItemHeight - nFontHeight)/2 + g_cfgParam->ms_pFmSmallN->ascent ();
        // Tn
        QString strItem = "T"+QString::number(
                    sigIndexs[idxSigval]);

        //水平居中
        QString strText = strItem + ": " + QString::number(fSigVal, FORMAT_DECIMAL);

        int ptXText = PosBarChart::iText_Gap;

        // 信号值
        pPainter->setPen( coordAxisTextColor );
        pPainter->drawText(
                    ptXText,
                    ptYText,
                    strText);
    }
}
} // end namespace


namespace PieChart {

void pieChart_paint(QPainter* pPainter,
                    QColor* colorMargins,
                    float* fMargins,
                    int nMarginNum,

                    int iFormat,
                    float* fSigVals,

                    QPixmap* pPixmapPointers,
                    QPixmap* pPixmapCover,
                    QPixmap* pPixmapCenter,
                    bool bAC
                    )
{
    pPainter->setFont( g_cfgParam->gFontSmallN );
    if ( bAC )
    {
        pPainter->translate(
                    PosPieChart::originX,
                    PosPieChart::originY
                    );
    }
    else
    {
        pPainter->translate(
                    PosPieChart::originX,
                    PosPieChart::originY_DC
                    );
    }
    pPainter->setRenderHint(
                QPainter::Antialiasing,
                true
                );

    // 画边界扇形
    pPainter->setPen( Qt::NoPen );
    for (int i=0; i<nMarginNum-1; ++i)
    {
        pPainter->setBrush( colorMargins[i] );
        pPainter->drawPie( QRect(-PosPieChart::nRadius,
                                -PosPieChart::nRadius,
                                PosPieChart::nRadius*2,
                                PosPieChart::nRadius*2
                                ),
                          calcPieAngle_PieChart(fMargins[i+1],
                                       fMargins[0],
                                       fMargins[nMarginNum-1]),
                          calcPieAngle_PieChart(fMargins[i],
                                       fMargins[0],
                                       fMargins[nMarginNum-1])-
                          calcPieAngle_PieChart(fMargins[i+1],
                                       fMargins[0],
                                       fMargins[nMarginNum-1])
                          );
    }

    // 盖罩
    pPainter->drawPixmap(
                -PosPieChart::coverX,
                -PosPieChart::coverY,
                *pPixmapCover
                );

    // 写字
    float fAngle = 0;
    pPainter->setPen( textNormalColor );
    QString strSigVal;
    float fAngleOldR = 180+(-Angle_NegativeX);
    float fAngle1  = Angle_NegativeX;
    float fAngle3 = fAngleOldR;
    const int SPAN_ANGLEL = 20;
    const int SPAN_ANGLER = 20;
    for (int i=0; i<nMarginNum; ++i)
    {
        strSigVal = QString::number(fMargins[i], FORMAT_DECIMAL);
        fAngle = calcCoordinateAngle_PieChart(
                    fMargins[i],
                    fMargins[0],
                    fMargins[nMarginNum-1]
                    );

        bool  bSame = false;
        // 有重叠的字错开
        switch (i)
        {
        case 1:
            if (fMargins[0] == fMargins[1])
            {
                fAngle = fAngle1;
                bSame = true;
            }
            else
            {
                if (fAngle-fAngle1 < SPAN_ANGLEL)
                {
                    fAngle = fAngle1+SPAN_ANGLEL;
                }
            }
            fAngle1 = fAngle;
            break;

        case 2:
            if (fMargins[1] == fMargins[2])
            {
                fAngle = fAngle1;
                bSame = true;
            }
            else
            {
                if (fAngle-fAngle1 < SPAN_ANGLEL)
                {
                    fAngle = fAngle1+SPAN_ANGLEL;
                }
            }
            break;

        case 3:
            if (fMargins[3] == fMargins[4])
            {
                if (fMargins[3] == fMargins[5])
                {
                    i = 10;
                }
                else if (fAngleOldR-fAngle < SPAN_ANGLER)
                {
                    fAngle = fAngleOldR-SPAN_ANGLER;
                }
            }
            else
            {
                if (fAngleOldR-fAngle < SPAN_ANGLER*2)
                {
                    fAngle = fAngleOldR-SPAN_ANGLER*2;
                }
            }
            fAngle3 = fAngle;
            break;

        case 4:
            if (fMargins[3] == fMargins[4])
            {
                fAngle = fAngle3;
                bSame = true;
            }
            else if (fMargins[4] == fMargins[5])
            {
                fAngle = fAngleOldR;
            }
            else
            {
                if (fAngle-fAngle3 < SPAN_ANGLER)
                {
                    fAngle = fAngle3+SPAN_ANGLER;
                }
                else if (fAngleOldR-fAngle < SPAN_ANGLER)
                {
                    fAngle = fAngleOldR-SPAN_ANGLER;
                }
            }
            fAngle3 = fAngle;
            break;

        case 5:
            if (fMargins[4] == fMargins[5])
            {
                bSame = true;
            }
            break;

        default:
            break;
        }
        //TRACEDEBUG( "pieChart_paint i<%d> fAngle<%f>", i, fAngle );

        if ( bSame )
        {
            continue;
        }

        pPainter->save();
        pPainter->rotate( fAngle );
        pPainter->translate( -QPoint(PosPieChart::nRadius, 0) );
        pPainter->rotate( -fAngle );

        if (fAngle < 0)
        {
            pPainter->drawText(
                        QPoint(-g_cfgParam->ms_pFmSmallN->width(strSigVal)-4, 0),
                        strSigVal
                        );
        }
        else if (fAngle < 90)
        {
            pPainter->drawText(
                        QPoint(-g_cfgParam->ms_pFmSmallN->width(strSigVal), 0),
                        strSigVal
                        );
        }
        else if (fAngle < 180)
        {
            pPainter->drawText(
                        QPoint(0, 0),
                        strSigVal
                        );
        }
        else
        {
            pPainter->drawText(
                        QPoint(3, 0),
                        strSigVal
                        );
        }


//        pPainter->drawText(
//                    QPoint(0, 0),
//                    strSigVal
//                    );
        pPainter->restore();
    }

    // 指针
    if ( bAC )
    {
        for (int i=0; i<3; ++i)
        {
            if (fSigVals[i] < fMargins[0])
            {
                fSigVals[i] = fMargins[0];
            }
            else if (fSigVals[i] > fMargins[nMarginNum-1])
            {
                fSigVals[i] = fMargins[nMarginNum-1];
            }
            fAngle = calcCoordinateAngle_PieChart(
                        fSigVals[i],
                        fMargins[0],
                        fMargins[nMarginNum-1]
                        );
            if (fAngle < Angle_NegativeX)
            {
                fAngle = Angle_NegativeX;
            }
            else if (fAngle > 180-Angle_NegativeX)
            {
                fAngle = 180-Angle_NegativeX;
            }
            //TRACEDEBUG( "****** %f", fAngle );
            pPainter->save();
            pPainter->rotate( fAngle );
            pPainter->drawPixmap(
                        -PosPieChart::sizePointer.width(),
                        -PosPieChart::sizePointer.height()/2,
                        pPixmapPointers[i]
                        );
            pPainter->restore();
        }
    }
    else
    {
        if (*fSigVals < fMargins[0])
        {
            *fSigVals = fMargins[0];
        }
        else if (*fSigVals > fMargins[nMarginNum-1])
        {
            *fSigVals = fMargins[nMarginNum-1];
        }

        fAngle = calcCoordinateAngle_PieChart(
                    *fSigVals,
                    fMargins[0],
                    fMargins[nMarginNum-1]
                    );
        if (fAngle < Angle_NegativeX)
        {
            fAngle = Angle_NegativeX;
        }
        else if (fAngle > 180-Angle_NegativeX)
        {
            fAngle = 180-Angle_NegativeX;
        }
        //TRACEDEBUG( "****** %f", fAngle );
        pPainter->save();
        pPainter->rotate( fAngle );
        pPainter->drawPixmap(
                    -PosPieChart::sizePointer.width(),
                    -PosPieChart::sizePointer.height()/2,
                    *pPixmapPointers
                    );
        pPainter->restore();
    }
    // 中心
    pPainter->drawPixmap(
                -PosPieChart::sizeCenter.width()/2,
                -PosPieChart::sizeCenter.height()/2,
                *pPixmapCenter
                );
}

} // end namespace


namespace ThermometerChart {

float calcPtX(float fVal,
              int nWidthThermometer,
              float* fMargins)
{
    return ( (nWidthThermometer / (fMargins[LIMNUM-1]-fMargins[0]))*(fVal-fMargins[0]) );
}

void degThermometerChart_paint(
        QPainter* painter,
        QColor* colorMargins,
        float*  fMargins,
        float   fSigVal,
        QPixmap* pixmapPointer
        )
{
    painter->setFont( g_cfgParam->gFontSmallN );
    painter->translate( PosThermometer::ptOrigin ); // 设置原点
    painter->setRenderHint(QPainter::Antialiasing);

    // x刻度
    int nValueScales = fMargins[LIMNUM-1]-fMargins[0];
    int nSpawn = 0;

    //当取值的范围大于150则说明是华氏度计数，否则为摄氏度计数；
    //当使用摄氏度计数时刻度间隔为5，当使用华氏度计数时，间隔为9
    if(nValueScales > 150)
    {
      nSpawn = DEG_F_INTERVAL;
    }
    else
    {
      nSpawn = DEG_C_INTERVAL;
    }

    int nScales = (int)( (fMargins[LIMNUM-1]-
               fMargins[0])/1.0/nSpawn*5 );

    //温度计外壳的框体
    QRect rectDegMeter(-PosThermometer::penWidth,
                       -PosThermometer::penWidth,
                       PosThermometer::meterWidth+PosThermometer::penWidth*2,
                       PosThermometer::meterHeight/*-PosThermometer::penWidth*2*/);
    QPainterPath bgPaintPath;
    bgPaintPath.addRoundRect (rectDegMeter,10,99);


    //画刻度
    QPen pen;
    pen.setColor( coordAxisTextColor );
    pen.setWidth( 1 );
    painter->setPen( pen );
    QString strAxisTxt;
    int nAxisTxtWidth = 0;
    int nAxisTxtHeight = fmSmallN->height();
    float fScaleX = 0;
    for (int i=0; i<=nScales; ++i)
    {
      if(i%5 == 0)
      {
        // long scale
        fScaleX = calcPtX_Thermometer(i*(nSpawn*1.0/5)+fMargins[0]);
        strAxisTxt = QString::number(i/5*nSpawn+fMargins[0]);
        nAxisTxtWidth = fmSmallN->width( strAxisTxt );
        painter->drawLine(
              QPointF(fScaleX+4*PosThermometer::penWidth,PosThermometer::meterHeight),
              QPointF(fScaleX+4*PosThermometer::penWidth,
                      PosThermometer::meterHeight+PosThermometer::longScaleLen));

        painter->drawText(
              QPointF(fScaleX+4*PosThermometer::penWidth-nAxisTxtWidth/2,
                      PosThermometer::meterHeight+PosThermometer::longScaleLen+nAxisTxtHeight),
              strAxisTxt);
      }
      else
      {
        // short scale
        fScaleX = calcPtX_Thermometer(i*nSpawn*1.0/5+fMargins[0]);
        painter->drawLine(
              QPointF(fScaleX+4*PosThermometer::penWidth,PosThermometer::meterHeight),
              QPointF(fScaleX+4*PosThermometer::penWidth,
                      PosThermometer::meterHeight+PosThermometer::shortScaleLen));
      }
    }

    // 画条形边界 red yellow green yellow red
    float x1 = 0;
    float x2 = 0;
    for (int i=0; i<LIMNUM-1; ++i)
    {
        painter->setPen( colorMargins[i] );
        painter->setBrush( colorMargins[i] );
        x1 = calcPtX_Rect(fMargins[i]);
        x2 = calcPtX_Rect(fMargins[i+1]);
        if (x2-x1 > 0.1)
        {
            QPainterPath fgPaintPath;
            fgPaintPath.addRect (QRectF(x1,0,x2-x1,PosThermometer::meterHeight-2*PosThermometer::penWidth));

            QPainterPath intersecPath = fgPaintPath.intersected(bgPaintPath);

            painter->drawPath(intersecPath);
        }
    }

    //绘制温度计的外壳
    pen.setColor (QColor(219,219,219));
    pen.setWidth( PosThermometer::penWidth );
    painter->setPen( pen );
    painter->setBrush (Qt::NoBrush);
    painter->drawRoundRect(rectDegMeter,10,99);

    //绘制指针
    float xPoint = calcPtX_Thermometer(fSigVal) + 4*PosThermometer::penWidth - (PosThermometer::sizePointer.width()/2);
    float yPoint = -(PosThermometer::penWidth+PosThermometer::sizePointer.height()) + PosThermometer::meterHeight;
    if ((fSigVal>=fMargins[0]) && (fSigVal<=fMargins[LIMNUM-1]))
    {
        painter->drawPixmap( QPointF(xPoint,yPoint),*pixmapPointer);
    }
}

} // end namespace

namespace CurveChart {

float calcPtY_CurveChart(float fVal, int nPixsPerMark)
{
    return (-(fVal-MIN_TEMP_VALUE)/25.0*nPixsPerMark);
}

void curveChart_paint(
        QPainter* painter,
        CurveParam_t &curveParam,
        TEMP_DISP_FORMAT tempDispFormat
        )
{
    enum CURVE_TYPE curveType = curveParam.curveType;
    bool bCurrent = (CURVE_TYPE_CURRENT==curveParam.curveType);
    int nSigNum      = curveParam.nSigNum;

    painter->setFont( g_cfgParam->gFontSmallN );
    // 原点
    painter->translate(
                PosCurve::originX,
                PosCurve::originY
                );

    // 找曲线点坐标
    int nMarksY = 0;    //Y轴刻度线的个数
//    int ptyEndVAxis = -PosCurve::vAxisLen;

    //

    if (CURVE_TYPE_CURRENT==curveType)
    {
        nMarksY = MARKS_Y_CURRENT_CurveChart;

//        QString strUnit = "(%)";
//        int nUnitWidth = fmSmallN->width( strUnit );
//        painter->setPen( coordAxisTextColor );
//        painter->drawText(
//                    PosCurve::hAxisLen-nUnitWidth,
//                    -PosCurve::vAxisLen-PosBase::unitTextOffset,
//                    strUnit);
    }
    else
    {
//        QString strUnit;
        //
        if(tempDispFormat==TEMP_FORMAT_TYPE_CELS)
        {
            nMarksY = MARKS_Y_DEG_CurveChart;
//            strUnit = "(deg.C)";
        }
        else
        {
            nMarksY = MARKS_Y_CURRENT_DEG_F_CurveChart; //华氏度
//            strUnit = "(deg.F)";
        }
//        int nUnitWidth = fmSmallN->width( strUnit );
//        painter->setPen( coordAxisTextColor );
//        painter->drawText(
//                    PosCurve::hAxisLen-nUnitWidth,
//                    -PosCurve::vAxisLen-PosBase::unitTextOffset,
//                    strUnit
//                    );
    }

    //刻度线,文字和横轴网格线显示,
    //
    int nScaleIdx = 1;
    int ptxEndHAxis = PosCurve::hAxisLen;
    for (nScaleIdx=1; nScaleIdx<=nMarksY; ++nScaleIdx)
    {
        // 与X轴平行的网格线
        painter->setPen( coordGridColor );
        painter->drawLine(
                    0,
                    -pixsPerMarkY_CurveChart(nMarksY)*nScaleIdx,
                    ptxEndHAxis,
                    -pixsPerMarkY_CurveChart(nMarksY)*nScaleIdx
                    );

        // Y轴刻度
        painter->setPen( coordAxisColor );
        painter->drawLine(
                    0,
                    -pixsPerMarkY_CurveChart(nMarksY)*nScaleIdx,
                    PosBase::axisMarkLen,
                    -pixsPerMarkY_CurveChart(nMarksY)*nScaleIdx
                    );

        // Y轴文字
        painter->setPen( coordAxisTextColor );
        QString strItem;
        if ( bCurrent )
        {
            strItem = QString::number( 25*nScaleIdx );
        }
        else
        {
            if(tempDispFormat==TEMP_FORMAT_TYPE_CELS)
            {
                strItem = QString::number( 25*nScaleIdx+MIN_TEMP_VALUE);
            }
            else
            {
                strItem = QString::number( 36*nScaleIdx+MIN_DEG_F_TEMP_VALUE);
            }
        }

        painter->drawText(
                    -fmSmallN->width(strItem)-PosBase::vAxisTextXOffset,
                    -pixsPerMarkY_CurveChart(nMarksY)*nScaleIdx+5,
                    strItem);
    }

    //下限值的显示
    painter->setPen( coordAxisTextColor );
    QString strItem;
    if ( !bCurrent )
    {
        if(tempDispFormat==TEMP_FORMAT_TYPE_CELS)
        {
            strItem = QString::number( MIN_TEMP_VALUE );
        }
        else
        {
            strItem = QString::number( MIN_DEG_F_TEMP_VALUE );
        }
        painter->drawText(
                    -fmSmallN->width(strItem)-PosBase::vAxisTextXOffset,
                    5,
                    strItem
                    );
    }
    else
    {
        strItem=QString::number(MIN_CURRENT_VALUE);
        painter->drawText(
                    -fmSmallN->width(strItem)-PosBase::vAxisTextXOffset,
                    5,
                    strItem
                    );
    }

    // ------------华氏度显示----------------------

// 20180816移除Y轴
//    // Y轴线
//    painter->setPen( coordAxisColor );
//    painter->setBrush( coordAxisColor );
//    painter->drawLine( 0,0,0,ptyEndVAxis );

    // X轴
    //X刻度
    for (nScaleIdx=1; nScaleIdx<=MARKS_X_CurveChart; ++nScaleIdx)
    {
// 20180816移除Y轴
//        // 与Y轴平行的网格线
//        painter->setPen( coordGridColor );
//        painter->drawLine(pixsPerMarkX_CurveChart*nScaleIdx,
//                          0,
//                          pixsPerMarkX_CurveChart*nScaleIdx,
//                          ptyEndVAxis-2 );

        // X轴刻度
        painter->setPen( coordGridColor );
        painter->drawLine(
                    pixsPerMarkX_CurveChart*nScaleIdx,
                    0,
                    pixsPerMarkX_CurveChart*nScaleIdx,
                    -PosBase::axisMarkLen);
    }

    // X轴线
    painter->setPen( coordGridColor );
    painter->setBrush( coordAxisColor );
    painter->drawLine( 0, 0, ptxEndHAxis, 0 );

    // X箭头
    if (nSigNum > 0)
    {
        // 画曲线,标注最大值
        drawCurveLine(
                    painter,
                    curveParam,
                    nMarksY,tempDispFormat
                    );
    }
    else
    {
        // draw no data text
        QString strNoData = QObject::tr("No Data");
        painter->translate(-PosCurve::originX, -PosCurve::originY );

        painter->setPen( nodataColor );
        painter->drawText(
                    PosBase::screenWidth/2-fmSmallN->width(strNoData)/2,
                    PosBase::screenHeight/2,
                    strNoData
                    );
    }
}

void drawCurveLine(
        QPainter *painter,
        CurveParam_t &curveParam,
        const int nMarksY,
        TEMP_DISP_FORMAT tempDispFormat
        )
{
    enum CURVE_TYPE curveType = curveParam.curveType;
    //int nSigNum      = curveParam.nSigNum;
    int iFormat      = curveParam.iFormat;
    int idxMaxVal    = curveParam.idxMaxVal;
    float fMaxSigVal = curveParam.floatMaxVal;
    float fMaxCurrent= curveParam.fMaxCurrent;
    int idxMin       = 0;
    int idxMax       = 0;

    float fMinVal = INVALID_VALUE;
    float fMaxVal = INVALID_VALUE;
    if (CURVE_TYPE_CURRENT==curveType)
    {
        fMinVal = MIN_CURRENT_VALUE;
        fMaxVal = MAX_CURRENT_VALUE;

        idxMin  = 0;
        idxMax  = MAXNUM_TREND-1-1;
    }
    else
    {
        if(tempDispFormat == TEMP_FORMAT_TYPE_CELS)
        {
            fMinVal = MIN_TEMP_VALUE;
            fMaxVal = MAX_TEMP_VALUE;
        }
        else
        {
            fMinVal = MIN_DEG_F_TEMP_VALUE;
            fMaxVal = MAX_DEG_F_TEMP_VALUE;
        }

        idxMin  = curveParam.idxMin;
        idxMax  = curveParam.idxMax;
    }

    QPointF ptsCurve[MAXNUM_TREND];
    float fPtx       = 0;
    float fPty       = 0;
    int nValidSigNum = 0;

    // draw curve line
    QPen pen( QBrush(curveColor), CURVE_WIDTH_CURVECHART );
    painter->setPen( pen );
    float fVal    = INVALID_VALUE;
    for(int i=idxMin; i<=idxMax; ++i)
    {
        if (CURVE_TYPE_CURRENT==curveType)
        {
            fVal = curveParam.currentData.fData[i];
        }
        else
        {
            fVal = curveParam.tempData.TempTrend[i].fTemp;
        }
        if (fVal>=fMinVal && fVal<=fMaxVal)
        {
            // MAXNUM_TREND-1 is total load value
            if (i == idxMax)
            {
                fPtx = pixsPerMarkX_CurveChart*
                        MARKS_X_CurveChart;
            }
            else
            {
                fPtx = MARKS_X_CurveChart/(idxMax*1.0)*
                        i*pixsPerMarkX_CurveChart;
            }
            if (CURVE_TYPE_CURRENT==curveType)
            {
                fPty = calcPtY_CURRENT_CurveChart(
                            fVal,
                            pixsPerMarkY_CurveChart(nMarksY)
                            );
            }
            else
            {
                if(tempDispFormat == TEMP_FORMAT_TYPE_CELS)
                {
                    fPty = calcPtY_DEG_C_CurveChart(
                                fVal,
                                pixsPerMarkY_CurveChart(nMarksY)
                                );
                }
                else
                {
                    fPty = calcPtY_DEG_F_CurveChart(
                                fVal,
                                pixsPerMarkY_CurveChart(nMarksY)
                                );
                }
            }
//            TRACEDEBUG("drawCurveLine:(%f,%f)",fPtx,fPty);
            ptsCurve[nValidSigNum] = QPointF( fPtx, fPty );
            ++nValidSigNum;
        }
        else
        {
            if (nValidSigNum > 0)
            {

                if (nValidSigNum == 1)
                {
                    painter->drawPoint( ptsCurve[0] );
                }
                else
                {
                    painter->drawPolyline(ptsCurve, nValidSigNum);
                }
                nValidSigNum = 0;
            }
        }
    }
    if (nValidSigNum > 0)
    {
        if (nValidSigNum == 1)
        {
            painter->drawPoint( ptsCurve[0] );
        }
        else
        {
            painter->drawPolyline(ptsCurve, nValidSigNum);
        }
    }

    // pt of the Max value
    float fPtxMax  = -9999;
    float fPtyMax  = 9999;
    if (idxMaxVal == idxMax)
    {
        fPtxMax = pixsPerMarkX_CurveChart*
                MARKS_X_CurveChart;
    }
    else
    {
        fPtxMax = MARKS_X_CurveChart/((idxMax)*1.0)*
                idxMaxVal*pixsPerMarkX_CurveChart;
    }
    if (CURVE_TYPE_CURRENT==curveType)
    {
        fPtyMax = calcPtY_CURRENT_CurveChart(
                    fMaxSigVal,
                    pixsPerMarkY_CurveChart(nMarksY)
                    );
    }
    else
    {
        if(tempDispFormat == TEMP_FORMAT_TYPE_CELS)
        {
            fPtyMax = calcPtY_DEG_C_CurveChart(
                        fMaxSigVal,
                        pixsPerMarkY_CurveChart(nMarksY)
                        );
        }
        else
        {
            fPtyMax = calcPtY_DEG_F_CurveChart(
                        fMaxSigVal,
                        pixsPerMarkY_CurveChart(nMarksY)
                        );
        }

    }
/*
    // draw the peak value text
    // 温度曲线标出最大值
    if (fPtxMax > -9999 && fPtyMax < 9999)
    {
        painter->setBrush( Qt::red );
        painter->setPen( Qt::red );
        QPointF ptsMaxVal[3] = {
            QPointF(fPtxMax,   fPtyMax-3),
            QPointF(fPtxMax-3, fPtyMax+3),
            QPointF(fPtxMax+3, fPtyMax+3)
        };
        painter->drawPolygon(ptsMaxVal, 3);
        // drawPoint( QPointF(fPtxMax, fPtyMax) );
        painter->setPen( coordAxisTextColor );

        QString strVal;
        if (CURVE_TYPE_CURRENT==curveType)
        {
            strVal = QString::number(fMaxCurrent, FORMAT_DECIMAL) +
                    "A," +
                    QString::number(fMaxSigVal, FORMAT_DECIMAL) +
                    "%";
        }
        else
        {
            strVal = QString::number(fMaxSigVal, FORMAT_DECIMAL);
        }

        if (fPtxMax+fmSmallN->width(strVal) > PosCurve::hAxisLen)
        {
            fPtxMax = PosCurve::hAxisLen-fmSmallN->width(strVal) - 3;
        }
        painter->drawText(
                    QPointF(fPtxMax, fPtyMax-2),
                    strVal
                    );
    }
*/
}

} // end namespace

/******************************************************************************
文件名：    buzzpushbutton.h
功能：      QPushButton的继承类，实现切换tab蜂鸣器响
作者：      刘金煌
创建日期：   2013年12月7日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef BUZZTABWIDGET_H
#define BUZZTABWIDGET_H

#include <QTabWidget>

class BuzzTabWidget : public QTabWidget
{
    Q_OBJECT
public:
    explicit BuzzTabWidget(QWidget *parent = 0);

public slots:
    /***************************************************************
    功能：切换Tab
    输入：无
    输出：同功能描述
    ****************************************************************/
    void CurrentChanged(int index);


private slots:
    /***************************************************************
    功能：停止鸣叫
    输入：无
    输出：同功能描述
    ****************************************************************/
    void StopBuzz();

};

#endif // BUZZTABWIDGET_H

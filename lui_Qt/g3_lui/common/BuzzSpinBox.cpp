#include "BuzzSpinBox.h"
#include <QKeyEvent>
#include "common/pubInclude.h"
#include "common/uidefine.h"

/* wufang,20130921,modify:3/4, for ENPCIPMS ID=23 , set cursor position to left*/
#include <QLineEdit>

BuzzSpinBox::BuzzSpinBox(QWidget *parent) :
    QSpinBox(parent)
{
    // 背景设置透明
    SET_STYLE_SPINBOX;
    setWrapping( true );
}

BuzzSpinBox::~BuzzSpinBox()
{

}

void BuzzSpinBox::setType(CtrlInputParam* ctrlParam)
{
    iKeyPressCount = 0;
    //TRACEDEBUG( "BuzzSpinBox::setType ipt<%d> %x", ctrlParam->ipt, this );
    m_ctrlParam = *ctrlParam;

    setPrefix( m_ctrlParam.strPrefix );
    setSuffix( m_ctrlParam.strSuffix );
    setSingleStep( m_ctrlParam.nStep );
    setRange(m_ctrlParam.nMin, m_ctrlParam.nMax);

    switch (m_ctrlParam.ipt)
    {
    case IPT_SBT_CHAR:
    case IPT_SBT_CHAR_PW:
        setRange( 0, 62 );
        break;

    case IPT_SBT_INT:
        //TRACEDEBUG( "BuzzSpinBox::setType nInit<%d>", m_ctrlParam.nInit );
        setValue( m_ctrlParam.nInit );
        break;

    case IPT_SBT_ULONG:
        //TRACEDEBUG( "BuzzSpinBox::setType nInit<%d>", m_ctrlParam.nInit );
        setValue( m_ctrlParam.ulInit );
        break;

    case IPT_SBT_CUSTOM:
    {
        setValue( m_ctrlParam.nInit );
    }
        break;

    case IPT_IP:
        setRange(0, 255);
        break;

    case IPT_IPV6_1:
    case IPT_IPV6_2:
    case IPT_IPV6_1_V:
    case IPT_IPV6_2_V:
    case IPT_IPV6_3_V:
        setRange(0, 0xFFFF);
        break;

    default:
        break;
    }
}

void BuzzSpinBox::setKeySpeedUp(int nLevel)
{
    if (m_ctrlParam.ipt == IPT_SBT_CHAR ||
            m_ctrlParam.ipt == IPT_SBT_CHAR_PW ||
            m_ctrlParam.ipt == IPT_SBT_CUSTOM)
    {
        setSingleStep( m_ctrlParam.nStep );
        return;
    }

    int nRange = maximum()-minimum();
    //TRACEDEBUG( "BuzzSpinBox::keyPressEvent nRange<%d>", nRange );
    if ( nLevel==0 )
    {
        setSingleStep( m_ctrlParam.nStep );
    }
    else
    {
        if (nRange > 100000)
        {
            setSingleStep( m_ctrlParam.nStep*600*nLevel );
        }
        else if (nRange > 10000)
        {
            //setSingleStep( m_ctrlParam.nStep*500*nLevel );
            setSingleStep( m_ctrlParam.nStep*100*nLevel );
        }
        else if (nRange > 1000)
        {
            setSingleStep( m_ctrlParam.nStep*50*nLevel );
        }
        else if (nRange > 500)
        {
            setSingleStep( m_ctrlParam.nStep*10*nLevel );
        }
        else if (nRange > 100)
        {
            setSingleStep( m_ctrlParam.nStep*10 );
        }
        else if (nRange > 50)
        {
            setSingleStep( m_ctrlParam.nStep+6 );
        }
        else
        {
            setSingleStep( m_ctrlParam.nStep );
        }
    }
}

void BuzzSpinBox::keyPressEvent(QKeyEvent *keyEvent)
{
    int nKey = keyEvent->key();
    //TRACEDEBUG( "BuzzSpinBox::keyPressEvent key<%x>", nKey );
    emit sigCellUnitKeyPress( nKey );

    if (nKey == Qt::Key_Up || nKey == Qt::Key_Down)
    {
        QTime tmNow = QTime::currentTime();
        int iElaps = tmNow.msecsTo(timeLastKeyUp);
        if(qAbs(iElaps) < KEY_PRESS_INTERVAL)
        {
            // 按一次键200ms
            iKeyPressCount++;
            iKeyPressCount = iKeyPressCount>400 ? 400:iKeyPressCount;
            setKeySpeedUp( iKeyPressCount/20 ); //x*200ms
        }
        else
        {
            iKeyPressCount = 0;
            setKeySpeedUp( 0 );
        }
        timeLastKeyUp = tmNow;
    }

    /* wufang,20130921,modify:4/4, for ENPCIPMS ID=23 , set cursor position to left*/
    lineEdit()->setCursorPosition(0);

    return QSpinBox::keyPressEvent(keyEvent);
}

void BuzzSpinBox::Enter(void*)
{
    lineEdit()->setCursorPosition( 0 );
    setFocus();
    iKeyPressCount = 0;
//    TRACELOG1( "BuzzDoubleSpinBox::Enter" );
}

void BuzzSpinBox::Leave()
{
    iKeyPressCount = 0;
}

//QValidator::State HexSpinBox::validate(QString &text, int &pos) const
//{
//    return validator->validate(text, pos);
//}

QString BuzzSpinBox::textFromValue(int value) const
{
    //TRACEDEBUG( "BuzzSpinBox::textFromValue %d ipt %d", value, m_ctrlParam.ipt );
    QString strRet;
    switch (m_ctrlParam.ipt)
    {
    case IPT_SBT_CHAR:
    case IPT_SBT_CHAR_PW:
    {
        int nNow = value;
        if (value<10)
        {
            // 0-9
            nNow += 0x30;
        }
        else if (value>=10 && value<36)
        {
            //10-35 a-z
            nNow += ('a'-10);
        }
        else if (value>=36 && value<62)
        {
            //36-61 A-Z
            nNow += ('A'-36);
        }
        else
        {
            nNow = (' ');
        }
        strRet = QString( nNow );
    }
        break;

    case IPT_SBT_INT:
    case IPT_SBT_ULONG:
    case IPT_IP:
    {
        return QSpinBox::textFromValue(value);
        //strRet = QString::number( value );
    }
        break;

    case IPT_IPV6_1:
    case IPT_IPV6_2:
    case IPT_IPV6_1_V:
    case IPT_IPV6_2_V:
    case IPT_IPV6_3_V:
    {
        return QString::number(value, 16).toUpper();
    }
        break;

    case IPT_SBT_CUSTOM:
//        TRACELOG1( "BuzzSpinBox::textFromValue IPT_SBT_CUSTOM count<%d> value<%d>",
//                   m_ctrlParam.strListEnumText.count(),
//                   value );

        if (m_ctrlParam.strListEnumText.count() > value)
        {
            strRet = m_ctrlParam.strListEnumText[value];
        }
        else
        {
            strRet = "";
        }
//        TRACELOG1( "BuzzSpinBox::textFromValue IPT_SBT_CUSTOM strRet<%s>", strRet.toUtf8().constData() );
        break;

    default:
        //TRACELOG1( "BuzzSpinBox::textFromValue default <%d>", m_ctrlParam.ipt );
        strRet = QString::number( value );
        break;
    }

    return strRet;
}


int BuzzSpinBox::valueFromText(const QString &text) const
{
    //TRACEDEBUG( "BuzzSpinBox::valueFromText %s", text.toUtf8().constData() );
    int nRet = 0;
    switch (m_ctrlParam.ipt)
    {
    case IPT_SBT_CHAR:
    case IPT_SBT_CHAR_PW:
    {
        char ch = text.at(0).toAscii();
        char ch2 = text.at(0).toAscii();
        QString str;
        if (ch<='9')
        {
            str = ch2;
        }
        else if (ch>='a' && ch<='z')
        {
            ch2 = ch-'a'+10;
            str = ch2;
        }
        else if (ch>='A' && ch<='Z')
        {
            ch2 = ch-'A'+36;
            str = ch2;
        }
        else
        {
            str = "32";
        }

        nRet = str.toInt();
        //TRACELOG1( "BuzzSpinBox::valueFromText IPT_SBT_CHAR text<%s>", str.toUtf8().constData() );
    }
        break;

    case IPT_SBT_INT:
    case IPT_SBT_ULONG:
    case IPT_IP:
        return QSpinBox::valueFromText(text);

    case IPT_IPV6_1:
    case IPT_IPV6_2:
    case IPT_IPV6_1_V:
    case IPT_IPV6_2_V:
    case IPT_IPV6_3_V:
    {
        bool ok;
        return text.toInt(&ok, 16);
    }

    case IPT_SBT_CUSTOM:
        //TRACELOG1( "BuzzSpinBox::valueFromText IPT_SBT_CUSTOM text<%s> value<%d> nRet<%d>", text.toUtf8().constData(), value(), nRet );
        nRet = value();
        break;

    default:
        nRet = text.toInt();
        break;
    }

    return nRet;
}

#include "buzztablewidget.h"

#include <QKeyEvent>
#include <QHeaderView>
#include "common/global.h"
#include "common/pubInclude.h"
#include "common/pubInclude.h"

BuzzTableWidget::BuzzTableWidget(QWidget *parent) :
    QTableWidget(parent)
{
    //编辑模式
    setEditTriggers(
                    QAbstractItemView::AnyKeyPressed );
    //选择模式
    setSelectionBehavior(
                QAbstractItemView::SelectRows);
    setSelectionMode(
                QAbstractItemView::SingleSelection);

    /*添加透明底色，使透明化*/
    QPalette palette;
    QPixmap img;    //img不加载图片，则默认透明
    palette.setBrush(QPalette::Window, QBrush(img));
    setPalette(palette);
}

void BuzzTableWidget::CellClicked(int, int)
{
    //if(column != 0 && column != 1)
    {
        sys_setBuzz( BUZZ_BEEP );
    }
}

void BuzzTableWidget::clearItems()
{
    int row = this->rowCount();
    int column = this->columnCount();
    //qDebug( "BuzzTableWidget::clearItems() row:%d col:%d", row, column );
    for (int i=row-1; i>=0; --i)
    {
        for (int j=column-1; j>=0; --j)
        {
            delete this->takeItem(i, j);
            //this->removeColumn(j);
        }
        this->removeRow(i);
    }
    this->clearContents();
}

void BuzzTableWidget::clearItemsTexts()
{
  int row = this->rowCount();
  int column = this->columnCount();
  //qDebug( "BuzzTableWidget::clearItems() row:%d col:%d", row, column );
  for (int i=row-1; i>=0; --i)
  {
      for (int j=column-1; j>=0; --j)
      {
        QTableWidgetItem *item = this->item(i,j);
        item->setText("");
      }
  }
}

void BuzzTableWidget::StopBuzz()
{
    if(sys_getBuzz() && sys_getSoundType() == BUTTON_BEEP)
    {
        sys_setBuzz( BUZZ_QUIET );
    }
}

int BuzzTableWidget::selectedRow()
{
    return currentRow();
//    QList<QTableWidgetItem*> list = selectedItems();
//    return (list.count()>0 ? list.at(0)->row():0);
}

void BuzzTableWidget::setSelectedItem(int row, int column)
{
    QTableWidgetItem* pItem = item(row, column);
    if (pItem)
    {
        pItem->setSelected( true );
    }
}

void BuzzTableWidget::Enter(void*)
{

}

void BuzzTableWidget::Leave()
{

}

// spaw key event in table
void BuzzTableWidget::keyPressEvent(QKeyEvent* keyEvent)
{
    m_key = keyEvent->key();
    //TRACEDEBUG( "BuzzTableWidget::keyPressEvent key<%x>", m_key );
    //QTableWidget::keyPressEvent(keyEvent);
    switch (m_key)
    {
        case Qt::Key_Return:
        case Qt::Key_Enter:
        case Qt::Key_Escape:
        {
            QTableWidget::keyPressEvent(keyEvent);
        }
        break;

        case Qt::Key_Up:
        {
            QTableWidget::keyPressEvent(keyEvent);
            emit sigTableKeyPress(m_key);
        }
        break;

        case Qt::Key_Down:
        {
            QTableWidget::keyPressEvent(keyEvent);
            emit sigTableKeyPress(m_key);
        }
        break;

        default:
        break;
    }
//    if (m_key==Qt::Key_Up || m_key==Qt::Key_Down)
//    {
//        TRACEDEBUG( "BuzzTableWidget::keyPressEvent emit sigTableKeyPress" );
//        QTableWidget::keyPressEvent(keyEvent);
//        emit sigTableKeyPress(m_key);
//    }
}

#include "BuzzDoubleSpinBox.h"
#include <QKeyEvent>
#include "common/pubInclude.h"
#include "common/InputCtrlDef.h"
#include "common/uidefine.h"

/* wufang,20130921,modify:1/4, for ENPCIPMS ID=23 , set cursor position to left*/
#include <QLineEdit>

BuzzDoubleSpinBox::BuzzDoubleSpinBox(QWidget *parent) :
    QDoubleSpinBox(parent)
{
    // ��������͸��
    SET_STYLE_SPINBOX;

    setWrapping( true );
}

BuzzDoubleSpinBox::~BuzzDoubleSpinBox()
{

}

void BuzzDoubleSpinBox::setType(CtrlInputParam* ctrlParam)
{
    iKeyPressCount = 0;
    m_ctrlParam = *ctrlParam;

    setSingleStep( m_ctrlParam.dbStep );
    setRange( m_ctrlParam.dbMin, m_ctrlParam.dbMax );
    setDecimals( m_ctrlParam.nDecimals );
    setValue( m_ctrlParam.dbInit );

    setPrefix( m_ctrlParam.strPrefix );
    setSuffix( m_ctrlParam.strSuffix );
}

void BuzzDoubleSpinBox::setKeySpeedUp(int nLevel)
{
    double fRange = maximum()-minimum();
    int nRange = 0;
    if (m_ctrlParam.dbStep > 9)
    {
        nRange = qRound( fRange/10 );
    }
    else if (m_ctrlParam.dbStep > 4.9)
    {
        nRange = qRound( fRange/5 );
    }
    else if (m_ctrlParam.dbStep > 0.9)
    {
        nRange = qRound( fRange );
    }
    else
    {
        nRange = qRound( fRange*10 );
    }

    TRACEDEBUG( "BuzzDoubleSpinBox::setKeySpeedUp step<%f> fRange<%f> nRange<%d>", m_ctrlParam.dbStep, fRange, nRange );
    if (nLevel == 0)
    {
        setSingleStep( m_ctrlParam.dbStep );
    }
    else
    {
        if (nRange > 100000)
        {
            setSingleStep( m_ctrlParam.dbStep*5000*nLevel );
        }
        else if (nRange > 10000)
        {
            setSingleStep( m_ctrlParam.dbStep*500*nLevel );
        }
        else if (nRange > 1000)
        {
            setSingleStep( m_ctrlParam.dbStep*50*nLevel );
        }
        else if (nRange > 500)
        {
            setSingleStep( m_ctrlParam.dbStep*10*nLevel );
        }
        else if (nRange > 100)
        {
            setSingleStep( m_ctrlParam.dbStep*10 );
        }
        else if (nRange > 50)
        {
            setSingleStep( m_ctrlParam.dbStep+6 );
        }
        else
        {
            setSingleStep( m_ctrlParam.dbStep );
        }
    }
}

void BuzzDoubleSpinBox::keyPressEvent(QKeyEvent *keyEvent)
{
    int nKey = keyEvent->key();
    if (nKey == Qt::Key_Up || nKey == Qt::Key_Down)
    {
        QTime tmNow = QTime::currentTime();
        int iElaps = tmNow.msecsTo(timeLastKeyUp);
        if(qAbs(iElaps) < KEY_PRESS_INTERVAL)
        {
            iKeyPressCount++;
            iKeyPressCount = iKeyPressCount>30 ? 30:iKeyPressCount;
            setKeySpeedUp( iKeyPressCount/6 );
        }
        else
        {
            iKeyPressCount = 0;
            setKeySpeedUp( 0 );
        }
        timeLastKeyUp = tmNow;
    }

    /* wufang,20130921,modify:2/4, for ENPCIPMS ID=23 , set cursor position to left*/
    lineEdit()->setCursorPosition(0);

    return QDoubleSpinBox::keyPressEvent(keyEvent);
}
void BuzzDoubleSpinBox::Enter(void*)
{
    lineEdit()->setCursorPosition( 0 );
    setFocus();
    iKeyPressCount = 0;
    TRACELOG1( "BuzzDoubleSpinBox::Enter" );
}

void BuzzDoubleSpinBox::Leave()
{
    iKeyPressCount = 0;
}

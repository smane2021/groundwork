/******************************************************************************
文件名：    uidefine.h
功能：      界面设置相关的宏
作者：      刘金煌
创建日期：   2013年3月25日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef UIDEFINE_H
#define UIDEFINE_H

/////////////////////////////////
#ifdef Q_OS_LINUX
    #define PATH_APP "/app/qt4/"
#else
    #define PATH_APP "./"
#endif
#define FILENAME_NEED_WIZARD  (PATH_APP "noWizard")
#define FILENAME_LANGUAGE     (PATH_APP "language")
#define FILENAME_LCD_ROTATION ("/etc/lcd_rotation")
#define PATH_IMG g_cfgParam->pathImg
#define fmLargeN  g_cfgParam->ms_pFmLargeN
#define fmLargeB  g_cfgParam->ms_pFmLargeB
#define fmSmallN  g_cfgParam->ms_pFmSmallN

#define FORMAT_DECIMAL   'f', iFormat
#define FORMAT_1DECIMAL  'f',1
#define INVALID_VALUE    (-9998)
// 初始化表格项对应的值索引属性，此值可以跳行
#define SIGIDX_INIT     (-1)

#define MAX_LEN_SITENAME (8)
#define MAX_LEN_PASSWORD (13)

#include <QColor>
// selected item color
#define LIGHT_COLOR_TABLE_SELECTED  "f1f1f1"
#define LIGHT_COLOR_ENTER           "333333"
static QColor textNormalColor( 0xFF,0xFF,0xFF );
static QColor textFocusColor(0x61,0x61,0x61);

static QColor coordGridColor(0xd1,0xd3,0xd4 );
static QColor coordAxisTextColor( 0xe1,0xe1,0xe1 );
static QColor coordAxisColor( textFocusColor );
static QColor nodataColor(Qt::red);
// orange
static QColor chartYesColor(130, 190, 32);
static QColor chartNotColor(Qt::gray);
static QColor chartWarnColor(210,50,65);

static QColor curveColor(130, 190, 32);
static QColor barColor( 130, 190, 32 );
static QColor battBackColor(Qt::gray);


// 2015.1.20 修改回最初颜色
static QColor gs_pieMarginColors[5] = {
    QColor(255,140,0),   // orange
    QColor(255,204,0), // yellow
    QColor(130,190,32),   // Green
    QColor(255,204,0),
    QColor(255,140,0)
};

//#define COLOR_LINE1 "255,255,  0" // Yellow
//#define COLOR_LINE2 "  0,255,  0" // Green
//#define COLOR_LINE3 "255,  0,  0" // Red

#define COLOR_LINE1 "255,0,255"
#define COLOR_LINE2 "255,102,0"
#define COLOR_LINE3 "0,0,255"

// DC Batt thermometer
static QColor gs_meterMarginColors[5] = {
  QColor(255,140,0),   // orange
  QColor(255,204,0), // yellow
  QColor(130,190,32),   // Green
  QColor(255,204,0),
  QColor(255,140,0)
};

static QColor norSigColor(0, 0, 0);
static QColor abnorSigColor(70, 70, 70);

static QColor OAcolor(Qt::darkYellow);
static QColor MAcolor(Qt::darkRed);
static QColor CAcolor(Qt::red);
static QColor NAcolor(Qt::transparent);  //透明

//温度显示的格式类型（摄氏度或华氏度显示）
enum _Temp_Display_Format
{
    TEMP_FORMAT_TYPE_CELS = 0,      //摄氏度
    TEMP_FORMAT_TYPE_FAHR           //华氏度
};
typedef enum _Temp_Display_Format TEMP_DISP_FORMAT;

// old cfg
// 显示界面
enum WIDGET_TYPE
{
/*First*/
    WT1_AC,          // 0 WdgFP0AC
    WT1_MODULE,           //   WdgFP1Module
    WT1_DCV,              // 2 WdgFP2DCV
    WT1_DCA,              //   WdgFP3DCA
    WT1_DCDEG_METER,      //   WdgFP4Deg1
    WT1_DCDEG_CURVE,      //   WdgFP5Deg2Curve
    WT1_BATT_REMAIN_TIME, // 6 WdgFP6BattRemainTime
    WT1_BATT_DEG_METER,   //   WdgFP7BattDegMeter
    WT1_BATT_DEG_CURVE,   //   WdgFP8BattDegCurve
    WT1_ALARM,            // 9  FirstStackedWdg
    WT1_INVENTORY,        // 10 WdgFP10Inventory
    WT1_SCREENSAVER,      // 11 WdgFP11ScreenSaver
    WT1_FIRST_MAX,
/*Second*/
    WT2_RECTINFO,          // 14 Wdg2Table                   WT1_MODULE
    WT2_SOLINFO,                 //    Wdg2Table
    WT2_CONVINFO,                //    Wdg2Table
    WT2_SLAVE1INFO,              //    Wdg2Table
    WT2_SLAVE2INFO,              //    Wdg2Table
    WT2_SLAVE3INFO,              //    Wdg2Table
    WT2_DCA_BRANCH,              //    Wdg2DCABranch               WT1_DCA
    WT2_DCDEG_METER_BRANCH,      //    Wdg2DCDeg1Branch DC支路温度   WT1_DCDEG_METER
    WT2_BATT_SINGLE_REMAIN_TIME, //    Wdg2P5BattRemainTime 电池时间 WT1_BATT_REMAIN_TIME
    WT2_BATT_SINGLE_DEG,         //    Wdg2P6BattDeg 电池温度
    WT2_ACT_ALARM,               //    Wdg2Table alarm
    WT2_HIS_ALARM,               //    Wdg2Table
    WT2_EVENT_LOG,               //    Wdg2Table
    WT2_INVENTORY,               //    SecondStackedWdg
    WT2_SOURCE_INFO,
    WT2_CFG_SETTING,             //    Wdg2P9Cfg
    WT2_SECOND_MAX,              //
/*Third*/
    WT3_ACT_ALARM,          //    Wdg3Table
    WT3_HIS_ALARM,               //    Wdg3Table
    WT3_CFG_SETTING_OTHER,       //    Wdg2P9Cfg
    WT3_CFG_SETTING_OTHER_BATT1, //    Wdg2P9Cfg
    WT3_CFG_SETTING_OTHER_BATT2, //    Wdg2P9Cfg
    WT3_THIRD_MAX,
/*Fourth*/
    WT4_ALARM_HELP,   // 36  Wdg3Table 第四层界面只有一个告警帮助
    WT4_FOURTH_MAX,
    WT_MAX_BASE,

    WT_GUIDE_WINDOW,
    WT_MAX_GUIDE,

    WT_HOME_WINDOW,
    WT_MAX_HOME,

    WT_LOGIN_WINDOW,
    WT_MAX_LOGIN,

    WT_MAIN_WINDOW,
    WT_MAX_MAIN,

    WT_MAX
};

enum WIDGET_GUIDE
{
    WGUIDE_LANGUAGE,
    WGUIDE_ENTERESC,
    WGUIDE_WIZARD,
    WGUIDE_CONTINUE,
    WGUIDE_FINISH,
    WGUIDE_PASSWORD,
    WGUIDE_MAX//changed by hhy

};

enum WIDGET_INPUT_TYPE
{
    WDG_INPUT_GUIDE,
    WDG_INPUT_CONFIG,
    WDG_INPUT_MAX
};

typedef struct _EnterInputParam
{
    int  wdgInputType;
    int  screenID;
} EnterInputParam_t;

typedef struct _EnterGuideParam
{
    enum WIDGET_GUIDE wt_Guide;
    int  screenID;
} EnterGuideParam_t;

enum POPUP_TYPE
{
    POPUP_TYPE_GUIDE_WINDOW, // wizard是否继续框
    POPUP_TYPE_QUESTION,     // 弹出框
    POPUP_TYPE_QUESTION_LANGUAGE,
    POPUP_TYPE_INFOR,
    POPUP_TYPE_WARNING,
    POPUP_TYPE_CRITICAL,
    POPUP_TYPE_SETFAILED,
    POPUP_TYPE_INFOR_ALIVE,
    POPUP_TYPE_QUERY_CMDEXEC,
    POPUP_TYPE_QUESTION_PASSWORD,
    POPUP_TYPE_MAX

};

enum LOGIN_TYPE
{
    LOGIN_TYPE_GUIDE,
    LOGIN_TYPE_CONFIG
};

enum ENTER_PAGE_TYPE
{
    ENTER_PAGE_TYPE_KEY_ESC,
    ENTER_PAGE_TYPE_KEY_ENT,
    ENTER_PAGE_TYPE_AUTO
};

#define TIME_RESET_FRAME_BUFFER   (10*1000/TIMER_DETECT_ALARM_INTERVAL)
// screen saver
#define TIME_SCREEN_SAVER         (8*60*1000/TIMER_DETECT_ALARM_INTERVAL)
// 需要跳转到当前告警页面的无按键间隔时间
#define TIME_GOTO_ALARM_PAGE_NOKEY (2*60*1000)
// 按键扫描间隔
#define KEY_PRESS_INTERVAL      (250)
#define TIME_OUT_LANG           (5*60*1000)
#define TIME_OUT_WIZARD         (1*60*1000)
#define TIME_OUT_POPUPDLG       (15*1000)
#define TIME_OUT_LoginWindow    (10*1000)
#define TIME_OUT_LOGIN_USER     (8*60*1000)
#define TIME_WAIT_SYNC          (-1)

#endif // UIDEFINE_H

/******************************************************************************
文件名：    buzztablewidget.h
功能：      QTableWidget的继承类，实现按键蜂鸣器响
作者：      刘金煌
创建日期：   2013年12月6日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef BUZZTABLEWIDGET_H
#define BUZZTABLEWIDGET_H

#include <QTableWidget>
#include <QHeaderView>

#include "common/uidefine.h"
#include "config/PosTableWidget.h"

// tablewidget parameter

enum TABLE_TYPE
{
    TT_NOT_TITLE_NOT_SCROOL,
    TT_NOT_TITLE_YES_SCROOL,
    TT_YES_TITLE_NOT_SCROOL,
    TT_YES_TITLE_YES_SCROOL
};
// count of rows per tablewidget page
#define TABLEWDG_ROWS_PERPAGE_NOTITLE \
    (PosTableWidget::rowsPerpageNotitle)

#define TABLEWDG_ROWS_PERPAGE_TITLE   \
    (PosTableWidget::rowsPerpageTitle)
// row height(6 rows)
//#define TABLEWDG_ROW_HEIGHT       (20)
#define TABLEWDG_ROW_HEIGHT       (PosTableWidget::rowHeight)
#define TABLEWDG_ROW_WIDTH       (PosTableWidget::langWidth - 10)
// column width(one column only)
#define SET_TABLEWDG_STYLE(TableWdg, TT) \
{ \
    ui->TableWdg->setColumnCount(1); \
    /*去掉表头*/ \
    ui->TableWdg->horizontalHeader()->setHidden(true); \
    ui->TableWdg->verticalHeader()->setHidden(true);\
    /*去掉滚动条*/ \
    ui->TableWdg->setHorizontalScrollBarPolicy( \
                Qt::ScrollBarAlwaysOff); \
    ui->TableWdg->setVerticalScrollBarPolicy( \
                Qt::ScrollBarAlwaysOff); \
    /*无边框*/ \
    ui->TableWdg->setFrameShape(QFrame::NoFrame); \
    /*无格子线*/ \
    ui->TableWdg->setShowGrid(false); \
    /*编辑模式*/ \
    ui->TableWdg->setEditTriggers( \
        QAbstractItemView::NoEditTriggers ); \
    /*选择模式*/ \
    ui->TableWdg->setSelectionBehavior( \
        QAbstractItemView::SelectRows); \
    ui->TableWdg->setSelectionMode( \
        QAbstractItemView::SingleSelection); \
    ui->TableWdg->setStyleSheet( \
                "QTableWidget{" \
                "background-color:rgba(255, 255, 255, 0);" \
                "selection-color:#111111;" \
                "selection-background-color:#" LIGHT_COLOR_TABLE_SELECTED ";}" \
                ); \
    /*字体*/ \
    ui->TableWdg->setFont( g_cfgParam->gFontLargeN ); \
    switch (TT) \
    { \
    case TT_NOT_TITLE_NOT_SCROOL: \
        ui->TableWdg->setGeometry( \
            PosTableWidget::xNotTitle, \
            PosTableWidget::yNotTitle, \
            PosTableWidget::widthNotScrool, \
            PosTableWidget::heightNotTitle); \
        ui->TableWdg->setColumnWidth(0, PosTableWidget::widthNotScrool); \
        break; \
    case TT_NOT_TITLE_YES_SCROOL: \
        ui->TableWdg->setGeometry( \
            PosTableWidget::xNotTitle, \
            PosTableWidget::yNotTitle, \
            PosTableWidget::widthHasScrool, \
            PosTableWidget::heightNotTitle); \
        ui->TableWdg->setColumnWidth(0, PosTableWidget::widthHasScrool); \
        break; \
    case TT_YES_TITLE_NOT_SCROOL: \
        ui->TableWdg->setGeometry( \
            PosTableWidget::xHasTitle, \
            PosTableWidget::yHasTitle, \
            PosTableWidget::widthNotScrool, \
            PosTableWidget::heightHasTitle); \
        ui->TableWdg->setColumnWidth(0, PosTableWidget::widthNotScrool); \
        break; \
    case TT_YES_TITLE_YES_SCROOL: \
        ui->TableWdg->setGeometry( \
            PosTableWidget::xHasTitle, \
            PosTableWidget::yHasTitle, \
            PosTableWidget::widthHasScrool, \
            PosTableWidget::heightHasTitle); \
        ui->TableWdg->setColumnWidth(0, PosTableWidget::widthHasScrool); \
        break; \
    default: \
        break; \
    } \
    this->setFocusPolicy( Qt::WheelFocus ); \
}


class BuzzTableWidget : public QTableWidget
{
    Q_OBJECT
public:
    explicit BuzzTableWidget(QWidget *parent = 0);

public:
    int   selectedRow();
    void  setSelectedItem(int row, int column);
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void clearItems();
    void clearItemsTexts();

protected:
    virtual void keyPressEvent(QKeyEvent* keyEvent);

public slots:
    /***************************************************************
    功能：切换Tab
    输入：无
    输出：同功能描述
    ****************************************************************/
    void CellClicked (int , int);

private slots:
    /***************************************************************
    功能：停止鸣叫
    输入：无
    输出：同功能描述
    ****************************************************************/
    void StopBuzz();

signals:
    void sigTableKeyPress(int key);
private:
    int m_key;
};

#endif // BUZZTABLEWIDGET_H

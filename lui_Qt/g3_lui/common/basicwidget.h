/******************************************************************************
文件名：    basicwidget.h
功能：      LUI的窗口父类，所有的窗口部件都继承于此父类
作者：      刘金煌
创建日期：   2013年03月29日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef BASICWIDGET_H
#define BASICWIDGET_H

#include <QWidget>
#include <QStyleOption>
#include <QPainter>
#include "dataDef.h"
#include "Macro.h"
#include "config/PosBase.h"

/*图标数据*/
class IconButtonData
{
public:
    QIcon * normalIcon;         //正常图标
    QIcon * abnormalIcon;       //异常图标
    QSize iconSize;             //图标大小
    //QRegion * region;         //掩码区域(即按钮的显示区域，不显示的部分按下不响应，用以制作不规则的图标)
    QString iconName[LANG_TYPE_MAX];     //图标显示的名称（ToolButton使用）
};
//class BasicWidget : public QDialog
class BasicWidget : public QWidget
{
    Q_OBJECT

public :
    BasicWidget( QWidget * parent = 0 );
    virtual ~BasicWidget();

public:
    /****************************************************************
    功能：进入此窗口，即从其它界面切换至此界面
    输入：无
    输出：无
    *****************************************************************/
    virtual void Enter(void* param=NULL);

    /****************************************************************
    功能：离开此窗口，即从此界面切换至其它窗口
    输入：无
    输出：无
    *****************************************************************/
    virtual void Leave();

    /***************************************************************
    功能：刷新窗口显示，在刷新窗口之前建议使用getData函数以获取最新显示数据
    输入：无
    输出：无
    ****************************************************************/
    virtual void Refresh();

protected:
    /****************************************************************
    功能：显示数据，具体数据因具体窗口而异
    输入：无
    输出：无
    *****************************************************************/
    virtual void ShowData(void* pData);

    /****************************************************************
    功能：初始化窗口内相关的widget
    输入：无
    输出：无
    *****************************************************************/
    virtual void InitWidget();

    /****************************************************************
    功能：初始化此widget相关的信号与槽的连接
    输入：无
    输出：无
    *****************************************************************/
    virtual void InitConnect();
    virtual QString GetType();

    virtual void paintEvent ( QPaintEvent * event );

public:
    static BasicWidget* ms_showingWdg;

public:
    QString m_strType;
};

#endif // BASICWIDGET_H

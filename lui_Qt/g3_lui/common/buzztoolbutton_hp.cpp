#include "buzztoolbutton_hp.h"
#include "common/global.h"
#include "common/uidefine.h"
#include "common/pubInclude.h"

BuzzToolButton_HP::BuzzToolButton_HP(QWidget *parent) :
    BuzzToolButton(parent)
{
    connect(this, SIGNAL(clicked()), this, SLOT(Press()));
//    setStyleSheet(
//            "QToolButton"
//            BTN_STYLE_HP_UP

//            "QToolButton:hover"
//            BTN_STYLE_HP_UP

//            "QToolButton:hover:pressed"
//            BTN_STYLE_HP_DOWN

//            "QToolButton:hover:!pressed"
//            BTN_STYLE_HP_UP
//            );
}
void BuzzToolButton_HP::Press()
{
    //Buzz::SetBeepType(BUTTON_BEEP);

    //������100�����رյ�
    //Buzz::Moo();
    //QTimer::singleShot(BEEP_DURATION, this, SLOT(StopBuzz()));

    sys_setBuzz( BUZZ_BEEP );
}

void BuzzToolButton_HP::Release()
{
    sys_setBuzz( BUZZ_QUIET );
}

void BuzzToolButton_HP::StopBuzz()
{
    if(sys_getBuzz() && sys_getSoundType() == BUTTON_BEEP)
    {
        sys_setBuzz( BUZZ_QUIET );
    }
}

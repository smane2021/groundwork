#ifndef BUZZDOUBLESPINBOX_H
#define BUZZDOUBLESPINBOX_H

#include <QDoubleSpinBox>
#include "common/InputCtrlDef.h"
#include <QTime>

class BuzzDoubleSpinBox : public QDoubleSpinBox
{
    Q_OBJECT
public:
    explicit BuzzDoubleSpinBox(QWidget *parent = 0);
    ~BuzzDoubleSpinBox();

public:
    void setType(CtrlInputParam* ctrlParam);
    virtual void Enter(void* param=NULL);
    virtual void Leave();

private:
    void setKeySpeedUp(int nLevel);//设置加速按键

protected:
    virtual void keyPressEvent(QKeyEvent *keyEvent);
    //virtual void keyReleaseEvent(QKeyEvent *keyEvent);

private:
    CtrlInputParam m_ctrlParam;
    QTime timeLastKeyUp;//上一次按键抬起动作的时间，用于判断是不是长按
    int iKeyPressCount; //连续按键次数
};

#endif // BUZZDOUBLESPINBOX_H

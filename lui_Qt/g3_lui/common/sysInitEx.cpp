#include "sysInitEx.h"

#define EQPID_SYSTEM                  (1)
#define SIGID_CTRLALM_VOICE           (251)
#define ALM_VOICE_ACTIVE_NO           (0)
#define ALM_VOICE_ACTIVE_YES          (1)

int sys_setBuzzEx(enum SET_BUZZ buzz, int iType)
{
  sys_setBuzz(buzz,iType);

  void *pData = NULL;
  CMD_INFO cmdItem;
  cmdItem.CmdType = CT_SET;
  cmdItem.ScreenID = SCREEN_ID_SetBuzzStatus;
  cmdItem.setinfo.EquipID = EQPID_SYSTEM;
  cmdItem.setinfo.SigID = SIGID_CTRLALM_VOICE;
  cmdItem.setinfo.SigType = SIG_TYPE_SAMPLING;
  cmdItem.setinfo.value.enumValue = ((buzz == BUZZ_BEEP) || (buzz == BUZZ_MOO)) ? ALM_VOICE_ACTIVE_YES : ALM_VOICE_ACTIVE_NO;

  data_getDataSync (&cmdItem,&pData);
}
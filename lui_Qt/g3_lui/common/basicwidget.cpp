/******************************************************************************
文件名：    basicwidget.cpp
功能：      LUI的窗口父类，所有的窗口部件都继承于此父类
           包含4个窗口基础操作函数：GetData，Refresh，Enter，Leave
作者：      刘金煌
创建日期：   2013年03月29日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "basicwidget.h"

BasicWidget* BasicWidget::ms_showingWdg = NULL;

BasicWidget::BasicWidget( QWidget * parent ):QWidget( parent )
{
  m_strType = "";
}

BasicWidget::~BasicWidget()
{
    ;
}

/*界面的获取数据的操作*/
void BasicWidget::ShowData(void* pData)
{
    Q_UNUSED( pData );
}

/*界面的定时刷新操作*/
void BasicWidget::Refresh()
{
    ;
}

/*进入界面要进行的操作*/
void BasicWidget::Enter(void*)
{
    ;
}

/*离开界面要进行的操作*/
void BasicWidget::Leave()
{
    ;
}

/*初始化静态UI的操作*/
void BasicWidget::InitWidget()
{
    ;
}

/*初始化connect链接的操作*/
void BasicWidget::InitConnect()
{
    ;
}

QString BasicWidget::GetType()
{
    return m_strType;
}

void BasicWidget::paintEvent(QPaintEvent *)
 {
     QStyleOption opt;
     opt.init(this);
     QPainter p(this);
     style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
 }

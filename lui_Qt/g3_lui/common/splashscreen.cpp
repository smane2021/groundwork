#include "splashscreen.h"

SplashScreen::SplashScreen(QApplication *app, const QPixmap &pix)
    : QSplashScreen(pix)
{
    this->app = app;
    this->setWindowFlags(Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
}

SplashScreen::~SplashScreen()
{
    ;
}

#include "buzztabwidget.h"

#include <QTimer>
#include <QTableWidgetItem>

#include "common/global.h"
#include "common/pubInclude.h"


BuzzTabWidget::BuzzTabWidget(QWidget *parent) :
    QTabWidget(parent)
{
    /*放在构造函数中，会在生成tabWidget时，因为改变cuurentTab而产生currentChanged的信号
      造成蜂鸣器误响，所以信号与槽链接放到tabWidget构造完成后*/
    //connect(this, SIGNAL(currentChanged(int)), this, SLOT(CurrentChanged(int)));

}


void BuzzTabWidget::CurrentChanged(int index)
{
    index = index;  //无用，防止出现warning
    //Buzz::SetBeepType(BUTTON_BEEP);

    //蜂鸣器100毫秒后关闭掉
    //Buzz::Moo();
    //QTimer::singleShot(BEEP_DURATION, this, SLOT(StopBuzz()));

    sys_setBuzz( BUZZ_BEEP );
}

void BuzzTabWidget::StopBuzz()
{
    if(sys_getBuzz() && sys_getSoundType() == BUTTON_BEEP)
    {
        sys_setBuzz( BUZZ_QUIET );
    }
}

/******************************************************************************
文件名：    buzzlistwidget.h
功能：      QListWidget的继承类，实现按键蜂鸣器响
作者：      刘金煌
创建日期：   2013年12月6日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef BUZZLISTWIDGET_H
#define BUZZLISTWIDGET_H

#include <QListWidget>

class BuzzListWidget : public QListWidget
{
    Q_OBJECT
public:
    explicit BuzzListWidget(QWidget *parent = 0);

public slots:
    /***************************************************************
    功能：切换Tab
    输入：无
    输出：同功能描述
    ****************************************************************/
    void CurrentRowChanged(int index);


private slots:
    /***************************************************************
    功能：停止鸣叫
    输入：无
    输出：同功能描述
    ****************************************************************/
    void StopBuzz();

};

#endif // BUZZLISTWIDGET_H

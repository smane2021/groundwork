/******************************************************************************
文件名：    CtrlInputChar.cpp
功能：      输入数据控件
作者：      刘金煌
创建日期：   2013年04月10日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "CtrlInputChar.h"
#include "ui_CtrlInputChar.h"

#include <QKeyEvent>
#include "common/buzztablewidget.h"
#include "common/pubInclude.h"
#include "config/configparam.h"
#include "config/PosBase.h"
#include "common/global.h"

#define CHAR_WIDTH_LABEL_DIGITAL    (6)
//#define CHAR_WIDTH_SPINBOX  (20)
#define CHAR_WIDTH_SPINBOX  (PosBase::iSPinBox_Char_Width)

QString CtrlInputChar::m_strIPV6;

CtrlInputChar::CtrlInputChar(QWidget *parent, CtrlInputParam* pCtrlParam) :
    BasicWidget(parent),
    ui(new Ui::CtrlInputChar)
{
    ui->setupUi(this);

    m_nPrefixLen = 0;
    m_nSuffixLen = 0;

    m_ctrlParam = *pCtrlParam;
    //setParam( pCtrlParam );

    memset(&m_stMenuNode, 0, sizeof(m_stMenuNode));
    m_stMenuNode.iNodeId = MENUNODE_ID_INVALID;
    m_bIsMenuNode = false;

    InitWidget();
    InitConnect();
}

CtrlInputChar::~CtrlInputChar()
{
    delete ui;
}

void CtrlInputChar::InitWidget()
{
    setFont(g_cfgParam->gFontLargeN);
    ui->spinBox_single->setGeometry(
                0,
                0,
                TABLEWDG_ROW_WIDTH,
                TABLEWDG_ROW_HEIGHT
                );


    ui->spinBox_enum->setGeometry(
                0,
                0,
                TABLEWDG_ROW_WIDTH,
                TABLEWDG_ROW_HEIGHT
                );

    ui->dSpinBox_single->setGeometry(
                0,
                0,
                TABLEWDG_ROW_WIDTH,
                TABLEWDG_ROW_HEIGHT
                );


    // date time
    ui->dateEdit->setStyleSheet("background-color:rgba(0,0,0, 0);");
    ui->dateEdit->setButtonSymbols( QAbstractSpinBox::NoButtons );
    ui->dateEdit->setLayoutDirection( Qt::LeftToRight );
    ui->dateEdit->setDisplayFormat( "yyyy-MM-dd" );
    ui->dateEdit->setCurrentSectionIndex( 0 );
    ui->dateEdit->setGeometry(
                3,
                0,
                PosTableWidget::widthNotScrool,
                TABLEWDG_ROW_HEIGHT
                );

    ui->timeEdit->setStyleSheet("background-color:rgba(0,0,0, 0);");
    ui->timeEdit->setButtonSymbols( QAbstractSpinBox::NoButtons );
    ui->timeEdit->setLayoutDirection( Qt::LeftToRight );
    ui->timeEdit->setDisplayFormat( "HH:mm:ss" );
    ui->timeEdit->setCurrentSectionIndex( 0 );
    ui->timeEdit->setGeometry(
                3,
                0,
                PosTableWidget::widthNotScrool,
                TABLEWDG_ROW_HEIGHT
                );
}

void CtrlInputChar::InitConnect()
{
    connect( ui->spinBox_char,
             SIGNAL(sigCellUnitKeyPress(int)),
             this,
             SIGNAL(sigCellUnitKeyPress(int)) );

    connect( ui->spinBox_enum,
             SIGNAL(sigCellUnitKeyPress(int)),
             this,
             SIGNAL(sigCellUnitKeyPress(int)) );
}

void CtrlInputChar::Enter(void*)
{
    m_key = Qt::Key_Enter;
    m_nEnter      = 1;

    TRACELOG1( "CtrlInputChar::Enter() ipt<%d> m_nEnter<%d>",
               m_ctrlParam.ipt, m_nEnter );

    switch (m_ctrlParam.ipt)
    {
        case IPT_LABEL_READONLY:
        {
            ui->label_old->clear();
        }
        break;

        case IPT_SBT_CHAR:
        {
            ui->label_new->clear();
            ui->label_new->setVisible( true );

            ui->label_old->clear();
            ui->label_old->setVisible( false );

            ui->spinBox_char->setValue( 62 );
            ui->spinBox_char->Enter();
            ui->spinBox_char->setGeometry(
                        0, 0,
                        CHAR_WIDTH_SPINBOX,
                        TABLEWDG_ROW_HEIGHT
                        );
            ui->spinBox_char->setVisible( true );
        }
        break;

        case IPT_SBT_CHAR_PW:
        {
            ui->label_new->clear();
            ui->label_new->setVisible( false );

            ui->label_pw->clear();
            ui->label_pw->setGeometry(0, 0, 0, TABLEWDG_ROW_HEIGHT);
            ui->label_pw->setVisible( true );

            ui->spinBox_char->setValue( 62 );
            ui->spinBox_char->Enter();
            ui->spinBox_char->setGeometry(
                        0, 0,
                        CHAR_WIDTH_SPINBOX,
                        TABLEWDG_ROW_HEIGHT
                        );
            ui->spinBox_char->setVisible( true );
        }
        break;

        case IPT_SBT_INT:
        case IPT_SBT_ULONG:
        {
            ui->spinBox_single->Enter();
        }
        break;

        case IPT_SBT_CUSTOM:
        {
            ui->spinBox_enum->Enter();
        }
        break;

        case IPT_DSBT_SINGLE:
        {
            ui->dSpinBox_single->Enter();
        }
        break;

        case IPT_DATE:
        {
            ui->dateEdit->Enter();
        }
        break;

        case IPT_TIME:
        {
            ui->timeEdit->Enter();
        }
        break;

        case IPT_IP:
        {
            ui->spinBox_IP1->Enter();
            m_nIPCtrlNow = 1;
        }
        break;

        case IPT_IPV6_1:
        case IPT_IPV6_2:
        case IPT_IPV6_1_V:
        case IPT_IPV6_2_V:
        case IPT_IPV6_3_V:
        {
            ui->spinBox_IP1->Enter();
            m_nIPCtrlNow = 1;
        }
        break;

        default:
            break;
    }
}

void CtrlInputChar::Leave()
{
    ;
}

void CtrlInputChar::Reset()
{
    //TRACEDEBUG( "CtrlInputChar::Reset()" );
    setParam( &m_ctrlParam );
}

void CtrlInputChar::setHighLight(bool bHighLight)
{
#define COLOR_HIGHLIGHTED_FONT "background-color:rgba(241,241,241,0);color:#111111;"
#define COLOR_NORMAL_FONT      "background-color:rgba(77,77,77,0);color:#f1f1f1;"

    if ( bHighLight )
    {
        switch (m_ctrlParam.ipt)
        {
            case IPT_LABEL_READONLY:
            {
                ui->label_old->setStyleSheet( COLOR_HIGHLIGHTED_FONT );
            }
            break;

            case IPT_SBT_CHAR:
            {
                ui->label_old->setStyleSheet( COLOR_HIGHLIGHTED_FONT );
                ui->label_new->setStyleSheet( COLOR_HIGHLIGHTED_FONT );
                ui->spinBox_char->setStyleSheet( COLOR_HIGHLIGHTED_FONT );
            }
            break;

            case IPT_SBT_CHAR_PW:
            {
                ui->label_pw->setStyleSheet( COLOR_HIGHLIGHTED_FONT );
                ui->spinBox_char->setStyleSheet( COLOR_HIGHLIGHTED_FONT );
            }
            break;

            case IPT_SBT_INT:
            case IPT_SBT_ULONG:
            {
                ui->spinBox_single->setStyleSheet( COLOR_HIGHLIGHTED_FONT );
            }
            break;

            case IPT_SBT_CUSTOM:
            {
                ui->spinBox_enum->setStyleSheet( COLOR_HIGHLIGHTED_FONT );
            }
            break;

            //DoubleSpinBox
            case IPT_DSBT_SINGLE:
            {
                ui->dSpinBox_single->setStyleSheet( COLOR_HIGHLIGHTED_FONT );
            }
            break;

            case IPT_DATE:
            {
                ui->dateEdit->setStyleSheet( COLOR_HIGHLIGHTED_FONT );
            }
            break;

            case IPT_TIME:
            {
                ui->timeEdit->setStyleSheet( COLOR_HIGHLIGHTED_FONT );
            }
            break;

            case IPT_IP:
            {
                ui->spinBox_IP1->setStyleSheet( COLOR_HIGHLIGHTED_FONT );
                ui->label_IP1->setStyleSheet( COLOR_HIGHLIGHTED_FONT );

                ui->spinBox_IP2->setStyleSheet( COLOR_HIGHLIGHTED_FONT );
                ui->label_IP2->setStyleSheet( COLOR_HIGHLIGHTED_FONT );

                ui->spinBox_IP3->setStyleSheet( COLOR_HIGHLIGHTED_FONT );
                ui->label_IP3->setStyleSheet( COLOR_HIGHLIGHTED_FONT );

                ui->spinBox_IP4->setStyleSheet( COLOR_HIGHLIGHTED_FONT );
            }
            break;

            case IPT_IPV6_1:
            case IPT_IPV6_2:
            case IPT_IPV6_1_V:
            case IPT_IPV6_2_V:
            case IPT_IPV6_3_V:
            {
                ui->spinBox_IP1->setStyleSheet( COLOR_HIGHLIGHTED_FONT );
                ui->label_IP1->setStyleSheet( COLOR_HIGHLIGHTED_FONT );

                ui->spinBox_IP2->setStyleSheet( COLOR_HIGHLIGHTED_FONT );
                ui->label_IP2->setStyleSheet( COLOR_HIGHLIGHTED_FONT );

                ui->spinBox_IP3->setStyleSheet( COLOR_HIGHLIGHTED_FONT );
                ui->label_IP3->setStyleSheet( COLOR_HIGHLIGHTED_FONT );

                ui->spinBox_IP4->setStyleSheet( COLOR_HIGHLIGHTED_FONT );
            }
            break;

            default:
            break;
        }
    }
    else
    {
        switch (m_ctrlParam.ipt)
        {
            case IPT_LABEL_READONLY:
            {
                ui->label_old->setStyleSheet( COLOR_NORMAL_FONT );
            }
            break;

            case IPT_SBT_CHAR:
            {
                ui->label_old->setStyleSheet( COLOR_NORMAL_FONT );
                ui->spinBox_char->setStyleSheet( COLOR_NORMAL_FONT );
            }
            break;

            case IPT_SBT_CHAR_PW:
            {
                ui->label_pw->setStyleSheet( COLOR_NORMAL_FONT );
                ui->spinBox_char->setStyleSheet( COLOR_NORMAL_FONT );
            }
            break;

            case IPT_SBT_INT:
            case IPT_SBT_ULONG:
            {
                ui->spinBox_single->setStyleSheet( COLOR_NORMAL_FONT );
            }
            break;

            case IPT_SBT_CUSTOM:
            {
                ui->spinBox_enum->setStyleSheet( COLOR_NORMAL_FONT );
            }
            break;

                //DoubleSpinBox
            case IPT_DSBT_SINGLE:
            {
                ui->dSpinBox_single->setStyleSheet( COLOR_NORMAL_FONT );
            }
            break;

            case IPT_DATE:
            {
                ui->dateEdit->setStyleSheet( COLOR_NORMAL_FONT );
            }
            break;

            case IPT_TIME:
            {
                ui->timeEdit->setStyleSheet( COLOR_NORMAL_FONT );
            }
            break;

            case IPT_IP:
            {
                ui->spinBox_IP1->setStyleSheet( COLOR_NORMAL_FONT );
                ui->label_IP1->setStyleSheet( COLOR_NORMAL_FONT );

                ui->spinBox_IP2->setStyleSheet( COLOR_NORMAL_FONT );
                ui->label_IP2->setStyleSheet( COLOR_NORMAL_FONT );

                ui->spinBox_IP3->setStyleSheet( COLOR_NORMAL_FONT );
                ui->label_IP3->setStyleSheet( COLOR_NORMAL_FONT );

                ui->spinBox_IP4->setStyleSheet( COLOR_NORMAL_FONT );
            }
            break;

            case IPT_IPV6_1:
            case IPT_IPV6_2:
            case IPT_IPV6_1_V:
            case IPT_IPV6_2_V:
            case IPT_IPV6_3_V:
            {
                ui->spinBox_IP1->setStyleSheet( COLOR_NORMAL_FONT );
                ui->label_IP1->setStyleSheet( COLOR_NORMAL_FONT );

                ui->spinBox_IP2->setStyleSheet( COLOR_NORMAL_FONT );
                ui->label_IP2->setStyleSheet( COLOR_NORMAL_FONT );

                ui->spinBox_IP3->setStyleSheet( COLOR_NORMAL_FONT );
                ui->label_IP3->setStyleSheet( COLOR_NORMAL_FONT );

                ui->spinBox_IP4->setStyleSheet( COLOR_NORMAL_FONT );
            }
            break;

            default:
            break;
        }
    }
}

void CtrlInputChar::setParam(CtrlInputParam* pCtrlParam)
{
    if (pCtrlParam == NULL)
    {
        TRACELOG2( "CtrlInputChar::setParam pCtrlParam == NULL" );
        return;
    }
    TRACEDEBUG( "CtrlInputChar::setParam() ipt<%d>", pCtrlParam->ipt );
    m_ctrlParam = *pCtrlParam;
    m_nPrefixLen = m_ctrlParam.strPrefix.length();
    m_nSuffixLen = m_ctrlParam.strSuffix.length();

    setCtrlVisible( false );
    switch (m_ctrlParam.ipt)
    {
        case IPT_LABEL_READONLY:
        {
            ui->label_old->setText( m_ctrlParam.strInit );
            ui->label_old->setGeometry(
                        3,
                        0,
                        PosTableWidget::widthNotScrool,
                        TABLEWDG_ROW_HEIGHT
                        );
            ui->label_old->setVisible( true );
        }
        break;

        case IPT_SBT_CHAR:
        {
            TRACEDEBUG( "CtrlInputChar::setParam IPT_SBT_CHAR" );
            ui->label_old->setText( m_ctrlParam.strInit );
            ui->label_old->setVisible( true );
            ui->label_old->setGeometry(
                        3,
                        0,
                        PosTableWidget::widthNotScrool,
                        TABLEWDG_ROW_HEIGHT
                        );
            ui->label_new->clear();
            ui->label_new->setGeometry(0, 0, 0, TABLEWDG_ROW_HEIGHT);

            ui->spinBox_char->setVisible( false );
            ui->spinBox_char->setType( &m_ctrlParam );
        }
        break;

        case IPT_SBT_CHAR_PW:
        {
            TRACEDEBUG( "CtrlInputChar::setParam IPT_SBT_CHAR_PW" );
            ui->label_new->clear();
            ui->label_new->setVisible( false );

            ui->label_pw->clear();
            ui->label_pw->setGeometry(0, 0, 0, TABLEWDG_ROW_HEIGHT);
            ui->label_pw->setVisible( true );

            ui->spinBox_char->setVisible( false );
            ui->spinBox_char->setType( &m_ctrlParam );
        }
        break;

        case IPT_SBT_INT:
        {
            TRACEDEBUG( "CtrlInputChar::setParam IPT_SBT_INT" );
            ui->spinBox_single->setVisible( true );
            ui->spinBox_single->setType( &m_ctrlParam );
            ui->spinBox_single->setGeometry(0, 0,
                TABLEWDG_ROW_WIDTH, TABLEWDG_ROW_HEIGHT );
        }
        break;

        case IPT_SBT_ULONG:
        {
            TRACEDEBUG( "CtrlInputChar::setParam IPT_SBT_ULONG" );
            ui->spinBox_single->setVisible( true );
            ui->spinBox_single->setType( &m_ctrlParam );
            ui->spinBox_single->setGeometry(0, 0,
                TABLEWDG_ROW_WIDTH, TABLEWDG_ROW_HEIGHT );
        }
        break;

        case IPT_SBT_CUSTOM:
        {
            TRACEDEBUG( "CtrlInputChar::setParam IPT_SBT_CUSTOM" );
            ui->spinBox_enum->setVisible( true );
            ui->spinBox_enum->setType( &m_ctrlParam );
            ui->spinBox_enum->setGeometry(0, 0,
                TABLEWDG_ROW_WIDTH, TABLEWDG_ROW_HEIGHT );
        }
        break;

            //DoubleSpinBox
        case IPT_DSBT_SINGLE:
        {
            TRACEDEBUG( "CtrlInputChar::setParam IPT_DSBT_SINGLE dbInit<%f>", m_ctrlParam.dbInit );
            ui->dSpinBox_single->setVisible( true );
            ui->dSpinBox_single->setType( &m_ctrlParam );
            ui->dSpinBox_single->setGeometry(0, 0,
                TABLEWDG_ROW_WIDTH, TABLEWDG_ROW_HEIGHT );
        }
        break;

        case IPT_DATE:
        {
            ui->dateEdit->setVisible( true );

            QDateTime dateTime = QDateTime::fromTime_t( m_ctrlParam.nInit );

    //        Added by alpha wang
            ui->dateEdit->setDisplayFormat (g_szHomePageDateFmt[g_cfgParam->ms_initParam.timeFormat]);
    //        Added end
            ui->dateEdit->setDate( dateTime.date());
            TRACEDEBUG( "CtrlInputChar::setParam IPT_DATE date<%s>",
                        dateTime.date().toString( "yyyy-MM-dd" ).toUtf8().constData() );
        }
        break;

        case IPT_TIME:
        {
            ui->timeEdit->setVisible( true );
            QDateTime dateTime = QDateTime::fromTime_t( m_ctrlParam.nInit );
            ui->timeEdit->setTime( dateTime.time() );
            TRACEDEBUG( "CtrlInputChar::setParam IPT_TIME time<%s>",
                        dateTime.time().toString( "HH:mm:ss" ).toUtf8().constData() );
        }
        break;

        case IPT_IP:
        {
            TRACEDEBUG( "CtrlInputChar::setParam IPT_IP" );

            ui->spinBox_IP1->setVisible( true );
            ui->label_IP1->setText( "." );
            ui->label_IP1->setVisible( true );

            ui->spinBox_IP2->setVisible( true );
            ui->label_IP2->setText( "." );
            ui->label_IP2->setVisible( true );

            ui->spinBox_IP3->setVisible( true );
            ui->label_IP3->setText( "." );
            ui->label_IP3->setVisible( true );

            ui->spinBox_IP4->setVisible( true );

            ui->spinBox_IP1->setType( &m_ctrlParam );
            ui->spinBox_IP2->setType( &m_ctrlParam );
            ui->spinBox_IP3->setType( &m_ctrlParam );
            ui->spinBox_IP4->setType( &m_ctrlParam );

            BYTE bysIp[4];
            DEPART_IP( bysIp, m_ctrlParam.ulInit );
            ui->spinBox_IP1->setValue( bysIp[3] );
            ui->spinBox_IP2->setValue( bysIp[2] );
            ui->spinBox_IP3->setValue( bysIp[1] );
            ui->spinBox_IP4->setValue( bysIp[0] );
            TRACELOG1( "CtrlInputChar::setParam IP <%d %d %d %d>",
                       bysIp[3], bysIp[2], bysIp[1], bysIp[0] );
        }
        break;

        case IPT_IPV6_1:
        case IPT_IPV6_2:
        case IPT_IPV6_1_V:
        case IPT_IPV6_2_V:
        case IPT_IPV6_3_V:
        {
            TRACEDEBUG( "CtrlInputChar::setParam IPT_IPV6" );

            ui->spinBox_IP1->setVisible( true );
            ui->label_IP1->setText( ":" );
            ui->label_IP1->setVisible( true );

            ui->spinBox_IP2->setVisible( true );
            ui->label_IP2->setText( ":" );
            ui->label_IP2->setVisible( true );

            ui->spinBox_IP3->setVisible( true );
            ui->label_IP3->setText( ":" );
            ui->label_IP3->setVisible( true );

            ui->spinBox_IP4->setVisible( true );
            if (m_ctrlParam.ipt==IPT_IPV6_1_V ||
                    m_ctrlParam.ipt==IPT_IPV6_2_V ||
                    m_ctrlParam.ipt==IPT_IPV6_3_V)
            {
                ui->label_IP3->setVisible( false );
                ui->spinBox_IP4->setVisible( false );
                if (m_ctrlParam.ipt==IPT_IPV6_3_V)
                {
                    ui->label_IP2->setVisible( false );
                    ui->spinBox_IP3->setVisible( false );
                }
            }


            ui->spinBox_IP1->setType( &m_ctrlParam );
            ui->spinBox_IP2->setType( &m_ctrlParam );

            ui->spinBox_IP1->setValue( m_ctrlParam.usInit[0] );
            ui->spinBox_IP2->setValue( m_ctrlParam.usInit[1] );
            if (m_ctrlParam.ipt==IPT_IPV6_1_V ||
                    m_ctrlParam.ipt==IPT_IPV6_2_V ||
                    m_ctrlParam.ipt==IPT_IPV6_1 ||
                    m_ctrlParam.ipt==IPT_IPV6_2)
            {
                ui->spinBox_IP3->setType( &m_ctrlParam );
                ui->spinBox_IP3->setValue( m_ctrlParam.usInit[2] );

                if (m_ctrlParam.ipt==IPT_IPV6_1 ||
                        m_ctrlParam.ipt==IPT_IPV6_2)
                {
                    ui->spinBox_IP4->setType( &m_ctrlParam );
                    ui->spinBox_IP4->setValue( m_ctrlParam.usInit[3] );
                }
            }
            TRACELOG1( "CtrlInputChar::setParam IPV6 <%X %X %X %X>",
                       m_ctrlParam.usInit[0], m_ctrlParam.usInit[1],
                       m_ctrlParam.usInit[2], m_ctrlParam.usInit[3] );
        }
        break;

        default:
            //TRACEDEBUG( "CtrlInputChar::setParam default" );
            break;
    }

    if (m_ctrlParam.ipt>=IPT_IP && m_ctrlParam.ipt<=IPT_IPV6_3_V)
    {
        // IP
        int widthSpinBox = PosBase::iSpinBox_IP_Width;
        int widthLabel   = PosBase::iSpinBox_Dot_Width;
        switch (m_ctrlParam.ipt)
        {
            case IPT_IP:
            {
                ui->label_IP1->setText( "." );
                ui->label_IP2->setText( "." );
                ui->label_IP3->setText( "." );
            }
            break;

            case IPT_IPV6_1:
            case IPT_IPV6_2:
            {
                widthSpinBox = PosBase::iSpinBox_IP_Width_V6;
                ui->label_IP1->setText( ":" );
                ui->label_IP2->setText( ":" );
                ui->label_IP3->setText( ":" );
            }
            break;

            case IPT_IPV6_1_V:
            case IPT_IPV6_2_V:
            case IPT_IPV6_3_V:
            {
                widthSpinBox = PosBase::iSpinBox_IP_Width_V6;
                ui->label_IP1->setText( ":" );
                ui->label_IP2->setText( ":" );
            }
            break;

            default:
                break;
        }

        int idxObj = 0;
        ui->spinBox_IP1->setGeometry( idxObj*(widthSpinBox+widthLabel), 0,
                                      widthSpinBox, TABLEWDG_ROW_HEIGHT );
        ui->label_IP1->setGeometry( idxObj*(widthSpinBox+widthLabel)+widthSpinBox, 0,
                                    widthLabel, TABLEWDG_ROW_HEIGHT );

        ++idxObj;
        ui->spinBox_IP2->setGeometry( idxObj*(widthSpinBox+widthLabel), 0,
                                      widthSpinBox, TABLEWDG_ROW_HEIGHT );
        ui->label_IP2->setGeometry( idxObj*(widthSpinBox+widthLabel)+widthSpinBox, 0,
                                    widthLabel, TABLEWDG_ROW_HEIGHT );

        ++idxObj;
        ui->spinBox_IP3->setGeometry( idxObj*(widthSpinBox+widthLabel), 0,
                                      widthSpinBox, TABLEWDG_ROW_HEIGHT );
        ui->label_IP3->setGeometry( idxObj*(widthSpinBox+widthLabel)+widthSpinBox, 0,
                                    widthLabel, TABLEWDG_ROW_HEIGHT );

        ++idxObj;
        ui->spinBox_IP4->setGeometry( idxObj*(widthSpinBox+widthLabel), 0,
                                      widthSpinBox, TABLEWDG_ROW_HEIGHT );
        TRACEDEBUG( "CtrlInputChar::setParam widthSpinBox<%d>", widthSpinBox );
    }
}

CtrlInputParam* CtrlInputChar::getParam()
{
    return &m_ctrlParam;
}

CtrlInputRetval CtrlInputChar::getValue()
{
    TRACEDEBUG( ">>>CtrlInputChar::getValue()" );
    switch (m_ctrlParam.ipt)
    {
        case IPT_SBT_CHAR:
        case IPT_SBT_CHAR_PW:
        {
            m_retVal.strVal = ui->label_new->text();
        }
        break;

        case IPT_SBT_INT:
        case IPT_SBT_ULONG:
        {
            //TRACELOG1( "CtrlInputChar::getValue() IPT_SBT_CUSTOM idx<%d>", ui->spinBox_single->value() );
            m_retVal.idx = ui->spinBox_single->value();

            m_retVal.strVal = ui->spinBox_single->text();
            int nValLen = m_retVal.strVal.length();
            m_retVal.strVal = m_retVal.strVal.mid(m_nPrefixLen,
                           nValLen-m_nPrefixLen-m_nSuffixLen);
        }
        break;

        case IPT_SBT_CUSTOM:
        {
            m_retVal.idx = ui->spinBox_enum->value();
            m_retVal.strVal = ui->spinBox_enum->text();

            TRACEDEBUG( "CtrlInputChar::getValue() IPT_SBT_CUSTOM idx<%d> str<%s> <%d-%d>",
                        m_retVal.idx,
                        m_retVal.strVal.toUtf8().constData(),
                        ui->spinBox_enum->minimum(),
                        ui->spinBox_enum->maximum()
                        );

            int nValLen = m_retVal.strVal.length();
            m_retVal.strVal = m_retVal.strVal.mid(m_nPrefixLen,
                           nValLen-m_nPrefixLen-m_nSuffixLen);
        }
        break;

        //DoubleSpinBox
        case IPT_DSBT_SINGLE:
        {
            m_retVal.strVal = ui->dSpinBox_single->text();
            int nValLen = m_retVal.strVal.length();
            m_retVal.strVal = m_retVal.strVal.mid(m_nPrefixLen,
                           nValLen-m_nPrefixLen-m_nSuffixLen);
        }
        break;

        case IPT_DATE:
        {
            m_dateTime = QDateTime::currentDateTime();
            m_dateTime.setDate( ui->dateEdit->date() );
            m_retVal.strVal = QString::number( m_dateTime.toTime_t() );
            TRACELOG1( "CtrlInputChar::getValue date<%d-%d-%d> time<%d:%d:%d>",
                       m_dateTime.date().year(),
                       m_dateTime.date().month(),
                       m_dateTime.date().day(),
                       m_dateTime.time().hour(),
                       m_dateTime.time().minute(),
                       m_dateTime.time().second()
                       );

            TRACEDEBUG( "CtrlInputChar::getValue displayFormat<%s> <%d>",
                        ui->dateEdit->displayFormat().toUtf8().constData(),
                        ui->dateEdit->currentSectionIndex() );
        }
        break;

        case IPT_TIME:
        {
            m_dateTime = QDateTime::currentDateTime();
            m_dateTime.setTime( ui->timeEdit->time() );
            m_retVal.strVal = QString::number( m_dateTime.toTime_t() );
            TRACELOG1( "CtrlInputChar::getValue date<%d-%d-%d> time<%d:%d:%d>",
                       m_dateTime.date().year(),
                       m_dateTime.date().month(),
                       m_dateTime.date().day(),
                       m_dateTime.time().hour(),
                       m_dateTime.time().minute(),
                       m_dateTime.time().second()
                       );
        }
        break;

        case IPT_IP:
        {
            BYTE bysIp[4];
            bysIp[3] = ui->spinBox_IP1->text().toInt();
            bysIp[2] = ui->spinBox_IP2->text().toInt();
            bysIp[1] = ui->spinBox_IP3->text().toInt();
            bysIp[0] = ui->spinBox_IP4->text().toInt();
            ULONG IP  = MAKE_IP( bysIp );
            m_retVal.strVal = QString::number( IP );
            TRACELOG1( "CtrlInputRetval CtrlInputChar::getValue() <%d %d %d %d> <%u> %s",
                       bysIp[3], bysIp[2],
                       bysIp[1], bysIp[0],
                       IP,
                       m_retVal.strVal.toUtf8().constData()
                       );
        }
        break;

        case IPT_IPV6_1:
        case IPT_IPV6_2:
        case IPT_IPV6_1_V:
        case IPT_IPV6_2_V:
        case IPT_IPV6_3_V:
        {
            m_retVal.strVal = (
                        ui->spinBox_IP1->text()+":"+
                        ui->spinBox_IP2->text()
                        );
            if (m_ctrlParam.ipt!=IPT_IPV6_3_V)
            {
                m_retVal.strVal += ":"+
                        ui->spinBox_IP3->text()+":";
                if (m_ctrlParam.ipt==IPT_IPV6_1 ||
                        m_ctrlParam.ipt==IPT_IPV6_2)
                {
                    m_retVal.strVal += ui->spinBox_IP4->text();
                    if (m_ctrlParam.ipt==IPT_IPV6_1)
                    {
                        m_retVal.strVal += ":";
                    }
                }
            }
            TRACELOG1( "CtrlInputRetval CtrlInputChar::getValue() IPT_IPV6<%s>",
                       m_retVal.strVal.toUtf8().constData()
                       );
        }
        break;

        default:
        {
            m_retVal.strVal = "";
        }
        break;
    }

    TRACELOG1( "CtrlInputChar::getValue() idx<%d> str<%s>",
               m_retVal.idx,
               m_retVal.strVal.toUtf8().constData() );
    return m_retVal;
}

// for IPV6
void CtrlInputChar::setIPV6(QString& strIPV6)
{
    m_strIPV6 = strIPV6;
}

QString CtrlInputChar::getIPV6()
{
    return m_strIPV6;
}

void CtrlInputChar::on_spinBox_char_editingFinished()
{
    if (m_key==Qt::Key_Escape)
    {
        TRACELOG1( "CtrlInputChar::on_spinBox_char_editingFinished m_key==Qt::Key_Escape>>>" );
        return;
    }

    QString strChar = ui->spinBox_char->text();
    QString strVal, strSym;
    strVal = ui->label_new->text();
    strVal.append( strChar.trimmed() );
    ui->label_new->setText( strVal );

    switch ( m_ctrlParam.ipt )
    {
    case IPT_SBT_CHAR:
    {
        int nStrValWidth = fmLargeN->width( strVal );
        ui->label_new->setGeometry(
                    0,
                    0,
                    nStrValWidth,
                    TABLEWDG_ROW_HEIGHT
                    );
        ui->spinBox_char->setGeometry(
                    nStrValWidth,
                    0,
                    CHAR_WIDTH_SPINBOX,
                    TABLEWDG_ROW_HEIGHT
                    );
    }
        break;

    case IPT_SBT_CHAR_PW:
    {
        strSym = ui->label_pw->text();
        strSym.append( "*" );
        int nSymWidth = fmLargeN->width(strSym);
        ui->label_pw->setText( strSym );

        ui->label_pw->setGeometry(
                    0,
                    0,
                    nSymWidth,
                    TABLEWDG_ROW_HEIGHT
                    );
        ui->spinBox_char->setGeometry(
                    nSymWidth,
                    0,
                    CHAR_WIDTH_SPINBOX,
                    TABLEWDG_ROW_HEIGHT
                    );
    }
        break;

    default:
        break;
    }
}

void CtrlInputChar::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        //changed by Frank Wu,20140110
        //ui->retranslateUi(this);
        QString sTmp = ui->label_old->text();
        ui->retranslateUi(this);

        if(m_bIsMenuNode)
        {
            ui->label_old->setText(sTmp);
        }

    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void CtrlInputChar::keyPressEvent(QKeyEvent* keyEvent)
{
    TRACEDEBUG( "CtrlInputChar::keyPressEvent" );
    m_key = keyEvent->key();
    switch ( m_key )
    {
    case Qt::Key_Escape:
    {
        switch (m_ctrlParam.ipt)
        {
        case IPT_SBT_CHAR:
        case IPT_SBT_CHAR_PW:
            TRACELOG1( "CtrlInputChar::keyPressEvent Key_Escape" );
            ui->spinBox_char->setValue( 62 );
            ui->spinBox_char->setVisible( false );
            break;

        default:
            break;
        }
        emit escapeMe(m_ctrlParam.ipt);
    }
        // 不让table处理，防止退出。
        return;

    case Qt::Key_Return:
    case Qt::Key_Enter:
        switch (m_ctrlParam.ipt)
        {
        case IPT_SBT_CHAR:
            if (ui->label_new->text().length() >= MAX_LEN_SITENAME)
            {
                ui->spinBox_char->setRange( 62, 62 );
                ui->spinBox_char->setValue( 62 );
                if (++m_nEnter>2)
                {
                    emit escapeMe(IPT_SUBMIT);
                    ui->label_pw->clear();
                }
                return;
            }
            else
            {
                if (ui->spinBox_char->text().trimmed().length() > 0)
                {
                    TRACEDEBUG( "CtrlInputChar::keyPressEvent %s", ui->spinBox_char->text().toUtf8().constData() );
                    // don't summit if have data
                    m_nEnter = 0;
                    ui->spinBox_char->setValue( 62 );
                }
            }
            break;

        case IPT_SBT_CHAR_PW:
            if (ui->label_pw->text().length() >= MAX_LEN_PASSWORD)
            {
                ui->spinBox_char->setRange( 62, 62 );
                ui->spinBox_char->setValue( 62 );
                if (++m_nEnter>2)
                {
                    emit escapeMe(IPT_SUBMIT);
                    ui->label_pw->clear();
                }
                return;
            }
            else
            {
                if (ui->spinBox_char->text().trimmed().length() > 0)
                {
                    TRACEDEBUG( "CtrlInputChar::keyPressEvent %s", ui->spinBox_char->text().toUtf8().constData() );
                    // don't summit if have data
                    m_nEnter = 0;
                    ui->spinBox_char->setValue( 62 );
                }
            }
            break;

        case IPT_DATE:
        {
            int nCurIdx = ui->dateEdit->currentSectionIndex();
            if (nCurIdx < 2)
            {
                ++nCurIdx;
                m_nEnter = 0;
                ui->dateEdit->setCurrentSectionIndex(nCurIdx);
                /* wufang,20130921,modify:1/3, set editbox hight light after press key enter */
                CTRL_INPUT_CHAR_SET_DATETIMEEDIT_FOCUS(ui->dateEdit);
            }
        }
            break;

        case IPT_TIME:
        {
            int nCurIdx = ui->timeEdit->currentSectionIndex();
            if (nCurIdx<2)
            {
                ++nCurIdx;
                m_nEnter = 0;
                ui->timeEdit->setCurrentSectionIndex(nCurIdx);
                /* wufang,20130921,modify:2/3, set editbox hight light after press key enter */
                CTRL_INPUT_CHAR_SET_DATETIMEEDIT_FOCUS(ui->timeEdit);
            }
        }
            break;

        case IPT_IP:
        case IPT_IPV6_1:
        case IPT_IPV6_2:
        case IPT_IPV6_1_V:
        case IPT_IPV6_2_V:
        case IPT_IPV6_3_V:
        {
            //TRACEDEBUG( "CtrlInputChar::keyPressEvent Enter IPT_IP" );
            m_nIPCtrlNow++;
            if (IPT_IPV6_1_V==m_ctrlParam.ipt ||
                    IPT_IPV6_2_V==m_ctrlParam.ipt)
            {
                if (m_nIPCtrlNow > 3)
                {
                    break;
                }
            }
            else if (IPT_IPV6_3_V==m_ctrlParam.ipt)
            {
                if (m_nIPCtrlNow > 2)
                {
                    break;
                }
            }

            switch (m_nIPCtrlNow)
            {
            case 2:
                //TRACEDEBUG( "CtrlInputChar::keyPressEvent Enter 2" );
                m_nEnter = 0;
                ui->spinBox_IP2->Enter();
                break;
            case 3:
                //TRACEDEBUG( "CtrlInputChar::keyPressEvent Enter 3" );
                m_nEnter = 0;
                ui->spinBox_IP3->Enter();
                break;
            case 4:
                //TRACEDEBUG( "CtrlInputChar::keyPressEvent Enter 4" );
                m_nEnter = 0;
                ui->spinBox_IP4->Enter();
                break;
            default:
                TRACEDEBUG( "CtrlInputChar::keyPressEvent Enter default" );
                break;
            }
        }
            break;

        default:
            break;
        }
        if (++m_nEnter>1)
        {
            TRACEDEBUG( "CtrlInputChar::keyPressEvent Enter>=2 emit escapeMe(IPT_SUBMIT)" );
            emit escapeMe(IPT_SUBMIT);
            ui->label_pw->clear();
        }
        return;

    case Qt::Key_Up:
    case Qt::Key_Down:
        TRACELOG1( "CtrlInputChar::keyPressEvent<%0x> m_nEnter<%d>",
                   m_key, m_nEnter );
        return;

    default:
        break;
    }

    return QWidget::keyPressEvent(keyEvent);
}

void CtrlInputChar::setCtrlVisible(bool bVisible)
{
    ui->label_old->setVisible( bVisible );
    ui->label_new->setVisible( bVisible );
    ui->label_pw->setVisible( bVisible );

    ui->spinBox_char->setVisible( bVisible );
    ui->spinBox_single->setVisible( bVisible );
    ui->spinBox_enum->setVisible( bVisible );
    ui->dSpinBox_single->setVisible( bVisible );

    ui->dateEdit->setVisible( bVisible );
    ui->timeEdit->setVisible( bVisible );

    ui->spinBox_IP1->setVisible( bVisible );
    ui->label_IP1->setVisible( bVisible );
    ui->spinBox_IP2->setVisible( bVisible );
    ui->label_IP2->setVisible( bVisible );
    ui->spinBox_IP3->setVisible( bVisible );
    ui->label_IP3->setVisible( bVisible );
    ui->spinBox_IP4->setVisible( bVisible );
}



void CtrlInputChar::setMenuNode(MenuNode_t *pMenuNode)
{
    if(pMenuNode != NULL)
    {
        memcpy(&m_stMenuNode, pMenuNode, sizeof(m_stMenuNode));
        m_bIsMenuNode = true;
    }
}

void CtrlInputChar::getMenuNode(MenuNode_t *pMenuNode)
{
    if(pMenuNode != NULL)
    {
        memcpy(pMenuNode, &m_stMenuNode, sizeof(m_stMenuNode));
    }
}




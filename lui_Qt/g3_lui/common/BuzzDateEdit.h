#ifndef BUZZDATEEDIT_H
#define BUZZDATEEDIT_H

#include <QDateEdit>
#include <QKeyEvent>
#include <QSpinBox>
#include <QLineEdit>

class BuzzDateEdit : public QDateEdit
{
    Q_OBJECT
public:
    explicit BuzzDateEdit(QWidget *parent = 0);
    ~BuzzDateEdit();

public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();

protected:
    virtual void keyPressEvent(QKeyEvent *keyEvent);
};

#endif // BUZZDATEEDIT_H

/******************************************************************************
文件名：    equipbutton.cpp
功能：      设备图标按钮的类，在设备组界面，根据点击的按钮的iBtnSeq号，来路由进入哪个设备页面
作者：      刘金煌
创建日期：   2013年08月15日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "equipbutton.h"

EquipButton::EquipButton(int iSeq)
{
    iBtnSeq = iSeq;
    connect(this,SIGNAL(clicked()),this,SLOT(EquipBtnClicked()));
}

void EquipButton::EquipBtnClicked()
{
    //qDebug("EquipButton::EquipBtnClicked iBtnSeq = %d\n", iBtnSeq);
    emit equipBtnClicked(iBtnSeq);
}

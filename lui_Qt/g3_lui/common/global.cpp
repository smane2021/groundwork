#include "global.h"
#include "pubInclude.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include <qglobal.h>
#include <QObject>
#include <QString>
#include <QCoreApplication>
#include <sys/time.h>

bool         g_bSendCmd = true;
QTranslator  g_translator;
//Frank Wu,20131220
//void*        g_Wdg2P9Cfg;
void *         g_WdgFCfgGroup;
// global var define
QTime        g_timeLoginConfig;
QTime        g_timeElapsedNoKeyPress;
QTime        g_timeElapsedKeyPress;
bool         g_bEnterFirstly = true;
bool         g_bDetectWork   = true;
bool         g_bLogin = false;
int          g_nInAlarmType       = IN_ALARM_TYPE_KEYPRESS;
int          g_nLastWTNokey       = -1;
int          g_nLastWTScreenSaver = -1;
int          g_nEnterPageType;

QMap<enum LANGUAGE_TYPE, const char*> g_mapLangStr;
QMap<QString,enum LANGUAGE_TYPE> g_mapStrLangType;
QMap<QString,QString> g_mapLangSymbol;

//Time Display Format
// 0-EMEA(16/11/2004)��
// 1-NA(11/16/2004)��
// 2-CN��2004/11/16��
const char*  g_szTimeFormat[3] =
{
    "dd/MM/yyyy",
    "MM/dd/yyyy",
    "yyyy/MM/dd"
};
const char*  g_szHomePageDateFmt[3] =
{
    "dd/MM/yyyy",
    "MM/dd/yyyy",
    "yyyy/MM/dd"
};

const char *g_szHomePageDateFmtWithoutYear[3] =
{
  "dd/MM",
  "MM/dd",
  "MM/dd"
};

char         g_dataBuff[SIZEOF_MAPFILE];

void initGlobalVar()
{
    g_mapLangStr[LANG_English]  = "en";
//    g_mapLangStr[LANG_Chinese]  = "zh";
    g_mapLangStr[LANG_French]   = "fr";
//    g_mapLangStr[LANG_German]   = "de";
//    g_mapLangStr[LANG_Italian]  = "it";
//    g_mapLangStr[LANG_Russia]   = "ru";
    g_mapLangStr[LANG_Spanish]  = "es";
//    g_mapLangStr[LANG_TraditionalChinese] = "tw";
//    g_mapLangStr[LANG_Portugal] = "pt";
//    g_mapLangStr[LANG_Turkish]  = "tr";

    g_mapLangSymbol.insert("en","English");
    g_mapLangSymbol.insert("zh", "Chinese");
    g_mapLangSymbol.insert("fr","French");
    g_mapLangSymbol.insert("de", "German");
    g_mapLangSymbol.insert("it","Italian");
    g_mapLangSymbol.insert("ru","Russia");
    g_mapLangSymbol.insert("es","Spanish");
    g_mapLangSymbol.insert("tw","TraditionalChinese");
    g_mapLangSymbol.insert("pt","Portugal");
    g_mapLangSymbol.insert("tr","Turkish");

    g_mapStrLangType["en"] = LANG_English;
//    g_mapStrLangType["zh"] = LANG_Chinese;
    g_mapStrLangType["fr"] = LANG_French;
//    g_mapStrLangType["de"] = LANG_German;
//    g_mapStrLangType["it"] = LANG_Italian;
//    g_mapStrLangType["ru"] = LANG_Russia;
    g_mapStrLangType["es"] = LANG_Spanish;
//    g_mapStrLangType["tw"] = LANG_TraditionalChinese;
//    g_mapStrLangType["tr"] = LANG_Turkish;
//    g_mapStrLangType["pt"] = LANG_Portugal;
}

void mainSleep(int mSec)
{
    QTime t;
    t.start();
    while(t.elapsed() < mSec)
    {
        QCoreApplication::processEvents(
                    QEventLoop::ExcludeUserInputEvents, 20);
    }
}



#ifdef Q_OS_WIN32
static char buffer[1024];
void printLogDeb(const char *format, ...)
{
    time_t now;
    struct tm *timenow;
    time( &now );
    timenow = localtime( &now );
    sprintf(buffer, "%04d-%02d-%02d %02d:%02d:%02d ",
            timenow->tm_year+1900, timenow->tm_mon, timenow->tm_mday,
            timenow->tm_hour, timenow->tm_min, timenow->tm_sec
            );


    va_list va;
    va_start(va, format);
    vsprintf(buffer+20, format, va);
    va_end(va);
    printf("%s \n", buffer);
}

void printLog1(const char *format, ...)
{
    time_t now;
    struct tm *timenow;
    time( &now );
    timenow = localtime( &now );
    sprintf(buffer, "%04d-%02d-%02d %02d:%02d:%02d ",
            timenow->tm_year+1900, timenow->tm_mon, timenow->tm_mday,
            timenow->tm_hour, timenow->tm_min, timenow->tm_sec
            );


    va_list va;
    va_start(va, format);
    vsprintf(buffer+20, format, va);
    va_end(va);
    printf("%s \n", buffer);
}

void printLog2(const char *file, int line, const char *format, ...)
{
    va_list va;
    va_start(va, format);
    vsprintf(buffer, format, va);
    va_end(va);
    printf("file[%s] line[%d] %s \n", file, line, buffer);
}

int sys_open(enum DEV_OP op)
{
    TRACELOG1( "Q_OS_WIN32" );
    return 0;
}

int sys_close(void)
{
    return 0;
}

long  sys_getClickTime()
{
    return 0;
}

int sys_setBuzz(enum SET_BUZZ buzz, int iType)
{
    return 0;
}

int sys_getSoundType()
{
    return 0;
}

int sys_getBuzz()
{
    return 0;
}

int sys_setLCD(enum SET_LCD lcd)
{
    return 0;
}

int sys_getLCD()
{
    return 0;
}

int sys_setLED(enum SET_LED led)
{
    return 0;
}

int sys_getLED()
{
    return 0;
}

int data_open(void)
{
    return 0;
}

int data_close(void)
{
    return 0;
}

int data_sendCmd(CmdItem* pCmd)
{
    return 0;
}

int data_recvData(void** ppData)
{
    return 1;
}

int data_getDataSync(CmdItem* pCmd, void** ppData, int nSecTimeOut)
{
    QTime t;
    t.start();
    while ( t.elapsed()<nSecTimeOut*1000 );
        //QCoreApplication::processEvents();

    return 0;
}

int data_hasAlarm()
{
    return 0;
}

int data_sendHeartBeat()
{
    return 0;
}

#endif

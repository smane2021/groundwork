#include "buzztoolbutton.h"

#include <QTimer>

#include "common/global.h"
#include "common/pubInclude.h"
#include "common/uidefine.h"

BuzzToolButton::BuzzToolButton(QWidget *parent) :
    QToolButton(parent)
{

    //connect(this, SIGNAL(pressed()), this, SLOT(Press()));
    //connect(this, SIGNAL(released()), this, SLOT(Release()));
    connect(this, SIGNAL(clicked()), this, SLOT(Press()));
//    this->setFocusPolicy(Qt::NoFocus);
//    this->setStyleSheet("QPushButton:focused { padding: -120; }");
//    setStyleSheet( "border-style:flat;" );
//    setStyleSheet(
//            "QToolButton"
//            BTN_STYLE_UP

//            "QToolButton:hover"
//            BTN_STYLE_UP

//            "QToolButton:hover:pressed"
//            BTN_STYLE_DOWN

//            "QToolButton:hover:!pressed"
//            BTN_STYLE_UP
//            );

}


void BuzzToolButton::Press()
{
    //Buzz::SetBeepType(BUTTON_BEEP);

    //蜂鸣器100毫秒后关闭掉
    //Buzz::Moo();
    //QTimer::singleShot(BEEP_DURATION, this, SLOT(StopBuzz()));

    sys_setBuzz( BUZZ_BEEP );
}

void BuzzToolButton::Release()
{
    sys_setBuzz( BUZZ_QUIET );
}

void BuzzToolButton::StopBuzz()
{
    if(sys_getBuzz() && sys_getSoundType() == BUTTON_BEEP)
    {
        sys_setBuzz( BUZZ_QUIET );
    }
}

//void BuzzToolButton::mousePressEvent(QMouseEvent* e)
//{
////qDebug() << "mousePressEvent";
//// QIcon icon(":/Icon_Home.png");
//// setIcon(icon); //设置图标
//// setIconSize(QSize(100, 100)); //设置图标大小
//// setFixedSize(50, 50); //设置按钮大小
////setAutoRaise(true); //滤除图标的边框
//setStyleSheet( BTN_STYLE_DOWN );
//QToolButton::mousePressEvent(e);
//}


//void BuzzToolButton::mouseReleaseEvent(QMouseEvent* e)
//{
////qDebug() << "mouseReleaseEvent";
////QIcon icon(":/Icon_Home.png");
////setIcon(icon); //设置图标
//// setIconSize(QSize(100, 100)); //设置图标大小
//// setFixedSize(100, 100); //设置按钮大小
////setAutoRaise(true); //滤除图标的边框
//setStyleSheet( BTN_STYLE_UP );
////emit clicked();
//QToolButton::mouseReleaseEvent(e);
//}

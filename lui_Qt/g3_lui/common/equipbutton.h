/******************************************************************************
文件名：    equipbutton.h
功能：      设备图标按钮的类，在设备组界面，根据点击的按钮的iBtnSeq号，来路由进入哪个设备页面
作者：      刘金煌
创建日期：   2013年08月15日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef EQUIPBUTTON_H
#define EQUIPBUTTON_H

#include <QToolButton>
#include "common/buzztoolbutton.h"

class EquipButton : public BuzzToolButton
{
    Q_OBJECT
public:
    EquipButton(int iSeq);

private:
    int iBtnSeq;

signals:
    void equipBtnClicked(int);

public slots:
    void EquipBtnClicked();
};

#endif // EQUIPBUTTON_H

/******************************************************************************
文件名：    InputCtrlDef.h
功能：      输入数据控件 公共定义头文件
作者：      刘金煌
创建日期：   2013年05月15日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef INPUTCTRLDEF_H
#define INPUTCTRLDEF_H

#include "dataDef.h"
#include <QDateTime>
// 输入控件
enum INPUT_TYPE
{
    IPT_LABEL_READONLY, // readonly item
    IPT_SBT_CHAR,    // spinbox单个字符
    IPT_SBT_CHAR_PW, // spinbox单个字符 密码
    IPT_SBT_INT,     // spinbox整数
    IPT_SBT_ULONG,     // unsigned long
    IPT_SBT_CUSTOM,    // spinbox 自定义选择框 ENUM类型

    IPT_SBT_ABLE,

    IPT_DSBT_SINGLE, // doublespinbox 单个double
    IPT_DATE,        // 日期
    IPT_TIME,        // 时间
    IPT_IP,          // IP
    IPT_IPV6_1,      // IPV6
    IPT_IPV6_2,      // IPV6
    IPT_IPV6_1_V,    // IPV6 垂直屏
    IPT_IPV6_2_V,
    IPT_IPV6_3_V,
    IPT_SUBMIT,      // 提交数据
    IPT_MAX = -1
};

typedef struct _CtrlInputParam
{
    _CtrlInputParam()
    {
        ipt   = IPT_MAX;

        nInit = 0;
        ulInit= 0;
        nStep = 1;
        nMin  = 0;
        nMax  = 99;

        dbInit    = 1.0;
        dbStep    = 1.0;
        dbMin     = 0.0;
        dbMax     = 99.0;
        nDecimals = 1;

        strPrefix = "";
        strSuffix = "";
    }
    enum INPUT_TYPE ipt;

    int      nStep;
    int      nMin;
    int      nMax;
    long     nInit; //int enum 初始值
    unsigned long ulInit;

    QString  strInit;

    double   dbStep;
    double   dbMin;
    double   dbMax;
    int      nDecimals; // decimal digits
    double   dbInit;

    QString  strPrefix;
    QString  strSuffix;
    QStringList  strListEnumText;
    unsigned short usInit[4];
} CtrlInputParam;

typedef struct _CtrlInputRetval
{
    _CtrlInputRetval()
    {
        strVal.clear();
        idx = -1;
    }
    QString strVal;
    int     idx;
} CtrlInputRetval;

#define SET_STYLE_SPINBOX \
{ \
    setStyleSheet("background-color:rgba(0,0,0, 0);"); \
    setButtonSymbols( QAbstractSpinBox::NoButtons ); \
}

#endif // INPUTCTRLDEF_H

/******************************************************************************
文件名：    global.h
功能：      定义一些全局性的变量，宏，枚举、结构体,用于本程序内使用
作者：      刘金煌
创建日期：   2013年3月25日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef GLOBAL_H
#define GLOBAL_H

#include "dataDef.h"
#include "pubDef.h"
#include <fcntl.h>
#include <QTime>
#include <QMap>
#include <QTranslator>
#include "util/DlgLoading.h"
#include <QVector>
#include <QSplashScreen>

// make sure to call data_sendCmd firstly
extern bool         g_bSendCmd;
extern QTranslator  g_translator;
//Frank Wu,20131220
//extern void*        g_Wdg2P9Cfg;
extern void *         g_WdgFCfgGroup;
extern QTime        g_timeLoginConfig;
extern QTime        g_timeElapsedNoKeyPress;
extern QTime        g_timeElapsedKeyPress;
// 防止数据刷新时翻页慢
// 第一次Enter时要刷新数据与g_timeElapsedKeyPress配合使用
extern bool         g_bEnterFirstly;
// 防止升级时弹出对话框还要检测背光告警进入屏保
extern bool         g_bDetectWork;
extern bool         g_bLogin;
extern int          g_nInAlarmType;
extern int          g_nLastWTNokey;
extern int          g_nLastWTScreenSaver;
// 进入页面的方式，ESC按键进入:ENT按键进入:自动进入
extern int          g_nEnterPageType;

extern QMap<enum LANGUAGE_TYPE, const char*> g_mapLangStr;
extern QMap<QString,QString> g_mapLangSymbol;
extern QMap<QString,enum LANGUAGE_TYPE> g_mapStrLangType;
extern const char*  g_szTimeFormat[3];
extern const char* g_szHomePageDateFmt[3];
extern const char* g_szHomePageDateFmtWithoutYear[3];
#define FORMAT_TIEM  "hh:mm:ss"
extern char         g_dataBuff[SIZEOF_MAPFILE];

enum IN_ALARM_TYPE
{
    IN_ALARM_TYPE_KEYPRESS,
    IN_ALARM_TYPE_NOKEYPRESS,
    IN_ALARM_TYPE_SCREENSAVER
};

void initGlobalVar();
void mainSleep(int mSec);

#endif // GLOBAL_H

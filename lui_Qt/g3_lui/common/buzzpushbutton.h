/******************************************************************************
文件名：    buzzpushbutton.h
功能：      QPushButton的继承类，实现按键蜂鸣器响
作者：      刘金煌
创建日期：   2013年12月6日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef BUZZPUSHBUTTON_H
#define BUZZPUSHBUTTON_H

#include <QPushButton>


class BuzzPushButton : public QPushButton
{
    Q_OBJECT
public:
    explicit BuzzPushButton(QWidget *parent = 0);

signals:

public slots:
    /***************************************************************
    功能：按钮被按下，按钮被释放处理
    输入：无
    输出：同功能描述
    ****************************************************************/
    void Press();
    void Release();

protected:
    //virtual void keyPressEvent(QKeyEvent * event);

private slots:
    /***************************************************************
    功能：停止鸣叫
    输入：无
    输出：同功能描述
    ****************************************************************/
    void StopBuzz();

};

#endif // BUZZPUSHBUTTON_H

#ifndef PUBINCLUDE_H
#define PUBINCLUDE_H

#include <qglobal.h>

// 测试gui 不是真实数据
// delete release
#ifdef Q_OS_LINUX
    //#define TEST_GUI
#else
    #define TEST_GUI
#endif

#include "utility.h"
#include "sysInit.h"
#include "dataSource.h"

#endif // PUBINCLUDE_H

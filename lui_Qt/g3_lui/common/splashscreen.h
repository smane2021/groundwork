/******************************************************************************
文件名：    splashscreen.h
功能：      在Splash的启动画面中加入进度条
作者：      刘金煌
创建日期：   2012年2月6日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef SPLASHSCREEN_H
#define SPLASHSCREEN_H

#include <QSplashScreen>
#include <QProgressBar>
#include <QApplication>

class SplashScreen : public QSplashScreen
{
    Q_OBJECT
public:
    //explicit SplashScreen(QWidget *parent = 0);
    explicit SplashScreen(QApplication *app, const QPixmap &pix);
    virtual ~SplashScreen();

    //void SetCurrentProgress(int value);
    //void SetProgressBar();

signals:

public slots:

protected:
    //virtual void drawContents(QPainter *painter);

private slots:
    //void ProgressBarUpdated(int);

private:
    QApplication *app;
//    QProgressBar *progressBar;
//    int currVal;

};

#endif // SPLASHSCREEN_H

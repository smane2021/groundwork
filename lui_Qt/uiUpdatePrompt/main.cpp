/******************************************************************************
文件名：    main.cpp
功能：      M830B1.8寸(128*160像素)和M830D3.2寸(240*320像素)

作者：
创建日期：   2013年9月16日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include <QtGui/QApplication>
#include <QDesktopWidget>
#include <QString>


#include "sysInit.h"


#include "CUpdatePrompt.h"



#ifdef Q_OS_LINUX
#include <unistd.h>
#include <time.h>
#endif

#define APP_ARG_COUNT   (3)

int main(int argc, char *argv[])
{
    int nRet = 0;
    QString sArgScreenSize('s'), sArgScreenType('n'), sArgProgress('0');
    QString sArgMaxUpdateTimeMs('0');
    CUpdatePrompt::SCREEN_SIZE eScreenSize = CUpdatePrompt::SML;
    CUpdatePrompt::SCREEN_TYPE eScreenType = CUpdatePrompt::NORMAL;
    int nArgMaxUpdateTimeMs = 0;
    int nProgressPercent = 0;
    char ch;

    int app_argc = APP_ARG_COUNT;
    char buf[APP_ARG_COUNT][100];
    char *app_argv[APP_ARG_COUNT];

    /* set QApplication args */
    app_argv[0] = &buf[0][0];
    app_argv[1] = &buf[1][0];
    app_argv[2] = &buf[2][0];
    memset(buf, 0, sizeof(buf));
    strcpy(app_argv[0], argv[0]);
    strcpy(app_argv[1], "-qws");
    strcpy(app_argv[2], "-nomouse");

    if(argc <= 1) /* size: small, middle, big */
    {
        printf("usage: %s screen_size screen_type [max_update_time_ms] [progress]\n", argv[0]);
        printf("screen_size: s/small or m/middle or b/big\n");
        printf("screen_type: n/normal or v/vertical\n");
        printf("max_update_time_ms:a number, default value is 240000=4Min\n");
        printf("progress: 0-100\n");
         printf("eg. %s s n\n", argv[0]);
        printf("version: 0.2\n");
        printf("build date: %s %s\n", __DATE__, __TIME__);
    }

    if(argc > 1) /* size: small, middle, big */
    {
        sArgScreenSize = argv[1];
    }
    if(argc > 2) /* type: normal, vertical */
    {
        sArgScreenType = argv[2];
    }
    if(argc > 3) /* max_update_time_ms */
    {
        sArgMaxUpdateTimeMs = argv[3];
    }
    if(argc > 4) /* progress */
    {
        sArgProgress = argv[4];
    }


    /*process args: size of screen */
    ch = sArgScreenSize.at(0).toLatin1();
    switch(ch)
    {
        case 's':
        {
            eScreenSize = CUpdatePrompt::SML;
            break;
        }
        case 'm':
        {
            eScreenSize = CUpdatePrompt::MID;
            break;
        }
        case 'b':
        {
            eScreenSize = CUpdatePrompt::BIG;
            break;
        }
        default:
        {
            eScreenSize = CUpdatePrompt::SML;
            break;
        }
    }

    /*process args: type of screen */
    ch = sArgScreenType.at(0).toLatin1();
    switch(ch)
    {
        case 'n':
        {
            eScreenType = CUpdatePrompt::NORMAL;
            break;
        }
        case 'v':
        {
            eScreenType = CUpdatePrompt::VERTICAL;
            break;
        }
        default:
        {
            eScreenType = CUpdatePrompt::NORMAL;
            break;
        }
    }

    /*process args: max_update_time_ms */
    nArgMaxUpdateTimeMs = sArgMaxUpdateTimeMs.toInt();

    /*process args: progress percent */
    nProgressPercent = sArgProgress.toInt();
    nProgressPercent = (nProgressPercent < 0)? 0: nProgressPercent;
    nProgressPercent = (nProgressPercent > 100)? 100: nProgressPercent;

    printf("eScreenSize=%d, eScreenType=%d, nProgressPercent=%d\n", eScreenSize, eScreenType, nProgressPercent);
    printf("nArgMaxUpdateTimeMs=%d\n", nArgMaxUpdateTimeMs);

    sys_open(); /* init driver, include framebuffer */
    //sys_actFrameBuffer(); /* enable framebuffer */

    QApplication app(app_argc, app_argv);

    Qt::WindowFlags flags = Qt::FramelessWindowHint;//disable top frame
    CUpdatePrompt w(eScreenSize, eScreenType, nArgMaxUpdateTimeMs);
    w.setWindowFlags(flags);
    w.show();
    nRet = app.exec();

    sys_close();

    return nRet;
}

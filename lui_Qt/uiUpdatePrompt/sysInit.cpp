
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>


#include "sysInit.h"

#include <sys/types.h>


static int   gs_frameBufferFD = -1;

int sys_open(void)
{
    printf( "sys_open 913" );
    gs_frameBufferFD = open(DEV_FRAME_BUFFER, O_RDWR);
    if(gs_frameBufferFD < 0)
    {
        printf( "sys_open failed gs_frameBufferFD < 0" );
        return ERROR_SYS_OPEN_FAILED;
    }

    return ERROR_SYS_OK;
}


int sys_close(void)
{
    if(gs_frameBufferFD > 0)
    {
       close( gs_frameBufferFD );
       gs_frameBufferFD = -1;
    }
    return ERROR_SYS_OK;
}


int sys_actFrameBuffer(void)
{
    if(gs_frameBufferFD > 0)
    {
	ioctl( gs_frameBufferFD, IOCTL_ACT_LCD, 0 );
    }
    return ERROR_SYS_OK;
}



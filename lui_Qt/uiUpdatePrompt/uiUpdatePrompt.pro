# -------------------------------------------------
# Project created by QtCreator 2013-03-14T09:12:29
# -------------------------------------------------
win32:QT += testlib

# M830B是1.8寸(128*160像素) M830D是3.2寸(240*320像素)
# M830B M830D
DEFINES += M830D
QT += core \
    gui
CONFIG += qt

# debug
TARGET = ui_update
TEMPLATE = app
DESTDIR = ../output
INCLUDEPATH += 
LIBS += -L../output \
    -L../output
SOURCES += main.cpp \
    CUpdatePrompt.cpp \
    sysInit.cpp
HEADERS += CUpdatePrompt.h \
    CUpdatePrompt.h \
    sysInit.h
FORMS += 

# basicWidget/HomePageWindow2.ui
RESOURCES += img.qrc
TRANSLATIONS += 
unix { 
    # INCLUDEPATH +=  ./tmp
    # 指定uic命令将.ui文件转化成ui_*.h文件的存放的目录
    UI_DIR += ./tmp
    
    # 指定rcc命令将.qrc文件转换成qrc_*.h文件的存放目录
    RCC_DIR += ./tmp
    
    # 指定moc命令将含Q_OBJECT的头文件转换成标准.h文件的存放目录
    MOC_DIR += ./tmp
    
    # 指定目标文件(obj)的存放目录
    OBJECTS_DIR += ./tmp
    HEADERS += 
    SOURCES += 
    FORMS += 
    RESOURCES += 
    LIBS += 
}

#ifndef _C_UPDATE_PROMPT__H_
#define _C_UPDATE_PROMPT__H_

#include <QPixmap>
#include <QLabel>
#include <QTimer>


#include <QDialog>

class CUpdatePrompt :
        public QDialog
{
    Q_OBJECT
public:
    enum SCREEN_SIZE
    {
        SML,
        MID,
        BIG
    };
    enum SCREEN_TYPE
    {
        NORMAL,
        VERTICAL
    };

public:
    CUpdatePrompt(SCREEN_SIZE eScreenSize, SCREEN_TYPE eScreenType, int nMaxUpdateTimeMs = 0, QWidget *parent = 0);
    virtual ~CUpdatePrompt();
private:
    void layoutWindow(SCREEN_SIZE eScreenSize, SCREEN_TYPE eScreenType);
    void loadImgAndTxt(SCREEN_SIZE eScreenSize, SCREEN_TYPE eScreenType);

private slots:
    void sltUpdateImgAndTxt(void);
    void sltActiveFb(void);

private:
    QList<QPixmap> m_oImgList;
    QLabel *m_pStatusLbl;
    QLabel *m_pTxtLbl;
    QString m_sTxtInfo;
    QTimer *m_pUpdateTimer;
    QTimer *m_pActiveFbTimer;
    int m_nCurImgNum;
    int m_nUpdateTimeMs;//ms
    int m_nMaxUpdateTimeMs;//ms

    SCREEN_SIZE m_eScreenSize;
    SCREEN_TYPE m_eScreenType;

};




#endif // _C_UPDATE_PROMPT__H_



/******************************************************************************
文件名：    sysInit.h
功能：      系统初始化、设置接口头文件
作者：      刘金煌
创建日期：  2013年03月25日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef SYSINIT_H
#define SYSINIT_H

#ifdef __cplusplus
extern "C"
{
#endif

#if defined(SYSINIT_LIBRARY)
    #define SYSINITSHARED __attribute__((visibility("default")))
    #define SYSINITHIDDEN __attribute__((visibility("hidden")))
#else
    #define SYSINITSHARED
#endif

#define DEV_FRAME_BUFFER       "/dev/fb0"

// framebuffer fb0
#define IOCTL_RESET_LCD                 0x5600
#define IOCTL_ACT_LCD			0x5602
#define IOCTL_STOP_LCD			0x5601

SYSINITSHARED int   sys_open(void);
SYSINITSHARED int   sys_close(void);


// FrameBuffer
SYSINITSHARED int sys_actFrameBuffer();

#define ERROR_SYS_OK               (0)
#define ERROR_SYS_OPEN_SHM_FAILED  (1)
#define ERROR_SYS_OPEN_FAILED      (2)
#define ERROR_SYS_BUZZ_FAILED      (3)
#define ERROR_SYS_LED_FAILED       (4)


#ifdef __cplusplus
}
#endif

#endif // SYSINIT_H

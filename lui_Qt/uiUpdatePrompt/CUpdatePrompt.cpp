/******************************************************************************
文件名：    mainwindow.cpp
功能：

作者：
创建日期：   2013年3月25日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include <QFont>
#include <QFontMetrics>




#include "CUpdatePrompt.h"

#include "sysInit.h"

#define DEFAULT_MAX_UPDATE_TIME_MS      (4*60*1000)//4Min
#define PAPGE_LOAD_TIMER_INTERVAL       (300) /* msec */
#define FB_ACTIVE_TIMER_INTERVAL        (2000) /* msec */

#define TEST_FACTOR     (1) /* just be used for debug */

#define LAYOUT_TYPE_SML_NORMAL_WIDTH     (160/TEST_FACTOR)  /* 1.8' LCD */
#define LAYOUT_TYPE_SML_NORMAL_HEIGHT    (128/TEST_FACTOR)
#define LAYOUT_TYPE_SML_VERTICAL_WIDTH   (128/TEST_FACTOR)  /* 1.8' LCD vertical*/
#define LAYOUT_TYPE_SML_VERTICAL_HEIGHT  (160/TEST_FACTOR)
#define LAYOUT_TYPE_MID_NORMAL_WIDTH     (320/TEST_FACTOR)  /* 2.8' LCD */
#define LAYOUT_TYPE_MID_NORMAL_HEIGHT    (240/TEST_FACTOR)
#define LAYOUT_TYPE_MID_VERTICAL_WIDTH   (240/TEST_FACTOR)  /* 2.8' LCD vertical */
#define LAYOUT_TYPE_MID_VERTICAL_HEIGHT  (320/TEST_FACTOR)
#define LAYOUT_TYPE_DEFAULT_WIDTH     LAYOUT_TYPE_SML_NORMAL_WIDTH  /* default LCD , 1.8' LCD */
#define LAYOUT_TYPE_DEFAULT_HEIGHT    LAYOUT_TYPE_SML_NORMAL_HEIGHT

/* config font image and text offset */
#define LAYOUT_IMG_H_SCALE     (0.5)  /* image horizontal offset, left to right */
#define LAYOUT_IMG_V_SCALE     (0.4)  /* image vertical offset, top to bottom */
#define LAYOUT_TXT_H_SCALE     (0.5)  /* text horizontal offset, left to right */
#define LAYOUT_TXT_V_SCALE     (0.8)  /* text vertical offset, top to bottom*/

/* config font */
#define LAYOUT_FONT_FAMILY_SML     "wenquanyi_zen_hei"   /* 1.8' LCD */
#define LAYOUT_FONT_SIZE_SML       (12)
#define LAYOUT_FONT_FAMILY_MID     "wenquanyi_zen_hei"   /* 2.8' LCD */
#define LAYOUT_FONT_SIZE_MID       (20)
#define LAYOUT_FONT_FAMILY_DEFAULT     LAYOUT_FONT_FAMILY_SML
#define LAYOUT_FONT_SIZE_DEFAULT       LAYOUT_FONT_SIZE_SML


static int gs_aiProgressTable[] =
{
    0, 1, 5, 10, 18, 28, 35, 35, 35, 35,
    36, 36, 36, 36, 36, 36, 37, 37, 37, 37,
    40, 45, 70, 71, 72, 73, 74, 75, 76, 77,
    77, 77, 77, 78, 78, 78, 78, 79, 79, 79,
    79, 80, 80, 80, 81, 81, 82, 82, 82, 82,
    83, 83, 83, 83, 83, 84, 85, 86, 87, 88,
    89, 90, 90, 90, 90, 90, 90, 90, 91, 91,
    91, 91, 91, 91, 92, 92, 92, 92, 92, 92,
    93, 93, 93, 93, 94, 94, 94, 94, 94, 94,
    96, 96, 96, 96, 96, 97, 98, 98, 98, 99,

};


CUpdatePrompt::CUpdatePrompt(SCREEN_SIZE eScreenSize, SCREEN_TYPE eScreenType, int nMaxUpdateTimeMs, QWidget *parent) :
    QDialog(parent)
{
    m_eScreenSize = eScreenSize;
    m_eScreenType = eScreenType;

    m_nUpdateTimeMs = 0;
    if(nMaxUpdateTimeMs <= 0)
    {
        m_nMaxUpdateTimeMs = DEFAULT_MAX_UPDATE_TIME_MS;
    }
    else
    {
        m_nMaxUpdateTimeMs = nMaxUpdateTimeMs;
    }

    loadImgAndTxt(m_eScreenSize, m_eScreenType);  /* load pictures and text*/
    m_nCurImgNum = -1;

    m_pStatusLbl = new QLabel(this);  /* for show picture */
    m_pTxtLbl = new QLabel(this);  /* for show text info */
    sltUpdateImgAndTxt();

    m_pUpdateTimer = new QTimer(this); /* for update picture */
    if(m_pUpdateTimer != NULL)
    {
        m_pUpdateTimer->setInterval(PAPGE_LOAD_TIMER_INTERVAL); /* need to find out suitable time interval */
        m_pUpdateTimer->start();
    }

    m_pActiveFbTimer = new QTimer(this); /* for active framebuffer  without winking */
    if(m_pActiveFbTimer != NULL)
    {
        m_pActiveFbTimer->setSingleShot(true);
        m_pActiveFbTimer->setInterval(FB_ACTIVE_TIMER_INTERVAL); /* need to find out suitable time interval */
        m_pActiveFbTimer->start();
    }

    layoutWindow(m_eScreenSize, m_eScreenType);

    connect(m_pUpdateTimer, SIGNAL(timeout()), this, SLOT(sltUpdateImgAndTxt()));
    connect(m_pActiveFbTimer, SIGNAL(timeout()), this, SLOT(sltActiveFb()));

}

CUpdatePrompt::~CUpdatePrompt()
{
    if(m_pUpdateTimer)
    {
        m_pUpdateTimer->stop();
        delete m_pUpdateTimer;
    }

    if(m_pActiveFbTimer)
    {
        m_pActiveFbTimer->stop();
        delete m_pActiveFbTimer;
    }

    if(m_pStatusLbl)
    {
        delete m_pStatusLbl;
    }

    if(m_pTxtLbl)
    {
        delete m_pTxtLbl;
    }
}



void CUpdatePrompt::layoutWindow(SCREEN_SIZE eScreenSize, SCREEN_TYPE eScreenType)
{
    int nScreenWidth = LAYOUT_TYPE_DEFAULT_WIDTH, nScreenHeight = LAYOUT_TYPE_DEFAULT_HEIGHT;

    /* set font */
    QFont ft;
    ft.setWeight(QFont::Bold);
    if(eScreenSize == SML)
    {
        ft.setFamily(LAYOUT_FONT_FAMILY_SML);
        ft.setPointSize(LAYOUT_FONT_SIZE_SML);
    }
    else if((eScreenSize == MID) || (eScreenSize == BIG))
    {
        ft.setFamily(LAYOUT_FONT_FAMILY_MID);
        ft.setPointSize(LAYOUT_FONT_SIZE_MID);
    }
    else
    {
        ft.setFamily(LAYOUT_FONT_FAMILY_DEFAULT);
        ft.setPointSize(LAYOUT_FONT_SIZE_DEFAULT);
    }
    setFont(ft);

    /* compute screen size */
    if(eScreenSize == SML)
    {
        if(eScreenType == VERTICAL)
        {
            nScreenWidth = LAYOUT_TYPE_SML_VERTICAL_WIDTH;
            nScreenHeight = LAYOUT_TYPE_SML_VERTICAL_HEIGHT;
        }
        else
        {
            nScreenWidth = LAYOUT_TYPE_SML_NORMAL_WIDTH;
            nScreenHeight = LAYOUT_TYPE_SML_NORMAL_HEIGHT;
        }
    }
    else if((eScreenSize == MID)|| (eScreenSize == BIG))
    {
        if(eScreenType == VERTICAL)
        {
            nScreenWidth = LAYOUT_TYPE_MID_VERTICAL_WIDTH;
            nScreenHeight = LAYOUT_TYPE_MID_VERTICAL_HEIGHT;
        }
        else
        {
            nScreenWidth = LAYOUT_TYPE_MID_NORMAL_WIDTH;
            nScreenHeight = LAYOUT_TYPE_MID_NORMAL_HEIGHT;
        }
    }
    else
    {
        if(eScreenType == VERTICAL)
        {
            nScreenWidth = LAYOUT_TYPE_SML_VERTICAL_WIDTH;
            nScreenHeight = LAYOUT_TYPE_SML_VERTICAL_HEIGHT;
        }
        else
        {
            nScreenWidth = LAYOUT_TYPE_SML_NORMAL_WIDTH;
            nScreenHeight = LAYOUT_TYPE_SML_NORMAL_HEIGHT;
        }
    }


    setGeometry(0, 0, nScreenWidth, nScreenHeight);


    /* layout Image QLabel*/
    if(m_pStatusLbl != NULL)
    {
        int nStartX = 0, nStartY = 0;
        double nStartHScale = LAYOUT_IMG_H_SCALE, nStartVScale = LAYOUT_IMG_V_SCALE;


        if(m_oImgList.count() > 0)
        {
           QSize sz = m_oImgList.at(0).size(); /* use first picture size as reference */
           nStartX = (int)( (nScreenWidth - sz.width())*nStartHScale);
           nStartY = (int)( (nScreenHeight - sz.height())*nStartVScale);
        }

        /* make sure in range */
        if( (nStartX < 0) || (nStartX > nScreenWidth) )
        {
           nStartX = 0;
           nStartY = 0;
        }
        else if( (nStartY < 0) || (nStartY > nScreenHeight) )
        {
            nStartX = 0;
            nStartY = 0;
        }

        m_pStatusLbl->move(nStartX, nStartY);
    }

    /* layout Text QLabel*/
    if(m_pTxtLbl != NULL)
    {
        QString sTxtInfo = m_sTxtInfo + " %0  ";//%100
        QFont qf = font();
        QFontMetrics fm(qf);
        int nTxtWidth = fm.width(sTxtInfo);
        int nTxtHeight = fm.height();
        int nStartX = 0, nStartY = 0;
        double nStartHScale = LAYOUT_TXT_H_SCALE, nStartVScale = LAYOUT_TXT_V_SCALE;

        nStartX = (int)( (nScreenWidth - nTxtWidth)*nStartHScale );
        nStartY = (int)( (nScreenHeight - nTxtHeight)*nStartVScale );

        m_pTxtLbl->setFont(qf);
        m_pTxtLbl->setText(sTxtInfo);
        m_pTxtLbl->setGeometry(nStartX, nStartY, nTxtWidth, nTxtHeight);
    }
}

void CUpdatePrompt::loadImgAndTxt(SCREEN_SIZE eScreenSize, SCREEN_TYPE eScreenType)/* maybe need to modify */
{
    QString sImgLocatePrefix = ":/img/";
    QString sImgLocateSuffix = "/loading%1.png";
    QString sImgLocate;
    QPixmap oPixmap;
    int nStart = 1, nEnd = 6;
    int i;

    /* compute screen size */
    if(eScreenSize == SML)
    {
        if(eScreenType == VERTICAL)
        {
            sImgLocatePrefix.append("sml_v");
        }
        else
        {
            sImgLocatePrefix.append("sml_n");
        }
    }
    else if((eScreenSize == MID) || (eScreenSize == BIG))
    {
        if(eScreenType == VERTICAL)
        {
            sImgLocatePrefix.append("mid_n");
        }
        else
        {
            sImgLocatePrefix.append("mid_n");
        }
    }
    else
    {
        if(eScreenType == VERTICAL)
        {
            sImgLocatePrefix.append("sml_v");
        }
        else
        {
            sImgLocatePrefix.append("sml_n");
        }
    }


    for(i = nStart; i <= nEnd; i++)
    {
        sImgLocate = sImgLocatePrefix+ sImgLocateSuffix.arg(i);
        qWarning("load:" + sImgLocate.toLatin1());
        oPixmap.load(sImgLocate);
        m_oImgList.append(oPixmap);
    }

    /* load text info string */
    m_sTxtInfo = QString(tr("App Updating"));
}

void CUpdatePrompt::sltUpdateImgAndTxt(void)
{
    int nImgCount = m_oImgList.count();

    /* calc update progress */
    m_nUpdateTimeMs += PAPGE_LOAD_TIMER_INTERVAL;
    m_nUpdateTimeMs = (m_nUpdateTimeMs > m_nMaxUpdateTimeMs)? m_nMaxUpdateTimeMs: m_nUpdateTimeMs;
    int nTableNodeNum = sizeof(gs_aiProgressTable)/sizeof(gs_aiProgressTable[0]);
    int nCurrentProgressNode = (int)((float)nTableNodeNum*((float)m_nUpdateTimeMs/(float)m_nMaxUpdateTimeMs));
    nCurrentProgressNode = (nCurrentProgressNode >= nTableNodeNum)? nTableNodeNum - 1: nCurrentProgressNode;
    QString sUpdateProgress;
    sUpdateProgress.sprintf(" %%%d", gs_aiProgressTable[nCurrentProgressNode]);

    if( nImgCount > 0 )
    {
        m_nCurImgNum++;
        /* make sure in range[0, nImgCount) */
        m_nCurImgNum = (m_nCurImgNum < 0)? 0: m_nCurImgNum;
        m_nCurImgNum = (m_nCurImgNum >= nImgCount)? 0: m_nCurImgNum;

        /* show image */
        if(m_pStatusLbl != NULL)
        {
            m_pStatusLbl->resize(m_oImgList.at(m_nCurImgNum).size());
            m_pStatusLbl->setPixmap( m_oImgList.at(m_nCurImgNum));
        }
        /* show text */
        if(m_pTxtLbl != NULL)
        {
#if 0
            QStringList sSuffixList;
            sSuffixList.append("");
            sSuffixList.append(".");
            sSuffixList.append("..");
            sSuffixList.append("...");
            if( (m_nCurImgNum >= 0) && (m_nCurImgNum < sSuffixList.count()) )
            {
                m_pTxtLbl->setText(m_sTxtInfo + sSuffixList.at(m_nCurImgNum));
            }
#else
            m_pTxtLbl->setText(m_sTxtInfo + sUpdateProgress);
#endif
        }



    }
}


void CUpdatePrompt::sltActiveFb(void)
{
    sys_actFrameBuffer(); /* enable framebuffer */
}



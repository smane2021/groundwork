//cd /home/ljhuang/g3_lui/utility
//arm-wrs-linux-gnueabi-armv5tel-glibc_small-readelf -s ../output/libutility.so
#include "utility.h"

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

//arm-wrs-linux-gnueabi-armv5tel-glibc_small-g++ -shared ./utility/Utility.cpp -o ./output/libutility.so -I ./public

static ShmDataUtility* pShmData;
void util_init(int nClass)
{
	printf( "util_init 12.13\n" );
	static int shmid = -1;
	shmid = shmget( (key_t)SHM_KEY_UTILITY, sizeof(ShmDataUtility), IPC_CREAT|0666 );
	if(shmid < 0)
	{
		printf( "util_init shmget failed \n" );
	}
	pShmData = (ShmDataUtility*)shmat(shmid, NULL, 0);
	pShmData->nLogClass = nClass;
}

void util_writeLogDeb(const char * format, ...)
{
	char buffer[4096];
	if (pShmData->nLogClass < 0) return;

	time_t now;
	struct tm *timenow;
	time( &now );
	timenow = localtime( &now );
	sprintf(buffer, "%04d-%02d-%02d %02d:%02d:%02d ",
		timenow->tm_year+1900, timenow->tm_mon+1, timenow->tm_mday,
		timenow->tm_hour, timenow->tm_min, timenow->tm_sec
		);

	va_list arglist;
	va_start(arglist, format);
	vsnprintf(buffer+20, 4076, format, arglist);
	va_end(arglist);
	printf("%s \n", buffer);
}

void util_writeLog1(const char* format, ...)
{
	char buffer[4096];
	if (pShmData->nLogClass < 1) return;

	time_t now;
	struct tm *timenow;
	time( &now );
	timenow = localtime( &now );
	sprintf(buffer, "%04d-%02d-%02d %02d:%02d:%02d ",
		timenow->tm_year+1900, timenow->tm_mon+1, timenow->tm_mday,
		timenow->tm_hour, timenow->tm_min, timenow->tm_sec
		);

	va_list arglist;
	va_start(arglist, format);
	vsnprintf(buffer+20, 4076, format, arglist);
	va_end(arglist);
	printf("%s \n", buffer);
}

void util_writeLog2(const char *file, int line, const char *format, ...)
{
	char buffer[4096];
	if (pShmData->nLogClass < 2) return;

	time_t now;
	struct tm *timenow;
	time( &now );
	timenow = localtime( &now );
	sprintf(buffer, "%04d-%02d-%02d %02d:%02d:%02d ",
		timenow->tm_year+1900, timenow->tm_mon+1, timenow->tm_mday,
		timenow->tm_hour, timenow->tm_min, timenow->tm_sec
		);

	va_list arglist;
	va_start(arglist, format);
	vsnprintf(buffer+20, 4076, format, arglist);
	va_end(arglist);
	printf("file[%s] line[%d] %s \n", file, line, buffer);
}


void printLogDeb(const char *format, ...)
{
	char buffer[4096];
	va_list va;
	va_start(va, format);
	vsprintf(buffer, format, va);
	va_end(va);
	printf("%s \n", buffer);
}

void printLog1(const char *format, ...)
{
	char buffer[4096];
	va_list va;
	va_start(va, format);
	vsprintf(buffer, format, va);
	va_end(va);
	printf("%s \n", buffer);
}

void printLog2(const char *file, int line, const char *format, ...)
{
	char buffer[4096];
	va_list va;
	va_start(va, format);
	vsprintf(buffer, format, va);
	va_end(va);
	printf("file[%s] line[%d] %s \n", file, line, buffer);
}


UTILITYHIDDEN int main(int argc, char *argv[])
{
    printf( "main\n" );

	util_init();

	int nWriting = 1;
	while (1)
	{
		printf( "%d \n", nWriting );
		TRACEDEBUG( "hello debug" );
		TRACELOG1( "hello trace 1" );
		TRACELOG2( "hello trace 2" );
		++nWriting;
		sleep( 3 );
	}

    printf( "end return\n" );
    return 0;
}

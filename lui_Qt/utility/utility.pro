#-------------------------------------------------
#
# Project created by QtCreator 2013-03-11T15:19:42
#
#-------------------------------------------------

QT       -= core gui
TARGET = utility
TEMPLATE = lib
#DEFINES += UTILITY_LIBRARY
DESTDIR = ../output
INCLUDEPATH += ../public
LIBS += -L ../output


SOURCES += utility.cpp

HEADERS += ../public/utility.h

unix{
INCLUDEPATH += ./release
#指定uic命令将.ui文件转化成ui_*.h文件的存放的目录
UI_DIR += ./release
#指定rcc命令将.qrc文件转换成qrc_*.h文件的存放目录
RCC_DIR += ./release
#指定moc命令将含Q_OBJECT的头文件转换成标准.h文件的存放目录
MOC_DIR += ./release
#指定目标文件(obj)的存放目录
OBJECTS_DIR += ./release
}

OTHER_FILES += \
    Makefile

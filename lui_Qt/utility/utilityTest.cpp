#include <stdlib.h>
#include "../public/utility.h"

int main(int argc,char **argv)
{
	int iLevel = 0;
	if(argc < 2)
	{
		return (-1); 
	}
	
	iLevel = atoi(argv[1]);
	
	util_init(iLevel);
	
	return 0;
}
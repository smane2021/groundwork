/******************************************************************************
文件名：    dataSource.cpp
功能：      数据交互接口
作者：      刘金煌
创建日期：   2013年5月3日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "dataSource.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <semaphore.h>

#include "utility.h"
#include "dataDef.h"

static sem_t* g_psemCmd     = NULL;
static sem_t* g_psemAck     = NULL;
static sem_t* g_psemAlm     = NULL; // 告警信号量
static sem_t* g_psemBeat    = NULL; // 心跳信号量

static int    g_fd_mapfile        = -1;   //共享内存fd
static int    g_fd_mapfile_alarm  = -1;   //告警共享内存fd
static void*  g_map_addr          = NULL; //共享内存首地址
static void*  g_map_addr_alarm    = NULL; //告警共享内存首地址

static int gs_semVal = 0;

int data_open(void)
{
	TRACELOG1( "dataSource data_open 14-8-5" );
	// semaphore
	//发送
	g_psemCmd = sem_open(SYNCNAME_WRITE, O_CREAT,
		S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH, 0);
	if(g_psemCmd == SEM_FAILED || g_psemCmd == NULL)
	{
		TRACELOG2( "data_open sem_open write failed!" );
		return ERRDS_SEM_OPEN;
	}
	sem_getvalue(g_psemCmd, &gs_semVal);
	TRACELOG1( "data_open sem_getvalue g_psemCmd <%d>", gs_semVal);
	//接收
	g_psemAck = sem_open(SYNCNAME_READ, O_CREAT,
		S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH, 0);
	if(g_psemAck == SEM_FAILED || g_psemAck == NULL)
	{
		TRACELOG2( "data_open sem_open read failed!" );
		return ERRDS_SEM_OPEN;
	}
	sem_getvalue(g_psemAck, &gs_semVal);
	TRACELOG1( "data_open sem_getvalue g_psemAck <%d>", gs_semVal);
	// 告警
	g_psemAlm = sem_open(SYNCNAME_ALARM, O_CREAT,
		S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH, 0);
	if(g_psemAlm == SEM_FAILED || g_psemAlm == NULL)
	{
		TRACELOG2( "data_open sem_open alarm failed!" );
		return ERRDS_SEM_OPEN;
	}
	sem_getvalue(g_psemAlm, &gs_semVal);
	TRACELOG1( "data_open sem_getvalue g_psemAlm <%d>", gs_semVal);
	// 心跳
	g_psemBeat = sem_open(SYNCNAME_BEAT, O_CREAT,
		S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH, 0);
	if(g_psemBeat == SEM_FAILED || g_psemBeat == NULL)
	{
		TRACELOG2( "data_open sem_open heatbeat failed!" );
		return ERRDS_SEM_OPEN;
	}
	sem_getvalue(g_psemBeat, &gs_semVal);
	TRACELOG1( "data_open sem_getvalue g_psemBeat <%d>", gs_semVal);

	//mmap
	g_fd_mapfile = ::open(MAPFILE_SIG_NAME,
		O_RDWR | O_CREAT | O_TRUNC, 00777);
	if (g_fd_mapfile < 0)
	{
		TRACELOG2("data_open mapfile open failed!");
		return ERRDS_MAPFILE_OPEN;
	}
	::lseek(g_fd_mapfile, SIZEOF_MAPFILE, SEEK_SET);
	::write(g_fd_mapfile, "", 1);
	g_map_addr = mmap(NULL, SIZEOF_MAPFILE, PROT_READ|PROT_WRITE,
		MAP_SHARED, g_fd_mapfile, 0);
	if (MAP_FAILED == g_map_addr)
	{
		TRACELOG2("data_open mapfile mmap failed!");
		return ERRDS_MAPFILE_MMAP;
	}

	//mmap alarm
	g_fd_mapfile_alarm = ::open(MAPFILE_SIG_NAME_alarm,
		O_RDWR | O_CREAT | O_TRUNC, 00777);
	if (g_fd_mapfile_alarm < 0)
	{
		TRACELOG2("data_open mapfile alarm open failed!");
		return ERRDS_MAPFILE_OPEN;
	}
	::lseek(g_fd_mapfile_alarm, SIZEOF_MAPFILE_alarm, SEEK_SET);
	::write(g_fd_mapfile_alarm, "", 1);
	g_map_addr_alarm = mmap(NULL, SIZEOF_MAPFILE_alarm, PROT_READ|PROT_WRITE,
		MAP_SHARED, g_fd_mapfile_alarm, 0);
	if (MAP_FAILED == g_map_addr_alarm)
	{
		TRACELOG2("data_open mapfile alarm mmap failed!");
		return ERRDS_MAPFILE_MMAP;
	}

	return ERRDS_OK;
}

int data_close(void)
{
	int nRet = 0;
	// close
	if ( (nRet=sem_close(g_psemCmd)) )
	{
		TRACELOG2( "data_close sem_close cmd ret<%d>", nRet );
		goto ERR;
	}
	
	if ( (nRet=sem_close(g_psemAck)) )
	{
		TRACELOG2( "data_close sem_close ack ret<%d>", nRet );
		goto ERR;
	}

	if ( (nRet=sem_close(g_psemAlm)) )
	{
		TRACELOG2( "data_close sem_close alarm ret<%d>", nRet );
		goto ERR;
	}

	if ( (nRet=sem_close(g_psemBeat)) )
	{
		TRACELOG2( "data_close sem_close heatbeat ret<%d>", nRet );
		goto ERR;
	}
	

	// unlink
	if ( (nRet=sem_unlink(SYNCNAME_WRITE)) )
	{
		TRACELOG2( "data_close sem_unlink cmd ret<%d>", nRet );
		goto ERR;
	}

	if ( (nRet=sem_unlink(SYNCNAME_READ)) )
	{
		TRACELOG2( "data_close sem_unlink ack ret<%d>", nRet );
		goto ERR;
	}

	if ( (nRet=sem_unlink(SYNCNAME_ALARM)) )
	{
		TRACELOG2( "data_close sem_unlink alarm ret<%d>", nRet );
		goto ERR;
	}

	if ( (nRet=sem_unlink(SYNCNAME_BEAT)) )
	{
		TRACELOG2( "data_close sem_unlink heatbeat ret<%d>", nRet );
		goto ERR;
	}

	if ( (nRet=munmap(g_map_addr, SIZEOF_MAPFILE)) )
	{
		TRACELOG2( "data_close munmap ret<%d>", nRet );
		goto ERR;
	}
	::close( g_fd_mapfile );

	if ( (nRet=munmap(g_map_addr_alarm, SIZEOF_MAPFILE_alarm)) )
	{
		TRACELOG2( "data_close alarm munmap ret<%d>", nRet );
		goto ERR;
	}
	::close( g_fd_mapfile_alarm );

	return ERRDS_OK;

ERR:
	return ERRDS_CLOSE;
}

int data_sendCmd(CmdItem* pCmd)
{
	//TRACELOG1( ">>> data_sendCmd" );
	int nSemRet = 0;
	// g_psemAck 有信号，还有数据未取 返回
	sem_getvalue(g_psemAck, &gs_semVal);
	while (gs_semVal > 0)
	{
		// 信号量归零
		//TRACELOG1( "data_sendCmd have data to read" );
		nSemRet = sem_trywait( g_psemAck );
		sem_getvalue(g_psemAck, &gs_semVal);
	}
	
	memset( g_map_addr, 0, SIZEOF_MAPFILE );
	memcpy( g_map_addr, pCmd, sizeof(CmdItem) );
	sem_getvalue(g_psemCmd, &gs_semVal);
	if (gs_semVal == 0)
	{
		nSemRet = sem_post( g_psemCmd );
		//TRACELOG1( "data_sendCmd sem_post ret<%d>", nSemRet );
	}

	return ERRDS_OK;
}

int data_recvData(/*void* pCmd, */void** ppData)
{
	// 非阻塞式
	sem_getvalue( g_psemAck, &gs_semVal );
	//TRACELOG1( ">>> data_recvData sem_trywait g_psemAck <%d>", gs_semVal);
	int nSemRet = 0;
	nSemRet = sem_trywait( g_psemAck );
	if(nSemRet != 0)
	{
	//	TRACELOG1( "data_recvData sem_trywait NODATA" );
		return ERRDS_NODATA;
	}
	//TRACELOG1( "data_recvData sem_trywait >>>" );
	
	*ppData = g_map_addr;

	return ERRDS_OK;
}


// 同步方式获取数据
int data_getDataSync(CmdItem* pCmd, void** ppData, int nSecTimeOut)
{
	// 阻塞式
	//TRACEDEBUG( "data_getDataSync nSecTimeOut<%d>", nSecTimeOut );
	int nSemRet = 0;
	// g_psemAck 有信号，还有数据未取 返回
	sem_getvalue(g_psemAck, &gs_semVal);
	while (gs_semVal > 0)
	{
		// 信号量归零
		//TRACEDEBUG( "data_getDataSync have data to read" );
		nSemRet = sem_trywait( g_psemAck );
		sem_getvalue(g_psemAck, &gs_semVal);
	}
	memset( g_map_addr, 0, SIZEOF_MAPFILE );
	memcpy( g_map_addr, pCmd, sizeof(CmdItem) );
	sem_getvalue(g_psemCmd, &gs_semVal);
	if (gs_semVal == 0)
	{
		nSemRet = sem_post( g_psemCmd );
		//TRACEDEBUG( "data_getDataSync sem_post g_psemCmd ret<%d>", nSemRet );
	}
	
	sem_getvalue(g_psemCmd, &gs_semVal);
	//TRACEDEBUG( "data_getDataSync g_psemCmd <%d>", gs_semVal);
	sem_getvalue(g_psemAck, &gs_semVal);
	//TRACEDEBUG( "data_getDataSync g_psemAck <%d>", gs_semVal);

	struct timespec ts;
	if (nSecTimeOut > 0)
	{
		//TRACEDEBUG( "data_getDataSync if (nSecTimeOut > 0) <%d>", nSecTimeOut );
		ts.tv_sec  = time(NULL)+nSecTimeOut;
		ts.tv_nsec = 0;
		if ( sem_timedwait(g_psemAck, &ts) )
		{
			//TRACEDEBUG( "data_getDataSync sem_timewait timeout" );
			return ERRDS_TIMEOUT;
		}
	}
	else
	{
		//TRACEDEBUG( "data_getDataSync else <%d>", nSecTimeOut );
		sem_wait( g_psemAck );
	}

	//TRACEDEBUG( "data_getDataSync sem_wait ret<%d> >>>", nSemRet );
	*ppData = g_map_addr;
	return ERRDS_OK;
}

// 同步方式获取告警数据
int data_recvData_alarm(void** ppData)
{
	*ppData = g_map_addr_alarm;
	return ERRDS_OK;
}

int data_hasAlarm()
{
	// 非阻塞式
	//sem_getvalue( g_psemAlm, &gs_semVal );
	//TRACELOG1( ">>> data_hasAlarm sem_getvalue g_psemAlm <%d>", gs_semVal);
	int nSemRet = sem_trywait( g_psemAlm );
	//sem_getvalue( g_psemAlm, &gs_semVal );
	//TRACELOG1( "data_hasAlarm sem_getvalue sem_trywait g_psemAlm <%d>", gs_semVal);
	if ( nSemRet==0 )
	{
//		TRACELOG1( "data_hasAlarm yes" );
		return ERRDS_HASALARM;
	}
	else
	{
		//TRACELOG1( "data_hasAlarm NO" );
		return ERRDS_NODATA;
	}
}

int data_sendHeartBeat()
{
	//TRACELOG1( "data_sendHeartBeat" );
	int nSemRet = 0;
	// g_psemBeat 有信号，还有数据未取 返回
	sem_getvalue(g_psemBeat, &gs_semVal);
	
	while (gs_semVal > 0)
	{
		// 信号量归零
		//TRACELOG1( "data_sendHeartBeat have signals" );
		nSemRet = sem_trywait( g_psemBeat );
		sem_getvalue(g_psemBeat, &gs_semVal);
	}

	nSemRet = sem_post( g_psemBeat );
	//TRACELOG1( "data_sendHeartBeat >>>" );
	return ERRDS_OK;
}



DATASOURCEHIDDEN int main(int argc, char *argv[])
{
    TRACELOG1( "main in dataSource" );
	if (argc<2)
	{
		TRACELOG1( "main arg not enough" );
		return 1;
	}

	if (strcmp(argv[1], "w") == 0)
	{
		//////////////////////////////////////////////////////////////////////////
		//头
		if ( data_open() )
			TRACELOG1( "main data_open error" );

		int nSemRet = sem_wait( g_psemCmd );
		TRACELOG1( "main sem_wait sem_wait<%d>", nSemRet );
		CmdItem* pcmd = (CmdItem*)g_map_addr;
		TRACELOG1( "main CmdType<%d> ScreenID<%d> EquipID<%d> SigID<%d>", 
			pcmd->CmdType, pcmd->ScreenID, pcmd->setinfo.EquipID,
			pcmd->setinfo.SigID );

		/*
		//////////////////////////////////////////////////////////////////////////
		//主页
		PackGetInfo* info = (PackGetInfo*)g_map_addr;
		
		// Alarm Status
		info->vSigValue.lValue = 1;

		// RectNum ac
		info++;
		info->vSigValue.lValue = 1;

		// MPPTNum solar
		info++;
		info->vSigValue.lValue = 1;

		// DG Existence
		info++;
		info->vSigValue.lValue = 0;

		// Mains failure 线条
		info++;

		// DG Running
		info++;

		// Solar Running
		info++;

		// BattStat
		info++;
		strcpy( info->cEnumText, "junchong" );

		// DCVolt
		info++;
		info->vSigValue.fValue = 53.5;
		strcpy( info->cSigUnit, "V" );

		// Current
		info++;
		info->vSigValue.fValue = 55.2;
		strcpy( info->cSigUnit, "A" );

		// BattCap
		info++;
		info->vSigValue.fValue = 90;
		strcpy( info->cSigUnit, "%" );

		// SysUsed
		info++;
		strcpy( info->cSigName, "Sys Used:" );
		info->vSigValue.fValue = 80;
		strcpy( info->cSigUnit, "%" );
		*/

		//////////////////////////////////////////////////////////////////////////
		//告警
// 		{
// 			PackActAlmInfo* info = (PackGetInfo*)g_map_addr;
// 			
// 		}
// 		
// 
// 		// Alarm Status
// 		info->vSigValue.lValue = 1;
		
		//////////////////////////////////////////////////////////////////////////
		//尾
		sem_getvalue(g_psemAck, &gs_semVal);
		if (gs_semVal == 0)
		{
			nSemRet = sem_post( g_psemAck );
			TRACELOG1( "main sem_post ret<%d>", nSemRet );
		}

		sem_getvalue(g_psemAck, &gs_semVal);
		TRACELOG1( "main sem_getvalue g_psemAck <%d>", gs_semVal);
	}


	if (strcmp(argv[1], "r") == 0)
	{
		if ( data_open() )
			TRACELOG1( "main data_open error" );
		
		CmdItem cmdItem;
		cmdItem.CmdType   = CT_READ;
		cmdItem.ScreenID  = 2;
		cmdItem.setinfo.EquipID = 11;
		cmdItem.setinfo.SigID   = 12;
		data_sendCmd( &cmdItem );
		void* pData = NULL;
		while ( data_recvData(&pData) )
			sleep(3);

		PackGetInfo* info = (PackGetInfo*)pData;
		for (int i=0; i<2; ++i)
		{
			TRACELOG1( "main read data i<%d>"
				"SigID<%d> SigName<%s> SigValue<%f> "
				"cSigUnit<%s>", i,
				info->iSigID, info->cSigName, info->vSigValue.fValue,
				info->cSigUnit );
			info++;
		}

		//data_close();
	}

	// 告警
	if (strcmp(argv[1], "m") == 0)
	{

		TRACELOG1( ">>> main alarm" );
		if ( data_open() )
			TRACELOG1( "main data_open error" );

		int nSemRet = 0;
		//while (true)
		{
			nSemRet = sem_post( g_psemAlm );
			TRACELOG1( "main sem_post ret<%d>", nSemRet );
			sem_getvalue(g_psemCmd, &gs_semVal);
			TRACELOG1( "main sem_getvalue g_psemCmd <%d>", gs_semVal);
			//data_hasAlarm();
			sleep( 100 );
		}

	}

	if (strcmp(argv[1], "c") == 0)
	{
		data_close();
	}

	
	printf( "main end return\n" );
    return 0;
}

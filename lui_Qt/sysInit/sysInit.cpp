#include "sysInit.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>

#include "buzz.h"
#include "lcd.h"
#include "led.h"
#include "sysInitDef.h"

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

static int   gs_globalFD = -1; //LED, LCD, buzzer
static int   gs_frameBufferFD = -1;
static int   gs_fdKeyboard;    // keyboard open in stdKeyboard
static int   gs_shmid = -1;

int sys_open(enum DEV_OP op)
{
	TRACELOG1( "sys_open 12.30 1" );
	gs_shmid = shmget( (key_t)SHM_KEY, sizeof(ShmData), IPC_CREAT|0666 );
	if(gs_shmid < 0)
	{
		TRACELOG2( "sys_open shmget failed" );
		return ERROR_SYS_OPEN_SHM_FAILED;
	}
	ShmData* pShmData = (ShmData*)shmat(gs_shmid, NULL, 0);
	pShmData->bSound = true;
	int* pfdGlobal = &(pShmData->fdDriverGlobal);

	switch (op)
	{
	case DO_WRITE:
		TRACELOG1( "sys_open DO_WRITE pShmData<%x>", pShmData );
		if (gs_globalFD < 0)
		{
			gs_globalFD = ::open(GLOBAL_DRIVE_NAME, O_RDWR);
		}

		if(gs_globalFD < 0)
		{
			TRACELOG2( "sys_open failed gs_globalFD < 0" );
			return ERROR_SYS_OPEN_FAILED;
		}
		*pfdGlobal = gs_globalFD;

		gs_frameBufferFD = ::open(DEV_FRAME_BUFFER, O_RDWR);
		if(gs_frameBufferFD < 0)
		{
			TRACELOG2( "sys_open failed gs_frameBufferFD < 0" );
			return ERROR_SYS_OPEN_FAILED;
		}
		break;

	case DO_READ:
		TRACELOG1( "sys_open DO_READ pShmData<%x>", pShmData );
		gs_globalFD = *pfdGlobal;
		if (gs_globalFD < 0)
		{
			TRACELOG2( "sys_open DO_READ failed gs_globalFD < 0" );
			return ERROR_SYS_OPEN_FAILED;
		}

		gs_fdKeyboard = pShmData->fdKeypad;
		if (gs_fdKeyboard < 0)
		{
			TRACELOG2( "sys_open DO_READ failed gs_fdKeyboard < 0" );
			return ERROR_SYS_OPEN_FAILED;
		}
		break;
	}
	TRACELOG1( "sys_open gs_globalFD<%d> gs_fdKeyboard<%d>", gs_globalFD, gs_fdKeyboard );

 	//写数据完毕，将其从自己的内存段中“删除”出去
	if (shmdt(pShmData) < -1)
 	{
 		TRACELOG2( "sys_open shmdt failed" );
 	}

	Buzz buzz( gs_globalFD );
	buzz.InitBuzz();

	LCD lcd( gs_globalFD );
	lcd.InitLcd();

	Led led( gs_globalFD );
	led.InitLed();

	return ERROR_SYS_OK;
}

int sys_open(void)
{
	if (gs_globalFD < 0)
	{
		gs_globalFD = ::open(GLOBAL_DRIVE_NAME, O_RDWR);
	}
	
	if(gs_globalFD < 0)
	{
		TRACELOG2( "sys_open failed gs_globalFD < 0" );
		return ERROR_SYS_OPEN_FAILED;
	}

	Buzz buzz( gs_globalFD );
	buzz.InitBuzz();
	sys_setBuzz( BUZZ_BEEP );

	LCD lcd( gs_globalFD );
	lcd.InitLcd();

	Led led( gs_globalFD );
	led.InitLed();

	return ERROR_SYS_OK;
}

int sys_close(void)
{
	close( gs_globalFD );
	close( gs_frameBufferFD );
	shmctl(gs_shmid, IPC_RMID, NULL);
	return ERROR_SYS_OK;
}

int sys_setBuzz(enum SET_BUZZ buzz, int iType)
{
	//TRACELOG1( ">>>sys_setBuzz" );
	Buzz::SetBeepType( iType );
	switch (buzz)
	{
	case BUZZ_QUIET:
		Buzz::Quiet();
		break;

	case BUZZ_BEEP:
		Buzz::Beep();

	case BUZZ_MOO:
		Buzz::Moo();
		break;

	default:
		TRACELOG1( "sys_setBuzz default BuzzType<%d>", buzz );
		Buzz::Quiet();
		return ERROR_SYS_BUZZ_FAILED;
	}

	return ERROR_SYS_OK;
}

int sys_getSoundType()
{
	return Buzz::GetBeepType();
}

int sys_getBuzz()
{
	return (Buzz::GetBuzzState() ? 1 : 0);
}

int sys_setBuzzKeyboard(bool bSound)
{
	ShmData* pShmData = (ShmData*)shmat(gs_shmid, NULL, 0);
//	TRACELOG1( "sys_setBuzzKeyboard pShmData<%x> bSound<%d> pShmData->bSound<%d>",pShmData, bSound, pShmData->bSound );
	pShmData->bSound = bSound;

	if (shmdt(pShmData) < -1)
 	{
 		TRACELOG2( "sys_open shmdt failed" );
 	}	

	return ERROR_SYS_OK;
}

int sys_setLCD(enum SET_LCD lcd)
{
	if (sys_getLCD() != lcd)
	{
		LCD::Adjust_BackLight( lcd );
	}

	return ERROR_SYS_OK;
}

int sys_getLCD()
{
	return LCD::GetBackLight();
}

int sys_setLED(enum SET_LED led)
{
	switch (led)
	{
	case LED_RED_ON:
		Led::RedLedOn();
		break;
	case LED_RED_OFF:
		Led::RedLedOff();
		break;

	case LED_YELLOW_ON:
		Led::YellowLedOn();
		break;
	case LED_YELLOW_OFF:
		Led::YellowLedOff();
		break;

	case LED_GREEN_ON:
		Led::GreenLedOn();
		break;
	case LED_GREEN_OFF:
		Led::GreenLedOff();
		break;

	case LED_FLASH:
		Led::GreenLedFlash();
		break;

	default:
		TRACELOG2( "sys_setLED default led<%d>", led );
		return ERROR_SYS_LED_FAILED;
	}

	return ERROR_SYS_OK;
}

int sys_getLED()
{
	return Led::GetLedState();
}

#define IOCTL_GAMMA1         0x5604
#define IOCTL_GAMMA2         0x5605
#define IOCTL_GAMMA3         0x5606
#define IOCTL_GAMMA4         0x5607
int sys_actFrameBuffer()
{
	ioctl( gs_frameBufferFD, IOCTL_ACT_LCD, 0 );
	return ERROR_SYS_OK;
}

int sys_resetFrameBuffer()
{
	ioctl( gs_frameBufferFD, IOCTL_RESET_LCD, 0 );
	return ERROR_SYS_OK;
}

int sys_setLCDGarmar(enum GAMMA_LEVEL gamma)
{
	switch (gamma)
	{
	case GAMMA_LEVEL_1:
		ioctl( gs_frameBufferFD, IOCTL_GAMMA1, 0 );
		break;

	case GAMMA_LEVEL_2:
		ioctl( gs_frameBufferFD, IOCTL_GAMMA2, 0 );
		break;

	case GAMMA_LEVEL_3:
		ioctl( gs_frameBufferFD, IOCTL_GAMMA3, 0 );
		break;

	case GAMMA_LEVEL_4:
		ioctl( gs_frameBufferFD, IOCTL_GAMMA4, 0 );
		break;

	default:
		break;
	}

	return ERROR_SYS_OK;
}

#define KEY_MAJOR_KEYBOARD      250
#define IOC_TIMER_EN                  _IOW(KEY_MAJOR_KEYBOARD, 3, unsigned long) //开定时器轮询键值
#define IOC_TIMER_DIS                 _IOW(KEY_MAJOR_KEYBOARD, 4, unsigned long)

int sys_setKeyboard(bool bEnable)
{
	if (gs_fdKeyboard < 0)
	{
		TRACELOG2( "sys_setKeyboard gs_fdKeyboard < 0" );
		return ERROR_SYS_KEYBOARD_FAILED;
	}
	if ( bEnable )
	{
		ioctl(gs_fdKeyboard,  IOC_TIMER_EN, 2);
	}
	else
	{
		ioctl(gs_fdKeyboard,  IOC_TIMER_DIS, 2);
	}

	return ERROR_SYS_OK;
}

SYSINITHIDDEN int main(int argc, char *argv[])
{
#define  SLEEP_TIME_DRIVER (2)
    TRACELOG1( "sysInit main" );

	//sys_open( DO_READ );
	sys_open( DO_WRITE );

	for (int buzz=BUZZ_QUIET; buzz<BUZZ_MAX; buzz++)
	{
		TRACELOG1( "buzz<%d>", buzz );
		sys_setBuzz( (enum SET_BUZZ)(buzz) );
		sleep( SLEEP_TIME_DRIVER );
	}
	sys_setBuzz( BUZZ_QUIET );

	for (int lcd=LCD_ON_BRIGHTNESS; lcd<LCD_MAX; lcd++)
	{
		TRACELOG1( "lcd<%d>", lcd );
		sys_setLCD( (enum SET_LCD)(lcd) );
		sleep( SLEEP_TIME_DRIVER );
	}
	sys_setLCD( LCD_BACK_LIGHT_1 );

	
	for (int led=LED_RED_ON; led<LED_MAX; led++)
	{
		TRACELOG1( "led<%d>", led );
		sys_setLED( (enum SET_LED)(led) );
		sleep( SLEEP_TIME_DRIVER );
	}

// 	sys_setLED( LED_RED_OFF );
// 	for (int i=0; i<10; ++i)
// 	{
// 		sys_setBuzz( BUZZ_BEEP );
// 		sys_setLED( LED_RED_ON );
// 		sleep( SLEEP_TIME_DRIVER );
// 	}
// 	sys_setLED( LED_RED_OFF );

	
	//sys_close();

    TRACELOG1( "end return" );
    return 0;
}

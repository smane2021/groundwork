/******************************************************************************
文件名：    lcd.h
功能：      lcd背光控制
作者：      刘金煌
创建日期：   2013年05月25日
最后修改日期
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef LCD_H
#define LCD_H

#include "pubDef.h"

class LCD
{
public:
    LCD(int fd);
    //关闭、打开背光
    static void TurnOffBackLight();
    static void TurnOnBackLight();

    static bool GetBackLightState();
	static int  GetBackLight();

    static void Adjust_BackLight(int iLevel);

    static void InitLcd();
private:
    static int lcdFd;
    static bool bIsBackLightOn;
    static int iCurrLCDBackLight;
	// 保存原来的背光值
	static int iOldLCDBackLight;
};
#endif // LCD_H

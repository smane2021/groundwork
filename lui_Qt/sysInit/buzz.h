/******************************************************************************
文件名：    buzz.h
功能：      蜂鸣器控制
作者：      刘金煌
创建日期：   2013年05月23日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef BUZZ_H
#define BUZZ_H

#define BEEP_DURATION   50 //beep的时间（ms级）

class Buzz
{
public:
    Buzz(int fd);

    static void Moo();                  //蜂鸣器长响
    static void Quiet();                //蜂鸣器关闭
    static void Beep();                 //蜂鸣器响一声

    //设置和获取声响的来源
    static void SetBeepType(int iType);
    static int  GetBeepType();

    static bool GetBuzzState();         //获取蜂鸣器状态 true:正在响

    static bool bAlarmVoiceDisabled; //禁止告警音
    static int iTimerInterval; //告警延时

    static void InitBuzz();


private:
    static int buzzFd;
	static bool bIsBeeping;             //是否正在响，true 正在响
    static int  iBeepType;              //声响的类型，0：button，1：告警，2：全部
};

#endif // BUZZ_H

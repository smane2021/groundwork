/******************************************************************************
文件名：    led.h
功能：      led灯光控制
作者：      刘金煌
创建日期：   2013年05月25日
最后修改日期
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef LED_H
#define LED_H

class Led
{
public:
    Led(int fd);

	static void InitLed();

    static void RedLedOn();
    static void RedLedOff();

    static void YellowLedOn();
    static void YellowLedOff();

    static void GreenLedOn();
    static void GreenLedOff();

//    static bool GetYellowLedState();
//    static bool GetRedLedState();

    static void GreenLedFlash();

	static int  GetLedState();

    static bool bIsRedLedOn;
    static bool bIsYellowLedOn;
    static bool bIsGreenLedOn;

private:
    static int ledFd;
	static int ledState;
};

#endif // LED_H

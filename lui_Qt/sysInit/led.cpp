#include "led.h"

#include <sys/ioctl.h>
#include <fcntl.h>
#include "sysInitDef.h"
#include "pubDef.h"

int  Led::ledFd     = -1000;
int  Led::ledState  = 0;
bool Led::bIsRedLedOn     = false;
bool Led::bIsYellowLedOn  = false;
bool Led::bIsGreenLedOn   = false;

Led::Led(int fd)
{
   // fd = ledFd;
    ledFd = fd;

    bIsRedLedOn     = false;
    bIsYellowLedOn  = false;
    bIsGreenLedOn   = false;

	ledState = 0;
}

void Led::InitLed()
{
	ledState = 0;

	if(ledFd < 0)
	{
		ledFd = open(LED_NAME, O_RDWR);

	}
	if(ledFd < 0)
	{
		TRACELOG2( "Led::InitLed() ledFd < 0" );
		return;
	}
}

void Led::RedLedOn()
{
    if(ledFd == -1000)
    {
        InitLed();
    }

    if(ledFd >= 0)
    {
        ioctl(ledFd,IOCTL_SET_RLED,1);

        bIsRedLedOn = true;

		ledState |= LED_STATE_RED_ON;
    }
}

void Led::RedLedOff()
{
    if(ledFd == -1000)
    {
        InitLed();
    }

    if(ledFd >= 0)
    {
        ioctl(ledFd,IOCTL_SET_RLED,0);

        bIsRedLedOn = false;

		ledState &= ~LED_STATE_RED_ON;
    }
}

void Led::YellowLedOn()
{
    if(ledFd == -1000)
    {
        InitLed();
    }

    if(ledFd >= 0)
    {
        ioctl(ledFd, IOCTL_SET_YLED, 1);

        bIsYellowLedOn = true;

		ledState |= LED_STATE_YELLOW_ON;
    }
}

void Led::YellowLedOff()
{
    if(ledFd == -1000)
    {
        InitLed();
    }

    if(ledFd >= 0)
    {
        ioctl(ledFd, IOCTL_SET_YLED, 0);

        bIsYellowLedOn = false;

		ledState &= ~LED_STATE_YELLOW_ON;
    }
}

void Led::GreenLedOn()
{
    if(ledFd == -1000)
    {
        InitLed();
    }

    if(ledFd >= 0)
    {
        ioctl(ledFd, IOCTL_SET_GLED, 1);

        bIsGreenLedOn = true;

		ledState |= LED_STATE_GREEN_ON;
		ledState &= ~LED_STATE_GREEN_FLASH_ON;
    }
}

void Led::GreenLedOff()
{
    if(ledFd == -1000)
    {
        InitLed();
    }

    if(ledFd >= 0)
    {
        ioctl(ledFd, IOCTL_SET_GLED, 0);

        bIsGreenLedOn = false;

		ledState &= ~LED_STATE_GREEN_ON;
		ledState &= ~LED_STATE_GREEN_FLASH_ON;
    }
}

//bool Led::GetYellowLedState()
//{
//    return bIsYellowLedOn;
//}

//bool Led::GetRedLedState()
//{
//    return bIsRedLedOn;
//}

void Led::GreenLedFlash()
{
    if(ledFd == -1000)
    {
        InitLed();
    }

    if(ledFd >= 0)
    {
        ioctl(ledFd, IOCTL_COR_GLED);

        bIsGreenLedOn = false;
		ledState |= LED_STATE_GREEN_FLASH_ON;
		ledState &= ~LED_STATE_GREEN_ON;
    }
}

int Led::GetLedState()
{
	//TRACELOG1( "led state:<%02X>", ledState);
	return ledState;
}

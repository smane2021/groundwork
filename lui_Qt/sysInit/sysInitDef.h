#ifndef SYSINITDEF_H
#define SYSINITDEF_H

#include "utility.h"
// #include <stdio.h>
// #define TRACELOG1 printf
// #define TRACELOG2 printf

//LED, LCD, buzzer共用一个驱动文件
#define GLOBAL_DRIVE_NAME      "/dev/led"
#define GLOBAL_DRIVE_MAJOR     251
#define DEV_FRAME_BUFFER       "/dev/fb0"

// BUZZ
#define BUZZ_NAME        GLOBAL_DRIVE_NAME
#define BUZZ_MAJOR       GLOBAL_DRIVE_MAJOR
/*长响*/
#define IOCTL_BUZZ _IOW(BUZZ_MAJOR, 7, unsigned long)
/*BEEP响一声*/
#define IOCTL_BEEP _IOW(BUZZ_MAJOR, 5, unsigned long)

// LCD
#define LCD_NAME        GLOBAL_DRIVE_NAME
#define LCD_MAJOR       GLOBAL_DRIVE_MAJOR

//#define IOCTL_BLIGHT	  _IOW(LCD_MAJOR, 5, unsigned long)   /*LCD背光开关控制*/
#define IOCTL_BLIGHT_PWM  _IOW(LCD_MAJOR, 6, unsigned long)   /*LCD背光PWM 控制*/

// LED
#define LED_NAME        GLOBAL_DRIVE_NAME
#define LED_MAJOR       GLOBAL_DRIVE_MAJOR
#define IOCTL_SET_RLED	     _IOW(LED_MAJOR, 1, unsigned long)
#define IOCTL_SET_GLED	     _IOW(LED_MAJOR, 2, unsigned long)
#define IOCTL_SET_YLED	     _IOW(LED_MAJOR, 3, unsigned long)
#define IOCTL_COR_GLED       _IOW(LED_MAJOR, 4, unsigned long)   /*闪烁控制*/

// framebuffer fb0
#define IOCTL_RESET_LCD	     0x5600
#define IOCTL_ACT_LCD			0x5602
#define IOCTL_STOP_LCD			0x5601

#endif


/******************************************************************************
文件名：    buzz.cpp
功能：      蜂鸣器控制
作者：      刘金煌
创建日期：   2013年05月23日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "buzz.h"

#include <sys/ioctl.h>
#include <fcntl.h>
#include "sysInitDef.h"
#include "pubDef.h"

int Buzz::buzzFd = -1000;
bool Buzz::bIsBeeping = false;
int Buzz::iBeepType = BUTTON_BEEP;
bool Buzz::bAlarmVoiceDisabled = false;

int Buzz::iTimerInterval = -1; //-1表示一直响

Buzz::Buzz(int fd)
{
    buzzFd = fd;

   // buzzFd = -1000;
   // InitBuzz();
}

void Buzz::InitBuzz()
{
	if(buzzFd < 0)
    {
        buzzFd = open(BUZZ_NAME, O_RDWR);

    }
    if(buzzFd < 0)
    {
		TRACELOG2( "Buzz::InitBuzz() buzzFd < 0" );
		return;
    }
	
// 	if(ConfigParam::iAlarmVoiceLevel == 0)//常开
//     {
//         Buzz::iTimerInterval = -1;
//     }
//     else if(ConfigParam::iAlarmVoiceLevel == 1)//常闭
//     {
//         Buzz::bAlarmVoiceDisabled = true;
//         Buzz::Quiet();
//     }
//     else if(ConfigParam::iAlarmVoiceLevel == 2)//3分钟
//     {
//         Buzz::iTimerInterval = 3*60;
//     }
//     else if(ConfigParam::iAlarmVoiceLevel == 3)//10分钟
//     {
//         Buzz::iTimerInterval = 10*60;
//     }
//     else if(ConfigParam::iAlarmVoiceLevel == 4)//1小时
//     {
//         Buzz::iTimerInterval = 60*60;
//     }
//     else if(ConfigParam::iAlarmVoiceLevel == 5)//4小时
//     {
//        Buzz::iTimerInterval = 4*60*60;
//     }
}

/*长响*/
void Buzz::Moo()
{
//     if(bAlarmVoiceDisabled)
//     {
//         return;//告警音去掉
//     }
    if(buzzFd == -1000)
    {
        InitBuzz();
    }

    if(buzzFd >= 0)
    {
        ioctl(buzzFd, IOCTL_BUZZ, 3);
        bIsBeeping = true;
    }
}

/*停止*/
void Buzz::Quiet()
{
	if(buzzFd == -1000)
    {
        InitBuzz();
    }

    if(buzzFd >= 0)
    {
        ioctl(buzzFd, IOCTL_BUZZ, 0);
        bIsBeeping = false;
    }
}

/*响一声*/
void Buzz::Beep()
{
//     if(!ConfigParam::bKeyVoiceEnable)
//     {
//         return;
//     }
	
	if(buzzFd == -1000)
    {
        InitBuzz();
    }

    if(buzzFd >= 0)
    {
        ioctl(buzzFd, IOCTL_BEEP, 2);   //1表示20ms， 2表示50ms， 3表示100ms
        bIsBeeping = false;
    }
}

/*设置蜂鸣器叫的来源 0：按键，1：告警，2：全部*/
void Buzz::SetBeepType(int iType)
{  
    if(iType < BUTTON_BEEP || iType > ALL_BEEP)
    {
        return;
    }

    //如果没有正在响的话，设置什么是什么
    if ( !bIsBeeping )
    {
        iBeepType = iType;
    }
    else    //正在响
    {
        if(iBeepType == ALL_BEEP)
        {
            return;
        }
        else if(iBeepType == ALARM_BEEP)
        {
            if(iType != ALARM_BEEP)
            {
                iBeepType = ALL_BEEP;
            }
        }
        else if(iBeepType == BUTTON_BEEP)
        {
            if(iType != BUTTON_BEEP)
            {
                iBeepType = ALL_BEEP;
            }
        }
    }

}

/*返回蜂鸣器叫的来源*/
int  Buzz::GetBeepType()
{
    return iBeepType;
}

/*返回蜂鸣器的状态*/
bool Buzz::GetBuzzState()
{
    return bIsBeeping;
}

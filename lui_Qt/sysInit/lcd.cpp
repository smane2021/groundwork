#include "lcd.h"

#include <sys/ioctl.h>
#include <fcntl.h>
#include "sysInitDef.h"
int LCD::lcdFd = -1000;
bool LCD::bIsBackLightOn = true;
int LCD::iCurrLCDBackLight = LCD_BACK_LIGHT_1;
int LCD::iOldLCDBackLight = LCD_BACK_LIGHT_1;

LCD::LCD(int fd)
{
    lcdFd = fd;
    ioctl(lcdFd, IOCTL_BLIGHT_PWM, LCD_BACK_LIGHT_1);  //0~4 亮度从小到大
    //InitLcd();
}

void LCD::InitLcd()
{
	iOldLCDBackLight  = LCD_BACK_LIGHT_1;
	iCurrLCDBackLight = LCD_BACK_LIGHT_1; //ConfigParam::iLCDBackLight;
    if(lcdFd < 0)
    {
       lcdFd = open(LCD_NAME, O_RDWR);
    }

    if(lcdFd < 0)
    {
        TRACELOG2( "lcd open error!!!" );
        return;
    }

    ioctl(lcdFd, IOCTL_BLIGHT_PWM, iCurrLCDBackLight);
}

void LCD::TurnOffBackLight()
{
    if(lcdFd == -1000)
    {
        InitLcd();
    }

    if(lcdFd >= 0)
    {
        ioctl(lcdFd,IOCTL_BLIGHT_PWM, LCD_OFF_BRIGHTNESS);

        bIsBackLightOn = false;
    }
}

void LCD::TurnOnBackLight()
{
    if(lcdFd == -1000)
    {
        InitLcd();
    }

    if(lcdFd >= 0)
    {
        ioctl(lcdFd, IOCTL_BLIGHT_PWM, iCurrLCDBackLight);

        bIsBackLightOn = true;
    }
}

bool LCD::GetBackLightState()
{
    return bIsBackLightOn;
}

int LCD::GetBackLight()
{
	return iCurrLCDBackLight;
}

void LCD::Adjust_BackLight(int iLevel)
{
    if(iLevel <= LCD_MIN || iLevel > LCD_MAX)
    {
        return;
    }

	if(lcdFd == -1000)
    {
        InitLcd();
    }

	if (iLevel == LCD_MAX)
	{
		iLevel = iOldLCDBackLight;
	}

    if(lcdFd >= 0)
    {
        ioctl(lcdFd, IOCTL_BLIGHT_PWM, iLevel);

        if(iLevel == LCD_OFF_BRIGHTNESS)
        {
            bIsBackLightOn = false;
        }
        else
        {
			iOldLCDBackLight = iLevel;
            bIsBackLightOn = true;
        }
	}

	iCurrLCDBackLight = iLevel;
}

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru">
<context>
    <name>CMenuData</name>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="203"/>
        <source>Settings</source>
        <translation>настройки</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="213"/>
        <source>Maintenance</source>
        <translation>обслуживания</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="221"/>
        <source>Energy Saving</source>
        <translation>Энергосбережение</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="229"/>
        <source>Alarm Settings</source>
        <translation>Установка сигналов аварии</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="237"/>
        <source>Rect Settings</source>
        <translation>Настройки выпрямителя</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="245"/>
        <source>Batt Settings</source>
        <translation>Установка батарей</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="253"/>
        <source>LVD Settings</source>
        <translation>Настройки низкого напряжения постоянного тока</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="261"/>
        <source>AC Settings</source>
        <translation>Настройки переменного тока</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="269"/>
        <source>Sys Settings</source>
        <translation>Настройки переменного тока</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="277"/>
        <source>Comm Settings</source>
        <translation>Настройки связи</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="285"/>
        <source>Other Settings</source>
        <translation>Другие настройки</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="294"/>
        <source>Slave Settings</source>
        <translation>Настройки ведомого</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="305"/>
        <source>Basic Settings</source>
        <translation>Настройки основные</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="313"/>
        <source>Charge</source>
        <translation>Заряд</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="321"/>
        <source>Battery Test</source>
        <translation>Тест батарей</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="329"/>
        <source>Temp Comp</source>
        <translation>Температурная компенсация</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="337"/>
        <source>Batt1 Settings</source>
        <translation>Батарея 1 установки</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="345"/>
        <source>Batt2 Settings</source>
        <translation>Батарея 2 установки</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="354"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="711"/>
        <source>Restore Default</source>
        <translation>Востановить по умолчанию</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="362"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="396"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="717"/>
        <source>Update App</source>
        <translation>обновить приложение</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="371"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="743"/>
        <source>Auto Config</source>
        <translation>Автоматическая настройка</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="379"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="723"/>
        <source>LCD Display Wizard</source>
        <translation>ЖКИ дисплей помощник</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="387"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="737"/>
        <source>Start Wizard Now</source>
        <translation>Пуск помощника</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="712"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="718"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="724"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="738"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="744"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="713"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="719"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="725"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="739"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="745"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="751"/>
        <source>Protocol</source>
        <translation>протокол</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="758"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="780"/>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="763"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="785"/>
        <source>Media</source>
        <translation>Медиа</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="770"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="792"/>
        <source>Baudrate</source>
        <translation>Скорость передачи данных </translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="804"/>
        <source>Date</source>
        <translation>Дата </translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="809"/>
        <source>Time</source>
        <translation>время</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="814"/>
        <source>IP Address</source>
        <translation>IP адрес</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="819"/>
        <source>Mask</source>
        <translation>Маска</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="824"/>
        <source>Gateway</source>
        <translation>шлюза</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="829"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="830"/>
        <source>Disabled</source>
        <translation>Отключить</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="831"/>
        <source>Enabled</source>
        <translation>Включить</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="832"/>
        <source>Error</source>
        <translation>ошибка</translation>
    </message>
</context>
<context>
    <name>CtrlInputChar</name>
    <message>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="291"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="304"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="317"/>
        <source>.</source>
        <translation>.</translation>
    </message>
</context>
<context>
    <name>DlgInfo</name>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="156"/>
        <source>ENT to OK</source>
        <translation>ENT для подтверждения</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="159"/>
        <source>ESC to Cancel</source>
        <translation>ESС для отмены</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="310"/>
        <source>Update OK Num</source>
        <translation>Обновление ОК колличество</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="319"/>
        <source>Open File Failed</source>
        <translation>Ошибка открытия файла</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="323"/>
        <source>Comm Time-Out</source>
        <translation>Связь прервана</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="531"/>
        <source>ENT or ESC to exit</source>
        <translation>ENT или ESС для выхода</translation>
    </message>
</context>
<context>
    <name>DlgUpdateApp</name>
    <message>
        <location filename="../../g3_lui/util/DlgUpdateApp.cpp" line="44"/>
        <source>Qt Exited</source>
        <translation>Qt выход</translation>
    </message>
</context>
<context>
    <name>FirstStackedWdg</name>
    <message>
        <location filename="../../g3_lui/equipWidget/FirstStackedWdg.ui" line="58"/>
        <location filename="../../g3_lui/equipWidget/FirstStackedWdg.cpp" line="30"/>
        <source>Alarm</source>
        <translation>Аварии</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/FirstStackedWdg.cpp" line="166"/>
        <source>Active Alarms</source>
        <translation>Активные аварии</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/FirstStackedWdg.cpp" line="170"/>
        <source>Alarm History</source>
        <translation> История аварий</translation>
    </message>
</context>
<context>
    <name>GuideWindow</name>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="290"/>
        <source>Installation Wizard</source>
        <translation>Установка помощника</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="294"/>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="355"/>
        <source>ENT to continue</source>
        <translation>ENT для продолжения</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="298"/>
        <source>ESC to skip </source>
        <translation>ESC чтобы пропустить</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="345"/>
        <source>OK to exit</source>
        <translation>ОК, чтобы выйти</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="350"/>
        <source>ESC to exit</source>
        <translation>ESC для выхода</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="369"/>
        <source>Wizard finished</source>
        <translation>Помощник завершён</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="521"/>
        <source>Site Name</source>
        <translation>Имя Сайта</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="525"/>
        <source>Battery Settings</source>
        <translation>Установки батареи</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="529"/>
        <source>Capacity Settings</source>
        <translation>Настройки ёмкости</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="533"/>
        <source>ECO Parameter</source>
        <translation>ЭКО параметры</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="537"/>
        <source>Alarm Settings</source>
        <translation>Установка сигналов аварии</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="541"/>
        <source>Common Settings</source>
        <translation>Настройки связи</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="545"/>
        <source>IP address</source>
        <translation>IP адрес</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="563"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="578"/>
        <source>Time</source>
        <translation>время</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="606"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="613"/>
        <source>Disabled</source>
        <translation>Отключить</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="614"/>
        <source>Enabled</source>
        <translation>Включить</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="615"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="635"/>
        <source>IP Address</source>
        <translation>IP адрес</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="639"/>
        <source>MASK</source>
        <translation>Маска</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="643"/>
        <source>Gateway</source>
        <translation>шлюза</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="867"/>
        <source>Rebooting</source>
        <translation>Перезагрузка</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1318"/>
        <source>Set language failed</source>
        <translation>Установка языка не удалась</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1349"/>
        <source>Adjust LCD</source>
        <translation>Регулеровка ЖКИ</translation>
    </message>
</context>
<context>
    <name>LoginWindow</name>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="132"/>
        <source>Select User</source>
        <translation>Выбор пользователя</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="135"/>
        <source>Enter Password</source>
        <translation>Введите пароль</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="349"/>
        <source>Password error</source>
        <translation>Ошибка пароля</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../g3_lui/basicWidget/mainwindow.cpp" line="415"/>
        <source>OK to reboot</source>
        <translation>ОК, чтобы перезагрузить</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="256"/>
        <source>OK to clear</source>
        <translation>ОК, чтобы очистить</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="257"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="311"/>
        <source>Please wait</source>
        <translation>Пожалуйста, ожидайте</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="258"/>
        <source>Set successful</source>
        <translation>Установлено успешно</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="259"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="523"/>
        <source>Set failed</source>
        <translation>Установка не удалась</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="273"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="284"/>
        <source>OK to change</source>
        <translation>ОК, чтобы изменить</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="310"/>
        <source>OK to update</source>
        <translation>ОК, чтобы обновить</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="312"/>
        <source>Update successful</source>
        <translation>обновлении успешно</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="313"/>
        <source>Update failed</source>
        <translation>Обновление не удалось</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="445"/>
        <source>Adjust LCD</source>
        <translation>Регулеровка ЖКИ</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="750"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="1578"/>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="341"/>
        <source>No Data</source>
        <translation>Пользователь определён</translation>
    </message>
</context>
<context>
    <name>Wdg2P5BattRemainTime</name>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="253"/>
        <source>No Data</source>
        <translation>Пользователь определён</translation>
    </message>
</context>
<context>
    <name>Wdg2Table</name>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="472"/>
        <source>Active Alarms</source>
        <translation>Активные аварии</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="478"/>
        <source>Alarm History</source>
        <translation>История аварий</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="640"/>
        <source>Observation</source>
        <translation>обзор</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="644"/>
        <source>Major</source>
        <translation>тяжелый</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="648"/>
        <source>Critical</source>
        <translation>критический</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="843"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="883"/>
        <source>Index</source>
        <translation>Num</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="848"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="892"/>
        <source>Iout(A)</source>
        <translation>Iout(A)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="853"/>
        <source>State</source>
        <translation>состояние</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="888"/>
        <source>Vin(V)</source>
        <translation>Vin(V)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1135"/>
        <source>No Data</source>
        <translation>Нет данных</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1230"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1234"/>
        <source>SN</source>
        <translation>SN</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1238"/>
        <source>Number</source>
        <translation>Num</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1242"/>
        <source>Product Ver</source>
        <translation>Модель программы</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1246"/>
        <source>SW Ver</source>
        <translation>Версия программы</translation>
    </message>
</context>
<context>
    <name>Wdg3Table</name>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="25"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="26"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="667"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="668"/>
        <source>OA Alarm</source>
        <translation>обзор авария</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="27"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="669"/>
        <source>MA Alarm</source>
        <translation>тяжелый авария</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="28"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="670"/>
        <source>CA Alarm</source>
        <translation>Критическая авария</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="527"/>
        <source>No Data</source>
        <translation>Нет данных</translation>
    </message>
</context>
<context>
    <name>WdgFCfgGroup</name>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1301"/>
        <source>OK to restore default</source>
        <translation>ОК, чтобы востановить по умолчанию</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1302"/>
        <source>Restore Default</source>
        <translation>Востановить по умолчанию</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1381"/>
        <source>OK to update app</source>
        <translation>ОК, чтобы обновить приложение</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1401"/>
        <source>Without USB drive</source>
        <translation>Без USB привода</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1410"/>
        <source>USB drive is empty</source>
        <translation>USB привод отсутвует</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1419"/>
        <source>Update is not needed</source>
        <translation>В обновлении не нуждается</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1428"/>
        <source>App program not found</source>
        <translation>Прикладная программа не найдена</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1443"/>
        <source>Without script file</source>
        <translation>Без скрипт файла</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1463"/>
        <source>Start auto config</source>
        <translation>Старт автонастройки</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1464"/>
        <source>Auto Config</source>
        <translation>Автонастройка</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1503"/>
        <source>Rebooting</source>
        <translation>Перезагрузка</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1504"/>
        <source>Restore failed</source>
        <translation>Востановление не удалось</translation>
    </message>
</context>
<context>
    <name>WdgFP0AC</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP0AC.cpp" line="141"/>
        <source>AC</source>
        <translation>переменного тока</translation>
    </message>
</context>
<context>
    <name>WdgFP10Inventory</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="111"/>
        <source>ENT to Inventory</source>
        <translation>Вход в каталогизатор</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="139"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="143"/>
        <source>SN</source>
        <translation>SN</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="151"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="165"/>
        <source>SW Ver</source>
        <translation>Версия программы</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="170"/>
        <source>HW Ver</source>
        <translation>Версия контроллера</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="175"/>
        <source>Config Ver</source>
        <translation>Версия конфигурации</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="180"/>
        <source>File Sys</source>
        <translation>Системные файлы</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="185"/>
        <source>MAC Address</source>
        <translation>MAC адрес</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="198"/>
        <source>DHCP Server IP</source>
        <translation> IP DHCP сервера</translation>
    </message>
</context>
<context>
    <name>WdgFP12ScreenSaverOut</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP12ScreenSaverOut.cpp" line="58"/>
        <source>Sys Used</source>
        <translation>Системный пользователь</translation>
    </message>
</context>
<context>
    <name>WdgFP1Module</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP1Module.cpp" line="107"/>
        <source>Module</source>
        <translation>модуль</translation>
    </message>
</context>
<context>
    <name>WdgFP3DCA</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="116"/>
        <source>Last 7 days</source>
        <translation>Последние 7 дней</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="164"/>
        <source>Total Load</source>
        <translation>Полная нагрузка</translation>
    </message>
</context>
<context>
    <name>WdgFP5Deg2Curve</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="105"/>
        <source>Last 7 days</source>
        <translation>Последние 7 дней</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="106"/>
        <source>Ambient Temp</source>
        <translation>Температура окружающей среды</translation>
    </message>
</context>
<context>
    <name>WdgFP8BattDegCurve</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="107"/>
        <source>Last 7 days</source>
        <translation>Последние 7 дней</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="108"/>
        <source>Temp Comp</source>
        <translation>Температурная компенсация</translation>
    </message>
</context>
</TS>

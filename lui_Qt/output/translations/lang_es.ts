<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>CMenuData</name>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="203"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="213"/>
        <source>Maintenance</source>
        <translation>Mantenimiento</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="221"/>
        <source>Energy Saving</source>
        <translation>Ahorro Energía</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="229"/>
        <source>Alarm Settings</source>
        <translation>Ajuste de Alarmas</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="237"/>
        <source>Rect Settings</source>
        <translation>Ajustes Rec</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="245"/>
        <source>Batt Settings</source>
        <translation>Ajustes Bat</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="253"/>
        <source>LVD Settings</source>
        <translation>Ajustes LVD</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="261"/>
        <source>AC Settings</source>
        <translation>Ajustes CA</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="269"/>
        <source>Sys Settings</source>
        <translation>Ajustes Sist</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="277"/>
        <source>Comm Settings</source>
        <translation>Comunicaciones</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="285"/>
        <source>Other Settings</source>
        <translation>Otros Ajustes</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="294"/>
        <source>Slave Settings</source>
        <translation>Ajustes Escalvo</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="305"/>
        <source>Basic Settings</source>
        <translation>Ajustes Básicos</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="313"/>
        <source>Charge</source>
        <translation>Carga</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="321"/>
        <source>Battery Test</source>
        <translation>Prueba Baterías</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="329"/>
        <source>Temp Comp</source>
        <translation>CompTemp</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="337"/>
        <source>Batt1 Settings</source>
        <translation>Ajustes Bat1</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="345"/>
        <source>Batt2 Settings</source>
        <translation>Ajustes Bat2</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="354"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="713"/>
        <source>Restore Default</source>
        <translation>Restaurar</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="362"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="396"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="719"/>
        <source>Update App</source>
        <translation>actualizar app</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="371"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="745"/>
        <source>Auto Config</source>
        <translation>Autoconfig</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="379"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="725"/>
        <source>LCD Display Wizard</source>
        <translation>Asistente LCD</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="387"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="739"/>
        <source>Start Wizard Now</source>
        <translation>Iniciar Asistente ahora</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="714"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="720"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="726"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="740"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="746"/>
        <source>Yes</source>
        <translation>Sí</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="715"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="721"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="727"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="741"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="747"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="753"/>
        <source>Protocol</source>
        <translation>Protocolo</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="761"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="783"/>
        <source>Address</source>
        <translation>Dirección</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="766"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="788"/>
        <source>Media</source>
        <translation>Medio</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="773"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="795"/>
        <source>Baudrate</source>
        <translation>Velocidad</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="807"/>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="812"/>
        <source>Time</source>
        <translation>Hora</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="818"/>
        <source>IP Address</source>
        <translation>Dirección IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="823"/>
        <source>Mask</source>
        <translation>Máscara</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="828"/>
        <source>Gateway</source>
        <translation>Gateway</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="833"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="834"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="867"/>
        <source>Disabled</source>
        <translation>Deshabilitado</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="835"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="868"/>
        <source>Enabled</source>
        <translation>Habilitado</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="836"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="869"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="841"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="846"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="873"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="878"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="883"/>
        <source>IPV6 IP</source>
        <translation>IP IPV6</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="851"/>
        <source>IPV6 Prefix</source>
        <translation>Prefijo IPV6</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="856"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="861"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="888"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="893"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="898"/>
        <source>IPV6 Gateway</source>
        <translation>Puerta de enlace IPV6</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="866"/>
        <source>IPV6 DHCP</source>
        <translation>DHCP IPV6</translation>
    </message>
</context>
<context>
    <name>CtrlInputChar</name>
    <message>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="291"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="304"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="317"/>
        <source>.</source>
        <translation>.</translation>
    </message>
</context>
<context>
    <name>DlgInfo</name>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="157"/>
        <source>ENT to OK</source>
        <translation>ENT Aceptar</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="159"/>
        <source>ESC to Cancel</source>
        <translation>ESC Cancelar</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="310"/>
        <source>Update OK Num</source>
        <translation>Actualiz Bien</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="319"/>
        <source>Open File Failed</source>
        <translation>Fallo al abrir archivo</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="323"/>
        <source>Comm Time-Out</source>
        <translation>Tiempo agotado COM</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="540"/>
        <source>ENT or ESC to exit</source>
        <translation>ENT o ESC para salir</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="574"/>
        <source>Rebooting</source>
        <translation>Reiniciando</translation>
    </message>
</context>
<context>
    <name>DlgUpdateApp</name>
    <message>
        <location filename="../../g3_lui/util/DlgUpdateApp.cpp" line="44"/>
        <source>Qt Exited</source>
        <translation>Saliendo Qt</translation>
    </message>
</context>
<context>
    <name>FirstStackedWdg</name>
    <message>
        <location filename="../../g3_lui/equipWidget/FirstStackedWdg.ui" line="58"/>
        <source>Alarm</source>
        <translation>Alarma</translation>
    </message>
    <message>
        <source>Active Alarms</source>
        <translation type="obsolete">Alarmas Activas</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">Histórico Alarmas</translation>
    </message>
</context>
<context>
    <name>GuideWindow</name>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="279"/>
        <source>Installation Wizard</source>
        <translation>Asistente Instalación</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="283"/>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="347"/>
        <source>ENT to continue</source>
        <translation>ENT Continuar</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="287"/>
        <source>ESC to skip </source>
        <translation>ESC para saltar</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="339"/>
        <source>OK to exit</source>
        <translation>OK Salir</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="343"/>
        <source>ESC to exit</source>
        <translation>ENT o ESC para salir</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="362"/>
        <source>Wizard finished</source>
        <translation>El Asistente ha finalizado</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="528"/>
        <source>Site Name</source>
        <translation>Nombre Central</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="534"/>
        <source>Battery Settings</source>
        <translation>Ajustes Batería</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="540"/>
        <source>Capacity Settings</source>
        <translation>Ajustes Capacidad</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="546"/>
        <source>ECO Parameter</source>
        <translation>Parámetro ECO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="552"/>
        <source>Alarm Settings</source>
        <translation>Ajuste de Alarmas</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="558"/>
        <source>Common Settings</source>
        <translation>Configuración Común</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="564"/>
        <source>IP address</source>
        <translation>Dirección IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="585"/>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="600"/>
        <source>Time</source>
        <translation>Hora</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="628"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="634"/>
        <source>Disabled</source>
        <translation>Deshabilitado</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="635"/>
        <source>Enabled</source>
        <translation>Habilitado</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="636"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="656"/>
        <source>IP Address</source>
        <translation>Dirección IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="660"/>
        <source>MASK</source>
        <translation>Máscara</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="664"/>
        <source>Gateway</source>
        <translation>Gateway</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="884"/>
        <source>Rebooting</source>
        <translation>Reiniciando</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1346"/>
        <source>Set language failed</source>
        <translation>Fallo ajuste idioma</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1377"/>
        <source>Adjust LCD</source>
        <translation>Ajustes LCD</translation>
    </message>
</context>
<context>
    <name>LoginWindow</name>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="127"/>
        <source>Select User</source>
        <translation>Seleccione Usuario</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="130"/>
        <source>Enter Password</source>
        <translation>Entre contraseña</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="341"/>
        <source>Password error</source>
        <translation>Error Contraseña</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../g3_lui/basicWidget/mainwindow.cpp" line="376"/>
        <source>OK to reboot</source>
        <translation>OK para reiniciar</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="325"/>
        <source>OK to clear</source>
        <translation>OK para borrar</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="326"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="380"/>
        <source>Please wait</source>
        <translation>Por favor, espere</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="327"/>
        <source>Set successful</source>
        <translation>Ajuste correcto</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="328"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="592"/>
        <source>Set failed</source>
        <translation>Fallo ajuste</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="342"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="353"/>
        <source>OK to change</source>
        <translation>OK para cambiar</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="379"/>
        <source>OK to update</source>
        <translation>OK para actualizar</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="381"/>
        <source>Update successful</source>
        <translation>Actualizar Correcto</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="382"/>
        <source>Update failed</source>
        <translation>Falló actualización</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="514"/>
        <source>Adjust LCD</source>
        <translation>Ajustes LCD</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="918"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="1610"/>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="353"/>
        <source>No Data</source>
        <translation>No hay datos</translation>
    </message>
</context>
<context>
    <name>SecondStackedWdg</name>
    <message>
        <source>Please wait</source>
        <translation type="obsolete">Por favor, espere</translation>
    </message>
</context>
<context>
    <name>Wdg2P5BattRemainTime</name>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="260"/>
        <source>No Data</source>
        <translation>No hay datos</translation>
    </message>
</context>
<context>
    <name>Wdg2Table</name>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="462"/>
        <source>Active Alarms</source>
        <translation>Alarmas Activas</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="468"/>
        <source>Alarm History</source>
        <translation>Histórico Alarmas</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="632"/>
        <source>Observation</source>
        <translation>Observación</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="637"/>
        <source>Major</source>
        <translation>Urgente</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="642"/>
        <source>Critical</source>
        <translation>Crítica</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="843"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="888"/>
        <source>Index</source>
        <translation>Num</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="849"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="899"/>
        <source>Iout(A)</source>
        <translation>Iout(A)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="855"/>
        <source>State</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="894"/>
        <source>Vin(V)</source>
        <translation>Vin(V)</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation type="obsolete">No hay datos</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Nombre</translation>
    </message>
    <message>
        <source>SN</source>
        <translation type="obsolete">SN</translation>
    </message>
    <message>
        <source>Number</source>
        <translation type="obsolete">Num</translation>
    </message>
    <message>
        <source>Product Ver</source>
        <translation type="obsolete">Ver Producto</translation>
    </message>
    <message>
        <source>SW Ver</source>
        <translation type="obsolete">Versión SW</translation>
    </message>
</context>
<context>
    <name>Wdg3Table</name>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="25"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="26"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="550"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="551"/>
        <source>OA Alarm</source>
        <translation>Alarma O1</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="27"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="552"/>
        <source>MA Alarm</source>
        <translation>Alarma A2</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="28"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="553"/>
        <source>CA Alarm</source>
        <translation>Alarma A1</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="448"/>
        <source>No Data</source>
        <translation>No hay datos</translation>
    </message>
</context>
<context>
    <name>WdgAlmMenu</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="58"/>
        <source>Alarm</source>
        <translation>Alarma</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="67"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="97"/>
        <source>Active Alarms</source>
        <translation>Alarmas Activas</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="74"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="100"/>
        <source>Alarm History</source>
        <translation>Histórico Alarmas</translation>
    </message>
</context>
<context>
    <name>WdgFCfgGroup</name>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1735"/>
        <source>OK to restore default</source>
        <translation>OK para ajustes fábrica</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1736"/>
        <source>Restore Default</source>
        <translation>Restaurar valores de fábrica</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1818"/>
        <source>OK to update app</source>
        <translation>OK para actualizar app</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1838"/>
        <source>Without USB drive</source>
        <translation>Sin memoria USB</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1847"/>
        <source>USB drive is empty</source>
        <translation>Disco USB vacío</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1856"/>
        <source>Update is not needed</source>
        <translation>No se necesita actualizar</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1865"/>
        <source>App program not found</source>
        <translation>Programa app no encontrado</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1880"/>
        <source>Without script file</source>
        <translation>Sin archivo script</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1900"/>
        <source>Start auto config</source>
        <translation>Iniciar Autoconfig</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1901"/>
        <source>Auto Config</source>
        <translation>Autoconfig	</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1940"/>
        <source>Rebooting</source>
        <translation>Reiniciando</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1941"/>
        <source>Restore failed</source>
        <translation>Fallo Restaurar</translation>
    </message>
</context>
<context>
    <name>WdgFP0AC</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP0AC.cpp" line="158"/>
        <source>AC</source>
        <translation>CA</translation>
    </message>
</context>
<context>
    <name>WdgFP10Inventory</name>
    <message>
        <source>ENT to Inventory</source>
        <translation type="obsolete">ENT para Inventario</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="91"/>
        <source>ENT to inventory</source>
        <translation>ENT para Inventario</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="146"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="151"/>
        <source>SN</source>
        <translation>SN</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="160"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="175"/>
        <source>SW Ver</source>
        <translation>Ver SW</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="181"/>
        <source>HW Ver</source>
        <translation>Ver HW</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="187"/>
        <source>Config Ver</source>
        <translation>Ver Config</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="212"/>
        <source>File Sys</source>
        <translation>File Sys</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="218"/>
        <source>MAC Address</source>
        <translation>Dirección MAC</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="233"/>
        <source>DHCP Server IP</source>
        <translation>IP serv DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="257"/>
        <source>Link-Local Addr</source>
        <translation>Dirección enlace local</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="279"/>
        <source>Global Addr</source>
        <translation>Dir Global</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="301"/>
        <source>DHCP Server IPV6</source>
        <translation>Servidor DHCP IPV6</translation>
    </message>
</context>
<context>
    <name>WdgFP12ScreenSaverOut</name>
    <message>
        <source>Sys Used</source>
        <translation type="obsolete">Sys usado</translation>
    </message>
</context>
<context>
    <name>WdgFP1Module</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP1Module.cpp" line="111"/>
        <source>Module</source>
        <translation>Módulo</translation>
    </message>
</context>
<context>
    <name>WdgFP3DCA</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="109"/>
        <source>Last 7 days</source>
        <translation>Ultimos 7 días</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="160"/>
        <source>Total Load</source>
        <translation>Carga Total</translation>
    </message>
</context>
<context>
    <name>WdgFP5Deg2Curve</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="110"/>
        <source>Last 7 days</source>
        <translation>Ultimos 7 días</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="111"/>
        <source>Ambient Temp</source>
        <translation>Temp Amb</translation>
    </message>
</context>
<context>
    <name>WdgFP6BattInfo</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP6BattInfo.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>WdgFP8BattDegCurve</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="108"/>
        <source>Last 7 days</source>
        <translation>Ultimos 7 días</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="109"/>
        <source>Temp Comp</source>
        <translation>Comp Temp</translation>
    </message>
</context>
<context>
    <name>WdgInventory</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="317"/>
        <source>No Data</source>
        <translation>No hay datos</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="396"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="401"/>
        <source>SN</source>
        <translation>SN</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="406"/>
        <source>Number</source>
        <translation>Num</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="411"/>
        <source>Product Ver</source>
        <translation>Ver Producto</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="416"/>
        <source>SW Ver</source>
        <translation>Ver SW</translation>
    </message>
</context>
</TS>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh_CN">
<context>
    <name>CMenuData</name>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="203"/>
        <source>Settings</source>
        <translation>参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="213"/>
        <source>Maintenance</source>
        <translation>控制输出</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="221"/>
        <source>Energy Saving</source>
        <translation>节能参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="229"/>
        <source>Alarm Settings</source>
        <translation>告警参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="237"/>
        <source>Rect Settings</source>
        <translation>模块参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="245"/>
        <source>Batt Settings</source>
        <translation>电池参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="253"/>
        <source>LVD Settings</source>
        <translation>LVD参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="261"/>
        <source>AC Settings</source>
        <translation>交流参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="269"/>
        <source>Sys Settings</source>
        <translation>系统参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="277"/>
        <source>Comm Settings</source>
        <translation>通信参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="285"/>
        <source>Other Settings</source>
        <translation>其他参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="294"/>
        <source>Slave Settings</source>
        <translation>从机参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="305"/>
        <source>Basic Settings</source>
        <translation>基本参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="313"/>
        <source>Charge</source>
        <translation>充电管理</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="321"/>
        <source>Battery Test</source>
        <translation>电池测试</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="329"/>
        <source>Temp Comp</source>
        <translation>温补参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="337"/>
        <source>Batt1 Settings</source>
        <translation>电池1参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="345"/>
        <source>Batt2 Settings</source>
        <translation>电池2参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="354"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="713"/>
        <source>Restore Default</source>
        <translation>恢复默认配置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="362"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="396"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="719"/>
        <source>Update App</source>
        <translation>升级应用程序</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="371"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="745"/>
        <source>Auto Config</source>
        <translation>自动配置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="379"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="725"/>
        <source>LCD Display Wizard</source>
        <translation>LCD显示向导</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="387"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="739"/>
        <source>Start Wizard Now</source>
        <translation>启动向导</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="714"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="720"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="726"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="740"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="746"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="715"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="721"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="727"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="741"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="747"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="753"/>
        <source>Protocol</source>
        <translation>协议</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="761"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="783"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="766"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="788"/>
        <source>Media</source>
        <translation>媒质</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="773"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="795"/>
        <source>Baudrate</source>
        <translation>波特率</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="807"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="812"/>
        <source>Time</source>
        <translation>时间</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="818"/>
        <source>IP Address</source>
        <translation>IP地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="823"/>
        <source>Mask</source>
        <translation>子网掩码</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="828"/>
        <source>Gateway</source>
        <translation>默认网关</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="833"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="834"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="867"/>
        <source>Disabled</source>
        <translation>禁止</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="835"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="868"/>
        <source>Enabled</source>
        <translation>开启</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="836"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="869"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="841"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="846"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="873"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="878"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="883"/>
        <source>IPV6 IP</source>
        <translation>IPV6地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="851"/>
        <source>IPV6 Prefix</source>
        <translation>IPV6前缀</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="856"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="861"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="888"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="893"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="898"/>
        <source>IPV6 Gateway</source>
        <translation>IPV6网关</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="866"/>
        <source>IPV6 DHCP</source>
        <translation>IPV6 DHCP</translation>
    </message>
</context>
<context>
    <name>CtrlInputChar</name>
    <message>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="291"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="304"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="317"/>
        <source>.</source>
        <translation>.</translation>
    </message>
</context>
<context>
    <name>DlgInfo</name>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="157"/>
        <source>ENT to OK</source>
        <translation>按ENT确定</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="159"/>
        <source>ESC to Cancel</source>
        <translation>按ESC取消</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="310"/>
        <source>Update OK Num</source>
        <translation>升级成功数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="319"/>
        <source>Open File Failed</source>
        <translation>打开文件失败</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="323"/>
        <source>Comm Time-Out</source>
        <translation>通信超时</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="540"/>
        <source>ENT or ESC to exit</source>
        <translation>按ENT或ESC退出</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="574"/>
        <source>Rebooting</source>
        <translation>重启</translation>
    </message>
</context>
<context>
    <name>DlgUpdateApp</name>
    <message>
        <location filename="../../g3_lui/util/DlgUpdateApp.cpp" line="44"/>
        <source>Qt Exited</source>
        <translation>Qt退出</translation>
    </message>
</context>
<context>
    <name>FirstStackedWdg</name>
    <message>
        <location filename="../../g3_lui/equipWidget/FirstStackedWdg.ui" line="58"/>
        <source>Alarm</source>
        <translation>告警</translation>
    </message>
    <message>
        <source>Active Alarms</source>
        <translation type="obsolete">当前告警</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">历史告警</translation>
    </message>
</context>
<context>
    <name>GuideWindow</name>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="279"/>
        <source>Installation Wizard</source>
        <translation>安装向导</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="283"/>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="347"/>
        <source>ENT to continue</source>
        <translation>按ENT继续</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="287"/>
        <source>ESC to skip </source>
        <translation>按ESC跳过</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="339"/>
        <source>OK to exit</source>
        <translation>是否退出</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="343"/>
        <source>ESC to exit</source>
        <translation>按ESC退出</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="362"/>
        <source>Wizard finished</source>
        <translation>向导结束</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="528"/>
        <source>Site Name</source>
        <translation>站名</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="534"/>
        <source>Battery Settings</source>
        <translation>电池设置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="540"/>
        <source>Capacity Settings</source>
        <translation>容量设置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="546"/>
        <source>ECO Parameter</source>
        <translation>节能参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="552"/>
        <source>Alarm Settings</source>
        <translation>告警设置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="558"/>
        <source>Common Settings</source>
        <translation>通用设置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="564"/>
        <source>IP address</source>
        <translation>IP地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="585"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="600"/>
        <source>Time</source>
        <translation>时间</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="628"/>
        <source>DHCP</source>
        <translation>DHCP功能</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="634"/>
        <source>Disabled</source>
        <translation>禁止</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="635"/>
        <source>Enabled</source>
        <translation>开启</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="636"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="656"/>
        <source>IP Address</source>
        <translation>IP地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="660"/>
        <source>MASK</source>
        <translation>子网掩码</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="664"/>
        <source>Gateway</source>
        <translation>网关</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="884"/>
        <source>Rebooting</source>
        <translation>重启</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1346"/>
        <source>Set language failed</source>
        <translation>设置语言失败</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1377"/>
        <source>Adjust LCD</source>
        <translation>LCD自适应</translation>
    </message>
</context>
<context>
    <name>LoginWindow</name>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="127"/>
        <source>Select User</source>
        <translation>选择用户</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="130"/>
        <source>Enter Password</source>
        <translation>输入密码</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="341"/>
        <source>Password error</source>
        <translation>密码错误</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../g3_lui/basicWidget/mainwindow.cpp" line="376"/>
        <source>OK to reboot</source>
        <translation>确定重启吗</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="325"/>
        <source>OK to clear</source>
        <translation>确定清除吗</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="326"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="380"/>
        <source>Please wait</source>
        <translation>请等待</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="327"/>
        <source>Set successful</source>
        <translation>设置成功</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="328"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="592"/>
        <source>Set failed</source>
        <translation>设置失败</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="342"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="353"/>
        <source>OK to change</source>
        <translation>确定更改吗</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="379"/>
        <source>OK to update</source>
        <translation>确定升级吗</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="381"/>
        <source>Update successful</source>
        <translation>升级成功</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="382"/>
        <source>Update failed</source>
        <translation>升级失败</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="514"/>
        <source>Adjust LCD</source>
        <translation>LCD自适应</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="918"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="1610"/>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="353"/>
        <source>No Data</source>
        <translation>没有数据</translation>
    </message>
</context>
<context>
    <name>SecondStackedWdg</name>
    <message>
        <source>Please wait</source>
        <translation type="obsolete">请等待</translation>
    </message>
</context>
<context>
    <name>Wdg2P5BattRemainTime</name>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="260"/>
        <source>No Data</source>
        <translation>没有数据</translation>
    </message>
</context>
<context>
    <name>Wdg2Table</name>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="462"/>
        <source>Active Alarms</source>
        <translation>当前告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="468"/>
        <source>Alarm History</source>
        <translation>历史告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="632"/>
        <source>Observation</source>
        <translation>一般告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="637"/>
        <source>Major</source>
        <translation>重要告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="642"/>
        <source>Critical</source>
        <translation>紧急告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="843"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="888"/>
        <source>Index</source>
        <translation>序号</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="849"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="899"/>
        <source>Iout(A)</source>
        <translation>Iout(A)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="855"/>
        <source>State</source>
        <translation>状态</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="894"/>
        <source>Vin(V)</source>
        <translation>Vin(V)</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation type="obsolete">无数据</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">名称</translation>
    </message>
    <message>
        <source>SN</source>
        <translation type="obsolete">SN</translation>
    </message>
    <message>
        <source>Number</source>
        <translation type="obsolete">编码</translation>
    </message>
    <message>
        <source>Product Ver</source>
        <translation type="obsolete">产品版本</translation>
    </message>
    <message>
        <source>SW Ver</source>
        <translation type="obsolete">软件版本</translation>
    </message>
</context>
<context>
    <name>Wdg3Table</name>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="25"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="26"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="550"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="551"/>
        <source>OA Alarm</source>
        <translation>一般告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="27"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="552"/>
        <source>MA Alarm</source>
        <translation>重要告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="28"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="553"/>
        <source>CA Alarm</source>
        <translation>紧急告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="448"/>
        <source>No Data</source>
        <translation>没有数据</translation>
    </message>
</context>
<context>
    <name>WdgAlmMenu</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="58"/>
        <source>Alarm</source>
        <translation>告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="67"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="97"/>
        <source>Active Alarms</source>
        <translation>当前告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="74"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="100"/>
        <source>Alarm History</source>
        <translation>历史告警</translation>
    </message>
</context>
<context>
    <name>WdgFCfgGroup</name>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1735"/>
        <source>OK to restore default</source>
        <translation>恢复默认配置吗</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1736"/>
        <source>Restore Default</source>
        <translation>恢复默认配置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1818"/>
        <source>OK to update app</source>
        <translation>确定升级应用程序吗</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1838"/>
        <source>Without USB drive</source>
        <translation>没有U盘</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1847"/>
        <source>USB drive is empty</source>
        <translation>U盘是空的</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1856"/>
        <source>Update is not needed</source>
        <translation>不需要升级</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1865"/>
        <source>App program not found</source>
        <translation>App程序不存在</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1880"/>
        <source>Without script file</source>
        <translation>没有脚本文件</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1900"/>
        <source>Start auto config</source>
        <translation>确认开始自动配置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1901"/>
        <source>Auto Config</source>
        <translation>自动配置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1940"/>
        <source>Rebooting</source>
        <translation>重启</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1941"/>
        <source>Restore failed</source>
        <translation>恢复失败</translation>
    </message>
</context>
<context>
    <name>WdgFP0AC</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP0AC.cpp" line="158"/>
        <source>AC</source>
        <translation>交流</translation>
    </message>
</context>
<context>
    <name>WdgFP10Inventory</name>
    <message>
        <source>ENT to Inventory</source>
        <translation type="obsolete">ENT进入产品信息</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="91"/>
        <source>ENT to inventory</source>
        <translation>按ENT查看产品信息</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="146"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="151"/>
        <source>SN</source>
        <translation>SN</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="160"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="175"/>
        <source>SW Ver</source>
        <translation>软件版本</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="181"/>
        <source>HW Ver</source>
        <translation>硬件版本</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="187"/>
        <source>Config Ver</source>
        <translation>配置版本</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="212"/>
        <source>File Sys</source>
        <translation>文件系统</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="218"/>
        <source>MAC Address</source>
        <translation>MAC地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="233"/>
        <source>DHCP Server IP</source>
        <translation>DHCP服务器</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="257"/>
        <source>Link-Local Addr</source>
        <translation>本地链接地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="279"/>
        <source>Global Addr</source>
        <translation>IPV6地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="301"/>
        <source>DHCP Server IPV6</source>
        <translation>IPV6服务器</translation>
    </message>
</context>
<context>
    <name>WdgFP12ScreenSaverOut</name>
    <message>
        <source>Sys Used</source>
        <translation type="obsolete">系统使用</translation>
    </message>
</context>
<context>
    <name>WdgFP1Module</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP1Module.cpp" line="111"/>
        <source>Module</source>
        <translation>模块</translation>
    </message>
</context>
<context>
    <name>WdgFP3DCA</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="109"/>
        <source>Last 7 days</source>
        <translation>最近7天</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="160"/>
        <source>Total Load</source>
        <translation>总负载</translation>
    </message>
</context>
<context>
    <name>WdgFP5Deg2Curve</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="110"/>
        <source>Last 7 days</source>
        <translation>最近7天</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="111"/>
        <source>Ambient Temp</source>
        <translation>环境温度</translation>
    </message>
</context>
<context>
    <name>WdgFP6BattInfo</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP6BattInfo.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>WdgFP8BattDegCurve</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="108"/>
        <source>Last 7 days</source>
        <translation>最近7天</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="109"/>
        <source>Temp Comp</source>
        <translation>温补温度</translation>
    </message>
</context>
<context>
    <name>WdgInventory</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="317"/>
        <source>No Data</source>
        <translation>无数据</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="396"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="401"/>
        <source>SN</source>
        <translation>序列号</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="406"/>
        <source>Number</source>
        <translation>编码</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="411"/>
        <source>Product Ver</source>
        <translation>产品版本</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="416"/>
        <source>SW Ver</source>
        <translation>软件版本</translation>
    </message>
</context>
</TS>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="it_IT">
<context>
    <name>CMenuData</name>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="203"/>
        <source>Settings</source>
        <translation>Settaggi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="213"/>
        <source>Maintenance</source>
        <translation>Mantenimento</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="221"/>
        <source>Energy Saving</source>
        <translation>Abilita risparmio energia</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="229"/>
        <source>Alarm Settings</source>
        <translation>Ajustes Allarm</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="237"/>
        <source>Rect Settings</source>
        <translation>Ajustes RD</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="245"/>
        <source>Batt Settings</source>
        <translation>Settaggi batt</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="253"/>
        <source>LVD Settings</source>
        <translation>Ajustes LVD</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="261"/>
        <source>AC Settings</source>
        <translation>Ajustes CA</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="269"/>
        <source>Sys Settings</source>
        <translation>Ajustes Sist</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="277"/>
        <source>Comm Settings</source>
        <translation>Ajustes Com</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="285"/>
        <source>Other Settings</source>
        <translation>Altre Ajustes</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="294"/>
        <source>Slave Settings</source>
        <translation>Ajustes Slave</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="305"/>
        <source>Basic Settings</source>
        <translation>Ajustes Base</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="313"/>
        <source>Charge</source>
        <translation>Carica</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="321"/>
        <source>Battery Test</source>
        <translation>Test Batteria</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="329"/>
        <source>Temp Comp</source>
        <translation>Comp Temp</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="337"/>
        <source>Batt1 Settings</source>
        <translation>Ajustes Batt1</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="345"/>
        <source>Batt2 Settings</source>
        <translation>Ajustes Batt2</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="354"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="713"/>
        <source>Restore Default</source>
        <translation>Ripristina Ajustes</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="362"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="396"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="719"/>
        <source>Update App</source>
        <translation>Aggiornamento applicazione</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="371"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="745"/>
        <source>Auto Config</source>
        <translation>Auto configurazione</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="379"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="725"/>
        <source>LCD Display Wizard</source>
        <translation>Wizard Display LCD</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="387"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="739"/>
        <source>Start Wizard Now</source>
        <translation>Inizia Wizard ora</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="714"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="720"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="726"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="740"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="746"/>
        <source>Yes</source>
        <translation>Sì</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="715"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="721"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="727"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="741"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="747"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="753"/>
        <source>Protocol</source>
        <translation>Protocollo</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="761"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="783"/>
        <source>Address</source>
        <translation>Indirizzo</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="766"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="788"/>
        <source>Media</source>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="773"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="795"/>
        <source>Baudrate</source>
        <translation>Connessione</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="807"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="812"/>
        <source>Time</source>
        <translation>Ora</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="818"/>
        <source>IP Address</source>
        <translation>Indirizzo IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="823"/>
        <source>Mask</source>
        <translation>MASK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="828"/>
        <source>Gateway</source>
        <translation>Gateway</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="833"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="834"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="867"/>
        <source>Disabled</source>
        <translation>Disabilitato</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="835"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="868"/>
        <source>Enabled</source>
        <translation>Abilitato</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="836"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="869"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="841"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="846"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="873"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="878"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="883"/>
        <source>IPV6 IP</source>
        <translation>IPV6 IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="851"/>
        <source>IPV6 Prefix</source>
        <translation>IPV6 Prefisso</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="856"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="861"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="888"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="893"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="898"/>
        <source>IPV6 Gateway</source>
        <translation>IPV6 Gateway</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="866"/>
        <source>IPV6 DHCP</source>
        <translation>IPV6 DHCP</translation>
    </message>
</context>
<context>
    <name>CtrlInputChar</name>
    <message>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="291"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="304"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="317"/>
        <source>.</source>
        <translation>.</translation>
    </message>
</context>
<context>
    <name>DlgInfo</name>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="157"/>
        <source>ENT to OK</source>
        <translation>ENT per OK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="159"/>
        <source>ESC to Cancel</source>
        <translation>ENT per Cancellare</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="310"/>
        <source>Update OK Num</source>
        <translation>Aggiorn Num OK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="319"/>
        <source>Open File Failed</source>
        <translation>Apertura File Fallito</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="323"/>
        <source>Comm Time-Out</source>
        <translation>Tempo Scaduto Com</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="540"/>
        <source>ENT or ESC to exit</source>
        <translation>ENT o ESC per uscire</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="574"/>
        <source>Rebooting</source>
        <translation>Riavvio</translation>
    </message>
</context>
<context>
    <name>DlgUpdateApp</name>
    <message>
        <location filename="../../g3_lui/util/DlgUpdateApp.cpp" line="44"/>
        <source>Qt Exited</source>
        <translation>Uscita applicazione</translation>
    </message>
</context>
<context>
    <name>FirstStackedWdg</name>
    <message>
        <location filename="../../g3_lui/equipWidget/FirstStackedWdg.ui" line="58"/>
        <source>Alarm</source>
        <translation>Allarme</translation>
    </message>
    <message>
        <source>Active Alarms</source>
        <translation type="obsolete">Allarmi attivi</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">Storico allarmi</translation>
    </message>
</context>
<context>
    <name>GuideWindow</name>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="279"/>
        <source>Installation Wizard</source>
        <translation>Wizard installazione</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="283"/>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="347"/>
        <source>ENT to continue</source>
        <translation>ENT per continuare</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="287"/>
        <source>ESC to skip </source>
        <translation>SC per saltare</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="339"/>
        <source>OK to exit</source>
        <translation>OK per Uscire</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="343"/>
        <source>ESC to exit</source>
        <translation>ESC per uscire</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="362"/>
        <source>Wizard finished</source>
        <translation>Wizard concluso</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="528"/>
        <source>Site Name</source>
        <translation>Nome del sito</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="534"/>
        <source>Battery Settings</source>
        <translation>Settaggi batteria</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="540"/>
        <source>Capacity Settings</source>
        <translation>Ajustes Capacità</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="546"/>
        <source>ECO Parameter</source>
        <translation>Parametri ECO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="552"/>
        <source>Alarm Settings</source>
        <translation>Impostazione allarme</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="558"/>
        <source>Common Settings</source>
        <translation>Settaggi comuni</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="564"/>
        <source>IP address</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="585"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="600"/>
        <source>Time</source>
        <translation>Ora</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="628"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="634"/>
        <source>Disabled</source>
        <translation>Disabilitato</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="635"/>
        <source>Enabled</source>
        <translation>Abilitato</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="636"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="656"/>
        <source>IP Address</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="660"/>
        <source>MASK</source>
        <translation>MASK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="664"/>
        <source>Gateway</source>
        <translation>Gateway</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="884"/>
        <source>Rebooting</source>
        <translation>Riavvio</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1346"/>
        <source>Set language failed</source>
        <translation>Impostazione lingua fallita</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1377"/>
        <source>Adjust LCD</source>
        <translation>Contrasto LCD</translation>
    </message>
</context>
<context>
    <name>LoginWindow</name>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="127"/>
        <source>Select User</source>
        <translation>Selezionare Utente</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="130"/>
        <source>Enter Password</source>
        <translation>Inserire Password</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="341"/>
        <source>Password error</source>
        <translation>Errore Password</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../g3_lui/basicWidget/mainwindow.cpp" line="376"/>
        <source>OK to reboot</source>
        <translation>OK per riavvio</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="325"/>
        <source>OK to clear</source>
        <translation>OK per cancellare</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="326"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="380"/>
        <source>Please wait</source>
        <translation>Attendere prego</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="327"/>
        <source>Set successful</source>
        <translation>Modifica avvenuta con successo</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="328"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="592"/>
        <source>Set failed</source>
        <translation>Impostazione Fallita</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="342"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="353"/>
        <source>OK to change</source>
        <translation>OK per modifica</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="379"/>
        <source>OK to update</source>
        <translation>OK per aggiornare</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="381"/>
        <source>Update successful</source>
        <translation>Aggiornamento Successo</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="382"/>
        <source>Update failed</source>
        <translation>Aggiornamento fallito</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="514"/>
        <source>Adjust LCD</source>
        <translation>Contrasto LCD</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="918"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="1610"/>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="353"/>
        <source>No Data</source>
        <translation>Nessun dato</translation>
    </message>
</context>
<context>
    <name>SecondStackedWdg</name>
    <message>
        <source>Please wait</source>
        <translation type="obsolete">Attendere prego</translation>
    </message>
</context>
<context>
    <name>Wdg2P5BattRemainTime</name>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="260"/>
        <source>No Data</source>
        <translation>Nessun dato</translation>
    </message>
</context>
<context>
    <name>Wdg2Table</name>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="462"/>
        <source>Active Alarms</source>
        <translation>Allarmi attivi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="468"/>
        <source>Alarm History</source>
        <translation>Storico allarmi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="632"/>
        <source>Observation</source>
        <translation>Osservazione</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="637"/>
        <source>Major</source>
        <translation>Urgente</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="642"/>
        <source>Critical</source>
        <translation>Critico</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="843"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="888"/>
        <source>Index</source>
        <translation>Num</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="849"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="899"/>
        <source>Iout(A)</source>
        <translation>Iout(A)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="855"/>
        <source>State</source>
        <translation>tato</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="894"/>
        <source>Vin(V)</source>
        <translation>Vin(V)</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation type="obsolete">Nessun dato</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Nome</translation>
    </message>
    <message>
        <source>SN</source>
        <translation type="obsolete">SN</translation>
    </message>
    <message>
        <source>Number</source>
        <translation type="obsolete">Num</translation>
    </message>
    <message>
        <source>Product Ver</source>
        <translation type="obsolete">Ver prodotto</translation>
    </message>
    <message>
        <source>SW Ver</source>
        <translation type="obsolete">Ver.SW</translation>
    </message>
</context>
<context>
    <name>Wdg3Table</name>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="25"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="26"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="550"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="551"/>
        <source>OA Alarm</source>
        <translation>Allarme OA</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="27"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="552"/>
        <source>MA Alarm</source>
        <translation>Allarme MA</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="28"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="553"/>
        <source>CA Alarm</source>
        <translation>Allarme CA</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="448"/>
        <source>No Data</source>
        <translation>Nessun dato</translation>
    </message>
</context>
<context>
    <name>WdgAlmMenu</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="58"/>
        <source>Alarm</source>
        <translation>Allarme</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="67"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="97"/>
        <source>Active Alarms</source>
        <translation>Allarmi attivi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="74"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="100"/>
        <source>Alarm History</source>
        <translation>Storico allarmi</translation>
    </message>
</context>
<context>
    <name>WdgFCfgGroup</name>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1735"/>
        <source>OK to restore default</source>
        <translation>OK per ripristinare</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1736"/>
        <source>Restore Default</source>
        <translation>Ripristina Ajustes</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1818"/>
        <source>OK to update app</source>
        <translation>OK per aggiornare app</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1838"/>
        <source>Without USB drive</source>
        <translation>Senza disco USB</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1847"/>
        <source>USB drive is empty</source>
        <translation>Disco USB è vuoto</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1856"/>
        <source>Update is not needed</source>
        <translation>Aggiornamento non necessario</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1865"/>
        <source>App program not found</source>
        <translation>Programma non trovato</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1880"/>
        <source>Without script file</source>
        <translation>Senza file di scrittura</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1900"/>
        <source>Start auto config</source>
        <translation>Inizia Auto Config</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1901"/>
        <source>Auto Config</source>
        <translation>Auto configurazione</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1940"/>
        <source>Rebooting</source>
        <translation>Riavvio</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1941"/>
        <source>Restore failed</source>
        <translation>Ripristino Fallito</translation>
    </message>
</context>
<context>
    <name>WdgFP0AC</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP0AC.cpp" line="158"/>
        <source>AC</source>
        <translation>CA</translation>
    </message>
</context>
<context>
    <name>WdgFP10Inventory</name>
    <message>
        <source>ENT to Inventory</source>
        <translation type="obsolete">ENT per Inventario</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="91"/>
        <source>ENT to inventory</source>
        <translation>ENT per Inventario</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="146"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="151"/>
        <source>SN</source>
        <translation>SN</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="160"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="175"/>
        <source>SW Ver</source>
        <translation>Ver. SW</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="181"/>
        <source>HW Ver</source>
        <translation>Ver. HW</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="187"/>
        <source>Config Ver</source>
        <translation>Configura Ver</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="212"/>
        <source>File Sys</source>
        <translation>File di Sis.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="218"/>
        <source>MAC Address</source>
        <translation>Indirizzo MAC</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="233"/>
        <source>DHCP Server IP</source>
        <translation>HCP Server IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="257"/>
        <source>Link-Local Addr</source>
        <translation>Indirzz Coll-Locale</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="279"/>
        <source>Global Addr</source>
        <translation>Indirzz Globale</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="301"/>
        <source>DHCP Server IPV6</source>
        <translation>DHCP Server IPV6</translation>
    </message>
</context>
<context>
    <name>WdgFP12ScreenSaverOut</name>
    <message>
        <source>Sys Used</source>
        <translation type="obsolete">Sis.in uso</translation>
    </message>
</context>
<context>
    <name>WdgFP1Module</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP1Module.cpp" line="111"/>
        <source>Module</source>
        <translation>Modulo</translation>
    </message>
</context>
<context>
    <name>WdgFP3DCA</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="109"/>
        <source>Last 7 days</source>
        <translation>Ultimi 7 giorni</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="160"/>
        <source>Total Load</source>
        <translation>Carico Totale</translation>
    </message>
</context>
<context>
    <name>WdgFP5Deg2Curve</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="110"/>
        <source>Last 7 days</source>
        <translation>Ultimi 7 giorni</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="111"/>
        <source>Ambient Temp</source>
        <translation>Temp Amb</translation>
    </message>
</context>
<context>
    <name>WdgFP6BattInfo</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP6BattInfo.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>WdgFP8BattDegCurve</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="108"/>
        <source>Last 7 days</source>
        <translation>Ultimi 7 giorni</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="109"/>
        <source>Temp Comp</source>
        <translation>Comp Temp</translation>
    </message>
</context>
<context>
    <name>WdgInventory</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="317"/>
        <source>No Data</source>
        <translation>Nessun dato</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="396"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="401"/>
        <source>SN</source>
        <translation>SN</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="406"/>
        <source>Number</source>
        <translation>Num</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="411"/>
        <source>Product Ver</source>
        <translation>Ver prodotto</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="416"/>
        <source>SW Ver</source>
        <translation></translation>
    </message>
</context>
</TS>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>CMenuData</name>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="203"/>
        <source>Settings</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="213"/>
        <source>Maintenance</source>
        <translation>Maintenance</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="221"/>
        <source>Energy Saving</source>
        <translation>MODE ECO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="229"/>
        <source>Alarm Settings</source>
        <translation>Config Alarmes</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="237"/>
        <source>Rect Settings</source>
        <translation>Config Red</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="245"/>
        <source>Batt Settings</source>
        <translation>Config Batt</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="253"/>
        <source>LVD Settings</source>
        <translation>Config LVD</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="261"/>
        <source>AC Settings</source>
        <translation>Config AC</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="269"/>
        <source>Sys Settings</source>
        <translation>Config Sys</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="277"/>
        <source>Comm Settings</source>
        <translation>Config Com</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="285"/>
        <source>Other Settings</source>
        <translation>Config Autre</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="294"/>
        <source>Slave Settings</source>
        <translation>Config Esclave</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="305"/>
        <source>Basic Settings</source>
        <translation>Config Basic</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="313"/>
        <source>Charge</source>
        <translation>Charge</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="321"/>
        <source>Battery Test</source>
        <translation>Test Batt</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="329"/>
        <source>Temp Comp</source>
        <translation>Temp Comp</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="337"/>
        <source>Batt1 Settings</source>
        <translation>Config Batt1</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="345"/>
        <source>Batt2 Settings</source>
        <translation>Config Batt2</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="354"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="713"/>
        <source>Restore Default</source>
        <translation>Restauration Defaut</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="362"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="396"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="719"/>
        <source>Update App</source>
        <translation>Mise à jour de l&apos;app</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="371"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="745"/>
        <source>Auto Config</source>
        <translation>Auto config</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="379"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="725"/>
        <source>LCD Display Wizard</source>
        <translation>Wizard sur afficheur LCD</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="387"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="739"/>
        <source>Start Wizard Now</source>
        <translation>Démarrer Wizard maintenant</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="714"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="720"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="726"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="740"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="746"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="715"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="721"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="727"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="741"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="747"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="753"/>
        <source>Protocol</source>
        <translation>Protocole</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="761"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="783"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="766"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="788"/>
        <source>Media</source>
        <translation>communication</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="773"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="795"/>
        <source>Baudrate</source>
        <translation>Baudrate</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="807"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="812"/>
        <source>Time</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="818"/>
        <source>IP Address</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="823"/>
        <source>Mask</source>
        <translation>MASK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="828"/>
        <source>Gateway</source>
        <translation>Passerelle</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="833"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="834"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="867"/>
        <source>Disabled</source>
        <translation>Invalidé</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="835"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="868"/>
        <source>Enabled</source>
        <translation>Validé</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="836"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="869"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="841"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="846"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="873"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="878"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="883"/>
        <source>IPV6 IP</source>
        <translation>IPV6 IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="851"/>
        <source>IPV6 Prefix</source>
        <translation>Prefixe IPV6</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="856"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="861"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="888"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="893"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="898"/>
        <source>IPV6 Gateway</source>
        <translation>Passerelle IPV6</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="866"/>
        <source>IPV6 DHCP</source>
        <translation>IPV6 DHCP</translation>
    </message>
</context>
<context>
    <name>CtrlInputChar</name>
    <message>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="291"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="304"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="317"/>
        <source>.</source>
        <translation>.</translation>
    </message>
</context>
<context>
    <name>DlgInfo</name>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="157"/>
        <source>ENT to OK</source>
        <translation>ENT pour OK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="159"/>
        <source>ESC to Cancel</source>
        <translation>ESC pour Annuler</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="310"/>
        <source>Update OK Num</source>
        <translation>Nombre M a J OK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="319"/>
        <source>Open File Failed</source>
        <translation>Fichier ouvert HS</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="323"/>
        <source>Comm Time-Out</source>
        <translation>Tempo COM OUT</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="540"/>
        <source>ENT or ESC to exit</source>
        <translation>ENT ou ESC pour Quitter</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="574"/>
        <source>Rebooting</source>
        <translation>Redémarrer</translation>
    </message>
</context>
<context>
    <name>DlgUpdateApp</name>
    <message>
        <location filename="../../g3_lui/util/DlgUpdateApp.cpp" line="44"/>
        <source>Qt Exited</source>
        <translation>Quitter QT</translation>
    </message>
</context>
<context>
    <name>FirstStackedWdg</name>
    <message>
        <location filename="../../g3_lui/equipWidget/FirstStackedWdg.ui" line="58"/>
        <source>Alarm</source>
        <translation>Alarme</translation>
    </message>
    <message>
        <source>Active Alarms</source>
        <translation type="obsolete">Alarmes actives</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">Histor Alarmes</translation>
    </message>
</context>
<context>
    <name>GuideWindow</name>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="279"/>
        <source>Installation Wizard</source>
        <translation>Installation Wizard</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="283"/>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="347"/>
        <source>ENT to continue</source>
        <translation>ENT pour Continuer</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="287"/>
        <source>ESC to skip </source>
        <translation>ESC pour Passer</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="339"/>
        <source>OK to exit</source>
        <translation>OK pour Sortir</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="343"/>
        <source>ESC to exit</source>
        <translation>ESC pour Quitter</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="362"/>
        <source>Wizard finished</source>
        <translation>Wizard terminé</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="528"/>
        <source>Site Name</source>
        <translation>Nom du site</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="534"/>
        <source>Battery Settings</source>
        <translation>Config Batt</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="540"/>
        <source>Capacity Settings</source>
        <translation>Config capacite</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="546"/>
        <source>ECO Parameter</source>
        <translation>Parametres ECO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="552"/>
        <source>Alarm Settings</source>
        <translation>Config Alarmes</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="558"/>
        <source>Common Settings</source>
        <translation>Config de base</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="564"/>
        <source>IP address</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="585"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="600"/>
        <source>Time</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="628"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="634"/>
        <source>Disabled</source>
        <translation>Invalidé</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="635"/>
        <source>Enabled</source>
        <translation>Validé</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="636"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="656"/>
        <source>IP Address</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="660"/>
        <source>MASK</source>
        <translation>MASK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="664"/>
        <source>Gateway</source>
        <translation>Passerelle</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="884"/>
        <source>Rebooting</source>
        <translation>Redémarrer</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1346"/>
        <source>Set language failed</source>
        <translation>Echec de config de la langue</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1377"/>
        <source>Adjust LCD</source>
        <translation>Regler LCD</translation>
    </message>
</context>
<context>
    <name>LoginWindow</name>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="127"/>
        <source>Select User</source>
        <translation>Select un utilisateur</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="130"/>
        <source>Enter Password</source>
        <translation>Entrer mot de passe</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="341"/>
        <source>Password error</source>
        <translation>Erreur de mot de passe</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../g3_lui/basicWidget/mainwindow.cpp" line="376"/>
        <source>OK to reboot</source>
        <translation>OK pour reboot</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="325"/>
        <source>OK to clear</source>
        <translation>OK pour init</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="326"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="380"/>
        <source>Please wait</source>
        <translation>Attendre SVP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="327"/>
        <source>Set successful</source>
        <translation>Config OK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="328"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="592"/>
        <source>Set failed</source>
        <translation>Config HS</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="342"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="353"/>
        <source>OK to change</source>
        <translation>OK pour modif</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="379"/>
        <source>OK to update</source>
        <translation>OK pour M a J</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="381"/>
        <source>Update successful</source>
        <translation>Mise à jour succès</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="382"/>
        <source>Update failed</source>
        <translation>M a J HS</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="514"/>
        <source>Adjust LCD</source>
        <translation>Regler LCD</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="918"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="1610"/>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="353"/>
        <source>No Data</source>
        <translation>Pas de donnée</translation>
    </message>
</context>
<context>
    <name>SecondStackedWdg</name>
    <message>
        <source>Please wait</source>
        <translation type="obsolete">Attendre SVP</translation>
    </message>
</context>
<context>
    <name>Wdg2P5BattRemainTime</name>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="260"/>
        <source>No Data</source>
        <translation>Pas de donnée</translation>
    </message>
</context>
<context>
    <name>Wdg2Table</name>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="462"/>
        <source>Active Alarms</source>
        <translation>Alarmes actives</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="468"/>
        <source>Alarm History</source>
        <translation>Histori Alarmes</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="632"/>
        <source>Observation</source>
        <translation>Observation</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="637"/>
        <source>Major</source>
        <translation>Majeure</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="642"/>
        <source>Critical</source>
        <translation>Critique</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="843"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="888"/>
        <source>Index</source>
        <translation>Num</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="849"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="899"/>
        <source>Iout(A)</source>
        <translation>Iout(A)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="855"/>
        <source>State</source>
        <translation>Etat</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="894"/>
        <source>Vin(V)</source>
        <translation>Vin(V)</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation type="obsolete">Pas de donnée</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Nom</translation>
    </message>
    <message>
        <source>SN</source>
        <translation type="obsolete">SN</translation>
    </message>
    <message>
        <source>Number</source>
        <translation type="obsolete">Num</translation>
    </message>
    <message>
        <source>Product Ver</source>
        <translation type="obsolete">Ver Mater</translation>
    </message>
    <message>
        <source>SW Ver</source>
        <translation type="obsolete">Ver Logic.</translation>
    </message>
</context>
<context>
    <name>Wdg3Table</name>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="25"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="26"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="550"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="551"/>
        <source>OA Alarm</source>
        <translation>OA Alarme</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="27"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="552"/>
        <source>MA Alarm</source>
        <translation>MA Alarme</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="28"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="553"/>
        <source>CA Alarm</source>
        <translation>CA Alarme</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="448"/>
        <source>No Data</source>
        <translation>Pas de donnée</translation>
    </message>
</context>
<context>
    <name>WdgAlmMenu</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="58"/>
        <source>Alarm</source>
        <translation>Alarme</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="67"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="97"/>
        <source>Active Alarms</source>
        <translation>Alarmes actives</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="74"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="100"/>
        <source>Alarm History</source>
        <translation>Histoire Alarme</translation>
    </message>
</context>
<context>
    <name>WdgFCfgGroup</name>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1735"/>
        <source>OK to restore default</source>
        <translation>OK pour restauration</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1736"/>
        <source>Restore Default</source>
        <translation>Restaurer configuration par defaut</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1818"/>
        <source>OK to update app</source>
        <translation>OK pour M a J Appl</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1838"/>
        <source>Without USB drive</source>
        <translation>Sans Cle USB</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1847"/>
        <source>USB drive is empty</source>
        <translation>Cle USB vide</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1856"/>
        <source>Update is not needed</source>
        <translation>M a J inutile</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1865"/>
        <source>App program not found</source>
        <translation>App introuvable</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1880"/>
        <source>Without script file</source>
        <translation>Pas de fichier script</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1900"/>
        <source>Start auto config</source>
        <translation>Lancer auto config</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1901"/>
        <source>Auto Config</source>
        <translation>Auto-config</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1940"/>
        <source>Rebooting</source>
        <translation>Redémarrer</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1941"/>
        <source>Restore failed</source>
        <translation>Defaut restauration</translation>
    </message>
</context>
<context>
    <name>WdgFP0AC</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP0AC.cpp" line="158"/>
        <source>AC</source>
        <translation>AC</translation>
    </message>
</context>
<context>
    <name>WdgFP10Inventory</name>
    <message>
        <source>ENT to Inventory</source>
        <translation type="obsolete">ENT pour Inventaire</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="91"/>
        <source>ENT to inventory</source>
        <translation>ENT pour Inventaire</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="146"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="151"/>
        <source>SN</source>
        <translation>SN</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="160"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="175"/>
        <source>SW Ver</source>
        <translation>Ver Logic.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="181"/>
        <source>HW Ver</source>
        <translation>Ver Mater.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="187"/>
        <source>Config Ver</source>
        <translation>Ver.Config.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="212"/>
        <source>File Sys</source>
        <translation>Fichier Sys.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="218"/>
        <source>MAC Address</source>
        <translation>MAC Adresse</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="233"/>
        <source>DHCP Server IP</source>
        <translation>IP serveur DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="257"/>
        <source>Link-Local Addr</source>
        <translation>Link-Local Adr</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="279"/>
        <source>Global Addr</source>
        <translation>Global Adr</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="301"/>
        <source>DHCP Server IPV6</source>
        <translation>DHCP Serveur IPV6</translation>
    </message>
</context>
<context>
    <name>WdgFP12ScreenSaverOut</name>
    <message>
        <source>Sys Used</source>
        <translation type="obsolete">Sys utilise</translation>
    </message>
</context>
<context>
    <name>WdgFP1Module</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP1Module.cpp" line="111"/>
        <source>Module</source>
        <translation>Module</translation>
    </message>
</context>
<context>
    <name>WdgFP3DCA</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="109"/>
        <source>Last 7 days</source>
        <translation>7 derniers jours</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="160"/>
        <source>Total Load</source>
        <translation>Charge totale</translation>
    </message>
</context>
<context>
    <name>WdgFP5Deg2Curve</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="110"/>
        <source>Last 7 days</source>
        <translation>7 derniers jours</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="111"/>
        <source>Ambient Temp</source>
        <translation>Temp Amb</translation>
    </message>
</context>
<context>
    <name>WdgFP6BattInfo</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP6BattInfo.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>WdgFP8BattDegCurve</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="108"/>
        <source>Last 7 days</source>
        <translation>7 derniers jours</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="109"/>
        <source>Temp Comp</source>
        <translation>Temp Comp</translation>
    </message>
</context>
<context>
    <name>WdgInventory</name>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="317"/>
        <source>No Data</source>
        <translation>Pas de donnée</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="396"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="401"/>
        <source>SN</source>
        <translation>SN</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="406"/>
        <source>Number</source>
        <translation>Num</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="411"/>
        <source>Product Ver</source>
        <translation>Ver Mater</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgInventory.cpp" line="416"/>
        <source>SW Ver</source>
        <translation>Ver Logic.</translation>
    </message>
</context>
</TS>

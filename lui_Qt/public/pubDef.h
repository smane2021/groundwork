/******************************************************************************
文件名：    pubDef.h
功能：      定义一些全局性的宏，枚举、结构体,用于本Qt程序所有模块范围内
作者：      刘金煌
创建日期：   2013年3月25日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef PUBDEF_H
#define PUBDEF_H

//////////////////////////////////////////////
//基本类型宏定义 basic type define
typedef unsigned char  BYTE;
typedef unsigned long  ULONG;
typedef bool           BOOL;

#define BUTTON_BEEP      0
#define ALARM_BEEP       1
#define ALL_BEEP         2

// buzz lcd led
enum SET_BUZZ
{
    BUZZ_QUIET,
    BUZZ_BEEP,
    BUZZ_MOO,
    BUZZ_MAX
};

enum SET_LCD
{
	LCD_MIN           = -1,
    LCD_ON_BRIGHTNESS = 0,
    LCD_BACK_LIGHT_4  = 1,
    LCD_BACK_LIGHT_3  = 2,
    LCD_BACK_LIGHT_2  = 3,
    LCD_BACK_LIGHT_1  = 4,
    LCD_OFF_BRIGHTNESS= 5,
    LCD_MAX
};

enum SET_LED
{
    LED_RED_ON,
    LED_RED_OFF,

    LED_YELLOW_ON,
    LED_YELLOW_OFF,

    LED_GREEN_ON,
    LED_GREEN_OFF,
    LED_FLASH,
    LED_MAX
};

enum GAMMA_LEVEL
{
	GAMMA_LEVEL_1,
	GAMMA_LEVEL_2,
	GAMMA_LEVEL_3,
	GAMMA_LEVEL_4,
	GAMMA_LEVEL_MAX
};

#define LED_STATE_RED_ON         0x01
#define LED_STATE_YELLOW_ON      0x02
#define LED_STATE_GREEN_ON       0x04
#define LED_STATE_GREEN_FLASH_ON 0x08

#endif // PUBDEF_H

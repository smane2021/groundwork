/******************************************************************************
文件名：    dataDef.h
功能：      定义一些全局性的宏，枚举、结构体,用于与ACU+的数据交互
作者：      刘金煌
创建日期：   2013年3月25日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef DATADEF_H
#define DATADEF_H
#include <time.h>
#include "pubDef.h"

#define SIZEOF_MAPFILE        (1024*64)
// 其他界面的映射文件名
#define MAPFILE_SIG_NAME   "/var/mmap.dat"
#define SYNCNAME_WRITE    "/semCmd"     // 写同步名
#define SYNCNAME_READ     "/semAck"     // 读同步名
#define SYNCNAME_ALARM    "/SemNewAlm"  // 告警信号量
#define SYNCNAME_BEAT     "/semBeat"    // 心跳信号量

#define SIZEOF_MAPFILE_alarm   (128)
// 主屏MainWindow告警数据的映射文件名
#define MAPFILE_SIG_NAME_alarm "/var/mAlm.dat"


#define SET_PARAM_OK       1             // 设置参数成功
#define SET_PARAM_FAILED   0             // 设置参数失败

//////////////////////////////////////////////
//               最大长度宏
#define MAX_LEN_ITEM_VAL   (50)      //信号值(字符串类型)最大长度
#define MAX_LEN_SIG_NAME   (20)      //信号名最大长度
#define MAX_LEN_UNIT       (5)       //单位最大长度
#define MAX_LEN_EQUIP_ITEM (100)     //同一设备类型的设备项最大个数
#define MAX_LEN_SIG_ITEM   (1000)     //同一设备信号项最大个数
#define MAX_IPV6_LEN       (16)

#define  VAR_LONG            1
#define  VAR_FLOAT           2
#define  VAR_UNSIGNED_LONG  3
#define  VAR_DATE_TIME      4  // 现在用的VAR_LONG
#define  VAR_ENUM           5
//////////////////////////////////////////////
//               枚举型
enum LANGUAGE_TYPE
{
    LANG_English,
//    LANG_Chinese,
    LANG_French,
//    LANG_German,
//    LANG_Italian,
//    LANG_Russia,
    LANG_Spanish,
//    LANG_TraditionalChinese,
//    LANG_Turkish,
//    LANG_Portugal,
    LANG_TYPE_MAX
};

enum LANGUAGE_LOCATE
{
    LANG_LOCATE_English,
    LANG_LOCATE_Native
};

enum ALARM_VOICE
{
    ALARM_VOICE_ON,
    ALARM_VOICE_OFF,
    ALARM_VOICE_3M,
    ALARM_VOICE_10M,
    ALARM_VOICE_1H,
    ALARM_VOICE_4H,
    ALARM_VOICE_MAX
};

enum KEYPAD_VOICE
{
    KEYPAD_VOICE_ON,
    KEYPAD_VOICE_OFF
};

enum LCD_ROTATION
{
    LCD_ROTATION_0DEG,
    LCD_ROTATION_90DEG,
    LCD_ROTATION_BIG
};

enum TIME_FORMAT
{
    TIME_FORMAT_EMEA, // dd/MM/yyyy
    TIME_FORMAT_NA,   // MM/dd/yyyy
    TIME_FORMAT_CN    // 2004/11/16
};

enum HOMEPAGE_TYPE
{
    HOMEPAGE_TYPE_INDEPENDENT,
    HOMEPAGE_TYPE_MAIN,
    HOMEPAGE_TYPE_SLAVE,
    HOMEPAGE_TYPE_MAX
};

enum DISPLAY_VERSION
{
  DISPLAY_VERSION_NORMAL = 0,
  DISPLAY_VERSION_VERIZON
};

enum SIG_TYPE_DEFINITIONS
{
  SIG_TYPE_SAMPLING = 0,
  SIG_TYPE_CTRL,
  SIG_TYPE_SET,
  SIG_TYPE_ALARM
};

enum ALMVOICE_CLOSE_TYPE
{
  ALMVOICE_CLOSE_YES = 0,
  ALMVOICE_CLOSE_NO
};

enum ALMVOICE_ACTIVATE_TYPE
{
  ALMVOICE_ACTIVATE_TYPE_NO = 0,
  ALMVOICE_ACTIVATE_TYPE_YES
};

// 初始化参数
struct Init_Param
{
    enum LANGUAGE_TYPE     langType;
    enum LANGUAGE_LOCATE   langLocate;
    enum LANGUAGE_TYPE     langApp;

    enum ALARM_VOICE       alarmVoice;
    // -1:长响 0:不响 其他:秒数
    int                    alarmTime;

    enum KEYPAD_VOICE      keypadVoice;
    // 水平 垂直 大屏
    enum LCD_ROTATION      lcdRotation;
    enum TIME_FORMAT       timeFormat;
    enum HOMEPAGE_TYPE     homePageType;
    enum DISPLAY_VERSION   dispVersion;
    enum ALMVOICE_CLOSE_TYPE almVoiceType;
    enum ALMVOICE_ACTIVATE_TYPE almVoiceActType;
};

//////////////////////////////////////////////
//               与app交互的 macro and structure
#define MAXLEN_ENUM		10
#define MAXLEN_NAME		20
#define MAXLEN_UNIT		10
#define MAXLEN_SET		15
#define MAXLEN_BARCODE		13
//Frank Wu,20151103
//#define MAXLEN_VER		4
#define MAXLEN_VER		16

#define MAXNUM_SEL		100
#define MAXNUM_NORVALUE		10
#define MAXNUM_BATT		(120+32)
#define MAXNUM_MODULE		180
#define MAXNUM_SIG		3
#define MAXNUM_ACTALM		200
#define MAXNUM_HISALM		500
#define MAXNUM_HELP		64
#define MAXNUM_TREND		85
#define MODINFO_MAX		10

#define MAXLEN_NUMBER   16
#define LEN_MAC             32
#define LEN_IPV6		40	//"09AF: 19AF: 29AF: 39AF: 49AF: 59AF: 69AF: 79AF", len=4*8 + 7 + 1=40
#define MAXNUM_BATINFO  3
#define  MAXLEN_PROINFO     16

#define  MAX_USERS          16
#define MAXLEN_SIG        3
#define MAXNUM_BRANCH       750

#define LIMNUM              6
// max chars one row
#define MAX_CHARS_ROW       -1
//changed by Frank Wu,1/N,20140225
#define MAXNUM_NORLEVEL  3

#define EQUIPID_SPECIAL     (-1)

enum CMD_TYPE
{
  CT_READ = 0,
  CT_SET,
  CT_CTRL,
  CT_HEARTBEAT,
  CT_REBOOT,
  CT_RESETPASSWORD
    //0-读；1-设置；2-控制；3-喂狗；4-reboot;5-resetpassword
};

typedef long				SIG_TIME;
typedef long				SIG_ENUM;
typedef union _VarValue
{
	long			lValue;
	float			fValue;
	unsigned long	ulValue;
	SIG_TIME		dtValue;
	SIG_ENUM		enumValue;
}VAR_VALUE;

struct _Head
{
         int iScreenID;
};
typedef struct _Head Head_t;

//一、	qt命令格式
struct SetInformation
{
   int EquipID;
   int SigID;
   int SigType;
   VAR_VALUE value;
   char			Name[MAXLEN_NAME];
};
typedef struct SetInformation CMD_SET_INFO;

struct CmdItem
{
   int CmdType;
   int ScreenID;
   CMD_SET_INFO setinfo;
};
typedef struct CmdItem CMD_INFO;
//设置语言：
//CmdType = 1；
//ScreenID = 0；
//EquipID = 1；
//SigID = 22；
//SigType = 3
//value 0-英语 1-本地

//二、	控制命令
//1．	控制模块灯闪
// 0-Rect模块灯闪；1-MPPT模块灯闪；2-Conv模块灯闪；
// 3-S1           4-S2          5-S3
enum MODULE_TYPE {
    MODULE_TYPE_INVALID = -1,
    MODULE_TYPE_RECT = 0,
    MODULE_TYPE_MPPT,
    MODULE_TYPE_CONV,
    MODULE_TYPE_S1RECT,
    MODULE_TYPE_S2RECT,
    MODULE_TYPE_S3RECT,
    MODULE_TYPE_MAX
};
//value 0-所有同类模块灯长亮；1-绿灯闪。（在模块被选中时灯闪，退出模块信息屏长亮）
enum MODULE_LIGHT_TYPE {
    MODULE_LIGHT_TYPE_ON,
    MODULE_LIGHT_TYPE_GREEN_FLASH
};

enum SIGID_SPECIAL {
    SIGID_SPECIAL_Sitename,         // 0
    SIGID_SPECIAL_Datetime,         // 1
    SIGID_SPECIAL_IP,               // 2
    SIGID_SPECIAL_MASK,             // 3
    SIGID_SPECIAL_gateway,          // 4
    SIGID_SPECIAL_ClearWarn,        // 5
    SIGID_SPECIAL_BattTest,         // 6
    SIGID_SPECIAL_RestoreDefaultCfg,// 7
    SIGID_SPECIAL_DHCP,             // 8
    SIGID_SPECIAL_AutoCfg,          // 9
    SIGID_SPECIAL_PROTOCOL,        //10
    SIGID_SPECIAL_YDN23_ADDR,        //11
    SIGID_SPECIAL_YDN23_METHOD,      //12
    SIGID_SPECIAL_YDN23_BAUDRATE,    //13
    SIGID_SPECIAL_MODBUS_ADDR,        //14
    SIGID_SPECIAL_MODBUS_METHOD,      //15
    SIGID_SPECIAL_MODBUS_BAUDRATE,    //16
    SIGID_SPECIAL_DATE,               //17
    SIGID_SPECIAL_TIME,               //18
    SIGID_SPECIAL_IPV6_IP_1,          //19 IPV6
    SIGID_SPECIAL_IPV6_IP_2,          //20
    SIGID_SPECIAL_IPV6_PREFIX,
    SIGID_SPECIAL_IPV6_GATEWAY_1,
    SIGID_SPECIAL_IPV6_GATEWAY_2,
    SIGID_SPECIAL_IPV6_DHCP,
    SIGID_SPECIAL_IPV6_IP_1_V,        //25 IPV6 V
    SIGID_SPECIAL_IPV6_IP_2_V,
    SIGID_SPECIAL_IPV6_IP_3_V,
    SIGID_SPECIAL_IPV6_GATEWAY_1_V,
    SIGID_SPECIAL_IPV6_GATEWAY_2_V,
    SIGID_SPECIAL_IPV6_GATEWAY_3_V,   //30
    SIGID_SPECIAL_MAX
};

//三、	设置命令 CT_SET
/*
对于特殊的设置，iEquipID = -1。
                  iSigID = 0		表示sitename
                  iSigID = 1		表示时间
                  iSigID = 2		表示IP
                  iSigID = 3		表示MASK
  iSigID = 4		表示Gateway
  iSigID = 5		表示清除告警
  iSigID = 6		表示电池测试
  iSigID = 7		表示恢复默认配置
后台执行完成后返回int型状态：1表示成功；0表示失败。
*/

//四、	查询命令
// 1．	首页信息（ScreenID = 0）
#define SCREEN_ID_HomePageWindow        0x000000
/*
Alarm Status	指示告警灯状态。0-normal；1-alarm
Converter模式（CM）
MPPT运行模式(MM)
DG Existence	指示输入图标状态。
                CM=1：交流图标亮色
                MM=0或1：交流图标亮色
                CM=0且MM=2：交流图标灰色
                CM=0且(MM=1或2)：太阳图标亮色；
                CM=1或MM=0：太阳图标灰色；
                DG Existence=0：油机图标亮色
                DG Existence=1：油机图标灰色
Mains failure
DG Running
Solar Running	指示输入到模块的连线状态。
                Mains failure=1；DG Running=0；Solar Running=0则连线为灰色；否则连线动态显示。
BattStat        显示字符串（不显示）
DCVolt          显示浮点数(一位小数)+单位
Current         显示浮点数(一位小数)+单位
BattCap         显示浮点数（无小数）+单位
SysUsed         显示信号名称+浮点数（无小数）+单位

************模块************* WdgFP1Module
Converter模式	1-“RectNum”行和“Sol Conv Num”行都不显示
MPPT运行模式	0- “Sol Conv Num”行不显示
    1-“RectNum”行和“Sol Conv Num”行都显示
    2-“RectNum”行不显示
RectNum	Rect个数
Sol Conv Num	Sol Conv个数
ConvNum	Conv个数 =0时，ConvNum” 行不显示；
6 Controller Mode 0-S1Num、S2Num、S3Num都不显示
7 Slave1 State	Controller Mode 为1时，
    //0-	S1Num显示
    //1-	S1Num不显示
8 Slave2 State	Controller Mode 为1时，
    //0-	S2Num显示
    //1-	S2Num不显示
9 Slave3 State	Controller Mode 为1时，
    //0-	S3Num显示
    //1-	S3Num不显示
10 11 12 s1Num s2Num s3Num

结构SpecialID2Info目前只用到了iDispCtrl 和iDispStyle
当iEquipID == -2时，设备结构stSpecialID2Info的值有效：
iDispCtrl为1时，表示该项需要显示；如果需要显示，iDispStyle表示显示风格，目前只用到值1，表示仅仅作为显示，没有下级菜单
iDispCtrl为2时，表示该项不需要显示；
*/
#define MAX_DISP_ARGS_NUM   6
//changed by Frank Wu,20140915,1/N/N, for Adding converter and rectifier total current and voltage to the local display
struct SpecialID2Info
{
         int iDispCtrl;//0, auto; 1, show; 2, hide;
         int iCtrlArgs[MAX_DISP_ARGS_NUM];
         int iDispStyle;//0, auto; 1, label, just for displaying; 2, menu, can enter;
};
typedef struct SpecialID2Info SPECIAL_ID2_INFO;

struct PackGetInfo
{
    Head_t stHead;
    int        iEquipID;
    int        iSigID;
    char       cSigName[MAXLEN_NAME];
    int        iSigValueType;
    int        iFormat;
    VAR_VALUE  vSigValue;
    char       cSigUnit[MAXLEN_UNIT];
    char       cEnumText[MAXLEN_NAME];
    //changed by Frank Wu,20140915,1/N/N, for Adding converter and rectifier total current and voltage to the local display
    SPECIAL_ID2_INFO stSpecialID2Info;//just if (iEquipID == -2), it becomes valid
    //all PackGetInfo items counts just for WdgFP1Module now
    int iDataNum;
};
typedef struct PackGetInfo PACK_INFO;

// 2. ModuleNum信息页（ScreenID=0x000400）
#define SCREEN_ID_WdgFP1Module          0x000400
//同上

// 3. Slave MainScreen（ScreenID=0x0000FE）
#define SCREEN_ID_HomePageType          0x0000FE
//同上

//Source information
#define SCREEN_ID_Source_Info           0x000410

//获取温度显示的格式（摄氏度或者华氏度显示）
#define SCREEN_ID_Temp_Format           0x000401

// 4．	Rect Inv信息页（ScreenID=0x060004）
#define SCREEN_ID_Wdg2Table_Rect_Inv    0x060004
struct ModInv
{
    int         iEqIDType;
    int         iEquipID;
    int         iSigID;
    char	cEquipName[MAXLEN_PROINFO];
    char	cSerialNumber[MAXLEN_PROINFO];
    char	cPartNumber[MAXLEN_PROINFO];
    char	cPVer[MAXLEN_VER];          //Modified by mhwang,the old value is MAXLEN_VER
    char	cSWver[MAXLEN_PROINFO];
};
typedef struct ModInv MOD_INV;

struct PackInvInfo
{
    Head_t stHead;
    int		iModuleNum;
    MOD_INV		ModuleInv[MAXNUM_MODULE];
};
typedef struct PackInvInfo PACK_INVINFO;


#define SCREEN_ID_Wdg2Table_SlaveRect_Inv 0x060005
//      SM & I2C Inv信息页（ScreenID=0x060006）
#define SCREEN_ID_Wdg2Table_SMI2C_Inv     0x060006
//      RS485 Inv信息页（ScreenID=0x060007）
#define SCREEN_ID_Wdg2Table_RS485_Inv     0x060007
//      Conv Inv信息页（ScreenID=0x060008）
#define SCREEN_ID_Wdg2Table_Conv_Inv      0x060008
//      LiBatt Inv信息页（ScreenID=0x060009）
#define SCREEN_ID_Wdg2Table_LiBatt_Inv    0x060009
//      Sol Conv Inv信息页（ScreenID=0x06000A）
#define SCREEN_ID_Wdg2Table_SolConv_Inv   0x06000A
//      SMDUE Inv信息页（ScreenID=0x06000B）
#define SCREEN_ID_Wdg2Table_SMDUE_Inv    0x06000B
//同上

//      Self Inv信息页（ScreenID=0x060001）
#define SCREEN_ID_WdgFP10Inventory      0x060001
struct PackSelfInv
{
    Head_t stHead;
     char cPartNumber[MAXLEN_NUMBER];
     char cSN[MAXLEN_NAME];
     char cPVer[MAXLEN_NUMBER];
     char cSWver[MAXLEN_NUMBER];
     char cCfgFileVer[MAXLEN_NUMBER];
     char cFileSystemVer[MAXLEN_NUMBER];
     char cEthaddr[LEN_MAC];
     ULONG    DHCP_IP;
     ULONG    MAIN_IP;
     //changed by Hrishikesh Oak,20190904,for CR - Improve the ability to determine a controller’s IP address
     ULONG	FRONT_CRAFT_IP;
     //end -- changed by Hrishikesh Oak,20190904,for CR - Improve the ability to determine a controller’s IP address
     //changed by Frank Wu,20140811,1/N/N,for support ipv6 settings
     char	cIpv6Local[LEN_IPV6];//format:"09AF:19AF:29AF:39AF:49AF:59AF:69AF:79AF"
     char	cIpv6Global[LEN_IPV6];//format:"09AF:19AF:29AF:39AF:49AF:59AF:69AF:79AF"
     char	cIpv6DHCPServer[LEN_IPV6];//format:"09AF:19AF:29AF:39AF:49AF:59AF:69AF:79AF"
};
typedef struct PackSelfInv PACK_SELFINV;

//11．	密码设置页（ScreenID=0x060003）
#define SCREEN_ID_DlgLogin              0x060003
struct tagUserInfo
{
    char szUserName[16];	//User name
    char szPassword[25];	//User password
    BYTE byLevel;			//User level	暂时不管
};
typedef struct tagUserInfo USER_INFO_STRU;
struct PackPwdInfo
{
    Head_t stHead;
    int		iNum;
    USER_INFO_STRU	UserInfo[MAX_USERS];
};
typedef struct PackPwdInfo PACK_PWD;

//12．	初始化（ScreenID=0x0300ff）
/*
参数按一下顺序：
Language	0-英文；1-本地语
alarm voice	0-On； 1-Off；
2-3 min；3-10 min；
4-1 hr；5-4 hrs
Keypad Voice Enabled	0-On；1-Off
LCD Rotation	0-转0度；1-转90度；2-大液晶
Time Display Format	0-EMEA(16/11/2004)；
1-NA(11/16/2004)；
2-CN（2004/11/16）
*/
#define SCREEN_ID_Init                  0x0300ff
#define SCREEN_ID_GetWebAlmVoice        0x030001
struct NormalInfo
{
    int		iEquipID;
    int		iSigID;
    int		iSigType;
    VAR_VALUE	vValue;
};
typedef struct NormalInfo NORMAL_INFO;

struct PackDataInfo
{
    Head_t stHead;
    int		DataNum;
    NORMAL_INFO	DataInfo[MAXNUM_NORVALUE];
};
typedef struct PackDataInfo PACK_DATAINFO;

// MainWindow::DetectActiveAlarm() 单独的信号量和共享内存
struct ActAlmSummaryInfo
{
         int                       iEquipID;
         int                       iSigID;
         time_t                StartTime;
         bool operator == (const ActAlmSummaryInfo &other) const
         {
             return (iEquipID==other.iEquipID &&
                     iSigID==other.iSigID &&
                     StartTime==other.StartTime);
         }
};
typedef struct ActAlmSummaryInfo ACTALM_SUMM_INFO;

struct PackActAlmSummInfo
{
         //Head_t must be the first, do not move
         Head_t stHead;//changed by Frank Wu,20140110,5/19, for add ScreenID
         int              OANum;
         int              MANum;
         int              CANum;
         int              ActAlmNum;
         ACTALM_SUMM_INFO          ActAlmItem[MAXNUM_ACTALM];
};
typedef struct PackActAlmSummInfo PACK_ACTALM_SUMM;

//13．	当前告警数信息（ScreenID=0x060100）
#define SCREEN_ID_Wdg2Table_ActAlmNum   0x060100
struct PackAlmNumInfo
{
    Head_t stHead;
    int		OANum;
    int		MANum;
    int		CANum;
};
typedef struct PackAlmNumInfo PACK_ALMNUM;

//14．	当前告警（ScreenID=0x060101）
#define SCREEN_ID_Wdg3Table_ActAlm      0x060101
struct ActAlmInfo
{
    int		iEqIDType;
    int		iEquipID;
    int		iSigID;
    char    AlmName[MAXLEN_NAME];
    char    EquipName[MAXLEN_NAME];
    int     AlmLevel;
    time_t  StartTime;
    //char		EquipSN[32];
    char    AlmHelp[MAXNUM_HELP];
};
typedef struct ActAlmInfo ACTALM_INFO;

struct PackActAlmInfo
{
    Head_t stHead;
    int		ActAlmNum;
    ACTALM_INFO	ActAlmItem[MAXNUM_ACTALM];
};
typedef struct PackActAlmInfo PACK_ACTALM;
//最多200条

//15．	历史告警数目（ScreenID=0x060102）
#define SCREEN_ID_Wdg2Table_HisAlmNum   0x060102
//struct PackAlmNumInfo
//{
//  int		OANum;
//  int		MANum;
//  int		CANum;
//};

//16．	历史告警（ScreenID=0x060103）
#define SCREEN_ID_Wdg3Table_HisAlm      0x060103
struct HisAlmInfo
{
    char		EquipName[MAXLEN_NAME];
    char		AlmName[MAXLEN_NAME];
    BYTE		AlmLevel;
    time_t		StartTime;
    time_t		EndTime;
    //char		EquipSN[32];
    //char		AlmHelp[MAXNUM_HELP];
};
typedef struct HisAlmInfo HISALM_INFO;

struct PackHisAlmInfo
{
    Head_t stHead;
    int		HisAlmNum;
    HISALM_INFO	HisAlmItem[MAXNUM_HISALM];
};
typedef struct PackHisAlmInfo PACK_HISALM;

//17．	控制日志（ScreenID=0x060104）
#define SCREEN_ID_Wdg2Table_EventLog    0x060104

//18．	交流信息 （ScreenID=0x010300）
#define SCREEN_ID_WdgFP0AC              0x010300
struct IcoInfo
{
    int		iEquipID;
    int		iSigID;
    int		iFormat;
    char		cSigName[MAXLEN_NAME];
    float		fSigValue;
    char		cSigUnit[MAXLEN_UNIT];
};
typedef struct IcoInfo ICO_INFO;

struct Pack_Ico
{
    Head_t stHead;
     float         LimitValue[LIMNUM];    //从大到小
     int      iDataNum;
     ICO_INFO DispData[MAXNUM_SIG];
};
typedef struct Pack_Ico PACK_ICOINFO;
//参数顺序为：L1、L2、L3

//19．	DC电压图形页（ScreenID=0x010500）
//同上
#define SCREEN_ID_WdgFP2DCV             0x010500

//20．	DC中环境温度图形页（ScreenID=0x010502）
//同上 温度计
#define SCREEN_ID_WdgFP4Deg1            0x010502

//21．	Batt中电池剩余时间图形页（ScreenID=0x010600）
//同上
#define SCREEN_ID_WdgFP6BattRemainTime  0x010600
#define SCREEN_ID_WdgFP6BattInfo        0x010605

//22．	Batt中Comp Temp图形页（ScreenID=0x010601）
// 温度计
#define SCREEN_ID_WdgFP7BattDegMeter    0x010601

//23．	Rect信息（ScreenID=0x020401）
#define SCREEN_ID_Wdg2Table_RectInfo    0x020401
struct EachModule
{
     int      iSigID;
     int      iSigType;
     VAR_VALUE vSigValue;
     int      iFormat;
     char     cEnumText[MAXLEN_ENUM];
};
typedef struct EachModule EACHMOD_INFO;

struct ModuleInfo
{
    int		iEquipID;
    char		cEqName[MAXLEN_NAME];
    EACHMOD_INFO	EachMod[MAXNUM_SIG];
};
typedef struct ModuleInfo MODINFO;

struct PackModInfo
{
    Head_t stHead;
    int		iModuleNum;
    MODINFO		ModInfo[MAXNUM_MODULE];
};
typedef struct PackModInfo PACK_MODINFO;
//在EachMod中第一个是通讯状态，0-正常；非0-通讯中断。

//24．	MPPT信息（ScreenID=0x020402）
//同17
#define SCREEN_ID_Wdg2Table_SolInfo     0x020402
#define SCREEN_ID_Wdg2Table_S1RectInfo  0x020403
#define SCREEN_ID_Wdg2Table_S2RectInfo  0x020404
#define SCREEN_ID_Wdg2Table_S3RectInfo  0x020405

//25．	Converter信息（ScreenID=0x020507）
#define SCREEN_ID_Wdg2Table_ConvInfo    0x020507
//同上

//20．	DC中支路电流图形页（ScreenID=0x070503）
#define SCREEN_ID_Wdg2DCABranch         0x070503
struct LoadInfo
{
     int      iEquipID;
     int      iSigNum[MAXLEN_SIG];
     int      iRespond; // 0正常 1异常
     int		iFormat;
     VAR_VALUE vValue[MAXLEN_SIG];
     char     cEqName[MAXLEN_NAME];
};
typedef struct LoadInfo LOAD_INFO;

struct PackLoadInfo
{
    Head_t stHead;
     int      LoadNum;
     LOAD_INFO LoadInfo[MAXNUM_BRANCH];
};
typedef struct PackLoadInfo PACK_LOADINFO;

//21．	DC中分路环境温度图形页（ScreenID=0x070504）
#define SCREEN_ID_Wdg2DCDeg1Branch      0x070504
struct GroupInfo
{
    int		iEquipID;
    int		iSigID;
    int		iSigType;
    int		iRespond; // 0正常 1异常
    int		iFormat;
    int     iIndex;   // 序号
    VAR_VALUE	vValue;
    //changed by Frank Wu,1/N,20140225,for TR160 which Missing alarm levels on temperature bar.
    VAR_VALUE  vaLevel[MAXNUM_NORLEVEL];//0, Low Level; 1, High Level;2, Very High Level
};
typedef struct GroupInfo GROUP_INFO;

struct PackGrpInfo
{
    Head_t stHead;
     int      DataNum;
     GROUP_INFO    DataInfo[MAXNUM_MODULE];
};
typedef struct PackGrpInfo PACK_GRPINFO;

//22．	Batt中分路电池温度图形页（ScreenID=0x070603）
#define SCREEN_ID_Wdg2P6BattDeg         0x070603
//同上

//23．	Batt中分组电池信息图形页（ScreenID=0x040602）
// 电池条形图
#define SCREEN_ID_Wdg2P5BattRemainTime  0x040602
struct DataInfo
{
    int		iSigID;
    int		iFormat;
    VAR_VALUE	vValue;
};
typedef struct DataInfo DATA_INFO;

struct BattInfo
{
    int		iRespond;
    int		iEquipID;
    char		cEquipName[MAXLEN_NAME];
    DATA_INFO	BattItem[MAXNUM_BATINFO];
};
typedef struct BattInfo BATT_INFO;

struct PackBattInfo
{
    Head_t stHead;
    int		BattNum;
    BATT_INFO	BattInfo[MAXNUM_BATT];
};
typedef struct PackBattInfo PACK_BATTINFO;

//参数顺序为：标称容量（Ah）、电流（A）、使用容量（%）

//24．	负载趋势图（ScreenID=0x060501）
#define SCREEN_ID_WdgFP3DCA             0x060501
struct PackTrendInfo
{
    Head_t stHead;
    // %
    float		fData[MAXNUM_TREND];
    //changed by Frank Wu,1/N,20140221 for TR203 On DC load graph show the load in % if possible show the A and %
    // A
    float       fCurrentData[MAXNUM_TREND];
};
typedef struct PackTrendInfo PACK_TRENDINFO;

//25．	环境温度趋势图（ScreenID=0x060505）
#define SCREEN_ID_WdgFP5Deg2Curve       0x060505
struct Temp_Trend
{
    time_t		TimeTrend;
    float		fTemp;
};
typedef struct Temp_Trend TEMP_TREND;

struct PackTempTrend
{
    Head_t stHead;
    int		iNum;
    TEMP_TREND	TempTrend[MAXNUM_TREND];
};
typedef struct PackTempTrend PACK_TEMPTREND;

//26．	电池温度趋势图（ScreenID=0x060604）
#define SCREEN_ID_WdgFP8BattDegCurve    0x060604

struct SettingInfo
{
     int      iEquipID;
     int      iSigID;
     int      iSigType;  //1：表示控制；2表示设置
     int      iSigValueType;
     VAR_VALUE vSigValue;
     VAR_VALUE vUpLimit;
     VAR_VALUE vDnLimit;
     VAR_VALUE iStep;
     char     cSigName[MAXLEN_NAME];
     char     cSigUnit[MAXLEN_UNIT];
     char     cEnumText[MAXNUM_SEL][MAXLEN_NAME];
};
typedef struct SettingInfo SET_INFO;

struct PackSetInfo
{
    Head_t stHead;
    int		SetNum;
    SET_INFO	SettingInfo[MAXLEN_SET];
};
typedef struct PackSetInfo PACK_SETINFO;

//对于特殊的设置，iEquipID = -1。
//iSigID = 0		表示sitename
//iSigID = 1		表示时间


//	Wizard-Sitename页（ScreenID=0x050201）
#define SCREEN_ID_WizSiteName           0x050201
//	Wizard-Common（ScreenID=0x050202）
#define SCREEN_ID_WizCommon             0x050202
//	Wizard-Batt（ScreenID=0x050203）
#define SCREEN_ID_WizBatt               0x050203
#define SCREEN_ID_WizCapacity           0x050204
//	Wizard-ECO（ScreenID=0x050204）
#define SCREEN_ID_WizECO                0x050205
//	Wizard-Alarm（ScreenID=0x050205）
#define SCREEN_ID_WizAlarm              0x050206
#define SCREEN_ID_WizCommunicate        0x050207

//	Slave Setting（ScreenID=0x050207）
#define SCREEN_ID_Wdg2P9Cfg_Slave           0x050271
#define SCREEN_ID_Wdg2P9Cfg_Maintenance     0x050272
#define SCREEN_ID_Wdg2P9Cfg_EnergySaving    0x050273
#define SCREEN_ID_Wdg2P9Cfg_AlarmSetting    0x050274
#define SCREEN_ID_Wdg2P9Cfg_BatBasic        0x050275
#define SCREEN_ID_Wdg2P9Cfg_BatCharge       0x050276
#define SCREEN_ID_Wdg2P9Cfg_BatTest         0x050277
#define SCREEN_ID_Wdg2P9Cfg_BatTempComp     0x050278
#define SCREEN_ID_Wdg2P9Cfg_BatBat1         0x050279
#define SCREEN_ID_Wdg2P9Cfg_BatBat2         0x05027A
#define SCREEN_ID_Wdg2P9Cfg_ACSetting       0x05027B
#define SCREEN_ID_Wdg2P9Cfg_LVDSetting      0x05027C
#define SCREEN_ID_Wdg2P9Cfg_RectSetting     0x05027D
#define SCREEN_ID_Wdg2P9Cfg_SysSetting      0x05027E
#define SCREEN_ID_Wdg2P9Cfg_CommSetting     0x05027F
#define SCREEN_ID_Wdg2P9Cfg_OtherSetting    0x050280
//Frank Wu,20131225
#define SCREEN_ID_Wdg2P9Cfg_BatSettingGroup 0x050281


// old cfg
//33．	参数设置页（ScreenID=0x050200）
#define SCREEN_ID_Wdg2P9Cfg             0x050200
//	Other Setting（ScreenID=0x050208）
#define SCREEN_ID_Wdg2P9Cfg_Other       0x050208
//	Batt1 Setting（ScreenID=0x050209）
#define SCREEN_ID_Wdg2P9Cfg_Other_Batt1 0x050209
//	Batt2 Setting（ScreenID=0x05020A）
#define SCREEN_ID_Wdg2P9Cfg_Other_Batt2 0x05020A
#define SCREEN_ID_SetBuzzStatus         0x080000

#endif // DATADEF_H

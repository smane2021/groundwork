/******************************************************************************
文件名：    sysInit.h
功能：      系统初始化、设置接口头文件
作者：      刘金煌
创建日期：  2013年03月25日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef SYSINIT_H
#define SYSINIT_H

#ifdef __cplusplus
extern "C"
{
#endif

#if defined(SYSINIT_LIBRARY)
    #define SYSINITSHARED __attribute__((visibility("default")))
    #define SYSINITHIDDEN __attribute__((visibility("hidden")))
#else
    #define SYSINITSHARED
#endif

#include "pubDef.h"
#include <time.h>

enum DEV_OP
{
	DO_WRITE,
	DO_READ
};

// shared memory
#define SHM_KEY    987456
typedef struct _ShmData
{
    int  fdDriverGlobal; // 驱动 fdbuzz
    bool bSound;         // 要声音？
    int  fdKeypad; // 驱动 Keypad
    //time_t timeClickBtn;   //最近的按键时间
}ShmData;

//SYSINITSHARED int sys_open(void);
SYSINITSHARED int   sys_open(enum DEV_OP op);
SYSINITSHARED int   sys_close(void);
// Buzz
SYSINITSHARED int sys_setBuzz(enum SET_BUZZ buzz, int iType=BUTTON_BEEP);
//声音来源 按键还是告警
SYSINITSHARED int sys_getSoundType();
//是否在响
SYSINITSHARED int sys_getBuzz();
//键盘声音是否要响
SYSINITSHARED int sys_setBuzzKeyboard(bool bSound=true);

// LCD
// LCD_MAX 设置为原来的背光值
SYSINITSHARED int sys_setLCD(enum SET_LCD lcd);
// 返回 enum SET_LCD
SYSINITSHARED int sys_getLCD();

// LED
SYSINITSHARED int sys_setLED(enum SET_LED led);
SYSINITSHARED int sys_getLED();

// FrameBuffer
SYSINITSHARED int sys_actFrameBuffer();
SYSINITSHARED int sys_resetFrameBuffer();
SYSINITSHARED int sys_setLCDGarmar(enum GAMMA_LEVEL);

// keyboard
SYSINITSHARED int sys_setKeyboard(bool bEnable=true);

#define ERROR_SYS_OK               (0)
#define ERROR_SYS_OPEN_SHM_FAILED  (1)
#define ERROR_SYS_OPEN_FAILED      (2)
#define ERROR_SYS_BUZZ_FAILED      (3)
#define ERROR_SYS_LED_FAILED       (4)
#define ERROR_SYS_KEYBOARD_FAILED  (5)


#ifdef __cplusplus
}
#endif

#endif // SYSINIT_H

/******************************************************************************
文件名：    dataSource.h
功能：      数据交互头文件
作者：      刘金煌
创建日期：   2013年3月25日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef DATASOURCE_H
#define DATASOURCE_H

#ifdef __cplusplus
extern "C"
{
#endif

#if defined(DATASOURCE_LIBRARY)
    #define DATASOURCESHARED __attribute__((visibility("default")))
    #define DATASOURCEHIDDEN __attribute__((visibility("hidden")))
#else
    #define DATASOURCESHARED
#endif

#include "dataDef.h"

DATASOURCESHARED int data_open(void);
DATASOURCESHARED int data_close(void);
DATASOURCESHARED int data_sendCmd(CmdItem* pCmd);
DATASOURCESHARED int data_recvData(void** ppData);
DATASOURCESHARED int data_recvData_alarm(void** ppData);
DATASOURCESHARED int data_getDataSync(
        CmdItem* pCmd,
        void** ppData,
        int nSecTimeOut=6
        );
DATASOURCESHARED int data_hasAlarm();
DATASOURCESHARED int data_sendHeartBeat();

#define ERRDS_OK              (0)
#define ERRDS_SEM_OPEN        (1)
#define ERRDS_MAPFILE_OPEN    (2)
#define ERRDS_MAPFILE_MMAP    (3)
#define ERRDS_NODATA          (4)
#define ERRDS_CLOSE           (5)
#define ERRDS_HAVEDATA_TOREAD (6)
#define ERRDS_HASALARM        (7)
#define ERRDS_TIMEOUT         (8)


#ifdef __cplusplus
}
#endif

#endif // DATASOURCE_H

/******************************************************************************
文件名：    utility.h
功能：      打印日志等工具头文件
作者：      刘金煌
创建日期：   2013年3月25日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#ifndef UTILITY_H
#define UTILITY_H

#ifdef __cplusplus
extern "C"
{
#endif

#if defined(UTILITY_LIBRARY)
    #define UTILITYSHARED __attribute__((visibility("default")))
    #define UTILITYHIDDEN __attribute__((visibility("hidden")))
#else
    #define UTILITYSHARED
#endif

#define SHM_KEY_UTILITY 123456
typedef struct _ShmDataUtility
{
	int  nLogClass; // 打印日志等级
} ShmDataUtility;

UTILITYSHARED void util_init(int nClass=-1);
UTILITYSHARED void util_writeLogDeb(const char * format, ...);
UTILITYSHARED void util_writeLog1(const char * format, ...);
UTILITYSHARED void util_writeLog2(const char *file, int line, const char *format, ...);

UTILITYSHARED void printLogDeb(const char *format, ...);
UTILITYSHARED void printLog1(const char *format, ...);
UTILITYSHARED void printLog2(const char *file, int line, const char *format, ...);

#ifdef Q_OS_WIN32
    #define TRACEDEBUG(format...) \
        {printLogDeb(format);}

    #define TRACELOG1(format...) \
        {printLog1(format);}

    #define TRACELOG2(format...) \
        {printLog2(__FILE__, __LINE__, format);}
#else
    #define TRACEDEBUG(format...) \
        {util_writeLogDeb(format);}

    #define TRACELOG1(format...) \
        {util_writeLog1(format);}

    #define TRACELOG2(format...) \
        {util_writeLog2(__FILE__, __LINE__, format);}
#endif

#ifdef __cplusplus
}
#endif

#endif // UTILITY_H

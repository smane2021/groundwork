/**
	功能描述:本工具是用来实现截屏，原理是获取/dev/fb0的内容即截图内容，并根据fb0的参数
			转换成png图片保存。
	编制作者：alpha wang
	创建时间：2018-09-26
*/


#include <QApplication>
#include <QImage>
#include <QStringList>
#include <QString>
#include <QDesktopWidget>

#include <sys/types.h>
#include <sys/stat.h>
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>

#define  DEV_FRAME_BUFF   "/dev/fb0"
//#define  MAX_BUFFER_LEN   (1024*1024*2)

int main(int argc,char **argv)
{
  
  int fd = ::open(DEV_FRAME_BUFF,O_RDONLY);
  if(fd < 0)
  {
      exit(-1);
  }

  struct fb_var_screeninfo vinfo;

  if (ioctl(fd,FBIOGET_VSCREENINFO,&vinfo))
  {
      qDebug("Error reading variable information\n");
      exit(-2);
  }

//  printf("bits_per_pixel:%d\n",vinfo.bits_per_pixel);
//  printf("fb_bitfield red # offset:%d length:%d\n",vinfo.red.offset,vinfo.red.length);
//  printf("fb_bitfield green # offset:%d length:%d\n",vinfo.green.offset,vinfo.green.length);
//  printf("fb_bitfield blue # offset:%d length:%d\n",vinfo.blue.offset,vinfo.blue.length);
//  printf("fb_bitfield transp # offset:%d length:%d\n",vinfo.transp.offset,vinfo.transp.length);


  char szFrameBuffer[320*240*2]={0};
  ::read(fd,szFrameBuffer,320*240*2);
  ::close (fd);

  QString defaultFileName = "untitled.png";
  if(argc == 3)
  {
      defaultFileName=argv[2];
  }

  QImage image((uchar*)szFrameBuffer,320,240,QImage::Format_RGB16);
  image.save (defaultFileName,"PNG");
}